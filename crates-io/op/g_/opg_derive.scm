(define-module (crates-io op g_ opg_derive) #:use-module (crates-io))

(define-public crate-opg_derive-0.0.1 (c (n "opg_derive") (v "0.0.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "18kvjmfkzlbkr6wggnrvymd4kjpnk75cwnxwgscry8hyqynan1hs")))

(define-public crate-opg_derive-0.0.2 (c (n "opg_derive") (v "0.0.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1yy9mqhdr4mx7l15hkxd2b8ra9vp9dvl7zi2xsshs5x2kb53h2zq")))

(define-public crate-opg_derive-0.0.3 (c (n "opg_derive") (v "0.0.3") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0i6ar9majj3nyzqf3s95m2qkig3ygba2wrm2xj1m5l66pyva0pvp")))

(define-public crate-opg_derive-0.0.4 (c (n "opg_derive") (v "0.0.4") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0il6073wbrz0hhh0236lzdyhpkwbvvfyn0xrcmg49wxb2wl2zvw6")))

(define-public crate-opg_derive-0.0.5 (c (n "opg_derive") (v "0.0.5") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0gr0g1hfsiajamip1zcvcsdhds1xjpxiigp13ip37793i4plqs0g")))

(define-public crate-opg_derive-0.0.6 (c (n "opg_derive") (v "0.0.6") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0w9kl0g4ffhzh55al4i1s8iv7fmhj8i47av7paxryr7cbhs8wvfm")))

(define-public crate-opg_derive-0.0.7 (c (n "opg_derive") (v "0.0.7") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "043x43swnr3irsincdn92cdrazazwdyqvy42aj3dqmm1q4rkv2q3")))

(define-public crate-opg_derive-0.0.8 (c (n "opg_derive") (v "0.0.8") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0kg5rl6h2bypwijwyjxk9k3qwv3lgb84gnsm962vk3ib84n5pywg")))

(define-public crate-opg_derive-0.0.9 (c (n "opg_derive") (v "0.0.9") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0ipnpzp2lqap9p9k3zxg5i9sy12zais6m8qpykxhg4hkqll9qwd3")))

(define-public crate-opg_derive-0.0.10 (c (n "opg_derive") (v "0.0.10") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1a67chlbq6k15fmcslj5ss00apdmingb60cgia460n4mxljzsm30")))

(define-public crate-opg_derive-0.0.11 (c (n "opg_derive") (v "0.0.11") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1f140pnhny6ngrhqwrg29asv1q78fkq5n2idaa5s8csmiwz8r586")))

(define-public crate-opg_derive-0.0.12 (c (n "opg_derive") (v "0.0.12") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "16pd6ahviiyc1fqjya2yp6abaxk82ngmw6s2yailhiqmj1bf6520")))

(define-public crate-opg_derive-0.0.13 (c (n "opg_derive") (v "0.0.13") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhzp966a0z5r22qcl67665m66wmzsnr9pg7mfb1kcwrk0252z1c")))

(define-public crate-opg_derive-0.0.14 (c (n "opg_derive") (v "0.0.14") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0jhk1020qgg9xljyki43zd1i06rmg2bqvy4s9ijy0al2p1rr5rsc")))

(define-public crate-opg_derive-0.0.15 (c (n "opg_derive") (v "0.0.15") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0kyjq9dnnykqdvcbn3q99hid070flbxx77g4s1w88sy0nrld1d0i")))

(define-public crate-opg_derive-0.0.16 (c (n "opg_derive") (v "0.0.16") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "09c7qr18c20jrnpcpl4ms86pzmsm2vvdwga0h7949d20mqng6b59")))

(define-public crate-opg_derive-0.0.17 (c (n "opg_derive") (v "0.0.17") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1gpniib9jml6yh9ciqxgplxnfh2xi0h32pdmc7nr7v8vaymy7krj")))

(define-public crate-opg_derive-0.0.18 (c (n "opg_derive") (v "0.0.18") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1ss5wvcxkq59fg0n7lvflx9r4d9cppkb4w13wygbhms3mkf8qhfb")))

(define-public crate-opg_derive-0.0.19 (c (n "opg_derive") (v "0.0.19") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0j0r6k97dlswin1ck6vk6c04960azzv9v1f2n0ng64n6pslbplsv")))

(define-public crate-opg_derive-0.0.20 (c (n "opg_derive") (v "0.0.20") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0wiym89nrqcjv87n6imzkrm4k70qhz2jsya5c39byvx0a458ny2a")))

(define-public crate-opg_derive-0.0.21 (c (n "opg_derive") (v "0.0.21") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "194jksh8xqcpdfcl5r0mxipspfpi6hq9bg2pr37hzqvcb2lm85sj")))

(define-public crate-opg_derive-0.0.22 (c (n "opg_derive") (v "0.0.22") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1lrr6rki2zkscg41z2sx4b1b2y6b34xmld99n9fki7icm7d9nhl7")))

(define-public crate-opg_derive-0.0.23 (c (n "opg_derive") (v "0.0.23") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksm83j5k69sz4fmds9pq22s7pdsw65jkb461j1j0z1lxw5wxgjc")))

(define-public crate-opg_derive-0.0.24 (c (n "opg_derive") (v "0.0.24") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0imr4njwbdryymnyqlziki2fvyh4fjdgzbpq9lvy57dbv3a4j76d")))

(define-public crate-opg_derive-0.0.25 (c (n "opg_derive") (v "0.0.25") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0knf9xykq0kkba55kglcfg6pscszgx2bv1x6qfwaw4ydz8vbrgwl")))

(define-public crate-opg_derive-0.0.26 (c (n "opg_derive") (v "0.0.26") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "12kw8nba59chrnlfhxbvwrmnnl6xd78x6cgm92v4rr55mcws84rv")))

(define-public crate-opg_derive-0.0.27 (c (n "opg_derive") (v "0.0.27") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "14bawssdmghgp41vwgr1n6k6619mlja2dxd9nagl15vmfilw9kl9")))

(define-public crate-opg_derive-0.0.28 (c (n "opg_derive") (v "0.0.28") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0nb2swyin9qz16sax1hlna1gs60j6728mlbqcbjbjxpxpj2vqdi5")))

(define-public crate-opg_derive-0.0.29 (c (n "opg_derive") (v "0.0.29") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0lcmbrdy3sfdjn3w65qa0wcjz8gdb2vda8rdj9h0iwj96ysvzrla")))

(define-public crate-opg_derive-0.0.30 (c (n "opg_derive") (v "0.0.30") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1b9f2b8sqxm96z822lmyln4djzyfndqrbrl1rr798pfl1x73lc3w")))

(define-public crate-opg_derive-0.0.31 (c (n "opg_derive") (v "0.0.31") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0dhg5ydl87jssm5dd56r5cj3hg70xj7pd5pl54hibgb1nkpskj24")))

(define-public crate-opg_derive-0.0.32 (c (n "opg_derive") (v "0.0.32") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0w8p5klj5y5fkpj2nl55zlg7xyspqx2af3c36g6b2p3svb32r8wm")))

(define-public crate-opg_derive-0.0.33 (c (n "opg_derive") (v "0.0.33") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "176rzangk767x2q338km97ni4wycl5mnv5v4m818z90kr7wxj5sh")))

(define-public crate-opg_derive-0.0.34 (c (n "opg_derive") (v "0.0.34") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "132i40flm9k94vyg5vs30cp328rvfbrbnpnxyk1czp1llx9adc6f")))

(define-public crate-opg_derive-0.0.35 (c (n "opg_derive") (v "0.0.35") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "107f4bhzl1gk7cnzai4g5q1c9qadpjwabbrcmiavhfffvf782yh2")))

(define-public crate-opg_derive-0.1.0 (c (n "opg_derive") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "opg") (r "^0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1l29rw5zcjmc55ywxhbfvlr1wics73w40xwah7bcg85hhhnjm2hn")))

