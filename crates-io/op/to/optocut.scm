(define-module (crates-io op to optocut) #:use-module (crates-io))

(define-public crate-optocut-0.1.0 (c (n "optocut") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0l95ylbivi28ddklcf6c3s67n1c2q95j4ypr9kyawzvl1bbm13m1")))

(define-public crate-optocut-0.1.1 (c (n "optocut") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "09159m0450wph820qfd132zzxwy6qa1080ww36bqsfavnw3ik6qz")))

