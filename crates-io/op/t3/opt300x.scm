(define-module (crates-io op t3 opt300x) #:use-module (crates-io))

(define-public crate-opt300x-0.1.0 (c (n "opt300x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "111jqrraaixjmlgwh04h25rarcrlk83j436y9xb2wigpz1kczflg")))

(define-public crate-opt300x-0.1.1 (c (n "opt300x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "01vg3xjc5cflc74k4nqi7ag4sx6xcwgms4prillh2nqzvly5fj53")))

