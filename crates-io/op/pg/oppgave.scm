(define-module (crates-io op pg oppgave) #:use-module (crates-io))

(define-public crate-oppgave-0.1.0 (c (n "oppgave") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "redis") (r "^0.5.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0978k8f9m3na8x7bvcjq1kzf883pki0mvm62ab9sks8infw0a6lp")))

