(define-module (crates-io op ta optargs) #:use-module (crates-io))

(define-public crate-optargs-0.1.0 (c (n "optargs") (v "0.1.0") (d (list (d (n "optargs-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0pis1g8zzw7fnmw2prrkl63af3jq5vg2bjrqgzak501ml8ll74k7")))

(define-public crate-optargs-0.1.1 (c (n "optargs") (v "0.1.1") (d (list (d (n "optargs-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0zifgr4h9y35902v63xyi029w6q8p31nr1r2krph1gsd6155rziz")))

(define-public crate-optargs-0.1.2 (c (n "optargs") (v "0.1.2") (d (list (d (n "optargs-macro") (r "^0.1.1") (d #t) (k 0)))) (h "059l631lgdky3ffmrpyyxvavrdc1d0bslhik12crndwarb2kkivw")))

