(define-module (crates-io op ta optarg2chain) #:use-module (crates-io))

(define-public crate-optarg2chain-0.1.0 (c (n "optarg2chain") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1xkwllhpx0d0fs4q6sd1d9irqlgywmqimm298vz8m5nvs75vhpr3")))

