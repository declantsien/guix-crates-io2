(define-module (crates-io lr pp lrpp) #:use-module (crates-io))

(define-public crate-lrpp-0.1.0 (c (n "lrpp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0k0xrriqqbp6jsgi6jm9qxq5m8vg89fhgw86vwsf6prnjd782igh") (y #t)))

(define-public crate-lrpp-0.1.1 (c (n "lrpp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1p5nkmwfbwp3b5rpiy4q3c69d8h7vgr7gqhbdc9jjgqmzrqpi9yf") (y #t)))

(define-public crate-lrpp-0.1.2 (c (n "lrpp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "09cs8q29qsv2ak823d4xzsnh0cchp7wir9l2i4gyy7q8600wsccp") (y #t)))

