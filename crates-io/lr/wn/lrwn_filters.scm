(define-module (crates-io lr wn lrwn_filters) #:use-module (crates-io))

(define-public crate-lrwn_filters-4.3.2 (c (n "lrwn_filters") (v "4.3.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n9m6k59dyxnb2vnf82k5wgy2yz68h7nl049968b588bjda4makd") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lrwn_filters-4.3.3-test.1 (c (n "lrwn_filters") (v "4.3.3-test.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pbd9jlqknywjn73rlnl2gpaq33x2b5dmv4zl2d8qbcxp4d3ss5y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lrwn_filters-4.4.3 (c (n "lrwn_filters") (v "4.4.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17rj4y7g8vap4fzqgdy6djh1agw9qvj1r0a81rkmr7x4kw9j73n3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lrwn_filters-4.5.1 (c (n "lrwn_filters") (v "4.5.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09yv6sk2wf5rax19q7nc8xpgp3w2kj60gmmrmc7312gdj2xdcik3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lrwn_filters-4.6.0 (c (n "lrwn_filters") (v "4.6.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dzxfl7n97z5npqgpkcr3pm1qyrq0q5jnl0ilzlhsd6vzkdjwpkp") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lrwn_filters-4.7.0 (c (n "lrwn_filters") (v "4.7.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zgm9s5z5jx3ppgmj71qqafipzm5zg096jdqwywbvgqlrfi22pj2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

