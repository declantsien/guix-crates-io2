(define-module (crates-io lr c- lrc-nom) #:use-module (crates-io))

(define-public crate-lrc-nom-0.1.0 (c (n "lrc-nom") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1sbjfwm0ymkxsyxji3sh69akcdh9lgxcph9z7kmz4640nm0vmck7")))

(define-public crate-lrc-nom-0.1.1 (c (n "lrc-nom") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1q26qmcc5ybfw3gpvwdnwzg0nidf4icm46n4imbrg483f3y5sdpb")))

(define-public crate-lrc-nom-0.1.2 (c (n "lrc-nom") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1yrxgpnx49qc4pmjh6d4db4pzv08qs3q08sm8a7xgdkpl9qdjznd")))

(define-public crate-lrc-nom-0.2.0 (c (n "lrc-nom") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0wd6ssdismkxsqaq2379jfsk3k7sk3cmvyq0whik0qdr65764yyb")))

(define-public crate-lrc-nom-0.3.0 (c (n "lrc-nom") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.29.1") (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.29.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0w2gb1gvmm5ap53g0ln0b53vjs615a9pmhs7y9y7rzdw4y975yx0")))

