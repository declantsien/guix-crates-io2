(define-module (crates-io lr -c lr-cw-ownable) #:use-module (crates-io))

(define-public crate-lr-cw-ownable-2.0.0 (c (n "lr-cw-ownable") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-address-like") (r "^1.0.4") (d #t) (k 0)) (d (n "cw-ownable-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wpapsyipda9a4aya4q9msqb820xky4a5n1gf2fvz837wk0cv0p9") (y #t)))

