(define-module (crates-io lr um lrumap) #:use-module (crates-io))

(define-public crate-lrumap-0.0.0-reserve.0 (c (n "lrumap") (v "0.0.0-reserve.0") (h "11ji2ip1cki2sl2aaidqfiycb1gmpb8k0q4l4hd5lq1vykmv0a66")))

(define-public crate-lrumap-0.1.0 (c (n "lrumap") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)))) (h "17cypwfp8ynz6wyrbxawd3k6910dqm5fgjvx287602criy7cwl8j")))

