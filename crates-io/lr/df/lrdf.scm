(define-module (crates-io lr df lrdf) #:use-module (crates-io))

(define-public crate-lrdf-0.2.0 (c (n "lrdf") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "fastx") (r "^0.2.0") (d #t) (k 0)) (d (n "permutation") (r "^0.4.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vin8sbhrlqi02zigdpy9mnbwzkzx3sf264f36k8vwm8m1yvr35z")))

