(define-module (crates-io lr pc lrpc-macros) #:use-module (crates-io))

(define-public crate-lrpc-macros-0.1.0 (c (n "lrpc-macros") (v "0.1.0") (h "00id51s7cb6whih8wxhj772akpj3khzxjbfyv0sbvgyyjlc1v3jw")))

(define-public crate-lrpc-macros-1.0.0 (c (n "lrpc-macros") (v "1.0.0") (h "1vk4za0my0gby94yaw09n5sx7bniz9f5qlh7d9xxi4qcv0clkpxi")))

