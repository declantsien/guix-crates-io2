(define-module (crates-io lr pc lrpc) #:use-module (crates-io))

(define-public crate-lrpc-0.1.0 (c (n "lrpc") (v "0.1.0") (d (list (d (n "lrpc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "04hvc22xb944s626fr72zvvsxddfjbrg8y8mqvsj885yw166qlvs")))

(define-public crate-lrpc-0.1.1 (c (n "lrpc") (v "0.1.1") (d (list (d (n "lrpc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1pmk02hb9wnhxsidm95ciddjcn23znxnhvzng26wrfh86501vd0y")))

(define-public crate-lrpc-1.0.0 (c (n "lrpc") (v "1.0.0") (d (list (d (n "lrpc-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1hk4n9vl0sskrm4yybb93yjwcpm58fry79s7ndjmhvw3m1vmas67")))

(define-public crate-lrpc-1.1.0 (c (n "lrpc") (v "1.1.0") (d (list (d (n "lrpc-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1vv68zxicdygj16fxw9lg0an8ibjspi5jqmrafx8q68ck9q3hmbr")))

