(define-module (crates-io lr _p lr_parser) #:use-module (crates-io))

(define-public crate-lr_parser-0.1.0 (c (n "lr_parser") (v "0.1.0") (h "1v4dcgnl4pykwc4hk0qqbfhshx2vlrv3hbkg7gv9cbpcwxh8jfyg")))

(define-public crate-lr_parser-0.1.1 (c (n "lr_parser") (v "0.1.1") (h "0ik68k6wng8y0ms35ni5mvyina9ky1y4pjqk70cqrhx29i29rikk")))

(define-public crate-lr_parser-0.1.2 (c (n "lr_parser") (v "0.1.2") (h "0xc3fp0np5nhbh2lsim9r3aksvd3nj1jfasjrh4q9mrpidbwds5a")))

