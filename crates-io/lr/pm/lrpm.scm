(define-module (crates-io lr pm lrpm) #:use-module (crates-io))

(define-public crate-lrpm-0.1.0 (c (n "lrpm") (v "0.1.0") (d (list (d (n "clearscreen") (r "^3.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive" "serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (f (quote ("indexmap" "preserve_order" "alloc" "arbitrary_precision" "float_roundtrip" "raw_value" "unbounded_depth"))) (d #t) (k 0)) (d (n "termimad") (r "^0.29.2") (d #t) (k 0)))) (h "00pbssgd49b3j8lfbzfcf33mxpxvxsx9jl8wi2vhq8rqp2ivsxiq")))

