(define-module (crates-io lr pe lrpeg) #:use-module (crates-io))

(define-public crate-lrpeg-0.1.0 (c (n "lrpeg") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1w6ykrcqdsf6dk4mricdrdslmsv10ig8cpsqrh6jvhkpywvibv4l") (y #t)))

(define-public crate-lrpeg-0.2.0 (c (n "lrpeg") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "188gx5pqf2fff0cynrq7l9p8594rz113ksc31gczcs75k1hfrafn") (y #t)))

(define-public crate-lrpeg-0.3.0 (c (n "lrpeg") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1xxmv0ylmbn2c5amygadbhcwfir7l280xyr9lkwgh021lrw8mzih") (y #t)))

(define-public crate-lrpeg-0.4.0 (c (n "lrpeg") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0z64jwf09hgbnhvmgd0q7d4shr41cwg6w4i8pzfl8i2glk15lav1") (y #t)))

(define-public crate-lrpeg-0.4.1 (c (n "lrpeg") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1jznjsjwlp1j4pg25p7k0jwfijh8d3iwhk3h1aiskcjag3vym4av") (y #t)))

