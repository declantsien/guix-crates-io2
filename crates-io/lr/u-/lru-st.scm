(define-module (crates-io lr u- lru-st) #:use-module (crates-io))

(define-public crate-lru-st-0.1.0 (c (n "lru-st") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "02riz4f0lh23ifallwzk3zjfdz9gc8fd5j8d649q22kwnvqvdzn7") (f (quote (("sync"))))))

(define-public crate-lru-st-0.1.1 (c (n "lru-st") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0zr712bkjbv2l62zr84nxnn39gcdfypy6nqcd37p9gd3fpblgsgk") (f (quote (("sync"))))))

(define-public crate-lru-st-0.1.2 (c (n "lru-st") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0bzw4p0r387xhmnamc03sz81qj2298d017p7yakps23dwggh1xn2") (f (quote (("sync")))) (r "1.60.0")))

