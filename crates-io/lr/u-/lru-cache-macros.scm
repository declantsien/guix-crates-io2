(define-module (crates-io lr u- lru-cache-macros) #:use-module (crates-io))

(define-public crate-lru-cache-macros-0.1.0 (c (n "lru-cache-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "12nlbzw97agvkb7yvh4smdy0w034rnqw8dvm4p4s0fkkf11l1938")))

(define-public crate-lru-cache-macros-0.2.0 (c (n "lru-cache-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "085qdfnlj64x7kr3db5asz0y1zzn1sl0lggk1jrb94wz19zwldzc")))

(define-public crate-lru-cache-macros-0.2.1 (c (n "lru-cache-macros") (v "0.2.1") (d (list (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "11g5cl2hqs4dmjkq8vzpqnhxrnqhava824vwi711zhfm29lc1nkc")))

(define-public crate-lru-cache-macros-0.2.2 (c (n "lru-cache-macros") (v "0.2.2") (d (list (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w2y33fnj1hn82wf13kqqcin7wmjhnssgv0lfx2b36a19x0ba7aq")))

(define-public crate-lru-cache-macros-0.2.3 (c (n "lru-cache-macros") (v "0.2.3") (d (list (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r7lx440vljbs4v291gfkp2azzcg8d18q3ch7xnmv12c2j73zmkc")))

(define-public crate-lru-cache-macros-0.3.0 (c (n "lru-cache-macros") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z1x2vnpd34mnm9vlicf24f5g8gl3v86ziv9jv457rd2f2msv79l")))

(define-public crate-lru-cache-macros-0.3.1 (c (n "lru-cache-macros") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hdhkkqisb2683psbn2aaihijyl341mg27jpnj2rrphqyzga83mx")))

