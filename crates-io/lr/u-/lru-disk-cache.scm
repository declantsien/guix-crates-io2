(define-module (crates-io lr u- lru-disk-cache) #:use-module (crates-io))

(define-public crate-lru-disk-cache-0.1.0 (c (n "lru-disk-cache") (v "0.1.0") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0dazvgavjqdpp1r7w17in545vdil4sbpy0gbgjymxw53mcb7ibgd") (y #t)))

(define-public crate-lru-disk-cache-0.2.0 (c (n "lru-disk-cache") (v "0.2.0") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1l234scphrks6s862m4fj59mx4ym0vfz2j05ry37bkwi1bq4jfn0") (y #t)))

(define-public crate-lru-disk-cache-0.3.0 (c (n "lru-disk-cache") (v "0.3.0") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "01n3himb10wvvx6m0kkx59p1y0gxifj4ry4rm1md43j040gz4148") (y #t)))

(define-public crate-lru-disk-cache-0.4.0 (c (n "lru-disk-cache") (v "0.4.0") (d (list (d (n "filetime") (r "^0.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0w02g205nd95vlfi09pnnq58azrff2iq8c55s7h234n4dplxf4gy") (y #t)))

(define-public crate-lru-disk-cache-0.4.1 (c (n "lru-disk-cache") (v "0.4.1") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "10sqnq9lh608293a1ygfypqdhmk8hv0a33bkxifsp6c7yqq95znr") (y #t)))

