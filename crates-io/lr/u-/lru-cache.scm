(define-module (crates-io lr u- lru-cache) #:use-module (crates-io))

(define-public crate-lru-cache-0.0.1 (c (n "lru-cache") (v "0.0.1") (d (list (d (n "linked-hash-map") (r "*") (d #t) (k 0)))) (h "1snsvs02wq5f5z3da4xhwc85wnqk8mdv7cfwr2crdmbyis8ny4d5")))

(define-public crate-lru-cache-0.0.2 (c (n "lru-cache") (v "0.0.2") (d (list (d (n "linked-hash-map") (r "*") (d #t) (k 0)))) (h "1jars4rd35wj71njp7hbkzqsshif722rf2sza0lkd7s7jgxw3q08")))

(define-public crate-lru-cache-0.0.3 (c (n "lru-cache") (v "0.0.3") (d (list (d (n "linked-hash-map") (r "*") (d #t) (k 0)))) (h "1z27sdy2n6n0f49clm7w9lwpkjd0s5pacyxq3zca8y0b3m71s6c1")))

(define-public crate-lru-cache-0.0.4 (c (n "lru-cache") (v "0.0.4") (d (list (d (n "linked-hash-map") (r "*") (d #t) (k 0)))) (h "1jyx52nk2y1wwyqli5dvzyxcrx089k81kk7wd4j0fh221ajhr7c8")))

(define-public crate-lru-cache-0.0.5 (c (n "lru-cache") (v "0.0.5") (d (list (d (n "linked-hash-map") (r "*") (d #t) (k 0)))) (h "1wdxwv0m44dpbi8smvfwiw8by39r5r4bm581ia57ysmgcv6jvzkd")))

(define-public crate-lru-cache-0.0.6 (c (n "lru-cache") (v "0.0.6") (d (list (d (n "linked-hash-map") (r "^0.0.8") (d #t) (k 0)))) (h "1fh82crswby00fwlvprzfiv8apwsz5ryci9ka8mygw7wqm1b41s3") (y #t)))

(define-public crate-lru-cache-0.0.7 (c (n "lru-cache") (v "0.0.7") (d (list (d (n "linked-hash-map") (r "^0.0.9") (d #t) (k 0)))) (h "135z7hzvqnk5lj614klwfyf7rhy039z20hqh7gw5s54zbp5hvma2")))

(define-public crate-lru-cache-0.1.0 (c (n "lru-cache") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.2") (d #t) (k 0)))) (h "1dzxdmyagcg8i6lci67y58gyw0skdbzs6asr7h3g3g02rggs8vv5")))

(define-public crate-lru-cache-0.1.1 (c (n "lru-cache") (v "0.1.1") (d (list (d (n "heapsize") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.4.2") (d #t) (k 0)))) (h "08fwa341bpibpfgy1qaarmcvq44dp1n8f9z2ykjrqwkgy1zzy1jd") (f (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

(define-public crate-lru-cache-0.1.2 (c (n "lru-cache") (v "0.1.2") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "071viv6g2p3akwqmfb3c8vsycs5n7kr17b70l7la071jv0d4zqii") (f (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

