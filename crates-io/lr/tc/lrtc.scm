(define-module (crates-io lr tc lrtc) #:use-module (crates-io))

(define-public crate-lrtc-0.1.0 (c (n "lrtc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0as8pgh4hrwihr48zimk5sdkjvpfkfkd22v7q12nfh6l6fd1pz8d") (r "1.71")))

(define-public crate-lrtc-0.1.1 (c (n "lrtc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1qvzq39rnhn3f246nl0262n6mphjm099cs8k5l68b1rxpyy0c9jn") (r "1.71")))

(define-public crate-lrtc-0.1.2 (c (n "lrtc") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "005bs7ilhl26ds8n1vbzsianm1gfb4n6fxpp0p2l8vfl6rk6m8xg") (r "1.70")))

(define-public crate-lrtc-0.1.3 (c (n "lrtc") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "1cnsys1w2zcivnfs0c0h1w30s3y7awfg0wzyimrbx2xjnx5qqaml") (r "1.70")))

(define-public crate-lrtc-0.1.4 (c (n "lrtc") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "csv") (r "^1.2.2") (d #t) (k 2)) (d (n "flate2") (r "^1.0.17") (f (quote ("zlib-ng"))) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (d #t) (k 0)))) (h "0jqnlcmfxry952kcfskhd1ah0bnj69rnv3nfc30iq7ipm2dj4s65") (r "1.70")))

