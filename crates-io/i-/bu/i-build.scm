(define-module (crates-io i- bu i-build) #:use-module (crates-io))

(define-public crate-i-build-1.0.0-beta.3 (c (n "i-build") (v "1.0.0-beta.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "i-codegen") (r "^1.0.0-beta.3") (o #t) (d #t) (k 0)) (d (n "i-utils") (r "^1.0.0-beta.0") (f (quote ("build"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winres") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1vqgiz3rlds86a58yw3w2009c0gihdrxzgfa91az9ndjzr93d69c") (f (quote (("codegen" "i-codegen"))))))

