(define-module (crates-io i- sl i-slint-common) #:use-module (crates-io))

(define-public crate-i-slint-common-0.2.0 (c (n "i-slint-common") (v "0.2.0") (h "18q6l8fviw5azipiw48f14rrjdpm3zngkp59b7mdvki0aiwzv07n")))

(define-public crate-i-slint-common-0.2.1 (c (n "i-slint-common") (v "0.2.1") (h "0igyksgihnpnwsq4fn4z00767735rv62jcywmq5v18nbag5f636n")))

(define-public crate-i-slint-common-0.2.2 (c (n "i-slint-common") (v "0.2.2") (h "1nmsxy4ixmxm4yasa1dzbv6212nfzgi8faa0rmb2lg37i060lyi3")))

(define-public crate-i-slint-common-0.2.3 (c (n "i-slint-common") (v "0.2.3") (h "0z29rg3jchshqzcxqnbhnlyywrp4zq84yr9fi1kq0ykvmrj5q0gq")))

(define-public crate-i-slint-common-0.2.4 (c (n "i-slint-common") (v "0.2.4") (h "0fgipg03wsdi7jwz19riipg5d7r0x1cl6dj8bmxjwl8fvyf5066f")))

(define-public crate-i-slint-common-0.2.5 (c (n "i-slint-common") (v "0.2.5") (h "0584xn5n2anv7qci359n02c7jcb2yg85ibz5dzzy7bah825b26a7")))

(define-public crate-i-slint-common-0.3.0 (c (n "i-slint-common") (v "0.3.0") (h "0pral7h3z1avy6s329x9hnb69ns9cprc0z5mrg1h4brgs48vr0c8")))

(define-public crate-i-slint-common-0.3.1 (c (n "i-slint-common") (v "0.3.1") (h "0ph4pgy4vvg590q79m3j3a4rnbw34900xw307806c5wy3f7bmg8y")))

(define-public crate-i-slint-common-0.3.2 (c (n "i-slint-common") (v "0.3.2") (h "1f0m5k21g8mc5gvbwa1vbg00z58vkc144lj6bahv7xcs26fr6fhx")))

(define-public crate-i-slint-common-0.3.3 (c (n "i-slint-common") (v "0.3.3") (h "0fhfvax8kaa65d09yaqbpkd64qf0rnmd1hadbl645h3vczsi8jsy")))

(define-public crate-i-slint-common-0.3.4 (c (n "i-slint-common") (v "0.3.4") (h "0dz4ya4h2bgclrfvmk762zxkyglcv1n6f7a8kqnn2rzvjlaz9xk3")))

(define-public crate-i-slint-common-0.3.5 (c (n "i-slint-common") (v "0.3.5") (h "0d5ihm7qnh99kd7031z4fp0wj1z7z22xp03jsrad1vglfks1v7iq")))

(define-public crate-i-slint-common-1.0.0 (c (n "i-slint-common") (v "1.0.0") (h "1955crksplxqj11n12bfr5klbvy4w6vwv40qm1iypgcxkzh7rrq3")))

(define-public crate-i-slint-common-1.0.1 (c (n "i-slint-common") (v "1.0.1") (h "1sgs1inxi58bqsd1y98r429sc404ylxw92p21xqj9p3jii4cy49n")))

(define-public crate-i-slint-common-1.0.2 (c (n "i-slint-common") (v "1.0.2") (h "1v3z0kmlic55yfqx8miqfb2hmpmxk6yb804drrnh2h2v5dgxql05")))

(define-public crate-i-slint-common-1.1.0 (c (n "i-slint-common") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "1xpv56j61gaa63jlpbd7jb4zy9bh9ybykzbkfyznzlaps843vxbw") (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if"))))))

(define-public crate-i-slint-common-1.1.1 (c (n "i-slint-common") (v "1.1.1") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "0xyqzr5sg6gyxx19xygj8qc6krmk0ygdbfgsxk8ilsir8rh4rnb6") (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if"))))))

(define-public crate-i-slint-common-1.2.0 (c (n "i-slint-common") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "0lzcdlhwllcyp1mp9llb6w7839jgd9ik7fsw1hbf440rlvz9j8q5") (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if"))))))

(define-public crate-i-slint-common-1.2.1 (c (n "i-slint-common") (v "1.2.1") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "1xdcn47f3sfkmr41azj33ih453k67fi2z8ds9i98as2960hll9mc") (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if"))))))

(define-public crate-i-slint-common-1.2.2 (c (n "i-slint-common") (v "1.2.2") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "0i10klnkvdbrwrnpmra8b7wc0qv0wv36f5shnw37xwlbws2g9np7") (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if"))))))

(define-public crate-i-slint-common-1.3.0 (c (n "i-slint-common") (v "1.3.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (o #t) (k 0)) (d (n "fontdb") (r "^0.14.1") (f (quote ("fontconfig" "memmap"))) (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "1ga097nxn8alvnnv1068scrzx1wm8wyyv7yjkjh4nblk21vmj6hq") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.3.1 (c (n "i-slint-common") (v "1.3.1") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.15.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.15.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "1121ddbw0hq00bjvi4fk98mdj73g7gzblsbiiczgv897mrpygz12") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.3.2 (c (n "i-slint-common") (v "1.3.2") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.15.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.15.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\")))") (k 0)))) (h "0r9qsh69yq00yzn3wlgkhbsc0inf77w1mnhx0wkd8h1ikhdvyrk6") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.4.0 (c (n "i-slint-common") (v "1.4.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\", target_os = \"android\")))") (k 0)))) (h "15jnvmfvqhv2a1j5nffdnis8fl3wcii7jmaaxckv9f2ahfsc2503") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.4.1 (c (n "i-slint-common") (v "1.4.1") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\", target_os = \"android\")))") (k 0)))) (h "1300mdkv21795awj2clgfmy99vqsilb2i9spw1nbg254zn1943jm") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.5.0 (c (n "i-slint-common") (v "1.5.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\", target_os = \"android\")))") (k 0)))) (h "0r2klb66873f78ypkr17bhmjwfw8bf9ma6jyslmdlda0ylnr769p") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.5.1 (c (n "i-slint-common") (v "1.5.1") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\", target_os = \"android\")))") (k 0)))) (h "0w719ifimnjjj46ajkvg5n1vdnmcvxh4g5aa15lc5crzkkknszfn") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.70")))

(define-public crate-i-slint-common-1.6.0 (c (n "i-slint-common") (v "1.6.0") (d (list (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (o #t) (d #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (k 0)) (d (n "fontdb") (r "^0.16.0") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (t "cfg(not(any(target_family = \"windows\", target_os = \"macos\", target_os = \"ios\", target_arch = \"wasm32\", target_os = \"android\")))") (k 0)))) (h "1cfaknzqp8l6lw52yi60bnnxr8nwkyi5g23j1kx6x9q6y3j0qxwq") (f (quote (("default")))) (s 2) (e (quote (("shared-fontdb" "dep:fontdb" "dep:libloading" "derive_more" "cfg-if")))) (r "1.73")))

