(define-module (crates-io i- sl i-slint-core-macros) #:use-module (crates-io))

(define-public crate-i-slint-core-macros-0.2.0 (c (n "i-slint-core-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yfslzzd5dqcpkj02nfwfvrqdz667rpld49ma1g7c94097kmg736")))

(define-public crate-i-slint-core-macros-0.2.1 (c (n "i-slint-core-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08djl5ypcpjbafwy04ydqzkg5jjkf2sv0r108k2kxyyck3rj4crn")))

(define-public crate-i-slint-core-macros-0.2.2 (c (n "i-slint-core-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ldmjf8jj9kbrzwgznj015fl7aaqz6yqyhwfqwpkb5kq7dbx2fgk")))

(define-public crate-i-slint-core-macros-0.2.3 (c (n "i-slint-core-macros") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ff7sa68jc9g8wxqzqwmfkm08abqxm2nr3yz15zpjc668ayjyj2c")))

(define-public crate-i-slint-core-macros-0.2.4 (c (n "i-slint-core-macros") (v "0.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "087g5zc98xgfr5gv30assvb0bqchiqc62bfpbba6fjg9gnwb2xih")))

(define-public crate-i-slint-core-macros-0.2.5 (c (n "i-slint-core-macros") (v "0.2.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1wdjfnpq7vlfv8idi12czhcjg3hwcilrp4ckzi82r3i0fgy1aj")))

(define-public crate-i-slint-core-macros-0.3.0 (c (n "i-slint-core-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ijz8p0zp7bvpdhzh5pamnw3dg8mxjqbgryy4v7nq9041da4xh5s")))

(define-public crate-i-slint-core-macros-0.3.1 (c (n "i-slint-core-macros") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h5ms2gq35dl9r2l7dwb5wbs5gd4rrf86w38kq5f6s0ri72axhx0")))

(define-public crate-i-slint-core-macros-0.3.2 (c (n "i-slint-core-macros") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pc630yk1hic4w22hi0ifhbmdb67br259075lid5f22628pz36ii")))

(define-public crate-i-slint-core-macros-0.3.3 (c (n "i-slint-core-macros") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0665vlrf0pagpb58y9wdd58kidn6p5wdzscd47p25jd978r223qx")))

(define-public crate-i-slint-core-macros-0.3.4 (c (n "i-slint-core-macros") (v "0.3.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04q394hn6c9v3ahwxwfxlnacdi456d259gc8h40rz498zw8dpyrp")))

(define-public crate-i-slint-core-macros-0.3.5 (c (n "i-slint-core-macros") (v "0.3.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cj6mal3irmig9sgay22pip7nr1j18wbxk8y9akgs434r93s3y6p")))

(define-public crate-i-slint-core-macros-1.0.0 (c (n "i-slint-core-macros") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jxdk7m4pnfkwrlv9zhg1b2wab8s2l1gvqmpqb1sfs1xcv7n9jr2")))

(define-public crate-i-slint-core-macros-1.0.1 (c (n "i-slint-core-macros") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1imaalc5j2xgywznk3i2a4i17f77p5cm3zbi0s4vdibrbx1zha6f")))

(define-public crate-i-slint-core-macros-1.0.2 (c (n "i-slint-core-macros") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0s8gjw80pxd2cm2v2zllmqm19amvnl7lisvr4rcp8c23xpnsh1iw")))

(define-public crate-i-slint-core-macros-1.1.0 (c (n "i-slint-core-macros") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08wv5myjdm2sny7q5s9hyp53h81agms1xsizylwgiqbsrkizha7k")))

(define-public crate-i-slint-core-macros-1.1.1 (c (n "i-slint-core-macros") (v "1.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04wwd69z62i39hb600pk5p1mwk6ccc9x5xfi58iybmm8j3yn5h9h")))

(define-public crate-i-slint-core-macros-1.2.0 (c (n "i-slint-core-macros") (v "1.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1b9bprszrww62xq63ybfsffc0cc2spb85hp971abivp045ldvi31")))

(define-public crate-i-slint-core-macros-1.2.1 (c (n "i-slint-core-macros") (v "1.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0crc4sjdj7plk3vnzgj0d7k4910li6agd16kjkaixcs7wlgi01bd")))

(define-public crate-i-slint-core-macros-1.2.2 (c (n "i-slint-core-macros") (v "1.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05y1r7bckg3910qcw3rrrl7qamc1ibimn7hp3yzw4hpwx7qid932")))

(define-public crate-i-slint-core-macros-1.3.0 (c (n "i-slint-core-macros") (v "1.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1vr62sl44zh7zxi2p5pkds9rc92v194r7wqkx2782paacjhq5mf8") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.3.1 (c (n "i-slint-core-macros") (v "1.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05724hpm6mps4wy4rslkydnbqhs5kwlj52j2qk6ph612mqvlnwp8") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.3.2 (c (n "i-slint-core-macros") (v "1.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "04zdnwh949z9f24x18bf0i51qk2aps12w3q6lpfmq19gcjlxnj4i") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.4.0 (c (n "i-slint-core-macros") (v "1.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0vpymijfcy6b2imlfgaa34z3sj1id3vchjlzjn9rpy86wzsgqcj7") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.4.1 (c (n "i-slint-core-macros") (v "1.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1n2szc1xqh3l1zr1zg64zqwfh86r87qvc0g1mf0h2rpd4564wajg") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.5.0 (c (n "i-slint-core-macros") (v "1.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13jdl8rlb6pkhhd8ajlxizigri760dwn130ifv48fj8d86vd67kb") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.5.1 (c (n "i-slint-core-macros") (v "1.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1db9jvwym88scbcjf6rcy21rsvinp8hysjhdav8zhdp0f1ilfgch") (f (quote (("default")))) (r "1.70")))

(define-public crate-i-slint-core-macros-1.6.0 (c (n "i-slint-core-macros") (v "1.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0jy2d70mkbc6bn28r93jq1v7w53hsz0b8jsars2hz4gkna7d3ghj") (f (quote (("default")))) (r "1.73")))

