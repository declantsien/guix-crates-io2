(define-module (crates-io i- co i-codegen) #:use-module (crates-io))

(define-public crate-i-codegen-1.0.0-beta.3 (c (n "i-codegen") (v "1.0.0-beta.3") (d (list (d (n "blake3") (r "^1.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "i-utils") (r "^1.0.0-beta.2") (f (quote ("build"))) (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "1mnrrgfrp68lsi89p8r26vjvm1mpj1385g1wpgnigiphfb0hk0gk")))

