(define-module (crates-io i- ma i-macros) #:use-module (crates-io))

(define-public crate-i-macros-1.0.0-beta.4 (c (n "i-macros") (v "1.0.0-beta.4") (d (list (d (n "i-codegen") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05y4jr24ivl0nlm1i63vmcipb9p76zxlw290z9cxv9m3dwk8js16") (f (quote (("custom-protocol"))))))

