(define-module (crates-io i- ut i-utils) #:use-module (crates-io))

(define-public crate-i-utils-1.0.0-beta.2 (c (n "i-utils") (v "1.0.0-beta.2") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "zstd") (r "^0.9") (d #t) (k 0)))) (h "0c4nz6zbgpvnbmawlrlj87gzcdxp50mbvmiaffqkdd64y773sbv1") (f (quote (("build" "proc-macro2" "quote"))))))

