(define-module (crates-io i- wa i-wanna-build) #:use-module (crates-io))

(define-public crate-i-wanna-build-0.1.0 (c (n "i-wanna-build") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "0x2ny9f5fnfgzxll5z0c94xd3rc0p3awkz9q90j2cxbll06b0x23")))

