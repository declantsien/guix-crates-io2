(define-module (crates-io vm w_ vmw_backdoor) #:use-module (crates-io))

(define-public crate-vmw_backdoor-0.1.0 (c (n "vmw_backdoor") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ln6y5gq2vsq9cc2gwz1j61h7vf2mfhlfrf53icbp2cnz6a05hqa")))

(define-public crate-vmw_backdoor-0.1.1 (c (n "vmw_backdoor") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wl05zxfxx39b2q5v358cvc99mkpsbmmbbsqjc8k4mw1h6k6n14n")))

(define-public crate-vmw_backdoor-0.1.2 (c (n "vmw_backdoor") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g9m4qnl2j36bq7c9smnaj9cyi5sgjlz4pzzlzn7rkikf5i7dfpm")))

(define-public crate-vmw_backdoor-0.1.3 (c (n "vmw_backdoor") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gjqq28haq3q6qpdlh9dm266fh23id4q3mgmnm7hvgyxspzqb8k2")))

(define-public crate-vmw_backdoor-0.2.0 (c (n "vmw_backdoor") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rvq37lsl0wadarbmg42jjzwmshz64i2m9q0bljkngplbbi768kq")))

(define-public crate-vmw_backdoor-0.2.1 (c (n "vmw_backdoor") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cz0vyccbk9y03rqlm77r27dfl0nvlnqra3vyx6h7z958ckdpnjr")))

(define-public crate-vmw_backdoor-0.2.2 (c (n "vmw_backdoor") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08x59q277sidwgwp3jk3xl2w0wshlm8894zwkj2xmdzji8lb2xwy") (y #t)))

(define-public crate-vmw_backdoor-0.2.3 (c (n "vmw_backdoor") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dyq10igdzy99lvspmj1nly3k66x3y4pnmawkkqni4gz9qblvz88")))

(define-public crate-vmw_backdoor-0.2.4 (c (n "vmw_backdoor") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "errno") (r ">=0.2, <0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n8fsgcpnlk081fayrvmlp9509jq1iyyx8glni2lq1qk4xj0wkrn") (r "1.56.0")))

