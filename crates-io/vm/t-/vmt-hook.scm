(define-module (crates-io vm t- vmt-hook) #:use-module (crates-io))

(define-public crate-vmt-hook-0.1.0 (c (n "vmt-hook") (v "0.1.0") (h "1d2r2gccdxl8r0vvvr1r9kkv7l8hirrx6vvak7dmjn0y9jc16i2l")))

(define-public crate-vmt-hook-0.2.0 (c (n "vmt-hook") (v "0.2.0") (h "0pv7fbay3vym863c4yfk7ccrc5kqlzavazn1933jma9p96vf9av7")))

