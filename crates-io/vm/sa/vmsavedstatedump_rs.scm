(define-module (crates-io vm sa vmsavedstatedump_rs) #:use-module (crates-io))

(define-public crate-vmsavedstatedump_rs-0.1.0 (c (n "vmsavedstatedump_rs") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0fi0hqc4z429r7gvcpv5rh4706xi3bkhy2m88d94lj5nrb1ia03i") (y #t)))

(define-public crate-vmsavedstatedump_rs-0.1.1 (c (n "vmsavedstatedump_rs") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0yfblzgmdc641la0zi5nzky8021qh1ag9c9fdnvqbmf8vr1nin73") (y #t)))

(define-public crate-vmsavedstatedump_rs-0.1.2 (c (n "vmsavedstatedump_rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1rkx477n6yq4nzqwa5b0w51q87ayhv7lmfw97767frzsd7f25808") (y #t)))

(define-public crate-vmsavedstatedump_rs-0.1.3 (c (n "vmsavedstatedump_rs") (v "0.1.3") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winbase"))) (d #t) (k 0)))) (h "0zip20ckqhaij2zmpnlf2fyx21d8nsbibn9qkg96ph69m62nvvd4") (y #t)))

(define-public crate-vmsavedstatedump_rs-0.2.0 (c (n "vmsavedstatedump_rs") (v "0.2.0") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1igjmgjj95b4aszfy40szpc93rd7f7lb3kz02s7jhzz2wp7l2ccg")))

