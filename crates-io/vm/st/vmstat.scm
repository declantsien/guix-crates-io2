(define-module (crates-io vm st vmstat) #:use-module (crates-io))

(define-public crate-vmstat-1.6.0 (c (n "vmstat") (v "1.6.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9.4") (d #t) (k 0)))) (h "1r4j41fd5amr1b9jan1wh4ik44qjarjcjncvk94m4m6810krc30j") (y #t)))

