(define-module (crates-io vm ai vmail-rs) #:use-module (crates-io))

(define-public crate-vmail-rs-0.4.0 (c (n "vmail-rs") (v "0.4.0") (d (list (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (d #t) (k 0)) (d (n "diesel-derive-enum") (r "^1.1") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rpassword") (r "5.*") (d #t) (k 0)) (d (n "sha-crypt") (r "^0.1.0") (f (quote ("include_simple"))) (d #t) (k 0)))) (h "1sdpslcacj61ysa55llm3gsrh8smjkz38jyiarf40qbrmzcwyxpj") (f (quote (("postgres" "diesel/postgres" "diesel-derive-enum/postgres") ("mysql" "diesel/mysql" "diesel-derive-enum/mysql") ("default" "mysql"))))))

