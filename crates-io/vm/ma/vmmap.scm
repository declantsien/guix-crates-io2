(define-module (crates-io vm ma vmmap) #:use-module (crates-io))

(define-public crate-vmmap-0.1.4 (c (n "vmmap") (v "0.1.4") (h "16pag8wif2a8vmvy0c04yahdz74sv44p38mvlzr4zppfvcjrixlg") (y #t)))

(define-public crate-vmmap-0.1.5 (c (n "vmmap") (v "0.1.5") (h "1q6x4j6ii1yz05zj3i6q1igq12bw9i1bric5gaiglckr04ayglh2") (y #t)))

(define-public crate-vmmap-0.1.7 (c (n "vmmap") (v "0.1.7") (h "1ys91y02z39762qfcwzh7km5jadsq5az66gl2v5k3jp8jr2iylr0") (y #t)))

(define-public crate-vmmap-0.1.0 (c (n "vmmap") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "1zjvww4zz2rh3rd6y33vb5h81b2fqdsl0dq64wh4lsba0qfsqj9r") (y #t)))

(define-public crate-vmmap-0.1.8 (c (n "vmmap") (v "0.1.8") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "18vdmld7y2v9rzl454h3by7ly81y6s52q6f63wxrp2s7xnbr8nnp") (y #t)))

(define-public crate-vmmap-0.3.1 (c (n "vmmap") (v "0.3.1") (d (list (d (n "machx") (r "^0.4.4") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "1600f8h924i2dl5q6mriir8pi89fc5j9bdlmbsqggn9k2h751wr9") (y #t)))

(define-public crate-vmmap-0.4.0 (c (n "vmmap") (v "0.4.0") (d (list (d (n "machx") (r "^0.4.5") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "02yv4h5aqg27xalwqxdayr54i6dhk9pchxkym8jd526q2gdl8c6v")))

(define-public crate-vmmap-0.6.0 (c (n "vmmap") (v "0.6.0") (d (list (d (n "machx") (r "^0.4.5") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "00wpm0v2nqx60fg19ds4y4bz3qsghwl9hk87m7kyrc9kky7mgnqy")))

(define-public crate-vmmap-0.6.1 (c (n "vmmap") (v "0.6.1") (d (list (d (n "machx") (r "^0.4.5") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "05frifh3q140pqnlr4pas5clradcjpz8pjh0g9q6allypkiyc3vi")))

(define-public crate-vmmap-0.6.2 (c (n "vmmap") (v "0.6.2") (d (list (d (n "machx") (r "^0.4.6") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "1ngj212alx9w07zcd6b2qk8s1iqj40i4bhbiwqwfpgc1d4bykx11")))

(define-public crate-vmmap-0.6.3 (c (n "vmmap") (v "0.6.3") (d (list (d (n "machx") (r "^0.4.6") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "memmap2") (r "^0.9.3") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "0iwvb87dl1s17m21d6hfxkb3xvplv8d4p5yh2wlz00lgy80qpic8")))

(define-public crate-vmmap-0.6.4 (c (n "vmmap") (v "0.6.4") (d (list (d (n "machx") (r "^0.4.8") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "memmap2") (r "^0.9.3") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "0p3d66nra02mxq3l99ab77wqvj7rcsv0wj0l0iwwdby95bddr99a")))

(define-public crate-vmmap-0.6.5 (c (n "vmmap") (v "0.6.5") (d (list (d (n "machx") (r "^0.4.8") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "memmap2") (r "^0.9.3") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "0wzgn1gnj60h3w04nykql5wh1ir2hrqqqcpqqrhvxr6p7cz0dhcs")))

(define-public crate-vmmap-0.6.6 (c (n "vmmap") (v "0.6.6") (d (list (d (n "machx") (r "^0.4.8") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "0l694yzi81cvlzqq405fcl2adaa999v0n691cypk4f9602q5wxkd")))

(define-public crate-vmmap-0.6.7 (c (n "vmmap") (v "0.6.7") (d (list (d (n "machx") (r "^0.4.8") (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug" "Win32_System_Memory" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Environment"))) (t "cfg(windows)") (k 0)))) (h "14fl6iasbimxmdcd5n78plhgl78lzzcg8mmrhs7xjn19ya10zq26")))

