(define-module (crates-io vm m- vmm-sys-util) #:use-module (crates-io))

(define-public crate-vmm-sys-util-0.1.0 (c (n "vmm-sys-util") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "1lbsjn2ly6l7n3m4fcrlj0rvg38gkaq0iawg31dxrfd23rmkjp0l") (y #t)))

(define-public crate-vmm-sys-util-0.1.1 (c (n "vmm-sys-util") (v "0.1.1") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "0v2akwjdkgzffq6c261vy01962009sa7mr9aag0gncdfmrb6z6a6")))

(define-public crate-vmm-sys-util-0.2.0 (c (n "vmm-sys-util") (v "0.2.0") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "046hca3yhi2ips1smp5l0a6nlvmy2dgishq5y2n6d7r9dgv40m5p")))

(define-public crate-vmm-sys-util-0.2.1 (c (n "vmm-sys-util") (v "0.2.1") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "0kkjwhxjq4hj5rcs39hnyv9w0i9r6s68rarzzndvclsnpaknpfvf")))

(define-public crate-vmm-sys-util-0.3.0 (c (n "vmm-sys-util") (v "0.3.0") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "09xbscp3gd8xb9lc5radkph9m2g6iwfr64p1xw52fxzv5iavfv7q")))

(define-public crate-vmm-sys-util-0.3.1 (c (n "vmm-sys-util") (v "0.3.1") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "0f6yff5by81dlg97h98scwyzj5r5bj2yqddfyi82hs3pdqhqwn7l")))

(define-public crate-vmm-sys-util-0.4.0 (c (n "vmm-sys-util") (v "0.4.0") (d (list (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)))) (h "0w9mydc1xwmf038vh6k4a5b179sj82ba35m1rkd8f7869yki12q4")))

(define-public crate-vmm-sys-util-0.5.0 (c (n "vmm-sys-util") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "serde") (r ">= 1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 1.0.9") (d #t) (k 2)))) (h "1jbz0knpdiz7l090nqfcaxrsabbkjf8nqrv108jmsm4ycphwfv1s") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.6.0 (c (n "vmm-sys-util") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r ">= 0.2.39") (d #t) (k 0)) (d (n "serde") (r ">= 1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">= 1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">= 1.0.9") (d #t) (k 2)))) (h "0sz53818hpjglw7qzwlxv0byvkzi7zlgp7g3w14fr35mmg4vhh3w") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.6.1 (c (n "vmm-sys-util") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 2)))) (h "1l3vrc1qlga5kassmprlxpflmw4hwxwcajj6xwcgb9k1dasjag8q") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.7.0 (c (n "vmm-sys-util") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 2)))) (h "1yjxxv18iw9d0pnficg3l880xi8s9jiasrfy2jqbyar65vbx3kfi") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.8.0 (c (n "vmm-sys-util") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 2)))) (h "0186lqc7rcncrksa5yhmzivjc17235ysfj3pg9f0vg2fpjpi3kq1") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.9.0 (c (n "vmm-sys-util") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 2)))) (h "049f4is08a933n3bf0sakwfhvcr7fycsx1gp8csskah3xnykfdbk") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.10.0 (c (n "vmm-sys-util") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "serde") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.9") (d #t) (k 2)))) (h "0qhbg79jhpfshh7adk6i0nrm1x9bxx5fvqyfncrnxciyw1xlsq08") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.11.0 (c (n "vmm-sys-util") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 2)))) (h "090dg9bar5i5ag7d9lrxidmqc2njn0q40c7dk8kdkw7bx1pa21nc") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.11.1 (c (n "vmm-sys-util") (v "0.11.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 2)))) (h "0iigyzj4j6rqhrd4kdvjjrpga5qafrjddrr4qc0fd078v04zwr6x") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.11.2 (c (n "vmm-sys-util") (v "0.11.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 2)))) (h "0a9azxk6wsahwkggshbdga4jdryzfw6j5r21f11gf50j4f2b1ds8") (f (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-vmm-sys-util-0.12.0 (c (n "vmm-sys-util") (v "0.12.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 2)))) (h "07ds91p49kivj309i6nngq9cwpvhyqkhicp5wdjd6r9mj7ly75cx") (f (quote (("with-serde" "serde" "serde_derive")))) (y #t)))

(define-public crate-vmm-sys-util-0.12.1 (c (n "vmm-sys-util") (v "0.12.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 2)))) (h "1pjfjdhnab1x14fakxssn2sgf5mrw4paf1ymz2j0vqj6jw1ka50x") (f (quote (("with-serde" "serde" "serde_derive"))))))

