(define-module (crates-io vm ul vmulti-client) #:use-module (crates-io))

(define-public crate-vmulti-client-0.1.0 (c (n "vmulti-client") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi"))) (d #t) (k 0)))) (h "1yqvi190n2xp2rc35npxl81687hysw8y3g552af8vxm0rks64cpj")))

(define-public crate-vmulti-client-0.2.0 (c (n "vmulti-client") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "1rns13596r8918pwbd4dfh6pg02kzz8j9ym673dqzdhld7282a1s")))

(define-public crate-vmulti-client-0.2.1 (c (n "vmulti-client") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "180dq3xh5i82hjnh1nj9p12r1l4im98qsfskwv5i0wlyv19kv1dx")))

(define-public crate-vmulti-client-0.3.0 (c (n "vmulti-client") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "1yhzy42j1qndz0lxmqdigxh7pvh3r964dm1g4l146d78s5f9vx23")))

(define-public crate-vmulti-client-0.3.1 (c (n "vmulti-client") (v "0.3.1") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "0m7qh23f9nzifjqz6p5661ijw7h7yk1q5y8jh5w6w33qp0cqakmv")))

(define-public crate-vmulti-client-0.3.2 (c (n "vmulti-client") (v "0.3.2") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "0dvhhj01zhhxk7k59mna7lp6y529h7x7wbcjzzldfajw902937a6")))

(define-public crate-vmulti-client-0.3.3 (c (n "vmulti-client") (v "0.3.3") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "08fz80cnmdx9w4r7ijmjzr5wcckaipwhfn96shnk4w057z8h2d58")))

(define-public crate-vmulti-client-0.3.4 (c (n "vmulti-client") (v "0.3.4") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "0bskfsc8nyb3qcn2zwffgnx45f5hrgvrzsfhmfakyvfmy74f80jv")))

