(define-module (crates-io vm p4 vmp4-dump) #:use-module (crates-io))

(define-public crate-vmp4-dump-1.0.0 (c (n "vmp4-dump") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2.1") (d #t) (k 0)))) (h "1pl2sdi78wggxyjy41hd74vwqg285kz036lp9mcnw4xb7z71dd70")))

(define-public crate-vmp4-dump-1.0.1 (c (n "vmp4-dump") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2.1") (d #t) (k 0)))) (h "1rc19bs9nhvirck9ah0ifvqwc59816gbahhxgmmmnmkjhp0dfvqx")))

(define-public crate-vmp4-dump-1.0.2 (c (n "vmp4-dump") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2.1") (d #t) (k 0)))) (h "1dcx42idwiqdm90q20fcn74ivfy1514asa07i3m6i04md50hqjfx")))

(define-public crate-vmp4-dump-1.0.3 (c (n "vmp4-dump") (v "1.0.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2.1") (d #t) (k 0)))) (h "13hal33fg0j832dk6fgqycngk4n64kx0c7yy9x0sgkbwy1323qbb")))

(define-public crate-vmp4-dump-1.0.4-alpha.0 (c (n "vmp4-dump") (v "1.0.4-alpha.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2") (d #t) (k 0)))) (h "1zrgixdm1d44qiz7q8jmbchjsclh6lirjp749iaghhjmxvhq4jj9")))

(define-public crate-vmp4-dump-1.0.5-alpha.0 (c (n "vmp4-dump") (v "1.0.5-alpha.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2") (d #t) (k 0)))) (h "1bgj0pcxccp6nd5dngzr7xlizvz6xk9vyz664dl1b4k2p8x709il")))

(define-public crate-vmp4-dump-1.1.0 (c (n "vmp4-dump") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hxdmp") (r "^0.2") (d #t) (k 0)))) (h "1a8cya99ihhvkvjq2hqmp6qj403j03hjvsy7d73n0vb480w4d0vw")))

