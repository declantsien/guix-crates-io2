(define-module (crates-io vm d_ vmd_parser) #:use-module (crates-io))

(define-public crate-vmd_parser-0.1.0 (c (n "vmd_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r5qh9d1d7m6i62nml2r5hzqcpr37aqm3b5cykzrbqklvr1dgzgx")))

