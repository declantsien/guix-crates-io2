(define-module (crates-io vm _r vm_runner) #:use-module (crates-io))

(define-public crate-vm_runner-0.1.0 (c (n "vm_runner") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http_io") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qapi") (r "^0.13") (f (quote ("qapi-qmp"))) (d #t) (k 0)) (d (n "rexpect") (r "^0.5") (d #t) (k 0)) (d (n "simple-logging") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "10b5m936ixzm8ziab4l72g9czbiccnp38r15b09lgbsdp34xx33r")))

(define-public crate-vm_runner-0.1.1 (c (n "vm_runner") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http_io") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "qapi") (r "^0.13") (f (quote ("qapi-qmp"))) (d #t) (k 0)) (d (n "rexpect") (r "^0.5") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 1)) (d (n "simple-logging") (r "^2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0zpmjwhv2agshmc3znzvsa4ajmnbcqj0riwr59zryjfisrlg2ykj")))

