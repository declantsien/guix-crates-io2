(define-module (crates-io vm ci vmcircbuf) #:use-module (crates-io))

(define-public crate-vmcircbuf-0.1.0 (c (n "vmcircbuf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19805l11pn14ffgyl98vjil6cpi0hxlf0n1gp65kjvznp5nzw7hj")))

(define-public crate-vmcircbuf-0.1.1 (c (n "vmcircbuf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "128srv0my8a259gkyyccm6vzpxcdklzghwjvc78jjdw6nhnvpm3m")))

(define-public crate-vmcircbuf-0.1.3 (c (n "vmcircbuf") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i6p82bb6cwwhvw6zn1p433isazxw5c5rqq77a2d27a42wb0n511")))

(define-public crate-vmcircbuf-0.2.0 (c (n "vmcircbuf") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "063rlrwy8f91vaj2j2dfh0zi7rwdy76baj8qyx4sqxclalz0v54d")))

(define-public crate-vmcircbuf-0.2.1 (c (n "vmcircbuf") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04fs1crfhb2i7ah0zwm6vxdf12hypgiwcmay35w296pqz2v8kr5d")))

(define-public crate-vmcircbuf-0.2.2 (c (n "vmcircbuf") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l65mwblrq8wcryahq7ljxklvxgbpcdvxx5kkqsqgffd365bhgza")))

(define-public crate-vmcircbuf-0.2.3 (c (n "vmcircbuf") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ixwq477w7sm0zd0r42ym5d52rcmpvb7fa4sfk7h0gjyb7nzh12s")))

(define-public crate-vmcircbuf-0.2.5 (c (n "vmcircbuf") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1im1wpcgly33hvamgp80ap46l8h0kllyalc2m1p0c24n61m31yn8")))

(define-public crate-vmcircbuf-0.2.6 (c (n "vmcircbuf") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f4jyaqj8gdhzac2d1932p2h5alsabd9i2inknhjbr7m42xvb1xs")))

(define-public crate-vmcircbuf-0.2.7 (c (n "vmcircbuf") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12g3r4z3q9rq8gw4p06mhjv9h78nzfzamb8s9by2ijn0jsqq527y")))

(define-public crate-vmcircbuf-0.2.8 (c (n "vmcircbuf") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hcr6ba4j30jijdir66z5q1x3ixxnc6gxv5lk57xhpl7hf45mahq")))

