(define-module (crates-io vm ml vmml) #:use-module (crates-io))

(define-public crate-vmml-0.1.0 (c (n "vmml") (v "0.1.0") (d (list (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)))) (h "1lhxkh26myl19q39hgb9drkn6dpsjq81hppcar24krlwb8k06xy2")))

(define-public crate-vmml-1.0.0 (c (n "vmml") (v "1.0.0") (d (list (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fliy57pkaxpi64dczmd769mi75klabxc8kph1w39jiajcbx4y9d")))

(define-public crate-vmml-1.0.1 (c (n "vmml") (v "1.0.1") (d (list (d (n "pest") (r "^2.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "08vd8hnqld4r880xpcmx732hsq8gwbyqfs4j2pnb0rb1x8qajrwy")))

