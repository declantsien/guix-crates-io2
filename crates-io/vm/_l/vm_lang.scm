(define-module (crates-io vm _l vm_lang) #:use-module (crates-io))

(define-public crate-vm_lang-0.1.0 (c (n "vm_lang") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z0xswq1n0s90cama979zm2vb3z97xs2gp0avmb7zqfsfmngj76c") (y #t)))

(define-public crate-vm_lang-0.1.1 (c (n "vm_lang") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d2jh1lnzwin7319ynsyk1iihsdisqsf1qn6c3jd19a5vb39nm8s")))

