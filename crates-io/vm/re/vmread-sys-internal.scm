(define-module (crates-io vm re vmread-sys-internal) #:use-module (crates-io))

(define-public crate-vmread-sys-internal-0.1.1 (c (n "vmread-sys-internal") (v "0.1.1") (d (list (d (n "vmread-sys") (r "^0.1.1") (f (quote ("internal_rw"))) (d #t) (k 0)))) (h "1knapfcca8ampskgs0vcy7ar0q8smhd3zsk8dmspq2nsnag4588x")))

(define-public crate-vmread-sys-internal-0.1.2 (c (n "vmread-sys-internal") (v "0.1.2") (d (list (d (n "vmread-sys") (r "^0.1.2") (f (quote ("internal_rw"))) (d #t) (k 0)))) (h "11skzqwc494ajc7wsgch2nkh6y79clgi7lc3nk3ch7xwk2jdg8hq")))

(define-public crate-vmread-sys-internal-0.1.4 (c (n "vmread-sys-internal") (v "0.1.4") (d (list (d (n "vmread-sys") (r "^0.1.4") (f (quote ("internal_rw"))) (d #t) (k 0)))) (h "1k717g2ix5pzr9980ghhkbvb023hazkf4pwplvj8bhk9ddrj7bp2")))

(define-public crate-vmread-sys-internal-0.1.5 (c (n "vmread-sys-internal") (v "0.1.5") (d (list (d (n "vmread-sys") (r "^0.1.5") (f (quote ("internal_rw"))) (d #t) (k 0)))) (h "0d6xdjk32pwz4v43cxz5pj926c7bn86rckz54jr6ky5c780mxr1d")))

