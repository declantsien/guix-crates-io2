(define-module (crates-io vm re vmread-sys-kmod) #:use-module (crates-io))

(define-public crate-vmread-sys-kmod-0.1.1 (c (n "vmread-sys-kmod") (v "0.1.1") (d (list (d (n "vmread-sys") (r "^0.1.1") (f (quote ("kmod_rw"))) (d #t) (k 0)))) (h "1dlpl4zqsyjxznp2z26gxilm4dl7fi0v1c8rk6j5wj1nqr526cv1")))

(define-public crate-vmread-sys-kmod-0.1.2 (c (n "vmread-sys-kmod") (v "0.1.2") (d (list (d (n "vmread-sys") (r "^0.1.2") (f (quote ("kmod_rw"))) (d #t) (k 0)))) (h "156i9yq4lwkbzwglb9p2j8fpr4ndf5mmgv6imsfvnfkrqh6knvhw")))

(define-public crate-vmread-sys-kmod-0.1.4 (c (n "vmread-sys-kmod") (v "0.1.4") (d (list (d (n "vmread-sys") (r "^0.1.4") (f (quote ("kmod_rw"))) (d #t) (k 0)))) (h "16mq1vp34cfw3xq0zjlbah6z2qz8814hdiw1dj03rzzcpfq1z1md")))

(define-public crate-vmread-sys-kmod-0.1.5 (c (n "vmread-sys-kmod") (v "0.1.5") (d (list (d (n "vmread-sys") (r "^0.1.5") (f (quote ("kmod_rw"))) (d #t) (k 0)))) (h "0bar35c490lmmy3c5kmxpwhkc6wzsdp04pr19gwxn9csjbcxvm3r")))

