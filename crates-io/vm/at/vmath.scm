(define-module (crates-io vm at vmath) #:use-module (crates-io))

(define-public crate-vmath-0.1.0 (c (n "vmath") (v "0.1.0") (h "1xkhwrclk524dfqyp4rxa84j9wfr8kwwphp6zh34kp2q4pgg11a7")))

(define-public crate-vmath-0.1.1 (c (n "vmath") (v "0.1.1") (h "1dgnychlnznf148xrm9mfsa4b8c7vvbwg02ksh4gngzxv553qlp9")))

