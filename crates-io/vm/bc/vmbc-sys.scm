(define-module (crates-io vm bc vmbc-sys) #:use-module (crates-io))

(define-public crate-vmbc-sys-0.1.0 (c (n "vmbc-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_LibraryLoader"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1qdn5wfkv8895cwjg839l7c0fkllh7q8zr1vlvvd81l9hi29xmyl") (r "1.56")))

(define-public crate-vmbc-sys-0.1.1 (c (n "vmbc-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_LibraryLoader"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "14gq62axzizfw6mcbjzdqn8iph8yb3lmmmm9i4va2mhsf1xyxq00") (r "1.56")))

