(define-module (crates-io vm pr vmprotect) #:use-module (crates-io))

(define-public crate-vmprotect-0.1.0 (c (n "vmprotect") (v "0.1.0") (d (list (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0k0v10ny84dxl6z71as5wkpydr8jdphxh6pvs44zxdi0kydlx6wj")))

(define-public crate-vmprotect-0.1.1 (c (n "vmprotect") (v "0.1.1") (d (list (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "1wypizz9v8g8nzrb9vysjqfbpy93i0r12j0ydzsqwl8frd9y8j52")))

(define-public crate-vmprotect-0.1.2 (c (n "vmprotect") (v "0.1.2") (d (list (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "1ilxyv6wg5d1d7majggb0j7c2dg39w1j82c430mwj3fxk9hjm96s")))

(define-public crate-vmprotect-0.1.3 (c (n "vmprotect") (v "0.1.3") (d (list (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "1s7lw324v1fswrjb2q9ppavrb4784v78lywcd2n2kjac6bvnk3lc")))

(define-public crate-vmprotect-0.1.4 (c (n "vmprotect") (v "0.1.4") (d (list (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0zp4rixdxrbl2sqj6b9pfhphr19anzvmjy82w3i1m4xcbfgp5br3")))

(define-public crate-vmprotect-0.1.5 (c (n "vmprotect") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "1416v60gc3r7a7r7lm56bxg04lqr4vhvxjigkyzqwmqqx5hvgbzh") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.6 (c (n "vmprotect") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0468zfg8l5yvd9a8w769vkp3bncspcjkyh0y0bk1r6wayzqq1p5a") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.7 (c (n "vmprotect") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "1nm822h014y0zp5cqzzx8w6wlh7yg6mgy3r4y7q2gisx75kjfg1c") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.8 (c (n "vmprotect") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0fpyvgghm1jfffq6yqjn6lwv98xaabbh18dg2450j1f0s12c6ara") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.9 (c (n "vmprotect") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "04yjm4cxcq535cg789whdr8gb30w4mcps2i5mfiiab38hkz441gv") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.10 (c (n "vmprotect") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "0dq6hz2zlg5lc9667671ll5fjnhhsqz3syb3nflnm43qykyjs143") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.1.11 (c (n "vmprotect") (v "0.1.11") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "real_c_string") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)))) (h "01qlynzv6lsiz8bhwbybnbf20gd9lq3aqhlj9lj8m5sgw5iwp9js") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

(define-public crate-vmprotect-0.2.0 (c (n "vmprotect") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "real_c_string") (r "^1.0") (d #t) (k 0)) (d (n "vmprotect-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "vmprotect-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)))) (h "0y1qa2314jf9ddryjsydiv6g2mbmik339193vnb6rbvs72wzy77g") (f (quote (("strings") ("service") ("licensing") ("default" "licensing" "service" "strings"))))))

