(define-module (crates-io vm pr vmprotect-sys) #:use-module (crates-io))

(define-public crate-vmprotect-sys-0.1.0 (c (n "vmprotect-sys") (v "0.1.0") (h "0cmqw3n03rzxsfpssbv9w3d9fx2gnq0d9vjwwblaaqqy6djbwvwj")))

(define-public crate-vmprotect-sys-0.1.1 (c (n "vmprotect-sys") (v "0.1.1") (h "015la63a0r98qgp94w6lrmkha7cm0dflvy8ysnnfjhjiqm6kfkil")))

