(define-module (crates-io vm pr vmprotect-macros) #:use-module (crates-io))

(define-public crate-vmprotect-macros-0.1.0 (c (n "vmprotect-macros") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1.6") (d #t) (k 0)))) (h "0bky5ih2w62hzi2dl5vpgnbw5qv0dhcmg0gq5vqqp15405ylrpk5")))

