(define-module (crates-io vm ne vmnet-derive) #:use-module (crates-io))

(define-public crate-vmnet-derive-0.1.0 (c (n "vmnet-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0h609f66k7ky9vs8mvr7phcsx2ql5gpb360crr5g5rznq3d4xw69")))

(define-public crate-vmnet-derive-0.1.1 (c (n "vmnet-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0ayy16jhcs8z94jcbrjg1zcwcwc7lbr9rmz6jfaw7lxbz6fqp3gz")))

(define-public crate-vmnet-derive-0.2.0 (c (n "vmnet-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0m7g7hwsq8zdg42p5amyj40aj5n0ppmph95kk7h33ywa95pc510h")))

(define-public crate-vmnet-derive-0.3.0 (c (n "vmnet-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full"))) (d #t) (k 0)))) (h "0s3hwi1ccx186j179wq4g5i9smvafp7c6yf06ymgcykm7vnlr84n")))

