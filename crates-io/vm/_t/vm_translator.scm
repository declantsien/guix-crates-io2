(define-module (crates-io vm _t vm_translator) #:use-module (crates-io))

(define-public crate-vm_translator-0.1.0 (c (n "vm_translator") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1agx5rqy2whlh37b0lfqvqjmwmwp4ag5951mxhxfval2r4qvxl20")))

(define-public crate-vm_translator-0.1.1 (c (n "vm_translator") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0inxp88z486s69n74g20rw3r8715hcq0hhjpqy8yf8a0i2mf8ggr")))

(define-public crate-vm_translator-0.1.2 (c (n "vm_translator") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "16gpby4cpzp8nppv1crg3bxnsppk6h6lpr43rxcbr5whvq6jcw0r")))

