(define-module (crates-io vm _t vm_test_fixture) #:use-module (crates-io))

(define-public crate-vm_test_fixture-0.1.0 (c (n "vm_test_fixture") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 1)) (d (n "simple-logging") (r "^2") (d #t) (k 1)) (d (n "vm_runner") (r "^0.1") (d #t) (k 0)) (d (n "vm_runner") (r "^0.1") (d #t) (k 1)))) (h "1h1a95y4wm7nj73r5lh1a437f6m313hyrbgi24ic9nkpixcg7jc4")))

(define-public crate-vm_test_fixture-0.1.1 (c (n "vm_test_fixture") (v "0.1.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple-logging") (r "^2") (d #t) (k 0)) (d (n "vm_runner") (r "^0.1") (d #t) (k 0)))) (h "06a0nk94vvy6ni3zm1f5vhpyqvspgmfv0vp46ajzkymwg54672sv")))

