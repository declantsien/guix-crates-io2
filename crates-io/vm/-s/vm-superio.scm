(define-module (crates-io vm -s vm-superio) #:use-module (crates-io))

(define-public crate-vm-superio-0.1.0 (c (n "vm-superio") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.6.1") (d #t) (k 0)))) (h "0jcgw3k5pyq4j73qm5mrk7njxnc8scy8gqy0mqlx8y0ck92s9kdw") (y #t)))

(define-public crate-vm-superio-0.1.1 (c (n "vm-superio") (v "0.1.1") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.1") (d #t) (k 0)))) (h "0xsc6a02hqwnf551ziqmm97j9psgk65rq9rp9nz9qarz750w546n")))

(define-public crate-vm-superio-0.2.0 (c (n "vm-superio") (v "0.2.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "0v463fnp6j5yv23crcqnsz0rlb7afldpzf2mcj79w8ab84a9mnrh")))

(define-public crate-vm-superio-0.3.0 (c (n "vm-superio") (v "0.3.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "017xvxdy7dv5z7s0l470bdhj9a6gv7hcyp8qmxz7g59hx5wqakp0")))

(define-public crate-vm-superio-0.4.0 (c (n "vm-superio") (v "0.4.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "1zacxqdkwdrspph15wrzh1sqiv4j6pq96fh5l0186fsihdq6mqiv")))

(define-public crate-vm-superio-0.5.0 (c (n "vm-superio") (v "0.5.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "0laan3w4s2967xckdxvd3m4hfk1f08dalk3h48xw1nsf6cfj7dd4")))

(define-public crate-vm-superio-0.6.0 (c (n "vm-superio") (v "0.6.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "1laqmk1bj3i8g6918pimgayay6sfjy29s6rprjyj3pamn4dn13nd")))

(define-public crate-vm-superio-0.7.0 (c (n "vm-superio") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 2)))) (h "0r5gnlhdnqs8k9jw8979jzqwpihghn4c7yhrfhr5i1fp8q0xrqvd")))

(define-public crate-vm-superio-0.8.0 (c (n "vm-superio") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "vmm-sys-util") (r "^0.12.0") (d #t) (k 2)))) (h "187dviy3f5ql350x97f3ayyrr270fqw09wh08v8mxixzmhjywa1l")))

