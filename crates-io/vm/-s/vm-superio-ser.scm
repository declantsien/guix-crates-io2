(define-module (crates-io vm -s vm-superio-ser) #:use-module (crates-io))

(define-public crate-vm-superio-ser-0.1.0 (c (n "vm-superio-ser") (v "0.1.0") (d (list (d (n "serde") (r ">=1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "versionize") (r ">=0.1.6") (d #t) (k 0)) (d (n "versionize_derive") (r ">=0.1.3") (d #t) (k 0)) (d (n "vm-superio") (r "=0.5.0") (d #t) (k 0)))) (h "070h13q0hk10q19yk1i0ml123b9awk8wzzr99k95nw9436rfzfn7")))

(define-public crate-vm-superio-ser-0.2.0 (c (n "vm-superio-ser") (v "0.2.0") (d (list (d (n "bincode") (r ">=1.3") (d #t) (k 2)) (d (n "libc") (r ">=0.2.39") (d #t) (k 2)) (d (n "serde") (r ">=1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "versionize") (r ">=0.1.6") (d #t) (k 0)) (d (n "versionize_derive") (r ">=0.1.3") (d #t) (k 0)) (d (n "vm-superio") (r "=0.6.0") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.7.0") (d #t) (k 2)))) (h "1q5wrwx6d7i7d43azldlx8pzad3sdf6b2xwfxzw7fn7m0878hd0r")))

(define-public crate-vm-superio-ser-0.3.0 (c (n "vm-superio-ser") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "versionize") (r "^0.1.6") (d #t) (k 0)) (d (n "versionize_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "vm-superio") (r "=0.7.0") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 2)))) (h "1nv5y7akw6d4xv4misvfagcaacnqa3mcj15c70kc4pdwlhpz29lj")))

(define-public crate-vm-superio-ser-0.4.0 (c (n "vm-superio-ser") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.39") (d #t) (k 2)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "versionize") (r "^0.2.0") (d #t) (k 0)) (d (n "versionize_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "vm-superio") (r "=0.8.0") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.0") (d #t) (k 2)))) (h "130ianip3rjyzla59s8a587b47bp7qznflj3cbhih5hnd6s8dz7n")))

