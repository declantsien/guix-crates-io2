(define-module (crates-io vm af vmaf-sys) #:use-module (crates-io))

(define-public crate-vmaf-sys-0.0.1 (c (n "vmaf-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1276234wjfnnyvqxm8idqvd5m9agl69s2lfzxwnnppp7pq76i12q")))

(define-public crate-vmaf-sys-0.0.2 (c (n "vmaf-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1jpx6pdxsc6fhryk7cyq6hppza9r0panmzspm3hjn1k29z8xg5x4")))

(define-public crate-vmaf-sys-0.0.3 (c (n "vmaf-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1wgclzaq25lqdyrdzf3vbrrii9lpr41ympdp3ddv8s4lbs5ndn64")))

(define-public crate-vmaf-sys-0.0.4 (c (n "vmaf-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1i723ydll14biirbhfdvifbf7ww5i1bmsyhd07i58w74zffvk834") (f (quote (("docs-only"))))))

(define-public crate-vmaf-sys-0.0.5 (c (n "vmaf-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0rbvb2lbhv6araw3zrxzjjassg4bdi4g0dsiyiwdlb9hdqbhs07n") (f (quote (("default") ("buildtype-docs-only"))))))

(define-public crate-vmaf-sys-0.0.6 (c (n "vmaf-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0y9777nj4h4i9bvpc4ylld2dqc13rsm3c858x7cf0w0ksf12sk8f") (f (quote (("default") ("buildtype-docs-only"))))))

(define-public crate-vmaf-sys-0.0.7 (c (n "vmaf-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1in4nr4l85gpdld6yc0rp6xad9l7l8hn0cp3cnlpginq4gnq98f4") (f (quote (("default") ("buildtype-docs-only"))))))

(define-public crate-vmaf-sys-0.0.8 (c (n "vmaf-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "073h0nwrhf1fy8y2lqbgwh2y165iq4jqhwy3qjydk158476r4jvi") (f (quote (("default") ("buildtype-docs-only"))))))

(define-public crate-vmaf-sys-0.0.9 (c (n "vmaf-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "05301n6cqz9kjppz3abb2dh29m1d4d2m92sxbc9nbnfhpqn8704x") (f (quote (("default") ("buildtype-docs-only"))))))

(define-public crate-vmaf-sys-0.0.10 (c (n "vmaf-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "13761f3s6srld3zq6sfqawlij3mm8mp9nip0bpy6xph708kcx9jm") (f (quote (("default") ("buildtype-docs-only"))))))

