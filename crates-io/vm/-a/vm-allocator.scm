(define-module (crates-io vm -a vm-allocator) #:use-module (crates-io))

(define-public crate-vm-allocator-0.1.0 (c (n "vm-allocator") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r25bljzv5fc5yjwl5i47jlg5d3xm683s9699vrkn6yxny36hnsn")))

(define-public crate-vm-allocator-0.1.1 (c (n "vm-allocator") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h92f57bzbax88hwp5qmjzb1nsssf5zy4qvkfjqp93afplcffk2y")))

