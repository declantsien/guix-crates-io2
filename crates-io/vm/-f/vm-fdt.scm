(define-module (crates-io vm -f vm-fdt) #:use-module (crates-io))

(define-public crate-vm-fdt-0.1.0 (c (n "vm-fdt") (v "0.1.0") (h "0mrlrq0qfq5j8zrgz25719mxxsdh7yqfskvv3hcb56llvx7nz65x") (f (quote (("long_running_test"))))))

(define-public crate-vm-fdt-0.2.0 (c (n "vm-fdt") (v "0.2.0") (h "19j51mdmymz4iwzi0yl4rx37b6vi6q8800i8swx44z8spnkbagzl") (f (quote (("long_running_test"))))))

(define-public crate-vm-fdt-0.3.0 (c (n "vm-fdt") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)))) (h "08rgjni8bp7y7g222nnd48v6028z94f49s3wc9ibnnd084l2h8by") (f (quote (("std") ("long_running_test") ("default" "std") ("alloc" "hashbrown"))))))

