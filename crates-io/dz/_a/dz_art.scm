(define-module (crates-io dz _a dz_art) #:use-module (crates-io))

(define-public crate-dz_art-0.1.0 (c (n "dz_art") (v "0.1.0") (h "0q5s83wd5c7w243qjnyy6arc02gvk01fr0q3vjj2r36jn71qx7mz") (y #t)))

(define-public crate-dz_art-0.1.1 (c (n "dz_art") (v "0.1.1") (h "09czfc177l7gmhq3wg239wr3cd3jqxbvmvx42z6nrjfiyqhfa4kx")))

