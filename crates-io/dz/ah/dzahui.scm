(define-module (crates-io dz ah dzahui) #:use-module (crates-io))

(define-public crate-Dzahui-0.1.0 (c (n "Dzahui") (v "0.1.0") (h "0i1m7rhz3141xw2is4dp1zdgi646xpb49ardz5as0gw9wp3q78f1")))

(define-public crate-Dzahui-0.1.0-beta.1 (c (n "Dzahui") (v "0.1.0-beta.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "16dy6aczm838b4ld9gl5i9lrjvrbfrzla4qm8x2ks659d7qhn927")))

(define-public crate-Dzahui-0.1.0-beta.2 (c (n "Dzahui") (v "0.1.0-beta.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1a91m3pp0rcpvi7jrsnng0lyhgcyb7f0g13jl365sksx2cz4s2q5")))

(define-public crate-Dzahui-0.2.0 (c (n "Dzahui") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "gl") (r "^0.6.0") (d #t) (k 0)) (d (n "glutin") (r "^0.28.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1nms2732lq9rhny697fxd019jfq2nly4c0xx39nwwn6014n116f2")))

