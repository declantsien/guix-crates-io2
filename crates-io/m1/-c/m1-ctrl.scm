(define-module (crates-io m1 -c m1-ctrl) #:use-module (crates-io))

(define-public crate-m1-ctrl-0.1.0 (c (n "m1-ctrl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "dosio") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^0.1.0") (d #t) (k 0)))) (h "0yrfgwkpa0jk22wl5pjq8phxja8d4575isjagk962wqf9qhcbp2p")))

(define-public crate-m1-ctrl-0.1.1 (c (n "m1-ctrl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "dosio") (r "^0.1.3") (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "seq-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.0") (d #t) (k 0)) (d (n "simulink-rs") (r "^0.1.0") (d #t) (k 0)))) (h "05c7qbsxgz5kkrs5ia01d1q0n03m0wasmxwlf7lwgg1xqqjfqfdv") (f (quote (("dos-prqt" "dosio/prqt") ("default" "dosio/default"))))))

(define-public crate-m1-ctrl-0.1.2 (c (n "m1-ctrl") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.0") (d #t) (k 0)))) (h "0s1jksz1qbfh0z0nwqn327w6a398nq9gjdp8qpg6awhi5vq41nlm")))

