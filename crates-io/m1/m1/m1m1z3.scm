(define-module (crates-io m1 m1 m1m1z3) #:use-module (crates-io))

(define-public crate-m1m1z3-0.1.0 (c (n "m1m1z3") (v "0.1.0") (h "1924vyng4slcqxfisf8hnhrhk0fiyc03kd2dknb09p74rpjxbhb7") (y #t)))

(define-public crate-m1m1z3-0.1.1 (c (n "m1m1z3") (v "0.1.1") (h "1pkinnd2xbvip4jc9kwfyzl8ffbq18zlsz82in1lywh4aybn74lk") (y #t)))

