(define-module (crates-io o5 m- o5m-stream) #:use-module (crates-io))

(define-public crate-o5m-stream-1.0.0 (c (n "o5m-stream") (v "1.0.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "0bs4yn0a9ypgh068sqvjzc1y8bbsnashyxrqybbv7f1x2ipgrf28")))

(define-public crate-o5m-stream-1.0.1 (c (n "o5m-stream") (v "1.0.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)))) (h "1zxnrdbpc660jq7cmmam6b33n0s0s4ls6qg5vj86047fvh9jkc73")))

(define-public crate-o5m-stream-1.1.0 (c (n "o5m-stream") (v "1.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0kysccqy72jn319lyfihkj81v5hqbz1zb86x4d74fqbbbp5a8yqb")))

(define-public crate-o5m-stream-1.1.1 (c (n "o5m-stream") (v "1.1.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0lq2hn305sn19c9fqgfc9xf2svjm69y8mb3sa5dgywn01kfzgf34")))

(define-public crate-o5m-stream-1.1.2 (c (n "o5m-stream") (v "1.1.2") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0ml0gvkn7dqdwif8cnwhw306krkqky6b34n7yg9spygl8nmhs000")))

(define-public crate-o5m-stream-1.1.3 (c (n "o5m-stream") (v "1.1.3") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "01kwpa99wmp5jzzp41i595k19y76p2ci3m8fxcjkazmlrqz5vmqx")))

(define-public crate-o5m-stream-1.1.4 (c (n "o5m-stream") (v "1.1.4") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0xhxh4b20bi5ndc7xvj83s6rn1mli645cqzrcsclj67nc8w369gw")))

(define-public crate-o5m-stream-2.0.0 (c (n "o5m-stream") (v "2.0.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes" "unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0f23lvzyh4j3ni0rn30q64m0gxg62vg6pdspvsm8635p9bwjbm20")))

