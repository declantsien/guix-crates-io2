(define-module (crates-io r5 #{3d}# r53ddns) #:use-module (crates-io))

(define-public crate-r53ddns-1.0.0 (c (n "r53ddns") (v "1.0.0") (d (list (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.43.0-beta.1") (d #t) (k 0)) (d (n "rusoto_credential") (r "^0.43.0-beta.1") (d #t) (k 0)) (d (n "rusoto_route53") (r "^0.43.0-beta.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1lmbibha9m1czsp4dfx2xs9fbyf7cs5469a9dwqpinqkq426dbng")))

