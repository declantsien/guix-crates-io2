(define-module (crates-io r5 #{28}# r528-pac) #:use-module (crates-io))

(define-public crate-r528-pac-0.0.1 (c (n "r528-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0namw0v1mppvlwvhfp9z3g09pkhly4n9mv44nr8ilvs2ak20w8sh") (f (quote (("rt"))))))

(define-public crate-r528-pac-0.0.2 (c (n "r528-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gni5qjd2m0s55523b8iqaal5grd7mxk4gwhbz1h1iz61bm3pjij") (f (quote (("rt"))))))

(define-public crate-r528-pac-0.0.3 (c (n "r528-pac") (v "0.0.3") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0is486zdac5g8v03zwhfvwar59n7by1h4ylsz832wxsj4igy1niw") (f (quote (("rt"))))))

