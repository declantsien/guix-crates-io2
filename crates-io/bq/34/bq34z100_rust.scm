(define-module (crates-io bq #{34}# bq34z100_rust) #:use-module (crates-io))

(define-public crate-bq34z100_rust-0.1.0 (c (n "bq34z100_rust") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0zz9a75p2a0gwp3x2lw5i81sm9hhqgsdxr96977rq128ag2dh16s") (y #t)))

