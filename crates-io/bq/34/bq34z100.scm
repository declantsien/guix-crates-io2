(define-module (crates-io bq #{34}# bq34z100) #:use-module (crates-io))

(define-public crate-bq34z100-0.1.0 (c (n "bq34z100") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0ayvq5yl11xd7vbagbliv5ibca83zk1wmrj06547b83k9zppdraz")))

(define-public crate-bq34z100-0.2.0 (c (n "bq34z100") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0w9nnb0gsffwfzkdwly0v47hrv0y7jqcj44m7q76xdmggl9i39ny") (f (quote (("write" "std") ("std"))))))

(define-public crate-bq34z100-0.2.1 (c (n "bq34z100") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0xxlcn1q5y3d8a0mjxznaw2vp7j8wqcqf35bri6wpqx6p65vwl2h") (f (quote (("write" "std") ("std"))))))

