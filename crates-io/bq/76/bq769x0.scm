(define-module (crates-io bq #{76}# bq769x0) #:use-module (crates-io))

(define-public crate-bq769x0-0.3.0 (c (n "bq769x0") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (k 0)))) (h "02c1jggymkjp1mbbm4dfyryp0g0mvwy8gq0z0vnzpzch3nwlxwnb") (f (quote (("std") ("no_std") ("default" "no_std") ("crc"))))))

(define-public crate-bq769x0-0.3.1 (c (n "bq769x0") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "crc-any") (r "^2.3") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (k 0)))) (h "0n9svrxdrwqmd5013jl3k2jzw10y59w1vmaxbdvnmbmpzgfb97ps") (f (quote (("std") ("no_std") ("default" "no_std") ("crc"))))))

