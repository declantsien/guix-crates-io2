(define-module (crates-io bq #{24}# bq24195) #:use-module (crates-io))

(define-public crate-bq24195-0.1.0 (c (n "bq24195") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "0d63y4h26cwi21dw44z8dglsa2r3an4qiqapw3skk6w9qcclrxd9")))

(define-public crate-bq24195-0.1.2 (c (n "bq24195") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)))) (h "1xv1ql5d85zz9vyi9sqfmjmc86p79ivxdg4rzg98wwx995nc6fsq")))

