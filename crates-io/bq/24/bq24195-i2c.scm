(define-module (crates-io bq #{24}# bq24195-i2c) #:use-module (crates-io))

(define-public crate-bq24195-i2c-0.1.0 (c (n "bq24195-i2c") (v "0.1.0") (d (list (d (n "arduino_mkrvidor4000") (r "^0.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r ">= 0.1.10") (d #t) (k 0)))) (h "0fjmppkbmrmc6fapbslnls4gbafx2hssb59d6bmzqlcqzyyfkfl2") (y #t)))

(define-public crate-bq24195-i2c-0.1.1 (c (n "bq24195-i2c") (v "0.1.1") (d (list (d (n "arduino_mkrvidor4000") (r "^0.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r ">= 0.1.10") (d #t) (k 0)))) (h "1p1vh976bdxpr16dasad4ih7kz6ckdy2gypqgqwn6k5ijqk1w707") (y #t)))

(define-public crate-bq24195-i2c-0.1.2 (c (n "bq24195-i2c") (v "0.1.2") (d (list (d (n "arduino_mkrvidor4000") (r "^0.1") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r ">= 0.1.10") (d #t) (k 0)))) (h "17dvvd3mp21qzm3sc2bk2wvy0dgl0d5yh0pc842d15i7rkpnf8zl")))

