(define-module (crates-io bq #{40}# bq40z50) #:use-module (crates-io))

(define-public crate-bq40z50-0.1.0 (c (n "bq40z50") (v "0.1.0") (h "1p6cfdch68wmaxh0djl9s0r104fpc37lghgydgiqxbj0w16znm4r")))

(define-public crate-bq40z50-0.1.1 (c (n "bq40z50") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0v6ifxfqfklfg0nghzmh3i2v1rnijmyldgdqbyy19s7vs24mjd7j")))

(define-public crate-bq40z50-0.1.2 (c (n "bq40z50") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "1gwwipravcpgfkf7hphfsl7d81jx99hdxkw72vl9y6g7scrapl64")))

(define-public crate-bq40z50-0.1.3 (c (n "bq40z50") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0nj72364m3kh44m1y36961par5dryw1wkfw0rk4f5px6vwnfvy4b")))

(define-public crate-bq40z50-0.1.4 (c (n "bq40z50") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (f (quote ("hal"))) (d #t) (k 2)))) (h "0rfn21qj2dkk8h6f6pjmgpi8daa07l27s7inkg8nmcab649nbail")))

