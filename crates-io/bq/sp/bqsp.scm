(define-module (crates-io bq sp bqsp) #:use-module (crates-io))

(define-public crate-bqsp-0.4.3 (c (n "bqsp") (v "0.4.3") (d (list (d (n "tokio") (r "^1.26.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "146h584a22hfj8bm6gh48l8k6l995whgm1g197kz11z49dsv5p9j") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-bqsp-0.5.1 (c (n "bqsp") (v "0.5.1") (d (list (d (n "tokio") (r "^1.26.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "1830p9n6niy237l4g5f3s7iva329iwn9xw4nmkzgnk9m7qvh4ihj") (f (quote (("default" "async") ("async" "tokio"))))))

