(define-module (crates-io bq #{27}# bq27xxx) #:use-module (crates-io))

(define-public crate-bq27xxx-0.0.1 (c (n "bq27xxx") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-alpha.9") (d #t) (k 0)))) (h "1iyzr5jipldhyl4c821adp5sc9ac8nix3iqrsidac03yv0hgxwbg")))

(define-public crate-bq27xxx-0.0.2 (c (n "bq27xxx") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.3") (d #t) (k 0)))) (h "0y9izdxlm4lmzrf6wmsq7czzvfj5rmr08lai3zvxsyybh7mxwfnv")))

