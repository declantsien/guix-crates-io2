(define-module (crates-io xt ok xtoken) #:use-module (crates-io))

(define-public crate-xtoken-0.1.0 (c (n "xtoken") (v "0.1.0") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "166nh8i2wsfxp7zi6777myxflwkbi5djbila65ydhjvk1jcca3r1")))

(define-public crate-xtoken-0.1.1 (c (n "xtoken") (v "0.1.1") (d (list (d (n "memchr") (r "^2.5.0") (d #t) (k 0)))) (h "1h3n8kn698g85c4371c1fhn5047ahh49gmqkaxyr5bvm57sf30lr")))

