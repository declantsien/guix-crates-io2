(define-module (crates-io xt or xtorrent) #:use-module (crates-io))

(define-public crate-xtorrent-0.1.0 (c (n "xtorrent") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzc19073rs47bnl578lqgrzzasrbrdmpxidpzbz0zrn2g1hbr6z")))

(define-public crate-xtorrent-0.1.1 (c (n "xtorrent") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "18gs64bfxw7nzn7qyn1p7zshbf74sx2nwl45a998n9g5da910nw9")))

