(define-module (crates-io xt or xtor_derive) #:use-module (crates-io))

(define-public crate-xtor_derive-0.9.0 (c (n "xtor_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5d8zh73bp4ff7vdmw3qxim29nd7ap2x58iw1dsbbydm3limbki")))

(define-public crate-xtor_derive-0.9.9 (c (n "xtor_derive") (v "0.9.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gfjg3b33jchaxacys8vm0vswjq8sjl1lzwlaglx64qp2d3pj7hk")))

