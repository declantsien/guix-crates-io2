(define-module (crates-io xt b_ xtb_api) #:use-module (crates-io))

(define-public crate-xtb_api-0.0.1 (c (n "xtb_api") (v "0.0.1") (h "095m3lxcdwxnkhiq48dg1nyk7bff1hkmly7c7bi3j15a6clbdzp3")))

(define-public crate-xtb_api-0.0.2 (c (n "xtb_api") (v "0.0.2") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "02w5ggpmmbg9znv0kh2psyg706ilavfim8lcvl5yj18x1k95220k")))

(define-public crate-xtb_api-0.0.3 (c (n "xtb_api") (v "0.0.3") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0d1fdssz040daycn19bgh3lr12wx3zrpxyayzzf5iqn2jlv11rwb")))

(define-public crate-xtb_api-0.0.4 (c (n "xtb_api") (v "0.0.4") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "007920xvn3si9cp98ans8ga6vkij7am0vhdy5gpjsxy90flm6jh4")))

(define-public crate-xtb_api-0.0.5 (c (n "xtb_api") (v "0.0.5") (d (list (d (n "native-tls") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0ch5ksmd26c1xd5awanjx0r2w4hbsiah293hh14skildhv2slr97")))

