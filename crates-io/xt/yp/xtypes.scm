(define-module (crates-io xt yp xtypes) #:use-module (crates-io))

(define-public crate-xtypes-0.1.0 (c (n "xtypes") (v "0.1.0") (d (list (d (n "insta") (r "^0.6.2") (d #t) (k 2)) (d (n "jens") (r "^0.6.0") (d #t) (k 0)) (d (n "jens_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0zwylkl4185qvnx8cg2zw6bd3w1qik9cjfkr5pjmgs6j729yy810")))

