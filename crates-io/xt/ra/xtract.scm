(define-module (crates-io xt ra xtract) #:use-module (crates-io))

(define-public crate-xtract-0.0.1 (c (n "xtract") (v "0.0.1") (h "1smb3vq5zpq9aq02bshq7s48ax3lc388nm3l7prv6vz9j5qwy71c")))

(define-public crate-xtract-0.0.2 (c (n "xtract") (v "0.0.2") (h "1sz3h6p5n6b7b664l5131p6h1mcjhc73vhqbfi443g1cs4sypcfp")))

