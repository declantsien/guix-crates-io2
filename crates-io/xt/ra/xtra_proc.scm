(define-module (crates-io xt ra xtra_proc) #:use-module (crates-io))

(define-public crate-xtra_proc-0.1.0 (c (n "xtra_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full"))) (d #t) (k 0)))) (h "1c7bg3cz1sn50yfpwhs78bdrgjnj6zlvcl2vs0aigksa4h798wh9")))

