(define-module (crates-io xt ra xtra-addons) #:use-module (crates-io))

(define-public crate-xtra-addons-0.0.1 (c (n "xtra-addons") (v "0.0.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "xtra") (r "^0.5.0-rc.1") (f (quote ("with-async_std-1"))) (d #t) (k 0)))) (h "1jis4kqz0906c0np5wqi42zmza62wwmicch78ckq7cnpqxdrg8k2")))

(define-public crate-xtra-addons-0.0.2 (c (n "xtra-addons") (v "0.0.2") (d (list (d (n "async-std") (r "^1.9") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "xtra") (r "^0.5.0-rc.1") (f (quote ("with-async_std-1"))) (d #t) (k 0)))) (h "04s5rpxm9hqhr5k8mfzngga5b3qgvj1r68f2xs1gscxc86d9v8dc")))

