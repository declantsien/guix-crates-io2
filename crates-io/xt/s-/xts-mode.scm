(define-module (crates-io xt s- xts-mode) #:use-module (crates-io))

(define-public crate-xts-mode-0.1.0 (c (n "xts-mode") (v "0.1.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0czfjcyqzfy57xdy3b9nj5im9nnh14vz2r5qc9jf5bkz7gw1hsyx")))

(define-public crate-xts-mode-0.2.0 (c (n "xts-mode") (v "0.2.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0bcq012rp86dggcblingqcc6a75kik5grdbzk04q2x6pirsq1s20")))

(define-public crate-xts-mode-0.3.0 (c (n "xts-mode") (v "0.3.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1rn8dv1ljj3vrl5qb2rp6lkimc8nbfdqbshpq9mw7lw34vrrpgvk")))

(define-public crate-xts-mode-0.4.0 (c (n "xts-mode") (v "0.4.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1qsqm2b17kppd6z2cn22031rc2nlchhvh566qdrkf1fk2p1pddc4")))

(define-public crate-xts-mode-0.4.1 (c (n "xts-mode") (v "0.4.1") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1nfw42jwfgspxam36hircqxj3imn8fy8agvk2i9jfj0xyai9k83m")))

(define-public crate-xts-mode-0.5.0 (c (n "xts-mode") (v "0.5.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0icpx6grvzjzhzx4dynis2lh6mqm2cnlipmchhhg5dkbqrkykdk4") (f (quote (("openssl_tests" "openssl"))))))

(define-public crate-xts-mode-0.5.1 (c (n "xts-mode") (v "0.5.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0815sjzcvwi5xn6939l74rqy20rkfkl57imxlzzvk82wajvxvjq9") (f (quote (("std") ("openssl_tests" "openssl") ("default" "std")))) (r "1.57")))

