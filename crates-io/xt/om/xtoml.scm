(define-module (crates-io xt om xtoml) #:use-module (crates-io))

(define-public crate-xtoml-0.0.0 (c (n "xtoml") (v "0.0.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "06223pc9xjmvxi9znfbnkn3cl95mr24qybizsqm74f4j5azxij1b") (f (quote (("default"))))))

