(define-module (crates-io xt ag xtagger) #:use-module (crates-io))

(define-public crate-xtagger-0.1.0 (c (n "xtagger") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "xtag") (r "^0.1.0") (d #t) (k 0)))) (h "09dff4ssdn108ik9vv8ivbv28pq3v6x3sk7gwdb856241j0l8idq")))

(define-public crate-xtagger-1.0.0 (c (n "xtagger") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "xtag") (r "^1.0.0") (d #t) (k 0)))) (h "1774yydgs98hmlg0ccjxy67cniz5fdnyd14f8m21zkw5gidah4sr")))

(define-public crate-xtagger-1.1.0 (c (n "xtagger") (v "1.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xtag") (r "^1.1.0") (d #t) (k 0)))) (h "0iy18nw20rkn14qj9ckchqih56zxnsmpxaxjl4lb2q2gzinxhjsl")))

(define-public crate-xtagger-1.2.0 (c (n "xtagger") (v "1.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "xtag") (r "^1.1.0") (d #t) (k 0)))) (h "0cvpf5gcccmha0s048rrx63hlz16mdsgxymjj1m7j2kdh02asj0n")))

