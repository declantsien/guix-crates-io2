(define-module (crates-io xt ag xtag) #:use-module (crates-io))

(define-public crate-xtag-0.1.0 (c (n "xtag") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xattr") (r "^0.2") (d #t) (k 0)))) (h "1hr6sxrg6klmal19ncaj32mzbjjka85jq148jgc2p6qnhvjhdl7r")))

(define-public crate-xtag-1.0.0 (c (n "xtag") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xattr") (r "^0.2") (d #t) (k 0)))) (h "03p2kkw27cfsq5nim3y9ff60xx6zj7s0aj2z8q9304kn11yqwvq8")))

(define-public crate-xtag-1.1.0 (c (n "xtag") (v "1.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "xattr") (r "^0.2") (d #t) (k 0)))) (h "1h411dm4i9f5fy58s8yk5npm02bp2iw2c0nvh5p9qkyjvxfpib1z")))

