(define-module (crates-io xt ld xtld) #:use-module (crates-io))

(define-public crate-xtld-0.0.1 (c (n "xtld") (v "0.0.1") (d (list (d (n "psl") (r "^2.1.4") (d #t) (k 0)) (d (n "xstr") (r "^0.0.1") (d #t) (k 0)))) (h "1sjsqflhpay7pjy8hhmkh29mi881av7ml39fs6yjwj9p94xl6qp5")))

(define-public crate-xtld-0.0.2 (c (n "xtld") (v "0.0.2") (d (list (d (n "psl") (r "^2.1.4") (d #t) (k 0)) (d (n "xstr") (r "^0.0.1") (d #t) (k 0)))) (h "00xq7rc8yfbr27n53aah255m36z01h72yqvcbxmwd0qj32nkml4s")))

(define-public crate-xtld-0.0.3 (c (n "xtld") (v "0.0.3") (d (list (d (n "psl") (r "^2.1.4") (d #t) (k 0)) (d (n "xstr") (r "^0.0.1") (d #t) (k 0)))) (h "0phl1nf8phzskx06z6jbd2vkgm1whmxrfmg9pcl481w41kc3wfhx")))

(define-public crate-xtld-0.1.0 (c (n "xtld") (v "0.1.0") (d (list (d (n "psl") (r "^2.1.4") (d #t) (k 0)) (d (n "xstr") (r "^0.1.0") (d #t) (k 0)))) (h "1wmp25r8jmwdr54y3dr3r88c69jsxfnc6jpdys6rcfp67vmd0b3z")))

(define-public crate-xtld-0.1.1 (c (n "xtld") (v "0.1.1") (d (list (d (n "psl") (r "^2.1.9") (d #t) (k 0)) (d (n "xstr") (r "^0.1.0") (d #t) (k 0)))) (h "1xy8zzplm5mqd979vb25l1bkzdyj8gahwhzq6qjjnbh5yisnrvq0")))

