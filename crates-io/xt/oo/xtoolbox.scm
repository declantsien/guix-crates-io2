(define-module (crates-io xt oo xtoolbox) #:use-module (crates-io))

(define-public crate-xtoolbox-0.1.0 (c (n "xtoolbox") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wzyjdjygy02ybplxdqcw1fhqgnvh3rmkm8nbmab98g6p8wi0xd6")))

(define-public crate-xtoolbox-0.1.1 (c (n "xtoolbox") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "16xy9y8hn464wkc554qx9pa904nbnypk1ylfnbszkk3w2hqh86ij")))

(define-public crate-xtoolbox-0.1.2 (c (n "xtoolbox") (v "0.1.2") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fhpvrh815ifidmzybiynag9cl8vd3hv4nzpjv5v7r4q3qqzdrzp")))

