(define-module (crates-io xt ea xtea-cipher) #:use-module (crates-io))

(define-public crate-xtea-cipher-0.0.1 (c (n "xtea-cipher") (v "0.0.1") (h "0qrfdc76wvm14hq59g5j6c16b61la3nfrhn5wqhvy0kfsiq0bxrl") (r "1.60")))

(define-public crate-xtea-cipher-0.0.2 (c (n "xtea-cipher") (v "0.0.2") (h "0f3s5k3qcm6h5hnrfv0ipag08slszd64qdpxrq3lcnqkh63bq3lw") (r "1.60")))

