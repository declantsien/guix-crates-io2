(define-module (crates-io xt en xtensa-lx-rt) #:use-module (crates-io))

(define-public crate-xtensa-lx-rt-0.5.0 (c (n "xtensa-lx-rt") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1axgizb6r04rrrcpvqn463hjipv1yd1xx0d5qhvmwa2407fraisa") (f (quote (("lx6") ("lx106"))))))

(define-public crate-xtensa-lx-rt-0.6.0 (c (n "xtensa-lx-rt") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "0p8kdq2sf0dbr00cw3hny0yl8jkjyg16nz0sp8dfd8sxn0z645d3") (f (quote (("lx6") ("lx106"))))))

(define-public crate-xtensa-lx-rt-0.7.0 (c (n "xtensa-lx-rt") (v "0.7.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1dqkk2pmm2n58mxx41ijdmrvbw68sc2lx63r1f40gmx7jps6118m") (f (quote (("lx6") ("lx106"))))))

(define-public crate-xtensa-lx-rt-0.8.0 (c (n "xtensa-lx-rt") (v "0.8.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "0pz4n21x685991l5bg4ski3zyvsv03qkfq12fbdrjgjrpdnvy8zj") (f (quote (("esp8266") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.9.0 (c (n "xtensa-lx-rt") (v "0.9.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "^0.1.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.13.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "12k9fs162xl0g8h1kyjk03kz9ff1h39mrrcgnnx2f0j81d62c3rm") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.10.0 (c (n "xtensa-lx-rt") (v "0.10.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1i5i3b619hc8fyhfbq6mb21wm3br7b3xwkf5ycj527qn30cmcfaz") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.11.0 (c (n "xtensa-lx-rt") (v "0.11.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "0lqn2dm4gk41z8cnr0gwhqxnyg40k6fwi45181ik29xmjliwxa9g") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.12.0 (c (n "xtensa-lx-rt") (v "0.12.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1nciky7d88a6v980ipawg5i96w4z76jsyqaspm7ca7d6ax6zy2r7") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.13.0 (c (n "xtensa-lx-rt") (v "0.13.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1x6jrdfv87h1mkmin8g0pp9l18vf8nvaz1fa5m7xm62rsa1pm89k") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.14.0 (c (n "xtensa-lx-rt") (v "0.14.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1nzdwp2x1ba6m3y09s0ld5q1ib0x3lc8hjc7kfx6fanv4rkd7ry5") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.15.0 (c (n "xtensa-lx-rt") (v "0.15.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.2.0") (d #t) (k 0)))) (h "1z538pc8p64m86zlms652r5lhvm5yhkpadlxr7s5ybcfxcvfm3zn") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32"))))))

(define-public crate-xtensa-lx-rt-0.16.0 (c (n "xtensa-lx-rt") (v "0.16.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "core-isa-parser") (r "=0.2.0") (d #t) (k 1)) (d (n "minijinja") (r "^1.0.7") (d #t) (k 1)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx-rt-proc-macros") (r "=0.2.1") (d #t) (k 0)))) (h "06nm8ys9hm2id1w279axmr1yqj637p3g7ibm69g9l33qic804hch") (f (quote (("esp8266") ("esp32s3") ("esp32s2") ("esp32")))) (r "1.65")))

