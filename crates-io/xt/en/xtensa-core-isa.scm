(define-module (crates-io xt en xtensa-core-isa) #:use-module (crates-io))

(define-public crate-xtensa-core-isa-0.1.0 (c (n "xtensa-core-isa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0bckiqkb58b38xgcmynzs0v9hkpqm14q5m9gqqzfxnzp7mm2ff18") (y #t)))

