(define-module (crates-io xt en xtensa-lx6-rt) #:use-module (crates-io))

(define-public crate-xtensa-lx6-rt-0.1.0 (c (n "xtensa-lx6-rt") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "03pcxgi9mh4a8b3kqsv61ic2lanjmsvjsvvsw36566sb6icpmb75") (l "xtensa-lx6-rt")))

(define-public crate-xtensa-lx6-rt-0.2.0 (c (n "xtensa-lx6-rt") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1v79n4y2crv9x7mav6yy9r86gkv2kh8qpd60jpjwxmw80xnnw1fr")))

(define-public crate-xtensa-lx6-rt-0.3.0 (c (n "xtensa-lx6-rt") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "1axzclqq6i8nyxnymj69i6aa2czgflgbxp4fgbzdakxlsxwkx8v4")))

(define-public crate-xtensa-lx6-rt-0.4.0 (c (n "xtensa-lx6-rt") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "xtensa-lx6-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "13k07r80ypvmm40703vqysn0xbcg3zizlz1k3w0j397vbp43a818")))

