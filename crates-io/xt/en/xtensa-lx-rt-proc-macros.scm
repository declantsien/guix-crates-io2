(define-module (crates-io xt en xtensa-lx-rt-proc-macros) #:use-module (crates-io))

(define-public crate-xtensa-lx-rt-proc-macros-0.1.0 (c (n "xtensa-lx-rt-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "02vkrpqfh3cghwwsbsd8jwagwvdx6dha9xriq8axbnz2604j1a11")))

(define-public crate-xtensa-lx-rt-proc-macros-0.2.0 (c (n "xtensa-lx-rt-proc-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0yab824qr437gc9b32c24f2agci00wacghwdhqhmsgjd6dg830rj")))

(define-public crate-xtensa-lx-rt-proc-macros-0.2.1 (c (n "xtensa-lx-rt-proc-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19rdmw1j04dqsypffzar4clnzlgkwn2l1rxh2npwkglb17gdwb08") (r "1.65")))

