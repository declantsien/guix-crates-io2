(define-module (crates-io xt en xtensa-lx106-rt) #:use-module (crates-io))

(define-public crate-xtensa-lx106-rt-0.0.1 (c (n "xtensa-lx106-rt") (v "0.0.1") (d (list (d (n "esp8266") (r "^0.0.6") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "xtensa-lx106-rt-proc-macros") (r "=0.1.0") (d #t) (k 0)))) (h "14p0fj56c20vkk6dcbz0hmvmmhvwh3p48mwf9s0k4kslpwz58srp")))

(define-public crate-xtensa-lx106-rt-0.1.0 (c (n "xtensa-lx106-rt") (v "0.1.0") (d (list (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "xtensa-lx106-rt-proc-macros") (r "=0.1.1") (d #t) (k 0)))) (h "14p9v5bnjnj3y5pv64piw4qp2q1x9256rszrhqarvyhfszv83wqf")))

(define-public crate-xtensa-lx106-rt-0.1.1 (c (n "xtensa-lx106-rt") (v "0.1.1") (d (list (d (n "r0") (r "^1.0") (d #t) (k 0)) (d (n "xtensa-lx106-rt-proc-macros") (r "=0.1.1") (d #t) (k 0)))) (h "1vvxnjqhab82l29n8xzjvcw9rshqvgrkd7d42bji58kcvr6s240x") (l "xtensa-lx106")))

(define-public crate-xtensa-lx106-rt-0.1.2 (c (n "xtensa-lx106-rt") (v "0.1.2") (d (list (d (n "r0") (r "^1.0") (d #t) (k 0)) (d (n "xtensa-lx106-rt-proc-macros") (r "=0.1.2") (d #t) (k 0)))) (h "1xwd6r4zjdjg2hyg0k3q55m8rxfn602bqy2xq23cysf32ih3knyv") (l "xtensa-lx106")))

