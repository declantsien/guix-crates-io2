(define-module (crates-io xt en xtensa-lx106-rt-proc-macros) #:use-module (crates-io))

(define-public crate-xtensa-lx106-rt-proc-macros-0.1.0 (c (n "xtensa-lx106-rt-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0d66qi21f0b2jfyknxqa054sx4z3amn6pj2g9cb8v7ax6zyn79xr")))

(define-public crate-xtensa-lx106-rt-proc-macros-0.1.1 (c (n "xtensa-lx106-rt-proc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qsx6xgabzq8aawyhdgz15a15psa02ls6yjhzpmw0b2swgvwysdz")))

(define-public crate-xtensa-lx106-rt-proc-macros-0.1.2 (c (n "xtensa-lx106-rt-proc-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "109psnb9abwq2rs14qky1bkmcr4in1fdhdwxlhyhxwgm4qx4g7ms")))

