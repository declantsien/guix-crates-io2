(define-module (crates-io xt en xtensa-lx6-rt-proc-macros) #:use-module (crates-io))

(define-public crate-xtensa-lx6-rt-proc-macros-0.1.0 (c (n "xtensa-lx6-rt-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1k20d9v5z4g8sipvyhdsnai1lg6svf5pga8hrifclzk2snrkziaw")))

