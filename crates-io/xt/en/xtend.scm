(define-module (crates-io xt en xtend) #:use-module (crates-io))

(define-public crate-xtend-0.1.0 (c (n "xtend") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "06vijp66f02knj0ag4c842kg375smqipsx98klkhq9w8n6kia019")))

(define-public crate-xtend-1.0.0 (c (n "xtend") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0rww5i7lvgjax5mnhfdns4k70amwclsmr4y2a2p69wdv2p444h17")))

(define-public crate-xtend-1.0.5 (c (n "xtend") (v "1.0.5") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "02drziv5pgklzjy4h9v7cvn2kczcqw67s1q9k0sw5j1knl318pjj")))

(define-public crate-xtend-1.1.0 (c (n "xtend") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1axhwddin6523gdyzh25khb62jh2gai4pah8yi1zm8bfifklpk56")))

(define-public crate-xtend-1.2.0 (c (n "xtend") (v "1.2.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1dg2mfbm426lbfkvx5whcvyxkwx27nf7hp7rx8p1q11yx3kx1m4a")))

(define-public crate-xtend-1.2.1 (c (n "xtend") (v "1.2.1") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0awd8ycm32pkx9fksg8q97dhwdd0p8633d125cxdrinf8d726y5v")))

(define-public crate-xtend-1.2.2 (c (n "xtend") (v "1.2.2") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1qybk4zijvqrlhmkwgab2nig3jyakcr3gld09v88622iv8qs58ch")))

(define-public crate-xtend-1.3.0 (c (n "xtend") (v "1.3.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0paswfwzkz15vy7a5x1gv8ypm9fmhdjp7ly4xys2622850mbz4s0")))

(define-public crate-xtend-2.0.0 (c (n "xtend") (v "2.0.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1rxkagivbkwrbkz1ywas9fndw1169ygz0dx0facwwdlxqf77sw2a")))

(define-public crate-xtend-3.0.0 (c (n "xtend") (v "3.0.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qvcr8q3f6aqbzw2rmjddyn7is567marh7zqmxl05kdysa6big1j")))

