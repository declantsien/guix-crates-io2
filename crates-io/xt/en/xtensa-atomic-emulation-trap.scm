(define-module (crates-io xt en xtensa-atomic-emulation-trap) #:use-module (crates-io))

(define-public crate-xtensa-atomic-emulation-trap-0.1.0 (c (n "xtensa-atomic-emulation-trap") (v "0.1.0") (d (list (d (n "xtensa-lx-rt") (r "^0.12.0") (d #t) (k 0)))) (h "0zk3cq8wgk84nqdsr3pjb24kkd7bp664lv8mbm6nsimrllwlxziw") (f (quote (("esp8266" "xtensa-lx-rt/esp8266") ("esp32s3" "xtensa-lx-rt/esp32s3") ("esp32s2" "xtensa-lx-rt/esp32s2") ("esp32" "xtensa-lx-rt/esp32"))))))

(define-public crate-xtensa-atomic-emulation-trap-0.2.0 (c (n "xtensa-atomic-emulation-trap") (v "0.2.0") (d (list (d (n "xtensa-lx-rt") (r "^0.13.0") (d #t) (k 0)))) (h "1h9ngi6ji5mwr5zsmhszwlzarhpfs1n6mbrmx8yl96k864lrqk96") (f (quote (("esp8266" "xtensa-lx-rt/esp8266") ("esp32s3" "xtensa-lx-rt/esp32s3") ("esp32s2" "xtensa-lx-rt/esp32s2") ("esp32" "xtensa-lx-rt/esp32"))))))

(define-public crate-xtensa-atomic-emulation-trap-0.3.0 (c (n "xtensa-atomic-emulation-trap") (v "0.3.0") (d (list (d (n "xtensa-lx-rt") (r "^0.14.0") (d #t) (k 0)))) (h "12wvv0g0dnv3ppc5a22n7lbzh8c49r1ym5mldxd5qd695alw2pgj") (f (quote (("esp8266" "xtensa-lx-rt/esp8266") ("esp32s3" "xtensa-lx-rt/esp32s3") ("esp32s2" "xtensa-lx-rt/esp32s2") ("esp32" "xtensa-lx-rt/esp32"))))))

(define-public crate-xtensa-atomic-emulation-trap-0.3.1 (c (n "xtensa-atomic-emulation-trap") (v "0.3.1") (d (list (d (n "xtensa-lx-rt") (r "^0.14.0") (d #t) (k 0)))) (h "0f45wdlpqh5slj7ml64d0c0kpjywsaab4xyl0bf8cfjxzh1d8g7d") (f (quote (("esp8266" "xtensa-lx-rt/esp8266") ("esp32s2" "xtensa-lx-rt/esp32s2"))))))

(define-public crate-xtensa-atomic-emulation-trap-0.4.0 (c (n "xtensa-atomic-emulation-trap") (v "0.4.0") (h "0asqy2ad2gca2dwp8winyih5arqdd808pzxyv9wbmn7m64jn7lcc")))

