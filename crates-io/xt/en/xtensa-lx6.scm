(define-module (crates-io xt en xtensa-lx6) #:use-module (crates-io))

(define-public crate-xtensa-lx6-0.1.0 (c (n "xtensa-lx6") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)))) (h "04f6xg0ly025xy4lr5qpk7wwr612ixaz3j9sz94g2d826vb599k5") (l "xtensa-lx6")))

(define-public crate-xtensa-lx6-0.2.0 (c (n "xtensa-lx6") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (f (quote ("const-fn"))) (d #t) (k 0)) (d (n "mutex-trait") (r "^0.2") (d #t) (k 0)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)))) (h "1kzf3igmqsbsfv08qfdmnaza8s1p8hrarh8yjzj48cxd9ngh4p1r") (l "xtensa-lx6")))

