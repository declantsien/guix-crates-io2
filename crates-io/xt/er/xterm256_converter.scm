(define-module (crates-io xt er xterm256_converter) #:use-module (crates-io))

(define-public crate-xterm256_converter-0.1.0 (c (n "xterm256_converter") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "1wdq4yywfx4srgjsq2ncn581nmgsxvxzi2x5nark1isq97xyzw8y")))

(define-public crate-xterm256_converter-0.2.0 (c (n "xterm256_converter") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)))) (h "0i3w2hjnmg9hymrw5c442wxxhmnh866n0ca5gprky9gqmzrj4mbv")))

(define-public crate-xterm256_converter-0.2.1 (c (n "xterm256_converter") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)))) (h "19dklfbpad32sph9mrgnmfqqvpkakksjvshbaydlnw9rkxd4w4gk")))

