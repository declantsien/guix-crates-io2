(define-module (crates-io xt er xterm-parser) #:use-module (crates-io))

(define-public crate-xterm-parser-0.1.0 (c (n "xterm-parser") (v "0.1.0") (h "0silvpsivr8c1608gwl37mch6vb9nkm4vphy63pm9ajgagvz20p7") (y #t)))

(define-public crate-xterm-parser-0.0.1 (c (n "xterm-parser") (v "0.0.1") (h "0nnsadgvjzwq1j3rfppxmsqv1gix3i08c9ahfrvq59z39yxm0dd0")))

