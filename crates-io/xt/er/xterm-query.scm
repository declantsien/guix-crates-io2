(define-module (crates-io xt er xterm-query) #:use-module (crates-io))

(define-public crate-xterm-query-0.1.0 (c (n "xterm-query") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d10j5gchdih9j452kzb13mlx77w2knk4r9najkkkng2cy9axwzw")))

(define-public crate-xterm-query-0.2.0 (c (n "xterm-query") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04va2jf7k2d6yppbhgjarmvk8rall320pnf0ml8b1g7gqzlsn0pc")))

(define-public crate-xterm-query-0.3.0 (c (n "xterm-query") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rn0d28wx7r8111wcnp9j0g16k3gvmljzncp7rkgfv52qnag0sjs")))

(define-public crate-xterm-query-0.4.0 (c (n "xterm-query") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.21") (d #t) (k 2)) (d (n "nix") (r "^0.28") (f (quote ("poll"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z4v7s8gqlri8dlrd76gn9bja3b6i9d3khc18xqw3a1c196m0a9g")))

