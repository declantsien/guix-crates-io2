(define-module (crates-io xt p- xtp-test) #:use-module (crates-io))

(define-public crate-xtp-test-0.0.1-rc1 (c (n "xtp-test") (v "0.0.1-rc1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)))) (h "0p9qnm8lsasq5wcblkjyv8frnp4nkw8jyzjjmw0s44vw5ailhmdn")))

(define-public crate-xtp-test-0.0.1-rc2 (c (n "xtp-test") (v "0.0.1-rc2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)))) (h "0jxwr5hj4cb2kkmqgs7qcdghmwb47gkbrdq6l1fn7xgzlqdhp7wi")))

(define-public crate-xtp-test-0.0.1-rc3 (c (n "xtp-test") (v "0.0.1-rc3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)))) (h "04343lp41jk1vg66cqyzy8iqb9ls8fas3xwjdqnrivngk9dadg28")))

(define-public crate-xtp-test-0.0.1-rc4 (c (n "xtp-test") (v "0.0.1-rc4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)))) (h "1m9fxw7yxl3h8bkcnw8x0xw361jpy19ad8f4j8dggbbf5cins0k4")))

(define-public crate-xtp-test-0.0.1-rc5 (c (n "xtp-test") (v "0.0.1-rc5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "00wv1x6yv37p4avjnix8cpy1m7q1cj704xgdf4x0z7g2qnvm8ari")))

(define-public crate-xtp-test-0.0.1-rc6 (c (n "xtp-test") (v "0.0.1-rc6") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "0l8x5k11j2r7b0cy0xrk2mm21dvk901ki1c5jgr81ssfqbwa1866")))

(define-public crate-xtp-test-0.0.1-rc7 (c (n "xtp-test") (v "0.0.1-rc7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "157k378fvcbh6m9ykxj0mpm4k014lkgpyazmims4m7n0rnyjxwsc")))

(define-public crate-xtp-test-0.0.1 (c (n "xtp-test") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "1wkygn8dfrszp5g1fwch47901vfh60x7qp6smx7m1gyp3nkgc6dg")))

