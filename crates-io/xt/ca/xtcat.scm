(define-module (crates-io xt ca xtcat) #:use-module (crates-io))

(define-public crate-xtcat-0.2.0 (c (n "xtcat") (v "0.2.0") (h "1p0rjfp1b6nfkkidsr7m68cyy2cbwbp1fzqjf0np29ifsykl0glj") (y #t)))

(define-public crate-xtcat-0.3.0 (c (n "xtcat") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0msdd25ab9vpir47v7mnxhp5rcf3w9maix9zzkpwz0x6aq5a0dlz") (y #t)))

(define-public crate-xtcat-0.3.1 (c (n "xtcat") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "03nkxgvwpzcq0ifq947cyg7wfc82kiqdsq732na1shqlmg8276vx")))

(define-public crate-xtcat-0.3.2 (c (n "xtcat") (v "0.3.2") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0cxqcn379ni2xby59zmrzn3mkhkhmfb3y41f6c247r9zblr4bp6k")))

