(define-module (crates-io xt li xtlid) #:use-module (crates-io))

(define-public crate-xtlid-0.1.0 (c (n "xtlid") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 1)))) (h "08czh5d4rd6q60cn9x8fs3psb9vcaambik4mk0aq3z093s407vad") (y #t)))

(define-public crate-xtlid-0.1.1 (c (n "xtlid") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 1)))) (h "0myffcp22yx6jw22i92agicqjrsma3i1867qjkp6qpyijhgiip12")))

(define-public crate-xtlid-0.1.2 (c (n "xtlid") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 1)))) (h "15l8zgzh7pdv2qhcbg30kk4pn6izzg723whvdjw6kh288gd4p5rz")))

