(define-module (crates-io xt re xtree) #:use-module (crates-io))

(define-public crate-xtree-0.1.0 (c (n "xtree") (v "0.1.0") (h "1d6srnr8z5a62b74xwjz7syx5h44i6agrl15jspzdl02i7s8gcaf")))

(define-public crate-xtree-0.1.1 (c (n "xtree") (v "0.1.1") (h "1bkgvw4vhhnmjjzh9fw7ii939bznhjii84z9l5n7xqiwikf42zbq")))

(define-public crate-xtree-0.1.2 (c (n "xtree") (v "0.1.2") (h "012c8fv4jdsvykvzcvmz78zphf6a8gbjxm533r2n8xvqijix3gyx")))

(define-public crate-xtree-0.1.3 (c (n "xtree") (v "0.1.3") (h "0x5hgimi9az9w2vn5429d3vg6ff9xrww21sipqh7nqkdhp1ipckx")))

(define-public crate-xtree-0.1.4 (c (n "xtree") (v "0.1.4") (h "1k9yzd88qy94g0hgwiyzm6y1d6m9148kynh2gjdk1vip4hc1prgi")))

(define-public crate-xtree-0.1.5 (c (n "xtree") (v "0.1.5") (h "1qa48qx91hlddb90w1h7qb7g9wc4k0050jb65g2mxbmkk0rh56zy")))

(define-public crate-xtree-0.1.6 (c (n "xtree") (v "0.1.6") (h "03zjyr9yqaa79v5mvssqfrh981bw1cl7b5f1ypr96fg92fcmgnng")))

(define-public crate-xtree-0.1.7 (c (n "xtree") (v "0.1.7") (h "018vcgfygvcqdzzyf3l5kbdjh7720086bfglz9qjq2aiwzicqfx4")))

(define-public crate-xtree-0.1.8 (c (n "xtree") (v "0.1.8") (h "1qq9c21z9pzvqdhzzbsmi3cmvzf197i6xcrjyy00zflyn282agyj")))

