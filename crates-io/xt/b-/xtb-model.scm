(define-module (crates-io xt b- xtb-model) #:use-module (crates-io))

(define-public crate-xtb-model-0.0.6 (c (n "xtb-model") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0y9mac6lsxv76c5gi4r69plj3vp4v0qcw4kl7a22qxn95603s20f") (f (quote (("adhoc"))))))

