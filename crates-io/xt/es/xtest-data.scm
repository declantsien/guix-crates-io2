(define-module (crates-io xt es xtest-data) #:use-module (crates-io))

(define-public crate-xtest-data-0.0.1 (c (n "xtest-data") (v "0.0.1") (d (list (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "slotmap") (r "^1") (k 0)) (d (n "url") (r "^2") (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "0facmld3k8b388wnj8h9hd5h92d5wckrgha9n0rpbdsrrwbby0vi")))

(define-public crate-xtest-data-0.0.2 (c (n "xtest-data") (v "0.0.2") (d (list (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "slotmap") (r "^1") (k 0)) (d (n "url") (r "^2") (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "1wnb3pgfci3b905inf2y92zl19bsbppabyjl3lzxhl9n3hldfbw8")))

(define-public crate-xtest-data-0.0.3 (c (n "xtest-data") (v "0.0.3") (d (list (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "18s9x3l3pdsyfj1pl2239arzqk5njjd3lsxw1zynhy10ajqk1rbv")))

(define-public crate-xtest-data-0.0.4 (c (n "xtest-data") (v "0.0.4") (d (list (d (n "fslock") (r "^0.1.8") (f (quote ("multilock"))) (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "0z9rdms4c99g9kfha7z06k90q8k35vz7kpzc9kidvhp1prgq8l7r") (r "1.53")))

(define-public crate-xtest-data-1.0.0-beta (c (n "xtest-data") (v "1.0.0-beta") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "0g8rqwaw06r2w61hssariyzf5bhbp9rpxiqn5hs8aklc83nybj5k") (r "1.53")))

(define-public crate-xtest-data-1.0.0-beta.2 (c (n "xtest-data") (v "1.0.0-beta.2") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "18fdikzqqck81ywp3m3i8l3lmd24zmy1y33v3ja6aqqkf74pmbxn") (r "1.53")))

(define-public crate-xtest-data-1.0.0-beta.3 (c (n "xtest-data") (v "1.0.0-beta.3") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "1i7kp82z9sxhybf1slrl5xz72zlz5w5qvhka3mw00a57l63z512z") (y #t) (r "1.53")))

(define-public crate-xtest-data-1.0.0-beta.4 (c (n "xtest-data") (v "1.0.0-beta.4") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "0xj0ckp3zyxmnc5m172mfv9kdcl9y9r8f5664s1j561jl4hz0jy2") (r "1.53")))

(define-public crate-xtest-data-1.0.0-beta.5 (c (n "xtest-data") (v "1.0.0-beta.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8") (o #t) (d #t) (k 0)) (d (n "tinyjson") (r "^2") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.5") (o #t) (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "1ad6g2x17ycbyj9yy8ab1zgdw0p4562q5l9xa0lrkrngi4y6cazk") (s 2) (e (quote (("bin-xtask" "dep:clap" "dep:serde" "dep:tempfile" "dep:tinytemplate" "dep:toml" "dep:ureq")))) (r "1.53")))

