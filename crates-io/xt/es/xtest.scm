(define-module (crates-io xt es xtest) #:use-module (crates-io))

(define-public crate-xtest-0.0.0 (c (n "xtest") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)) (d (n "xdoc") (r "^0.0.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0hpasfj3b8dri9m2v1q10nd3g1q8x46hisfpklv3yj8akaqgdm5y")))

(define-public crate-xtest-0.0.1 (c (n "xtest") (v "0.0.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "serde_plain") (r "^0.3.0") (d #t) (k 0)) (d (n "xdoc") (r "^0.0.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1x3c0ybimr3zbzgdhjzbbl35hfjyflcf3lqfjgjdi8yhz2ibjdh2")))

(define-public crate-xtest-0.0.2 (c (n "xtest") (v "0.0.2") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 1)))) (h "1gc7q67r1xzvis7g31wywax9z4n91d6n73v2q1h3rq5xa3kr51k6")))

