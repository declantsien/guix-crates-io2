(define-module (crates-io xt as xtask-wasm-run-example) #:use-module (crates-io))

(define-public crate-xtask-wasm-run-example-0.1.0 (c (n "xtask-wasm-run-example") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "020pjrrrz1m0cxm9k46v8ihdigwh9gyfv9babjm4fhr148v4gdsl")))

(define-public crate-xtask-wasm-run-example-0.1.1 (c (n "xtask-wasm-run-example") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "10pssaq444zk3m1q7hg43v3w4gibbn3rd53pm2gsvavmakk01718")))

(define-public crate-xtask-wasm-run-example-0.1.2 (c (n "xtask-wasm-run-example") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0l2qmpr0wp2sgj5hlvzisgqg283jzzymm0lyylqhhn8r9b6asg4d")))

(define-public crate-xtask-wasm-run-example-0.1.3 (c (n "xtask-wasm-run-example") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1x31wmq1rj75spwvnjm3j8wbrn9n9fkm456k103flnd0l923dmmr")))

(define-public crate-xtask-wasm-run-example-0.2.0 (c (n "xtask-wasm-run-example") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0rda516cjmmc4qz1ryfyvqjyr39bkjyiqxpzv7xlp9hw72lbwznh") (y #t)))

(define-public crate-xtask-wasm-run-example-0.2.1 (c (n "xtask-wasm-run-example") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5faigy89mcrsl7hymp6r2qdv9nf2bbzcdjn5pyxnfmd49bphp1")))

(define-public crate-xtask-wasm-run-example-0.2.3 (c (n "xtask-wasm-run-example") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzz754iy7410ddpyg03b4has2ggvbrkg9q5spdb19n4kny94d1h")))

