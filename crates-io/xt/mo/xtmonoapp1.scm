(define-module (crates-io xt mo xtmonoapp1) #:use-module (crates-io))

(define-public crate-xtmonoapp1-0.1.0 (c (n "xtmonoapp1") (v "0.1.0") (h "01bvngxdycn8l0z0swdgrr32n0qscwvcpbgq6vlx8diqb9km01cb")))

(define-public crate-xtmonoapp1-0.1.1 (c (n "xtmonoapp1") (v "0.1.1") (h "0h4gcf25v3klnsqqdjpvdynrbr3f40x1ldd0vnvldn3lq2w375hz")))

