(define-module (crates-io ig rf igrf) #:use-module (crates-io))

(define-public crate-igrf-0.1.0 (c (n "igrf") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "1algwzd36w2lvzmn2y1bcvvd00l925f2crrg1bmvdzispsr1fa7v")))

(define-public crate-igrf-0.2.0 (c (n "igrf") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "16n7byhj3b8sln1nmvimb1lfrc6s5k20y389n02irf5pcan51dxf")))

