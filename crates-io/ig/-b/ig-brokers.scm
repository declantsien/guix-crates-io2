(define-module (crates-io ig -b ig-brokers) #:use-module (crates-io))

(define-public crate-ig-brokers-0.0.1 (c (n "ig-brokers") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "11y9i3whppzqmal381qh4jphqwjkmfl6liga5xg6a638hwv1cvnx")))

