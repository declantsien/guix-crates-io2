(define-module (crates-io ig so igsolve) #:use-module (crates-io))

(define-public crate-igsolve-0.1.0 (c (n "igsolve") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "igs") (r "^0.1") (d #t) (k 0)))) (h "1h7y09kcr1i91pjy8wdcirx55savyx61vyarkd69ifbf4vcad4lf")))

(define-public crate-igsolve-0.1.1 (c (n "igsolve") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "igs") (r "^0.1") (d #t) (k 0)))) (h "1wxmi4dw5d5v2bl54iphm8dwvr5qi8bhiya1xv42k63sfpvj0i4c")))

(define-public crate-igsolve-0.1.2 (c (n "igsolve") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "igs") (r "^0.1") (d #t) (k 0)))) (h "1hbpv3402wly14vxq46a59yk6286qnmsc4v6p7xixcv0zbiian4n")))

