(define-module (crates-io ig c_ igc_parser) #:use-module (crates-io))

(define-public crate-igc_parser-0.1.0 (c (n "igc_parser") (v "0.1.0") (h "07a2qcvm8x29ikanf0srgq3rk37gwxvlbz6hiqn7295jsd329an1")))

(define-public crate-igc_parser-0.1.1 (c (n "igc_parser") (v "0.1.1") (h "1lg97z6maxyy1qb61jwb2v63sawih9x5ayp90h27mix93idw5xvb")))

(define-public crate-igc_parser-0.1.2 (c (n "igc_parser") (v "0.1.2") (h "0pwf12b1q6s0fg9wnj5k0730a410r713qrhhysidss16i8d1jbks")))

(define-public crate-igc_parser-0.1.3 (c (n "igc_parser") (v "0.1.3") (h "11gqji28nfd1vg9cw8lapkjfz0ps3v8g3gfarsn1hqrwg9ljw8zy")))

(define-public crate-igc_parser-0.1.4 (c (n "igc_parser") (v "0.1.4") (h "1vccaihqvl9hafzix30bgk1xgc4a3xx519jmmgkw52xiaxfblzx6")))

(define-public crate-igc_parser-0.1.5 (c (n "igc_parser") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "18mm02yy6q1q9j272bky5y7b0qsl2vb6yq22qrymyz9da62hfchd")))

