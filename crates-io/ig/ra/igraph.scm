(define-module (crates-io ig ra igraph) #:use-module (crates-io))

(define-public crate-igraph-0.1.0 (c (n "igraph") (v "0.1.0") (h "0aj8zsbccw12slcf9pyygv3ndjnikh6d1r9cw1b043clzi82d1cf") (y #t)))

(define-public crate-igraph-0.1.1 (c (n "igraph") (v "0.1.1") (h "0hcq6zb4r35hdsz6f1y3dpc991mk3s0s60mhiy6b23phyrq6qqk8")))

