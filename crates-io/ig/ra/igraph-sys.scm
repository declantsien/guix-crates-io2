(define-module (crates-io ig ra igraph-sys) #:use-module (crates-io))

(define-public crate-igraph-sys-0.0.0 (c (n "igraph-sys") (v "0.0.0") (h "1y2jhwyx7sl2sdqacr0522qplib0ss39g5lp0v3w4f9axwbv0k74")))

(define-public crate-igraph-sys-0.0.1 (c (n "igraph-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l1gddj749ixvlfaqagi0dyihqy988gpgagmm6gz46gfc3zk92hg") (l "igraph")))

