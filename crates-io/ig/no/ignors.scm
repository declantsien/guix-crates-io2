(define-module (crates-io ig no ignors) #:use-module (crates-io))

(define-public crate-ignors-0.1.0 (c (n "ignors") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)) (d (n "spinners") (r "^4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0pqlv3x578ksgpxkp4scbxplklv1mckgzy6aafx738y3qb92i4vh") (r "1.69")))

