(define-module (crates-io ig no ignor) #:use-module (crates-io))

(define-public crate-ignor-0.1.0 (c (n "ignor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "0vx3kfqxajcm6lryabrvm3caidjfnp5hymhkarnh0n3qkpk4w496")))

(define-public crate-ignor-0.2.0 (c (n "ignor") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "11a34wrbs9xgmw5gzax0jman9zw32rdw3slylfw1dd7bh6dwfpk4")))

