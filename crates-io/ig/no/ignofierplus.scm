(define-module (crates-io ig no ignofierplus) #:use-module (crates-io))

(define-public crate-ignofierplus-0.1.0 (c (n "ignofierplus") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "requestty") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1hsdmpgnhy3b6zzfd93q5dafhg498qph4n3fx2q9jizlslhi5kq1")))

(define-public crate-ignofierplus-0.2.0 (c (n "ignofierplus") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "requestty") (r "^0.5.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0izpzfnp2klkwfqh89w2756aypam29nzf39vd82cz7fx8xjryqqz")))

