(define-module (crates-io ig no ignorance) #:use-module (crates-io))

(define-public crate-ignorance-0.1.0 (c (n "ignorance") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "loading") (r "^0.1.2") (d #t) (k 0)) (d (n "predicates") (r "^2.0.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "006ij4ax6ayymby91b8ia2zbqi07zllwqy5azi97pf8qqk1mbi6j")))

