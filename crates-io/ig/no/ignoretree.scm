(define-module (crates-io ig no ignoretree) #:use-module (crates-io))

(define-public crate-ignoretree-0.1.0 (c (n "ignoretree") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0mywymx649dsi17sxmv9q9ljshdhxfxaz84pyg6g08awwl5a3qfg")))

