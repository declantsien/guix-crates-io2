(define-module (crates-io ig no ignorers) #:use-module (crates-io))

(define-public crate-ignorers-0.1.0 (c (n "ignorers") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "mockito") (r "^0.32.3") (d #t) (k 2)))) (h "1hjzq9nq4s56k1g3b6j9hqwqklp02b111k363bvvyrl88yl0wmn5")))

(define-public crate-ignorers-0.1.1 (c (n "ignorers") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "mockito") (r "^0.32.3") (d #t) (k 2)))) (h "0gi274ssa3s28svvkslqgv7ic2mdb41pc52sr7psc6n703xhks8y")))

(define-public crate-ignorers-0.1.2 (c (n "ignorers") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "mockito") (r "^0.32.4") (d #t) (k 2)))) (h "00f900x9l17jj3x9as8vqgpibys0lw6iyv3jcxrc8qh9rghbjh8y")))

(define-public crate-ignorers-0.1.3 (c (n "ignorers") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "mockito") (r "^0.32.4") (d #t) (k 2)))) (h "10vvrv41ncgywfgxq5yy5n6i3chb9yybhm973lhs942dpvg0vgai")))

(define-public crate-ignorers-0.1.4 (c (n "ignorers") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "mockito") (r "^0.32.4") (d #t) (k 2)))) (h "1nbmc4v0zyy4dy39cz2rxyz9xjhq8rb7ywh62b4x6jam5j58mr6k")))

