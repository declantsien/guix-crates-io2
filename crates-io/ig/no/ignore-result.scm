(define-module (crates-io ig no ignore-result) #:use-module (crates-io))

(define-public crate-ignore-result-0.1.0 (c (n "ignore-result") (v "0.1.0") (h "15cc9yfx8wh93qimvzj0j8ksb47ipvw9sbv4q039aw43887pr9yl")))

(define-public crate-ignore-result-0.2.0 (c (n "ignore-result") (v "0.2.0") (h "0375w27qiaw53zxq9i82zzdisbw3964vgk210r4hvlgdx3fg8pv6")))

