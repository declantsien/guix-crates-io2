(define-module (crates-io ig lo igloo) #:use-module (crates-io))

(define-public crate-igloo-0.1.0 (c (n "igloo") (v "0.1.0") (h "0w66sp4r2dvwzav0dbhy74v6di8xg2s5klf3xf99ajbszyh53prm")))

(define-public crate-igloo-0.1.1 (c (n "igloo") (v "0.1.1") (h "0bd3frm1a8jc3n46cw621qjb0qwfs795pg7v30cqrd09s4cx679w")))

(define-public crate-igloo-0.0.9999 (c (n "igloo") (v "0.0.9999") (h "17y1a98rqpi2gd4gkjk5ihlj3nrb9yxzzgna03373h7jk93bqfdl")))

(define-public crate-igloo-1.0.0 (c (n "igloo") (v "1.0.0") (h "1vnwqi9ni5ndr52hs6mcsrmh4yd3f77f07h8y6fijp3dxas79bm9")))

(define-public crate-igloo-1.0.1 (c (n "igloo") (v "1.0.1") (h "1zbs7r4na787n48zlm94rjrg2b7aly7ky5vi53zvmhmzya761aal")))

(define-public crate-igloo-1.0.2 (c (n "igloo") (v "1.0.2") (h "1pi4vkcblg3sz2zijfar98k8szmbi4n4rr3bkz7a77hvaz1jk74z")))

