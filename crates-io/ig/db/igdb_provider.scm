(define-module (crates-io ig db igdb_provider) #:use-module (crates-io))

(define-public crate-igdb_provider-0.1.0 (c (n "igdb_provider") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros"))) (d #t) (k 2)))) (h "12y5la8zrhk6ynmai8w1nb8r8z5nzglrpx95q8snism268f8l0wc")))

