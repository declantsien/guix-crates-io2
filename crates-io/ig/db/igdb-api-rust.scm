(define-module (crates-io ig db igdb-api-rust) #:use-module (crates-io))

(define-public crate-igdb-api-rust-0.1.0 (c (n "igdb-api-rust") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "microjson") (r "^0.1.2") (d #t) (k 0)) (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.1") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dz6g1jsbm0fsacbcrlhzwyw3qqyypgbw7hywdzqjxb0ndcbbqjd")))

(define-public crate-igdb-api-rust-0.1.1 (c (n "igdb-api-rust") (v "0.1.1") (d (list (d (n "heck") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "microjson") (r "^0.1.2") (d #t) (k 0)) (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12.1") (f (quote ("std"))) (k 0)) (d (n "prost-build") (r "^0.12.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1910df3afljaas54kz4f0ngd1wscg1y7yccl0xy93i93vyn2yg78")))

