(define-module (crates-io ig ni ignite-rs) #:use-module (crates-io))

(define-public crate-ignite-rs-0.1.0 (c (n "ignite-rs") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (o #t) (d #t) (k 0)))) (h "1k7b0gbg6zj7873h5asm9jr5rjaa96klh3y5dsll4iki0bwl70dd") (f (quote (("ssl" "rustls" "webpki") ("default"))))))

(define-public crate-ignite-rs-0.1.1 (c (n "ignite-rs") (v "0.1.1") (d (list (d (n "bufstream") (r "^0.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "webpki") (r "^0.21.3") (o #t) (d #t) (k 0)))) (h "0s0smfy199xph06mcmm3ni15sl6c2znwxvd8i2jfc2kn9f9hmw67") (f (quote (("ssl" "rustls" "webpki") ("default"))))))

