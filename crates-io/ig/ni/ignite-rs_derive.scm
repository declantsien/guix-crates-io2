(define-module (crates-io ig ni ignite-rs_derive) #:use-module (crates-io))

(define-public crate-ignite-rs_derive-0.1.0 (c (n "ignite-rs_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "12rm8m81j6sy80433dscmhiwvx329p732v757hlg5wd3wdijja6l")))

(define-public crate-ignite-rs_derive-0.1.1 (c (n "ignite-rs_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "01390mpmggxv7sjyxnwsngcc36hjmyn0sf02xsfws8sim700ad82")))

