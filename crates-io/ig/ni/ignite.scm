(define-module (crates-io ig ni ignite) #:use-module (crates-io))

(define-public crate-ignite-0.1.0 (c (n "ignite") (v "0.1.0") (h "05xka2ha28ix3rgai6l8idk6g5w28j690ixs3amww17lwqp4jyjw")))

(define-public crate-ignite-0.1.1 (c (n "ignite") (v "0.1.1") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1yb0dg7s1fib6c2pjy75vnnv2kgx9qlldz05hnc8qvdvavnqnvy6")))

(define-public crate-ignite-0.1.2 (c (n "ignite") (v "0.1.2") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "11riiqj8ffb1vlpsiifb7z985kkyj834571jd3fqbq5wzdakjmpw")))

(define-public crate-ignite-0.1.3 (c (n "ignite") (v "0.1.3") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0fzw7w4vpvj6fmkiry64al5riwidbp560gcv1ikrxzwcvpiwhrig")))

(define-public crate-ignite-0.1.4 (c (n "ignite") (v "0.1.4") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0z4ml5fg62fjj7y265g45h0ql960b0bvk5k8xxzs67hyp44y6hdv")))

(define-public crate-ignite-0.1.5 (c (n "ignite") (v "0.1.5") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0k0fibw9968xzfkwfkfc1gkp1zsclv6h4w0s2kwdlxnfmrw0cqrf")))

(define-public crate-ignite-0.1.6 (c (n "ignite") (v "0.1.6") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0a7ala4pp445ssm3dzjcid8iaj3y423hpdx5pc0x3qcb4c44z43l")))

