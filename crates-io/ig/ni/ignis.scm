(define-module (crates-io ig ni ignis) #:use-module (crates-io))

(define-public crate-ignis-0.1.0 (c (n "ignis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.20") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.3") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "nintendo-lz") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0f9xh19f2iz2zih92ksarkp53zjvwaw5qlz7zsbxm7aw318j7iw0")))

