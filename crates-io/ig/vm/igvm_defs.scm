(define-module (crates-io ig vm igvm_defs) #:use-module (crates-io))

(define-public crate-igvm_defs-0.1.0 (c (n "igvm_defs") (v "0.1.0") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.3.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "13g1bczcdq19nl7pkn4g1fgsblyzjmqlpjnk0dda5dkgrndsvrib") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.1 (c (n "igvm_defs") (v "0.1.1") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.1") (f (quote ("alloc" "derive"))) (d #t) (k 0)))) (h "18l97fa5rjj4davqmyyp9igpqfs8n42daaxk87pwyllm32pdbci9") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.2 (c (n "igvm_defs") (v "0.1.2") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02ifjdf9nmrfydxh6nqvwq06m7hnjz6yyss6brl9bhhn2zs30nca") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.3 (c (n "igvm_defs") (v "0.1.3") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "112v35nxi800n16dqimmc9x5b3qzyvm8mpdhqvynrr6m855ljzbm") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.4 (c (n "igvm_defs") (v "0.1.4") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y1f07ld1swv7qkyj6q9ryww6cp57y807m6zyq1i4d07z07bnp6q") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.5 (c (n "igvm_defs") (v "0.1.5") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vqwmxhz5iybzamqgq52p4ac1r102rs265g4p3xgc7v64a2l52vh") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.6 (c (n "igvm_defs") (v "0.1.6") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "01jwd8br6bf596468d7fhnmly9anmr90vpni7w61081djfdggmy1") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.7 (c (n "igvm_defs") (v "0.1.7") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ivbmf32280hbzgps3rm380kgn4j0hwzvckdlv1q2glyq6fhfi8") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.8 (c (n "igvm_defs") (v "0.1.8") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dvmq161bgwvngcpahj1xd8prgz89ycvli8vhra2gsswy632khsl") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.1.9 (c (n "igvm_defs") (v "0.1.9") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "09xq9bn8fzaql4l1vam5cbmgd3pddbzl4744hlhiqxjx9khz0j0k") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.2.0 (c (n "igvm_defs") (v "0.2.0") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0awvl2qi9245mlqs6wsqlsa7l8rhz1b989zwl60n56hhx5dfhmwa") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.2.1 (c (n "igvm_defs") (v "0.2.1") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mwfz9z490nrvj972r7h200qyrrrizjkm87x1zg7bhp75qqqrlcz") (f (quote (("unstable") ("default"))))))

(define-public crate-igvm_defs-0.3.0 (c (n "igvm_defs") (v "0.3.0") (d (list (d (n "bitfield-struct") (r "^0.5") (d #t) (k 0)) (d (n "open-enum") (r "^0.4.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "10k3q7kvj30wg40x29c13m2i1y8xysgpq5nynj56v6nvhddx43a3") (f (quote (("unstable") ("default"))))))

