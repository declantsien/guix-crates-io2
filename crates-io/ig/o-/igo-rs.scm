(define-module (crates-io ig o- igo-rs) #:use-module (crates-io))

(define-public crate-igo-rs-0.1.0 (c (n "igo-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0zvym3pyc4qiaicc09qhls7kvy2i8jdbczqi4n3izgix7dmdvv4z")))

(define-public crate-igo-rs-0.1.1 (c (n "igo-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0fgy87hv0sh46izzbpgwpmi02q3ix1728d20wn4r7hch0rhq7dr0")))

(define-public crate-igo-rs-0.2.0 (c (n "igo-rs") (v "0.2.0") (d (list (d (n "bit-set") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4") (d #t) (k 0)))) (h "0shrv7jf3grbn3r5qhalib83iiqgaj97j4v88wbqs0si9hlph9vm")))

(define-public crate-igo-rs-0.2.1 (c (n "igo-rs") (v "0.2.1") (d (list (d (n "bit-set") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4") (d #t) (k 0)))) (h "1wji9r181vgnbwc2qnxxrq1cd1lacby8q67wgwb348xdhyifrzbf")))

(define-public crate-igo-rs-0.2.2 (c (n "igo-rs") (v "0.2.2") (d (list (d (n "bit-set") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4") (d #t) (k 0)))) (h "0c7kg2f05bxqxd2qb4y4hcr3yrmfc4b6nc390kxbjyk5kksl1mnj")))

(define-public crate-igo-rs-0.2.3 (c (n "igo-rs") (v "0.2.3") (d (list (d (n "bit-set") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.6") (d #t) (k 0)) (d (n "getopts") (r "0.*") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "simple_logger") (r "^0.4") (d #t) (k 0)))) (h "09hph72qqfsd2wp206aw49s2fmdnn1bvx1pi98njcld100zsj4jn")))

(define-public crate-igo-rs-0.2.4 (c (n "igo-rs") (v "0.2.4") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^0.5") (d #t) (k 0)))) (h "191163w33091n7skxgzb47m7pn2c8da554rn8wm8563fc08d5z4s") (f (quote (("unstable"))))))

(define-public crate-igo-rs-0.3.0 (c (n "igo-rs") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3") (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (o #t) (k 0)))) (h "13s9xw3jax4v9zz03zsl1cl4ihafijnypgya7hciv8cy1ccxh82l") (f (quote (("unstable") ("default" "zip"))))))

