(define-module (crates-io ig ri igri_derive) #:use-module (crates-io))

(define-public crate-igri_derive-0.1.0 (c (n "igri_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0nj980m85ci37fbgfzclccw9g53mjanx58zpmsbmwpmcvg67ykg6") (f (quote (("dummy"))))))

(define-public crate-igri_derive-0.1.1 (c (n "igri_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0v126ffl8zyv9mf8bw51w0spkmnvfggaskd8w6c94mn6pa6jqd2c") (f (quote (("dummy"))))))

(define-public crate-igri_derive-0.1.2 (c (n "igri_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0j96dgdikwq12mqsg0y2xqiz27clv82c7qcc94lh3rdk2dslcq6w") (f (quote (("dummy"))))))

(define-public crate-igri_derive-0.1.3 (c (n "igri_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0jr4lyk2an0i0wlm6mlj81a1d78xk21lxsibjzn594113hi7pdya") (f (quote (("dummy"))))))

