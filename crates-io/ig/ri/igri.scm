(define-module (crates-io ig ri igri) #:use-module (crates-io))

(define-public crate-igri-0.1.0 (c (n "igri") (v "0.1.0") (d (list (d (n "igri_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1d8g0gfd1a55f52r8bf0sjqjc4fq8qra3c774mnj51jp0p2aizwq") (f (quote (("dummy" "igri_derive/dummy"))))))

(define-public crate-igri-0.1.1 (c (n "igri") (v "0.1.1") (d (list (d (n "igri_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "17wmjvi3w3dyzm7rjj5d4ygr6af7s8wijx3zkm4zyqs6bz5y0wbl") (f (quote (("dummy" "igri_derive/dummy"))))))

(define-public crate-igri-0.1.2 (c (n "igri") (v "0.1.2") (d (list (d (n "igri_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "101h7hvr3r3cazkwd4x87r6cs4n95lw7y6nm5qvpc1h4x89msfzm") (f (quote (("dummy" "igri_derive/dummy"))))))

(define-public crate-igri-0.1.3 (c (n "igri") (v "0.1.3") (d (list (d (n "igri_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "imgui") (r "^0.8.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "02hrs4ba5c9r9j38ak5fg372kbj7kxdwxhzadkpjqp7ww4gd19j2") (f (quote (("dummy" "igri_derive/dummy"))))))

