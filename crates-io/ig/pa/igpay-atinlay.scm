(define-module (crates-io ig pa igpay-atinlay) #:use-module (crates-io))

(define-public crate-igpay-atinlay-0.1.0 (c (n "igpay-atinlay") (v "0.1.0") (d (list (d (n "is-vowel") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "190xh0p49g2nq0ar5lz1l5z1bg5br39hzq7lcw0vkjqgad3h9qcw")))

