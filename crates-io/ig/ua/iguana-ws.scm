(define-module (crates-io ig ua iguana-ws) #:use-module (crates-io))

(define-public crate-iguana-ws-0.1.0 (c (n "iguana-ws") (v "0.1.0") (d (list (d (n "bitcoin-cash") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0318i2z43xlc9lf61wm182pxi0gq4p3rq125canwwbdnx409aiii")))

(define-public crate-iguana-ws-0.1.1 (c (n "iguana-ws") (v "0.1.1") (d (list (d (n "bitcoin-cash") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1x3scvr9f9byvnkbi806gjycdpzvgwwgkibg75ga4rm07rmy6yvg")))

(define-public crate-iguana-ws-0.2.0 (c (n "iguana-ws") (v "0.2.0") (d (list (d (n "bitcoin-cash") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1aqr873khixl7qyzg0jsq6kwsimg2mwdgqagh0fvb8m0jnzms9aw")))

