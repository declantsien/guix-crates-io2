(define-module (crates-io ig ua iguana-interpreter) #:use-module (crates-io))

(define-public crate-iguana-interpreter-0.1.0 (c (n "iguana-interpreter") (v "0.1.0") (d (list (d (n "bitcoin-cash") (r "^0.1.2") (d #t) (k 0)))) (h "1icq44dz8ikxbc8pnlcllvnjnaai0wr1nyim334h8mb8by6rybwl")))

(define-public crate-iguana-interpreter-0.1.1 (c (n "iguana-interpreter") (v "0.1.1") (d (list (d (n "bitcoin-cash") (r "^0.1.3") (d #t) (k 0)))) (h "1vg1f7px42b6f0lca0adbsh3a8np3pfrk50nf6yqw2gkf1pwc7ci")))

(define-public crate-iguana-interpreter-0.2.0 (c (n "iguana-interpreter") (v "0.2.0") (d (list (d (n "bitcoin-cash") (r "^0.2.0") (d #t) (k 0)))) (h "1rngm8wgg3wa53ljm1h8ysvabmqrbwaskbhdhvjn1hw0jd1zadf4")))

(define-public crate-iguana-interpreter-0.2.3 (c (n "iguana-interpreter") (v "0.2.3") (d (list (d (n "bitcoin-cash") (r "^0.2.3") (d #t) (k 0)))) (h "1vs6cb3rz9xcykcnf2a65gh87x4yyjsma1j6fr3pd26myjww6j60")))

