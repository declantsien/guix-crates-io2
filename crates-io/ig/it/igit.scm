(define-module (crates-io ig it igit) #:use-module (crates-io))

(define-public crate-igit-0.1.0 (c (n "igit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "gix") (r "^0.52.0") (f (quote ("blocking-network-client" "blocking-http-transport-reqwest-native-tls"))) (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-loader") (r "^0.20.0") (d #t) (k 0)))) (h "1r1r5845mr1bjy15zlnmf04b9861iybng8a7z313bnid5c86yyzy")))

