(define-module (crates-io du pf dupfind) #:use-module (crates-io))

(define-public crate-dupfind-1.0.0 (c (n "dupfind") (v "1.0.0") (d (list (d (n "opener") (r "^0.5.0") (d #t) (k 0)) (d (n "sha256") (r "^1.0.3") (d #t) (k 0)))) (h "0wg8nq0frf069wbxra3b2ydsqda7s0va46mvmnfikpfz0mjmizsk")))

