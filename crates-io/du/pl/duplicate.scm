(define-module (crates-io du pl duplicate) #:use-module (crates-io))

(define-public crate-duplicate-0.0.0 (c (n "duplicate") (v "0.0.0") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "0z54clhg611n7px8xq8jl44rrhjp436nfc3igzls8zv7ycdqvac2")))

(define-public crate-duplicate-0.0.1 (c (n "duplicate") (v "0.0.1") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "1cjplixik9530sv2dckl840r7rb3wqr9kxxf5j7j1jiijmadnz7z")))

(define-public crate-duplicate-0.1.0 (c (n "duplicate") (v "0.1.0") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "144g9czlgja8f87nr2y0f1cw5waazdhrzmv6ya90qjrm40wj905s")))

(define-public crate-duplicate-0.1.1 (c (n "duplicate") (v "0.1.1") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "1d0x0w6czpjn9syh4q7sksgpdvb2v9nk57wvi1irfwhzxwpkzwp5")))

(define-public crate-duplicate-0.1.2 (c (n "duplicate") (v "0.1.2") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "0qg5b5593rxcgx7rad69n6g1ajjmlrk8p1nl3xi69gb5zjyb57lf")))

(define-public crate-duplicate-0.1.3 (c (n "duplicate") (v "0.1.3") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "0q0v0rb0216m9fvl579qby8yaalhlh5icvnbcvq783g7pf8kvinc")))

(define-public crate-duplicate-0.1.4 (c (n "duplicate") (v "0.1.4") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "03sj3kz7a5lqbdqgyi1f87hhqxfn2379mm89ffyd3n5qhkvsdrrs")))

(define-public crate-duplicate-0.1.5 (c (n "duplicate") (v "0.1.5") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "0p5j6s53hrcm2qk6v3sa4z3nwjhs3sng8kkfacwbbf2246hdjw95")))

(define-public crate-duplicate-0.2.0 (c (n "duplicate") (v "0.2.0") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "0f677ha3i1zmhxb0aqkkfjz9fakqbxmvd2qvi40z9xw5wwrj0zzs")))

(define-public crate-duplicate-0.2.1 (c (n "duplicate") (v "0.2.1") (d (list (d (n "macrotest") (r "^0.1.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)))) (h "04a467hi2hg0ln03py86ckcniw6365532gk48ap32ynmvm3f65z5")))

(define-public crate-duplicate-0.2.2 (c (n "duplicate") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)))) (h "11pqnmcn7jhajhk9addg5842wvk45z2jnrr9f4xi5ks9dg491iwk")))

(define-public crate-duplicate-0.2.3 (c (n "duplicate") (v "0.2.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)))) (h "1sg5263j2nsrmcvnc5p0va5gsg9jp9hzlrxaxkbx6pjx0kjq79qv") (y #t)))

(define-public crate-duplicate-0.2.4 (c (n "duplicate") (v "0.2.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)))) (h "0mbv9s6hcn3pxjhawr1fs14hqqh4fpxj78538pan0ihzwjgdif3d")))

(define-public crate-duplicate-0.2.5 (c (n "duplicate") (v "0.2.5") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.2") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.3") (d #t) (k 0)))) (h "0wankss2xg6mmg438171rgp7smfgyigalp069pss70dlx4kjnwh2")))

(define-public crate-duplicate-0.2.6 (c (n "duplicate") (v "0.2.6") (d (list (d (n "convert_case") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.4") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.3") (o #t) (d #t) (k 0)))) (h "1vrqpn0b8cryzwf03vsm5yfpdyp2wjfd44zjnjmvfkq322lgl2vk") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "convert_case") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.2.7 (c (n "duplicate") (v "0.2.7") (d (list (d (n "convert_case") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "^1.0.4") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.4") (o #t) (d #t) (k 0)))) (h "0vgswalrkr42x2y4b2vcgci4ayvgwamhjzcvq708hrakjxs7f89i") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "convert_case") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.2.8 (c (n "duplicate") (v "0.2.8") (d (list (d (n "convert_case") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "=0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "=1.0.5") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.4") (o #t) (d #t) (k 0)))) (h "19kpiv0fk9bwh8z1zmfg3az29xlr8cbgkv3crk8wc982m92fpvk7") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "convert_case") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.2.9 (c (n "duplicate") (v "0.2.9") (d (list (d (n "convert_case") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "=0.3.3") (d #t) (k 2)) (d (n "macrotest") (r "=1.0.5") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.4") (o #t) (d #t) (k 0)))) (h "1xd9mshcjmsp412fj7gb54jgjv5h16kbshdavavi8gw3zgn5nxkm") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "convert_case") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.3.0 (c (n "duplicate") (v "0.3.0") (d (list (d (n "doc-comment") (r "=0.3.3") (d #t) (k 2)) (d (n "heck") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "macrotest") (r "^1.0.7") (d #t) (k 2)) (d (n "proc-macro-error") (r "=1.0.4") (o #t) (d #t) (k 0)))) (h "0ljzywh57bgaw1xhgya5szxlsax24n2gcfmfl7ylgk4vphxg5niw") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "heck") ("fail-on-warnings") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.4.0 (c (n "duplicate") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "heck") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 2)))) (h "053jgrag0z2spc1wsh34vfmmhhj73hsj2f8sv7h9sz23y7y3a9aa") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "heck") ("fail-on-warnings") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-0.4.1 (c (n "duplicate") (v "0.4.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "heck") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "macrotest") (r "=1.0.8") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (d #t) (k 2)))) (h "1y0g9195cm1adwy2y56554fg2jn2wzk5ilkavsc21s8hsx6bx950") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "heck") ("fail-on-warnings") ("default" "pretty_errors" "module_disambiguation"))))))

(define-public crate-duplicate-1.0.0 (c (n "duplicate") (v "1.0.0") (d (list (d (n "heck") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "duplicate_macrotest") (r "^1.0.3") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.105") (d #t) (k 2)))) (h "1jrh28a3g9xw0w9v21y92d4k08lgr1ffg8mjhw2h67h6r5mfcy6y") (f (quote (("pretty_errors" "proc-macro-error") ("module_disambiguation" "heck") ("fail-on-warnings") ("default" "pretty_errors" "module_disambiguation"))))))

