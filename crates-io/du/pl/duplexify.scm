(define-module (crates-io du pl duplexify) #:use-module (crates-io))

(define-public crate-duplexify-1.0.0 (c (n "duplexify") (v "1.0.0") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "0w91dvngcxr7plw9cvbbmvqhbxkv9dbaz9c14bnsdx0p1dg2w99k")))

(define-public crate-duplexify-1.0.1 (c (n "duplexify") (v "1.0.1") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "0d2f8f76mi2cb5s03gi42hvl6r85jzraf1vgqnvhifybp1a98s2b")))

(define-public crate-duplexify-1.0.2 (c (n "duplexify") (v "1.0.2") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1xq8yspr11i5hiybls8gzvr5549rz8r7cx8al7rlb36nhffl2r6h")))

(define-public crate-duplexify-1.1.0 (c (n "duplexify") (v "1.1.0") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1kh2rbvbw9wagzapsllqxn7zn24s25rwgv4c1xkzh03xpgn2c741")))

(define-public crate-duplexify-1.2.0 (c (n "duplexify") (v "1.2.0") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1k90bmi8iwhrnyl3dlm1h9v9d3yqm8zdzfg60nk2mh3mjkyxxd91")))

(define-public crate-duplexify-1.1.1 (c (n "duplexify") (v "1.1.1") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)))) (h "12nmh8h9kmiw8m1zlkxwhbm076872p66hypb7v8ikvzg4mn8j5qw") (y #t)))

(define-public crate-duplexify-1.2.1 (c (n "duplexify") (v "1.2.1") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)))) (h "018zyzd3wx8pg4wvlvn5ghnq5gxr9ay54y0smfd4ihni5i69wwxs")))

(define-public crate-duplexify-1.2.2 (c (n "duplexify") (v "1.2.2") (d (list (d (n "async-std") (r "^1.6.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1.7") (d #t) (k 0)))) (h "1mb5h3m1dh6bzg5ccizhfn7dngac08k9px9k5nmwwf6vsrn39k7i")))

