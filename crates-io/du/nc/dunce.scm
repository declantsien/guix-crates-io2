(define-module (crates-io du nc dunce) #:use-module (crates-io))

(define-public crate-dunce-0.1.0 (c (n "dunce") (v "0.1.0") (h "0q927w27lnixmw6x31y6hdrjh6hdp3l3c7ddm30rn1vbwid1n0vr") (y #t)))

(define-public crate-dunce-0.1.1 (c (n "dunce") (v "0.1.1") (h "0dxvxaa63j4jn904rpyhmn60rgmyl3xpz04mih2wf75gdcshfsz8") (y #t)))

(define-public crate-dunce-1.0.0 (c (n "dunce") (v "1.0.0") (h "0kys739zvwcvsngspa4lw2dksigiima17i25c09d2j45m3v6pbfh") (y #t)))

(define-public crate-dunce-1.0.1 (c (n "dunce") (v "1.0.1") (h "1d7f7wg83i1by16rxc1fdipi872nvkzjnmzaaggh2h8cgi51qr5j")))

(define-public crate-dunce-1.0.2 (c (n "dunce") (v "1.0.2") (h "0hbmlqjwj8q0vl3qsz72hlphszfb80jr9r205bypfmfgf7140d25")))

(define-public crate-dunce-1.0.3 (c (n "dunce") (v "1.0.3") (h "0g0wng3v9z0sh0b756wawm40ixhl7x6f6k8gcasdkfv0cl5b7m0b")))

(define-public crate-dunce-1.0.4 (c (n "dunce") (v "1.0.4") (h "0fqcbwfclldbknmawi69l6zyncaiqzxkpbybcb2cc7jmlxnqrkjn")))

