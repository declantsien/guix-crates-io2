(define-module (crates-io du xx duxx) #:use-module (crates-io))

(define-public crate-duxx-0.2.0 (c (n "duxx") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)))) (h "069n5b94x0a5vym0q6471ih34zr7gc41skc3jyl2s27r92xswab6")))

(define-public crate-duxx-0.2.1 (c (n "duxx") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)))) (h "0n9hy2sjqpi5mca6qg8k1y9gwcaylxqpzr1w9xbb8kafq8k0r5nx")))

(define-public crate-duxx-0.2.2 (c (n "duxx") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)))) (h "1l0pz213i8i7b3mxw4zvzr3vc6fnfzrs6nlv3n7psknwiq9nqyh7")))

