(define-module (crates-io du sa dusage) #:use-module (crates-io))

(define-public crate-dusage-0.0.1 (c (n "dusage") (v "0.0.1") (h "0xrnxahs260qz04b7lsnppidqyny5dvr15j3q2p1wmyqn0bns7z6") (y #t)))

(define-public crate-dusage-0.0.2 (c (n "dusage") (v "0.0.2") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0r3xjf7gm2x0jxl8jp54zwhm883ddsvrd2r2zr206ylbc6r0vwjy") (y #t)))

(define-public crate-dusage-0.0.3 (c (n "dusage") (v "0.0.3") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0hwxr8glh9yccsixch09kavna6yav6qj38scx9x58fnk2ii1g7zk") (y #t)))

(define-public crate-dusage-0.1.0 (c (n "dusage") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1kckyr60qwzfizd0z3yv60nlq5q4b30rmcn89b0sab9jss3fs21c") (y #t)))

(define-public crate-dusage-0.2.0 (c (n "dusage") (v "0.2.0") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "0zd97kyb21n7ck3clbmk2iq2l8ccf11x9qmfxmm2k98wm4qc9790")))

(define-public crate-dusage-0.2.1 (c (n "dusage") (v "0.2.1") (d (list (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "007lsbmaqdb226fzgdlaf16hnzfl20xkccx3as5p7iiaarccqg47")))

(define-public crate-dusage-0.2.2 (c (n "dusage") (v "0.2.2") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1j949csbdhxj5qnf8qxiqn4015gi7q5zhjiqkzpqyxp9mj0ck888")))

(define-public crate-dusage-0.2.3 (c (n "dusage") (v "0.2.3") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "12626n4v650lz4wgnnwr7hdq0m6wn641kci39dda40vmkdsfv4ff")))

(define-public crate-dusage-0.3.0 (c (n "dusage") (v "0.3.0") (d (list (d (n "autoclap") (r "^0.2.2") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1ba4slxay5rh9cfjisq8hffzka4vzwhmnp66cv9q24dyhcw7nr66")))

(define-public crate-dusage-0.3.1 (c (n "dusage") (v "0.3.1") (d (list (d (n "autoclap") (r "^0.2.2") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "1ggl300sd9p5jahabs04fxz0nlf02ln007y7irxm89c8d69dfjky")))

(define-public crate-dusage-0.3.2 (c (n "dusage") (v "0.3.2") (d (list (d (n "autoclap") (r "^0.2.2") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.13") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)))) (h "066ii7wj9ci2q8jx07p27jsp3l092ll7hhpwx0pjzsa6jrrkzy3q")))

(define-public crate-dusage-0.3.3 (c (n "dusage") (v "0.3.3") (d (list (d (n "autoclap") (r "^0.2.7") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.14") (d #t) (k 0)) (d (n "clap") (r "^4.0.8") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1956aya775la0zwcikg3cq12z887kxaxyjyx1m60a20b6p0pldng")))

(define-public crate-dusage-0.3.4 (c (n "dusage") (v "0.3.4") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1kys622q7liqyl472qra1mpq6z5d6b6yi4d763dygjxglran1qgg")))

(define-public crate-dusage-0.3.5 (c (n "dusage") (v "0.3.5") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1gyq9pskrrv74cdxzma0xzslq887cvravm9pamjx5n85m4qcx5is")))

(define-public crate-dusage-0.3.6 (c (n "dusage") (v "0.3.6") (d (list (d (n "autoclap") (r "^0.3.15") (d #t) (k 0)) (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "1c3imadr6yfzfgpk6ydhhhyqi6i31rprp6jzwvg1hnypxbyydki5")))

