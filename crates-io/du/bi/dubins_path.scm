(define-module (crates-io du bi dubins_path) #:use-module (crates-io))

(define-public crate-dubins_path-0.0.1 (c (n "dubins_path") (v "0.0.1") (h "0jiwn25dqjfwlwpcy3ccljl0phs7yaqb2x07n3llddxji0hf6dcr")))

(define-public crate-dubins_path-0.0.2 (c (n "dubins_path") (v "0.0.2") (d (list (d (n "euclid") (r "^0.20.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0daxg9lig9q0kay3xqd89f81vdyl6qd13rcf6pbi6k7ngn30aawf")))

