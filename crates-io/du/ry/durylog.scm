(define-module (crates-io du ry durylog) #:use-module (crates-io))

(define-public crate-durylog-0.1.0 (c (n "durylog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1rrz9l9ndhj5fpiqjhk0g830105p9ikxz5ni1q2p3h3lgj1gwmjn")))

(define-public crate-durylog-0.1.1 (c (n "durylog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1zrqg09pj5z41gp3jgdjq0sdlm7k7hcbfsnk9jc4bx51gal5hr5c")))

(define-public crate-durylog-0.1.2 (c (n "durylog") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1pkqpm125mp8a6mlr7mkhh72mi3dmhpv20w2jnvnjpv753f56khq")))

