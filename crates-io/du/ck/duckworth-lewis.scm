(define-module (crates-io du ck duckworth-lewis) #:use-module (crates-io))

(define-public crate-duckworth-lewis-0.1.0 (c (n "duckworth-lewis") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d2s0nrgm34n4vmkxry4qgnla6jp05rl2iwq8iyj9spw05pcp18r") (s 2) (e (quote (("ser" "dep:serde" "dep:serde_json") ("cli" "dep:clap" "ser"))))))

