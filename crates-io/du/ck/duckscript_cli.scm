(define-module (crates-io du ck duckscript_cli) #:use-module (crates-io))

(define-public crate-duckscript_cli-0.1.0 (c (n "duckscript_cli") (v "0.1.0") (d (list (d (n "duckscript") (r "^0.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1") (d #t) (k 0)))) (h "01283biy2mjin8pg0gk6mdg7fgqbly4m27pdz1g6nv9ff1fggqpx")))

(define-public crate-duckscript_cli-0.1.3 (c (n "duckscript_cli") (v "0.1.3") (d (list (d (n "duckscript") (r "^0.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1") (d #t) (k 0)))) (h "1pl9r2pi51maj9m3dxv937dyljlspbjcy79s8x6qh4fhs47hb8wn")))

(define-public crate-duckscript_cli-0.1.4 (c (n "duckscript_cli") (v "0.1.4") (d (list (d (n "duckscript") (r "^0.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1") (d #t) (k 0)))) (h "02pd5wy0wwc0maxi20g3hh768v470vws55hrzzdsb3sfis13p9dg")))

(define-public crate-duckscript_cli-0.1.5 (c (n "duckscript_cli") (v "0.1.5") (d (list (d (n "duckscript") (r "^0.1.4") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1.5") (d #t) (k 0)))) (h "0xa2f36b6fm5rfmq1sxmdgd9p7kx9g49v0z9dmkxd6250sq1s2yk")))

(define-public crate-duckscript_cli-0.1.6 (c (n "duckscript_cli") (v "0.1.6") (d (list (d (n "duckscript") (r "^0.1.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1.6") (d #t) (k 0)))) (h "0y4a69v7c9ys05sk3jdrm6w5z4c6frm0621k7hm8mma5rp30s4z4")))

(define-public crate-duckscript_cli-0.1.7 (c (n "duckscript_cli") (v "0.1.7") (d (list (d (n "duckscript") (r "^0.1.6") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1.7") (d #t) (k 0)))) (h "178q86bahdzzpck5xpl2bn3fif5i4di503cficxyvrqczq76pp15")))

(define-public crate-duckscript_cli-0.1.8 (c (n "duckscript_cli") (v "0.1.8") (d (list (d (n "duckscript") (r "^0.1.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.1.8") (d #t) (k 0)))) (h "1h7nxzrvcmmvnv5hs1fb9zzqris1cnhdg068qzlbbb4w94q6i6ba")))

(define-public crate-duckscript_cli-0.2.0 (c (n "duckscript_cli") (v "0.2.0") (d (list (d (n "duckscript") (r "^0.2.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.2.0") (d #t) (k 0)))) (h "0bsfxsg8kczffmxslxc0m2a32whq2l4s02nd95cgps30lq802zqz")))

(define-public crate-duckscript_cli-0.2.1 (c (n "duckscript_cli") (v "0.2.1") (d (list (d (n "duckscript") (r "^0.2.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.2.1") (d #t) (k 0)))) (h "1jnzz8prm3add2dv4l8bzyx2bpxan3x9gr2xkrmb7sskq9r6ram2")))

(define-public crate-duckscript_cli-0.3.0 (c (n "duckscript_cli") (v "0.3.0") (d (list (d (n "duckscript") (r "^0.3") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.3") (d #t) (k 0)))) (h "14bbqiknagznpdh7krw66awkb7in7402ajpyb4ks61hnaja20158")))

(define-public crate-duckscript_cli-0.3.1 (c (n "duckscript_cli") (v "0.3.1") (d (list (d (n "duckscript") (r "^0.3") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.3") (d #t) (k 0)))) (h "0r6lwggd2yvvd2dpkx6mws3jx4hxj5a33irkczhsj2j5yvfagqgb")))

(define-public crate-duckscript_cli-0.3.2 (c (n "duckscript_cli") (v "0.3.2") (d (list (d (n "duckscript") (r "^0.3") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.3.2") (d #t) (k 0)))) (h "0nqjmnvhax4zdqijjifd46ssiwrbr9dib7nk4zzc41qz8hs4zyf5")))

(define-public crate-duckscript_cli-0.3.3 (c (n "duckscript_cli") (v "0.3.3") (d (list (d (n "duckscript") (r "^0.3.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.3.3") (d #t) (k 0)))) (h "0rlizv2n33zbpsabnbib9h5496lm3vmybb5qymnbckihzp7ky0wj")))

(define-public crate-duckscript_cli-0.4.0 (c (n "duckscript_cli") (v "0.4.0") (d (list (d (n "duckscript") (r "^0.4.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.4.0") (d #t) (k 0)))) (h "1knzrvnkic4wdakx2yr5v0d1y7x96n7d5p3hxh5mm8g5h8j171g9")))

(define-public crate-duckscript_cli-0.4.1 (c (n "duckscript_cli") (v "0.4.1") (d (list (d (n "duckscript") (r "^0.4.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.4.1") (d #t) (k 0)))) (h "1cgdhm4qj9r5qzb6plq0zq36k7drsrvj38lkyvwh02l07a4zqnq7")))

(define-public crate-duckscript_cli-0.4.2 (c (n "duckscript_cli") (v "0.4.2") (d (list (d (n "duckscript") (r "^0.4.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.4.2") (d #t) (k 0)))) (h "01w606grc38g4ijkn7wa6fpwaa3z6wh3nyd8s3p23wmizzyncq8w")))

(define-public crate-duckscript_cli-0.5.0 (c (n "duckscript_cli") (v "0.5.0") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.5.0") (d #t) (k 0)))) (h "052p9q7390y0h3pnbz06kaa613ns5k469psgpp486nw0vr9vx9d4")))

(define-public crate-duckscript_cli-0.6.0 (c (n "duckscript_cli") (v "0.6.0") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.0") (d #t) (k 0)))) (h "0gdl04gcysn1n11kzbc4gwvavy1nig1zia2784rjy4fv8n3z2y0n")))

(define-public crate-duckscript_cli-0.6.1 (c (n "duckscript_cli") (v "0.6.1") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.1") (d #t) (k 0)))) (h "0h0w3s1jbxc0kf555snkkl3d08rkpvdr5nvzd7jcb04j89s4mlxf")))

(define-public crate-duckscript_cli-0.6.2 (c (n "duckscript_cli") (v "0.6.2") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.2") (d #t) (k 0)))) (h "0ssz4mfb7vpyq4l0xj2w8jzxbp9dnl36yjm188mkx9xz3mbf3yax")))

(define-public crate-duckscript_cli-0.6.3 (c (n "duckscript_cli") (v "0.6.3") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.3") (d #t) (k 0)))) (h "05b85blkzr7zhz84p48kjqf6mcrym2ng3wk4ksxk548j4kdzcf2a")))

(define-public crate-duckscript_cli-0.6.4 (c (n "duckscript_cli") (v "0.6.4") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.4") (d #t) (k 0)))) (h "0s6zlr2kfd844xxqwaasvmj97gm3w2573kjwaavwacazfhr2yjv5")))

(define-public crate-duckscript_cli-0.6.5 (c (n "duckscript_cli") (v "0.6.5") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.5") (d #t) (k 0)))) (h "0hlb9ql844gas6j378hmjcs051j3lham3df55nv82220vgjq1159")))

(define-public crate-duckscript_cli-0.6.6 (c (n "duckscript_cli") (v "0.6.6") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.6") (d #t) (k 0)))) (h "0k9nxd4afxybnca72axgxc3mi2ls7b3gvs65hfbd75c6na9g7qms")))

(define-public crate-duckscript_cli-0.6.7 (c (n "duckscript_cli") (v "0.6.7") (d (list (d (n "duckscript") (r "^0.5.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.7") (d #t) (k 0)))) (h "1h7c937zzpl8fkcmdymc41wafanmrm77g2ghywkhk7gnimzrg26a")))

(define-public crate-duckscript_cli-0.6.8 (c (n "duckscript_cli") (v "0.6.8") (d (list (d (n "duckscript") (r "^0.5.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.8") (d #t) (k 0)))) (h "0wvs5znln1i952k1602v9f8pckk3c063lhly1ms37mkp5xmpqqby")))

(define-public crate-duckscript_cli-0.6.9 (c (n "duckscript_cli") (v "0.6.9") (d (list (d (n "duckscript") (r "^0.5.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.6.9") (d #t) (k 0)))) (h "002s0dbrhsxhk3nxhipjasz1ir3cg9i9838dmyz24b3r3a4kq8zk")))

(define-public crate-duckscript_cli-0.7.0 (c (n "duckscript_cli") (v "0.7.0") (d (list (d (n "duckscript") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "1wdaa3kdlifzp25hqjhi4vd4jdmn0s8immx6nzm6i1f3bz3yjkki")))

(define-public crate-duckscript_cli-0.7.1 (c (n "duckscript_cli") (v "0.7.1") (d (list (d (n "duckscript") (r "^0.6.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.7.0") (d #t) (k 0)))) (h "0y4jjk4p41s110xjp0wd2dinqcxqz9sigqjdzqgkv2vwwbb8j2j6")))

(define-public crate-duckscript_cli-0.7.2 (c (n "duckscript_cli") (v "0.7.2") (d (list (d (n "duckscript") (r "^0.6.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.7.0") (d #t) (k 0)))) (h "0fyj255agis9hn03g84r7i4bnvya3ny83w3x97wfvw651d73xw4h")))

(define-public crate-duckscript_cli-0.7.3 (c (n "duckscript_cli") (v "0.7.3") (d (list (d (n "duckscript") (r "^0.6.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.7.0") (d #t) (k 0)))) (h "1py2ncxvdi9ry4dz2hyffh6pdzzr9kh78ssfsk85hagx5k840h8v")))

(define-public crate-duckscript_cli-0.7.4 (c (n "duckscript_cli") (v "0.7.4") (d (list (d (n "duckscript") (r "^0.6.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.7.0") (d #t) (k 0)))) (h "0qvaczb52kl0qs7nrp0hphvm6ikbzfjjm5khjp7xasqviin93avp")))

(define-public crate-duckscript_cli-0.8.0 (c (n "duckscript_cli") (v "0.8.0") (d (list (d (n "duckscript") (r "^0.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8") (d #t) (k 0)))) (h "0jr88dxqy99qpgi1pwz0g453zgjvhi5g7yi3i71lr260ikhhfs8l")))

(define-public crate-duckscript_cli-0.8.1 (c (n "duckscript_cli") (v "0.8.1") (d (list (d (n "duckscript") (r "^0.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.1") (d #t) (k 0)))) (h "16n0gy1s9rv7fjnvs27lr4bndyhhqcs0b35v7hc3jl2ms8jbmma5")))

(define-public crate-duckscript_cli-0.8.2 (c (n "duckscript_cli") (v "0.8.2") (d (list (d (n "duckscript") (r "^0.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.2") (d #t) (k 0)))) (h "01x1akjsqspljjyyw1bb0njpbgdzhr6g8xk0pzrig5625lva9qw0")))

(define-public crate-duckscript_cli-0.8.3 (c (n "duckscript_cli") (v "0.8.3") (d (list (d (n "duckscript") (r "^0.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.3") (d #t) (k 0)))) (h "085dk6p6ia1kcm7cv6ygwlf73snzkp5k88jydscr1cn3rpcilgm4")))

(define-public crate-duckscript_cli-0.8.4 (c (n "duckscript_cli") (v "0.8.4") (d (list (d (n "duckscript") (r "^0.7") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.4") (d #t) (k 0)))) (h "14l0x2d7jybvmp8q06ynfj6wrds5rkl5i795ii071j7l7riaxxdz")))

(define-public crate-duckscript_cli-0.8.5 (c (n "duckscript_cli") (v "0.8.5") (d (list (d (n "duckscript") (r "^0.7.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.5") (d #t) (k 0)))) (h "0ywivah8zdvi90jrjyi30fjaywfm09i0y1p6wdpxxm1iafblrb1c")))

(define-public crate-duckscript_cli-0.8.6 (c (n "duckscript_cli") (v "0.8.6") (d (list (d (n "duckscript") (r "^0.7.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.6") (d #t) (k 0)))) (h "0pjldjjprfl7gpk0w9f3mjxkhmf482mb898srab7lcwxznwqfzbk")))

(define-public crate-duckscript_cli-0.8.7 (c (n "duckscript_cli") (v "0.8.7") (d (list (d (n "duckscript") (r "^0.7.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.7") (d #t) (k 0)))) (h "07pv42vnms342whmi079y7dw7jxjigkp94xzkcd3c3amsx14j6kf")))

(define-public crate-duckscript_cli-0.8.9 (c (n "duckscript_cli") (v "0.8.9") (d (list (d (n "duckscript") (r "^0.7.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.8") (d #t) (k 0)))) (h "091f17l4mrs827i0f092i3x86fsdmwgzji8zh05nkvp7bngw3ia9")))

(define-public crate-duckscript_cli-0.8.10 (c (n "duckscript_cli") (v "0.8.10") (d (list (d (n "duckscript") (r "^0.7.1") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.10") (k 0)))) (h "196nwq5kqzq11n2brdi1lh75ga0rb89sg9qvimmrykj3dq89s39m") (f (quote (("tls" "duckscriptsdk/tls") ("default" "tls"))))))

(define-public crate-duckscript_cli-0.8.11 (c (n "duckscript_cli") (v "0.8.11") (d (list (d (n "duckscript") (r "^0.7.2") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.11") (k 0)))) (h "0n2s4jf0jyg7v77y2has988cdgzvyxpjb919rf9v4yfrirywrkcv") (f (quote (("tls" "duckscriptsdk/tls") ("default" "tls"))))))

(define-public crate-duckscript_cli-0.8.12 (c (n "duckscript_cli") (v "0.8.12") (d (list (d (n "duckscript") (r "^0.7.2") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.12") (k 0)))) (h "1ckckylri3lrhq9w25xxbsla9y6f5hhjdj0ybhs8a78jrf10d70z") (f (quote (("tls" "duckscriptsdk/tls") ("default" "tls"))))))

(define-public crate-duckscript_cli-0.8.13 (c (n "duckscript_cli") (v "0.8.13") (d (list (d (n "duckscript") (r "^0.7.3") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.13") (k 0)))) (h "08q7rzq8asaxmdhq8c433l2p5aq693ww3gw9h9f8dsdddx4876di") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.14 (c (n "duckscript_cli") (v "0.8.14") (d (list (d (n "duckscript") (r "^0.7.4") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.14") (k 0)))) (h "0zqfpq6mzkhfpqbp3n3421l4b8wa8cgp9ppb41p6x9qsq3dkh6n3") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.15 (c (n "duckscript_cli") (v "0.8.15") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.15") (k 0)))) (h "05ffb84jyqzp3zsqz41p3nlfla1q86l26gzknim38j6r8bkpm2h2") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.16 (c (n "duckscript_cli") (v "0.8.16") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.16") (k 0)))) (h "1n0zni12zqdlcglgg9k83n9zgkvxcsmvn0cx6gbyi92bmxhbhn0a") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.17 (c (n "duckscript_cli") (v "0.8.17") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.17") (k 0)))) (h "1hz19xj7h9b57ss2isb9r1niqipayiy2krlc3c2r8zzqhs9vzn2c") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.18 (c (n "duckscript_cli") (v "0.8.18") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.18") (k 0)))) (h "15y1ra118hh7c2xljhs8f90qsyag790sjfsywgbsn00304w876aw") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.19 (c (n "duckscript_cli") (v "0.8.19") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.19") (k 0)))) (h "0r5wbqksm8vqv986gzfkh4mgk9dmz4nwp7lnqzahpfjx9z64q105") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.8.20 (c (n "duckscript_cli") (v "0.8.20") (d (list (d (n "duckscript") (r "^0.7.5") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.8.20") (k 0)))) (h "0l4lv1vqa0vs1z5lmzp9ka8li4q7hawm9l86qjmz7346gqs33sns") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.9.0 (c (n "duckscript_cli") (v "0.9.0") (d (list (d (n "duckscript") (r "^0.8.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.9.0") (k 0)))) (h "1dwyyfp480xdzkli8nbprlyzvssmmrj0wm01w0m3cazagdyadba2") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.9.1 (c (n "duckscript_cli") (v "0.9.1") (d (list (d (n "duckscript") (r "^0.8.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.9.1") (k 0)))) (h "14rzckwy09jfkwggmsj0ba8l9247xxkwn2m9mx6ml6435ndgy8pi") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.9.2 (c (n "duckscript_cli") (v "0.9.2") (d (list (d (n "duckscript") (r "^0.8.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.9.1") (k 0)))) (h "034s95fc3linar5gr27l8hsg29nl3l3l3pw4m95hnlkn18w90kkl") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

(define-public crate-duckscript_cli-0.9.3 (c (n "duckscript_cli") (v "0.9.3") (d (list (d (n "duckscript") (r "^0.8.0") (d #t) (k 0)) (d (n "duckscriptsdk") (r "^0.9.3") (k 0)))) (h "1149ibix93ffcd4jlnsq2zz37qdfh5cj08ywpl7x271qgy0xgyjd") (f (quote (("tls-rustls" "duckscriptsdk/tls-rustls") ("tls-native" "duckscriptsdk/tls-native") ("tls" "tls-rustls") ("default" "tls-rustls"))))))

