(define-module (crates-io du ck duckdb-loadable-macros) #:use-module (crates-io))

(define-public crate-duckdb-loadable-macros-0.1.0 (c (n "duckdb-loadable-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "0k45lj47661zc9iw0wm9yhl6zpy317lqa2vv8pl6xzfsjg36jqna")))

(define-public crate-duckdb-loadable-macros-0.1.1 (c (n "duckdb-loadable-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits" "full" "fold" "parsing"))) (d #t) (k 0)))) (h "0lcqvy43wbdzgsdvy8ydcp3bs2nq6qc7i0k321f1jcbglf0i3frq")))

