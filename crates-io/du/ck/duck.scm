(define-module (crates-io du ck duck) #:use-module (crates-io))

(define-public crate-duck-0.1.0 (c (n "duck") (v "0.1.0") (h "000j9zg38m0v0lf8zd3bwmh20dgvlcdszjzflpdjbp201my5y9hc")))

(define-public crate-duck-0.2.0 (c (n "duck") (v "0.2.0") (h "0jlynnm66h90n4yladca6qjlm7z48d3yi1bp9wd911584xf95r67")))

