(define-module (crates-io du ck ducklogic) #:use-module (crates-io))

(define-public crate-ducklogic-0.0.0 (c (n "ducklogic") (v "0.0.0") (h "193ykmrgal8v3mpwbnqgqac6xxqv5vmws191al79w7ixj1bqnjyq") (y #t)))

(define-public crate-ducklogic-0.0.1 (c (n "ducklogic") (v "0.0.1") (h "0b1w1v5q9faqd4x6vhk2npfyjdrfilwg5cny7mdrcf74mlipgrqp")))

