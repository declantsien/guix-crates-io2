(define-module (crates-io du ck duckbubble) #:use-module (crates-io))

(define-public crate-duckbubble-0.1.0 (c (n "duckbubble") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0y5b4a5s2990b3pxj20i4rxm3n1linx8hmii29ap981iqgrk6p4q")))

(define-public crate-duckbubble-0.1.2 (c (n "duckbubble") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kl9q8y30y5909g8xabqa1xi826bxs01iq1k1w0nsl5dkn704pn4")))

