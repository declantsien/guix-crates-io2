(define-module (crates-io du ck ducktor) #:use-module (crates-io))

(define-public crate-ducktor-0.1.0 (c (n "ducktor") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "1bl8ryhzlzcisz2fpg7xplipn6gx6ycfwxka7vsnbnlb6bvan8f4")))

