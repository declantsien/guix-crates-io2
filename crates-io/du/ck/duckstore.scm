(define-module (crates-io du ck duckstore) #:use-module (crates-io))

(define-public crate-duckstore-0.1.0 (c (n "duckstore") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "10r5wnpg3hpx5g2v4lpiv732zrdb8wxjnln5m22zgpny4j7pdac6") (r "1.58")))

(define-public crate-duckstore-0.1.1 (c (n "duckstore") (v "0.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "1k7h30g1a7dplwwmi0m2ns9k31wm4ibbxb911v7k5giww58np64r") (r "1.58")))

(define-public crate-duckstore-1.0.0 (c (n "duckstore") (v "1.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "0cp2fwwqcvpij8kcq71h4i2lqzwpvwxw3c8lzbnn63m67qvhn14c") (r "1.58")))

(define-public crate-duckstore-2.0.0 (c (n "duckstore") (v "2.0.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "0s9ycnpfihlx7cn0aksvjl0m77m2nggc6k6q10maw90bynmhgl2h") (r "1.58")))

(define-public crate-duckstore-2.1.0 (c (n "duckstore") (v "2.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)))) (h "106nplz8a8737sj9fz1gwm1zhqgpryqc2xcpk46dfpfhn1b8x1vz") (r "1.58")))

