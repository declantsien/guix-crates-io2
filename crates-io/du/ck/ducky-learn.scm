(define-module (crates-io du ck ducky-learn) #:use-module (crates-io))

(define-public crate-ducky-learn-0.1.0 (c (n "ducky-learn") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0frbk531bn6dkmvy0m4pdm26s0x4yxng7lr3nwp521i88rh0x9pv")))

(define-public crate-ducky-learn-0.1.1 (c (n "ducky-learn") (v "0.1.1") (d (list (d (n "mnist") (r "^0.5.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "0kbki1g3ralj6qy32f0lcshjgmndqfv6c5wd4ik3cm7qrjyivvwf")))

(define-public crate-ducky-learn-0.2.0 (c (n "ducky-learn") (v "0.2.0") (d (list (d (n "mnist") (r "^0.5.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "19p0vhkx6dm8iv8fx6yj22ilkxsz9pjzzcxf7ga7hwkdrs1d9zlg")))

(define-public crate-ducky-learn-0.2.1 (c (n "ducky-learn") (v "0.2.1") (d (list (d (n "mnist") (r "^0.5.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)))) (h "1d8yg14sb7hgi89prcizwzgl5d3f9gdp5q0hgkpk1nj2b6v2z3ng")))

