(define-module (crates-io du ck duck_you_publish_test) #:use-module (crates-io))

(define-public crate-duck_you_publish_test-0.1.0 (c (n "duck_you_publish_test") (v "0.1.0") (h "1dv14xy3syclxh3rfq7f7ssild7xnqn4a6xsf6f28yfdyy7ga23x") (y #t)))

(define-public crate-duck_you_publish_test-0.1.5 (c (n "duck_you_publish_test") (v "0.1.5") (h "07rz56shgprqql28mv8mx52ynk33lr0ia0jfi84qz5xxnlwxk7i5")))

