(define-module (crates-io du ck duckscript) #:use-module (crates-io))

(define-public crate-duckscript-0.1.0 (c (n "duckscript") (v "0.1.0") (h "1amnd1p5wwbpxjdkcb953wq13kkmbvg2qjnlr891r924wasi74hi")))

(define-public crate-duckscript-0.1.1 (c (n "duckscript") (v "0.1.1") (h "1s6aqi1iw7n7ap6lpdcb82fl4vi0ydikn0pxjnpjxnd3bg9swjzk")))

(define-public crate-duckscript-0.1.3 (c (n "duckscript") (v "0.1.3") (h "19i6vp291dv2b63b19qrna46jc7kzk8m296p71n6w71dxbamvcv4")))

(define-public crate-duckscript-0.1.4 (c (n "duckscript") (v "0.1.4") (h "1h5hi1vrflz299wjksvcmcmz8bhy2498jfzjf48g90m14pas181a")))

(define-public crate-duckscript-0.1.5 (c (n "duckscript") (v "0.1.5") (h "1pl7clfk8y4agr7jc2db96vhx0whpa6gybykn5q9iwg9glc2djap")))

(define-public crate-duckscript-0.1.6 (c (n "duckscript") (v "0.1.6") (h "0ib19q83q9pxqfj1r37b2mjc6zrcadfv463n64wmbhbcjf14sfdm")))

(define-public crate-duckscript-0.1.7 (c (n "duckscript") (v "0.1.7") (h "081md1yxj6v84lih9flwlmdr820hif7wnrxg3vp3h93mdzdxxqlc")))

(define-public crate-duckscript-0.2.0 (c (n "duckscript") (v "0.2.0") (h "1d9l2s1syiszlcsvyvslhfb2bp6k8l801z5zh52hp0x087jv8yvl")))

(define-public crate-duckscript-0.2.1 (c (n "duckscript") (v "0.2.1") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "1ivj779waw1iz529vmd85c90hzcdnjcd1vnqdqnlq9pwbw8rkxn0")))

(define-public crate-duckscript-0.3.0 (c (n "duckscript") (v "0.3.0") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "1afz8vpgm7ik84ss2gaq06qm37i77cfai6lvjalsvq6w2a4k6jma")))

(define-public crate-duckscript-0.3.1 (c (n "duckscript") (v "0.3.1") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "11hkf2n24ykap7al6j8y72nlifgc95z0nldhf44vzfml2vsa2sv0")))

(define-public crate-duckscript-0.4.0 (c (n "duckscript") (v "0.4.0") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "0m8vfm4lknlgs5hnaqk8aj1lj3mkpps2y74j48lr6jgqpar6ddk2")))

(define-public crate-duckscript-0.5.0 (c (n "duckscript") (v "0.5.0") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "19kb3svsmjw7nz10mfyqz2cmlwcjj0lhnk6idc46yy3hajl3sk2z")))

(define-public crate-duckscript-0.5.1 (c (n "duckscript") (v "0.5.1") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)))) (h "1bj9hjq5cqcba7xlbk4dqhmfksvpy4p9c05afz4dyy060za07vd1")))

(define-public crate-duckscript-0.6.0 (c (n "duckscript") (v "0.6.0") (d (list (d (n "fsio") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0a4qn8xahiyyv6dnm7hk6yiy7kid9jlj15w0h2g12bq60hgvmghg")))

(define-public crate-duckscript-0.7.0 (c (n "duckscript") (v "0.7.0") (d (list (d (n "fsio") (r "^0.2") (d #t) (k 0)))) (h "0jg0nhv1hhlhx8sjafcr8n2fff15yblmf7wazyhn1kv65xf9k6f9")))

(define-public crate-duckscript-0.7.1 (c (n "duckscript") (v "0.7.1") (d (list (d (n "fsio") (r "^0.3") (d #t) (k 0)))) (h "0sa7hpni4al0frrjd5fisb7nfp247rv2hzjpwxbskraqgaqi04lh")))

(define-public crate-duckscript-0.7.2 (c (n "duckscript") (v "0.7.2") (d (list (d (n "fsio") (r "^0.3") (d #t) (k 0)))) (h "0cb2mfp72g8y4z3i28d8wfhhjhz6yfw4da96i2clh358m9yj5wdc")))

(define-public crate-duckscript-0.7.3 (c (n "duckscript") (v "0.7.3") (d (list (d (n "fsio") (r "^0.3") (d #t) (k 0)))) (h "0x9qnzqxnxa3vzlpsnipf5rxkaslx961jawrzfx094pcq0pzb6c7")))

(define-public crate-duckscript-0.7.4 (c (n "duckscript") (v "0.7.4") (d (list (d (n "fsio") (r "^0.4") (d #t) (k 0)))) (h "0di5nxpcrw7mfwnkpmbznyg5rc1wngb0i000xic6iakknn7nb8r5")))

(define-public crate-duckscript-0.7.5 (c (n "duckscript") (v "0.7.5") (d (list (d (n "fsio") (r "^0.4") (d #t) (k 0)))) (h "02jlwi9fsdf0h52718zjwmkn9zbccbv9pvp5sff7rbdqy1kkncby")))

(define-public crate-duckscript-0.8.0 (c (n "duckscript") (v "0.8.0") (d (list (d (n "fsio") (r "^0.4") (d #t) (k 0)))) (h "1zlp3ykh83rg8b5fs0gvkmi4ksrg6jvgs7wdshglh5si70wqs9gx")))

