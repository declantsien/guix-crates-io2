(define-module (crates-io du ck duckdb-extension-framework) #:use-module (crates-io))

(define-public crate-duckdb-extension-framework-0.1.0 (c (n "duckdb-extension-framework") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0hlwgsad69ya4ydhqhf67qipspsfd8fc8s950y4ll5bfx55j6b4f") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.1.1 (c (n "duckdb-extension-framework") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "00qpj3byxjd1isbajhgzxaxr98bn26qz1awil4w4kmv3zv3hg9z2") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.1.2 (c (n "duckdb-extension-framework") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1lx9iwh3yn65fah0ry3qag29vaqp090ycis4jqmjk5kbmvy051xl") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.1.3 (c (n "duckdb-extension-framework") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0fd6ad4jihl28356nd8xv78444sm5cf6lpcnj1y9sx5i4vb4v38j") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.2.0 (c (n "duckdb-extension-framework") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0j35jr2p307v6gbqb2jdibm77qqf9pbpf1b7r0xq7pa0v14gdg1x") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.3.0 (c (n "duckdb-extension-framework") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0cnhvd0y2sb6h5sj7slsslzwm5xnn5dpi58zjng3w92hzfjvqfcd") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.4.0 (c (n "duckdb-extension-framework") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1i73icgs6gigpb80lyzx3lywnpibz21h1w3qw7xiasda6y4dm0wg") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.4.1 (c (n "duckdb-extension-framework") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1qynm0y4h68q4fpcc3jfvk3mjy6qw89j7wngzw3f69zafsf5vxf5") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.5.0 (c (n "duckdb-extension-framework") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.76") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "03cykd8fl6l8a09awppihlqgzbqyz4i3lm8b980s9q8l402x4hwk") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.6.0 (c (n "duckdb-extension-framework") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.76") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1wjw12d2wq80dixksqsxf4vgnh440p6lvdfbsi2rwshb57wyqvyn") (f (quote (("statically_linked"))))))

(define-public crate-duckdb-extension-framework-0.7.0 (c (n "duckdb-extension-framework") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build_script") (r "^0.2.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.76") (d #t) (k 1)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "04v2b3yhrd40f5xvad73ydr5p0bzmkplkp969khpix8s2mmnvkyv") (f (quote (("statically_linked"))))))

