(define-module (crates-io du ck duckdb-bitstring) #:use-module (crates-io))

(define-public crate-duckdb-bitstring-0.1.0 (c (n "duckdb-bitstring") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "duckdb") (r "^0.10.0") (d #t) (k 0)))) (h "16vmxxmvyjk7gq6aqylwkj36aq18glag92gyf4lfhrlzz5i3l2c2") (y #t)))

(define-public crate-duckdb-bitstring-0.1.1 (c (n "duckdb-bitstring") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "duckdb") (r "^0.10.0") (d #t) (k 0)))) (h "1qdsyh9jwyxfr2jnpihhssfkg85vb5bi4l337l4ig6jnqf5022qb")))

(define-public crate-duckdb-bitstring-0.1.2 (c (n "duckdb-bitstring") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "duckdb") (r "^0.10.1") (d #t) (k 0)))) (h "19ypyhgsvrwc99ympgbnmvxljg298cgq17w8qqqr57qb39mg253z")))

(define-public crate-duckdb-bitstring-0.2.0 (c (n "duckdb-bitstring") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "duckdb") (r "^0.10") (d #t) (k 0)))) (h "1dbw7c1f3370wc2v635li00q9s17wnb5wffch86ljzxalhiiwsmd")))

