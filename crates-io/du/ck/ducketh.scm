(define-module (crates-io du ck ducketh) #:use-module (crates-io))

(define-public crate-ducketh-0.1.0 (c (n "ducketh") (v "0.1.0") (d (list (d (n "alloy-json-abi") (r "^0.3.2") (d #t) (k 0)) (d (n "alloy-primitives") (r "^0.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0hlvn6hfmd3p5n734h6sg7abphw2l6wds1k13rrz7m412k3ilmhp")))

