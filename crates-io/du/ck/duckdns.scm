(define-module (crates-io du ck duckdns) #:use-module (crates-io))

(define-public crate-duckdns-0.1.0 (c (n "duckdns") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.11") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1kvdqbmp7bqsx65brbnylpa3hpbfssw2lrml77pghmb30l7vizb8")))

(define-public crate-duckdns-0.1.1 (c (n "duckdns") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.11") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "003wh7iirzc480qn3d5vyrcwss79b0bqi90ss7vmcaazf09lrhra")))

