(define-module (crates-io du ck duckduckgeo) #:use-module (crates-io))

(define-public crate-duckduckgeo-0.1.0 (c (n "duckduckgeo") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1psvhdpvlpk4xlkydmvibpy869lf3zw69xxmq8fzbhcnc43sa0f8")))

(define-public crate-duckduckgeo-0.2.0 (c (n "duckduckgeo") (v "0.2.0") (d (list (d (n "axgeom") (r "^1.0.5") (d #t) (k 0)) (d (n "compt") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02b9xcvbzybfndjfncvxl2220f0ddhi9c36xnavkd9ymgq5jsg55")))

(define-public crate-duckduckgeo-0.2.1 (c (n "duckduckgeo") (v "0.2.1") (d (list (d (n "axgeom") (r "^1.0.5") (d #t) (k 0)) (d (n "compt") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gb7bfzw4w3brmd879iq5a5jdmwpp0dl36nx1f2hv8fdihdfxwmq")))

(define-public crate-duckduckgeo-0.2.2 (c (n "duckduckgeo") (v "0.2.2") (d (list (d (n "axgeom") (r "^1.2.1") (d #t) (k 0)) (d (n "dists") (r "^0.1.6") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "0ad20qcp4w25836cqs42ycr6bkr0kpmxgmrf3mp8wfinf9i2kfdz")))

(define-public crate-duckduckgeo-0.2.3 (c (n "duckduckgeo") (v "0.2.3") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "dists") (r "^0.2") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1jy66x29x1q7a37nz9q3lygbr57mdxy6lhp7fmckv011c09h0zqs")))

(define-public crate-duckduckgeo-0.2.4 (c (n "duckduckgeo") (v "0.2.4") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "0as77kxlvb20cxyvqhhwzmagv0f44ic33ng9j41jqhj881bf0vrg")))

(define-public crate-duckduckgeo-0.2.5 (c (n "duckduckgeo") (v "0.2.5") (d (list (d (n "axgeom") (r "^1.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1lv56q660w87v2jay59zyiwawi3iqi3bljj4aldjzqq5wbpl3zv1")))

(define-public crate-duckduckgeo-0.2.6 (c (n "duckduckgeo") (v "0.2.6") (d (list (d (n "axgeom") (r "^1.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1jfnxbf7d3kbg9vnf9krlvfx2r114bmdmgp9p6q97jhirmvh339p")))

(define-public crate-duckduckgeo-0.3.0 (c (n "duckduckgeo") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)))) (h "1s33yw6ad5ywbkyrs27x1m95sk2mqsxb5lz45h2kq7pa80lzjwcp")))

(define-public crate-duckduckgeo-0.3.1 (c (n "duckduckgeo") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0r1hpi3mg4c94z67di80bfy429jjv6jq1pif2iyji1ikqfqynns8")))

(define-public crate-duckduckgeo-0.4.0 (c (n "duckduckgeo") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0hd38lj0pg3d22cdxzivs58n2l57wjkwan1765ff0syzx006xlj9")))

(define-public crate-duckduckgeo-0.4.1 (c (n "duckduckgeo") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1bkd4vq95wl0f8ksx0gsf9428h7mm7vvg56na5i24vrb4y19nr25")))

(define-public crate-duckduckgeo-0.4.2 (c (n "duckduckgeo") (v "0.4.2") (d (list (d (n "axgeom") (r "^1.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "dists") (r "^0.4") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09lhkpihbsp4c7hy3b7wlbslafcii9fbmc9f694bh7gdnc2awnrg")))

