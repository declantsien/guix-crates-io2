(define-module (crates-io du an duang) #:use-module (crates-io))

(define-public crate-duang-0.1.0 (c (n "duang") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1z5ynwy7xzhrc6wpkrxywz0yv8jf6dk4r3h90ibaf1ay9x4ipkdp")))

(define-public crate-duang-0.1.1 (c (n "duang") (v "0.1.1") (d (list (d (n "demo-duang") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1jkb179a25vfw7y0lywc2q6lcjhiq3mmmrvbypa6bb2a1irj5zhr")))

(define-public crate-duang-0.1.2 (c (n "duang") (v "0.1.2") (d (list (d (n "demo-duang") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0aak6i2iq4pacl3m9v1n8s069p051ivjqk1z8w42g7m63xailb1b")))

