(define-module (crates-io du pp dupper) #:use-module (crates-io))

(define-public crate-dupper-0.1.0 (c (n "dupper") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("cargo" "color" "derive" "error-context" "suggestions" "usage"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11ka31j5d5x8qrp9cscj4k0xmigz4xhi9nvprmpkzc67kq2c1f88")))

(define-public crate-dupper-0.1.1 (c (n "dupper") (v "0.1.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xwsh2dm5al17xqc7h3x24wj6p649smagc6dig1hdjryy3ya0kjz")))

(define-public crate-dupper-0.1.2 (c (n "dupper") (v "0.1.2") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v09k2iikh70y83l30mh26bfdhj1sgf7z2yyzv0v0ry1f6dvdkyb")))

(define-public crate-dupper-0.2.0 (c (n "dupper") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x4k3rlc1xs0mk3r3lc8nizvi8njrjb7kw6rp7whlw1smk1rn64p")))

(define-public crate-dupper-0.2.1 (c (n "dupper") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01rixw4br87k8fw83yj79sb5392r66ik4bvcvbmyiqxcjj2v87yk")))

(define-public crate-dupper-0.2.2 (c (n "dupper") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03fs82gfy2mwl8p5zryhysmmkj4nmrw311gl7qix6wb7s189ihyz")))

