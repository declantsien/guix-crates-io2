(define-module (crates-io du mp dumpbin) #:use-module (crates-io))

(define-public crate-dumpbin-1.0.0 (c (n "dumpbin") (v "1.0.0") (d (list (d (n "forensic-rs") (r "^0") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1nd2n16zvirps3f1g7x2bym9v4sh8fa6ajflqws5hxcw4r9mbri0")))

(define-public crate-dumpbin-1.1.0 (c (n "dumpbin") (v "1.1.0") (d (list (d (n "forensic-rs") (r "^0.9") (d #t) (k 0)) (d (n "frnsc-liveregistry-rs") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0ylrz03kg3wb8kpgjsr201hrzk5c0921l3g07mn9vks5km6zz2m2")))

