(define-module (crates-io du mp dump) #:use-module (crates-io))

(define-public crate-dump-0.1.0 (c (n "dump") (v "0.1.0") (h "1i5fp4adn488zcvshxmqm3pyyqjypkrrylff56fwvln73lb8imc7")))

(define-public crate-dump-0.1.1 (c (n "dump") (v "0.1.1") (h "02jhs8xv69lcblmp7pqrqw22lbiqp50b7mjvj85shmsn7jl37prc")))

