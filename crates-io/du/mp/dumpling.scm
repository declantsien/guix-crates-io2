(define-module (crates-io du mp dumpling) #:use-module (crates-io))

(define-public crate-dumpling-0.0.0 (c (n "dumpling") (v "0.0.0") (h "1wr0y5zhrk1rqgn4v7q2mqzszbmkp34z88axqdz3n9f5gxdb25hh")))

(define-public crate-dumpling-0.0.1 (c (n "dumpling") (v "0.0.1") (h "03d8a6vvx8l64yvxqbp0zdcidks5vvwpr7158qn44bxcl2rmy605")))

