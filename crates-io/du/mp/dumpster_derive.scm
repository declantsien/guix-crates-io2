(define-module (crates-io du mp dumpster_derive) #:use-module (crates-io))

(define-public crate-dumpster_derive-0.1.0 (c (n "dumpster_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1pvk0c0ind91fv8wqwc59s4j7wxp9ndxrwyqkfpisj5c7gs36zdh")))

(define-public crate-dumpster_derive-0.1.2 (c (n "dumpster_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05mx1aaa6lxwfm7sspaavkmx7bw2s19albqh7h1r263vsbl5hmy7")))

