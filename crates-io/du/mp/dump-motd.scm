(define-module (crates-io du mp dump-motd) #:use-module (crates-io))

(define-public crate-dump-motd-0.1.0 (c (n "dump-motd") (v "0.1.0") (d (list (d (n "motd") (r "^0.1.0") (d #t) (k 0)))) (h "09ybdldqy2s7psjdakrxfpk8k0qsn5nf1xmjgbb8ynn3hlbzmqbd")))

(define-public crate-dump-motd-0.2.0 (c (n "dump-motd") (v "0.2.0") (d (list (d (n "motd") (r "^0.2.0") (d #t) (k 0)))) (h "0vz272p5lbj6jfsma898vm521m0nadrk9wgwa6nxylsglbwrzdmr")))

