(define-module (crates-io du mp dumplingh) #:use-module (crates-io))

(define-public crate-dumplingh-0.1.0 (c (n "dumplingh") (v "0.1.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ilg172wwnp39cxf5sknn7pv27kvb0h91i0sg05c20zpp77ly4qv")))

(define-public crate-dumplingh-0.2.0 (c (n "dumplingh") (v "0.2.0") (d (list (d (n "clap") (r "^2.27") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0d84jx0fi1g07c5kb04l6hjy9a61znarcsrksn68jyvvrc9ip9vm")))

