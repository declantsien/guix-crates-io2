(define-module (crates-io du ni duniter-wotb) #:use-module (crates-io))

(define-public crate-duniter-wotb-0.4.0 (c (n "duniter-wotb") (v "0.4.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)))) (h "1ikijxiysg86fl8ccxwfgs4gcz9jza0pzbnzcm7pn5f09vaicl30")))

(define-public crate-duniter-wotb-0.4.1 (c (n "duniter-wotb") (v "0.4.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "1dn882ns39mq67rg7yq1jhc6jfb56qxb8qlgspv4fp2v4cabf2x6")))

(define-public crate-duniter-wotb-0.6.1 (c (n "duniter-wotb") (v "0.6.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "0gqlbm0f1s3fqkxsvrhfjm52f6wsqvihpabp1jcrk8qd1lvbqj3z")))

(define-public crate-duniter-wotb-0.7.0 (c (n "duniter-wotb") (v "0.7.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "0isy305rqdf0cy62qxr57ksxxdyq53fb6wrr6ws60dyhsmpz622y") (f (quote (("strict"))))))

(define-public crate-duniter-wotb-0.7.1 (c (n "duniter-wotb") (v "0.7.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "1hadkznjg8g65a87kmns3ahhyddxjqpzxhs5abj1jbjmcsyqajw3") (f (quote (("strict"))))))

(define-public crate-duniter-wotb-0.8.0-a0.2 (c (n "duniter-wotb") (v "0.8.0-a0.2") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "15vkhcqln8r3rwm5qm8h3w5g4mqndmx5wzajqc7q222wsrk40d6z") (f (quote (("strict"))))))

(define-public crate-duniter-wotb-0.8.0-a0.5 (c (n "duniter-wotb") (v "0.8.0-a0.5") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.24") (d #t) (k 0)))) (h "1bg6x68nyjmb3vh0hfq4xzwlbbygdwkb29czvb15wh2mcy5r8fl7") (f (quote (("strict"))))))

(define-public crate-duniter-wotb-0.8.0-a0.7-deprecated (c (n "duniter-wotb") (v "0.8.0-a0.7-deprecated") (d (list (d (n "bincode") (r "1.0.*") (d #t) (k 0)) (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "rayon") (r "1.0.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)))) (h "1ixlby5aqclmqybaqy17ir2pl7j9hlw1chz9kq1317p3vz52s4i9") (f (quote (("strict"))))))

