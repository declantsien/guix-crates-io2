(define-module (crates-io du ni duniter-rs-wotb) #:use-module (crates-io))

(define-public crate-duniter-rs-wotb-0.2.0 (c (n "duniter-rs-wotb") (v "0.2.0") (h "0qwcj4rsp6pgrn7xvfmb014dv10vp0i8041dgcq0j99hb7qxg81g")))

(define-public crate-duniter-rs-wotb-0.2.1 (c (n "duniter-rs-wotb") (v "0.2.1") (h "0zjixny0rlpvlvcy5ffv0x3yw3gvf6l01bq86sbkac4mwdjbgl0x")))

(define-public crate-duniter-rs-wotb-0.3.0 (c (n "duniter-rs-wotb") (v "0.3.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)))) (h "01bikhm0d188ij1xiscqzva77fxhfxxf8f236q1529d521b86859")))

(define-public crate-duniter-rs-wotb-0.3.1 (c (n "duniter-rs-wotb") (v "0.3.1") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)))) (h "1i0drg8zqkzb0vzajhwrqgzxy7xr74p3rf9wclzng90fkm9gz8pw")))

(define-public crate-duniter-rs-wotb-0.3.2 (c (n "duniter-rs-wotb") (v "0.3.2") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)))) (h "04cp3fbpns591mnkalhplvikxaqg61ylc0c0a03hq93nzfydhsdr")))

(define-public crate-duniter-rs-wotb-0.4.0 (c (n "duniter-rs-wotb") (v "0.4.0") (d (list (d (n "bincode") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)))) (h "08xg294nqc6mz79r81knzbpq9adqgwc5z4r1h4dkv7l0fa64l3d4")))

