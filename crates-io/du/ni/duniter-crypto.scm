(define-module (crates-io du ni duniter-crypto) #:use-module (crates-io))

(define-public crate-duniter-crypto-0.1.0 (c (n "duniter-crypto") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "150ilsw6ivw32sqykbqdhz9hj59r30hbcz4hyhh3367r012whkzj")))

(define-public crate-duniter-crypto-0.1.1 (c (n "duniter-crypto") (v "0.1.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0jhpflqcrl5458ylpim5vih1ab8szd8jf08b8pap8dpyhsm1ifhr") (f (quote (("strict"))))))

(define-public crate-duniter-crypto-0.1.2 (c (n "duniter-crypto") (v "0.1.2") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0hn5dpda6bh44ga9ggrd90pxq74mkc6zm4hh7gzdlsbg5iv7pw87") (f (quote (("strict"))))))

(define-public crate-duniter-crypto-0.2.0-a0.1-deprecated (c (n "duniter-crypto") (v "0.2.0-a0.1-deprecated") (d (list (d (n "base58") (r "0.1.*") (d #t) (k 0)) (d (n "base64") (r "0.9.*") (d #t) (k 0)) (d (n "rust-crypto") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)))) (h "1bvmf9nc5hmgxn2cbbpmw4kkx18bj5ddva4cbqbmp2hasygm3lxz") (f (quote (("strict"))))))

