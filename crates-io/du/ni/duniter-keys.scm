(define-module (crates-io du ni duniter-keys) #:use-module (crates-io))

(define-public crate-duniter-keys-0.1.0 (c (n "duniter-keys") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "06b5z30asslj2q997vl1zjiy698bd72n5yq1fvg8bmqysqcphjwd")))

(define-public crate-duniter-keys-0.2.0 (c (n "duniter-keys") (v "0.2.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0bsh26fvndwmk5ippcq7s6fqfqzsq1s2yg304lki0fq16hnh1b1h")))

(define-public crate-duniter-keys-0.3.0 (c (n "duniter-keys") (v "0.3.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "148cd48vwjbzxrakl223fkd4741wy736ymc4hxmn5pqwxz7wwhmr")))

(define-public crate-duniter-keys-0.3.1-deprecated (c (n "duniter-keys") (v "0.3.1-deprecated") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0vdhi3i14970lkgbcgfv54dac2sfx2czxcr78aabxcvsblizngc3")))

