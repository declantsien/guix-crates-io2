(define-module (crates-io du bp dubp-wot) #:use-module (crates-io))

(define-public crate-dubp-wot-0.9.0 (c (n "dubp-wot") (v "0.9.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.14") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hii66x8y13589pakkky49fh21c3q8zpmi0mn6l6fg39l8fqg136")))

(define-public crate-dubp-wot-0.10.0 (c (n "dubp-wot") (v "0.10.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yrviyywrnszy4wrb9s36ymwwi17w1x76vc74w16gvcqnhjkw6jh")))

(define-public crate-dubp-wot-0.10.1 (c (n "dubp-wot") (v "0.10.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mxxvhpbm20pdxbn14bhl6sdg1w09hvknxphzs5wkasbxrbhbx1x")))

(define-public crate-dubp-wot-0.11.0 (c (n "dubp-wot") (v "0.11.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "06i033381kw1vjn7bkafp0hwn8fvrh108irwc7jlplrlnza6vxyh")))

(define-public crate-dubp-wot-0.11.1 (c (n "dubp-wot") (v "0.11.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q1d61v6c2vbsi460d52jx586a3kdzj7zzf9jibscbdz3bf2pxjr")))

