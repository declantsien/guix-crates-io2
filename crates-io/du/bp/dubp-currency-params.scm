(define-module (crates-io du bp dubp-currency-params) #:use-module (crates-io))

(define-public crate-dubp-currency-params-0.2.0 (c (n "dubp-currency-params") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mpinsd8wgk05ysj6sjfny8blgp7vn8a7f0b6q4nxp5g6jbwl4ya")))

(define-public crate-dubp-currency-params-0.3.0 (c (n "dubp-currency-params") (v "0.3.0") (d (list (d (n "dubp-common") (r "^0.2.0") (f (quote ("rand" "scrypt"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1a3q163i6wnnsg0hag42is2d9c4x2n7b8xcn3rlmp47dk01wm0gk")))

