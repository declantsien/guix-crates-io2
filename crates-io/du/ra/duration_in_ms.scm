(define-module (crates-io du ra duration_in_ms) #:use-module (crates-io))

(define-public crate-duration_in_ms-0.1.1 (c (n "duration_in_ms") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v1az9lh8xlxnyf5q9z4n9mp0sf1znqdgdbk7w34sr6zi4997yf7")))

(define-public crate-duration_in_ms-0.1.2 (c (n "duration_in_ms") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jlzhizwc3gd0syf7ys2ygg6277m5bz1ljlddsj392by7zyvf27k")))

(define-public crate-duration_in_ms-0.1.3 (c (n "duration_in_ms") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "lazy-regex") (r "^2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17dzchvqxnm3vfa59g4lg46lk485ghg2f6nfgbabijw4cy2hg58d")))

