(define-module (crates-io du ra duration_in_ms_macros) #:use-module (crates-io))

(define-public crate-duration_in_ms_macros-0.1.1 (c (n "duration_in_ms_macros") (v "0.1.1") (d (list (d (n "duration_in_ms") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09x166gky2lnvf6xrrwxx3pnpdl09cnc1gz1cd7jbw3iyddfvy0y")))

(define-public crate-duration_in_ms_macros-0.1.2 (c (n "duration_in_ms_macros") (v "0.1.2") (d (list (d (n "duration_in_ms") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qv7m866x52xbkdgim5d1106sm51sfxwvgmxf3lks8h88y18b68a")))

(define-public crate-duration_in_ms_macros-0.1.3 (c (n "duration_in_ms_macros") (v "0.1.3") (d (list (d (n "duration_in_ms") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16r62f457kv5y3h57kcmnnlqr06scgzn294animxv8gnphnrkpqc")))

