(define-module (crates-io du ra duration-string) #:use-module (crates-io))

(define-public crate-duration-string-0.0.1 (c (n "duration-string") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.105") (o #t) (d #t) (k 0)))) (h "0f91samkqscfr71bj66s042m0dgz1jfvls3mx1irli0x2cvkb9zf") (f (quote (("serde_support" "serde"))))))

(define-public crate-duration-string-0.0.2 (c (n "duration-string") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.105") (o #t) (d #t) (k 0)))) (h "12rasccm9c8mapi87p43plaj681iyfspnpx40mi3w00vlnf212fr") (f (quote (("serde_support" "serde"))))))

(define-public crate-duration-string-0.0.3 (c (n "duration-string") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.105") (o #t) (d #t) (k 0)))) (h "0pz1sj0kr7c5r4fizns54s3jkb0ihw61zr0dwaq58243mk0ndmi2") (f (quote (("serde_support" "serde"))))))

(define-public crate-duration-string-0.0.4 (c (n "duration-string") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.105") (o #t) (d #t) (k 0)))) (h "0ng5vdmbwiazfcdg2bjq5liaglf0hpw2lmr1phff58pjinp69iw2") (f (quote (("serde_support" "serde"))))))

(define-public crate-duration-string-0.0.5 (c (n "duration-string") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "0pr768prgm4s9g9xszylgkcyy5105244srrz420881gd4cw54v35")))

(define-public crate-duration-string-0.0.6 (c (n "duration-string") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "1ivablw6jrjff8pkj9qd1vjn1gxri1aq8ashnf3p4f01xgydhnxn")))

(define-public crate-duration-string-0.1.0 (c (n "duration-string") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "08msmymg3hlb4b59ybw40n52ja4bdpkyqx2g0x3zic7p3dfwp89v")))

(define-public crate-duration-string-0.1.1 (c (n "duration-string") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "0cpr3criad40q26ss1010m74qrz9k8cirbf084i3zzql5j74am7d")))

(define-public crate-duration-string-0.2.0 (c (n "duration-string") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "0izsk0wisn3qwmvv9rz02l42nmjgps2j9dqyvra71wrmzl841my5")))

(define-public crate-duration-string-0.3.0 (c (n "duration-string") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "04zflvxja25j5yj4az873jbzxczjwlgf3s7abb85x8clwad1vk3g")))

(define-public crate-duration-string-0.4.0 (c (n "duration-string") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.49") (d #t) (k 2)))) (h "1vm12islni5w61pfz0jqafdiqkg96z775ah2hvhi7hnphj36ad13")))

