(define-module (crates-io du ra duration-parser) #:use-module (crates-io))

(define-public crate-duration-parser-0.1.0 (c (n "duration-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ppq23mps1h31dhmz4yifvfr295mxzcsr9c9l1glajap1f6llmsf")))

(define-public crate-duration-parser-1.1.0 (c (n "duration-parser") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0gyz23lilz8pfk15jvvk3h95sh806kav4x7fhwxg6bb8baask7vv") (y #t)))

(define-public crate-duration-parser-0.2.0 (c (n "duration-parser") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1fkpi8kxfjiar2vkana5qan6psapp244k1936b6yw1c9hvf68rm7")))

