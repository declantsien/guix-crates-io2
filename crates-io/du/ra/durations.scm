(define-module (crates-io du ra durations) #:use-module (crates-io))

(define-public crate-durations-0.1.0 (c (n "durations") (v "0.1.0") (h "0jgbk009vbfx1mv1hqlpky2xpjnhjp82p8yj6rhrh6kmwdyh5jr2")))

(define-public crate-durations-0.1.1 (c (n "durations") (v "0.1.1") (h "11a98xvkwcvxghjq7lilkd5826kzq22vmzkpn7163y49bzgl46yj")))

