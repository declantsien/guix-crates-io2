(define-module (crates-io du ra durationfmt) #:use-module (crates-io))

(define-public crate-durationfmt-0.1.0 (c (n "durationfmt") (v "0.1.0") (h "141a3kiizi77cdnzaa06hnprq71y24gb2mq0l5lsz6w0z86pf4vi")))

(define-public crate-durationfmt-0.1.1 (c (n "durationfmt") (v "0.1.1") (h "0bg2d6dwlbgcy3879kpjb29564rqxq2m3vrahpv8l4km1w9n0a13")))

