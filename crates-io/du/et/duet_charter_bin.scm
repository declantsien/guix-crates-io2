(define-module (crates-io du et duet_charter_bin) #:use-module (crates-io))

(define-public crate-duet_charter_bin-0.1.1 (c (n "duet_charter_bin") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duet_charter_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)))) (h "1scr0ly7m73qshmf6pa5ipdlqrab6gzj6gh8c2ym4nmnwlibb25r")))

