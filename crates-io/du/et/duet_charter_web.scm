(define-module (crates-io du et duet_charter_web) #:use-module (crates-io))

(define-public crate-duet_charter_web-0.1.1 (c (n "duet_charter_web") (v "0.1.1") (d (list (d (n "duet_charter_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "gloo") (r "^0.8.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Blob"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1fs3082dfiyp37kjx4b510m7pchvhpq2bx13m63zngiv3iqmq6k4")))

