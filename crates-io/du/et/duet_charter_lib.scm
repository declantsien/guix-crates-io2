(define-module (crates-io du et duet_charter_lib) #:use-module (crates-io))

(define-public crate-duet_charter_lib-0.1.1 (c (n "duet_charter_lib") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1nn224zgdfz9lk8xd290zanrhrjl1pflypd1s9q6dqqddwsz8fim")))

