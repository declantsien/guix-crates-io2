(define-module (crates-io du sk dusk_dire) #:use-module (crates-io))

(define-public crate-dusk_dire-0.1.0 (c (n "dusk_dire") (v "0.1.0") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "0a1if0hwfi7cllrikx5dzkyq8g121bgqh57ygdzc9vghh5j25i1c") (y #t)))

(define-public crate-dusk_dire-0.1.1 (c (n "dusk_dire") (v "0.1.1") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "0n7v0ysa7mpky2dcr7mgpwiyhh7xh3js9ml42h9i4d1iv5jb1n0k")))

(define-public crate-dusk_dire-0.1.2 (c (n "dusk_dire") (v "0.1.2") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "1v5lshj3rdj0r7ilia1f8nj9d2y6vfhrnb1ddci378jpz1897hzd")))

(define-public crate-dusk_dire-0.1.3 (c (n "dusk_dire") (v "0.1.3") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "13wlb3zxwppr9w6ni9x96lbijqllbfmq40k550msz1x7r3v7akk0")))

(define-public crate-dusk_dire-0.1.4 (c (n "dusk_dire") (v "0.1.4") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "087qa4bspdnjlc5gf5b9j25b7v6c9picah71mrsvfxyq73l929ic")))

(define-public crate-dusk_dire-0.1.5 (c (n "dusk_dire") (v "0.1.5") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "168ps4f4a058ha2n1ckvyxn9riz5x3dv1yr02fd542ywdpjfcdf8") (s 2) (e (quote (("serde" "dep:serde" "index_vec/serde"))))))

(define-public crate-dusk_dire-0.1.6 (c (n "dusk_dire") (v "0.1.6") (d (list (d (n "display_adapter") (r "^0.1") (d #t) (k 0)) (d (n "index_vec") (r "^0.1.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.5.1") (d #t) (k 0)) (d (n "string-interner") (r "^0.12.1") (d #t) (k 0)))) (h "11zda1qs7di9k96f878392z6y2iy2xbfdlgj4wxr9s1qlkf4747y") (s 2) (e (quote (("serde" "dep:serde" "index_vec/serde"))))))

