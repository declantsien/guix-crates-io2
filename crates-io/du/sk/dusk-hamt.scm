(define-module (crates-io du sk dusk-hamt) #:use-module (crates-io))

(define-public crate-dusk-hamt-0.2.0 (c (n "dusk-hamt") (v "0.2.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.7") (d #t) (k 0)))) (h "0l7q6hrb9vv3bbsnz78g9zzplc94zhqn67lmv7r513chlnx2jaa7")))

(define-public crate-dusk-hamt-0.3.0 (c (n "dusk-hamt") (v "0.3.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.7") (d #t) (k 0)))) (h "1f54h4871dn9z99wr78i44mzf81i5vf4lpczspybqqlpaj3aw26p")))

(define-public crate-dusk-hamt-0.3.1 (c (n "dusk-hamt") (v "0.3.1") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.7") (d #t) (k 0)))) (h "17gsp661lbcwq7p8xjaj2hhyxm8b1cw50j9bldpcja9zi30f6afw")))

(define-public crate-dusk-hamt-0.3.2 (c (n "dusk-hamt") (v "0.3.2") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.9.0-rc") (d #t) (k 0)))) (h "1hbagisbiwhwmjnldrf2x7myq27ipn5wz9gb2zypijww0rgw3avb") (f (quote (("persistance" "microkelvin/persistance")))) (y #t)))

(define-public crate-dusk-hamt-0.4.0 (c (n "dusk-hamt") (v "0.4.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.9") (d #t) (k 0)))) (h "19byll2dm7snfpxnjglla6f7fq4fazrq4vlzs7yq5q4ll7pyapg3") (f (quote (("persistance" "microkelvin/persistance"))))))

(define-public crate-dusk-hamt-0.5.0-rc.0 (c (n "dusk-hamt") (v "0.5.0-rc.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.10.0-rc") (d #t) (k 0)))) (h "07gvn0x4h1r5q1l3ixvb2ymv5911dfcc5b63qing2rkqwdhx07w5") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-dusk-hamt-0.5.0-rc.1 (c (n "dusk-hamt") (v "0.5.0-rc.1") (d (list (d (n "microkelvin") (r "^0.11.0-rc") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.19") (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)))) (h "1ib34kxb61pzix1h7rbzd29v6iypxdzfqbnfswzzqpl0i4rc5msz") (y #t)))

(define-public crate-dusk-hamt-0.6.0-rc.0 (c (n "dusk-hamt") (v "0.6.0-rc.0") (d (list (d (n "microkelvin") (r "^0.12.0-rc") (k 0)) (d (n "microkelvin") (r "^0.12.0-rc") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.19") (k 0)) (d (n "seahash") (r "^4.1.0") (k 0)))) (h "06s6qljs5ppk8jsbdmdhra401glp11jy4fmnij15kv4vn14l7dsf")))

(define-public crate-dusk-hamt-0.7.0 (c (n "dusk-hamt") (v "0.7.0") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.13.0-rc") (k 0)) (d (n "microkelvin") (r "^0.13.0-rc") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)) (d (n "seahash") (r "^4.1.0") (k 0)))) (h "02avcx90lm9qg0yiw21wl2fczcwly7p53m2wmj8knb89816csg3v")))

(define-public crate-dusk-hamt-0.5.0 (c (n "dusk-hamt") (v "0.5.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.10") (d #t) (k 0)))) (h "17izajf8f16z267r66c7g7y46fx801c5zrsjwvz0xb7f4fg7nz8h") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-dusk-hamt-0.8.0 (c (n "dusk-hamt") (v "0.8.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "0zq4ip6bf50mlnxsaw40v36f93g5vi1y729bw7j8sx3815qh4g9i") (f (quote (("persistence" "microkelvin/persistence")))) (y #t)))

(define-public crate-dusk-hamt-0.8.1 (c (n "dusk-hamt") (v "0.8.1") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "0bmcmb26wagwh5jada9bw3zl36x3rpp6w6d9jqskfkk84ni0fl0x") (f (quote (("persistence" "microkelvin/persistence")))) (y #t)))

(define-public crate-dusk-hamt-0.9.0 (c (n "dusk-hamt") (v "0.9.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "0j8xz24s68rawyf1g0mq9sj353v0zk48z7igsgjmbx6kxab8drb2") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-dusk-hamt-0.8.2 (c (n "dusk-hamt") (v "0.8.2") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "1vqdy50fcdah1g76x4dhn55hg6xf7hmgcwd935p0k4pk5wqmvhqh") (f (quote (("persistence" "microkelvin/persistence")))) (y #t)))

(define-public crate-dusk-hamt-0.9.1 (c (n "dusk-hamt") (v "0.9.1") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.14") (d #t) (k 0)))) (h "0w4cf02gd9v7wgmix0lwv27cp680n7lwl899s55pnyrdywjyw09h") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-dusk-hamt-0.10.0-rc.0 (c (n "dusk-hamt") (v "0.10.0-rc.0") (d (list (d (n "canonical") (r "^0.7") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.7") (d #t) (k 0)) (d (n "microkelvin") (r "^0.15.0-rc") (d #t) (k 0)))) (h "126zpffxal1wag2ggy1q7a3gfa5y15hsc188vrxbrd5b3y6v7bz6") (f (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-dusk-hamt-0.10.0-rkyv.0 (c (n "dusk-hamt") (v "0.10.0-rkyv.0") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv.0") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv.0") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)) (d (n "seahash") (r "^4.1.0") (k 0)))) (h "1amq6jk340lfjbb4vbhf1p9wzaccm57xyk1qbpw446c65km3qhzf") (y #t)))

(define-public crate-dusk-hamt-0.11.0-rkyv.0 (c (n "dusk-hamt") (v "0.11.0-rkyv.0") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv.0") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv.1") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)) (d (n "seahash") (r "^4.1.0") (k 0)))) (h "0v8lbqj5wrsrka5s33a3fsk2yd0kmhxddqmszdzzsvwr6x67sk63")))

(define-public crate-dusk-hamt-0.11.0-rkyv.1 (c (n "dusk-hamt") (v "0.11.0-rkyv.1") (d (list (d (n "bytecheck") (r "^0.6.7") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv") (k 0)) (d (n "microkelvin") (r "^0.16.0-rkyv") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.29") (f (quote ("validation"))) (k 0)) (d (n "seahash") (r "^4.1.0") (k 0)))) (h "1knjsyjzxz6r1wy7j5m2n7jmmvkk8hq4j8fxy6h7mcf0b5hxsx10")))

