(define-module (crates-io du sk dusk-cdf) #:use-module (crates-io))

(define-public crate-dusk-cdf-0.1.0 (c (n "dusk-cdf") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0kjysinx2mq8ka94sl1h00jlwxh06kl4gpfbww1xgmk5fkyyv22n")))

(define-public crate-dusk-cdf-0.2.0 (c (n "dusk-cdf") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1q3gh80ihklznzi6w3zy80l3zfg04k0kjhy5asjwk4rwvpsv1lhl")))

(define-public crate-dusk-cdf-0.3.0 (c (n "dusk-cdf") (v "0.3.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05w3911mgs120zj6ylq3xp4fd10s43im006r1mdrai1wkxygl7hf")))

(define-public crate-dusk-cdf-0.4.0 (c (n "dusk-cdf") (v "0.4.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "msgpacker") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14p1v4l7sc1lq3l960f3gxwjm697fjfp8229vlrb2als08w3gi2n")))

(define-public crate-dusk-cdf-0.5.0 (c (n "dusk-cdf") (v "0.5.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "msgpacker") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01w7l34ys91lziiaq9lvvj8vxdhlqr1adbyx2nk4m8glv78vc63a")))

