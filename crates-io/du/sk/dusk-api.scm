(define-module (crates-io du sk dusk-api) #:use-module (crates-io))

(define-public crate-dusk-api-0.0.9 (c (n "dusk-api") (v "0.0.9") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "1c3003jxz8qk3yzad7rl5yq8ng5m6xcwmwdyjha9c870l67b43a6")))

(define-public crate-dusk-api-0.0.10 (c (n "dusk-api") (v "0.0.10") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "0wh6v9a91wb2l5hd9by5m704mxbgqzvji3br2sssgggm6lz043bb")))

(define-public crate-dusk-api-0.0.11 (c (n "dusk-api") (v "0.0.11") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)))) (h "0w13i6fisb6mk7cflj0crm8xbdqygvhc6fdzdcvdznk7iwlp30gs")))

