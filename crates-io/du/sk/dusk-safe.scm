(define-module (crates-io du sk dusk-safe) #:use-module (crates-io))

(define-public crate-dusk-safe-0.1.0 (c (n "dusk-safe") (v "0.1.0") (d (list (d (n "dusk-bls12_381") (r "^0.13") (k 2)) (d (n "ff") (r "^0.13") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "std_rng"))) (k 2)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1b8krfbxjzycdydbikpnsns2vsjfp1j53wz9d9c71c32cj8xvamz")))

(define-public crate-dusk-safe-0.2.0-rc.0 (c (n "dusk-safe") (v "0.2.0-rc.0") (d (list (d (n "dusk-bls12_381") (r "^0.13") (f (quote ("zeroize"))) (k 2)) (d (n "dusk-jubjub") (r "^0.14") (k 2)) (d (n "ff") (r "^0.13") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "std_rng"))) (k 2)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0yw8sq9fzbkcv687x4bjsziwxw3g58mpky0172k1a657csw57d29") (f (quote (("encryption"))))))

(define-public crate-dusk-safe-0.2.0 (c (n "dusk-safe") (v "0.2.0") (d (list (d (n "dusk-bls12_381") (r "^0.13") (f (quote ("zeroize"))) (k 2)) (d (n "dusk-jubjub") (r "^0.14") (k 2)) (d (n "ff") (r "^0.13") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "std_rng"))) (k 2)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1sy9rngafagk6yzjy1aylfi8mb22kpmsr93sgbk1zwqkdsq5mlrg") (f (quote (("encryption"))))))

(define-public crate-dusk-safe-0.2.1 (c (n "dusk-safe") (v "0.2.1") (d (list (d (n "dusk-bls12_381") (r "^0.13") (f (quote ("zeroize"))) (k 2)) (d (n "dusk-jubjub") (r "^0.14") (k 2)) (d (n "ff") (r "^0.13") (k 2)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "std_rng"))) (k 2)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "121hjd9iq4s3lkgk13cyhb56pqplnlxcg0n45n35s7wgnys8869b") (f (quote (("encryption"))))))

