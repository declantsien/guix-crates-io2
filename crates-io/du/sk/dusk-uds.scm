(define-module (crates-io du sk dusk-uds) #:use-module (crates-io))

(define-public crate-dusk-uds-0.1.0 (c (n "dusk-uds") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wqrmrbg94ghz5rdlyvnz684gch4bd9jhp5qn3ykdz57baw0kpxl")))

(define-public crate-dusk-uds-0.1.1 (c (n "dusk-uds") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zj4hxw7fmhfz6x5s8nwnjq3m7k58z7k5spn1w8xvsmsbx1q0b65")))

(define-public crate-dusk-uds-0.2.0 (c (n "dusk-uds") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11") (d #t) (k 0)))) (h "1lbj3ia1lldmm4m9xqkr71qm8jnmjfyqax23rvyswr0ilijryl5k")))

(define-public crate-dusk-uds-0.2.1 (c (n "dusk-uds") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11") (d #t) (k 0)))) (h "091zsv6zh35hr9adi67x5g6lvh7pvhrqkda5mxh9lj0xbakm3zcz")))

