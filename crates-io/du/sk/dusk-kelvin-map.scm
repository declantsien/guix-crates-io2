(define-module (crates-io du sk dusk-kelvin-map) #:use-module (crates-io))

(define-public crate-dusk-kelvin-map-0.1.0 (c (n "dusk-kelvin-map") (v "0.1.0") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1pdzznlcg9wpfnaciqddspppwhhs44smq8i8nhps1ldsf7a790g1")))

(define-public crate-dusk-kelvin-map-0.1.1 (c (n "dusk-kelvin-map") (v "0.1.1") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1bc24an87vjmid4innd76xa5akmnssvgkws71n42dr2r41kbgmss")))

(define-public crate-dusk-kelvin-map-0.1.2 (c (n "dusk-kelvin-map") (v "0.1.2") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1qq6zzbsx5fqz8ia31mlpbzfz04zhbaj9yz50s6gd2w319wyjklf")))

(define-public crate-dusk-kelvin-map-0.2.0 (c (n "dusk-kelvin-map") (v "0.2.0") (d (list (d (n "canonical") (r "^0.4") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.4") (d #t) (k 0)) (d (n "canonical_host") (r "^0.4") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1grjq8yd76si6ljbabambd16f1a9nj59xfciv8ww3vkn2v9cqc7j")))

(define-public crate-dusk-kelvin-map-0.2.1 (c (n "dusk-kelvin-map") (v "0.2.1") (d (list (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "microkelvin") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1iqnn9kpwwm052cb8p6bv3dfnhmxrp0vxz0i58p4x65zldm25bw3")))

(define-public crate-dusk-kelvin-map-0.3.0 (c (n "dusk-kelvin-map") (v "0.3.0") (d (list (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "microkelvin") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0m8n4b03csz9x7gdcbnbw3x4psc9wx28d5caxqzkvlm8zr0hlmil")))

(define-public crate-dusk-kelvin-map-0.4.0 (c (n "dusk-kelvin-map") (v "0.4.0") (d (list (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "microkelvin") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "07vlg8fq6j6kvz8am9fng28plwch2fs4n97jz36whq7cy6iqlvws")))

