(define-module (crates-io du sk dusk-abi) #:use-module (crates-io))

(define-public crate-dusk-abi-0.4.0 (c (n "dusk-abi") (v "0.4.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mazk7jpv6zgchggjyznj81y7mbs3yap6hfdxlz8c7nsnnn6c1fr") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.5.0 (c (n "dusk-abi") (v "0.5.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0ygwpzhz0ih8hjrc0mlajwbvx59r1f32g35d302j27allkd52imx") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.5.1 (c (n "dusk-abi") (v "0.5.1") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0x28ki1lawj1n1b6bylrrh7iyaqsk85larx5q5winvasbjlrh4rl") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.6.0 (c (n "dusk-abi") (v "0.6.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1wh2dk1ljfi644fji2q47cfpgz0p410kacdn4bl6af9vdgsnqg8d") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.7.0 (c (n "dusk-abi") (v "0.7.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0997i4s81wm0rrsaab6abv7l8swc6j0wn7p8j2ykirxb5dkriswd") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.8.0 (c (n "dusk-abi") (v "0.8.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.5") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.5") (d #t) (k 0)) (d (n "canonical_fuzz") (r "^0.5") (d #t) (k 2)) (d (n "canonical_host") (r "^0.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0zfsa6r39125vh8fw9vi8g2hg8n0llrhgksqx6glncywkqmp6nip") (f (quote (("std"))))))

(define-public crate-dusk-abi-0.9.0-rc.0 (c (n "dusk-abi") (v "0.9.0-rc.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0m2zy2zsd06z2qbdkrkxa8vn25rzi6mdd2zqhgwsr6r17n0wz3z2")))

(define-public crate-dusk-abi-0.9.0-rc.1 (c (n "dusk-abi") (v "0.9.0-rc.1") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0k9xgc1h75sdj3xlw29wcysdbj4mcs3kshlbd441qgvb8dj20yqv")))

(define-public crate-dusk-abi-0.9.0 (c (n "dusk-abi") (v "0.9.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1jn3lb59xghyx84y7sjfsg79xl5za9h3bprpg2qm8062ycaqb5q1")))

(define-public crate-dusk-abi-0.9.1 (c (n "dusk-abi") (v "0.9.1") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0461lrs2w9pkhx7cb3npgf11yf83ljia8gflmkjfkfzx7j7jgszc")))

(define-public crate-dusk-abi-0.10.0 (c (n "dusk-abi") (v "0.10.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0f4x5xpnlcbs2rqj7ly5hca8d6p2m7cgmciyp560dwl5wzi6kq3p")))

(define-public crate-dusk-abi-0.10.1 (c (n "dusk-abi") (v "0.10.1") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.6") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.6") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1nsa0vcnqfc2l9ircxzh8rcdv5awjq2p6zk8y3s54q1bk4fybw89")))

(define-public crate-dusk-abi-0.11.0 (c (n "dusk-abi") (v "0.11.0") (d (list (d (n "arbitrary") (r "^0.3") (d #t) (k 2)) (d (n "canonical") (r "^0.7") (d #t) (k 0)) (d (n "canonical_derive") (r "^0.7") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1jqf6jf6gz19xwgj998vbhj9r6l4q38xfn5j72xlygycfxbc0f3h")))

