(define-module (crates-io du sk dusklang) #:use-module (crates-io))

(define-public crate-dusklang-0.1.0 (c (n "dusklang") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)))) (h "09d9cm4wsygni21a1wdd7lxr65giag48qv8gkp7vdsxf8xl0wngd")))

(define-public crate-dusklang-0.1.2 (c (n "dusklang") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)))) (h "1yz3bw26z2lfq2c5xv3d5q079sxcnqxy3mlw219cz2pnqrxmza2s")))

(define-public crate-dusklang-0.1.3 (c (n "dusklang") (v "0.1.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)))) (h "1jjdfbbg607l5wgy0slvyfkrjpay25656ipikc87da9frn2sm1x3")))

(define-public crate-dusklang-0.1.4 (c (n "dusklang") (v "0.1.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)))) (h "0s9ggv9x2ggzyz6dc9a1ii0gsaax313inszjdmzgx4yhsgyzzgdi")))

(define-public crate-dusklang-0.1.5 (c (n "dusklang") (v "0.1.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.30.2") (o #t) (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (f (quote ("tables-api"))) (o #t) (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "interprocess") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1lpal0sgfccski822kwa8m9f4lk0fsf4m95hi04ms43hlz81ixvn") (s 2) (e (quote (("dvd" "libdusk/dvd" "dep:glium" "dep:imgui" "dep:rand" "dep:imgui-winit-support" "dep:interprocess" "dep:imgui-glium-renderer"))))))

(define-public crate-dusklang-0.1.6 (c (n "dusklang") (v "0.1.6") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dusk_dire") (r "^0.1") (d #t) (k 0)) (d (n "glium") (r "^0.30.2") (o #t) (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (f (quote ("tables-api"))) (o #t) (d #t) (k 0)) (d (n "imgui-glium-renderer") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8.2") (o #t) (d #t) (k 0)) (d (n "interprocess") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "libdusk") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0zz1rask54r480nvdn1013swsc9zq4shw7ra9502x51x4v6cjkbz") (s 2) (e (quote (("dvd" "libdusk/dvd" "dep:glium" "dep:imgui" "dep:rand" "dep:imgui-winit-support" "dep:interprocess" "dep:imgui-glium-renderer"))))))

