(define-module (crates-io du sk dusk-bytes) #:use-module (crates-io))

(define-public crate-dusk-bytes-0.1.0 (c (n "dusk-bytes") (v "0.1.0") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "0cd3ns4xgf4acljdpgp0jkgmp9bv89sgax19hzxb7afxlzpazpi5")))

(define-public crate-dusk-bytes-0.1.1 (c (n "dusk-bytes") (v "0.1.1") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "15wd58i6g0v25dz401536s80qvx8wld87pmppnh4lyz041321h9n")))

(define-public crate-dusk-bytes-0.1.2 (c (n "dusk-bytes") (v "0.1.2") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "09am4n3bfayb498n3dmqnzz7wj782civy67m5dz5z6dvw6ii8n33")))

(define-public crate-dusk-bytes-0.1.3 (c (n "dusk-bytes") (v "0.1.3") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "0g0i96ijwiq5mcrjd057k0nq193m2smpfxfq3ddz39lmg91rkxyw")))

(define-public crate-dusk-bytes-0.1.4 (c (n "dusk-bytes") (v "0.1.4") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "0ayn4kqbfi6z41axcg490gq2aqa8r1k0ir2zb4v8b0qji2i86v05")))

(define-public crate-dusk-bytes-0.2.0 (c (n "dusk-bytes") (v "0.2.0") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "1zjlhpbympjlxdmxq13x1zsh4q2bja01952j3nsh9865bzm78j4x") (y #t)))

(define-public crate-dusk-bytes-0.1.5 (c (n "dusk-bytes") (v "0.1.5") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "12hgr7nnp8q56jxk8bsjffyaj0c1wmwmz9w9v0swqa55sqryq8j4")))

(define-public crate-dusk-bytes-0.1.6 (c (n "dusk-bytes") (v "0.1.6") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "1iyq3hzfgagzfixh77lcjnnaxs3m4mhwb6xpwmfcjrfrjqkf3apl")))

(define-public crate-dusk-bytes-0.1.7 (c (n "dusk-bytes") (v "0.1.7") (d (list (d (n "derive-hex") (r "^0.1") (d #t) (k 0)))) (h "0zrkc4ydzc1zkq7j51g8hl5gvwhy396bssckv7qysh875ywhklnm")))

