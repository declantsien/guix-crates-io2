(define-module (crates-io du at duat-term) #:use-module (crates-io))

(define-public crate-duat-term-0.1.0 (c (n "duat-term") (v "0.1.0") (d (list (d (n "cassowary") (r ">=0.3.0") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "duat-core") (r ">=0.1.0") (d #t) (k 0)) (d (n "smallvec") (r ">=1.10.0") (d #t) (k 0)) (d (n "unicode-width") (r ">=0.1.10") (d #t) (k 0)))) (h "16kjf3djcws66xb9hilqqvycrvf3bdc3pwnibxlf7ph30l5s2q76")))

