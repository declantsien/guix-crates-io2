(define-module (crates-io du at duat) #:use-module (crates-io))

(define-public crate-duat-0.1.0 (c (n "duat") (v "0.1.0") (d (list (d (n "atomic-wait") (r "^1.1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 1)) (d (n "duat-core") (r ">=0.1.0") (d #t) (k 0)) (d (n "duat-term") (r ">=0.1.0") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "176xyq75xs9q9kw15fk97c7m5jrx0qdwh6wc37nmxwanib31mdah") (f (quote (("default" "term-ui")))) (s 2) (e (quote (("term-ui" "dep:duat-term"))))))

(define-public crate-duat-0.1.1 (c (n "duat") (v "0.1.1") (d (list (d (n "atomic-wait") (r "^1.1.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 1)) (d (n "duat-core") (r ">=0.1.0") (d #t) (k 0)) (d (n "duat-term") (r ">=0.1.0") (o #t) (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)))) (h "0ix5jvivik6g8nds5i4xbpmaqar71h2i1c6fjhmk8li95d13ns5l") (f (quote (("default" "term-ui")))) (s 2) (e (quote (("term-ui" "dep:duat-term"))))))

