(define-module (crates-io du at duat-core) #:use-module (crates-io))

(define-public crate-duat-core-0.1.0 (c (n "duat-core") (v "0.1.0") (d (list (d (n "any-rope") (r ">=1.2.2") (d #t) (k 0)) (d (n "crossterm") (r ">=0.26.0") (d #t) (k 0)) (d (n "ropey") (r ">=1.6.0") (d #t) (k 0)))) (h "1w4k44j3bwrxfvp7mbqi138fik2qhq2hcwimw3z1zrw72629bb0f") (f (quote (("wacky-colors") ("default"))))))

