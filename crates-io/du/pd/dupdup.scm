(define-module (crates-io du pd dupdup) #:use-module (crates-io))

(define-public crate-dupdup-0.1.0 (c (n "dupdup") (v "0.1.0") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1id7y324kqg010748022355i8dyrjwx22jz63f3q9cw6665q38fx")))

(define-public crate-dupdup-0.2.0 (c (n "dupdup") (v "0.2.0") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "13j0s1s62sal3lbrnhyhk4jkqpydzv819rbqyylp1wjliid3mkfk")))

(define-public crate-dupdup-0.2.1 (c (n "dupdup") (v "0.2.1") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0kdnlscmra1qs43widbb2mry9zb9ajrpwn62h5lmi3vnlcpwpn5g")))

(define-public crate-dupdup-0.2.2 (c (n "dupdup") (v "0.2.2") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0dcx4my7a58skahh0q6002zb4gx0r5ld8aiv7vadsp2b04wm4jac")))

(define-public crate-dupdup-0.2.3 (c (n "dupdup") (v "0.2.3") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0jihv47a3pyvcqrpn64mf6nrbh71hpkyywnb48kc8qhpv3xbsjpr")))

(define-public crate-dupdup-0.3.0 (c (n "dupdup") (v "0.3.0") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "md5") (r "^0.3.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1c50r6m9h4ga9hxix7vc00wyvv3v89rwf2wshajd12jc0qh9jxhk")))

