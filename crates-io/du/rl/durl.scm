(define-module (crates-io du rl durl) #:use-module (crates-io))

(define-public crate-durl-0.1.0 (c (n "durl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11csfamqylg5g7l2igf8kdk8nd4gjjp47jgsdqa8gj8nz8hzpf1l")))

(define-public crate-durl-0.1.1 (c (n "durl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0imms8cpvfm7nk1nl0p8dmardx2bb66yw2h3hsyxgmibq35gr7b9")))

(define-public crate-durl-0.1.2 (c (n "durl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ak8vj2jgn1mhin9qbcfp93wi306yg169wkn3j433rshim9r0ids") (y #t)))

(define-public crate-durl-0.1.3 (c (n "durl") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "122p2ici01ziqvm9ky94kjr1d5yvns76ayqskxwdzkyfgzsd5jlw")))

(define-public crate-durl-0.1.4 (c (n "durl") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.1.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmjwxp21ls6diwpxmybkgg578wxdbb3f0msi80nlmdb89203p3i")))

(define-public crate-durl-0.2.0 (c (n "durl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pk2v4di3qrpyl5zwkz87icf1xv99q41ha20sr1rvmgf6ffm3329")))

(define-public crate-durl-0.2.1 (c (n "durl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n0jscdpdyjz242sqgzpcwknma36rg3f6cmysmr9lvsqrvy9daq3")))

(define-public crate-durl-0.2.2 (c (n "durl") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "download-lib") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15giql5biywwwwvpakyczkp3rr0z9bcl4v5cb52gwh5hl3fh34p0")))

