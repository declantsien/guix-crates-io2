(define-module (crates-io du k- duk-sys) #:use-module (crates-io))

(define-public crate-duk-sys-0.2.0 (c (n "duk-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.48") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4.10") (o #t) (d #t) (k 0)))) (h "06div3r350nqmxxj2dxpsk48hq9110yahw6dag9fyblfj1lzcj5l") (f (quote (("trace" "log") ("spam" "log") ("debug" "log"))))))

(define-public crate-duk-sys-0.3.0 (c (n "duk-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "cc") (r "^1.0.48") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4.10") (o #t) (d #t) (k 0)))) (h "0a5g4vbs6q5xp75ipvr576h66433z70v3l96yxsg3b6y2pqngjmp") (f (quote (("trace" "log") ("spam" "log") ("debug" "log"))))))

