(define-module (crates-io du ri durian_proc_macros) #:use-module (crates-io))

(define-public crate-durian_proc_macros-0.1.0 (c (n "durian_proc_macros") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "durian") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11lk7c5qrd05v7m5p3hrsy1gxpcczvgcmb6za1ny560m2ndwbn2s")))

(define-public crate-durian_proc_macros-0.2.0 (c (n "durian_proc_macros") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "durian") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgbsjilwmxpbg8lc90b0sa5jagqr2q4p0bqyx8khq5s0mhqxmp0")))

(define-public crate-durian_proc_macros-0.2.1 (c (n "durian_proc_macros") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "durian") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02qxif7bc3554jiid2c3b3m3jm6lskdm23cnrc895jfg7p0n4jpb")))

