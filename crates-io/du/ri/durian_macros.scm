(define-module (crates-io du ri durian_macros) #:use-module (crates-io))

(define-public crate-durian_macros-0.1.0 (c (n "durian_macros") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12sjbn4z031q4iavrk94xzr0cz40z1jxsj8j0h4nhx4fjh1izlgj")))

(define-public crate-durian_macros-0.2.0 (c (n "durian_macros") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "durian") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iamz8wr8bvc6vrrz824b57sacijh0478lxazic0mpzczyash8i4")))

(define-public crate-durian_macros-0.3.0 (c (n "durian_macros") (v "0.3.0") (d (list (d (n "durian_proc_macros") (r "^0.1") (d #t) (k 0)))) (h "1nx38wvsd31dnv5s6lgz2xrqb2v4psfnk209nf1j4vrfslj228zc")))

(define-public crate-durian_macros-0.4.0 (c (n "durian_macros") (v "0.4.0") (d (list (d (n "durian") (r "^0.3") (d #t) (k 2)) (d (n "durian_proc_macros") (r "^0.1") (d #t) (k 0)))) (h "06jzpcfrzc1jj3ab70ji149lb4s7cc17gll8vgfm74qggxr5qngf")))

(define-public crate-durian_macros-0.4.1 (c (n "durian_macros") (v "0.4.1") (d (list (d (n "durian") (r "^0.4") (d #t) (k 2)) (d (n "durian_proc_macros") (r "^0.2") (d #t) (k 0)))) (h "1nj0afsmvnlyjxi4gjxr7hmjn1lc6n46lyan6s6dy8dj0vjfjgwa")))

