(define-module (crates-io du ri during) #:use-module (crates-io))

(define-public crate-during-0.1.0 (c (n "during") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "127wi89xi8adlyh59f21hhkcyp6b0diiab21jf9qikja1rw3y39x")))

(define-public crate-during-0.1.1 (c (n "during") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0qh863jmry5rhshxjm65d7k88i4brdf7i3cjsxayjf111yfs4p0m")))

