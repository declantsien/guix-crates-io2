(define-module (crates-io du -d du-dup) #:use-module (crates-io))

(define-public crate-du-dup-0.2.0 (c (n "du-dup") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "01r5gc2qq0hj2vnij4nl0mlr3zzs8b1f2cmr0hwsdmss3a5yi6pp")))

