(define-module (crates-io du rs durs-common-tools) #:use-module (crates-io))

(define-public crate-durs-common-tools-0.1.0 (c (n "durs-common-tools") (v "0.1.0") (d (list (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1hxr9lrwdag9a8ggckrqlnn4g2zqdxpkip0g8djqig3gsxw2ghy8")))

(define-public crate-durs-common-tools-0.2.0 (c (n "durs-common-tools") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0snx5nvcfw48skrazd0lagza16k00psdr7ivm1m9q0f178d2a9qp")))

