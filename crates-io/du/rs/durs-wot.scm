(define-module (crates-io du rs durs-wot) #:use-module (crates-io))

(define-public crate-durs-wot-0.8.0-a0.9 (c (n "durs-wot") (v "0.8.0-a0.9") (d (list (d (n "bincode") (r "1.0.*") (d #t) (k 0)) (d (n "byteorder") (r "1.2.*") (d #t) (k 0)) (d (n "rayon") (r "1.0.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)))) (h "13djrcxmypsm441xz7cg38nlw52qj56a120kiy9b9y9dakv04mbc") (f (quote (("strict"))))))

(define-public crate-durs-wot-0.9.0 (c (n "durs-wot") (v "0.9.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 2)) (d (n "durs-common-tools") (r "^0.2.0") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v9p570k360ksrxah6zczhasx4yr88p8w72hfzj24qyir926x7m2")))

