(define-module (crates-io du mb dumbmath) #:use-module (crates-io))

(define-public crate-dumbmath-0.0.1 (c (n "dumbmath") (v "0.0.1") (h "1ylmd6w4151kjbwpdca8963hppgdjx1nz8mn22nzhxkkw1j3byq0")))

(define-public crate-dumbmath-0.0.2 (c (n "dumbmath") (v "0.0.2") (h "0clxg4k5fn5bwl0104va9m9ph0jp6zd2w3alndh518jv3wz5h7s4")))

(define-public crate-dumbmath-0.0.3 (c (n "dumbmath") (v "0.0.3") (h "0nx3mjaaqidbwiay48lzk0qf4ysp1h38j0p2da7fr65i9iyg7sv6")))

(define-public crate-dumbmath-0.0.4 (c (n "dumbmath") (v "0.0.4") (h "0nzb3g4hgf1mc5gkjcbbcj0divrhx4cgzrcsg1cf9rgsmwy75h58")))

(define-public crate-dumbmath-0.0.5 (c (n "dumbmath") (v "0.0.5") (h "0yp3m4yr75d5v6kfcdydxvalbwbpx03w5j143fq35h78iyhgnn3f")))

(define-public crate-dumbmath-0.0.6 (c (n "dumbmath") (v "0.0.6") (h "0iddpz67s2a8c3is5ya6bv0jax2iv495a380xkzy47aldfz12vxf")))

(define-public crate-dumbmath-0.0.7 (c (n "dumbmath") (v "0.0.7") (h "073h0kxcwwcd6sjmm87356nwf14g9jsk0r6yi3cnw8s829jnsbby")))

(define-public crate-dumbmath-0.0.8 (c (n "dumbmath") (v "0.0.8") (h "1w7g2vw2ib5dfh1sg9649xw70wbbajlqf3a2r0qpnml6ph9zxp43")))

(define-public crate-dumbmath-0.1.0 (c (n "dumbmath") (v "0.1.0") (h "0i1rzvzazramc9ca83a3m2k7yd5v92z5fgcj44rm1vsq5f55kzhv")))

(define-public crate-dumbmath-0.1.1 (c (n "dumbmath") (v "0.1.1") (h "1fq255ip1ks9qhasaww8ci21w2b76jjsvnamqncxdkpshz5h8lzf")))

(define-public crate-dumbmath-0.1.2 (c (n "dumbmath") (v "0.1.2") (h "0zkb9bd9xnlapjlqxizc6d4vygwvrdgac1zhsq1ibv7hpif4ksbf")))

(define-public crate-dumbmath-0.1.3 (c (n "dumbmath") (v "0.1.3") (h "03xa6plx98mcn84b64as2j3gixnawasvcxdq9gypi5gfwawy8jyq")))

(define-public crate-dumbmath-0.1.4 (c (n "dumbmath") (v "0.1.4") (h "1hs8d74snbycqc2f145h0rwby7qvyrvwwlr7370xv0mlify14vh3")))

(define-public crate-dumbmath-0.1.6 (c (n "dumbmath") (v "0.1.6") (h "098qw1w3dca61r65wpba77cv1ymj65f67dkx23vq1diksyj4xsap")))

(define-public crate-dumbmath-0.1.7 (c (n "dumbmath") (v "0.1.7") (h "0n111p529lwn4aw3ag6n41yr7smng4slgalpfhkb47vva9m3nv3x")))

(define-public crate-dumbmath-0.2.0 (c (n "dumbmath") (v "0.2.0") (h "1c7jdnfnrrgkp7aszjirngdj7gb742izlrxw1j5jabrc86a61khn")))

(define-public crate-dumbmath-0.2.2 (c (n "dumbmath") (v "0.2.2") (h "101nnbb3n17wjq4sadandm57grf0hp6sj4vj72yrlv38fxfmd3cy")))

(define-public crate-dumbmath-0.2.3 (c (n "dumbmath") (v "0.2.3") (h "0zfx917hh9bqr7yym6aary0n2l9g666vgwvbfrngsdmac6f13fvv")))

