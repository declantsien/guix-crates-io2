(define-module (crates-io du mb dumb_ai) #:use-module (crates-io))

(define-public crate-dumb_ai-1.0.0 (c (n "dumb_ai") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0cxvkgi9y9q91crb5qzfq7p95p7dpnaf0g9knh1q6n27bhn90d4y") (y #t)))

(define-public crate-dumb_ai-1.0.1 (c (n "dumb_ai") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "115zshpz43bffds2nwsd0gg5v0yi93wdshcn6zl118k8c6hh5m7x") (y #t)))

(define-public crate-dumb_ai-2.0.0 (c (n "dumb_ai") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0njdvc2xygpbnxs9s8aqa81svqg22k235pxgp14yhnnk730vh7bb") (y #t)))

(define-public crate-dumb_ai-2.0.1 (c (n "dumb_ai") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "16dxzjshg8pi0l8pjz5vp03qd6i2cwwwvkll1q84r1awp3755kbb") (y #t)))

(define-public crate-dumb_ai-2.0.2 (c (n "dumb_ai") (v "2.0.2") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1pwzgba6x62yqkvaxcjb43i772ncy143bnrfhpa22nb6zgj4207h") (y #t)))

(define-public crate-dumb_ai-2.1.0 (c (n "dumb_ai") (v "2.1.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1p85isiwzlj9v3v1gxadd3ghy4y1hp5j8smry1xh7mr4g1x8fkv9") (y #t)))

(define-public crate-dumb_ai-3.0.0 (c (n "dumb_ai") (v "3.0.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1q8lnbp0vsymrlwvr4vi3jcap76flxlfhi9pzi50ww4ph2amg3ni") (y #t)))

(define-public crate-dumb_ai-3.1.0 (c (n "dumb_ai") (v "3.1.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0nwh8pwxpsl41yzapqqg63fikxif3n5bh7zji8rmr5v5vc00971p")))

