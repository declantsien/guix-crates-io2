(define-module (crates-io du mb dumbnet) #:use-module (crates-io))

(define-public crate-dumbnet-0.1.0 (c (n "dumbnet") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.1") (f (quote ("dummy"))) (k 0)) (d (n "gnuplot") (r "^0.0.32") (d #t) (k 2)) (d (n "indicatif") (r "^0.13") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (k 0)) (d (n "numeric-array") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "10zffc59ynbcmfxylpajqv0zav6m2ymssb7aimga55nklvskxnrx")))

