(define-module (crates-io du mb dumb_http_parser) #:use-module (crates-io))

(define-public crate-dumb_http_parser-0.1.1 (c (n "dumb_http_parser") (v "0.1.1") (d (list (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1fkgirq5c8prabljvj8fha7fqkpa4b3m1h9836dik7d2f313b2dx")))

(define-public crate-dumb_http_parser-0.1.2 (c (n "dumb_http_parser") (v "0.1.2") (d (list (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "086kwdfkxjswrpvgxvlvahskvr78x81wgvxvqy7c8a8h1587vdrr")))

