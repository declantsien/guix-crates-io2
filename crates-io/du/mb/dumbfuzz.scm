(define-module (crates-io du mb dumbfuzz) #:use-module (crates-io))

(define-public crate-dumbfuzz-0.1.0 (c (n "dumbfuzz") (v "0.1.0") (h "0236grf15l8bf3pmg602nkbj5m2km6c14vqgd3g0649gh727wbg2")))

(define-public crate-dumbfuzz-1.0.0 (c (n "dumbfuzz") (v "1.0.0") (h "1d5ldk0bi88xg4ljsv94avfbaf31741h02adsnnalzmqf5ymqksw")))

