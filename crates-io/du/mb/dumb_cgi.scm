(define-module (crates-io du mb dumb_cgi) #:use-module (crates-io))

(define-public crate-dumb_cgi-0.5.0 (c (n "dumb_cgi") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (o #t) (d #t) (k 0)))) (h "09xxa16nfv6ypprjmr4gxqwmsqqj488zl5n11cibagzwzrsqchk4") (s 2) (e (quote (("log" "dep:simplelog"))))))

(define-public crate-dumb_cgi-0.6.0 (c (n "dumb_cgi") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0942gsx3njcrrwid5i9ga29a25c9x97q9qaqbzac92sfl8x7d86y") (s 2) (e (quote (("log" "dep:simplelog"))))))

(define-public crate-dumb_cgi-0.7.0 (c (n "dumb_cgi") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1dnvp9xxpdn1h4v254wqd5ghfn5d5xjqk02b8q6dy6jy9kxjv2aj") (s 2) (e (quote (("log" "dep:log" "dep:simplelog") ("default" "dep:log"))))))

(define-public crate-dumb_cgi-0.8.0 (c (n "dumb_cgi") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1pdms67vk120r97y3varvnnccvf48vaz7xhx9r0pq8qmmq5np29d") (f (quote (("default")))) (s 2) (e (quote (("log" "dep:log" "dep:simplelog"))))))

