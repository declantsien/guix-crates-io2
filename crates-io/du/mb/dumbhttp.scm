(define-module (crates-io du mb dumbhttp) #:use-module (crates-io))

(define-public crate-dumbhttp-0.1.0 (c (n "dumbhttp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "hyper") (r "^0.14.17") (d #t) (k 0)) (d (n "routerify") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p5nw7a8l4yk3i9hq9snmpz4abxbvi8rappj9h80nsaik4cizmy2")))

(define-public crate-dumbhttp-0.1.1 (c (n "dumbhttp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "hyper") (r "^0.14.17") (d #t) (k 0)) (d (n "routerify") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "116657bcpykbk4lzsc51bkd6sm7a2zg6am53x4xq0rsjk5pkkbaf")))

(define-public crate-dumbhttp-0.1.2 (c (n "dumbhttp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "hyper") (r "^0.14.17") (d #t) (k 0)) (d (n "routerify") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lphwpvpgpl14sxqdxd5z3j0inbj6c9ikdrs0g71hcphb4a87xii")))

(define-public crate-dumbhttp-0.1.3 (c (n "dumbhttp") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "hyper") (r "^0.14.17") (d #t) (k 0)) (d (n "routerify") (r "^3.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4i9bkr72qn6j7pynq8kfnd30p9dqsjxig75w9gnbqhdb5i3x30")))

(define-public crate-dumbhttp-0.1.4 (c (n "dumbhttp") (v "0.1.4") (d (list (d (n "hyper") (r "^0.14.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kvrd35mj7icvkmwcrl2zpfdpdqr62h4mpsxjnx9rl60q4xx874h")))

