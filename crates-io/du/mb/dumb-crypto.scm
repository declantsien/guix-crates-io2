(define-module (crates-io du mb dumb-crypto) #:use-module (crates-io))

(define-public crate-dumb-crypto-1.0.0 (c (n "dumb-crypto") (v "1.0.0") (h "005yn965i6kq8vg9zvxf60574968p7fq5k52q1nsx9xgzgg8b7np")))

(define-public crate-dumb-crypto-1.0.1 (c (n "dumb-crypto") (v "1.0.1") (h "0130yp90kwfm0jagl5lxydmn138gkl5qq2cgm84f0hh9is44z208")))

(define-public crate-dumb-crypto-1.0.2 (c (n "dumb-crypto") (v "1.0.2") (h "054ylri5j442z3ml2fbgr24ni2gczy2g6xl0rl36mbar369yayd5")))

(define-public crate-dumb-crypto-1.0.3 (c (n "dumb-crypto") (v "1.0.3") (h "0bfsx0piphiv0d8psrka7qnrbsrhgvnvmn8z84v5ch79phc7fmm9")))

(define-public crate-dumb-crypto-1.0.4 (c (n "dumb-crypto") (v "1.0.4") (h "0gd219ydgi4yfbxcm6mdvf5zw6j3s85vkq0py38s4l5sk7ig6vw1")))

(define-public crate-dumb-crypto-1.0.5 (c (n "dumb-crypto") (v "1.0.5") (h "1afmpwsd4lb8mpkqy39hnd2fjskzbg5zjzhmvj4zq31a0v6kzayk")))

(define-public crate-dumb-crypto-2.0.0 (c (n "dumb-crypto") (v "2.0.0") (h "0kx1b1yba159yvr2z6ziy0v31sp5xf9mlvxf7yznrqrnnxlrdr6r")))

(define-public crate-dumb-crypto-3.0.0 (c (n "dumb-crypto") (v "3.0.0") (h "1lc46alp9ra82y59w50znp8b2bl9lla39991dq46m47cizb37v73")))

(define-public crate-dumb-crypto-3.0.1 (c (n "dumb-crypto") (v "3.0.1") (h "17x6xhpj8f8pv2r3pklwqzsz8jhgccc0bkkmil16gbp30mz3g4qx")))

(define-public crate-dumb-crypto-3.1.0 (c (n "dumb-crypto") (v "3.1.0") (h "0rx9q3rvqqk938xhq94nkkzyz2yyan26gwb458glrfbzamllm0vx")))

