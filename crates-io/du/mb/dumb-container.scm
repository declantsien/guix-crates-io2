(define-module (crates-io du mb dumb-container) #:use-module (crates-io))

(define-public crate-dumb-container-0.0.1 (c (n "dumb-container") (v "0.0.1") (h "03f12xvh54x2ynsb4syldx345m0nazcisgrhmysr18xih7xgm46f") (y #t)))

(define-public crate-dumb-container-0.0.2 (c (n "dumb-container") (v "0.0.2") (h "1advl55yfvxnsi36qd1akmgs76pabm5hnw93m0hkbn7v2al2v1gx") (y #t)))

