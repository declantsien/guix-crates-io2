(define-module (crates-io du mb dumb-exec) #:use-module (crates-io))

(define-public crate-dumb-exec-0.0.1 (c (n "dumb-exec") (v "0.0.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("nightly"))) (k 0)))) (h "06h0vzqv959h6w5q5ccc4hrdnjhydmkjsprwbybkjjma4i8wl15k")))

(define-public crate-dumb-exec-0.0.2 (c (n "dumb-exec") (v "0.0.2") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("nightly"))) (k 0)))) (h "0y25lbpyfmkls7cw4b2qdq6f9glfx1yanbxzkrhkfgvb77hp3jm2")))

(define-public crate-dumb-exec-0.0.3 (c (n "dumb-exec") (v "0.0.3") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("nightly"))) (k 0)))) (h "0m8dfa5w0vwyb807br7115svvc6pjz210vdjcg8y244b4zvk9chy")))

(define-public crate-dumb-exec-0.0.4 (c (n "dumb-exec") (v "0.0.4") (d (list (d (n "futures-preview") (r "^0.3.0-alpha") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0cwkxwr3fmnrqrhlxrb973li50id6r3612bv7iym1pj6vh5rxsi8")))

(define-public crate-dumb-exec-0.0.5 (c (n "dumb-exec") (v "0.0.5") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.7") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "spin") (r "^0.4.9") (d #t) (k 0)))) (h "1nia8z4ld31k2af30w3az1z5kwxjfp1cqzf4ram8z9jhiaqv0f2w") (f (quote (("default") ("alloc"))))))

(define-public crate-dumb-exec-0.0.6 (c (n "dumb-exec") (v "0.0.6") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.7") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "spin") (r "^0.4.9") (d #t) (k 0)))) (h "03hwbscshas9bsgv8c2g0izjkmdacjc36rlm9yahm8kajqba9sia") (f (quote (("default") ("alloc"))))))

(define-public crate-dumb-exec-0.0.7 (c (n "dumb-exec") (v "0.0.7") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.7") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "spin") (r "^0.4.9") (d #t) (k 0)))) (h "1b7ph3gsakic57zb11sh0g2pml0c4b17v3fa003rk89x4s6nwqv5") (f (quote (("default") ("alloc"))))))

