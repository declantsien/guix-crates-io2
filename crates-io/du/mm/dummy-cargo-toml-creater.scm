(define-module (crates-io du mm dummy-cargo-toml-creater) #:use-module (crates-io))

(define-public crate-dummy-cargo-toml-creater-0.1.2 (c (n "dummy-cargo-toml-creater") (v "0.1.2") (d (list (d (n "cargo_toml") (r "^0.6.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "16b7z1wkh84xw5xn0dkw8ds8hhsar42sqc2ynzlx8nnnyasbzl6i")))

(define-public crate-dummy-cargo-toml-creater-0.1.5 (c (n "dummy-cargo-toml-creater") (v "0.1.5") (d (list (d (n "cargo_toml") (r "^0.6.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "0ljdv5l1pbl0xg5fg98nnmry41vd4sqljlhsy0vlky439kbl4pnw")))

(define-public crate-dummy-cargo-toml-creater-0.1.6 (c (n "dummy-cargo-toml-creater") (v "0.1.6") (d (list (d (n "cargo_toml") (r "^0.6.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "01wd76vl3lsyyzi6njz2gf62zc91000s2wy1h1llck3ycqnhyqix")))

