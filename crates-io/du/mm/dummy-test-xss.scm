(define-module (crates-io du mm dummy-test-xss) #:use-module (crates-io))

(define-public crate-dummy-test-xss-0.1.0 (c (n "dummy-test-xss") (v "0.1.0") (h "0hkr0npjrvq0abff3irma3acpbgqaflqk3y4mncqay8y3xais6dn")))

(define-public crate-dummy-test-xss-0.1.1 (c (n "dummy-test-xss") (v "0.1.1") (h "0xnnwd5k0cc350g8m18faaxwa81r8kz22jdbq9lhwvqcrd35554h")))

(define-public crate-dummy-test-xss-0.1.2 (c (n "dummy-test-xss") (v "0.1.2") (h "13l2jmnrwlxdfdx0912xs4cmv8vrljj2k8w3qfqh6z3qma9s88bq")))

(define-public crate-dummy-test-xss-0.1.3 (c (n "dummy-test-xss") (v "0.1.3") (h "1rbhah7mr3m8w774brf3qgbmbs781vwswp0z80bk33d47zxvvxn8")))

(define-public crate-dummy-test-xss-0.1.4 (c (n "dummy-test-xss") (v "0.1.4") (h "0alcwjcam4ja1hrci00n648lw7irv53q642sgcy3wcz5s200cdpi")))

(define-public crate-dummy-test-xss-0.1.5 (c (n "dummy-test-xss") (v "0.1.5") (h "1x835z6lma46fxr0jnx46mlxgpy60s7hg7gflghakq87vqvdbsxh")))

