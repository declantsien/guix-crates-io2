(define-module (crates-io du mm dummy-queue) #:use-module (crates-io))

(define-public crate-dummy-queue-1.0.0 (c (n "dummy-queue") (v "1.0.0") (h "12sw2y5sx58dj3b5r71ci8614zixrp8hl10w3z0hm130cpg969gx")))

(define-public crate-dummy-queue-1.0.1 (c (n "dummy-queue") (v "1.0.1") (h "03mp3f3ga0rh8s8xcr46x2kg1xajdyq6p7qcap8qd5wm8ihcwdbs")))

(define-public crate-dummy-queue-1.1.0 (c (n "dummy-queue") (v "1.1.0") (h "007lb2wwx7dvapar8p36l222ginyxpb761bvh9rvacrmi3l19lmy")))

