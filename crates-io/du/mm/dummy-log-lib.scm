(define-module (crates-io du mm dummy-log-lib) #:use-module (crates-io))

(define-public crate-dummy-log-lib-0.1.0 (c (n "dummy-log-lib") (v "0.1.0") (h "0gilwxday80y2hdfsv3gsbwhnbg6d8kmqfc5m524rl9638jhvqb4")))

(define-public crate-dummy-log-lib-0.1.1 (c (n "dummy-log-lib") (v "0.1.1") (h "0b9cywqnba9hd5l8252z1cl1y9srrvpf8n0b61560m4iikh9621b")))

