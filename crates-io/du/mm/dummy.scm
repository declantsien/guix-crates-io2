(define-module (crates-io du mm dummy) #:use-module (crates-io))

(define-public crate-dummy-0.1.0 (c (n "dummy") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1a6pzkhfx0vk823fkzi5f2yf929kljyvxihfmq29wmxvgqpg86z8")))

(define-public crate-dummy-0.2.0 (c (n "dummy") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0kdm451yj0nh8ld8zqkapzizf45adsab6b6a0yyk5cx1ax4mgkr5")))

(define-public crate-dummy-0.2.1 (c (n "dummy") (v "0.2.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0l8b175lrz2wav87m66m529has7r0408dqibfrvmnw7jw228r1l9")))

(define-public crate-dummy-0.3.0 (c (n "dummy") (v "0.3.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "17v3i1arj10nzi2bi9c13m7n9i7nkl7qzq9mws2vq8cvmmkgk9im")))

(define-public crate-dummy-0.3.1 (c (n "dummy") (v "0.3.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00dfsf7a9jnxvlsawhwnkwkqjj5acdk18cv8whj75cybk99h4cw4")))

(define-public crate-dummy-0.3.2 (c (n "dummy") (v "0.3.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08m6a5di5syjsrk3hp7kmabw72zhvl1dlrvkyzqhlqi74qxha0jl")))

(define-public crate-dummy-0.3.3 (c (n "dummy") (v "0.3.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "054wnn5qbjn9rym9c011b2dgrwff53r3dnp9hmh92nkpsjbw7rh9")))

(define-public crate-dummy-0.3.4 (c (n "dummy") (v "0.3.4") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08q9pdbwd2wg03nlw4hdira5pdlay67mbmj3ild5kbphqmnk5jrq")))

(define-public crate-dummy-0.4.0 (c (n "dummy") (v "0.4.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "141r8ik97b6mvi1qw13jzs2gp5awjik1np4bs7n9pgjil2xs2i04")))

(define-public crate-dummy-0.4.1 (c (n "dummy") (v "0.4.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wr27ffsjb0nzj892v09qwzqyg9vj2572mv0dsxhn2qmfipzzxav")))

(define-public crate-dummy-0.5.0 (c (n "dummy") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1khr3awrxbkv7j3pdqc9ms5v3m5s12dm5l1zw18vnwfflvd09acd")))

(define-public crate-dummy-0.6.0 (c (n "dummy") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0591bqkdl2y7kz3bfz8yfzz81ix3pz8hnavs24pirh4kbnhki8rb")))

(define-public crate-dummy-0.7.0 (c (n "dummy") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xi8dwqi1qnqislf281ln7yrc9hj1ybb7qh1dr8sszz5d4my2mvy")))

