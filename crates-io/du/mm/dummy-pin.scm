(define-module (crates-io du mm dummy-pin) #:use-module (crates-io))

(define-public crate-dummy-pin-0.1.0 (c (n "dummy-pin") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 0)))) (h "12carg06y6cdh9ijn5rcbbjsbvvx718swa8af3m85cvjcjwwxain")))

(define-public crate-dummy-pin-0.1.1 (c (n "dummy-pin") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0j62s7gck0b5ngb7290ibzy2141iqprk8p02cihg8k3v2k8ri8rm")))

(define-public crate-dummy-pin-0.2.0-alpha.1 (c (n "dummy-pin") (v "0.2.0-alpha.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "=0.4.0-alpha.3") (d #t) (k 2)))) (h "1xkw61fx4mw9njbcmc7w26617n8f7az9if31rl3v0909hgwfzg88")))

(define-public crate-dummy-pin-1.0.0 (c (n "dummy-pin") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "0lm2kcppqzb3vx18pk3assnl31s7ghhgz82s233banxi5k6a8f0c") (r "1.62.0")))

