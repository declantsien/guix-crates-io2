(define-module (crates-io du mm dummy-waker) #:use-module (crates-io))

(define-public crate-dummy-waker-1.0.0 (c (n "dummy-waker") (v "1.0.0") (h "018kchx65n38ygm985qksn6xvd2bbfxs5gfcixgh8pxnrfc8a273")))

(define-public crate-dummy-waker-1.1.0 (c (n "dummy-waker") (v "1.1.0") (h "0pkfblnjax0cwgrda141vhka471pilv8kish11s0av11fcnng9lf")))

