(define-module (crates-io du mm dummy-transitive) #:use-module (crates-io))

(define-public crate-dummy-transitive-0.3.0 (c (n "dummy-transitive") (v "0.3.0") (h "0hagsgqn0s5wg5pkzidi6rgly709inl71y0n7q8q2vhl3c1aa0f3")))

(define-public crate-dummy-transitive-0.4.0 (c (n "dummy-transitive") (v "0.4.0") (h "08c4ls8hksbzkx5bg74g8d91pmi52a03c64z2y25dqmnsyd19nn7") (r "1.58.1")))

