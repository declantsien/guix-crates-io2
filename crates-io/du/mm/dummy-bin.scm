(define-module (crates-io du mm dummy-bin) #:use-module (crates-io))

(define-public crate-dummy-bin-0.1.0 (c (n "dummy-bin") (v "0.1.0") (h "1ga3ipfjwj2xq9jgav2j8sczycip6gy81x6n7r1qzdn2nhwqk87w")))

(define-public crate-dummy-bin-0.2.0 (c (n "dummy-bin") (v "0.2.0") (d (list (d (n "dummy-lib") (r "^0.2") (d #t) (k 0)))) (h "158qm5wycsnx1r5qsf12gkf16ns247pcx9b64pm9w59s5f0c6910")))

(define-public crate-dummy-bin-0.3.0 (c (n "dummy-bin") (v "0.3.0") (d (list (d (n "dummy-lib") (r "^0.3") (d #t) (k 0)))) (h "094kibm7lp072q4gxzgls8vxp6jjis8g95jrzgb6bgsq6cylnzna")))

(define-public crate-dummy-bin-0.4.0 (c (n "dummy-bin") (v "0.4.0") (d (list (d (n "dummy-lib") (r "^0.4") (d #t) (k 0)))) (h "029g2rakw06xy9jszgv5bkgnnn8qdd288xdqsnhdrshj0wp0mslm") (r "1.58.1")))

