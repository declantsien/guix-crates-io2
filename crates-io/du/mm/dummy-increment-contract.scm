(define-module (crates-io du mm dummy-increment-contract) #:use-module (crates-io))

(define-public crate-dummy-increment-contract-0.0.0 (c (n "dummy-increment-contract") (v "0.0.0") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1nmi36a7g0ics357l2s0wpiv0b02j21c5kk4jzvzks6zwlwdxp7z")))

(define-public crate-dummy-increment-contract-0.0.1 (c (n "dummy-increment-contract") (v "0.0.1") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "06vw2chxx471680a07xx17mgry18z8wrhy1iipmyr9v61db6x897")))

