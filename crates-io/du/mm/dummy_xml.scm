(define-module (crates-io du mm dummy_xml) #:use-module (crates-io))

(define-public crate-dummy_xml-0.1.0 (c (n "dummy_xml") (v "0.1.0") (h "0hbvsv5zgbxjgiv3xil5g4yn9dspbziljwk88ks053bn3nzlaskj")))

(define-public crate-dummy_xml-0.1.1 (c (n "dummy_xml") (v "0.1.1") (h "01byqkjn00s23y15np42chlzz3ysyyxfbrzdfg1liqygdlcr5sqg")))

(define-public crate-dummy_xml-0.1.2 (c (n "dummy_xml") (v "0.1.2") (h "0l9mb1npd56w8gj1x930a4207jm8q5nvyrpzgvx52r5iz2sbzm6y")))

(define-public crate-dummy_xml-0.1.3 (c (n "dummy_xml") (v "0.1.3") (h "1jl0sf1mikq725dycdgji4xmpwrc0w1zr27zh481qdwaq3xw2rzp")))

(define-public crate-dummy_xml-0.1.4 (c (n "dummy_xml") (v "0.1.4") (h "14wkp151y7273p27f5m8ll9fxxlk4ahm253d8yajwjxf3nddb0i8")))

(define-public crate-dummy_xml-0.1.5 (c (n "dummy_xml") (v "0.1.5") (h "183p8464jmpr31xrklfx1pihwfipi93xv3h3a024jm51c37nqh9c")))

(define-public crate-dummy_xml-0.1.6 (c (n "dummy_xml") (v "0.1.6") (h "1l3rkb3vs8qzcn28y27xl04wmmcbl6pasc7allbm2606giyaz648")))

