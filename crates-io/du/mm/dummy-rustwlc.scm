(define-module (crates-io du mm dummy-rustwlc) #:use-module (crates-io))

(define-public crate-dummy-rustwlc-0.3.1 (c (n "dummy-rustwlc") (v "0.3.1") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0v5mqil42k8djjcvvg8wr34q002q7mja465fyvcza005dqhfd90b")))

(define-public crate-dummy-rustwlc-0.3.2 (c (n "dummy-rustwlc") (v "0.3.2") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "141izvc5w2xin99npnzi0z3r30xnpw7blvis1jzz30138fy2ib2x")))

(define-public crate-dummy-rustwlc-0.3.3 (c (n "dummy-rustwlc") (v "0.3.3") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0b30bwp4ipnmnivnsnf1zfm52x61a56df9fdgyhz7fql7z0sy8a7")))

(define-public crate-dummy-rustwlc-0.4.0 (c (n "dummy-rustwlc") (v "0.4.0") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0xmwmd5a0cm0a3id80k98j6xw5p8hhwjs9rvh4cps74q19pranw4")))

(define-public crate-dummy-rustwlc-0.4.1 (c (n "dummy-rustwlc") (v "0.4.1") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "097ima65w36mdbdslziqg3jxdx84r7pfvxq4vhhiz1r17hmb951w")))

(define-public crate-dummy-rustwlc-0.5.1 (c (n "dummy-rustwlc") (v "0.5.1") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0kq4sws3ivl044i4jq31axd0cnvf7jj0jf4bh9v2m7jmc2arnhxc")))

(define-public crate-dummy-rustwlc-0.4.2 (c (n "dummy-rustwlc") (v "0.4.2") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "18nxpyj35s1pp3ca1pw7b4hpms6zm2cww9mg6vhcll5zbgrbglcr")))

(define-public crate-dummy-rustwlc-0.4.3 (c (n "dummy-rustwlc") (v "0.4.3") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0b5vzszjsymmbqfzn3k64axkhxc2ylfwbjxlw2zc01wl8s50xzc0")))

(define-public crate-dummy-rustwlc-0.4.4 (c (n "dummy-rustwlc") (v "0.4.4") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1gpgrzkmdgj0a6i3dzcvs655jvi39iak68d2s10vm4g6fpjwkv80")))

(define-public crate-dummy-rustwlc-0.6.0 (c (n "dummy-rustwlc") (v "0.6.0") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "16yxyl8as8nhwqvnxlla67nqq976xa34flpy8sga5gr043grp11c")))

(define-public crate-dummy-rustwlc-0.6.1 (c (n "dummy-rustwlc") (v "0.6.1") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0lwqynxbmn9l58kmgsnpg6ayxvsbs84zix14qbhjgji8ls9i2ivr")))

(define-public crate-dummy-rustwlc-0.6.2 (c (n "dummy-rustwlc") (v "0.6.2") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0bkm81wacfvj1d0hc3gfyn0bx720hzql3hkg4c6n931xanmkp8w9")))

(define-public crate-dummy-rustwlc-0.6.3 (c (n "dummy-rustwlc") (v "0.6.3") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.1") (f (quote ("server" "dlopen"))) (d #t) (k 0)))) (h "03a1va6ibm07p89clnmk82mp8xn5002z0i192aliq26pifki3p0i")))

(define-public crate-dummy-rustwlc-0.6.4 (c (n "dummy-rustwlc") (v "0.6.4") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.1") (f (quote ("server" "dlopen"))) (d #t) (k 0)))) (h "1qynnks9rbkawrmn0fa5gylhf2i1rscdlqn1m0rvmh0l0ca5ypc9")))

(define-public crate-dummy-rustwlc-0.6.5 (c (n "dummy-rustwlc") (v "0.6.5") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.1") (f (quote ("server" "dlopen"))) (d #t) (k 0)))) (h "11namwvxdnf60plwd0ynfsd3rvn8c3w40ihdlywc304n2nwnkk02")))

(define-public crate-dummy-rustwlc-0.7.0 (c (n "dummy-rustwlc") (v "0.7.0") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.1") (f (quote ("server" "dlopen"))) (d #t) (k 0)))) (h "116wzdb0ivgqsj35pzgls2sgl49vafphf7xzib7p85gwglzarl0i")))

(define-public crate-dummy-rustwlc-0.7.1 (c (n "dummy-rustwlc") (v "0.7.1") (d (list (d (n "bitflags") (r "0.6.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.1") (f (quote ("server" "dlopen"))) (d #t) (k 0)))) (h "1c1cnicfwd4h1d16w9j4pfqvpmwh61y2xf8z1qy5m2m9r211rzqd")))

