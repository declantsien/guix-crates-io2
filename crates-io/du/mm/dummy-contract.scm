(define-module (crates-io du mm dummy-contract) #:use-module (crates-io))

(define-public crate-dummy-contract-0.0.0 (c (n "dummy-contract") (v "0.0.0") (d (list (d (n "num-integer") (r "^0.1.45") (f (quote ("i128"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0vjl5dw4j4pdmpl699ibqryp6m9jvscx1x70hcrv06n1cqad2b0w")))

(define-public crate-dummy-contract-0.0.1 (c (n "dummy-contract") (v "0.0.1") (h "0is4yhfwm36shlhlnxry2vlj8zdwl4lncjgi4bmg6va066rhc010")))

(define-public crate-dummy-contract-0.0.2 (c (n "dummy-contract") (v "0.0.2") (h "1k341ng4a30nirk81dm857xhryxjs2fyrnjhqq3nmxfld6an8gnx")))

(define-public crate-dummy-contract-0.0.3 (c (n "dummy-contract") (v "0.0.3") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "110115rd2w032fqra2yn4flm9ygdp41iggjmz7sdzgg9m27h5v0w")))

(define-public crate-dummy-contract-0.0.4 (c (n "dummy-contract") (v "0.0.4") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1hrn8blplxgz88k9ism0aan129rmkybp05kg93yrzv3196y3q7ny")))

(define-public crate-dummy-contract-0.0.5 (c (n "dummy-contract") (v "0.0.5") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0lvqvxlx2fkmhbpcmpr80r2vmq7m7py7bxxgw9p0nydmwagyxxhf")))

(define-public crate-dummy-contract-0.0.6 (c (n "dummy-contract") (v "0.0.6") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0bx9wj9y226pxxqkgvznxbqkkk7gd2mk2ax4fvw85pd3faasx7wi")))

(define-public crate-dummy-contract-0.0.7 (c (n "dummy-contract") (v "0.0.7") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1v0a921lk0ws59vvjisl7k6v3csww9j2i22pgqah97bmx0iq0w9c")))

(define-public crate-dummy-contract-0.0.8 (c (n "dummy-contract") (v "0.0.8") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "16bga8masfcbz4fr1i1yj1qw8g305c2g9k9lzx2msywr9bykr63s")))

(define-public crate-dummy-contract-0.0.9 (c (n "dummy-contract") (v "0.0.9") (d (list (d (n "soroban-sdk") (r "^20.0.0-rc2") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0-rc2") (f (quote ("testutils"))) (d #t) (k 2)))) (h "0xnhxza46nzrz42l7lawasm8iq6w444v1bndw60jisdw1q938p30")))

