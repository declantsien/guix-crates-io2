(define-module (crates-io du mm dummy-anchor-serialize) #:use-module (crates-io))

(define-public crate-dummy-anchor-serialize-0.1.0 (c (n "dummy-anchor-serialize") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.27") (d #t) (k 0)))) (h "0vd31xky8fgac64kz2z2arwbgh0dl2kfv5caxrzdlrg6650h5i6d")))

(define-public crate-dummy-anchor-serialize-0.1.1 (c (n "dummy-anchor-serialize") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.27") (d #t) (k 0)))) (h "10anqqmvhs7pmv934sc28pbsm5jajyhzadphnxav5i2s8cbn8jlz")))

(define-public crate-dummy-anchor-serialize-0.1.2 (c (n "dummy-anchor-serialize") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.27") (d #t) (k 0)))) (h "0dvp7vjcyf60sbwndm6f62z13vzqhf2gmggln5cqsgp724kxnc7l")))

