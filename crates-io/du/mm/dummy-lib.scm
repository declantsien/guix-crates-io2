(define-module (crates-io du mm dummy-lib) #:use-module (crates-io))

(define-public crate-dummy-lib-0.2.0 (c (n "dummy-lib") (v "0.2.0") (h "0hr39mmkh8n8srf424ncnmagbr9ms5lw28bwka0fdwrvlz8xfx02")))

(define-public crate-dummy-lib-0.3.0 (c (n "dummy-lib") (v "0.3.0") (h "0wcs85n75zldf4kmb0ycm1iihwmb7gsq3xjmak0ks9j5r1ah2fmi")))

(define-public crate-dummy-lib-0.4.0 (c (n "dummy-lib") (v "0.4.0") (h "004zh7kz1rzckljs4fzwspav0c6kj4alsq3xxlyf4d7s1hg7vgp1") (r "1.58.1")))

