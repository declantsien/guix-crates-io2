(define-module (crates-io du kt dukt-sys) #:use-module (crates-io))

(define-public crate-dukt-sys-0.1.0 (c (n "dukt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "093rd6hyrnymcw9mg91a8ndvwnm38q7f24kp23i0yhp10sqawrm7")))

