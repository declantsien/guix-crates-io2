(define-module (crates-io du kt duktape_ffi_raw) #:use-module (crates-io))

(define-public crate-duktape_ffi_raw-2.3.0 (c (n "duktape_ffi_raw") (v "2.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07dy0d40d721py9qrhvcw5ryka16ivywh1pqnw6a31v5jxpssa1v") (l "duktape")))

(define-public crate-duktape_ffi_raw-2.30.0 (c (n "duktape_ffi_raw") (v "2.30.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "113dblwzy102f1c87kgqfq89c9kgs3c7wbrhhjljx32c8j3792mi") (l "duktape")))

