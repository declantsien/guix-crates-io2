(define-module (crates-io du kt duktape_sys) #:use-module (crates-io))

(define-public crate-duktape_sys-0.0.1 (c (n "duktape_sys") (v "0.0.1") (d (list (d (n "gcc") (r "~0.0.1") (d #t) (k 1)))) (h "1qr07fqy1q4ihapapnws492mwkfiw3ri92w7hqyw5qz5y54jli25") (y #t)))

(define-public crate-duktape_sys-0.0.2 (c (n "duktape_sys") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0fjp7r5rnhb1ia602cph6z7jnchcxz5vjrzd9vs88wv3b6k9vpfl") (y #t)))

