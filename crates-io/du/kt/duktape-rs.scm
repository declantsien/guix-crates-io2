(define-module (crates-io du kt duktape-rs) #:use-module (crates-io))

(define-public crate-duktape-rs-0.0.1 (c (n "duktape-rs") (v "0.0.1") (d (list (d (n "dukbind") (r "^0.0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)))) (h "0dbymb5hqinhf209ikd5sgary57vm1pdrazcywlzijg8cnshs36h")))

(define-public crate-duktape-rs-0.0.2 (c (n "duktape-rs") (v "0.0.2") (d (list (d (n "dukbind") (r "^0.0.4") (d #t) (k 0)))) (h "089wkliz85mimwzg680qmj3khpirlmwwv3dnxfwd55kky29qw7gs")))

(define-public crate-duktape-rs-0.0.3 (c (n "duktape-rs") (v "0.0.3") (d (list (d (n "dukbind") (r "^0.0.4") (d #t) (k 0)))) (h "0qqg0kqpwynszi2gzwjqz7k2c24ajyg568vf81h17kv2rw2pmizb")))

(define-public crate-duktape-rs-0.0.4 (c (n "duktape-rs") (v "0.0.4") (d (list (d (n "dukbind") (r "^0.0.4") (d #t) (k 0)))) (h "011ypgn58xkkzdn39z5y6wv90kvy5rbyyb9z8dn92s27rwm5gb76")))

