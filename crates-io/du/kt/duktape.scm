(define-module (crates-io du kt duktape) #:use-module (crates-io))

(define-public crate-duktape-0.0.1 (c (n "duktape") (v "0.0.1") (d (list (d (n "duktape_sys") (r "*") (d #t) (k 0)))) (h "1qcrhb4ax6galsd39fb35jmqjk27q5kiq2ih4215ga3x1cnycc7s")))

(define-public crate-duktape-0.0.2 (c (n "duktape") (v "0.0.2") (d (list (d (n "abort_on_panic") (r "*") (d #t) (k 0)) (d (n "cesu8") (r "*") (d #t) (k 0)) (d (n "duktape_sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1r5cjp9x6inns5gfaq39f0xf8f78z6wj8blqx9grdllcjr45npgx")))

