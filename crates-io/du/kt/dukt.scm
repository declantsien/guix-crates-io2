(define-module (crates-io du kt dukt) #:use-module (crates-io))

(define-public crate-dukt-0.1.0 (c (n "dukt") (v "0.1.0") (d (list (d (n "dukt-macros") (r "^0.1") (d #t) (k 0)) (d (n "dukt-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "075xkiffz1d5mx5awj16if02pqbk77lqvxxwl19ly0p9jid5908j")))

