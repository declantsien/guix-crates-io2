(define-module (crates-io du pc dupchecker) #:use-module (crates-io))

(define-public crate-dupchecker-0.1.0 (c (n "dupchecker") (v "0.1.0") (h "0jnjh5ww4la0f91qrxv93wxkfvzji08gss2nf84r5iqrbhp4xabl")))

(define-public crate-dupchecker-0.2.0 (c (n "dupchecker") (v "0.2.0") (h "1cdphipwh7hb6q1iw3l49ijbnwfjg0alcyihjyvbypslyabfpiaw")))

(define-public crate-dupchecker-0.2.1 (c (n "dupchecker") (v "0.2.1") (h "1v183zn8iw9z6sxfhgg1830xbp7arn6aw4awlnvcq0kh0papn1s4")))

(define-public crate-dupchecker-0.3.0 (c (n "dupchecker") (v "0.3.0") (h "1g6153nh1iwxp2yxar7sz96w4mb9ndza7x0zvzq5irjvyvn6lii0")))

(define-public crate-dupchecker-0.4.0 (c (n "dupchecker") (v "0.4.0") (h "1v29z8w5r3kb7pfj6kkcx6f448v2rfx85bp08f2y3bmfnmlwhnr1")))

(define-public crate-dupchecker-0.4.1 (c (n "dupchecker") (v "0.4.1") (h "1sn83vp64pbsbj3smy5lm6ghsy82myn7r0zcbyxg1if10pv2n3sv")))

