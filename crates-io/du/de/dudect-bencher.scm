(define-module (crates-io du de dudect-bencher) #:use-module (crates-io))

(define-public crate-dudect-bencher-0.1.0 (c (n "dudect-bencher") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "take_mut") (r "^0.1") (d #t) (k 0)))) (h "076vflwhyx41qh5172f1kdg3ln8x8nk2fqbzbgpk7v0ya4prm3b7")))

(define-public crate-dudect-bencher-0.2.0 (c (n "dudect-bencher") (v "0.2.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "take_mut") (r "^0.1") (d #t) (k 0)))) (h "05x1zlqmkh9zxd0iar2q9crcc7nz4cw0j97jrzvswv969pbn28cs")))

(define-public crate-dudect-bencher-0.2.1 (c (n "dudect-bencher") (v "0.2.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "07pmsjdpi7wac7aypaj3h6w5yhbh6nfxglfzn9j593kixiqrn1fs")))

(define-public crate-dudect-bencher-0.3.0 (c (n "dudect-bencher") (v "0.3.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0fj54hrxp94sj0l3g4nq3cmq9bdpw73caq30lbabw5iwgn9j9xji")))

(define-public crate-dudect-bencher-0.3.1 (c (n "dudect-bencher") (v "0.3.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1") (d #t) (k 0)))) (h "05q1g0xagdppwsj0jaa95yg94vqwy4whxaw34n71w1i273pw9hca") (y #t)))

(define-public crate-dudect-bencher-0.4.0 (c (n "dudect-bencher") (v "0.4.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1") (d #t) (k 0)))) (h "126pydi3919kb38prd9www40d6c25fld6xr3hgmd3yvm1nihzh9a")))

(define-public crate-dudect-bencher-0.4.1 (c (n "dudect-bencher") (v "0.4.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 0)))) (h "1rpaqm8f16yzsd2c0immgw3xxi9caajpck7bb7jddcdf8w6zdvdn")))

(define-public crate-dudect-bencher-0.5.0 (c (n "dudect-bencher") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)))) (h "0qwp7xpj29rjf1japbs4sa22znlg4w0dbdc98q1izb0zxjvvpyb2")))

(define-public crate-dudect-bencher-0.6.0 (c (n "dudect-bencher") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ctrlc") (r "^3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)))) (h "0f1xnzs5mf4j6dm2j68p8dxwx2gmqy9gd0gy2d1rpzn4xbhnbl5m") (f (quote (("default" "core-hint-black-box") ("core-hint-black-box"))))))

