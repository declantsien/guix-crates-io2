(define-module (crates-io du ng dunge_shader) #:use-module (crates-io))

(define-public crate-dunge_shader-0.2.0 (c (n "dunge_shader") (v "0.2.0") (h "0mq306xda9d4s18gp14b87rshbmnfl72p8zr71a5ssnxva60d14j")))

(define-public crate-dunge_shader-0.2.1 (c (n "dunge_shader") (v "0.2.1") (h "1jvbwas9pk5r56fwn7lsgb7crvzrwk8rk5ibzbvp48xij5ysfvvq")))

(define-public crate-dunge_shader-0.2.2 (c (n "dunge_shader") (v "0.2.2") (h "044d5nd3wp03xwphb2shaw3xw32dh69bmhxpbsy270wikw7wafbw")))

(define-public crate-dunge_shader-0.3.0-alpha (c (n "dunge_shader") (v "0.3.0-alpha") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.14") (d #t) (k 0)))) (h "163405dn23lqlw2wzaans1f9zbg5q0qmbnj8abvmzk5j07raa6y8")))

(define-public crate-dunge_shader-0.3.0-alpha.1 (c (n "dunge_shader") (v "0.3.0-alpha.1") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.19") (d #t) (k 0)))) (h "1jhhq7kml9wjv6xr7wm9i3p6w4pn7bnpfcymcj76gnyjlcms7fl4") (r "1.75")))

(define-public crate-dunge_shader-0.3.0-rc (c (n "dunge_shader") (v "0.3.0-rc") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.19") (d #t) (k 0)))) (h "10f0w4dyp5fchzimbf473c7lfzlz1d6sb2pvp8aq8j5x389l7pcq") (f (quote (("wgsl" "naga/wgsl-out")))) (r "1.76")))

(define-public crate-dunge_shader-0.3.0-rc.1 (c (n "dunge_shader") (v "0.3.0-rc.1") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.19") (d #t) (k 0)))) (h "1cwnmbhj1w1fy4x376p254wib9pm09bjm6l7an43zc7zgj5gzvpq") (f (quote (("wgsl" "naga/wgsl-out")))) (r "1.76")))

(define-public crate-dunge_shader-0.3.0 (c (n "dunge_shader") (v "0.3.0") (d (list (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.19") (d #t) (k 0)))) (h "0hzj0zqcaschih1zvi2h1vj7630apqmlwnfw5fgblk70lkfzvzx5") (f (quote (("wgsl" "naga/wgsl-out")))) (r "1.76")))

(define-public crate-dunge_shader-0.3.1 (c (n "dunge_shader") (v "0.3.1") (d (list (d (n "glam") (r "^0.27") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naga") (r "^0.19") (d #t) (k 0)))) (h "1zc90mylp2mv0xqhaks692i6zgxkal78pvkqjgpy4hsng2f47gwc") (f (quote (("wgsl" "naga/wgsl-out")))) (r "1.77")))

