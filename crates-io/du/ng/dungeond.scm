(define-module (crates-io du ng dungeond) #:use-module (crates-io))

(define-public crate-dungeond-0.1.0 (c (n "dungeond") (v "0.1.0") (d (list (d (n "pledge") (r "^0.3.1") (d #t) (k 0)) (d (n "unveil") (r "^0.2.0") (d #t) (k 0)))) (h "05f4jrwhns35xp411ddg6wq0n5dnvw6aii79g4988z95mn6zg3bk")))

