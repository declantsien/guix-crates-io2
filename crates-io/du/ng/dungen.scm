(define-module (crates-io du ng dungen) #:use-module (crates-io))

(define-public crate-dungen-0.1.1 (c (n "dungen") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1cmwjfnmw0ygxcl9p45jkfr29m2k6cwwfa8pdicaddyn6ngx45i2")))

(define-public crate-dungen-0.1.2 (c (n "dungen") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0lbnkhagwmzdlhx9j6xc8nm8qqy022lnxcsbv6l7szy349qjawy2")))

