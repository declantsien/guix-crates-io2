(define-module (crates-io du ng dunge_macros) #:use-module (crates-io))

(define-public crate-dunge_macros-0.2.0 (c (n "dunge_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05c8qr1fvcivfsl8gbbncgz3hn9xj4gnllm0wwfh1srd2x131abs")))

(define-public crate-dunge_macros-0.2.1 (c (n "dunge_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wlbni4p4ilacgsidxgf5vm21jy4qa1v5bn98l0jc1wvl1y54dkd")))

(define-public crate-dunge_macros-0.2.2 (c (n "dunge_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03brhh8nrzan5n6rjz3jww2bxz7j3d43f57np6ah14fhrx9gjzfy")))

(define-public crate-dunge_macros-0.3.0-alpha (c (n "dunge_macros") (v "0.3.0-alpha") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "18h46mvayj4zvg58h88hd7pvk9i8m4w5xzhdmjwh5s8gjxh5v2bx")))

(define-public crate-dunge_macros-0.3.0-alpha.1 (c (n "dunge_macros") (v "0.3.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1klzvyc90pmmjq18l8grz0dc4hgn4a4pg8s7pmw5955f28samy94") (r "1.75")))

(define-public crate-dunge_macros-0.3.0-rc (c (n "dunge_macros") (v "0.3.0-rc") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16nfzs8488shyq2p6jk96xasl2a5wrjrflxn4xcdhala2n84f4i5") (r "1.76")))

(define-public crate-dunge_macros-0.3.0-rc.1 (c (n "dunge_macros") (v "0.3.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13ywwnr0nzqdajdlmzi5ydyhdcsc33hp5a6ix1q1zkcsk05vkl0n") (r "1.76")))

(define-public crate-dunge_macros-0.3.0 (c (n "dunge_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "06qjwwyzygqgib05yy5g0hd07rwdh54ybb14x8s3qni5srz0gr62") (r "1.76")))

(define-public crate-dunge_macros-0.3.1 (c (n "dunge_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ia3j93k506sq9w3737fnniywp41yxd4401yhp1ncal1gixpz9pz") (r "1.77")))

