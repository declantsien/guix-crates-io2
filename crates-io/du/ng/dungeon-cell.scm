(define-module (crates-io du ng dungeon-cell) #:use-module (crates-io))

(define-public crate-dungeon-cell-0.0.1 (c (n "dungeon-cell") (v "0.0.1") (h "0qn8yfgafi0gy6k04gz576x303bgx4c6kqaczx0c6gw30v19hmmp") (f (quote (("assert_runtime"))))))

(define-public crate-dungeon-cell-0.0.2 (c (n "dungeon-cell") (v "0.0.2") (d (list (d (n "const_panic") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1gh6sc4s628gbb504q67x6gsmjy1k3czvi1r65dqxi0zr3h2kx88") (f (quote (("nightly") ("default" "compile_errors")))) (s 2) (e (quote (("compile_errors" "dep:const_panic"))))))

(define-public crate-dungeon-cell-0.0.3 (c (n "dungeon-cell") (v "0.0.3") (d (list (d (n "const_panic") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0265iqn907m55326qmf8028ikgyrqy1l8m3njpicbncps1il7c33") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-dungeon-cell-0.1.0 (c (n "dungeon-cell") (v "0.1.0") (d (list (d (n "const_panic") (r "^0.2.0") (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0bxzdcdp3hngqf7d6dd3s6sf6i504crs8wan36i7f4wvnci0mipi") (f (quote (("unsize") ("std" "alloc") ("many_arg_fn") ("default") ("alloc")))) (r "1.64.0")))

