(define-module (crates-io du ng dungen_minion_rooms) #:use-module (crates-io))

(define-public crate-dungen_minion_rooms-0.1.0 (c (n "dungen_minion_rooms") (v "0.1.0") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.0") (d #t) (k 0)))) (h "0k2hcwf3y8hm3chx09iq6bdvsc4xfa3050wb1ya0c0a16a3d5sn3") (y #t)))

(define-public crate-dungen_minion_rooms-0.1.1 (c (n "dungen_minion_rooms") (v "0.1.1") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.0") (d #t) (k 0)))) (h "156jdjss59qwgmfyy1apgpydzp0w0wig0vhm6s8ixd0k3fn6rj4h") (y #t)))

(define-public crate-dungen_minion_rooms-0.1.2 (c (n "dungen_minion_rooms") (v "0.1.2") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.3") (d #t) (k 0)))) (h "1v4l9in153bcdaipk0r1xx50kjsg5hc251f48dk84ay84c9rzd50") (y #t)))

(define-public crate-dungen_minion_rooms-0.1.3 (c (n "dungen_minion_rooms") (v "0.1.3") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.4") (d #t) (k 0)))) (h "0w9gqr0lf1xwrf54l6qglkn907p3hhsgld9qrnqm46ip2bhaasdd") (y #t)))

(define-public crate-dungen_minion_rooms-0.1.4 (c (n "dungen_minion_rooms") (v "0.1.4") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.5") (d #t) (k 0)))) (h "0mq6ic4rlwifg10fl48ljj1p7019xkw67x773fcchh70ih917ys2") (y #t)))

(define-public crate-dungen_minion_rooms-0.1.5 (c (n "dungen_minion_rooms") (v "0.1.5") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.1.6") (d #t) (k 0)))) (h "0pb5x0mpqvivh2ndj7ivmxr20rvidqd8lxf0zxpnhx4x6033ygc6") (y #t)))

(define-public crate-dungen_minion_rooms-0.2.0 (c (n "dungen_minion_rooms") (v "0.2.0") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.2.0") (d #t) (k 0)))) (h "04xj9mkh2gnfv5dkplxdn4hm70f25wvxpq9cff43f4xrqq277f2f") (y #t)))

(define-public crate-dungen_minion_rooms-0.2.1 (c (n "dungen_minion_rooms") (v "0.2.1") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.2.1") (d #t) (k 0)))) (h "1y0p0imswxnys72qzfqjn478f7x1nk23m6gmgfnb8vfjfl4n7zbm") (y #t)))

(define-public crate-dungen_minion_rooms-0.2.2 (c (n "dungen_minion_rooms") (v "0.2.2") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.2.2") (d #t) (k 0)))) (h "1n2566v0gb1wnrxi5kdfjr173xwq0mkvp456r2ic9arniy3zabac") (y #t)))

(define-public crate-dungen_minion_rooms-0.2.3 (c (n "dungen_minion_rooms") (v "0.2.3") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.2.3") (d #t) (k 0)))) (h "0akmwvp2mi9233w1q8072294b0abzncgm892g3ycgvym4paxzmi0") (y #t)))

(define-public crate-dungen_minion_rooms-0.3.0 (c (n "dungen_minion_rooms") (v "0.3.0") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.3.0") (d #t) (k 0)))) (h "18si3l88vma29jhzy2z54h9fm0hwlm2j7z2ycg03f4f2rrm77jg8") (y #t)))

(define-public crate-dungen_minion_rooms-0.3.1 (c (n "dungen_minion_rooms") (v "0.3.1") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.3.1") (d #t) (k 0)))) (h "0k14aaja5zbwdhhqdpqli68kqkh718p8fz6z0fhkbygbmqjgq36j") (f (quote (("strict")))) (y #t)))

(define-public crate-dungen_minion_rooms-0.3.2 (c (n "dungen_minion_rooms") (v "0.3.2") (d (list (d (n "dungen_minion_rooms_abstract") (r "^0.3.2") (d #t) (k 0)))) (h "19qp5di9380js85n12vdjs0pfbydax35hccpjkbg1h2q8sdvdmih") (f (quote (("strict")))) (y #t)))

