(define-module (crates-io du ng dungen_minion_geometry) #:use-module (crates-io))

(define-public crate-dungen_minion_geometry-0.1.0 (c (n "dungen_minion_geometry") (v "0.1.0") (h "0ppazmajmwv4kbvrw5f1px6r9asgp25hzgdiwkhjrfpj40x72bwm") (y #t)))

(define-public crate-dungen_minion_geometry-0.1.1 (c (n "dungen_minion_geometry") (v "0.1.1") (h "0481ahx1r4vbma068hx18pnj6l5h42kcl617h9ak7d0nb2pdxrw6") (y #t)))

(define-public crate-dungen_minion_geometry-0.1.2 (c (n "dungen_minion_geometry") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "0fcfn3qjrwfb9dzyvicg0agk6cwc6gdzzfqf4mhvmh0x13p13ipw") (y #t)))

(define-public crate-dungen_minion_geometry-0.1.3 (c (n "dungen_minion_geometry") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)))) (h "1fz6bmkrm5x3kl5fkqqxz5gvljyq9gazk1jqfvy98v1slb1pr22i") (y #t)))

(define-public crate-dungen_minion_geometry-0.2.0 (c (n "dungen_minion_geometry") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00gcc4b2qplpqsks777mwfyk03via9c7rf5dkyky9ajvi23ppdh7") (y #t)))

(define-public crate-dungen_minion_geometry-0.2.1 (c (n "dungen_minion_geometry") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vn3hga8chfv6rgac4h23qllcmb9pk5f4ddgk45j4y7zb2sq1wkv") (y #t)))

(define-public crate-dungen_minion_geometry-0.2.2 (c (n "dungen_minion_geometry") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0y2sz0xh6r7sd380l56n89kv70gi8kai4wacxplrymm7c288l4hw") (y #t)))

(define-public crate-dungen_minion_geometry-0.2.3 (c (n "dungen_minion_geometry") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1vrppf752kn33vlscy5vizr60gj0x8p99zqbiqsn37pp7hl78w3v") (y #t)))

(define-public crate-dungen_minion_geometry-0.3.0 (c (n "dungen_minion_geometry") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1rd52s0wcc9n9w29p21simmispflfa705ss0sz2zshfmkxgb6x7r") (y #t)))

(define-public crate-dungen_minion_geometry-0.3.1 (c (n "dungen_minion_geometry") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "01kp24cc04qwlkf4iqs2lxcsnaj9bx39vv7jpa9gina3ws7pwv39") (f (quote (("strict")))) (y #t)))

(define-public crate-dungen_minion_geometry-0.3.2 (c (n "dungen_minion_geometry") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1lwf1558szpflzrvxadmpdw66idwpnk1d91lng8rmimg43icihg2") (f (quote (("strict")))) (y #t)))

