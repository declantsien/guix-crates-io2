(define-module (crates-io du ng dungen_minion_rooms_abstract) #:use-module (crates-io))

(define-public crate-dungen_minion_rooms_abstract-0.1.0 (c (n "dungen_minion_rooms_abstract") (v "0.1.0") (d (list (d (n "dungen_minion_geometry") (r "^0.1.0") (d #t) (k 0)))) (h "00bnhy9r8l107mfnp733vj8sxc2x8gdkczckcvskmxz9q1674ciq") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.1 (c (n "dungen_minion_rooms_abstract") (v "0.1.1") (d (list (d (n "dungen_minion_geometry") (r "^0.1.0") (d #t) (k 0)))) (h "1917yxz6p7lhz0hj0k9fhi7i1sx1j81dxv0p6v0cdfx9j3aqszjg") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.2 (c (n "dungen_minion_rooms_abstract") (v "0.1.2") (d (list (d (n "dungen_minion_geometry") (r "^0.1.1") (d #t) (k 0)))) (h "02l99898ya9lllxcj8arjb88h8yzc709m53gkch3s49l77q6b4sa") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.3 (c (n "dungen_minion_rooms_abstract") (v "0.1.3") (d (list (d (n "dungen_minion_geometry") (r "^0.1.1") (d #t) (k 0)))) (h "1l5l6vqdc0hrirzqpygq2dhaayfbl7nknhszsvjgf9wq7xrgmkhl") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.4 (c (n "dungen_minion_rooms_abstract") (v "0.1.4") (d (list (d (n "dungen_minion_geometry") (r "^0.1.1") (d #t) (k 0)))) (h "0h9rn6y2nl6pa3wjk0zfxrj01qbsifyfishxn9pw9qahf3ds0ria") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.5 (c (n "dungen_minion_rooms_abstract") (v "0.1.5") (d (list (d (n "dungen_minion_geometry") (r "^0.1.2") (d #t) (k 0)))) (h "1n2i305d57dwikcia04k5plzcgny4gzb5sil08fdvmsqsrx7x1hw") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.1.6 (c (n "dungen_minion_rooms_abstract") (v "0.1.6") (d (list (d (n "dungen_minion_geometry") (r "^0.1.3") (d #t) (k 0)))) (h "0ii7nvkr6qv8dg5nwphx9mjk3qyhgll78mh5iaz28a14fv34r5p1") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.2.0 (c (n "dungen_minion_rooms_abstract") (v "0.2.0") (d (list (d (n "dungen_minion_geometry") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "0jqlphj7wkih5ywdpgk0yfr6yfypwlman2l2pb0xk2g7mb6m229x") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.2.1 (c (n "dungen_minion_rooms_abstract") (v "0.2.1") (d (list (d (n "dungen_minion_geometry") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "0dz7jis9raxcm7d38zjva9pqq7jdcj7r9lps1l81rnn122vr3g0p") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.2.2 (c (n "dungen_minion_rooms_abstract") (v "0.2.2") (d (list (d (n "dungen_minion_geometry") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "1979ahacmja5q3rfljnkskjzvbx937b5wgpm31adg82aicz89gg8") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.2.3 (c (n "dungen_minion_rooms_abstract") (v "0.2.3") (d (list (d (n "dungen_minion_geometry") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "0yccsvzlwqxnsgkn1inz6a7zzbp25bp2a46cznqfd42mgxrmxmh4") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.3.0 (c (n "dungen_minion_rooms_abstract") (v "0.3.0") (d (list (d (n "dungen_minion_geometry") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "1hbplkj4ibrxqimhckwqxr04zdhzlxfw91rizim9inriicy5xl9v") (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.3.1 (c (n "dungen_minion_rooms_abstract") (v "0.3.1") (d (list (d (n "dungen_minion_geometry") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0pg749y5izkzqibv5vdvdbkb5a6s9w7nmdk7gqhibp4h3fzg0b7l") (f (quote (("strict")))) (y #t)))

(define-public crate-dungen_minion_rooms_abstract-0.3.2 (c (n "dungen_minion_rooms_abstract") (v "0.3.2") (d (list (d (n "dungen_minion_geometry") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "15idqq1y1mcak8xlq1h08xwp6jg75wkqka0rqbw5rrx7dxp6iw61") (f (quote (("strict")))) (y #t)))

