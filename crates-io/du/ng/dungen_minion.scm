(define-module (crates-io du ng dungen_minion) #:use-module (crates-io))

(define-public crate-dungen_minion-0.1.0 (c (n "dungen_minion") (v "0.1.0") (d (list (d (n "dungen_minion_rooms") (r "^0.1.0") (d #t) (k 0)))) (h "13ddsfpj4zkqz1mkvr9cx7mprsnydxvp36l32h5ylcrp5ycq8id9") (y #t)))

(define-public crate-dungen_minion-0.1.1 (c (n "dungen_minion") (v "0.1.1") (d (list (d (n "dungen_minion_rooms") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0jd9f9clp6bcgajfgc02cwrq7nsp7y54vbx536i44yynvcg8ncxw") (y #t)))

(define-public crate-dungen_minion-0.1.3 (c (n "dungen_minion") (v "0.1.3") (d (list (d (n "dungen_minion_rooms") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0iqwpaljk4ahsnxki2ldyc04ydwjpi1p0nniw93n2g6xl8vmx474") (y #t)))

(define-public crate-dungen_minion-0.1.4 (c (n "dungen_minion") (v "0.1.4") (d (list (d (n "dungen_minion_rooms") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0kfc5gxkrwfmbgwrpfvaidp9d2iadyr2zr29jdgrb6y12qnqg8dy") (y #t)))

(define-public crate-dungen_minion-0.1.5 (c (n "dungen_minion") (v "0.1.5") (d (list (d (n "dungen_minion_rooms") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19bybfpyiri0c09xn4hk9d83qfgxpac2zjr7lf4rphsly3z6snnk") (y #t)))

(define-public crate-dungen_minion-0.1.6 (c (n "dungen_minion") (v "0.1.6") (d (list (d (n "dungen_minion_rooms") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0mif31s6xs59awqm2fz8r2bc93hw3v9wnpqlfm1mf6ipx2ll7xfy") (y #t)))

(define-public crate-dungen_minion-0.2.0 (c (n "dungen_minion") (v "0.2.0") (d (list (d (n "dungen_minion_rooms") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04znbyxffwbbhzdm49xkbal7xfp0yszl110hvfrcdrs4pyg6qvxc") (y #t)))

(define-public crate-dungen_minion-0.2.1 (c (n "dungen_minion") (v "0.2.1") (d (list (d (n "dungen_minion_rooms") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hw9p9kdc9w0kbl0kg2ylj3da9c8jyk61avwkicxp4cw19fjfx65") (y #t)))

(define-public crate-dungen_minion-0.2.2 (c (n "dungen_minion") (v "0.2.2") (d (list (d (n "dungen_minion_rooms") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0hx2pa35v41y3w9crdkqn4rjvv8xy90ms5vilp7ld6n0dr0r97b9") (y #t)))

(define-public crate-dungen_minion-0.2.3 (c (n "dungen_minion") (v "0.2.3") (d (list (d (n "dungen_minion_rooms") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1n78j1h212iss375r0dy4x4zd2jnr6hjqvx9c37vb3w0m8rbvj68") (y #t)))

(define-public crate-dungen_minion-0.3.0 (c (n "dungen_minion") (v "0.3.0") (d (list (d (n "dungen_minion_rooms") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0kpbd1sv13fbhsh5nvi6fhcjgkdnikmfa44qix8a1aahsrx55rc4") (y #t)))

(define-public crate-dungen_minion-0.3.1 (c (n "dungen_minion") (v "0.3.1") (d (list (d (n "dungen_minion_rooms") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1kgbd2mffq8i0b8vphi4j60klxk1mdjy85n0pa5d56a1i98fvsqv") (f (quote (("strict")))) (y #t)))

(define-public crate-dungen_minion-0.3.2 (c (n "dungen_minion") (v "0.3.2") (d (list (d (n "dungen_minion_rooms") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1wqw2g37jgxy55rg9aapybg61irqap2a5z2ry1d47bxmi04cg6ii") (f (quote (("strict")))) (y #t)))

