(define-module (crates-io du cc ducci) #:use-module (crates-io))

(define-public crate-ducci-1.0.0 (c (n "ducci") (v "1.0.0") (h "0hpv3q24bjf7qq213984l11aj70d7fzyn45xvvl5idg15jpn07r1")))

(define-public crate-ducci-1.1.0 (c (n "ducci") (v "1.1.0") (h "0al3jn5p3jihz77qlf8cbmw59rz1h9bglj52p8sviizjv4pw4f9g")))

(define-public crate-ducci-1.2.0 (c (n "ducci") (v "1.2.0") (h "1zg34wqwc8995k3a82q9srlvm7iynsl4l37bnkpav887f384j07c")))

