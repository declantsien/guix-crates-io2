(define-module (crates-io du cc ducc-sys) #:use-module (crates-io))

(define-public crate-ducc-sys-0.1.0 (c (n "ducc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06mk075l1qw5zgddagdswy3yjb1mb1rsw6bx24z77gq9k809060g") (f (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

(define-public crate-ducc-sys-0.1.1 (c (n "ducc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.36") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rkdgdn5wc4515pg10hjrm8gwk393g1qnl8ql8x3n6m1gqhx9946") (f (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

(define-public crate-ducc-sys-0.1.2 (c (n "ducc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.36") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vgzsjmylxldhzr19s0fzijh8pswd4249nvl4d9dw3vapwsaiphc") (f (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

