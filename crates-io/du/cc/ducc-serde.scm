(define-module (crates-io du cc ducc-serde) #:use-module (crates-io))

(define-public crate-ducc-serde-0.1.0 (c (n "ducc-serde") (v "0.1.0") (d (list (d (n "ducc") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0582vsr6spx2njg7bsd8dqxfdp7jckdf6cgkkzpvzk7whc35z607")))

