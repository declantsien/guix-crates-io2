(define-module (crates-io du cc ducc0) #:use-module (crates-io))

(define-public crate-ducc0-0.28.0 (c (n "ducc0") (v "0.28.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i4vi2p2dkfif5825k2zcfhq923hm6ic002cm0p55fd4najlkj8y") (y #t)))

(define-public crate-ducc0-0.30.0 (c (n "ducc0") (v "0.30.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndrustfft") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "1387cply47gh1y74g0pshhh2ba3m5fzflrclb882spz85gqv1r1f")))

(define-public crate-ducc0-0.30.1 (c (n "ducc0") (v "0.30.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndrustfft") (r "^0.3.0") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)))) (h "1n4zsvfz9h2n8vhjcij49d58psrmvsrfags803m772ayvq1k15hb")))

