(define-module (crates-io du cc ducc) #:use-module (crates-io))

(define-public crate-ducc-0.1.0 (c (n "ducc") (v "0.1.0") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1z8sh683dsyji3nwm6jczq7xkwfy2aq5h4mss2dlw6ib9knw20yh")))

(define-public crate-ducc-0.1.1 (c (n "ducc") (v "0.1.1") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.1") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)))) (h "0gg48chp4bf2fpyqsyxk6jy6w6yb5r51g0ygk4f9d39q90wjq2br")))

(define-public crate-ducc-0.1.2 (c (n "ducc") (v "0.1.2") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.2") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)))) (h "1h9snm92rnjm8czck460fwp04xa3gfyl909kmrag7q57ashks0qw")))

(define-public crate-ducc-0.1.3 (c (n "ducc") (v "0.1.3") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.2") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)))) (h "15l5vj4vp90n5mk20pkq1ixq2i68i8dnrqlh7zx700zxnpiiln6y")))

(define-public crate-ducc-0.1.4 (c (n "ducc") (v "0.1.4") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.2") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)))) (h "1a1ya1578gj67b6l3d65mzfa2xz3x10qrcsn02bkgi0nl07zqyhs")))

(define-public crate-ducc-0.1.5 (c (n "ducc") (v "0.1.5") (d (list (d (n "cesu8") (r "^1.1") (d #t) (k 0)) (d (n "ducc-sys") (r "^0.1.2") (f (quote ("use-exec-timeout-check"))) (d #t) (k 0)))) (h "01wjhbzxl62gihx25fvks50zp7cd45zp91ag8nkvcbki6251zg21")))

