(define-module (crates-io du tt dutty) #:use-module (crates-io))

(define-public crate-dutty-1.0.0 (c (n "dutty") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "16qg1lp869l09v7183bb1aks6ykqksx2gn7n7w0ymjhm84h31z8s")))

(define-public crate-dutty-1.0.1 (c (n "dutty") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0j30wq1m5b061kgi8x5ci8zj7sf4rvkqihbncbagsvw42yg0424l")))

(define-public crate-dutty-1.0.3 (c (n "dutty") (v "1.0.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)))) (h "1dazs6l4ixm4hallrbghfl58lvjpnq1sw767374nii5gjcg93xia")))

(define-public crate-dutty-1.0.4 (c (n "dutty") (v "1.0.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.21.0") (d #t) (k 0)))) (h "1bfjax6lf5bv876714gp1mdl7asknz7vjfjqnzyg8h9762722dzi")))

