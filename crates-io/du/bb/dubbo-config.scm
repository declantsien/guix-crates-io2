(define-module (crates-io du bb dubbo-config) #:use-module (crates-io))

(define-public crate-dubbo-config-0.1.0 (c (n "dubbo-config") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1vh75x828b449zvxszxm3p4fqskx50yhcn3av3g27syj9ajlpymx")))

(define-public crate-dubbo-config-0.2.0 (c (n "dubbo-config") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07fad3riwgpz2928az829bjw97hfck6dcxsp9c9shxq5qwibnymq")))

(define-public crate-dubbo-config-0.3.0 (c (n "dubbo-config") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00ma55i6a7rim764wn3l3ahfg9fkkzi2ycpaqb6wg989vxm5321v")))

