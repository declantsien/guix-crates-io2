(define-module (crates-io du bb dubbo-registry-zookeeper) #:use-module (crates-io))

(define-public crate-dubbo-registry-zookeeper-0.3.0 (c (n "dubbo-registry-zookeeper") (v "0.3.0") (d (list (d (n "dubbo") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)) (d (n "zookeeper") (r "^0.7.0") (d #t) (k 0)))) (h "0p5fs4nc42sbl649d20hrnhcc3m8j9gr12nabxf9n3iyqf3g1svk")))

