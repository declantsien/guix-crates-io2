(define-module (crates-io du bb dubbo-registry-nacos) #:use-module (crates-io))

(define-public crate-dubbo-registry-nacos-0.3.0 (c (n "dubbo-registry-nacos") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "dubbo") (r "^0.3.0") (d #t) (k 0)) (d (n "nacos-sdk") (r "^0.2") (f (quote ("naming" "auth-by-http"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 2)))) (h "16nxz5m4knlai54mw2iyq82z7vpqxaxhnnhl8yywaa17j4fjd0f8")))

