(define-module (crates-io du bb dubbo-build) #:use-module (crates-io))

(define-public crate-dubbo-build-0.1.0 (c (n "dubbo-build") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "103p8ixw5wgy355da3rwgrv9zpj81ck9ap5g0vf4z9gp9r1y6asf")))

(define-public crate-dubbo-build-0.2.0 (c (n "dubbo-build") (v "0.2.0") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06pdh0fycf822snbnwixh7ljaw43x8984qi6zr21jph61ra8g7kd")))

(define-public crate-dubbo-build-0.3.0 (c (n "dubbo-build") (v "0.3.0") (d (list (d (n "prettyplease") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05zfwg19qqcgbgq4d2p8h4hara17qks45946jr8h1w8cca2jnglg")))

