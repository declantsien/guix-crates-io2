(define-module (crates-io du ct ductile) #:use-module (crates-io))

(define-public crate-ductile-0.1.0 (c (n "ductile") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08jsviv4f16cc1wcqz42mzhpwbsacdnkbbn77fx832h2i4ca3424")))

(define-public crate-ductile-0.2.0 (c (n "ductile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q3bf1fp60cnlq098nbdxh87jdcqak9d8dgr8f96xdv7zvkarc66")))

(define-public crate-ductile-0.2.1 (c (n "ductile") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chacha20") (r "^0.9") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10vq5czj7b29a266a0j413mbk2br4d0byh1bqd3h8sbfgxb2m1kd")))

(define-public crate-ductile-0.3.0 (c (n "ductile") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "chacha20") (r "^0.9") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "04i8jgkgz9nim8jaki05pbqp357919iljwm2j744jrw8arcy5k8j")))

