(define-module (crates-io du ct duct_sh) #:use-module (crates-io))

(define-public crate-duct_sh-0.1.0 (c (n "duct_sh") (v "0.1.0") (d (list (d (n "duct") (r ">= 0.8.2") (d #t) (k 0)))) (h "060k80mgam8yxdq0qq0fmm5j9q1dsvas217kdpy641gamxkx1sj8")))

(define-public crate-duct_sh-0.13.4 (c (n "duct_sh") (v "0.13.4") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "0w5365h458j2l840rqp6m3f6yrzfc6f4xn1nvmmakky0mpfljrqp")))

(define-public crate-duct_sh-0.13.5 (c (n "duct_sh") (v "0.13.5") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "1a8kzd7izjw9vprr4zr8zizzzwlwr978qxpjx1la5mykc7fdz0fc")))

(define-public crate-duct_sh-0.13.6 (c (n "duct_sh") (v "0.13.6") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "0q3kh901sg1lr1h8hfdwj7n4sqr7f9rzbahxw8kz698kmff0hpb1")))

(define-public crate-duct_sh-0.13.7 (c (n "duct_sh") (v "0.13.7") (d (list (d (n "duct") (r "^0.13") (d #t) (k 0)))) (h "0ii1sqihq7l4bqzpff5jbzyb4jywk8prlag0pdgm8mx5vg536rjs")))

