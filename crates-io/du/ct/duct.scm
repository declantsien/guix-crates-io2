(define-module (crates-io du ct duct) #:use-module (crates-io))

(define-public crate-duct-0.1.0 (c (n "duct") (v "0.1.0") (h "1as650avg0f4n7w4sxnmrd2mj4rkrnvw6gp85v4yx3jlibpjazgc")))

(define-public crate-duct-0.2.0 (c (n "duct") (v "0.2.0") (d (list (d (n "crossbeam") (r ">= 0.2.8") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.8") (d #t) (k 0)) (d (n "tempdir") (r ">= 0.3.4") (d #t) (k 2)) (d (n "tempfile") (r ">= 2.1.1") (d #t) (k 2)))) (h "04xcwbri2m492as95d8b2q7ix8vbgi3343s7pv8pnmp8hrrhm5lr")))

(define-public crate-duct-0.4.0 (c (n "duct") (v "0.4.0") (d (list (d (n "crossbeam") (r ">= 0.2.8") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.8") (d #t) (k 0)) (d (n "tempdir") (r ">= 0.3.4") (d #t) (k 2)) (d (n "tempfile") (r ">= 2.1.1") (d #t) (k 2)))) (h "1c5ii5s0gn49fzyhqzxjn3vy19k6ggnbgiyp3b0aa6g2c1zhnzh0")))

(define-public crate-duct-0.5.0 (c (n "duct") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.2.8") (d #t) (k 0)) (d (n "os_pipe") (r "^0.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "03dv1s95vazc9ar76xqbjz4g125sjh9bj9rw79q3fyylzk5aajz4")))

(define-public crate-duct-0.5.1 (c (n "duct") (v "0.5.1") (d (list (d (n "os_pipe") (r "^0.4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "12dgsaavvjcik888pv8dwbrx2wivq7m0svadfd9ianhng0plgn2l")))

(define-public crate-duct-0.6.0 (c (n "duct") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.7.2") (d #t) (k 0)) (d (n "os_pipe") (r "^0.4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1jwnaq2mdd2kfnkxnanazzpb88i3nsf2sjqnn9b2bkfz236x8r65")))

(define-public crate-duct-0.7.0 (c (n "duct") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0c0vclf62r27pcz4ka1ysnjyc3l4hpsz3m06fiyh3jvbywqqx65p")))

(define-public crate-duct-0.8.0 (c (n "duct") (v "0.8.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.5.1") (d #t) (k 0)) (d (n "shared_child") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0n9pd4vdb92f9vg7l7qbnwj0k0s1w71yw0qdzz54b97gxd6jr9if")))

(define-public crate-duct-0.8.1 (c (n "duct") (v "0.8.1") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.5.1") (d #t) (k 0)) (d (n "shared_child") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0r4hx36wq3xdp8qmsl0cay5ag8xkdllfhfkahvv40aamg76qswvs")))

(define-public crate-duct-0.8.2 (c (n "duct") (v "0.8.2") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.5.1") (d #t) (k 0)) (d (n "shared_child") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0p06yslfi1wa68c2x068wmi9pr2mzmm64d6qwq8zba58w1gs2np4")))

(define-public crate-duct-0.9.0 (c (n "duct") (v "0.9.0") (d (list (d (n "lazycell") (r "^0.5.0") (d #t) (k 0)) (d (n "os_pipe") (r "^0.5.1") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0a677a3s658nz30753dbiw3bmmmi7cig1zq23x7giz95lyasv8v9")))

(define-public crate-duct-0.9.1 (c (n "duct") (v "0.9.1") (d (list (d (n "lazycell") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.5.1") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "118fz2h3g2vw2l5lkqycc1hvmwmd2fdcl3ds79y4vql5j951bwz2")))

(define-public crate-duct-0.9.2 (c (n "duct") (v "0.9.2") (d (list (d (n "lazycell") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "17al305wq9qw54c1idlnjpvwyq37kd1g76z427vgfx0fyiwksmcc")))

(define-public crate-duct-0.10.0 (c (n "duct") (v "0.10.0") (d (list (d (n "lazycell") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "08x48npncnyf0f8yn223qjnwbiw7x0i2yb4ngfcyaksvgk0rhqhn")))

(define-public crate-duct-0.11.0 (c (n "duct") (v "0.11.0") (d (list (d (n "lazycell") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.6.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1ii4sjcch24wr7y4rwcgjyhk8lgvsd2fw83bmw3b1clfhwh9xxcx")))

(define-public crate-duct-0.11.1 (c (n "duct") (v "0.11.1") (d (list (d (n "lazycell") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.7.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0a97wiw5prrppkp9s12i253m931y27j4c4gjpw4r4n810vw5lzzq")))

(define-public crate-duct-0.12.0 (c (n "duct") (v "0.12.0") (d (list (d (n "lazycell") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1vm1nzyi434h2zwix7c925qfa886ri1qx4nkq4hdrgkq7h9ayh1n")))

(define-public crate-duct-0.13.0 (c (n "duct") (v "0.13.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1ir3884i1yznkfdccqqbcb9v5sdpcgxlv41hgzncrqaljv18r0wj")))

(define-public crate-duct-0.13.1 (c (n "duct") (v "0.13.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "12pklawhsq2dmgn069smczrmjacwlhb2gaa2j8ig99yq4cmx9klh")))

(define-public crate-duct-0.13.2 (c (n "duct") (v "0.13.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0nfycvvnplkmzl4rxn8gnn2yyjd1wccm68nb80gcnx0nvgcq3f2x")))

(define-public crate-duct-0.13.3 (c (n "duct") (v "0.13.3") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "1c230w8c0fnw93asbccyrzxwjrgcq7xzzg6g7gl0iqjvsmlgl1qn")))

(define-public crate-duct-0.13.4 (c (n "duct") (v "0.13.4") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0lqmxbvb47n0ya71qbf12s1fx1s47kw562nlqwwdbyxa4lx9q2pr")))

(define-public crate-duct-0.13.5 (c (n "duct") (v "0.13.5") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.9.0") (d #t) (k 0)) (d (n "shared_child") (r "^0.3.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "13bxiy0y1dck3xz28rqw5ylf2ykv6mk8ww6g8408x26hksjs1ihg")))

(define-public crate-duct-0.13.6 (c (n "duct") (v "0.13.6") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^1.0.0") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0gkx9qmaxjgwb5jw7wf8fhpaxc3qwgnsx8zdghm4rxrm331kzbip")))

(define-public crate-duct-0.13.7 (c (n "duct") (v "0.13.7") (d (list (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.0.1") (d #t) (k 0)) (d (n "os_pipe") (r "^1.0.0") (d #t) (k 0)) (d (n "shared_child") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "174jk13rlvfgypha4f3l27mzzyc0ci7zginh5hjn6jr2s4c5gaz4")))

