(define-module (crates-io du ll dull) #:use-module (crates-io))

(define-public crate-dull-1.0.0 (c (n "dull") (v "1.0.0") (h "04phmxca0vq0kibsmzin3ccbqdm6zzqn2s9rrmkb6d9h7gm1n0b7")))

(define-public crate-dull-1.0.1 (c (n "dull") (v "1.0.1") (h "1wdwi2k6jc9ghspxm3m8p474f129z9m53430pq3cqp5qxcy95259")))

