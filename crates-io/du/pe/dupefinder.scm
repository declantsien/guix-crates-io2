(define-module (crates-io du pe dupefinder) #:use-module (crates-io))

(define-public crate-dupefinder-0.1.0 (c (n "dupefinder") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0sq6h868mkm6nj4nvzs9bbvnzc5l8wamdcxdrvmdax1m4xcj5c46")))

(define-public crate-dupefinder-0.1.1 (c (n "dupefinder") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1v0ln22rzi8d05xak8hvrdbyy4f07zchdhs1k8ln7w8jfwzq8pdy")))

(define-public crate-dupefinder-0.2.0 (c (n "dupefinder") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.8") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "14bdv20g2lw7xqh7f1xng32yxm1v0r6vrr1dj9zsrqffszbkgfwh")))

