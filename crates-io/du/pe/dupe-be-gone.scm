(define-module (crates-io du pe dupe-be-gone) #:use-module (crates-io))

(define-public crate-dupe-be-gone-0.0.2 (c (n "dupe-be-gone") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1ikzi5qpwqplqjbqhc06904hy4pazhw7w1731pr3wm19lia86z3p")))

