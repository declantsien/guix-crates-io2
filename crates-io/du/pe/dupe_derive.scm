(define-module (crates-io du pe dupe_derive) #:use-module (crates-io))

(define-public crate-dupe_derive-0.9.0 (c (n "dupe_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0s47wqs6gf79n8p61h9zczky02p9gj4nrnhwjw7shmm5is40yz9y")))

