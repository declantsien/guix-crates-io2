(define-module (crates-io du st dust-network) #:use-module (crates-io))

(define-public crate-dust-network-0.1.0 (c (n "dust-network") (v "0.1.0") (h "111sy304hgjnilxlj2lv2kxrm3d43056rmvqd3maf1fri0mrm9gv")))

(define-public crate-dust-network-0.0.0 (c (n "dust-network") (v "0.0.0") (h "1fzkmmq3hrbl8n6sfr2d3rx5zz46s05gh3qi3sryfkh6cjg3l2nx")))

(define-public crate-dust-network-0.2.0 (c (n "dust-network") (v "0.2.0") (h "1h8j64xmpylx518vqxkq0zi4mz8zchs9hrpbzakqxywbfy1b0d9r")))

