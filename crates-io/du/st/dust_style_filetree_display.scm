(define-module (crates-io du st dust_style_filetree_display) #:use-module (crates-io))

(define-public crate-dust_style_filetree_display-0.8.5 (c (n "dust_style_filetree_display") (v "0.8.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "lscolors") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stfu8") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "=3") (d #t) (k 2)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)) (d (n "thousands") (r "^0.2") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "winapi-util") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1phsph3lgsh0dcj0rksr4xn41jaqcp27z4vgy5148dvgchak9yrw")))

