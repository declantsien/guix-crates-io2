(define-module (crates-io du st dust_dds_derive) #:use-module (crates-io))

(define-public crate-dust_dds_derive-0.1.0 (c (n "dust_dds_derive") (v "0.1.0") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11l5yr9mpi76n4nqnb5k7kb875cq7d1r81m7lkqmkqw9pr9fwafq")))

(define-public crate-dust_dds_derive-0.2.0 (c (n "dust_dds_derive") (v "0.2.0") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "197nzw33492yv91hlvdd6px4b7a9y0d4kqcwzf67dsfj8dnwmd1n")))

(define-public crate-dust_dds_derive-0.2.1 (c (n "dust_dds_derive") (v "0.2.1") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wx4m77ayz6731h4mjl9raxmviai60y2l1i8m6knihxvqhm8r04i")))

(define-public crate-dust_dds_derive-0.3.0 (c (n "dust_dds_derive") (v "0.3.0") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18mnz6j9gbrxjinhghb6l1ax71gq13hyxgq59w35xfqpgsndi6ry")))

(define-public crate-dust_dds_derive-0.3.1 (c (n "dust_dds_derive") (v "0.3.1") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kj3w32lsm3s91w4my4mg0rmmans3npyqak5wdl9ralx3ss50wwa")))

(define-public crate-dust_dds_derive-0.4.0 (c (n "dust_dds_derive") (v "0.4.0") (d (list (d (n "cdr") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bab6mr9k2hnxvbj4ihk05w4nvh0jln7fwn9wpjiagi5dgn0fg62")))

(define-public crate-dust_dds_derive-0.5.0 (c (n "dust_dds_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zhnsfkxyc3fc2i007ji09lvsa99xa2wdmpf41kf9c0dzhik5kzr")))

(define-public crate-dust_dds_derive-0.6.0 (c (n "dust_dds_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "013zrg0cv8zk5g6bny5i02pvj5y9bkr0bllp0xay25rxx1j4v3wv")))

(define-public crate-dust_dds_derive-0.7.0 (c (n "dust_dds_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "1fy284b8sk0iw6jbn6zljxny3ghgrnfz47657rysff00mghcz0cg")))

(define-public crate-dust_dds_derive-0.8.0 (c (n "dust_dds_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "0b47b47n9f73ypfk8as1f8k4ihq62470gvfpw3ddg539116m2bhx")))

(define-public crate-dust_dds_derive-0.8.1 (c (n "dust_dds_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "041jx96gqvxxbxshd7qgqvbjbrq0g677sjv33cbg2dzj83fac30b")))

(define-public crate-dust_dds_derive-0.8.2 (c (n "dust_dds_derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "1n4rmmgshmc1dl95szcks9nyc1ial84ic85sizh7zv70gbxin8cl")))

