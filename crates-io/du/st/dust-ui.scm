(define-module (crates-io du st dust-ui) #:use-module (crates-io))

(define-public crate-dust-ui-0.1.0 (c (n "dust-ui") (v "0.1.0") (h "08i3ma72l5685sb1hs05n9w1s81ard7yxpyrl42rcp2nhwm59s45")))

(define-public crate-dust-ui-0.0.0 (c (n "dust-ui") (v "0.0.0") (h "1pbd3x7wdy1lj71vdxhjj4xkvnv8a6g1vcpkwajci0h7dyk32g6w")))

(define-public crate-dust-ui-0.2.0 (c (n "dust-ui") (v "0.2.0") (h "11dcnqlnkdg5r9f0bhnls92nsz41kkwv4a9yk3r1jf7gs1vzhmwa")))

