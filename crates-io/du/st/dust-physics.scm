(define-module (crates-io du st dust-physics) #:use-module (crates-io))

(define-public crate-dust-physics-0.1.0 (c (n "dust-physics") (v "0.1.0") (h "077bpr1szl8qafrgvzzdn7s400v05y4q7xn4s86zj0rn4bkxxkj1")))

(define-public crate-dust-physics-0.0.0 (c (n "dust-physics") (v "0.0.0") (h "03ls8hfxpbj1bbv76rzilf6w77hzpx7y349520my8pc430x2whr5")))

(define-public crate-dust-physics-0.2.0 (c (n "dust-physics") (v "0.2.0") (h "1mn6x2lmnvr8lnb1g8cwd26kfa8lq2l6q30wyg8k8mqg01ay2s9d")))

