(define-module (crates-io du st dust_dds_gen) #:use-module (crates-io))

(define-public crate-dust_dds_gen-0.1.0 (c (n "dust_dds_gen") (v "0.1.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "1x7sfqrqnk95bl82m90rb06sgkrr9b2zyfnxpy6cpd81i37qwzgz")))

(define-public crate-dust_dds_gen-0.2.0 (c (n "dust_dds_gen") (v "0.2.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "100mmm26rccm5xdkrwq48vgcjb1xgbi1nm0vy06c0307r16pf0i3")))

(define-public crate-dust_dds_gen-0.3.0 (c (n "dust_dds_gen") (v "0.3.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "187r9hgdvjqxvaa424czz7pn19blglx9988iln82sbxalcc9rabz")))

(define-public crate-dust_dds_gen-0.4.0 (c (n "dust_dds_gen") (v "0.4.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "1064xlh05098prkd3xvvkr7055hwb0rr9h193pfdczd6q50ccgk8")))

(define-public crate-dust_dds_gen-0.5.0 (c (n "dust_dds_gen") (v "0.5.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "1b2mdpg9szyfj0jh3vgpsj31gbwb78a047sp9s48gj1h0zx5l0y2")))

(define-public crate-dust_dds_gen-0.6.0 (c (n "dust_dds_gen") (v "0.6.0") (d (list (d (n "pest") (r "=2.5.3") (d #t) (k 0)) (d (n "pest_derive") (r "=2.5.3") (d #t) (k 0)))) (h "1an5zsk12j18mqclw5apyj20wyrbswsy0zwbfz9pywrml6gy2lcp")))

(define-public crate-dust_dds_gen-0.7.0 (c (n "dust_dds_gen") (v "0.7.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "12px3nr12w4kz2jc85g19is3c18qqpnhfxbkg1dw61cicik8whiy")))

(define-public crate-dust_dds_gen-0.8.0 (c (n "dust_dds_gen") (v "0.8.0") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1kcpqr4wsd6njxpsykv5q9nlmkmjql427ydqwvc71szv5z0f6l2c")))

(define-public crate-dust_dds_gen-0.8.1 (c (n "dust_dds_gen") (v "0.8.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0bb29ljk9bh3nppqmqji0gzwmm7pdd8hykgwrlsalqrp6wqg1kk9")))

(define-public crate-dust_dds_gen-0.8.2 (c (n "dust_dds_gen") (v "0.8.2") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0hzgkygvyxqzhpq2i6b30hfi52s1167b4w0vmphnsn7plrq1gs5a")))

