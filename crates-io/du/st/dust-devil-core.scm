(define-module (crates-io du st dust-devil-core) #:use-module (crates-io))

(define-public crate-dust-devil-core-1.0.0 (c (n "dust-devil-core") (v "1.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "19l1x84ccnkzsh764n7x5xiiksxx4sr7901f3h5jfz9gh7rq6463")))

(define-public crate-dust-devil-core-1.1.0 (c (n "dust-devil-core") (v "1.1.0") (d (list (d (n "tokio") (r "^1.36") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1ygx2d0s0ij2ynrnj3ycchr28wqn2lp3nxais3d1g8ggwg4kgnd5") (r "1.76.0")))

