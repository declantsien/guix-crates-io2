(define-module (crates-io du st duster) #:use-module (crates-io))

(define-public crate-duster-0.1.0 (c (n "duster") (v "0.1.0") (h "0w4nflf0v0m1adh1bdm29r85f3sj1xasl99fcnx8s1lz8l6x5cd0")))

(define-public crate-duster-0.1.1 (c (n "duster") (v "0.1.1") (d (list (d (n "nom") (r "^6.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (o #t) (d #t) (k 0)))) (h "12b9pc533caj36882bzjp9mc7wbsh6chx8vq16nsxr0sivgrrl8p") (f (quote (("json-integration" "serde" "serde_json") ("default" "json-integration"))))))

