(define-module (crates-io du st dust-core) #:use-module (crates-io))

(define-public crate-dust-core-0.1.0 (c (n "dust-core") (v "0.1.0") (h "0qb2p6csski88dry6j39c4n8wgpp2f0kx4vyc6l2nkgw3rs8szsd")))

(define-public crate-dust-core-0.0.0 (c (n "dust-core") (v "0.0.0") (h "1q20f138rzfg52yq9hhzi1lxn9wh28r1lylv21qav61g6rswpifq")))

(define-public crate-dust-core-0.2.0 (c (n "dust-core") (v "0.2.0") (h "03919spb4vh8mq1cpk08pzncckvsmn5m1dx9anx8hwhh2m0j3wr5")))

