(define-module (crates-io du st dust-devil-sandstorm) #:use-module (crates-io))

(define-public crate-dust-devil-sandstorm-1.0.0 (c (n "dust-devil-sandstorm") (v "1.0.0") (d (list (d (n "dust-devil-core") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "sync" "signal" "macros"))) (d #t) (k 0)))) (h "1pmav4dnqnpxa4g0yfnfpw4grk1656nfsyi9vhzhwc1y28ljn9cf")))

(define-public crate-dust-devil-sandstorm-1.1.0 (c (n "dust-devil-sandstorm") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dust-devil-core") (r "^1.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("rt" "net" "sync" "time" "signal" "macros"))) (d #t) (k 0)))) (h "0jk2fbhsc7yannwx10cra1mh16fh6ffs731z4myn4mj7i65n6d37") (r "1.76.0")))

