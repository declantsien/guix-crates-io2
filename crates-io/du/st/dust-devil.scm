(define-module (crates-io du st dust-devil) #:use-module (crates-io))

(define-public crate-dust-devil-1.0.0 (c (n "dust-devil") (v "1.0.0") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "dust-devil-core") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-std" "net" "sync" "fs" "signal" "io-util" "macros" "parking_lot"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0") (d #t) (k 0)))) (h "0dxfna2fka7afwg1gcrqiv86c2rr5wy5g1p6684l2vaxg6y40d73")))

(define-public crate-dust-devil-1.1.0 (c (n "dust-devil") (v "1.1.0") (d (list (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "dust-devil-core") (r "^1.1") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("rt-multi-thread" "io-std" "net" "sync" "fs" "signal" "io-util" "macros" "parking_lot"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "0f71r0f6ncvm76zvnlkxdkpgvna1jp359q3w48kqradd30hh9qwl") (r "1.76.0")))

