(define-module (crates-io du st dusty-room) #:use-module (crates-io))

(define-public crate-dusty-room-0.0.1 (c (n "dusty-room") (v "0.0.1") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1244m0a72vvnqzwlm0gfhzf5mn0acrgfb0i5ar4kik4yxyii0jkv")))

