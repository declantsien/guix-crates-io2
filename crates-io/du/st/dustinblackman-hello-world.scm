(define-module (crates-io du st dustinblackman-hello-world) #:use-module (crates-io))

(define-public crate-dustinblackman-hello-world-0.1.0 (c (n "dustinblackman-hello-world") (v "0.1.0") (h "15xl207sn6aqfqy5jmdfwm8p76zbdcj0xlffpn6j0j3sxrjs4aki")))

(define-public crate-dustinblackman-hello-world-0.2.0 (c (n "dustinblackman-hello-world") (v "0.2.0") (h "0l8s74fih88l1b2hiabg7vm87x338bn9drafwxc9vlcnmrl785zm")))

(define-public crate-dustinblackman-hello-world-0.2.1 (c (n "dustinblackman-hello-world") (v "0.2.1") (h "0j8538w6qb40vihnbjwclvyvf0ijn3izvb0ancraxkh7jhajq4xj")))

