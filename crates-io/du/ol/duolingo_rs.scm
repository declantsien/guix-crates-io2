(define-module (crates-io du ol duolingo_rs) #:use-module (crates-io))

(define-public crate-duolingo_rs-0.2.4 (c (n "duolingo_rs") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gdv5zzqklah51xs9bfwb4hpvxnsamfwfwxgbhlpq722pzravbhr")))

(define-public crate-duolingo_rs-0.3.2 (c (n "duolingo_rs") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "cookies"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01l0fv7m6k8lx6zf3shb9v6hancpn7kr0wdmlsp7h0568n5xi8am")))

