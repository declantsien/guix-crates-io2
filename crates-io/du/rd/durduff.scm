(define-module (crates-io du rd durduff) #:use-module (crates-io))

(define-public crate-durduff-0.1.0-alpha.1 (c (n "durduff") (v "0.1.0-alpha.1") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "05qry0nlg5p849f3g5m4dw264lfd1vrxr29mdhka5qwbrf5zix97")))

