(define-module (crates-io du id duid) #:use-module (crates-io))

(define-public crate-duid-0.1.0 (c (n "duid") (v "0.1.0") (d (list (d (n "duid-core") (r "^0.1") (d #t) (k 0)))) (h "0zwqf5dj66nhgrfs705wf9pni9gwxl3f5vgxpqcb8adsbwl2c1ir") (f (quote (("with-request-animation-frame" "duid-core/with-request-animation-frame") ("default" "duid-core/default" "with-request-animation-frame"))))))

