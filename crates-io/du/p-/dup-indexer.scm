(define-module (crates-io du p- dup-indexer) #:use-module (crates-io))

(define-public crate-dup-indexer-0.1.0 (c (n "dup-indexer") (v "0.1.0") (h "0yqib3l9rrpv0sq61ffp0vwfgp9dm413jhllwi2kbcm2qmfacmk4")))

(define-public crate-dup-indexer-0.2.0 (c (n "dup-indexer") (v "0.2.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "028p8yw1f6xcwbyhpl6254wsh6d08hg3msl1di7jps911s52s1ij")))

(define-public crate-dup-indexer-0.2.1 (c (n "dup-indexer") (v "0.2.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0pqkg4bv4j7zmc2v1sgjph00w6xxwvv1bxb07ndazd1aw67dg4nx")))

(define-public crate-dup-indexer-0.2.2 (c (n "dup-indexer") (v "0.2.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "17fa610kpbp6b7wnz1cszap0dbgk2wrindslqhhs3p74yrb1cp8b")))

(define-public crate-dup-indexer-0.2.3 (c (n "dup-indexer") (v "0.2.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0nq58862zzab58bwc56ndqyiggz1nhv4a4xirlgpl7nh30bwwaki")))

(define-public crate-dup-indexer-0.3.0 (c (n "dup-indexer") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "164qd3pjnyajl0hmp3b60v0jpb9fyvxk59df49qc8j86awhzz9a9")))

(define-public crate-dup-indexer-0.3.1 (c (n "dup-indexer") (v "0.3.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "12547sasls9ssv1df8r2galij79mlv4vlfzik9rmvgpgk6bcqrz2") (r "1.60.0")))

(define-public crate-dup-indexer-0.3.2 (c (n "dup-indexer") (v "0.3.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0ljvxz2fqgyn2qi2lmwqf6hrfvcvf3kn0l7s8h8zam7ncxy92yyr") (r "1.60.0")))

(define-public crate-dup-indexer-0.4.0 (c (n "dup-indexer") (v "0.4.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "16siscv9i0ijlssi831ba9w1hshhlch89vv8m9nwxbjk0c4mb1q2") (r "1.60.0")))

