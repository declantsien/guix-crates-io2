(define-module (crates-io du al dualhashkey) #:use-module (crates-io))

(define-public crate-dualhashkey-0.1.0 (c (n "dualhashkey") (v "0.1.0") (d (list (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)))) (h "19y98kqa14ayr6yj0mwkrd4jx62r9fcqnh07k6vsn0f96ngpcylz")))

(define-public crate-dualhashkey-0.1.1 (c (n "dualhashkey") (v "0.1.1") (d (list (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0v9y8g50ppi8226d7hl1k00lmr1kv7hw5kbl78mbx0kcdp2j7id1")))

