(define-module (crates-io du al dualnum) #:use-module (crates-io))

(define-public crate-dualnum-0.1.0 (c (n "dualnum") (v "0.1.0") (d (list (d (n "num-dual") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.13") (f (quote ("extension-module" "abi3" "abi3-py36"))) (d #t) (k 0)))) (h "04w0w7xvy55bqcis2brsv4y7y0k10fqws579h1p5x6rjb07nvrc2") (y #t)))

(define-public crate-dualnum-0.2.0 (c (n "dualnum") (v "0.2.0") (d (list (d (n "num-dual") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("extension-module" "abi3" "abi3-py36" "multiple-pymethods"))) (d #t) (k 0)))) (h "0kfxqpfl74jq3pn7y23d1s1mngyqrbgph4914dhxg7dr7rnwj4rb") (y #t)))

(define-public crate-dualnum-0.2.1 (c (n "dualnum") (v "0.2.1") (d (list (d (n "num-dual") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("extension-module" "abi3" "abi3-py36" "multiple-pymethods"))) (d #t) (k 0)))) (h "0wi4s6ghwmlaqgylqbh3ffxq9kvxp922lh3lm5400n9yjr4wl50c") (y #t)))

