(define-module (crates-io du al duals) #:use-module (crates-io))

(define-public crate-duals-0.0.1 (c (n "duals") (v "0.0.1") (h "0vlw0gqdlsaxqp49gn0gh1bfzyby2idy79lxgwsivbjg0k1w8mhy")))

(define-public crate-duals-0.0.2 (c (n "duals") (v "0.0.2") (h "11bi348plrxa9nh04l1fxfqf5yqzvng4gllpm7ni3mmyg29mwck5") (y #t)))

(define-public crate-duals-0.0.3 (c (n "duals") (v "0.0.3") (h "0dv7xrvyi5j5k7gk7zbzmzfpndvm6fc44khackzpgh00gis0ibv3")))

