(define-module (crates-io du al dual_balanced_ternary) #:use-module (crates-io))

(define-public crate-dual_balanced_ternary-0.1.0-a1 (c (n "dual_balanced_ternary") (v "0.1.0-a1") (h "1jw60vzbclj9a0cwb16fk0d6i53bpi0wb1yww3fxywb74fkhyi7z")))

(define-public crate-dual_balanced_ternary-0.1.0-a2 (c (n "dual_balanced_ternary") (v "0.1.0-a2") (h "1l8slw5ap08v4107i88fyq0gkjic0mdx3anz132prwb1i4jgrbfx")))

(define-public crate-dual_balanced_ternary-0.1.0-a3 (c (n "dual_balanced_ternary") (v "0.1.0-a3") (h "0h4hb4bjyxdbi4q0287njb6g6qmcm03q116ha4cpwrhbzri3i3cw")))

(define-public crate-dual_balanced_ternary-0.1.0-a4 (c (n "dual_balanced_ternary") (v "0.1.0-a4") (h "08lmyfydjhr6bg8ra5y3s040lg1lrh8naj930yfv9qinb3zrs8zh")))

(define-public crate-dual_balanced_ternary-0.1.0-a5 (c (n "dual_balanced_ternary") (v "0.1.0-a5") (h "0z0prcbrsb2d5ygji686r6zvq55hsfigdyijhshjbans6cfk8ny8")))

(define-public crate-dual_balanced_ternary-0.1.0-a6 (c (n "dual_balanced_ternary") (v "0.1.0-a6") (h "0wybsvk7fq65ydzl0rih2ny340x8mybyp7zacgqyaxmhg95iy09l")))

(define-public crate-dual_balanced_ternary-0.1.0-a7 (c (n "dual_balanced_ternary") (v "0.1.0-a7") (h "1j0xv6hzx66hw263p0hcz21mcxzmjf8bdzviz1g0c2vc7qzzlhs6")))

(define-public crate-dual_balanced_ternary-0.1.0-a8 (c (n "dual_balanced_ternary") (v "0.1.0-a8") (h "11yid1pvbcs7k8s7lj3baiw9xvpmjx3vjp81id0839yqa5d1dzw7")))

(define-public crate-dual_balanced_ternary-0.1.0 (c (n "dual_balanced_ternary") (v "0.1.0") (h "1y0qmhabzna6x76rf178w3j5zrhv89g0nqa4p8k6adkmjd4bg4a0")))

