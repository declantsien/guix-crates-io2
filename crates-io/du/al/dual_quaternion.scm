(define-module (crates-io du al dual_quaternion) #:use-module (crates-io))

(define-public crate-dual_quaternion-0.1.0 (c (n "dual_quaternion") (v "0.1.0") (d (list (d (n "quaternion") (r "*") (d #t) (k 0)) (d (n "vecmath") (r "*") (d #t) (k 0)))) (h "0xdf3ifzcf51j49i9p0jgkjvjbvj2bwig16khf9b1fv5jhysp7gq")))

(define-public crate-dual_quaternion-0.2.0 (c (n "dual_quaternion") (v "0.2.0") (d (list (d (n "quaternion") (r "^0.4.1") (d #t) (k 0)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 0)))) (h "0p2pcp26xk6hfclf28sp5n81jzgsrrqv5dxakv31ymgyf98q2xh7")))

