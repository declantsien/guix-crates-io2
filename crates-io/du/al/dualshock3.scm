(define-module (crates-io du al dualshock3) #:use-module (crates-io))

(define-public crate-dualshock3-0.1.0 (c (n "dualshock3") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2.1") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stoppable_thread") (r "^0.2.1") (d #t) (k 0)))) (h "053x579ap4yzb20qqf1daq3zgw36a7438wn64ijnkgjnwcrihz5k")))

