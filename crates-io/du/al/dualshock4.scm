(define-module (crates-io du al dualshock4) #:use-module (crates-io))

(define-public crate-dualshock4-0.1.0 (c (n "dualshock4") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.187") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "scroll") (r "^0.9.0") (d #t) (k 0)))) (h "0v52mzf5r8a0zhbzjdxmgsyhnvnrfp0cv64w31ifrasw0dbhki9p")))

(define-public crate-dualshock4-0.1.1 (c (n "dualshock4") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.187") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "scroll") (r "^0.9.0") (d #t) (k 0)))) (h "1jhnchcm8kc3nx1k054jfkl20zln67b7w9wzs8minf6nhmn7i4ai")))

