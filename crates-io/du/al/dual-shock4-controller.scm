(define-module (crates-io du al dual-shock4-controller) #:use-module (crates-io))

(define-public crate-dual-shock4-controller-0.1.0 (c (n "dual-shock4-controller") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)))) (h "14yxrbdw9xhwn1n73adwzkvq6y5fjbqfhix2nlj0cb8danrgwafh")))

(define-public crate-dual-shock4-controller-0.1.1 (c (n "dual-shock4-controller") (v "0.1.1") (d (list (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)))) (h "0fb9avblsa47kd0y1dwdmr3nxk88gsqz4fij7xl52mws8qzk9xqs")))

