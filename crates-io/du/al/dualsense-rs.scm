(define-module (crates-io du al dualsense-rs) #:use-module (crates-io))

(define-public crate-dualsense-rs-0.1.0 (c (n "dualsense-rs") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "1v1q9gklwj8v0ixn2inmf5j3ryx7lqzg59p8vkv60z7if58pjrsf")))

(define-public crate-dualsense-rs-0.2.0 (c (n "dualsense-rs") (v "0.2.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "1xlry6zg6d8av7dpyfz7avg0d1xx2rp146zwcmi3wjb0aqvr72c4")))

(define-public crate-dualsense-rs-0.3.0 (c (n "dualsense-rs") (v "0.3.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "191bd7j1ji199aidljjlbyx6pradh9a3r2cav2iyc8pcf7x7f3mg")))

(define-public crate-dualsense-rs-0.4.0 (c (n "dualsense-rs") (v "0.4.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "1n9nd5cj7mggi3ybbn0h7p8xp926541c67fwlj9a9nfm9rl2drv9")))

(define-public crate-dualsense-rs-0.5.0 (c (n "dualsense-rs") (v "0.5.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "14jjxpfhlxaivf4j1959ipvx308biim69rp7inrcd112ihxnwl0h")))

(define-public crate-dualsense-rs-0.6.0 (c (n "dualsense-rs") (v "0.6.0") (d (list (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "0j7szh5ip8rdi0kfvhhiy8rv0vnazljxy48i5hy7hzga8101z07l")))

