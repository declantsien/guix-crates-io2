(define-module (crates-io du al dual_rewards_distribution_recipient_crate) #:use-module (crates-io))

(define-public crate-dual_rewards_distribution_recipient_crate-0.2.0 (c (n "dual_rewards_distribution_recipient_crate") (v "0.2.0") (d (list (d (n "casper-contract") (r "^1.4.4") (d #t) (k 0)) (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "casperlabs-contract-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.3.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (k 0)))) (h "0qs98h5i3xayqzai6iwrbx6hamq1mgpl469g6wqgpavgbxhmk8hl")))

