(define-module (crates-io du al dual_num) #:use-module (crates-io))

(define-public crate-dual_num-0.1.0 (c (n "dual_num") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1ihda47k8rskmgiandzbaxafydsif35jhcs1h30x71232kmgw4lf")))

(define-public crate-dual_num-0.1.1 (c (n "dual_num") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1nvw1x15dy5m0jncw1g0wg2fnsh8yczpkplb6yiaiv8xqp47c62f")))

(define-public crate-dual_num-0.2.0 (c (n "dual_num") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "078zm3nmlb52q3prf17mlqjdv5ppwy0n0gqa6dq5qfd6gpqid0rq")))

(define-public crate-dual_num-0.2.1 (c (n "dual_num") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09rd8w9j4g9khh0awrahmvzai5369fzypgcfqij9agdgvlnywhf4")))

(define-public crate-dual_num-0.2.2 (c (n "dual_num") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "01ji4p7zm8ppccq2sp5jykhlw5y7rliq5k20q3dl5alnd8w6fj14")))

(define-public crate-dual_num-0.2.3 (c (n "dual_num") (v "0.2.3") (d (list (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qkjlnrdjphf1fvxzzq8j03zsfn8yppl2ai6awj892019mappr02")))

(define-public crate-dual_num-0.2.4 (c (n "dual_num") (v "0.2.4") (d (list (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15233wiy5c4zvbxglk0syl8z9d0hgi9dv3pihjln36kjjawjkgym")))

(define-public crate-dual_num-0.2.5 (c (n "dual_num") (v "0.2.5") (d (list (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vq6yq36vvyz4gy9fjmcrv054bicqxiifav2g5bnlh7js4sndp9n")))

(define-public crate-dual_num-0.2.6 (c (n "dual_num") (v "0.2.6") (d (list (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0a60szrxfs07b0vfjl75iys6rv5nc1aw2z0ypgr8f4g3j20gi5ww")))

(define-public crate-dual_num-0.2.7 (c (n "dual_num") (v "0.2.7") (d (list (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ildxx060r6p2lyghm3cp3z21klbs2h5dba27iyz2cp1nxq41vzs")))

