(define-module (crates-io qc al qcalc) #:use-module (crates-io))

(define-public crate-qcalc-0.1.0 (c (n "qcalc") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1sbp9qadzwygf503lbbhwgs1zxhx6xw32fp7z82l2jbxipxd2r4z")))

(define-public crate-qcalc-0.1.1 (c (n "qcalc") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "0ipg75h9xzpdlv5jlqr6rh2pfkycp262hsvlgc8yhxpjzjh4sy93")))

(define-public crate-qcalc-0.1.2 (c (n "qcalc") (v "0.1.2") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1ix2mnbpfgl3qkk65bxm3j2q1ym6fldqz5dwlcp1s9afpyv93j8m")))

(define-public crate-qcalc-0.1.3 (c (n "qcalc") (v "0.1.3") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "05zdsavdh0qj60nhdlx3s9miazf9hvc5jv4ns06qbfr29wv93zh4")))

(define-public crate-qcalc-0.2.0 (c (n "qcalc") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "0ngyihk9is0i50gqp0hfm8f13xcyr0171hw7cwi07iizz22bcpyh")))

(define-public crate-qcalc-0.2.1 (c (n "qcalc") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "12brmqpadsb62yniky7a9dg2blwq45fyknf9xfgf7ay1gd8r4xpd")))

(define-public crate-qcalc-0.2.2 (c (n "qcalc") (v "0.2.2") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1sa2rfj0wpk90d1xkkdncqqz7zx9qk3k9xvdh2v9cib3hj0q9sz1")))

(define-public crate-qcalc-0.2.3 (c (n "qcalc") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "0kvvdg115x5qna4gvms6gnbnk735palr17l49xmyafxlhdglclfp")))

(define-public crate-qcalc-0.3.0 (c (n "qcalc") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "11jg2w3r1hbaqyvcbpx34lnkgykf63i98ipl02irrsk55vvr5wc3")))

(define-public crate-qcalc-0.4.0 (c (n "qcalc") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1pas1xq6q2abdb7v17x3gz5z1i3xxwkbzavnv05syjfx2ybwagm8")))

(define-public crate-qcalc-0.4.1 (c (n "qcalc") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "13iilq1k83s2zy8ymz5mxd6dpfgwl91vfhkpv4sary0114lyiqv3")))

(define-public crate-qcalc-0.5.0 (c (n "qcalc") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "06k2cnsy1i1n3bg0ca5n245y7qb41gg02lmi124jqngz9f2ah1mp")))

(define-public crate-qcalc-0.6.0 (c (n "qcalc") (v "0.6.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r "^0.4.0") (d #t) (k 0)))) (h "1z5583n280da2d1acyyxkjgjs978n4frfsl70qqwfjz3hr7xfzh4")))

(define-public crate-qcalc-0.6.1 (c (n "qcalc") (v "0.6.1") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "108d4qdygwzf75s8jnav27k1j6lk7jxxm3wjf2fahsnsdfax4r0v")))

(define-public crate-qcalc-0.6.2 (c (n "qcalc") (v "0.6.2") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0cbc25pi3q64xdlwccxxdig99vq3f81r66rxxvgadk7kc0g1rq36")))

(define-public crate-qcalc-0.7.0 (c (n "qcalc") (v "0.7.0") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "00sfwnsl062cirh5n8pw2h59x68mkggx864x7ljgp9aldfadigfz")))

(define-public crate-qcalc-0.8.0 (c (n "qcalc") (v "0.8.0") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "11s81ibm4z7vl0ighdpsqld68kjx54mxlq1by0g34v8nhc8qdj2h")))

(define-public crate-qcalc-0.8.1 (c (n "qcalc") (v "0.8.1") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0ivbc0rhhvza6jww631j1nrsg8f9pmjzf6lnwa71n557cb2m1a7v")))

(define-public crate-qcalc-0.9.0 (c (n "qcalc") (v "0.9.0") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0a5wka1v4zv80s2y3kr5fpfsjlzl06331ci7r6pqksgz3d5v617q")))

(define-public crate-qcalc-0.9.1 (c (n "qcalc") (v "0.9.1") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "1r7yhy5z781cwk4m7c45jwchwmnh7v0c70mzkmc4f9g9dxb1iyyv")))

(define-public crate-qcalc-0.9.2 (c (n "qcalc") (v "0.9.2") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0ph0si2fj4av16cy6y99i336zgffm8xzl65wigyw7arncd21im8w")))

(define-public crate-qcalc-0.9.3 (c (n "qcalc") (v "0.9.3") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0xjz60n4dzlvmj5fqmxx6q64ady9pfw8lqv0ya5jf2isj5v1dk4p")))

(define-public crate-qcalc-0.9.4 (c (n "qcalc") (v "0.9.4") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "104ar5wqdc7sc5l6l51asm65q4i30gy1gdaw2v92pp8h3bmh4h61")))

(define-public crate-qcalc-0.9.5 (c (n "qcalc") (v "0.9.5") (d (list (d (n "crossterm") (r ">=0.27.0") (d #t) (k 0)) (d (n "dirs-next") (r ">=2.0.0") (d #t) (k 0)) (d (n "ratatui") (r ">=0.25.0") (d #t) (k 0)) (d (n "tui-textarea") (r ">=0.4.0") (d #t) (k 0)))) (h "0aka7ciqnjaxlnhh22may5p3jn9jllgp30ks3bp7l19g5fwks8zb")))

