(define-module (crates-io qc lo qcloud) #:use-module (crates-io))

(define-public crate-qcloud-0.1.0 (c (n "qcloud") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 1)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "qcos") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0xp2k03s4j3jqcq7l4ivdhsw5jsi8q2kh4va63l0ywzj9yr7vcrz")))

(define-public crate-qcloud-0.1.2 (c (n "qcloud") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 1)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)) (d (n "qcos") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0x0rkgk3xrrijjvmcngkz53adld5g1k2vxvji6id4yxnhlh6xm31")))

