(define-module (crates-io qc el qcell) #:use-module (crates-io))

(define-public crate-qcell-0.1.0 (c (n "qcell") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1f91ka51n4vap1ax6rfscry4slyis6vpfk4mc660zqnvdphrsvg6") (y #t)))

(define-public crate-qcell-0.1.1 (c (n "qcell") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "10nmhw10zgh6rmkr0n3j7f1vs69cyp7nmqs2p1c7bpqrbsnkg64k") (y #t)))

(define-public crate-qcell-0.1.2 (c (n "qcell") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1skkf9jwvwxn47wlgn2nqs5cbdc7b1x5lam6i160dvnllr4kbz4l") (y #t)))

(define-public crate-qcell-0.2.0 (c (n "qcell") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1j9z0zqi3zynh4yiarjss7gbm30pyv35yxhlv4pyrgkw4prjnmb8") (y #t)))

(define-public crate-qcell-0.3.0 (c (n "qcell") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1w2z4717b55lf3idglz9w4qj13fkxpv267prvhgy9dggl5fm72ng") (f (quote (("no-thread-local")))) (y #t)))

(define-public crate-qcell-0.4.0 (c (n "qcell") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0v41f73mswi9349f4vmaa6bqgaac4bmljrij8dsmvyvpz5csfprj") (y #t)))

(define-public crate-qcell-0.4.1 (c (n "qcell") (v "0.4.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1i0gdi3k5sph6bisy6crrr8iw911wm15ji4hjk89qjzrrsi36vlh") (y #t)))

(define-public crate-qcell-0.4.2 (c (n "qcell") (v "0.4.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0p5cr04bs9is9g36xc4pg98hdqak1av15bzqxirnqg3783a163d3") (y #t)))

(define-public crate-qcell-0.4.3 (c (n "qcell") (v "0.4.3") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16msx3z7wascsi3bqp5kiyakd8dnva2mk2gb26xk4pb1imylqarf")))

(define-public crate-qcell-0.5.0 (c (n "qcell") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1j1s9z44n2c0ixvy14390ax0d56v08gqkgmxbsxrzyibnw33y0p5") (f (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-qcell-0.5.1 (c (n "qcell") (v "0.5.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "generativity") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)))) (h "1553diq422hy6ah6c9lqabsx5y4fg9sjj56pgn133iz0mm1iqnri") (f (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-qcell-0.5.2 (c (n "qcell") (v "0.5.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "generativity") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)))) (h "0h6c8wbl9h7vwfj77n6yhmknylqwh2gkhzpq0nl6mfcxx6kjlqqv") (f (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-qcell-0.5.3 (c (n "qcell") (v "0.5.3") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "generativity") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)))) (h "1qv1fg2d1cxhs5pgp6sd2brgn7fr454zr6vwg9qywbnrakwvrfkn") (f (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-qcell-0.5.4 (c (n "qcell") (v "0.5.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "exclusion-set") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "generativity") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.0") (d #t) (k 2)))) (h "0i9ka5si32f40wmzxwf0i31n8mjdm6dl91vdbx4ankga7nm08v4z") (f (quote (("default" "std") ("alloc")))) (s 2) (e (quote (("std" "alloc" "once_cell" "exclusion-set?/std")))) (r "1.60.0")))

