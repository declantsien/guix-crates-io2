(define-module (crates-io qc el qcells) #:use-module (crates-io))

(define-public crate-qcells-0.1.0 (c (n "qcells") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r9qq63xa9zxjis9k8hs0b5gm6kbs4q1vy1qagb1kfyy6xpbjcj1")))

(define-public crate-qcells-0.1.1 (c (n "qcells") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xjpbp8xvazwpkv80z5ggas01lr9sy72rd0907cwh0r7ksg5rsi2")))

