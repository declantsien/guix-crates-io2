(define-module (crates-io qc _f qc_file_parsers) #:use-module (crates-io))

(define-public crate-qc_file_parsers-0.2.2 (c (n "qc_file_parsers") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "09yy4vc3wdyp1gg70p7085r15r15jjgmgz9cp3rphssxsjw5rq3s")))

(define-public crate-qc_file_parsers-0.2.3 (c (n "qc_file_parsers") (v "0.2.3") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "16jm2kv3bhrayzhblb0h6zn3c6sjby3z3q6bb48r118bvmvacrgl")))

(define-public crate-qc_file_parsers-1.0.0 (c (n "qc_file_parsers") (v "1.0.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "1zdak4gsmj9xnyhsigjq6bgdqj7bi2fj8fsw42p8d6cjy1rjdykp")))

(define-public crate-qc_file_parsers-2.0.0 (c (n "qc_file_parsers") (v "2.0.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "1fjqky1hqks8r6lzn52jq1zd0plxgsz14sddd85yhyvhddjvjw5x")))

(define-public crate-qc_file_parsers-2.0.1 (c (n "qc_file_parsers") (v "2.0.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "093yfgc25ll4z6cski7vcvmjqpkcfvinbvr6526mx40q75h3fsfy")))

(define-public crate-qc_file_parsers-3.3.0 (c (n "qc_file_parsers") (v "3.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0m5rdas7w1fh06dnfiq2h399z4v0qyaskg99xd3ijpvm3d5rrck3")))

(define-public crate-qc_file_parsers-3.3.2 (c (n "qc_file_parsers") (v "3.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "09whd5i11q2924z6sq2v09awna55lsy026wrpqyr8y7zfbl2khla")))

