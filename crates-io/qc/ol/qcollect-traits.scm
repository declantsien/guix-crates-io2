(define-module (crates-io qc ol qcollect-traits) #:use-module (crates-io))

(define-public crate-qcollect-traits-0.1.0 (c (n "qcollect-traits") (v "0.1.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0fkw477gd1g34yxyndy1a8n01jjn4jyldzhb0cfhx8cax5rrhyag")))

(define-public crate-qcollect-traits-0.2.0 (c (n "qcollect-traits") (v "0.2.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0ri6y5hblvnj6lmfjdpww6rgxgh838dgbjravvca0a3p4kz6qpd8")))

(define-public crate-qcollect-traits-0.3.0 (c (n "qcollect-traits") (v "0.3.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "01sj94419v6zvwzz088zlbq0i2wcj5zm3iv7vcahl2gjd9ww1q77")))

(define-public crate-qcollect-traits-0.3.1 (c (n "qcollect-traits") (v "0.3.1") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "03ygz4bl6c72l3xmvlk6mcl2w82zhvr0phclfsrqrs61q5c9wiww")))

(define-public crate-qcollect-traits-0.4.0 (c (n "qcollect-traits") (v "0.4.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0waw5b5njxn7w5saxsl26mfgg21vlckr0s3mmmknb8sy6gpcy80x")))

(define-public crate-qcollect-traits-0.4.1 (c (n "qcollect-traits") (v "0.4.1") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "05v6hrcfp4v6dy8c2pibccgclab2cl4clh5jc9qfi0gjlvj2s8qk")))

(define-public crate-qcollect-traits-0.5.0 (c (n "qcollect-traits") (v "0.5.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0fzryxxpyfshkcaasffvy7nbiwy905qpmknzzh92ajqlnk7l250m")))

(define-public crate-qcollect-traits-0.5.1 (c (n "qcollect-traits") (v "0.5.1") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0b1w99nc7250b7qi4m36axmpcv2sqz09c7n77h2ibrynwmwpaggm")))

(define-public crate-qcollect-traits-0.5.2 (c (n "qcollect-traits") (v "0.5.2") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1yclbz17svm5adzzmbrc9425rm83grljdj15jw8s576khvlbvikd")))

(define-public crate-qcollect-traits-0.5.3 (c (n "qcollect-traits") (v "0.5.3") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1r3bz06803yb4k4d4zlzmpdpclchfkfkdv081i313dcllhsdk0li")))

(define-public crate-qcollect-traits-0.6.0 (c (n "qcollect-traits") (v "0.6.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1413vdk3n131c0kalirq9hadkxs8k8jnki3g9smws29dqwy3acjh")))

(define-public crate-qcollect-traits-0.6.1 (c (n "qcollect-traits") (v "0.6.1") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1fnmbskgzj7c1prfnva4khdxaa7zdhxn8agw9ixliqb5mqqf731b")))

(define-public crate-qcollect-traits-0.7.0 (c (n "qcollect-traits") (v "0.7.0") (d (list (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "199k11f80kz8fk05pg98y7pjg04m7k91z7lfjrxv39yylfcsr9rk")))

(define-public crate-qcollect-traits-0.7.3 (c (n "qcollect-traits") (v "0.7.3") (d (list (d (n "bit-vec") (r "^0.4.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1nr7yk6wncb4pspphxzisnidz8s5psim13hbia7xm4rl9fy4grkm")))

(define-public crate-qcollect-traits-0.7.4 (c (n "qcollect-traits") (v "0.7.4") (d (list (d (n "bit-vec") (r "^0.4.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0djgq38sca0zi1vzjqkr3xnm0af26g7hygk1931z7fbaz1hf05mr")))

