(define-module (crates-io qc ol qcollect) #:use-module (crates-io))

(define-public crate-qcollect-0.1.0 (c (n "qcollect") (v "0.1.0") (d (list (d (n "qindex_multi") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1sfw3jz69yscxmv36xq9jkjs4jwiw2jy39ns72hf71pjnpxrv59q")))

(define-public crate-qcollect-0.2.0 (c (n "qcollect") (v "0.2.0") (d (list (d (n "qcollect-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "qindex_multi") (r "^0.1.0") (d #t) (k 0)) (d (n "raw-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0s5bsvpc7nc030gy3c852fzx8svzbxcmm6gbzfpbm9fv9lysrp22")))

(define-public crate-qcollect-0.3.0 (c (n "qcollect") (v "0.3.0") (d (list (d (n "qcollect-traits") (r "^0.3.0") (d #t) (k 0)) (d (n "qindex_multi") (r "^0.3.0") (d #t) (k 0)) (d (n "raw-vec") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0faj78xi0rlm32xmij2dc52mlzkig0nr8w2icrrzndvjri8mqy0z")))

(define-public crate-qcollect-0.4.0 (c (n "qcollect") (v "0.4.0") (d (list (d (n "qcollect-traits") (r "^0.4") (d #t) (k 0)) (d (n "qindex_multi") (r "^0.4") (d #t) (k 0)) (d (n "raw-vec") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0rhgqcf6pgm83h4244gpnb2svprygqydz8hyjak3gw68vyd10hqy")))

(define-public crate-qcollect-0.4.1 (c (n "qcollect") (v "0.4.1") (d (list (d (n "qcollect-traits") (r "^0.4") (d #t) (k 0)) (d (n "qindex_multi") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "10gbi3qqh30b7bcz15h4p52fmsyijxarj4dpkddk3r6sds1zg1wh")))

