(define-module (crates-io qc ms qcms) #:use-module (crates-io))

(define-public crate-qcms-0.1.0 (c (n "qcms") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jhs467m2hvf68zf5xbdki36jv5ka2xk1m41ccnvh08xjyf7b7rr")))

(define-public crate-qcms-0.2.0 (c (n "qcms") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ry3x5vix5qc1axg93q7sz346sp0ljy0spj5xn9q4sylx9wqrx34") (f (quote (("iccv4-enabled") ("default") ("c_bindings" "libc"))))))

(define-public crate-qcms-0.3.0 (c (n "qcms") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1yihv9rsa0qc4mmhzp8f0xdfrnkw7q8l7kr4ivcyb9amszazrv7d") (f (quote (("neon") ("iccv4-enabled") ("default" "iccv4-enabled" "cmyk") ("cmyk") ("c_bindings" "libc"))))))

