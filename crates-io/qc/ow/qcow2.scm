(define-module (crates-io qc ow qcow2) #:use-module (crates-io))

(define-public crate-qcow2-0.1.0 (c (n "qcow2") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "lru-cache") (r "^0.0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.0") (d #t) (k 0)))) (h "16nb2bywrh1dlj8s0krp0fg5hw5r789ab477v1pdvacavgvcdn08")))

(define-public crate-qcow2-0.1.1 (c (n "qcow2") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "lru-cache") (r "^0.0.7") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.0") (d #t) (k 0)))) (h "1w56m9njspx30wr9grd4ilgqbbs2252bzr7rl7kg1d70ygd8vmq9")))

(define-public crate-qcow2-0.1.2 (c (n "qcow2") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "lru-cache") (r "^0.0.7") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.0") (d #t) (k 0)))) (h "1yiq67kxch56vp9wdy4w1nif49jn17lw7sixgnaf1qv3nfjxl2dj")))

