(define-module (crates-io qc ow qcow) #:use-module (crates-io))

(define-public crate-qcow-1.0.0 (c (n "qcow") (v "1.0.0") (d (list (d (n "binread") (r "^2.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (f (quote ("zlib-ng-compat"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "0mbjsfy6pazmwbzwxmmd8g4ljdh8zlry5qahrasbdw96njw4jmx0")))

(define-public crate-qcow-1.1.0 (c (n "qcow") (v "1.1.0") (d (list (d (n "binread") (r "^2.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (f (quote ("zlib-ng-compat"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1yc6fm2n14kas37109zvr7qnay48sw8h6lir25qz40rgsamsyln3")))

(define-public crate-qcow-1.2.0 (c (n "qcow") (v "1.2.0") (d (list (d (n "binread") (r "^2.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (f (quote ("zlib-ng-compat"))) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd") (r "^0.9.0") (d #t) (k 0)))) (h "1sirkj2qb0q72vp157gqm9w1zccyx97y7wha1y0lgjmdr47pxd3s")))

