(define-module (crates-io qc ow qcow2-fuse) #:use-module (crates-io))

(define-public crate-qcow2-fuse-0.1.0 (c (n "qcow2-fuse") (v "0.1.0") (d (list (d (n "daemonize") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "fuse") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.0") (d #t) (k 0)) (d (n "qcow2") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "006pqs65qswlaffac62rjbxfwsg8fws3v2px85bkknj2wavnlr0j")))

(define-public crate-qcow2-fuse-0.1.1 (c (n "qcow2-fuse") (v "0.1.1") (d (list (d (n "daemonize") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3.4") (d #t) (k 0)) (d (n "fuse") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "positioned-io") (r "^0.2.0") (d #t) (k 0)) (d (n "qcow2") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0cz63v2sf7j0lipxajzbyv93khkknmd2db58n5wg96bgz5lnzalm")))

