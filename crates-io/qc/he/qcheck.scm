(define-module (crates-io qc he qcheck) #:use-module (crates-io))

(define-public crate-qcheck-1.0.0 (c (n "qcheck") (v "1.0.0") (d (list (d (n "qcheck-macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng"))) (k 0)))) (h "1w7knnk2zhhczmymh6hdddsld4vlvmm6lpn930nxclfs891bsfdl")))

