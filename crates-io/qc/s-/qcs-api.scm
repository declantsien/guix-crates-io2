(define-module (crates-io qc s- qcs-api) #:use-module (crates-io))

(define-public crate-qcs-api-0.0.1 (c (n "qcs-api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "00mr9cq9r3slfrbmqrib8skmgc988rn1mny2mi70y7hm5293qhg8")))

(define-public crate-qcs-api-0.0.2 (c (n "qcs-api") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1sxlgkavx28cwgxyxvghjbvylfrchaywryvqgb604zgj5xjjgd5f")))

(define-public crate-qcs-api-0.0.3 (c (n "qcs-api") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1zm7l0nng44b1kxsrmn4py825q0c851ph2z7870lsk6sv4hsx57k")))

(define-public crate-qcs-api-0.0.4 (c (n "qcs-api") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0ymradb3bk42jhk9p1g2gpvs6m4072238s1321hx6yznbkgd0g8q")))

(define-public crate-qcs-api-0.0.5 (c (n "qcs-api") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "002xkd95y6ca3yvvifxrhm9zsqpn9vgf21ij5br5knzr29x1jra0")))

(define-public crate-qcs-api-0.0.6 (c (n "qcs-api") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1kbm5ma8bzpkkhrl4g4g5srx71y6h6wdjkwmc6j1xb33wx9pf3ar")))

(define-public crate-qcs-api-0.0.7 (c (n "qcs-api") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1zhd1gmvdknxdjx3jrfdwqgg8iaknlf9wzp99wdh7bqajshbbfdw")))

(define-public crate-qcs-api-0.2.0 (c (n "qcs-api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0m2p7izxf60a6hxj209a5hxyb9im0s3kj6c19q4scyp5si596sgr")))

(define-public crate-qcs-api-0.2.1 (c (n "qcs-api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("json" "multipart" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.140") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0rzjy73bq6lah92qajbim1pv3718n8m45zl93ysdbqr6zmwk8bvv")))

