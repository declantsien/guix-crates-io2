(define-module (crates-io qc gp qcgpu) #:use-module (crates-io))

(define-public crate-qcgpu-0.1.0 (c (n "qcgpu") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.2") (d #t) (k 2)) (d (n "libquantum-patched") (r "^0.1.3") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.43") (d #t) (k 0)) (d (n "ocl") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "12lzw3p94nwry1z10axcj06m0ncxa4lkap2y49khy0bcc54zhgvl") (f (quote (("default") ("decoherence"))))))

