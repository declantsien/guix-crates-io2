(define-module (crates-io qc on qcontext-derive) #:use-module (crates-io))

(define-public crate-qcontext-derive-0.1.0 (c (n "qcontext-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xd74d3z457j5dfhg2xsvi4zd432qq46j5j3r7lslsq9c06hm2ax")))

(define-public crate-qcontext-derive-0.2.0 (c (n "qcontext-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1fwkff43cw5q92ssywvmnnj0h96dzwvprm70biqzadifs8jbsd59")))

(define-public crate-qcontext-derive-0.3.0 (c (n "qcontext-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "199z77cgdr3sajnmaxjgf7bsfhvzsbkhr43hy3nnadahvhjc41q4")))

(define-public crate-qcontext-derive-0.4.0 (c (n "qcontext-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0x3rfvvj5m0iyszf18g04g7lc3fgi9j0dixaw9v8xwb396c5h4d4")))

