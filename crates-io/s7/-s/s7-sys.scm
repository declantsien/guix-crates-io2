(define-module (crates-io s7 -s s7-sys) #:use-module (crates-io))

(define-public crate-s7-sys-0.1.0 (c (n "s7-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0c6sy4g7gpxmnnpnqdl00k1mllff7b0czz3hrwzpp0050kjrynd1") (y #t)))

(define-public crate-s7-sys-0.1.1 (c (n "s7-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0wl4fmdxb8z4xmb9kq7hfg34dppryi7ca1847wr5rag6s8ihdhcw")))

(define-public crate-s7-sys-0.1.2 (c (n "s7-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0swza9armnn0wai0jhjksv8n3w60ky8q7p9z2m6kb1lg1a2a30ih")))

