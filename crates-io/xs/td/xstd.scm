(define-module (crates-io xs td xstd) #:use-module (crates-io))

(define-public crate-xstd-0.1.0 (c (n "xstd") (v "0.1.0") (h "0hcxzllgylxvvi4j7gs29nfa2pwdshkyaqxd19papzzh346mmsx5")))

(define-public crate-xstd-0.2.0 (c (n "xstd") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (o #t) (d #t) (k 0)))) (h "18nl9x5ibxd9m4y3yxyrrwbgw7gif7yz8jjmifdhnsrv7nm5zfch") (f (quote (("stream" "futures-core" "pin-utils") ("all" "stream"))))))

