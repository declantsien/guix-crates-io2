(define-module (crates-io xs s- xss-probe) #:use-module (crates-io))

(define-public crate-xss-probe-0.1.0 (c (n "xss-probe") (v "0.1.0") (h "01xznailg0l7hvgcx4dcczbs48qb2af7c9l10hj7gmgcyx8qq0kh")))

(define-public crate-xss-probe-0.2.0 (c (n "xss-probe") (v "0.2.0") (h "1fn5969zndnk6fs7a0z17ykf4y8f7g5fxsgipf3cw5qj3s94wgwi")))

