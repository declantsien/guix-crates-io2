(define-module (crates-io xs he xshell-venv) #:use-module (crates-io))

(define-public crate-xshell-venv-0.1.0 (c (n "xshell-venv") (v "0.1.0") (d (list (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "0hg1qwcl8rxdka7rbgs30ylxmqv3s8985p1kdg2iih1c7myjp9cn")))

(define-public crate-xshell-venv-0.2.0 (c (n "xshell-venv") (v "0.2.0") (d (list (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "1p05zj0013zdr9182l7l24is8f9drf9wplqr63k3dilvkcnglzgg")))

(define-public crate-xshell-venv-0.3.0 (c (n "xshell-venv") (v "0.3.0") (d (list (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "182gjvkh143p89z3av7s96afqg47bckvyy8lb8dfyq048h90fmwn")))

(define-public crate-xshell-venv-1.0.0 (c (n "xshell-venv") (v "1.0.0") (d (list (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "0v2636hrgcnz13l9ga914nmgh3pxq1s7sz8z6s4s50qgnnnhiaxc")))

(define-public crate-xshell-venv-1.1.0 (c (n "xshell-venv") (v "1.1.0") (d (list (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "0z4im04zkvyqqk7970kmkb3gf697xxrw1wjy5cwhvm5z6s92wg7l")))

(define-public crate-xshell-venv-1.2.0 (c (n "xshell-venv") (v "1.2.0") (d (list (d (n "fd-lock") (r "^3.0.10") (d #t) (k 0)) (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "0k3xsw1q91kxykfnpk5n805gxwv0y83l335f2a28fdqnnd8mrcv4")))

(define-public crate-xshell-venv-1.3.0 (c (n "xshell-venv") (v "1.3.0") (d (list (d (n "fd-lock") (r "^4.0.0") (d #t) (k 0)) (d (n "xshell") (r "^0.2") (d #t) (k 0)))) (h "00jbvhijqbfyirw9k957ly5d03wvpi5j2414agg6wwdrsqjzncvr")))

