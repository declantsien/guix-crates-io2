(define-module (crates-io xs he xshell) #:use-module (crates-io))

(define-public crate-xshell-0.1.0 (c (n "xshell") (v "0.1.0") (d (list (d (n "xshell-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1d6ascdjj8ih0mf8fjnf9fg2rawqcmqxni6v03dkmdxa5xppazqz")))

(define-public crate-xshell-0.1.2 (c (n "xshell") (v "0.1.2") (d (list (d (n "xshell-macros") (r "^0.1.2") (d #t) (k 0)))) (h "1qjl1p5xlv8hs799xpv29jgj3aapfh76apszm6pf6cdn613w6liy")))

(define-public crate-xshell-0.1.3 (c (n "xshell") (v "0.1.3") (d (list (d (n "xshell-macros") (r "^0.1.3") (d #t) (k 0)))) (h "0r5smq07d4r32jszvnp5a1x0ffbsgm7mqgrhzy3dk3wy2p961bxf")))

(define-public crate-xshell-0.1.4 (c (n "xshell") (v "0.1.4") (d (list (d (n "xshell-macros") (r "^0.1.4") (d #t) (k 0)))) (h "119ciwjp7h0zdjs6krx94jssx32bifn05dk7qngkjka6d3zd392b")))

(define-public crate-xshell-0.1.5 (c (n "xshell") (v "0.1.5") (d (list (d (n "xshell-macros") (r "^0.1.5") (d #t) (k 0)))) (h "1bnsw0ihcyw1n3lb17vxbdi9yhfkxx6r0i2f3abk9ssq278hdpv1")))

(define-public crate-xshell-0.1.6 (c (n "xshell") (v "0.1.6") (d (list (d (n "xshell-macros") (r "^0.1.6") (d #t) (k 0)))) (h "0zf3r6jyas77jj1mx6i4kqp406l1n3a70y1v8yqfccr2pg6bz6ry")))

(define-public crate-xshell-0.1.7 (c (n "xshell") (v "0.1.7") (d (list (d (n "xshell-macros") (r "=0.1.7") (d #t) (k 0)))) (h "16073m6bva8xjphhfvfhmv357flaf8sj9ndpj0257bpji38jh1zd")))

(define-public crate-xshell-0.1.8 (c (n "xshell") (v "0.1.8") (d (list (d (n "xshell-macros") (r "=0.1.8") (d #t) (k 0)))) (h "1q4dhw1yfvkyjx34g2zcdzqgn3m83gp4hbzj1a63x86f63g3wdzd")))

(define-public crate-xshell-0.1.9 (c (n "xshell") (v "0.1.9") (d (list (d (n "xshell-macros") (r "=0.1.9") (d #t) (k 0)))) (h "1hf0hjajzdclf38p70iayw73mixlik3lllawx9q8yg25g0i1063g")))

(define-public crate-xshell-0.1.10 (c (n "xshell") (v "0.1.10") (d (list (d (n "xshell-macros") (r "=0.1.10") (d #t) (k 0)))) (h "0wsl85rkg9shx3sf0dkd4qf47f2ywscql8w9g0frk7rgd1y229da")))

(define-public crate-xshell-0.1.11 (c (n "xshell") (v "0.1.11") (d (list (d (n "xshell-macros") (r "=0.1.11") (d #t) (k 0)))) (h "0sfcpi7gpj4z48l77dhrvrmsz3qbks7l2y93dh9nq6qfa5fn9822")))

(define-public crate-xshell-0.1.12 (c (n "xshell") (v "0.1.12") (d (list (d (n "xshell-macros") (r "=0.1.12") (d #t) (k 0)))) (h "1gy6xvj6w3wh8vryfnpch0l3s785rq9rcm044rdkxiv7wzar6gsf")))

(define-public crate-xshell-0.1.13 (c (n "xshell") (v "0.1.13") (d (list (d (n "xshell-macros") (r "=0.1.13") (d #t) (k 0)))) (h "1iilg7cjgz3342f3na500dp3c371jk198qh053kfy57b84dvn5gb")))

(define-public crate-xshell-0.1.14 (c (n "xshell") (v "0.1.14") (d (list (d (n "xshell-macros") (r "=0.1.14") (d #t) (k 0)))) (h "08vwavxq7dnxhk0zql6caqfjyyqsn3j0d1p8frmih3hm3cpkch66")))

(define-public crate-xshell-0.1.15 (c (n "xshell") (v "0.1.15") (d (list (d (n "xshell-macros") (r "=0.1.15") (d #t) (k 0)))) (h "0rqqc083mj6lghmm653fi3f25dfvvvdgkgniaicrf83rpxw6p807")))

(define-public crate-xshell-0.1.16 (c (n "xshell") (v "0.1.16") (d (list (d (n "xshell-macros") (r "=0.1.16") (d #t) (k 0)))) (h "07fpnvpm2dqbc2qmydz13lq6qpg9cdkaanlbc5bp7ydikmm7vi6w")))

(define-public crate-xshell-0.1.17 (c (n "xshell") (v "0.1.17") (d (list (d (n "xshell-macros") (r "=0.1.17") (d #t) (k 0)))) (h "054zbbs1sjm1azhi14avyfjn0g4hbzd7yk9xaw2xlmjc4hsj1bga")))

(define-public crate-xshell-0.2.0-pre.1 (c (n "xshell") (v "0.2.0-pre.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.0-pre.1") (d #t) (k 0)))) (h "0xrwsqx30q32iqwrwr6papdxcglz8bhc324j01gzr460ahi47384") (r "1.59")))

(define-public crate-xshell-0.2.0 (c (n "xshell") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.0") (d #t) (k 0)))) (h "1glnrimqq0am4b23dzs3rkgidga4afs4sjdnmhm8m6g21fwwlcik") (r "1.59")))

(define-public crate-xshell-0.2.1 (c (n "xshell") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.1") (d #t) (k 0)))) (h "1al6vimvq6cbq17fa4pj2gkss9lm2ybwgvjgy6zklvc8d5v43128") (r "1.59")))

(define-public crate-xshell-0.2.2 (c (n "xshell") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.2") (d #t) (k 0)))) (h "10d9xi5751p3bjpb8dfxxwxrplfn5m1b6l8qwjqk8ln8qmyhjivd") (r "1.59")))

(define-public crate-xshell-0.2.3 (c (n "xshell") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.3") (d #t) (k 0)))) (h "147yvh1ai2ak0dp6lz9f853pbh45cmy3jj22k97cy5kv7adh6b4n") (r "1.59")))

(define-public crate-xshell-0.2.5 (c (n "xshell") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.5") (d #t) (k 0)))) (h "1b93f33r723051116m3cypx2xn4fslk7dbbi9hxkan750gz0f8ff") (r "1.59")))

(define-public crate-xshell-0.2.6 (c (n "xshell") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "xshell-macros") (r "=0.2.6") (d #t) (k 0)))) (h "0dv4igym5whcr8fws0afmhq414a1c38x7a2ln38yyfg7xa3apc3d") (r "1.59")))

