(define-module (crates-io xs ha xshade-parser) #:use-module (crates-io))

(define-public crate-xshade-parser-0.1.0 (c (n "xshade-parser") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "nom_locate") (r "^0.3.0") (d #t) (k 0)))) (h "14dkrxxp6h8n3zy3m6ahmrmq73skc3l6lsbhd29f1w6d5yjpvj7v")))

(define-public crate-xshade-parser-0.1.1 (c (n "xshade-parser") (v "0.1.1") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "nom_locate") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fcjvygrzvgc2ndw3h5m1l785pi6ygyn0950j1van46awav2pzbi")))

(define-public crate-xshade-parser-0.1.2 (c (n "xshade-parser") (v "0.1.2") (d (list (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "nom_locate") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mpbkrigzjalgsb34mbisk67zlmp4amvc913b3jpdx2051kll17w")))

