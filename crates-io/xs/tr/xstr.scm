(define-module (crates-io xs tr xstr) #:use-module (crates-io))

(define-public crate-xstr-0.0.1 (c (n "xstr") (v "0.0.1") (h "0jiclwbg656q2hrffrnpfyqh93gavyf22v2cvzrv31k2g90f6z35")))

(define-public crate-xstr-0.0.2 (c (n "xstr") (v "0.0.2") (h "06zkrvlzm3kx024y0jzgk159fvdnj0h49rsaradwxg7cba2y9sn4")))

(define-public crate-xstr-0.0.3 (c (n "xstr") (v "0.0.3") (h "1cyx2ka5f78lzh5hsj715j4i5byrg2mmbjda0p51jlkj28d24m4k")))

(define-public crate-xstr-0.1.0 (c (n "xstr") (v "0.1.0") (h "1lijnp20y6vl86x51rmk5gwjmj7bay76v2vrar3d6lwrggf1qs26")))

(define-public crate-xstr-0.1.1 (c (n "xstr") (v "0.1.1") (h "1dlacwpyc008qc90bbf04fk8sffdlr20wcv0xdw3abmc6n0jnfx2")))

(define-public crate-xstr-0.1.2 (c (n "xstr") (v "0.1.2") (h "1gxc2ysld8j6bp480sw1lmkw3pkps3xhnlr1azdlqdd9r1jjqsl6")))

(define-public crate-xstr-0.1.3 (c (n "xstr") (v "0.1.3") (h "1p1zlnf01pnqq4s6fhr0k692yr4901gglylfs17d82l058786xpf")))

(define-public crate-xstr-0.1.4 (c (n "xstr") (v "0.1.4") (h "0aaac26b77vzfkjpkbwbj5c3wy8xxkmz018fzf0b2li7nh838ng3")))

(define-public crate-xstr-0.1.5 (c (n "xstr") (v "0.1.5") (h "0n456q9y8dv1b3jasb5a75bkcp25gbpgki4cxd323v5l24c3x7yr")))

(define-public crate-xstr-0.1.6 (c (n "xstr") (v "0.1.6") (h "0w3p60yvh3jpck9qf38m0fwm0skw2ihz8knmxq8pzm851qz3z83f")))

(define-public crate-xstr-0.1.7 (c (n "xstr") (v "0.1.7") (h "1p58h6wj9z45na8shcf2ym7bv8ki2z0xlf8rjdyzgqdrdw6p3jdg")))

(define-public crate-xstr-0.1.8 (c (n "xstr") (v "0.1.8") (h "07ld53fcc1px9bx43yaj1pfj1mygkqlm9w764r51cvzsqygynd4a")))

(define-public crate-xstr-0.1.9 (c (n "xstr") (v "0.1.9") (h "1wi87vd1q4dbl2g6540j2wmdjysz79swsmkmmjc737n23bjk69iy")))

(define-public crate-xstr-0.1.10 (c (n "xstr") (v "0.1.10") (h "1da00zd56jw5114wwwadwarr46r4w6rgd9335hy9pl98qzqx5cdk")))

