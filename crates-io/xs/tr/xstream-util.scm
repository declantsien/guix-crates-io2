(define-module (crates-io xs tr xstream-util) #:use-module (crates-io))

(define-public crate-xstream-util-0.1.0 (c (n "xstream-util") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1xlcxw0rlnhhnq7r4qczjjbi2rcx54x1g43hr4dqfpf528w2dar0")))

(define-public crate-xstream-util-0.1.1 (c (n "xstream-util") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "0kmwq21q2py3j70qilx2likmyy82z8rwcbrzpcjn5mqs4iwj37ww")))

(define-public crate-xstream-util-1.0.0 (c (n "xstream-util") (v "1.0.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)))) (h "1jpzw84mw2g1hr5sp4144h2jfs8crjq5kxw4bkjz1v7lxvjmrlhs")))

(define-public crate-xstream-util-1.1.0 (c (n "xstream-util") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1imvjqhd7lhyhkklx8m9lpw7iyfpab2bk80bzlkdj2nw395jj3sn")))

(define-public crate-xstream-util-1.2.0 (c (n "xstream-util") (v "1.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1zq3s2aibjgbkbqlz7s0cam8yx9sf9zxvnjrgvph5p69x2k3r0db")))

(define-public crate-xstream-util-1.2.1 (c (n "xstream-util") (v "1.2.1") (d (list (d (n "clap") (r "^2.34") (d #t) (k 0)))) (h "1w208ndmc7zv0x5yfngc0gi1dk27jqpkschd1jd03xfz0xgss28b")))

(define-public crate-xstream-util-1.3.0 (c (n "xstream-util") (v "1.3.0") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14na0jbajfg6yxfqvkv4h4zdkrr5dyj69f1dqk2hff0rhasj0mll") (f (quote (("binary" "clap"))))))

(define-public crate-xstream-util-1.3.1 (c (n "xstream-util") (v "1.3.1") (d (list (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07qxw9vf0khrsmizab7f0vagb5kxab23wigrsq23y1gw1k6hca6z") (f (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1.3.2 (c (n "xstream-util") (v "1.3.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)))) (h "110wgwnzb3vshlzpks2hd3bxp2a88wi6xwpinjiflxxdkmsvxq54") (f (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1.3.3 (c (n "xstream-util") (v "1.3.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)))) (h "1ld9i4ixxp5v763mqr1wmlzd562hh5k2gmlbayf6vy9ck2pj09sh") (f (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1.3.4 (c (n "xstream-util") (v "1.3.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)))) (h "15jcrzgh4glcvbjp1pb6lh980dqrlc5r1yq6axmry15yqvlpbjgq") (f (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-2.0.0 (c (n "xstream-util") (v "2.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (o #t) (d #t) (k 0)))) (h "1xli6g01rb4rf8v1yxil7j033wiicldizpck1kbdnx389hrd4i33") (f (quote (("default" "binary") ("binary" "clap"))))))

