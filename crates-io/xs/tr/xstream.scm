(define-module (crates-io xs tr xstream) #:use-module (crates-io))

(define-public crate-xstream-0.0.0 (c (n "xstream") (v "0.0.0") (h "0391ikgaadzwrynp3mwc7a9ca97m4xwdsbbmal11p9kzs508k70a") (y #t)))

(define-public crate-xstream-0.1.0 (c (n "xstream") (v "0.1.0") (d (list (d (n "clear_on_drop") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "data-encoding") (r "^2.0") (d #t) (k 2)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)) (d (n "hkdf") (r "^0.3") (d #t) (k 0)) (d (n "miscreant") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.7") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.1") (d #t) (k 0)))) (h "06w5prlfcpznaxw24zvi5aylm56f0nvc1i1bwf747kkbca7ly3sj") (y #t)))

(define-public crate-xstream-0.1.1 (c (n "xstream") (v "0.1.1") (h "1yfsp7v33s7xfqja64f83xax4wsba1bcsa3g953q7xggg2ivcxby") (y #t)))

