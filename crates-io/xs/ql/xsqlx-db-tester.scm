(define-module (crates-io xs ql xsqlx-db-tester) #:use-module (crates-io))

(define-public crate-xsqlx-db-tester-0.1.0 (c (n "xsqlx-db-tester") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0q9lnglf1b8x69g4j4qv668hhzyhwivagndvafi5d96l71slwd3x")))

(define-public crate-xsqlx-db-tester-0.1.1 (c (n "xsqlx-db-tester") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "03y7d99kvpqy93211b22f8jybpjaqhax8678qnlgwn6q2904ccbl")))

(define-public crate-xsqlx-db-tester-0.1.2 (c (n "xsqlx-db-tester") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "07an5bg1wlk3bl6izw6f185c0hjz0jri9lg8mzqiq2pil893044r")))

(define-public crate-xsqlx-db-tester-0.1.3 (c (n "xsqlx-db-tester") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f3kk62asirhvhh5wgs7v8azrbz72zbhpqwi203cw54lk224y152")))

