(define-module (crates-io xs pf xspf) #:use-module (crates-io))

(define-public crate-xspf-0.3.3 (c (n "xspf") (v "0.3.3") (d (list (d (n "xml") (r "^0.8.10") (d #t) (k 0)))) (h "1agbm29vl6aszsw4ia0wx03hx4ib2nrjlfj0d0hysjp7vbxbwbww")))

(define-public crate-xspf-0.4.0 (c (n "xspf") (v "0.4.0") (d (list (d (n "xml") (r "^0.8.10") (d #t) (k 0)))) (h "1yi2vx34rbhvzd8iarvqw0ahssspl2d8q3fnnba28dsinlcnq5hp") (f (quote (("setters") ("print") ("parse") ("default" "print" "parse" "setters"))))))

