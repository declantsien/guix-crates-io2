(define-module (crates-io xs rf xsrf) #:use-module (crates-io))

(define-public crate-xsrf-0.1.0 (c (n "xsrf") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "subtle") (r "^2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xaczch72803nrs9fa626ib72pikqa8wdsxmhi2k4jyd9a4pwl7r")))

