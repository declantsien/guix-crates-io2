(define-module (crates-io xs et xsettings) #:use-module (crates-io))

(define-public crate-xsettings-0.1.0 (c (n "xsettings") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11") (r "^2.3") (f (quote ("xlib"))) (d #t) (k 0)))) (h "0k9acqngpsnaxsipg7sjmddbyz4mr8ixhd0jxmw9d91jwsh123wp")))

(define-public crate-xsettings-0.2.0 (c (n "xsettings") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "x11-dl") (r "^2.2") (d #t) (k 0)))) (h "16d443kghv3z503gvfi8ama5daa2qp200d7qiqv70xwgnrxpz32d")))

