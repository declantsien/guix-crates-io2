(define-module (crates-io xs en xsender) #:use-module (crates-io))

(define-public crate-xsender-0.1.0 (c (n "xsender") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "xml") (r "^0.8.10") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1c062nywcrajd5smzich6kx8r3c4dm5b7618ij9iabj3lrmpm14r")))

