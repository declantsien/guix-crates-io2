(define-module (crates-io xs wa xswag-base) #:use-module (crates-io))

(define-public crate-xswag-base-0.2.0 (c (n "xswag-base") (v "0.2.0") (d (list (d (n "term-painter") (r "^0.2") (d #t) (k 0)))) (h "0a3qpjp4wp362v506mw2vi3ckqr11z3j8fqiysrqi9s62c4l97lz")))

(define-public crate-xswag-base-0.3.0 (c (n "xswag-base") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term-painter") (r "^0.2") (d #t) (k 0)))) (h "1zinpyacdqz63isa7bwc4hq0lgxdhjnhhn10sv8z6nwqi2xm8xdq")))

(define-public crate-xswag-base-0.3.1 (c (n "xswag-base") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "term-painter") (r "^0.2") (d #t) (k 0)))) (h "0xdlgi05c37qx7jqaxvvvpq2hxxqm77s536n6s0p9mi4gk543cd9")))

