(define-module (crates-io xs #{23}# xs233-sys) #:use-module (crates-io))

(define-public crate-xs233-sys-0.1.0 (c (n "xs233-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "06hwdydkjd612r8bzh91yqrlz00p10m405bp1gdppqb7viaga37s")))

(define-public crate-xs233-sys-0.2.0 (c (n "xs233-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1hc849azwvkqiv69n29n3dzyiq4mnyc6n2vhdrkkyhgf2zn974f6") (f (quote (("sse41") ("pclmul") ("avx2"))))))

