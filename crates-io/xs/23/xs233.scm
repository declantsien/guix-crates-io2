(define-module (crates-io xs #{23}# xs233) #:use-module (crates-io))

(define-public crate-xs233-0.1.0 (c (n "xs233") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "xs233-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0q76yay3rab0r6fc80k587g30xfllwb30yxp2y07y4v5f6hwg6ap")))

(define-public crate-xs233-0.1.1 (c (n "xs233") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "xs233-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1nnzhkv0p1cblm7f70hi89y2dldxyl39rjhzchs1fc95dgb4465s")))

(define-public crate-xs233-0.1.2 (c (n "xs233") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "xs233-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1l19c3wnyhfnqriwfcng0zf7zf9ipiqlkv4bq867zzcdxl8a98j3")))

(define-public crate-xs233-0.2.0 (c (n "xs233") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "xs233-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19ig03qw20gkzx5qpshp7754sqyd8jmi2nfnn5017hbb1wwqpg38")))

(define-public crate-xs233-0.3.0 (c (n "xs233") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "subtle") (r "^2.5.0") (d #t) (k 0)) (d (n "xs233-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0582pacx0h4rz3bz6b1kfb41c0paj8as8snnhy1lk6p2hxgpzhxq") (f (quote (("sse41" "xs233-sys/sse41") ("pclmul" "xs233-sys/pclmul") ("avx2" "xs233-sys/avx2"))))))

