(define-module (crates-io xs l- xsl-rs) #:use-module (crates-io))

(define-public crate-xsl-rs-0.1.0 (c (n "xsl-rs") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 2)) (d (n "libc-print") (r "^0.1.22") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1n08r3z9l8b3hjphj0r5s7kly15zsgvnbb4rq4pb6kbvmxnq6xfj") (f (quote (("std") ("default" "allocator-api2")))) (s 2) (e (quote (("allocator-api2" "dep:allocator-api2"))))))

