(define-module (crates-io fq -f fq-filter-reads) #:use-module (crates-io))

(define-public crate-fq-filter-reads-0.1.0 (c (n "fq-filter-reads") (v "0.1.0") (d (list (d (n "bio") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0f7n9wdnzzzlw4ka5z409vfq08bpiyh203l9si24za194l45jnzh")))

