(define-module (crates-io fq dn fqdn-trie) #:use-module (crates-io))

(define-public crate-fqdn-trie-0.1.0 (c (n "fqdn-trie") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "cpu-time") (r "^1") (d #t) (k 2)) (d (n "fqdn") (r "^0.2.2") (d #t) (k 0)))) (h "0i8cb6zimpgb807skzv15p3j1621izhq722304kb17rkb29xh8r5") (f (quote (("graphviz"))))))

(define-public crate-fqdn-trie-0.3.0 (c (n "fqdn-trie") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "cpu-time") (r "^1") (d #t) (k 2)) (d (n "fqdn") (r "^0.3.0") (d #t) (k 0)))) (h "1y9xwas1csqlbhq95x599rvf3g75cwghrvdi6vl7y1d0hlrxcxr6") (f (quote (("graphviz")))) (y #t)))

(define-public crate-fqdn-trie-0.4.0 (c (n "fqdn-trie") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "cpu-time") (r "^1") (d #t) (k 2)) (d (n "fqdn") (r "^0.3.0") (d #t) (k 0)))) (h "03yjppf2kmlhvq6008v9war1h21ji76ibk3jrm9p7k4ycgxdigya") (f (quote (("graphviz")))) (y #t)))

(define-public crate-fqdn-trie-0.4.1 (c (n "fqdn-trie") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "cpu-time") (r "^1") (d #t) (k 2)) (d (n "fqdn") (r "^0.3") (d #t) (k 0)))) (h "0dz5i060xx8748c99fl6csz8wk28xb6ldff3nyxh0v5ajnj96b78") (f (quote (("graphviz"))))))

(define-public crate-fqdn-trie-0.4.2 (c (n "fqdn-trie") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (k 2)) (d (n "cpu-time") (r "^1") (d #t) (k 2)) (d (n "fqdn") (r "^0.3") (d #t) (k 0)))) (h "00p4x6h1v8gjl1dfhs6j20hc60ins1f3myn0k7xpq13nc8z9n603") (f (quote (("graphviz"))))))

