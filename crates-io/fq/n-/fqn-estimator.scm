(define-module (crates-io fq n- fqn-estimator) #:use-module (crates-io))

(define-public crate-fqn-estimator-0.0.1 (c (n "fqn-estimator") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "0g5121057yyyba2w0gm68b000v7rnjjprh3h64frhqdbjzndk4d4") (f (quote (("default" "num-traits")))) (s 2) (e (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.1.0 (c (n "fqn-estimator") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "0g92y6glr8shh234cc7vh9q5f693rnhqmgs4pa3labx8bq21k134") (f (quote (("default" "num-traits")))) (s 2) (e (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.2.0 (c (n "fqn-estimator") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "0isisqp6ylcnvlmdlisypqlmzfbnlas17ycpwzw13kdcys95s0s2") (f (quote (("default" "num-traits")))) (s 2) (e (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.2.1 (c (n "fqn-estimator") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.18") (o #t) (d #t) (k 0)))) (h "1xijqnyd74i6s5h6cidnphqlniziq22g941ssa4rq4hvs8ai1h5a") (f (quote (("default" "num-traits")))) (s 2) (e (quote (("num-traits" "dep:num-traits"))))))

