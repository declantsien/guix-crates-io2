(define-module (crates-io fq l_ fql_server) #:use-module (crates-io))

(define-public crate-fql_server-0.1.0 (c (n "fql_server") (v "0.1.0") (d (list (d (n "configparser") (r "^2.0.0") (d #t) (k 0)) (d (n "file_sql") (r "^0.2.1") (d #t) (k 0)) (d (n "nickel") (r "^0.11.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0gpfwmhvhqamllxmxnpi11jf7f5k8h9i0n5qzhcpclz50x8niv3n")))

