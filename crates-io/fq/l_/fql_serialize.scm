(define-module (crates-io fq l_ fql_serialize) #:use-module (crates-io))

(define-public crate-fql_serialize-0.2.0 (c (n "fql_serialize") (v "0.2.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0x1y9bzpp12609cp49cqzynmn0x4b298r35bkhm893qh3a3nag0r")))

(define-public crate-fql_serialize-0.2.4 (c (n "fql_serialize") (v "0.2.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0yly7i6748a5m62z8hjz8pz5zzfc2j0fqsi28w7hmymr9fih4h9f")))

(define-public crate-fql_serialize-0.2.6 (c (n "fql_serialize") (v "0.2.6") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "13kdmn2n1bld4ymgqzyasl3mzc0j68qpsxykk54dg125igf3ra9c")))

