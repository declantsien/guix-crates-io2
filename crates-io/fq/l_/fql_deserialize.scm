(define-module (crates-io fq l_ fql_deserialize) #:use-module (crates-io))

(define-public crate-fql_deserialize-0.2.0 (c (n "fql_deserialize") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "151swgxvmgl1w1y0pq09nl780rlc8iq9j9f7d3inqd13n9vl41nc")))

(define-public crate-fql_deserialize-0.2.7 (c (n "fql_deserialize") (v "0.2.7") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13m24m1q6fc1zs5cml31a8s3d3cs6zkv6xpl89pyf79kglkblf85")))

(define-public crate-fql_deserialize-0.2.8 (c (n "fql_deserialize") (v "0.2.8") (d (list (d (n "indexmap") (r "^1.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fbigag5kalqcpav7ga4048alran7imd0yh6k14y1617wb9ps431")))

