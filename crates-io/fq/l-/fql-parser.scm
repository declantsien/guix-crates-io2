(define-module (crates-io fq l- fql-parser) #:use-module (crates-io))

(define-public crate-fql-parser-0.1.2 (c (n "fql-parser") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1dywyggkb4smp9lijzzxalskl697annziki0p7xrvdmhwqicqi68") (y #t)))

(define-public crate-fql-parser-0.1.3 (c (n "fql-parser") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1zd218dipcwq5z5gljzkp0q2wbp2ff6qhqkyg0ck03hfj4xsd4gj")))

(define-public crate-fql-parser-0.1.4 (c (n "fql-parser") (v "0.1.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "17j17b14z656mbppzpc9dr9zwkzbbsfd4bps0z16fyf459nn98mc")))

(define-public crate-fql-parser-0.1.5 (c (n "fql-parser") (v "0.1.5") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1503ilvj5p4wsf4cn26jvx8vj75idqzxj7fw09mzvg7zz8s56rl7")))

(define-public crate-fql-parser-0.1.6 (c (n "fql-parser") (v "0.1.6") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "12sawn2wq8h1nsfsrv3ki5ynd4n0sr5sjifsdzk8ccim1g60b1b4")))

(define-public crate-fql-parser-0.1.7 (c (n "fql-parser") (v "0.1.7") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "1sbdyc0fvl2y9frxjy7zllbw5zj4r8d9sbrdnaqd9zvzavcmcz1d")))

(define-public crate-fql-parser-0.1.8 (c (n "fql-parser") (v "0.1.8") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "092dcyzady4frarg04wg3l774h8kvyarfnay5knxpwh55m2h8f7m")))

(define-public crate-fql-parser-0.2.0 (c (n "fql-parser") (v "0.2.0") (d (list (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.0") (d #t) (k 0)))) (h "1hvzd0rpskimcck5k2q94i1rc5r2k6ypn59all3xjmkg2j22gfpb")))

(define-public crate-fql-parser-0.2.1 (c (n "fql-parser") (v "0.2.1") (d (list (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0jzdy2gq2ag64dfzj7pw59n9mhby8y04pm5zzbcx6d6y7yhhyn4m")))

(define-public crate-fql-parser-0.2.2 (c (n "fql-parser") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0cxnwnnbfiadds44gqx80s6xiwrm63fdiqhq73hdkfl1wb00n5dd")))

(define-public crate-fql-parser-0.2.3 (c (n "fql-parser") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1bbg67fj2mv3c4lb5ywcvax19qsxklssdaxp5yr0z7wz68rb8g6a")))

(define-public crate-fql-parser-0.2.5 (c (n "fql-parser") (v "0.2.5") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1vnv4gv7g0cqx5vqxwg1k7pdqkb59hvmrmicq3n1l527x4cx69v1")))

(define-public crate-fql-parser-0.2.6 (c (n "fql-parser") (v "0.2.6") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.0") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1zwhv4vhdrlkji9s8di6ws37s96ghy36kv9jcs1ag5rr32f001mv")))

(define-public crate-fql-parser-0.2.7 (c (n "fql-parser") (v "0.2.7") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.7") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "1jp5hqdi4qgb5mmd027rl3x1c8ivyms77s7nyl7rrjigzbzs5lr3")))

(define-public crate-fql-parser-0.2.8 (c (n "fql-parser") (v "0.2.8") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fql_deserialize") (r "^0.2.8") (d #t) (k 0)) (d (n "fql_serialize") (r "^0.2.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0iabhgfa9634jq4sfw3i2s0ckbqn2a0k4ccvz3wwl33yw16cc1a4")))

