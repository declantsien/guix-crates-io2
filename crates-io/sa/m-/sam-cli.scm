(define-module (crates-io sa m- sam-cli) #:use-module (crates-io))

(define-public crate-sam-cli-0.1.0 (c (n "sam-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xd9nq1imrfjv9yp9jyfjsngw9sybc0ribcb45kiig06dz4n2fyw")))

