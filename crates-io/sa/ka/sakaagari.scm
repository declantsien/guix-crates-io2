(define-module (crates-io sa ka sakaagari) #:use-module (crates-io))

(define-public crate-sakaagari-0.1.0 (c (n "sakaagari") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "15lsa1p6cf8zpm08srpg8rzi33ansy2vx2kpk2b8fxv2qvda074w")))

(define-public crate-sakaagari-0.1.1 (c (n "sakaagari") (v "0.1.1") (d (list (d (n "git2") (r "^0.13.12") (k 0)) (d (n "serde") (r "^1.0.117") (k 0)) (d (n "toml") (r "^0.5.7") (k 0)))) (h "0f1v1826gc66w9ph8iwy2zphmqmn8lzb9r3nvnc5b078a0gv1qna")))

(define-public crate-sakaagari-0.2.0 (c (n "sakaagari") (v "0.2.0") (d (list (d (n "git2") (r "^0.13.12") (k 0)) (d (n "serde") (r "^1.0.117") (k 0)) (d (n "toml") (r "^0.5.7") (k 0)))) (h "1aiqiczwxqr2a8byv7bdqpssq7khg72nk8x5msy5in7rhpd7znbb")))

