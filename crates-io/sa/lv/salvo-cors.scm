(define-module (crates-io sa lv salvo-cors) #:use-module (crates-io))

(define-public crate-salvo-cors-0.36.0 (c (n "salvo-cors") (v "0.36.0") (d (list (d (n "salvo_core") (r "^0.36.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09w9dx81g2ax3vsq1qg8ffwm9v3m3cywbnq8cr87dng4dr854ggi")))

(define-public crate-salvo-cors-0.37.0 (c (n "salvo-cors") (v "0.37.0") (d (list (d (n "salvo_core") (r "^0.37.0") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.0") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0szpsq1w3g6lr886njhpmb1p464xgkcwysryqqdxmhnwannqbiax")))

(define-public crate-salvo-cors-0.37.1 (c (n "salvo-cors") (v "0.37.1") (d (list (d (n "salvo_core") (r "^0.37.1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.1") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0iw46ykj0f0rs3k0p8c45j5l19xykkqywds2vgqw7randwzflkcz")))

(define-public crate-salvo-cors-0.37.2 (c (n "salvo-cors") (v "0.37.2") (d (list (d (n "salvo_core") (r "^0.37.2") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.2") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01kfxks7d45fyrhk3hq66qfx8bxyq92ij4dmhimrcjp6jgb04fxz")))

(define-public crate-salvo-cors-0.37.3 (c (n "salvo-cors") (v "0.37.3") (d (list (d (n "salvo_core") (r "^0.37.3") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.3") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1q3pyjx9b0kg9y5wm24jxjbj83cy8wvf5qa68d8q56ndn4ws5jq4")))

(define-public crate-salvo-cors-0.37.4 (c (n "salvo-cors") (v "0.37.4") (d (list (d (n "salvo_core") (r "^0.37.4") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.4") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xjmdiwp4bwz5yvyslzqq8yqrqpwgwc99b99dv1rjw5ywpf6rl2k")))

(define-public crate-salvo-cors-0.37.5 (c (n "salvo-cors") (v "0.37.5") (d (list (d (n "salvo_core") (r "^0.37.5") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.5") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0742824kx3z6rdqj34y9h5jxrp2ngydvxgd0plm9i2lxjx8mbkx4")))

(define-public crate-salvo-cors-0.37.6 (c (n "salvo-cors") (v "0.37.6") (d (list (d (n "salvo_core") (r "^0.37.6") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.6") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "12r35h7z9nk2d7p0f33skkn5kg8w72h2ai36bw5957kzcf9dpq9s")))

(define-public crate-salvo-cors-0.37.7 (c (n "salvo-cors") (v "0.37.7") (d (list (d (n "salvo_core") (r "^0.37.7") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.7") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00dd5w7bs09zdhmwhqmzy7ls3gz5shw09v08l4msraqibyixdmmc")))

(define-public crate-salvo-cors-0.37.8 (c (n "salvo-cors") (v "0.37.8") (d (list (d (n "salvo_core") (r "^0.37.8") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.8") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1sh1dfr52xf5xn9zvflykbwsn1r731q0qscky90b99zigajz618i")))

(define-public crate-salvo-cors-0.37.9 (c (n "salvo-cors") (v "0.37.9") (d (list (d (n "salvo_core") (r "^0.37.9") (d #t) (k 0)) (d (n "salvo_core") (r "^0.37.9") (f (quote ("test"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0m0dh6fcyjd47c5idvjx2fswirq71dllg08ngfcf8rzkw9b3jkq8")))

(define-public crate-salvo-cors-0.39.1 (c (n "salvo-cors") (v "0.39.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.39.1") (k 0)) (d (n "salvo_core") (r "^0.39.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rk3xqsls6dvz7a4afmm6ygpk3wcmz9cmwhmkzwgfghgcxxw7sc7")))

(define-public crate-salvo-cors-0.40.0 (c (n "salvo-cors") (v "0.40.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.40.0") (k 0)) (d (n "salvo_core") (r "^0.40.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0smlbyj60bwd2x789rdyg9l8lmrbh70qlfpxadwi6x4ljqqwq3lc")))

(define-public crate-salvo-cors-0.41.0 (c (n "salvo-cors") (v "0.41.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.41.0") (k 0)) (d (n "salvo_core") (r "^0.41.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0p2q7s7jxscsxpf1b78rigd6mgf5sjnq87rrg32sgcmcxx9ls9f8")))

(define-public crate-salvo-cors-0.41.1 (c (n "salvo-cors") (v "0.41.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.41.1") (k 0)) (d (n "salvo_core") (r "^0.41.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0rf7562n252n09nn7h6a523pm37vqhdvxjprkr88m83jffyl04lz")))

(define-public crate-salvo-cors-0.42.0 (c (n "salvo-cors") (v "0.42.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.42.0") (k 0)) (d (n "salvo_core") (r "^0.42.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fsdbmr4n081wbvmp8d4ghvq9zlwcg259fi070dfbj9ckggjjpf4")))

(define-public crate-salvo-cors-0.42.1 (c (n "salvo-cors") (v "0.42.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.42.1") (k 0)) (d (n "salvo_core") (r "^0.42.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19azvldrmk1npkl3fp8jm1hsqj28v1rr59a4mvm3kp60s0bfka6q")))

(define-public crate-salvo-cors-0.43.0 (c (n "salvo-cors") (v "0.43.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.43.0") (k 0)) (d (n "salvo_core") (r "^0.43.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gh84igz1qj9m1brd9zi1pqscpxcffjpzj73d7frxra3jcbfaay1")))

(define-public crate-salvo-cors-0.44.0 (c (n "salvo-cors") (v "0.44.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.44.0") (k 0)) (d (n "salvo_core") (r "^0.44.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0hh2vsd3pcvpykd9ygcqqzwpkr63xk9dg6q17dia4gxi2m79n4xr")))

(define-public crate-salvo-cors-0.44.1 (c (n "salvo-cors") (v "0.44.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.44.1") (k 0)) (d (n "salvo_core") (r "^0.44.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ghq25fhr2fkaj6w2s7jc0ryz48p39lmrvk5gjh0v3ky6zd15r2x")))

(define-public crate-salvo-cors-0.45.0 (c (n "salvo-cors") (v "0.45.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.45.0") (k 0)) (d (n "salvo_core") (r "^0.45.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06mrzq05qp3x7gil2nns4k77hv5nsn10pn3gzccmgdrj2fp28vc8")))

(define-public crate-salvo-cors-0.46.0 (c (n "salvo-cors") (v "0.46.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.46.0") (k 0)) (d (n "salvo_core") (r "^0.46.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gkgkk9s1z0fxi72mvjwp4sysw2qmqifj11b3rfki94jg698fg60")))

(define-public crate-salvo-cors-0.47.0 (c (n "salvo-cors") (v "0.47.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.47.0") (k 0)) (d (n "salvo_core") (r "^0.47.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "043bcx4j6drkg41bs78xjz7dva1shj9h8568fk7vkrvw3q5a3cga")))

(define-public crate-salvo-cors-0.48.0 (c (n "salvo-cors") (v "0.48.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.48.0") (k 0)) (d (n "salvo_core") (r "^0.48.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10cmh7vvql9xwfq28z6rdb49lnzx6s418svkqnd17gz1y74j05mb")))

(define-public crate-salvo-cors-0.48.2 (c (n "salvo-cors") (v "0.48.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.48.2") (k 0)) (d (n "salvo_core") (r "^0.48.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "006524li3cgl40b0d7jv7y8ngwvc2f4fz76vr6m6i4kcnh60yfml")))

(define-public crate-salvo-cors-0.49.0 (c (n "salvo-cors") (v "0.49.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.49.0") (k 0)) (d (n "salvo_core") (r "^0.49.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v9zhmanam6g4kgmfp5kvqbmsc8bklnxhm8f0wryldirp72jc14f")))

(define-public crate-salvo-cors-0.49.1 (c (n "salvo-cors") (v "0.49.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.49.1") (k 0)) (d (n "salvo_core") (r "^0.49.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1caajfli1lsgj7xzbfvar4znvhbar8vdd8c3iv0dnn7abcfzfwxm")))

(define-public crate-salvo-cors-0.50.1 (c (n "salvo-cors") (v "0.50.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.50.1") (k 0)) (d (n "salvo_core") (r "^0.50.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fxgnc136y7pxmhw9ffpg2badb4r64391l6yarxy9xfrz83j5xwx")))

(define-public crate-salvo-cors-0.50.2 (c (n "salvo-cors") (v "0.50.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.50.2") (k 0)) (d (n "salvo_core") (r "^0.50.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10liyzxxz6bhj2l2hilv91m57yqg582g1zqh7f2hap4dbhpwq3fc")))

(define-public crate-salvo-cors-0.50.3 (c (n "salvo-cors") (v "0.50.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.50.3") (k 0)) (d (n "salvo_core") (r "^0.50.3") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09xp4lgg117r8s9p2fjs27shfdh4bybw747v2a2fzlczd7cc1b2x")))

(define-public crate-salvo-cors-0.50.4 (c (n "salvo-cors") (v "0.50.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.50.4") (k 0)) (d (n "salvo_core") (r "^0.50.4") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fbx8w33mchkgif72pcnyhpmnpz5qlji8v3vzz94ygmv2hrlkzy3")))

(define-public crate-salvo-cors-0.50.5 (c (n "salvo-cors") (v "0.50.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.50.5") (k 0)) (d (n "salvo_core") (r "^0.50.5") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1v75xmfisdjhmcp66qq9nfsqwr7sr5qa3a9f9fzjpgn8khirwzil")))

(define-public crate-salvo-cors-0.51.0 (c (n "salvo-cors") (v "0.51.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.51.0") (k 0)) (d (n "salvo_core") (r "^0.51.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bj8kzc0sjrk40lfssf7jz2i60ip2h7fcz26m722my488p1pgncd")))

(define-public crate-salvo-cors-0.52.0 (c (n "salvo-cors") (v "0.52.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.52.0") (k 0)) (d (n "salvo_core") (r "^0.52.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1p30a3zyf3xj456jiqw4qnncsc39a4g7dqg7xbf3maikw5p6y2p9")))

(define-public crate-salvo-cors-0.52.1 (c (n "salvo-cors") (v "0.52.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.52.1") (k 0)) (d (n "salvo_core") (r "^0.52.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1890094vcz0xdbip8l1v9wayw9rvwrq8anhf9yxdd12qrni71s0p")))

(define-public crate-salvo-cors-0.53.0 (c (n "salvo-cors") (v "0.53.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.53.0") (k 0)) (d (n "salvo_core") (r "^0.53.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1wdax8d5yfzs87ngwfc49izlarqxhmlvckgd8f6mkrc5pvzml6ij")))

(define-public crate-salvo-cors-0.54.1 (c (n "salvo-cors") (v "0.54.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.54.1") (k 0)) (d (n "salvo_core") (r "^0.54.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dbp1iqna67xm603qzl86a7nsyx90i017cz19sgnwzisk1m81q89")))

(define-public crate-salvo-cors-0.54.3 (c (n "salvo-cors") (v "0.54.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.54.3") (k 0)) (d (n "salvo_core") (r "^0.54.3") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1fpkpx61p7ksclspnlxqydszggx1p6km5dy5cv54prr19fwpd6a6")))

(define-public crate-salvo-cors-0.55.2 (c (n "salvo-cors") (v "0.55.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.55.2") (k 0)) (d (n "salvo_core") (r "^0.55.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "114scz2bpjqxqdj4glaqsqrxylcrprdbpvjha62y6na5xiwsjbkv")))

(define-public crate-salvo-cors-0.55.3 (c (n "salvo-cors") (v "0.55.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.55.3") (k 0)) (d (n "salvo_core") (r "^0.55.3") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bdipv06m0418szdsj7wxzpdgfwyhpkwjxljm74pl75d5fbbps0w")))

(define-public crate-salvo-cors-0.55.4 (c (n "salvo-cors") (v "0.55.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.55.4") (k 0)) (d (n "salvo_core") (r "^0.55.4") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00zpcz738nq243ppb0216j57ndi9i67njnzswxkwamk1fgqzmk11")))

(define-public crate-salvo-cors-0.55.5 (c (n "salvo-cors") (v "0.55.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.55.5") (k 0)) (d (n "salvo_core") (r "^0.55.5") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09fnzfjkz7hdv20i8g46nw067gbdj5zdi4nmgrlicydc8b1i9zfx")))

(define-public crate-salvo-cors-0.56.0 (c (n "salvo-cors") (v "0.56.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.56.0") (k 0)) (d (n "salvo_core") (r "^0.56.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lcmp3r7c2nhprkwada4ppw35yqay5xy0jgqa2lbjcj7vyznlz0l")))

(define-public crate-salvo-cors-0.57.0 (c (n "salvo-cors") (v "0.57.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.57.0") (k 0)) (d (n "salvo_core") (r "^0.57.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gnk9ib7m99m10g99i60cznbn85k4l8sypwklfahp297splfxy4v")))

(define-public crate-salvo-cors-0.58.0 (c (n "salvo-cors") (v "0.58.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.0") (k 0)) (d (n "salvo_core") (r "^0.58.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1x7nmpni718l71pa2faax7j62ar83426xgfk3iq4acgm5y84prr6")))

(define-public crate-salvo-cors-0.58.1 (c (n "salvo-cors") (v "0.58.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.1") (k 0)) (d (n "salvo_core") (r "^0.58.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0prdllcdpgfa7qzsw0yqvbn3316sckrp2mxrlq7j44yqv93y3gzw")))

(define-public crate-salvo-cors-0.58.2 (c (n "salvo-cors") (v "0.58.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.2") (k 0)) (d (n "salvo_core") (r "^0.58.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ywkgj6jy46w2l4bzgrflmy27f36vp6xmw4ckim4ab9d669jicnz")))

(define-public crate-salvo-cors-0.58.3 (c (n "salvo-cors") (v "0.58.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.3") (k 0)) (d (n "salvo_core") (r "^0.58.3") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0y3gham23ha1hqys76qb8ypcpcchygs81jydzhxxczax7mbwp9ac")))

(define-public crate-salvo-cors-0.58.4 (c (n "salvo-cors") (v "0.58.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.4") (k 0)) (d (n "salvo_core") (r "^0.58.4") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zsrw10d5s5c4c4gnz2a1jdb824wkkfi91h28iqx1jar872swkia")))

(define-public crate-salvo-cors-0.58.5 (c (n "salvo-cors") (v "0.58.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.58.5") (k 0)) (d (n "salvo_core") (r "^0.58.5") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z3jnlp87gai6s8dzyhh0qm2p7as0h33br51gx230acn8nmlwckb")))

(define-public crate-salvo-cors-0.59.0 (c (n "salvo-cors") (v "0.59.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.59.0") (k 0)) (d (n "salvo_core") (r "^0.59.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1lb428xsqi2y316zqr07hsh30nr7xh71k2fhaqnwmr8bwv8i0l0h")))

(define-public crate-salvo-cors-0.59.1 (c (n "salvo-cors") (v "0.59.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.59.1") (k 0)) (d (n "salvo_core") (r "^0.59.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rncfygppwdhnkhc2fyfwdwqxk45fa3phmhraibck7dylx9maydd")))

(define-public crate-salvo-cors-0.60.0 (c (n "salvo-cors") (v "0.60.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.60.0") (k 0)) (d (n "salvo_core") (r "^0.60.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ycjr26313khpfjyr7ijqfdw8y4a8dlf1717119wxqiadkr2s74g")))

(define-public crate-salvo-cors-0.61.0 (c (n "salvo-cors") (v "0.61.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.61.0") (k 0)) (d (n "salvo_core") (r "^0.61.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0gzlvsw62lsir1pg6w3vcjp60qxz6wwli8kn88g6vpjjbs33c164")))

(define-public crate-salvo-cors-0.63.0 (c (n "salvo-cors") (v "0.63.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.63.0") (k 0)) (d (n "salvo_core") (r "^0.63.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zmk3iisabq462256j9rq15wxcrfm9a95l1mh7b43gs23hsizdn1")))

(define-public crate-salvo-cors-0.63.1 (c (n "salvo-cors") (v "0.63.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.63.1") (k 0)) (d (n "salvo_core") (r "^0.63.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nd4a5khya179d57k0xj9swcbyy3l2qyb593l0jpjs1pfznwvwl6")))

(define-public crate-salvo-cors-0.64.0 (c (n "salvo-cors") (v "0.64.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.64.0") (k 0)) (d (n "salvo_core") (r "^0.64.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1r30hv2cih8wn7ddgka9n4mk1n7rzyiw10qx3zrxb1s3vb0awnzi")))

(define-public crate-salvo-cors-0.64.1 (c (n "salvo-cors") (v "0.64.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.64.1") (k 0)) (d (n "salvo_core") (r "^0.64.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ylwlgfaw8hpy82nrw4dqj9g4z2d03vmaakcfl8mdwpizbwqz614")))

(define-public crate-salvo-cors-0.64.2 (c (n "salvo-cors") (v "0.64.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.64.2") (k 0)) (d (n "salvo_core") (r "^0.64.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "17sav1grwjdzaf4bisx9phar4spv3885djs4max96fhkcjqg8p5r")))

(define-public crate-salvo-cors-0.65.0 (c (n "salvo-cors") (v "0.65.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.65.0") (k 0)) (d (n "salvo_core") (r "^0.65.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1174m1p69hqm239g3nbfrcgj8rf2gh80dri7ks07q49f9ywzq3m1")))

(define-public crate-salvo-cors-0.65.1 (c (n "salvo-cors") (v "0.65.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.65.1") (k 0)) (d (n "salvo_core") (r "^0.65.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1dkgal1i46yzy1bf2sxqpcqz8wnsv47nhp07dxj0nkdgpicvqmhb")))

(define-public crate-salvo-cors-0.65.2 (c (n "salvo-cors") (v "0.65.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.65.2") (k 0)) (d (n "salvo_core") (r "^0.65.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0nm42kf986k083g63rs65c9c1q5p8gpslylmf3c4g6acckg9004d")))

(define-public crate-salvo-cors-0.66.0 (c (n "salvo-cors") (v "0.66.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.66.0") (k 0)) (d (n "salvo_core") (r "^0.66.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0dvw8a2iy5l0yljs61r9y21bs4wri6xxfbp5nvs1rlq8jqmkx7d5")))

(define-public crate-salvo-cors-0.66.1 (c (n "salvo-cors") (v "0.66.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.66.1") (k 0)) (d (n "salvo_core") (r "^0.66.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1h1chc0c61z8rpqyq1k7z7l5rrad6qhcks3m18sm8wkmyf8z49y2")))

(define-public crate-salvo-cors-0.66.2 (c (n "salvo-cors") (v "0.66.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.66.2") (k 0)) (d (n "salvo_core") (r "^0.66.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j6m2173ja11h28323h5b5g53xsvrb51jmbhg9idpq5h657aangl")))

(define-public crate-salvo-cors-0.67.0 (c (n "salvo-cors") (v "0.67.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.67.0") (k 0)) (d (n "salvo_core") (r "^0.67.0") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1qij1v9qnzp8kyi0s6vxkw524iryy5ikk35qrxqqda3bgjjqm52d")))

(define-public crate-salvo-cors-0.67.1 (c (n "salvo-cors") (v "0.67.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.67.1") (k 0)) (d (n "salvo_core") (r "^0.67.1") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05clqy887cc7l3zzk1v4nr867cb0zn9ph3riwh03djil1nqksqc3")))

(define-public crate-salvo-cors-0.67.2 (c (n "salvo-cors") (v "0.67.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "salvo_core") (r "^0.67.2") (k 0)) (d (n "salvo_core") (r "^0.67.2") (f (quote ("test"))) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1n8p9lhxp99ggjgpc0wbi6kz3ygz92xn5pca0ck4fp0agxrvy5qx")))

