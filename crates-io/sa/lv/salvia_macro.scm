(define-module (crates-io sa lv salvia_macro) #:use-module (crates-io))

(define-public crate-salvia_macro-0.1.0 (c (n "salvia_macro") (v "0.1.0") (d (list (d (n "async-recursion") (r "^0.3.2") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full" "visit"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8.1") (f (quote ("rt-multi-thread"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0p53j205xkafmrh1j1a6sh0sm2nn3dwpaxpx21m7p1kbi77cighh")))

