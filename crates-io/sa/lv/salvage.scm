(define-module (crates-io sa lv salvage) #:use-module (crates-io))

(define-public crate-salvage-0.7.2 (c (n "salvage") (v "0.7.2") (d (list (d (n "bollard") (r "^0.15") (d #t) (k 0)) (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("timestamps"))) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("local-offset" "macros" "formatting"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "xz2") (r "^0.1") (d #t) (k 0)) (d (n "zstd") (r "^0.13") (d #t) (k 0)))) (h "09wf4gw0lkhvd3z978lwkz0rxis4sydg8j2f7r2x5r5zd2123zby") (r "1.70")))

