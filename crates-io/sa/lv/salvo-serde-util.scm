(define-module (crates-io sa lv salvo-serde-util) #:use-module (crates-io))

(define-public crate-salvo-serde-util-0.0.1 (c (n "salvo-serde-util") (v "0.0.1") (d (list (d (n "proc-macro-crate") (r ">=2, <=3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p7lsh4x2hv0bxyj575y28qgc0bkjqn1hszd6dfi9brsr1gqjn38")))

(define-public crate-salvo-serde-util-0.64.0 (c (n "salvo-serde-util") (v "0.64.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vd3876vhx3l03lsdv646c43gskbk2xb77d85qms0szffmbhbb8v")))

(define-public crate-salvo-serde-util-0.64.1 (c (n "salvo-serde-util") (v "0.64.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nhhi86p35vgy1rzxy60sk3blpamk7r0caj6344zcaivlwkvhpm5")))

(define-public crate-salvo-serde-util-0.64.2 (c (n "salvo-serde-util") (v "0.64.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10gzl89kzc4c3448c6lndsiczr4msn2lkycb5dry98nm0ij6qlcw")))

(define-public crate-salvo-serde-util-0.65.0 (c (n "salvo-serde-util") (v "0.65.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bzyg8n4hv0p4b8hjwqy0kljnp0i1icndr54c1qkl8xpbh75p96h")))

(define-public crate-salvo-serde-util-0.65.1 (c (n "salvo-serde-util") (v "0.65.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x4ciac23vsx7ffnmbxbkkkp5bbimghfvcy4jvl9hacz8q3q06bi")))

(define-public crate-salvo-serde-util-0.65.2 (c (n "salvo-serde-util") (v "0.65.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zxacn9157z16pnfr5ym7ap9jvlrnhl6ib7b14s7hqr1df84vav7")))

(define-public crate-salvo-serde-util-0.66.0 (c (n "salvo-serde-util") (v "0.66.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0irq7v92qgxm0hkpdl5g65vbq04jjbh3hx1xzyidm380sb6v9c2s")))

(define-public crate-salvo-serde-util-0.66.1 (c (n "salvo-serde-util") (v "0.66.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h15y4js88psfihapic02pxga5rd8x5qk81ly019ccn894z7rh4i")))

(define-public crate-salvo-serde-util-0.66.2 (c (n "salvo-serde-util") (v "0.66.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f77wsya099fkm1gnkdszczlllxf0dyc3ij4wllipdjbwhxn7p4v")))

(define-public crate-salvo-serde-util-0.67.0 (c (n "salvo-serde-util") (v "0.67.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d723xhf4vpnv6dmfcz9w26iv5mh786ci2cyvavsdbz7qy8cfdw4")))

(define-public crate-salvo-serde-util-0.67.1 (c (n "salvo-serde-util") (v "0.67.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yxpvc8q7qk3hj8298hbmhviibazivzv2rqa80imcj011raspf7f")))

(define-public crate-salvo-serde-util-0.67.2 (c (n "salvo-serde-util") (v "0.67.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "180hfv3zddc8q1qbnlyhc12rdhmiz2l6wb8hyvgj0mp81xxif2f2")))

