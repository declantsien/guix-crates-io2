(define-module (crates-io sa sh sash) #:use-module (crates-io))

(define-public crate-sash-0.1.0 (c (n "sash") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "directories") (r "^5") (d #t) (k 0)) (d (n "maud") (r "^0.25") (f (quote ("axum"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0sgpxbm0l0clrq9v2w3l1vwcj1rv4w2c0bki8ahvkg58wnb0nj2w")))

