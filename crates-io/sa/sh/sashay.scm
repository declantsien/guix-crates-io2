(define-module (crates-io sa sh sashay) #:use-module (crates-io))

(define-public crate-sashay-0.1.0 (c (n "sashay") (v "0.1.0") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "1b5hvcq0n87gywff04qfh4zvsa7vipdpy35p52n7j12z8r30dpxj") (y #t)))

(define-public crate-sashay-0.1.1 (c (n "sashay") (v "0.1.1") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "0vawm03sfxsmh661j0sp6kj7z9zzpc0irj3jrxc5ghgih4rffzyh") (y #t)))

(define-public crate-sashay-0.2.0 (c (n "sashay") (v "0.2.0") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "1w1r8g3ng57mf1r5i5m3ac4x20dnspv0hdv06rbr8md6z05911jj")))

(define-public crate-sashay-0.2.1 (c (n "sashay") (v "0.2.1") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "100as8pv0m6zxj90il8nwdqa1klxrpf0klrnspn58ns80nj51hym")))

(define-public crate-sashay-0.3.0 (c (n "sashay") (v "0.3.0") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "0rwdi0q9vcab6lfj74zniw7p9fk748lyjdhza7gsa8yq9bjw7gsl")))

(define-public crate-sashay-0.3.1 (c (n "sashay") (v "0.3.1") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "0z7wvh0lifnqipwwy8kpksbv7sa2nqg26pidr91z2d04a5qjq6ip")))

(define-public crate-sashay-0.3.2 (c (n "sashay") (v "0.3.2") (d (list (d (n "erasable") (r "^1.2.1") (d #t) (k 0)))) (h "1lmnwfg6shc98jbrq05pwdq7wrfhhbzpapdj53g4bal0yfmy0y9s")))

(define-public crate-sashay-0.4.0 (c (n "sashay") (v "0.4.0") (h "0y2jhybdzisf75ks3wna3h1vi1q3vhfag44dvskiq819zqg0ykf4")))

(define-public crate-sashay-0.5.0 (c (n "sashay") (v "0.5.0") (h "1bkr0h21x8sd3xza3p2w09ig9b1vbwl9knnbxpff9k45xbpwyg68")))

