(define-module (crates-io sa qi saqif) #:use-module (crates-io))

(define-public crate-saqif-0.1.0 (c (n "saqif") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "druid") (r "^0.8.3") (d #t) (k 0)) (d (n "druid-widget-nursery") (r "^0.1.0") (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.9") (d #t) (k 0)))) (h "1xf2fhscnqx33whphqj28hxh483zqzpaw6qlmj8lf89kly4drgg7")))

