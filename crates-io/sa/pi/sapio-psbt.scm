(define-module (crates-io sa pi sapio-psbt) #:use-module (crates-io))

(define-public crate-sapio-psbt-0.2.4 (c (n "sapio-psbt") (v "0.2.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bitcoin") (r "^0.28.0") (f (quote ("use-serde" "rand" "base64"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "miniscript") (r "^7.0.0") (f (quote ("compiler" "use-serde" "rand" "use-schemars" "serde"))) (d #t) (k 0) (p "sapio-miniscript")) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ld749317nkjsbzy0s3332qiwg5kd1m1c9qly428ikcz5wqv27k0")))

