(define-module (crates-io sa pi sapiens-sys) #:use-module (crates-io))

(define-public crate-sapiens-sys-0.1.0 (c (n "sapiens-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "1p3kc2bjdngf9rn5nl3a1fhwd2mbhg4anxxz6s5m1kcccsq2k9cz")))

(define-public crate-sapiens-sys-0.2.0 (c (n "sapiens-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)))) (h "1g77j34kjabqxh9l5ygcsfq76c7kbr5asxq53ridvv6wnfs77bgi")))

