(define-module (crates-io sa pi sapio-trait) #:use-module (crates-io))

(define-public crate-sapio-trait-0.2.3 (c (n "sapio-trait") (v "0.2.3") (d (list (d (n "bitcoin") (r "^0.26.0") (f (quote ("use-serde" "rand"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "jsonschema") (r "^0.12") (k 0)) (d (n "sapio-base") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1216jbkkapyvq6csl0alh0damaxvviq3kxrz7jzgmf39vf3j5p48")))

