(define-module (crates-io sa pi sapio-front) #:use-module (crates-io))

(define-public crate-sapio-front-0.1.2 (c (n "sapio-front") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.26.0") (f (quote ("use-serde" "rand"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "sapio") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02icw8dqvars8j3vxhkdh73ymm6davlxkx8j2bm0waa3dj7swahr")))

(define-public crate-sapio-front-0.2.0 (c (n "sapio-front") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.26.0") (f (quote ("use-serde" "rand"))) (d #t) (k 0) (p "sapio-bitcoin")) (d (n "sapio") (r "^0.2.0") (d #t) (k 0)) (d (n "sapio-ctv-emulator-trait") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qx8mslsygiqfd9c9p1ylii6i55v1ffaglgfbx3mgwy01daxn0np")))

