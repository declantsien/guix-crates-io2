(define-module (crates-io sa pi sapiens-rs) #:use-module (crates-io))

(define-public crate-sapiens-rs-0.1.0 (c (n "sapiens-rs") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sapiens-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0y9sgd48gyy1z64fzw9y50vzjwp4d1ydydf9y2lx1gh6sh2m1bs7")))

