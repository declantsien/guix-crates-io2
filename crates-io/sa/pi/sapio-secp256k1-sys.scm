(define-module (crates-io sa pi sapio-secp256k1-sys) #:use-module (crates-io))

(define-public crate-sapio-secp256k1-sys-0.21.4 (c (n "sapio-secp256k1-sys") (v "0.21.4") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1rq26plnyb5p4z3yq6ms59agn9wkh477lszhjm6b4g5l14r8ld23") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (y #t) (l "rustsecp256k1_v0_4_1")))

(define-public crate-sapio-secp256k1-sys-0.5.2 (c (n "sapio-secp256k1-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "162a1khhcmqc5qrc9sp13yav2qvab22d32v9nivr1dcrss9pn8jr") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-sapio-secp256k1-sys-0.22.3 (c (n "sapio-secp256k1-sys") (v "0.22.3") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0val0h0gryh10sc8mm25x6jm7c8jyv1xfqf7k2h2a0sh6bj3nz7h") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (y #t) (l "rustsecp256k1_v0_5_0")))

(define-public crate-sapio-secp256k1-sys-0.5.3 (c (n "sapio-secp256k1-sys") (v "0.5.3") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0z3wsps990whx9l9ajghqx3899i6mf119djk1yl30jirx1c89fcb") (f (quote (("std") ("recovery") ("lowmemory") ("default" "std")))) (l "rustsecp256k1_v0_5_0")))

(define-public crate-sapio-secp256k1-sys-0.9.2 (c (n "sapio-secp256k1-sys") (v "0.9.2") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0vph2r3vn4w8m9bfal0a6y80qgq671sayiaps5bzpggiir9ji45v") (f (quote (("std" "alloc") ("recovery") ("lowmemory") ("default" "std") ("alloc")))) (l "rustsecp256k1_v0_9_2")))

