(define-module (crates-io sa rm sarmio) #:use-module (crates-io))

(define-public crate-sarmio-0.1.0 (c (n "sarmio") (v "0.1.0") (h "03niwbdm23blzvn50rh08n1jyczkmra1yqqmhkkhd6j0dwiyik11")))

(define-public crate-sarmio-0.1.1 (c (n "sarmio") (v "0.1.1") (h "1dpwxf7qkmvfg04sww9arx5fn43m1czd3hqgsgvmkc6rp14f25sb")))

