(define-module (crates-io sa cn sacn-unofficial) #:use-module (crates-io))

(define-public crate-sacn-unofficial-0.9.0 (c (n "sacn-unofficial") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.3.4") (f (quote ("reuseport"))) (d #t) (k 0)) (d (n "uuid") (r "^0.6.3") (f (quote ("use_std" "v4"))) (d #t) (k 0)))) (h "0dr3mcsc1p8504rhip59imxipmyj3yaaib7lagdw0qkdava0zlmp") (f (quote (("unstable" "heapless") ("std" "net2") ("default" "std"))))))

