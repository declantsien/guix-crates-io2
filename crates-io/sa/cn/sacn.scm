(define-module (crates-io sa cn sacn) #:use-module (crates-io))

(define-public crate-sacn-0.1.0 (c (n "sacn") (v "0.1.0") (d (list (d (n "net2") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "032y0ck1s8scfh0mjpms7yr783qx1yzwywkpgygvfb8c7qwg2227")))

(define-public crate-sacn-0.1.1 (c (n "sacn") (v "0.1.1") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "18cnwf3wc07dxbmnlhjadabyf04kbj892rckaja47vqp8w9qr6fv")))

(define-public crate-sacn-0.2.0 (c (n "sacn") (v "0.2.0") (d (list (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "05iz2x5fgnqnzahbqnp9a856z8ai2vx18s935ymcxvvcl5gd9hlj")))

(define-public crate-sacn-0.3.0 (c (n "sacn") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "045f0hs50044kd2bwvwwc1al6ri1msidhvp98mcr44mwcx1w73ky") (f (quote (("std") ("default" "std"))))))

(define-public crate-sacn-0.4.0 (c (n "sacn") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mh79crwd95mq6s70524vc40hs729i1jks7dh4fpnx6svfc8jlwi") (f (quote (("std") ("default" "std"))))))

(define-public crate-sacn-0.4.1 (c (n "sacn") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jxwhdf98hfa05cyb7yxdd3lyigvznjlfhfs2zmr0axfcj0yil6p") (f (quote (("std") ("default" "std"))))))

(define-public crate-sacn-0.4.2 (c (n "sacn") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0pk5z6rdwp18yxbk4zj3mmf3q2c5zvag0xr0ylqfkbp03439pxrv") (f (quote (("std") ("default" "std"))))))

(define-public crate-sacn-0.4.3 (c (n "sacn") (v "0.4.3") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "0ny96v2mvg92v6gy15fviw9b8bcz06nf71fhj43dl2mav50gczka") (f (quote (("std") ("default" "std"))))))

(define-public crate-sacn-0.4.4 (c (n "sacn") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "net2") (r "^0.2.31") (d #t) (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("use_std" "v4"))) (d #t) (t "cfg(feature = \"std\")") (k 0)) (d (n "uuid") (r "^0.6.2") (f (quote ("v4"))) (d #t) (t "cfg(not(feature = \"std\"))") (k 0)))) (h "1kq07hhwxqq5dghs1p73977pkwbhqddm3livvryqi72i10l1sqf9") (f (quote (("std") ("default" "std"))))))

