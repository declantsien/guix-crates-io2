(define-module (crates-io sa fe safex) #:use-module (crates-io))

(define-public crate-safex-0.0.1 (c (n "safex") (v "0.0.1") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)))) (h "1ci3fmsa41rrzdvzj1f011ia3kiwk13l7rkk6glz1mwm6ppv0bwj") (f (quote (("xlib" "x11/xlib") ("raw") ("glx" "x11/glx" "xlib"))))))

(define-public crate-safex-0.0.2 (c (n "safex") (v "0.0.2") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)))) (h "0ji3jsbl4b76pwp6fi0zc6vz4yc28nx4df92r8s3gb2d2n9mj34p") (f (quote (("xlib" "x11/xlib") ("raw") ("glx" "x11/glx" "xlib"))))))

(define-public crate-safex-0.0.2-rev.2 (c (n "safex") (v "0.0.2-rev.2") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)))) (h "1x7syslxczp74ga76d9qvg9yhd1h788knmwxi6n452mdda5ibd1x") (f (quote (("xlib" "x11/xlib") ("raw") ("glx" "x11/glx" "xlib")))) (y #t)))

(define-public crate-safex-0.0.3 (c (n "safex") (v "0.0.3") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)))) (h "0ih92px4zd0qqvm4ixp1fkk3mf11yfs2jxnc1vw8g5s0jcnxg4qc") (f (quote (("xlib" "x11/xlib") ("raw") ("glx" "x11/glx" "xlib"))))))

(define-public crate-safex-0.0.3-2 (c (n "safex") (v "0.0.3-2") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)))) (h "19kfyy88nabl0nkrvs3mpsam8ba5qffx1v6krxixk6hjijmgvv1x") (f (quote (("xlib" "x11/xlib") ("raw") ("glx" "x11/glx" "xlib")))) (y #t)))

(define-public crate-safex-0.0.4 (c (n "safex") (v "0.0.4") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.3") (d #t) (k 2)))) (h "136z6y0sx4hmq1h9b8g561wljlpbh97xllsajrd0mphkk7x4qa38") (f (quote (("xlib" "x11/xlib") ("glx" "x11/glx" "xlib"))))))

(define-public crate-safex-0.0.5 (c (n "safex") (v "0.0.5") (d (list (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.1") (d #t) (k 2)) (d (n "winit") (r "^0.28.3") (d #t) (k 2)))) (h "0jg4zcrsqyhqywrj27pl0c67y8xp130q4yss8qh04rs5rbgg3wij") (f (quote (("xlib_xcb" "xlib" "x11/xlib_xcb") ("xlib" "x11/xlib") ("xinput" "x11/xinput") ("xcursor" "xlib" "x11/xcursor") ("glx" "x11/glx" "xlib"))))))

