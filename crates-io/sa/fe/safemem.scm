(define-module (crates-io sa fe safemem) #:use-module (crates-io))

(define-public crate-safemem-0.1.0 (c (n "safemem") (v "0.1.0") (h "1bmpyyhz3xhlghwklvxmaa215n31m54axaidhclp946klxidi66x")))

(define-public crate-safemem-0.1.1 (c (n "safemem") (v "0.1.1") (h "0290w89qwip1nd4md43vkyi2d51n82ahlpvvsay4l2z4gbs3nnvj")))

(define-public crate-safemem-0.2.0 (c (n "safemem") (v "0.2.0") (h "13rx2vl5bqc7x4xpfc0can3a39f3bhgqg3l112lsxxrmp0cqnyp2")))

(define-public crate-safemem-0.3.0 (c (n "safemem") (v "0.3.0") (h "1n9xm7hbh7k2xjbzp2ggpxhlmrdln3idzk1nkrsb0v5990r4bjld") (f (quote (("std") ("default" "std"))))))

(define-public crate-safemem-0.3.1 (c (n "safemem") (v "0.3.1") (h "1mcx6gfcmzf56by3i3lbcdzxqmkx50cgczwarj4lzkfiyk2cqcz1") (f (quote (("std") ("default" "std"))))))

(define-public crate-safemem-0.3.2 (c (n "safemem") (v "0.3.2") (h "1l1ljkm4lpyryrv2ndaxi1f7z1f3v9bwy1rzl9f9mbhx04iq9c6j") (f (quote (("std") ("default" "std"))))))

(define-public crate-safemem-0.3.3 (c (n "safemem") (v "0.3.3") (h "0wp0d2b2284lw11xhybhaszsczpbq1jbdklkxgifldcknmy3nw7g") (f (quote (("std") ("default" "std"))))))

