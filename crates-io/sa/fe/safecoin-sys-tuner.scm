(define-module (crates-io sa fe safecoin-sys-tuner) #:use-module (crates-io))

(define-public crate-safecoin-sys-tuner-1.6.16 (c (n "safecoin-sys-tuner") (v "1.6.16") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "safecoin-clap-utils") (r "=1.6.16") (d #t) (k 0)) (d (n "safecoin-logger") (r "=1.6.16") (d #t) (k 0)) (d (n "safecoin-version") (r "=1.6.16") (d #t) (k 0)) (d (n "sysctl") (r "^0.4.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "unix_socket2") (r "^0.5.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "users") (r "^0.10.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1nl9yifxx8xmq3p03bn8116crmvfzxkpf4nwgkyg6na0nqi37fam")))

