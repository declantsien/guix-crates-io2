(define-module (crates-io sa fe safecoin-vest-program) #:use-module (crates-io))

(define-public crate-safecoin-vest-program-1.6.16 (c (n "safecoin-vest-program") (v "1.6.16") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safecoin-config-program") (r "=1.6.16") (d #t) (k 0)) (d (n "safecoin-runtime") (r "=1.6.16") (d #t) (k 2)) (d (n "safecoin-sdk") (r "=1.6.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ia8rfg7vm690i7lx1nijsamx3jqckrgnh1zs8zsznhz47b4w38m")))

