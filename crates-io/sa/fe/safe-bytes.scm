(define-module (crates-io sa fe safe-bytes) #:use-module (crates-io))

(define-public crate-safe-bytes-0.1.0 (c (n "safe-bytes") (v "0.1.0") (d (list (d (n "safe-bytes-derive") (r "=0.1.0") (d #t) (k 0)))) (h "00z7ngcbrm7xb4h9ybs8yrvw49n6hnanvng41a1xq3xmpgmd855a")))

(define-public crate-safe-bytes-0.1.1 (c (n "safe-bytes") (v "0.1.1") (d (list (d (n "safe-bytes-derive") (r "=0.1.1") (d #t) (k 0)))) (h "1anx836s63rd04x28qbcn8jxilhznzbsrikzdcssfyraawzmaxln")))

