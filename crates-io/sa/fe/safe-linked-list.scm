(define-module (crates-io sa fe safe-linked-list) #:use-module (crates-io))

(define-public crate-safe-linked-list-0.1.0 (c (n "safe-linked-list") (v "0.1.0") (h "0nbaz6fayay9w05nnj3glq209cgg3xqddlgjm3mrmf8hvhj0zqgm") (y #t)))

(define-public crate-safe-linked-list-0.1.1 (c (n "safe-linked-list") (v "0.1.1") (h "01i6z8l9vcyzsg0ikcna8drnsf3sc7gycm5divajwds96mnhlsnn") (y #t)))

(define-public crate-safe-linked-list-0.2.0 (c (n "safe-linked-list") (v "0.2.0") (h "00q2pqvjsqjgqnxpc51cxd65ifp7cymlmmbx16myw3mphwcqdipj") (y #t)))

(define-public crate-safe-linked-list-0.3.0 (c (n "safe-linked-list") (v "0.3.0") (h "1r4x2csxpb5k14100nahfqv9my9rsblvz0all05hxbzxwwsvnl9j") (y #t)))

