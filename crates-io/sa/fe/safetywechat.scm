(define-module (crates-io sa fe safetywechat) #:use-module (crates-io))

(define-public crate-safetywechat-0.1.0 (c (n "safetywechat") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "hex") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08acaqv06qjh8iw4v2w23r6vkzg3x69wykbkr3q9qqaalw9jc1cv")))

