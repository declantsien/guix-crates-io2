(define-module (crates-io sa fe safe_http_async) #:use-module (crates-io))

(define-public crate-safe_http_async-0.1.0-beta.4 (c (n "safe_http_async") (v "0.1.0-beta.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "safe_http") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "safe_uri") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "shared_bytes") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "13mhj01aip08gig412wpr7s5zww2znc9ivjni5qx0l0i4x1mwwz8") (f (quote (("unstable"))))))

