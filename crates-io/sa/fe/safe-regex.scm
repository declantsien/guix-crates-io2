(define-module (crates-io sa fe safe-regex) #:use-module (crates-io))

(define-public crate-safe-regex-0.1.0 (c (n "safe-regex") (v "0.1.0") (d (list (d (n "safe-regex-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1z1as86mzxqh0jkanlwbngqlbrnqyr141wb6ffy83fp4dw3r67nm") (y #t)))

(define-public crate-safe-regex-0.1.1 (c (n "safe-regex") (v "0.1.1") (d (list (d (n "safe-regex-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0clilcqqy1yyyc45mq6z5szrkxxr3ajkcs0a1hclsi07shsc8hdn") (y #t)))

(define-public crate-safe-regex-0.2.0 (c (n "safe-regex") (v "0.2.0") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0kpv6n4ml5qwirnb155wi3skpnqawlbgl2wc70vkpsqwkwpr0j6j") (y #t)))

(define-public crate-safe-regex-0.2.1 (c (n "safe-regex") (v "0.2.1") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1x5j7hws2a747dpzis5h84310m98n0yj7h26dax33430zd37h4nk")))

(define-public crate-safe-regex-0.2.2 (c (n "safe-regex") (v "0.2.2") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0xfgskyafnv1wr90ywkfwj71xb13bhcn4s2qmd7cfhz2skxp5ny1")))

(define-public crate-safe-regex-0.2.3 (c (n "safe-regex") (v "0.2.3") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1i7l37vimpi5ib4dw3c4k0nsrg4y5ygb2qq4hvankp7blalgd1dl")))

(define-public crate-safe-regex-0.2.4 (c (n "safe-regex") (v "0.2.4") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1dbr248fvpwbyxq3r4hdb73b3rxs84wvy9akrccwg0qd7f4m0jcm")))

(define-public crate-safe-regex-0.2.5 (c (n "safe-regex") (v "0.2.5") (d (list (d (n "safe-regex-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1c12929wplp2yr58izhm3bn7h8vz2sa838an4zap61if6azqjlm1")))

(define-public crate-safe-regex-0.2.6 (c (n "safe-regex") (v "0.2.6") (d (list (d (n "safe-regex-macro") (r "^0.2.6") (d #t) (k 0)))) (h "17f4ygvxds9nj1cvqisda47p0rv7ggyiwf4vwyf0lj7ghk24paz6")))

(define-public crate-safe-regex-0.3.0 (c (n "safe-regex") (v "0.3.0") (d (list (d (n "safe-regex-macro") (r "^0.3") (d #t) (k 0)))) (h "1hj5xv3y1jml26z14v9rsrmmivfx7wgzmpxnrbh8knmr7kxgm52i") (f (quote (("std") ("default" "std"))))))

