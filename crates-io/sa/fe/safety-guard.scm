(define-module (crates-io sa fe safety-guard) #:use-module (crates-io))

(define-public crate-safety-guard-0.1.0 (c (n "safety-guard") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "05k6ni6kz9r720qkrcflkj0dql0k9yca0bcp0dd4bi37yg299bfj")))

(define-public crate-safety-guard-0.1.1 (c (n "safety-guard") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "11s4gvvjjwl1achbyzbbh33kdfk1d2zpw6cimwnkzg195yx52i9y")))

(define-public crate-safety-guard-0.1.2 (c (n "safety-guard") (v "0.1.2") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "0cxfj1mjjb47a2vaxdhv8i3rkl7mpijcqad1szfm5lg982c379fl")))

(define-public crate-safety-guard-0.1.4 (c (n "safety-guard") (v "0.1.4") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "0rdr0898g6yg4bm4milagilx5j90d6xvvp6kbmws88vb8cqb245c")))

(define-public crate-safety-guard-0.1.5 (c (n "safety-guard") (v "0.1.5") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "1bbfd4fv943xa09v0q5pbzmqggaarhvg9ngrxfk2il117f6a3yyb")))

(define-public crate-safety-guard-0.1.6 (c (n "safety-guard") (v "0.1.6") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "1vz22fg4v0s3ijyx69p7zirzn62ik4p457sq21fnv40mzqzp7wq7")))

(define-public crate-safety-guard-0.1.7 (c (n "safety-guard") (v "0.1.7") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "124pzmy71s11d2dbw9vwsmb6k94p9yjyvq5x78jl1bd5gg4wbnj2")))

(define-public crate-safety-guard-0.1.8 (c (n "safety-guard") (v "0.1.8") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "1m82k4qwy413nrjln51dqsz17868hgsbhrs2cgvi9znqc05gz5n0")))

(define-public crate-safety-guard-0.1.9 (c (n "safety-guard") (v "0.1.9") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "1givpcdwh7037mi8dbg95ajn2sqdjyaywzyx55bn0wsncymbba8i")))

