(define-module (crates-io sa fe safehouse) #:use-module (crates-io))

(define-public crate-safehouse-0.1.0 (c (n "safehouse") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1gv7bn6wv69lkqrk92n03vgc32kjqxpc74vhshm8sjgsf6z7c830")))

