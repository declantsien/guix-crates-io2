(define-module (crates-io sa fe safe_attr) #:use-module (crates-io))

(define-public crate-safe_attr-1.0.0 (c (n "safe_attr") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)))) (h "0g8kpiv2bsqgbkykal5r4pl12bvxf2cmkq574v7ws8x8p41xfvcg")))

