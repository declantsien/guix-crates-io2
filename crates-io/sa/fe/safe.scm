(define-module (crates-io sa fe safe) #:use-module (crates-io))

(define-public crate-safe-0.0.1 (c (n "safe") (v "0.0.1") (h "159dm27ldvrr3whjd2q32n52flb5m38w2mb8c0g5smxkfxyycw4c")))

(define-public crate-safe-0.1.0 (c (n "safe") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "0ikwri5i5j35sbr25pxsq3m7wwf0vsp2nywxr6r4w2f962pnx035")))

