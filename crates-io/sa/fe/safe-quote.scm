(define-module (crates-io sa fe safe-quote) #:use-module (crates-io))

(define-public crate-safe-quote-1.0.9 (c (n "safe-quote") (v "1.0.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "safe-proc-macro2") (r "^1.0.20") (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vdmc3bbdsim9wkiblwz1bnn64904pwp60qpw653zwpw9b1mav2n") (f (quote (("proc-macro" "safe-proc-macro2/proc-macro") ("default" "proc-macro"))))))

(define-public crate-safe-quote-1.0.15 (c (n "safe-quote") (v "1.0.15") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "safe-proc-macro2") (r "^1.0.36") (k 0)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "1xw8iakyq0qgx340fh3b8fcdjp90mh3f9apispyflgqzhgvk1rbp") (f (quote (("proc-macro" "safe-proc-macro2/proc-macro") ("default" "proc-macro")))) (r "1.31")))

