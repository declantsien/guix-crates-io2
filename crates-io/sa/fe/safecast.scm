(define-module (crates-io sa fe safecast) #:use-module (crates-io))

(define-public crate-safecast-0.1.0 (c (n "safecast") (v "0.1.0") (h "1zn489lxdn8yksj3pvsbz68xkl1v3nqdgrm4zrv3jdr0zcg3d48x")))

(define-public crate-safecast-0.1.1 (c (n "safecast") (v "0.1.1") (h "0x5729hv4yknkbj5ripxm3h9pgk2h1w79jnd6xyhs1cqqvyz3n9h")))

(define-public crate-safecast-0.1.2 (c (n "safecast") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1f03asym1v2xz1brvbmk515rr2s3ydwqd8c6zkrq2cl0ds1dsrgh")))

(define-public crate-safecast-0.1.3 (c (n "safecast") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pas0a2sxndpz83s1c03fir83b00xap89iqck4z4daw8hkwzszl4")))

(define-public crate-safecast-0.1.4 (c (n "safecast") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1n6b05a35c3ga8g5j7kq96cppa246c97r7rl3bxqbha7b48djlf9")))

(define-public crate-safecast-0.2.0 (c (n "safecast") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sc7mqygv1fcsz1yxpkqczjzvy9ajr6w717fc1qkx6smlvfvsp0v")))

(define-public crate-safecast-0.2.1 (c (n "safecast") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kmgnlsxcxvispak2z7rb7had1z2sxmgaaq5405ridymfaazm7ip")))

(define-public crate-safecast-0.2.2 (c (n "safecast") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p02cp6hr4ssql9sacx1n7w1cv1x8pcvrfbv12n1byg7mnysy5nr")))

