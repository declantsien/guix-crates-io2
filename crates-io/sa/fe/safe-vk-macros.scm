(define-module (crates-io sa fe safe-vk-macros) #:use-module (crates-io))

(define-public crate-safe-vk-macros-0.1.0 (c (n "safe-vk-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p7ckq8mld72jhy1mmmbyggyqijpsc4pbv653jyf0ld82bd0vdc0") (f (quote (("default"))))))

