(define-module (crates-io sa fe safe-path) #:use-module (crates-io))

(define-public crate-safe-path-0.1.0 (c (n "safe-path") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.100") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1yvz8hwszj4hdmlxx374ibqq6qx37hbpp81yr9krp88a4b9vs2lq")))

