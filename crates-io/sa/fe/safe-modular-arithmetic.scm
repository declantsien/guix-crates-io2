(define-module (crates-io sa fe safe-modular-arithmetic) #:use-module (crates-io))

(define-public crate-safe-modular-arithmetic-0.1.0 (c (n "safe-modular-arithmetic") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "14gywsmf3d5dzl16f00n6c536dpmj0rahbnmxll0wl8fqbigjmx3")))

(define-public crate-safe-modular-arithmetic-0.1.1 (c (n "safe-modular-arithmetic") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1qcrjxmwljhhz04q21fyvkq7ydr1h7kvgz60x45i43hprnprs8bq")))

(define-public crate-safe-modular-arithmetic-0.2.0 (c (n "safe-modular-arithmetic") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "15xahzpikkviwyv9m6cnc8wcmq63rb1m4bjb7vh97yrlg0fl6p53")))

(define-public crate-safe-modular-arithmetic-0.2.1 (c (n "safe-modular-arithmetic") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0s8sm6x0k031nqxp0xl0qn41a8w77imic7k17hyqxd51dxlnb05f")))

(define-public crate-safe-modular-arithmetic-0.2.2 (c (n "safe-modular-arithmetic") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1m7a8cf5bpyy7d2a9gdkj7rl6bmd3zbb0waxr2ysdxpwinlksk7w")))

(define-public crate-safe-modular-arithmetic-0.2.3 (c (n "safe-modular-arithmetic") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0s5b7dw0vkqs95fbmz5ggn37pkv485y9kmk0ks6gm3j6576q1l0k")))

(define-public crate-safe-modular-arithmetic-0.2.4 (c (n "safe-modular-arithmetic") (v "0.2.4") (d (list (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "02hbnxcinvhad8raxzdfp8lrrd1fpfgixmrp9gnq538jflcpylrm")))

