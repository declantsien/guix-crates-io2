(define-module (crates-io sa fe saferm) #:use-module (crates-io))

(define-public crate-saferm-0.1.0 (c (n "saferm") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mmq8i35pk5bmwmjvcwvhw23gjc3yv76ia131db2adxggkbjh1bi")))

(define-public crate-saferm-0.1.1 (c (n "saferm") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s7w1sym74ahyajf8wi50yp1slkv524x695zv2fi80aqm325yaf7")))

(define-public crate-saferm-0.1.2 (c (n "saferm") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "122xdzz43b301fj9xl6zag3zj21gvsp9k7jqb4frs7ynxb1mfwlk")))

(define-public crate-saferm-0.1.3 (c (n "saferm") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nj7fprpdvjd2i2dn0x8gh4xjdrf3qhxqlj1yiw61ck2478lkgxn")))

(define-public crate-saferm-0.1.4 (c (n "saferm") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cvhfpj197q0q9a06x89qsnm3yz6zjypsp0ar146b6qgcbrbz45g")))

