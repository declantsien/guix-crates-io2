(define-module (crates-io sa fe safe-vk-common) #:use-module (crates-io))

(define-public crate-safe-vk-common-0.1.0 (c (n "safe-vk-common") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0z4cfqcyv21kxdrh2sqi77pginnixn0rn568lgmnljpn3m469i3l")))

