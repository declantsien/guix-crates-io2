(define-module (crates-io sa fe safecoin-banks-interface) #:use-module (crates-io))

(define-public crate-safecoin-banks-interface-1.6.16 (c (n "safecoin-banks-interface") (v "1.6.16") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.6.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "16g8rh53mlimxsfs46w1p3pkqrwyfn6hqi5bm2dkgwmi14qrc89j")))

(define-public crate-safecoin-banks-interface-1.6.18 (c (n "safecoin-banks-interface") (v "1.6.18") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.6.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1v15cidlyv0256xp64936lzfjzdyshrmwxz6mizc7pp8jzw6n3kb")))

(define-public crate-safecoin-banks-interface-1.6.19 (c (n "safecoin-banks-interface") (v "1.6.19") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "safecoin-sdk") (r ">=1.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0ipw7sh579gyvyv79k545hhapqclc3ypdm61yzj9z0ap0rgndicv")))

(define-public crate-safecoin-banks-interface-1.7.17 (c (n "safecoin-banks-interface") (v "1.7.17") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.7.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "105y5ikh3ja9mv623kjz3ag6z3gr9cdxsvmaign8809nhzd4p9f0")))

(define-public crate-safecoin-banks-interface-1.8.12 (c (n "safecoin-banks-interface") (v "1.8.12") (d (list (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "00m6i0l7wdspr5y0qgcpkgm0q6ydmgpn50pkqxdmklyw5havsgja")))

(define-public crate-safecoin-banks-interface-1.9.29 (c (n "safecoin-banks-interface") (v "1.9.29") (d (list (d (n "safecoin-sdk") (r "=1.9.29") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xznpjc2dfv43lqwcvn3yjq770fj4zxg7p8zj89ybzw24xz6gp53")))

(define-public crate-safecoin-banks-interface-1.10.32 (c (n "safecoin-banks-interface") (v "1.10.32") (d (list (d (n "safecoin-sdk") (r "=1.10.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vnlacv5cdka0r54nyqyqp3sc5ingg2f4f4ryw7xh37xmviqrdgi")))

(define-public crate-safecoin-banks-interface-1.10.34 (c (n "safecoin-banks-interface") (v "1.10.34") (d (list (d (n "safecoin-sdk") (r "=1.10.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.27.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hk7jf4vhwfi83drqfnl2n7brdqz3km0k2v5gpp2432d8ircfgxb")))

(define-public crate-safecoin-banks-interface-1.14.3 (c (n "safecoin-banks-interface") (v "1.14.3") (d (list (d (n "safecoin-sdk") (r "=1.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nkl31ifd6niki0665sfhwzrzm0a33mrr1mb2xc3s3f14nssnwf5")))

(define-public crate-safecoin-banks-interface-1.14.17 (c (n "safecoin-banks-interface") (v "1.14.17") (d (list (d (n "safecoin-sdk") (r "=1.14.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z8fziik82j1rh95cvl6wld97lyaq0p1qw5n68h3nk2x2blxxv2x")))

