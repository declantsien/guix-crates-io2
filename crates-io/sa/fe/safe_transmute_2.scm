(define-module (crates-io sa fe safe_transmute_2) #:use-module (crates-io))

(define-public crate-safe_transmute_2-0.1.0 (c (n "safe_transmute_2") (v "0.1.0") (h "1x6myrp2zmrydxh7ra4245i610na9nay448m116flw7l1cab78i4") (y #t)))

(define-public crate-safe_transmute_2-0.1.1 (c (n "safe_transmute_2") (v "0.1.1") (h "069w8lrvn5jpszzg2dx83q0hpq2pzwmz38sii44xcc11rfiyf4nc")))

