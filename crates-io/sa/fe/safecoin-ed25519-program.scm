(define-module (crates-io sa fe safecoin-ed25519-program) #:use-module (crates-io))

(define-public crate-safecoin-ed25519-program-1.8.12 (c (n "safecoin-ed25519-program") (v "1.8.12") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "safecoin-logger") (r "=1.8.12") (d #t) (k 2)) (d (n "safecoin-sdk") (r "=1.8.12") (d #t) (k 0)))) (h "1djii3ap700hsmh080fz2pzlg0lmyxdd4d2vabxmhz90jj1438zk")))

