(define-module (crates-io sa fe safe-proc-macro2) #:use-module (crates-io))

(define-public crate-safe-proc-macro2-1.0.24 (c (n "safe-proc-macro2") (v "1.0.24") (d (list (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0gnql5zab6fkp7lv3w85i418hknwsdand6l14vbp7hv771ls172c") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro"))))))

(define-public crate-safe-proc-macro2-1.0.36 (c (n "safe-proc-macro2") (v "1.0.36") (d (list (d (n "safe-quote") (r "^1.0") (k 2)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0rn6l83yfb2ayg9l3fnpmdq8xlk2mnvxl666jqrg1b17rmnm6k41") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.31")))

(define-public crate-safe-proc-macro2-1.0.67 (c (n "safe-proc-macro2") (v "1.0.67") (d (list (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "safe-quote") (r "^1.0") (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "1dhfy8fxqf31cbsmmbfcbl0apwj8kylhvz8k7jm6hwdqgpk5pn3z") (f (quote (("span-locations") ("proc-macro") ("nightly") ("default" "proc-macro")))) (r "1.56")))

