(define-module (crates-io sa fe safeminsort) #:use-module (crates-io))

(define-public crate-safeminsort-1.0.0 (c (n "safeminsort") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0cgapvcb1vq4k4b7w6d2z73z5gibvaqwm8cnfr271dgs6qs83c7q") (y #t)))

(define-public crate-safeminsort-1.1.0 (c (n "safeminsort") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "14mshwlmd0mbdxkvrza1p4b224fi9qbjr890g6ad9bdlq7ykdhlq") (y #t)))

(define-public crate-safeminsort-1.1.1 (c (n "safeminsort") (v "1.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "02q3rrvrc2ixgzlk92gs10cp5ly34fqs6ydn7724hpja164r1al7") (y #t)))

