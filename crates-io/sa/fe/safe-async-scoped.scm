(define-module (crates-io sa fe safe-async-scoped) #:use-module (crates-io))

(define-public crate-safe-async-scoped-0.1.0 (c (n "safe-async-scoped") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "1lxxflgjl2fqdn78dswfsg794w2aq5vs6s30gbxlq86mx2qpcfa9")))

(define-public crate-safe-async-scoped-0.1.1 (c (n "safe-async-scoped") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "10qg0vvknppab8a5kzkmbl1m59vfnrr0j788bv3dqskks6yqr6mp")))

