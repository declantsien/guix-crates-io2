(define-module (crates-io sa fe safe_cache) #:use-module (crates-io))

(define-public crate-safe_cache-0.1.0 (c (n "safe_cache") (v "0.1.0") (h "0rm8j81s8r7pd3qvlbrylf9935xpy2mq0j4qgfz3glfmbbyliz4q")))

(define-public crate-safe_cache-0.1.1 (c (n "safe_cache") (v "0.1.1") (h "1ywxf4x0hwr3f9va31ivs5nxf4ss17mg83q8dix84i1nxn2s6ask")))

(define-public crate-safe_cache-0.1.2 (c (n "safe_cache") (v "0.1.2") (h "18h6a0bjhg7v7w8jprdmrqzsziibzx7njn0rrc4z0nmp7askdcxn")))

(define-public crate-safe_cache-0.2.0 (c (n "safe_cache") (v "0.2.0") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "0pq5p1vhzbzniicx4zp1n0d5gr8j1wrs61rd8wc854sq4zymj3la")))

(define-public crate-safe_cache-0.2.1 (c (n "safe_cache") (v "0.2.1") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "19h5v0kl25nff6yfwrxd4h5fphrjdfrgb8jgxlvfl5pn1dyk7rky")))

(define-public crate-safe_cache-0.2.2 (c (n "safe_cache") (v "0.2.2") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "time"))) (d #t) (k 0)))) (h "1qwh72py86168m70dskvd8i49cpnj2affnpgy1imw39iw0ipn6vv")))

