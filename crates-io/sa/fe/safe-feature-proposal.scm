(define-module (crates-io sa fe safe-feature-proposal) #:use-module (crates-io))

(define-public crate-safe-feature-proposal-1.0.0 (c (n "safe-feature-proposal") (v "1.0.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "safe-token") (r "^3.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "safecoin-program") (r "^1.7.17") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.7.17") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.7.17") (d #t) (k 2)))) (h "19b1ajb3im423ykjsqywiibnyh7rzvcajd1hqy1450ylf7dcx672") (f (quote (("test-bpf") ("no-entrypoint"))))))

