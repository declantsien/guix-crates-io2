(define-module (crates-io sa fe safety-postgres) #:use-module (crates-io))

(define-public crate-safety-postgres-0.1.0 (c (n "safety-postgres") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "testcontainers") (r "^0.15") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7") (f (quote ("with-chrono-0_4"))) (d #t) (k 0)))) (h "0cz3fh3hw2w4jw1c3wqvlhdb8rnp8ridw4lznm51aw3zj4sjk7cq")))

