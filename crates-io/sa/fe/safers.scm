(define-module (crates-io sa fe safers) #:use-module (crates-io))

(define-public crate-safers-0.1.5 (c (n "safers") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "syspass_api") (r "^0.1.4") (d #t) (k 0)))) (h "064lpxd3wxvvb69k73yb220dfxpr3g86bsibrm3sbx804zcyy5gr")))

(define-public crate-safers-0.1.6 (c (n "safers") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syspass_api") (r "^0.1.5") (d #t) (k 0)))) (h "0f8bsbvgkddxb9qlwq0l76r64z9j3sgnxyfkgxxg5isnyfvpawqx")))

