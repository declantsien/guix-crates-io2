(define-module (crates-io sa fe safecoin-notifier) #:use-module (crates-io))

(define-public crate-safecoin-notifier-1.6.16 (c (n "safecoin-notifier") (v "1.6.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rzhbn5646igi3q0blmfr4d76n363gmwijf3mkhvf6ic0wbylp24")))

