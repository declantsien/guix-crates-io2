(define-module (crates-io sa fe safecoin-ownable) #:use-module (crates-io))

(define-public crate-safecoin-ownable-1.6.16 (c (n "safecoin-ownable") (v "1.6.16") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "safecoin-runtime") (r "=1.6.16") (d #t) (k 2)) (d (n "safecoin-sdk") (r "=1.6.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "133br3h5lszvpj8dq1rs2s4vwwggkdaya53w61w3h32h9xr9jpwj")))

