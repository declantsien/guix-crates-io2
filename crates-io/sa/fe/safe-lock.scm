(define-module (crates-io sa fe safe-lock) #:use-module (crates-io))

(define-public crate-safe-lock-0.1.0 (c (n "safe-lock") (v "0.1.0") (h "0chdar4almvlprmklsa9iv65yqnn7vpnq594k7l2c2l3r6yy06bm")))

(define-public crate-safe-lock-0.1.1 (c (n "safe-lock") (v "0.1.1") (h "0xra0s3vwviwx2rp7296vgb3ibqnc48pzfs4iphhcnshibzdkz88")))

(define-public crate-safe-lock-0.1.2 (c (n "safe-lock") (v "0.1.2") (h "1nxy2k6c3hp6h11hrdz9hiv715sfr8987d7z6c1lbhd54b0k7hky")))

(define-public crate-safe-lock-0.1.3 (c (n "safe-lock") (v "0.1.3") (h "141w4psg59cy4s420lb7m2x5j96w6id1xzsaxdiwzk3kg7dp6z87")))

