(define-module (crates-io sa fe safecoin-sdk-macro) #:use-module (crates-io))

(define-public crate-safecoin-sdk-macro-1.6.16 (c (n "safecoin-sdk-macro") (v "1.6.16") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bzzdr1c1iqjqrj4jp9rb76clx33dzljf4f0nx81p5m1digpabp2")))

(define-public crate-safecoin-sdk-macro-1.6.17 (c (n "safecoin-sdk-macro") (v "1.6.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g2ms4249pxs4b85jpys4shva6gf9rkpynfdxvy0mjsfzfal4cdb")))

(define-public crate-safecoin-sdk-macro-1.6.18 (c (n "safecoin-sdk-macro") (v "1.6.18") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15lv06i0pplp38as76jmgj5lhacapffsh8ymrk72xzsalggj8x15")))

(define-public crate-safecoin-sdk-macro-1.7.17 (c (n "safecoin-sdk-macro") (v "1.7.17") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "046ywfzk6i93qqhnrapb0549z5aj7wmldnp6hg032y1pd1avy5p4")))

(define-public crate-safecoin-sdk-macro-1.8.12 (c (n "safecoin-sdk-macro") (v "1.8.12") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19h2mb6mdnc3f1x5h6iz8js74vkakrmny6f2j8q44kws8z7gv2y1")))

(define-public crate-safecoin-sdk-macro-1.9.29 (c (n "safecoin-sdk-macro") (v "1.9.29") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12xlcrd8xzggbhdwp4m7m2vdnbahxqa04jkig8w24bdpbfm9i8mv")))

(define-public crate-safecoin-sdk-macro-1.10.32 (c (n "safecoin-sdk-macro") (v "1.10.32") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ybqsbw36m9ca78477af7d2gs00xijbpn6zcslcar08nzin9qc56")))

(define-public crate-safecoin-sdk-macro-1.10.34 (c (n "safecoin-sdk-macro") (v "1.10.34") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0myrq0qgdkgbvm0gz6i199brx00pfmzasg5jw4jzm6dh2agyvk30")))

(define-public crate-safecoin-sdk-macro-1.14.3 (c (n "safecoin-sdk-macro") (v "1.14.3") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ilqnv52cb70yr8lk1xrdhhdg4mizcjajpxlrxarrsf81i2w0l0r")))

(define-public crate-safecoin-sdk-macro-1.14.17 (c (n "safecoin-sdk-macro") (v "1.14.17") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q5k4d4943ky89mfalw5n8y1abv9dmsdca6a9c5xs5zfmdkvfzal")))

