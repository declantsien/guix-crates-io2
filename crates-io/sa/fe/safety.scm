(define-module (crates-io sa fe safety) #:use-module (crates-io))

(define-public crate-safety-0.1.0 (c (n "safety") (v "0.1.0") (d (list (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "syntex") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.52.0") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.52.0") (d #t) (k 0)))) (h "1xsfggid3606hq1fqri1zddk9i6nasck97qqa5xl4j3fmvxbd6cd")))

