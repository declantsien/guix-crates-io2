(define-module (crates-io sa fe safe-package) #:use-module (crates-io))

(define-public crate-safe-package-1.0.0 (c (n "safe-package") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debug_print") (r "^1.0.0") (d #t) (k 0)) (d (n "fork") (r "^0.1.21") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "14kl8plz0fypvc32m7bl9411g2sjgm8ihcjbkjhhaq4k1856l31n")))

