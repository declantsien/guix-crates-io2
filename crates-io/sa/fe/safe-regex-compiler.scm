(define-module (crates-io sa fe safe-regex-compiler) #:use-module (crates-io))

(define-public crate-safe-regex-compiler-0.1.0 (c (n "safe-regex-compiler") (v "0.1.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "0fwqsh1kc0nz4kzxx1m24ywaja600afbc5lkrzx2nxpgmf27ribl") (y #t)))

(define-public crate-safe-regex-compiler-0.1.1 (c (n "safe-regex-compiler") (v "0.1.1") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "1907q4vlric4pjnjb3885v7i5ajfxgic2j49w3y58rgmkrmdbwkj") (y #t)))

(define-public crate-safe-regex-compiler-0.2.0 (c (n "safe-regex-compiler") (v "0.2.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "06bdwsbsrc129jbnbhb5drkw6argbikhyvhrmkaxwdzpcp07s85c") (y #t)))

(define-public crate-safe-regex-compiler-0.2.1 (c (n "safe-regex-compiler") (v "0.2.1") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "0nxidkf2j86wi677mf1hx2i65xsjq3ysp9w0gqpnl9wa1fs16zl2")))

(define-public crate-safe-regex-compiler-0.2.2 (c (n "safe-regex-compiler") (v "0.2.2") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "1dr1p8xj9hb4pc2187l4hnnqbz2qcda6rf881rx21iqpxswi8hcp")))

(define-public crate-safe-regex-compiler-0.2.3 (c (n "safe-regex-compiler") (v "0.2.3") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "05ndg0869ymiz4jj582bb07758dwyj37s4dw8rmq417wzjk8k1m8")))

(define-public crate-safe-regex-compiler-0.2.4 (c (n "safe-regex-compiler") (v "0.2.4") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "0ivhlnmc6v8xs5sb0qy0382r7d7kdk7l8sbxfci231ghgak8igkp")))

(define-public crate-safe-regex-compiler-0.2.5 (c (n "safe-regex-compiler") (v "0.2.5") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "0hhmqpvmbkrg4q7fdid0077bm309nmbizswx4xjscahab6p6z9zv")))

(define-public crate-safe-regex-compiler-0.2.6 (c (n "safe-regex-compiler") (v "0.2.6") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "06v2rr4gcpjzfdm0pigdbs2xs90xs5ggl6syb5i7kc7zig3zhwbd")))

(define-public crate-safe-regex-compiler-0.3.0 (c (n "safe-regex-compiler") (v "0.3.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "1cqmxpqrfmm817k4xhjx38b5fz1zz1y26z1ik3bcy6r5c4gaw8p8")))

