(define-module (crates-io sa fe safe_paillier_rust) #:use-module (crates-io))

(define-public crate-safe_paillier_rust-0.1.0 (c (n "safe_paillier_rust") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0waqrsw08r407i1w72a2y1cc8mmxvr818yid3b064qry00nkqn8q")))

(define-public crate-safe_paillier_rust-0.1.1 (c (n "safe_paillier_rust") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1z2kbr29y6mb2xc5kqpdnc5zy0k75dwcnbjx8bpz3zrppzsb1vcq")))

(define-public crate-safe_paillier_rust-0.1.2 (c (n "safe_paillier_rust") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1av2w11ch558xakf1liawrwjcyxi4mnzyqb9qi1rbvkzy1r1gan4")))

(define-public crate-safe_paillier_rust-0.1.3 (c (n "safe_paillier_rust") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1nb4r9r98j1fisvqqgb3bvph0z65yh6ji522wi35dgz0n4fiim4y")))

(define-public crate-safe_paillier_rust-0.1.4 (c (n "safe_paillier_rust") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-modular") (r "^0.6.1") (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09ika43pgfklccdhn4nh650lzxpg8rwlghvchga6k4pbc35dlxs0")))

