(define-module (crates-io sa fe safeword) #:use-module (crates-io))

(define-public crate-safeword-0.1.0 (c (n "safeword") (v "0.1.0") (d (list (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio-signal") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2.0") (d #t) (k 2)))) (h "0q73k3h8kv547w7966k0yqs9q3gjdd7zac1p13ssa0wxwnccwyaf")))

