(define-module (crates-io sa fe safer-bytes) #:use-module (crates-io))

(define-public crate-safer-bytes-0.1.0 (c (n "safer-bytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "04v8xk6zkqbpqn2q9iwyd3rhcyb6j9d3z9qxwff6v169ijzbndrc")))

(define-public crate-safer-bytes-0.1.1 (c (n "safer-bytes") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "05qx06zvyvnffgkdd5j1a5cm59a3n5p7572v7jvbraq7w2w4ckzj")))

(define-public crate-safer-bytes-0.2.0 (c (n "safer-bytes") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0m7aynnr0ngy53p0yl2f1sv381pdvvb92w5mzhwa89sgaf6wf54q")))

