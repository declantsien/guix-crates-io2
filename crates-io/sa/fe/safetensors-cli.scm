(define-module (crates-io sa fe safetensors-cli) #:use-module (crates-io))

(define-public crate-safetensors-cli-0.1.0 (c (n "safetensors-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "memmap2") (r "^0") (d #t) (k 0)) (d (n "safetensors") (r "^0.3.1") (d #t) (k 0)))) (h "0ynyyd46jg0dsmqqxjvr7v2ga106789b8hb8nlnii7md4cgn1b6l")))

