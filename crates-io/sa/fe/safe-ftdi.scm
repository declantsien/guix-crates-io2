(define-module (crates-io sa fe safe-ftdi) #:use-module (crates-io))

(define-public crate-safe-ftdi-0.1.0 (c (n "safe-ftdi") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 2)) (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0wm19kf2wvsagp7w3yy5rb06qi4f6qhyd8bicnwdx0mivnxplr5v")))

(define-public crate-safe-ftdi-0.2.0 (c (n "safe-ftdi") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 2)) (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0j3kiqdjbawbz9fbd6fv1wpca9bvflz6f3wlh5ipaa8g9ddwplbz")))

(define-public crate-safe-ftdi-0.2.1 (c (n "safe-ftdi") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 2)) (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)))) (h "18kj4amn385hj5f17yjp03n925w3hdpc2qiv6ycyz9vdz6xml7bw")))

(define-public crate-safe-ftdi-0.2.2 (c (n "safe-ftdi") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 2)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 2)) (d (n "bitreader") (r "^0.3.1") (d #t) (k 2)) (d (n "byteorder") (r "^1.2.6") (d #t) (k 2)) (d (n "libftdi1-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0wsw2gxndh1npp6v5yp6fn3x7y69ynzfah6p1fnkb47hpzah9r0c")))

