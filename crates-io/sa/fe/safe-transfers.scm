(define-module (crates-io sa fe safe-transfers) #:use-module (crates-io))

(define-public crate-safe-transfers-0.1.0 (c (n "safe-transfers") (v "0.1.0") (d (list (d (n "bincode") (r "= 1.1.4") (d #t) (k 0)) (d (n "crdts") (r "= 2.0.0") (d #t) (k 0)) (d (n "safe-nd") (r "= 0.9.0") (d #t) (k 0)) (d (n "serde") (r "~1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "threshold_crypto") (r "~0.3.2") (d #t) (k 0)))) (h "0gfw9ad9aqjpykp0wms93azh3ndi1idbz0bwwh2yf7hzb251acks")))

