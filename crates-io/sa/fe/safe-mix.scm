(define-module (crates-io sa fe safe-mix) #:use-module (crates-io))

(define-public crate-safe-mix-1.0.0 (c (n "safe-mix") (v "1.0.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0iz3558wz2j2cdf38snzyh5cd6dwhbqmbm4ha1nw321ss8ig8yvz") (f (quote (("strict") ("std") ("nightly") ("default" "std"))))))

(define-public crate-safe-mix-1.0.1 (c (n "safe-mix") (v "1.0.1") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0p110l33lgw1a21k4dpn5c4r38qa0hj1a753sw7b1rl24md0agbd") (f (quote (("std") ("default" "std"))))))

