(define-module (crates-io sa fe safeeft) #:use-module (crates-io))

(define-public crate-safeeft-0.0.2 (c (n "safeeft") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)))) (h "0kqy4l475g5gqrnl28p1dp0vhnfnz3sfjsjxdqsap9r6fvbqwjr1") (f (quote (("use-fma") ("doc"))))))

(define-public crate-safeeft-0.0.3 (c (n "safeeft") (v "0.0.3") (d (list (d (n "fma") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)))) (h "0jhs43qz9vx2b9y926x1djr3pnaq3szrvs65fda856caw3502yg9") (f (quote (("use-fma" "fma") ("doc" "fma"))))))

(define-public crate-safeeft-0.0.4 (c (n "safeeft") (v "0.0.4") (d (list (d (n "float-traits") (r "^0.0.2") (d #t) (k 0)) (d (n "fma") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)))) (h "06q2mgp1g7z1hrzzrlq2mlf0p7fy2dciq4xvs7xrg4hqzyg50x3z") (f (quote (("use-fma" "fma") ("doc" "fma"))))))

(define-public crate-safeeft-0.0.5 (c (n "safeeft") (v "0.0.5") (d (list (d (n "float-traits") (r "^0.0.3") (d #t) (k 0)) (d (n "fma") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)))) (h "1ycpqrdpjxhf858h48dh1sfqdwj6kqm7mxd2lq754cwficdafxc5") (f (quote (("use-fma" "fma") ("doc" "fma"))))))

