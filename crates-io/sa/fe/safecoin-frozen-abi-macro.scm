(define-module (crates-io sa fe safecoin-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-safecoin-frozen-abi-macro-1.6.16 (c (n "safecoin-frozen-abi-macro") (v "1.6.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hk98wgz77ycbply0rx6cjjlf8zjbwi0wc1d4j38cgfika3b6mlz")))

(define-public crate-safecoin-frozen-abi-macro-1.6.17 (c (n "safecoin-frozen-abi-macro") (v "1.6.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0an06vgi2b3bif69zbwv660ffg7714bl4r1qrjw0m55qqf48qkji")))

(define-public crate-safecoin-frozen-abi-macro-1.6.18 (c (n "safecoin-frozen-abi-macro") (v "1.6.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16x1mdg9ffs7798bbdb1dq9wm99fi2qc6jcxdhhhsffgxjkw7rj9")))

(define-public crate-safecoin-frozen-abi-macro-1.6.19 (c (n "safecoin-frozen-abi-macro") (v "1.6.19") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zix5d5h9ag4l9ad5gy7xn6i16knhhx5wv4k6wlp2fxqclwf3h54")))

(define-public crate-safecoin-frozen-abi-macro-1.7.17 (c (n "safecoin-frozen-abi-macro") (v "1.7.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w1ywb934ydh5ywffqygi71kpqfd9hh96f8g9zvdz0ddc8j0bkxk")))

(define-public crate-safecoin-frozen-abi-macro-1.8.12 (c (n "safecoin-frozen-abi-macro") (v "1.8.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q0pl6hyssh5gmk8cj7z6qpijsrcn8ya1l3iyrs3pph0ki1mvdg9")))

(define-public crate-safecoin-frozen-abi-macro-1.9.29 (c (n "safecoin-frozen-abi-macro") (v "1.9.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kq5zw2sp7jklx7859xpk6x5cf7r3jcqlqqq08dja7mhbgv3pvlk")))

(define-public crate-safecoin-frozen-abi-macro-1.10.32 (c (n "safecoin-frozen-abi-macro") (v "1.10.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fz2grhqp1022m70y1i9s501a5z37841724w914f85appl9c15rq")))

(define-public crate-safecoin-frozen-abi-macro-1.10.34 (c (n "safecoin-frozen-abi-macro") (v "1.10.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qj2yrfrkps6gqsfg436axxsbxi8l0gq7dsf5jljzb5n4rbxp0mw")))

(define-public crate-safecoin-frozen-abi-macro-1.14.3 (c (n "safecoin-frozen-abi-macro") (v "1.14.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0k21a37cxs5x31ypcbwl1jhhc3gybvagb06mhzjx6g4kr0d0zdja")))

(define-public crate-safecoin-frozen-abi-macro-1.14.17 (c (n "safecoin-frozen-abi-macro") (v "1.14.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "0g73m46lklk0pjkpkbkflr6vchv9sz0x6rpd3dcdh48pbq67mglr")))

