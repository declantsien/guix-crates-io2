(define-module (crates-io sa fe safe_http) #:use-module (crates-io))

(define-public crate-safe_http-0.1.0-beta.1 (c (n "safe_http") (v "0.1.0-beta.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "boar") (r "^0.2.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "safe_uri") (r "^0.1.0-beta.1") (d #t) (k 0)))) (h "0xqlrjlbgq14k6k61af8qjpv2pbkjp3xqb9dm7zkpnvnaf790igv")))

(define-public crate-safe_http-0.1.0-beta.2 (c (n "safe_http") (v "0.1.0-beta.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "boar") (r "^0.2.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "safe_uri") (r "^0.1.0-beta.2") (d #t) (k 0)))) (h "1pn25cipj4jzl3s2vxlqwvbz3gwaq15zfn4qrzzbb14cla064d8b")))

(define-public crate-safe_http-0.1.0-beta.3 (c (n "safe_http") (v "0.1.0-beta.3") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "boar_") (r "^0.2.4") (d #t) (k 0) (p "boar")) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "safe_uri") (r "^0.1.0-beta.3") (d #t) (k 0)))) (h "1q39gmxcykhrsrrp04h2nx2nscy8fra1m7p1cqazhknr7s9w9xp9") (f (quote (("boar"))))))

(define-public crate-safe_http-0.1.0-beta.4 (c (n "safe_http") (v "0.1.0-beta.4") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "safe_uri") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "shared_bytes") (r "^0.1.0-beta.4") (d #t) (k 0)))) (h "1w2bias08gsbqgai40phc24285viy1m1g3hn19nj01ix7xd9v5k8")))

