(define-module (crates-io sa fe safe-graph) #:use-module (crates-io))

(define-public crate-safe-graph-0.1.1 (c (n "safe-graph") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "1dwdfnnvyh62f8z2iamphifjg4xi8cr4i39izb9prrfhyyr65zbr")))

(define-public crate-safe-graph-0.1.2 (c (n "safe-graph") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "0i99jfp1a9j58grd5zz9w5hbmahv63aifsnn4y9k9ih3al1ajm47")))

(define-public crate-safe-graph-0.1.3 (c (n "safe-graph") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "1ri1zk6aw4cjw9hn779q1npb9jzlizfm15hxz33brxckxpn324bs")))

(define-public crate-safe-graph-0.1.4 (c (n "safe-graph") (v "0.1.4") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "0iz83s81p9v8pw1r4ag6c6hpljjz31lrn7mf947wpkxqsw6akx6v")))

(define-public crate-safe-graph-0.1.5 (c (n "safe-graph") (v "0.1.5") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "0shwl5blf02q4yi36m7cqjhcw2icq78mhnh3l1pdq48y75n0v5n2")))

(define-public crate-safe-graph-0.1.6 (c (n "safe-graph") (v "0.1.6") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)))) (h "1aplbc57cln6i5q050d12p71brwq8f9sr0kbi2hfxxml2jxd97fv")))

