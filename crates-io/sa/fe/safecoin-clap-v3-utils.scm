(define-module (crates-io sa fe safecoin-clap-v3-utils) #:use-module (crates-io))

(define-public crate-safecoin-clap-v3-utils-1.14.3 (c (n "safecoin-clap-v3-utils") (v "1.14.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rpassword") (r "^6.0") (d #t) (k 0)) (d (n "safecoin-perf") (r "=1.14.3") (d #t) (k 0)) (d (n "safecoin-remote-wallet") (r "=1.14.3") (k 0)) (d (n "safecoin-sdk") (r "=1.14.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.8.2") (d #t) (k 0)) (d (n "uriparse") (r "^0.6.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0q9b3wnl0f8azp6gfc717wqfnsyrxajdv2y9fw2qgx9y6bsxnx72")))

