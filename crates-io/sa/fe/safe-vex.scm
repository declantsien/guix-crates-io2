(define-module (crates-io sa fe safe-vex) #:use-module (crates-io))

(define-public crate-safe-vex-0.0.0 (c (n "safe-vex") (v "0.0.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1fn51zc4l7ziqm4zzhhsn2bwd12ws90lzd3pp7lq555ff1z5cznw")))

(define-public crate-safe-vex-0.1.0 (c (n "safe-vex") (v "0.1.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1xxwszj5q3h7rs9sagg6p9j60amf7yaak1b8npl3541p18n16hd5")))

(define-public crate-safe-vex-0.1.1 (c (n "safe-vex") (v "0.1.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0l23b6c5642myld7fbhqlc7iayr30p7xli8z352sjyja2vwqzfh3")))

(define-public crate-safe-vex-0.1.2 (c (n "safe-vex") (v "0.1.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0j46798k5iacj0kh1yginrm6yks7my9ja9qhbjwcvzix8nyfgzhk")))

(define-public crate-safe-vex-0.1.3 (c (n "safe-vex") (v "0.1.3") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1csv4i6hfhayga9bgf4fbyzi55mwq5pnqdz3ja9fm96r85m3fak3")))

(define-public crate-safe-vex-0.1.4 (c (n "safe-vex") (v "0.1.4") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "05cfsd803by71y6xnjnllzwa4njwi356djhj4inwgh93n70k1gc0")))

(define-public crate-safe-vex-0.2.0 (c (n "safe-vex") (v "0.2.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0bb2ysjd8y4q589226cljkr5s80215z1ai2b5pgyaifv4y5fh5bi")))

(define-public crate-safe-vex-0.3.0 (c (n "safe-vex") (v "0.3.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1iacy409znw12mjyi1fw925ya0i6axbaza4jw0krq0cfwclz9xsy")))

(define-public crate-safe-vex-0.3.1 (c (n "safe-vex") (v "0.3.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "06da0i0rh3bi54j9d1d2vkwibs8q5sa2fzvwifrb2c6jagkwrj81")))

(define-public crate-safe-vex-0.3.2 (c (n "safe-vex") (v "0.3.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "07p1y979775mfnbb1iwh9r0mvkj0ljwkgxd6bpapxpxwv9rxkcyn")))

(define-public crate-safe-vex-1.0.0 (c (n "safe-vex") (v "1.0.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "02bqzilw1kay4ljb3rm820g65yiw9vwkiq4j0q339d24wcw9fysm")))

(define-public crate-safe-vex-1.0.1 (c (n "safe-vex") (v "1.0.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0fsxr0qrzfkabqxcrfbii4zlnqvwrn0mm4j6mbb5yd4mjs062gsz")))

(define-public crate-safe-vex-1.0.2 (c (n "safe-vex") (v "1.0.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1c14xl41c81k2x5qwxzv2dd8hc3cq5r3nkbc79yiga13g82m3b3j")))

(define-public crate-safe-vex-1.0.3 (c (n "safe-vex") (v "1.0.3") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0acayv3cnql9rqgc5cy1g3dy0a5h9vpcfsabnfh9z4qrpv6yy82b")))

(define-public crate-safe-vex-1.0.4 (c (n "safe-vex") (v "1.0.4") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0rzvn740azrsdalkv2ppr99kkgc4c8vwn4k6djvvx95i4ijv28zg")))

(define-public crate-safe-vex-1.1.0 (c (n "safe-vex") (v "1.1.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "09kvich9ycndq4669m49k64jmjmry7i4kyl59dx2f89h5ncl154y")))

(define-public crate-safe-vex-1.1.1 (c (n "safe-vex") (v "1.1.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "12zqxvvgl2kzfl0al0y46hglx7hxacvvwlmgl9g4isvw0lzzgli6")))

(define-public crate-safe-vex-1.1.2 (c (n "safe-vex") (v "1.1.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "19yqyi7wrs56s7hrx22px5wvrdfhnzrjhwp682gmja8rxnfdxkja")))

(define-public crate-safe-vex-1.1.3 (c (n "safe-vex") (v "1.1.3") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "06inhk0xrd9669hqwj3k0w1r2f3n1cqgr4k118l6h20hi1jfqbpk")))

(define-public crate-safe-vex-1.1.4 (c (n "safe-vex") (v "1.1.4") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1rvzizhnpm0arj0d7w2ghq7rrqlqqbca3ixcfygjhqh7kskhhdai")))

(define-public crate-safe-vex-1.2.0 (c (n "safe-vex") (v "1.2.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1psnp9qfkz13x3ba9f4gcgdsxp4c1p7a4804kr50bqavjh2p5g45")))

(define-public crate-safe-vex-2.0.0 (c (n "safe-vex") (v "2.0.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1bkgs5a5j8h3y7xvr276nrl1rfjwd2m4ash59wgb7wjz44s7f6n9")))

(define-public crate-safe-vex-2.0.1 (c (n "safe-vex") (v "2.0.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1j7yy059izcwqmn9mjh4l4pwx6grbr03iyz7n9a74va7wwhnnggy")))

(define-public crate-safe-vex-2.1.0 (c (n "safe-vex") (v "2.1.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "006ii4ld3yfx6nzqlnn9vhmzp37ykp4xkzxlsbmvbrayw629dc39") (f (quote (("simulate"))))))

(define-public crate-safe-vex-2.2.0-beta (c (n "safe-vex") (v "2.2.0-beta") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1fp7sw7fqrg6zzgcnfxmsdsa7l0ig9z00rfqmk2b2jib0hlpa8jn") (f (quote (("simulate"))))))

(define-public crate-safe-vex-2.2.0 (c (n "safe-vex") (v "2.2.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0hmngdsqdcbi79w7i9aay504z1ll6h7jn59kmxj3ivvvk3s7m1cr") (f (quote (("simulate"))))))

(define-public crate-safe-vex-2.2.1 (c (n "safe-vex") (v "2.2.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0zk82ml7c416l51vqfffaw1rb8gfjklyc3y2z98ip8fsjz5vj23s") (f (quote (("simulate"))))))

(define-public crate-safe-vex-2.2.2 (c (n "safe-vex") (v "2.2.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "13gw285df6pv51m6ls0cs8yfdbz1h0szsr42fvcbdy9g154gqj88") (f (quote (("simulate"))))))

(define-public crate-safe-vex-3.0.0-beta (c (n "safe-vex") (v "3.0.0-beta") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1snm875c3m812wyf7bmxshpk9p36kbxfigfg3l970nbv72956b1j")))

(define-public crate-safe-vex-3.0.0 (c (n "safe-vex") (v "3.0.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1bk5acq7ilv8fsgj94ll6znr2ly66iip887hnq545ggns636kj5c")))

(define-public crate-safe-vex-3.1.0 (c (n "safe-vex") (v "3.1.0") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "087kp1a7g8401ymx1jzf2vx0jd8zdg0j5ksq73vnfivkajckbfy9")))

(define-public crate-safe-vex-3.1.1 (c (n "safe-vex") (v "3.1.1") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "06awi03sazc6046k337wn8lhd746k9lk5frsda0nwwgx9g2ai5s0")))

(define-public crate-safe-vex-3.1.2 (c (n "safe-vex") (v "3.1.2") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1q3h72mbr6kbyh3n9k3ychdr8w8p28cf5vp0dkwi03ssfz7jhg9b")))

(define-public crate-safe-vex-3.1.3 (c (n "safe-vex") (v "3.1.3") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1m3k5ndrlwgynlrh471s44dyh6j1dvsvlwrmsfq9mgxhdkjgwb8r")))

(define-public crate-safe-vex-3.2.0-experimental (c (n "safe-vex") (v "3.2.0-experimental") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "17p9sjn8rvbss9l4idwi99nqxji1d2dlcaqj437ip5d0n5zxk0r6")))

(define-public crate-safe-vex-3.2.2-experimental (c (n "safe-vex") (v "3.2.2-experimental") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1kmb49zq7h9arbdw3vhca3ws2yz1l1gbijqazc3hsa6v1zkzrrdc")))

(define-public crate-safe-vex-3.2.3-experimental (c (n "safe-vex") (v "3.2.3-experimental") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1j79w7q7n7x60c3fgjr560jm5a2q2yvaic9xc5vgir1crcjym5fn")))

(define-public crate-safe-vex-3.1.4 (c (n "safe-vex") (v "3.1.4") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "1rgg6wq5h9yd6pwcgmylapi087w23gbfrrlclhq5xlkh89w24bj6")))

(define-public crate-safe-vex-3.1.7 (c (n "safe-vex") (v "3.1.7") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "171416y0wp9s2pi5cv5xvq9666zxfjnn85nh9by55d1ya11a1hkq")))

(define-public crate-safe-vex-3.1.8 (c (n "safe-vex") (v "3.1.8") (d (list (d (n "vex-rt") (r "^0.15.1") (d #t) (k 0)))) (h "0wrcby26phn49pq202gjm8ylsqg57yj3drxxsf5383pbrgpj9m58")))

