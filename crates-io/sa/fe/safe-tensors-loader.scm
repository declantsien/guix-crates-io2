(define-module (crates-io sa fe safe-tensors-loader) #:use-module (crates-io))

(define-public crate-safe-tensors-loader-0.0.0 (c (n "safe-tensors-loader") (v "0.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "onnx-protobuf") (r "^0.2.1") (f (quote ("safetensors"))) (d #t) (k 0)) (d (n "safetensors") (r "^0.3.0") (d #t) (k 0)) (d (n "serde-pickle") (r "^1.1.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 2)))) (h "0bmqhsck1vcvr3c4xfxv9226g5idxzvr19v016013fi7jzhd1r67") (f (quote (("default"))))))

