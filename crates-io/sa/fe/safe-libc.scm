(define-module (crates-io sa fe safe-libc) #:use-module (crates-io))

(define-public crate-safe-libc-0.1.0 (c (n "safe-libc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11ggwc6s8x24fnab4ygpf2nhbl1zaml0q892v45y9s4l4pyplga2")))

(define-public crate-safe-libc-0.1.1 (c (n "safe-libc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "191mld83bb5aahm8i6v6d9i4idzlnw0g71kr15nl3za5dy4g1hc8")))

(define-public crate-safe-libc-0.1.2 (c (n "safe-libc") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "033l60pbj430cazl78v2ibykkwbadcmrniiw4xass25kpyw2zfwr")))

(define-public crate-safe-libc-0.1.3 (c (n "safe-libc") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z8rhfg288bprmx4wv53pvz67pk3skwd0mx30anxxmnc3q4d5395")))

(define-public crate-safe-libc-0.1.4 (c (n "safe-libc") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0669lz1kdajrvn9z6d6qxg9cq6q33pkp8djvhbh992cpyx1n111m")))

