(define-module (crates-io sa fe safe-tensors-export) #:use-module (crates-io))

(define-public crate-safe-tensors-export-0.0.0 (c (n "safe-tensors-export") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "safe-tensors-loader") (r "^0.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1l1z693d2qnx6niknk6z103hxiv24hx6q4dqnfg1ymcq2zig2zrk") (f (quote (("default"))))))

(define-public crate-safe-tensors-export-0.1.0 (c (n "safe-tensors-export") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.10") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "safe-tensors-loader") (r "^0.0.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13dadpbrqyk1bvc9w2mjiz88mvqy3sbzksc5ybhfhxbb6lk35wzw") (f (quote (("default"))))))

