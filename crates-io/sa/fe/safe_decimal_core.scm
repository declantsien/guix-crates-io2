(define-module (crates-io sa fe safe_decimal_core) #:use-module (crates-io))

(define-public crate-safe_decimal_core-0.1.0 (c (n "safe_decimal_core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "06r3p74kr3a0gghzwk0vwc7whgilkqf02wg8ba1dgick2nh9x388")))

(define-public crate-safe_decimal_core-0.1.1 (c (n "safe_decimal_core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0rgj2aq3rvdbvr4xl1ml115s1f8f4328qylpzs6r8n83cjipzjjf")))

