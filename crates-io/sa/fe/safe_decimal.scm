(define-module (crates-io sa fe safe_decimal) #:use-module (crates-io))

(define-public crate-safe_decimal-0.1.0 (c (n "safe_decimal") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.3.1") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "119dp25pn88cv2rjny6hf6q288bmd37g16lvfnagfihh813f5kxl")))

