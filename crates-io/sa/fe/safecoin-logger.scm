(define-module (crates-io sa fe safecoin-logger) #:use-module (crates-io))

(define-public crate-safecoin-logger-1.6.16 (c (n "safecoin-logger") (v "1.6.16") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1cwynzjr3gg4qisrj30sqszny4km7r2pvfa3w7j09qz154dndy11")))

(define-public crate-safecoin-logger-1.6.17 (c (n "safecoin-logger") (v "1.6.17") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1ysx16lbrnffr4q149rvvkw5c1a9l9vf93pddf5sg0i4wvq2srcn")))

(define-public crate-safecoin-logger-1.6.18 (c (n "safecoin-logger") (v "1.6.18") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1rbsgkcb24fh7jffdmj8pw2cwrb8nwqzzdklxk701d3wr9glgsdc")))

(define-public crate-safecoin-logger-1.7.17 (c (n "safecoin-logger") (v "1.7.17") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "04zrp0p9m5irv0iwb3cd548h4d54sa410g9y99mxc4kcjcjva63z")))

(define-public crate-safecoin-logger-1.8.12 (c (n "safecoin-logger") (v "1.8.12") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1nnckf4hkcrmwab8vd2bm4pjs32j7hyqnf7vx1qsphnf9ilgg2h0")))

(define-public crate-safecoin-logger-1.9.29 (c (n "safecoin-logger") (v "1.9.29") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0p5j68sqaxwjcxcjzm33ayh4227rk590dlcpsk1knaxzp5jwq4xd")))

(define-public crate-safecoin-logger-1.10.32 (c (n "safecoin-logger") (v "1.10.32") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1kxvi6dabnaz8c68ih2dd2a1hir3y2q2k41cd30rja0bbjccymhx")))

(define-public crate-safecoin-logger-1.10.34 (c (n "safecoin-logger") (v "1.10.34") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "19rilai5br690x55f1bd92hfld738pl72ckvppbg9rg569c0yv8x")))

(define-public crate-safecoin-logger-1.14.3 (c (n "safecoin-logger") (v "1.14.3") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1s8f53dmnzl03aicld7kxf2isfr9xz9gz0l7fii09a3qrzvpfphn")))

(define-public crate-safecoin-logger-1.14.17 (c (n "safecoin-logger") (v "1.14.17") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0h7y8w3sx21br5zy5mrfqz8dald69kk8bsrzqj2s51wsvkmndn9g")))

