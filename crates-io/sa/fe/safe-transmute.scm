(define-module (crates-io sa fe safe-transmute) #:use-module (crates-io))

(define-public crate-safe-transmute-0.1.0 (c (n "safe-transmute") (v "0.1.0") (h "1yqpi0i4j2k5g0a9j7j2mvm7p46krw3qh98x7yr559c5sqifyxp2")))

(define-public crate-safe-transmute-0.2.0 (c (n "safe-transmute") (v "0.2.0") (h "01fckxw58v3amqmigbr83x8j5qhmqgqxbq0lg9859x84d0pa1cjr")))

(define-public crate-safe-transmute-0.3.0 (c (n "safe-transmute") (v "0.3.0") (h "04i2vq631a8ad20blkpvqncm6410x6yv5ivq9x0dsgm13p1a3scn")))

(define-public crate-safe-transmute-0.3.1 (c (n "safe-transmute") (v "0.3.1") (h "08swm1c3xpjfzia2vmyz904i1jjkw82akwp86p68aca1csh25n6d")))

(define-public crate-safe-transmute-0.4.0 (c (n "safe-transmute") (v "0.4.0") (h "0bmgpd922845qvsh4liw3yxyign7w0fhsvpzmcv4152vvdhzgik8")))

(define-public crate-safe-transmute-0.5.0 (c (n "safe-transmute") (v "0.5.0") (h "1si0g7cqaq9l7501gm5c5qz7430x3x8mx6350asxibz68z1dm6wz")))

(define-public crate-safe-transmute-0.6.0 (c (n "safe-transmute") (v "0.6.0") (h "0q8p9nkmw3i4i2pasd4zaia6rb7i6b4vd0jvsd95aiynfanqvpqx")))

(define-public crate-safe-transmute-0.7.0 (c (n "safe-transmute") (v "0.7.0") (h "1bhprnddkmaw0cz3iws0qbyfv77jdj038hlh8zwfq3q36d14i079")))

(define-public crate-safe-transmute-0.8.0 (c (n "safe-transmute") (v "0.8.0") (h "11z72g8w5kwwgy4ingpp20in4d2lrw848i643nmad0pzf2qvp0h5")))

(define-public crate-safe-transmute-0.9.0 (c (n "safe-transmute") (v "0.9.0") (h "1nd4k89d41isikrmr8y16m5ydnrj632glvnmgfxyiynbg7fly57v") (f (quote (("test-unaligned"))))))

(define-public crate-safe-transmute-0.10.0 (c (n "safe-transmute") (v "0.10.0") (h "0lsngv0y4hl1d8s4gilscrvpn6bahhn1n1y1cw6nvzspx8f5kdpi") (f (quote (("test-unaligned") ("std") ("default" "std")))) (y #t)))

(define-public crate-safe-transmute-0.10.1 (c (n "safe-transmute") (v "0.10.1") (h "0vhng02z2pzrg8mybna8q90s3b68cm5700wi2wgvr00rzqzqf14n") (f (quote (("test-unaligned") ("std") ("default" "std"))))))

(define-public crate-safe-transmute-0.11.0-rc.1 (c (n "safe-transmute") (v "0.11.0-rc.1") (h "17rxppw42l3vr474j217gmcvbxz9882d336faz5pksnvrdcjwnvh") (f (quote (("std") ("default" "std"))))))

(define-public crate-safe-transmute-0.11.0-rc.2 (c (n "safe-transmute") (v "0.11.0-rc.2") (h "0df44d5afavbrpp33qxz9f8z49zvl8xvjqdwk5zv61l4kb67qcr3") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-safe-transmute-0.11.0 (c (n "safe-transmute") (v "0.11.0") (h "12hp27gy08xhk7gjxq1ivgnc192bjnbx3bls8rllyx3z736v5f2h") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-safe-transmute-0.11.1 (c (n "safe-transmute") (v "0.11.1") (h "0iwdfj5fi89r87mhncd0kp6cjmq1rl23j0knmwj7xnab9clfg58x") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-safe-transmute-0.11.2 (c (n "safe-transmute") (v "0.11.2") (h "1s5ab5rgj2sny6n8wgl0n9yg2clzakfmn829pr9jd6fgdamiv84q") (f (quote (("std" "alloc") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-safe-transmute-0.11.3 (c (n "safe-transmute") (v "0.11.3") (h "0zdb839pfgxgfi7bzwqnkalld52byi7cnfmsk849707sz1pq4i1r") (f (quote (("std" "alloc") ("default" "std") ("const_generics") ("alloc"))))))

