(define-module (crates-io sa fe safe_uri_ext) #:use-module (crates-io))

(define-public crate-safe_uri_ext-0.1.0-beta.1 (c (n "safe_uri_ext") (v "0.1.0-beta.1") (d (list (d (n "safe_uri") (r "^0.1.0-beta.1") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "1hvmbzf7sca2h4ggpcbf4a5jc27hfzzqhg38p3ry2cfs610syz6x")))

(define-public crate-safe_uri_ext-0.1.0-beta.2 (c (n "safe_uri_ext") (v "0.1.0-beta.2") (d (list (d (n "safe_uri") (r "^0.1.0-beta.2") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "10fkk061jzqknmly64layv7j65abjb15c1bzlyjw07pcg91sjsdi")))

(define-public crate-safe_uri_ext-0.1.0-beta.3 (c (n "safe_uri_ext") (v "0.1.0-beta.3") (d (list (d (n "safe_uri") (r "^0.1.0-beta.3") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "023av4l278ccw4f20n056wb2m1l835bjp9bg8vchbm718hij6dsj")))

