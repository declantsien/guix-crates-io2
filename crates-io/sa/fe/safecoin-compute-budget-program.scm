(define-module (crates-io sa fe safecoin-compute-budget-program) #:use-module (crates-io))

(define-public crate-safecoin-compute-budget-program-1.8.12 (c (n "safecoin-compute-budget-program") (v "1.8.12") (d (list (d (n "safecoin-sdk") (r "=1.8.12") (d #t) (k 0)))) (h "1xxk72is2adkdafqgw9p2sywfj8faz4d3hfilaqy42ymm6klccvw")))

(define-public crate-safecoin-compute-budget-program-1.9.29 (c (n "safecoin-compute-budget-program") (v "1.9.29") (d (list (d (n "safecoin-program-runtime") (r "=1.9.29") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.9.29") (d #t) (k 0)))) (h "0nqkiisiixclrhdbpklkdy78rnkg3wwrlscllkzlilndmsjs7icm")))

(define-public crate-safecoin-compute-budget-program-1.10.32 (c (n "safecoin-compute-budget-program") (v "1.10.32") (d (list (d (n "safecoin-program-runtime") (r "=1.10.32") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.10.32") (d #t) (k 0)))) (h "0hpx4q7j9clanyhxix0gryafj7vnfil8n7qkanlw5nwrabd3x824")))

(define-public crate-safecoin-compute-budget-program-1.10.34 (c (n "safecoin-compute-budget-program") (v "1.10.34") (d (list (d (n "safecoin-program-runtime") (r "=1.10.34") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.10.34") (d #t) (k 0)))) (h "1cqpgzfcrwavhf596zxrsfviswh9brsxnpa1ff5zsh7cdahyqirm")))

(define-public crate-safecoin-compute-budget-program-1.14.3 (c (n "safecoin-compute-budget-program") (v "1.14.3") (d (list (d (n "safecoin-program-runtime") (r "=1.14.3") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.14.3") (d #t) (k 0)))) (h "15wp3kzfl4l5grpi3sjrxsiw568j0m3362p3qnwyw3v2fvhfl685")))

(define-public crate-safecoin-compute-budget-program-1.14.17 (c (n "safecoin-compute-budget-program") (v "1.14.17") (d (list (d (n "safecoin-program-runtime") (r "=1.14.17") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.14.17") (d #t) (k 0)))) (h "1c6wi66mn9db3bd6p5vq4446n3gcgqq5ai064mykybxlmj2j0yqx")))

