(define-module (crates-io sa fe safe-uninit-derive) #:use-module (crates-io))

(define-public crate-safe-uninit-derive-0.1.0 (c (n "safe-uninit-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0yk61hdfls71xgmi1f1bb60d2g01j76kv8i6n6l15yi9q252abs5")))

