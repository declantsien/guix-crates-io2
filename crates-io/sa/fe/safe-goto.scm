(define-module (crates-io sa fe safe-goto) #:use-module (crates-io))

(define-public crate-safe-goto-0.1.0 (c (n "safe-goto") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1dza1mf569llajs0mwrjv7j7hiq29pza1wf1k2k6ngnjqnjff6y9")))

(define-public crate-safe-goto-0.2.0 (c (n "safe-goto") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1pr4ng23k7sp52apdlhf6i7fapab9k4vm5226xp2nfwmhckj0h1c")))

