(define-module (crates-io sa fe safe-memo) #:use-module (crates-io))

(define-public crate-safe-memo-3.0.1 (c (n "safe-memo") (v "3.0.1") (d (list (d (n "safecoin-program") (r "^1.6.16") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.6.16") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.6.16") (d #t) (k 2)))) (h "12xbcmspjmymvbcgwrd50y06zx3qqzycxlb52ksasv54nql3c2ny") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.2 (c (n "safe-memo") (v "3.0.2") (d (list (d (n "safecoin-program") (r "^1.6.16") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.6.16") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.6.16") (d #t) (k 2)))) (h "0gwbkak4mlx7h1mmh3zxgjr5jnkqvajqzyaa5h258wn0b75drpnb") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.3 (c (n "safe-memo") (v "3.0.3") (d (list (d (n "safecoin-program") (r "^1.6.16") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.6.16") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.6.16") (d #t) (k 2)))) (h "0b5s3kd0vi0a652d1h8jf1rz432xxcqlb456443izhzl3n71a9g1") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.4 (c (n "safe-memo") (v "3.0.4") (d (list (d (n "safecoin-program-test") (r "^1.6.18") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.6.18") (d #t) (k 2)) (d (n "solana_program") (r "^1.6.18") (d #t) (k 0) (p "safecoin-program")))) (h "0wic9as4hygbm099bhl6x5cnj56ysmaqr75and7kf98r02ns28gh") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.5 (c (n "safe-memo") (v "3.0.5") (d (list (d (n "safecoin-program") (r "^1.7.17") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.7.17") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.7.17") (d #t) (k 2)))) (h "0srxw8hws1s4f0qarwq8ygysapki3fiak94acgrw6g4zf8n60ws0") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.6 (c (n "safe-memo") (v "3.0.6") (d (list (d (n "safecoin-program") (r "^1.8.12") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.8.12") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.8.12") (d #t) (k 2)))) (h "0piwvpjmlx299qcra398fj1qzjbl452nz0lr3s8727mz2g311fdh") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.7 (c (n "safe-memo") (v "3.0.7") (d (list (d (n "safecoin-program") (r "^1.9.29") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.9.29") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.9.29") (d #t) (k 2)))) (h "1265mc1lpimz83dls3v6mnyh7w8qgkg9ip013ngi26dbn106v8n2") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.8 (c (n "safe-memo") (v "3.0.8") (d (list (d (n "safecoin-program") (r "^1.10.34") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.10.34") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.10.34") (d #t) (k 2)))) (h "00ajf28aharnx0a4bv3qxb0002s6p0csqx7glbak93qp5w33scn5") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-safe-memo-3.0.9 (c (n "safe-memo") (v "3.0.9") (d (list (d (n "safecoin-program") (r "^1.14.17") (d #t) (k 0)) (d (n "safecoin-program-test") (r "^1.14.17") (d #t) (k 2)) (d (n "safecoin-sdk") (r "^1.14.17") (d #t) (k 2)))) (h "01bblvxksjhc4x3y2ry95aw7xsfxw5nag5xazfx529nzalvv25xx") (f (quote (("test-sbf") ("no-entrypoint"))))))

