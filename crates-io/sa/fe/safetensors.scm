(define-module (crates-io sa fe safetensors) #:use-module (crates-io))

(define-public crate-safetensors-0.2.6 (c (n "safetensors") (v "0.2.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dzg8k2mrc5y1li7rd1gj0qlckczvimz3d5748x37p9qbl5mraa6")))

(define-public crate-safetensors-0.2.7 (c (n "safetensors") (v "0.2.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rmjdzh0db1fd9yg9imkpda9nf0vj3630lizwqkzgrd2hm9plsy3")))

(define-public crate-safetensors-0.2.8 (c (n "safetensors") (v "0.2.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08i5lr9mj5diwrih4063gbdb5yya8ldj3w10ia5mxgx5dy0b7ibz")))

(define-public crate-safetensors-0.3.0 (c (n "safetensors") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12s6whvc90qr403ra3c1pdlcg4cdmlxgbgy1f50q0q3d6s9fbacv")))

(define-public crate-safetensors-0.3.1 (c (n "safetensors") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fncj7m5yyxw7bvlfslqs65frizmmh8m10xy8hc4lmizrfi1in51")))

(define-public crate-safetensors-0.3.2 (c (n "safetensors") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pwx39r74xsgbc85ysqwjnfmj08114pd52jxaqlb1848qf8bv35d")))

(define-public crate-safetensors-0.3.3 (c (n "safetensors") (v "0.3.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pxi5w8q0lm2yr3rrc8plibzlg63rc3dsm481a16zrrxdfw7jcnr")))

(define-public crate-safetensors-0.4.0 (c (n "safetensors") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b9x91127v8iqqns8pgmcws1zdccbqzqahyjk44zkqqssmmiirgi")))

(define-public crate-safetensors-0.4.1 (c (n "safetensors") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13ls4rqwy48hj3hd6q5lp6f8rdnh0ralnrjk5n6bazli4wffyn8n")))

(define-public crate-safetensors-0.4.2 (c (n "safetensors") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vb9wsmsm7wfyvkl795wpc2iiwa3mx0vqhhym2q6yhrlzdmhx64d")))

(define-public crate-safetensors-0.4.3 (c (n "safetensors") (v "0.4.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fbx56wikqcvqb4y0ym0cys68lj0v3cpanhsy5i13fkz5jr7dvcc")))

