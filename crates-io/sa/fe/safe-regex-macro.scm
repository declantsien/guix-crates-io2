(define-module (crates-io sa fe safe-regex-macro) #:use-module (crates-io))

(define-public crate-safe-regex-macro-0.1.0 (c (n "safe-regex-macro") (v "0.1.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex-compiler") (r "^0.1.0") (d #t) (k 0)))) (h "0dfghvlz937fj0zfcxvvb6908cxhpyfxh0yh5slbhdcpyrvq55xy") (y #t)))

(define-public crate-safe-regex-macro-0.1.1 (c (n "safe-regex-macro") (v "0.1.1") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0.1") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.1.1") (d #t) (k 0)))) (h "0dyf933lsmq5r3bv6gx66yqzx3sjk5j32608nd72wx6cwncl83js") (y #t)))

(define-public crate-safe-regex-macro-0.2.0 (c (n "safe-regex-macro") (v "0.2.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.0") (d #t) (k 0)))) (h "06d7ajd9bb7xn9h1npz592zfxmyrd9357n6f16dvjy0gxgi4rqk2")))

(define-public crate-safe-regex-macro-0.2.1 (c (n "safe-regex-macro") (v "0.2.1") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.0") (d #t) (k 0)))) (h "1vwkjzn4lwpmhn6347093zvyyyk60pwdds1krvlqj1xrc511kdfa")))

(define-public crate-safe-regex-macro-0.2.2 (c (n "safe-regex-macro") (v "0.2.2") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.0") (d #t) (k 0)))) (h "0l95pw2kljdslyi11xn48isff1ihfs8h1qc3h3c0zc2d6v1a8zi8")))

(define-public crate-safe-regex-macro-0.2.3 (c (n "safe-regex-macro") (v "0.2.3") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.0") (d #t) (k 0)))) (h "0pa8j2lzni1b465ln7qjddr4rs275gbxbfdrza4jv1asa026s9zr")))

(define-public crate-safe-regex-macro-0.2.5 (c (n "safe-regex-macro") (v "0.2.5") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.0") (d #t) (k 0)))) (h "09yiy68sx2jij3vy73afmps9axqk2psrm9vbn78miw83bimykhln")))

(define-public crate-safe-regex-macro-0.2.6 (c (n "safe-regex-macro") (v "0.2.6") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.2.6") (d #t) (k 0)))) (h "1vvl69k2m8b8ghkmy1ncgv3qg202d8fm8rnd051g47aifx5sn289")))

(define-public crate-safe-regex-macro-0.3.0 (c (n "safe-regex-macro") (v "0.3.0") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-regex") (r "^0.2.6") (d #t) (k 2)) (d (n "safe-regex-compiler") (r "^0.3.0") (d #t) (k 0)))) (h "01najbcwy12agvb74cayz54qmdhjxw3w4fiwzp2mk7zidrzdws17")))

