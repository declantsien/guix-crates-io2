(define-module (crates-io sa fe safe-string) #:use-module (crates-io))

(define-public crate-safe-string-0.1.0 (c (n "safe-string") (v "0.1.0") (h "0yslzz8wi30p5hd3zin6a49kim1sf4rc9vi0g258wbvy7d18wj0a")))

(define-public crate-safe-string-0.1.1 (c (n "safe-string") (v "0.1.1") (h "08hzssi72skqr326l67dpn6v50s9i4h77qqi02qai92i8ismrfgr")))

(define-public crate-safe-string-0.1.2 (c (n "safe-string") (v "0.1.2") (h "1j4n3f7px2kpr3kka5b09q7ixlz00f6wa4nl2wh171928vz0sa50")))

(define-public crate-safe-string-0.1.3 (c (n "safe-string") (v "0.1.3") (h "0hp8shdlc8fs0yi0ybd1n6lr2vfrg4szlpp9qf48h5bq9cmf6mzw")))

(define-public crate-safe-string-0.1.4 (c (n "safe-string") (v "0.1.4") (h "134628izjviakixp74m5aiq2hxx79vwfsvqxxfrvxqzv8zianmnq")))

(define-public crate-safe-string-0.1.5 (c (n "safe-string") (v "0.1.5") (h "0vnciwf06rp2d42aqgazpgll42i2l6dv174da2s6vrj6y1gdb6f6")))

(define-public crate-safe-string-0.1.6 (c (n "safe-string") (v "0.1.6") (h "1wnvxg2cn7vg793xi34cc8y5af40cvbwa7mqr4v2vdsj25adbikp")))

(define-public crate-safe-string-0.1.7 (c (n "safe-string") (v "0.1.7") (h "0cnp8vziv9p42ssdjj43zv8fkqjnw4pak80yb359js7z00fil427")))

(define-public crate-safe-string-0.1.8 (c (n "safe-string") (v "0.1.8") (h "1psaq2nrxcyry9rps92hzm7wfayxnkmscp14shvy9ycb5b5xwz21")))

(define-public crate-safe-string-0.1.9 (c (n "safe-string") (v "0.1.9") (h "06l4m8cjydm7yj5kygzfi96rp82f7xy7jz16yskh3x0sibrkyg0c")))

(define-public crate-safe-string-0.1.10 (c (n "safe-string") (v "0.1.10") (h "0mvj85acgmphzm01agy439nryzkp24i2c4lacb685ls3qncmr8nn")))

