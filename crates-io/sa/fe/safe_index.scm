(define-module (crates-io sa fe safe_index) #:use-module (crates-io))

(define-public crate-safe_index-0.9.0 (c (n "safe_index") (v "0.9.0") (h "1fvg4hpv6l87vgb29ipiwgc1860wwl5f0h2543pz6j3ldn5yibiz")))

(define-public crate-safe_index-0.9.1 (c (n "safe_index") (v "0.9.1") (h "0aaz7cx65i52q24y38fjn01zs6492zlnkv71xg8sq55blwydn55b")))

(define-public crate-safe_index-0.9.2 (c (n "safe_index") (v "0.9.2") (h "1z9j4ij8pplv97c9ag5mq3q59sa3gqh6hpkrsgbfhyxgb4s9klpj")))

(define-public crate-safe_index-0.9.3 (c (n "safe_index") (v "0.9.3") (h "03a3a377z1mqkpb69cmgdjvg7bs3895jsml8rjab09dz72lcd5cd")))

(define-public crate-safe_index-0.9.6 (c (n "safe_index") (v "0.9.6") (h "1cjda9y48wkk5mjkzdznnidixszdb5grc477d9ckqwpjxda0rvr9")))

(define-public crate-safe_index-0.9.9 (c (n "safe_index") (v "0.9.9") (h "10i7zg4lyhw3pygb30ba5nq4c98q549i04ljhvjas7qj2fbx3lvn")))

(define-public crate-safe_index-0.9.11 (c (n "safe_index") (v "0.9.11") (h "1wwv41jj28dxy1jwlxm18avbqw2zlhk6dc27j3vrficxmi9flv6f")))

(define-public crate-safe_index-0.9.17 (c (n "safe_index") (v "0.9.17") (h "0s31bqadh21cda91b9mw3s3z0kcvzpd55bfxadnyv5pis6xyf3bw") (f (quote (("strict"))))))

(define-public crate-safe_index-0.9.19 (c (n "safe_index") (v "0.9.19") (h "1ynzl8vnyp0zq9xd4pz2i13ya7c44k831yxl5rda290f5nlz7m1v") (f (quote (("strict"))))))

(define-public crate-safe_index-0.9.20 (c (n "safe_index") (v "0.9.20") (h "1ksb52nnyd9jlsh4sbn7f3cklgfmdqg7rdhc1rwch8x761w7v64b") (f (quote (("strict"))))))

(define-public crate-safe_index-0.10.0 (c (n "safe_index") (v "0.10.0") (h "1a7fkcwyk9p5hd39d1f6ysc0s1dw96afgfshmiaprhbgwb9z2bg7") (f (quote (("strict"))))))

