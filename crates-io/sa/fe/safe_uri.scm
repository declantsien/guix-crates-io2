(define-module (crates-io sa fe safe_uri) #:use-module (crates-io))

(define-public crate-safe_uri-0.1.0-beta.1 (c (n "safe_uri") (v "0.1.0-beta.1") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "boar") (r "^0.2.4") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "0hj029ljn7pp1zk17fnsdf6444w8408absz1pfbilzvhpnanpdfl")))

(define-public crate-safe_uri-0.1.0-beta.2 (c (n "safe_uri") (v "0.1.0-beta.2") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "boar") (r "^0.2.4") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "1ry4f577ppqykpkx3fjgki9lym2bvc8zvax3ch660qkd2i5kxc3f")))

(define-public crate-safe_uri-0.1.0-beta.3 (c (n "safe_uri") (v "0.1.0-beta.3") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "boar_") (r "^0.2.4") (d #t) (k 0) (p "boar")) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "09lmdzvhjnbvnz4x7mbi1xwk2qlk4zg852lvss00yl4i1sy8iwnf") (f (quote (("boar"))))))

(define-public crate-safe_uri-0.1.0-beta.4 (c (n "safe_uri") (v "0.1.0-beta.4") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "shared_bytes") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "057439hcp5mb8ijvwyph59d2zz1hwl135i794rv1f7lvx6b1yrzr")))

