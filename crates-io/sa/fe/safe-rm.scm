(define-module (crates-io sa fe safe-rm) #:use-module (crates-io))

(define-public crate-safe-rm-0.1.0 (c (n "safe-rm") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01nsdz5al1bhvjrn8zz8zs23s12kqh4g0g6nmb1qjibq87misjn4")))

