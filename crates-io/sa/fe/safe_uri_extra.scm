(define-module (crates-io sa fe safe_uri_extra) #:use-module (crates-io))

(define-public crate-safe_uri_extra-0.1.0-beta.4 (c (n "safe_uri_extra") (v "0.1.0-beta.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "safe_uri") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "09zhj00vsq0ckln6dqz4b8vahdslfd7l4j2y57nncmvkcwg8ffgz")))

