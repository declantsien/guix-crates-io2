(define-module (crates-io sa fe safe-bytes-derive) #:use-module (crates-io))

(define-public crate-safe-bytes-derive-0.1.0 (c (n "safe-bytes-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rv3lzcgx1fcy2156hfiaw6f5da0a2lplyz7idirv95ms1vs3psx")))

(define-public crate-safe-bytes-derive-0.1.1 (c (n "safe-bytes-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jzghd66hgmh6bxlj0vqhfzpmijydmi0ch4rdg5gflv6v99fvfkk")))

