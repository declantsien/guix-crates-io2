(define-module (crates-io sa fe safecoin-measure) #:use-module (crates-io))

(define-public crate-safecoin-measure-1.6.16 (c (n "safecoin-measure") (v "1.6.16") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "safecoin-metrics") (r "=1.6.16") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.6.16") (d #t) (k 0)))) (h "1limryfjfv3631qcmxqkl8nkcxkw5fv5ayxdvgy8qcfav57kd2iv") (f (quote (("no-jemalloc"))))))

(define-public crate-safecoin-measure-1.6.18 (c (n "safecoin-measure") (v "1.6.18") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "safecoin-metrics") (r "=1.6.18") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.6.18") (d #t) (k 0)))) (h "1d3l8fdqcmicazm1n32g5rfkh6fxf9p18x6l4namy6xv37bmavsk") (f (quote (("no-jemalloc"))))))

(define-public crate-safecoin-measure-1.6.19 (c (n "safecoin-measure") (v "1.6.19") (d (list (d (n "jemalloc-ctl") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "safecoin-metrics") (r ">=1.6.19") (d #t) (k 0)) (d (n "safecoin-sdk") (r ">=1.6.20") (d #t) (k 0)))) (h "1m4p65x25ml0ngndwzbgvixywkhlciwfkgx1pynkhxnj3wqmjplp") (f (quote (("no-jemalloc"))))))

(define-public crate-safecoin-measure-1.7.17 (c (n "safecoin-measure") (v "1.7.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "safecoin-metrics") (r "=1.7.17") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.7.17") (d #t) (k 0)))) (h "0dn2jmllb7rqgzg1nzbxdixfbliz6nfvhi19krgk4cshnqs2kn58")))

(define-public crate-safecoin-measure-1.8.12 (c (n "safecoin-measure") (v "1.8.12") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "safecoin-metrics") (r "=1.8.12") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.8.12") (d #t) (k 0)))) (h "1vipzgsyzazzlym0l1dgdrnwjvaandqpxazd4wnl22n1ib5vrmzv")))

(define-public crate-safecoin-measure-1.9.29 (c (n "safecoin-measure") (v "1.9.29") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.9.29") (d #t) (k 0)))) (h "0vylvki536pmk929ms7gcmkim8c46va3sd3flzz9nw29z7l7vaj5")))

(define-public crate-safecoin-measure-1.10.32 (c (n "safecoin-measure") (v "1.10.32") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.10.32") (d #t) (k 0)))) (h "14ac63mfdqnf2dbllaa5wjxs2sfhps3l4792zf43bqmzsry9ai87")))

(define-public crate-safecoin-measure-1.10.34 (c (n "safecoin-measure") (v "1.10.34") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.10.34") (d #t) (k 0)))) (h "1176gdfg0vq5xy2ycicyc4bxvpfrdv6x86km2lcxyzm3hc0v5dhh")))

(define-public crate-safecoin-measure-1.14.3 (c (n "safecoin-measure") (v "1.14.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.14.3") (d #t) (k 0)))) (h "143q61rinznyy8k8gy27d0gsdz3jg50f5s1i1fs9snqdalqn3r37")))

(define-public crate-safecoin-measure-1.14.17 (c (n "safecoin-measure") (v "1.14.17") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "safecoin-sdk") (r "=1.14.17") (d #t) (k 0)))) (h "0yzy0zaix0s6r119k499fms3vf0sbdnh3n6n2yz8wfxdhmc899yw")))

