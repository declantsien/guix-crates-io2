(define-module (crates-io sa fe safe-ipc) #:use-module (crates-io))

(define-public crate-safe-ipc-0.1.0 (c (n "safe-ipc") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "oorandom") (r "^11.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "handleapi" "memoryapi" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j5b6i9yrapzl4b8hgiiqv5mdsi46v2ykjb3m7w5q81mlvyls11i") (y #t)))

