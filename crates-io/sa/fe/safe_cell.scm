(define-module (crates-io sa fe safe_cell) #:use-module (crates-io))

(define-public crate-safe_cell-0.1.0 (c (n "safe_cell") (v "0.1.0") (h "0kdjhhbjlid9k0w2khi6jf88rspmvkd907r1fq6xmp3xspzwh0mp")))

(define-public crate-safe_cell-0.1.1 (c (n "safe_cell") (v "0.1.1") (h "1ryr1l99gwbhw722wyypklg0d7ifa8r2mfqsghsryw8nm8m4zzh1")))

