(define-module (crates-io sa fe safe_shl) #:use-module (crates-io))

(define-public crate-safe_shl-1.0.0 (c (n "safe_shl") (v "1.0.0") (h "15jdv0nvivk5wpzayzxhasa4rayr6868qrrcc2kry93mi2lyc2mc")))

(define-public crate-safe_shl-1.1.0 (c (n "safe_shl") (v "1.1.0") (h "0iadril6yia18zbdygi8bp053raqrvr9k2xba0dhm04l4a24k2h7")))

(define-public crate-safe_shl-1.1.1 (c (n "safe_shl") (v "1.1.1") (h "0lw6r8xgi1yba80vg0m4h3d8vj5zf6a3il2hibnqqa2my608q3zy")))

