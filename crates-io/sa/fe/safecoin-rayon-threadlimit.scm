(define-module (crates-io sa fe safecoin-rayon-threadlimit) #:use-module (crates-io))

(define-public crate-safecoin-rayon-threadlimit-1.6.16 (c (n "safecoin-rayon-threadlimit") (v "1.6.16") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "120ap043n1b87jrrq6wd097r4qka7xg7i15xkwmys92imxcwwbk3")))

(define-public crate-safecoin-rayon-threadlimit-1.6.18 (c (n "safecoin-rayon-threadlimit") (v "1.6.18") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0dxwk5a6bp7qwynca4rwnigwpg6algjdylnwn8ny8jx27mmclna9")))

(define-public crate-safecoin-rayon-threadlimit-1.7.17 (c (n "safecoin-rayon-threadlimit") (v "1.7.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "19vw87x5pf77d03g79lb0aqj5mi9wx1qb9d602q9fl390q1lmgq1")))

(define-public crate-safecoin-rayon-threadlimit-1.8.12 (c (n "safecoin-rayon-threadlimit") (v "1.8.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1q6m7v48jg51215kq215dyx165xyaxbf7ic4sw903kqm1jwp261n")))

(define-public crate-safecoin-rayon-threadlimit-1.9.29 (c (n "safecoin-rayon-threadlimit") (v "1.9.29") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "05w8ccwcrcqv53c0czax31d61pgz9f937whifby9nn0myl3ljlv7")))

(define-public crate-safecoin-rayon-threadlimit-1.10.32 (c (n "safecoin-rayon-threadlimit") (v "1.10.32") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1nylzni744cdiim5svx5d9yiw53yamc9qwwvpanwh8d1y75ir6jb")))

(define-public crate-safecoin-rayon-threadlimit-1.10.34 (c (n "safecoin-rayon-threadlimit") (v "1.10.34") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0law250wk59d78pk5nkm04adbiaacdhq2q7d3d63mjl8235drql0")))

(define-public crate-safecoin-rayon-threadlimit-1.14.3 (c (n "safecoin-rayon-threadlimit") (v "1.14.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1iq3yh70hjzwq45q3d5lw4whl7g2s5bw3axx4xfmdsravpvi0d6r")))

(define-public crate-safecoin-rayon-threadlimit-1.14.17 (c (n "safecoin-rayon-threadlimit") (v "1.14.17") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0nwp3w62n3cnjzh289j1fw2sd0lqxj017rznl6yw09kxc6pzlzp9")))

