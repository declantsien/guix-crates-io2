(define-module (crates-io sa fe safe_math) #:use-module (crates-io))

(define-public crate-safe_math-0.1.0 (c (n "safe_math") (v "0.1.0") (h "088diknf3594djvrc02cidh5cih2p5sr6srk8gfrs2v66434wg74")))

(define-public crate-safe_math-0.1.1 (c (n "safe_math") (v "0.1.1") (h "0y4q622qwy3agvc6vqkppagxhrqsfihsq871q1kr46vszisc10pq")))

