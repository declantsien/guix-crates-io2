(define-module (crates-io sa fe safe_en) #:use-module (crates-io))

(define-public crate-safe_en-1.3.2 (c (n "safe_en") (v "1.3.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "153n1jwjh32rh3hhik5jr1rj0nssv5qa0h2k29xwpjg79j2dlnba")))

(define-public crate-safe_en-1.4.2 (c (n "safe_en") (v "1.4.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w4ll469zz6rq00h47xj9rlyrgb1ysa3fk920a3sa5j23w2bfwxg")))

(define-public crate-safe_en-1.5.3 (c (n "safe_en") (v "1.5.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "155g4amixjx2dzka5wfyqvas0lj7mfln09frhkc5lgv1ghfzbs7y")))

(define-public crate-safe_en-1.5.4 (c (n "safe_en") (v "1.5.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "169r7yy16r2wvpdb3qhcxzk398pw3wxy46m5jssb6vvj17gr3d4k")))

(define-public crate-safe_en-1.5.5 (c (n "safe_en") (v "1.5.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ywfiv4vn3qakyhnn03my7ncnbb89isylqc36l605ap68lrgmphw")))

(define-public crate-safe_en-1.6.5 (c (n "safe_en") (v "1.6.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06066bmmz87nrkg6dbkch4vwfs4avc8a1sxpcxyy3gxazhy3jrdr")))

(define-public crate-safe_en-1.6.6 (c (n "safe_en") (v "1.6.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15y1jx032ffkwh057bkgz3phpjra4wpyc06ajb4099akqb76m18c")))

(define-public crate-safe_en-1.6.7 (c (n "safe_en") (v "1.6.7") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z5nhxc2bfxh0xahzdd6fz8qvg7rm6jj4nqmwjra9yzbb17h78a0") (y #t)))

(define-public crate-safe_en-1.6.8 (c (n "safe_en") (v "1.6.8") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mlp3n4j4jppb41rbf1nhhspkxnkrsg7xsmx91s3fnrs7wplpndw") (y #t)))

(define-public crate-safe_en-1.6.9 (c (n "safe_en") (v "1.6.9") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lry5m5r4w8x1qslwjs624ipbav9p1mb9mlw6xvjc66q5mgy3rlx") (y #t)))

(define-public crate-safe_en-1.6.10 (c (n "safe_en") (v "1.6.10") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1p8c4qqd8b3zwq22sla44302cscg6bgsbnsjq0naz1428wgv6bd7") (y #t)))

(define-public crate-safe_en-1.7.11 (c (n "safe_en") (v "1.7.11") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19fk72y5wmzg9xdr9s8n9ihvir92bjmczv8zx1s25w8q8c32nk07") (y #t)))

(define-public crate-safe_en-1.7.12 (c (n "safe_en") (v "1.7.12") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0sw1kqwrxil8gjk8nffamc267vdvflhdfnma4jkyggi3q99cidai")))

(define-public crate-safe_en-1.8.0 (c (n "safe_en") (v "1.8.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p6m9hj3xdy3n7hslcw9nd2x04mzk1npsqy3n4h4q8wfn6iq86zl")))

