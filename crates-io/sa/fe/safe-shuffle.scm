(define-module (crates-io sa fe safe-shuffle) #:use-module (crates-io))

(define-public crate-safe-shuffle-0.1.1 (c (n "safe-shuffle") (v "0.1.1") (d (list (d (n "num-format") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "16lna3hbcsd90c21g2yk9b5nwqsg71qcxnzip84mbfn7vgnpav8a")))

