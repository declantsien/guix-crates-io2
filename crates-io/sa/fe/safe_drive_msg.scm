(define-module (crates-io sa fe safe_drive_msg) #:use-module (crates-io))

(define-public crate-safe_drive_msg-0.1.3 (c (n "safe_drive_msg") (v "0.1.3") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1fikf406i64rx3vssxb3rdcjjhyhpfv69iyl7sp1and3lm315y1i")))

(define-public crate-safe_drive_msg-0.1.4 (c (n "safe_drive_msg") (v "0.1.4") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1scbn4ldq2zcv7ghq5963743qs385k02xvamnkjkp8fhpzair08m")))

(define-public crate-safe_drive_msg-0.2.1 (c (n "safe_drive_msg") (v "0.2.1") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1viqfw4nx9mwgy98ypi5a6dhfl930g9gf5ragqsmsa92lpgp24vk")))

(define-public crate-safe_drive_msg-0.2.2 (c (n "safe_drive_msg") (v "0.2.2") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "0dzbaprxsz5pg00syj521jzx6j19g6csdask937bjgbrzq8blk0y")))

(define-public crate-safe_drive_msg-0.2.3 (c (n "safe_drive_msg") (v "0.2.3") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1hzg08q0d3msad2fn8jyi3fkdkwr5l1sj2pqgcb5jvwqwmv8ik3b")))

(define-public crate-safe_drive_msg-0.2.4 (c (n "safe_drive_msg") (v "0.2.4") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1x0yc1q0c03fb6xdjybz6ys6rk30iksdamf3ag17z4ls9szynk2h")))

(define-public crate-safe_drive_msg-0.2.5 (c (n "safe_drive_msg") (v "0.2.5") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "00590w7x4m1xw55j6shvjv4f3ii016wqyhcspdginpxpmnxnyl4b")))

(define-public crate-safe_drive_msg-0.2.6 (c (n "safe_drive_msg") (v "0.2.6") (d (list (d (n "checksumdir") (r "^0.3") (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "t4_idl_parser") (r "^0.1") (d #t) (k 0)))) (h "1wfpr99vw6avxfyqc3fskscp7plsy0mkqv6idglsrzqg904l74m1")))

