(define-module (crates-io sa fe safe_unwrap) #:use-module (crates-io))

(define-public crate-safe_unwrap-0.1.0 (c (n "safe_unwrap") (v "0.1.0") (h "0h12bsjhipd8h8cr7snl0byh7gybnz45755z674l0jk38v4ayk37")))

(define-public crate-safe_unwrap-0.1.1 (c (n "safe_unwrap") (v "0.1.1") (h "03allb1ka9xilcljxh799pvsxffjfhzlcdbaqv11vvsgd5nd2l0f")))

(define-public crate-safe_unwrap-0.2.0 (c (n "safe_unwrap") (v "0.2.0") (h "1apgq74kivznj6fd26hr9bmnj1xm0qwdssvbp2an7ksm7kk2hkhf")))

(define-public crate-safe_unwrap-0.3.0 (c (n "safe_unwrap") (v "0.3.0") (h "0mchvyslxjwx5zkji0ph6gybvy60hkizwnc759wgxhim6nnh5pfw") (f (quote (("std") ("default"))))))

(define-public crate-safe_unwrap-0.4.0 (c (n "safe_unwrap") (v "0.4.0") (h "10xqq28g2xya71j6x0i75j8l46vb6mg29vybpvaypjgrbka8h2cv") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-safe_unwrap-0.3.1 (c (n "safe_unwrap") (v "0.3.1") (h "13gr8f7kaqx4yc81p35vy4jcrx6yz6bi5ym44bw8z06wp4yalsg1") (f (quote (("std") ("default"))))))

(define-public crate-safe_unwrap-0.4.1 (c (n "safe_unwrap") (v "0.4.1") (h "1jycck7jgyj287k5i2b4f4bxl3yp2rxrx87b9ddwl0bb8zx5ninx") (f (quote (("std") ("default"))))))

