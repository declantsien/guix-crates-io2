(define-module (crates-io sa fe safe_arch) #:use-module (crates-io))

(define-public crate-safe_arch-0.1.0 (c (n "safe_arch") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "13r0lgpywp6vz1d0jy9q8zg7szkjh4s34grhnk5d59rpp4yrh656")))

(define-public crate-safe_arch-0.2.0 (c (n "safe_arch") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)))) (h "1mqf17gj5hfm76qrjjxm9gnx0maifzj8925w7r8q82vm8qhbrx52")))

(define-public crate-safe_arch-0.2.1 (c (n "safe_arch") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "194v296fx0q5vxikanwvldh0llvcdypjgj83ricclfzp9ffxjyrf")))

(define-public crate-safe_arch-0.2.2 (c (n "safe_arch") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1wk2c89agqjyjxr7znyggpcirvb5bj9rjw02zhwcrxfwwjc42y25")))

(define-public crate-safe_arch-0.2.3 (c (n "safe_arch") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "03xi88zkhm9y3lvamyfs152f51gj2glaf3wxrhxzqy103ggrg20h")))

(define-public crate-safe_arch-0.2.4 (c (n "safe_arch") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0qihwp1qak40kiykggjimpvi73mkv1vhwpazgdys39ky8070bl70")))

(define-public crate-safe_arch-0.3.0 (c (n "safe_arch") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1cp8njc0s08blq2jhbxq4z2s7649kn4a3iknq88qcdj9h7d62w56") (f (quote (("default"))))))

(define-public crate-safe_arch-0.3.1 (c (n "safe_arch") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1s0zy03fccigj4sp967sjgssz129jzs6wgi3xaac624jj56ngxj7") (f (quote (("default"))))))

(define-public crate-safe_arch-0.4.0 (c (n "safe_arch") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1c3dj4qald318wkb39fq9izdfc9lv3vpydhs0vs0734v0gxz5i12") (f (quote (("default"))))))

(define-public crate-safe_arch-0.5.0 (c (n "safe_arch") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0bgg34pz5b3gllcml8acvdm661dkla7gwy3rmmlbammzgkp0776b") (f (quote (("default"))))))

(define-public crate-safe_arch-0.5.1 (c (n "safe_arch") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1j1ffwrx09lin48fa4vp8rkk8jhqd0vnavnp71s68l0pgfh8q0av") (f (quote (("default"))))))

(define-public crate-safe_arch-0.5.2 (c (n "safe_arch") (v "0.5.2") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "01ffy9aw9v1n4bcwl525zw3gnh18jk7aq38iqcn51bwnjrnkvzy1") (f (quote (("default"))))))

(define-public crate-safe_arch-0.6.0 (c (n "safe_arch") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0ad5ykwgq9ll1ymp83d9cayzj8q191rik71ga5wzkndhrkj22j3r") (f (quote (("default"))))))

(define-public crate-safe_arch-0.7.0 (c (n "safe_arch") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "03v5xalqmd458k3i2fws26p2r3qh633wmk5srkvzhh5x0x1li9v2") (f (quote (("default"))))))

(define-public crate-safe_arch-0.7.1 (c (n "safe_arch") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0m63dasp3rs9mkaa5wai6l6v14lbb788igaidys7k8g6w5f0g67k") (f (quote (("default"))))))

