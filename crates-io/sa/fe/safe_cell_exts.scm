(define-module (crates-io sa fe safe_cell_exts) #:use-module (crates-io))

(define-public crate-safe_cell_exts-0.1.0 (c (n "safe_cell_exts") (v "0.1.0") (h "02ii3bwbbk8n07mwliv6rzmbrrzpyrnjni6lj854zg4v16qq47rr") (y #t)))

(define-public crate-safe_cell_exts-0.2.0 (c (n "safe_cell_exts") (v "0.2.0") (h "1dvcb38skdzvji24y57c25az3ph9yc6vfhka45w2d460np1zdcr0") (y #t)))

(define-public crate-safe_cell_exts-0.3.0 (c (n "safe_cell_exts") (v "0.3.0") (h "1gcv7i82hvyprjmsqc89sgn6dqv06p5c2vpaiayyvbhp1jp6aavb") (y #t)))

