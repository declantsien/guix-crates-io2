(define-module (crates-io sa fe safe_http_parser) #:use-module (crates-io))

(define-public crate-safe_http_parser-0.1.0-beta.4 (c (n "safe_http_parser") (v "0.1.0-beta.4") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "safe_http") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "safe_uri") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "shared_bytes") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 2)))) (h "194k6hxacg6nwx57d5cd901amh08dnf1vfrzb8s2p4c0nxzgpjrc")))

