(define-module (crates-io sa fe safepass) #:use-module (crates-io))

(define-public crate-safepass-0.1.0 (c (n "safepass") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1mpknfvgqpsmm6b2gqhj0gyjnb511sclza3405jlbnzwnjg8whsq")))

(define-public crate-safepass-0.1.1 (c (n "safepass") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1gzvs547amb7yiysxxr4bi6fqrp21mbfz8byzz21j406ndigfn3p")))

(define-public crate-safepass-0.1.2 (c (n "safepass") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1xfgkx9621hd4jxmrvmxk51k1bsmp4a8j4ihp5ahfp7zkg8vxdmb")))

(define-public crate-safepass-0.1.3 (c (n "safepass") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0ndpfk2vjz6mwg4zhvaxgdvhzf4nxjgbb2ywn2xydkj68d65d5pq")))

(define-public crate-safepass-0.1.4 (c (n "safepass") (v "0.1.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "08vr3nvcc9qf09664ziqq95nfz6gr652sb2m2bnmmq751ccd1y0d")))

(define-public crate-safepass-1.0.0 (c (n "safepass") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1mx05bs6cmlimmsc3djafn9kvzf1xbndblyx8086whgi8lcf3ndv")))

(define-public crate-safepass-1.0.1 (c (n "safepass") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "copypasta") (r "^0.8.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "fernet") (r "^0.1") (f (quote ("fernet_danger_timestamps"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "053wlz8mcwhr68d8k73kd76ncn2lnwf0lpqcbmj170ip26x49r42")))

