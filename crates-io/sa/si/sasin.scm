(define-module (crates-io sa si sasin) #:use-module (crates-io))

(define-public crate-sasin-0.1.0 (c (n "sasin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.199") (d #t) (k 0)))) (h "0i5mz1l26mjzdr602j350y3p9qnr3ispq0dzqr4z0pvn5hpx5h8k")))

