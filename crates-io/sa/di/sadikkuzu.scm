(define-module (crates-io sa di sadikkuzu) #:use-module (crates-io))

(define-public crate-sadikkuzu-0.1.0 (c (n "sadikkuzu") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j3zlpxdwl8a3p843vk4rxws9km93ri2x6cy87fzd0344qw755np")))

(define-public crate-sadikkuzu-0.1.1 (c (n "sadikkuzu") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kbk25nvcjnjxj27xnsnzwgfx418zpsz4klr7k829w9llrjn9g2")))

(define-public crate-sadikkuzu-0.1.2 (c (n "sadikkuzu") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wbxswhy0jn4qd63h4avqzkvfzczf5bfp41j3ljlba8n97qfvs5l")))

(define-public crate-sadikkuzu-0.1.3 (c (n "sadikkuzu") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yg75ig8g0sbffbnw06m2rh4lnryr4z1wz6x9bkl6j2xs50v2w3z")))

(define-public crate-sadikkuzu-0.2.0 (c (n "sadikkuzu") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19rx159n74byx6w1vn22n20ipw2imy1fj41mmhgi3ylgzj99gx09")))

