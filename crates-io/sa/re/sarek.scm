(define-module (crates-io sa re sarek) #:use-module (crates-io))

(define-public crate-sarek-0.1.0 (c (n "sarek") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "decorum") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "panic-control") (r "^0.1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "pyo3") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (k 0)))) (h "1c0g7dixlckm2f0b7w0nf2pw1x34ln5f4gy11l57m3cam7m8g19v")))

