(define-module (crates-io sa ph saphir-cookie) #:use-module (crates-io))

(define-public crate-saphir-cookie-0.13.0 (c (n "saphir-cookie") (v "0.13.0") (d (list (d (n "base64") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xcik9wyiqvcmiqifw8prll6kc5fxa2l1grvz8ddlhpbyj00rf9d") (f (quote (("secure" "ring" "base64") ("percent-encode" "percent-encoding"))))))

(define-public crate-saphir-cookie-0.13.1 (c (n "saphir-cookie") (v "0.13.1") (d (list (d (n "base64") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00dlahv8vxfp4dmr6492kvq98azgb5q0p0gvv3784h6c6nsrmqmf") (f (quote (("secure" "ring" "base64") ("percent-encode" "percent-encoding"))))))

(define-public crate-saphir-cookie-0.13.2 (c (n "saphir-cookie") (v "0.13.2") (d (list (d (n "base64") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0x7zifkw7k3s1wpr70ixscwx2c5lxdbjdkydd090cqp8xnydvgzb") (f (quote (("secure" "ring" "base64") ("percent-encode" "percent-encoding"))))))

