(define-module (crates-io sa ph saphyr-parser) #:use-module (crates-io))

(define-public crate-saphyr-parser-0.0.1 (c (n "saphyr-parser") (v "0.0.1") (d (list (d (n "arraydeque") (r "^0.5.1") (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "yaml-rust2") (r "^0.8.0") (d #t) (k 2)))) (h "1my3p905zbcb7l3ca057xgircqrk5ihkwm63ai7r9lpc82q4nsw8") (f (quote (("debug_prints")))) (r "1.70.0")))

