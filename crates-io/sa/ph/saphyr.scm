(define-module (crates-io sa ph saphyr) #:use-module (crates-io))

(define-public crate-saphyr-0.0.0 (c (n "saphyr") (v "0.0.0") (h "0qzsvhi8swl8mr71k46m02f5537cmyzk4a4d2vsyn1yn39vn4r0r")))

(define-public crate-saphyr-0.0.1 (c (n "saphyr") (v "0.0.1") (d (list (d (n "arraydeque") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (o #t) (d #t) (k 0)) (d (n "hashlink") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "saphyr-parser") (r "^0.0.1") (d #t) (k 0)))) (h "1qzjjy66qllm70d2d00fn5mmkf10qkzz49vi9gbcv8wbhk22zpl0") (f (quote (("default" "encoding")))) (s 2) (e (quote (("encoding" "dep:encoding_rs")))) (r "1.70.0")))

