(define-module (crates-io sa ph saphir_macro) #:use-module (crates-io))

(define-public crate-saphir_macro-1.0.0-beta (c (n "saphir_macro") (v "1.0.0-beta") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00d0dk6wd8pjl21d1ybc65c7wa2vqzxvp0p6c8c1yq0bp97x4nhy")))

(define-public crate-saphir_macro-1.0.0 (c (n "saphir_macro") (v "1.0.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yqlxwxl9ys7k6z5j30ngcqqc3z38bkd5lwxwixw6xj928lcymaj")))

(define-public crate-saphir_macro-1.1.0 (c (n "saphir_macro") (v "1.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07r0gh9ih5wmbkr3x6kb7wgc6d177zbzx0lsx4p25km5vn0s73y4")))

(define-public crate-saphir_macro-2.0.0 (c (n "saphir_macro") (v "2.0.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ra4i9x9pqssa63xhmbql4yndnjzxkh1znp3vq0am5yhd200h3a5")))

(define-public crate-saphir_macro-2.0.1 (c (n "saphir_macro") (v "2.0.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jsj9shjydnb15kwal9qpznwjx0v7dbgjqz7xsam6i6ciix0zpp9")))

(define-public crate-saphir_macro-2.0.2 (c (n "saphir_macro") (v "2.0.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q54clkw41rylnvnb0bp17hbwc6rvhng22apv6wiywj0m67gnqjr")))

(define-public crate-saphir_macro-2.0.3 (c (n "saphir_macro") (v "2.0.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ixpnnsvrrqcw6760gp7ydlvbqfsib0qafd6qm669zflgr2fmqgb")))

(define-public crate-saphir_macro-2.0.4 (c (n "saphir_macro") (v "2.0.4") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ssg5zzksxv1in0px1v3j22ycrgmca19waablqfbfclmg83lm247")))

(define-public crate-saphir_macro-2.0.5 (c (n "saphir_macro") (v "2.0.5") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0miqrjbfd9rsa7gqxqkkrc4n792m6asi65m959vvgrz16m1p6ghj")))

(define-public crate-saphir_macro-2.0.6 (c (n "saphir_macro") (v "2.0.6") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jz22wmxq2nfk2j152bv8g28zzymxnh4y3blvd137kfzg0bi4xfn")))

(define-public crate-saphir_macro-2.0.8 (c (n "saphir_macro") (v "2.0.8") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01zv4q66xbxixs959k9vnhsr0lm7glf1206ljjiasam1ah11wz9z")))

(define-public crate-saphir_macro-2.0.9 (c (n "saphir_macro") (v "2.0.9") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17ffkw0jbaki1gfs5159rmmm4ghh0yznvwhxn3s3ncphzzxswvcd")))

(define-public crate-saphir_macro-2.0.10 (c (n "saphir_macro") (v "2.0.10") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03d3bq3ynvhy06wsq81hqs66w6scjdp2qkxgjlpd6gc2h1zs3cj7")))

(define-public crate-saphir_macro-2.1.0 (c (n "saphir_macro") (v "2.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15l8yzba6qyzlx88c2kfz4gfh2yzyvvwqa7pbhjbw44yz957d6v6")))

(define-public crate-saphir_macro-2.1.1 (c (n "saphir_macro") (v "2.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g9d93xi838ala5hhbidx0hqny80sg92vw2zpd6k701ck5gqbyzp")))

(define-public crate-saphir_macro-2.1.2 (c (n "saphir_macro") (v "2.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j9zqfjd4k2m0vyvksmw56821kfk32xrspi5y6p4szpc2l2zny6d")))

(define-public crate-saphir_macro-2.2.0 (c (n "saphir_macro") (v "2.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w5j4801i03vxrrgdd1vpivyshin6lx0r4y9yw80c4nw21a3dcz7") (f (quote (("validate-requests") ("tracing-instrument") ("full" "validate-requests") ("default"))))))

