(define-module (crates-io sa p- sap-parser) #:use-module (crates-io))

(define-public crate-sap-parser-1.0.0 (c (n "sap-parser") (v "1.0.0") (d (list (d (n "sap-ast") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)))) (h "167023y815nf5dj43pqcqywz0a4hinmy2xd3pyjm07byb0xfnwcg")))

(define-public crate-sap-parser-1.0.1 (c (n "sap-parser") (v "1.0.1") (d (list (d (n "sap-ast") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.1") (d #t) (k 0)))) (h "15j2vg4z4bqa10x621vbn93gif26nxr03kggxa343n54032vwfpb")))

