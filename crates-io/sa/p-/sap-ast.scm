(define-module (crates-io sa p- sap-ast) #:use-module (crates-io))

(define-public crate-sap-ast-1.0.0 (c (n "sap-ast") (v "1.0.0") (d (list (d (n "sap-lexer") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1j5z37zyl03scbll86v3d3nm8pgb1nj5sxg1wz92mmi49rvf6snq")))

(define-public crate-sap-ast-1.0.1 (c (n "sap-ast") (v "1.0.1") (d (list (d (n "sap-lexer") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "18zxz73l2j0zlb3j4yigs5ps4rzvd09zw1x24bjgzp4hyzdzpncq")))

