(define-module (crates-io sa p- sap-scripting) #:use-module (crates-io))

(define-public crate-sap-scripting-0.1.0 (c (n "sap-scripting") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0mz8vzzz79ax49wif1rbisyknwd1d8i7adv5lfz7331abkp321x3")))

(define-public crate-sap-scripting-0.2.0 (c (n "sap-scripting") (v "0.2.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "12i2cgifcw51nic3q6c475ylrhbdwz3ja97063g1s5r2dls2hw3v")))

(define-public crate-sap-scripting-0.3.0 (c (n "sap-scripting") (v "0.3.0") (d (list (d (n "com-shim") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0vhgxylk307fl9bf61r202s7jh51wvr0ld0achlr9bpc7l1sgx11")))

(define-public crate-sap-scripting-0.3.1 (c (n "sap-scripting") (v "0.3.1") (d (list (d (n "com-shim") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1z8461mpibkc2dm2n64x59ncglyagr0376pj536l7zqwzc6rggj9")))

(define-public crate-sap-scripting-0.3.2 (c (n "sap-scripting") (v "0.3.2") (d (list (d (n "com-shim") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1bjmwcdyh8k3fzjbnl7d22kwlf16ii3ahgdnypjijji0d01ryc96")))

(define-public crate-sap-scripting-0.3.3 (c (n "sap-scripting") (v "0.3.3") (d (list (d (n "com-shim") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "05461nhjrmk88jd3jd07ys34kh6j4jfsshsays2464h9zn4i0pqx")))

(define-public crate-sap-scripting-0.3.4 (c (n "sap-scripting") (v "0.3.4") (d (list (d (n "com-shim") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0lxjr6xw7klazyg3v0xnn1wihg7bylfqvw5bqp87m70qqfshbbmj")))

(define-public crate-sap-scripting-0.3.5 (c (n "sap-scripting") (v "0.3.5") (d (list (d (n "com-shim") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1msxhcsjscgzpx3f1ydnxcniw4xryrpb1r4qn9zs057kmfgmdd63")))

(define-public crate-sap-scripting-0.3.6 (c (n "sap-scripting") (v "0.3.6") (d (list (d (n "com-shim") (r "^0.3.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Variant"))) (d #t) (k 0)))) (h "1h76ff83bsszldw7hfp6qy6sk689van042g2ak6x2h2bx6s97ijr")))

(define-public crate-sap-scripting-0.3.7 (c (n "sap-scripting") (v "0.3.7") (d (list (d (n "com-shim") (r "^0.3.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Variant"))) (d #t) (k 0)))) (h "0amcyvbyzpqy9bzn075fygg2vzqi80xz1ccg7w59bap7ax2rgg6d")))

(define-public crate-sap-scripting-0.3.8 (c (n "sap-scripting") (v "0.3.8") (d (list (d (n "com-shim") (r "^0.3.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole" "Win32_System_Variant"))) (d #t) (k 0)))) (h "1s2qh83v930xqck7fg9fcsi1crp580nxifq7mjqz91833bs10hxm")))

