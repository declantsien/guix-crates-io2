(define-module (crates-io sa p- sap-shared) #:use-module (crates-io))

(define-public crate-sap-shared-1.0.0 (c (n "sap-shared") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "1hrjfcrf5kdvr264s075wv1k65a1jgps9s8ix5b4n136mxy008kw")))

(define-public crate-sap-shared-1.0.1 (c (n "sap-shared") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "15wd1xzyfddryxz0icsb7p05ia9j5r27dafzfm7sz9mddg2ikqzq")))

