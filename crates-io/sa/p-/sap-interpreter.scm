(define-module (crates-io sa p- sap-interpreter) #:use-module (crates-io))

(define-public crate-sap-interpreter-1.0.0 (c (n "sap-interpreter") (v "1.0.0") (d (list (d (n "sap-ast") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-parser") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)))) (h "0c2slxrqlz4clxr913ybgpsc9v10jijij8629dw79dmc7d5n2v0w")))

(define-public crate-sap-interpreter-1.0.1 (c (n "sap-interpreter") (v "1.0.1") (d (list (d (n "sap-ast") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-parser") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)))) (h "1x2la4jhk5pkl20ph6qykxq6mhsvnc6sl99aw3002k6whazj8jz6")))

(define-public crate-sap-interpreter-1.0.2 (c (n "sap-interpreter") (v "1.0.2") (d (list (d (n "sap-ast") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-parser") (r "^1.0.0") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)))) (h "1927hw9jw33g8yzi9yaak5ws8m13aaysp69bfghi0lp8f1hz9zfg")))

(define-public crate-sap-interpreter-1.0.3 (c (n "sap-interpreter") (v "1.0.3") (d (list (d (n "sap-ast") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-lexer") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-parser") (r "^1.0.1") (d #t) (k 0)) (d (n "sap-shared") (r "^1.0.1") (d #t) (k 0)))) (h "0nq0a5vvmqnrlqzdg7i0468yzz9k8amgahrkh4w1y5lbrs2cxkz0")))

