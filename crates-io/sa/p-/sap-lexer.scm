(define-module (crates-io sa p- sap-lexer) #:use-module (crates-io))

(define-public crate-sap-lexer-1.0.0 (c (n "sap-lexer") (v "1.0.0") (d (list (d (n "sap-shared") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1571svkqwxvpi2msnwragsirlmnpqzb05cdrc7nf6aqjib4bfkk7")))

(define-public crate-sap-lexer-1.0.1 (c (n "sap-lexer") (v "1.0.1") (d (list (d (n "sap-shared") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x6md8mr5hwgfx2k9gv2yjmdr06iknc41sabqb2djfd89hs2wwv4")))

