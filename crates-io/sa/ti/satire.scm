(define-module (crates-io sa ti satire) #:use-module (crates-io))

(define-public crate-satire-0.0.1 (c (n "satire") (v "0.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "typed-index-collections") (r "^3.0") (d #t) (k 0)))) (h "084790wpz90a0w23jppxjq7anjcv7nc9anikiksy069nb3hrgghn")))

