(define-module (crates-io sa ti satisfactory-save-file) #:use-module (crates-io))

(define-public crate-satisfactory-save-file-0.1.0 (c (n "satisfactory-save-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wk0k27s4z9f048llc2gx8rsa6sxh3a7ib96vkz6xjmhvc52yirf")))

(define-public crate-satisfactory-save-file-0.1.1 (c (n "satisfactory-save-file") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f3k1s7qf2wrgsbdgs3kqji9281cii6035830jlnf7b756fzy2r3")))

(define-public crate-satisfactory-save-file-0.2.0 (c (n "satisfactory-save-file") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zrw4iq41fazd7pj00xli51yjfmchi6axqr1rmliyc298cpz04hc")))

