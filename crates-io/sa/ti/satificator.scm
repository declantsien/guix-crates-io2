(define-module (crates-io sa ti satificator) #:use-module (crates-io))

(define-public crate-satificator-0.1.0 (c (n "satificator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bigs") (r "^0.2.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nc9h83az17rf5sshfba35gzkmcd61mz19rhqh82x8zp629rnkx5")))

(define-public crate-satificator-0.1.1 (c (n "satificator") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bigs") (r "^0.2.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fslfd5s7rkb5gai577xfhnl183n299mi61xybicacf2rja7yqz1")))

(define-public crate-satificator-0.1.2 (c (n "satificator") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bigs") (r "^0.2.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vimm9yw6q94qn7m7rmj7br9783np9bs2ryj9fq628xwy3sm4clp")))

