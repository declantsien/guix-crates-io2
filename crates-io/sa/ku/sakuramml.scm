(define-module (crates-io sa ku sakuramml) #:use-module (crates-io))

(define-public crate-sakuramml-0.1.13 (c (n "sakuramml") (v "0.1.13") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0p8jpvl74x5pnlnmhbr2x8qwb3j43wa2zjy1jbaxwx4azk6drr14") (r "1.64.0")))

(define-public crate-sakuramml-0.1.14 (c (n "sakuramml") (v "0.1.14") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1w7s3ljhy2mpd6v7na8dralwz1ac0zmnvrxcgafw5x33fcdaqrj8") (r "1.64.0")))

(define-public crate-sakuramml-0.1.15 (c (n "sakuramml") (v "0.1.15") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0p0lqkdpqvqrd2rvhfb69bfp8k08mv07a6g8sbngam96smqyilac") (r "1.64.0")))

(define-public crate-sakuramml-0.1.16 (c (n "sakuramml") (v "0.1.16") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1yif78rsklacps79rw1dvf832lng6xsnk2sllp8bdng54ss4rqlw") (r "1.64.0")))

(define-public crate-sakuramml-0.1.18 (c (n "sakuramml") (v "0.1.18") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1ldimr33rcxbh0cdirhh5lyvzpycnvsdi96dcam0ggjfpgza9b74") (r "1.64.0")))

(define-public crate-sakuramml-0.1.21 (c (n "sakuramml") (v "0.1.21") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1y02c84gyx3r2z8znz85qwbaqdxaih5xg1g08c9j3x1297lqhfi8") (r "1.64.0")))

(define-public crate-sakuramml-0.1.22 (c (n "sakuramml") (v "0.1.22") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0jzkm5lll4arn1i0m74ya7ddzfcsf478cdm9xc5jk2pkzqfxh7rp") (r "1.64.0")))

(define-public crate-sakuramml-0.1.23 (c (n "sakuramml") (v "0.1.23") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0by4gyhgrvsji4p70nx4j38s2d86pf6msix4cpy6xghz7s22ksnk") (r "1.64.0")))

(define-public crate-sakuramml-0.1.24 (c (n "sakuramml") (v "0.1.24") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1rm4j35vd67jkxqyhwjnkqf5k6fdh55s3ml9p55gcnfjqjp3ca4b") (r "1.64.0")))

(define-public crate-sakuramml-0.1.25 (c (n "sakuramml") (v "0.1.25") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0a937fi7dx4015c82ysbpnf1yjw1sks4dc73z0dvghqn4jkc93lm") (r "1.64.0")))

(define-public crate-sakuramml-0.1.26 (c (n "sakuramml") (v "0.1.26") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0ygfd701izm5w4hns1k7ydrgvbnkxyvl4kil9d7ay22da95a5xaz") (r "1.64.0")))

