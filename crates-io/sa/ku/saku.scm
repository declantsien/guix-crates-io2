(define-module (crates-io sa ku saku) #:use-module (crates-io))

(define-public crate-saku-0.1.0 (c (n "saku") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "0ipvb4ghsrv3pmjn5s88sql0izwqd56h8j7r5pp54q3dxp9dpc8v")))

(define-public crate-saku-0.1.1 (c (n "saku") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "15z6r2d2jjkhz4ss5jx9lws5wkwjxcgqb49hiz6wx82ay4zqr0nh")))

(define-public crate-saku-0.1.2 (c (n "saku") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1csva8rpk3qazys2mbf1s4di98ffp2f662hr3ri9s17862nll8hy")))

(define-public crate-saku-0.1.4 (c (n "saku") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1jyw9mh54773q8cnw4bp0579yxplpnqbzayn39gqs4v5cn57m8ji")))

(define-public crate-saku-0.1.5 (c (n "saku") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0h0p4sxy5fwgj05hhw7wv41hinni7y06p5xb8x3fhwikqlacjz43")))

(define-public crate-saku-0.1.6 (c (n "saku") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "12li03v0pplf21g06d4mjbk3gcbg3bpwkx7mz796wgjrvqq0wpws")))

