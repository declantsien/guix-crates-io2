(define-module (crates-io sa ku sakuin) #:use-module (crates-io))

(define-public crate-sakuin-0.1.0 (c (n "sakuin") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "19586mh0n2f8ik8fqj0ababwcgqf6vdj58w10ckgcadkkiv9m0fl") (y #t)))

