(define-module (crates-io sa ve save_state_derive) #:use-module (crates-io))

(define-public crate-save_state_derive-0.1.0 (c (n "save_state_derive") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "16fxcb1jifgi71w6ryf9b3mk9k6bxj7asirplzi8dcan2qkcrkma")))

(define-public crate-save_state_derive-0.1.1 (c (n "save_state_derive") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^1.0.69") (d #t) (k 0)))) (h "1hrgnf8fk2n1zb93pq49cc5ia122g3s1pwphl7lf8lf4h50083dr")))

