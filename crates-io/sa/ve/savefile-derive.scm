(define-module (crates-io sa ve savefile-derive) #:use-module (crates-io))

(define-public crate-savefile-derive-0.1.0 (c (n "savefile-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1vmmf1njfmpczh65qgz3r1balqns411mr9kp3mgg58lgaxwqfjl2")))

(define-public crate-savefile-derive-0.1.2 (c (n "savefile-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0qw826jqnz1rwxf8gfj4anh6jddkpd2hhkj16lyb0ry1ig408649")))

(define-public crate-savefile-derive-0.1.3 (c (n "savefile-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "155zhiksvnlmyfy8wp12kf7ji6k07k52npmz9xj2p4v2h1w7zpif")))

(define-public crate-savefile-derive-0.1.4 (c (n "savefile-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "savefile") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0mwpxkkylghqdy6gi266q285vnv8n3ln504yfcp8l3mp7kidz5ry")))

(define-public crate-savefile-derive-0.1.5 (c (n "savefile-derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "166xh8zjg1y680g2arkvh0ry88gxwjv98gmwk6bj2ay83d6yp331")))

(define-public crate-savefile-derive-0.1.6 (c (n "savefile-derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0cyfxvhc10whaggabrbq2s0dk6f6218vvkvch6rl5cisirb5422q")))

(define-public crate-savefile-derive-0.1.7 (c (n "savefile-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1ksrjgw2fiv4rf8d1bp6dj5zjmr12rhhc8np8pm2bvgr8qpmdf8j")))

(define-public crate-savefile-derive-0.1.8 (c (n "savefile-derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1mrm8b83i7dlggly69bf47sl918pr1kb9m3akc454x88wgi71s7y")))

(define-public crate-savefile-derive-0.2.1 (c (n "savefile-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0f4sm5zi9908s1zdd7fiidfx9dfkq9v5i07ar0ls1f959b15qjca")))

(define-public crate-savefile-derive-0.2.2 (c (n "savefile-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0wvry8a10rkj57a8nzbnqkycwvf7glhkczz366x0lisaa716q7fa")))

(define-public crate-savefile-derive-0.2.3 (c (n "savefile-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1x00ghwi9c916s40x86h0pp5h4f0b4a5vgwsxhdkcb5r3wxixlbj")))

(define-public crate-savefile-derive-0.2.4 (c (n "savefile-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0vphxk2hr2mvry07hiak930x419288qpp4y5kngs23bigh6vrxar")))

(define-public crate-savefile-derive-0.2.5 (c (n "savefile-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0gszl91dzzsifsha3pn6dyjyh8vixr36pn1a37gfvw7nh72j6b1q")))

(define-public crate-savefile-derive-0.2.6 (c (n "savefile-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1bxw5ls9rc6r4aj9a418byycpdwkv1235fplkaa3x0q5apcvc8hg")))

(define-public crate-savefile-derive-0.2.7 (c (n "savefile-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1hvhgvqsaw6bwyxbk25rmzs9dajwzgjjj794yqn4fhnrn14mz1dy")))

(define-public crate-savefile-derive-0.2.8 (c (n "savefile-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "0w1l52jcd5lm399bd1llf1zqkaryq3297hmi9a9ljyzayaxzxlyf")))

(define-public crate-savefile-derive-0.2.9 (c (n "savefile-derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (d #t) (k 0)))) (h "05d5vxx2cmc0b56mdbmlz07ddzwmk9f3lj8n96jg8r70n736plf6")))

(define-public crate-savefile-derive-0.2.11 (c (n "savefile-derive") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0xycxz3dbwhl74v6rgk4i9lmrwmwib4jlxgbarass33rgnlb3bkj")))

(define-public crate-savefile-derive-0.2.12 (c (n "savefile-derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0ywqhhjx1mknfi79vjqwxvdlaaqrn4q1yfqsf1992v2jv6cvc860")))

(define-public crate-savefile-derive-0.3.0 (c (n "savefile-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1yjg39ykwfiyj2nl69wg008kdlq9pg8vishns0fr999d40sjc9bb")))

(define-public crate-savefile-derive-0.3.1 (c (n "savefile-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1a2pgw83sjhk7lp1c2mfh5brv81q79g8nw9b9jykwgqg6l59g6b0")))

(define-public crate-savefile-derive-0.4.0 (c (n "savefile-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "15yxkjdq5y83x92isyaqf7l6ny1mva9hxi2b2qpl5fb30fasjdlb")))

(define-public crate-savefile-derive-0.4.1 (c (n "savefile-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1ij73dwa1w54qz8vpk3w72a7lb5mx4d5pidh1cb7c3xadp6fyan6")))

(define-public crate-savefile-derive-0.4.2 (c (n "savefile-derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0x3xlk9krxvjijrki5fv2qviw09j8zksv7rcjczym0192c55v91h")))

(define-public crate-savefile-derive-0.5.0 (c (n "savefile-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "146axbgl0jj49jklp82znkx31v6vcw7plp73d1c25v6nldx0d12k")))

(define-public crate-savefile-derive-0.6.0 (c (n "savefile-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "12m5n8nn2aipvby4khbzvab092a1hgm6gi1yv9fj3lq8qrhx60pp")))

(define-public crate-savefile-derive-0.6.1 (c (n "savefile-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1hpms6z704n6a7bvpggvz04m1q1ys68salzwpjzyslcwrx07sk0z")))

(define-public crate-savefile-derive-0.7.0 (c (n "savefile-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0axb7z68sf80mxz6afx9dnhym83ma8qfyj4zwsi0hsnya23m4wpv")))

(define-public crate-savefile-derive-0.7.2 (c (n "savefile-derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "081qr7agpjq8992pxk36m96af91w38bya9bc7xzpk3bczq70ns1v") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.7.3 (c (n "savefile-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1dxzk234niyim88l7532vbrhxi43v1vxa2w2q7lh7czdb5s24gbn") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.7.4 (c (n "savefile-derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1daxcabdavb89vg6b6ppqfy86ays33s8z21kjzm0g667gnfvs4r8") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.8.0 (c (n "savefile-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1scw2sp2ijkjwrrjp7fjjha6fn0mvsxlxs03la5q94kiwkds09ig") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-savefile-derive-0.8.1 (c (n "savefile-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "09ddab8mhylnmfcxcq7lszvla9v7sgjyhv8flqqqsqpd92lhj9bp") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.8.2 (c (n "savefile-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "03nw747f630nl90l3vvk6mms44pipb0a2xqijpdfmj1cqnfhrfip") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.8.3 (c (n "savefile-derive") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1n4f6scs8yl02kdizvymkhjhvq3ji4mm25dp7yybnkx8fmkijgvl") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.9.1 (c (n "savefile-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0pynndjd6nqxsm7mcsblaigy6rqprsprbj682mj8433ki30pfvrg") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.10.1 (c (n "savefile-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0fq8lxqswh3bj4pjv94xzypaqgix2g6vnnihz8z6mdxr10ck30yy") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.11.0 (c (n "savefile-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pldkrn99fakni90gf4i8kfrpg0j6nk4pi1b5swm2jlmb8ab2wd5") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.12.0 (c (n "savefile-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13vvqca28wvym617f192nrykmb8lv2m1wirs5d96bklg7v4q04dy") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.13.0 (c (n "savefile-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cv1r3l5jaw83ibvxfdybvdbr6y3r7dssrswmrifw4261bcyb6qf") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.14.0 (c (n "savefile-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v2z16h35jxb0b1nzj8h5n0pqmqdsb99pi4aqv74444calksmjxl") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.14.3 (c (n "savefile-derive") (v "0.14.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1za0rpdpsjky73gscailx36ls2d6x5kybjwnlr0kn77w9pvi8x3k") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.15.0 (c (n "savefile-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ypprl9qr3m1dzbly9j6l0ylgj4rqsfib58gcp92sw4i85j005x7") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.16.0 (c (n "savefile-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r1wx9c37mf9jgmac7gzqpcl7d4ysi35vij5r4a02hv0piiznshn") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.16.2 (c (n "savefile-derive") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "109l6kmkhrsg4n1f7jf66nrp60nrbjqim20shnka767n9sjsdd91") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.16.4 (c (n "savefile-derive") (v "0.16.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gspbp35kganb1s5lxjaam9q0c7fbbzyn3zvhg5v9v95anywb938") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.16.5 (c (n "savefile-derive") (v "0.16.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zv349abw4fp9ldq9lqsww3fzag0233xwm1zk65zzvnf9ar4qqic") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.1 (c (n "savefile-derive") (v "0.17.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14r46m2fv75snw46dq8m0f9yzk8q3cc4jan94jjwq1pxhd4x5j8x") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-savefile-derive-0.17.0-beta.2 (c (n "savefile-derive") (v "0.17.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mqrxdd11c7mzgyyphah7bnnhn40k3hdv0dhi91zhnk9isdsg1v0") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-savefile-derive-0.17.0-beta.3 (c (n "savefile-derive") (v "0.17.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05a1m0br8x8kfj96pm5z4a8vi8absgrlgxw6sjf2249qjwhnrlx5") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-savefile-derive-0.17.0-beta.4 (c (n "savefile-derive") (v "0.17.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f31p2103nvz7dgkg66mnr6l955x3rfrm34jnwqfhniciakn2h67") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-savefile-derive-0.17.0-beta.5 (c (n "savefile-derive") (v "0.17.0-beta.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xbya8xdxdw51dbn1qzk5h7b512mhi60d416mhvk2xwr2y1js4a7") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.6 (c (n "savefile-derive") (v "0.17.0-beta.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x5bassq2nfp59y3pxqrs5rdxn8vcpimwgkjcyck49blaf4i0pfs") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.7 (c (n "savefile-derive") (v "0.17.0-beta.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pdwl8fpzf8ikl224ccgp7k6rbz34zf4sbr0b5xkd06m80lzcrgn") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.8 (c (n "savefile-derive") (v "0.17.0-beta.8") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mvvjalfw98c1riz0kkl69vpxswmzyi0vd54qqcjsxgrqxwmrwcx") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.9 (c (n "savefile-derive") (v "0.17.0-beta.9") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q4s3dq5b7jv1lixa65z3divr6aq9h2djcghcpp66hg8k4xpjrq9") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.10 (c (n "savefile-derive") (v "0.17.0-beta.10") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gmac5v0pvbn4q9vf0rzfsd11fbn5bfsvd77xc46varcdi3hxn11") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.11 (c (n "savefile-derive") (v "0.17.0-beta.11") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rwgdqllb5mgcz79ma1hiz248nqyp98g6b7pzafh9nqg51ax7bjv") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.12 (c (n "savefile-derive") (v "0.17.0-beta.12") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18xyr6dxlq1xq53f9qjx2c7pr9mbj4s2ndjbzhf648cwjzisyv9p") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.13 (c (n "savefile-derive") (v "0.17.0-beta.13") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dnxxfys00v4nygnyrq8y2b6v5xzrc9ia7prnnkqiz9an94rc484") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.14 (c (n "savefile-derive") (v "0.17.0-beta.14") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09m47icw3is6mzsji1f7k228c8nlqni2gn1qgzxik29rdwvxg6k3") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0-beta.15 (c (n "savefile-derive") (v "0.17.0-beta.15") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qi7z2d81swr5cfip07xmdxcr0ig3rw6gl3hi2zyjqkaarpaxinb") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.0 (c (n "savefile-derive") (v "0.17.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gf6gxpv4zhjmnbfsq8j70zsssyb2fxj2iwdlb2cdnyxhss0wi0v") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.1 (c (n "savefile-derive") (v "0.17.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0imikn3v72zgkp4i4842yv2v1zg6giakairv9kcwh1llckl4plxr") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.2 (c (n "savefile-derive") (v "0.17.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bxm3cwwkknrglg4zvismv1z0k1mrxaq7rw8nn59fjljm1gi81k9") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.3 (c (n "savefile-derive") (v "0.17.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h78jfr8yj04y774vpsfyky9kay3xxndw7s8nwf7zp74s614w4vq") (f (quote (("nightly") ("default"))))))

(define-public crate-savefile-derive-0.17.4 (c (n "savefile-derive") (v "0.17.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18gz2qc07zx997gkgxjhg51qyljx4yc8q6saws7l3dj5gmnjyd6b") (f (quote (("nightly") ("default")))) (r "1.74")))

