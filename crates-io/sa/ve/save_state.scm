(define-module (crates-io sa ve save_state) #:use-module (crates-io))

(define-public crate-save_state-0.1.0 (c (n "save_state") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "save_state_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1w7ri10dxv5p3nzbrn5d7ilxzzi66979nvvjchc94jdg2h2rkp1j")))

(define-public crate-save_state-0.1.1 (c (n "save_state") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "ciborium") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "save_state_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0khq8dmnygziix4bkgix0k754rbqwh56p5k6w74gix3g9c75g2rx")))

