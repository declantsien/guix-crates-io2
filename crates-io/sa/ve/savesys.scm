(define-module (crates-io sa ve savesys) #:use-module (crates-io))

(define-public crate-savesys-3.5.0 (c (n "savesys") (v "3.5.0") (h "1plwyxpii8ahsym9vvx6zy0g6wg5z7q21nn3lrajhdsw90asznl8")))

(define-public crate-savesys-3.5.1 (c (n "savesys") (v "3.5.1") (h "0zv0jkyh35iqgmx1i11rik8jqs6vn7zvf14y7al1bapnhrpl1p9d")))

(define-public crate-savesys-3.5.2 (c (n "savesys") (v "3.5.2") (h "1sl6jrhr0ncbldpvf03lp1b9rlm8glazw1wc0bwbs4a1hd5c702k")))

(define-public crate-savesys-3.5.3 (c (n "savesys") (v "3.5.3") (h "1421frsf3b8p7a0s0778c77j1rd24sgbqxkd9rnx6wpgqd4fjh7k")))

(define-public crate-savesys-3.5.4 (c (n "savesys") (v "3.5.4") (h "0zc6hpic2j5dfy9l6cv2d7i0972akfnpkmw6hccqgn6zqpxg4pd4")))

(define-public crate-savesys-3.5.5 (c (n "savesys") (v "3.5.5") (h "1dp85w96fpi8cp1p7f8nwfmmiq0x782q1ygjfiwsp78g3jfbpjc2")))

(define-public crate-savesys-3.5.6 (c (n "savesys") (v "3.5.6") (h "15gix2nm5573zpqhyhq88zaah0q8m6j3d2x2ilvsnpb5h18h96gs")))

(define-public crate-savesys-3.5.7 (c (n "savesys") (v "3.5.7") (h "1sh3593khk16dcr0wakhvfb9nrcxyipmscbzv5g3wisa830rmn6b")))

(define-public crate-savesys-3.5.8 (c (n "savesys") (v "3.5.8") (h "011pq4qfxgk2sjjvdwz0mwg7ra6pjhf1ss70q0kc8z8gc9wz4r6k")))

(define-public crate-savesys-3.5.9 (c (n "savesys") (v "3.5.9") (h "1a399x17vdw8l659b608lyci4nh6bk3658gyxayc5j5wbgdix4cs")))

(define-public crate-savesys-3.5.10 (c (n "savesys") (v "3.5.10") (h "06ycv1v9pb17l5qbwz6nkyfn0dgljggzzq6r4glabwjcl2zl93ss")))

(define-public crate-savesys-3.5.11 (c (n "savesys") (v "3.5.11") (h "0w1h40wmyx4q5b8iisnmg4p353mmqkls7clz4398jkjgjwp2yyca")))

