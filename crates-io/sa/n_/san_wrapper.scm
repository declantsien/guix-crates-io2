(define-module (crates-io sa n_ san_wrapper) #:use-module (crates-io))

(define-public crate-san_wrapper-0.1.0 (c (n "san_wrapper") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)))) (h "0fh0jz5xsafmzahfrpggf0f6cv40m1i5vwqrw692rj5imnfjq2kl")))

