(define-module (crates-io sa lz salzweg) #:use-module (crates-io))

(define-public crate-salzweg-0.1.0 (c (n "salzweg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lzw") (r "^0.10") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "weezl") (r "^0.1") (d #t) (k 2)))) (h "19h28s47yzyx0f073avgbxmrk6igqwqbpkwvxlsy2a0gm513issa")))

(define-public crate-salzweg-0.1.1 (c (n "salzweg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lzw") (r "^0.10") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "weezl") (r "^0.1") (d #t) (k 2)))) (h "02017nqgrpm01m8xx8wgi7zy96vaimi2vgsj4jqiiksw83vka3h2")))

(define-public crate-salzweg-0.1.2 (c (n "salzweg") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lzw") (r "^0.10") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "weezl") (r "^0.1") (d #t) (k 2)))) (h "1lydni45j57ng4zmzd44fpmsw33f9jhqlq4d7avb3g1ajjqzza6a")))

(define-public crate-salzweg-0.1.3 (c (n "salzweg") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lzw") (r "^0.10") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "weezl") (r "^0.1") (d #t) (k 2)))) (h "0igrb1q7424pcf5fhybam4655jkz707rcqkrsx24sgvp9h5njhbc")))

(define-public crate-salzweg-0.1.4 (c (n "salzweg") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lzw") (r "^0.10") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "weezl") (r "^0.1") (d #t) (k 2)))) (h "1ljh956w5ahx2xr6v7dp8kdpsa1y3xf98ssmwr9djq0lvf2yra99")))

