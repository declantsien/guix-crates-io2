(define-module (crates-io sa t- sat-rs) #:use-module (crates-io))

(define-public crate-sat-rs-0.0.1 (c (n "sat-rs") (v "0.0.1") (h "0i1g3zhw55g7hj7l6pzbk8xhyi34vqxha1fpl7kxphzisikfcr0g")))

(define-public crate-sat-rs-0.0.2 (c (n "sat-rs") (v "0.0.2") (h "1jc1xlgfjx3a2dr1715ss8xvwmzxbcjb5jjqb8p7kc2jkdip1ycx")))

(define-public crate-sat-rs-0.0.3 (c (n "sat-rs") (v "0.0.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bz3b9vk5fzm4rpg1mqyck9xsak8lbz61282vvb3i7zj790chnkh")))

