(define-module (crates-io sa t- sat-solver) #:use-module (crates-io))

(define-public crate-sat-solver-0.1.0 (c (n "sat-solver") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.7.0") (d #t) (k 0)))) (h "08y5djxri5722jwzkb77m8i95q2ivsri621aw60mc9fwfb91p9sp")))

(define-public crate-sat-solver-0.1.1 (c (n "sat-solver") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num-rational") (r "^0.2.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)) (d (n "priority-queue") (r "^0.7.0") (d #t) (k 0)))) (h "0pxw7b1jjhm7nkd4z0aw8ks4k8h8bvk1mz50q05497kiszsskcda")))

