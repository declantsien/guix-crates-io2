(define-module (crates-io sa nk sankey) #:use-module (crates-io))

(define-public crate-sankey-0.1.0 (c (n "sankey") (v "0.1.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "0f7k459b63mxd2hss6b141yzjxh749lv2zcygsrcpaqky30hykrj")))

(define-public crate-sankey-0.1.1 (c (n "sankey") (v "0.1.1") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "069zac5883133np2n1bcimrshmxjn2p6gjg246xd0svzry5q83id")))

(define-public crate-sankey-0.1.2 (c (n "sankey") (v "0.1.2") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "svg") (r "^0.10.0") (d #t) (k 0)))) (h "1cxxyxk60s22lcq51yvmj4mvxlrnrlv1phnds5a5f4jaz5rbmxqp")))

