(define-module (crates-io sa wp sawp-pop3) #:use-module (crates-io))

(define-public crate-sawp-pop3-0.12.0 (c (n "sawp-pop3") (v "0.12.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "sawp") (r "^0.12.0") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)))) (h "006a5gsy0kypr0vbf4wwzsyv03jrr9j6r5bf8wx6wv26n56zv5m3") (f (quote (("verbose" "sawp/verbose") ("ffi" "cbindgen" "sawp/ffi" "sawp-ffi"))))))

(define-public crate-sawp-pop3-0.12.1 (c (n "sawp-pop3") (v "0.12.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "sawp") (r "^0.12.1") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.1") (d #t) (k 0)) (d (n "rstest") (r "^0.6.4") (d #t) (k 2)) (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)))) (h "1ihns27jgxszh0rm28fczxxpc7d5p5kzsy6dlpv7g1mpvvaqcp26") (f (quote (("verbose" "sawp/verbose") ("ffi" "cbindgen" "sawp/ffi" "sawp-ffi"))))))

