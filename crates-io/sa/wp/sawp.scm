(define-module (crates-io sa wp sawp) #:use-module (crates-io))

(define-public crate-sawp-0.1.1-rc.1 (c (n "sawp") (v "0.1.1-rc.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.0.0") (d #t) (k 0)))) (h "1vqybr9xc9ckgi1kjfdv8gvwn4xsnf84wrv5xdxpmnybr97f6267")))

(define-public crate-sawp-0.1.1-rc.2 (c (n "sawp") (v "0.1.1-rc.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "=5.1.1") (d #t) (k 0)))) (h "0cah2wr11s0bf9yisvpz9fyvpm027l13kwskf50p3mcghxfljr8s")))

(define-public crate-sawp-0.1.1-rc.3 (c (n "sawp") (v "0.1.1-rc.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "1zbiqlbzd2qbwbxj039ncpx5qwb7lmyk8r374qxd8la76aqdyqxi")))

(define-public crate-sawp-0.1.1 (c (n "sawp") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "cdylib-link-lines") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0jx3rcpyx4rx2x703r32c1viwhb7fl1xxflyr51l86n483d3ddh3") (f (quote (("verbose") ("ffi" "cbindgen" "cdylib-link-lines" "sawp-ffi"))))))

(define-public crate-sawp-0.2.0 (c (n "sawp") (v "0.2.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "cdylib-link-lines") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0syfh0gvbjkc5i1yi9rcjp8hdc3y3mhaaldjbky0x2xg9s8mr05r") (f (quote (("verbose") ("ffi" "cbindgen" "cdylib-link-lines" "sawp-ffi"))))))

(define-public crate-sawp-0.3.0 (c (n "sawp") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "cdylib-link-lines") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1594i37ll83f28bd7rnqvwxv1qcxhh7w7vpqhl2m3y7025fmsdm7") (f (quote (("verbose") ("ffi" "cbindgen" "cdylib-link-lines" "sawp-ffi"))))))

(define-public crate-sawp-0.4.0 (c (n "sawp") (v "0.4.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "cdylib-link-lines") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0kgc9pclkajbabar07hkx6d0g4dgyqmwrm3s9pzp57yqdibfi4sh") (f (quote (("verbose") ("ffi" "cbindgen" "cdylib-link-lines" "sawp-ffi"))))))

(define-public crate-sawp-0.5.0 (c (n "sawp") (v "0.5.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "cdylib-link-lines") (r "^0.1.1") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "12pdl7kziqnh0fw6df94kwsf6yijk6mh3z93g0i8s5pdgka41x94") (f (quote (("verbose") ("ffi" "cbindgen" "cdylib-link-lines" "sawp-ffi"))))))

(define-public crate-sawp-0.6.0 (c (n "sawp") (v "0.6.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0pfasgs4dxcnpc3y25n8q3952ngh1dn08f4hx10i3is47h3p0w7c") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.7.0 (c (n "sawp") (v "0.7.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "01ck9fj3xqfbb7pqcfdgsci96pj0xqcimh0arcnwk0hbf6la1a9l") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.8.0 (c (n "sawp") (v "0.8.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "half") (r "~1.7") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1bsyrvvmf7kcwkif1zc2x87sk09pz8bwc19kc0g70vginiwvld20") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.9.0 (c (n "sawp") (v "0.9.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "half") (r "~1.7") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0gfw6pnprqxs4bpvisib7hdj4dscqvvgv4hwp491lin18rgnrsyz") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.10.0 (c (n "sawp") (v "0.10.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "half") (r "~1.7") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "00zch50173lvid72apbhq8wm9bhkp70v8yp6hd1jl5vax1k83w8v") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.11.0 (c (n "sawp") (v "0.11.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "half") (r "~1.7") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "1f5k5qxq858hkdd7lijf4w00wgivw5f3hqvljg78lhz09604qflm") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.11.1 (c (n "sawp") (v "0.11.1") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "half") (r "~1.7") (d #t) (k 0)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "034syf6vixrl2ccyk5w2vscpgiddf00a2pwq461vc95plxy2sqyn") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi"))))))

(define-public crate-sawp-0.12.0 (c (n "sawp") (v "0.12.0") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "sawp-ffi") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "18f642cjyb6g2525z7fdf27m1ywzi90mx408djh4cnzfhqn11gl4") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi")))) (r "1.58.1")))

(define-public crate-sawp-0.12.1 (c (n "sawp") (v "0.12.1") (d (list (d (n "cbindgen") (r "^0.15") (o #t) (d #t) (k 1)) (d (n "clap") (r "~2.33") (d #t) (k 2)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "csv") (r "=1.1.6") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "rayon") (r "=1.6.1") (d #t) (k 2)) (d (n "rayon-core") (r "=1.10.2") (d #t) (k 2)) (d (n "sawp-ffi") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0c1yz0xrszlkiwjgxhvm8z7rfp179m4rls1glz5sy834fd6zhx3y") (f (quote (("verbose") ("ffi" "cbindgen" "sawp-ffi")))) (r "1.58.1")))

