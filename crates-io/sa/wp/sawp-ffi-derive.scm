(define-module (crates-io sa wp sawp-ffi-derive) #:use-module (crates-io))

(define-public crate-sawp-ffi-derive-0.1.1 (c (n "sawp-ffi-derive") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lg0zs7jjvr05vkj43cwhb0hymyb79bx8va1rm8griapd6brp818")))

(define-public crate-sawp-ffi-derive-0.2.0 (c (n "sawp-ffi-derive") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03mg4rk9554vxskjhwcllfjx0qrrpwsji6nmscvwbvs3avjv6hhs")))

(define-public crate-sawp-ffi-derive-0.3.0 (c (n "sawp-ffi-derive") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.3.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17049bnhydyny4kxym9c524m21p5fda248705dq2f12z61bc5s8v")))

(define-public crate-sawp-ffi-derive-0.4.0 (c (n "sawp-ffi-derive") (v "0.4.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.4.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s24k721vnx13k0vn82pgzkp18vskvqbxgnym45c195bsf77gswm")))

(define-public crate-sawp-ffi-derive-0.5.0 (c (n "sawp-ffi-derive") (v "0.5.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n7i0jsffpbkhy99hhh1lpj37s1a6in5425hbzw1fq4pf1n26g5w")))

(define-public crate-sawp-ffi-derive-0.6.0 (c (n "sawp-ffi-derive") (v "0.6.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.6.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y32gzabixmr5an8470pgsb1gk0xhdzf72vm5azwbj2m5iywd0lj")))

(define-public crate-sawp-ffi-derive-0.7.0 (c (n "sawp-ffi-derive") (v "0.7.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1garc2xc5jnrbiv1jhjm9l71m423ici8gq3fpriy7f1lr040sy8q")))

(define-public crate-sawp-ffi-derive-0.8.0 (c (n "sawp-ffi-derive") (v "0.8.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.8.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v9zqahxnw3kg06sayp3f4aszxslgzsrz161652p0953a7w61x3s")))

(define-public crate-sawp-ffi-derive-0.9.0 (c (n "sawp-ffi-derive") (v "0.9.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.9.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lc95hdd75w4qc9rnwxskm1735kbca748k0k33vry5yfv04n0q71")))

(define-public crate-sawp-ffi-derive-0.10.0 (c (n "sawp-ffi-derive") (v "0.10.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.10.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "002dbgxicl3s053rj17dqm1xcms87h38v63lbxa4cfd48b1vk0p3")))

(define-public crate-sawp-ffi-derive-0.11.0 (c (n "sawp-ffi-derive") (v "0.11.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.11.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qcax4gpx8qr0x6q09zkm5mh0hjy5lshiqqkix0acakyg4av57wf")))

(define-public crate-sawp-ffi-derive-0.11.1 (c (n "sawp-ffi-derive") (v "0.11.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.11.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a1h9sy53wrkir9zd0af3fiayy5hfs5l7s8sdp6vj6ai4rq4p4dv")))

(define-public crate-sawp-ffi-derive-0.12.0 (c (n "sawp-ffi-derive") (v "0.12.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.0") (d #t) (k 2)))) (h "14n2026inynx1ls89vkjrjmcw63pyr5ldxrjbrch8dl7712jl4dh")))

(define-public crate-sawp-ffi-derive-0.12.1 (c (n "sawp-ffi-derive") (v "0.12.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.1") (d #t) (k 2)))) (h "156ivpy09zq81bbxc8dia6awr5vdh2cjmrxdl8ini3fd6mrvh3lv")))

