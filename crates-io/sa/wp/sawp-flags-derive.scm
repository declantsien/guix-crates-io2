(define-module (crates-io sa wp sawp-flags-derive) #:use-module (crates-io))

(define-public crate-sawp-flags-derive-0.3.0 (c (n "sawp-flags-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rcwhlf7mj7mjy3ya4241b8d198ddj7xqx7dnyd0x01ni09j97j7")))

(define-public crate-sawp-flags-derive-0.4.0 (c (n "sawp-flags-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12hz4jp836k4avxfqjqv36q6m90986akykaybqlwr2y880qm9127")))

(define-public crate-sawp-flags-derive-0.5.0 (c (n "sawp-flags-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17kmhxvsxirr6brqdlrwlpk87wka7c4namhnpz88ybxj2gq8savg")))

(define-public crate-sawp-flags-derive-0.6.0 (c (n "sawp-flags-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19znp043ckbb2gix2nwcc09yd901rcwc1dzmwa85ksh0c6r62xlf")))

(define-public crate-sawp-flags-derive-0.7.0 (c (n "sawp-flags-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lab1achqsyyzwa96j2jnw2cngw7wm0mhxz377w52l6mc7ik5jc6")))

(define-public crate-sawp-flags-derive-0.8.0 (c (n "sawp-flags-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02nb260xw06dhj50lxjjyn8nffs873nzbjdzlqkg342mla9p9pi8")))

(define-public crate-sawp-flags-derive-0.9.0 (c (n "sawp-flags-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ixis5g4r294nmgjjw9754cc014cs3lgqbz413sgwhxgih212cvl")))

(define-public crate-sawp-flags-derive-0.10.0 (c (n "sawp-flags-derive") (v "0.10.0") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "012wgd2y6bs1x6rqnkbwh8n1bciqi3xlr180rp37mvwcj74sfa55")))

(define-public crate-sawp-flags-derive-0.11.0 (c (n "sawp-flags-derive") (v "0.11.0") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a52rj54yxsgqrn1qfq9i0v0s7bwsz25dwah5ymk0708gxbfazb9")))

(define-public crate-sawp-flags-derive-0.11.1 (c (n "sawp-flags-derive") (v "0.11.1") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kvvwiahvadmisfmcdwv6ci4x4djlvfpxfcrj05rwx2w68gr43bp")))

(define-public crate-sawp-flags-derive-0.12.0 (c (n "sawp-flags-derive") (v "0.12.0") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wl5gf6fwzhm7wd2s852x6hp4kfnr19mnksfi9m4lh3vdkaxwj37")))

(define-public crate-sawp-flags-derive-0.12.1 (c (n "sawp-flags-derive") (v "0.12.1") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hyny9kf8jgvignbx2gsw4baghlnrsw05mkdn0xx51r8qb9qb9a9")))

