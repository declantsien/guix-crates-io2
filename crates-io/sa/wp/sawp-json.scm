(define-module (crates-io sa wp sawp-json) #:use-module (crates-io))

(define-public crate-sawp-json-0.1.1-rc.1 (c (n "sawp-json") (v "0.1.1-rc.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.1.1-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12mw859g6b9bw9w8kl1mrivz5cxhzrwqq82vaasb5kbp773srkkd")))

(define-public crate-sawp-json-0.1.1-rc.2 (c (n "sawp-json") (v "0.1.1-rc.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.1.1-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h6m14jci2gn3glwh43i6mw1nr7qv3ygjmkdj56bd2abs91qlds7")))

(define-public crate-sawp-json-0.1.1-rc.3 (c (n "sawp-json") (v "0.1.1-rc.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.1.1-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sqr1gql201n817fqgc44wkflsfg29w97sc51arjq4qpjl7jrh3k")))

(define-public crate-sawp-json-0.1.1 (c (n "sawp-json") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aq9g5icc6i1vqmi58pysdcf7ql8ssv174lnwpgh6cslffrvzxsi")))

(define-public crate-sawp-json-0.2.0 (c (n "sawp-json") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07v10sf4s79q4sm2k9j305wj2mgspj3l55v85rq47qv8jza5wmwm") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.3.0 (c (n "sawp-json") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lv507nn84v1j31kib56c84jmr178cl64f74cnxq21yaxppm4qfx") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.4.0 (c (n "sawp-json") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qv1c4axqhy6007hz10vi5hqljwmy0pp0qyq4pr3zfvzr7jyqg0c") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.5.0 (c (n "sawp-json") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "137znn90kazjrh384n4fsrgdx3rhv3h871y9ihfqqh97krsrg6np") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.6.0 (c (n "sawp-json") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "004dz0fvqhfa1m13ixwd3ly479551lmy9j90i42hq5milg079yyn") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.7.0 (c (n "sawp-json") (v "0.7.0") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10x0v02ay17qpcbp8j8n3nrbr1680qvz69sn0cwn5ikc9y9v5bl3") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.8.0 (c (n "sawp-json") (v "0.8.0") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g2633ifx0f1njgnhx4azy8vjcp8dyjbafqmd90ri8dn2wj69fzq") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.9.0 (c (n "sawp-json") (v "0.9.0") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lc1pxag3gad8fl717wfq6wkmx3ci0wya8ikyycfa231bc9fvf7h") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.10.0 (c (n "sawp-json") (v "0.10.0") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16r6zqgrcnnzpk866qxcg7fbmg9xgmb41pd27apc7jw7w3gnxrim") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.11.0 (c (n "sawp-json") (v "0.11.0") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02v18vxf8lbyzfkz61frzyz32a016f05i0dybvhbw4hbmykx3n9x") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.11.1 (c (n "sawp-json") (v "0.11.1") (d (list (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "sawp") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m9z4ay2xn85rnk83rbqmyiir1m9whl1xr80c68czq65wqd9s7lx") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.12.0 (c (n "sawp-json") (v "0.12.0") (d (list (d (n "sawp") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "15ic1zwnqcfhg44095gw324rj08cwmjgr31rmf5rp4928wm2rmq1") (f (quote (("verbose" "sawp/verbose"))))))

(define-public crate-sawp-json-0.12.1 (c (n "sawp-json") (v "0.12.1") (d (list (d (n "sawp") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "=0.3.4") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "0rcdqc9asvp47mkyxsibvigdz32ivvwdmrk261x1hlzpp1gkyix4") (f (quote (("verbose" "sawp/verbose"))))))

