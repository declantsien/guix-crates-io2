(define-module (crates-io sa wp sawp-ffi) #:use-module (crates-io))

(define-public crate-sawp-ffi-0.1.1 (c (n "sawp-ffi") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "sawp-ffi-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1sdh483f7j5vl84c8nw7c2k5xkwkpwxlg8rz7c7qy8wpifbanv1j")))

(define-public crate-sawp-ffi-0.2.0 (c (n "sawp-ffi") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "sawp-ffi-derive") (r "^0.2.0") (d #t) (k 0)))) (h "14h9lccf5h6k0rvcl1ljrw71fg4cxg0ki22ab6lbndvhsbwl49m7")))

(define-public crate-sawp-ffi-0.3.0 (c (n "sawp-ffi") (v "0.3.0") (d (list (d (n "sawp-ffi-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.3.0") (d #t) (k 0)))) (h "1nhdymd204bw14x9fdcbl518afnbk66nc83443nz4mwzsw76qmk2")))

(define-public crate-sawp-ffi-0.4.0 (c (n "sawp-ffi") (v "0.4.0") (d (list (d (n "sawp-ffi-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.4.0") (d #t) (k 0)))) (h "1qgdmj5mn34k0ah4jw3s49pjq0xsqql7fb0c1q29c1v7q35xdnbn")))

(define-public crate-sawp-ffi-0.5.0 (c (n "sawp-ffi") (v "0.5.0") (d (list (d (n "sawp-ffi-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.5.0") (d #t) (k 0)))) (h "1bl7pmgz09383scrdkpl6xn53701p4mf7dygnkm7myh13d42bnm8")))

(define-public crate-sawp-ffi-0.6.0 (c (n "sawp-ffi") (v "0.6.0") (d (list (d (n "sawp-ffi-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.6.0") (d #t) (k 0)))) (h "1gxqrd9a7r9wib4n2g9wij3f8hj31rjfsncgfx7v5qxkzdpksm0b")))

(define-public crate-sawp-ffi-0.7.0 (c (n "sawp-ffi") (v "0.7.0") (d (list (d (n "sawp-ffi-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.7.0") (d #t) (k 0)))) (h "0jxdr3wi9r15x1jr6wv8v6ns72211y0gwixm3ij0i9y4krsah8f1")))

(define-public crate-sawp-ffi-0.8.0 (c (n "sawp-ffi") (v "0.8.0") (d (list (d (n "sawp-ffi-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.8.0") (d #t) (k 0)))) (h "0i8x053dy8w3l2w9jfa8qz8br934m2s98bhk64v814xcg9arfjn0")))

(define-public crate-sawp-ffi-0.9.0 (c (n "sawp-ffi") (v "0.9.0") (d (list (d (n "sawp-ffi-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.9.0") (d #t) (k 0)))) (h "07l8x86iyfdng21pzncr1b5ydzddvb4c4bbiapigr6123d9yi5kn")))

(define-public crate-sawp-ffi-0.10.0 (c (n "sawp-ffi") (v "0.10.0") (d (list (d (n "sawp-ffi-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.10.0") (d #t) (k 0)))) (h "0drzq13zi1k28zllpc1j91pb90mplgigmqp5kliblkp658gwfvqw")))

(define-public crate-sawp-ffi-0.11.0 (c (n "sawp-ffi") (v "0.11.0") (d (list (d (n "sawp-ffi-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.11.0") (d #t) (k 0)))) (h "15ziw4mwm52wrc5a52apqynq1h1spfa2mavv1742spx7q0qapxgq")))

(define-public crate-sawp-ffi-0.11.1 (c (n "sawp-ffi") (v "0.11.1") (d (list (d (n "sawp-ffi-derive") (r "^0.11.1") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.11.1") (d #t) (k 0)))) (h "1mazf4rfvcki4nf3da29n8qmf37f3kc1g5lhm1c4jyi0isnqnycx")))

(define-public crate-sawp-ffi-0.12.0 (c (n "sawp-ffi") (v "0.12.0") (d (list (d (n "sawp-ffi-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.0") (d #t) (k 0)))) (h "1q40bmhn40za0dhj228aklrq374g7a7grffsq08gbm1zhi6vjyvd")))

(define-public crate-sawp-ffi-0.12.1 (c (n "sawp-ffi") (v "0.12.1") (d (list (d (n "sawp-ffi-derive") (r "^0.12.1") (d #t) (k 0)) (d (n "sawp-flags") (r "^0.12.1") (d #t) (k 0)))) (h "05rgdirrcmsll27yjx8qa7lyfp52gw8ny4mglwk56pnf1lf5c8il")))

