(define-module (crates-io sa wp sawp-file) #:use-module (crates-io))

(define-public crate-sawp-file-0.1.1-rc.1 (c (n "sawp-file") (v "0.1.1-rc.1") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.1.1-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "08dg69qkspslxgisqdqz9fx6g78znv7wq8mdgzc798fzy4mbhi7k")))

(define-public crate-sawp-file-0.1.1-rc.2 (c (n "sawp-file") (v "0.1.1-rc.2") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.1.1-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "1r6asjqmci1j8i17ngkzizhljimkacr4vkkb69fg014n2ph8a4ys")))

(define-public crate-sawp-file-0.1.1-rc.3 (c (n "sawp-file") (v "0.1.1-rc.3") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.1.1-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "16pmghb1m2whx1n8ckh00pvx5f7fw8qh56y11hhdcifh9mmygbjc")))

(define-public crate-sawp-file-0.1.1 (c (n "sawp-file") (v "0.1.1") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "0r3mlildyidn7jbmafzai9g3mnc38rhkxywr8bcs9vgyiccadicn")))

(define-public crate-sawp-file-0.2.0 (c (n "sawp-file") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "01pd2nkajkd6pdb271lw208agcc9gxbnc6xnk1pf2d59ih2rrhrc")))

(define-public crate-sawp-file-0.3.0 (c (n "sawp-file") (v "0.3.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "0zpa7wpwcaqckd3qn7xx8fh7ss107dhjrl01hcy27d5pkz1jsn4r")))

(define-public crate-sawp-file-0.4.0 (c (n "sawp-file") (v "0.4.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "0km4wmhcklhq9dzi1ccpxkdgxsiq4i6qf07wn7qiwc7wdb9a8438")))

(define-public crate-sawp-file-0.5.0 (c (n "sawp-file") (v "0.5.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "1pn7v4055kxqqrpqfi78v43hjkgs72lmwyzfck98fzgjvh3a1g06")))

(define-public crate-sawp-file-0.6.0 (c (n "sawp-file") (v "0.6.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "0avg12zlsifm332lfbixykpcans0sd9ap5csqjmlzphgi69yprcx")))

(define-public crate-sawp-file-0.7.0 (c (n "sawp-file") (v "0.7.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "1aa8qrim247z525azgb4ciyh9agy0fi0r79canzvas76gqailbdl")))

(define-public crate-sawp-file-0.8.0 (c (n "sawp-file") (v "0.8.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "11xhglyfdrkw4mb4jswyz24ip4q7q6g6fn785lrazw27dkblai2g")))

(define-public crate-sawp-file-0.9.0 (c (n "sawp-file") (v "0.9.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "1mhqjr3d1a51402wq57ly54cx9kxbli9w8mcpr98c3anhs1k2f04")))

(define-public crate-sawp-file-0.10.0 (c (n "sawp-file") (v "0.10.0") (d (list (d (n "rmp") (r "=0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "12kfxlkmzcbdk4b685avy8qh1pd1nndk3n1qhc3jzqfcxl94srmm")))

(define-public crate-sawp-file-0.11.0 (c (n "sawp-file") (v "0.11.0") (d (list (d (n "rmp") (r "=0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "1s3p6jk3k8i96xkzzfphqs7b5l7s52zah8xjzs952ixg7v5g9jkp")))

(define-public crate-sawp-file-0.11.1 (c (n "sawp-file") (v "0.11.1") (d (list (d (n "rmp") (r "=0.8.10") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "12rn0dyga0h2xnlq5k846ynh0560zbaf38ncmp4h55jxfk2a4fqa")))

(define-public crate-sawp-file-0.12.0 (c (n "sawp-file") (v "0.12.0") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "0p858zfkz22sizk5f94ck3mnbfmwxdsy4mqscaz4b1blybnqk3iw")))

(define-public crate-sawp-file-0.12.1 (c (n "sawp-file") (v "0.12.1") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "sawp") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 0)))) (h "17c5hb7xz5why4ikyj80xrx449hg9qvvz5xm6v8x9z4gmw4d1cbp")))

