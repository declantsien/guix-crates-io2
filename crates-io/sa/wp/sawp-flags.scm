(define-module (crates-io sa wp sawp-flags) #:use-module (crates-io))

(define-public crate-sawp-flags-0.3.0 (c (n "sawp-flags") (v "0.3.0") (d (list (d (n "sawp-flags-derive") (r "^0.3.0") (d #t) (k 0)))) (h "16vi9bjvad6nfg6vfwicf1gylf7n21jjzrfy2qgvxzx20rr0yfwd")))

(define-public crate-sawp-flags-0.4.0 (c (n "sawp-flags") (v "0.4.0") (d (list (d (n "sawp-flags-derive") (r "^0.4.0") (d #t) (k 0)))) (h "1z0d73jz9z1l3av3gl5v3yr7rgj5wppgziyd9xagic5v3cdc3sx7")))

(define-public crate-sawp-flags-0.5.0 (c (n "sawp-flags") (v "0.5.0") (d (list (d (n "sawp-flags-derive") (r "^0.5.0") (d #t) (k 0)))) (h "1xnwnvlyllidzqvhi47wb1p35wsipy4mvyi1k1ls8pyl4ymwihn3")))

(define-public crate-sawp-flags-0.6.0 (c (n "sawp-flags") (v "0.6.0") (d (list (d (n "sawp-flags-derive") (r "^0.6.0") (d #t) (k 0)))) (h "1mkp2jfhqyn16vf6mb7ajbk2fg56ih8j22m456wj1za68i93im7p")))

(define-public crate-sawp-flags-0.7.0 (c (n "sawp-flags") (v "0.7.0") (d (list (d (n "sawp-flags-derive") (r "^0.7.0") (d #t) (k 0)))) (h "02kg7g6dxx22h8pmckjw5ngiaschvdlkmq2v568yww3px2cy262h")))

(define-public crate-sawp-flags-0.8.0 (c (n "sawp-flags") (v "0.8.0") (d (list (d (n "sawp-flags-derive") (r "^0.8.0") (d #t) (k 0)))) (h "1a9nlspvixjv976chwjh91zc1njbl02ldyhbn9sinl7s7krzkrrj")))

(define-public crate-sawp-flags-0.9.0 (c (n "sawp-flags") (v "0.9.0") (d (list (d (n "sawp-flags-derive") (r "^0.9.0") (d #t) (k 0)))) (h "1s6mhjl6yjxdp26akw700av1hgds7jraq4dwirw0b0p45cc3i3z2")))

(define-public crate-sawp-flags-0.10.0 (c (n "sawp-flags") (v "0.10.0") (d (list (d (n "sawp-flags-derive") (r "^0.10.0") (d #t) (k 0)))) (h "1gpkx28jkr4mfa3g4fpwjq355qv4ch90i4yxlvj81sic10ckl3sd")))

(define-public crate-sawp-flags-0.11.0 (c (n "sawp-flags") (v "0.11.0") (d (list (d (n "sawp-flags-derive") (r "^0.11.0") (d #t) (k 0)))) (h "0jnwmn9yxbav1aqvssyw6lznzyd5z4jdrkl4shkpf7g85pn5nsad")))

(define-public crate-sawp-flags-0.11.1 (c (n "sawp-flags") (v "0.11.1") (d (list (d (n "sawp-flags-derive") (r "^0.11.1") (d #t) (k 0)))) (h "1mb82jl9h24camh14wfknfdwqm3fjipzxlp95rxxi7z4n5776d0z")))

(define-public crate-sawp-flags-0.12.0 (c (n "sawp-flags") (v "0.12.0") (d (list (d (n "sawp-flags-derive") (r "^0.12.0") (d #t) (k 0)))) (h "17dr0fm46v462yvdalag0kr9vh12g5d9zz82lh0z2ankx3gdzzpj")))

(define-public crate-sawp-flags-0.12.1 (c (n "sawp-flags") (v "0.12.1") (d (list (d (n "sawp-flags-derive") (r "^0.12.1") (d #t) (k 0)))) (h "1l6sk4kify7b9ym3ai0az4ndqlyavfrbylqysla56jr27l124aqz")))

