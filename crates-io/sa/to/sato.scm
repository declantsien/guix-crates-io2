(define-module (crates-io sa to sato) #:use-module (crates-io))

(define-public crate-sato-0.1.0 (c (n "sato") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1d6qcn3a0af8klrrfs92k7i4h97j6mfx3di44i3nn3qdn762zpqg")))

(define-public crate-sato-0.1.1 (c (n "sato") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1nj1hyd5d141dfxm4m6bmd976fgilcn482sn8mxqmc7hzh12l8qq")))

(define-public crate-sato-0.1.3 (c (n "sato") (v "0.1.3") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "05vq96bzwmyw0qsxb8m3l8iycfbl00xns1c19xjjsbkcy6x97lv5")))

(define-public crate-sato-0.1.4 (c (n "sato") (v "0.1.4") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "00awghnwd1cjwmy3119i7ajgg3ab3k00344h2cykw6g5z5zqrvk6")))

(define-public crate-sato-0.1.6 (c (n "sato") (v "0.1.6") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1w0mj6csd3qka7rfjgdyljhm5h5pai7wwgg0fh4bwvm8z80hnnkl")))

(define-public crate-sato-0.1.7 (c (n "sato") (v "0.1.7") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "16am7p86rz0qlchky45pkw6ngsdga2mlk2qxpxsjm7mfi1pakzid")))

(define-public crate-sato-0.1.8 (c (n "sato") (v "0.1.8") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "18b6rm8ibp0ynbpfpaz202hgqqklx65zqfwgynlmricbgw1yxf46")))

(define-public crate-sato-0.1.9 (c (n "sato") (v "0.1.9") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1hi3cj1zgfyzk7p88cxzybdsgx3ryr11gwqnladmhkkhxm6mpqy3")))

(define-public crate-sato-0.1.10 (c (n "sato") (v "0.1.10") (d (list (d (n "sexp") (r "^1.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1d3abq04l2bycgc9aiq0k1r01plkj5qjgnv7760ign5b4wc6qzwj")))

