(define-module (crates-io sa fi safina-async-test) #:use-module (crates-io))

(define-public crate-safina-async-test-0.1.0 (c (n "safina-async-test") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina") (r "^0.1") (d #t) (k 0)))) (h "0rv3hsh7yzc7krzwagzyiyv92yx5n50kk8g94wkjsfqj1qbvnhqx") (y #t)))

(define-public crate-safina-async-test-0.1.1 (c (n "safina-async-test") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina") (r "^0.1") (d #t) (k 0)))) (h "0988vfiwamjydhxayrxipgwvjd9g9xp8ch2vr43sfi6ixj3ri9x6") (y #t)))

(define-public crate-safina-async-test-0.1.2 (c (n "safina-async-test") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina") (r "^0.1") (d #t) (k 0)))) (h "099kzcwpaxzqysh6za0g1hvfw4zk0pqd1ng83s53wl7gkp2kcxjv") (y #t)))

(define-public crate-safina-async-test-0.1.3 (c (n "safina-async-test") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1") (d #t) (k 2)))) (h "1ixkpava78v6446cbi11xbskkfrwm05bq4c99msgw2rzsr44sn6b") (y #t)))

(define-public crate-safina-async-test-0.1.4 (c (n "safina-async-test") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.2") (d #t) (k 2)))) (h "1dfwn936risl99fi7wh07ibjcf85fw39czbblsafd2spgrlyqywq") (y #t)))

(define-public crate-safina-async-test-0.1.5 (c (n "safina-async-test") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)))) (h "1w23xfg2bslvr7nlg7p31r7wfriklnrra6441s4dzc7qhnhwp9f4") (y #t)))

(define-public crate-safina-async-test-0.1.6 (c (n "safina-async-test") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 2)))) (h "0hi7i8h3i6j6vqz057pczfd64picfm0d1wpywri66fzgl705nyp1") (y #t)))

(define-public crate-safina-async-test-0.1.7 (c (n "safina-async-test") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 2)))) (h "1wc8c43ymf621bm7429fiqpgwxcccsi454c3pikdwixsfbz6r69b") (y #t)))

(define-public crate-safina-async-test-0.1.8 (c (n "safina-async-test") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 2)))) (h "0v0yvh4dllsm3is62i8p96kiavfbihdb22hr5nq7qv25nphwzx5c") (f (quote (("once_cell" "safina-timer/once_cell")))) (y #t)))

(define-public crate-safina-async-test-0.1.9 (c (n "safina-async-test") (v "0.1.9") (d (list (d (n "safina") (r "^0.1.8") (d #t) (k 2)) (d (n "safina-async-test-core") (r "^0.1.0") (d #t) (k 0)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 0)))) (h "03msxljxf1k6wh8hqsw9665229b9g202kbbjxy2knl386bxy8w9l") (f (quote (("once_cell" "safina-timer/once_cell") ("default" "once_cell"))))))

(define-public crate-safina-async-test-0.1.10 (c (n "safina-async-test") (v "0.1.10") (d (list (d (n "safina") (r "^0.1.10") (d #t) (k 2)) (d (n "safina-async-test-core") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 0)))) (h "13hmzbcgsx8cb708jw2szzxzy3ik2m26pl1khf637cw5wjfcy6qx") (f (quote (("once_cell" "safina-timer/once_cell") ("default" "once_cell"))))))

(define-public crate-safina-async-test-0.1.11 (c (n "safina-async-test") (v "0.1.11") (d (list (d (n "safina") (r "^0.2.0") (d #t) (k 2)) (d (n "safina-async-test-core") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 0)))) (h "13q1v5p805bf142z98r9gcmwmxig1nhzd7zj09pkdfqagdgj8gkf") (f (quote (("once_cell" "safina-timer/once_cell") ("default" "once_cell"))))))

(define-public crate-safina-async-test-0.1.12 (c (n "safina-async-test") (v "0.1.12") (d (list (d (n "safina-async-test-core") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-executor") (r "^0.3.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 0)))) (h "0dgzdxgp0kawdf7054wh39b5y77cyrzbvifhngbfrgl3567swf69") (f (quote (("once_cell" "safina-timer/once_cell") ("default" "once_cell"))))))

(define-public crate-safina-async-test-0.1.13 (c (n "safina-async-test") (v "0.1.13") (d (list (d (n "safina-async-test-core") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-executor") (r "^0.3.1") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 0)))) (h "1600zkfj293snqbfkgnmgwhs7l2z5iwdzb687263f69xwbsmpcpq") (f (quote (("once_cell" "safina-timer/once_cell") ("default" "once_cell"))))))

