(define-module (crates-io sa fi safir) #:use-module (crates-io))

(define-public crate-safir-0.1.0 (c (n "safir") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0rcfmwag73i8b0fpnlfgzv6rj8dzdz7ii8h5mwrhnnxxgsmsp6ys")))

(define-public crate-safir-0.1.1 (c (n "safir") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mhycbs0j8qylw9vsc34jf348d31n2pmw4fw11j2s48qvn5y7kl0")))

(define-public crate-safir-0.2.0 (c (n "safir") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yp5sas9l261b6chxkqyz2gk9h4cjnwqahhq7d4ylmc0mpvmd7lv")))

(define-public crate-safir-0.2.1 (c (n "safir") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qghif8px8aypyj9zdi9h0qsphxhpvmh4w56f5d4fnw7103ii9fn")))

(define-public crate-safir-0.3.0 (c (n "safir") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "rubin") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ryyhb525c58j11rdwmrv44gv3zdi2632942lykdx3qkr48izr2q")))

(define-public crate-safir-0.4.0 (c (n "safir") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "rubin") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1962fgn5fn20zx72lkl36xzazf05w1spnwp1yfzk9lwpzgd9ml0f")))

(define-public crate-safir-0.5.0 (c (n "safir") (v "0.5.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "psutil") (r "^3.2.2") (o #t) (d #t) (k 0)) (d (n "rubin") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0qgps5pndmjx6gs5grzxjarnz7rfyqf3ca81ycrp7k6a3w610a9b")))

(define-public crate-safir-0.6.0 (c (n "safir") (v "0.6.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "rubin") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j9p9apd5wkgfalrydgd82rgv4w02bylsklfh6bmfc6dwg71gnnd")))

(define-public crate-safir-0.7.0 (c (n "safir") (v "0.7.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ld8smvn9asv98fqxy06lv9laawbjb82p8skvdvzdpwmfkhxpcwc")))

(define-public crate-safir-0.7.1 (c (n "safir") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17jmx4bdz4agpzjh91fzr9qx2akkph3z9808p79ayyx2ck71l05c")))

(define-public crate-safir-0.8.0 (c (n "safir") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bjvrvfphhwg3yjn5vdmm03d6cdx81wiz6mxvq1zkfrna0wcr4s9")))

(define-public crate-safir-0.8.1 (c (n "safir") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15q233y5zc36h2axbm31zjayl6c3dfk6h26l387hmaaq659hdbpz")))

(define-public crate-safir-0.9.0 (c (n "safir") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0m4ghk9plnrpiwd8psb2ic15fm5pg0w6c0jfz1nsb6qa95nfwf9f")))

