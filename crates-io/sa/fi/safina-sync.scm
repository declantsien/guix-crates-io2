(define-module (crates-io sa fi safina-sync) #:use-module (crates-io))

(define-public crate-safina-sync-0.1.0 (c (n "safina-sync") (v "0.1.0") (d (list (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.2") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.2") (d #t) (k 2)))) (h "1aabn3iybmqljc73g31qprsj48f1imdrvk606arnaafrd209xy9y")))

(define-public crate-safina-sync-0.1.1 (c (n "safina-sync") (v "0.1.1") (d (list (d (n "safina-async-test") (r "^0.1.6") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 2)))) (h "1x58nixq2prqq734f9jayijg9kfmr84c96c2j87cmc81kdggc6g1")))

(define-public crate-safina-sync-0.1.2 (c (n "safina-sync") (v "0.1.2") (d (list (d (n "safina-async-test") (r "^0.1.6") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 2)))) (h "0mrc73dldsrf10bqmjw21z60xhh5gvj4pjbyvfa4dmyrxa1pa7ng")))

(define-public crate-safina-sync-0.1.3 (c (n "safina-sync") (v "0.1.3") (d (list (d (n "safina-async-test") (r "^0.1.6") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 2)))) (h "1i61pchap7nbvkaqxhjn9a85ndvnqvnc6iy411mb93500i9mqzbv")))

(define-public crate-safina-sync-0.1.4 (c (n "safina-sync") (v "0.1.4") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 2)))) (h "10dfvqb5pr1vg9j2l2c624qv8d5lncv56ylvs40mq83l9jk7bbga")))

(define-public crate-safina-sync-0.1.5 (c (n "safina-sync") (v "0.1.5") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.7") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "07b02kql90j06pmdzy8528wy6xqf3c87y4h45602syrwcifxnr7w")))

(define-public crate-safina-sync-0.1.6 (c (n "safina-sync") (v "0.1.6") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.7") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "1ff408rikm1ysfqw0x6ra8hh098p21r99gff3mbp2dk0qa5qyqcg")))

(define-public crate-safina-sync-0.2.0 (c (n "safina-sync") (v "0.2.0") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "1bc8ch8yccm7a3308cmngxmxpkbh9bgqzzisz3lr4k3qp2dilzxg")))

(define-public crate-safina-sync-0.2.1 (c (n "safina-sync") (v "0.2.1") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "1h9d2y8jpd5hwjp390jl7bgpidhkp0qara38hrfyspmwmh4q69nz") (y #t)))

(define-public crate-safina-sync-0.2.2 (c (n "safina-sync") (v "0.2.2") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "0ws2hbv265fs2jr68acchs9vc97918mji1sgl97yffagnpshbyzw") (y #t)))

(define-public crate-safina-sync-0.2.3 (c (n "safina-sync") (v "0.2.3") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "18aqvfz0ipignj0hxms4332lz058x72x4iz5lyihpjv91zs7an3i")))

(define-public crate-safina-sync-0.2.4 (c (n "safina-sync") (v "0.2.4") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "00pvhdgc8ck8ny15kkycvlapd0df47876l68wcfmk7chp7228yiq")))

