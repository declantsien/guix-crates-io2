(define-module (crates-io sa fi safina-executor) #:use-module (crates-io))

(define-public crate-safina-executor-0.1.0 (c (n "safina-executor") (v "0.1.0") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)))) (h "0ykiq9132yvmjin22ggncy3w8ci4a9n2bpa45xqfv774ahz8pc1z")))

(define-public crate-safina-executor-0.1.1 (c (n "safina-executor") (v "0.1.1") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)))) (h "1qgc4hbak3v280lg3izlyi2k2n38fsz4yvyzjcnkdm3d71fld7p5")))

(define-public crate-safina-executor-0.1.2 (c (n "safina-executor") (v "0.1.2") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)))) (h "060x4429f3xw91d1qs6k0sdrxnrsv9109gq4gnr5jdywg5m0g8kp")))

(define-public crate-safina-executor-0.1.3 (c (n "safina-executor") (v "0.1.3") (d (list (d (n "safina-threadpool") (r "^0.1.1") (d #t) (k 0)))) (h "0zpds39p1rhaz4lcnpaiq8ykyzagczkk9x2w9jhw45zlgz4pbgxw")))

(define-public crate-safina-executor-0.1.4 (c (n "safina-executor") (v "0.1.4") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (d #t) (k 2)))) (h "1l31rs97df4bwp5hqpbdqmgbbh0b6v848rwzy44g8dmpah1gpdww")))

(define-public crate-safina-executor-0.1.5 (c (n "safina-executor") (v "0.1.5") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (d #t) (k 2)))) (h "1van7wnb2vgnphlk087j84j46avdb42k59rvf804yzwlfp2dm32i")))

(define-public crate-safina-executor-0.1.6 (c (n "safina-executor") (v "0.1.6") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "0zy0k3mp57kf909j05hl08yasd8l2n46xcyqs76yr53ww81ph7i9")))

(define-public crate-safina-executor-0.1.7 (c (n "safina-executor") (v "0.1.7") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.1.1") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "069cnha878fhm01q3dnvi8nb4kcq275d1rs4r4jf7iklk86cppgf")))

(define-public crate-safina-executor-0.2.0 (c (n "safina-executor") (v "0.2.0") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "19qh2sals57idf4jr56y9mk373h537k1bxnylc2ckllrvli1prpm")))

(define-public crate-safina-executor-0.2.1 (c (n "safina-executor") (v "0.2.1") (d (list (d (n "safina-sync") (r "^0.1.3") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.6") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "0cy74wiw0v4cjpij62r2z2js2l33kg2gwmlhlml4myy2rs8dxpgi")))

(define-public crate-safina-executor-0.3.0 (c (n "safina-executor") (v "0.3.0") (d (list (d (n "safina-sync") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "0smrsjfkhmmz04i0jhqhvz06jrriasibv3cif3nqwkpfnvai0p0g")))

(define-public crate-safina-executor-0.3.1 (c (n "safina-executor") (v "0.3.1") (d (list (d (n "safina-sync") (r "^0.2.1") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "1zz451mzj5bsiwacnnjj62046sz38z03mwxvil3crssf29l8h9cj")))

(define-public crate-safina-executor-0.3.2 (c (n "safina-executor") (v "0.3.2") (d (list (d (n "safina-sync") (r "^0.2.1") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "1rda7cpwmy2qpls0rj2l2m0xablwpk8kfvm1dcajy01i3vcvxfm8")))

(define-public crate-safina-executor-0.3.3 (c (n "safina-executor") (v "0.3.3") (d (list (d (n "safina-sync") (r "^0.2.1") (d #t) (k 0)) (d (n "safina-threadpool") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "143i6adsxakfbq39ibm10l8q0dn2l22pzzpmv7nxmx84mbm457mm")))

