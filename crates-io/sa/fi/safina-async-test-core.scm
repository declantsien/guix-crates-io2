(define-module (crates-io sa fi safina-async-test-core) #:use-module (crates-io))

(define-public crate-safina-async-test-core-0.1.0 (c (n "safina-async-test-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "12psks96wgsg9gqld7mp7fbfb1p1ir9icx9xrmhnwmf03rhm2ch1") (y #t)))

(define-public crate-safina-async-test-core-0.1.1 (c (n "safina-async-test-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "071g4sxklgcjsbdrar8g6p0hyh1aipilhm0vx92wlknng0qgsdcy")))

(define-public crate-safina-async-test-core-0.1.2 (c (n "safina-async-test-core") (v "0.1.2") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "1l2sqfw2rf1wcpkc02spka42nagli8bj7pi586sfn4w4x69p7vc7")))

(define-public crate-safina-async-test-core-0.1.3 (c (n "safina-async-test-core") (v "0.1.3") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "14h9mdw5jc2vfl3qkkknrsdb8qyxfvlv2smfmpb8r7yxj0awblsh")))

(define-public crate-safina-async-test-core-0.1.4 (c (n "safina-async-test-core") (v "0.1.4") (d (list (d (n "safe-proc-macro2") (r "^1") (d #t) (k 0)) (d (n "safe-quote") (r "^1") (d #t) (k 0)))) (h "04xrmg422a70jzwf8ji97lidkm0ijwf4ksg954icba64wl4xh861")))

