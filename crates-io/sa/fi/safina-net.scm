(define-module (crates-io sa fi safina-net) #:use-module (crates-io))

(define-public crate-safina-net-0.1.0 (c (n "safina-net") (v "0.1.0") (d (list (d (n "safina-async-test") (r "^0.1.6") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)))) (h "1477prgwj7kp3bvznmkm7y2d3n4nligglcrchfqnrvip1zhbrjqr")))

(define-public crate-safina-net-0.1.1 (c (n "safina-net") (v "0.1.1") (d (list (d (n "safina-async-test") (r "^0.1.6") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)))) (h "0bci3cv658vybq5pq98dhalllpdcbcfn6jba519kagcv8q9vi6im")))

(define-public crate-safina-net-0.1.2 (c (n "safina-net") (v "0.1.2") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.5") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "0j1c5im1cjbf9gb7vip2sjjf14dqkcw59dw3kfrs5bhkhrwwwy1d")))

(define-public crate-safina-net-0.1.3 (c (n "safina-net") (v "0.1.3") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.5") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "1siwphapwvmcjvcaqdgl3j1ggbndq4gvxz3adxnhna3r43wvpfgk")))

(define-public crate-safina-net-0.1.4 (c (n "safina-net") (v "0.1.4") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.5") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "124iv6kff67grwxz12iqb0zva8sixdhqcw9gfnj9g1xpfrn2k5hh")))

(define-public crate-safina-net-0.1.5 (c (n "safina-net") (v "0.1.5") (d (list (d (n "safina-async-test") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 0)) (d (n "safina-sync") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0.1.5") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "14y6v4hp5r4bzx72d5adanh5yy1nxn1zzj6qabc3hwc6541927gy")))

(define-public crate-safina-net-0.1.6 (c (n "safina-net") (v "0.1.6") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0.3.0") (d #t) (k 0)) (d (n "safina-sync") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "16zgywqxa20ylac7hy7wsdpvf4vx6v0w577cfkl9qjjz8g7vs2cv")))

(define-public crate-safina-net-0.1.7 (c (n "safina-net") (v "0.1.7") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0.3.1") (d #t) (k 0)) (d (n "safina-sync") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "0j7f19c569phynh5zig62gi6jv5pm3ah7myj5kxha2wlh61qx9xi")))

(define-public crate-safina-net-0.1.8 (c (n "safina-net") (v "0.1.8") (d (list (d (n "safina-async-test") (r "^0") (d #t) (k 2)) (d (n "safina-executor") (r "^0.3.1") (d #t) (k 0)) (d (n "safina-sync") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.5") (d #t) (k 0)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "1n1012b40g0xi1xlcpgbcrizs9l5bs04b6dzc003mx7kfsw6dx9l")))

