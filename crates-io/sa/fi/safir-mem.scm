(define-module (crates-io sa fi safir-mem) #:use-module (crates-io))

(define-public crate-safir-mem-0.1.0 (c (n "safir-mem") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "psutil") (r "^3.2.2") (o #t) (d #t) (k 0)) (d (n "rubin") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1sn2caw4sb2hqq72gjsq49nayvm12c6qg7hzd423jsw1lr8knh0g")))

(define-public crate-safir-mem-0.2.0 (c (n "safir-mem") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09jyyljxdqh1jhxrnlcl89af8b1yq143xglc4zd02zwnicah1mph")))

(define-public crate-safir-mem-0.2.1 (c (n "safir-mem") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "116pfas4cymj4mdsd4cj86dr82n9hh2n4vr59dy63dphwlcz5ss5")))

(define-public crate-safir-mem-0.3.1 (c (n "safir-mem") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ridlnl3y3qczkwd6ssv47q69szr5lj1ja6sbraxrmln6mkfm4q5")))

(define-public crate-safir-mem-0.3.2 (c (n "safir-mem") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "safir-core") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)))) (h "18s04p19p6snjjrffrwhf6d4hw90s5qw0zh6vs0v2zzzi0pky2rb")))

