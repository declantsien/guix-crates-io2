(define-module (crates-io sa fi safina-threadpool) #:use-module (crates-io))

(define-public crate-safina-threadpool-0.1.0 (c (n "safina-threadpool") (v "0.1.0") (h "0dxz45fjqlm97a3r063y1zq9r3z5mi375lfi71h80d2q8ix6cynl")))

(define-public crate-safina-threadpool-0.1.1 (c (n "safina-threadpool") (v "0.1.1") (h "0pfxjr2cli7kbkcz2q4vajyibm4lkgwhmqqm3xkfkhafg66x4k3a")))

(define-public crate-safina-threadpool-0.1.2 (c (n "safina-threadpool") (v "0.1.2") (d (list (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (d #t) (k 2)))) (h "043df1yw189f5hdnsziial6kc0afflps3bhqm90fi95y27rbjdgn")))

(define-public crate-safina-threadpool-0.1.3 (c (n "safina-threadpool") (v "0.1.3") (d (list (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "10rkyqbrmj4w1ghanr6wivcx118jlqr63wj4b4vxnc6q83p0fmzv")))

(define-public crate-safina-threadpool-0.1.4 (c (n "safina-threadpool") (v "0.1.4") (d (list (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "185r5kx4bp359fc890lck7ykblzp3171v9cdm72n83nl7cbih8na")))

(define-public crate-safina-threadpool-0.2.0 (c (n "safina-threadpool") (v "0.2.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.0") (d #t) (k 2)) (d (n "permit") (r "^0.1.4") (d #t) (k 2)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.7") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "14dbjmm83dzc9702myghkcz7p8iskpyvqvk959vmw9bdn3zdhrw8") (y #t)))

(define-public crate-safina-threadpool-0.2.1 (c (n "safina-threadpool") (v "0.2.1") (d (list (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "1zc2mq9z0zrfyxahnfb218sng8wbph4r4n8lzjhagaxrjpy06nfw")))

(define-public crate-safina-threadpool-0.2.2 (c (n "safina-threadpool") (v "0.2.2") (d (list (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.4") (f (quote ("once_cell"))) (d #t) (k 2)))) (h "0n0f55i9mazgxj4sh2x1k0119zrsbnqsrmpagzm7yjxwxwkb0m02") (y #t)))

(define-public crate-safina-threadpool-0.2.3 (c (n "safina-threadpool") (v "0.2.3") (d (list (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "0nvr2rdmr9w9x5z849z0nd48nhhsgxp0l4by9pcx8jmfib14q7zj") (f (quote (("testing") ("default"))))))

(define-public crate-safina-threadpool-0.2.4 (c (n "safina-threadpool") (v "0.2.4") (d (list (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)) (d (n "safina-timer") (r "^0") (d #t) (k 2)))) (h "0dhhpvlaz5krhk3dak7sw734rrhjhv602j6kzqnclz4fhay5iwpv") (f (quote (("testing") ("default"))))))

