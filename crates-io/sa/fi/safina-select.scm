(define-module (crates-io sa fi safina-select) #:use-module (crates-io))

(define-public crate-safina-select-0.1.0 (c (n "safina-select") (v "0.1.0") (d (list (d (n "safina") (r "^0.1") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.2") (d #t) (k 2)))) (h "16c8b6g5a0b6bpkmbl5rkir7gnnq8nrn0ddaaykjilrs0206n0yg")))

(define-public crate-safina-select-0.1.1 (c (n "safina-select") (v "0.1.1") (d (list (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.2") (d #t) (k 2)))) (h "0mjijjyg0jpa89sldb3pkq3xyjmv4cffr4jv9r9za1cn3mmdxpfs")))

(define-public crate-safina-select-0.1.2 (c (n "safina-select") (v "0.1.2") (d (list (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.2") (d #t) (k 2)))) (h "0515nnk158fiy622c1xjf653w1skdawfmkn5z842rh37bns836cp")))

(define-public crate-safina-select-0.1.3 (c (n "safina-select") (v "0.1.3") (d (list (d (n "safina-async-test") (r "^0.1.9") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.8") (d #t) (k 2)))) (h "1kwbnq18fyd0l0hjqdwhms9sgvcfjw1wxynawzcgnqcc1bnhlh32")))

(define-public crate-safina-select-0.1.4 (c (n "safina-select") (v "0.1.4") (d (list (d (n "safina-async-test") (r "^0.1.9") (d #t) (k 2)) (d (n "safina-timer") (r "^0.1.8") (d #t) (k 2)))) (h "0dy0h91hnj17qf3rzk95chx3jvq3ffihdnvsa5b120mrqrrz7f1p")))

