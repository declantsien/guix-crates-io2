(define-module (crates-io sa fi safina-timer) #:use-module (crates-io))

(define-public crate-safina-timer-0.1.0 (c (n "safina-timer") (v "0.1.0") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina") (r "^0.1") (d #t) (k 2)))) (h "0mpxyd0x2bjah4dp6g5rqpv186cil83f009d02bvsrf83fswxa1z")))

(define-public crate-safina-timer-0.1.1 (c (n "safina-timer") (v "0.1.1") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina") (r "^0.1") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)))) (h "066qyyz4b8n59p2kjy6cnyrfllama4za4y0ikzgmlbnrrsan4msh")))

(define-public crate-safina-timer-0.1.2 (c (n "safina-timer") (v "0.1.2") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina") (r "^0.1") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)))) (h "10qs3hw78riz5nn0sz3f9asd7kzyyffwxly3rvcbixc8q32jpz70")))

(define-public crate-safina-timer-0.1.3 (c (n "safina-timer") (v "0.1.3") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1") (d #t) (k 2)))) (h "0h8ni6j3i3dghwzirya4fs8qpw8kfip94xqldn10ypg38ar7b6qb")))

(define-public crate-safina-timer-0.1.4 (c (n "safina-timer") (v "0.1.4") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.2") (d #t) (k 2)))) (h "071wc9nlb7imnl2xr2i2dsfk5g2h4anki6x4mksckyf19pq6vi50")))

(define-public crate-safina-timer-0.1.5 (c (n "safina-timer") (v "0.1.5") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-async-test") (r "^0.1") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.2") (d #t) (k 2)))) (h "0c6q752w5finlhnzc1rf6kai4ybk4a9y2da2jvqzjfq6hbi8gsp2")))

(define-public crate-safina-timer-0.1.6 (c (n "safina-timer") (v "0.1.6") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.3") (d #t) (k 2)))) (h "00p26r0wbbzyjs4gm96889ik3xzrz2fgbi7caazjaffmhyj8qqi1")))

(define-public crate-safina-timer-0.1.7 (c (n "safina-timer") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)))) (h "0xx4r6f5b0w9js7azhvcaarrfh6ilwi0vgf7xvqvd94fikqwrdr2")))

(define-public crate-safina-timer-0.1.8 (c (n "safina-timer") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)))) (h "0zshmhq88pyixipfaz0ks58hyhpmhf8dff0yj3fz191iwlmi5bn4")))

(define-public crate-safina-timer-0.1.9 (c (n "safina-timer") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.1.4") (d #t) (k 2)))) (h "141m3632xd6wzc1y6xlqdwia2hq2kbk0m1h1xb49yslcx95ii1mg") (f (quote (("default" "once_cell"))))))

(define-public crate-safina-timer-0.1.10 (c (n "safina-timer") (v "0.1.10") (d (list (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0.2.0") (d #t) (k 2)))) (h "036x7drr04s4zzqd66j9szlri3ncar31rvmwhysj47x7864q5rha") (f (quote (("default" "once_cell"))))))

(define-public crate-safina-timer-0.1.11 (c (n "safina-timer") (v "0.1.11") (d (list (d (n "once_cell") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "safina-executor") (r "^0") (d #t) (k 2)))) (h "1pxrg6825ddw8yylrlfyq9qf8v7vjih6kmdwqisips53s5ja508h") (f (quote (("default" "once_cell"))))))

