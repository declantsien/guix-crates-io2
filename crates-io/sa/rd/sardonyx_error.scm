(define-module (crates-io sa rd sardonyx_error) #:use-module (crates-io))

(define-public crate-sardonyx_error-0.0.1 (c (n "sardonyx_error") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "1mqn81527kgxk8p04xa3qvpfq6nbsgi06cgwm8n6nm3py74z4b1m") (y #t)))

(define-public crate-sardonyx_error-0.0.3 (c (n "sardonyx_error") (v "0.0.3") (d (list (d (n "backtrace") (r "^0.3.13") (d #t) (k 0)))) (h "102lpjjnik4zfkdz4xpp5kblhqij4wbqncjifdjx38kl28dnnzji") (y #t)))

