(define-module (crates-io sa rd sardonyx_test) #:use-module (crates-io))

(define-public crate-sardonyx_test-0.0.1 (c (n "sardonyx_test") (v "0.0.1") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "sardonyx") (r "^0.0.1") (d #t) (k 0)))) (h "1rfxlvgilbvn68zw6v1pnc018dr6gyh9vvf4826ywq7rfq8fd9gd") (f (quote (("vulkan" "sardonyx/vulkan") ("nightly" "sardonyx/nightly") ("metal" "sardonyx/metal") ("empty" "sardonyx/empty") ("default") ("audio")))) (y #t)))

(define-public crate-sardonyx_test-0.0.3 (c (n "sardonyx_test") (v "0.0.3") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "sardonyx") (r ">= 0.0.1") (d #t) (k 0)))) (h "1xiyzqv3s5y4grjym1d454pn34dkr7fa95fz92v9zr9bvhpip8pz") (f (quote (("vulkan" "sardonyx/vulkan") ("nightly" "sardonyx/nightly") ("metal" "sardonyx/metal") ("empty" "sardonyx/empty") ("default") ("audio")))) (y #t)))

