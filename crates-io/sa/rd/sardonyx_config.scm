(define-module (crates-io sa rd sardonyx_config) #:use-module (crates-io))

(define-public crate-sardonyx_config-0.0.1 (c (n "sardonyx_config") (v "0.0.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "thread_profiler") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0fhwwaz5mnc17xi1gp7gnq3zl2zaxi297kj5hmb60h1p8pvalais") (f (quote (("profiler" "thread_profiler/thread_profiler") ("nightly")))) (y #t)))

(define-public crate-sardonyx_config-0.0.3 (c (n "sardonyx_config") (v "0.0.3") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "thread_profiler") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1836855770lw537771mihiigihd2x4gq2lpdh8i85kr6zrp30nhd") (f (quote (("profiler" "thread_profiler/thread_profiler") ("nightly")))) (y #t)))

