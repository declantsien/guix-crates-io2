(define-module (crates-io sa ur sauron-syntax) #:use-module (crates-io))

(define-public crate-sauron-syntax-0.1.1 (c (n "sauron-syntax") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sauron") (r "^0.25.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "sauron_vdom") (r "^0.25.0") (d #t) (k 0)))) (h "1l5hmjbq49jmamm4z367mc4dvahpwwrfgb7cfnm3222qkn9j9jbg")))

(define-public crate-sauron-syntax-0.1.2 (c (n "sauron-syntax") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sauron") (r "^0.26.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)) (d (n "sauron_vdom") (r "^0.26.0") (d #t) (k 0)))) (h "0gg38pfa8r283f7h2w60ba2aq09i67gz2942yhj5z5vx7yky9bb9")))

(define-public crate-sauron-syntax-0.1.3 (c (n "sauron-syntax") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sauron") (r "^0.27.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)))) (h "0py40l68dcnf5av5lg10vg55dsxs6zv5bx1xlj0hxw20b21284l4")))

(define-public crate-sauron-syntax-0.1.4 (c (n "sauron-syntax") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sauron") (r "^0.28") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1") (d #t) (k 0)))) (h "1sc7pcabbnsm5s9s1ajggdx7rp6i73wc7fzhmj2jjci0wv82q5iz")))

