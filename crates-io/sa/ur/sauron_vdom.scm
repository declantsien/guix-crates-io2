(define-module (crates-io sa ur sauron_vdom) #:use-module (crates-io))

(define-public crate-sauron_vdom-0.1.0 (c (n "sauron_vdom") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "1pr18x5ci0c2f40m2dqz0gr3cx0gqk97jc3j2jvxrd7q5kscqlj2")))

(define-public crate-sauron_vdom-0.2.0 (c (n "sauron_vdom") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "0hqlg26kxgvqfn1wynws46q5vkqqvws6akm6b9kshkp8ifys544m")))

(define-public crate-sauron_vdom-0.3.0 (c (n "sauron_vdom") (v "0.3.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "0cqj97fyb8zd68wa1p2rnwycwv93s71qi35n3y9lr7mczp936l33")))

(define-public crate-sauron_vdom-0.4.0 (c (n "sauron_vdom") (v "0.4.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "0m2ybmss50i8c71yd88zb9jgybaakphaqxbqm5kh81mcp01ga7zs")))

(define-public crate-sauron_vdom-0.5.0 (c (n "sauron_vdom") (v "0.5.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "0x1l2yp4750dnjz10iswwxij6srvl6vnx416xyp8rfr7nzdx7wrn")))

(define-public crate-sauron_vdom-0.6.0 (c (n "sauron_vdom") (v "0.6.0") (d (list (d (n "maplit") (r "^1.0.1") (d #t) (k 0)))) (h "1klnvb8fzcsy0n0lvmwqx3a8v88cds834wipyv9g11cz5gq7li23")))

(define-public crate-sauron_vdom-0.7.0 (c (n "sauron_vdom") (v "0.7.0") (h "0vsn2gy6pvchm3j601pkz5bsrg7bhsf09nyqhq3mffmm3ccpnqal")))

(define-public crate-sauron_vdom-0.7.1 (c (n "sauron_vdom") (v "0.7.1") (h "1hyl9sl3zszm2zc3644sq73rjn5ygr5iwjmnxq2scl7c4gx6iasx")))

(define-public crate-sauron_vdom-0.10.0 (c (n "sauron_vdom") (v "0.10.0") (h "19k6896gyn0c0m9zg93y7z1z4g9fqrn87k1mjm1knifa87kgfzml")))

(define-public crate-sauron_vdom-0.10.1-alpha.0 (c (n "sauron_vdom") (v "0.10.1-alpha.0") (h "1iqrkp2g0x364q2wh4ycqrwq6igng794p2jcghhynvl16s4qz8l5")))

(define-public crate-sauron_vdom-0.10.1 (c (n "sauron_vdom") (v "0.10.1") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lipdl5nqi7sj46jlhvna5nv15bqin73nnr497886yyl7535lfk7") (f (quote (("default" "serde"))))))

(define-public crate-sauron_vdom-0.10.2 (c (n "sauron_vdom") (v "0.10.2") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pqgnw1y826y5589dss8m47x245bnvayd2f1z7cxsmj5xvc2jdcv") (f (quote (("default" "serde"))))))

(define-public crate-sauron_vdom-0.11.0 (c (n "sauron_vdom") (v "0.11.0") (h "12a85mpcf6dicwjrm3v9r0bjyg52wr1qbbh1qj86hczr8lyidspy")))

(define-public crate-sauron_vdom-0.20.0 (c (n "sauron_vdom") (v "0.20.0") (h "0716r46a087kwbb1fri63a4s7iqw43jmprqrk8kwijjws2231sfz")))

(define-public crate-sauron_vdom-0.21.0 (c (n "sauron_vdom") (v "0.21.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1gjyxd93b3fxcv38wfflbmnkk8x2lgs0ym86cwv77yg3nqny37ff")))

(define-public crate-sauron_vdom-0.22.0 (c (n "sauron_vdom") (v "0.22.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0y9i7xz3bbg0i42xlalrfmafirhl9p0g1v577lh2563fvbydc3pn")))

(define-public crate-sauron_vdom-0.22.2 (c (n "sauron_vdom") (v "0.22.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0kwj5rkjyyrg01a7an8z0qcb9yxgw6ppjxky1bzxah6cyprbsl9n")))

(define-public crate-sauron_vdom-0.23.0 (c (n "sauron_vdom") (v "0.23.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "171xilvf6mwx0jyys9qb8hjpva7sjn7i9zf2p5yi3q2r7zdb79am")))

(define-public crate-sauron_vdom-0.24.0 (c (n "sauron_vdom") (v "0.24.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0znvi957681s7sj1rbfh4ba6pkcd6sfik1n5y9bksxhb1nhpr5v5")))

(define-public crate-sauron_vdom-0.25.0 (c (n "sauron_vdom") (v "0.25.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0vvy48cm3kfhkbzs69dwd0ihpgql8ssn2dsbal6i85jsp3xlxmf7")))

(define-public crate-sauron_vdom-0.25.1 (c (n "sauron_vdom") (v "0.25.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "10n6b246ncl0kj6yyzqjr6d1c6hc5ykns4jmy9whzlrvlj5a9v59")))

(define-public crate-sauron_vdom-0.26.0 (c (n "sauron_vdom") (v "0.26.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "05hvhb6rag4ghinkp97yn30n9z8976gi8il2kdi12a0r1ylix6k9")))

