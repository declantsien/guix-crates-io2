(define-module (crates-io sa ur sauron-component-macro) #:use-module (crates-io))

(define-public crate-sauron-component-macro-0.50.0 (c (n "sauron-component-macro") (v "0.50.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1zdygvzwkpx1ypznrm1bhisvwfjjzlc24jrg2nbghb2mc0d5264y")))

(define-public crate-sauron-component-macro-0.50.1 (c (n "sauron-component-macro") (v "0.50.1") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "1hrzayb0fjjqqx9jb5sp8aq4b9q3fff50bfli1s51n3d97xpbsms")))

(define-public crate-sauron-component-macro-0.50.2 (c (n "sauron-component-macro") (v "0.50.2") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "13r175zz1xa31ghryvbiyglkj9qs5969ir356laxaxgv0csd22nm")))

(define-public crate-sauron-component-macro-0.50.3 (c (n "sauron-component-macro") (v "0.50.3") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "07hyksc7nq2cg6x55w28gigz1y0fi6qc8hj2vyxq9np55iqnhg5x")))

(define-public crate-sauron-component-macro-0.50.4 (c (n "sauron-component-macro") (v "0.50.4") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "07g2rqqf179kdi8b4nir4mxnwgjgy40zbcsxbblvhcfnh6za9v66")))

(define-public crate-sauron-component-macro-0.50.5 (c (n "sauron-component-macro") (v "0.50.5") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8mrmbvdf9qbsk7lv6b1vvr817ap2rb5q5jpddijr2a17yx3k1f")))

(define-public crate-sauron-component-macro-0.50.6 (c (n "sauron-component-macro") (v "0.50.6") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)))) (h "0din1jcbd0rkzjcc2rr3c1garc0vps1fpr15vixbiwba1yxnmvnb")))

