(define-module (crates-io sa ur saurus) #:use-module (crates-io))

(define-public crate-saurus-0.1.0 (c (n "saurus") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1ndas38y4fz7zk93aap7qgr92i4xwbwqgznp222pd3lzbkcvyr5x")))

(define-public crate-saurus-0.1.1 (c (n "saurus") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0dwyylwhqcwanl97awjxyxd4112840vhsqa9dwlx3h1gnm4l9hd8")))

(define-public crate-saurus-0.1.2 (c (n "saurus") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0x2vm1cs9prpb4xdafn3vckpcj6n31fya93lfm45zx0x0abp44ah")))

(define-public crate-saurus-0.1.3 (c (n "saurus") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1acqfnfhsn8j2f1q9fszaa6avwaj85i7jyvyrb497bmcbmvpb08a")))

