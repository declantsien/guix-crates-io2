(define-module (crates-io sa ur sauron-parse) #:use-module (crates-io))

(define-public crate-sauron-parse-0.1.1 (c (n "sauron-parse") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "sauron") (r "^0.25.0") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sjdkz0fv71hjkx2yvyxnaf7yi81dqyms0q2i6w0pdh0ay1s3yy3")))

(define-public crate-sauron-parse-0.1.2 (c (n "sauron-parse") (v "0.1.2") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "sauron") (r "^0.26.0") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00xm7503w2cgknqqk86v19d41girmppf22z5bpnlizjf20ashbkc")))

(define-public crate-sauron-parse-0.1.3 (c (n "sauron-parse") (v "0.1.3") (d (list (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1.0") (d #t) (k 0)) (d (n "sauron") (r "^0.27.0") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1crzb8hd17739n5jg578fgy925zqzi3vcpnkknfw50b5pbdxnn3m")))

(define-public crate-sauron-parse-0.1.4 (c (n "sauron-parse") (v "0.1.4") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron") (r "^0.28") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05cwxdlppq8qb5h9qavdhx1ilqs00bdsvnnvyy5bqibgg36bnlh4")))

(define-public crate-sauron-parse-0.1.5 (c (n "sauron-parse") (v "0.1.5") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.30") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09frdqnxfn0qyz9dpvcp7hn4dsbpj8vbdjisj7b1f89m54f40slm")))

(define-public crate-sauron-parse-0.1.6 (c (n "sauron-parse") (v "0.1.6") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.31") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i3dk5dafhangpfdqn30flajrwbxqmqvprygyny2jbdw7kq3c5sa")))

(define-public crate-sauron-parse-0.1.7 (c (n "sauron-parse") (v "0.1.7") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.32") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l1507p5mpkajp5i2dj8dk130g8m08xihfv6rpqa8mvblc2i196p")))

(define-public crate-sauron-parse-0.1.8 (c (n "sauron-parse") (v "0.1.8") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.32") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kbmchkgsaa8p4m3wm4np93k1fcyj0sszn0mn6zkyw93crxnyc3b")))

(define-public crate-sauron-parse-0.33.0 (c (n "sauron-parse") (v "0.33.0") (d (list (d (n "html5ever") (r ">=0.25.0, <0.26.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "sauron-core") (r ">=0.33.0, <0.34.0") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "18xk1yzzxcczwfz1grn73zw093785w005ijf7qpmcv64pg19wpm7")))

(define-public crate-sauron-parse-0.34.0 (c (n "sauron-parse") (v "0.34.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.34") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qdfspyikiy9nmkmy4hackm7a8ax1wcw8dzgf2lmlk79cw3kfsjy")))

(define-public crate-sauron-parse-0.35.0 (c (n "sauron-parse") (v "0.35.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.35") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08xzc9skmbamh0j7wix04d6zcb9azzcaxnlc04qsj1mjva794m1r")))

(define-public crate-sauron-parse-0.36.0 (c (n "sauron-parse") (v "0.36.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.36") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "048xwbq204f2rfpkjndp75j10agfyj2hi9hk1bb05kr9yvbxy98r")))

(define-public crate-sauron-parse-0.37.0 (c (n "sauron-parse") (v "0.37.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.37") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m5hd6vzsqqnsn9y6cjr6nllhvj3kh44nv9qpfhsrnsknsykfw2f")))

(define-public crate-sauron-parse-0.38.0 (c (n "sauron-parse") (v "0.38.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.38") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hrmm4w6y1l31zdmq80rmpfyx565cqnnf9c67n7fnn6lp42813i5")))

(define-public crate-sauron-parse-0.39.0 (c (n "sauron-parse") (v "0.39.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "sauron-core") (r "^0.39") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "168197k0dsq7ssk626qg43cdjh7020faflb0g1ki3619jzc23wqz")))

(define-public crate-sauron-parse-0.40.0 (c (n "sauron-parse") (v "0.40.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "sauron-core") (r "^0.40") (f (quote ("with-parser"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wc5c52xg8c9gvqgnhdcj9azh9a8nlxy006bl0x28l4353z7l8jl")))

