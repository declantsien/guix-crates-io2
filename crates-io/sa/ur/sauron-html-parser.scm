(define-module (crates-io sa ur sauron-html-parser) #:use-module (crates-io))

(define-public crate-sauron-html-parser-0.60.6 (c (n "sauron-html-parser") (v "0.60.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.60") (f (quote ("with-lookup"))) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "17n6mgkvhssy7g5z1as99y1vdbfrpzk6180h2nass9g55f7s1gh7")))

(define-public crate-sauron-html-parser-0.60.7 (c (n "sauron-html-parser") (v "0.60.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.60") (f (quote ("with-lookup"))) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1q516szkavdyhbsjbvdg9wxmzxp396v9f2kva76223s57h2ac6rl")))

(define-public crate-sauron-html-parser-0.61.0 (c (n "sauron-html-parser") (v "0.61.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0vilyg06xmbsc9xgx8lj79qgbmzd7r77cb1xjyzy0gz4rbb1hynw")))

(define-public crate-sauron-html-parser-0.61.3 (c (n "sauron-html-parser") (v "0.61.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0r8hnq8ln2gzbd5hyfi9lwj7nxm68mj804sy3p8d2k0sc4j02i2q")))

(define-public crate-sauron-html-parser-0.61.4 (c (n "sauron-html-parser") (v "0.61.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0wv5abisp3jcf5fj2kmd8pr3rlj1srhgzzi0lyvgya8sqq4bjiz6")))

(define-public crate-sauron-html-parser-0.61.5 (c (n "sauron-html-parser") (v "0.61.5") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "06x512bgpw7nih85xf46mkk6q42idnh5ik6lr6l4nywhzw34i7vv")))

(define-public crate-sauron-html-parser-0.61.6 (c (n "sauron-html-parser") (v "0.61.6") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "07g5vhfj6sfz2h6j07rh8xz4h8xbdp239nx5f7mcphhhn9sy67b3")))

(define-public crate-sauron-html-parser-0.61.7 (c (n "sauron-html-parser") (v "0.61.7") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1cc2ivyl48960kvn7ml9bhqvpi161b0kvkvig5h2ya9mbxsyxqx8")))

(define-public crate-sauron-html-parser-0.61.8 (c (n "sauron-html-parser") (v "0.61.8") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rphtml") (r "^0.5.9") (d #t) (k 0)) (d (n "sauron-core") (r "^0.61") (f (quote ("with-lookup"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1iymvmw6nwnia107ca70wypxcn1111bby4x4f854mzs9i2k83r21")))

