(define-module (crates-io sa ur sauron-md) #:use-module (crates-io))

(define-public crate-sauron-md-0.1.1 (c (n "sauron-md") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "sauron") (r "^0.25.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)))) (h "0a59sqyhgpzbna941hp7j4nh1mr6q3kxr65gpzag9p91ylh8i659")))

(define-public crate-sauron-md-0.1.2 (c (n "sauron-md") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "sauron") (r "^0.26.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)))) (h "0x96cjrjvpxdcp3kf8d1jbbq6y06cq6a4m4mraz6g786dfc7pday")))

(define-public crate-sauron-md-0.1.3 (c (n "sauron-md") (v "0.1.3") (d (list (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "sauron") (r "^0.27.0") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1.0") (d #t) (k 0)))) (h "1fcbyja5ylwrxd6kgn3n5yh5aa67hcjmhb1sfnipnvm0g401bb3j")))

(define-public crate-sauron-md-0.1.4 (c (n "sauron-md") (v "0.1.4") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "sauron") (r "^0.28") (d #t) (k 0)) (d (n "sauron-parse") (r "^0.1") (d #t) (k 0)))) (h "1j3kx1q5vf1apvwxb9hwa94l0p43cmj3z0b8kzkkscqldi9dhdai")))

