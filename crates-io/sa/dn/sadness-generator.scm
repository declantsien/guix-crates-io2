(define-module (crates-io sa dn sadness-generator) #:use-module (crates-io))

(define-public crate-sadness-generator-0.1.0 (c (n "sadness-generator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cmdl1c2h1majax8n9z4wzfrh7ldl3crlvmzcghpz4s3gv1qd6lz") (r "1.59.0")))

(define-public crate-sadness-generator-0.3.0 (c (n "sadness-generator") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rhr3rjrdg6rbnkyglz65gynk67k7wi5rb5n3qh2qiaa8ipzmzsq") (r "1.59.0")))

(define-public crate-sadness-generator-0.3.1 (c (n "sadness-generator") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bmbkk2xyjb4a5dh38r12kdxhvdiskn5si0wk993jzj36kghiw4g") (r "1.59.0")))

(define-public crate-sadness-generator-0.4.0 (c (n "sadness-generator") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "173gm9gjlgi2x26ibx88q30w0nary5lgbn5yp519swqldw0rhx3w") (r "1.59.0")))

(define-public crate-sadness-generator-0.4.1 (c (n "sadness-generator") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fnn6ffpg00yhajfd3i970gk3gv06dmzi5jpscf4vkj8bnl81c40") (r "1.59.0")))

(define-public crate-sadness-generator-0.4.2 (c (n "sadness-generator") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0az7zhbgmi13zjzgiapmvrxpn2lbhbk52dkkxgrl2ni2ngh79gsl") (r "1.59.0")))

(define-public crate-sadness-generator-0.5.0 (c (n "sadness-generator") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mwj8m4n60596058ipmw4gqxvjb0wwcv8r6q69fvnknisppgbnjw") (r "1.59.0")))

