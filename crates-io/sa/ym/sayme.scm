(define-module (crates-io sa ym sayme) #:use-module (crates-io))

(define-public crate-sayme-0.1.0 (c (n "sayme") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "blocking" "rustls-tls"))) (k 0)) (d (n "serde_json") (r "^1.0.96") (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1hg0ny1wbbdbv1p0l5caymch5i9ivdm87hkckbz6rp32ihgrwhyv")))

(define-public crate-sayme-0.1.1 (c (n "sayme") (v "0.1.1") (d (list (d (n "async-openai") (r "^0.11.1") (d #t) (k 0)) (d (n "bat") (r "^0.23.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qn590d948jj3i5mdsjbdl1iy1a092cjrzwi17g1qb4a9iw9rqxd")))

(define-public crate-sayme-0.1.2 (c (n "sayme") (v "0.1.2") (d (list (d (n "async-openai") (r "^0.11.1") (d #t) (k 0)) (d (n "bat") (r "^0.23.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlixklqkny4bngvf2qg73rfqwvp8hhld9hp7z6i71asvjrj325p")))

