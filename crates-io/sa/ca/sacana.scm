(define-module (crates-io sa ca sacana) #:use-module (crates-io))

(define-public crate-sacana-1.0.0 (c (n "sacana") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "websocket") (r "^0.24.0") (d #t) (k 0)))) (h "01g2lgrxjk72ia19991i7i72446sgz1znflcglkrhsjmrwnn9336")))

