(define-module (crates-io sa ca sacabase) #:use-module (crates-io))

(define-public crate-sacabase-1.0.0 (c (n "sacabase") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.9") (d #t) (k 0)))) (h "13f4z1gs900b9shxp2wby05yvvvx4p0iz2i11sql43idwfjmdy55")))

(define-public crate-sacabase-2.0.0 (c (n "sacabase") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2.9") (d #t) (k 0)))) (h "113s4s39s043w2misq7y7ac5yyqzpkw051lh9nsqpmz3dhyzr0wq")))

