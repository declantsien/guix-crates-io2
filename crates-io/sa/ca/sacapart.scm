(define-module (crates-io sa ca sacapart) #:use-module (crates-io))

(define-public crate-sacapart-2.0.0 (c (n "sacapart") (v "2.0.0") (d (list (d (n "divsufsort") (r "^2.0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.9") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "sacabase") (r "^2.0.0") (d #t) (k 0)))) (h "0w9r099cx9ybzd7c17f3b02x5hb41ylgx16s636k00rhgwzv1l9m")))

