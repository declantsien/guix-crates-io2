(define-module (crates-io sa lu salus) #:use-module (crates-io))

(define-public crate-salus-0.0.1 (c (n "salus") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14.15") (f (quote ("server" "http1" "http2" "runtime"))) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1jv5d4iblrhkxhdbcrgfm7c43g1brlqn4pldn6y0814hfgw1fk3r")))

