(define-module (crates-io sa mu samurai) #:use-module (crates-io))

(define-public crate-samurai-0.1.0 (c (n "samurai") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "11062k5y41biyp4wnfbv5m12qmlswyyqi9x95d2v9nbk58mpicvy")))

(define-public crate-samurai-0.1.1 (c (n "samurai") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "01wy3xy4wp0zcg95qw4ng08d8iybr3cdis4hnmj62cayq17i2wdm")))

(define-public crate-samurai-0.1.2 (c (n "samurai") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "00ysnp1h553g0kfxrdclmchc9dgy5yggwz600p6pwjva5ln3dbp3")))

(define-public crate-samurai-0.1.3 (c (n "samurai") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1rqzfgmfp9vl3xldm18shh413c2gi2ijy0d4v933svlpmirvlfx1")))

(define-public crate-samurai-0.1.4 (c (n "samurai") (v "0.1.4") (d (list (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mgbvjwi9fn4v26znbpaynl22x8x05j85qwkcsimxl385nbjzvb1")))

(define-public crate-samurai-0.1.5 (c (n "samurai") (v "0.1.5") (d (list (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f0pj5d9a9i8agv7g2lx84ag7nk5nlbww8anq1c4l6z4c0xrd48z")))

