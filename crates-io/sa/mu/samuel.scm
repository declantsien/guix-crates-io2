(define-module (crates-io sa mu samuel) #:use-module (crates-io))

(define-public crate-samuel-0.1.0 (c (n "samuel") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "1q12kljdpmqmbvzzdi46m46xi5va5j154hbf26m3cys1h6d8z7sz")))

(define-public crate-samuel-0.1.1 (c (n "samuel") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "1zg0w9hr2nxwp1zbp03whf5sd4lmgax200w8w05093mhlnq38qzl")))

(define-public crate-samuel-0.1.2 (c (n "samuel") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.3") (d #t) (k 0)))) (h "0jzszid2ii7bkhs52k9wry9d555zn0psmimr1qffp70iynar6syr")))

