(define-module (crates-io sa va savan) #:use-module (crates-io))

(define-public crate-savan-0.1.0 (c (n "savan") (v "0.1.0") (d (list (d (n "clingo") (r "^0.7.2") (f (quote ("static-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02xjyh951h5wid63vh56831jy1zcivc48hwk7a4z9y01mzm32c8b")))

(define-public crate-savan-0.2.0 (c (n "savan") (v "0.2.0") (d (list (d (n "clingo") (r "^0.7.2") (f (quote ("static-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wp2qgx2i2m51803x8mz6229ys5a1s8ql31hnhi02qfsab1dhm37")))

(define-public crate-savan-0.3.0 (c (n "savan") (v "0.3.0") (d (list (d (n "clingo") (r "^0.7.2") (f (quote ("static-linking"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qal4qqxhycqgiiicfcb3fvj7s18raa1rmc85ghbqxyzm9c0hm8n")))

