(define-module (crates-io sa va savage_macros) #:use-module (crates-io))

(define-public crate-savage_macros-0.1.0 (c (n "savage_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0dpmdjsqpv49a3szy6jrc7z80v3b52z4qh088lsy3pnhjkwqglaz")))

