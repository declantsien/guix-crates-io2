(define-module (crates-io sa va savage) #:use-module (crates-io))

(define-public crate-savage-0.1.0 (c (n "savage") (v "0.1.0") (d (list (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "savage_core") (r "^0.1.0") (d #t) (k 0)))) (h "1qaygvsz3svpykld4v0n8nr3wbd7181n1hxqqrwik79if9mw99gd")))

(define-public crate-savage-0.2.0 (c (n "savage") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "savage_core") (r "^0.2.0") (d #t) (k 0)))) (h "0azgd2g350ch7qfm1b9s2f7jfd195spk9wn2i76gh8v7i8ppvdyh")))

