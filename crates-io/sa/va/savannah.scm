(define-module (crates-io sa va savannah) #:use-module (crates-io))

(define-public crate-savannah-0.1.0 (c (n "savannah") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)))) (h "00a6wy6wb836c0bm1j07bnzi0qfjg4prd7dj5lphzwx39j5bxmh5") (y #t)))

