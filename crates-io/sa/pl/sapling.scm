(define-module (crates-io sa pl sapling) #:use-module (crates-io))

(define-public crate-sapling-0.1.0 (c (n "sapling") (v "0.1.0") (h "084brkmng2z39jl0p1faranxcxf4x0f21fb13v6swlj7imqrmfw6")))

(define-public crate-sapling-0.1.1 (c (n "sapling") (v "0.1.1") (h "04lzbk395kdlb9v35gxrm125f54h7mwyi777qcq43v18h617np17")))

