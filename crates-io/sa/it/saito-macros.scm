(define-module (crates-io sa it saito-macros) #:use-module (crates-io))

(define-public crate-saito-macros-0.0.1 (c (n "saito-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qva225qynr48kvh1wk5b3pphlzj6af70jy8f76phrhbq4sv5v0y")))

(define-public crate-saito-macros-0.0.2 (c (n "saito-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v3j02997pl06jzrknkvx1l6adwpxf98lbd6lnwr7wa3gz7fafas")))

