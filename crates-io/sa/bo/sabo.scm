(define-module (crates-io sa bo sabo) #:use-module (crates-io))

(define-public crate-sabo-0.1.0 (c (n "sabo") (v "0.1.0") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "0iv2r9ys4v1m5l83zh6l5scv8wp2h243kb3j5mk6xjmj9ivjxgb7")))

(define-public crate-sabo-0.1.1 (c (n "sabo") (v "0.1.1") (d (list (d (n "glium") (r "^0.31.0") (d #t) (k 0)))) (h "0a9fa0sxr3b180wh2w1k68xc5wvjv7r304g86d7j3xc9hlg4vhgb")))

