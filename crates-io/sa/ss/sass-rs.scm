(define-module (crates-io sa ss sass-rs) #:use-module (crates-io))

(define-public crate-sass-rs-0.0.1 (c (n "sass-rs") (v "0.0.1") (d (list (d (n "sass-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1xb9lhsay8d74qxj3wgrm31q2mir1fbvczva0xxmhf2a2xa2r4q2")))

(define-public crate-sass-rs-0.0.2 (c (n "sass-rs") (v "0.0.2") (d (list (d (n "sass-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0kqbkq128flf1f9p474pid00vf3c8rxi374w1jmb1dq4mw5pqn9s")))

(define-public crate-sass-rs-0.0.3 (c (n "sass-rs") (v "0.0.3") (d (list (d (n "sass-sys") (r "^0.0.3") (d #t) (k 0)))) (h "1lvmv19avm2xdp9w0r46q74m45l4h7bify1m4ixizz1i5l6w370q")))

(define-public crate-sass-rs-0.0.4 (c (n "sass-rs") (v "0.0.4") (d (list (d (n "sass-sys") (r "^0.0.3") (d #t) (k 0)))) (h "13nhwcl7xmdj7407sr72jgrl24y9h3v16ji0q5q4gipb9xyc7dh5")))

(define-public crate-sass-rs-0.0.5 (c (n "sass-rs") (v "0.0.5") (d (list (d (n "sass-sys") (r "^0.0.3") (d #t) (k 0)))) (h "1zbkdbiplrlacmp2nm99ia8zdashmh3509pg96czmf5gf0hl72j8")))

(define-public crate-sass-rs-0.0.6 (c (n "sass-rs") (v "0.0.6") (d (list (d (n "sass-sys") (r "^0.0.3") (d #t) (k 0)))) (h "1ipsbwvz8mzljp8ii1nb83g5pkkqx01fvyp3960zjqj5d28hnh23")))

(define-public crate-sass-rs-0.0.7 (c (n "sass-rs") (v "0.0.7") (d (list (d (n "sass-sys") (r "^0.0.7") (d #t) (k 0)))) (h "103s287qziglc91xs478ak7cgkgjd206zqq4c5l7ip05zb1in6c5")))

(define-public crate-sass-rs-0.0.8 (c (n "sass-rs") (v "0.0.8") (d (list (d (n "sass-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0v25zc8x5rya8haricpz1v12v6c9jl5z484bx194d5cckdb10196")))

(define-public crate-sass-rs-0.0.9 (c (n "sass-rs") (v "0.0.9") (d (list (d (n "sass-sys") (r "^0.0.8") (d #t) (k 0)))) (h "15jssssa47xq6qgld25mpy78d164dy0aj9689b0fhhcq37xksjfp")))

(define-public crate-sass-rs-0.0.10 (c (n "sass-rs") (v "0.0.10") (d (list (d (n "sass-sys") (r "^0.0.8") (d #t) (k 0)))) (h "1lcl5bij1m5fgi8m6i74s21p6n0ddad27lird8nhpnf9zdx3ghf4")))

(define-public crate-sass-rs-0.0.11 (c (n "sass-rs") (v "0.0.11") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "1hk1py972s554pvvzqm606abyn70kfqr5jkzfyf84pivqzm51mki")))

(define-public crate-sass-rs-0.0.12 (c (n "sass-rs") (v "0.0.12") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "0y5xhhiivjgf2sw6zkaa9108ss77dqs5lryjmy7y0mcg3wfadi45")))

(define-public crate-sass-rs-0.0.13 (c (n "sass-rs") (v "0.0.13") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "06bswrb5hnpvhxclkapr1klz4ba0df2ms4xinkzgyxhmnjazlmi1")))

(define-public crate-sass-rs-0.0.14 (c (n "sass-rs") (v "0.0.14") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "1blpcffl8jkj7q3pal39m4gnb3kgrkb8068m616x0hr3qszd403y")))

(define-public crate-sass-rs-0.0.15 (c (n "sass-rs") (v "0.0.15") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "1zicvpqmpmy3wwawk729g5qjkgixm8mpqq8r2b7s6ddd0rs47sq9")))

(define-public crate-sass-rs-0.0.16 (c (n "sass-rs") (v "0.0.16") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "0d68q6wc1w340yc7dglsq2ghjzfzm5lmhig42psvmwdn84il4vff")))

(define-public crate-sass-rs-0.0.17 (c (n "sass-rs") (v "0.0.17") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "0vysc5vgzwyvgijvmdmk1z93s4f0nk9w0nmfsn5gz3834fay2lx8")))

(define-public crate-sass-rs-0.0.18 (c (n "sass-rs") (v "0.0.18") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sass-sys") (r "^0") (d #t) (k 0)))) (h "0vhskswnpa196p2pi0iv6gqdycllciry468695nlq70qdfkjwmcq")))

(define-public crate-sass-rs-0.1.0 (c (n "sass-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4") (d #t) (k 0)))) (h "14vvga1qi7hl31rsa9gpl84ixwg72n117cf7rn8j5ammwz7ab2xl")))

(define-public crate-sass-rs-0.2.0 (c (n "sass-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4") (d #t) (k 0)))) (h "0bkyjq47xlglx4xcgsn7vnz46w63kgs7phj4pyzv0hpgxvn8wj92")))

(define-public crate-sass-rs-0.2.1 (c (n "sass-rs") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4") (d #t) (k 0)))) (h "01byx9xafadjmsaiij0llggj3wmiaa3ixp6bzzzl7a2scipczy4h")))

(define-public crate-sass-rs-0.2.2 (c (n "sass-rs") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4") (d #t) (k 0)))) (h "1gba93p6y9pc5nbsx31k2kfd5ah92i0ar1qij5cz6lshwp3ggg6a")))

