(define-module (crates-io sa ss sass-sys) #:use-module (crates-io))

(define-public crate-sass-sys-0.0.1 (c (n "sass-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0dawab0sqxvqbyp9ihp3pqib1y442nvwk7h1f4n0d50gl6gv8r5k")))

(define-public crate-sass-sys-0.0.2 (c (n "sass-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15anwy3gwm43pvmxa2wrl313mkrfzpri4fck3blvagxsgzz7rxm6")))

(define-public crate-sass-sys-0.0.3 (c (n "sass-sys") (v "0.0.3") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "04smvd5yc3a4l8qkywly6910v21r4sdkgl7azpw577c004bkihib")))

(define-public crate-sass-sys-0.0.4 (c (n "sass-sys") (v "0.0.4") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "19f32ahfbpazwa8apmyapw68kyj87v3r5r6nds8niq7fcgbsfkxq")))

(define-public crate-sass-sys-0.0.5 (c (n "sass-sys") (v "0.0.5") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "13pdcv4fxs7rbldnsih5gm1f221a2rbx6f3hf7sm4a4h2r84v1l7")))

(define-public crate-sass-sys-0.0.7 (c (n "sass-sys") (v "0.0.7") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15zfbd1cl1cgdq1r37ijihrn32qnbvii05z05gy7nx05lls86sdh")))

(define-public crate-sass-sys-0.0.8 (c (n "sass-sys") (v "0.0.8") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "04iy07p63kw8rrmvcwl6kmvxj414wpgqbx27rvzw31l1v63gfaav")))

(define-public crate-sass-sys-0.0.9 (c (n "sass-sys") (v "0.0.9") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0ymd2limbngidakmxr1f59dh0fvmmwz4xb2zfj1c0iri8rhv25w2")))

(define-public crate-sass-sys-0.0.10 (c (n "sass-sys") (v "0.0.10") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "03h0x6mka7i2g9rk13bj2j0vm2ywvfgijywvvb9nzkak2rzkp9pn")))

(define-public crate-sass-sys-0.0.11 (c (n "sass-sys") (v "0.0.11") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "08mnnrnvnfizdxg1gwffbfgh3fag4flq36h5f0kyjdj5nkq17v0w")))

(define-public crate-sass-sys-0.0.12 (c (n "sass-sys") (v "0.0.12") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "12nnhfiy0cpb2pi2pr31crs4gnh89y24l3c3czxq5g5jbb6kvcvs")))

(define-public crate-sass-sys-0.0.13 (c (n "sass-sys") (v "0.0.13") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1w2f5rgg6c00c2n4p5wza0xz04ap5n0inhanir5623r8yxbnvswy")))

(define-public crate-sass-sys-0.0.14 (c (n "sass-sys") (v "0.0.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rj8j680ww5wz2h43rw5k85vzpslbssbaq98c1csky84za31hmw7")))

(define-public crate-sass-sys-0.1.0 (c (n "sass-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03icyn8d1m37sk70y65h8pn9hgmykhb6pb7hzmajpxd95jm3l94a")))

(define-public crate-sass-sys-0.2.0 (c (n "sass-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zn57sj86kf390v23p0kl8d3v3wnzpkfaxhp37x3afk64zr3j1ym")))

(define-public crate-sass-sys-0.4.0 (c (n "sass-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)) (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0nh7cj77fsqzj55va3181swgbnc5h3pdda5fax8j7ss2y5gh5rj9")))

(define-public crate-sass-sys-0.4.1 (c (n "sass-sys") (v "0.4.1") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "197hkfhxj3kf2gxxqdxzqmg1yhjqx3as59aml20adc0nrm1g9zz0")))

(define-public crate-sass-sys-0.4.2 (c (n "sass-sys") (v "0.4.2") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dvi19swy3piwpxmx8zr2mhvpbkmqv5g33250mirwvvaj2l6b7kr")))

(define-public crate-sass-sys-0.4.3 (c (n "sass-sys") (v "0.4.3") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hhp9f1h5qxcv2kykm42l496c89h3zx967vmc87fsv4hqq8wpj4b")))

(define-public crate-sass-sys-0.4.4 (c (n "sass-sys") (v "0.4.4") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0q9mmwzl2i178m9s8h0rasccccjr26bqh4mrrqpn5hx2bv9ha4d7")))

(define-public crate-sass-sys-0.4.6 (c (n "sass-sys") (v "0.4.6") (d (list (d (n "gcc") (r "^0.3.51") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1sdls9k5hl50g1m2fgzwirmmm0bd57cwyi7542cc6nazj6m8ps5s") (l "sass")))

(define-public crate-sass-sys-0.4.7 (c (n "sass-sys") (v "0.4.7") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hfdd46296q97bamfr1lri7mi1x7fl8ljn8iaaqwypjqnh1c4fhp") (l "sass")))

(define-public crate-sass-sys-0.4.8 (c (n "sass-sys") (v "0.4.8") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1paqrbmlpg2y44kczb0wxihpr4bx9lxd65kbz4g5yw8fg2wh30zq") (l "sass")))

(define-public crate-sass-sys-0.4.9 (c (n "sass-sys") (v "0.4.9") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04w35jx0fyq0q9jk52b06injkn0wl08l1dkz5myfrii41c702ri1") (l "sass")))

(define-public crate-sass-sys-0.4.10 (c (n "sass-sys") (v "0.4.10") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gc95xzhs7h86damqjd0dc1frb5k8vfj9zphahkzy637c02vwm12") (l "sass")))

(define-public crate-sass-sys-0.4.11 (c (n "sass-sys") (v "0.4.11") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11xqg8fr5cxcds1pas4wck4i6pmk68yf3n2wx6kg93zj3fwvgdbi") (l "sass")))

(define-public crate-sass-sys-0.4.12 (c (n "sass-sys") (v "0.4.12") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kx37906gf1sqj77qdni8vnq59cv11pk4bd26jqw5ca83535bs0f") (l "sass")))

(define-public crate-sass-sys-0.4.13 (c (n "sass-sys") (v "0.4.13") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wi9w85f6w4bsiqpdim634dqym93316gaxfnw9kw6nrkqabsq5kf") (l "sass")))

(define-public crate-sass-sys-0.4.14 (c (n "sass-sys") (v "0.4.14") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ih3x8f454jq1sf0vzgf4jcjnb3lcf93dmlwibl2l13vhvas6mvj") (l "sass")))

(define-public crate-sass-sys-0.4.15 (c (n "sass-sys") (v "0.4.15") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08ixx20jy0zandlnd43lc3886wvd922yxbh4fkqi7nni06anyjrh") (l "sass")))

(define-public crate-sass-sys-0.4.16 (c (n "sass-sys") (v "0.4.16") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lb84v93f95zr5py7b2n9gfmcjywfpvgmmpzhpw88vld52j1dsgd") (l "sass")))

(define-public crate-sass-sys-0.4.17 (c (n "sass-sys") (v "0.4.17") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gb4p8lmn9f3w1jswjjx1zsvbffi8bm58y78sa7ka480h71rp3zw") (l "sass")))

(define-public crate-sass-sys-0.4.18 (c (n "sass-sys") (v "0.4.18") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s8866y0f1k5wrq9ddgjxvvkqs6svxxksmya1ik9r518gzng9z9n") (l "sass")))

(define-public crate-sass-sys-0.4.19 (c (n "sass-sys") (v "0.4.19") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v926q8lasf4mi712xaqknr40fizjgacxmpmdp3fd7x1iwy4sifx") (l "sass")))

(define-public crate-sass-sys-0.4.20 (c (n "sass-sys") (v "0.4.20") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m1jby34xyghv9xjnfdbxx1l7zgy15037zq8cqcjv1yb9lcskcy3") (y #t) (l "sass")))

(define-public crate-sass-sys-0.4.21 (c (n "sass-sys") (v "0.4.21") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0229h21j22x45nqzjjdz20z1wi3cspf9904lp6cjmmmqs07sry8d") (l "sass")))

(define-public crate-sass-sys-0.4.22 (c (n "sass-sys") (v "0.4.22") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0765kxzvqsjg671nlx0kh1sgrvvdh8m76y8ynw57ydq2nhiwlfwk") (l "sass")))

