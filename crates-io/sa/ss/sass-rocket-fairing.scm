(define-module (crates-io sa ss sass-rocket-fairing) #:use-module (crates-io))

(define-public crate-sass-rocket-fairing-0.1.0 (c (n "sass-rocket-fairing") (v "0.1.0") (d (list (d (n "normpath") (r "^0.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "sass-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0x111vnbqsikp88hhni238gyk768dlcpd06y5j3pkv3jc4wqvx96") (y #t)))

(define-public crate-sass-rocket-fairing-0.2.0 (c (n "sass-rocket-fairing") (v "0.2.0") (d (list (d (n "normpath") (r "^0.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rsass") (r "^0.25.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0z931414y37j3b7hgy00rjgc0p611dpihk90ajra9z21vsjjr3nj")))

