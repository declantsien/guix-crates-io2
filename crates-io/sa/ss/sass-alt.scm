(define-module (crates-io sa ss sass-alt) #:use-module (crates-io))

(define-public crate-sass-alt-0.2.0 (c (n "sass-alt") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "079wn9n963bpwh688xwvrh9wv8749yxypfzaqbwn932rlx83x83k")))

(define-public crate-sass-alt-0.2.1 (c (n "sass-alt") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "1g2rrnd2kz10v67fdmdp2g67kwr5p5zq012w13lwfx14124mkgrq")))

(define-public crate-sass-alt-0.2.2 (c (n "sass-alt") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "1yw0r1rs3h540c6pjxb41brdyb9mswbnfwabdrnka3nhm41cwvs0")))

(define-public crate-sass-alt-0.2.3 (c (n "sass-alt") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "1kbmicd8jsqk2lbxzf4f7asrkn1099n17p1n3zq909x3wrisnvhc")))

(define-public crate-sass-alt-0.2.4 (c (n "sass-alt") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "055lias7wazjgd5cyqni530r0bm4xs9v6pl9h5a7hccqy79jhz6g")))

(define-public crate-sass-alt-0.2.5 (c (n "sass-alt") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "0nhp9cf85ykmsx8xgacds3cnm1n5lilddv2p0s57hj1jsq02mzp9")))

(define-public crate-sass-alt-0.2.6 (c (n "sass-alt") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "07x1wbpy2i2x2j5xs4rn6pz3blx4bwiqy70wxv0i4d9bsiww7mz8")))

(define-public crate-sass-alt-0.2.7 (c (n "sass-alt") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.1") (d #t) (k 0)) (d (n "sass-sys") (r "^0.4.3") (d #t) (k 0)))) (h "1vlbsk4vr4h5id3a490fslliajf13y2sd0pmjn1fnhqfhrph7agx")))

