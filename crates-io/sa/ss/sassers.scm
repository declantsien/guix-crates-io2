(define-module (crates-io sa ss sassers) #:use-module (crates-io))

(define-public crate-sassers-0.1.0-nope (c (n "sassers") (v "0.1.0-nope") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "0yfrsbb5sdf03a6q5v2aagr6sj793127zz9n3024f432ii8ar7n2")))

(define-public crate-sassers-0.2.0-notevenclose (c (n "sassers") (v "0.2.0-notevenclose") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "12qysb3cla2r86rj62bnlwaj6q3b90k56q2s361nswscp4v753bl")))

(define-public crate-sassers-0.3.0-badidea (c (n "sassers") (v "0.3.0-badidea") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "142ksa8mq36hipfyax6f4lpspfz22dgqgfkbssacybpfwygp6p2h")))

(define-public crate-sassers-0.4.0-uugghh (c (n "sassers") (v "0.4.0-uugghh") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "0fcvbxf6xlwggwplgcqk5nncram22hb9ag7vxh2r62j9yk3mbr5i")))

(define-public crate-sassers-0.5.0-blergh (c (n "sassers") (v "0.5.0-blergh") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "07xxdhr4654c3rmzp0za8x402xjg7lqrv4k0y7c5kam6fqib87qb")))

(define-public crate-sassers-0.6.0-caution (c (n "sassers") (v "0.6.0-caution") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "0q55hx6spizbhr6jp0w2vc8iw0m7vw8dmqzfinvpyxgpwlh6s4qk")))

(define-public crate-sassers-0.7.0-wompwomp (c (n "sassers") (v "0.7.0-wompwomp") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)))) (h "0fmkv620ivhd3n06my4pdgqg1924vxbq7qzyswnpy0yd9480c2dv")))

(define-public crate-sassers-0.8.0-ehhhh (c (n "sassers") (v "0.8.0-ehhhh") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "0j7sx96q9ggdw9r5gcjxfwzwki4y3n66plz2kxvinqr3cycivc5w")))

(define-public crate-sassers-0.9.0-poop (c (n "sassers") (v "0.9.0-poop") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "0hyclq4xn0vix0cavix2fs4isa5vdm9r6bi4glzhhwgcidxixzha")))

(define-public crate-sassers-0.10.0-whatnow (c (n "sassers") (v "0.10.0-whatnow") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "1ci2yxrwgvfhp0i1xs6dfyr11d9dgzfxjg1a6138m1hjh2zc4gjw")))

(define-public crate-sassers-0.11.0-shitshow (c (n "sassers") (v "0.11.0-shitshow") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "082l81qslch2y1qh7jwi1yqip3px86sbqrsbhj901nii3fwzfsc5")))

(define-public crate-sassers-0.12.0-againagain (c (n "sassers") (v "0.12.0-againagain") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "0aczynvjr9sjwqp0280fj4pygslhz593zn61vh4wcv0yc4wlpyjs")))

(define-public crate-sassers-0.13.0-now-with-more-trait-objects (c (n "sassers") (v "0.13.0-now-with-more-trait-objects") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "1aqj4jhq57aghd8gqmh41i30rjk1is2kvl9rifhc2x9vaw06xyxh")))

(define-public crate-sassers-0.13.1-more-ci (c (n "sassers") (v "0.13.1-more-ci") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "065gw8dcc626d8m85ch64ya3arjm2437cm07idiq225l01azims0")))

(define-public crate-sassers-0.13.2-everything-is-broken (c (n "sassers") (v "0.13.2-everything-is-broken") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "12dbq9yfhdjq3fdban378f0blcd8avq3ppsiiw92xwq1nzfyp8p0")))

(define-public crate-sassers-0.13.3-is-everything-broken (c (n "sassers") (v "0.13.3-is-everything-broken") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "01hiw0dqigmk19jfhrfxgz95ijrrqwsa0chpfs2qc8dp7aixi2rb")))

(define-public crate-sassers-0.13.4-maybe-not-broken (c (n "sassers") (v "0.13.4-maybe-not-broken") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "1xjm94bbk80bb14ps2s2dyzddd90svqyk24dda1vy5d2in0n831n")))

(define-public crate-sassers-0.13.5-h28 (c (n "sassers") (v "0.13.5-h28") (d (list (d (n "docopt") (r "^0.6.64") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.38") (d #t) (k 0)))) (h "0kjifq624jnii9w4mcv59zpaz58rinn1205rc69sc19p244d9kgi")))

