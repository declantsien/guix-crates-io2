(define-module (crates-io sa tu saturating_arithmetic) #:use-module (crates-io))

(define-public crate-saturating_arithmetic-0.1.0 (c (n "saturating_arithmetic") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0randbwhl93csgycb77h44i4if5x22d97r5vki5ga3idwjyng9vb")))

(define-public crate-saturating_arithmetic-0.1.1 (c (n "saturating_arithmetic") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1sg9i4n8bqinap1wgbqyvkkq2z9qnb8n9575va58fwvzf82xy5iy")))

(define-public crate-saturating_arithmetic-0.1.2 (c (n "saturating_arithmetic") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "04ais9h8m71vhcf9vn0y1ss1p9lpqkc0ihns4wn4slkqw5ix1vax")))

(define-public crate-saturating_arithmetic-0.1.3 (c (n "saturating_arithmetic") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0x5lv8ild4vgwdivwvxcqa1wd8n7wyik9n1m8lakky6mdmz43d32")))

