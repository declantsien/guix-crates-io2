(define-module (crates-io sa nt santa) #:use-module (crates-io))

(define-public crate-santa-0.1.0 (c (n "santa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("color" "derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "config") (r "^0.12") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-enum-str") (r "^0.2.4") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "tabular") (r "^0.2.0") (d #t) (k 0)))) (h "09p0fw1hpjvnp5y373y839qfdyw36z95wwrm4az04vl83krhd2nv") (r "1.56.0")))

