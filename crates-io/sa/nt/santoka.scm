(define-module (crates-io sa nt santoka) #:use-module (crates-io))

(define-public crate-santoka-1.0.0 (c (n "santoka") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "174z7nci6s18kk6sj06xhanimdqz3yzbi0k2lrzwz3g62ll3zs1m")))

(define-public crate-santoka-1.0.1 (c (n "santoka") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "07996x1xjhnd70x10bm83h2l6096gbk9mvdcx6a0pjbkg7lnscah")))

(define-public crate-santoka-1.0.2 (c (n "santoka") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0pvdih1vj6j39bjwvflq4y1dl71zfc8wkfq6f5p6gi1ykz28jspn")))

