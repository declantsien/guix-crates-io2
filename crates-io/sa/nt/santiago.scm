(define-module (crates-io sa nt santiago) #:use-module (crates-io))

(define-public crate-santiago-0.1.0 (c (n "santiago") (v "0.1.0") (h "1bqnhw1rr9bd2zcq0zsikmh4kayqm682nmsgn1ky1gi7jmc9vg1i")))

(define-public crate-santiago-0.1.1 (c (n "santiago") (v "0.1.1") (h "1znb5iv504lwfwhdk3dd6c816gaahk21zc76q49ndbisidhqj1c9")))

(define-public crate-santiago-0.2.0 (c (n "santiago") (v "0.2.0") (h "03858jbgx4hh73a8q4pjq0d5wq65pdh3kymmhsysffbh0y0prvs4")))

(define-public crate-santiago-0.3.0 (c (n "santiago") (v "0.3.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1hk11kw7ams9d08g7azz5wbix873qsmdv2mqmlz83xm6mlsy7gmw") (f (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.3.1 (c (n "santiago") (v "0.3.1") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "123b61wz1fvk654nikvmfm7abrp0c6wrjvqwk86gsjhkszd1rfk2") (f (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.4.0 (c (n "santiago") (v "0.4.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1vsnf763b7yxcgcz3ny772x6sjjcaw19x9njspn4rj5467zk2h1l") (f (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.5.0 (c (n "santiago") (v "0.5.0") (d (list (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "17zxqqmrdjmqqpyn1k1j8c9w0i6jn6873hibazlij7225f34hyq7") (f (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.6.0 (c (n "santiago") (v "0.6.0") (d (list (d (n "crate_regex") (r "^1") (o #t) (d #t) (k 0) (p "regex")))) (h "1602qin2zpy4ilx702anrmmjjyj43ncqqjbsp7icv9075nflzaan") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.7.0 (c (n "santiago") (v "0.7.0") (d (list (d (n "crate_regex") (r "^1") (o #t) (d #t) (k 0) (p "regex")))) (h "02dqbjqbfrmv5898ynqw7s41bbaa05snn5kvg6vhzq522had31sd") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.8.0 (c (n "santiago") (v "0.8.0") (d (list (d (n "crate_regex") (r "^1") (o #t) (d #t) (k 0) (p "regex")))) (h "003wy7kvqc8sgshsvvpv5jqjnln1g7vfyf9nsg79wwvvw2cakkmy") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.9.0 (c (n "santiago") (v "0.9.0") (d (list (d (n "crate_regex") (r "^1") (o #t) (d #t) (k 0) (p "regex")))) (h "13l476ghzf79sjjl6kz6p4fmj2xmm27zdpig9pns46f95q6qkyc1") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.10.0 (c (n "santiago") (v "0.10.0") (d (list (d (n "crate_regex") (r "^1") (o #t) (d #t) (k 0) (p "regex")))) (h "07s20112ylk5iarby8an94fcaissjp668vpinn0qxlpp2dgrml9y") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.11.0 (c (n "santiago") (v "0.11.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "09fwraxdsnqp801zs8l1m15yjzc9qg2rrlspyykrh62qnrn42bqz") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.12.0 (c (n "santiago") (v "0.12.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0bk1pymjvs1x6hl4imqx9605za4d72jlmjfqr91n0kxiqpf23h51") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13.0 (c (n "santiago") (v "0.13.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0vym4hm7zz9lms6q2bzh19mkh1mihg723gf0r5yjx7zd70x3a3kn") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13.1 (c (n "santiago") (v "0.13.1") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0rfz1l8qy7dbdlpizy2nhd82xazd82gmh9nq0yakw5wi2cyxsb4r") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13.2 (c (n "santiago") (v "0.13.2") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "153mzh76bb1rxjxvdnhy0hx6pv0aw4wk17wlhpw5yfak14bnhcs9") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.14.0 (c (n "santiago") (v "0.14.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1dkb02xik18n5l3k2cd6kaijnxfm9knlxwyblkf857vszd2fjvpp") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.15.0 (c (n "santiago") (v "0.15.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "04ijq1jr81lm6s824f0n0nyykdk4l3sik1zrvg5fs3hzsrf6bp3w") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.16.0 (c (n "santiago") (v "0.16.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0iz5vpr1ymgdxdv6adzr8ys095w3bhwldm88ncdrdimj5f7wnk9c") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.17.0 (c (n "santiago") (v "0.17.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0lrxniihks1yx72a1gbd6ncf5h2y94bz2gn02azqhnf4snikc8nl") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.18.0 (c (n "santiago") (v "0.18.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1l9ldk5mla98bh7wv8kxm4w6f1vnpg8h89nx56fqixxpxcww887h") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.19.0 (c (n "santiago") (v "0.19.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1q1psvn7gy9n1v5ifh5y9z66rmv2aq8crw818wp7v8gfg56kalwr") (f (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.20.0 (c (n "santiago") (v "0.20.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1fkvk7r07ghcrqlkmga6h1hi4vxm0bvwa05hf1q73dq8gq7hd6dy") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-0.21.0 (c (n "santiago") (v "0.21.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "03kfnzj4jgp7l9f04fj1l59pgy0xpyrj50p2fldyja8azvgcfikk") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-0.21.1 (c (n "santiago") (v "0.21.1") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1l3a0diqsvmrkb8vd8dc2fmkpjasg18rf3nh2hqld3178n8zwb3k") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.0.0 (c (n "santiago") (v "1.0.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0m1dj37lbkpv80l9m6ahj2m7dzx5pcdjwdf1r13zf4zw16xkabgd") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.0.1 (c (n "santiago") (v "1.0.1") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0lsk0s5cclhmkqpc8k0zz6cyyvvmwgz4kgfwq0sq0y1h4xjq2nwp") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.0.2 (c (n "santiago") (v "1.0.2") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "16djjv4snsj40kr523dbp8gbzxh7iaafv02k2qy3jlrww31ihfv8") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.1.0 (c (n "santiago") (v "1.1.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "0pdiw6scjvxc6h5p530jpycl752h057hfqsjb2aj8iny80vhwffl") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.2.0 (c (n "santiago") (v "1.2.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1ysbhwc9max6kbi8ip7g0pr7864lv5bnx55asikcwl7ifia9z0yz") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.3.0 (c (n "santiago") (v "1.3.0") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1p8w712igvfimslmir3j406nbd2mhajakf2favslnr1r4cdcdxmr") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1.3.1 (c (n "santiago") (v "1.3.1") (d (list (d (n "crate_regex") (r "^1") (f (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (o #t) (k 0) (p "regex")))) (h "1v47xcgl4k0zf4h8hiwv8pj7ad7g1x3gmgsmizmqc85wj8i04dny") (f (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

