(define-module (crates-io sa bl sablier-plugin-utils) #:use-module (crates-io))

(define-public crate-sablier-plugin-utils-1.0.0-alpha.0 (c (n "sablier-plugin-utils") (v "1.0.0-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.17.14") (d #t) (k 0)))) (h "1d1gcvmchk6wp3fywqxcird2s4hsha6pc98i1d1wlw47wjb9qkk2")))

(define-public crate-sablier-plugin-utils-1.0.0-alpha.1 (c (n "sablier-plugin-utils") (v "1.0.0-alpha.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.17.22") (d #t) (k 0)))) (h "0jj2f5cmwq6985rpjn9mhijs7hgis2k7ahmgc7zy7438bm3q8n69")))

(define-public crate-sablier-plugin-utils-1.0.0-alpha.2 (c (n "sablier-plugin-utils") (v "1.0.0-alpha.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.18.11") (d #t) (k 0)))) (h "1bpzjfdm5jgpm67v6brlvc0djnziadsn1k37hybs7if2vrmvn50s")))

(define-public crate-sablier-plugin-utils-1.0.0-alpha.3 (c (n "sablier-plugin-utils") (v "1.0.0-alpha.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "solana-geyser-plugin-interface") (r "=1.18.11") (d #t) (k 0)))) (h "1f80q9w03hiyg456f0wl100p9azlsj5x4jhwd2b50w26phb5bf7j")))

