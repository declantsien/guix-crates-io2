(define-module (crates-io sa bl sablier-webhook-program) #:use-module (crates-io))

(define-public crate-sablier-webhook-program-1.0.0-alpha.0 (c (n "sablier-webhook-program") (v "1.0.0-alpha.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "17hrjjqkgppqdaqhyiznrr745j1g85dal5g07pyv5iv71hpha59f") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sablier-webhook-program-1.0.0-alpha.1 (c (n "sablier-webhook-program") (v "1.0.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "000yl3g01jjbaa0gcf9fpgq40sjpjgc7d83winr0p7hp8hbp6lj1") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sablier-webhook-program-1.0.0-alpha.2 (c (n "sablier-webhook-program") (v "1.0.0-alpha.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10z1grh530blimjvmw5jf42k9g9d0d420ki8ny6wl02fgp2qcyn4") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-sablier-webhook-program-1.0.0-alpha.3 (c (n "sablier-webhook-program") (v "1.0.0-alpha.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (f (quote ("init-if-needed"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1njr97475amy85qr4b7319yfz3b6x8794yi9g0ijg7m3vpyz1p0w") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

