(define-module (crates-io sa bl sablier-utils) #:use-module (crates-io))

(define-public crate-sablier-utils-1.0.0-alpha.0 (c (n "sablier-utils") (v "1.0.0-alpha.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "0s2812zzpvnv64iwgvi8rryzzv69jb9ggc9j42y7jljx1jr8wjdw")))

(define-public crate-sablier-utils-1.0.0-alpha.1 (c (n "sablier-utils") (v "1.0.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "1r4c9k91dzr0sr1m9glrkng3z4qik28m7gh26gpnqjx0sazqnblw")))

(define-public crate-sablier-utils-1.0.0-alpha.2 (c (n "sablier-utils") (v "1.0.0-alpha.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "16f71mzc5wpybmqff65ypp3dk44rrlhamj5c5cs8y712d7h21j1k")))

(define-public crate-sablier-utils-1.0.0-alpha.3 (c (n "sablier-utils") (v "1.0.0-alpha.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "base64") (r "~0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "09d5wx7my0bfi0l50d12qqpdf5fi5l3a1lndj1lzm47ifhk0qgz0")))

