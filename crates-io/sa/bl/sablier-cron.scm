(define-module (crates-io sa bl sablier-cron) #:use-module (crates-io))

(define-public crate-sablier-cron-1.0.0-alpha.0 (c (n "sablier-cron") (v "1.0.0-alpha.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)))) (h "189ag87m55kg4zrw6qpzzvr9ap1ibfaw41l3wv8c6w74d4ysrbcw")))

(define-public crate-sablier-cron-1.0.0-alpha.1 (c (n "sablier-cron") (v "1.0.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)))) (h "1x3rx7ibcx60aahg7v32i9yyn77w56wcgldgfspg8p6rknncr3lv")))

(define-public crate-sablier-cron-1.0.0-alpha.2 (c (n "sablier-cron") (v "1.0.0-alpha.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)))) (h "1hxd7yyydkfx2qkifdvq8gv484c40nnlndf596h4xjbjl2gmcvkx")))

(define-public crate-sablier-cron-1.0.0-alpha.3 (c (n "sablier-cron") (v "1.0.0-alpha.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "nom") (r "~7") (d #t) (k 0)))) (h "0nv68aggc7b6xf1pgm2sp4bf59f7lqixy1l9g1j5j1ffrrl4j5s8")))

