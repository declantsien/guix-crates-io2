(define-module (crates-io sa bl sablier-relayer-api) #:use-module (crates-io))

(define-public crate-sablier-relayer-api-1.0.0-alpha.0 (c (n "sablier-relayer-api") (v "1.0.0-alpha.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.14") (d #t) (k 0)))) (h "11hrs8dn6qa90v8nzgnlhdx6wli9fillqf4bzk6x72s16sgn5hr6")))

(define-public crate-sablier-relayer-api-1.0.0-alpha.1 (c (n "sablier-relayer-api") (v "1.0.0-alpha.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.17.22") (d #t) (k 0)))) (h "0xq2h5cxb2l5k6x9n33n5jgdbd9kzlkvp0irx71xmb9lg7sn8xck")))

(define-public crate-sablier-relayer-api-1.0.0-alpha.2 (c (n "sablier-relayer-api") (v "1.0.0-alpha.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)))) (h "0bdsblr3qqqvypsn0vmh0ll7i5g6cw14d8fmkfqlggf0l0scc3cv")))

(define-public crate-sablier-relayer-api-1.0.0-alpha.3 (c (n "sablier-relayer-api") (v "1.0.0-alpha.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.18.11") (d #t) (k 0)))) (h "0yb2msr6kfjbm0dpi5csr2gdmwk9jnnr06ryvjd2cca7vgbn36as")))

