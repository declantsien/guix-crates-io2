(define-module (crates-io sa bl sablier-sdk) #:use-module (crates-io))

(define-public crate-sablier-sdk-1.0.0-alpha.0 (c (n "sablier-sdk") (v "1.0.0-alpha.0") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "sablier-thread-program") (r "=1.0.0-alpha.0") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1k5m8j1xria2j0yffslpfhmp6i1ivzp8yrzrj773i2lzpimyzg7k") (f (quote (("default"))))))

(define-public crate-sablier-sdk-1.0.0-alpha.1 (c (n "sablier-sdk") (v "1.0.0-alpha.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "sablier-thread-program") (r "=1.0.0-alpha.1") (f (quote ("cpi"))) (d #t) (k 0)))) (h "0zjp7vmrk0qgfwx694raz6cajph4b80dx1766afbnlldqw5hj7w0") (f (quote (("default"))))))

(define-public crate-sablier-sdk-1.0.0-alpha.2 (c (n "sablier-sdk") (v "1.0.0-alpha.2") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "sablier-thread-program") (r "=1.0.0-alpha.2") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1cyyy2q8j8pl3421in9ab5gfj5z7r5ygk0rpqmdk2mp8nsqjyls4") (f (quote (("default"))))))

(define-public crate-sablier-sdk-1.0.0-alpha.3 (c (n "sablier-sdk") (v "1.0.0-alpha.3") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "sablier-thread-program") (r "=1.0.0-alpha.3") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1m9cwhlv2ciqfz0qlahgw62hh84dj7cc6sdzcn7skxl4lxw295jl") (f (quote (("default"))))))

