(define-module (crates-io sa rs sars) #:use-module (crates-io))

(define-public crate-sars-0.1.0 (c (n "sars") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bio") (r "^0.41") (d #t) (k 0)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hmzn2wzwm42xffxkcvg296s125k0m756006f8485f4gas8b0xql")))

