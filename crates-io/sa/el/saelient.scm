(define-module (crates-io sa el saelient) #:use-module (crates-io))

(define-public crate-saelient-0.1.0 (c (n "saelient") (v "0.1.0") (d (list (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)))) (h "14jh3cgdki0pxq6y47fjjgy87wrdzk02l1nkkxjliaylxz39y298")))

(define-public crate-saelient-0.1.1 (c (n "saelient") (v "0.1.1") (d (list (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)))) (h "12sgk3qz2mndhjacmxs6ckdzxny1ykwdy7205r46s75bnah2kyji")))

