(define-module (crates-io sa c- sac-base) #:use-module (crates-io))

(define-public crate-sac-base-0.0.1 (c (n "sac-base") (v "0.0.1") (d (list (d (n "graphlib") (r "^0.6.1") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "heapless") (r "^0.4.1") (d #t) (k 0)))) (h "1jw9x1v9vmjy8cshhqd830262az6z0sp5zyxfnn64cwb4nhxg72f") (y #t)))

(define-public crate-sac-base-0.0.2 (c (n "sac-base") (v "0.0.2") (d (list (d (n "graphlib") (r "^0.6.1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1j21bc844jj3cj7hv7vxxrvdgqpxy1sfi3r8gm419x464x73szvn")))

(define-public crate-sac-base-0.0.3 (c (n "sac-base") (v "0.0.3") (d (list (d (n "graphlib") (r "^0.6.1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0ypw85mhg8k488b297rm9h4y1vvgalnls3sbzgv6vgw9gfavc09v")))

(define-public crate-sac-base-0.0.4 (c (n "sac-base") (v "0.0.4") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "graphlib") (r "^0.6.1") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0m76hmmzicrg322xlvfs2v57yqsb0swvyyymy7yjw658ki1zlm19")))

(define-public crate-sac-base-0.0.5 (c (n "sac-base") (v "0.0.5") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "graphlib") (r "^0.6.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)))) (h "16rk8d4xb640xpgdn2j0kslm4652arj3hzrfp9wy57cr1nz3lkrl")))

(define-public crate-sac-base-0.0.6 (c (n "sac-base") (v "0.0.6") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "graphlib") (r "^0.6.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)))) (h "1v97fg2hhv70ifayvg571ap7bsgjqrw7wn1rwkgf670zz5mrdbnl")))

(define-public crate-sac-base-0.0.7 (c (n "sac-base") (v "0.0.7") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "graphlib") (r "^0.6.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)))) (h "0fxznjfaryppzh11ngif2mbmcz8s164gfpr3mayi6q0qsl5b3y04")))

