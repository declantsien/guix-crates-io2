(define-module (crates-io sa zp sazparser) #:use-module (crates-io))

(define-public crate-sazparser-0.1.0 (c (n "sazparser") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0nx80knfg9iw3vm6ichpmii1yjmyf1cihcg5c4sfl7ymgfzwn44w")))

(define-public crate-sazparser-0.1.1 (c (n "sazparser") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1ppyfwqj6c573rl7d51w48ablylddrpl7l7mjcnh3na3bawdq6yl")))

