(define-module (crates-io sa xx saxx) #:use-module (crates-io))

(define-public crate-saxx-0.1.0 (c (n "saxx") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10d1jqfxm73s0282nyf6qa91wa4xqfy61y1haim086iw266kgmna")))

(define-public crate-saxx-0.1.1 (c (n "saxx") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ra8gri535kbkjxccga60mpnsj0gb4xn1jvjhy6akdfg62framlz")))

(define-public crate-saxx-0.1.2 (c (n "saxx") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pkn47lzvx4wg8kwr5cii1fqdwngc24qq2d2h7a8yiigscp7rfzb")))

