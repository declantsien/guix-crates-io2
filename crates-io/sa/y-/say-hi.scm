(define-module (crates-io sa y- say-hi) #:use-module (crates-io))

(define-public crate-say-hi-0.1.0 (c (n "say-hi") (v "0.1.0") (h "1fg9hz4k306hl0m8wm15bssxskhapyvvc871ph9xcazs2p6qrvwf")))

(define-public crate-say-hi-0.1.1 (c (n "say-hi") (v "0.1.1") (h "1ibaikhk4mifm7nz373s5hg7q5hvaawby9hx0b59lv189slvmx09")))

(define-public crate-say-hi-0.1.2 (c (n "say-hi") (v "0.1.2") (h "05mgdpdmrvq99ncs24zk8wzblv4cdai6m0g9mbshfyszznqhbygg")))

