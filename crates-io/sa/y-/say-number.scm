(define-module (crates-io sa y- say-number) #:use-module (crates-io))

(define-public crate-say-number-0.1.0 (c (n "say-number") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "phf") (r "^0.8") (f (quote ("macros"))) (d #t) (k 0)))) (h "0944qv99b35ljp0jz84gmcypcsamk6aa0vlrji2fpcwvdb4hv1bq")))

(define-public crate-say-number-0.1.1 (c (n "say-number") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1y7g2z3ybbcf6g7bg1a721prx9qnvakvv9zwhash47vx0h282m5c")))

(define-public crate-say-number-0.1.2 (c (n "say-number") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1rrcigmz519sk1c16xjwv7hz15sjmx1ia7lvcm2gy0zad3jwgidl")))

(define-public crate-say-number-0.1.3 (c (n "say-number") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1g3lpfk790j4z1pmk6vvyb7c2bbvpvfwah3cqy0xdc0ibm0icf49")))

(define-public crate-say-number-0.1.4 (c (n "say-number") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1pdd3grc9rqvx8v2is8bzsgh8flalsgx6a7fk44hdd4zp5dxgznl")))

(define-public crate-say-number-0.1.5 (c (n "say-number") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1lainfh2axxj5pay0kq9k0c0436hfplzdxhbzrryyqch2a974lbx")))

(define-public crate-say-number-0.1.6 (c (n "say-number") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1r7hxny4b0xcvmi2rgk3z68bxwngy9p35ap9kkx7rkfnxlc5l9ba")))

(define-public crate-say-number-1.0.0 (c (n "say-number") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1skf7lnskc4d22ay1znf8r558vmkzhmxyz400p08l21v6589f00f")))

