(define-module (crates-io sa y- say-hello-ikoafianando) #:use-module (crates-io))

(define-public crate-say-hello-ikoafianando-0.1.0 (c (n "say-hello-ikoafianando") (v "0.1.0") (h "1hs55072b7v36qd2jp8c1h5a3ns3g6fvping3ffkd0zpkpvx700f")))

(define-public crate-say-hello-ikoafianando-0.2.0 (c (n "say-hello-ikoafianando") (v "0.2.0") (h "1ifc8q0id1qg6grsni1rgdl79lkjwvfja79l1lcrbzvjshhy25lf")))

(define-public crate-say-hello-ikoafianando-0.3.0 (c (n "say-hello-ikoafianando") (v "0.3.0") (h "1d3aka9ivc6ffhby2lb2b7klq1lg0cak01yjalp5bkwihshidi62") (f (quote (("hello") ("default" "hello") ("bye") ("all" "hello" "bye"))))))

(define-public crate-say-hello-ikoafianando-0.3.1 (c (n "say-hello-ikoafianando") (v "0.3.1") (h "01qig22iaprdnmn6mzh7mn7m9wdj1kzxygnlv0asf014a505drd2") (f (quote (("word") ("hello") ("default" "hello") ("bye") ("all" "hello" "bye" "word"))))))

