(define-module (crates-io sa nc sanctity) #:use-module (crates-io))

(define-public crate-sanctity-0.1.0 (c (n "sanctity") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "1rs4zslr85l55bvcwmss3d4b7gg0yq74ndq1m2w6s3qqz8shryf4") (y #t)))

(define-public crate-sanctity-0.7.1 (c (n "sanctity") (v "0.7.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "18w4nzgy4344mpkapi97qq5ms74a0a2rdy7dqrl594ahlcqrk56v") (y #t)))

(define-public crate-sanctity-0.7.2 (c (n "sanctity") (v "0.7.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "00gxgcr12kazn4ldhq1c5bb90g8pn9lqhd2kcf2lb81vbq97v51d") (y #t)))

(define-public crate-sanctity-0.7.3 (c (n "sanctity") (v "0.7.3") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)))) (h "1fhl3398bdyfkb0z849qkjvwx3lfy7wj5hf88i260719v486p5f1") (y #t)))

(define-public crate-sanctity-1.2.0 (c (n "sanctity") (v "1.2.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "0fxvn09ps07nhlsjhc3f95zapqlma7q01l9nxv3fh0f51j8d6ly3")))

(define-public crate-sanctity-1.2.1 (c (n "sanctity") (v "1.2.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)))) (h "1znnc0rdiqrmcq8qmvjlblbzqn62mqdq18sc43w3r4ljqy8bj8pk")))

