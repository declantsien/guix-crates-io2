(define-module (crates-io sa nc sancov) #:use-module (crates-io))

(define-public crate-sancov-0.1.0 (c (n "sancov") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sancov-sys") (r "^0.1.0") (d #t) (k 0)))) (h "18qzblk131lisngl8s7rrz1j478xpimnklazpicssbxgx1p865mg") (s 2) (e (quote (("hash_increment" "dep:fxhash"))))))

