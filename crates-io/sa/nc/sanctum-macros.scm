(define-module (crates-io sa nc sanctum-macros) #:use-module (crates-io))

(define-public crate-sanctum-macros-1.0.0 (c (n "sanctum-macros") (v "1.0.0") (d (list (d (n "heck") (r ">=0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1c61prq6k76ay95gwyin8668js53qzw26v73pnpa7mg9cahfm617")))

(define-public crate-sanctum-macros-1.1.0 (c (n "sanctum-macros") (v "1.1.0") (d (list (d (n "heck") (r ">=0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0hvq2lgbhdpyidxaw7rfb11m9s69kngbx1m26vb0b0nr2lnm9274")))

(define-public crate-sanctum-macros-1.2.0 (c (n "sanctum-macros") (v "1.2.0") (d (list (d (n "heck") (r ">=0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "03gsfbmdpcx9c79mdgxgkn9x6ip2pzij7f7vmdrkbrhzcv2qyri3")))

(define-public crate-sanctum-macros-1.3.0 (c (n "sanctum-macros") (v "1.3.0") (d (list (d (n "heck") (r ">=0.4, <0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "solana-program") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k6sx3jbn8vyvr0yymh648rqgnww4q1x82lvd1ssgfzc8fsp8dw6")))

