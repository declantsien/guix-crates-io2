(define-module (crates-io sa ye sayegh_bit_parser) #:use-module (crates-io))

(define-public crate-sayegh_bit_parser-0.1.0 (c (n "sayegh_bit_parser") (v "0.1.0") (h "0ribmg24zkds4n0p0mj95b2i8h07z8y3nq9w2wkxlwh45qzdzzs1") (y #t)))

(define-public crate-sayegh_bit_parser-0.1.1 (c (n "sayegh_bit_parser") (v "0.1.1") (h "0ib8vnsknnw5m8b93kqrvrg98d7wlxy492a7p17ya9fi9mijfbd1") (y #t)))

(define-public crate-sayegh_bit_parser-0.1.2 (c (n "sayegh_bit_parser") (v "0.1.2") (h "1ah4vj6gw8nfyv2z1vvxhzahhf49clraxg86asgsy402hjx2iwir") (y #t)))

(define-public crate-sayegh_bit_parser-0.1.3 (c (n "sayegh_bit_parser") (v "0.1.3") (h "1raw3n58ay7a806vlrhx1fvarihg4shqf6zc655kyqnkqcyy1pnj")))

