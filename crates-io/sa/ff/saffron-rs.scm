(define-module (crates-io sa ff saffron-rs) #:use-module (crates-io))

(define-public crate-saffron-rs-0.1.0 (c (n "saffron-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("macros" "io-util"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "002qphhmd8pfc9wq9gk07a0f8g2qwxz03654djl7p5vwlg20kd23") (y #t)))

