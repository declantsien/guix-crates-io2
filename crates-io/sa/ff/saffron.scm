(define-module (crates-io sa ff saffron) #:use-module (crates-io))

(define-public crate-saffron-0.1.0 (c (n "saffron") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1") (k 0)))) (h "1agfnagxbahqn6bmyzqhvxwbx1rjc6qggnvyxf87bz4nhmi9myq3") (f (quote (("std") ("default"))))))

