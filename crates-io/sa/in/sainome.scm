(define-module (crates-io sa in sainome) #:use-module (crates-io))

(define-public crate-sainome-0.1.0 (c (n "sainome") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z1b7cl3rmiyr3ndvl3b145jmpkclhb5j2755x9rid0vddkp520b")))

(define-public crate-sainome-0.1.1 (c (n "sainome") (v "0.1.1") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mkhng4x1hxywzhm0y2ji0n9dxm455g0rsp6adwg0j9nknmma1ld")))

(define-public crate-sainome-0.1.2 (c (n "sainome") (v "0.1.2") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "186q6ac0qs35diixqg4glhi501rj6mxfyjn14cxdj0vl4p2s4csc")))

(define-public crate-sainome-0.1.3 (c (n "sainome") (v "0.1.3") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "006amx8v810f5b8dsf3sfpz1ycj6ip7dh2lwmg1wbpkr3w09s9l0")))

(define-public crate-sainome-0.1.4 (c (n "sainome") (v "0.1.4") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14fg3b3b062k8s7v80k5v2fm3wnzpa5dxmbmba3xh1fflvyv3h24")))

(define-public crate-sainome-0.1.5 (c (n "sainome") (v "0.1.5") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ws6r25nk8fwj79zn80bx3aj6p3qw9jkrwza792yz1vavyd8pany")))

(define-public crate-sainome-0.1.6 (c (n "sainome") (v "0.1.6") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1mk3hhnihxka56hjv6k5licad7yqp0f1vablgbp253sszdpzwf8v")))

(define-public crate-sainome-0.1.7 (c (n "sainome") (v "0.1.7") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v2m2y243zbpc1s21jniv5plrjcr9iyhfknij8bx5i7ac3fjlw7i")))

(define-public crate-sainome-0.1.8 (c (n "sainome") (v "0.1.8") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ifzp3zgc5fbzr4xzl0vjl6gkhag910y7g4ikg3fl4qn7h1h7p8m")))

(define-public crate-sainome-0.1.9 (c (n "sainome") (v "0.1.9") (d (list (d (n "peg") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0miwcm9pq1c68c8y5m6ba3wjxal7jzxc7d09q1wgp62alp9lz9nk")))

