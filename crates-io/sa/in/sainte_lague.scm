(define-module (crates-io sa in sainte_lague) #:use-module (crates-io))

(define-public crate-sainte_lague-0.1.0 (c (n "sainte_lague") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0akjnlmwlinh6v34dkr835b65mcsngja4wd8s2yaa90h7xqvbs40")))

(define-public crate-sainte_lague-0.1.1 (c (n "sainte_lague") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0mz0rlm11mpa1ix33pmpzavj885nrmn0nqv0k4rq0d8qbzz7z058")))

(define-public crate-sainte_lague-0.1.2 (c (n "sainte_lague") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bqbqbpqmb8n01738q1v0dagms84450fq0nrfqdv4z0w0gfg8wpn")))

