(define-module (crates-io sa n- san-rs) #:use-module (crates-io))

(define-public crate-san-rs-0.1.0 (c (n "san-rs") (v "0.1.0") (d (list (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "0bv3jp6pfc2nvygvv5rwvmdkmggl72526nwi14nx6lq060wwgcq9")))

(define-public crate-san-rs-0.2.0 (c (n "san-rs") (v "0.2.0") (d (list (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "1jw9lz9hsijkbicsv70k0apms9c2njkjwf1ngxqmgcrvjp7p6840")))

(define-public crate-san-rs-0.3.0 (c (n "san-rs") (v "0.3.0") (d (list (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "02r1md9kikz4pxcxwyrf102p86dj938hqga9w0j5kq7qkczz8rpg")))

(define-public crate-san-rs-0.3.1 (c (n "san-rs") (v "0.3.1") (d (list (d (n "regex") (r "^1.1.9") (d #t) (k 0)))) (h "1b7asqpscm8cfhzhgdk9s11fbznsjw3nx6v53pqwn00zwglxvyix")))

