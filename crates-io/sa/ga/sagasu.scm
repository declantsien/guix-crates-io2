(define-module (crates-io sa ga sagasu) #:use-module (crates-io))

(define-public crate-sagasu-0.1.0 (c (n "sagasu") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1v29cggr1ipyp12fwjs32sp4svwd2b2vlcfw3k4j2fwayv8xgls7")))

(define-public crate-sagasu-0.2.0 (c (n "sagasu") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1c41cbs9wlfb199mwcqm48r7v3lspm1d9vfhxdm2nj7504kgkk0g")))

(define-public crate-sagasu-0.3.0 (c (n "sagasu") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "196gv42yiracnsbfpm00cj7f37wflz1ka5gm7dcz0j58q4rpp6rc")))

(define-public crate-sagasu-0.3.1 (c (n "sagasu") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wrhw4j20j7976f1jb2jcdm2ri5gl4j7d0asllf6i1z1yrlsp9wb")))

