(define-module (crates-io sa hi sahih) #:use-module (crates-io))

(define-public crate-sahih-0.1.0 (c (n "sahih") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "openapiv3") (r "^0.5.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "14k5m1fy4zn1hmvnv3wbgkfvd8wdv653vwww2ijcxx4bzrmzns4c")))

