(define-module (crates-io sa sl sasl) #:use-module (crates-io))

(define-public crate-sasl-0.1.0 (c (n "sasl") (v "0.1.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (d #t) (k 0)))) (h "0nlb4wl67kks2vbafbnpvcwvlx7r24nmz0kargh491xfc542akvf")))

(define-public crate-sasl-0.1.1 (c (n "sasl") (v "0.1.1") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (d #t) (k 0)))) (h "0h8119v7928600mzzncnmchvk1wk4vfwijignj3q969wj2nzf6ag")))

(define-public crate-sasl-0.2.0 (c (n "sasl") (v "0.2.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (d #t) (k 0)))) (h "0mj4kyk46ps5mlipgsxbs7d7f934fqw67pjdk20x2riizwnrb3vz")))

(define-public crate-sasl-0.3.0 (c (n "sasl") (v "0.3.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (d #t) (k 0)))) (h "0q13h7ahg4p3lcrw2w3m5nwwxmln2ragqic7x0vwnd8hsbsw814v")))

(define-public crate-sasl-0.4.0 (c (n "sasl") (v "0.4.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (o #t) (d #t) (k 0)))) (h "1mcd5fici9a9r8iwjjl29wzx300j4ng8n2jchy30drylrgnaa6jz") (f (quote (("scram" "openssl") ("default" "scram"))))))

(define-public crate-sasl-0.4.1 (c (n "sasl") (v "0.4.1") (d (list (d (n "base64") (r "^0.5.0") (d #t) (k 0)) (d (n "openssl") (r "^0.9.7") (o #t) (d #t) (k 0)))) (h "09qr4djk79hp1a5ads6787ibv8l4b89cag9lzyyxwnw4rv2f4r1i") (f (quote (("scram" "openssl") ("default" "scram"))))))

(define-public crate-sasl-0.4.2 (c (n "sasl") (v "0.4.2") (d (list (d (n "base64") (r "^0.9.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.7") (o #t) (d #t) (k 0)))) (h "0nc83k2y6aqz1iix1vslsg5f7ib4f5xbi15z9yj9s1fddm1sbgad") (f (quote (("scram" "openssl") ("default" "scram"))))))

(define-public crate-sasl-0.4.3 (c (n "sasl") (v "0.4.3") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "hmac") (r "^0.7") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.3") (k 0)) (d (n "rand_os") (r "^0.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "007654lhj7d5lzdq067q9iaik71arl3982fwasnvndmphn67amz4") (f (quote (("scram") ("default" "scram"))))))

(define-public crate-sasl-0.5.0 (c (n "sasl") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hmac") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "pbkdf2") (r "^0.6") (o #t) (k 0)) (d (n "sha-1") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1j9d6q580r18i90ksr0frjks3mzll73966p2rp0vn9w90b77sbap") (f (quote (("scram" "base64" "getrandom" "sha-1" "sha2" "hmac" "pbkdf2") ("default" "scram"))))))

(define-public crate-sasl-0.5.1 (c (n "sasl") (v "0.5.1") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "hmac") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12") (o #t) (k 0)) (d (n "sha-1") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0ghlwddm0qas3g5rhfr7wnmwk47ilhj543hhicc9ca6rk0izga97") (f (quote (("scram" "base64" "getrandom" "sha-1" "sha2" "hmac" "pbkdf2") ("default" "scram" "anonymous") ("anonymous" "getrandom"))))))

