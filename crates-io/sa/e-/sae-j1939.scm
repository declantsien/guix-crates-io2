(define-module (crates-io sa e- sae-j1939) #:use-module (crates-io))

(define-public crate-sae-j1939-0.1.0 (c (n "sae-j1939") (v "0.1.0") (h "1934fw9h90c73jjyp0f5zsw44xflp1awf64dbzabw5xhhnc5jlgn")))

(define-public crate-sae-j1939-0.1.1 (c (n "sae-j1939") (v "0.1.1") (h "0qd1q9c8nz7mjigdrfsyzj4qs75wgsbg6c3kpv5gghaf28nrnjxd")))

