(define-module (crates-io sa il sailfish-macros) #:use-module (crates-io))

(define-public crate-sailfish-macros-0.0.1 (c (n "sailfish-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.0.1") (f (quote ("procmacro"))) (d #t) (k 0)))) (h "1g88axys3khr7s4bn9crhl4vdibj1a3f9k9cvhs1hsnhcgyz3v46")))

(define-public crate-sailfish-macros-0.0.3 (c (n "sailfish-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.0.3") (f (quote ("procmacro"))) (d #t) (k 0)))) (h "1w9k85rs3a44vr06n1wadr85qzyh4x0v6aflfpmr4jrciga6c2hx")))

(define-public crate-sailfish-macros-0.0.4 (c (n "sailfish-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.0.4") (f (quote ("procmacro"))) (d #t) (k 0)))) (h "1bi7vf3cji8589ccqzm52dyz01cm0n143cbzvdqgvavfkqbwnckz")))

(define-public crate-sailfish-macros-0.0.5 (c (n "sailfish-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.0.5") (f (quote ("procmacro"))) (d #t) (k 0)))) (h "1igzp99flhcp9f97g0gjrz8cy8j7ypblm5rpwzh9w4k8v40hjjkk")))

(define-public crate-sailfish-macros-0.1.0 (c (n "sailfish-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.1.0") (f (quote ("procmacro"))) (k 0)))) (h "09ym9xkvbya7ayqkq25ynkj5mvny5w97w54bilssl4h7277ykj5w") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.1.1 (c (n "sailfish-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.1.1") (f (quote ("procmacro"))) (k 0)))) (h "190hnmkkm099c5xpp1q3cj0kmfb0bkvxpbf9g60lr4l7jgms4sfk") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.1.2 (c (n "sailfish-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.1.2") (f (quote ("procmacro"))) (k 0)))) (h "1jdzvfabzabj5fvv8fjnlcxv9vf428mp3mshgv242a7rc3l0nzs4") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.1.3 (c (n "sailfish-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.1.3") (f (quote ("procmacro"))) (k 0)))) (h "1nqjmmsz1mh89528f35iv5gdhybz8xyphpqlq9zcfjgfrhbzmwva") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.2.0 (c (n "sailfish-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.2.0") (f (quote ("procmacro"))) (k 0)))) (h "099d65im0disq2llcanvdhw8205rsaij7l7qzggmkahf3x9vmfdc") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.2.1 (c (n "sailfish-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.2.1") (f (quote ("procmacro"))) (k 0)))) (h "11y7k09fc1ibkqismw58bhadd0fc55ggfgmar8nm1dcj4zwb8r8m") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.2.2 (c (n "sailfish-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.2.2") (f (quote ("procmacro"))) (k 0)))) (h "0m5r6yfccpjm7smxpn8c50a6f224n3lmiw3m71x6hc0p7j9fz2zy") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.2.3 (c (n "sailfish-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.2.3") (f (quote ("procmacro"))) (k 0)))) (h "119q59fi20851sdvlq2p5f303l6h7b9znxp0hrf4v82vf1bh9hi1") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.3.0 (c (n "sailfish-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "=0.3.0") (f (quote ("procmacro"))) (k 0)))) (h "08m1gkm880g31g6hx0bhgx0sl1zqrm1q0xnhgf5nvbln6gyv1ax4") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.3.1 (c (n "sailfish-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.3.1") (f (quote ("procmacro"))) (k 0)))) (h "1bx8d0ha5qqdbl5f275zmpwslnafbqkqqqzcqkkyrjqi67jjhxdb") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.3.2 (c (n "sailfish-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.3.2") (f (quote ("procmacro"))) (k 0)))) (h "044ybrr71k303qwbdq6hn7mx3ix39ckmqv8sligvvlg3kdn1mcwh") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.3.3 (c (n "sailfish-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.3.3") (f (quote ("procmacro"))) (k 0)))) (h "0ija3ay84a9vc7rc78hrrwhrqbyvqdkbivfjmv4i5bh7xxc29fkb") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.3.4 (c (n "sailfish-macros") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.3.4") (f (quote ("procmacro"))) (k 0)))) (h "1szx8s4ahzq393f59c5lz9p2dg3f8x3k4zi995yn7m6y5d9ys7dk") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.4.0 (c (n "sailfish-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.4.0") (f (quote ("procmacro"))) (k 0)))) (h "1cbix04nnw2w1cw099z1ckf1fm0q7dl1y1s719p322yf3lgnqcl6") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.5.0 (c (n "sailfish-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.5.0") (f (quote ("procmacro"))) (k 0)))) (h "194753lxxivgi6xrdafhdhg4a2pwvklrryn4pi3r3qf9ckhrrlyh") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.6.0 (c (n "sailfish-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.6.0") (f (quote ("procmacro"))) (k 0)))) (h "00r34s9z6nsjrqws0qvksbkqshqy3g3kgxaw24wsjnh83njs3nfs") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.6.1 (c (n "sailfish-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.6.1") (f (quote ("procmacro"))) (k 0)))) (h "1jc866sbrnb5jz19blh3vp5jpfsm3pcgh86gl5c18m7jg6d2y5j1") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.7.0 (c (n "sailfish-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.7.0") (f (quote ("procmacro"))) (k 0)))) (h "08jdcizmxcv4dz9maykaxiva52vsimkhdyvgvrna5nnni5i99rf3") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.0 (c (n "sailfish-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.0") (f (quote ("procmacro"))) (k 0)))) (h "1qib43xmcgh4ffakk638sbycwgz2xhvyv91hyikbyn8gi9mmma86") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.1 (c (n "sailfish-macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.1") (f (quote ("procmacro"))) (k 0)))) (h "1ic9202swli9mqw6chd9sas99zbaqxmanrlir2jn3y2n8jqkvxy8") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.3 (c (n "sailfish-macros") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.3") (f (quote ("procmacro"))) (k 0)))) (h "198rlrk9yglr5j8kyp335bsf93wnll2qsmljk7lk14jz1j8k2zp4") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.4-beta.0 (c (n "sailfish-macros") (v "0.8.4-beta.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.4-beta.0") (f (quote ("procmacro"))) (k 0)))) (h "02ir8s84bs7szk23ripzx3nm5b1f4a0rbgpf0y0ar04ps005pa5j") (f (quote (("minify" "sailfish-compiler/minify") ("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.4-beta.1 (c (n "sailfish-macros") (v "0.8.4-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.4-beta.1") (f (quote ("procmacro"))) (k 0)))) (h "10nsxi7a8lihfx2apjmmiz5hdymflnf34cn7jk663sjxylavaapc") (f (quote (("minify" "sailfish-compiler/minify") ("default" "config") ("config" "sailfish-compiler/config"))))))

(define-public crate-sailfish-macros-0.8.4-beta.2 (c (n "sailfish-macros") (v "0.8.4-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "sailfish-compiler") (r "^0.8.4-beta.2") (f (quote ("procmacro"))) (k 0)))) (h "12g379j19bvk2l5iff9khdhvlpa5bir0yvi6g1r5jwg6j64mds8m") (f (quote (("default" "config") ("config" "sailfish-compiler/config"))))))

