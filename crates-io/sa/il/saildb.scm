(define-module (crates-io sa il saildb) #:use-module (crates-io))

(define-public crate-saildb-0.0.1 (c (n "saildb") (v "0.0.1") (d (list (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "sia") (r "^0.0.4") (d #t) (k 0)) (d (n "srpc") (r "^0.0.2") (d #t) (k 0)))) (h "0v39p9cxgz464bhzx8hg1sz0hjcwh21450nffyxgmql37j45pldl")))

(define-public crate-saildb-0.0.2 (c (n "saildb") (v "0.0.2") (d (list (d (n "canary") (r "^0.0.6") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "srpc") (r "^0.0.3") (d #t) (k 0)))) (h "1v1mrxqff3x8dxzsqrmr6sxyxxg4ym219a6yww1cmssj1rdwc3pw")))

