(define-module (crates-io sa il sailhouse) #:use-module (crates-io))

(define-public crate-sailhouse-0.1.0 (c (n "sailhouse") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1sca5gmlc0np6dx8lp9sfhw0ymbar2dvza1b1497fjxd3njj9p3d")))

