(define-module (crates-io sa il sailboat) #:use-module (crates-io))

(define-public crate-sailboat-0.1.0 (c (n "sailboat") (v "0.1.0") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)) (d (n "sha-1") (r "^0.10.1") (d #t) (k 2)))) (h "1k4afl9dm50gv6x0bzv942p3irxmyq3lhjbjgcz60iizq46njmn0")))

(define-public crate-sailboat-0.1.0+1 (c (n "sailboat") (v "0.1.0+1") (d (list (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 2)) (d (n "sha-1") (r "^0.10.1") (d #t) (k 2)))) (h "10hz5hchg2jc3s80z55dmxzbz5rxagvzgb9clwl1xphy30h452wy")))

