(define-module (crates-io sa ml saml10e15a) #:use-module (crates-io))

(define-public crate-saml10e15a-0.2.0 (c (n "saml10e15a") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1sb5r8vx166ivaqz2igr3nvss02q29nwfh6xvkvjy0kw4qjj0lrz") (f (quote (("rt" "cortex-m-rt/device"))))))

