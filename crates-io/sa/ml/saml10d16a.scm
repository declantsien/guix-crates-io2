(define-module (crates-io sa ml saml10d16a) #:use-module (crates-io))

(define-public crate-saml10d16a-0.1.0 (c (n "saml10d16a") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11yr9iwb0492znih3plawif4v714kpw5qppp4lr4jh0mbhg1zd69") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-saml10d16a-0.1.1 (c (n "saml10d16a") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1k8fdzw20dafbrpmzhfsnjwy8lsgvnxriqp8y68q9wradixvkw61") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-saml10d16a-0.2.0 (c (n "saml10d16a") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1h37qlnwwin1ryrmm8rwhim3vdjymdy0wxawlkaqxcz1dpxm6gqc") (f (quote (("rt" "cortex-m-rt/device"))))))

