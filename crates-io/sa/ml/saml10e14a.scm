(define-module (crates-io sa ml saml10e14a) #:use-module (crates-io))

(define-public crate-saml10e14a-0.2.0 (c (n "saml10e14a") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bh8bac08sknb4zyvw2kl5s4ax0wla71fawch97nij5bjzf4vscg") (f (quote (("rt" "cortex-m-rt/device"))))))

