(define-module (crates-io sa ml saml10d15a) #:use-module (crates-io))

(define-public crate-saml10d15a-0.2.0 (c (n "saml10d15a") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zrmrgz3rs3djd2zbvjbf5za135whbk4vik58qpk3xh44w3ph5rw") (f (quote (("rt" "cortex-m-rt/device"))))))

