(define-module (crates-io sa ml saml10d14a) #:use-module (crates-io))

(define-public crate-saml10d14a-0.2.0 (c (n "saml10d14a") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03rasmh35g2wxhjr7cqxq71kj85hz31zlmw696gfbmv7ds40xxky") (f (quote (("rt" "cortex-m-rt/device"))))))

