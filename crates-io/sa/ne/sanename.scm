(define-module (crates-io sa ne sanename) #:use-module (crates-io))

(define-public crate-sanename-0.1.0 (c (n "sanename") (v "0.1.0") (h "1k646dr1if8xz8l37xznf8jmqkn01wxz71lrc2mang3brs5p5m7w")))

(define-public crate-sanename-0.1.1 (c (n "sanename") (v "0.1.1") (h "1mk8y5jvrgzck2af7lv62wbsb93jldsy14q1mr8ri6biwd0iyx6k")))

(define-public crate-sanename-0.1.2 (c (n "sanename") (v "0.1.2") (h "1v5mpdspmcj2hps6fkcfy6dgkfsz28zw25h840ncslyrv9xqdlaj")))

