(define-module (crates-io sa ne sane-scan) #:use-module (crates-io))

(define-public crate-sane-scan-0.1.0 (c (n "sane-scan") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 1)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0zib29yq3vfwxrj6029yf0cw71wamjjlcsifa5hk4x3018c0wwpl")))

(define-public crate-sane-scan-0.1.1 (c (n "sane-scan") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 1)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17jwqfm9klv89vzf8dac1zin4l247rf2k1i24jyzgmwsv6jvb2qc")))

(define-public crate-sane-scan-0.1.2 (c (n "sane-scan") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 1)))) (h "0mdrl21yy35lv31m6xscphhvxrvg34yip98m22k10bw3szx9r1vq")))

