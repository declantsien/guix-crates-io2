(define-module (crates-io sa ne sane-array) #:use-module (crates-io))

(define-public crate-sane-array-0.1.0 (c (n "sane-array") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)))) (h "16409ldswi7qvgykaxhb3gsjzsi8651ww6v56m2l54hghm6hmg4y") (y #t)))

(define-public crate-sane-array-0.1.1 (c (n "sane-array") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)))) (h "0dxxqmixm9g0zpajk9qqkbx82aycc81mnqc1g02gmxwa69dw2yd8")))

(define-public crate-sane-array-0.1.2 (c (n "sane-array") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)))) (h "09172a6dskavg1lka0pdq0pjw29xg8q80vmw27qlbkfmann6glk0")))

(define-public crate-sane-array-0.1.3 (c (n "sane-array") (v "0.1.3") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)))) (h "0i72c28pa7vwcqd5a0r1h1gx33y3k0aghr4z455y9j8lf7ap9wl5")))

