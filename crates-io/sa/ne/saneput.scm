(define-module (crates-io sa ne saneput) #:use-module (crates-io))

(define-public crate-saneput-0.1.0 (c (n "saneput") (v "0.1.0") (d (list (d (n "saneput-proc") (r "^0.1.0") (d #t) (k 0)))) (h "0vhka24h6m1p1yq4szkyb484b7cg1b0x5l66wri3raiw0h82143k")))

(define-public crate-saneput-0.2.0 (c (n "saneput") (v "0.2.0") (d (list (d (n "saneput-proc") (r "^0.2") (d #t) (k 0)))) (h "1p90icr2iqcfbg4fiqra9k4bn6zlrcccxm42bapfvcm2cb6h7fra")))

