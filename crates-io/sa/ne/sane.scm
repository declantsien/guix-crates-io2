(define-module (crates-io sa ne sane) #:use-module (crates-io))

(define-public crate-sane-0.0.1 (c (n "sane") (v "0.0.1") (h "0v9jc8f8hb5qb902gd9dvap9dz7z9zjbnb5g10nl29wphz94dx1v")))

(define-public crate-sane-0.1.0 (c (n "sane") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0q6mx2da2nl3mjlp7yll66z4nvfsrrc005pkm88ysm2mw9zbzs8p")))

(define-public crate-sane-0.2.0 (c (n "sane") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "17fg3pfv4g539si1d9znv77bhdrnclay731n8xmaxisc3ck0064i")))

(define-public crate-sane-0.2.1 (c (n "sane") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1lckvq41pbgjk2al0zi742xhhn1d2psp7pv42g4ssvz4rz1dr56b")))

