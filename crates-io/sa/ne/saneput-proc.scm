(define-module (crates-io sa ne saneput-proc) #:use-module (crates-io))

(define-public crate-saneput-proc-0.1.0 (c (n "saneput-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1d2q77pvqgw63zj3dyp70fxp1g3j4zr8higbkgbsglbm2pa7pwkb")))

(define-public crate-saneput-proc-0.2.0 (c (n "saneput-proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1x2bp7kb8vnsag7k54zs9xwj260ciilv77py24cr95j2wpp69h8f")))

