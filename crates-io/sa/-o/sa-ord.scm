(define-module (crates-io sa -o sa-ord) #:use-module (crates-io))

(define-public crate-sa-ord-0.0.1 (c (n "sa-ord") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "built") (r "^0.5") (f (quote ("git2"))) (d #t) (k 1)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "16dn0pngnkh35xxzcg662r9r6ikgv80zavzdrxdh9xaj0pbnahxg")))

