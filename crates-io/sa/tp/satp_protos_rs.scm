(define-module (crates-io sa tp satp_protos_rs) #:use-module (crates-io))

(define-public crate-satp_protos_rs-0.1.0 (c (n "satp_protos_rs") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "12hdz7xqjmkvlclm8yjgvl6hf2kzs6nlw5br9lilh7gv5dwmn4bh")))

(define-public crate-satp_protos_rs-0.1.1 (c (n "satp_protos_rs") (v "0.1.1") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("tls"))) (d #t) (k 0)))) (h "19jxxs1h58qya9ibc6nx5sl0vwbnjl1q7rplz2qy6r95c0wiym9l")))

