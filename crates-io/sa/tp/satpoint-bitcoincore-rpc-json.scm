(define-module (crates-io sa tp satpoint-bitcoincore-rpc-json) #:use-module (crates-io))

(define-public crate-satpoint-bitcoincore-rpc-json-0.17.1 (c (n "satpoint-bitcoincore-rpc-json") (v "0.17.1") (d (list (d (n "bitcoin") (r "^0.30.0") (f (quote ("serde" "rand-std"))) (d #t) (k 0)) (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0lgxg5dsswbnzdhf9j18rsiv5yidrjw9f0x850n087sff5c2qv2a")))

