(define-module (crates-io sa tp satpoint-bitcoincore-rpc) #:use-module (crates-io))

(define-public crate-satpoint-bitcoincore-rpc-0.17.2 (c (n "satpoint-bitcoincore-rpc") (v "0.17.2") (d (list (d (n "bitcoin-private") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "satpoint-bitcoincore-rpc-json") (r "^0.17.1") (d #t) (k 0)) (d (n "satpoint-jsonrpc") (r "^0.16.1") (f (quote ("minreq_http"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18km4n1k6ydb9qns35fzbgvd895b7lfrbz2jcsj5qmrqfmbij5h1")))

