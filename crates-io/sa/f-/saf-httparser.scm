(define-module (crates-io sa f- saf-httparser) #:use-module (crates-io))

(define-public crate-saf-httparser-0.1.0 (c (n "saf-httparser") (v "0.1.0") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "1wcs5dy6ilx7ry8rpxd8xyd76f5y3zfgy49rwfmch2hrn5v301ki")))

(define-public crate-saf-httparser-0.1.1 (c (n "saf-httparser") (v "0.1.1") (d (list (d (n "http") (r "^0.2.1") (d #t) (k 0)))) (h "1797aaj85abkpxnvfbfac9s3brgncwnlc1v16zkiiflynsg74kx7")))

