(define-module (crates-io sa m3 sam3x8e) #:use-module (crates-io))

(define-public crate-sam3x8e-0.1.0 (c (n "sam3x8e") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ixssrdimp4r4r9xzvci8q3n6snivh77hqy8d0a35dcvg24mg3hk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-sam3x8e-0.1.1 (c (n "sam3x8e") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1q0rljgd4vng7wfv32yj8g5zshz1iylpnmh17fy4srhkksys2nf5") (f (quote (("rt" "cortex-m-rt/device"))))))

