(define-module (crates-io sa bi sabisabi) #:use-module (crates-io))

(define-public crate-sabisabi-0.1.0 (c (n "sabisabi") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1ac4rl2lh3qa7gsa4dklyzr5xckqhhn69hnzjinsfkzssdr0dcjq")))

(define-public crate-sabisabi-0.2.0 (c (n "sabisabi") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1p4ny2mf9iywdxibdic273yaqjzqjlqq1f3jpx7h75gzhaypa4di")))

(define-public crate-sabisabi-0.2.1 (c (n "sabisabi") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1f8dvk65pbldfg714lxm3lgzkzfz5dqg6wyfnvx491kv9chrkcph")))

(define-public crate-sabisabi-0.3.0 (c (n "sabisabi") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1ar1xg9r9sb5sw3afic2y35ccra1q10x1hkakk36igy07icdk69v")))

(define-public crate-sabisabi-0.3.1 (c (n "sabisabi") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nv9klqdyzmnjflay797x3r31lf6lh70yn699kn3m4zss129lhnr")))

(define-public crate-sabisabi-0.4.0 (c (n "sabisabi") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1l639mjmzpnn71xnfw61ny09lka7sq054qgiay7bfahjmczhw60q")))

(define-public crate-sabisabi-0.4.1 (c (n "sabisabi") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "05mnr4pwjj2qcvj41qb6m24vhaglscpm5rgxcmhk5824jd8h8khc")))

