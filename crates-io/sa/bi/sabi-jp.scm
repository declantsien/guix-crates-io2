(define-module (crates-io sa bi sabi-jp) #:use-module (crates-io))

(define-public crate-sabi-jp-0.4.0 (c (n "sabi-jp") (v "0.4.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d34xj6qqd85clrf402bvh6zqzszll3bml9a6n5y4z82pgv0csbi")))

