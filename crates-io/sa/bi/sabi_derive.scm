(define-module (crates-io sa bi sabi_derive) #:use-module (crates-io))

(define-public crate-sabi_derive-0.2.0 (c (n "sabi_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02zpbblf167i8kwifl09v7ilxl3c8q5f0ww1l3dydkcxf2zr4fa6")))

