(define-module (crates-io sa bi sabi) #:use-module (crates-io))

(define-public crate-sabi-0.1.0 (c (n "sabi") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "combine") (r "^3.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "0ncsgjvg2ggr7r6kmvw7avhkzp3iwblqpl5sbmfpkg1x9bfi5pml")))

