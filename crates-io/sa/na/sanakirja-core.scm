(define-module (crates-io sa na sanakirja-core) #:use-module (crates-io))

(define-public crate-sanakirja-core-1.0.0 (c (n "sanakirja-core") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zkdmf93r55svr1qqsbgqfsj7m1nqg2zbrm6vi2y8m8dcdrnggwb")))

(define-public crate-sanakirja-core-1.0.1 (c (n "sanakirja-core") (v "1.0.1") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "06xpkj6gn2hz3hxynn1ygh4j6yrh8j1pc1p9zjwlrc43miznqkzk") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.0.2 (c (n "sanakirja-core") (v "1.0.2") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "01qss8ckcv229cgbxqlgqjp0wv1szqdjqim4bjjp85yzgi3ham7z") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.0.3 (c (n "sanakirja-core") (v "1.0.3") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1jxvrf6i3237k584j1nwxf3x7dgirm2fycakv2dpgza6sy62yv2j") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.0.4 (c (n "sanakirja-core") (v "1.0.4") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1cs6cnrxsjffgz26rvjkpn1n5jyszw0zs3q06gd80a4inmr307ri") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.1.0 (c (n "sanakirja-core") (v "1.1.0") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1q6wrwj2bdx12rr1pah2sdyy802gm4710yhfpn77rbkppg7nypd5") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.1.1 (c (n "sanakirja-core") (v "1.1.1") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1i0d8kidgfrhxnv6gwbad1dn783x5y8365ki081v8l0ljd0rv6hi") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.0 (c (n "sanakirja-core") (v "1.2.0") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "0izqy6n6dfjll09087ajb8cn4bwzkfyvd28w9bf3x90h649lhq96") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.1 (c (n "sanakirja-core") (v "1.2.1") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1w00almxm0jh35h750xv4wpxwdz3vvd1c41pizy1bsc897q8xp1c") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.2 (c (n "sanakirja-core") (v "1.2.2") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "0hc6f3cqj27ys0fyvkzxkjgbbpbpd94nq9xhvm3mwg1qwjasrawj") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.3 (c (n "sanakirja-core") (v "1.2.3") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "0xjp2i4qqmmdsr9747802dk869lzp74pn1gs3jw4av1xgzb1ap9l") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.4 (c (n "sanakirja-core") (v "1.2.4") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "0fsaswn6lwcfv2ai8ha4afvdyw2cy2hr9rmyhnv5an96phc62ny1") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.5 (c (n "sanakirja-core") (v "1.2.5") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "08iqdn1m8qvn52hi9mqggp54kjv2r6zhj0qfhyaxhhalv9rkjl8j") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.6 (c (n "sanakirja-core") (v "1.2.6") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "0jwrsqvjzgx8xzpvyrfi1p017picdxxa8ay9ghiwsm50azs5pxjn") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.7 (c (n "sanakirja-core") (v "1.2.7") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1mclvasypzlinhil3fkdbsdgk9ymym8fwwsy9v7f725rpwxqzbc1") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.8 (c (n "sanakirja-core") (v "1.2.8") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "1vpnggj97zdrxjra90mvj1hbrgg58m06vi2n4sx41a9hspv5h4qj") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.9 (c (n "sanakirja-core") (v "1.2.9") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "015l0d3h21cwf9jxzf3q70sbjrg54mr6xbd236a5f9vg7zaxqxav") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.10 (c (n "sanakirja-core") (v "1.2.10") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)))) (h "12j7ig287qqhcbz14r2fhwqzd3hdb1ssj7h7rwykl1gm9pfnmns3") (f (quote (("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.11 (c (n "sanakirja-core") (v "1.2.11") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1dmwln93n2v34387flf7jmm35kp6ash8dg88lsd9qqkz750fniab") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.12 (c (n "sanakirja-core") (v "1.2.12") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1l8x0nah0l82kybl5fq8fpqp4r96fbc3y1ids781c6489j4n2zx6") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.13 (c (n "sanakirja-core") (v "1.2.13") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "005y1sr4vi58n1ysci097nc2jvnrmsl562zvkjqxcscr474sca4q") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.14 (c (n "sanakirja-core") (v "1.2.14") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0phn0s86qbcc6bd2yayikgnk1lvbv7xrh6g8qp4ankp3whp7h2bz") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast")))) (y #t)))

(define-public crate-sanakirja-core-1.2.15 (c (n "sanakirja-core") (v "1.2.15") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1mhmm439mavi8hjd0zk381bc5cf3szxkmc9cq340xzd6s1662185") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.2.16 (c (n "sanakirja-core") (v "1.2.16") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1asm3ss02ngrpbrvjwgvivq380k5diib3sbgsbvqaj2w8mr3wdak") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.3.0 (c (n "sanakirja-core") (v "1.3.0") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0x95b2jai1shviphlq47anx6ximlascnkzjxd7hp3b0yxsf17ljp") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast")))) (y #t)))

(define-public crate-sanakirja-core-1.3.1 (c (n "sanakirja-core") (v "1.3.1") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "03wqhsczwxj8gcdkhpq6cgqpgwbpnlmjv73ymhngkhhshpx5q9rb") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast")))) (y #t)))

(define-public crate-sanakirja-core-1.3.2 (c (n "sanakirja-core") (v "1.3.2") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "08jh6piwm67md4i7x90yr6lmxp4yjmb9ph3s0djga9ln9x0rh37w") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast")))) (y #t)))

(define-public crate-sanakirja-core-1.3.3 (c (n "sanakirja-core") (v "1.3.3") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1rkx4widy2fkdsg2zbbl20635rgmfbjvlb3zhv16gw2kivrfn62b") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.4.0 (c (n "sanakirja-core") (v "1.4.0") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (o #t) (d #t) (k 0)))) (h "1ip267arv4hk73kf2gg8pg06gwargcydcnip4wc17hwl47cql9z8") (f (quote (("typeids" "sha2") ("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-1.4.1 (c (n "sanakirja-core") (v "1.4.1") (d (list (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (o #t) (d #t) (k 0)))) (h "1xcb4d9gaxp61na4dyz9iiq3n1shhiivqs0iv5xnxb1ymqsdnxl3") (f (quote (("typeids" "sha2") ("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

