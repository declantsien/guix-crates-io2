(define-module (crates-io sa na sanakirja-core-async) #:use-module (crates-io))

(define-public crate-sanakirja-core-async-0.0.1 (c (n "sanakirja-core-async") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1cdp94c5q8v11a4d9qxrz5d0ni1r12qkri9k2z25q9nl4x1lbp34") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

(define-public crate-sanakirja-core-async-0.0.2 (c (n "sanakirja-core-async") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (o #t) (k 0)) (d (n "ed25519-zebra") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1hnrkwizxd7svcbj4wqmnmahqp9hmb9qqi2gqbcl336nyxhrv3y6") (f (quote (("std") ("ed25519" "ed25519-zebra" "ed25519-zebra/serde") ("crc32" "crc32fast"))))))

