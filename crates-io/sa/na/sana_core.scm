(define-module (crates-io sa na sana_core) #:use-module (crates-io))

(define-public crate-sana_core-0.1.0 (c (n "sana_core") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dot") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0") (d #t) (k 0)))) (h "0hh09dqv24pfy2r7n5i6gb62gqkzbhfzjnc5k741fp9905mb3bc1") (f (quote (("default") ("automata_dot" "dot"))))))

(define-public crate-sana_core-0.1.1 (c (n "sana_core") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dot") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1.0") (d #t) (k 0)))) (h "1s7qs67cbmfmpqn7plkdn0zjqwq0pc5anjhgnn79m7pxagnjbkwn") (f (quote (("default") ("automata_dot" "dot"))))))

