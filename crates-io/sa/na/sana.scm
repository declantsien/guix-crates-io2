(define-module (crates-io sa na sana) #:use-module (crates-io))

(define-public crate-sana-0.1.0 (c (n "sana") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sana_core") (r "^0.1.0") (d #t) (k 0)) (d (n "sana_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1zy7748ak0qr36qdi8gmxhq312jjx3b6fmymcamsr97p79h3wh03")))

(define-public crate-sana-0.1.1 (c (n "sana") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sana_core") (r "^0.1.1") (d #t) (k 0)) (d (n "sana_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0gxda5lgmnfr0rxwdnrzb90rsawabph72dkgi5sc1xghy0xbbg51")))

