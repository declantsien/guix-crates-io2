(define-module (crates-io sa ig saigon_core) #:use-module (crates-io))

(define-public crate-saigon_core-0.1.0 (c (n "saigon_core") (v "0.1.0") (h "0c8mfig6xi1hdijg9c2nv7f6hx0dabfnhmdxq3c2y2xxwamxm7yb")))

(define-public crate-saigon_core-0.1.1 (c (n "saigon_core") (v "0.1.1") (h "018sxr5x3ncyxr917bz878fhbp7cp093aj69lz04z6cgk4g2c0fa")))

(define-public crate-saigon_core-0.1.2 (c (n "saigon_core") (v "0.1.2") (h "0p2i31yc5bwl17c9r8dc2rj5cxpkkhn3zmg5wbcxvwn7cbsizxd5")))

