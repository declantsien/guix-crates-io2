(define-module (crates-io sa ig saigon_plugin_cat_fact) #:use-module (crates-io))

(define-public crate-saigon_plugin_cat_fact-0.1.0 (c (n "saigon_plugin_cat_fact") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qq51rbrpmgx6vpikvx4qqyv3dciy3hsnqbkj3y1qdgivxfcrkn5")))

(define-public crate-saigon_plugin_cat_fact-0.1.1 (c (n "saigon_plugin_cat_fact") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07crwy7v2y2l51ij79xrand148hl4w4zabjwn1rshb0dqgazqw49")))

(define-public crate-saigon_plugin_cat_fact-0.1.2 (c (n "saigon_plugin_cat_fact") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x4z4m64qbcg5awfbsszhmsb7rrgsg1h7v2lhdxyr2mfgdf2iw7v")))

