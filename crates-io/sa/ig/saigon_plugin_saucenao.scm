(define-module (crates-io sa ig saigon_plugin_saucenao) #:use-module (crates-io))

(define-public crate-saigon_plugin_saucenao-0.1.0 (c (n "saigon_plugin_saucenao") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wjigafvjavzrcjp9ri7mr9cy8w2rm388p28qywy564jwjrmn138")))

(define-public crate-saigon_plugin_saucenao-0.1.1 (c (n "saigon_plugin_saucenao") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lix4hrv8ic5np2h7n2yhhs9j0kiz9bzssv5ffyc2kdyl5s8fl4b")))

(define-public crate-saigon_plugin_saucenao-0.1.2 (c (n "saigon_plugin_saucenao") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mca2yfx1p2qr06iv7k27d9gii3im01h02ym4l2zjd83qrawyxs4")))

