(define-module (crates-io sa ig saigon) #:use-module (crates-io))

(define-public crate-saigon-0.1.0 (c (n "saigon") (v "0.1.0") (d (list (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)))) (h "01qs5z3rh0ah10f6bx4q3q67gb0nppa5by7np9m10h8m0a7fm0c2")))

(define-public crate-saigon-0.1.1 (c (n "saigon") (v "0.1.1") (d (list (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)))) (h "1f3k23finsg4i928pq0y1avw0c5cr92yswrd51si05ik6z2cgml7")))

(define-public crate-saigon-0.1.2 (c (n "saigon") (v "0.1.2") (d (list (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "saigon_core") (r "^0.1") (d #t) (k 0)))) (h "11dhspn97xxpr381mq7jiv6m57x22r0bdbsfc61kcfnrf05lfkf4")))

