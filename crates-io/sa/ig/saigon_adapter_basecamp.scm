(define-module (crates-io sa ig saigon_adapter_basecamp) #:use-module (crates-io))

(define-public crate-saigon_adapter_basecamp-0.1.0 (c (n "saigon_adapter_basecamp") (v "0.1.0") (d (list (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y6g9vssdf7pm7jiahm9p52a1dvsjbqb1nhpw9byqc07fkqlc3g2")))

(define-public crate-saigon_adapter_basecamp-0.1.1 (c (n "saigon_adapter_basecamp") (v "0.1.1") (d (list (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17cbmbjbf0w9iljp68krk84i1x4752kibxjw3nxq5rvpf701kkpc")))

(define-public crate-saigon_adapter_basecamp-0.1.2 (c (n "saigon_adapter_basecamp") (v "0.1.2") (d (list (d (n "saigon_core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1knbygyhj1gp08b390c85fc2iby7fjiclk8bz2kh6lhs881h56g6")))

