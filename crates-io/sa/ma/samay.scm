(define-module (crates-io sa ma samay) #:use-module (crates-io))

(define-public crate-samay-0.1.0 (c (n "samay") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "06vmcljaj7zinlxasyinfdvwch5rc8axz3j3ka6nsj6wq0mkfhbn")))

(define-public crate-samay-0.1.1 (c (n "samay") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (d #t) (k 0)))) (h "1xzr6ikgj9bhcmhijp9nlimar3dg306m9frpyii1k8c9a8mc42mg")))

