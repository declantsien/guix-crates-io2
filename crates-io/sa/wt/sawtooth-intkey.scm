(define-module (crates-io sa wt sawtooth-intkey) #:use-module (crates-io))

(define-public crate-sawtooth-intkey-0.5.0 (c (n "sawtooth-intkey") (v "0.5.0") (d (list (d (n "cbor-codec") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sawtooth-sdk") (r "^0.5") (d #t) (k 0)))) (h "12dnn13m3idjjrpibqi0b7dxspqz2z2dipp9gh9450l6xm9a4yli") (f (quote (("stable") ("experimental" "stable") ("default"))))))

(define-public crate-sawtooth-intkey-0.5.2 (c (n "sawtooth-intkey") (v "0.5.2") (d (list (d (n "cbor-codec") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "sawtooth-sdk") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "017q1ly50z9lkal2v6x6kdb88595s5s5hh3cabaj85j46l6i8978") (f (quote (("stable") ("experimental" "stable") ("default"))))))

