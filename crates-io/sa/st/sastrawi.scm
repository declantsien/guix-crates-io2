(define-module (crates-io sa st sastrawi) #:use-module (crates-io))

(define-public crate-sastrawi-0.1.0 (c (n "sastrawi") (v "0.1.0") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)))) (h "0x4njbxflw7j2xw17mvnlfw3w2qqf96213dkirxjzm7sj1i27mss")))

(define-public crate-sastrawi-0.1.1 (c (n "sastrawi") (v "0.1.1") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)))) (h "13rd9z80hjn383dk4kbr6na1dsnkp9cfhwq8w12zjmwmxk3yf7zh")))

