(define-module (crates-io sa tr satrs-shared) #:use-module (crates-io))

(define-public crate-satrs-shared-0.1.0 (c (n "satrs-shared") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "spacepackets") (r "^0.9") (k 0)))) (h "0jlm37vyvi29p7rw3qn5wlb52c3xxs1kp9z43s9dalmmrrnnficr") (s 2) (e (quote (("serde" "dep:serde" "spacepackets/serde"))))))

(define-public crate-satrs-shared-0.1.1 (c (n "satrs-shared") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "spacepackets") (r "^0.9") (k 0)))) (h "0bd1z1r0pljfxiqqkp2wq2np0kq23llsmdq0xlr780pn8vicwahs") (s 2) (e (quote (("serde" "dep:serde" "spacepackets/serde"))))))

(define-public crate-satrs-shared-0.1.2 (c (n "satrs-shared") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "spacepackets") (r "^0.10") (k 0)))) (h "0ldzl440bpgkjy3fj4a3cw4rn9d3cigb8dbhf3pmwzvaanx0593m") (s 2) (e (quote (("serde" "dep:serde" "spacepackets/serde"))))))

(define-public crate-satrs-shared-0.1.3 (c (n "satrs-shared") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "spacepackets") (r ">0.9, <=0.11") (k 0)))) (h "0rsg5jzjh5h9rkkr4qjrhv5ixx0vy4vmxnrrqlillcq2p42gh7gh") (s 2) (e (quote (("serde" "dep:serde" "spacepackets/serde"))))))

(define-public crate-satrs-shared-0.1.4 (c (n "satrs-shared") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "spacepackets") (r ">0.9, <=0.11") (k 0)))) (h "1wblwh8r723nnb2k2kcv2f2q99crqadhk9darkzkzm6231q4fhk0") (s 2) (e (quote (("spacepackets" "dep:defmt" "spacepackets/defmt") ("serde" "dep:serde" "spacepackets/serde"))))))

