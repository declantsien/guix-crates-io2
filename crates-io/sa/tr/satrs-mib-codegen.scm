(define-module (crates-io sa tr satrs-mib-codegen) #:use-module (crates-io))

(define-public crate-satrs-mib-codegen-0.1.0-alpha.0 (c (n "satrs-mib-codegen") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "000xjrzdkzjb8s0r3m2w06cm1nfxalskhnbm7igvvjjilshrxzcn")))

(define-public crate-satrs-mib-codegen-0.1.0-alpha.1 (c (n "satrs-mib-codegen") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-core") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "16im3sra2vwpg3drw58nv5k2pbnprnxkrkfdw0dslc897ll1bx9i")))

(define-public crate-satrs-mib-codegen-0.1.0-alpha.2 (c (n "satrs-mib-codegen") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-core") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "029v1kfjrmya39fvc4ryfbax21lxcmjcy4znd18p0y1mbni996h8")))

(define-public crate-satrs-mib-codegen-0.1.0 (c (n "satrs-mib-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-shared") (r "^0.1.1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yv6w1ryfpsw3w5r8x9achjsd6fgasp00zbf5r3dv0gsb4hiikfs")))

(define-public crate-satrs-mib-codegen-0.1.1 (c (n "satrs-mib-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-shared") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q6x2w8rkg14462sqpa0zd93scm7vl4njh9qk6zxhyy8k6krllv5")))

(define-public crate-satrs-mib-codegen-0.1.2 (c (n "satrs-mib-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "satrs-shared") (r ">=0.1.3, <0.2") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hqdyvsfxdjhfl34wx27mxh6p6p2kwfa27g758vbq7sjz8zmhjs7")))

