(define-module (crates-io sa i_ sai_component_derive) #:use-module (crates-io))

(define-public crate-sai_component_derive-0.1.0 (c (n "sai_component_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "0hjl4j7qnndnin8ikg6d9c5ib6cr94g4bcmcfihvy3l8avalc08d")))

(define-public crate-sai_component_derive-0.1.1 (c (n "sai_component_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "1f0vvbpn33mn027kj00h48j3xsw03i91rv234ng22izrz127baif")))

(define-public crate-sai_component_derive-0.1.2 (c (n "sai_component_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "1klkfp6b4p1y5js92aw9q48cvc81izfrqai4h0z4i0dxcb3asqpa")))

(define-public crate-sai_component_derive-0.1.3 (c (n "sai_component_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "17b8k60w9via372lc3i6ibbgydp7z6jw23cpfk5cawmndk694r4y")))

(define-public crate-sai_component_derive-0.1.4 (c (n "sai_component_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.29") (d #t) (k 0)))) (h "1c2w8wax3b384mlj6rac0dbnf53ms3dn0zn744afplhwky8x1vww")))

