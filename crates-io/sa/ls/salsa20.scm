(define-module (crates-io sa ls salsa20) #:use-module (crates-io))

(define-public crate-salsa20-0.0.0 (c (n "salsa20") (v "0.0.0") (h "0xgkjmgz7r96sahp6497smkx5hiyxvc95zk8npc8jhlmhkms19s9")))

(define-public crate-salsa20-0.1.0 (c (n "salsa20") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "salsa20-core") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1n12gi7dp8wxnrb8fgn0abgynhbl82ha9np58z4vpxvds8n5zrjk")))

(define-public crate-salsa20-0.1.1 (c (n "salsa20") (v "0.1.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "salsa20-core") (r "^0.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1mviil9x3d22l2mc5xl87i1gca68f1cbcqs1rbizdnwsfsbiipvn")))

(define-public crate-salsa20-0.2.0 (c (n "salsa20") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "salsa20-core") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "03az46ncbbiaqza6qpd3jz0f5pgmb4wfd9dcnjmdja39wv0rfmrm") (f (quote (("zeroize" "salsa20-core/zeroize"))))))

(define-public crate-salsa20-0.3.0 (c (n "salsa20") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "salsa20-core") (r "^0.2") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "1zli3zwwzq467w27h2p0ims2qzhfywv33nqzaxm5i6mvqglb0913") (f (quote (("zeroize" "salsa20-core/zeroize") ("xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.4.0 (c (n "salsa20") (v "0.4.0") (d (list (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0bpri5wa4l31kpchlsxg7v0n9w7294873l62hpk6xgyblfgdn59x") (f (quote (("xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.4.1 (c (n "salsa20") (v "0.4.1") (d (list (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0g7z7qhg8f6kg022kk841gz7r3ml45vkh5msy0jzzivnmp5gxkxy") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.5.0 (c (n "salsa20") (v "0.5.0") (d (list (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "1djk8355vbysmymfmbwj7r5vny3a6803mmfsqx5q4rkpfabf8ccl") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.5.1 (c (n "salsa20") (v "0.5.1") (d (list (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "012i3rzsyy82bggx5qh8sm26xspk5fibcq9c7z18avaxn8fkxz2x") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.5.2 (c (n "salsa20") (v "0.5.2") (d (list (d (n "stream-cipher") (r "^0.4.1") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0sb7lvpjpzbcv92p0h8gm0lihb7v8and78lmkw6h9lz5xv2pvhbg") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.6.0 (c (n "salsa20") (v "0.6.0") (d (list (d (n "stream-cipher") (r "^0.7") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "15akjn3klc72g42y3a82siwjj24gk76fgj6rpfdrdxl0z887px67") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.7.0 (c (n "salsa20") (v "0.7.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0k7074mzwa2v189wvqkgklxpqs2avmx23sfxy818acwkapzws05r") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.7.1 (c (n "salsa20") (v "0.7.1") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "09c16m566g45f41xx3673zyzwca3mykz630fmv2mbjbvmwcc4fw0") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("expose-core") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.7.2 (c (n "salsa20") (v "0.7.2") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "05cs0n4q63czqmi8rcpjhi3vx1412gaaapnf5w19d5a0zh7jk7rr") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("expose-core") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.8.0 (c (n "salsa20") (v "0.8.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0ykqc3zyh4qp8zx4kr9a3j6wb33cyhz3zd51w53skssbhq85yz0w") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("expose-core") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.8.1 (c (n "salsa20") (v "0.8.1") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "=1.3") (o #t) (k 0)))) (h "1c13p5f8iyx9ak009s1pak8p48fcfgipz0x00icanz7x76v2xggc") (f (quote (("xsalsa20") ("hsalsa20" "xsalsa20") ("expose-core") ("default" "xsalsa20"))))))

(define-public crate-salsa20-0.9.0 (c (n "salsa20") (v "0.9.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r ">=1, <1.4") (o #t) (k 0)))) (h "11i646kpgimimqiq8hyi0b7ngp588f7nl9xsc317d9kdcxgvn3qc") (f (quote (("hsalsa20") ("expose-core"))))))

(define-public crate-salsa20-0.10.0 (c (n "salsa20") (v "0.10.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1syqi7pvrcqvnz3lk0y7qscb6kkjj1sclaz04grfcp4zp82m6894") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

(define-public crate-salsa20-0.10.1 (c (n "salsa20") (v "0.10.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "03c9ww08rz637kgjcslvbfb70xbiskrdp0iqc93jnwy0m9qb4yi2") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

(define-public crate-salsa20-0.10.2 (c (n "salsa20") (v "0.10.2") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "04w211x17xzny53f83p8f7cj7k2hi8zck282q5aajwqzydd2z8lp") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

