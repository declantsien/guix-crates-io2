(define-module (crates-io sa ls salsa-macros) #:use-module (crates-io))

(define-public crate-salsa-macros-0.10.0-alpha1 (c (n "salsa-macros") (v "0.10.0-alpha1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cagpb58kmqz7kkkr7qh0kmhvhsa2m2x11981nhb394dc7milnhd")))

(define-public crate-salsa-macros-0.10.0-alpha3 (c (n "salsa-macros") (v "0.10.0-alpha3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y7z59wmf3fd2ndaa3d6sv6d2xl04w5lgn5yqhs16a6pzrg3pjf9")))

(define-public crate-salsa-macros-0.10.0-alpha4 (c (n "salsa-macros") (v "0.10.0-alpha4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hnymfdh2afl2mb7clfcnrbvkhxidnic304f8f0lid2fxqdilv1f")))

(define-public crate-salsa-macros-0.10.0-alpha5 (c (n "salsa-macros") (v "0.10.0-alpha5") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ilwjhf9h5bf9b45k1971dbnsz0zkcvrmkaghv0lyvczci6dmig7")))

(define-public crate-salsa-macros-0.10.0 (c (n "salsa-macros") (v "0.10.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fwz1gn25ci1jc7byxkfjsy8ajcy1adncrqkxw5bh8fz150p8ybw")))

(define-public crate-salsa-macros-0.11.0 (c (n "salsa-macros") (v "0.11.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n6j4l1d31gkq68s8jqfdy0c8qkrk7yh3bcfnxbahj293g5d20i9")))

(define-public crate-salsa-macros-0.11.1 (c (n "salsa-macros") (v "0.11.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ffxr5ksw7k8ykyx90hjdz1zxskdd9yd6sg4jsd77sc9w6x7rlhs")))

(define-public crate-salsa-macros-0.12.0 (c (n "salsa-macros") (v "0.12.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1adyzp8183y3vxzyb3zkid30af4svwqma97874xbv7m5xn9bl84i")))

(define-public crate-salsa-macros-0.12.1 (c (n "salsa-macros") (v "0.12.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n8cmjsxx6vgvj4gzmxbazyny5b2kn978qcl6bqbv5drl9ff5wdp")))

(define-public crate-salsa-macros-0.13.0 (c (n "salsa-macros") (v "0.13.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ablnkvy2asckppma0ll7n735f20k7jafq36yv1wbp30asff4z1p")))

(define-public crate-salsa-macros-0.13.1 (c (n "salsa-macros") (v "0.13.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15dnd84rwfkr0snzyv4rz6kazkpjgvwj2cr8hh5z9cw5bipg65bc")))

(define-public crate-salsa-macros-0.13.2 (c (n "salsa-macros") (v "0.13.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vpg75p9fv5g1dzfw6439dlql98v8vqryfa2vifvf79s5lhq5hfa")))

(define-public crate-salsa-macros-0.14.1 (c (n "salsa-macros") (v "0.14.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c2mfkrjlk2pps36jbpq9iy6im7npvjyfbql7w9g2ihl4yv0k2h3")))

(define-public crate-salsa-macros-0.15.0 (c (n "salsa-macros") (v "0.15.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17rbjibslx7c5xc9lxlh9wf6m3m44rv40jxchr5j3b0mbg40la1c")))

(define-public crate-salsa-macros-0.15.2 (c (n "salsa-macros") (v "0.15.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vwjnzh2mkfn81mgqx95av27am8bzclqa0bsrpa4wg660z0axhx1")))

(define-public crate-salsa-macros-0.16.0 (c (n "salsa-macros") (v "0.16.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qlg3iagwr2h2w8wvnlc7x29aw2cn0sgsxv1h48h578apaj08ffd")))

(define-public crate-salsa-macros-0.17.0-pre.1 (c (n "salsa-macros") (v "0.17.0-pre.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yrjiriw5bb0gxl2dk7c5ng7g5wlkc4gd65czx5x99bzc9hc0brf")))

(define-public crate-salsa-macros-0.17.0-pre.2 (c (n "salsa-macros") (v "0.17.0-pre.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xrgb38l5fhv3lqx2lwnqc3s2zrgxmj63cd7kl0vyl7m5lsjwv5c")))

