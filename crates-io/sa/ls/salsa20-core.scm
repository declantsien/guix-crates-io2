(define-module (crates-io sa ls salsa20-core) #:use-module (crates-io))

(define-public crate-salsa20-core-0.0.0 (c (n "salsa20-core") (v "0.0.0") (h "0bz9wa473m2jwkdmwwqfxb2xfbdiz37h9x3cndwm53201ss21fpj") (y #t)))

(define-public crate-salsa20-core-0.1.0 (c (n "salsa20-core") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1m1c0i0phbml51hwnwzqwajl3vd7id5568fs2f4nclvda912bwxr") (y #t)))

(define-public crate-salsa20-core-0.2.0 (c (n "salsa20-core") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1yj5d0gj3xyybj6nykz6rva183dd5hc9p2n0c1ycdhlynx2mlzbr") (y #t)))

(define-public crate-salsa20-core-0.2.1 (c (n "salsa20-core") (v "0.2.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^0.9") (o #t) (k 0)))) (h "1njwmshp5l0cdk788h5miyw937vdjrv5nnq35jmhlhjvn9f35zf7") (y #t)))

(define-public crate-salsa20-core-0.2.2 (c (n "salsa20-core") (v "0.2.2") (d (list (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "1z8jybql1q71ba733bq1d1mrw5nbw60dq4x2b4kgipk2ky5czp5w") (y #t)))

(define-public crate-salsa20-core-0.2.3 (c (n "salsa20-core") (v "0.2.3") (d (list (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0sbfiqjcj898wpzx2274g5v2y10gwyfhjqyy7a2nfn2skwdwrrig") (y #t)))

(define-public crate-salsa20-core-0.99.0 (c (n "salsa20-core") (v "0.99.0") (h "16ag4dqvjwdbqg059m5md504by7ys9rdd0licxm7a68wb07whdrw") (y #t)))

