(define-module (crates-io sa vo savory-derive) #:use-module (crates-io))

(define-public crate-savory-derive-0.5.1 (c (n "savory-derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "105aj3bq5pbnmmahxdmzixbbbkpdpwqmdcsnzd5v5768bfx9xad8")))

