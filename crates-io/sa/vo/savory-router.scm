(define-module (crates-io sa vo savory-router) #:use-module (crates-io))

(define-public crate-savory-router-0.6.0 (c (n "savory-router") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "darling") (r "^0.11.0") (d #t) (k 0)) (d (n "ident_case") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "047bswznqdd3fjrpn5dc17sakw8lri9dskigj80hqmxy1j0rwr5c")))

