(define-module (crates-io sa vo savon) #:use-module (crates-io))

(define-public crate-savon-0.1.0 (c (n "savon") (v "0.1.0") (h "158jwy3lj92nxiyzy9d87b6xx8npfrsq3jyazw44d30xyjify1km")))

(define-public crate-savon-0.2.0 (c (n "savon") (v "0.2.0") (d (list (d (n "case") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1npp7n16qrn39pszxw6kbs0m0vz8vx7wakpkk22c8p7hki65q9pg")))

