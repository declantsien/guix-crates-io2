(define-module (crates-io sa vo savory-elements-derive) #:use-module (crates-io))

(define-public crate-savory-elements-derive-0.6.0 (c (n "savory-elements-derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pd51zh91fan1wx74ss357cfb3n5wgq3r3r8f2l6w978iidic9i9")))

