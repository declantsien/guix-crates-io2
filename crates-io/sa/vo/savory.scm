(define-module (crates-io sa vo savory) #:use-module (crates-io))

(define-public crate-savory-0.0.1 (c (n "savory") (v "0.0.1") (h "0xcjh2ayyj86gjmk1lir6z55rrmfvjqpz0mqy0alqsnqyf7fxzh6")))

(define-public crate-savory-0.6.0 (c (n "savory") (v "0.6.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "seed") (r "^0.8.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.45") (f (quote ("WheelEvent" "CompositionEvent" "HtmlInputElement" "InputEvent" "FocusEvent" "DragEvent" "DomRect" "Element" "HtmlLabelElement" "UiEvent"))) (d #t) (k 0)))) (h "1y4smz2j1k42zbn9m24nzlbx26rkmrqbgjwj1fi88s0ymxy3qhjf")))

