(define-module (crates-io sa vo savory-core) #:use-module (crates-io))

(define-public crate-savory-core-0.5.0 (c (n "savory-core") (v "0.5.0") (d (list (d (n "seed") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 2)))) (h "152s6nr934n96xkclb0aw2hvb8kyxakhd6fi6ahsicq6p311hhsq")))

(define-public crate-savory-core-0.5.1 (c (n "savory-core") (v "0.5.1") (d (list (d (n "seed") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 2)))) (h "15vwj84iawpjfbj73m3z0d4kycz6cs6r3xs0fcjylsyqc9sz142v")))

