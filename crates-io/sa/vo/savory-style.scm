(define-module (crates-io sa vo savory-style) #:use-module (crates-io))

(define-public crate-savory-style-0.6.0 (c (n "savory-style") (v "0.6.0") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)) (d (n "savory") (r "^0.6") (d #t) (k 0)))) (h "05a4fypkm8m27qh18x125j8613mrfsqljlf5ibx2irf6w40vb1nn")))

