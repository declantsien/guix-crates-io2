(define-module (crates-io sa ni sanitizers) #:use-module (crates-io))

(define-public crate-sanitizers-0.0.1 (c (n "sanitizers") (v "0.0.1") (h "0arkxr12cw3mpg1l7h7vlv862m3gsvjvwjh646y9hlrfi4djlk2x")))

(define-public crate-sanitizers-0.0.2 (c (n "sanitizers") (v "0.0.2") (h "0x2i7l1a5s3ynqspdpk5gf78zkg88nbpgpip6l53mkcd8r0vz63j")))

