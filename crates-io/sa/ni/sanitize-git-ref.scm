(define-module (crates-io sa ni sanitize-git-ref) #:use-module (crates-io))

(define-public crate-sanitize-git-ref-1.0.0 (c (n "sanitize-git-ref") (v "1.0.0") (d (list (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.32") (d #t) (k 0)))) (h "0f1g80w5br79v3vj2g34qz519sq2k2my0k7cjs7vvw88g093mka3")))

(define-public crate-sanitize-git-ref-1.0.1 (c (n "sanitize-git-ref") (v "1.0.1") (d (list (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.36") (d #t) (k 0)))) (h "140sl2gnzknpbjnkdx3fqbiwlgmiw87b1fp7g9n5gx48f5fkm4qb")))

(define-public crate-sanitize-git-ref-1.0.2 (c (n "sanitize-git-ref") (v "1.0.2") (d (list (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "=1.6.0") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.37") (d #t) (k 0)))) (h "1bsmwfbifz22zk63px1k2j0xkqqpnbxxj83fbwkhzl4ralkcnwvy")))

(define-public crate-sanitize-git-ref-1.0.3 (c (n "sanitize-git-ref") (v "1.0.3") (d (list (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "=1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.37") (d #t) (k 0)))) (h "0gcwl8pn10nc8cqyxxm50d9a1cm35c9wv06khhciwbaygcgbwsii")))

(define-public crate-sanitize-git-ref-1.0.4 (c (n "sanitize-git-ref") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1swhjvi06fssmbq6vwnlhr91ddlgq7rws388794dcmc24ji9qa5l")))

(define-public crate-sanitize-git-ref-1.0.5 (c (n "sanitize-git-ref") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c40n7w8yf3blgqdv2hi6lx3kqgr2svd5k47kk55c8bqgwp7m4c3")))

(define-public crate-sanitize-git-ref-1.0.6 (c (n "sanitize-git-ref") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xigjf1kjdxg9abzm11pps1y4wv2nfbnl55jf8diiz5a1dv4015f")))

(define-public crate-sanitize-git-ref-1.0.7 (c (n "sanitize-git-ref") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "108gpdap1dnra0ymxdj0hsvl1l0ixczn2gs1n8q4vlmk5gdfslbi")))

(define-public crate-sanitize-git-ref-1.0.8 (c (n "sanitize-git-ref") (v "1.0.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "174i2hm5dd21lwr7syq3x8v322pblvljvlq1nvbyijn7i5qzl872")))

(define-public crate-sanitize-git-ref-1.0.9 (c (n "sanitize-git-ref") (v "1.0.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01a4633q24lwndl983ndhrwcfhigsivyjikdij57h90cf8xyym40")))

(define-public crate-sanitize-git-ref-1.0.10 (c (n "sanitize-git-ref") (v "1.0.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "=1.0.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)))) (h "00kzswmsvvk0jfrwdic4hsd3262c279x6kn6h8bw5apjrid7nc33")))

(define-public crate-sanitize-git-ref-1.0.11 (c (n "sanitize-git-ref") (v "1.0.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "proptest") (r "=1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)))) (h "0qja976cmizqvsf5q56dzqfk1iw3iyhfr17743rsr427slzcslmg")))

(define-public crate-sanitize-git-ref-1.0.12 (c (n "sanitize-git-ref") (v "1.0.12") (d (list (d (n "proptest") (r "=1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "=0.3.0") (d #t) (k 2)))) (h "1l9bhbm7vmihdivbqqvl8s35sh8dyndjcbmb0jddp6sl8gz61lfs")))

