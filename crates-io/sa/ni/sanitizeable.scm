(define-module (crates-io sa ni sanitizeable) #:use-module (crates-io))

(define-public crate-sanitizeable-0.1.0 (c (n "sanitizeable") (v "0.1.0") (d (list (d (n "sanitizeable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0bm64fqn5k47m5llh7a3bg7lmajk77lwhn591sv3q3vpi53sza4n")))

(define-public crate-sanitizeable-0.1.1 (c (n "sanitizeable") (v "0.1.1") (d (list (d (n "sanitizeable_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1fnzblbam3dkwcs9qzs4r5fphcfl2brw8phd6pm51halnqphmx2b")))

