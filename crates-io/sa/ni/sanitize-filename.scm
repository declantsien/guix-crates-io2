(define-module (crates-io sa ni sanitize-filename) #:use-module (crates-io))

(define-public crate-sanitize-filename-0.1.0 (c (n "sanitize-filename") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1jbsv4m0z4vcv93jvm1bfmyijnk5kcak2zlwvpanjcwbm2crjrhs")))

(define-public crate-sanitize-filename-0.2.0 (c (n "sanitize-filename") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "176jq0mrg0rn8mq49y6qkz26xywgjq4w51daq3ni4s355q4ligms")))

(define-public crate-sanitize-filename-0.2.1 (c (n "sanitize-filename") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "00a8qw27pskns6nm5j65v8vgn3kyqpvg9f3bv2zhlj7cjkn0zz93")))

(define-public crate-sanitize-filename-0.3.0 (c (n "sanitize-filename") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode-case"))) (k 0)))) (h "0bvwiq368mf5djs0qqxkcqsiwr6hynfswvasnp2ji0h12959665z")))

(define-public crate-sanitize-filename-0.4.0 (c (n "sanitize-filename") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std" "unicode-case"))) (k 0)))) (h "0v19h0928a29v3jl6pawlji9mcpk1rcb0z2615jkkw9qnsyh5i88")))

(define-public crate-sanitize-filename-0.5.0 (c (n "sanitize-filename") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.9") (f (quote ("std" "unicode-case"))) (k 0)))) (h "00r6awai2bs8xsl3fc2fqzby7yy4crlr28s9fi0jsvwfyyx2zmrf")))

