(define-module (crates-io sa ni sanitize-filename-reader-friendly) #:use-module (crates-io))

(define-public crate-sanitize-filename-reader-friendly-0.9.0 (c (n "sanitize-filename-reader-friendly") (v "0.9.0") (h "0zpl5fh9iklvmqcsmvcihzg8sjs9lk1188pqdkh7g7g98sv04pxn")))

(define-public crate-sanitize-filename-reader-friendly-0.9.1 (c (n "sanitize-filename-reader-friendly") (v "0.9.1") (h "1sngryv429fcwhfrd1nmlc4gdm3m7h1gvgfcafwinha3by5kw91w")))

(define-public crate-sanitize-filename-reader-friendly-0.9.2 (c (n "sanitize-filename-reader-friendly") (v "0.9.2") (h "14k8sg2g0z7agj5ph584gcamvc1743c86d2slx3gfsw40nra6kj6")))

(define-public crate-sanitize-filename-reader-friendly-1.0.0 (c (n "sanitize-filename-reader-friendly") (v "1.0.0") (h "0r73ryhn7dmsgz0hxzibic7i5jjml8m9dvw8nkrcczq6f1xkj4jb")))

(define-public crate-sanitize-filename-reader-friendly-1.1.0 (c (n "sanitize-filename-reader-friendly") (v "1.1.0") (h "0dmmr974vf4fi582ph5dn2c3ac5g2pq0kw3x75wf8w59srbgmly8")))

(define-public crate-sanitize-filename-reader-friendly-1.1.1 (c (n "sanitize-filename-reader-friendly") (v "1.1.1") (h "190fxhpb2r74sz5vp5cz0kq6zdd28sczpma09gzqxcp03pj7byy8")))

(define-public crate-sanitize-filename-reader-friendly-2.0.0 (c (n "sanitize-filename-reader-friendly") (v "2.0.0") (h "0bkksg14460vr8264n3dk4nb5ax0br2s64i038wxn0yx87n87p0b")))

(define-public crate-sanitize-filename-reader-friendly-2.1.0 (c (n "sanitize-filename-reader-friendly") (v "2.1.0") (h "0j0k9c8dh94jpp608ayga0hz0v8fcp0pxg2s3pfq4v8b3zrfvnpg")))

(define-public crate-sanitize-filename-reader-friendly-2.1.1 (c (n "sanitize-filename-reader-friendly") (v "2.1.1") (h "0plpxwcbdvpams9pqcixgr8v8lrznk54alfnsgxkd3wjpmnj34rc")))

(define-public crate-sanitize-filename-reader-friendly-2.2.0 (c (n "sanitize-filename-reader-friendly") (v "2.2.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "139h52cyc6ryrigqdx7hd3kp1723r4rns2bsqq6ddvvlcxir3m1w")))

(define-public crate-sanitize-filename-reader-friendly-2.2.1 (c (n "sanitize-filename-reader-friendly") (v "2.2.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)))) (h "1gs69iihz2c9p9gvw7pb4l9dxa1rfdp43jaahhwb5x46mhdffl5p")))

