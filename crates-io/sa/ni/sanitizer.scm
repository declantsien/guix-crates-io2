(define-module (crates-io sa ni sanitizer) #:use-module (crates-io))

(define-public crate-sanitizer-0.1.0 (c (n "sanitizer") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phonenumber") (r "^0.3.1") (d #t) (k 0)) (d (n "sanitizer_macros") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lgrczfgc0kdcjxk934ikq11yzk585imw6zjlca5hyk7q0waiynl") (f (quote (("derive" "sanitizer_macros") ("default" "derive"))))))

(define-public crate-sanitizer-0.1.2 (c (n "sanitizer") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phonenumber") (r "^0.3.1") (d #t) (k 0)) (d (n "sanitizer_macros") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1bp8pnkal19p6a9kbcsxv3yvqnlyl0saa4kcs3c9qmdws8gax2n2") (f (quote (("derive" "sanitizer_macros") ("default" "derive"))))))

(define-public crate-sanitizer-0.1.4 (c (n "sanitizer") (v "0.1.4") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phonenumber") (r "^0.3.1") (d #t) (k 0)) (d (n "sanitizer_macros") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0h5cxibm4j31px0wz9s599d4bskmygwhk51avc8ymzclk06nhp2q") (f (quote (("derive" "sanitizer_macros") ("default" "derive"))))))

(define-public crate-sanitizer-0.1.5 (c (n "sanitizer") (v "0.1.5") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phonenumber") (r "^0.3.1") (d #t) (k 0)) (d (n "sanitizer_macros") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "17yfii1wpq8dp7ybdc17bvmcrldywj5lqp7bwidz071mr2358s0g") (f (quote (("derive" "sanitizer_macros") ("default" "derive"))))))

(define-public crate-sanitizer-0.1.6 (c (n "sanitizer") (v "0.1.6") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phonenumber") (r "^0.3.1") (d #t) (k 0)) (d (n "sanitizer_macros") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1gakns2g8nnc87xp73y07jdv93b0v6fybi2f77wznmd7jr6a37ii") (f (quote (("derive" "sanitizer_macros") ("default" "derive"))))))

