(define-module (crates-io sa ni sanitise-file-name) #:use-module (crates-io))

(define-public crate-sanitise-file-name-1.0.0 (c (n "sanitise-file-name") (v "1.0.0") (d (list (d (n "tinyvec_string") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tinyvec_string") (r "^0.3") (f (quote ("rustc_1_55"))) (d #t) (k 2)))) (h "04dr9ivhjxpmph5g06kqydxs4hgvfnzyq14ggspbi5ibjycn5lqr") (f (quote (("std" "alloc") ("default" "std") ("const-fn-trait-bound") ("alloc"))))))

