(define-module (crates-io sa ni sanity_rs_client) #:use-module (crates-io))

(define-public crate-sanity_rs_client-0.1.0 (c (n "sanity_rs_client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("multipart" "default" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1icsziggcxrl83n22rls5yili276jyy23iv97kiww6lxqmwqjbkm") (r "1.60.0")))

