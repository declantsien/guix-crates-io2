(define-module (crates-io sa ni sanity) #:use-module (crates-io))

(define-public crate-sanity-0.1.0 (c (n "sanity") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "006nfmyv7wn9m9iknjyi7scbqc0z9x4mwxsy39zy0047njf3g2vf")))

(define-public crate-sanity-0.1.1 (c (n "sanity") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1xnb98g83njqr08q8nqxc4863wwpxhzh156azcsk8gf4n4d09cky")))

