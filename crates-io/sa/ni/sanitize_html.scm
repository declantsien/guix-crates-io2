(define-module (crates-io sa ni sanitize_html) #:use-module (crates-io))

(define-public crate-sanitize_html-0.2.0 (c (n "sanitize_html") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.3") (d #t) (k 0)))) (h "0mdf6q3bsvcwlhyll94niqrnmrhyc56x59xjr0hb4n29bbysry7l")))

(define-public crate-sanitize_html-0.3.0 (c (n "sanitize_html") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "1ziavc5mpprr09pb55xkq8kh1w1lvckdj21q9gcqnn6967wg799g")))

(define-public crate-sanitize_html-0.3.1 (c (n "sanitize_html") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "00dqkjwcwkh5w8qmswg6h048j3d7xx85lj0wgnqqvbwg60pjmhkf")))

(define-public crate-sanitize_html-0.3.2 (c (n "sanitize_html") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "html5ever") (r "= 0.22.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)))) (h "0cgmdgr837jrlj536fgrpc2rlv8b5x2q1w3sw1rr3bfx4jvw379d")))

(define-public crate-sanitize_html-0.4.0 (c (n "sanitize_html") (v "0.4.0") (d (list (d (n "html5ever") (r "^0.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jnhc48ycqhkfvwwmifdfilh0b8bgga94ghcf4ajlsgq2503l0r8")))

(define-public crate-sanitize_html-0.5.0 (c (n "sanitize_html") (v "0.5.0") (d (list (d (n "html5ever") (r "^0.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0d250h35sjaz5hbmb093dmr5kr7bp22zqck7nn5rj01fqmvz799g")))

(define-public crate-sanitize_html-0.5.1 (c (n "sanitize_html") (v "0.5.1") (d (list (d (n "html5ever") (r "^0.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c7hmv9ax87aklcb9za5xvp0cm37xiwd28wjhwxlz3nqkysyyl1h")))

(define-public crate-sanitize_html-0.6.0 (c (n "sanitize_html") (v "0.6.0") (d (list (d (n "html5ever") (r "^0.24") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a309938pnq84vmixxg21ifg66x0ys3m18i8kzf5nsd6bnmxyas2")))

(define-public crate-sanitize_html-0.7.0 (c (n "sanitize_html") (v "0.7.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ay5c5rgifw1pndry4ka1cg5ma0f53njak2blihnjp4a313fmmrc")))

(define-public crate-sanitize_html-0.8.0 (c (n "sanitize_html") (v "0.8.0") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "019nadpbsvgc3nr9mv7dbkq6m281rljycdcia86a4ln2lngi3ipd")))

