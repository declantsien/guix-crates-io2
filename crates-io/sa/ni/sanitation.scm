(define-module (crates-io sa ni sanitation) #:use-module (crates-io))

(define-public crate-sanitation-0.0.0 (c (n "sanitation") (v "0.0.0") (h "1jy4yfdxzzmrby6czii4jkz95h2mjg0ikqkbl2lf7p061w1mca93")))

(define-public crate-sanitation-0.0.1 (c (n "sanitation") (v "0.0.1") (h "107x6rfkwv0xy9lrjpp14ry6611cdgfm5llbxbc8d7b6bdc7k8qa")))

