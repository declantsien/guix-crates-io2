(define-module (crates-io sa ni sanitizer_macros) #:use-module (crates-io))

(define-public crate-sanitizer_macros-0.1.0 (c (n "sanitizer_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0i63d95aani808wisa8a2wapv88l60g5q1kakpnvsqznpbpyvh13")))

(define-public crate-sanitizer_macros-0.2.0 (c (n "sanitizer_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ww7xc3aw4ivmh8v6kyd10s4z6risgrpm9fd38yv33dbxbl6bim4")))

(define-public crate-sanitizer_macros-0.2.1 (c (n "sanitizer_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fc0wqxcxpw0sh5nbf6xz26ynsr74k2h3xql2qk5250xa5760bsq")))

(define-public crate-sanitizer_macros-0.2.2 (c (n "sanitizer_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ifh5d69yqry5l30kgx5rnn4wrc2lrm1rcgjjddglsbdndfv855m")))

