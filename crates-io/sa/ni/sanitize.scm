(define-module (crates-io sa ni sanitize) #:use-module (crates-io))

(define-public crate-sanitize-0.1.0 (c (n "sanitize") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1fblssnn2nds0rqnpvp59s4zrz1w03n6vly3mxm4pklwzgnnc003")))

