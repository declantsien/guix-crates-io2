(define-module (crates-io sa ni sanitise) #:use-module (crates-io))

(define-public crate-sanitise-0.1.0 (c (n "sanitise") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "01x43jl67ja3akacj026kyhymr5ifbsacqdxjpnsg1cj5r8ylnaj")))

(define-public crate-sanitise-0.1.1 (c (n "sanitise") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0mcfzz19wfjsxv4ashyn312rhc0jkjd91x1ynw0ls0rqm2j2ilmr")))

(define-public crate-sanitise-0.1.2 (c (n "sanitise") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1xgg9psc7p6mzajh7ihc6a2iw9l8d6f5fbylmmnxnzdhvbh1ir8x")))

(define-public crate-sanitise-0.1.3 (c (n "sanitise") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1hcjb7gggbhadhpc7r2nlpbrxw6x8030iwhalanpd5v0bhfnrgf3")))

(define-public crate-sanitise-0.2.0 (c (n "sanitise") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0711i8hacdrz5swzzd5r5avgn3xchvkcd0i7a4vk2kzzvglagmbl") (f (quote (("benchmark"))))))

(define-public crate-sanitise-0.2.1 (c (n "sanitise") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0mrnmmgw5bcrxlyccpdn2s1jmnfjhxdd6g5bjjxx5fxsyns3apmh") (f (quote (("benchmark"))))))

(define-public crate-sanitise-0.3.0 (c (n "sanitise") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "17l449p977zg4d75r3s1m0ny56w223nl3wi2qrnxa3yjr5szmy5i") (f (quote (("benchmark"))))))

(define-public crate-sanitise-0.4.0 (c (n "sanitise") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "09n20p44sqbam4k5zymchb55n1qip0pzkgnhqqx9wgnyiicg6gdi") (f (quote (("benchmark"))))))

