(define-module (crates-io sa rg sargparse) #:use-module (crates-io))

(define-public crate-sargparse-0.1.0 (c (n "sargparse") (v "0.1.0") (h "0rpszqczw84jm92l4m6da1i6nzlw5mxfgd55l0f34zkdq75zd4lv")))

(define-public crate-sargparse-0.0.2 (c (n "sargparse") (v "0.0.2") (h "1kdjhbabbgigp5saq6qh0s4q4r4ic073hg4qgrlbr8dckwgznx2y")))

(define-public crate-sargparse-0.2.0 (c (n "sargparse") (v "0.2.0") (h "0r0rdaqdnxk8874w864imwjhzs0nk3793svg999gil5nyzmnvw95")))

(define-public crate-sargparse-0.2.1 (c (n "sargparse") (v "0.2.1") (h "0qdmzl537dp18w35y8k2zj8idhyv9l5iq2i6ss6194ibazsp49ra")))

(define-public crate-sargparse-0.2.2 (c (n "sargparse") (v "0.2.2") (h "07jgzlymayy6n4bq3n9m9xs33xh5ml5fc78fca1n5bfky3dq6zjx")))

