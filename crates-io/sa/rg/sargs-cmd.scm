(define-module (crates-io sa rg sargs-cmd) #:use-module (crates-io))

(define-public crate-sargs-cmd-0.1.0 (c (n "sargs-cmd") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwv7q5ybr4q3lghllh5j3zs4mlajyslqp1snvgr3fpsryabwg5v")))

(define-public crate-sargs-cmd-0.1.1 (c (n "sargs-cmd") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1saah5l6j998b3xkb0hhz44dqzxbb3zis5jmpxzqiphikhpsswb2")))

(define-public crate-sargs-cmd-0.1.2 (c (n "sargs-cmd") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bksd76ra877lxrz4l5vnly4pd57422ljzvzfc6cxnlyw7zjih9l")))

