(define-module (crates-io sa rg sarge) #:use-module (crates-io))

(define-public crate-sarge-2.0.0 (c (n "sarge") (v "2.0.0") (h "0x1001r90cnp68g8lycnr0xh34fp5g0d635w4nlg4qgsw7hqr6i6")))

(define-public crate-sarge-2.0.2 (c (n "sarge") (v "2.0.2") (h "0yydxn5vmbj892y5alrf89jhb2ddb1wbvinlc85vwcwqh5nilhxv")))

(define-public crate-sarge-2.0.3 (c (n "sarge") (v "2.0.3") (h "1imh2yhph3qcwg801pr4l80ramw9n3h0xyj15g2wr716l9v90kng")))

(define-public crate-sarge-3.0.0 (c (n "sarge") (v "3.0.0") (h "0kv6gjvpp5ja01zlb4l5bzx6igsmcn4p65wmllvb9x2c83lkxks2")))

(define-public crate-sarge-3.0.1 (c (n "sarge") (v "3.0.1") (h "14n8q847fk3l6chwisdkd9kppx7dpghgdlv6y365b3v4mkz89ifa")))

(define-public crate-sarge-4.0.0 (c (n "sarge") (v "4.0.0") (h "1klp0vjnwkrvnifwvd7npd0cpcpmyz0bcj0fjjhhbds08gvfy96v")))

(define-public crate-sarge-4.0.1 (c (n "sarge") (v "4.0.1") (h "01vkwn1gi01245m8p6qrs622c7gb54ahh0k1llf345j47v4hys3h")))

(define-public crate-sarge-4.0.2 (c (n "sarge") (v "4.0.2") (h "0h34kwbpswv7g4f9fxiry5q11gaz4wadxq48qdf2sw9pd2afksca")))

(define-public crate-sarge-5.0.0 (c (n "sarge") (v "5.0.0") (h "1g3w6hl2493fbny2gxd5jns5xcy2h8xqwwr9ygn2m8wwpqd36ic2")))

(define-public crate-sarge-5.0.2 (c (n "sarge") (v "5.0.2") (h "1z6idmj7cikra432j86l0jljsil4n9j6sr9d8filzpgi4yfdm8by")))

(define-public crate-sarge-6.0.0 (c (n "sarge") (v "6.0.0") (h "1qb5hpflqwharm3k6w8w3czvl6airz9kw0j68xmr74pmi9by7jk3") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-6.0.1 (c (n "sarge") (v "6.0.1") (h "03ck9rgcy79j68rqwyqsz3xm7l0rvvxvi13gm5gz2sjkz8shsjk2") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-6.0.2 (c (n "sarge") (v "6.0.2") (h "0hjgk8wndpwdpij1br5j4g0473mg0ydnyvrl3h7qh1idck2fp7q4") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7.0.0 (c (n "sarge") (v "7.0.0") (h "0sx6dyifzyaf3bbqhb6gy1gj4xw2sri1pwaa4hazaa9calp5a92l") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7.0.1 (c (n "sarge") (v "7.0.1") (h "1gyn47sgzjrkfjyc6vh093qk824338pa2nqdmq9fyzq28aqx1gig") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7.0.2 (c (n "sarge") (v "7.0.2") (h "157nz9fl14kz9l76zim7qg32ck2mp8sv0smcwn1xwzdm6azgxy3q") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7.2.0 (c (n "sarge") (v "7.2.0") (h "0xc4cvlvn4ryvb6ky4kggblbhaxcgfhdj1d5agp556s43wzn8byp") (f (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7.2.1 (c (n "sarge") (v "7.2.1") (h "1qnrcbg4743w9lzaykdria71m9h6lnpxnhh4hd3xsbyaj17iv08g") (f (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7.2.2 (c (n "sarge") (v "7.2.2") (h "09z5x1ahp2knwbb63klm9l3sd7v4nd4p65h3m48ll8f63rm5cm55") (f (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7.2.3 (c (n "sarge") (v "7.2.3") (h "0j40zrfyd9frwbdyhdbr2a2cb11c1zfsharxffm2vpi944wcb3a1") (f (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7.2.4 (c (n "sarge") (v "7.2.4") (h "0vb9zwhz1fg1az9rn2xaba4dlv940n70k73vny29zd9ic9ypzr95") (f (quote (("macros") ("help") ("default" "help" "macros"))))))

