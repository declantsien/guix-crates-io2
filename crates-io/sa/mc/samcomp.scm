(define-module (crates-io sa mc samcomp) #:use-module (crates-io))

(define-public crate-samcomp-0.1.0 (c (n "samcomp") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1dz27c9cgvxiq46cq72n4psc3bbk8pxppwjs3xkzcdglrx5c3701")))

(define-public crate-samcomp-0.1.1 (c (n "samcomp") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0864j479h6iw50hms0k3rkl4ffn8n35i5cvb9yg01dvhyy3a5izz")))

(define-public crate-samcomp-0.1.2 (c (n "samcomp") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1qla00adsi198yhl2rnrz72wfnxfardhqyynydar76f2l1qhvm1c")))

(define-public crate-samcomp-0.1.3 (c (n "samcomp") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1w79fzfbqsq08yjp19cmkjcgy93cr2lwlaj25q88xyh35mlwr77y")))

