(define-module (crates-io sa is sais) #:use-module (crates-io))

(define-public crate-sais-0.0.1 (c (n "sais") (v "0.0.1") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "1i404vdxmay9hjpqp14nrjrqcak1752mww3dl4y4g88krr9hspw6")))

(define-public crate-sais-0.0.2 (c (n "sais") (v "0.0.2") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "03lz9dkis7izc0yk2w5bnf8lqipsrlqc0wz7zkdihq478d2gg0sc")))

