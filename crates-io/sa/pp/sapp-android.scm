(define-module (crates-io sa pp sapp-android) #:use-module (crates-io))

(define-public crate-sapp-android-0.1.0 (c (n "sapp-android") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0svgmdjmrks5m863fgsaa53jrl3gm21zyjznzzqhki6380bixl06")))

(define-public crate-sapp-android-0.1.1 (c (n "sapp-android") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w5w3nlkk180n27qf0v7gzk90h60jh3mpvpih27c7lwmg727lm6m")))

(define-public crate-sapp-android-0.1.2 (c (n "sapp-android") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ipnagfxdbg6ibln7684anqyr4bkbn1ggiqcnnzglsj0h73l1b3p")))

(define-public crate-sapp-android-0.1.3 (c (n "sapp-android") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03vg936js69jcyl94sdmilmbka61p0vym1z1nwy5wm6mfwgkgf5p")))

(define-public crate-sapp-android-0.1.4 (c (n "sapp-android") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xqr70hvmh1w6a62xdiy76wryc1f11p3ghzfqaamjb487w3x8iw0")))

(define-public crate-sapp-android-0.1.5 (c (n "sapp-android") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14rirnvzqpfaj79dbzxj99fgcprma92mbsiik69bl3a5z4w35z1c")))

(define-public crate-sapp-android-0.1.6 (c (n "sapp-android") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1yc49x6zq02hakqsvh9076r2sry5bd1bq1v2y6qzw6hzpmiw6myb")))

(define-public crate-sapp-android-0.1.7 (c (n "sapp-android") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0wnnmhzpm4vq6iiwsg9zbv6ifa0bfkhkpfym47myqgglniwqha1d")))

(define-public crate-sapp-android-0.1.8 (c (n "sapp-android") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "090p7v059j5mq50cj6nsjhpxz2hkm9h8alkq74hq69xscbs82jjs")))

(define-public crate-sapp-android-0.1.9 (c (n "sapp-android") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1v07632rzawq6a05ivyzdy8di99q275q0f0c60njsrxhx7x8gmlj")))

(define-public crate-sapp-android-0.1.10 (c (n "sapp-android") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0h09dsp0czg6l2mcl5if9nrfmjv9bl29rb4kyqrwa09carphw3cw")))

(define-public crate-sapp-android-0.1.11 (c (n "sapp-android") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0794sfvlyq2hn4dfwphlqhd9jfgndm983k3qqhpqpbaf2avv47mj") (y #t)))

(define-public crate-sapp-android-0.1.12 (c (n "sapp-android") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1wkh7sywq4gsc2w0a3iy5bsjp8xfc1hld0ahrksqw0i2pn76gynb") (y #t)))

(define-public crate-sapp-android-0.1.13 (c (n "sapp-android") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "06kina1x35l1xmdj12xriwcf2z1fv8hw747rf8djkw1z8l9lkjh5") (y #t)))

(define-public crate-sapp-android-0.1.14 (c (n "sapp-android") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "17vainj9m6gws13mzmm38s7dpirmpa7kmjjw1r58ff7wv344vi97")))

(define-public crate-sapp-android-0.1.15 (c (n "sapp-android") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndk-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0apkcxpz4hnjrgx3y20pwhhjbg62qp0dwi1cbxvh6ds50p75zscm")))

