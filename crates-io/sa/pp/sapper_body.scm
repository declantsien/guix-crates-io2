(define-module (crates-io sa pp sapper_body) #:use-module (crates-io))

(define-public crate-sapper_body-0.1.0 (c (n "sapper_body") (v "0.1.0") (d (list (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1gn41l9sa8vnwgyznp3hlgsy5my9h2ai580v7drpyxvhv9fg9v38")))

(define-public crate-sapper_body-0.1.1 (c (n "sapper_body") (v "0.1.1") (d (list (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "07qf79s4gjphrxf8yrdkaii5v9s5m4kmcmdlv0bc0ipvhn0vxkg3")))

(define-public crate-sapper_body-0.2.0 (c (n "sapper_body") (v "0.2.0") (d (list (d (n "sapper") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1k295c9s120112fy4vk6qrx0j60fhs33bphng9qvh93f7ywy655j")))

