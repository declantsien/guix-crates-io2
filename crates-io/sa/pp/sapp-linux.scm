(define-module (crates-io sa pp sapp-linux) #:use-module (crates-io))

(define-public crate-sapp-linux-0.1.0 (c (n "sapp-linux") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kkqp23zh3n4l3xl1m4jk4l6jc9bx5by4aspxfliq0h5hyp5v6ph")))

(define-public crate-sapp-linux-0.1.1 (c (n "sapp-linux") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gf151rbr39i8x13kw0ly76mqw70yq8ncwxhh9mx4n3f2hkg22v2")))

(define-public crate-sapp-linux-0.1.2 (c (n "sapp-linux") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yivb05zd1mb8y4q6nzcffzsb3mwagvrcbklbf8vvz54gqm4yi2c")))

(define-public crate-sapp-linux-0.1.3 (c (n "sapp-linux") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fbsin3qbdb9nh9k1vb5d4kwjyv3cp73bx047h14xn48a6rv0iwa")))

(define-public crate-sapp-linux-0.1.4 (c (n "sapp-linux") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "168n577zhw6d10j2a34d6018b4y2bb85l7k4nasvis9vwm7p4dyw")))

(define-public crate-sapp-linux-0.1.5 (c (n "sapp-linux") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vj72y1h0xkn9zvagvgfg7knwnlwh1ins4vkmj0i6i3b2bil7i8l")))

(define-public crate-sapp-linux-0.1.6 (c (n "sapp-linux") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1604nx746bdfwgrl09hls2sf7pq401c8v86ycgkx0irn0df0nxjq")))

(define-public crate-sapp-linux-0.1.7 (c (n "sapp-linux") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qfdbc63sdc0j31ap29h2ba4ndd8crbn95qik4nyadw9bqn0c3h0")))

(define-public crate-sapp-linux-0.1.8 (c (n "sapp-linux") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s9symxw2wqvvy2v0znb43syp4g9h02l3gswgjr6lsa05pj5qdqz")))

(define-public crate-sapp-linux-0.1.9 (c (n "sapp-linux") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m28fngkdw312rsclkr00fsiapk7kc13xg3dzqf09p66yjm06j5f")))

(define-public crate-sapp-linux-0.1.10 (c (n "sapp-linux") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jgiwg4ikpq0ahpydxknffmq3g6b55jmsqbm7c73jmg7nidq1bv3")))

(define-public crate-sapp-linux-0.1.11 (c (n "sapp-linux") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j6qn5vpia35m34fwm4rwlhk6dyaw070igamssv6il28wgkjhhq1")))

(define-public crate-sapp-linux-0.1.12 (c (n "sapp-linux") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14my6y2n9yqwmbsizb6kfjm1jjjd963234cp17c26a7vxyknzdi2")))

(define-public crate-sapp-linux-0.1.13 (c (n "sapp-linux") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hpi5awi25mqa8fl3mqbs088hy1y6dc6lqly9ma64p4m2602znxv")))

(define-public crate-sapp-linux-0.1.14 (c (n "sapp-linux") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gaw94ab4p976472rxa3mm38b3icd0br2sh4b0fz7n41fw552wfw")))

(define-public crate-sapp-linux-0.1.15 (c (n "sapp-linux") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18n8pj1wi7a6pphlzxkyc8mc47jrszyd9qgjvy9jdf5wjnkq0vm6")))

