(define-module (crates-io sa pp sapp-windows) #:use-module (crates-io))

(define-public crate-sapp-windows-0.1.0 (c (n "sapp-windows") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vgbs9405qnh8gn8dhaaxy7n0vpvr6hynzksvjal1xrqwh3gip25")))

(define-public crate-sapp-windows-0.2.0 (c (n "sapp-windows") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01nvhr9ck1sg8ja0n8mdbgbqc0i5v2sp2sv8bmfyq91m8sr0k8cq")))

(define-public crate-sapp-windows-0.2.1 (c (n "sapp-windows") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r0icfhpq4b26fgcs0f8g3sq0hbp6chi5gs14fzmhyksia72wdi1")))

(define-public crate-sapp-windows-0.2.2 (c (n "sapp-windows") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "007bg7il10594szjs1n2a9a3yrn060v5di2108w147mf0xhzk8r7")))

(define-public crate-sapp-windows-0.2.3 (c (n "sapp-windows") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zf5l2lq6a9icapgdmndvda5yzmdjrxinhg0mibasl5j7fr1506x")))

(define-public crate-sapp-windows-0.2.4 (c (n "sapp-windows") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hl3dsnwiz1bcl9qi7lvwlwkfk91pzcl1w39qdv5qxdy0ljp3wr5")))

(define-public crate-sapp-windows-0.2.5 (c (n "sapp-windows") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k4034ivz54vycv9zjv4dxmwg6vz7k0rwjm653f6fmdd8mgd9adg")))

(define-public crate-sapp-windows-0.2.6 (c (n "sapp-windows") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x27raxkj7wh36fnbyy4wagvk7s4m599dl4fi1wbv7vfb415sipq")))

(define-public crate-sapp-windows-0.2.7 (c (n "sapp-windows") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b3lcwd9f4h9zz2d6951889xfwb42h40z8jlasrc4z67izdg86xr")))

(define-public crate-sapp-windows-0.2.8 (c (n "sapp-windows") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k0nicx7d7x2bc1pqc3l4jad2vwj5458iw2js2ba10p9drhwhyki")))

(define-public crate-sapp-windows-0.2.9 (c (n "sapp-windows") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gv25n35l983f7da5j8nmfaxfz0xwhwpmfbzlk0m1ir1pgisrrv8")))

(define-public crate-sapp-windows-0.2.10 (c (n "sapp-windows") (v "0.2.10") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx"))) (d #t) (k 0)))) (h "1ipf53p6s6s6h78ch53h1i0rlkmhqacwk9jbqxb1z0g2ran6k904")))

(define-public crate-sapp-windows-0.2.11 (c (n "sapp-windows") (v "0.2.11") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (d #t) (k 0)))) (h "1nvzsh8kc6m2kl66qad0vlk53wqbp6bqfcryp68z6spy3m8gwvml")))

(define-public crate-sapp-windows-0.2.12 (c (n "sapp-windows") (v "0.2.12") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (d #t) (k 0)))) (h "11y5r4fc67rjmnz1wbifd6cshc8n96sn1h4fbcccpdb2np5fypy7")))

(define-public crate-sapp-windows-0.2.13 (c (n "sapp-windows") (v "0.2.13") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (d #t) (k 0)))) (h "02i1hks5ahax0k2nf9c0ifbk1ap72l2w31617gkqzxlbqslkl0w4")))

(define-public crate-sapp-windows-0.2.14 (c (n "sapp-windows") (v "0.2.14") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (d #t) (k 0)))) (h "1jxy68415clhmkfqg08sg5815y0jgw33s2fmrlkib404clv2rgvl")))

(define-public crate-sapp-windows-0.2.15 (c (n "sapp-windows") (v "0.2.15") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "031hkz5qkrj80794k7z6pjkv3j0y3wpv3rssnz7y2ypdl6zndi7a")))

(define-public crate-sapp-windows-0.2.16 (c (n "sapp-windows") (v "0.2.16") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "0mhfjkpiqhy8mbcqbhcpg17qsly64xrin769ghwzhxhzchgjvynq")))

(define-public crate-sapp-windows-0.2.17 (c (n "sapp-windows") (v "0.2.17") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "1lk44nck5hxh5l7x4fwf3lgjb2dgamrfx2y5yymqnpbz5c9b46r5")))

(define-public crate-sapp-windows-0.2.18 (c (n "sapp-windows") (v "0.2.18") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "1yhr0yy1gyhdb7jhmkd9zfwvldkcmq5gq5cqablyak5m7jcfrynq")))

(define-public crate-sapp-windows-0.2.19 (c (n "sapp-windows") (v "0.2.19") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "171wavh0blw0491hir6z669lfwskqx27py5cpgy2yxc28sms9sgk")))

(define-public crate-sapp-windows-0.2.20 (c (n "sapp-windows") (v "0.2.20") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (d #t) (k 0)))) (h "13hkm1118gqmdv9m5wl8d4d6lfr0wmxj65ywybsspp0zb4dr53q8")))

(define-public crate-sapp-windows-0.2.21 (c (n "sapp-windows") (v "0.2.21") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage" "shellapi"))) (d #t) (k 0)))) (h "1x9in6mgkjwiaz9s0kpnmgn2daqfgmy10w6qdl88mzsxrlldd5mq")))

