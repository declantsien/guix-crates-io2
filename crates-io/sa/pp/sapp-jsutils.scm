(define-module (crates-io sa pp sapp-jsutils) #:use-module (crates-io))

(define-public crate-sapp-jsutils-0.1.0 (c (n "sapp-jsutils") (v "0.1.0") (h "0cx0656mzm1x6dsji654960w2dcvxgc37n1cknn4hy926jprq83d")))

(define-public crate-sapp-jsutils-0.1.1 (c (n "sapp-jsutils") (v "0.1.1") (h "1d9nv1fgz09nfjmbyyk77vkzh5mc0wv5qjf5mvr9imababk5h69g")))

(define-public crate-sapp-jsutils-0.1.2 (c (n "sapp-jsutils") (v "0.1.2") (h "11n54vq5xljpyvsls0h7wkc2rf3gvmwds2kainfgf25sshqadhzp")))

(define-public crate-sapp-jsutils-0.1.3 (c (n "sapp-jsutils") (v "0.1.3") (h "02i8xh3vrramgkb71wbn3277qs6b0d562sqfmwzpfn5kz3h7cdrq")))

(define-public crate-sapp-jsutils-0.1.4 (c (n "sapp-jsutils") (v "0.1.4") (h "0r8dvkkkjf7bri1li6xlzz99cs31mhj7xh4qzdk213hz6n557dlm")))

(define-public crate-sapp-jsutils-0.1.5 (c (n "sapp-jsutils") (v "0.1.5") (h "1m11pd33k3mshf489japwcfmsyz7wyzli0hp19nglcblhsxbm2mv")))

