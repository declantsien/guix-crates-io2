(define-module (crates-io sa pp sapper_tmpl) #:use-module (crates-io))

(define-public crate-sapper_tmpl-0.1.0 (c (n "sapper_tmpl") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "tera") (r "^0.10.5") (d #t) (k 0)))) (h "053842d5mcm44l0hpi54i5dzq246z9ky1d6h1jh54qm04s4kpa1n")))

(define-public crate-sapper_tmpl-0.1.1 (c (n "sapper_tmpl") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "tera") (r "^0.10.5") (d #t) (k 0)))) (h "0asy9mpyakppzrqjr752wqcll1alaqfh38v59nbg9sp1h82psjpq")))

(define-public crate-sapper_tmpl-0.1.2 (c (n "sapper_tmpl") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "tera") (r "^0.10") (d #t) (k 0)))) (h "0bp2phkh5pcihlj1dbgfi294z7i45biz791iaskvpwymljvkyrf5") (f (quote (("monitor" "notify") ("default"))))))

(define-public crate-sapper_tmpl-0.2.0 (c (n "sapper_tmpl") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)))) (h "0ijgs7m774wp1c5wrpfzjvpmlv00h6pmfzk4bwmmwkabc6pplrm0") (f (quote (("monitor" "notify") ("default"))))))

