(define-module (crates-io sa pp sapper_session) #:use-module (crates-io))

(define-public crate-sapper_session-0.1.0 (c (n "sapper_session") (v "0.1.0") (d (list (d (n "cookie") (r "^0.8.1") (d #t) (k 0)) (d (n "sapper") (r "^0.1") (d #t) (k 0)))) (h "0vxws1jm02fvq4h3h4wfg18141r7pi1bm4kcabjlylsw4zgzg085")))

(define-public crate-sapper_session-0.1.1 (c (n "sapper_session") (v "0.1.1") (d (list (d (n "cookie") (r "^0.8.1") (d #t) (k 0)) (d (n "sapper") (r "^0.1") (d #t) (k 0)))) (h "0qghz8jysvva1dskx5k73fdxkp202nfm5ndy6j4vwzdr6rbx6jfc")))

(define-public crate-sapper_session-0.1.2 (c (n "sapper_session") (v "0.1.2") (d (list (d (n "cookie") (r "^0.10.1") (d #t) (k 0)) (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "146xqgdhw7j356z62zl0rpr1cywjc94mp9gv77f35gzvrl3w0c93")))

(define-public crate-sapper_session-0.2.0 (c (n "sapper_session") (v "0.2.0") (d (list (d (n "cookie") (r "^0.10") (d #t) (k 0)) (d (n "sapper") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1z2d5ild97pa0sd2izjv4yc139gaj3fc9m9fw1zz706x0dqgniga")))

