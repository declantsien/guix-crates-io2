(define-module (crates-io sa pp sapp-darwin) #:use-module (crates-io))

(define-public crate-sapp-darwin-0.1.0 (c (n "sapp-darwin") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ic4kkrq9jg71v7fmqs3mybxywj90fk5i11lwl9pg4r7nbdn03i5")))

(define-public crate-sapp-darwin-0.1.1 (c (n "sapp-darwin") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "000jkkdlkic5kr0w4jrq4lk9mrdps8crav83ga2jm0rfva0v361g")))

(define-public crate-sapp-darwin-0.1.2 (c (n "sapp-darwin") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wybzyr1ryr5w7g9vcr3cha15mp5wq6rmbcj31vdw0lk6x57c7h1")))

(define-public crate-sapp-darwin-0.1.3 (c (n "sapp-darwin") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05gxiyiw1n87i0mfmk6gqbrdlypksihk2hb9rcpd6zl0xkzmxsl7")))

(define-public crate-sapp-darwin-0.1.4 (c (n "sapp-darwin") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03ffvcd3x7xaspkagzilkp3yvqdfph7fn4b2ycvxw7pma9wqzmfx")))

(define-public crate-sapp-darwin-0.1.5 (c (n "sapp-darwin") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "061vvkqnj8ad20b7avab2kxpwc0zd9ji42mrsqm6q8rp56i3h3fg")))

(define-public crate-sapp-darwin-0.1.6 (c (n "sapp-darwin") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0caz2hdaygxyxlnn9fxg579qysxsdx6ykkgi2fm6hx1hbx2f4403")))

(define-public crate-sapp-darwin-0.1.7 (c (n "sapp-darwin") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "02aq1b1fc85l589kjskx5h43h93h201fw343p8ms9n9mvikli1zx")))

(define-public crate-sapp-darwin-0.1.8 (c (n "sapp-darwin") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0x732hc99gmgc3y19nl39mk3f2y2ai6zlxidz9737g46yilj6a7i")))

(define-public crate-sapp-darwin-0.1.9 (c (n "sapp-darwin") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0pcrx03z9ks8krx7bwgwrwpq9adgplha3cb0wxfij5ys8gnkmkn1")))

