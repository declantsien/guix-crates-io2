(define-module (crates-io sa pp sapp-dummy) #:use-module (crates-io))

(define-public crate-sapp-dummy-0.1.0 (c (n "sapp-dummy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x3188b4i3svz6wcfaj47jl7ngs3gd49caw9qh08hlzai2dann9k")))

(define-public crate-sapp-dummy-0.1.1 (c (n "sapp-dummy") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c6zvm5qf4smfvc8rj1k0dr1ilaxvclnv2paj25mp2xd07d77rvg")))

(define-public crate-sapp-dummy-0.1.2 (c (n "sapp-dummy") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mng22mlaczh3pf45y2qz2ajxq1vv01ivy6nz2k5mi16c2bvmz4k")))

(define-public crate-sapp-dummy-0.1.3 (c (n "sapp-dummy") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05zp28g3j6n46bsxckz99yxkvd63qpzvc2xg0jvb88699yq8ch44")))

(define-public crate-sapp-dummy-0.1.4 (c (n "sapp-dummy") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17cr4aaqa7f6bxf1khmfc7kvh4jqbm5nm1yggvscwq5jxdybfmx3")))

(define-public crate-sapp-dummy-0.1.5 (c (n "sapp-dummy") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y542m92hmzd3l8cfam8k26jn04imawnvii7rawq5imnllkavwb6")))

