(define-module (crates-io sa pp sapper_logger) #:use-module (crates-io))

(define-public crate-sapper_logger-0.1.0 (c (n "sapper_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1a1myjb157cgbmnkj793l9dcik8s503fh4ajcl685qwr5xsrrrgq")))

(define-public crate-sapper_logger-0.1.1 (c (n "sapper_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "12gan5pwr01m2l0nzgi22x55ppqxcgzm60b3byncyh1y1ncqldwl")))

(define-public crate-sapper_logger-0.2.0 (c (n "sapper_logger") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sapper") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1kvc6rdzwa7yvapbcfwkjdwx031scd4lf8hhm1n91f8mgbvgscix")))

