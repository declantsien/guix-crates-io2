(define-module (crates-io sa pp sapp-console-log) #:use-module (crates-io))

(define-public crate-sapp-console-log-0.1.0 (c (n "sapp-console-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sapp-wasm") (r "^0.1.4") (d #t) (k 0)))) (h "11ws6q6qv7x1z2npfzam5irwnj31g5viiph6017can1z168r0a5c")))

(define-public crate-sapp-console-log-0.1.8 (c (n "sapp-console-log") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sapp-wasm") (r "^0.1.8") (d #t) (k 0)))) (h "0ndmi2c2flqzdxv6rd08yn69k7drf9dis65rdqz7ay8cf8h3c37i")))

(define-public crate-sapp-console-log-0.1.9 (c (n "sapp-console-log") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sapp-wasm") (r "0.1.*") (d #t) (k 0)))) (h "1s8yx0hzqbkbq4i643lnxg6bcgrhnhhv98v1ili24q19z4nxhgsv")))

