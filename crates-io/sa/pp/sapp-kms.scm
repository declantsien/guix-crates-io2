(define-module (crates-io sa pp sapp-kms) #:use-module (crates-io))

(define-public crate-sapp-kms-0.1.0 (c (n "sapp-kms") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k63fj30r7zzsjgqfj882x1idf696aklqbc0nk674m47a6dbilap")))

(define-public crate-sapp-kms-0.1.1 (c (n "sapp-kms") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qx2y1aknrsn6c3q1h4b5ffg8jyvpa1pjn8v95ai9xyaql5w65pk")))

