(define-module (crates-io sa pp sapp-ios) #:use-module (crates-io))

(define-public crate-sapp-ios-0.1.0 (c (n "sapp-ios") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "144h737qby3hcyl2kg4harxa2mys22lasw7fm8gqjqfflj5lr65h")))

(define-public crate-sapp-ios-0.1.1 (c (n "sapp-ios") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1agazy859s4gq3iznr3qxgxx9q9rapjyf8273xlnl9360lrvwwyx")))

(define-public crate-sapp-ios-0.1.2 (c (n "sapp-ios") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1adl50q9vrp7bhh9gj63bw39yhrvlmaaidkri69jxb69c596w7h8")))

