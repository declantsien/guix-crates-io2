(define-module (crates-io sa pp sapp-wasm) #:use-module (crates-io))

(define-public crate-sapp-wasm-0.1.0 (c (n "sapp-wasm") (v "0.1.0") (h "1spxmq0ix0hv0gfi45jlqgkga259a5g6z5gyfj6yg7qbxspgrxj7")))

(define-public crate-sapp-wasm-0.1.1 (c (n "sapp-wasm") (v "0.1.1") (h "0lvlndlq3vvzdlwimqnzi23jgn7qiwjyqnk3jgf1jmcwgb40qqk5")))

(define-public crate-sapp-wasm-0.1.2 (c (n "sapp-wasm") (v "0.1.2") (h "07rbn5r1bc8kcflczf586rr9ax91a6xjjj16kymg0m6d0d2d9si1")))

(define-public crate-sapp-wasm-0.1.3 (c (n "sapp-wasm") (v "0.1.3") (h "0v3m1ka2wrnd8pij5hvl0b6sgk3azfzx4mnhxb8m2fc2qph6pwhv")))

(define-public crate-sapp-wasm-0.1.4 (c (n "sapp-wasm") (v "0.1.4") (h "0kax0i7id9ly5aw1wgl0jms7c77ma1ndq639mziqi93cc0143hf3")))

(define-public crate-sapp-wasm-0.1.5 (c (n "sapp-wasm") (v "0.1.5") (h "1vssmix7bk49xb315dq6r3d1kzp49jqz9mlcinj7np9y2d2jlsc8")))

(define-public crate-sapp-wasm-0.1.6 (c (n "sapp-wasm") (v "0.1.6") (h "15wjawdn85qaf7hvwq8gyws3lknmd122c68n9cbscc0n08q1lvmg")))

(define-public crate-sapp-wasm-0.1.8 (c (n "sapp-wasm") (v "0.1.8") (h "1ixnfwm7zap04pg9nqm03ca94rnx7jri4iqb81wq3gax1n4s3h74")))

(define-public crate-sapp-wasm-0.1.9 (c (n "sapp-wasm") (v "0.1.9") (h "1ya3ndzpd5ah7q7fq8rqzl2d6cx4mzw1hprh3kv7j3fqrncc33l7")))

(define-public crate-sapp-wasm-0.1.10 (c (n "sapp-wasm") (v "0.1.10") (h "0d6wypiyarc5wvq50byg8b75j74h1y8yhryx2svkpbfz0wd27i97")))

(define-public crate-sapp-wasm-0.1.11 (c (n "sapp-wasm") (v "0.1.11") (h "1ambnvp8c2x8il02m6f5yzgwljzq4i3pkpcsv03nww0i699brsw6")))

(define-public crate-sapp-wasm-0.1.12 (c (n "sapp-wasm") (v "0.1.12") (h "0japac43dyv16zmcxl270hdrmwcc29bg5xhm68k8yqxl9qa3s648")))

(define-public crate-sapp-wasm-0.1.13 (c (n "sapp-wasm") (v "0.1.13") (h "0r3n4ddr0rj0f3y55gk8ap8g627nxh84jrxpmhzwhv56w08p00pz")))

(define-public crate-sapp-wasm-0.1.14 (c (n "sapp-wasm") (v "0.1.14") (h "1zd61qig6vp5fw36cy2jgfadih5ibfgrpyv9yqqibbzrq5dyzkxh")))

(define-public crate-sapp-wasm-0.1.15 (c (n "sapp-wasm") (v "0.1.15") (h "0gnxayp9fzkmhw8bwjrsrn8l84zx9izxp1hdpf48k0mq7mab2583")))

(define-public crate-sapp-wasm-0.1.16 (c (n "sapp-wasm") (v "0.1.16") (h "1b51dmilg8l333d20ym0wqz138zj9gamqzw2n0cz8z7zripg34w9")))

(define-public crate-sapp-wasm-0.1.17 (c (n "sapp-wasm") (v "0.1.17") (h "12afk4c505gb4vha2kjssz9g5q7y81y712j86zh613n2c93cqxan")))

(define-public crate-sapp-wasm-0.1.18 (c (n "sapp-wasm") (v "0.1.18") (h "129ncmaavjakxw10zbnrcwdmh5h41qnsi19iizpn4ghrwp0lpg54")))

(define-public crate-sapp-wasm-0.1.19 (c (n "sapp-wasm") (v "0.1.19") (h "05c3c9n3b42pswiib6s5q75jpmi6l2kq3cny1k90is98glx39180")))

(define-public crate-sapp-wasm-0.1.20 (c (n "sapp-wasm") (v "0.1.20") (h "0a7sb6mjjryp5nrpd04198y7i92kdq7jc6qk0i76lfcn36vwzc1q")))

(define-public crate-sapp-wasm-0.1.21 (c (n "sapp-wasm") (v "0.1.21") (h "0h9g204y5dnhm1dyav8wdk58vfn7x6544k20pjqxlh9n46s39fqv")))

(define-public crate-sapp-wasm-0.1.22 (c (n "sapp-wasm") (v "0.1.22") (h "1lbkghcxfal5w7z6mxh3nri4r7hkffhdr6jb882x9c8cc8ifv8sn")))

(define-public crate-sapp-wasm-0.1.23 (c (n "sapp-wasm") (v "0.1.23") (h "1r3qafijl7a6bh2gps200s819ckw0s2ajwf9hsb45093f5axhck6")))

(define-public crate-sapp-wasm-0.1.24 (c (n "sapp-wasm") (v "0.1.24") (h "0z73ih80shih0lnvb1lir60rac3w3r6r90wd1ajx3fdrx311ip26")))

(define-public crate-sapp-wasm-0.1.25 (c (n "sapp-wasm") (v "0.1.25") (h "1shyv43vq59g2frsja99mcq7py42ks2iqf7wwkwq5vjr5cajfjqh")))

(define-public crate-sapp-wasm-0.1.26 (c (n "sapp-wasm") (v "0.1.26") (h "1fq8ymh8fd62xb5vq88vy82l33j3hfwblh6dms2wnfssckl5ks00")))

