(define-module (crates-io sa pp sapper_query) #:use-module (crates-io))

(define-public crate-sapper_query-0.1.0 (c (n "sapper_query") (v "0.1.0") (d (list (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "10yfxniyqc512w10yzkj5y5v5z7ac8mbhwgvh9a275pb4hsnhs8x")))

(define-public crate-sapper_query-0.1.1 (c (n "sapper_query") (v "0.1.1") (d (list (d (n "sapper") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0va8ynxdv65snks4dndcvz9xlck7wlpifjab5c4c62qy7y3zhvd9")))

(define-public crate-sapper_query-0.2.0 (c (n "sapper_query") (v "0.2.0") (d (list (d (n "sapper") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "0fwyxvzxnb3bxsqq91rhdaici6wr5czmcag26nbwcykaq1c4hjg8")))

