(define-module (crates-io sa pp sapp) #:use-module (crates-io))

(define-public crate-sapp-0.1.0 (c (n "sapp") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "043w2j5clz945b0yam13dcps99izvy43sn9l0bgk7dsav9ph338x")))

