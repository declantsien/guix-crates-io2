(define-module (crates-io sa pp sapper) #:use-module (crates-io))

(define-public crate-sapper-0.1.0 (c (n "sapper") (v "0.1.0") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "17adfa8rj8wfhbamj5mynnd2n87n049yi20k4l232dp1x38sr3dd")))

(define-public crate-sapper-0.1.1 (c (n "sapper") (v "0.1.1") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0xv14kfk7wz6rjwmkaf9hnb0afypx889ksjr1qr7krzl1z8fi5xm")))

(define-public crate-sapper-0.1.2 (c (n "sapper") (v "0.1.2") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1gjqqljxk28yfchpy11gsf94n9vvl1h9zci63b0xgrirnlmiaqwd")))

(define-public crate-sapper-0.1.3 (c (n "sapper") (v "0.1.3") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0r18sc32fy5imahgs4il71j993qaw4xcdq5jb6rz10n8qc0k80yp")))

(define-public crate-sapper-0.1.4 (c (n "sapper") (v "0.1.4") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1vqxabpqrpypsp1xm5ds146z8xfpjpjkv1ajy4mnb351m3wqimh7")))

(define-public crate-sapper-0.1.5 (c (n "sapper") (v "0.1.5") (d (list (d (n "conduit-mime-types") (r "^0.7.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "1nyv8kh4wcax5fmalmx7zx1f22j2jg4hqv01kqvny02nvp21y82c")))

(define-public crate-sapper-0.2.0 (c (n "sapper") (v "0.2.0") (d (list (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (d #t) (k 0)))) (h "01ck5gpylma58svg4b6mfnzs38hzrwnqvysrn0fmzlkdm25wrzha")))

