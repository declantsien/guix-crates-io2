(define-module (crates-io sa rc sarc) #:use-module (crates-io))

(define-public crate-sarc-1.0.0 (c (n "sarc") (v "1.0.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "1m048sb3lw7kpffp6alqpi6fla1mh387ax8pyayvb4mxmg7harv2")))

(define-public crate-sarc-1.0.1 (c (n "sarc") (v "1.0.1") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (d #t) (k 0)))) (h "1rqj2j5idzf7h73p567hkdcbycgxxhm5dkdwj6zhvfkdp9wqdihh")))

(define-public crate-sarc-1.1.0 (c (n "sarc") (v "1.1.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0iw3fgqhb1zpwhlf4wjqlsv9d5s6fb5m4pkhicnv25p095frw6ca") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc")))) (y #t)))

(define-public crate-sarc-1.1.1 (c (n "sarc") (v "1.1.1") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0afbj61i17d765wl6i0j3giz3swgwxl5andlwcpdmmqwdijvvmjl") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc"))))))

(define-public crate-sarc-1.1.2 (c (n "sarc") (v "1.1.2") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1r10bk830313gwl8k612v0021bwqqliz7m7mrahfpjamkxfzvcw7") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc"))))))

(define-public crate-sarc-1.1.3 (c (n "sarc") (v "1.1.3") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "06sabh55s52wsbfr91m8lb5zzdwixrf2rwb652mlksis748q6yvp") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc"))))))

(define-public crate-sarc-1.1.4 (c (n "sarc") (v "1.1.4") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0ff83df2fbcdw6j1z811bbj04llsrvamprliydvr66ghc1f8hqsl") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc"))))))

(define-public crate-sarc-1.2.0 (c (n "sarc") (v "1.2.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "yaz0") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "1vxz9zpr20xzxkpkzgkz6cm5bsm09q1ng898c232d0d31wakmchh") (f (quote (("zstd_sarc" "zstd") ("yaz0_sarc" "yaz0") ("default" "yaz0_sarc" "zstd_sarc"))))))

