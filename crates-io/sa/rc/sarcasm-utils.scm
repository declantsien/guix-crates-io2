(define-module (crates-io sa rc sarcasm-utils) #:use-module (crates-io))

(define-public crate-sarcasm-utils-0.1.0 (c (n "sarcasm-utils") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "09lgghxdvw0fdjyc5xh8iyn7srwk0igdd8k244qzzdq88bjjd61a")))

