(define-module (crates-io sa ik saika) #:use-module (crates-io))

(define-public crate-saika-0.1.0 (c (n "saika") (v "0.1.0") (h "1kyyqagk2r5zrddl1w3w0ghi3wwk51mdb5k9cm131js7a8s38sd9")))

(define-public crate-saika-0.1.1 (c (n "saika") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wmr3fylx0cnfcqp6f4jycg62kkrfma29l6xh9rpls1n7ndg4yv5")))

