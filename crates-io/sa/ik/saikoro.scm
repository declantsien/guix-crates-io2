(define-module (crates-io sa ik saikoro) #:use-module (crates-io))

(define-public crate-saikoro-1.0.0 (c (n "saikoro") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0hrhg6ssajj8ak4hipwz3k13bnxdsnrvmnp1vmdlgz5cr45sisp7")))

(define-public crate-saikoro-1.1.0 (c (n "saikoro") (v "1.1.0") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1pghf5px7a6dz8spxji1lvv08sjpx067lhdb6x349ql7lpf320i9")))

(define-public crate-saikoro-1.1.1 (c (n "saikoro") (v "1.1.1") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1avqilpf3sjpfs2w7klbq770m9szrkk63m2zhbsbwqaak9irj3kz")))

(define-public crate-saikoro-1.2.0 (c (n "saikoro") (v "1.2.0") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0z3z7xch216l8yf3pqp88vdpi8rs00xa27lczf29rpq1108kn1rg") (f (quote (("stats")))) (y #t)))

(define-public crate-saikoro-1.2.1 (c (n "saikoro") (v "1.2.1") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0xsxdh24lnny4g5xjvwr81ry356z82p6p9ffl4y9cmsisyl19j6j") (f (quote (("stats") ("full" "stats"))))))

(define-public crate-saikoro-1.2.2 (c (n "saikoro") (v "1.2.2") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1ynsvm6xcf472pykxgvljms9y87rsafg10n476z1zly0g1bs3cg8") (f (quote (("stats") ("full" "stats"))))))

