(define-module (crates-io sa ik saikou) #:use-module (crates-io))

(define-public crate-saikou-0.1.0 (c (n "saikou") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 1)) (d (n "hyper") (r "^0.11.25") (d #t) (k 1)) (d (n "hyper-tls") (r "^0.1.3") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "scraper") (r "^0.6.0") (d #t) (k 1)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 1)))) (h "1bnam2jw9if9qakgh6wq2y84pqm1j4p7g30n76rp57f4jlf8fqr2") (y #t)))

(define-public crate-saikou-0.1.1 (c (n "saikou") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 1)) (d (n "hyper") (r "^0.11.25") (d #t) (k 1)) (d (n "hyper-tls") (r "^0.1.3") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "scraper") (r "^0.6.0") (d #t) (k 1)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 1)))) (h "1rjdrp95fcwfw3gp0d6b3cxwajlljgbr31gky7q22b787435d5yv") (y #t)))

