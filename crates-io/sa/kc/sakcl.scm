(define-module (crates-io sa kc sakcl) #:use-module (crates-io))

(define-public crate-sakcl-0.1.0 (c (n "sakcl") (v "0.1.0") (d (list (d (n "ldap3") (r "^0.6") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1") (d #t) (t "cfg(target_env = \"musl\")") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "031xvm8r0paq1lw4aj2vnz496dz0jsgm4nlb09dr9n88d7zzkz83")))

(define-public crate-sakcl-0.2.0 (c (n "sakcl") (v "0.2.0") (d (list (d (n "ldap3") (r "^0.6") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1") (d #t) (t "cfg(target_env = \"musl\")") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "18ina03l7z9by1jzvfp2x0p48n3ps1p9m8ammh944ffwv91niaxc")))

(define-public crate-sakcl-0.2.1 (c (n "sakcl") (v "0.2.1") (d (list (d (n "ldap3") (r "^0.6") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1") (d #t) (t "cfg(target_env = \"musl\")") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1hw8maq1v03rvxb5r6nc0di0azchiaibjng39vvjznjx00pd44kv")))

