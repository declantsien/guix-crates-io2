(define-module (crates-io sa t_ sat_lab) #:use-module (crates-io))

(define-public crate-sat_lab-0.1.0 (c (n "sat_lab") (v "0.1.0") (d (list (d (n "bool_vec") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)))) (h "0p4cl5g1v7q00k4wfk9ph8im9i25nbf2hj0db7yxadf7zy6851lr") (s 2) (e (quote (("rand" "dep:rand"))))))

