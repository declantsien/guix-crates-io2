(define-module (crates-io sa t_ sat_toasty_helper) #:use-module (crates-io))

(define-public crate-sat_toasty_helper-0.0.1 (c (n "sat_toasty_helper") (v "0.0.1") (d (list (d (n "splr") (r "^0.17.0") (d #t) (k 0)))) (h "16jay2b550sasj5mk1wdp2qv1dqq2s95cz2fqx7kqib7zk07ljjl")))

(define-public crate-sat_toasty_helper-0.0.2 (c (n "sat_toasty_helper") (v "0.0.2") (d (list (d (n "splr") (r "^0.17.0") (d #t) (k 0)))) (h "0dipjnygm3hs31yimy85dia0amqbwfbghp5x42xfv3dssb86b29n")))

(define-public crate-sat_toasty_helper-0.0.3 (c (n "sat_toasty_helper") (v "0.0.3") (d (list (d (n "splr") (r "^0.17.2") (d #t) (k 0)))) (h "1aldwazp75klnaipz2zb0axwmcikckz0iwwn5kw4rzxsmrbp3vcx")))

