(define-module (crates-io sa be saber-redeemer-ntoken) #:use-module (crates-io))

(define-public crate-saber-redeemer-ntoken-0.1.0 (c (n "saber-redeemer-ntoken") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "redeemer") (r "^1.1.2") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0627j7k33fw8gzk5g319y4hkxj7sd8nhw3kdx87y2hnay849p28c")))

(define-public crate-saber-redeemer-ntoken-0.1.1 (c (n "saber-redeemer-ntoken") (v "0.1.1") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "redeemer") (r "^1.1.2") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "1pi8daprhyrm6qgwh5v7drxr7l7368yvqrqj7718x2qb5q5sv05c")))

(define-public crate-saber-redeemer-ntoken-0.1.2 (c (n "saber-redeemer-ntoken") (v "0.1.2") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "redeemer") (r "^1.1.2") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "04akkyy84whs66yrm3w98zccdq1asg9vc5cy37bg0vi9d2l0xgk9")))

(define-public crate-saber-redeemer-ntoken-0.1.3 (c (n "saber-redeemer-ntoken") (v "0.1.3") (d (list (d (n "anchor-lang") (r ">=0.22") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.22") (d #t) (k 0)) (d (n "redeemer") (r "^1.1.2") (f (quote ("cpi"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)))) (h "0sbp6v8nzjlvl16bs92j546z87r8pp9shcjvywcf5n3xrj5d82cz")))

