(define-module (crates-io sa be saberrs) #:use-module (crates-io))

(define-public crate-saberrs-0.2.0 (c (n "saberrs") (v "0.2.0") (d (list (d (n "cargo-readme") (r "^3.1.2") (d #t) (k 2)) (d (n "serialport") (r "^3.3.0") (k 0)))) (h "1ra646fxdnpyrgfvzmfb2q8pjf9hb7z936jxbyxiacvpi2qcrz51")))

(define-public crate-saberrs-0.2.1 (c (n "saberrs") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (o #t) (k 0)))) (h "1lg74vnsafhplivlqfvnr1g3cgml1ih7gandy2d14vcllzjvgwds") (f (quote (("default" "serialport"))))))

(define-public crate-saberrs-0.3.0 (c (n "saberrs") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (o #t) (k 0)))) (h "08dy510l7grjq46pwaiir98a7n7vplafddhxj795jqxcygqxkpfa") (f (quote (("default" "serialport"))))))

(define-public crate-saberrs-0.3.1 (c (n "saberrs") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (o #t) (k 0)))) (h "19y39yw6j02zy9m28h1qadpvcflac95smali3dqbvc046hrm8mhd") (f (quote (("default" "serialport"))))))

