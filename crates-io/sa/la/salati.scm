(define-module (crates-io sa la salati) #:use-module (crates-io))

(define-public crate-salati-0.0.1 (c (n "salati") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "0x2rl7glj1ms84y8f1x9c72297ygzs5abip84dk7p6f2kciimvj1") (r "1.62.1")))

