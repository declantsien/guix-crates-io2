(define-module (crates-io sa la salak_derive) #:use-module (crates-io))

(define-public crate-salak_derive-0.1.0 (c (n "salak_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "12dc8yll7f9y316ign0s97pp36kjwfqlm3ix71brnmk175yj00zn") (y #t)))

(define-public crate-salak_derive-0.1.1 (c (n "salak_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07xr2bp17slcbxsw62qvk29s2yvl59s9zqnsmlkl5hbl074ysahp") (y #t)))

(define-public crate-salak_derive-0.2.0 (c (n "salak_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0y3j11bzxw6f1njzfb1zx303baziw4aspdgp3zjl5mzlfjzhay3y") (y #t)))

(define-public crate-salak_derive-0.3.0 (c (n "salak_derive") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10dk4pmpsxyp0x3xkqvm9j4hmm0y9sdmx4s36kgfimf35611hrjc") (y #t)))

(define-public crate-salak_derive-0.4.0 (c (n "salak_derive") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d6xwzl49b3df92d777l9dm0cjx6ylr4qb6mnx93mzgazpqxg2c3")))

(define-public crate-salak_derive-0.5.0 (c (n "salak_derive") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1csz17mwgwg94arrrm7gd12kk7px478zi8ypg3jrlc2jn02idvac") (y #t)))

(define-public crate-salak_derive-0.5.1 (c (n "salak_derive") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19miifi82d0106x3azmrqi30bn12hjzwcmn9b9iy85r1smzv7r26")))

(define-public crate-salak_derive-0.6.0 (c (n "salak_derive") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03qzw8n4x1iajnzcn2smc0fi75glhr5dca7vn4m7087s7nfvwsdf")))

(define-public crate-salak_derive-0.7.0 (c (n "salak_derive") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zipl6ngzw5v2b6sv1plsinjry6nwyfiicvx87fzsxdx0y9w75d4")))

(define-public crate-salak_derive-0.8.0 (c (n "salak_derive") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0c7gwl560p9q9aqf6wf7a6nw5snibarm7wj4bh8xjribiadbv9k7")))

