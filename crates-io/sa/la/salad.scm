(define-module (crates-io sa la salad) #:use-module (crates-io))

(define-public crate-salad-0.1.0 (c (n "salad") (v "0.1.0") (h "0jjilff1lsbf51s2dx8vhlanwcv10sl3jqj3g93n62cvn5cd2zg3")))

(define-public crate-salad-0.1.1 (c (n "salad") (v "0.1.1") (h "0a9yly2wspqsif1fa87p01ankb1skl5pm9sw02bjaq2yq8nf90vg")))

(define-public crate-salad-0.2.1 (c (n "salad") (v "0.2.1") (h "0yjwqvrxidrjplcqcqq1p80imycbdxyvj261gqy8v3629cfnqwbn")))

(define-public crate-salad-0.2.2 (c (n "salad") (v "0.2.2") (h "0s39swp0clsc7bsfmkm3jrvb8svpyhmyqdyy4ymhi2y3p98rknmi")))

(define-public crate-salad-0.2.3 (c (n "salad") (v "0.2.3") (h "1lxj3bkbmlhlyl46bb6nfnam2la38yiwpf719x91nlr4ddr72a2s")))

