(define-module (crates-io sa la salah) #:use-module (crates-io))

(define-public crate-salah-0.4.2 (c (n "salah") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "04biq8ziawg30wvrw414093fgld95qvkg9wmyxjfr2fd92kqslhk")))

(define-public crate-salah-0.5.0 (c (n "salah") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "199iipdvrczvmx5ncz5pvlp7jfh985sry5hawd2wcn4fwa3h0ql7")))

(define-public crate-salah-0.7.0 (c (n "salah") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "028kczxylc28zl8dsrjzwsh26x1hzy99ggbr4jfl6nxkb0jds20r")))

(define-public crate-salah-0.7.1 (c (n "salah") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0ixnmzw21rh1bimy9w3s2dmm9ldv33268162nicqv3n2m3z75iz3")))

(define-public crate-salah-0.7.5 (c (n "salah") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "14dnwsbaha6p0ssc75d1msp4dmzvx1njlyr901x7b5qsxk5k1m2a")))

