(define-module (crates-io sa la salah_cli) #:use-module (crates-io))

(define-public crate-salah_cli-0.1.0 (c (n "salah_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tzfile") (r "^0.1.3") (d #t) (k 0)))) (h "05ixf2x75jkc3kxhpnaps409dz6xc8vw6kdbsfgiqrbd9lnb0942")))

