(define-module (crates-io sa la salat_mv) #:use-module (crates-io))

(define-public crate-salat_mv-0.3.8 (c (n "salat_mv") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "1s4myk05api7mzg3skva23p8zd126ynf9f7n1nks5s1n818z1ncp")))

(define-public crate-salat_mv-0.3.9 (c (n "salat_mv") (v "0.3.9") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "1rvns5dxxqq020fk8g97l9s435lr2rmxgf4n390d7h1kvggzkqca") (y #t)))

(define-public crate-salat_mv-0.3.10 (c (n "salat_mv") (v "0.3.10") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "109q9inki5mn49vpi21yh63mbnsfasacjaw89gbz7arw9x0d69i6")))

(define-public crate-salat_mv-0.3.11 (c (n "salat_mv") (v "0.3.11") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "0k3gpkqx7rgzcikabsc0mlybpgckj6bab7dpjq5fyd7cz1ssaa42")))

(define-public crate-salat_mv-0.3.12 (c (n "salat_mv") (v "0.3.12") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "1xfp7725yapf2l9rlrqhf07bmf4smslnbz1wadncph59nxfmikiy") (y #t)))

(define-public crate-salat_mv-0.3.13 (c (n "salat_mv") (v "0.3.13") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "uneval") (r "^0.2.4") (d #t) (k 0)))) (h "0xbzqy6jpr24n2cq7lfjxz9iya4jxgw6fai3xf1w1wxlclp81wm1")))

(define-public crate-salat_mv-0.3.14 (c (n "salat_mv") (v "0.3.14") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "1i6xw0q2v8cdjnsyjp4gs3ks28350xf28ngciraf51mld7jh7na2")))

(define-public crate-salat_mv-0.3.15 (c (n "salat_mv") (v "0.3.15") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "1zdz6wm6v39vn1qi38fyhfnykzckasy5lp44p9n2dl0z0dxzcifw")))

(define-public crate-salat_mv-0.3.16 (c (n "salat_mv") (v "0.3.16") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "0lddds6wy7gs81gz27rfx76h1cngcc1lpibcym2rcx3mq58ff45s")))

(define-public crate-salat_mv-0.3.17 (c (n "salat_mv") (v "0.3.17") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "11b7hzb9d9abyfn8qzhkwq6knhs8jxmv2hwivjqxi6dq05kf1v5d")))

(define-public crate-salat_mv-0.3.18 (c (n "salat_mv") (v "0.3.18") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)))) (h "1jh3ql6hii8v80dlwkybj77yxd2ivv54hfsm8ciqk8sn4yzz1kgd")))

