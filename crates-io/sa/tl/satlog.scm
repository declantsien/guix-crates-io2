(define-module (crates-io sa tl satlog) #:use-module (crates-io))

(define-public crate-satlog-0.0.1 (c (n "satlog") (v "0.0.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "0q1mzwd9ya01b06223bfcz7nn57kisrkf0kf323kakpbfglhlglz")))

(define-public crate-satlog-0.0.2 (c (n "satlog") (v "0.0.2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "1xlr1k6424m5wyiacziwhha4ywgfk82r98xdhjdsbz8swmc39r7p")))

(define-public crate-satlog-0.1.0 (c (n "satlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "0p9ixdkrvgbzda8m9cp28krisdplxi0655x7yqy5yjsq7kzmhf5l")))

(define-public crate-satlog-0.2.0 (c (n "satlog") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "0liaqal68v50f1zrx2ndcvrbsxz2pi45q5y71j61fpp1dq8yxk7b") (f (quote (("default" "color") ("color" "colored"))))))

(define-public crate-satlog-0.2.1 (c (n "satlog") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "16xz8h2cgg4p9vdbg2hf32h6vgix0abpiv8dg9m8p6hx90i0mh58") (f (quote (("default" "color") ("color" "colored"))))))

