(define-module (crates-io sa mo samotop-model) #:use-module (crates-io))

(define-public crate-samotop-model-0.10.0-preview.1 (c (n "samotop-model") (v "0.10.0-preview.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "0l0vr6608q6hn896llfd05aysml5ny163ckkiw3apsxiiz79i90v")))

(define-public crate-samotop-model-0.10.0-preview.2 (c (n "samotop-model") (v "0.10.0-preview.2") (h "1lwm1mk1h92ghwqmn5dwsjsi7d8bdzqw6ndp56y34kjzcqqnlw3i")))

(define-public crate-samotop-model-0.10.0-preview.3 (c (n "samotop-model") (v "0.10.0-preview.3") (h "1qq9sk100a1ncmkhslfys8668in76lxyb41y1xarxb389bl66dn4")))

(define-public crate-samotop-model-0.10.0 (c (n "samotop-model") (v "0.10.0") (h "1mqldqig2hm0zpnk2apwcb97p0cnxkpibnk3n0l0dn9cicd72q7g")))

(define-public crate-samotop-model-0.11.0 (c (n "samotop-model") (v "0.11.0") (d (list (d (n "futures-await-test") (r "^0.3") (d #t) (k 2)) (d (n "futures-io") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ml1is10x4vf7asax1r4harxjicbxhkiy9fqibak5mplv28caxhl")))

(define-public crate-samotop-model-0.12.0 (c (n "samotop-model") (v "0.12.0") (d (list (d (n "samotop-core") (r "^0.12.0") (d #t) (k 0)))) (h "1pcfvffffvgzyi17w9by2n7asx3igmxk7m5p68y29wgv2dpvhrmm")))

