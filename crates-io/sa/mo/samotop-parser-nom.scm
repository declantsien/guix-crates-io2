(define-module (crates-io sa mo samotop-parser-nom) #:use-module (crates-io))

(define-public crate-samotop-parser-nom-0.12.0 (c (n "samotop-parser-nom") (v "0.12.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "rustyknife") (r "^0.2") (d #t) (k 0)) (d (n "samotop-core") (r "^0.12.0") (d #t) (k 0)))) (h "0dv7sd80wj63nrp2rdqgcindzhr8g4d2m7yhi1885lj093bvj0aj")))

(define-public crate-samotop-parser-nom-0.13.0-samotop-dev (c (n "samotop-parser-nom") (v "0.13.0-samotop-dev") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rustyknife") (r "^0.2") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0-samotop-dev") (d #t) (k 0)))) (h "1wdk9ryalln2v65qjgnsbi9bihvpsj0jibkwfcw2xjy565bp2nfj")))

(define-public crate-samotop-parser-nom-0.13.0 (c (n "samotop-parser-nom") (v "0.13.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)) (d (n "rustyknife") (r "^0.2") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0") (d #t) (k 0)))) (h "02r3dzx0c1wvmaj15cx4nl4pqzkwmdblbpghhlmzh09k55ma20pf")))

