(define-module (crates-io sa mo samotop-with-native-tls) #:use-module (crates-io))

(define-public crate-samotop-with-native-tls-0.12.0 (c (n "samotop-with-native-tls") (v "0.12.0") (d (list (d (n "async-native-tls") (r "^0.3") (d #t) (k 0)) (d (n "samotop-core") (r "^0.12.0") (d #t) (k 0)))) (h "0333vvqx4hcs9d1vz4nanrnkkxzic8z432r66kb5df6s4qbza3dd") (f (quote (("vendored" "async-native-tls/vendored") ("default" "vendored"))))))

(define-public crate-samotop-with-native-tls-0.13.0-samotop-dev (c (n "samotop-with-native-tls") (v "0.13.0-samotop-dev") (d (list (d (n "async-native-tls") (r "^0.3") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0-samotop-dev") (d #t) (k 0)))) (h "0yw1cgz36d097xl0v1jz3bzc5cz2vizyhpwchv6j7wjflrcp39z5")))

(define-public crate-samotop-with-native-tls-0.13.0 (c (n "samotop-with-native-tls") (v "0.13.0") (d (list (d (n "async-native-tls") (r "^0.3") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0") (d #t) (k 0)))) (h "0411qrc3xjkg7x64b2mvnz2rcrbwrx58kahzwch8639wgwk7g65f")))

(define-public crate-samotop-with-native-tls-0.13.1 (c (n "samotop-with-native-tls") (v "0.13.1") (d (list (d (n "async-native-tls") (r "^0.4") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0") (d #t) (k 0)))) (h "0y9amd3pxnrjqp186pj6v4karxdn2x4j46r6j4vxvmmybyygsid7")))

