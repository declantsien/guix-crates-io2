(define-module (crates-io sa mo samotop-with-rustls) #:use-module (crates-io))

(define-public crate-samotop-with-rustls-0.12.0 (c (n "samotop-with-rustls") (v "0.12.0") (d (list (d (n "async-tls") (r "^0.11") (d #t) (k 0)) (d (n "samotop-core") (r "^0.12.0") (d #t) (k 0)))) (h "0mayqs2bgpjx4hqx0lnxnnmbfny8qjzq3ykqj0db8jn717igbyc7")))

(define-public crate-samotop-with-rustls-0.13.0-samotop-dev (c (n "samotop-with-rustls") (v "0.13.0-samotop-dev") (d (list (d (n "async-tls") (r "^0.11") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0-samotop-dev") (d #t) (k 0)))) (h "0hd1nj6hdd21lppfm89mvkj5mcsmwppfvbx4hmp6w7xslgrw5x81")))

(define-public crate-samotop-with-rustls-0.13.0 (c (n "samotop-with-rustls") (v "0.13.0") (d (list (d (n "async-tls") (r "^0.11") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0") (d #t) (k 0)))) (h "1qi03jcg0yzl4cjgp9cznn38hksxla5l3vj975j4cp7fd3a2ymyn")))

