(define-module (crates-io sa mo samotop-parser) #:use-module (crates-io))

(define-public crate-samotop-parser-0.10.0-preview.2 (c (n "samotop-parser") (v "0.10.0-preview.2") (d (list (d (n "peg") (r "^0.6") (d #t) (k 0)) (d (n "samotop-core") (r "^0.10.0-preview.3") (d #t) (k 0)))) (h "0gbj21w4bvlgd1mz0rf6zf0gy0q4b2ai89zg4birx9lhjwgx832c")))

(define-public crate-samotop-parser-0.10.0 (c (n "samotop-parser") (v "0.10.0") (d (list (d (n "peg") (r "^0.6") (d #t) (k 0)) (d (n "samotop-core") (r "^0.10.0") (d #t) (k 0)))) (h "04rrgrczyvcfvhqis6ad0hnmlw6nc1fknnndwh4x87db5x95y7fz")))

(define-public crate-samotop-parser-0.11.0 (c (n "samotop-parser") (v "0.11.0") (d (list (d (n "peg") (r "^0.6") (d #t) (k 0)) (d (n "samotop-model") (r "^0.11.0") (d #t) (k 0)))) (h "099s4kynpl791j64qx3bhmpmvn9kdgqhflpgdqf8qwj9k88d5ija")))

(define-public crate-samotop-parser-0.12.0 (c (n "samotop-parser") (v "0.12.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.6") (d #t) (k 0)) (d (n "samotop-core") (r "^0.12.0") (d #t) (k 0)))) (h "1whw3wciwwlw4741sr3rcbrpkm9iahmjyr59bvl07z7wrxb395l1")))

(define-public crate-samotop-parser-0.13.0-samotop-dev (c (n "samotop-parser") (v "0.13.0-samotop-dev") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0-samotop-dev") (d #t) (k 0)))) (h "166q75c53xj2v4ys1w2lr9i3khnzxpr76k67arbkkdxdmya45w7k")))

(define-public crate-samotop-parser-0.13.0 (c (n "samotop-parser") (v "0.13.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "peg") (r "^0.7") (d #t) (k 0)) (d (n "samotop-core") (r "^0.13.0") (d #t) (k 0)))) (h "1h8qm6jffnwwlgybb984wjgwsdyqn102wp5703ky0fm6glfykwnd")))

