(define-module (crates-io sa d_ sad_macros) #:use-module (crates-io))

(define-public crate-sad_macros-0.1.0 (c (n "sad_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0czn3ldyhcg9gr9xh6zfaaw9cjsnlw4f571sr9nypcydf40k8536")))

(define-public crate-sad_macros-0.2.0 (c (n "sad_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cbs6rirbv5c2c5hb0g2sdwc7kycgs0kkj0rqapn5dwxamplzmq2")))

