(define-module (crates-io sa mi samira) #:use-module (crates-io))

(define-public crate-samira-0.1.0 (c (n "samira") (v "0.1.0") (h "00ymhlg9qig62pbqr6wpwrhz97lmimw6jvryr6j9m202ihpvacpr")))

(define-public crate-samira-0.1.1 (c (n "samira") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1rkl2nm1hafkyiakl7l7343nk9j3rpnqhj3k6znil8z61vcbprzn")))

(define-public crate-samira-0.1.2 (c (n "samira") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "15vj06xab0n1kws5vfvgzl5jddfzqy6ak0vqvb3r7k8rvkn7y0jq")))

(define-public crate-samira-0.1.3 (c (n "samira") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "034j61szp9bmy2mjgx955d4hv50fiafjvd8qypzdx2nvb4i5zk6h")))

