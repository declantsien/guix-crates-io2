(define-module (crates-io sa sj sasja_euler) #:use-module (crates-io))

(define-public crate-sasja_euler-0.1.0 (c (n "sasja_euler") (v "0.1.0") (h "0i9fxv99nh4niznzwyklm9bikqfwc7s58cc705zn0apag971am41")))

(define-public crate-sasja_euler-0.1.1 (c (n "sasja_euler") (v "0.1.1") (h "1mwyzfrg656pv8vl44f9blixg6grccy4s07v3i402nh86fa3qdiq")))

(define-public crate-sasja_euler-0.1.2 (c (n "sasja_euler") (v "0.1.2") (h "141mizmjq8xkh4dqb4x9rm6vkriggnyrfvi8gsj64d7ii8yyq7ws")))

