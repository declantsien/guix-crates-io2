(define-module (crates-io sa vv savvy-ffi) #:use-module (crates-io))

(define-public crate-savvy-ffi-0.1.18 (c (n "savvy-ffi") (v "0.1.18") (h "14hl8srd497fsm100cksc5x8q0sd9qz0cha56ng7hm7rb1pw27cz")))

(define-public crate-savvy-ffi-0.1.19 (c (n "savvy-ffi") (v "0.1.19") (h "10j3nc1b4wm695ly7lnrknz1fjdpa4szzxf0mr551nvb54ag619l")))

(define-public crate-savvy-ffi-0.2.0 (c (n "savvy-ffi") (v "0.2.0") (h "0699spy3ykdvxiaxkbjmzfdfw2v5jjzr1han2nsn8wrb6mbp5pq7")))

(define-public crate-savvy-ffi-0.2.1 (c (n "savvy-ffi") (v "0.2.1") (h "0nssqx01sgfannj9krxmgajrnjp61727zyc2m99lfivyhw4xgl1a")))

(define-public crate-savvy-ffi-0.2.2 (c (n "savvy-ffi") (v "0.2.2") (h "19zhmbd33iszixl4ly0m4i7xl3dbjnr0r2f1j3iqnijdb95s7pwq")))

(define-public crate-savvy-ffi-0.2.3 (c (n "savvy-ffi") (v "0.2.3") (h "0b56as4alan2ki0v1bfml0j98xiwczznzvi98kgfcnrbgnicgcvw")))

(define-public crate-savvy-ffi-0.2.5 (c (n "savvy-ffi") (v "0.2.5") (h "0s314ndd7z5rk6iry9jxwsxgyjf5wn1k6nh9gz58kxawbnbf8v28")))

(define-public crate-savvy-ffi-0.2.6 (c (n "savvy-ffi") (v "0.2.6") (h "0v5fqa69fpvmvjf9cclfk7v774vr5b8ghj0yd5f65vbdm1pygdb6")))

(define-public crate-savvy-ffi-0.2.7 (c (n "savvy-ffi") (v "0.2.7") (h "0wj8adkq6bs3nfdzd0pnz72jhnqzh1y9vbpiish8rn7arqi6jj6z")))

(define-public crate-savvy-ffi-0.2.8 (c (n "savvy-ffi") (v "0.2.8") (h "0893nvm4rhw890a3knnaydl2v4h1q3pm7ap529sxhn94c8mksrbn")))

(define-public crate-savvy-ffi-0.2.9 (c (n "savvy-ffi") (v "0.2.9") (h "0vdllfsy0bw0mbk7gb6i2zk3zl4fg6ryqs7wk0frnrgrajyz6l2j")))

(define-public crate-savvy-ffi-0.2.10 (c (n "savvy-ffi") (v "0.2.10") (h "0nzv4rdf5zhnkiik6d434ncclyhmyz2q01xyirjcvz5zkwzz44js")))

(define-public crate-savvy-ffi-0.2.11 (c (n "savvy-ffi") (v "0.2.11") (h "1biyqyx18ml436qziy5vvq76i3xbmnkgjkkqqghnhb7g3agbf4sm")))

(define-public crate-savvy-ffi-0.2.12 (c (n "savvy-ffi") (v "0.2.12") (h "0pa22fnxxysf9r68as5ka6kmaxmjhb5pbzjd9n2iv38r9gn2cgaa")))

(define-public crate-savvy-ffi-0.2.13 (c (n "savvy-ffi") (v "0.2.13") (h "1cls24ckhsmax6mzhqzh3x0ribi9nmxy45xbb6jvvq8nsjav11da")))

(define-public crate-savvy-ffi-0.2.14 (c (n "savvy-ffi") (v "0.2.14") (h "0rsqgkyr7dlw344svycbh2m5rni7vypnh48qh1w7wc1m2aigrrac")))

(define-public crate-savvy-ffi-0.2.15 (c (n "savvy-ffi") (v "0.2.15") (h "02jqbarj1pv4j3qwmlcwkg52lf8mpyf9zmvaxnhd6fx7x4fqzv7i") (f (quote (("fake-libR"))))))

(define-public crate-savvy-ffi-0.2.16 (c (n "savvy-ffi") (v "0.2.16") (h "0q8gwqzp0hh9gl3cv3vfh75nrbdy7ckaai1ychgnvv1n0xh4k903")))

(define-public crate-savvy-ffi-0.2.17 (c (n "savvy-ffi") (v "0.2.17") (h "1cazj1bc2kigllb51a30npp67qj5q64dgdyql3yda8cdkpbsq756")))

(define-public crate-savvy-ffi-0.2.18 (c (n "savvy-ffi") (v "0.2.18") (h "0gpklyswa4wi4mcpzh3179n2ljw1k5nad3gv1606zi88sk1rdaaw")))

(define-public crate-savvy-ffi-0.2.20 (c (n "savvy-ffi") (v "0.2.20") (h "0sq4h1p3rf2bbf2j7by3ijklhj31nanl0g84hh9ddgd4gvkckk5k")))

(define-public crate-savvy-ffi-0.3.0 (c (n "savvy-ffi") (v "0.3.0") (h "1ab9iw8lagknxnwz8pm0h9ya5z1z1avz84w1r3zglv0l3gvrx686")))

(define-public crate-savvy-ffi-0.4.0 (c (n "savvy-ffi") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "052kw39gnhc5wkv9nc7bi340bnlkm06akd11dm8158x54vsjn9y6") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.4.1 (c (n "savvy-ffi") (v "0.4.1") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "13cp66w8xqhfp2ria32r87ldf7gkyni4cqkrzkk6wkr75s9r98pn") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.4.2 (c (n "savvy-ffi") (v "0.4.2") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "091m0sardc4rla591i38vdn0jmmjmg839va700flz9cw0rzvjb8l") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.5.0 (c (n "savvy-ffi") (v "0.5.0") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0pw8bdgdzdc3smg89l5yjwgwy0fvz9xvvj7apqi76hvn5zph6bwk") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.5.1 (c (n "savvy-ffi") (v "0.5.1") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1xwlzr6hw8p056ai4qr45xfgxag0ck6a9hzg2if0shmw5lap9cm2") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.5.2 (c (n "savvy-ffi") (v "0.5.2") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "11rj9linbh2j746n6znc0n64vjpb13hhy3v54115mg2gz03y2qpx") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.5.3 (c (n "savvy-ffi") (v "0.5.3") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0dg0afryx74nzn8fwvkmb724fcyh26fcfgac5pnfwwqbfjj9y5pa") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.6.0 (c (n "savvy-ffi") (v "0.6.0") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0i32llxmd47306fnyif7a6rsw1mn80c5hmf4a4mw9yrw2acpdmv4") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.6.1 (c (n "savvy-ffi") (v "0.6.1") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1n0zlvh1rmqmcl7l96qwzqgxhaxh9cqp4sdqyvx90l38i2nlnm7k") (f (quote (("default") ("complex" "num-complex"))))))

(define-public crate-savvy-ffi-0.6.2 (c (n "savvy-ffi") (v "0.6.2") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0pwgxjk06z2pkp7gdh9lqkpqd7ny13aair1wpl21p4l4jk49xqkh") (f (quote (("default") ("complex" "num-complex") ("altrep"))))))

(define-public crate-savvy-ffi-0.6.3 (c (n "savvy-ffi") (v "0.6.3") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0993xhpw4hsiaczsiad8pjb2p0crsdmc6l71lcfii6qrrzxjnr1x") (f (quote (("default") ("complex" "num-complex") ("altrep"))))))

(define-public crate-savvy-ffi-0.6.4 (c (n "savvy-ffi") (v "0.6.4") (d (list (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1x77wps6c1c5yxzcyn47sy90j99vy07arxjk9dkk2ibj6rfmmn7w") (f (quote (("default") ("complex" "num-complex") ("altrep"))))))

