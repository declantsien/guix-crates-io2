(define-module (crates-io sa vv savvycal) #:use-module (crates-io))

(define-public crate-savvycal-0.0.1-alpha (c (n "savvycal") (v "0.0.1-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros"))) (d #t) (k 2)))) (h "15h0g6adkssm2f5zmzvmz4s5wqym5sxg75b92w75g7f0bjc4xxpa")))

(define-public crate-savvycal-0.0.2-alpha (c (n "savvycal") (v "0.0.2-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5") (d #t) (k 2)))) (h "05xy6gliw7v0i3wp514q6dhmx8rbylih13mxm3k8wwywgb7cnj1j")))

