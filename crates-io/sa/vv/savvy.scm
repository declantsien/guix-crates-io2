(define-module (crates-io sa vv savvy) #:use-module (crates-io))

(define-public crate-savvy-0.1.9 (c (n "savvy") (v "0.1.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1m1j8ydnbvxvf995zyg2z6lyy1hsw44pfvl4ab6cajfp7wi6vnrq")))

(define-public crate-savvy-0.1.11 (c (n "savvy") (v "0.1.11") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "19by3l143809dgmysig5x2c9aw9sazd4hnc9m51dbs9j6jvqnqiz")))

(define-public crate-savvy-0.1.13 (c (n "savvy") (v "0.1.13") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0jck0fxa3v2k1iihmw5rdd42cnx8qr8rhns2wldn7n01lfh68imq")))

(define-public crate-savvy-0.1.14 (c (n "savvy") (v "0.1.14") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0mis0ygj79hg377xgvjm53hamxy1ck5dpkf5as56zisdyjd1i7vg")))

(define-public crate-savvy-0.1.15 (c (n "savvy") (v "0.1.15") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1wqinffyqbpm2y69fsdzyk3n1ll2wdrm1szvbsz2idlz3sbwa4wi")))

(define-public crate-savvy-0.1.16 (c (n "savvy") (v "0.1.16") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libR-sys") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0dcq19hcxd8xidcmk2pr9wjm2mbvdcqyksi37brlrnl74wss9n9a")))

(define-public crate-savvy-0.1.19 (c (n "savvy") (v "0.1.19") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1ckml9hgjv22ji30hkkzk5nbf35kpg7lp6i3vzjkc9rkr83xwksz")))

(define-public crate-savvy-0.2.0 (c (n "savvy") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1ajpdcf278cpi7i2cpq3ibc28d75r4z3x057fzwn62qzbp5r8gfb")))

(define-public crate-savvy-0.2.1 (c (n "savvy") (v "0.2.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "02g01ar4mf3cp6ridgl4j5qxg043ic4lccr7smhbsc0hvd16qbzn")))

(define-public crate-savvy-0.2.2 (c (n "savvy") (v "0.2.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1acy88bmpj4cknzzzssr2xq1i6rbv49gqpqy0120733pf0rhrs67")))

(define-public crate-savvy-0.2.3 (c (n "savvy") (v "0.2.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0hni6nsarz08xcl0darrnbsm7j0m56xh0jpxsg7dpsr5cg2qx8ng")))

(define-public crate-savvy-0.2.4 (c (n "savvy") (v "0.2.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "07q38j2d1hnzhj2yzgcwb0g44l8z45hz0qxjy927ydwqngwxn7ib")))

(define-public crate-savvy-0.2.5 (c (n "savvy") (v "0.2.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1rf1y53pqws28sbrljjb6qd82drvq9vdlpnhv6gc82mc3gc6w2m5")))

(define-public crate-savvy-0.2.6 (c (n "savvy") (v "0.2.6") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1kdq1y6j4dkn2hz24h4gkhjfr0r2kqpwl90j58kb0pj0s2rcrfqa")))

(define-public crate-savvy-0.2.7 (c (n "savvy") (v "0.2.7") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1b18c5jd7jnxy333plrvwam6fcm63cdx5d4qx7li0bkfvfxi11kf")))

(define-public crate-savvy-0.2.8 (c (n "savvy") (v "0.2.8") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1dw2mcm4kny7a7f7l7d0dyd7g6i1yz0f8899nd65vyqa96bg0rs3")))

(define-public crate-savvy-0.2.9 (c (n "savvy") (v "0.2.9") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1dwkvzc5n20l88nd4dnn8n9imlx06n5z6zvh08ldivbb0z7lbx55")))

(define-public crate-savvy-0.2.10 (c (n "savvy") (v "0.2.10") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1kb0cn779mxjygkz4c10hisvqc6jjs31ksg6lm70d809fn2wabbq")))

(define-public crate-savvy-0.2.11 (c (n "savvy") (v "0.2.11") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1bd3vavpban7h8nzkgbfgm5lpv35qrdhmzaldlcs795r9qh6xgzm")))

(define-public crate-savvy-0.2.12 (c (n "savvy") (v "0.2.12") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "1yz4vcd83lx0wlji57kxkghlhs7hvnydmrffm3mpa0xkhiqpcp4d")))

(define-public crate-savvy-0.2.13 (c (n "savvy") (v "0.2.13") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "14is764x3r8hs4nlcm3g9ax6w1appjm214ndr7irgby4lj3lv4y5")))

(define-public crate-savvy-0.2.14 (c (n "savvy") (v "0.2.14") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "060r01lm52ch1fnihqwfjf6qv60c18w6x40xbrv9vgh6mq7jslyj")))

(define-public crate-savvy-0.2.15 (c (n "savvy") (v "0.2.15") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "19gdqbl9nyl9hwic7wkr7ql50kkc285mhgj049k8slsmz3agp1sg") (f (quote (("fake-libR" "savvy-ffi/fake-libR"))))))

(define-public crate-savvy-0.2.16 (c (n "savvy") (v "0.2.16") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0bzn7bhymq9bincm1xgqw9q4w3gi4q53647z47nfzbvsc3q8q64g")))

(define-public crate-savvy-0.2.17 (c (n "savvy") (v "0.2.17") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0") (d #t) (k 0)))) (h "0kbjr552da6x05bap9zpx1n2jl02c6n5fjj90bmm3cdg0l0ymfns")))

(define-public crate-savvy-0.2.18 (c (n "savvy") (v "0.2.18") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r ">=0.2.17") (d #t) (k 0)) (d (n "savvy-macro") (r ">=0.2.15") (d #t) (k 0)))) (h "1kl0362lix28i9yw938ww7zljwnlaxs0hkxpb09g5i7wsnsmxhh5")))

(define-public crate-savvy-0.2.20 (c (n "savvy") (v "0.2.20") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.2.20") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.2.20") (d #t) (k 0)))) (h "0zy3hrmd7v2nv3k7i7l0c9iv4r01hrhfb2vscnmlccsgpjm0ih5y") (r "1.64.0")))

(define-public crate-savvy-0.3.0 (c (n "savvy") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.3.0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.3.0") (d #t) (k 0)))) (h "19l1p5vgacj8saix0yc9fiyfan8gjgy15jb6s3sa9aa9qklaccwa") (r "1.64.0")))

(define-public crate-savvy-0.4.0 (c (n "savvy") (v "0.4.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.4.0") (d #t) (k 0)))) (h "1q95z2fmy8rvhnyym7972xfm7akkv5jw67cnhwim6ix80avh1b3h") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.64.0")))

(define-public crate-savvy-0.4.1 (c (n "savvy") (v "0.4.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.4.1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.4.1") (d #t) (k 0)))) (h "1q9q9dgkzdj09vdwxji50djj3lf457rvd2a12lbqr7ddxr5dqi1j") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.64.0")))

(define-public crate-savvy-0.4.2 (c (n "savvy") (v "0.4.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.4.2") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.4.2") (d #t) (k 0)))) (h "1i9v5zcvw94ggdfyhs0aki7w1dchavwp0560k29m57zizcnjc4xj") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.64.0")))

(define-public crate-savvy-0.5.0 (c (n "savvy") (v "0.5.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.5.0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.5.0") (d #t) (k 0)))) (h "18x673swavap89wf4lp75k92zjzps26kia27f9bhm9fixp2m8d5n") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.64.0")))

(define-public crate-savvy-0.5.1 (c (n "savvy") (v "0.5.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.5.1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.5.1") (d #t) (k 0)))) (h "1665nlra5z92ari3q8m32gm1fqmjsa99bi55g3qv1jm3x651si04") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.64.0")))

(define-public crate-savvy-0.5.2 (c (n "savvy") (v "0.5.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.5.2") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.5.2") (d #t) (k 0)))) (h "0ks371p49rzca17pzgxl1j2jq0f3gnrnx10vf4mpnmpr6vy2raq0") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.65.0")))

(define-public crate-savvy-0.5.3 (c (n "savvy") (v "0.5.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.5.3") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.5.3") (d #t) (k 0)))) (h "1acizf4019z4hkw3kz5ws6csv53fkdqz0v3wfsmmz1d1zy81rw9q") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.65.0")))

(define-public crate-savvy-0.6.0 (c (n "savvy") (v "0.6.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.6.0") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.6.0") (d #t) (k 0)))) (h "1rqbc6zq3iqcgzr605lxf0cl4nk1cmvalynsn279xhzzspkdfvgl") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.65.0")))

(define-public crate-savvy-0.6.1 (c (n "savvy") (v "0.6.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.6.1") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.6.1") (d #t) (k 0)))) (h "1app0a6bhmra88zn3qqxmaa880pqrbf4bj5a7pfb1dfjlvh2pinb") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex")))) (r "1.65.0")))

(define-public crate-savvy-0.6.2 (c (n "savvy") (v "0.6.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.6.2") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.6.2") (d #t) (k 0)))) (h "11szg9y6fagnqdff10cqx446l7wppgwn4hivya7wmj2z4rdjywmd") (f (quote (("default") ("complex" "num-complex" "savvy-ffi/complex") ("altrep" "savvy-ffi/altrep")))) (r "1.65.0")))

(define-public crate-savvy-0.6.3 (c (n "savvy") (v "0.6.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "env_logger") (r "^0.11") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.6.3") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.6.3") (d #t) (k 0)))) (h "1sdqr0kc36mazc5c0wscm4r79m67b252vps0l11qdaf0gd8sbm7l") (f (quote (("logger" "log" "env_logger") ("default") ("complex" "num-complex" "savvy-ffi/complex") ("altrep" "savvy-ffi/altrep")))) (r "1.65.0")))

(define-public crate-savvy-0.6.4 (c (n "savvy") (v "0.6.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "env_logger") (r "^0.11") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "savvy-ffi") (r "^0.6.4") (d #t) (k 0)) (d (n "savvy-macro") (r "^0.6.4") (d #t) (k 0)))) (h "07ahgs5j9wx6rqrk90va7zakpmc46jyz4pvrxrjxm03bxrahgvm0") (f (quote (("savvy-test") ("logger" "log" "env_logger") ("default") ("complex" "num-complex" "savvy-ffi/complex") ("altrep" "savvy-ffi/altrep")))) (r "1.65.0")))

