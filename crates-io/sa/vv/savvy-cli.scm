(define-module (crates-io sa vv savvy-cli) #:use-module (crates-io))

(define-public crate-savvy-cli-0.1.9 (c (n "savvy-cli") (v "0.1.9") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1x8g0m6v1s9z3ah6mkwpm7ws976rc0i6s41iwfda6nxhcpx3iqqa")))

(define-public crate-savvy-cli-0.1.11 (c (n "savvy-cli") (v "0.1.11") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15bpbaif7gzp74585xqifb72vzc1h12s99kdbnsn4a21cbn9xq72")))

(define-public crate-savvy-cli-0.1.13 (c (n "savvy-cli") (v "0.1.13") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hnzmyxvlpwyfmzzkd2vdnxawl477swzdpjrwzza2ibwgbdphfq8")))

(define-public crate-savvy-cli-0.1.14 (c (n "savvy-cli") (v "0.1.14") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0p388f7vsl5wmxsnkpdl9k7lgqbss5lazr0lh77164x9jnzm3zf0")))

(define-public crate-savvy-cli-0.1.15 (c (n "savvy-cli") (v "0.1.15") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0vp4qsgq8drssnfq1hs009i7m926gvq31393xc1rrdcmc761rxvj")))

(define-public crate-savvy-cli-0.1.16 (c (n "savvy-cli") (v "0.1.16") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ac7ahqhkafvmgybxjkkyqnjw2xqmjjdwx4v1912w7fq5ka6zqi1")))

(define-public crate-savvy-cli-0.2.0 (c (n "savvy-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0jj4vrgd490nlx9gxl5xybpk1qgq4krr3jjw80yhymhpha9b3nlh")))

(define-public crate-savvy-cli-0.2.1 (c (n "savvy-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mi7imsc090gfc3yjmr4wg0jky6kpd4f3vkqgjbpvb4h6x3pg99a")))

(define-public crate-savvy-cli-0.2.2 (c (n "savvy-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1kamjyvxmfkys2z5b5nfgqkm2vgi0zihh2m8pb6zyx9gjzlpsci6")))

(define-public crate-savvy-cli-0.2.3 (c (n "savvy-cli") (v "0.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0fj4f494asciz2igy9lxzr5qw9h6hs8gw98r2ar8xjcrqypw03v1")))

(define-public crate-savvy-cli-0.2.5 (c (n "savvy-cli") (v "0.2.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0sxnr58yzx2mkx9bn7y5bydv1p2qdfncp8aq64y8bqflc23jhhjg")))

(define-public crate-savvy-cli-0.2.6 (c (n "savvy-cli") (v "0.2.6") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03blsc2qf1zd10l9hylxfy447ldlr7zx82ag4dhxv7a6kjvamhhl")))

(define-public crate-savvy-cli-0.2.7 (c (n "savvy-cli") (v "0.2.7") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "11rdmzbyzhjkyhqvsh7cm7ykjyvrclg7v9i1qdran5jf6r6xnycr")))

(define-public crate-savvy-cli-0.2.8 (c (n "savvy-cli") (v "0.2.8") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1smjvw6nbyxxpili4z58215rsr78y8j90c5fzb2ff5v6msx8b6wd")))

(define-public crate-savvy-cli-0.2.9 (c (n "savvy-cli") (v "0.2.9") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0bs9mzg8grwn23f24b34p20jp9a8nb7d4jfvh5li37vgrh4d9671")))

(define-public crate-savvy-cli-0.2.10 (c (n "savvy-cli") (v "0.2.10") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0gx66x370w88kd9f651cc8vzpgp31abbrnhpw3550x6dyp23bkym")))

(define-public crate-savvy-cli-0.2.11 (c (n "savvy-cli") (v "0.2.11") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1l2gdnkr035dg9v676c2l67524rxpn3ks02m0mgc5b2cwlhk98ln")))

(define-public crate-savvy-cli-0.2.12 (c (n "savvy-cli") (v "0.2.12") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1nzkmvqn3nq1ch5xg1lk7gjr6vzr18r7jszlgnvyz3qgqhp0bf74")))

(define-public crate-savvy-cli-0.2.13 (c (n "savvy-cli") (v "0.2.13") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0xh5vl49bfa4kr65z1lbkvn0g8wvyvspk0wxv04zl90xznd9bflx") (r "1.74")))

(define-public crate-savvy-cli-0.2.14 (c (n "savvy-cli") (v "0.2.14") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0xcfs8gr8i0im3z2wrm7f9sm212a9s3harw5272zz9axp99lxmm7") (r "1.74")))

(define-public crate-savvy-cli-0.2.15 (c (n "savvy-cli") (v "0.2.15") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1yf8knbriy1wa5lvjjn64psw63mpwskqbji3xa1qarggyqynwa6w") (r "1.74")))

(define-public crate-savvy-cli-0.2.16 (c (n "savvy-cli") (v "0.2.16") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1a4ss2a29gjk6f2r2i371g5ldd7wj5lr4xsg1svhl4l8gkclrbbg") (r "1.74")))

(define-public crate-savvy-cli-0.2.17 (c (n "savvy-cli") (v "0.2.17") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1b4pgrxrpg01vcq2mbf44x8lhld8cvd1mpdka4gvambll72dnrm9") (r "1.74")))

(define-public crate-savvy-cli-0.2.18 (c (n "savvy-cli") (v "0.2.18") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ijwvlsvag2p5vyvms7zn5pvw9jnzzvqg4387y4znya6is21cw7g") (r "1.74")))

(define-public crate-savvy-cli-0.2.20 (c (n "savvy-cli") (v "0.2.20") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1gv6pwc1bxz3c4qxc7r5l1jhnp9yh5hv26ip781agn84r17b4j7r") (r "1.74")))

(define-public crate-savvy-cli-0.3.0 (c (n "savvy-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15zw202m1fpyx68anva114xnkcf0fpsjx79784ng8nmdvf3b1wx0") (r "1.74")))

(define-public crate-savvy-cli-0.4.0 (c (n "savvy-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "144z2r7yp44vn3p9wcmpjjl3vjs7s51clhkn1za9lz893vg6pz4b") (r "1.74")))

(define-public crate-savvy-cli-0.4.1 (c (n "savvy-cli") (v "0.4.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "16b2mv1bi6qcy444r9djdpfy1p6jiarnmaw1k7f74wxkyv2maj7d") (r "1.74")))

(define-public crate-savvy-cli-0.4.2 (c (n "savvy-cli") (v "0.4.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1947z4k1wvh25g29vz1kgvy7mjsvs3gd2bna7gz2zga6sdyd0x1c") (r "1.74")))

(define-public crate-savvy-cli-0.5.0 (c (n "savvy-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rc62p0pl809920pr3fq6h60qnbhv8ca5lc4mhjwws66lz6nfkw5") (r "1.74")))

(define-public crate-savvy-cli-0.5.1 (c (n "savvy-cli") (v "0.5.1") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)))) (h "0xsnyvqiiks9kkhqqx9r05l7nbv2axm0k64mdd5kidnkv9iki3jl") (r "1.74")))

(define-public crate-savvy-cli-0.5.2 (c (n "savvy-cli") (v "0.5.2") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)))) (h "07md41jcppjn6aq3db183j8c89a15xafnhaqjsx778b5ccchxx2b") (r "1.74")))

(define-public crate-savvy-cli-0.5.3 (c (n "savvy-cli") (v "0.5.3") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)))) (h "0f7yqc0sy6278r4lngdpz8k0pr1d22451lr6jfiz5kj1wj8jm4ny") (r "1.74")))

(define-public crate-savvy-cli-0.6.0 (c (n "savvy-cli") (v "0.6.0") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1ddx3hjjb8ymz2vwwvyrzliwyv0gzp1kzkkv3c4ha7slkkza7ccl") (r "1.74")))

(define-public crate-savvy-cli-0.6.1 (c (n "savvy-cli") (v "0.6.1") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1w5phlkfv6y72b38ca23d31l7nkiv8b91q4w6kpw8dsynfbf6f22") (r "1.74")))

(define-public crate-savvy-cli-0.6.2 (c (n "savvy-cli") (v "0.6.2") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1cdp2bqw58s43ynpnlmzsk1iabs011risvrxsi7pl2grfj94pqlj") (r "1.74")))

(define-public crate-savvy-cli-0.6.3 (c (n "savvy-cli") (v "0.6.3") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1jqxki9v8vhqivd8my2j1638ar3inhr95rgfd1knpp6krgz7i45r") (r "1.74")))

(define-public crate-savvy-cli-0.6.4 (c (n "savvy-cli") (v "0.6.4") (d (list (d (n "async-process") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures-lite") (r "^2") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (f (quote ("use_formatter"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0abi63cbnw79aqmbw8ddzkhwgbxkkhy4i1nv8qs7njjh0nady8km") (r "1.74")))

