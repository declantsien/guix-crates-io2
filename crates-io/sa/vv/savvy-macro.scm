(define-module (crates-io sa vv savvy-macro) #:use-module (crates-io))

(define-public crate-savvy-macro-0.1.9 (c (n "savvy-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00siabamj5x8jlsgp97nx2w7w7j5m934lglv16p5bwq6ryn80k9d")))

(define-public crate-savvy-macro-0.1.11 (c (n "savvy-macro") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jm207flglf7zm5l0x26xhwx7ackmi2ba1kv55ka8cqr89y7wmaf")))

(define-public crate-savvy-macro-0.1.12 (c (n "savvy-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n4yallbllin7xfrr1zs7sqip1594g9n3mmzyz5b3airmagrxfym")))

(define-public crate-savvy-macro-0.1.13 (c (n "savvy-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "10s12vihvvb8dcrsnyn80vz7x8jr5i1y138xac96nsqy2n4kdv96")))

(define-public crate-savvy-macro-0.1.14 (c (n "savvy-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "059pzmb4ysrx7r35kb5nv3b8b4j8wvzz36l9dl3sh3dshz81782c")))

(define-public crate-savvy-macro-0.1.15 (c (n "savvy-macro") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jdzzcnbricy605cx8ry76bjm6nfr7psywapx379vl01hkk80nbf")))

(define-public crate-savvy-macro-0.1.16 (c (n "savvy-macro") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1i0r8pv0yanmz7ybmya70x7ygc2j4a7mzwdy7gn1cirn0hq9i7n5")))

(define-public crate-savvy-macro-0.1.19 (c (n "savvy-macro") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0mgn4r47d7pvm74rlw0fxcyqm77ijn0lamn6cjcjmr8b8ill522f")))

(define-public crate-savvy-macro-0.2.0 (c (n "savvy-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fcw1q238b02qc84xl1cxxz8pwcdfr5jzl9sj2bg29fhc6zf5da5")))

(define-public crate-savvy-macro-0.2.1 (c (n "savvy-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1c1vak7rc5p4i6n5cz83ard3iaz1xdzw7074zvbiw062x48x7prk")))

(define-public crate-savvy-macro-0.2.2 (c (n "savvy-macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0gcs426n0aljiagbclciqn2s11kpkp5qdbjppc514z2401vcv4ik")))

(define-public crate-savvy-macro-0.2.3 (c (n "savvy-macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "11h4bylbddgpi8l9jjygr1bn599i0yjgx8a8p4cfbfdz1nkjhbjh")))

(define-public crate-savvy-macro-0.2.5 (c (n "savvy-macro") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0hia2yvpss6jcsj9qdbh30yzhg8l68mizn30hkh5l0dlxszm2pg3")))

(define-public crate-savvy-macro-0.2.6 (c (n "savvy-macro") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1sn6rry87a0346kzj9ih65ppmiki82cn35wyqqxvkp5id7w71zb7")))

(define-public crate-savvy-macro-0.2.7 (c (n "savvy-macro") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0y3rn7xvzrvv77baj6p6cxz0gxdq16jp32y46jrrjg2l82fsiafr")))

(define-public crate-savvy-macro-0.2.8 (c (n "savvy-macro") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0a27gmi5frmm58jc3w3ikq5nwsfnrs71rk9an66w63r8syxripdd")))

(define-public crate-savvy-macro-0.2.9 (c (n "savvy-macro") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0h2cm18wl3332dxp49gg428n6h4fvpcninhskxmhmh07xnfwdb3w")))

(define-public crate-savvy-macro-0.2.10 (c (n "savvy-macro") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "059bq5lbc9na4c6x2ymhnqycv3h5xlc5ag966rh9lcaia2v1dix7")))

(define-public crate-savvy-macro-0.2.11 (c (n "savvy-macro") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "02mynzl076nsv2vpq3vaf9fvwbagjgkcspg6sp5w8ywmf9d65sm5")))

(define-public crate-savvy-macro-0.2.12 (c (n "savvy-macro") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09nxgird5synwxgzfx6hrgg3wx6c5xnlvj16b3z2qxj52wn2h4m5")))

(define-public crate-savvy-macro-0.2.13 (c (n "savvy-macro") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "194jxqchjlf3s8m75wwb0i7vmxsx4x8ajv7ws7xcbly7m8z6rxab")))

(define-public crate-savvy-macro-0.2.14 (c (n "savvy-macro") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1d8jhfr2nsarwrwkp2c8srgk44az44b6jnaa5njfa3mbj2hhjxb4")))

(define-public crate-savvy-macro-0.2.15 (c (n "savvy-macro") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "15kk0cx4sxlhhvsbzp27d1367g226skbj4kdxsvqv6m8p42731bs")))

(define-public crate-savvy-macro-0.2.16 (c (n "savvy-macro") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1ngf8g7bn70q6i04wjirs955dk5an6zrwiga60p3h3rpsjnjpjcf")))

(define-public crate-savvy-macro-0.2.17 (c (n "savvy-macro") (v "0.2.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05lxnypy31sf2qb0d4x6556qi0b976jnb66bvfg4ca7gwq7c07jj")))

(define-public crate-savvy-macro-0.2.18 (c (n "savvy-macro") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r ">=0.2.16") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1ivbvz2xlli9ywblma58xd4f1s9cyzv05jsrx9wvxmimrkxk9q34")))

(define-public crate-savvy-macro-0.2.19 (c (n "savvy-macro") (v "0.2.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.2.19") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0xb0f268av9rxkfbqbyijb9z0832xlncrgvflnhlb8wnmkgxcswg")))

(define-public crate-savvy-macro-0.2.20 (c (n "savvy-macro") (v "0.2.20") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.2.20") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "08vyz4w301n98v0n0hkxfm2qdazb8kxq21j5bh5738myaz8ggyxk")))

(define-public crate-savvy-macro-0.3.0 (c (n "savvy-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1hmzh9hj1nwmd21g0kdzpcq3wdfcn1f2k11bs9ywdiyi5rc4d7c9")))

(define-public crate-savvy-macro-0.4.0 (c (n "savvy-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0zrbs151a2h57g05yplyw7fky1iz3im77bn1jjfwc4p8i2bfhvyx")))

(define-public crate-savvy-macro-0.4.1 (c (n "savvy-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.4.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "11cwfsh4ranqqx987q0q8df56cpxqwi1r4311dzvsjqvvc3xsak9")))

(define-public crate-savvy-macro-0.4.2 (c (n "savvy-macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "152dizw80c9kmk2q1f6ymik64kkasgrc2gjil846kz3k5i42gjsn")))

(define-public crate-savvy-macro-0.5.0 (c (n "savvy-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0x0nqd84zybz9v6da41w10hynxssvamwj5dwv2psmc1s8dpyvihs")))

(define-public crate-savvy-macro-0.5.1 (c (n "savvy-macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0fzjrvwqi1yk5h6cnzn02rwj6ibxirsxzdbkff89ljx622fny0yy")))

(define-public crate-savvy-macro-0.5.2 (c (n "savvy-macro") (v "0.5.2") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0hdcn9pvvrnclc2rc9zpy0rpx3b1v24m07vfg0hwsb2zjdsn86fa")))

(define-public crate-savvy-macro-0.5.3 (c (n "savvy-macro") (v "0.5.3") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.5.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0r50sn3k6011vhcl9ll4cpfjgiqz1sarfs3348ha57bipnnczlpq")))

(define-public crate-savvy-macro-0.6.0 (c (n "savvy-macro") (v "0.6.0") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0fcpq412w520rrm74bpppn5mys8707jya4gcnci33iawrpnhha8k")))

(define-public crate-savvy-macro-0.6.1 (c (n "savvy-macro") (v "0.6.1") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.6.1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "19sbfpnny1pj9s5h1jlx9gxz3ldarxp4zn77qax3jmk4mdfxjn4i")))

(define-public crate-savvy-macro-0.6.2 (c (n "savvy-macro") (v "0.6.2") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.6.2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1x9vlvnx79564raiw55z5zzgw36ysp129fl4k428zx7mipqbw4xi")))

(define-public crate-savvy-macro-0.6.3 (c (n "savvy-macro") (v "0.6.3") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05qxfnzan15w5y88bjdm194gxiihrx112vakjayf1rfb8clxprmq")))

(define-public crate-savvy-macro-0.6.4 (c (n "savvy-macro") (v "0.6.4") (d (list (d (n "insta") (r "^1.38.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "savvy-bindgen") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1bbzbfjqxdim52ciqvciz5hs26dmxn1rdl366lyjbri1dzwxa8p9")))

