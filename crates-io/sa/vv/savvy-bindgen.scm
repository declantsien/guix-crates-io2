(define-module (crates-io sa vv savvy-bindgen) #:use-module (crates-io))

(define-public crate-savvy-bindgen-0.1.9 (c (n "savvy-bindgen") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c28z0akjzvihxjjrhg6x5l26djbci5dcvr5rfamnyryr3kadd9q")))

(define-public crate-savvy-bindgen-0.1.10 (c (n "savvy-bindgen") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v5rwq4c7298pcxp0krxv44918y4vm1fygv25j5cra7mkq82li3i")))

(define-public crate-savvy-bindgen-0.1.11 (c (n "savvy-bindgen") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1csslq067k35h17ks595a1lw1a18i0gia576x164aprl21a3ghdg")))

(define-public crate-savvy-bindgen-0.1.12 (c (n "savvy-bindgen") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p36fc5zjv2skic16wd563rh8vqrmkbs3jlc36hnq2j465695lms")))

(define-public crate-savvy-bindgen-0.1.13 (c (n "savvy-bindgen") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "049j42hwn3031y7xz0zr7mmna2v2wa7lh22gs0cmcm93fsyk16jc")))

(define-public crate-savvy-bindgen-0.1.14 (c (n "savvy-bindgen") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a2yp8syl9k1k2k0niysdgzqrhny9zmp1m1rdrgqwz6d37qympj8")))

(define-public crate-savvy-bindgen-0.1.15 (c (n "savvy-bindgen") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yqnfy22l85s51bpvzjsrjbg2ls2amka1jwjwcsvj3aqpp06binf")))

(define-public crate-savvy-bindgen-0.1.16 (c (n "savvy-bindgen") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bgsp8dz3jrmrrcirldxxq2ymradhwxs5g3lydk0ylp0cq58450f")))

(define-public crate-savvy-bindgen-0.1.19 (c (n "savvy-bindgen") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n59pzp94x26swpfxh912m0fzcfisdqzc5am1gk6rxfd3w37z69x")))

(define-public crate-savvy-bindgen-0.2.0 (c (n "savvy-bindgen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f2xr1bgl1alyqsrn078sgv6cciqxs6adim9vns1i2lpqwvql11m")))

(define-public crate-savvy-bindgen-0.2.1 (c (n "savvy-bindgen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bfagapxz9fffnx87ppdsni8lphg89bjaqgyasd3ji29rzyj9pny")))

(define-public crate-savvy-bindgen-0.2.2 (c (n "savvy-bindgen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b349m6vn2rjajvdq8hxim72shcjnabpwcvvx0g0zhdpnykx3dsk")))

(define-public crate-savvy-bindgen-0.2.3 (c (n "savvy-bindgen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ax3va15crni5252lvcq35p1fnzgnvp0p5pb4dyymladl6na6xcf")))

(define-public crate-savvy-bindgen-0.2.5 (c (n "savvy-bindgen") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gkjy2b9v1qrrn1qib68027g8rlp8qqndadn9hkmg2pb8k0h0x1x")))

(define-public crate-savvy-bindgen-0.2.6 (c (n "savvy-bindgen") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r6lnb4j5nb7wky22s9cfn31bf7gp9pciah73ay0arkvy0a1zmxw")))

(define-public crate-savvy-bindgen-0.2.7 (c (n "savvy-bindgen") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15kgmsr9vfar77f84lnvrf4kz0q4pp5v06c8c4aviwmxalagn3jw")))

(define-public crate-savvy-bindgen-0.2.8 (c (n "savvy-bindgen") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wyq98cq2cgk8xbsmf4fb8da7rkgzbwi6hrni96irhbp62lsbh0p")))

(define-public crate-savvy-bindgen-0.2.9 (c (n "savvy-bindgen") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j1a35qdzikqc5pln3wv99gp5axzzfcmn809sxi9bffkha4np9jw")))

(define-public crate-savvy-bindgen-0.2.10 (c (n "savvy-bindgen") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "060mj5rradl0gg1bljf2h4091505zbzmw9vwx8n3v6ndi0fxxvfl")))

(define-public crate-savvy-bindgen-0.2.11 (c (n "savvy-bindgen") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h5ysrja50b1mz854vfs77hpy64x9f652k6kqdw5w13338yl4bl6")))

(define-public crate-savvy-bindgen-0.2.12 (c (n "savvy-bindgen") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07rzi0xzfgfy0xwfyd3bp5kbpk22s0wjjs4p2mhrhcadgydwf4vm")))

(define-public crate-savvy-bindgen-0.2.13 (c (n "savvy-bindgen") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xmnnd5lpvawhx1s5djmhlizqxcnqanb9ivx4kv899jv2zcb880w")))

(define-public crate-savvy-bindgen-0.2.14 (c (n "savvy-bindgen") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qgh6ah5dj7wlfyr2q4m7y1qvmghs1w59h33wbmqh086rffcwys7")))

(define-public crate-savvy-bindgen-0.2.15 (c (n "savvy-bindgen") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kgim9556cif684b001qgsqs932rkkhl9lp723h47cz1wh18jp9d")))

(define-public crate-savvy-bindgen-0.2.16 (c (n "savvy-bindgen") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1svzdvgzkbyamz3wbz2aqnf6k0v7jg3lj15kjbvhxcfcjks69kw4")))

(define-public crate-savvy-bindgen-0.2.17 (c (n "savvy-bindgen") (v "0.2.17") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07vgfpwl1y42vg0grsrd9nf3kdyxdms1bpyjww1xmwa5rnszajy4")))

(define-public crate-savvy-bindgen-0.2.18 (c (n "savvy-bindgen") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15aivr6yl410h9r3lq27fkmn5dwmmc15gs3993dhd2adjq5l835w")))

(define-public crate-savvy-bindgen-0.2.19 (c (n "savvy-bindgen") (v "0.2.19") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08cpi6s1zls11mnp89488r9y3nysa2piqn23hbxlrsk5lc1b0k1v")))

(define-public crate-savvy-bindgen-0.2.20 (c (n "savvy-bindgen") (v "0.2.20") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02gp9jq6qj73w24p91dfgrgf178xx6dqbs4nbyyc1vr4q2xg098s")))

(define-public crate-savvy-bindgen-0.3.0 (c (n "savvy-bindgen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sdshym1qwxhc8wfmhb0kq58r21va7jyf7bxbgs5747b9icq78rq")))

(define-public crate-savvy-bindgen-0.4.0 (c (n "savvy-bindgen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17i5j31cwlm02v2ab0kjz38gdah6ms1k20lb9adzvc32qcsj2p32")))

(define-public crate-savvy-bindgen-0.4.1 (c (n "savvy-bindgen") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "101vhskb70sisr52pafy2844mcwdznk2b2szzf7n3wwqvj15fdb2")))

(define-public crate-savvy-bindgen-0.4.2 (c (n "savvy-bindgen") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fb3j99gipbw8hwgza41vs5mig1sf0xy2j647g18hy1nydqqx1n9")))

(define-public crate-savvy-bindgen-0.5.0 (c (n "savvy-bindgen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mzkhr8dqfndz6mgv1d3k60cb2b868jgvkaabmq5xlvsyi4yl4xz")))

(define-public crate-savvy-bindgen-0.5.1 (c (n "savvy-bindgen") (v "0.5.1") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1305yvrgwahi5na5glndwwid0mxhhwrffzh4x81d2sq3pzn268dp") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.5.2 (c (n "savvy-bindgen") (v "0.5.2") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mkds8lagkxc96sr317755hwpd33p0a6ja3rfgq7sdrcmxnmz74m") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.5.3 (c (n "savvy-bindgen") (v "0.5.3") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ss8668mmmsq63g12ffp545ckrr5hwq0yyrh8ldx533bk8whqn7x") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.6.0 (c (n "savvy-bindgen") (v "0.6.0") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19jln956j3n8faxyfqga34hf5akad3dh35z46zdqy3s9h4igw35f") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.6.1 (c (n "savvy-bindgen") (v "0.6.1") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w8dqxx507rcwyd6g1xxygfbapp74hdgpfxq7m0gw8alw05wvy30") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.6.2 (c (n "savvy-bindgen") (v "0.6.2") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h35x5mn9agjfldcvamdzqhlvgcw8gzm91pqcz5c296vh3g5ipsd") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.6.3 (c (n "savvy-bindgen") (v "0.6.3") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17hpqg2szvgbq0fdyj0vwscyz7wjmq7m61imsflgcqdpqhvfin72") (f (quote (("use_formatter" "prettyplease") ("default"))))))

(define-public crate-savvy-bindgen-0.6.4 (c (n "savvy-bindgen") (v "0.6.4") (d (list (d (n "prettyplease") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wp70khynp7lbg4vv2imyijmqmp9pp6xjib30lxqzqdp260z53lm") (f (quote (("use_formatter" "prettyplease") ("default"))))))

