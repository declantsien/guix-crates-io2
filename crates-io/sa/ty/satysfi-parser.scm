(define-module (crates-io sa ty satysfi-parser) #:use-module (crates-io))

(define-public crate-satysfi-parser-0.0.1 (c (n "satysfi-parser") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0xf3xbnssdmnz24hf8lc2ck3fxycakvcir9g952m602sl48i83zp")))

(define-public crate-satysfi-parser-0.0.2 (c (n "satysfi-parser") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1w86y8lb1gjqkzsbhr54nyqxpm0xiw1i52bbh5d62lfqg1f5a120")))

(define-public crate-satysfi-parser-0.0.3 (c (n "satysfi-parser") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1mq965hpsk9hj121j5g30q92mjaqhgifkkiqz6gq6q2n581p94mk")))

