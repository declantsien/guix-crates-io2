(define-module (crates-io sa li salih) #:use-module (crates-io))

(define-public crate-salih-0.1.0 (c (n "salih") (v "0.1.0") (h "0v69c3fvyz3gdcg53snvjjybasr3i09fs7qs8dx41r2cnl48v71a") (y #t)))

(define-public crate-salih-0.1.1 (c (n "salih") (v "0.1.1") (h "0c5r5069jn3a8yb2f03nip4357mmx84s4z41h8vgm4xcx71ivmjm") (y #t)))

(define-public crate-salih-0.1.2 (c (n "salih") (v "0.1.2") (h "0gziqzqj81h29anrncaa694xfh51nmzc6dq2g9knkb6srbv60yd8") (y #t)))

(define-public crate-salih-0.1.3 (c (n "salih") (v "0.1.3") (h "13kalgzixg5nnar6wb7hjdzlvpssgz8whyi27478cqkbq5584w2z") (y #t)))

