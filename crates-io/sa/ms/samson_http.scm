(define-module (crates-io sa ms samson_http) #:use-module (crates-io))

(define-public crate-samson_http-0.1.0 (c (n "samson_http") (v "0.1.0") (d (list (d (n "concat-idents") (r "^1.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "19gq415566sibclwg8yxpsvkswvjh8d96h11v473r4xdqhdy0hp4")))

