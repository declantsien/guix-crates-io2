(define-module (crates-io sa ng sanguine) #:use-module (crates-io))

(define-public crate-sanguine-0.1.0-dev (c (n "sanguine") (v "0.1.0-dev") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vbif53k22nn4b8x46i5fcdmp2595g3lkmiz7izf9zi5p67l334l")))

(define-public crate-sanguine-0.1.0-dev-r1 (c (n "sanguine") (v "0.1.0-dev-r1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0yrk0xswpk595z3gxq4vgaip8phllgrxiy84yjd7za56nm7rqv8i")))

(define-public crate-sanguine-0.1.0-dev-r2 (c (n "sanguine") (v "0.1.0-dev-r2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0khhsi2d6rq54qlnxj6vx2jc9di1nzmm807sjcr51ir7cl5p4gbr")))

(define-public crate-sanguine-0.1.0 (c (n "sanguine") (v "0.1.0") (d (list (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1b1nrpnyy7n60yim36psdqrmvxx2wk6if45hfvz1n7pwrpc9y2gz")))

(define-public crate-sanguine-0.1.1 (c (n "sanguine") (v "0.1.1") (d (list (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "139hw5ffan3gc2lwg76vybh59dwlw325kkf6a00625knqw9p4k0m")))

(define-public crate-sanguine-0.2.0 (c (n "sanguine") (v "0.2.0") (d (list (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1spjjx56jv3jbardahvv2q0ghm9mnv7k2dc7mbjcq5yaw77s3mhm")))

(define-public crate-sanguine-0.3.0 (c (n "sanguine") (v "0.3.0") (d (list (d (n "ansi-to-tui") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1jlinnwbkq3jnry7rvxp7djki8siqbr4dnsanaa9rfpd3qa09ba7") (f (quote (("default")))) (s 2) (e (quote (("tui" "dep:ratatui") ("ansi" "tui" "dep:ansi-to-tui"))))))

(define-public crate-sanguine-0.3.1 (c (n "sanguine") (v "0.3.1") (d (list (d (n "ansi-to-tui") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "termwiz") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0c3gr7pi2msaznjj7rq8s4xzvsnq17gc6lgpbhdmh4lf0csvrjbv") (f (quote (("default")))) (s 2) (e (quote (("tui" "dep:ratatui") ("ansi" "tui" "dep:ansi-to-tui"))))))

