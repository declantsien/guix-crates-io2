(define-module (crates-io sa ll sallyport) #:use-module (crates-io))

(define-public crate-sallyport-0.1.0 (c (n "sallyport") (v "0.1.0") (d (list (d (n "goblin") (r "^0.4") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "primordial") (r "^0.3") (d #t) (k 0)))) (h "18nxf7pxdwk3liqs03dn2shbhmj9fmmppdji846m762wkz4zp7n5") (f (quote (("default") ("asm"))))))

(define-public crate-sallyport-0.2.0 (c (n "sallyport") (v "0.2.0") (d (list (d (n "goblin") (r "^0.4") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "primordial") (r "^0.4.0") (d #t) (k 0)))) (h "11s9cbl1a94n8zng0ajjr6ih87m5kk1xvpimvqhh5srgfyai3dpa") (f (quote (("default") ("asm"))))))

(define-public crate-sallyport-0.3.0 (c (n "sallyport") (v "0.3.0") (d (list (d (n "gdbstub") (r "^0.6.0") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.119") (o #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 2)) (d (n "memoffset") (r "^0.6.5") (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)))) (h "013ly8x641zxk7in0p1hh12ls9ykk6j847inhr0w25k0caixnr7j") (f (quote (("doc" "gdbstub" "libc"))))))

(define-public crate-sallyport-0.4.0 (c (n "sallyport") (v "0.4.0") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.102") (o #t) (k 0)) (d (n "libc") (r "^0.2.102") (f (quote ("extra_traits"))) (d #t) (k 2)) (d (n "serial_test") (r "^0.6") (d #t) (k 2)) (d (n "testaso") (r "^0.1") (d #t) (k 2)))) (h "09rrgghk2ifnyczpj47l4dsl68zm6saq5fvkhhlvj3vhln22ilvp") (f (quote (("doc" "gdbstub" "libc"))))))

(define-public crate-sallyport-0.5.0 (c (n "sallyport") (v "0.5.0") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.124") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.6") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "0njm8h2jc3imki9qkp493pxll4bq5bzlyf7w06v93v3xsp7qmpay") (f (quote (("doc" "gdbstub"))))))

(define-public crate-sallyport-0.5.1 (c (n "sallyport") (v "0.5.1") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.126") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.6") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "1dk5mgbbm600xcd25nzifd2nlzzzwrwfim1hs8kbxbd7m5gynqnl") (f (quote (("doc" "gdbstub"))))))

(define-public crate-sallyport-0.6.0 (c (n "sallyport") (v "0.6.0") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.126") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.8") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "1y6r8xj0raag63cy5hpyl69gidxmlwj90k9glzva2bj93ryr2rpl") (s 2) (e (quote (("doc" "dep:gdbstub"))))))

(define-public crate-sallyport-0.6.1 (c (n "sallyport") (v "0.6.1") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.126") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.8") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "12vj0v0lbg1khlvpwnaz0lpvcxjrx8cz3diy9aisf6x7jl6sigbp") (s 2) (e (quote (("doc" "dep:gdbstub"))))))

(define-public crate-sallyport-0.6.2 (c (n "sallyport") (v "0.6.2") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.126") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.8") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "0i2zivkil06p0nng3b1r9sa0i9n3jrcjmydf299yqx10ywn657pl") (s 2) (e (quote (("doc" "dep:gdbstub"))))))

(define-public crate-sallyport-0.6.3 (c (n "sallyport") (v "0.6.3") (d (list (d (n "gdbstub") (r "^0.6") (o #t) (k 0)) (d (n "goblin") (r "^0.5") (f (quote ("elf64"))) (k 0)) (d (n "libc") (r "^0.2.126") (f (quote ("extra_traits"))) (k 2)) (d (n "serial_test") (r "^0.8") (k 2)) (d (n "testaso") (r "^0.1") (k 2)))) (h "1y97zkmx54x24k1n5l31g174582n0zvj35n8cdiv652ncvl5whm7") (s 2) (e (quote (("doc" "dep:gdbstub"))))))

