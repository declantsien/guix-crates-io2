(define-module (crates-io sa w_ saw_mcr) #:use-module (crates-io))

(define-public crate-saw_mcr-0.1.0 (c (n "saw_mcr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11imcbqqkcjb1d8njh14mcg241mk79z09lpyckba6sccacvrqrid")))

(define-public crate-saw_mcr-0.2.0 (c (n "saw_mcr") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01prw9jagzk9kcvwbi1llfyxl2w6jmllpny7ava93lh7a5s6mpp4")))

