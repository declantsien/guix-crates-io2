(define-module (crates-io sa vg savgol-rs) #:use-module (crates-io))

(define-public crate-savgol-rs-0.1.0 (c (n "savgol-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "lstsq") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "polyfit-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 2)))) (h "0w677ixgif956mqhrmnprsgvpy90wm2k2dmh2lz20vcn5qb4dgvd")))

