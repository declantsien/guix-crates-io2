(define-module (crates-io sa br sabre-sdk) #:use-module (crates-io))

(define-public crate-sabre-sdk-0.0.1 (c (n "sabre-sdk") (v "0.0.1") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)))) (h "0c519x178ak1mb1jhd845p20hdzfm6pzdvk6016dacfzpcmr6fpl")))

(define-public crate-sabre-sdk-0.1.3 (c (n "sabre-sdk") (v "0.1.3") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)))) (h "1sbl2q4i48qy64n5v99h0w3m6dlvpm4fd6fwdmgarz8lvvvwyxfi")))

(define-public crate-sabre-sdk-0.2.0 (c (n "sabre-sdk") (v "0.2.0") (d (list (d (n "protobuf") (r "^2") (d #t) (k 0)))) (h "0ivg5x4f6lxivzlkfin7yy24smv4550pv9zawmdlbkrwa0f5xs13")))

(define-public crate-sabre-sdk-0.3.0 (c (n "sabre-sdk") (v "0.3.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)))) (h "1b8wrqawvmdb3j98h92wqacxflnyz9fqxbj0rczzq0bz1a3q2ih6")))

(define-public crate-sabre-sdk-0.4.1 (c (n "sabre-sdk") (v "0.4.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)))) (h "14rbcpfizp95y62r8rmraxpwn9mvl9lsywh7wr5dflhm7g6k2i3a") (y #t)))

(define-public crate-sabre-sdk-0.4.0 (c (n "sabre-sdk") (v "0.4.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)))) (h "00rpyqhqrkz303bxia7rh8r0cd27ivi94ipzcq1hq6k92l1y3dlr")))

(define-public crate-sabre-sdk-0.4.2 (c (n "sabre-sdk") (v "0.4.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)))) (h "1lrdlq67z1f2g58p33z9binxk14bi5124bbpg4pa9wicf91wyplr")))

(define-public crate-sabre-sdk-0.5.0 (c (n "sabre-sdk") (v "0.5.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0pjs6wcrr00d681zy038rswbklggnrp10m54m1hls971r6pszlvv")))

(define-public crate-sabre-sdk-0.5.1 (c (n "sabre-sdk") (v "0.5.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02mvnw5mwmk0ck4g0l9a34msgwcm9f3pm7c4a201zyql8qslhykk")))

(define-public crate-sabre-sdk-0.5.3-dev1 (c (n "sabre-sdk") (v "0.5.3-dev1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "182mkscs5mrr2bf7fsq3xnp24rlnhnnbfsq8x17v0l55czisi0fx") (y #t)))

(define-public crate-sabre-sdk-0.5.2 (c (n "sabre-sdk") (v "0.5.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0kafjhbv5kp9mq24sfjmc9zlfzk9rgq41ahwhvq0vj2f51ymaki9")))

(define-public crate-sabre-sdk-0.6.0 (c (n "sabre-sdk") (v "0.6.0") (d (list (d (n "cylinder") (r "^0.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1253xxdpnanx0jzyq0yb5xd00qmmmks71047z5pwyaf5d95f50rd")))

(define-public crate-sabre-sdk-0.6.1 (c (n "sabre-sdk") (v "0.6.1") (d (list (d (n "cylinder") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "07lrxl1wmhymarmakjkvwqmvqf2dd0pz4wzk5rh7lavmq7mrprki")))

(define-public crate-sabre-sdk-0.7.1 (c (n "sabre-sdk") (v "0.7.1") (d (list (d (n "cylinder") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1n0ylma02wzxm5fil8fy9krvpi3n32y5995zik9w3n0qbyg5r3dp")))

(define-public crate-sabre-sdk-0.7.2 (c (n "sabre-sdk") (v "0.7.2") (d (list (d (n "cylinder") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "protobuf") (r "^2.19") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "transact") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1zna7qyczm9m59gc21bzvsd07qhh9r1jv3xqak10grzs7qhgjk3m")))

(define-public crate-sabre-sdk-0.8.1 (c (n "sabre-sdk") (v "0.8.1") (d (list (d (n "cylinder") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "protobuf") (r "^2.19") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "transact") (r "^0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1yp39favmqq9y8dpwlgjwlg6dr69h6gw348air3rzn85qb2534mf") (f (quote (("stable" "default") ("experimental" "stable") ("default"))))))

