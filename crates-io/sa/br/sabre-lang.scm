(define-module (crates-io sa br sabre-lang) #:use-module (crates-io))

(define-public crate-sabre-lang-0.1.1 (c (n "sabre-lang") (v "0.1.1") (d (list (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "19w28vn98bn6f1khhs6i08fja8pnspgbkkbjq7jmzpcj7g1a2xpq") (f (quote (("default" "backend_node") ("backend_node") ("backend_c"))))))

(define-public crate-sabre-lang-0.2.0 (c (n "sabre-lang") (v "0.2.0") (d (list (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "052f9ljlw0cbqghfkm1y25hv4qydc8cay32818lwml7qa6ilk5w4") (f (quote (("default" "backend_node") ("backend_node") ("backend_c"))))))

(define-public crate-sabre-lang-0.2.1 (c (n "sabre-lang") (v "0.2.1") (d (list (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0h9f1n8cb20frq2ssy3gphh3ba3zqc85hlxwsdbnq1l1ywqngf8a") (f (quote (("default" "backend_node") ("backend_node") ("backend_c"))))))

(define-public crate-sabre-lang-0.3.0 (c (n "sabre-lang") (v "0.3.0") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1bwlzagdb5c24hg300xqkzcy5vzmj4mkv4km9zwin6c05392bss0") (f (quote (("default" "backend_node") ("backend_node") ("backend_llvm" "inkwell") ("backend_c"))))))

(define-public crate-sabre-lang-0.4.0 (c (n "sabre-lang") (v "0.4.0") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0lrjp5ax1d006nd7zz4i5kwws08n7z2syjfj9y09c114vzysjg8j") (f (quote (("default" "backend_node") ("backend_node") ("backend_llvm" "inkwell") ("backend_c"))))))

(define-public crate-sabre-lang-0.5.0 (c (n "sabre-lang") (v "0.5.0") (d (list (d (n "inkwell") (r "^0.1.0-beta.2") (f (quote ("llvm10-0"))) (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0x35axm0fibar6w0sgas6pigwfjmjbr3rd1sj1jqd4vds4k7g14j") (f (quote (("default" "backend_node") ("backend_node") ("backend_llvm" "inkwell") ("backend_c"))))))

