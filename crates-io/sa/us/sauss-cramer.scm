(define-module (crates-io sa us sauss-cramer) #:use-module (crates-io))

(define-public crate-sauss-cramer-0.1.0 (c (n "sauss-cramer") (v "0.1.0") (h "1m2r9pcxgz3srqpjp6kpinxczbn4wq46665cnjkry6y6s33glb4n")))

(define-public crate-sauss-cramer-0.1.1 (c (n "sauss-cramer") (v "0.1.1") (h "0mfn47vmqzm95cq24wgv51p6vwjxnywg4p1zncl352fkvyyw3dzf") (y #t)))

(define-public crate-sauss-cramer-0.1.2 (c (n "sauss-cramer") (v "0.1.2") (h "0qcd72x3s6yzs2lk2z13xvgvl3m4l760man6mcxxw8776hbdpxn6") (y #t)))

(define-public crate-sauss-cramer-0.1.3 (c (n "sauss-cramer") (v "0.1.3") (h "05h9pbhqb5zdxy355b22az7bigrl1ygywrk3n96xm2zpbj0yq3ns") (y #t)))

(define-public crate-sauss-cramer-0.1.4 (c (n "sauss-cramer") (v "0.1.4") (h "19y6p6c42dqa3li02kvkp363zkpqb6bl0a2ic7r21g4dwn0vjgzm") (y #t)))

(define-public crate-sauss-cramer-0.1.5 (c (n "sauss-cramer") (v "0.1.5") (h "1pjv72da06hfqsi9rqhkpidd7qnmx5474ffdxjdlb4101rja1wfg") (y #t)))

(define-public crate-sauss-cramer-1.0.0 (c (n "sauss-cramer") (v "1.0.0") (h "1pln9g1f89rpm2zs5kn4ayhs51aib8lj9gyfnklmbmxyxlb4dwrx")))

(define-public crate-sauss-cramer-1.0.1 (c (n "sauss-cramer") (v "1.0.1") (h "0khp3vr4y5dj79s1s2vd1hhi2wk8n40brxddpkdbkh4sfx5n9kxm")))

(define-public crate-sauss-cramer-1.0.2 (c (n "sauss-cramer") (v "1.0.2") (h "11jrcq8i5h8z2h5wqny1dxmwkdr9027sj3mhbq3pm73ka20xizsg")))

