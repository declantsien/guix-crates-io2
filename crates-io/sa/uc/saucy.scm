(define-module (crates-io sa uc saucy) #:use-module (crates-io))

(define-public crate-saucy-0.1.0 (c (n "saucy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1yn00vdd2504f7nc4y18xqp0i8kj5nrv1m3i6cyswi5fzvypwb65")))

(define-public crate-saucy-0.1.1 (c (n "saucy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1g5ymbpc2k4bl01vzinlzpawjgxixqfcx4wsayfil8l5ldm19x40")))

(define-public crate-saucy-0.1.2 (c (n "saucy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "056bqx0pp2jdbphh8x65d23cp901rlqp8zmbrn50chqqsnp0b31z")))

(define-public crate-saucy-0.1.3 (c (n "saucy") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dunce") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "09fgac14jmy4pqqdgj2qh049kjzzxz3kn786ij5kbhw5m04pysri")))

