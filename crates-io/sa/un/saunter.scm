(define-module (crates-io sa un saunter) #:use-module (crates-io))

(define-public crate-saunter-0.1.0 (c (n "saunter") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "1cwl5cxq5702wknqzhn6k27yi02s4ig8z10zfvd4mzz4fywijxq8")))

(define-public crate-saunter-0.1.1 (c (n "saunter") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "1bakh09dhvcqvgmd30sdyqlsa56lvhpv77rqrkmhh16n0bc77264")))

(define-public crate-saunter-0.1.2 (c (n "saunter") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "0g8qsxqcgpqxq4k6y3vs5q13h3qmrj9igja93ds6ill47in791h8")))

(define-public crate-saunter-0.1.3 (c (n "saunter") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "0l09pyliankm2bi9cdijfw5n3mfa2hikp8kd1dkw87pqby110xv1")))

(define-public crate-saunter-0.1.4 (c (n "saunter") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "087fzffajj122zym5hgknnj0khgxnn5hy5asnbmpwqqyxx538zs3") (y #t)))

(define-public crate-saunter-0.1.5 (c (n "saunter") (v "0.1.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "1lg5cqn4mwfpz1snxlmy41idra2jddzqf976x7wq4kd53k6x0j0z")))

(define-public crate-saunter-0.1.6 (c (n "saunter") (v "0.1.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "0j59h7kvibwfmh8pbxqj74lz6j61hww8sppi5qjmn11kzfqh9v9y")))

(define-public crate-saunter-0.1.7 (c (n "saunter") (v "0.1.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "08dvq1xl8pl2qmipmn2d0flj1rz9kmb86gw2zjxq2b241nzrxhb2")))

(define-public crate-saunter-0.2.0 (c (n "saunter") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "00n6ml9y8az8sd8mwidf901bbd6bd70vhrxpqrn8169a2y1w350z")))

(define-public crate-saunter-0.3.0 (c (n "saunter") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "saunter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 2)))) (h "06v0d4vn1ywr9aa78xwy79fj5wfw81b8950q58dbka8zlcd2xmsr") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:saunter-derive"))))))

(define-public crate-saunter-0.3.1 (c (n "saunter") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "saunter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.4") (d #t) (k 2)))) (h "0525pwi5vzdsmk9hgrdbp1azq3l413bc96bw06qjnjxjpqgbp6im") (f (quote (("default" "derive")))) (s 2) (e (quote (("derive" "dep:saunter-derive"))))))

