(define-module (crates-io sa le saleae) #:use-module (crates-io))

(define-public crate-saleae-0.0.1 (c (n "saleae") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "bufstream") (r "^0.1.1") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1.7") (d #t) (k 0)))) (h "1503al1h3mgb4fdb84s9lx6ggcgq275my17b0p8d5abam8iin87i")))

(define-public crate-saleae-0.1.0 (c (n "saleae") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1.7") (d #t) (k 0)) (d (n "faux") (r "^0.0.4") (d #t) (k 0)))) (h "1a1yv87fd0i0pn8m0n2rsydrz0x0gp45v1m074ckd9dylp5kqlbf")))

