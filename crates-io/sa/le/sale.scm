(define-module (crates-io sa le sale) #:use-module (crates-io))

(define-public crate-sale-0.1.0 (c (n "sale") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.14.0") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.6.1") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^0.6.0") (d #t) (k 0)) (d (n "cw20") (r "^0.6.1") (d #t) (k 0)) (d (n "cw20-base") (r "^0.6.1") (f (quote ("library"))) (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0zxhdd8ajnh4s33rfrfiagn49km1kqr6y9b4wr53b59nmqpb047r") (f (quote (("backtraces" "cosmwasm-std/backtraces")))) (r "1.58.1")))

