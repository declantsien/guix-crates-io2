(define-module (crates-io sa le saleae-importer) #:use-module (crates-io))

(define-public crate-saleae-importer-1.0.0 (c (n "saleae-importer") (v "1.0.0") (d (list (d (n "binrw") (r "^0.10.0") (d #t) (k 0)))) (h "1ykq6rdk1mj8rw166gvjnbp051hn1nbxqk1dpck6q9l4j36h9a34") (y #t)))

(define-public crate-saleae-importer-1.0.1 (c (n "saleae-importer") (v "1.0.1") (d (list (d (n "binrw") (r "^0.10.0") (d #t) (k 0)))) (h "1zj3swjv4vj7di12d4pg4k6x1xd9c4gx3h21h7r7d589n8qxk9c2")))

