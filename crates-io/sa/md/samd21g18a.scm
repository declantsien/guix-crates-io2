(define-module (crates-io sa md samd21g18a) #:use-module (crates-io))

(define-public crate-samd21g18a-0.1.0 (c (n "samd21g18a") (v "0.1.0") (d (list (d (n "bare-metal") (r "0.1.*") (d #t) (k 0)) (d (n "cortex-m") (r "0.3.*") (d #t) (k 0)) (d (n "cortex-m-rt") (r "0.3.*") (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "008pvrnqi4jmd02rbafxclxs9fl9lgm37aiwpkz01pbwjadhpirn")))

(define-public crate-samd21g18a-0.2.0 (c (n "samd21g18a") (v "0.2.0") (d (list (d (n "bare-metal") (r "0.1.*") (d #t) (k 0)) (d (n "cortex-m") (r "0.3.*") (d #t) (k 0)) (d (n "cortex-m-rt") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "09r8a378rcsirkpiqzbcpymawkgnksjkv0pdrgmhkgrsaf7gzsxa") (f (quote (("rt-abort-on-panic" "cortex-m-rt/abort-on-panic") ("rt" "cortex-m-rt"))))))

(define-public crate-samd21g18a-0.2.1 (c (n "samd21g18a") (v "0.2.1") (d (list (d (n "bare-metal") (r "0.1.*") (d #t) (k 0)) (d (n "cortex-m") (r "0.3.*") (d #t) (k 0)) (d (n "cortex-m-rt") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "vcell") (r "0.1.*") (d #t) (k 0)))) (h "0m751vhhmd348c78s3j3q9cszb6vzivn1v2wilv88bhbwijy8ffy") (f (quote (("rt-abort-on-panic" "cortex-m-rt/abort-on-panic") ("rt" "cortex-m-rt"))))))

