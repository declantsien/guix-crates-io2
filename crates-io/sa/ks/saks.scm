(define-module (crates-io sa ks saks) #:use-module (crates-io))

(define-public crate-saks-0.1.0 (c (n "saks") (v "0.1.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rppal") (r ">=0.11.0, <0.12.0") (d #t) (k 2)))) (h "18zv8w8b7wdaskcwvspcwba6wbivh2g9dv5ar0kjm0h5misblry7")))

(define-public crate-saks-0.1.1 (c (n "saks") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h0drm0mvq0wrqq9mzchfsw93sc2czkh22xy8y2rs47i4gwsiyjn")))

