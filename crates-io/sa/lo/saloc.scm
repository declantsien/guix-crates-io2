(define-module (crates-io sa lo saloc) #:use-module (crates-io))

(define-public crate-saloc-0.0.0 (c (n "saloc") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "saloc-lib") (r "^0.0.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "035l0x4whj7fcf90h2p0q356sqyydx9nviv1rv8liqcpspr2gy6s")))

