(define-module (crates-io sa lo saloc-lib) #:use-module (crates-io))

(define-public crate-saloc-lib-0.0.0 (c (n "saloc-lib") (v "0.0.0") (d (list (d (n "ariadne") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (f (quote ("issue-url"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.6") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)) (d (n "typestate") (r "^0.8.0") (d #t) (k 0)))) (h "16wb4b5r9nprxds2z8ijij9xbdd7dnv2dzzl3l6f863m6sj41ji6")))

