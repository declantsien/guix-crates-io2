(define-module (crates-io sa ci sacio) #:use-module (crates-io))

(define-public crate-sacio-0.1.0 (c (n "sacio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "flinn_engdahl") (r "^0.1.0") (d #t) (k 0)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 0)))) (h "04599438k1cw6km54pmyajqasw8nx1026d1i635507w2kb1882wf")))

