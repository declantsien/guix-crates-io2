(define-module (crates-io sa ge sage_mqtt) #:use-module (crates-io))

(define-public crate-sage_mqtt-0.1.0 (c (n "sage_mqtt") (v "0.1.0") (h "0p2rbx8isfwzhlyvapa5icv4x6mslhn84cqgqlcn1vxvfwiz81c7")))

(define-public crate-sage_mqtt-0.1.1 (c (n "sage_mqtt") (v "0.1.1") (h "1f16z6nlflq59ppzzari36hqza6ylnck6al37fhv4id9jl145z1a")))

(define-public crate-sage_mqtt-0.1.3 (c (n "sage_mqtt") (v "0.1.3") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "0rh2dfdkyrfsb00rjnm1w7ra49k0smf0c9r8phsilm59n2dgh0kk")))

(define-public crate-sage_mqtt-0.1.4 (c (n "sage_mqtt") (v "0.1.4") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "0kim3b0kd7mnm4amxbgvbrkfbbww8377rs0va696lvcin4p1mw1i")))

(define-public crate-sage_mqtt-0.1.5 (c (n "sage_mqtt") (v "0.1.5") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1sm53rrl2rakd317zrsj3ag4znyf12wr0by17v8v1marha3jf43i")))

(define-public crate-sage_mqtt-0.1.6 (c (n "sage_mqtt") (v "0.1.6") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "09s51whp96shwk8iw6ai249bxnvc1nr9pib77c4r6lqzg06d7xl8")))

(define-public crate-sage_mqtt-0.1.7 (c (n "sage_mqtt") (v "0.1.7") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1yfhgmiwi60wgjv1lm3zw9cmp61cq0dw6ff7h5jwflv58dj37h33")))

(define-public crate-sage_mqtt-0.2.0 (c (n "sage_mqtt") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0-beta.1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1m6nk9anscwmaz3ar63s23wwkn1p1bavi6mfa7qq6fr01wavvbd7")))

(define-public crate-sage_mqtt-0.2.1 (c (n "sage_mqtt") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "023ijil73415m9q0wq7sycpf68ab9y29lgi55mmzv4p250k0y6xc")))

(define-public crate-sage_mqtt-0.2.2 (c (n "sage_mqtt") (v "0.2.2") (d (list (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1rckv3khw3avijc675q74jgiv4h6h1pgq5429qv08sglmffdhgia") (y #t)))

(define-public crate-sage_mqtt-0.2.3 (c (n "sage_mqtt") (v "0.2.3") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "17c25rhah30s4iw6nyf9igyk5146wp640m5wrrwvddx0zwn6dvfr")))

(define-public crate-sage_mqtt-0.2.4 (c (n "sage_mqtt") (v "0.2.4") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1lq3hdy6gcibg450a3x0d5ky9nbqc7hblxwn7zgqz075pvmvs3hi")))

(define-public crate-sage_mqtt-0.2.5 (c (n "sage_mqtt") (v "0.2.5") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "1bnyd1ylpm2qg79p4im0s2mm0nn6rgcg6j86w7sw1r0k8y5rn8n8")))

(define-public crate-sage_mqtt-0.2.6 (c (n "sage_mqtt") (v "0.2.6") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "09vv9dcrqxhz8pjqvsqcbmc5lpbw8q1yqczd25d0vn1zzh0n6xpc")))

(define-public crate-sage_mqtt-0.2.7 (c (n "sage_mqtt") (v "0.2.7") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "18zqjinnnk0b2wrrg1ga3az3cl2882wz94hzikifdhh6r2vqhh9z")))

(define-public crate-sage_mqtt-0.3.0 (c (n "sage_mqtt") (v "0.3.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "0qhln6ripg9wr4h1ypfd10q42v0md1vcjf0m8xhipjxszvl5yz40")))

(define-public crate-sage_mqtt-0.4.0 (c (n "sage_mqtt") (v "0.4.0") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "0rd41bpj6xmhl4il9cn500qql795mpgi4nmk9rdg91mnawylkv2g")))

(define-public crate-sage_mqtt-0.4.1 (c (n "sage_mqtt") (v "0.4.1") (d (list (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "190vf243akidklmr652qsnm5nzk8hmwdhvvlpj1z7pyy36la9qc1")))

(define-public crate-sage_mqtt-0.5.0 (c (n "sage_mqtt") (v "0.5.0") (d (list (d (n "tokio") (r "^1.15.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt" "io-util"))) (d #t) (k 2)) (d (n "unicode_reader") (r "^1.0.0") (d #t) (k 0)))) (h "18jw6pbwmik01kfh1n402i0rq8pma3q54wj6z2qjr9rakpq93dqb")))

