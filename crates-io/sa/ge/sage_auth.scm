(define-module (crates-io sa ge sage_auth) #:use-module (crates-io))

(define-public crate-sage_auth-0.1.0 (c (n "sage_auth") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "url") (r "^2.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "12hyy6ai7l6lqzb2mmli75rzr5bc137qcjy6j25qh1wh4rlnzdzx")))

