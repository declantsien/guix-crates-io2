(define-module (crates-io sa nd sandvox) #:use-module (crates-io))

(define-public crate-sandvox-0.1.0 (c (n "sandvox") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "clamp") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "nd_iter") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "0f2vk93mnm5m5rmg2dzr7lnlmah6jm575vh2247l7lx890rsvhkl")))

(define-public crate-sandvox-0.2.0 (c (n "sandvox") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "clamp") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "nd_iter") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 0)))) (h "0ymg8051f8q4j3dz3vydr8axpnlwy5rc7raab22fi33fgg07nzmf")))

