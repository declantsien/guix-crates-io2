(define-module (crates-io sa nd sand) #:use-module (crates-io))

(define-public crate-sand-0.1.0 (c (n "sand") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "14mxv8jahgsqx9gjdmis5b3rj9bpj10nbjb73ccga1cblhf32crj")))

(define-public crate-sand-0.2.0 (c (n "sand") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "19grrczsx7p2v41cvqyrm3kyakbp7jj77dlakpgyzzgavz72sc3r")))

(define-public crate-sand-0.3.0 (c (n "sand") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "0301qa0xfblvhqlafk54qpycs3bm30xvrzlg1r85q2cfvz2ih4cn")))

(define-public crate-sand-0.4.0 (c (n "sand") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "0v3qsba8h3jp1jkxa3lay493ki6h4rfbxcsmxby2xylck693iks7")))

(define-public crate-sand-0.5.0 (c (n "sand") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "simon") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1gf3hxxddfdfr56rdi5vxl9vjxydiydgj7y9hwi9m0l8w85ad03y")))

(define-public crate-sand-0.5.1 (c (n "sand") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "meap") (r "^0.1") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1rid74cfkdg3hm3dzaizxjpvhb90sx0x63zp0p2f0pv06zrbwghp")))

(define-public crate-sand-0.5.2 (c (n "sand") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "meap") (r "^0.4") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "time"))) (d #t) (k 0)))) (h "1wawb4c1iqr33nwd8d0jni8314bzfaybr56skznk686fagj02rhi")))

(define-public crate-sand-0.5.3 (c (n "sand") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "meap") (r "^0.5") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "17bamkpyn0v0l6f98rbhylffn6pi4dnkgjryb68kd7wb6r9aj87n")))

