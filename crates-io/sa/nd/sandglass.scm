(define-module (crates-io sa nd sandglass) #:use-module (crates-io))

(define-public crate-sandglass-0.1.1 (c (n "sandglass") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ybs767wpg02yfpdc6c3x07cifpbwnf8bz97jx0hppm600gqbq5c")))

(define-public crate-sandglass-0.1.2 (c (n "sandglass") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.8") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dins2ipyd9ds25l4hb7fkgqbdg1p6n15ajjw473vsr55za83khx")))

