(define-module (crates-io sa nd sandbox_guessing_game) #:use-module (crates-io))

(define-public crate-sandbox_guessing_game-0.1.0 (c (n "sandbox_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0lvza6h76v1q4pgij9yj2wdzgf2kcc55hnf03lq0j0vy31pgfpcg")))

(define-public crate-sandbox_guessing_game-0.1.1 (c (n "sandbox_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0i1pgijh0kwcpdw94538yk8g3ncakmlgfcyfbh5jfllfgy0yb31n")))

(define-public crate-sandbox_guessing_game-0.1.2 (c (n "sandbox_guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0gq7b35wszz2y27lx3qpjcjmwk2pchl9a5cmrhph4n2ycgfmx290")))

(define-public crate-sandbox_guessing_game-0.1.3 (c (n "sandbox_guessing_game") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "13rxk3jpgmc3a0db3ggrkdj2jblpn6vaw40yzswc64czmw8pnc1g")))

