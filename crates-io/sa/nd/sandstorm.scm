(define-module (crates-io sa nd sandstorm) #:use-module (crates-io))

(define-public crate-sandstorm-0.0.1 (c (n "sandstorm") (v "0.0.1") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)))) (h "1sr19m36h17kfwmwp7xvhib5w93819m35yn9f18lx1a41ag987la")))

(define-public crate-sandstorm-0.0.2 (c (n "sandstorm") (v "0.0.2") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)))) (h "065fyz39aq0bhj71kmrssfqahk98l2nxf7p8my4dixg1mcy7q9b1")))

(define-public crate-sandstorm-0.0.3 (c (n "sandstorm") (v "0.0.3") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)))) (h "1zsbiybmybi9f27d3mz3705b0hbj7ks9mxmvv1vxzhph9qdr7vb1")))

(define-public crate-sandstorm-0.0.4 (c (n "sandstorm") (v "0.0.4") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)))) (h "10vwjn5dzpn9523fwrdajkvsy4lc1083k9zp37s9kagq179ay10j")))

(define-public crate-sandstorm-0.0.5 (c (n "sandstorm") (v "0.0.5") (d (list (d (n "capnp") (r "^0.7") (d #t) (k 0)) (d (n "capnpc") (r "^0.7") (d #t) (k 1)))) (h "0i9xjdhc81ypkv19sib23mdg0gqc1l0jmkl97xmrbc1jk5z65gpb")))

(define-public crate-sandstorm-0.0.6 (c (n "sandstorm") (v "0.0.6") (d (list (d (n "capnp") (r "^0.8") (d #t) (k 0)) (d (n "capnpc") (r "^0.8") (d #t) (k 1)))) (h "0cn99z71n4bf0q34m1i0fnywx3jb5j1hbsvav2gjb3z9h2c10k43")))

(define-public crate-sandstorm-0.0.7 (c (n "sandstorm") (v "0.0.7") (d (list (d (n "capnp") (r "^0.9") (d #t) (k 0)) (d (n "capnpc") (r "^0.9") (d #t) (k 1)))) (h "0sccfxx8i71mgd7a3r6pqq8fwmzh1vvcfjwh4rmbv9sq6sk5l0s7")))

(define-public crate-sandstorm-0.0.8 (c (n "sandstorm") (v "0.0.8") (d (list (d (n "capnp") (r "^0.9") (d #t) (k 0)) (d (n "capnpc") (r "^0.9") (d #t) (k 1)))) (h "0rvp5jjff791dq8nxnxvi1yj9pc00fdxj0sdbqyhgk095jyci28w")))

(define-public crate-sandstorm-0.0.9 (c (n "sandstorm") (v "0.0.9") (d (list (d (n "capnp") (r "^0.10") (d #t) (k 0)) (d (n "capnpc") (r "^0.10.1") (d #t) (k 1)))) (h "00fdn3pvpq3ghvdkzmkgmj9ii00knbxmd19b02nrgmf1s9agarkn")))

(define-public crate-sandstorm-0.0.10 (c (n "sandstorm") (v "0.0.10") (d (list (d (n "capnp") (r "^0.11") (d #t) (k 0)) (d (n "capnpc") (r "^0.11") (d #t) (k 1)))) (h "168nmm7ckfr8yq7azclwvzm8l47sllf0j6cbwi45bq5lm4kyamsi")))

(define-public crate-sandstorm-0.0.11 (c (n "sandstorm") (v "0.0.11") (d (list (d (n "capnp") (r "^0.12") (d #t) (k 0)) (d (n "capnpc") (r "^0.12") (d #t) (k 1)))) (h "1wjh1zqs6j0wq0lz3517zqhhihgxqh3fwcyvw0spfm2kqcz6f6xb")))

(define-public crate-sandstorm-0.0.12 (c (n "sandstorm") (v "0.0.12") (d (list (d (n "capnp") (r "^0.12") (d #t) (k 0)) (d (n "capnpc") (r "^0.12") (d #t) (k 1)))) (h "1v7kx1nqs2awiyddx0s8h2m3nd8g1x3vb6115mzm1kk816c9dams")))

(define-public crate-sandstorm-0.0.13 (c (n "sandstorm") (v "0.0.13") (d (list (d (n "capnp") (r "^0.12") (d #t) (k 0)) (d (n "capnpc") (r "^0.12") (d #t) (k 1)))) (h "0ybqhnbw792hjvmmm09h48aykjbyhw4bxvbvpk5aivr8fbk5wn3w")))

(define-public crate-sandstorm-0.0.14 (c (n "sandstorm") (v "0.0.14") (d (list (d (n "capnp") (r "^0.13") (d #t) (k 0)) (d (n "capnpc") (r "^0.13") (d #t) (k 1)))) (h "00p20i96nh0wj91hc2q9ixw6bb5j3gc672z5h4x78xj35c7pxn4s")))

(define-public crate-sandstorm-0.0.15 (c (n "sandstorm") (v "0.0.15") (d (list (d (n "capnp") (r "^0.14") (d #t) (k 0)) (d (n "capnpc") (r "^0.14") (d #t) (k 1)))) (h "1a72pfwvaisd19h40sqy5vlyl3j6y7rpp9z33nvjdcf05b72ilyf")))

(define-public crate-sandstorm-0.0.16 (c (n "sandstorm") (v "0.0.16") (d (list (d (n "capnp") (r "^0.15") (d #t) (k 0)) (d (n "capnpc") (r "^0.15") (d #t) (k 1)))) (h "07xz4y767jr90qk93dlly0aph09kpdgrnkkqac4p4swv9nfwvvyq")))

(define-public crate-sandstorm-0.0.17 (c (n "sandstorm") (v "0.0.17") (d (list (d (n "capnp") (r "^0.16") (d #t) (k 0)) (d (n "capnpc") (r "^0.16") (d #t) (k 1)))) (h "0j8dg79f8i3xhz5jfih5f473kv635r34bs3j46acv00hl0v596cy")))

(define-public crate-sandstorm-0.0.18 (c (n "sandstorm") (v "0.0.18") (d (list (d (n "capnp") (r "^0.17") (d #t) (k 0)) (d (n "capnpc") (r "^0.17") (d #t) (k 1)))) (h "0p7dxg17kgpf70ipwp9pnnw13hsvxqfqmsrkdda94s825g320dz6")))

(define-public crate-sandstorm-0.0.19 (c (n "sandstorm") (v "0.0.19") (d (list (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)))) (h "0yz4xl9i06lda5qryksdixq1zal3gkrzsln0l5j7i3kkjxay9n33")))

(define-public crate-sandstorm-0.18.0 (c (n "sandstorm") (v "0.18.0") (d (list (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)))) (h "13kjir99w12jac1d2g1g4635avwzk4wvh4sj2xp5h7h4jlgssl5i")))

(define-public crate-sandstorm-0.19.0 (c (n "sandstorm") (v "0.19.0") (d (list (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)))) (h "03xhji38jd70g4zlrf50fs21y2ky9h5glc7l1dlqas379k7svs4y")))

