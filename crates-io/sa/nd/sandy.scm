(define-module (crates-io sa nd sandy) #:use-module (crates-io))

(define-public crate-sandy-0.1.0 (c (n "sandy") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xcbilfq88ppwgvz1rq2cnr14w2iafydwm4hvhiq3cxcvr5cwws7") (y #t)))

(define-public crate-sandy-0.2.0 (c (n "sandy") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11r4amks4si4hhj4r95jbrxr3pl1zpqv6aiahsig3zrqzflwpaik") (y #t)))

(define-public crate-sandy-0.2.1 (c (n "sandy") (v "0.2.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s42jqa97f7s6vjwgbrdlnbiprkprrb302g1qq77agy92icbql8j") (y #t)))

(define-public crate-sandy-0.2.2 (c (n "sandy") (v "0.2.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19kj9gncmdr2gchrvay7fhxrfdllzkfw8vpn46ld10xlmzpiwcb2")))

(define-public crate-sandy-0.2.3 (c (n "sandy") (v "0.2.3") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wkpxgml2wi3lr851k8njn0lq7vf7ciw55z8lm5mnmqpyza7hnzf")))

(define-public crate-sandy-0.2.4 (c (n "sandy") (v "0.2.4") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04d3by0r29ckxc81wkfwv1z8y8bz6b1c64sgrp2ja17rwbil28ky")))

(define-public crate-sandy-0.2.5 (c (n "sandy") (v "0.2.5") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jhdj2ns8wyv0avsysw78wy8wj9xv4n1yyv4dhj038vjq8w3faqc")))

(define-public crate-sandy-0.2.6 (c (n "sandy") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wjdfrl7066p7hkx4py7lip6n5q329vqc5rw0ymjfgxvk23vzkd4")))

(define-public crate-sandy-0.2.7 (c (n "sandy") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lsyi3bdpq7v5r2qlrhn19wq8dqxkb72gd7p9878hqsg08s0an1q")))

