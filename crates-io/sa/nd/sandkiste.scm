(define-module (crates-io sa nd sandkiste) #:use-module (crates-io))

(define-public crate-sandkiste-0.0.1 (c (n "sandkiste") (v "0.0.1") (h "165hpi7ahy631g1yrh1pppwg67d4dlkcc3jqxh2gpy05ka02y0rs")))

(define-public crate-sandkiste-0.0.2 (c (n "sandkiste") (v "0.0.2") (h "06kfa7v3rsqpwhdzzhx4cc2az5ki7q63j1072rz2qkxa3h27sr69")))

(define-public crate-sandkiste-0.0.3 (c (n "sandkiste") (v "0.0.3") (h "07ih0x8y6a422v0ajr5rcayb2jbymxl8bwxin3kws0bng873l0xl")))

(define-public crate-sandkiste-0.0.4 (c (n "sandkiste") (v "0.0.4") (h "0yq4fz7yyqvj0jwdn6hm0ngxk12vap6ws2fgrm4v3sm1xym6259r")))

(define-public crate-sandkiste-0.0.5 (c (n "sandkiste") (v "0.0.5") (h "1577s83vfyq703f9nj0aab6cjxzh8gvi40df6n0i3my4iyvzirki")))

(define-public crate-sandkiste-0.1.0 (c (n "sandkiste") (v "0.1.0") (h "1f85g2mamyr1nzbzrz0lrndc9vh4b5ph5c0x6mrcyhcbn1yh7pvn")))

(define-public crate-sandkiste-0.2.0 (c (n "sandkiste") (v "0.2.0") (h "0ash1rnggsrmgf7sq1iv5xakdx0zb868w2navsv2q6rlpkfls84s")))

(define-public crate-sandkiste-0.3.0 (c (n "sandkiste") (v "0.3.0") (h "09qcg17v4mbjg8r041ln991vvd998klsa4cn2j2xc3vpkq0wsbq9")))

