(define-module (crates-io sa nd sandkiste_lua) #:use-module (crates-io))

(define-public crate-sandkiste_lua-0.0.1 (c (n "sandkiste_lua") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.1") (d #t) (k 0)))) (h "1z4firxy532c6m2gq6lq4av01hhjkz5fk06qb6rh9slw9rv2lw93") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.2 (c (n "sandkiste_lua") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.2") (d #t) (k 0)))) (h "1jdfc1hgr5vkibzpm3gbxw3v1bpcvp02vd48nis75yxyzh1cffvd") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.3 (c (n "sandkiste_lua") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.2") (d #t) (k 0)))) (h "0np3rkr2nh49kbxxqyipr8kgvxra3l17x6236zfgwxra94vydnhp") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.4 (c (n "sandkiste_lua") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.2") (d #t) (k 0)))) (h "0g6658fa29zrhrnk46yg7ill1m32y02dam77n34mhadjgpvc4p2p") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.5 (c (n "sandkiste_lua") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.3") (d #t) (k 0)))) (h "18dcn6zfxdxrrnnjsy8c3j0r84qgnmbr63j7vm2jza36vza6nl4y") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.6 (c (n "sandkiste_lua") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.4") (d #t) (k 0)))) (h "1ai5n963h0g7pr0inc41zn4p7vfbb1kifq5prr06r61g8y60xvyi") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.0.7 (c (n "sandkiste_lua") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.5") (d #t) (k 0)))) (h "1y982w6lr54svh7k6y4dyqn5n9cr21x99g3jrb0f3c25jsy7w797") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.1.0 (c (n "sandkiste_lua") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.0.5") (d #t) (k 0)))) (h "10v2f80c68zl6gxy5gskf2zm5bc6x3mxskzc4njj7bw5x5lm63z1") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.2.0 (c (n "sandkiste_lua") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.1.0") (d #t) (k 0)))) (h "1jpalgnh488zs92jdzrn1vaw0zspm2clv7vyvvnc0k14r7yjqk16") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.3.0 (c (n "sandkiste_lua") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.2.0") (d #t) (k 0)))) (h "1zn8xazhl3jfrfhv3q4xkwr6c77pqd54rnhmf37p9iwjswcs9n8n") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.3.1 (c (n "sandkiste_lua") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.2.0") (d #t) (k 0)))) (h "1m295fcf0s2cfr9w2rx4i4jdppq2a3d3dymrpczqj9sm8049y1m7") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

(define-public crate-sandkiste_lua-0.4.0 (c (n "sandkiste_lua") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "sandkiste") (r "^0.3.0") (d #t) (k 0)))) (h "19fg6p1w7z479sr4739qfvxylva5pvhp0gfh6zmjqcvnw1x6sh4c") (f (quote (("Lua5_4") ("Lua5_3")))) (l "lua")))

