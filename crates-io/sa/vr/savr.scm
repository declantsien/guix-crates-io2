(define-module (crates-io sa vr savr) #:use-module (crates-io))

(define-public crate-savr-0.1.0 (c (n "savr") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pam") (r "^0.7.0") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (d #t) (k 0)) (d (n "xkbcommon") (r "^0.4") (f (quote ("x11"))) (d #t) (k 0)))) (h "0mncwzjzqcwmcj5ja7qkg978l7l9262hclmkigffig0qc4vhsw46")))

