(define-module (crates-io sa me same-types) #:use-module (crates-io))

(define-public crate-same-types-0.1.0 (c (n "same-types") (v "0.1.0") (h "1r4q0fqkab9dax1b48vc0v8wjz5i9a26m3m0s4m1hqjin19v0zys")))

(define-public crate-same-types-0.1.1 (c (n "same-types") (v "0.1.1") (h "0jas0afqdllwipax8idghhgzyan1mwdbf147xk0l69psw5p0p5p3")))

