(define-module (crates-io sa me samesame) #:use-module (crates-io))

(define-public crate-samesame-0.1.0 (c (n "samesame") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1igbzxglnpzi1iryaks8bsm4i1f1qgqhqjcxac9c8hnsc9z6l58f")))

(define-public crate-samesame-0.1.1 (c (n "samesame") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0ahnnf8007a4mc459xf2f1apsf5sj15xrx99ffw68g1lph68v416")))

