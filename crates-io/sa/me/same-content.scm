(define-module (crates-io sa me same-content) #:use-module (crates-io))

(define-public crate-same-content-0.1.0 (c (n "same-content") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1qvb3z32iicx82kjq0skbnl3w44dz6xdjgmjaq1bvfvdcccmvx1f")))

(define-public crate-same-content-0.1.1 (c (n "same-content") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "04vaanrd84gbdppf51h51kdq2h93r3v9kmfpzcwgisazyhxcxd7q")))

(define-public crate-same-content-0.1.2 (c (n "same-content") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0i4jaan7jvj2wxh8i5ldvhaz5mvqn4v9ki7dfxc76m3v9bq97j3p")))

(define-public crate-same-content-0.1.3 (c (n "same-content") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10q2b3fnyc53h8p10hw9pv7g4pp8w42wzj9wki9nfn4q905992fm")))

(define-public crate-same-content-0.1.4 (c (n "same-content") (v "0.1.4") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("test-util" "fs" "io-util" "macros"))) (o #t) (d #t) (k 0)))) (h "00s2dssgpynh69kahix1441n4ipfqvis448p1g2xnnggfp7a5ikh")))

(define-public crate-same-content-0.1.5 (c (n "same-content") (v "0.1.5") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "0a0n9ysabga0j9np623mq82zki0i304q594r0n8ga0b7bjl95s9z")))

(define-public crate-same-content-0.1.6 (c (n "same-content") (v "0.1.6") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "06ah78625ypg014yc50pvzcryiinscgvs42ha233wwphnjf76212")))

(define-public crate-same-content-0.1.7 (c (n "same-content") (v "0.1.7") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "15kkkdvv1jkjjd46fv81cqzqblkhy4l2p9iczqkfz6j78j0xvwhs")))

(define-public crate-same-content-0.1.8 (c (n "same-content") (v "0.1.8") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1ximidcay481rwlbdnnzyjpk7f0kd60r2jimqhm2mqm4k04hwvs7")))

(define-public crate-same-content-0.1.9 (c (n "same-content") (v "0.1.9") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "00gw1yk22sk32ycgprrcga6q6kwm8s42vllxfqhkhzvh0v8gvhq4") (y #t) (r "1.60")))

(define-public crate-same-content-0.1.10 (c (n "same-content") (v "0.1.10") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("fs" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "058cq8651rs7j1c6qwzgi4ybyk8wvv92hw0d4qjdf2fxga6vmspq") (r "1.63")))

