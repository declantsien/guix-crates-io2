(define-module (crates-io sa me same-file) #:use-module (crates-io))

(define-public crate-same-file-0.1.0 (c (n "same-file") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1hs7cramm7fdbckyd8azj4yn28f1klz3p58b99d8knl4zzairs9s")))

(define-public crate-same-file-0.1.1 (c (n "same-file") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0xb86028c9480agddwsvws94l5zipby34pyzl17wggglh1vdrx85")))

(define-public crate-same-file-0.1.2 (c (n "same-file") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1sjn31kwaf7240c343vwf3qqlmfcchrb2cb45acdfaj3ipkbs8n7")))

(define-public crate-same-file-0.1.3 (c (n "same-file") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "19qpl6j8s3ph9jm8rh1k0wp2nkyw5ah34xly00vqcfx4v97s8cfr")))

(define-public crate-same-file-1.0.0 (c (n "same-file") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0q7vh55js530nfszikxn0rh0c88bdnrprcs1l2k9rys5swh8g8bh")))

(define-public crate-same-file-1.0.1 (c (n "same-file") (v "1.0.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "fileapi" "minwindef" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qjadrz5jg8mys52lxs31wgrkngzmxxsa0i1j2wbi91d8zq7l9gk")))

(define-public crate-same-file-1.0.2 (c (n "same-file") (v "1.0.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "fileapi" "minwindef" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0drnizb3jq6pqgrrdxn2c91rp30ki50g1g6xr09bb8061gnyvdng")))

(define-public crate-same-file-1.0.3 (c (n "same-file") (v "1.0.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "133jvna1m0c5b18bcs4f49p8hblnqpsra3l4cr45jzys5x77kxqh")))

(define-public crate-same-file-1.0.4 (c (n "same-file") (v "1.0.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0rsjk8zjppgc083jyx89hxi39xnjf12x2aqv3x6gz8d8afzc884g")))

(define-public crate-same-file-1.0.5 (c (n "same-file") (v "1.0.5") (d (list (d (n "winapi-util") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "08a4zy10pjindf2rah320s6shgswk13mqw7s61m8i1y1xpf8spjq")))

(define-public crate-same-file-1.0.6 (c (n "same-file") (v "1.0.6") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "winapi-util") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "00h5j1w87dmhnvbv9l8bic3y7xxsnjmssvifw2ayvgx9mb1ivz4k")))

