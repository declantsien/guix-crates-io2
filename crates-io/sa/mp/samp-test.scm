(define-module (crates-io sa mp samp-test) #:use-module (crates-io))

(define-public crate-samp-test-0.1.2 (c (n "samp-test") (v "0.1.2") (d (list (d (n "fern") (r "^0.5.7") (d #t) (k 0)) (d (n "samp-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "samp-sdk") (r "^0.9.2") (d #t) (k 0)))) (h "1wikp0ic3s3f7bd2c951hbjw3craavjsnzmpg5f00k2zjpx08nf1") (f (quote (("encoding" "samp-sdk/encoding") ("default"))))))

