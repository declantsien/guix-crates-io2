(define-module (crates-io sa mp samplr) #:use-module (crates-io))

(define-public crate-samplr-0.1.0 (c (n "samplr") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.2") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "077n68gwc0wrdaghg26siql7mpz48h7rw60mpkhzn9sf566hldwf") (f (quote (("statistical_tests"))))))

