(define-module (crates-io sa mp sampling) #:use-module (crates-io))

(define-public crate-sampling-0.1.0 (c (n "sampling") (v "0.1.0") (d (list (d (n "average") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (f (quote ("serde1"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 2)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "1ijqkmlny57l7hpq1gs72lfa8mpgwnawir8a74n7knc3gdvh7w52") (f (quote (("sweep_time_optimization") ("sweep_stats" "sweep_time_optimization") ("serde_support" "serde") ("replica_exchange" "rayon") ("default" "serde_support" "bootstrap" "replica_exchange") ("bootstrap" "average"))))))

(define-public crate-sampling-0.1.1 (c (n "sampling") (v "0.1.1") (d (list (d (n "average") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (f (quote ("serde1"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 2)) (d (n "transpose") (r "^0.2") (d #t) (k 0)))) (h "1r1sywqcx3ya0mv6x2a53n236pjb6s8p0dnk8by8a5sn000v4z4b") (f (quote (("sweep_time_optimization") ("sweep_stats" "sweep_time_optimization") ("serde_support" "serde") ("replica_exchange" "rayon") ("default" "serde_support" "bootstrap" "replica_exchange") ("bootstrap" "average"))))))

