(define-module (crates-io sa mp sample) #:use-module (crates-io))

(define-public crate-sample-0.0.3 (c (n "sample") (v "0.0.3") (h "1ix71m9jnf2b6bvdr02ix6hi3g6x34paxpsmcgzxv5l85p6y8f3y")))

(define-public crate-sample-0.0.4 (c (n "sample") (v "0.0.4") (h "10bv7nlzzxz7wfyn3kxwfn075d0cw5mn8mdir0rkp94kyv1al9fk")))

(define-public crate-sample-0.1.0 (c (n "sample") (v "0.1.0") (h "1wrq6a4l979cnh2jb16wzyxnj50jrk4n7zhkrfpbza9k9hfn20qd")))

(define-public crate-sample-0.1.1 (c (n "sample") (v "0.1.1") (h "172s1494q4qa06il6fpznqi8q0k1mi1m3lpq85p1g4vjc5zcp2r4")))

(define-public crate-sample-0.1.2 (c (n "sample") (v "0.1.2") (h "00gnr14sfxxj0792zfpvzhmha5wqhq8880y8kg9j62fsfmjfiiq4")))

(define-public crate-sample-0.1.3 (c (n "sample") (v "0.1.3") (h "1gbvf48cf79j24wnk1gm6dba1q3cb924bf8xkcxabnkriy9vcglg")))

(define-public crate-sample-0.2.0 (c (n "sample") (v "0.2.0") (h "12qbh0qmblfcr8xw01p4fgfpkgizkmz005p3dh77bgzjfy00xb47")))

(define-public crate-sample-0.3.0 (c (n "sample") (v "0.3.0") (h "12pvaxna1piys7cq3dli2ivq01gs2qyx848g3gif05w2vgipk0lv")))

(define-public crate-sample-0.4.0 (c (n "sample") (v "0.4.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^1.1.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)))) (h "1a26x5336phmwg18il316hhzr88xrh0ms1nj6pc77q4x4wm908rj")))

(define-public crate-sample-0.5.0 (c (n "sample") (v "0.5.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^1.1.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)))) (h "11kz1i9ilblg56ppd2shakiggqf8n5idkrjg8l49dmnw2p1q6kgy")))

(define-public crate-sample-0.5.1 (c (n "sample") (v "0.5.1") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^1.1.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)))) (h "0dvyqlvsmh7silhzknagx80ks116f354prdzaw80yzdm08a5yfrd") (f (quote (("no_std"))))))

(define-public crate-sample-0.6.0 (c (n "sample") (v "0.6.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^1.1.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)))) (h "0c0v9mnmfxa951afgqch6bqmc4rn8lnlk4sqciyr2fpfacndrv4z") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.6.1 (c (n "sample") (v "0.6.1") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 2)) (d (n "hound") (r "^1.1.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.6.3") (d #t) (k 2)))) (h "0q9sg583w8x2nyh36hkqgwad15l45c72ayw4frj1vb0hlm08p7qf") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.6.2 (c (n "sample") (v "0.6.2") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "0v6p225gjszmwjlsssic8mswh0bq89rn0rnxhgh8d4lhr44nmyw0") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.7.0 (c (n "sample") (v "0.7.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "1n9vjmx6g0fsiiq3x0d5ryx741lr68ydvcsvpxskj3r4srvlg9dx") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.7.1 (c (n "sample") (v "0.7.1") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "1p7k3ca7grz89sv3lpdvnlpcmycmwsjzj1lbl57762fsg6mfxljj") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.8.0 (c (n "sample") (v "0.8.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "126c00kah89z6nddy5acwn9k8pkz2yznz15cs7mfnzc6gf1nvaph") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.8.1 (c (n "sample") (v "0.8.1") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "1lrcw9kappgyqpcq4fx4f7w5sg9vydwrqhwv9rp30vikh3pz8llk") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.9.0 (c (n "sample") (v "0.9.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "14m2myngzy5cavwbfqj12yhhl3d1xwy7428w4lgx1gfjnrfwkwxb") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.9.1 (c (n "sample") (v "0.9.1") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "0vd75x0yhnkb1dbnqkm35hkn1bpa549pj1wwm3cf7hy88cw1v3vc") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.10.0 (c (n "sample") (v "0.10.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "0s71pxx75z6jmxm31ydp3033x1dp2730agk4caajiiwfg2nnc2fc") (f (quote (("std") ("default" "std"))))))

(define-public crate-sample-0.11.0 (c (n "sample") (v "0.11.0") (d (list (d (n "find_folder") (r "^0.3") (d #t) (k 2)) (d (n "hound") (r "^2.0") (d #t) (k 2)) (d (n "portaudio") (r "^0.7") (d #t) (k 2)))) (h "00klzvx5fxclcdv5mjl3bj1jcq2mqn0sdkc1z2fbb7n63n999fin") (f (quote (("std") ("default" "std"))))))

