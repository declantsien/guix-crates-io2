(define-module (crates-io sa mp sample-arrow2) #:use-module (crates-io))

(define-public crate-sample-arrow2-0.1.0 (c (n "sample-arrow2") (v "0.1.0") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "sample-std") (r "^0.1.0") (d #t) (k 0)) (d (n "sample-test") (r "^0.1.0") (d #t) (k 2)))) (h "1b0pqyxsv6qjg16xsj8isx5maj6cdmy12rg31b7gc14cg4npqmav")))

(define-public crate-sample-arrow2-0.17.0 (c (n "sample-arrow2") (v "0.17.0") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "sample-std") (r "^0.1") (d #t) (k 0)))) (h "0cgz0yz02fqcab5nygfj92l9p4laiw6cjq8x5jxgq24i0mzszppj")))

(define-public crate-sample-arrow2-0.17.1 (c (n "sample-arrow2") (v "0.17.1") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "sample-std") (r "^0.1") (d #t) (k 0)) (d (n "sample-test") (r "^0.1") (d #t) (k 2)))) (h "1yva5d1fs5xr6agvyvjg10b8fxmq9s1jvllg76an2l462pwm083j")))

(define-public crate-sample-arrow2-0.17.2 (c (n "sample-arrow2") (v "0.17.2") (d (list (d (n "arrow2") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "sample-std") (r "^0.2") (d #t) (k 0)) (d (n "sample-test") (r "^0.2") (d #t) (k 2)))) (h "0xw9mdhzb0wx26b4bq1d94ib179lmzc5kfsrhgp5gk75g84k0ash")))

