(define-module (crates-io sa mp samplers) #:use-module (crates-io))

(define-public crate-samplers-0.1.0 (c (n "samplers") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "statrs") (r "^0.12.0") (d #t) (k 0)))) (h "1iiqqxkmiivsjv58hx31ijz0zp31nvdif05xyhh4aidbdrgwn97x")))

(define-public crate-samplers-0.1.1 (c (n "samplers") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "statrs") (r "^0.12.0") (d #t) (k 0)))) (h "1x4aha5pg699mkrjid5csw0w1acm8qjb2j90mrwkbvrgry5kdxcn")))

(define-public crate-samplers-0.1.2 (c (n "samplers") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "statrs") (r "^0.12.0") (d #t) (k 0)))) (h "1d8nnw1560wjg3p360spsnz7pggr3s3h63d9bdvkm5q9bladkwzf")))

(define-public crate-samplers-0.1.3 (c (n "samplers") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "statrs") (r "^0.12.0") (d #t) (k 0)))) (h "0rx6c1q99kl9zf6q5xp9kvlj2cg7da9m4al7kvc8zdsj7gfm9515")))

