(define-module (crates-io sa mp samp-sdk) #:use-module (crates-io))

(define-public crate-samp-sdk-0.8.0 (c (n "samp-sdk") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "19rhl8lrnphkcx5xc3zz84aaxfkgkilmgp28ndxh9h98xpnc5nz3")))

(define-public crate-samp-sdk-0.8.1 (c (n "samp-sdk") (v "0.8.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1n8rkax9r66q5amdi5na83bh4fvdc3gmnx39jizcld4fkk2janr1")))

(define-public crate-samp-sdk-0.8.2 (c (n "samp-sdk") (v "0.8.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "19fbkfn0500253mb6dx37zj4shcv169x3kry2dxw791faw0hh8rf")))

(define-public crate-samp-sdk-0.8.3 (c (n "samp-sdk") (v "0.8.3") (d (list (d (n "detour") (r "^0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1k0ffqfqz932300an786mc26b9ajxlsm31fa8g8va9kzhb2a23ga")))

(define-public crate-samp-sdk-0.8.4 (c (n "samp-sdk") (v "0.8.4") (d (list (d (n "detour") (r "^0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "0315hl5ybca1nr2las8nvd4f5ip4nqsdimw510iqxm9dj7ckcdjv")))

(define-public crate-samp-sdk-0.9.0 (c (n "samp-sdk") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.17") (o #t) (d #t) (k 0)))) (h "14i9q4hin2q7v97a5wixzrkjy19cwia5nj6d0jmf6jpi73mvcz1s") (f (quote (("encoding" "encoding_rs") ("default"))))))

(define-public crate-samp-sdk-0.9.1 (c (n "samp-sdk") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.17") (o #t) (d #t) (k 0)))) (h "0g3zkymac3qwli2lr65n983fmqa8x804jxi989fzqsc4g3q1xznv") (f (quote (("encoding" "encoding_rs") ("default"))))))

(define-public crate-samp-sdk-0.9.2 (c (n "samp-sdk") (v "0.9.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 1)) (d (n "encoding_rs") (r "^0.8.17") (o #t) (d #t) (k 0)))) (h "1fxsvy5xkvw90522qlqgb14zz5g1d8hw32clp1s1nas673sl86y1") (f (quote (("encoding" "encoding_rs") ("default"))))))

