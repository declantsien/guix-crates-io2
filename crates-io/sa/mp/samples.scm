(define-module (crates-io sa mp samples) #:use-module (crates-io))

(define-public crate-samples-1.0.5 (c (n "samples") (v "1.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "181f88rbax816d21bnxsas4rzh39k643d598dy881jxbx1pz8cgr")))

