(define-module (crates-io sa mp sample-test-macros) #:use-module (crates-io))

(define-public crate-sample-test-macros-0.1.0 (c (n "sample-test-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sample-std") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d2krw05wa8m498banv8zc2vs7744wjf6s0rs6jwk7y4frfzzysv")))

(define-public crate-sample-test-macros-0.1.1 (c (n "sample-test-macros") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sample-std") (r "^0.1") (d #t) (k 0)) (d (n "sample-test") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kjkavrzz8zs6jkjya6l73ggy4bi6yma67gdnsasx6i55a1jq6nz")))

(define-public crate-sample-test-macros-0.2.0 (c (n "sample-test-macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sample-std") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lv1ln11wqz7dgz1279x7654i66rda4yd6777vy603s5mk6gr7z3")))

(define-public crate-sample-test-macros-0.2.1 (c (n "sample-test-macros") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sample-std") (r "^0.2") (d #t) (k 0)) (d (n "sample-test") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vxmx4i0hcnbbaky4d7xpzw2ckff00bkkdmdzn0lbfw9fnd47ijw")))

