(define-module (crates-io sa mp samplics) #:use-module (crates-io))

(define-public crate-samplics-0.1.0 (c (n "samplics") (v "0.1.0") (d (list (d (n "blas") (r "^0.20") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "08cn8vlbfd9vya3pj67j7m4a6vzj357mda7lwxss6qc8sa281q70")))

(define-public crate-samplics-0.1.1 (c (n "samplics") (v "0.1.1") (d (list (d (n "blas") (r "^0.20") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "179mg6lc9slvq1cp491vnrmvfxic6mnb608n4zvjip9d220ky581")))

