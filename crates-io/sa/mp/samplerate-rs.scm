(define-module (crates-io sa mp samplerate-rs) #:use-module (crates-io))

(define-public crate-samplerate-rs-0.1.0 (c (n "samplerate-rs") (v "0.1.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "0dhank3y346cvvpkcn5mgzqswq76ip18yf97a3xg8b1hzi8zhj9l")))

