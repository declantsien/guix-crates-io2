(define-module (crates-io sa mp sample-std) #:use-module (crates-io))

(define-public crate-sample-std-0.1.0 (c (n "sample-std") (v "0.1.0") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng" "alloc"))) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "0h2mgl7c99qlpmpdajqqlbsvkc4h15qrqxgvaapl0mi14az53gxy")))

(define-public crate-sample-std-0.1.1 (c (n "sample-std") (v "0.1.1") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng" "alloc"))) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "sample-test") (r "^0.1") (d #t) (k 2)))) (h "1j2p3bj3c8gcxrfx5402zygm5l564p09bc0gj8wfhb1hqcyiayjn")))

(define-public crate-sample-std-0.2.0 (c (n "sample-std") (v "0.2.0") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng" "alloc"))) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)))) (h "07d4k1kmb62chrhsgyhb1sl6w5glxx616jprjck3ia5rnr6a41s9")))

(define-public crate-sample-std-0.2.1 (c (n "sample-std") (v "0.2.1") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom" "small_rng" "alloc"))) (k 0)) (d (n "rand_regex") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "sample-test") (r "^0.2") (d #t) (k 2)))) (h "1j2q8flzjjcbfnqkj08fyqa2dwdwigxaxn04w2hjqazbqqcx52wl")))

