(define-module (crates-io sa mp sampled-dmc) #:use-module (crates-io))

(define-public crate-sampled-dmc-0.1.0 (c (n "sampled-dmc") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1xw9dydz4834ci91fk0blmhjcag5na3i7biniqhhvixj54aa6hnv")))

(define-public crate-sampled-dmc-0.1.1 (c (n "sampled-dmc") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "078kl4ilk5v01va9x5q0zzc4wrcd4bsd3svd5lmhf2pqnid0460b")))

(define-public crate-sampled-dmc-0.1.2 (c (n "sampled-dmc") (v "0.1.2") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "dashmap") (r "^4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0v6pfjgi2794pixmw3x59lz9dgjfy50hv0w8d3s0jnw155989x43")))

