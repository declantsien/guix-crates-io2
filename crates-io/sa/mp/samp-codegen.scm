(define-module (crates-io sa mp samp-codegen) #:use-module (crates-io))

(define-public crate-samp-codegen-0.1.0 (c (n "samp-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1irdz5fnmfnmwa7ds4rypynhafi8lj8fcs2r5ikalmxygxm0i5sr")))

(define-public crate-samp-codegen-0.1.1 (c (n "samp-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1sz5bacipfxhxg7x2c9ll5g9pidsld1gs4rmb4k196hn8kg2m42g")))

