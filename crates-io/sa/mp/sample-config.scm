(define-module (crates-io sa mp sample-config) #:use-module (crates-io))

(define-public crate-sample-config-0.1.0 (c (n "sample-config") (v "0.1.0") (d (list (d (n "sample-config-macros") (r "^0.1.0") (d #t) (k 0)))) (h "086nrlws430wvwpqgb879ixd9z8hkcay180anwv8ms3s30qdl4cs") (f (quote (("yaml" "sample-config-macros/yaml") ("default" "yaml"))))))

(define-public crate-sample-config-0.2.0 (c (n "sample-config") (v "0.2.0") (d (list (d (n "json5") (r "^0.4.1") (d #t) (k 2)) (d (n "sample-config-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (o #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 2)))) (h "1qnnhq3i49fg7h8wzn9bk0nzfaamn4sgnpalzgd4xz24w160258p") (f (quote (("yaml" "sample-config-macros/yaml") ("json" "sample-config-macros/json") ("default" "yaml" "json")))) (s 2) (e (quote (("url" "dep:url") ("tracing" "dep:tracing"))))))

