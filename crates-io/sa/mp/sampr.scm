(define-module (crates-io sa mp sampr) #:use-module (crates-io))

(define-public crate-sampr-0.1.0 (c (n "sampr") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.61") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("macros" "time" "signal"))) (d #t) (k 2)))) (h "0sxc87b2y6n6xwf58znnk8hmy45n2w5slcnrak1y93ksi4lcjl7p")))

