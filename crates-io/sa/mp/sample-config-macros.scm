(define-module (crates-io sa mp sample-config-macros) #:use-module (crates-io))

(define-public crate-sample-config-macros-0.1.0 (c (n "sample-config-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "09r868l6gqd4x4hayfxjkdy4g4axbgk92cqlla6nsnj6q4mi6cnq") (f (quote (("yaml") ("default" "yaml"))))))

(define-public crate-sample-config-macros-0.2.0 (c (n "sample-config-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0mqfh43x5bikafvy0sv4xd1bg61w948zns4mmqlxih3dnrn6lx74") (f (quote (("yaml") ("json") ("default"))))))

