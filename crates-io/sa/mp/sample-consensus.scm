(define-module (crates-io sa mp sample-consensus) #:use-module (crates-io))

(define-public crate-sample-consensus-0.1.0 (c (n "sample-consensus") (v "0.1.0") (h "00nxjv9ldp8wbbhpmflyxgsj70y0s9xmbq6ma1pf6q8389nnpp9v")))

(define-public crate-sample-consensus-0.2.0 (c (n "sample-consensus") (v "0.2.0") (h "1qjmkqaz1mymls8pr93mcyizxirfq0ij8bqjsh5s00shnxbgrmwp")))

(define-public crate-sample-consensus-1.0.0 (c (n "sample-consensus") (v "1.0.0") (h "072sd0dvscyfrhb4g8j3dwcfkv00gzx23apwfmhnw00i9hnjs93f")))

(define-public crate-sample-consensus-1.0.1 (c (n "sample-consensus") (v "1.0.1") (h "19fxffr8r6da6f6b27sbhcy1yz0i48aazdh7bls06v0bw1q961ki")))

(define-public crate-sample-consensus-1.0.2 (c (n "sample-consensus") (v "1.0.2") (h "0667wfzgh12jmqchzs318vy3ar51szjrfh7w2kzvsdd02jdzs11l")))

