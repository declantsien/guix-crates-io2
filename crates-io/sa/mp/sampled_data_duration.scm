(define-module (crates-io sa mp sampled_data_duration) #:use-module (crates-io))

(define-public crate-sampled_data_duration-0.1.0 (c (n "sampled_data_duration") (v "0.1.0") (h "06ky1kf0f261w8vpnbsvq4740300fi6vqbnkg17akimjdah4n9k8")))

(define-public crate-sampled_data_duration-0.2.0 (c (n "sampled_data_duration") (v "0.2.0") (h "00zz1b31k5pd1i4mfmswakqw03p66n0z7pnajjvsrh9a8fajmg56")))

(define-public crate-sampled_data_duration-0.3.0 (c (n "sampled_data_duration") (v "0.3.0") (h "17sxax022ryj9mqjzvnd78jj4a9dgrbza0cvm5gbcax6cfyly621")))

(define-public crate-sampled_data_duration-0.3.1 (c (n "sampled_data_duration") (v "0.3.1") (h "1kj1g3cvaqdzfr159say6yg7r41r46gyw7aaas6knsd8cvljmid1")))

