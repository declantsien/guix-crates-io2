(define-module (crates-io sa mp sampsyn) #:use-module (crates-io))

(define-public crate-sampsyn-0.1.0 (c (n "sampsyn") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sgfml1q1sshl55sd3zwfsgfzqcn2chg57fa93gq6bqqf68y18sc") (y #t)))

(define-public crate-sampsyn-0.1.1 (c (n "sampsyn") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.1.2") (d #t) (k 0)))) (h "1bxfv26dnzyh4d35gkz2m66kdd6gdz128grry8mm7x6acm25d9z1") (y #t)))

(define-public crate-sampsyn-0.1.2 (c (n "sampsyn") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.1.2") (d #t) (k 0)))) (h "0v7dznbxcxb5hpd38gqrl3qbrz96n3nl6h38z6c44vh3xacb8bjr") (y #t)))

(define-public crate-sampsyn-0.1.3 (c (n "sampsyn") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.1.2") (d #t) (k 0)))) (h "1w10xv6fyrjwib3961cm0m9hyhdyd5mw033b8jn4ddvr2w4yflli")))

(define-public crate-sampsyn-0.1.4 (c (n "sampsyn") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hound") (r "^3.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simpleio") (r "^0.2.2") (d #t) (k 0)))) (h "0gbfrjv1mp6wkkwarl4qc2jlj2lfdi3sfpplpr2547vcm2gjk0cd")))

