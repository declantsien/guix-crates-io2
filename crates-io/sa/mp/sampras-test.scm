(define-module (crates-io sa mp sampras-test) #:use-module (crates-io))

(define-public crate-sampras-test-0.1.0 (c (n "sampras-test") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocket") (r "=0.5.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0cfskiw9qs73wlnfz1nanmsfpxzkv5m4lxwrxwiqqah28vj8q9iv") (y #t)))

