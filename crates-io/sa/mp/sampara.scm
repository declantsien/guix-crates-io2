(define-module (crates-io sa mp sampara) #:use-module (crates-io))

(define-public crate-sampara-0.1.0 (c (n "sampara") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (d #t) (k 0)))) (h "1bmqrky25haj33dnsa1s6f833kcpk7xgsakqbn1k0yg56yig5nwi") (f (quote (("rms" "buffer") ("interpolate") ("buffer") ("biquad"))))))

(define-public crate-sampara-0.1.1 (c (n "sampara") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (d #t) (k 0)))) (h "1zk2g9ai318lwwwa1hbldxcgnrriqrd262cjp87y1pd25fw47gwm") (f (quote (("rms" "buffer") ("interpolate") ("buffer") ("biquad"))))))

(define-public crate-sampara-0.1.2 (c (n "sampara") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (d #t) (k 0)))) (h "0sdizdvpwimwyvdz3yx4347x0rqhr90ixhiccyz01dgqxh353ng6") (f (quote (("rms" "buffer") ("interpolate") ("generator") ("buffer") ("biquad"))))))

