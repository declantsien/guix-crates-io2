(define-module (crates-io sa mp samplerate) #:use-module (crates-io))

(define-public crate-samplerate-0.1.0 (c (n "samplerate") (v "0.1.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "1hpf7z654hc1p5v7x4qriw5qwcx6d30vhgby9623028yax2g4zf7")))

(define-public crate-samplerate-0.1.1 (c (n "samplerate") (v "0.1.1") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "0ys3v4wl35sa0h3kk29byy2lsv976kl88lpn9dcq7fvafm7nncqb")))

(define-public crate-samplerate-0.1.2 (c (n "samplerate") (v "0.1.2") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "16rj3sj4k6jgnhp6zzi5s7dikr5r05arhdwh3izx28mhcc5sidh4")))

(define-public crate-samplerate-0.1.3 (c (n "samplerate") (v "0.1.3") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "0kyhdmnw4049rqxirxxxhwvmvh12h46164d7l9lra6hvs0cn12ld")))

(define-public crate-samplerate-0.1.4 (c (n "samplerate") (v "0.1.4") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "1c5asc5ymzsv8dgnjqxzk4hqcsiiz3xdblijis75dv0013knvhjx")))

(define-public crate-samplerate-0.1.5 (c (n "samplerate") (v "0.1.5") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1") (d #t) (k 0)))) (h "06p07qz207jj2mspki0kkip61rcmnn1zmxm1phg7d599yn29f97h")))

(define-public crate-samplerate-0.2.0 (c (n "samplerate") (v "0.2.0") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1.5") (d #t) (k 0)))) (h "09vszi75y6qyigz8a8fqjgz9cmlr13bg3bv75hg1gilrlazai6ri")))

(define-public crate-samplerate-0.2.1 (c (n "samplerate") (v "0.2.1") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0g5a7ykzqf60444fl83ykl4mln7jigqnzzjkk4zwx26nl78p8pcd")))

(define-public crate-samplerate-0.2.2 (c (n "samplerate") (v "0.2.2") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "1s5f0pkip7smsy5vnhvdqs8dcq71hm1pwgsj52asml6p5agkx4d6")))

(define-public crate-samplerate-0.2.3 (c (n "samplerate") (v "0.2.3") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1.9") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "1gd34qi52656v7pvpq00ijjm9lq5wjm65pk3x5fli6j2b8mvwwgf")))

(define-public crate-samplerate-0.2.4 (c (n "samplerate") (v "0.2.4") (d (list (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libsamplerate-sys") (r "^0.1.10") (d #t) (k 0)) (d (n "rstest") (r "^0.6") (d #t) (k 2)))) (h "1616pbsriyfn8cl7ps33kx6l9fn9nfykmsl3yj1gki0m8yrb4cp0")))

