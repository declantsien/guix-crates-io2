(define-module (crates-io sa lt saltpig) #:use-module (crates-io))

(define-public crate-saltpig-0.0.0 (c (n "saltpig") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "half") (r "^1.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0wl8h5r4clhmj2q1nxhxvak9llw2w836zan5p7xwjf6v2gdwjy65") (f (quote (("ext-uuid" "uuid") ("7049-ordvalue"))))))

