(define-module (crates-io sa lt salt-compressor) #:use-module (crates-io))

(define-public crate-salt-compressor-0.4.0 (c (n "salt-compressor") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "loggerv") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1k2h2gwqwq54syzxvdnsrnrvvymgrnm56anlngws8glb10j1psf6")))

