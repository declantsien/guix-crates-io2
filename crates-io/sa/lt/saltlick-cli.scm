(define-module (crates-io sa lt saltlick-cli) #:use-module (crates-io))

(define-public crate-saltlick-cli-0.1.0 (c (n "saltlick-cli") (v "0.1.0") (d (list (d (n "assert_fs") (r "^0.13") (d #t) (k 2)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "saltlick") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zsqcimz0bkwfa8z3wk9zvlg2znc0h35agswvfpkv0i1jdw89ayq")))

