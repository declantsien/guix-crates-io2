(define-module (crates-io sa lt salty) #:use-module (crates-io))

(define-public crate-salty-0.0.0 (c (n "salty") (v "0.0.0") (h "0p4y70lkmnd6p5vcp749wg2r3v4b10yq4lj6mfvgjdqfmfs6dmyv")))

(define-public crate-salty-0.0.1 (c (n "salty") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "subtle") (r "^2.2.1") (k 0)))) (h "14s4zwnsvdvsjsck1n9szvmczg1dh7xqsd51jfcy49ab0q015c2y") (f (quote (("tweetnacl") ("default" "tweetnacl"))))))

(define-public crate-salty-0.1.0-alpha.0 (c (n "salty") (v "0.1.0-alpha.0") (d (list (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.2") (k 0)))) (h "1krd6ccxn38hjdvp7dn0caad965fwp3k6nfb0qdri9m05mgxn20z") (f (quote (("tweetnacl" "field-implementation") ("haase" "field-implementation") ("field-implementation"))))))

(define-public crate-salty-0.1.0-alpha.1 (c (n "salty") (v "0.1.0-alpha.1") (d (list (d (n "cosey") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.2") (k 0)))) (h "0palr62bpsh76dyffca9zbn39nr2dq67d2s4v636p1cg7m36wqrk") (f (quote (("slow-motion") ("cose" "cosey"))))))

(define-public crate-salty-0.1.0-alpha.2 (c (n "salty") (v "0.1.0-alpha.2") (d (list (d (n "cosey") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.4.0") (k 0)) (d (n "zeroize") (r "^1.2.0") (k 0)))) (h "0f9sbfcrls3bc8h3mbzzwqv5wscrfcn1v3xld4p7vdzqpy4jxrvn") (f (quote (("slow-motion") ("cose" "cosey"))))))

(define-public crate-salty-0.2.0-alpha.1 (c (n "salty") (v "0.2.0-alpha.1") (d (list (d (n "cosey") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.4.0") (k 0)) (d (n "zeroize") (r "^1.2.0") (k 0)))) (h "0b7rmk3n9m1z2k3bd4mp81izlgz5fgidzxv4pby0gjlciljc2s9y") (f (quote (("slow-motion") ("cose" "cosey"))))))

(define-public crate-salty-0.2.0-alpha.2 (c (n "salty") (v "0.2.0-alpha.2") (d (list (d (n "cosey") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.4.0") (k 0)) (d (n "zeroize") (r "^1.2.0") (k 0)))) (h "0qq0fgb719ci2l2hl2jdm79fapd44l1p0b3x4kc0n1g9g8vzc9zn") (f (quote (("slow-motion") ("cose" "cosey"))))))

(define-public crate-salty-0.2.0 (c (n "salty") (v "0.2.0") (d (list (d (n "cosey") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "ed25519") (r "^1.3.0") (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "subtle") (r "^2.4.0") (k 0)) (d (n "zeroize") (r "^1.2.0") (k 0)))) (h "0dscbnb8lnl3hg9frzawl384jhxrj43sw6lrxr9ixrdzv27d7kbp") (f (quote (("slow-motion") ("cose" "cosey"))))))

(define-public crate-salty-0.3.0 (c (n "salty") (v "0.3.0") (d (list (d (n "cosey") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "ed25519") (r "^2.2") (o #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1.6") (f (quote ("zeroize_derive"))) (k 0)))) (h "1vyg1xyjkinmhj7xd6vg6k63gdj0iwi9gh4y1qz7742yb1d34ixr") (f (quote (("slow-motion") ("rustcrypto" "ed25519") ("default" "rustcrypto") ("cose" "cosey"))))))

