(define-module (crates-io sa lt saltbabe) #:use-module (crates-io))

(define-public crate-saltbabe-0.1.0 (c (n "saltbabe") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1n2fb5lkajn6sbriaf6qv5jjmpgx9dfir9wczh671ixa944axdvi")))

(define-public crate-saltbabe-0.1.1 (c (n "saltbabe") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "0j2qrmjkg79pbx341b42rydrdv5wzwn4sahg99q202gp0wfh24w9")))

(define-public crate-saltbabe-0.1.2 (c (n "saltbabe") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "01axm9xszj6hzyf3cwhvk10qwfb81p4m6gyn44z2hmwbcmdvrd07")))

(define-public crate-saltbabe-0.1.3 (c (n "saltbabe") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1javmdx1vxl30v89nvhq12lqzpzbakql8qkgd37m66565ma4nj5c")))

(define-public crate-saltbabe-0.1.4 (c (n "saltbabe") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "0lv5bkm3ba051psylbd8lsc7dkvgw5msprbxis8fpycn98x0a03v")))

