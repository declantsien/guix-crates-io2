(define-module (crates-io sa lt saltdb) #:use-module (crates-io))

(define-public crate-saltdb-0.1.0 (c (n "saltdb") (v "0.1.0") (h "13mpg1mfwwz3wgh2vfrd9zimn20wq0v9bq98ib7hb0s2j07dvkfr") (y #t)))

(define-public crate-saltdb-0.1.1 (c (n "saltdb") (v "0.1.1") (h "0xc3sx8xwynkghfkp499j86ic8bm9l53aymmkpjixghhf2h0m74g") (y #t)))

