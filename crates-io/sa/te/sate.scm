(define-module (crates-io sa te sate) #:use-module (crates-io))

(define-public crate-sate-0.1.0 (c (n "sate") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.12") (d #t) (k 0)))) (h "11ydfp4yay8dakwv5gx4nrsnqm0nlvj1jh27jgifbhb4l9lgdj9y")))

(define-public crate-sate-0.1.1 (c (n "sate") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.12") (d #t) (k 0)))) (h "0ina8xcd5f2xn05bay13pjjbkvy5h2la3v7mhyjwma3dj88yy9nd")))

(define-public crate-sate-0.1.2 (c (n "sate") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.12") (d #t) (k 0)))) (h "0xb5ja1pfh1mpcr9q3vq927nq0sk83ixfqky1wy3xca7l6wj326g")))

(define-public crate-sate-0.1.4 (c (n "sate") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "subprocess") (r "^0.1.12") (d #t) (k 0)))) (h "05ggzisdiskssng1wm6bym2l98bjpyl5kz0g4h1jym6q30w5fkni")))

