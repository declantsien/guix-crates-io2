(define-module (crates-io sa te satex) #:use-module (crates-io))

(define-public crate-satex-0.1.3 (c (n "satex") (v "0.1.3") (d (list (d (n "satex-core") (r "^0.1.3") (d #t) (k 0)) (d (n "satex-serve") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "165kvhs394ypalx74y920kqp11ibavrbsdmv9j5bbgpnwq6k7i0b") (y #t)))

(define-public crate-satex-0.1.5 (c (n "satex") (v "0.1.5") (d (list (d (n "satex-core") (r "^0.1.5") (d #t) (k 0)) (d (n "satex-serve") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "173bab0wxk98inxifqyk7sf94swgcfhx5xcir8b8ynjdyvmkqkn2") (y #t)))

(define-public crate-satex-0.1.6 (c (n "satex") (v "0.1.6") (d (list (d (n "satex-core") (r "^0.1.6") (d #t) (k 0)) (d (n "satex-serve") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0z1nii3pff3cc8bv4wzndn43s8622251qk481icd6xxz4n7d69zz") (y #t)))

(define-public crate-satex-0.2.0 (c (n "satex") (v "0.2.0") (d (list (d (n "satex-core") (r "^0.2.0") (d #t) (k 0)) (d (n "satex-serve") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "059pmz4snk0x650w6nlnrnqknpj1da3a4pi6bnrb1kcvggga6c4i")))

(define-public crate-satex-0.2.1 (c (n "satex") (v "0.2.1") (d (list (d (n "satex-core") (r "^0.2.1") (d #t) (k 0)) (d (n "satex-serve") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "16268fsfl52vf6pq71p986jaqqyfy1lx7flm86cbflpn5xr8bkj6")))

(define-public crate-satex-0.2.2 (c (n "satex") (v "0.2.2") (d (list (d (n "satex-core") (r "^0.2.2") (d #t) (k 0)) (d (n "satex-serve") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "10w94p1dll32ax13w7s26vrf9vjixzqlyqvj8gqs31mnclfhqzs6")))

