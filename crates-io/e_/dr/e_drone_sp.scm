(define-module (crates-io e_ dr e_drone_sp) #:use-module (crates-io))

(define-public crate-e_drone_sp-21.3.1 (c (n "e_drone_sp") (v "21.3.1") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "13zhp5janmxrljm22nyi9zw3cd385821664ldi2h5isgjhf0vsn6") (y #t)))

(define-public crate-e_drone_sp-21.3.2 (c (n "e_drone_sp") (v "21.3.2") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "13a7j7vk3k4ml8hmgnvyc82jh20k5r97v87l3p0ilw1ajf0qrd3l") (y #t)))

(define-public crate-e_drone_sp-21.3.3 (c (n "e_drone_sp") (v "21.3.3") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "1wf9md56k0jc9vp4wi20k483pww5a5211qpsq430238kqarp4phf") (y #t)))

(define-public crate-e_drone_sp-21.3.4 (c (n "e_drone_sp") (v "21.3.4") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "1cak7g99r9lya7npqv6f17nlv5dxvibba969shcdm4jqls1zjkp4") (y #t)))

(define-public crate-e_drone_sp-21.3.5 (c (n "e_drone_sp") (v "21.3.5") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "1j9yh05gd4gydfs0vkwman6g4nr94i40rgp6pvg96h9ncq2fgcni") (y #t)))

(define-public crate-e_drone_sp-21.3.6 (c (n "e_drone_sp") (v "21.3.6") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "1yxnpgjpdc8nwi2avqqbdgga2mr59pn019idralvqjikixg7xvag") (y #t)))

(define-public crate-e_drone_sp-21.3.7 (c (n "e_drone_sp") (v "21.3.7") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "1fn1azhgln4z41768sj5nagqd5w9jfy31j40jmwn3d1ziljf14wx") (y #t)))

(define-public crate-e_drone_sp-21.3.8 (c (n "e_drone_sp") (v "21.3.8") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "0gkikg6akzkyqj3hjjiqgx17ipr6bnf1lslnyn5p93hfj2szf7r7") (y #t)))

(define-public crate-e_drone_sp-21.3.9 (c (n "e_drone_sp") (v "21.3.9") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "0yln1nk7p2b0vg3mvfaymfb20a10k13v7i28c20vavg3lkmk7qm9") (y #t)))

(define-public crate-e_drone_sp-21.3.10 (c (n "e_drone_sp") (v "21.3.10") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "0k24px57xn6g1j9v5il00f05wqgappfdxnjvvdph4zkcj3kdk5bj") (y #t)))

(define-public crate-e_drone_sp-21.3.11 (c (n "e_drone_sp") (v "21.3.11") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "12wsq4aqi5vc4gc5h22zlasfi33g1wwnnbhzn61s5iklcf0aa3sz") (y #t)))

(define-public crate-e_drone_sp-22.1.1 (c (n "e_drone_sp") (v "22.1.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "22.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "11mmdiryi0w882j6z54za2sv17hnxx31qmnia3cknh39p5kpbici")))

(define-public crate-e_drone_sp-22.4.1 (c (n "e_drone_sp") (v "22.4.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "22.*") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "03wqh02fnnf62j9a59p4lby3vbc86if4i6rrnwjz6qr1nam0g4vi")))

