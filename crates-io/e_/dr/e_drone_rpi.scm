(define-module (crates-io e_ dr e_drone_rpi) #:use-module (crates-io))

(define-public crate-e_drone_rpi-0.1.0 (c (n "e_drone_rpi") (v "0.1.0") (h "1nfhqh3p75q8v2bacc11z3gclv4bq9dm9mhqr9y2c9rizw3b98ms") (y #t)))

(define-public crate-e_drone_rpi-21.3.2 (c (n "e_drone_rpi") (v "21.3.2") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1r4mp7ci54jqrpa4r3bw12qivaq4didgnfb94a05w4bsfjbb0xw4") (y #t)))

(define-public crate-e_drone_rpi-21.3.3 (c (n "e_drone_rpi") (v "21.3.3") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1wlpm0lf8rwdw9r2yzsfpm2if08hnlkdrzx6d48w9xmx0i72zgzk") (y #t)))

(define-public crate-e_drone_rpi-21.3.4 (c (n "e_drone_rpi") (v "21.3.4") (d (list (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1c5qxsrnsxkg6f5nn7dbvfll7gbcmrwmprbmf6ybfm2pchdcpwq3") (y #t)))

(define-public crate-e_drone_rpi-21.3.5 (c (n "e_drone_rpi") (v "21.3.5") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0sb8pmrls95b3fzb6nvyb7ipm23j89fqmfaqi6i54iqm50vvdd8f") (y #t)))

(define-public crate-e_drone_rpi-21.3.6 (c (n "e_drone_rpi") (v "21.3.6") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "06hfdqzyiq7s1jwj2gjc2immspvnn2layhfqsn64pypcaxiixa3a") (y #t)))

(define-public crate-e_drone_rpi-21.3.7 (c (n "e_drone_rpi") (v "21.3.7") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1issn095v9fp2rqzxa12mpzvxbbycqb5s9zy71wbpqz6dgl7hhxs") (y #t)))

(define-public crate-e_drone_rpi-21.3.8 (c (n "e_drone_rpi") (v "21.3.8") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "113kyx7iwyx29m69ydcpca9wklpkhwxn084c48z46hb009ir6bqh") (y #t)))

(define-public crate-e_drone_rpi-21.7.1 (c (n "e_drone_rpi") (v "21.7.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "21.*") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "138986gw2jgmsnphc4mfb5pnz44lcpa3srlsnfzr8054wzf7cz50") (y #t)))

(define-public crate-e_drone_rpi-22.1.1 (c (n "e_drone_rpi") (v "22.1.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "e_drone") (r "22.*") (d #t) (k 0)) (d (n "rppal") (r "0.13.*") (d #t) (k 0)))) (h "1km1icg5kd3wq9da5fzqx0mlyi7p2al8pkzanf61w9wd9j6agpi8")))

