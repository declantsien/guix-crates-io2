(define-module (crates-io e_ dr e_drone) #:use-module (crates-io))

(define-public crate-e_drone-21.3.1 (c (n "e_drone") (v "21.3.1") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1aqj7x8gbsmsqwyqrx5ngbv121m4wxkxbn66sxdhlfdv3qp8lpi5") (y #t)))

(define-public crate-e_drone-21.3.2 (c (n "e_drone") (v "21.3.2") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02xj71s2jbp3np6h3jjzgv3qnfcmxhbnqh0aa3s2061fbbzc4dq0") (y #t)))

(define-public crate-e_drone-21.3.3 (c (n "e_drone") (v "21.3.3") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pi73cps7k56g7jhdifqi3f1s185z3lgrdc6vbh1amzsav8sli9p") (y #t)))

(define-public crate-e_drone-21.3.4 (c (n "e_drone") (v "21.3.4") (d (list (d (n "bincode") (r "^1.3.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04n46mygvblvr8cz4wi3pb0wsaj5hncz7gw4r7pdqnqwll96q12y") (y #t)))

(define-public crate-e_drone-21.3.5 (c (n "e_drone") (v "21.3.5") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "166jv8s1pwnqa9mc83a30xrc6i6qhd56hi10sjf1r3a2vd9asy16") (y #t)))

(define-public crate-e_drone-21.3.6 (c (n "e_drone") (v "21.3.6") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r4q626pfrkzwbngc12w6n0k8js3l5pw3vxp2p7ksdb12j8zg1hj") (y #t)))

(define-public crate-e_drone-21.3.7 (c (n "e_drone") (v "21.3.7") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1564nc4k6ryd76brgd4ac4cnxgr3hfzvp28m4d2v8s751ph9719m") (y #t)))

(define-public crate-e_drone-21.3.8 (c (n "e_drone") (v "21.3.8") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12pjmnqnah0azrdvrmbfbc96wi5nrbknzapsni11xbknn07l5brm") (y #t)))

(define-public crate-e_drone-21.3.9 (c (n "e_drone") (v "21.3.9") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16ggjqxh2i01950sh0arf167g0wnm4xnljdcscf0dkj7ny2f71mc") (y #t)))

(define-public crate-e_drone-21.3.10 (c (n "e_drone") (v "21.3.10") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16444xvz8i3fdpklqycax5261b9460ldrdn1abiimsya3b0nf52p") (y #t)))

(define-public crate-e_drone-21.3.11 (c (n "e_drone") (v "21.3.11") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1031s830aikki8dsylqrc20j06a5a3d6fc365is719xclff11fin") (y #t)))

(define-public crate-e_drone-21.3.12 (c (n "e_drone") (v "21.3.12") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n5h3764cmxd6fvblffn78jl57ggv53zqm2fyvn9i8zic2ycb7j9") (y #t)))

(define-public crate-e_drone-21.3.13 (c (n "e_drone") (v "21.3.13") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03kyyh9vdwb3l9q8flfgmd9z31hf4jh044vl0779a6mid4i0fnij") (y #t)))

(define-public crate-e_drone-21.3.14 (c (n "e_drone") (v "21.3.14") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hlphx8cvxmh2nwydd07a0kjnh18mibzgw1s2v3al9mimbq0y1i3") (y #t)))

(define-public crate-e_drone-21.3.15 (c (n "e_drone") (v "21.3.15") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1biyp03blzqnwb0jyf0w0kl4df5w0kh5i066wlm600lr4nmwpkff") (y #t)))

(define-public crate-e_drone-21.3.16 (c (n "e_drone") (v "21.3.16") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vqmpv43gakaj5pkynfxpqxy7fhriy7dvfg2ja5kgdxbqb7yfcf6") (y #t)))

(define-public crate-e_drone-21.3.17 (c (n "e_drone") (v "21.3.17") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "02nnc985bidviia95yyqi5rinbqwhq9sminpz2bpcva6a2w19k70") (y #t)))

(define-public crate-e_drone-21.3.18 (c (n "e_drone") (v "21.3.18") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (d #t) (k 0)))) (h "0f3hgsawb2ampw7rrmqdg240sai5c3nmna4wdyjwyhnx8p2gsv87") (y #t)))

(define-public crate-e_drone-21.3.19 (c (n "e_drone") (v "21.3.19") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wzz5v0pn57p3gy66kb9wigcjl21dhrzhpj6s37cimlz90bfgrlp") (y #t)))

(define-public crate-e_drone-21.3.20 (c (n "e_drone") (v "21.3.20") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cxgcn8gk3m0ahbfai2l6bf80cybxgzg9d2ljmcan91cmxknm7zh") (y #t)))

(define-public crate-e_drone-21.3.21 (c (n "e_drone") (v "21.3.21") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w397l1all6n1dnj2hyr3a6xsfrawla71x4n39p73s7d89abl9nj") (y #t)))

(define-public crate-e_drone-21.3.22 (c (n "e_drone") (v "21.3.22") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wxx4c2kj9i0csymhlxgm4hqcajmm0f9sf5gm908qd2azz660fhg") (y #t)))

(define-public crate-e_drone-21.3.23 (c (n "e_drone") (v "21.3.23") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13kxnd0pl11dbi7xy5j4ald0ybvba91ckhq11hvabi42x75a2xld") (y #t)))

(define-public crate-e_drone-21.3.24 (c (n "e_drone") (v "21.3.24") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w9nz38yjqrxrimdb9vd8m4sb5kc9rsppa970hgqcarzz81rwz7x") (y #t)))

(define-public crate-e_drone-21.3.25 (c (n "e_drone") (v "21.3.25") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0phwsnzfnn2k63is39ssvp069h8malc9sg8nikfq0acfnzn948fk") (y #t)))

(define-public crate-e_drone-21.3.26 (c (n "e_drone") (v "21.3.26") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16w7xw119v9n50dc4dmd3kr1f13kydp3azr3aij37s9lhw548b7b") (y #t)))

(define-public crate-e_drone-21.3.27 (c (n "e_drone") (v "21.3.27") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13gazpif2ra12m6hypdpmsi11a8qmm84c9si9lxyc3r94kmm5ahm") (y #t)))

(define-public crate-e_drone-21.3.28 (c (n "e_drone") (v "21.3.28") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19x5ini3n31g6xxv7fpn0imh7kg6x16grmzybsh6l639lxbbc6gw") (y #t)))

(define-public crate-e_drone-21.5.1 (c (n "e_drone") (v "21.5.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09v3q04jg12dwv6jbp402dgbnx3f5aq6yk9b900yf0c8wf5rz5vq") (y #t)))

(define-public crate-e_drone-21.6.1 (c (n "e_drone") (v "21.6.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08lfqfqmg96nynpdj9dyv4iqnv3rhf84lsnapylhrrzbjw9vsyjy") (y #t)))

(define-public crate-e_drone-21.7.1 (c (n "e_drone") (v "21.7.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ya6d0jan1dccl5k7kqznayj478h85a6di3r7xvxrbkxqn30d7zz") (y #t)))

(define-public crate-e_drone-21.7.2 (c (n "e_drone") (v "21.7.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y63xk1r4akr4yzbn9093nb8b3bqv0ps93amymgflsb18ms0l38z") (y #t)))

(define-public crate-e_drone-21.8.1 (c (n "e_drone") (v "21.8.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g9kkwg0bcwgrpqwwvfk5yv206v7dn49p43c44kkf7nwrlnwllpz") (y #t)))

(define-public crate-e_drone-21.9.1 (c (n "e_drone") (v "21.9.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sj4nclyrrb6w7c6hpsa5wfcjn9q5ajgxkvw487298arncgifxi3") (y #t)))

(define-public crate-e_drone-21.9.2 (c (n "e_drone") (v "21.9.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05zldgqcsl39c54ikz1pn5sal7gkxpjb5wd0b9z3sqpk5ja30c8n") (y #t)))

(define-public crate-e_drone-22.1.1 (c (n "e_drone") (v "22.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f1di8ps3qi2pnaqsngd71an060vxkm01k01hlc0bf0jlimijqdl") (y #t)))

(define-public crate-e_drone-22.4.1 (c (n "e_drone") (v "22.4.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1piiiiz2rkndwqxhflkkxvinkcfwg3lh3qp2r5hfgw0sn82bibh3") (y #t)))

(define-public crate-e_drone-22.4.2 (c (n "e_drone") (v "22.4.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kkariiqf3paymm96ihln9ky6d10zf7zny1cfp8v6190ahg37chp") (y #t)))

(define-public crate-e_drone-22.4.3 (c (n "e_drone") (v "22.4.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1585y2pd00dnnj52w4z7ghdhwqvnmwg2f15907h3x2k76l46dly6") (y #t)))

(define-public crate-e_drone-22.4.4 (c (n "e_drone") (v "22.4.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l9fs8p3ryc4i5aawgb0v5xk1gzbli99lx2ld22bhvvrdv711ziv") (y #t)))

(define-public crate-e_drone-22.5.1 (c (n "e_drone") (v "22.5.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0665r11zlpjzhbkbsqxc18skxgl9khrhdq75pqp8ylbqkgzrj90q") (y #t)))

(define-public crate-e_drone-22.5.2 (c (n "e_drone") (v "22.5.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sj9fpaas6jck8w1jmx4maa9cxjhydjvn5v970kbl1cnjx49w7ir") (y #t)))

(define-public crate-e_drone-22.5.3 (c (n "e_drone") (v "22.5.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ixgl90y1037k3m52r5s6mx5a2sr220bdli3kgk221sn9snnd69j") (y #t)))

(define-public crate-e_drone-22.6.1 (c (n "e_drone") (v "22.6.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hkbd4325xdgvcyqc45p8j97hvnfmarl9ky2jrwj00g7f0npsjjm") (y #t)))

(define-public crate-e_drone-22.6.2 (c (n "e_drone") (v "22.6.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sr531ry41gyzc40n12wc2agw75a85xwh8pfabvx4vlikm870a51")))

