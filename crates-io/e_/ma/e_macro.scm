(define-module (crates-io e_ ma e_macro) #:use-module (crates-io))

(define-public crate-e_macro-0.1.0 (c (n "e_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1n71cgh284ic74sg8xx66xyi34wc6bhxd92ic8n2pf2z7k7833vk") (y #t)))

(define-public crate-e_macro-0.1.1 (c (n "e_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1f6cxhfc84hx0dq752qiwa75a5798baq9d9ncdp2s9naypwpiixw") (y #t)))

(define-public crate-e_macro-0.2.0 (c (n "e_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0rd269ivjxj38qjgif36kr3rld1mwjda1njd2w4gph38q2h59n90") (y #t)))

(define-public crate-e_macro-0.2.1 (c (n "e_macro") (v "0.2.1") (h "0cwzy93kn6cw21shgc4fc2nlwrmyjdxyqcl1zyzqra6i11r38jh7") (y #t)))

(define-public crate-e_macro-0.0.0 (c (n "e_macro") (v "0.0.0") (h "0jpq8da7ny76hf1dpxdm13nq2mr6rnqv696a8qz13vgd6wniw0ms")))

