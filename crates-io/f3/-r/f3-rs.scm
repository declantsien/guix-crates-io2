(define-module (crates-io f3 -r f3-rs) #:use-module (crates-io))

(define-public crate-f3-rs-0.1.0 (c (n "f3-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "12j9nwvknmpfmjzrcndfrhlm3iy47ch9jn016fxllqmwjfv7d73d")))

