(define-module (crates-io f3 l_ f3l_core) #:use-module (crates-io))

(define-public crate-f3l_core-0.3.0 (c (n "f3l_core") (v "0.3.0") (d (list (d (n "f3l_glam") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (d #t) (k 0)))) (h "1v6cigpnb6ym3k5s6iz1p1lg435xpy9533p5r4a5021nq19fi8sf")))

