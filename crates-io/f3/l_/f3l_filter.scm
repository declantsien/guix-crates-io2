(define-module (crates-io f3 l_ f3l_filter) #:use-module (crates-io))

(define-public crate-f3l_filter-0.2.0 (c (n "f3l_filter") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "f3l_core") (r "^0.3") (d #t) (k 0)) (d (n "f3l_search_tree") (r "^0.3") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand" "glam025"))) (o #t) (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d5cv6mkivsrb418vs87l270rhkg4g0b5hr54zvvrmjpx7k0wsal") (f (quote (("default") ("app_kiss3d" "kiss3d" "nalgebra"))))))

