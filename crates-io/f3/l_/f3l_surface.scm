(define-module (crates-io f3 l_ f3l_surface) #:use-module (crates-io))

(define-public crate-f3l_surface-0.2.0 (c (n "f3l_surface") (v "0.2.0") (d (list (d (n "f3l_core") (r "^0.3") (d #t) (k 0)) (d (n "f3l_segmentation") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)) (d (n "kiss3d") (r "^0.35.0") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("rand" "glam025"))) (o #t) (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0pwihipcnyamvshwllskvnnpr6vh23gc1ar3gzhzm8js7078mrzi") (f (quote (("default") ("app_kiss3d" "kiss3d" "nalgebra" "image"))))))

