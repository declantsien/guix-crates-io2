(define-module (crates-io f3 l_ f3l_glam) #:use-module (crates-io))

(define-public crate-f3l_glam-0.1.0 (c (n "f3l_glam") (v "0.1.0") (d (list (d (n "glam") (r "^0.27.0") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "047ymqxx9bzlvmdsxhws14xchkvp20rl6nfwg3lkxsl24676dhqq")))

