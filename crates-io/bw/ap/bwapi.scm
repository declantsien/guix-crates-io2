(define-module (crates-io bw ap bwapi) #:use-module (crates-io))

(define-public crate-bwapi-0.1.0 (c (n "bwapi") (v "0.1.0") (h "06zm1c9iiamg1wrjq4cd6wqg8mwpc7z47yg6p8p11381cicmahr8")))

(define-public crate-bwapi-0.2.0 (c (n "bwapi") (v "0.2.0") (d (list (d (n "bwapi-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1v3b6n01i7pv29fkdrg51yj72akp8dpsa7s1zkb4n1cn0864mnkw")))

