(define-module (crates-io bw ap bwapi_wrapper) #:use-module (crates-io))

(define-public crate-bwapi_wrapper-0.1.0 (c (n "bwapi_wrapper") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0vqrjgfhisakj93pqdvgksdhw1z3y4knvp9b1nlbwpzfv9kh47ia")))

(define-public crate-bwapi_wrapper-0.2.0 (c (n "bwapi_wrapper") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1w4lhgznwh60zabznxn360z7czbzwczpxw29gklka8jah9y0bmlx")))

(define-public crate-bwapi_wrapper-0.2.1 (c (n "bwapi_wrapper") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1vj2ar3mdhyhqil34w8vp9gxw13xcxk6bn9nx5ngp6shx8fhfpfb")))

(define-public crate-bwapi_wrapper-0.2.2 (c (n "bwapi_wrapper") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0kbcb0mxpbfwxc959wwsji907cn02lfmn8p0aal7vc2r3shxmcpk")))

(define-public crate-bwapi_wrapper-0.3.0 (c (n "bwapi_wrapper") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0psrkm1nzwflcjv04krl487dqs2l4aqb6jzdzqj425aspic3ciqq")))

(define-public crate-bwapi_wrapper-0.3.2 (c (n "bwapi_wrapper") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1l10nbcix3ad6493ms32zmffvbkijldhxzbyggwrc1g45bpiq38y")))

(define-public crate-bwapi_wrapper-0.3.3 (c (n "bwapi_wrapper") (v "0.3.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mmd1f6v0xjbyrdmrsrkk4sc2k6ldwwn25r8yfmpz2pccwhdwk3m")))

