(define-module (crates-io bw ap bwapi-sys) #:use-module (crates-io))

(define-public crate-bwapi-sys-0.1.0 (c (n "bwapi-sys") (v "0.1.0") (h "0iy3hw8kdixrs6makdz7hmb3r2yvax86z58a6fniim5ambj615sy")))

(define-public crate-bwapi-sys-0.1.1 (c (n "bwapi-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.8") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1w57naxhy4kx5pl2kn5q4mc4nl8csysa6ifif0zz8q3rscd8h8mg") (l "BWAPIC")))

(define-public crate-bwapi-sys-0.1.2 (c (n "bwapi-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.31") (d #t) (k 1)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "reqwest") (r "^0.8") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0wi6aalc1laada11v2jv7knd9r91xamkvian8cc72lrrl38yx8k3") (l "BWAPIC")))

