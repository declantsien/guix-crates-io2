(define-module (crates-io bw ra bwrap) #:use-module (crates-io))

(define-public crate-bwrap-0.1.0-beta1 (c (n "bwrap") (v "0.1.0-beta1") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0zpki6gnsjpg5mwv1458kkm3v1mward9in322w2f4pyi20723pzr") (f (quote (("use_std"))))))

(define-public crate-bwrap-0.1.0-beta2 (c (n "bwrap") (v "0.1.0-beta2") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "15vl9h0inzz3wl7yjsjv95nlhnyxm3p4awg4cp984dqgdgpnvy2f") (f (quote (("use_std"))))))

(define-public crate-bwrap-0.1.0-beta3 (c (n "bwrap") (v "0.1.0-beta3") (d (list (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0ihxkzys98yrzpxv1fa7wvpnl0ap4lg21g986g5lrmwx0wpifyng") (f (quote (("use_std"))))))

(define-public crate-bwrap-0.1.0-beta4 (c (n "bwrap") (v "0.1.0-beta4") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "09ccg8vwzi5i8c03rg3id19kb1bf6dmp325jckzzs5y92dwk7m5d") (f (quote (("use_std"))))))

(define-public crate-bwrap-0.1.0-beta5 (c (n "bwrap") (v "0.1.0-beta5") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0731zfgxjrimnmcm4pb999vx8bjdbhvvjcmysfcr6d6lzj4hm0iy") (f (quote (("use_std"))))))

(define-public crate-bwrap-0.1.0-beta6 (c (n "bwrap") (v "0.1.0-beta6") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0688ws54psnm48g287whi87w48klcslf3h01jan3ag5vk4dna7y5") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.0.0 (c (n "bwrap") (v "1.0.0") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "163sn2x45vx5iwsyv5wpp0f9h6znc9qxr1pzjjrzawckljm4scp5") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.1.0 (c (n "bwrap") (v "1.1.0") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "1kzdiwpbcvgh5qvl1491hgl4jbyxwijr3m4kzs7m2ssshi531qvf") (f (quote (("use_std")))) (y #t)))

(define-public crate-bwrap-1.1.1 (c (n "bwrap") (v "1.1.1") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "1g1ac7jd1r4jvm2i1bqxn6gp7pbad4lzynqj355kx42ag9rb52mx") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.2.0 (c (n "bwrap") (v "1.2.0") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0r6hvv707llvnh06v41k40ihvjjan0bgj29w9lhjmy38s83yahjg") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.2.1 (c (n "bwrap") (v "1.2.1") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0kz47ikws7a6dz9hq2awi4gw17c06y301mlmvzpcbv6b9z2jlmca") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.2.3 (c (n "bwrap") (v "1.2.3") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "0848x6gghli2fi49vwnn107raxf6ip4121jlv5z1pqd9lmq4mvmm") (f (quote (("use_std"))))))

(define-public crate-bwrap-1.3.0 (c (n "bwrap") (v "1.3.0") (d (list (d (n "unicode-width") (r "^0.1.0") (d #t) (k 0)))) (h "15dv9mawgmwgpj88k832qjdi94w70advg9zv28bpgig5k8qs6gfi") (f (quote (("use_std"))))))

