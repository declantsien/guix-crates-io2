(define-module (crates-io bw as bwasm) #:use-module (crates-io))

(define-public crate-bwasm-0.1.0 (c (n "bwasm") (v "0.1.0") (d (list (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "wasmi-validation") (r "^0.3") (d #t) (k 0)))) (h "1acpj5cix0ss8kn0kd5jpvm7d6mdffl4dc461qr6bjgl2zi002pm")))

(define-public crate-bwasm-0.1.1 (c (n "bwasm") (v "0.1.1") (d (list (d (n "parity-wasm") (r "^0.41") (d #t) (k 0)) (d (n "wasmi-validation") (r "^0.3") (d #t) (k 0)))) (h "0lxbixb5qp744dapyac1sgghsdcm093l08r11gw5890grc0rd3vd")))

