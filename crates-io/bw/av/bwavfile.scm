(define-module (crates-io bw av bwavfile) #:use-module (crates-io))

(define-public crate-bwavfile-0.1.0 (c (n "bwavfile") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "1zq7kf7x0v1mn6sx27asqlfagiyvjqfqhxxydnpwj5n17iq6q6vd") (y #t)))

(define-public crate-bwavfile-0.1.1 (c (n "bwavfile") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0zsnzzxwpr4cdcirhyjag0k59hiw2hl2inkpnnh555lpdc0z26yw") (y #t)))

(define-public crate-bwavfile-0.1.2 (c (n "bwavfile") (v "0.1.2") (d (list (d (n "byteorder") (r ">=1.3.4, <2.0.0") (d #t) (k 0)) (d (n "encoding") (r ">=0.2.33, <0.3.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)))) (h "1gh55wxn7ichl7lz9d55lincn112m3zgs7n0j6sbg6ps0qdv9w5b") (y #t)))

(define-public crate-bwavfile-0.1.3 (c (n "bwavfile") (v "0.1.3") (d (list (d (n "byteorder") (r ">=1.3.4, <2.0.0") (d #t) (k 0)) (d (n "encoding") (r ">=0.2.33, <0.3.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)))) (h "1r5zif374pbbzd8ay76lddhqmmpcnpwl6cscl5m3kgxzj58gqxzl") (y #t)))

(define-public crate-bwavfile-0.1.4 (c (n "bwavfile") (v "0.1.4") (d (list (d (n "byteorder") (r ">=1.3.4, <2.0.0") (d #t) (k 0)) (d (n "encoding") (r ">=0.2.33, <0.3.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.59, <2.0.0") (d #t) (k 0)) (d (n "uuid") (r ">=0.8.1, <0.9.0") (d #t) (k 0)))) (h "1zmqlz8f1wphhsgs3x7ikynxq001m4g0gqrssggkm0m8fszb2iwc") (y #t)))

(define-public crate-bwavfile-0.1.5 (c (n "bwavfile") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1lich4nni6750gsr2a0i3l2asggcypnryd8ldz0ds8wrljp57mqa") (y #t)))

(define-public crate-bwavfile-0.1.6 (c (n "bwavfile") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0cr08h3ifagfqkfb4s2z9fq34ggrmg3k629xi67n89754irxjsh7") (y #t)))

(define-public crate-bwavfile-0.1.7 (c (n "bwavfile") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "16qj3nqsr2q448x7vfvcj3cb12bq5m9h3isbw5y2svpsq7x898ak") (y #t)))

(define-public crate-bwavfile-0.9.0 (c (n "bwavfile") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0r0f65w1lkjvmgf80rh5w0bhgy97jdx87qyxr7nnk9wpxwl5p4bp") (y #t)))

(define-public crate-bwavfile-0.9.1 (c (n "bwavfile") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0zc8pzjifjpsp83r0yfvrx8nj5d1clbj795bn4s1a62nds3zqwwc") (y #t)))

(define-public crate-bwavfile-0.9.2 (c (n "bwavfile") (v "0.9.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "10819b5ab5wv5sccq99nrzm3vry4qzm2dk2nlcj6sp8afi490579") (y #t)))

(define-public crate-bwavfile-0.9.3 (c (n "bwavfile") (v "0.9.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "04i7whlkmcdyd8yw05y0q4j26z0ys4dc3ilvala7qfb0mby92nhl") (y #t)))

(define-public crate-bwavfile-1.0.0 (c (n "bwavfile") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1bxix3k8bn0mliz5ak5bvrdry81crjwwjac4mvc575x5navxy2b3") (y #t)))

(define-public crate-bwavfile-1.1.0 (c (n "bwavfile") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0szhawzlms341xba2m41x0kp7mx668nks1935k29sjjll8308a7c") (y #t)))

(define-public crate-bwavfile-2.0.0 (c (n "bwavfile") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0bzrc7v0kvgc28i6hjfiliv6wzfw2agkv69kswwmv8v4h73y0zzc")))

(define-public crate-bwavfile-2.0.1 (c (n "bwavfile") (v "2.0.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1y6fkzraqs7xjphvk1c55ck8zx0mchy77d2vxyxdq3ydx0g9imjk")))

