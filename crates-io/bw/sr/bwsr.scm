(define-module (crates-io bw sr bwsr) #:use-module (crates-io))

(define-public crate-bwsr-0.1.0 (c (n "bwsr") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "feed-rs") (r "^1.3.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1cgdzmj4pjkvfrixjwhm9s54ma4wij07wzcyvasbyn88mmczyj7d")))

