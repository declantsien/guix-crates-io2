(define-module (crates-io bw s- bws-std) #:use-module (crates-io))

(define-public crate-bws-std-0.1.0 (c (n "bws-std") (v "0.1.0") (h "0il2lkjafvmfix6aan15g5dvmlx8jfcnidj8030xw21ay7nr0y5y") (y #t)))

(define-public crate-bws-std-0.0.0 (c (n "bws-std") (v "0.0.0") (h "07nnpxnc7pqdhxmi4b03fjh9mnkfaprhnjc3vcjiw232v43k91p6") (y #t)))

(define-public crate-bws-std-0.0.1 (c (n "bws-std") (v "0.0.1") (h "0qd3l33kk2na2vhmbq1vmkfwrlbg9crc74i78zqi6bab2c9vc2il") (y #t)))

