(define-module (crates-io zl ib zlib_stream) #:use-module (crates-io))

(define-public crate-zlib_stream-0.0.1 (c (n "zlib_stream") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("cloudflare_zlib"))) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0lfbhzj91i64zzpczhh3rhid8z6hca98hwgi3bcka1p5z3rgv9y7") (y #t)))

(define-public crate-zlib_stream-0.0.2 (c (n "zlib_stream") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("cloudflare_zlib"))) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "14ydmgzpdlvhgbj7yana2bbw2cr8hrvqvq3yy1rz3z9r1lbnx5k7")))

(define-public crate-zlib_stream-0.1.0 (c (n "zlib_stream") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("cloudflare_zlib"))) (k 0)) (d (n "futures-util") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1pmfhiwdbz0rvrd0z1br564m97ajhcjh3yh9cipib3z31h8rgqrv") (f (quote (("tokio-runtime" "stream" "tokio") ("stream" "futures-util") ("default" "stream"))))))

