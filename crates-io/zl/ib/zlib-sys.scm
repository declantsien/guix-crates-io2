(define-module (crates-io zl ib zlib-sys) #:use-module (crates-io))

(define-public crate-zlib-sys-0.0.1 (c (n "zlib-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1gshflgmlq3kz7n4pcxlighwvfyjvy9hmz8ydsda35f1ivgjm6ql") (y #t)))

(define-public crate-zlib-sys-0.0.2 (c (n "zlib-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0lia0h0xyzq4vwc0s4f6a4nn4hkq9yvg69i2d4pdjr0xqfb91hlq") (y #t)))

