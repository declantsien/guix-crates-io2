(define-module (crates-io zl ib zlib) #:use-module (crates-io))

(define-public crate-zlib-0.0.1 (c (n "zlib") (v "0.0.1") (d (list (d (n "crc32") (r "*") (d #t) (k 0)))) (h "10zhjhx8b67w7czzaz8199racyvbrj878y37p0b37k4hyjmx1hf6") (f (quote (("unsafe_fast") ("default"))))))

