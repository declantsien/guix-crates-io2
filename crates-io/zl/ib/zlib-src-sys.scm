(define-module (crates-io zl ib zlib-src-sys) #:use-module (crates-io))

(define-public crate-zlib-src-sys-0.0.1 (c (n "zlib-src-sys") (v "0.0.1") (h "0am5nhz9labg8w18dzlxvq405dkgabs23pfrcfw43d3f1sgpawby")))

(define-public crate-zlib-src-sys-0.0.2 (c (n "zlib-src-sys") (v "0.0.2") (h "0lqypfmwj4h514wb6vl7midsjfpq43zsr11vysab9xwmgpvfx4nn")))

(define-public crate-zlib-src-sys-0.1.0 (c (n "zlib-src-sys") (v "0.1.0") (h "07yzzhrxyp20im1vg5sh6l1gaxn16ma55v9qp2ax1c98l32w6ca5") (l "zlib")))

(define-public crate-zlib-src-sys-0.1.1 (c (n "zlib-src-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f2cml7bv3lkhi9fiaypsj1s3wmxhg9y9najrkxbxf5ad8xdac35") (l "zlib")))

(define-public crate-zlib-src-sys-0.1.2 (c (n "zlib-src-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0siyrbc7khh3ivqz6ymcfhf52340fg8v9jgjyq9j9dsfiv7qdx2j") (l "zlib")))

