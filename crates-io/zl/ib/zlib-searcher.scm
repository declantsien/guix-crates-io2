(define-module (crates-io zl ib zlib-searcher) #:use-module (crates-io))

(define-public crate-zlib-searcher-0.1.0 (c (n "zlib-searcher") (v "0.1.0") (d (list (d (n "actix-web") (r "^4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cang-jie") (r "^0.14") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "jieba-rs") (r "^0.6") (f (quote ("default-dict"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.18") (d #t) (k 0)))) (h "162viqmqibfdmxppmy0caq5bs9vdl5kl5mqaaxcvbyfhmmaf4rv7") (y #t)))

