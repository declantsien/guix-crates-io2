(define-module (crates-io zl ib zlib_cli) #:use-module (crates-io))

(define-public crate-zlib_cli-0.1.0 (c (n "zlib_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)))) (h "1w2kgca7hqhym71xfn7ygadkz1ag3gda0sni4blwln43633x458m")))

