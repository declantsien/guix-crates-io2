(define-module (crates-io zl _t zl_todos) #:use-module (crates-io))

(define-public crate-zl_todos-0.1.0 (c (n "zl_todos") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "slint") (r "^1.5.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.1") (d #t) (k 1)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0ds921hfap22940xzxxp527067lbg1808vfn4zh7rffx0r6cl57d")))

(define-public crate-zl_todos-0.1.1 (c (n "zl_todos") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "slint") (r "^1.5.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.1") (d #t) (k 1)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1dazh2gs3i59yhf8s69nakyi6mvn1hivk24v66bckq1px70n8094")))

(define-public crate-zl_todos-0.1.2 (c (n "zl_todos") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "slint") (r "^1.5.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.1") (d #t) (k 1)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0i622dc40swdwgr1rl92nxx70v0q4s7mg8gr960yrz2g836gb9rv")))

(define-public crate-zl_todos-0.1.3 (c (n "zl_todos") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "slint") (r "^1.5.1") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.1") (d #t) (k 1)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1v80g5jcsi6sqyjzk5shmyy3w7kkmbrysz22gylllsgccp4nin0b")))

