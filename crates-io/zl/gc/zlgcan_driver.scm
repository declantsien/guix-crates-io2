(define-module (crates-io zl gc zlgcan_driver) #:use-module (crates-io))

(define-public crate-zlgcan_driver-0.2.0 (c (n "zlgcan_driver") (v "0.2.0") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "log4rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.0") (d #t) (k 0)))) (h "15f10sjj2218n84rkjx2zl494vqn9gngmy4sw8v4i44804vgjymr") (y #t)))

(define-public crate-zlgcan_driver-0.2.1 (c (n "zlgcan_driver") (v "0.2.1") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "log4rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.0") (d #t) (k 0)))) (h "0vkd2k3cbn8ncjn8zfsldqakxrwxsx44bwk38ghmmybqi2zxkqsa") (y #t)))

(define-public crate-zlgcan_driver-0.2.2 (c (n "zlgcan_driver") (v "0.2.2") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "log4rs") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.0") (d #t) (k 0)))) (h "0z6g08h6awqphczgd7jrd4gl9sq0vn3k9py2cdkxzamm3s1ahf0d") (y #t)))

(define-public crate-zlgcan_driver-0.2.3 (c (n "zlgcan_driver") (v "0.2.3") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.2") (d #t) (k 0)))) (h "0f62jyl1d0gm13mhyd1hj9ivhsdv24q9vcx0ggr5qsvl22vxb4ac") (y #t)))

(define-public crate-zlgcan_driver-0.2.4-Alpha (c (n "zlgcan_driver") (v "0.2.4-Alpha") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.3-Alpha") (d #t) (k 0)))) (h "0a3sbga0fkxdhall08jjr0d3yznjfwprg9p0cysyas0da75v9lqm") (y #t)))

(define-public crate-zlgcan_driver-0.2.4-Beta1 (c (n "zlgcan_driver") (v "0.2.4-Beta1") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.2.3-Beta3") (d #t) (k 0)))) (h "0x24xcnsnn5vf0jvnnqg6hbz1d59zl0p1l12wq7jmrjly272br34") (y #t)))

(define-public crate-zlgcan_driver-0.3.0 (c (n "zlgcan_driver") (v "0.3.0") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.3.0") (d #t) (k 0)))) (h "1dggqsxm6qrc0f1ryazy8isxcwd0nzwhfs4yawnwfwpnrbdanwix") (y #t)))

(define-public crate-zlgcan_driver-0.3.1 (c (n "zlgcan_driver") (v "0.3.1") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.3.1") (d #t) (k 0)))) (h "0ycrqblz01msx4yr5nky6zvsigq36n0jkygrl68vvz4hqdprn00v") (y #t)))

(define-public crate-zlgcan_driver-0.3.2 (c (n "zlgcan_driver") (v "0.3.2") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.3.2") (d #t) (k 0)))) (h "06xkyih16pk1lm97hvblpv8yvr6ssfq52jh5sihzxydj6v1yhbz3") (y #t)))

(define-public crate-zlgcan_driver-0.3.3 (c (n "zlgcan_driver") (v "0.3.3") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.3.3") (d #t) (k 0)))) (h "03hmpa2s5dl4mf9m6r576rh56fffs2ahjzfq9c1jxc4p44k83m05") (y #t)))

(define-public crate-zlgcan_driver-0.3.4 (c (n "zlgcan_driver") (v "0.3.4") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^0.3.4") (d #t) (k 0)))) (h "0ibzvvm4wb7s66q871fydx2smrp2vrvrkd75lzch03ylgyx979lr") (y #t)))

(define-public crate-zlgcan_driver-1.0.0-rc1 (c (n "zlgcan_driver") (v "1.0.0-rc1") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^1.0.0-rc1") (d #t) (k 0)))) (h "0sch903v3wbs4a588v5vpag7rqjgmcaxscmdazaiw2hhvcpm11lk")))

(define-public crate-zlgcan_driver-1.0.0-rc2 (c (n "zlgcan_driver") (v "1.0.0-rc2") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^1.0.0-rc2") (d #t) (k 0)))) (h "1zdqz2i5py902x7iy8474vlkiq0014b5kzrh5mx9yh89xcxhv6xj")))

(define-public crate-zlgcan_driver-1.0.0-rc3 (c (n "zlgcan_driver") (v "1.0.0-rc3") (d (list (d (n "dlopen2") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "zlgcan_common") (r "^1.0.0-rc3") (d #t) (k 0)))) (h "13z4rjg722d7lqb861hby9k922m91nv5872x54kj1jz5f5925awv")))

