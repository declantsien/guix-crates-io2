(define-module (crates-io zl gc zlgcan_common) #:use-module (crates-io))

(define-public crate-zlgcan_common-0.2.0 (c (n "zlgcan_common") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "12zpp2ci8zvxkdd573p53zghck3xi5bsy47zc9k2i7vd4rnp9pjh") (y #t)))

(define-public crate-zlgcan_common-0.2.1 (c (n "zlgcan_common") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "15ikd848hsp6z9i0w7nbb34fpvhdpj0ykjj6hwdjarxs8a79zcj5") (y #t)))

(define-public crate-zlgcan_common-0.2.2 (c (n "zlgcan_common") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0h7b7rfxk7ya4im7m3gfpn5jkbpbvfh2kkfg464a3zv6yy289vpr") (y #t)))

(define-public crate-zlgcan_common-0.2.3-Alpha (c (n "zlgcan_common") (v "0.2.3-Alpha") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0qsymkjq15mpzm7sjbm2z39rpi54b28qav8871vc4j8swg6w2m6q") (y #t)))

(define-public crate-zlgcan_common-0.2.3-Beta1 (c (n "zlgcan_common") (v "0.2.3-Beta1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "085msq6ribpgaj8rk9z8i9yzfn4q2rd3zq5cj614hxg2wjh8qq1p") (y #t)))

(define-public crate-zlgcan_common-0.2.3-Beta2 (c (n "zlgcan_common") (v "0.2.3-Beta2") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "16yfswhpvzqy4hbr7nw7l8z4m6nj41j8393d8wlpwv0mysjgpmi8") (y #t)))

(define-public crate-zlgcan_common-0.2.3-Beta3 (c (n "zlgcan_common") (v "0.2.3-Beta3") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1p7xc6rhpqkkrhj5v8d0n5gnq6vyrbisiy1zx1asa8fq6av0v3r6") (y #t)))

(define-public crate-zlgcan_common-0.3.0 (c (n "zlgcan_common") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0y63fgj12dwqsxwa3gyi87p6shbfbf1ml4y0rxmgqz5kff5pyqgv") (y #t)))

(define-public crate-zlgcan_common-0.3.1 (c (n "zlgcan_common") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "03143c9hh9kimq8yysdaxjshq0226gglr7qqxmlqlwsm24wv38l7") (y #t)))

(define-public crate-zlgcan_common-0.3.2 (c (n "zlgcan_common") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0lrq9c95q8f96l48lmvv54y26jkx24s8hdg64haz3fcq6mpgq0ib") (y #t)))

(define-public crate-zlgcan_common-0.3.3 (c (n "zlgcan_common") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0xv7xzl14v5w0vylvfz0r6i6aybx9yqms4676vvdbw3gzya1kd3l") (y #t)))

(define-public crate-zlgcan_common-0.3.4 (c (n "zlgcan_common") (v "0.3.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1b8fl9spirrsy2y7as6ys5pg5f70xad6n6ai2kq1dfqjzin3am9j") (y #t)))

(define-public crate-zlgcan_common-1.0.0-rc1 (c (n "zlgcan_common") (v "1.0.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1028w2xfmwpfyggnmaq7zi1jyxwl7y336yyhg96bb6yx1w4hvv01")))

(define-public crate-zlgcan_common-1.0.0-rc2 (c (n "zlgcan_common") (v "1.0.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iwz1zvw7fdv1c2m0cl0zrr63ak46jl43ihp0wckc1v2cvb87nby")))

(define-public crate-zlgcan_common-1.0.0-rc3 (c (n "zlgcan_common") (v "1.0.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kp9k53m13ahfbpq9hvbfgfa24j9zw28xdqznljkzkgpwz4d4m9b")))

