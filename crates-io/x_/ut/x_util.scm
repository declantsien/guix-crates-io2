(define-module (crates-io x_ ut x_util) #:use-module (crates-io))

(define-public crate-x_util-0.0.1 (c (n "x_util") (v "0.0.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (o #t) (k 0)))) (h "1nqqqj80slpnj82aak1ixg3jskfr4610mn830566rm5jmyqfjcip") (f (quote (("timeout" "tokio/time"))))))

