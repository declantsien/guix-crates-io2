(define-module (crates-io x5 #{01}# x501) #:use-module (crates-io))

(define-public crate-x501-0.0.0 (c (n "x501") (v "0.0.0") (h "1n2bv83k10vy6r3g55zira10q42qcc51k8jdpnnma3sacz2zpcn4") (y #t)))

(define-public crate-x501-0.1.0-pre.0 (c (n "x501") (v "0.1.0-pre.0") (d (list (d (n "der") (r "=0.6.0-pre.1") (f (quote ("derive" "alloc" "oid"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1cxf8wf16hrg3awzb6vrrm2v7ymdd4vpxds5p92jmizjgrc30gja") (r "1.56")))

