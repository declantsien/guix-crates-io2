(define-module (crates-io x5 #{09}# x509) #:use-module (crates-io))

(define-public crate-x509-0.0.0 (c (n "x509") (v "0.0.0") (h "1y480r4k2qc3k5j9cm3vpgxxx6f52hl14f8jxzqbgvgcsgchw4ai") (y #t)))

(define-public crate-x509-0.1.0 (c (n "x509") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)))) (h "0259kwp3d1nhbxkdqsf0dj4c9bhmka4lzsz4rdwrx8zrr7m937ga")))

(define-public crate-x509-0.1.1 (c (n "x509") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)))) (h "0q7cx5fv5dkh088324p024qdrivcygk74vpzbclqbrxfg7hy5p7d")))

(define-public crate-x509-0.1.2 (c (n "x509") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)))) (h "06lxk7y76bji2982d9qcz4x4w8klrvyi3sry9ccdx85na3zvhdck")))

(define-public crate-x509-0.2.0 (c (n "x509") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cookie-factory") (r "^0.3") (d #t) (k 0)) (d (n "x509-parser") (r "^0.9") (d #t) (k 2)))) (h "0xhcaqcp5kn05va2q0hs54qw0pznappmiwsk2ls337wrqfafqg6a")))

