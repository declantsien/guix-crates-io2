(define-module (crates-io x5 #{09}# x509-tsp) #:use-module (crates-io))

(define-public crate-x509-tsp-0.1.0 (c (n "x509-tsp") (v "0.1.0") (d (list (d (n "cmpv2") (r "^0.2") (f (quote ("alloc" "std"))) (d #t) (k 0)) (d (n "cms") (r "^0.2.1") (f (quote ("alloc" "std"))) (d #t) (k 0)) (d (n "der") (r "^0.7.6") (f (quote ("alloc" "derive" "oid" "pem"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)))) (h "1q8mij1iskb7vh2hgc84k1azz8jnvddc5ibsbc2hf5m26klyrkpm") (r "1.65")))

