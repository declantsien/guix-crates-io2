(define-module (crates-io x5 #{09}# x509-test-gen) #:use-module (crates-io))

(define-public crate-x509-test-gen-0.1.0 (c (n "x509-test-gen") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.52") (d #t) (k 0)))) (h "0q3gyy66kvq2b774zz86dhddmv3hvzlgrffk4v2wp07r8k1q7nsn")))

(define-public crate-x509-test-gen-0.1.1 (c (n "x509-test-gen") (v "0.1.1") (d (list (d (n "asn1") (r "^0.15.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.52") (d #t) (k 0)))) (h "0a5x69m8kdlyn1s81mp9xrf5zg4zdnll87hhg5ks44nb15dhh3ph")))

