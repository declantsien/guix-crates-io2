(define-module (crates-io x5 #{09}# x509-test-certs) #:use-module (crates-io))

(define-public crate-x509-test-certs-0.1.0 (c (n "x509-test-certs") (v "0.1.0") (h "03j2x7pgj7fj03gmhglk35km6lhspyhgxni9jl51zskngyrhxbn7")))

(define-public crate-x509-test-certs-0.1.1 (c (n "x509-test-certs") (v "0.1.1") (h "083hz79zv5vipm370qjkhphfyqiqpb5yxc71wbpgxj8bb4jwaxi3")))

