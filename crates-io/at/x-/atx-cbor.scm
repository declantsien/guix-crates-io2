(define-module (crates-io at x- atx-cbor) #:use-module (crates-io))

(define-public crate-atx-cbor-0.0.1 (c (n "atx-cbor") (v "0.0.1") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06h3qmmsbvvgjbpm4z7jv0sy04qmgfli2f62hzq7ljkmhrprpp16")))

(define-public crate-atx-cbor-0.0.2 (c (n "atx-cbor") (v "0.0.2") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vrq4gfic1q43lm1ja73zm5y38f7hk5q9yf8201f3n6ppa2p8klv")))

(define-public crate-atx-cbor-0.0.3 (c (n "atx-cbor") (v "0.0.3") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dmvvn8dsm1cwjx9acyvrfij8fnwlfp6c8bxjj804kjwlwc82p2c")))

(define-public crate-atx-cbor-0.0.4 (c (n "atx-cbor") (v "0.0.4") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w57qa72wpixsfbcfxaf95mw3wy0cv7586xcwijxcxj40isy4317")))

(define-public crate-atx-cbor-0.0.5 (c (n "atx-cbor") (v "0.0.5") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "050wqczgdhxmbws8vch4wy84mlnckpwwi0hvmc8kqbh797bkxxgb")))

(define-public crate-atx-cbor-0.0.6 (c (n "atx-cbor") (v "0.0.6") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k79qscis3v2svaaj5j6bi9khzrm1xym4pqn9ic1g4jhi1396p5j")))

(define-public crate-atx-cbor-0.0.7 (c (n "atx-cbor") (v "0.0.7") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ccibbpbcc1mxrqspcpyxhdhw5ypfw7xhxkl37mzbjdxxwnbipq2")))

(define-public crate-atx-cbor-0.0.9 (c (n "atx-cbor") (v "0.0.9") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n4ddcp5a15cqr6l80w4g433qhd1kp5alzrqkbcr8442wvjwhxms")))

(define-public crate-atx-cbor-0.0.10 (c (n "atx-cbor") (v "0.0.10") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r1p8jdca0xzvc4badzil7zm8p2ym2ibsxn0ngn01p3m0vcl43d2")))

(define-public crate-atx-cbor-0.0.11 (c (n "atx-cbor") (v "0.0.11") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jlz1yi3adp2lkyhd8ayazsalswx6d8hlwx7fsyyvysfa7m5hbjj")))

(define-public crate-atx-cbor-0.0.12 (c (n "atx-cbor") (v "0.0.12") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11r9gjlzmv3ri13h92iwqs0183a2agilmzssmbl7f9q4i60m6i30")))

(define-public crate-atx-cbor-0.0.13 (c (n "atx-cbor") (v "0.0.13") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sz3xl9j2lrl31zbcfw64ph4xcn9x5ly8p87k1r25ih40gyd6vkk")))

(define-public crate-atx-cbor-0.0.14 (c (n "atx-cbor") (v "0.0.14") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01v5jimphn9zlkrb45i5777h8phz3dfgj55zx9x6msg107jndb7j")))

(define-public crate-atx-cbor-0.0.15 (c (n "atx-cbor") (v "0.0.15") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "040c6073n3xmgb083bjr9r74jgb2ym79bj4sz3g22j0wgsm7k6ss")))

(define-public crate-atx-cbor-0.0.16 (c (n "atx-cbor") (v "0.0.16") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v5z6pq6ai4qgy2c52hfb3p0q4aqmksq7pdmb5lra39kdnihf6nw")))

(define-public crate-atx-cbor-0.0.17 (c (n "atx-cbor") (v "0.0.17") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qbf3plg94mvwarar67yw31d6cdw8lv7dwzwq6czayz2qar4vcal")))

(define-public crate-atx-cbor-0.0.18 (c (n "atx-cbor") (v "0.0.18") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02hh60943h59vpby7mdjbf892q3p6j87b20dln9g1mczvc4drjvm")))

(define-public crate-atx-cbor-0.0.19 (c (n "atx-cbor") (v "0.0.19") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std" "alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17dhnk8s5i07iqdgwbdb3429r3icww0r364a74hynqzhxabfmzhg")))

(define-public crate-atx-cbor-0.0.20 (c (n "atx-cbor") (v "0.0.20") (d (list (d (n "minicbor") (r "^0.18") (f (quote ("derive" "std" "alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rn1ms29hqn21g0asszap6rdgv1439wvbggfni3kxmd7pmf2f46b")))

