(define-module (crates-io at pr atprotocol) #:use-module (crates-io))

(define-public crate-atprotocol-0.0.1 (c (n "atprotocol") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02axklp4p3203cclw95llbkdqzghix6jhc19503xp8hgq811a2z8")))

