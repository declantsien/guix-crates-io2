(define-module (crates-io at pr atproto) #:use-module (crates-io))

(define-public crate-atproto-0.1.0 (c (n "atproto") (v "0.1.0") (d (list (d (n "tokio") (r "^1.14") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1gmznj57yy674p5fvsxhih6r5n49rhv5pxp2x63rk3gi868zbdd8")))

