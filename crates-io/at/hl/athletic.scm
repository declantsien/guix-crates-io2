(define-module (crates-io at hl athletic) #:use-module (crates-io))

(define-public crate-athletic-0.1.0 (c (n "athletic") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (d #t) (k 0)) (d (n "nokhwa") (r "^0.10.0") (f (quote ("input-native"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0k4jyq7qbh7i62hal3mmrqwc3d3z6p3zzifnsdwxznkkb0ijj8is")))

