(define-module (crates-io at mu atmus) #:use-module (crates-io))

(define-public crate-atmus-0.1.0 (c (n "atmus") (v "0.1.0") (h "07dvpzw6rpb51w076cc8blkn3005bdlyx5df2jsy5lb3b0m78hnq")))

(define-public crate-atmus-0.2.1 (c (n "atmus") (v "0.2.1") (h "1rhi5dzryhfrbdxfkq3jczzzs9qw1yiwyjnrlsymplbj5f1bdcgf")))

(define-public crate-atmus-0.2.2 (c (n "atmus") (v "0.2.2") (h "0h446f69ghdc15c1blxbkgy7wji59qv5vrn7gadcx19hj9frpr11")))

(define-public crate-atmus-0.2.3 (c (n "atmus") (v "0.2.3") (h "0k3j7bbm5b3cxxr0ai8jl3xziswqi7hk72r57aps2047p09r3iy4")))

(define-public crate-atmus-0.2.4 (c (n "atmus") (v "0.2.4") (h "0a7962q18psg7jdpv4nk9cb9ibgmj83a3n2irv79mc9phwadv571")))

(define-public crate-atmus-0.2.5 (c (n "atmus") (v "0.2.5") (h "055pxqgmr04bwia67508xbsa16xp9ilha74l1qirn6f8kgjr6xzs")))

(define-public crate-atmus-0.2.6 (c (n "atmus") (v "0.2.6") (h "1rlwr65a0hhvzsg6vf89rgr8a3rl42w4zzrgak2kdlapry6gfx7y")))

(define-public crate-atmus-0.2.7 (c (n "atmus") (v "0.2.7") (h "0j8f9pcy4wkf7r00dkp4agrz24bi4yr9inbmwsv7fdd75j8i0q5r")))

