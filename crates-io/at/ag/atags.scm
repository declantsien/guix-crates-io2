(define-module (crates-io at ag atags) #:use-module (crates-io))

(define-public crate-atags-0.0.1 (c (n "atags") (v "0.0.1") (d (list (d (n "custom_debug_derive") (r "^0.5.0") (d #t) (k 0)))) (h "1fywm9y543yfrc1ag80f9qyalch5d1lyq3bdsw7l0cidpr5g6s7r") (f (quote (("nightly"))))))

(define-public crate-atags-0.0.2 (c (n "atags") (v "0.0.2") (d (list (d (n "custom_debug_derive") (r "^0.5.0") (d #t) (k 0)))) (h "07i3bhm0nsxz3m9a6glpav8y15y0wmspzp2mp4jfjbsm6lxdya3a") (f (quote (("nightly"))))))

