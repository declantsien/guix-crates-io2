(define-module (crates-io at ex atex) #:use-module (crates-io))

(define-public crate-atex-0.1.0 (c (n "atex") (v "0.1.0") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "1fzs3lqbmv268252nq072jvrvvhdbd5hkdhhps0rwpd9ha6yzyrq") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.0 (c (n "atex") (v "0.2.0") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "0cb2qxynx17jv998sd8ymcc9fvllvd900jkfvkzq6q56l68gx0kg") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.1 (c (n "atex") (v "0.2.1") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "067bag6ia4w40zscw0mmjrpjvnlpmrhzq22ngpmhr1vmg3hfxh9p") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.2 (c (n "atex") (v "0.2.2") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "19kq97q5rw7xhck2r42mii5a70bw1mjnfkcgys7gahrfn0w9mh3i") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.3 (c (n "atex") (v "0.2.3") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "0k6jb718w93d8s4v8plyalgrn8xwbrpvhakija9p4xnsbr7zj72l") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.4 (c (n "atex") (v "0.2.4") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "11649wmdky0yq3jsj0r4shxzp3wvr7awdlcps78fxx2rjxl7km66") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.5 (c (n "atex") (v "0.2.5") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "0nfc9ydd3jq7pyh78djbwpn0y53x9nvlwgab9wqih9kl9zm5qmip") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.6 (c (n "atex") (v "0.2.6") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "1s793z1sjf93dpzr5i1xwxjh09k68dkdd8r8khld7zrwx7prsccq") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.7 (c (n "atex") (v "0.2.7") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "088j5xg4s2yljw3k2vqcii321spyvya26rb29xyqyp9dwqxmry4z") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.2.8 (c (n "atex") (v "0.2.8") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "1xlvprj8wkppqxg81qdlmlh0scbzswqlf67d93vih53gkyzqf2n0") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

(define-public crate-atex-0.3.0 (c (n "atex") (v "0.3.0") (d (list (d (n "easy-sqlite") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "rt" "time" "macros"))) (d #t) (k 0)))) (h "1sdvigsnbhf27pfq2vxpdk4wy5scmdizrlhj1gcs31xb9v6w9fsx") (f (quote (("in_memory")))) (s 2) (e (quote (("sqlite" "dep:easy-sqlite" "dep:rusqlite"))))))

