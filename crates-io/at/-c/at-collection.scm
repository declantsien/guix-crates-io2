(define-module (crates-io at -c at-collection) #:use-module (crates-io))

(define-public crate-at-collection-0.1.0 (c (n "at-collection") (v "0.1.0") (h "1zvn18j8ldbjqqd0jcna7dcm1lh35xmdwq1g7hallg8w990k980v")))

(define-public crate-at-collection-0.2.0 (c (n "at-collection") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1pbnpi736h5hcvzicgq0k8vqaw914aqkd96x9ynkcgj8z8szr7zi")))

(define-public crate-at-collection-0.3.0 (c (n "at-collection") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "15cnmilzk25i56aj26kmvw4xh0bczbmbqwgkmqmqyx23lhf5607d")))

