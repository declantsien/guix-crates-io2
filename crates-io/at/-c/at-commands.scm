(define-module (crates-io at -c at-commands) #:use-module (crates-io))

(define-public crate-at-commands-0.1.1 (c (n "at-commands") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)))) (h "189dcj4fga5jnjrxgcgp8n62smga11jc72yvnw2zn6ddrwlijm6f")))

(define-public crate-at-commands-0.2.0 (c (n "at-commands") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)))) (h "032xnbggz1xxi4f9363gkv2w659awb4cr6nky11dhb16dpvckpl2")))

(define-public crate-at-commands-0.2.1 (c (n "at-commands") (v "0.2.1") (h "17md8zikyx95xp78g11nhq215zjf3y7i5f14d4d02xd3syn947bg")))

(define-public crate-at-commands-0.3.0 (c (n "at-commands") (v "0.3.0") (h "0i6xzr25x1yl5hwnval6p9gpwdb2rcnkn7x3pfklmkavlp32nkzk")))

(define-public crate-at-commands-0.4.0 (c (n "at-commands") (v "0.4.0") (h "0x0a746zxa5pkvrm25igasal87p7zkw7528bz94530d0zhwf43lw") (f (quote (("parser"))))))

(define-public crate-at-commands-0.4.1 (c (n "at-commands") (v "0.4.1") (h "1iy7sbg2q909czfipzv9pdk5jbbh3vi90w5ss0zqd1salf8mpsgd") (y #t)))

(define-public crate-at-commands-0.5.0 (c (n "at-commands") (v "0.5.0") (h "19gx9h1rsjja5z85hixz3mvzvgkhqnwd3vr8ny5gq03jvqwl04y9")))

(define-public crate-at-commands-0.5.1 (c (n "at-commands") (v "0.5.1") (h "0xb7bfnip9ffnqmjhz0l3k84bl6bqaxhhvdiif7l89gbnkrr9nhi")))

(define-public crate-at-commands-0.5.2 (c (n "at-commands") (v "0.5.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1f74422f2ljc1ywh9pz7fnnpiicawanvn8fdvfp55md1mcgd25nd") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-at-commands-0.5.3 (c (n "at-commands") (v "0.5.3") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1d0q7v6zzpn15w6h7g8vys9h5vr8gbn7hdiqrq8fyny6hp8ld1ja") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-at-commands-0.5.4 (c (n "at-commands") (v "0.5.4") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0rlx2q9v2wpxmrdqk2iqm617cwv8vi6bixnmy2w14ag5vnkbrrda") (s 2) (e (quote (("defmt" "dep:defmt"))))))

