(define-module (crates-io at ty atty) #:use-module (crates-io))

(define-public crate-atty-0.1.0 (c (n "atty") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0z31b5k8gl2i646vxqhhfa7n1m4a87xxi6yhy92gfvzk1gdjgprb")))

(define-public crate-atty-0.1.1 (c (n "atty") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13hz0k92q8h272n9kznqcnmczzz05vp5vdjqzbdwhwgif3hybf60")))

(define-public crate-atty-0.1.2 (c (n "atty") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1wjx1nkj7zjak468184nrf6gzdzb22vv7fx68p64hr7h6434rzfh")))

(define-public crate-atty-0.2.0 (c (n "atty") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1pdmgnalsd1dsrnlvrq6w2b90d77171wy4jrb5ifhibax8wg6n67")))

(define-public crate-atty-0.2.1 (c (n "atty") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "09i0acvrmwinz84z33adwgfrq74qj9q1n71k4v0bwzkj3h4wpd6p")))

(define-public crate-atty-0.2.2 (c (n "atty") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0n8196ml3kdx3mbha18bip7dmkg23xjs7325fi4531gsnw6xl4nr")))

(define-public crate-atty-0.2.3 (c (n "atty") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0q48nwhn9zmxlgkgp1jjk0zaadmi8vpgizs0c23p85crxh00ir91")))

(define-public crate-atty-0.2.4 (c (n "atty") (v "0.2.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0as27rcy3snppv912gpra7mgv0pmqsva6gn9p1hfi97j5qk5ax2h")))

(define-public crate-atty-0.2.6 (c (n "atty") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0n8qslyf3qhmbj1k10pi5amw0xrv3shdw9lxr31s0c1csipnall3")))

(define-public crate-atty-0.2.7 (c (n "atty") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vbpzisik6dm4czab8gml07inlfcxq89fa5ki6r0p97n2njdibw5")))

(define-public crate-atty-0.2.8 (c (n "atty") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1m6rfhrvbpqvlh52prmh83r9c70lbng7080mvx3df23ndwyi905g")))

(define-public crate-atty-0.2.9 (c (n "atty") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1l3d8yv4y7qy7fw200fqnamkhr2g7szra89nw472s6qsvmkah2b6")))

(define-public crate-atty-0.2.10 (c (n "atty") (v "0.2.10") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lfvh0nm0a5byaw9xwj2s8ci9y8sbj4821hg4n573h149jma3i1g")))

(define-public crate-atty-0.2.11 (c (n "atty") (v "0.2.11") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "termion") (r "^1.5") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lln6vaczj521qqjbaqnb81w5p6xk4fjfkg33r0m22cm4f3mnzcs")))

(define-public crate-atty-0.2.12 (c (n "atty") (v "0.2.12") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mgqsrwniyh0sh1zfzzlcq1pslc8s43h1v0i6riihfrbymlymapc")))

(define-public crate-atty-0.2.13 (c (n "atty") (v "0.2.13") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "140sswp1bwqwc4zk80bxkbnfb3g936hgrb77g9g0k1zcld3wc0qq")))

(define-public crate-atty-0.2.14 (c (n "atty") (v "0.2.14") (d (list (d (n "hermit-abi") (r "^0.1.6") (d #t) (t "cfg(target_os = \"hermit\")") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "processenv" "minwinbase" "minwindef" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s7yslcs6a28c5vz7jwj63lkfgyx8mx99fdirlhi9lbhhzhrpcyr")))

