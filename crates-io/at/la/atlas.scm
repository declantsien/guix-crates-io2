(define-module (crates-io at la atlas) #:use-module (crates-io))

(define-public crate-atlas-0.1.0 (c (n "atlas") (v "0.1.0") (d (list (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0rrlrglxvmpp6lii52fdqj45q39m2v29hhxg12h9abaadw1zrxkx")))

(define-public crate-atlas-0.2.0 (c (n "atlas") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "framing") (r "^0.3") (d #t) (k 0)) (d (n "png-framing") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0v3idlcl65jm6l6xjdd8fj9wgyr03z24xy2y43avc2v1kjab362c")))

(define-public crate-atlas-0.3.0 (c (n "atlas") (v "0.3.0") (d (list (d (n "framing") (r "^0.6") (d #t) (k 0)) (d (n "png-framing") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "12kvl86vby6k3cgv8726wqvkz61mmdvaf8q0hi1cxrmrcqdp8wix")))

