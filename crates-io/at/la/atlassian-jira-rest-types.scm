(define-module (crates-io at la atlassian-jira-rest-types) #:use-module (crates-io))

(define-public crate-atlassian-jira-rest-types-0.1.0 (c (n "atlassian-jira-rest-types") (v "0.1.0") (d (list (d (n "schemafy") (r "^0.6") (d #t) (k 0)) (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xj0dih41qm5ajr0vwp7bqn0j879w783fqvj0qfiqjack090pc5g")))

(define-public crate-atlassian-jira-rest-types-0.2.0 (c (n "atlassian-jira-rest-types") (v "0.2.0") (d (list (d (n "schemafy") (r "^0.6") (d #t) (k 0)) (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p3hgc2dd78vld6gk7wbqgkrlkscr7c749jj2jlsf8lbk32fp5w6")))

(define-public crate-atlassian-jira-rest-types-0.3.0 (c (n "atlassian-jira-rest-types") (v "0.3.0") (d (list (d (n "schemafy") (r "^0.6") (d #t) (k 0)) (d (n "schemafy_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "173kp6ljnakrlvxr8y821wpqchag10wm6jd5bdy6b7bjyyxdfn53")))

