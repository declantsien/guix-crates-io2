(define-module (crates-io at la atlatl) #:use-module (crates-io))

(define-public crate-atlatl-0.1.0 (c (n "atlatl") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "fst") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04h6dwc6c8jilswq3yzwfs8gbizdjqdl7nbzha8hn92z26x4jsqh") (f (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-atlatl-0.1.1 (c (n "atlatl") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "fst") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zx52iqx9yd0dhgmg1wwakg92i0wqnrrqrn21gn6954dz05s15sx")))

(define-public crate-atlatl-0.1.2 (c (n "atlatl") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "fst") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18kyvdm56fdb52b1sryi80xgs3nkjdylynsv324aiqnj85l1bfrj")))

