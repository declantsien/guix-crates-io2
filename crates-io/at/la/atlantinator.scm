(define-module (crates-io at la atlantinator) #:use-module (crates-io))

(define-public crate-atlantinator-0.1.0 (c (n "atlantinator") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "11fmc1y7xi9zvhz6ga6bb9q107p9npya2m3jnwd29r0jpy933rna")))

(define-public crate-atlantinator-0.1.1 (c (n "atlantinator") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0nwjfnkcakf6kaykzrr5lraw8xhz42v5va3xmwnpvgrgvx5kp5h3")))

