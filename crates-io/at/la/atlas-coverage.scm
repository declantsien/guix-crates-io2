(define-module (crates-io at la atlas-coverage) #:use-module (crates-io))

(define-public crate-atlas-coverage-0.1.0 (c (n "atlas-coverage") (v "0.1.0") (d (list (d (n "atlas-coverage-core") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "09n2zns8m2rqd9yc0kb0hsjkljyd6afnjcdckn7dgzmvppwmr62h")))

