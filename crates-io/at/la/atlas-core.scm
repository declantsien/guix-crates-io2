(define-module (crates-io at la atlas-core) #:use-module (crates-io))

(define-public crate-atlas-core-0.0.0 (c (n "atlas-core") (v "0.0.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1nmg9j7ky42y2zc30dpfxnxdg7sb6682jx2w3rpxb323z5wydqvj")))

(define-public crate-atlas-core-0.0.1 (c (n "atlas-core") (v "0.0.1") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1gv9j9dac1gi3gskc8xvxw9w3y5qyq9ja5qf0lyyqimpanffbjk5")))

(define-public crate-atlas-core-0.0.2-alpha (c (n "atlas-core") (v "0.0.2-alpha") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "07pxwm6rpz0az8g6qwr9c4vf04wzqsiz9r2ixz64jrkzglng1l7p")))

(define-public crate-atlas-core-0.0.2-alpha2 (c (n "atlas-core") (v "0.0.2-alpha2") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "004j68wr7r9b4myijvig9g93p5k6srk4yahzklpcx7qqabs529k7") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.0.2-beta1 (c (n "atlas-core") (v "0.0.2-beta1") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1373hxk38sxmqdpblckbcfgp80issy7lfznrqgfgcslydxrn6had") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.0.2 (c (n "atlas-core") (v "0.0.2") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1gc935gkv9n5in2xxw0p0xnfla6dyzmmynja5pnnf259snl5a1gd") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.0.3 (c (n "atlas-core") (v "0.0.3") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "1pbwznwncgia77r07hcmb01za1lck5cv18q4q484g7mmd1f0v308") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.1.0 (c (n "atlas-core") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)))) (h "0ig0mv1d73p3pcmin75v1znrcnq70knz7qkmq7bx6gyih7cw3jws") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.0 (c (n "atlas-core") (v "0.4.0") (d (list (d (n "ariadne") (r "^0.3.0") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "0fdmq7p5d4r731n36y900dbscyj8bxks66dmb3plhyxjvyirb6gc") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.1 (c (n "atlas-core") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "1l7nnypjw52vf6x27ac7xrcw4rlzsn566brm99hv5250c694zr6p") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.2 (c (n "atlas-core") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "1qjwb1n6vj2dlhz9xf072pll6zxl8j31yf512acx75k20yxljgh4") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.3 (c (n "atlas-core") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "0lalb9l7ps1nf6zbjk84fximfkv67yw99f0yrs7zv2gvc0021gd9") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.4 (c (n "atlas-core") (v "0.4.4") (d (list (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "0x5xcnhbz4kyzbkjfzd0fhwna08cs659f5b24d4sqfmlfhk0gal9") (f (quote (("default" "debug")))) (s 2) (e (quote (("debug" "dep:env_logger"))))))

(define-public crate-atlas-core-0.4.5 (c (n "atlas-core") (v "0.4.5") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "0mlx2ia2f7k8z60av1ln31v5pq7ld0jbgqb2z4br63i88acgnb6a")))

(define-public crate-atlas-core-0.4.6 (c (n "atlas-core") (v "0.4.6") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "internment") (r "^0.7.4") (d #t) (k 0)))) (h "1bhdaac4jjxyjb0rnxw6ajp6536pm27pg1d7jmxmp6glvb9mslhc")))

