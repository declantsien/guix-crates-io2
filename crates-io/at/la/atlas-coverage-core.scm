(define-module (crates-io at la atlas-coverage-core) #:use-module (crates-io))

(define-public crate-atlas-coverage-core-0.1.0 (c (n "atlas-coverage-core") (v "0.1.0") (d (list (d (n "ecmascript") (r "^0.1") (d #t) (k 0)) (d (n "globset") (r "^0.4") (d #t) (k 0)) (d (n "lcov-parser") (r "^3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "vlq") (r "^0.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0cdq09h76minpmw3xidf5ag4dzhvvzn7gkgpnrl7wn4p3adrmb2y")))

