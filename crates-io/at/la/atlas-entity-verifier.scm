(define-module (crates-io at la atlas-entity-verifier) #:use-module (crates-io))

(define-public crate-atlas-entity-verifier-0.1.0 (c (n "atlas-entity-verifier") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "07vd1lcwzfc7f9i40ngjjsfqm06nfg9rv1jk67rjl77i4ifwnngf")))

(define-public crate-atlas-entity-verifier-0.1.1 (c (n "atlas-entity-verifier") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1s21k5j3banmslhiif9xbx6lwnqk5h54shkly0nddm5vk7hv7wgw")))

(define-public crate-atlas-entity-verifier-0.1.2 (c (n "atlas-entity-verifier") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0nxh29g73892nxdwm16yjvv4a5aigyw98chcrl0yx50jpcgz2z2n")))

(define-public crate-atlas-entity-verifier-0.1.3 (c (n "atlas-entity-verifier") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1vy05jhz0iczrsmydimk7ighpj4h4k4s8fk7vn5plvdqrrzlkkz9")))

