(define-module (crates-io at la atlas-confluence) #:use-module (crates-io))

(define-public crate-atlas-confluence-0.1.0 (c (n "atlas-confluence") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "1vay216jvx3psa6bcammjv1yl8ymlyvvpbvlrz2b83gyx8h1a8vz")))

(define-public crate-atlas-confluence-0.2.0 (c (n "atlas-confluence") (v "0.2.0") (d (list (d (n "async-recursion") (r "^1.0.4") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "1w4cwl4s0x02s0adhhy1fhclnls58916br5lc3jrn2zb8v9f2jzn")))

