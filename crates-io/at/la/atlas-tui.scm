(define-module (crates-io at la atlas-tui) #:use-module (crates-io))

(define-public crate-atlas-tui-0.1.0 (c (n "atlas-tui") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.39") (d #t) (k 0)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "socket") (r "^0.0.7") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (d #t) (k 0)))) (h "082f8sbf4pc28y6yiaa823zz6vcssy3s241y61yh3gyw9y5dqyav")))

