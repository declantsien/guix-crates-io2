(define-module (crates-io at lv atlv) #:use-module (crates-io))

(define-public crate-atlv-0.1.0 (c (n "atlv") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "111f3d1s1v3y6961b0lpj2n9qpxn10qz4fs3717zsqarqq5kbwv4") (y #t)))

(define-public crate-atlv-0.1.1 (c (n "atlv") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "1ba9yad7i1gngzqmy13fi2qk4p3fl784jvm0d4mfk49jqpj03ycr")))

(define-public crate-atlv-0.2.0 (c (n "atlv") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "13n97jhxn8i1578jqswr80fdp2wzwrmqp6b249s47p1j5ihqz7mh") (y #t)))

(define-public crate-atlv-0.3.0 (c (n "atlv") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "1hzpwcvqmlz7cn2kjvq5ipwv2n7ja017amdxljgh35yg3d8785fy") (f (quote (("default" "quickcheck")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

(define-public crate-atlv-0.4.0 (c (n "atlv") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "09a0xgi926cyynh47nxsp0kvhwjmpfsz4k49dibh2jkynam4vcrh") (f (quote (("default" "quickcheck")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

(define-public crate-atlv-0.5.0 (c (n "atlv") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "0h3fahdglwldgc52kp0asisl2pd4j3g6dcl6yspy42hlc5p3a4n1") (f (quote (("default" "quickcheck")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

(define-public crate-atlv-0.6.0 (c (n "atlv") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "0fqqzs8c4aqdzhswpgzgryzsssxn11bgyilaqr14r8znpkfccb3j") (f (quote (("default" "quickcheck")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

(define-public crate-atlv-0.6.1 (c (n "atlv") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "truncate") (r "^0.1.0") (d #t) (k 0)) (d (n "vlq-bij") (r "^0.1.0") (d #t) (k 0)))) (h "1gvkyk279yjlfl01sp6jrn0n6vz37jb2mn1sy1ya6sp2iismdalx") (f (quote (("default" "quickcheck")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

(define-public crate-atlv-0.7.0 (c (n "atlv") (v "0.7.0") (d (list (d (n "quickcheck") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "vlq-bij") (r "^0.2.0") (d #t) (k 0)))) (h "02k7v7y7yg2q919yw2pmmp298nmigziygrj89xwkrr7nrsbfb8fm") (f (quote (("default")))) (s 2) (e (quote (("quickcheck" "dep:quickcheck"))))))

