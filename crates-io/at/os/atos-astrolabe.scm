(define-module (crates-io at os atos-astrolabe) #:use-module (crates-io))

(define-public crate-atos-astrolabe-0.0.1 (c (n "atos-astrolabe") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.27.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "object") (r "^0.31.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.5") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^12.1.5") (d #t) (k 0)))) (h "1kivj1m9nfacwcivlh8794cnkxgq7awm5dpwhwp2z10nmp3lz9qd") (y #t)))

(define-public crate-atos-astrolabe-0.0.2 (c (n "atos-astrolabe") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.27.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "object") (r "^0.31.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^12.1.5") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^12.1.5") (d #t) (k 0)))) (h "0qcmz89db054kqwf25a9hv0dg578r3ijppadsy2qa8hpm4x1y7ki")))

