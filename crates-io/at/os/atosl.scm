(define-module (crates-io at os atosl) #:use-module (crates-io))

(define-public crate-atosl-0.1.0 (c (n "atosl") (v "0.1.0") (h "05ivhp8plh01zzvyxd61y6cxylfs7swfqbs1vbvvax9g7xwfqnd9")))

(define-public crate-atosl-0.1.1 (c (n "atosl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)))) (h "0hsr8f7vjc6lm1xdlli8p9dxci9g31gg8qfxlva2rbf66160wdlw")))

(define-public crate-atosl-0.1.2 (c (n "atosl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)))) (h "0p9yapz4p2j70q4114mf970jwjf2ncri4bk87q1mfrl2f4d1jvrw")))

(define-public crate-atosl-0.1.3 (c (n "atosl") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)))) (h "0wrx2k93lxyxdqqp4hn0bn6ikk5bmsb2jsmd0qf58bgg0mmq7x9q")))

(define-public crate-atosl-0.1.4 (c (n "atosl") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)))) (h "0hrwki8b3swzhk35x8ksh5sf3bi78qy8p8l2fb29vdfsjjv3dr1d")))

(define-public crate-atosl-0.1.5 (c (n "atosl") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "1fim1v5lkiazy5nl1cxwfjzvdr0hmvc2177dxb5xg2nvzza603my")))

(define-public crate-atosl-0.1.6 (c (n "atosl") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "06clqkxl9zbqrlc8zzvgs07vvmnkkls4lhrlxnzl5x8i86hl4mh9")))

(define-public crate-atosl-0.1.7 (c (n "atosl") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "0gqzgs7xzly6kb39zfghgnwmqv67bgy8bsln6jjdidhklsc316qh")))

(define-public crate-atosl-0.1.8 (c (n "atosl") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "07k395jibgqan013y69nivvkc71fyifc94wci4419vh76yrgs9rl")))

(define-public crate-atosl-0.1.10 (c (n "atosl") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "16czy7fbihxaiq7knj09llvgmc3z9dszkkzj821a1nzjbbc8349f")))

(define-public crate-atosl-0.1.11 (c (n "atosl") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "1zgas3qjx70vd1lmvqgqrc4ixnkv1h7li466pdrfv2dyn5j66794")))

(define-public crate-atosl-0.1.12 (c (n "atosl") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "070d1xggvwnm5m23c49y89w2rsxw4hww1br671ardvsq1gkqw8va")))

(define-public crate-atosl-0.1.13 (c (n "atosl") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.5.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.5.0") (d #t) (k 0)))) (h "0b8b77zj7lvks5wvxlyrp5wa6dq2wc3h9mw1csij3wv7s6p7jcya")))

