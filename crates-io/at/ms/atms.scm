(define-module (crates-io at ms atms) #:use-module (crates-io))

(define-public crate-atms-0.1.0 (c (n "atms") (v "0.1.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "blst") (r "^0.3.7") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "136yg02nvllvi4g8iz89sv86w387q68fkwqkrnw0wvb0wsfasy4i")))

