(define-module (crates-io at re atree) #:use-module (crates-io))

(define-public crate-atree-0.5.0 (c (n "atree") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0agmvqph4d2bb4ps0p52yl76wi2yags38ff1l448r2s7f54n3zgm")))

(define-public crate-atree-0.5.1 (c (n "atree") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pmyv35if1aaa59qav1kxr9qwlbd530dq8wbmpfgc56xhydc2ajv")))

(define-public crate-atree-0.5.2 (c (n "atree") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "161sr3n46rzl8lrlh1k54rysfbf1b91fvl3mdwy9gzxrir3p698k")))

