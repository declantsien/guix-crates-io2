(define-module (crates-io at ax ataxx) #:use-module (crates-io))

(define-public crate-ataxx-0.1.0 (c (n "ataxx") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0b67m8v75qxlbs6bcqszlv7f8i279rvkzz3wxblrj22d0ibnwsbj")))

(define-public crate-ataxx-0.2.0 (c (n "ataxx") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08jh7aypcb2l4scp2727l8xbcdmd7j52a1z67g0rcgsgfm1xib6h")))

