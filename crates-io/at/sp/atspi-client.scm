(define-module (crates-io at sp atspi-client) #:use-module (crates-io))

(define-public crate-atspi-client-0.1.0 (c (n "atspi-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "atspi-common") (r "^0.1.0") (k 0)) (d (n "atspi-proxies") (r "^0.1.0") (f (quote ("unstable-traits"))) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zbus") (r "^3.12.0") (d #t) (k 0)))) (h "1ia4ywbw9qkr7k73dgx7z4ki4dg01lxzwac3cs8r0qcxwahffr74") (f (quote (("tokio" "atspi-proxies/tokio" "atspi-common/tokio") ("default" "async-std") ("async-std" "atspi-proxies/async-std" "atspi-common/async-std"))))))

