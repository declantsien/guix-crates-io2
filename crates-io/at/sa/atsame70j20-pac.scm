(define-module (crates-io at sa atsame70j20-pac) #:use-module (crates-io))

(define-public crate-atsame70j20-pac-0.1.0 (c (n "atsame70j20-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09l85j3f500hfj6xcalk8983g5m6920ln1kksxikai7iab01dglh") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20-pac-0.2.0 (c (n "atsame70j20-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qwqfhy9jrjv884sq2a99r0rv5jjrxcb8r3wrfnphvwmryb5nfzd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20-pac-0.2.1 (c (n "atsame70j20-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mlajk4ylr6h4566h5snik47g8q017y6cx8sm25qcq1ygy454k2s") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20-pac-0.2.2 (c (n "atsame70j20-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "191d9sasdf4milbrna2j2154zff8z7l9g0p9md8zsw3rx6yyc0d3") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20-pac-0.3.0 (c (n "atsame70j20-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rh2a97bn7rig9qd3lxjpr869n48lzjchih05ndkapbk3fgl9zkp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

