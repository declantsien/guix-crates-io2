(define-module (crates-io at sa atsamd11d) #:use-module (crates-io))

(define-public crate-atsamd11d-0.10.0 (c (n "atsamd11d") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09jy2waj7n1k1gxny9r7i1zcxaiqviq1hv79dxnjixwypi8hmx5c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11d-0.11.0 (c (n "atsamd11d") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00z4cnhfq61a93i5p7cpz7w67qbagh4gyamknz2n7w6c2i9rcq2k") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11d-0.12.0 (c (n "atsamd11d") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0gfwpmwssfvqq1jd14x9f73q4mz4wfdx66v9b0czyiysja48664i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11d-0.13.0 (c (n "atsamd11d") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1h9iqxjxdxvf8c2qcdlv5ixa3pp05jf55q36imvx4jx8ivfp4vsn") (f (quote (("rt" "cortex-m-rt/device"))))))

