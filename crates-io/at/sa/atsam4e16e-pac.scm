(define-module (crates-io at sa atsam4e16e-pac) #:use-module (crates-io))

(define-public crate-atsam4e16e-pac-0.1.0 (c (n "atsam4e16e-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1hagijkzdj62y7jccx2cxivg9pia5x183kmplavrj80m7asdvghw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.1 (c (n "atsam4e16e-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05nj0r9ryq93bzv43ffc63i3j80lr1bdmb41lbyvnnbi281vfd9f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.2 (c (n "atsam4e16e-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ycb43gz4zhl978jwiwjda4pyr5iqjsanmp5jl60xw2i23bygmig") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.3 (c (n "atsam4e16e-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0qms2vc1wczjd488c0qv54hxv56vfhdv5jyg3zc10byjnxm9apl4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.4 (c (n "atsam4e16e-pac") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1fyinwwjqi8xd5jvmb1xlpkpcvzd7klbisi5n1697abn1n2kpyr0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.5 (c (n "atsam4e16e-pac") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "02sdbhicgzgy7b5b6rw19pryjxzh11q6hd8an2g09g1d11lj33cy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.6 (c (n "atsam4e16e-pac") (v "0.1.6") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1y7745rggwg9qw3zsqz4cfdy4bhm7n7yqxlswyasjlmpj9yb8dr5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.7 (c (n "atsam4e16e-pac") (v "0.1.7") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rays4lmpisznqshsz14kdq0s28k3iw399r98syjsgwlz21gwa3f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.1.8 (c (n "atsam4e16e-pac") (v "0.1.8") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1fi59qilp39jrvifsvv2775xy7bw5ig7b8ncd76n19ry3836rz59") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.2.0 (c (n "atsam4e16e-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0sn9swp8r1z6zmayv1fs92qf5g9v2p1jrh1r7dikp6qmvv887mhc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.2.1 (c (n "atsam4e16e-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bpp166r598ccaic07dbazhapshm4zg213z55zix3a577mkrl8mv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.2.2 (c (n "atsam4e16e-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vv1rwcp9wxd4z94xknk1hqav92r2njn0v6qg95si20fnsmjjs2h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16e-pac-0.3.0 (c (n "atsam4e16e-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0w8xbmgnm7yzhi1ngan9bkrkjz3622qasx5msq6qkzz6ijsggsxv") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4e16e-pac-0.3.1 (c (n "atsam4e16e-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mxmvvhwyh4vy2gv92mvml56gbai7vdgn6mfn4nl2x4m83v04bzr") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

