(define-module (crates-io at sa atsame70j20) #:use-module (crates-io))

(define-public crate-atsame70j20-0.21.0 (c (n "atsame70j20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0w3q54wvj4s0yskn5g29vx74saghpb3w4lfy15xx930gdm5ild7p") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

