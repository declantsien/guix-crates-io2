(define-module (crates-io at sa atsam4lc8c) #:use-module (crates-io))

(define-public crate-atsam4lc8c-0.1.0 (c (n "atsam4lc8c") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0a899xqhsq7z56ikx1zd3hl347msvbn084y89rw7hm5l6bd5r1gi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8c-0.1.1 (c (n "atsam4lc8c") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-log") (r "^0.2.0") (f (quote ("semihosting"))) (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.2.0") (d #t) (k 2)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0n7s1fkzl001056dxwf5wrvrasq5kk2gp084r37vc33a23fqqjnc") (f (quote (("rt" "cortex-m-rt/device"))))))

