(define-module (crates-io at sa atsam4s4c-pac) #:use-module (crates-io))

(define-public crate-atsam4s4c-pac-0.1.1 (c (n "atsam4s4c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1d8xixl2nr8ld39arrd4mpdzz5nh63hf8z9xsxl3z4v7b9v9qg0m") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.1.2 (c (n "atsam4s4c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "12x0r0lpsxy9pk9njqixk393s1849qm7p8r2c8zdrkcv28k6w1i7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.1.3 (c (n "atsam4s4c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0r4afg08giczb0gvxz2sfhyfp69rf0nrdpv1m45bg2bf36qx8q9f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.2.0 (c (n "atsam4s4c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1kg49qgc0swb5bcd2hsjfbrc1xvg1im4zpfvdp0ky235llhf0j3k") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.2.1 (c (n "atsam4s4c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0wl86zxd4mdf14sjxpf0yziybz5570wckmzi7kyi51y72rmni2g8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.2.2 (c (n "atsam4s4c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xxcgpw716pkyx6iyjm92chk3awvfsyawi960bbmc73a4aqhj1cz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4c-pac-0.3.0 (c (n "atsam4s4c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00y5fxbsdpyi87k36q43m5lbf15hmg2c141ma9a61l7kns5r0kz6") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s4c-pac-0.3.1 (c (n "atsam4s4c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ibwcmydb2y756kk1s5i78xj0r7r8j8lrj4h1ih3330arsjqg6k5") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

