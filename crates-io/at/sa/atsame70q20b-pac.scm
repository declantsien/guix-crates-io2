(define-module (crates-io at sa atsame70q20b-pac) #:use-module (crates-io))

(define-public crate-atsame70q20b-pac-0.1.0 (c (n "atsame70q20b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zpvpla0dghy58f7nm64919xzigwpccrscsh4zyzrxpvlgh9nfvr") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20b-pac-0.2.0 (c (n "atsame70q20b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0p7v2hd9b8j43ai7264vqyvx0yb2pf0vx613hb559vk36lhz72mm") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20b-pac-0.2.1 (c (n "atsame70q20b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14pwkdpsc9kflbmf2gr12rvymppsmgzdb5y1k985zlhxlp1rdf21") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20b-pac-0.2.2 (c (n "atsame70q20b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1yzc8nf2vici7xaipa2mzfrnxg5avpq6v7h3axhgzwp4h3pzrx27") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20b-pac-0.3.0 (c (n "atsame70q20b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12kcmyyyyf2q0sm64f138ar1bz3vqd8xgnhzds7qs46dpmck94qg") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

