(define-module (crates-io at sa atsamv70n20) #:use-module (crates-io))

(define-public crate-atsamv70n20-0.21.0 (c (n "atsamv70n20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0arv5vf1bi3w8dp5k8mczf3fdv6l9g32ad1sphgv0h7vkgmc32jx") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

