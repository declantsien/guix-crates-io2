(define-module (crates-io at sa atsam4n8c-pac) #:use-module (crates-io))

(define-public crate-atsam4n8c-pac-0.1.0 (c (n "atsam4n8c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1w94kb2p8b9lb9m19a494xb878znnb8jkqz6p84a7lbn6172px1n") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8c-pac-0.2.0 (c (n "atsam4n8c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06lnvk1zn1j8ican847g2j9jwjxd19rz6w7984rj8daza8x1zll8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8c-pac-0.2.1 (c (n "atsam4n8c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1m4rhdyic2ip5afksg0qiyikmxwgqgb7y7mzsnbm402css8mn34k") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8c-pac-0.2.2 (c (n "atsam4n8c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11dbv8gg0qg3yvancx85bimag4hyh6dz1658d5qp0zf9nx3w4n1v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8c-pac-0.3.0 (c (n "atsam4n8c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r6nzc39b7xm9qql9a0agvnbkm9jj9831i3r3ans2q0ss9k4q98c") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4n8c-pac-0.3.1 (c (n "atsam4n8c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zmq7k6a3a1p51g9ws5c73g7v54jmi6pxv9qpfb5s0h8lg51p0zp") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

