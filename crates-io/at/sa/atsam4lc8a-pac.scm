(define-module (crates-io at sa atsam4lc8a-pac) #:use-module (crates-io))

(define-public crate-atsam4lc8a-pac-0.1.0 (c (n "atsam4lc8a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1n14rhpsv2qd0d06fgfcqgbmpryyn6m0mqrwfb96dzjhjbk88j7a") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8a-pac-0.2.0 (c (n "atsam4lc8a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14dwdq31c7ljpj7x881y0bp1vsr4x55rmsyycfjf73902wsffq7i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8a-pac-0.2.1 (c (n "atsam4lc8a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1972r5bbm18vjmn3b92wblqislqjqx0y64mzpqsd62s4m2pa3l8n") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8a-pac-0.2.2 (c (n "atsam4lc8a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kyvxylzxl91ik99ay30g9k3b7sryqsmqfjsa3czf2acldgr1b2j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8a-pac-0.3.0 (c (n "atsam4lc8a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00m5mqxyyn8imh4fjw2yhfbhk8mfrphwniip7qdgqip7vi97i01j") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8a-pac-0.3.1 (c (n "atsam4lc8a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1871574ddpvx0hdz72hyr5pz4s6lxyrhv69pjk6pazvdray5kw13") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8a-pac-0.3.2 (c (n "atsam4lc8a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1syk5f8i2ds9rhjrjssby3633k9d9vwx2263g19cj6f5xhxngxy0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

