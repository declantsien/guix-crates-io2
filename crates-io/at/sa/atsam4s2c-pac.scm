(define-module (crates-io at sa atsam4s2c-pac) #:use-module (crates-io))

(define-public crate-atsam4s2c-pac-0.1.1 (c (n "atsam4s2c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1idc3vs4389y4dnczfbwl9kkxzl3lglh1dw6rwclqy92idzlbnkp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.1.2 (c (n "atsam4s2c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0m70b2svv8791sbmqaxq4bp84zpzbfnkj5g9486nqn9jv7408d5h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.1.3 (c (n "atsam4s2c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03wr1lvv7qkk7jh8jcqd1lqxp9yyixc42bml4i54r41xvcqkszkm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.2.0 (c (n "atsam4s2c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1d1x79xdqwrnqhgpss4ii98zby0l199rsy9r5f5blf7f9kz1qnar") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.2.1 (c (n "atsam4s2c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12rajlm7fsz13amxqh2ar44m0a2smxxviw30pavisl8kz4fa63by") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.2.2 (c (n "atsam4s2c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rp1d149n9688b3jgcvpxvw0d6siwpxmhd8z546qlrf6hwd5l4mx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2c-pac-0.3.0 (c (n "atsam4s2c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m5ddv41mz6d30dpjxkrlj8hsndrsdkaiinnfn5v7xrabxzwzk55") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s2c-pac-0.3.1 (c (n "atsam4s2c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z9k73qrny592sfb9rsbzx97x52h2r122hy5sihygsi9c1dk3nh7") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

