(define-module (crates-io at sa atsam4sd16b-pac) #:use-module (crates-io))

(define-public crate-atsam4sd16b-pac-0.1.1 (c (n "atsam4sd16b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gk0dp738k53q8i1fw21v7d2rchdxifhrgj4cl0mhjbgvbqvs4vk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.1.2 (c (n "atsam4sd16b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ni66460hp3khp6ry0l3ycq2agf1ikaxs9jsywb04bzyvvjx427c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.1.3 (c (n "atsam4sd16b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1w8x2r2b7x4g28wsdlwgglw029vdgzyjwg7r16s6w10s6nnm1qlz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.2.0 (c (n "atsam4sd16b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0jv5pp2fldy5n1pcrlpahnphladv01xif31b5pk388ld2v7035fm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.2.1 (c (n "atsam4sd16b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qf0g3d8hqq7xafq4ah9dimh66j22qqqjkkl98pm4c34pf9pmhas") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.2.2 (c (n "atsam4sd16b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0w5lysndgqr8vlzp8226b8cq4qhnafrrfwfj450j2ilcp1xirfxc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16b-pac-0.3.0 (c (n "atsam4sd16b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1bsml2xkvgf5w0ki0mc88dn6fd6mcr3fks5s1mh3l06dywjmsypw") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sd16b-pac-0.3.1 (c (n "atsam4sd16b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1fq9zrviydgzg20xbl1v9dmsls5jf93ingfq1amv6515bh51xh6z") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

