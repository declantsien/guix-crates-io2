(define-module (crates-io at sa atsam4s2b-pac) #:use-module (crates-io))

(define-public crate-atsam4s2b-pac-0.1.1 (c (n "atsam4s2b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zj34064s1bpm8w4cdz86s4arivssd5iiqq69gdci5ixfyyix854") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.1.2 (c (n "atsam4s2b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0g8ybp7hc16r1ixpqawzygvwlr942bmrj1qa7m44b26qs2x97x8m") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.1.3 (c (n "atsam4s2b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ffb6idz6r61wj7gcbcqqjppbfsnqc8zd4zd7wng0g2nh9jkmg4x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.2.0 (c (n "atsam4s2b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08ljz5l69b739vkr38bd0kv7ifkwxr7ygdif00wkhg5km2gj9bki") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.2.1 (c (n "atsam4s2b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "180k35gchs632iz8c029zdqwprphylb0k8l5qkdar9qdiiywwb9g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.2.2 (c (n "atsam4s2b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1f1fg6qm7k7yjr633bifryqh4ydlg10c6g2mw99i7qmid19mrfcj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2b-pac-0.3.0 (c (n "atsam4s2b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03pwasshfvf7li026yh75q845yg3i6nfkjlijq92rf5h9yhgfdi5") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s2b-pac-0.3.1 (c (n "atsam4s2b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01w8hlswalimv4rijbm5chkcaf8sr1jmhlgvppgd6hgms8854gy6") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

