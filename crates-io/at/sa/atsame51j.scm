(define-module (crates-io at sa atsame51j) #:use-module (crates-io))

(define-public crate-atsame51j-0.8.0 (c (n "atsame51j") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "153q9syh5ckm7s67mc1506i8ad6qh42bg9iqx5imj4yyczrzflla") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51j-0.9.0 (c (n "atsame51j") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0lasycfmwlqy9h4zys3bzhirjz4k3gc1jx51wjhqp7lkp291ka8f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51j-0.10.0 (c (n "atsame51j") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0wzzp6imlvkxm9q8w7sp1lgg8hj62519386dvkc9vpch2rccjwy8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51j-0.11.0 (c (n "atsame51j") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0yxlg96w319sskpmaq7hj76wlzaylr54dycjc7jy4xqp48szcic0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51j-0.12.0 (c (n "atsame51j") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1vfpx15lvccshxb7glk19bqmi8gfn3g7adn8wwqq8l03f8ng389x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51j-0.13.0 (c (n "atsame51j") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1dqh1wlw18z6i73cf0g6ara6hml5hnl9g40qcpxxp4lf4zr5lacc") (f (quote (("rt" "cortex-m-rt/device"))))))

