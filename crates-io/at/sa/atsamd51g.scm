(define-module (crates-io at sa atsamd51g) #:use-module (crates-io))

(define-public crate-atsamd51g-0.8.0 (c (n "atsamd51g") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1qw8ba975d0k6dyy8fxyr7xxia7ybmagavg6ym4pfzd7y0lm5yca") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g-0.9.0 (c (n "atsamd51g") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0hqzxrkkfr8gkkgf46bsflqf96d003jaa8j1gkgrxf6ldjx18ck3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g-0.10.0 (c (n "atsamd51g") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qxranyl36qxgi0jrp9wbz4ak5s2c9wxm8x974mgw2gv4jbb3lpd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g-0.11.0 (c (n "atsamd51g") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "11biwchkbnnga4fnhagzgjlnyayl053c4h105bmzcf7xhs7fnfs0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g-0.12.0 (c (n "atsamd51g") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "08q8b3kmnlvsv46yb8w97y5rqjhwyiq0cick8q28ngfmwyh26bwf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g-0.13.0 (c (n "atsamd51g") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0g1igcp68hjvi6vqbnhp2jj37013hl3prxmz27z1lqiqyl42k9wb") (f (quote (("rt" "cortex-m-rt/device"))))))

