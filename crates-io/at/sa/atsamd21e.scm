(define-module (crates-io at sa atsamd21e) #:use-module (crates-io))

(define-public crate-atsamd21e-0.8.0 (c (n "atsamd21e") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1wn76nkgqm5d0mplg6hsklkmns11hvm8fn83hcq6j4yh5d1q0yzq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e-0.9.0 (c (n "atsamd21e") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0v51s1zxfy0wrfgxrk92b3nlwmjc70zyjsiq9q2c75cmxrg6f8dp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e-0.10.0 (c (n "atsamd21e") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "036gn11rc9r4lgvf21870xlsb26dh313v4ffkzbvlm97xrqd28r8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e-0.11.0 (c (n "atsamd21e") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0r33h2jn0qlalf98xj0bsq8fvcg8v1qlf02v2ahw0355wq6hi9sj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e-0.12.0 (c (n "atsamd21e") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0xgzncci86jy42d2s1r4vklcyvvp2x5x3mxsj8d3sqf8jn14c0mm") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e-0.13.0 (c (n "atsamd21e") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0i8gxzhylq9xxf0ljq89y5dcx5f2l1h60kgpx06kq3hkqdrq57rz") (f (quote (("rt" "cortex-m-rt/device"))))))

