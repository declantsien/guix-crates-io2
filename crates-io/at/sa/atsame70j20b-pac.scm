(define-module (crates-io at sa atsame70j20b-pac) #:use-module (crates-io))

(define-public crate-atsame70j20b-pac-0.1.0 (c (n "atsame70j20b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1z7xn266vrqb0srpj48jxialprv4k3sha5biqrjcpz26i1v59rlx") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20b-pac-0.2.0 (c (n "atsame70j20b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vjlg871nls29bylbvm3xk2fh9lhajbbxhh4yrm62jw8qba7sa01") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20b-pac-0.2.1 (c (n "atsame70j20b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0cjq12rr9kqm0jcs2qkr743jiad2h2jlzvv4rgxxzq75cxf49xr1") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20b-pac-0.2.2 (c (n "atsame70j20b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01ig01w444nsjccnnx6f84nc6jf3n5qnbdflf9fafxvqfdiwbd99") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j20b-pac-0.3.0 (c (n "atsame70j20b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10nkglp273lm23s5l5dmm68rd8cfzwfala3rfz1szjz22miy5rsw") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

