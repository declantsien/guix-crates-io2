(define-module (crates-io at sa atsam3x4e) #:use-module (crates-io))

(define-public crate-atsam3x4e-0.1.0 (c (n "atsam3x4e") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15x4br6sxfnkqjc5r6v9sv4zg3r6cnwn2gl104fm17w5wwych5gg") (f (quote (("rt" "cortex-m-rt/device"))))))

