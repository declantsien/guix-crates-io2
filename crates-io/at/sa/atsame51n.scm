(define-module (crates-io at sa atsame51n) #:use-module (crates-io))

(define-public crate-atsame51n-0.8.0 (c (n "atsame51n") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "06pp41avn039hvhky51pgqv4jbj1cm4glfdxn1yri24slcdmzc71") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51n-0.9.0 (c (n "atsame51n") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0lk66ckdj7bqb8hrb7kpffnznww6m7r4i0kibn0gxg6x1xcxljv8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51n-0.10.0 (c (n "atsame51n") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qpfi7kzrv4s7apf2w3nhm8allkdgnzzj8c70i92kk69fwx67756") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51n-0.11.0 (c (n "atsame51n") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1hlc4as42cr7vny8a6v8q50gcdj7sdi9578pr0sy5sa5zrs2qpi7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51n-0.12.0 (c (n "atsame51n") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1b5m4dgkx6404cg085lq1m4asr1cs7wb1j8r1pwi3j77g3aqz9jv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51n-0.13.0 (c (n "atsame51n") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1v9fkjd23m6iz237dwp1v8gi0k2y9q550ib0zyahl6zf7531m8k6") (f (quote (("rt" "cortex-m-rt/device"))))))

