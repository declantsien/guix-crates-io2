(define-module (crates-io at sa atsam4e8e-pac) #:use-module (crates-io))

(define-public crate-atsam4e8e-pac-0.1.5 (c (n "atsam4e8e-pac") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0vq0pqgvmjx21pmrsl6vvpgl2zlkivxbxzgg8g3wxfj91pb54y4q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.1.6 (c (n "atsam4e8e-pac") (v "0.1.6") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0zlb7i1xb2v566sg1m5jzwrs1alhdgshsd5xnrbyd9jhypmyxjm7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.1.7 (c (n "atsam4e8e-pac") (v "0.1.7") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ypl215y5zxrr8nj445jqcx6fd3xlgcl9b7h4bl8sfxzy29ac2l8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.1.8 (c (n "atsam4e8e-pac") (v "0.1.8") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0prkmr17dnn1fm176hkpyz9in8qa73bjlasw62a72ymmbw5j956q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.2.0 (c (n "atsam4e8e-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vxan4sp3w73g7572kbz1xv45kxj7mjylpcxrahf2vys1v9vwb18") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.2.1 (c (n "atsam4e8e-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vf8w0yd0a1nz2lcddxmmjna0jkzrmsvvqzk0lrik1bqd8ic9cw9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.2.2 (c (n "atsam4e8e-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "106sa3j4i6h87sgksy96xag1hkransf83wc0rawlphzjn2cpml1g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8e-pac-0.3.0 (c (n "atsam4e8e-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1hy7s98qv43cmrvs2860f2ln6q00h1f48jd9ma7sy70q2wyqhggj") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4e8e-pac-0.3.1 (c (n "atsam4e8e-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "194847na8z6zj7ga7z3w9kxaz9m43lwg8ccc7ii2xp1q2py8jf6p") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

