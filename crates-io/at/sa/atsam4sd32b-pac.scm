(define-module (crates-io at sa atsam4sd32b-pac) #:use-module (crates-io))

(define-public crate-atsam4sd32b-pac-0.1.1 (c (n "atsam4sd32b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1a2hpjqncvvqyanxaf9bcgp83prpd0mqzimjdr13w6rz6kxjjz2h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.1.2 (c (n "atsam4sd32b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1r7n9hm370w58gn6sjphi3glgbva50mxk5dr3y43ybixfq6mzq3g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.1.3 (c (n "atsam4sd32b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0vxpjhvajvyb7ihsig3inynsr9h0027ia109aq408yvgnaqvdns7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.2.0 (c (n "atsam4sd32b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0agwghz4ys1rdl9andjfj2nfbn9f04w2f3d3b9g5k2wh8i5pxj9b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.2.1 (c (n "atsam4sd32b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10s03kf8j8k8vbqww65cc0yr2a3jx22yh02qms2n44zgw8pc1vbx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.2.2 (c (n "atsam4sd32b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qggwipcw69kj9m83fmhz2r1rc35mc5y1ihrwwriaccy5d12bcfh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32b-pac-0.3.0 (c (n "atsam4sd32b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1rm2dv26hfdc1wkihlsxbci9pdpp2wq1bhwa3gppdgq9v23wv83q") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sd32b-pac-0.3.1 (c (n "atsam4sd32b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "112mfp7gls0d7f3ri883ihyk9mp8jsqnl0avlhyf3sgfh4b5gyak") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

