(define-module (crates-io at sa atsaml21g18a) #:use-module (crates-io))

(define-public crate-atsaml21g18a-0.1.1 (c (n "atsaml21g18a") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15bigvhbh19rplv7h7h25gr96kvwf2h48dlqrgl85yfyiph0avjj") (f (quote (("rt" "cortex-m-rt/device"))))))

