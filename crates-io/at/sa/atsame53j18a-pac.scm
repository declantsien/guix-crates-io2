(define-module (crates-io at sa atsame53j18a-pac) #:use-module (crates-io))

(define-public crate-atsame53j18a-pac-0.1.0 (c (n "atsame53j18a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gza45x5s77n648k7cdbn1dx60mc0m5378gvn6h4sk0gs6qm8wyw") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j18a-pac-0.2.0 (c (n "atsame53j18a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jfvsac0r02nax49w1pxk9lg8xhz0778wlpqlh6y1721nxxdickb") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j18a-pac-0.2.1 (c (n "atsame53j18a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ia66rhxngpqdajjxh0c8hslf30fmciqajmpx098qcgrc6bmp0g7") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

