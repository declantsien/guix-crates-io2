(define-module (crates-io at sa atsam4ls8a-pac) #:use-module (crates-io))

(define-public crate-atsam4ls8a-pac-0.1.0 (c (n "atsam4ls8a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0wvfqrfqzmnj9a8lqrhizczaj6vigmrl7pgbk1m1w52780n6fjd5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8a-pac-0.2.0 (c (n "atsam4ls8a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07r4r188b5m30hlpbxdxi0xsp3k11hqan96n7l9y9jgr488m4yhp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8a-pac-0.2.1 (c (n "atsam4ls8a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03g9smjbbha76ivfm4v5nbrki8bccv68zvkxdak1r2h926b7mf41") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8a-pac-0.2.2 (c (n "atsam4ls8a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wq995nbx2pwzy3666r53sg1fb9i5xvd17qznhwirgyvm4swljcn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8a-pac-0.3.0 (c (n "atsam4ls8a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wi99x1qi9hhx6a8xmq0hq1c0hiy91fp8ppc5b91gln9rvhg3p0m") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8a-pac-0.3.1 (c (n "atsam4ls8a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05vzfdnbwc5m2bhi83bi5vb34brlc1hqlsyzn1nn6md159lwgp19") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8a-pac-0.3.2 (c (n "atsam4ls8a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "04s4mz97fv6a75wlvr1qv6x40bmd7hsriwhr8i61qshljyhyjg4j") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

