(define-module (crates-io at sa atsam4lc2a-pac) #:use-module (crates-io))

(define-public crate-atsam4lc2a-pac-0.1.0 (c (n "atsam4lc2a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ic3xlmadf7c53ksz3vf8whh7l04sdcny7yf4ixdkxl0913vwvjc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2a-pac-0.2.0 (c (n "atsam4lc2a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ynxq5fx6nn3pydw1rs2xijy8hlqfmfmk04pc41vywdbx2pxflxj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2a-pac-0.2.1 (c (n "atsam4lc2a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z2aigba5l494j781r5pmh02crd5xjn7i4icncljx304a7f4hr4i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2a-pac-0.2.2 (c (n "atsam4lc2a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1z8lzgwznk2xa41gkflaav29ml29m8c92qn7cw3l9anh78l28pa2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2a-pac-0.3.0 (c (n "atsam4lc2a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kpd87g9mmri4hnaq0bjyg9lmxyng9b7f102c3lcxi57k07xb5ni") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2a-pac-0.3.1 (c (n "atsam4lc2a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16amjddyv0clgyakv11480992bgmixha3sp6d3fx3ixf8b43w5lg") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2a-pac-0.3.2 (c (n "atsam4lc2a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mjlgg8y2sfqv8gfwffi78nsmzmjqncdgrxfskaqxrzjancb5rnb") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

