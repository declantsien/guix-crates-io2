(define-module (crates-io at sa atsamd51j20a) #:use-module (crates-io))

(define-public crate-atsamd51j20a-0.4.0 (c (n "atsamd51j20a") (v "0.4.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0l1266n9qc20m9v2cnliaqwamnnfccvn1c0845jg2riayavhbb4g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j20a-0.5.0 (c (n "atsamd51j20a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1nrmis99g34mansj5ky0cqbsv0kbnl4y19xfjlh5gh3983v7489g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j20a-0.6.0 (c (n "atsamd51j20a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0l7xq4kk4inc3pw9528h14b0xi0fbyr1a504920d8dclj3m8zq73") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j20a-0.7.0 (c (n "atsamd51j20a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1nwcxh8q26n0pc6f343v604pi3jc66cz7a0n7s78sggxw7fh0dh3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j20a-0.7.1 (c (n "atsamd51j20a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "07326wmswgr20y11l1yxam6c9fbikr85297sw0b2yp9zb3kcn3y0") (f (quote (("rt" "cortex-m-rt/device"))))))

