(define-module (crates-io at sa atsame70j21-pac) #:use-module (crates-io))

(define-public crate-atsame70j21-pac-0.1.0 (c (n "atsame70j21-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "17s3w77ckwmjax25napvbvp5h3z45ps014l9fjpmj00b51idcl11") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21-pac-0.2.0 (c (n "atsame70j21-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18wj95z880p1vpipq6liyxcl4gjqgw7h1rgp5syi15nxi3fnjlgm") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21-pac-0.2.1 (c (n "atsame70j21-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0abi3h78il65f3pqlsajcb61dayn59yqbcx3bzialbink8x0z2f1") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21-pac-0.2.2 (c (n "atsame70j21-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ijf75l1crm5d7x23298py45w5zp9clg2p7p94vn8q85j09b0fdc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21-pac-0.3.0 (c (n "atsame70j21-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10kwfg15ap9126y8plzy177qvvbj94njr8a8c6r2w5746w5dpnxd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

