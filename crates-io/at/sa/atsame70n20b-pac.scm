(define-module (crates-io at sa atsame70n20b-pac) #:use-module (crates-io))

(define-public crate-atsame70n20b-pac-0.1.0 (c (n "atsame70n20b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1n9sa84zfzcinvjv32c8fzfnbzjbsq487881ar20d84jhw37jls7") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20b-pac-0.2.0 (c (n "atsame70n20b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x7j9c72p9l0gqm3r4cfpabnffsic3nbm12sy5ss0kqm30cv4wyg") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20b-pac-0.2.1 (c (n "atsame70n20b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02n7mbgvk06mmgbbhvs2m8q5i33gapzm5lqgb6i75fjyfzvn56fd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20b-pac-0.2.2 (c (n "atsame70n20b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15vh3r0wgvh8blanxsrc1hhn7xxsn33fgx4dmwnxyxzv8c78m816") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20b-pac-0.3.0 (c (n "atsame70n20b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ncxn4gkp6cavwv0vxn424i7ird3z02js141q2nywnbdpfaxfd36") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

