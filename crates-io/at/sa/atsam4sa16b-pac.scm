(define-module (crates-io at sa atsam4sa16b-pac) #:use-module (crates-io))

(define-public crate-atsam4sa16b-pac-0.1.1 (c (n "atsam4sa16b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pbjl61amgr3qnhvg44k8adbkjn184w3i9saw96lzjdvf9s1fr7l") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.1.2 (c (n "atsam4sa16b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rgdv66qppiggi72vcfir6ifjhmcplpzvgds07w5xwi3plw364hs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.1.3 (c (n "atsam4sa16b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1r2lhw0ncqz9wpqpgpjgqlyvviwmcx55r09ccl4xwnivahsxkvwk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.2.0 (c (n "atsam4sa16b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1p7l2h6if8r4h0rmisv9gh6nvm2wzwbr8w41ys1q0j3fm78853vv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.2.1 (c (n "atsam4sa16b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cniripdbph9x676h46xsqxbi1dszhhixqcy7bmnfqakjb25gy4q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.2.2 (c (n "atsam4sa16b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "09621nmrraafx1lawf1ypib6l9mi7plv7880k442angiabrkj3i0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16b-pac-0.3.0 (c (n "atsam4sa16b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wv27j4r56m4db0s8sllhy0anld2dsm61cn3ibqqflq60wflbys3") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sa16b-pac-0.3.1 (c (n "atsam4sa16b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "157zp93van465sy5qc0674qidqilyjiw0j8m4b8687g42bxx285f") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

