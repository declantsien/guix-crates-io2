(define-module (crates-io at sa atsams70n21) #:use-module (crates-io))

(define-public crate-atsams70n21-0.0.1 (c (n "atsams70n21") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0bq3416x69ra3j4fjvqy8bqaw04xmy086rnyl0s8p2xwcwrk917g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70n21-0.0.2 (c (n "atsams70n21") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1vn0m8vys9pzc9pz4r5hvg0rymjz3nqaffimp3ifrj1i8wmh49ml") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70n21-0.21.0 (c (n "atsams70n21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1336pvaamxhib9p4yny6j73vk0cgrfwrv2lqdmab1c6p7568gma2") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

