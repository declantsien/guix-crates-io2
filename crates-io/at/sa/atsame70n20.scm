(define-module (crates-io at sa atsame70n20) #:use-module (crates-io))

(define-public crate-atsame70n20-0.21.0 (c (n "atsame70n20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rvj62z96i1mhk5si6q9a4466l4ybqcql3jalqmn6v748zwg1iad") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

