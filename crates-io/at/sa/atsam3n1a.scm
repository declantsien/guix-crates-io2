(define-module (crates-io at sa atsam3n1a) #:use-module (crates-io))

(define-public crate-atsam3n1a-0.1.0 (c (n "atsam3n1a") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0nvd3fz2npffx0wg7rwhcq7hidg73fca0fns5jk69521p2ff2jfh") (f (quote (("rt" "cortex-m-rt/device"))))))

