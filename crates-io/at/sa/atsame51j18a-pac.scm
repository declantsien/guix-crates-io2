(define-module (crates-io at sa atsame51j18a-pac) #:use-module (crates-io))

(define-public crate-atsame51j18a-pac-0.1.0 (c (n "atsame51j18a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1p4ffgq9bfvmszpy03dvx0yd2ds0rcdj3xcpl2pby0izxg0mcir9") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j18a-pac-0.2.0 (c (n "atsame51j18a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qgv84in7ln48k8nfbsnldvsx387x36xjz6x071aajnivp84fri6") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j18a-pac-0.2.1 (c (n "atsame51j18a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17qbq0cpbr7gdgki1zk9vsq7jwys85c4bhv6imgljfhxbiv4j0a4") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

