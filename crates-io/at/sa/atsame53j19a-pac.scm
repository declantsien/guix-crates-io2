(define-module (crates-io at sa atsame53j19a-pac) #:use-module (crates-io))

(define-public crate-atsame53j19a-pac-0.1.0 (c (n "atsame53j19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03lglnl6lmfjrblqajmwcmgbw9qmn0bs92k8nd3hx7g7fllr5kzy") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j19a-pac-0.2.0 (c (n "atsame53j19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "022lax4sm6335lbd4wb53ydqyb952aw7pf26hv8j0pmbzkcz8vl6") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j19a-pac-0.2.1 (c (n "atsame53j19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cni23nbcrlnvyc2cchnxqf2mdaxsv3widfmkiy58fh3qk5yyn12") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

