(define-module (crates-io at sa atsaml21j18a) #:use-module (crates-io))

(define-public crate-atsaml21j18a-0.1.1 (c (n "atsaml21j18a") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0lzr4dn2wn479h2a9nicnwafi0n33wps5sd8iqxxs04769yqg1xc") (f (quote (("rt" "cortex-m-rt/device"))))))

