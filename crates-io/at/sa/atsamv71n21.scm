(define-module (crates-io at sa atsamv71n21) #:use-module (crates-io))

(define-public crate-atsamv71n21-0.21.0 (c (n "atsamv71n21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rljjfp1xdczlx4sw8xc8bibd3n8rlccwmdzayragv6j6zy6m5ws") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

