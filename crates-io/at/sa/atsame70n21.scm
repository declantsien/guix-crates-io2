(define-module (crates-io at sa atsame70n21) #:use-module (crates-io))

(define-public crate-atsame70n21-0.21.0 (c (n "atsame70n21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1m361j6qpvphgcz9w62qic868bn64cyn2l50n5xaw4449mb175ba") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

