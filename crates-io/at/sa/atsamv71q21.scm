(define-module (crates-io at sa atsamv71q21) #:use-module (crates-io))

(define-public crate-atsamv71q21-0.2.0 (c (n "atsamv71q21") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0c82fr8k4d728sz1sb97a7wyd1xvfvl9r2md0fygvpgixr2wavgw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamv71q21-0.21.0 (c (n "atsamv71q21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lac6qazkfyv8sxlhmqcm19yg67zf51q8adx4rdbbdh8kyc1qcaz") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

