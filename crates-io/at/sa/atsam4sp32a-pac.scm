(define-module (crates-io at sa atsam4sp32a-pac) #:use-module (crates-io))

(define-public crate-atsam4sp32a-pac-0.1.0 (c (n "atsam4sp32a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1kj7mwf18bk7w9vz0jdg0jl1spj37756lglz2vmsribk5qp67q8b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sp32a-pac-0.2.0 (c (n "atsam4sp32a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00qk1nf3a2vk9gbphlf1wijq2qfnnhp775j78rcc2pv7gm4m1c9n") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sp32a-pac-0.2.1 (c (n "atsam4sp32a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zq7x81kmz5qzqhc8i3ww7g191maiwcz35d8r5g0ka5l5vblyxps") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sp32a-pac-0.2.2 (c (n "atsam4sp32a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0adbsgwh9hslg3ib23i5pqbz6fsdicvii3i7djvmn66xhcdrk2q0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sp32a-pac-0.3.0 (c (n "atsam4sp32a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0x8n6gy11d7igc9v3slnj27nczj4a2bw4vcv9yg5rfd7vmgwksf0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sp32a-pac-0.3.1 (c (n "atsam4sp32a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1an72wcj2dqpfp9ll2vwavp3r9z9ic5c5vww59scm7rkfn71gj9l") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

