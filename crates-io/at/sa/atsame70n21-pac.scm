(define-module (crates-io at sa atsame70n21-pac) #:use-module (crates-io))

(define-public crate-atsame70n21-pac-0.1.0 (c (n "atsame70n21-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1kqpz5i6gmxcaq7i3m163akkq2lgyim87h0pm07nmhx4jziqqq6w") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21-pac-0.2.0 (c (n "atsame70n21-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0yli8ivmwzj11h5pdgylll784j6zl56jbn8nhrjpjcj1w4h4q7xr") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21-pac-0.2.1 (c (n "atsame70n21-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0jkzkvn3g1qb524xjqykyzvmakqz58lkwi9rj94sbhpdf8b8kpx8") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21-pac-0.2.2 (c (n "atsame70n21-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15sl94ynx0jzjkwwbafx0xpcv8qwdgcw44bmhdrps31kbzx5fjpx") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21-pac-0.3.0 (c (n "atsame70n21-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06595j0zi3gpyqa3zqhrn4s84lcyd0gimqdbsrzzkcgz8xfjzdaf") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

