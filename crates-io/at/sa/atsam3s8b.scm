(define-module (crates-io at sa atsam3s8b) #:use-module (crates-io))

(define-public crate-atsam3s8b-0.1.0 (c (n "atsam3s8b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1if21zflzc2pp6l5jx1nx9ml806chyv81ngk09kbfyl1ylp4s4pd") (f (quote (("rt" "cortex-m-rt/device"))))))

