(define-module (crates-io at sa atsam4sd16c-pac) #:use-module (crates-io))

(define-public crate-atsam4sd16c-pac-0.1.1 (c (n "atsam4sd16c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0caca1a5386r53myvw69aj7814a1gbzwqxn21g8wck3ppkv6zwnr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.1.2 (c (n "atsam4sd16c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0z8gappx467g5v30h3c8kkj2f68a9zsvg14k8mwmkni63kkkz1mw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.1.3 (c (n "atsam4sd16c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0y9qlrvv9hpdvx5b71icx640gxfzndh3acjxq2r313zzw1vzpc0z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.2.0 (c (n "atsam4sd16c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19c70n8w6adqwcp0wylzijyrrwnfy6jjcvvc34dhp8a5as9mkxlc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.2.1 (c (n "atsam4sd16c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z7ck1k527nc48n36x241a256z0jlxb0y6q17cyb4yrkvcdqkavs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.2.2 (c (n "atsam4sd16c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0d4srbsh6cvdrx51iw4nk339v4v7hamsyxj3w0b1p8bjksjc9iwd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd16c-pac-0.3.0 (c (n "atsam4sd16c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0dfvfal67k0qmqqrfskyfkrvnx8g8dswznazrfb0g0130ga7mi2i") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sd16c-pac-0.3.1 (c (n "atsam4sd16c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0d2fiyxdplhgs8aa16kz9fp7qw9ckg9nc4x6zlld1h3xx7xginrh") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

