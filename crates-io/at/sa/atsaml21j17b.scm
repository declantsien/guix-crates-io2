(define-module (crates-io at sa atsaml21j17b) #:use-module (crates-io))

(define-public crate-atsaml21j17b-0.1.1 (c (n "atsaml21j17b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0h6kk7znfi87rgs7ipakmkwfsh1k1k7kajc0x6fs6khyv45ds541") (f (quote (("rt" "cortex-m-rt/device"))))))

