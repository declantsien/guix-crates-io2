(define-module (crates-io at sa atsam4ls4b-pac) #:use-module (crates-io))

(define-public crate-atsam4ls4b-pac-0.1.0 (c (n "atsam4ls4b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0z7bng98fik03w7y885p9dws5vb5kdcqkz56nr9nf8mg4k8mczqi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4b-pac-0.2.0 (c (n "atsam4ls4b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gdmd8214z7y741x9i98snssx2srl73bclcmgkdxzj03dfr9f0nb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4b-pac-0.2.1 (c (n "atsam4ls4b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15738n29v1pnwxg31h5cn5sfs52a0lnk61agd01s55sgibvs39vj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4b-pac-0.2.2 (c (n "atsam4ls4b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0sgbx9qpw87p9z3s4dfpsrnpblk31ywgz9fgf8kp36lsdpk3hs9g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4b-pac-0.3.0 (c (n "atsam4ls4b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0vplanwzfz351mwn8ja2mj52ma3arpywrxhwcv49spc35s3wms15") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4b-pac-0.3.1 (c (n "atsam4ls4b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01p9fv0fqjmz1py69cl049vwib7457qm7nnwkrfam4rmzif8pcma") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4b-pac-0.3.2 (c (n "atsam4ls4b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1i5wrmkwm7y1xyxkzn5zawnfplwwfmlfrwq3zs0slbdds5d1j2qz") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

