(define-module (crates-io at sa atsam3x8h) #:use-module (crates-io))

(define-public crate-atsam3x8h-0.1.0 (c (n "atsam3x8h") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1jixxa86m5xzrwlwsa31s95gr719lllw84zdrwvbjb55s5iba5n1") (f (quote (("rt" "cortex-m-rt/device"))))))

