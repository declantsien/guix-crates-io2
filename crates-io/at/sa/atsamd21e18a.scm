(define-module (crates-io at sa atsamd21e18a) #:use-module (crates-io))

(define-public crate-atsamd21e18a-0.2.0 (c (n "atsamd21e18a") (v "0.2.0") (d (list (d (n "bare-metal") (r "~0.1") (d #t) (k 0)) (d (n "cortex-m") (r "~0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "018iksf6rkyj8rlnl3g048r6d9ifb28q7x4i4jxyrjzlfsnfrvjw") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-atsamd21e18a-0.3.0 (c (n "atsamd21e18a") (v "0.3.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0hkda4mm1kvhb1hjw9wh3lgisig7yw5hi0v81is7ih1gm5rrw3jp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.3.1 (c (n "atsamd21e18a") (v "0.3.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1k69p0n9y4cdczlddwfkikiaiq8x99mww550k2njdgp96ahcwhnp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.4.0 (c (n "atsamd21e18a") (v "0.4.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "15xqhbirp7r3jh8xslwvzq5bg0pm9i0gaw4m5sdd138i1niqg5qk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.5.0 (c (n "atsamd21e18a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "09pk0h4rwjbkcv219gdjrvr25n8h5n168mhnj527w9qsb0d96kxn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.6.0 (c (n "atsamd21e18a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xrfb7r6k69kcvxiph1q1gc200yvhzsk9kysjiw8x6939pm244zs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.7.0 (c (n "atsamd21e18a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0yj9gxpil75p8fd9g4vy2765h95a47hcrw1q41pir6vdfnkjgbzr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21e18a-0.7.1 (c (n "atsamd21e18a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1gdx29qvb4iknphl11ldjxy7skzxq8kal2f26zhdsyf9wbsglhdm") (f (quote (("rt" "cortex-m-rt/device"))))))

