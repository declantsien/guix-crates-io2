(define-module (crates-io at sa atsaml21e18b) #:use-module (crates-io))

(define-public crate-atsaml21e18b-0.1.1 (c (n "atsaml21e18b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0f0kz45ajwdqix0xbpawpl8bbh0rg3dz2ak12a5rq9b1i5w6a0x4") (f (quote (("rt" "cortex-m-rt/device"))))))

