(define-module (crates-io at sa atsam4ls4c-pac) #:use-module (crates-io))

(define-public crate-atsam4ls4c-pac-0.1.0 (c (n "atsam4ls4c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1i9vmhg9lyxk8ylkmjwv2ch4hpv3vq8lb9dskijp4apgbdc3kqc0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4c-pac-0.2.0 (c (n "atsam4ls4c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08lw5s9fb7lrkvy0ax7rkj68qal15b81w69g2l53rwga9m1nl83g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4c-pac-0.2.1 (c (n "atsam4ls4c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1i7mffjgp1gvcvbw48qjfkriqdv8pn46dw7jq4a3z15w6897dmz4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4c-pac-0.2.2 (c (n "atsam4ls4c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0knm3rxkv3qzqr0sb49jq9xp3lr5xwmfl9bn04q4bdy5bwfkdbwx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4c-pac-0.3.0 (c (n "atsam4ls4c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mpydwyxxjq0mf6gagi1j8balwa0pic90mmrgwlv6wdnyj8cajvv") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4c-pac-0.3.1 (c (n "atsam4ls4c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nnkmaglrqmlbdsp2yaxlm7qkdlaylnxbd6dz74mbijz1xg3491m") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4c-pac-0.3.2 (c (n "atsam4ls4c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s4kfrkwnzgn8gq1k1dl8fnl8dljsrrmmnxzjn35pzhgbd5z2phv") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

