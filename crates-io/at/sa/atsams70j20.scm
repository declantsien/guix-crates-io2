(define-module (crates-io at sa atsams70j20) #:use-module (crates-io))

(define-public crate-atsams70j20-0.0.1 (c (n "atsams70j20") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "038pb227vwz8ra8rjv3kxrqx42v0mzhcyxsd75rbzzzkc8kpcpg0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j20-0.0.2 (c (n "atsams70j20") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1iv1ixw7rhd5bj9q78vgk626raw80pcv61pxzdc8kf3pqyif3ir3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j20-0.21.0 (c (n "atsams70j20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rzh96j3rlvqdgjqf39dhysp6cxclwzln92ndlby352ygdw0gs68") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

