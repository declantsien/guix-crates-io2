(define-module (crates-io at sa atsam4s16c-pac) #:use-module (crates-io))

(define-public crate-atsam4s16c-pac-0.1.0 (c (n "atsam4s16c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1b3736bcdjrm75514gx7xhvkhh9vkfcga0a8s4pzw4p3q11ibkik") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16c-pac-0.1.1 (c (n "atsam4s16c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qgan6jin0z4fkc3adzgqk8gk2khjfkfrpipdggj2bcb7rhbff7g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16c-pac-0.2.0 (c (n "atsam4s16c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0p74c1i6x1h07xfrlw3x35fjrlni38bg04471zlv1fnsxvia49xg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16c-pac-0.2.1 (c (n "atsam4s16c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0j1a755s6j2dlip7x4h6by196djgnry10wgz3k0wr39j88vhcirs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16c-pac-0.2.2 (c (n "atsam4s16c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wxmyxhc5jvsr92vgaqk0fgcgd09ipm4x9pahz9srqncdsdblcb9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16c-pac-0.3.0 (c (n "atsam4s16c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jprcr0zwxrzwidcz8zjspij6z7w5qvn9rnn94fnya87q1lj75zw") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s16c-pac-0.3.1 (c (n "atsam4s16c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z7cfqbif23dsniaxskgsa4mmiqdvqrxg27ki9gfkwds0kj7v820") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

