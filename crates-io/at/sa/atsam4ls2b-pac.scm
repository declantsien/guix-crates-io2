(define-module (crates-io at sa atsam4ls2b-pac) #:use-module (crates-io))

(define-public crate-atsam4ls2b-pac-0.1.0 (c (n "atsam4ls2b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1g2pyqkgkipdv8fkf9swxiwhq0w4sbzwcgm6b38gw25was4xnfaq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2b-pac-0.2.0 (c (n "atsam4ls2b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12v7ii8xab9gymz3mvian0w0z36iqvplm9qfhy774l1m21qhh826") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2b-pac-0.2.1 (c (n "atsam4ls2b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xiyzba0gcblw7djwrrqrn4mjxpydln66b8fanb4dg2aliwgv2xw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2b-pac-0.2.2 (c (n "atsam4ls2b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11lywsfvlv7z9qiiby6z5hm6ks1w0y5b015rxyly94adbqbb3cnv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2b-pac-0.3.0 (c (n "atsam4ls2b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0w5xmlapiipjgkb3fv0yc2v1nbi0y8w8r4ym0v55sjfh9bgdqpp6") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2b-pac-0.3.1 (c (n "atsam4ls2b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02bwfidrznisx2z81hcfcarm75pxcm0q9yaikk7g47ska5brqw5j") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2b-pac-0.3.2 (c (n "atsam4ls2b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03q9vwkicaglglimh0qk0l7gz6vcfn2gzpwr7cbmckk48s8hv26n") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

