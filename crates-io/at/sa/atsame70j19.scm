(define-module (crates-io at sa atsame70j19) #:use-module (crates-io))

(define-public crate-atsame70j19-0.21.0 (c (n "atsame70j19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0yq60igyp1shark61xw7cl5sxglix91w046x29y07rfv9rkvx6rp") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

