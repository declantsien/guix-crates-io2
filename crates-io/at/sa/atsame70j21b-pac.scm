(define-module (crates-io at sa atsame70j21b-pac) #:use-module (crates-io))

(define-public crate-atsame70j21b-pac-0.1.0 (c (n "atsame70j21b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16a31965zaffj5x8i1jb97cwqj29i71spx8jqy11lf44zk8iwj8p") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21b-pac-0.2.0 (c (n "atsame70j21b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1np1hvivpdx5as3hrfa4c9qfi551zllyvm0w6099drc7w54smr4p") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21b-pac-0.2.1 (c (n "atsame70j21b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15a7i6x2snnh2b8z7n4sdqdzzampr9zzvr8g89a1rk1519z6gfmf") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21b-pac-0.2.2 (c (n "atsame70j21b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05jlblsypp7y1hrydpg13gyrl52ys2g0plyplwz34f1l8pminsdi") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j21b-pac-0.3.0 (c (n "atsame70j21b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0hzmkvg5d64cmz362kgf9x60hcsmm7idlnszh7mbwb0hl7zp4i07") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

