(define-module (crates-io at sa atsamd21j) #:use-module (crates-io))

(define-public crate-atsamd21j-0.8.0 (c (n "atsamd21j") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "096d1mp2rnypc73snb68sd5fn1wp2q5c4y0vm7cwfk812mhrrvc6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j-0.9.0 (c (n "atsamd21j") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1bgr4a3zpd49qri6rdvsiwdrjkflbcnf4q43l5i3sxivs6f70whs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j-0.10.0 (c (n "atsamd21j") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qbsaq700qca6r1xm6z9hr3i4slxarpj779jivpl9wxa77ymbcw8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j-0.11.0 (c (n "atsamd21j") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1w7w19fk7lq3bif37xm3v7igxw0r6z4nzxgl85bbmg8hv840dym3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j-0.12.0 (c (n "atsamd21j") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1x15sbpbwcsz9j729chl8xkz0vrd951vpshwznnkhvsxsfp3lwd5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j-0.13.0 (c (n "atsamd21j") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0iri013cg4xcgd7qwsas4wbax7ic47x0v9jkc78vya4281891wr6") (f (quote (("rt" "cortex-m-rt/device"))))))

