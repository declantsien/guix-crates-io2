(define-module (crates-io at sa atsam3u1c) #:use-module (crates-io))

(define-public crate-atsam3u1c-0.1.0 (c (n "atsam3u1c") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0zgs9i9d17xajy9lxsy03yz67cszaphxsix8pjd2kkr2v97npcn7") (f (quote (("rt" "cortex-m-rt/device"))))))

