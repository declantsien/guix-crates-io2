(define-module (crates-io at sa atsamv71q19) #:use-module (crates-io))

(define-public crate-atsamv71q19-0.21.0 (c (n "atsamv71q19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "003svaxg0dlsrl5yj5lgbz58g1iq1lq5adwlh6g7yh32w6yfhdlx") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

