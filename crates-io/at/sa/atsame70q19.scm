(define-module (crates-io at sa atsame70q19) #:use-module (crates-io))

(define-public crate-atsame70q19-0.21.0 (c (n "atsame70q19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0w2ck72ghr9qfr31qmgdgn6cwh68j673b8blqx516pq76y7dfgpg") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

