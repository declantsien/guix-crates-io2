(define-module (crates-io at sa atsam3n0b) #:use-module (crates-io))

(define-public crate-atsam3n0b-0.1.0 (c (n "atsam3n0b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15bhbq5rpyz0cr4v6w7va5a1wjnba0yilz9qd91vn8qghpayv7aw") (f (quote (("rt" "cortex-m-rt/device"))))))

