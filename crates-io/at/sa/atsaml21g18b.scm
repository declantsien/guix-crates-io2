(define-module (crates-io at sa atsaml21g18b) #:use-module (crates-io))

(define-public crate-atsaml21g18b-0.1.1 (c (n "atsaml21g18b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "05dbp6v9a41c5s1gza702xc4b1r86dgnlk3imdg88aaa18rnnr2m") (f (quote (("rt" "cortex-m-rt/device"))))))

