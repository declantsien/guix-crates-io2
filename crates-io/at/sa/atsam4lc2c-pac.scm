(define-module (crates-io at sa atsam4lc2c-pac) #:use-module (crates-io))

(define-public crate-atsam4lc2c-pac-0.1.0 (c (n "atsam4lc2c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "17z5s4w0ihs3xh0rbqiv0r8kbwms1r7v6kaqns8pin416kbvcb5c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2c-pac-0.2.0 (c (n "atsam4lc2c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1f67r2ssmcq8ghkglkmbajfww776yj2w4w4j00k6g95zr85axhg2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2c-pac-0.2.1 (c (n "atsam4lc2c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0hxgv8h7jl08xxfq415gpffnhrfplmc81p9i387nnnbsj75fp93r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2c-pac-0.2.2 (c (n "atsam4lc2c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m9gdl14nqws2h1k13x66jpa6gmxszb7cak0pms35ybddrpjwk24") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2c-pac-0.3.0 (c (n "atsam4lc2c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1sjln3grr43cxa0dpkg2yyczk2x34izwk7va5yvbmqyqfmn485f0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2c-pac-0.3.1 (c (n "atsam4lc2c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1hqw6b2rssc3v44x7zi1sjk36zcb35swg26q9h6a1bprcl029c7q") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2c-pac-0.3.2 (c (n "atsam4lc2c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12qy0fh36769nq3m9gg9h1bwgnxd4c6clfqv3dr09xcx3m3l4vpn") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

