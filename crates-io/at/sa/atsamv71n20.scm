(define-module (crates-io at sa atsamv71n20) #:use-module (crates-io))

(define-public crate-atsamv71n20-0.21.0 (c (n "atsamv71n20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16ibnj1svwdj00zn237g1k084x7ks31nhaki9bwgrrqa6abffcd3") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

