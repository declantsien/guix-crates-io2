(define-module (crates-io at sa atsam3n2b) #:use-module (crates-io))

(define-public crate-atsam3n2b-0.1.0 (c (n "atsam3n2b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "119vhlsxqmhcg8k6wqg6l5pxwcfjkvcgfhv0ilmnxsbyl3s5gsh5") (f (quote (("rt" "cortex-m-rt/device"))))))

