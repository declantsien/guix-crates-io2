(define-module (crates-io at sa atsamda1e14b) #:use-module (crates-io))

(define-public crate-atsamda1e14b-0.1.0 (c (n "atsamda1e14b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09dzjxlfc0hgjjxdfbbbpy0j0rsmnaam9flvc4x2ficrigc45dpb") (f (quote (("rt" "cortex-m-rt/device"))))))

