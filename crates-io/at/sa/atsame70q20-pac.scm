(define-module (crates-io at sa atsame70q20-pac) #:use-module (crates-io))

(define-public crate-atsame70q20-pac-0.1.0 (c (n "atsame70q20-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0bg358xpb7pazpy142qjgz4a4ad1rqqdqdn0rvazmzx2x47h1s98") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20-pac-0.2.0 (c (n "atsame70q20-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1afqjp92l5k23lyrvn87jmgjva707iyj71xv9iymm6806r56ba4d") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20-pac-0.2.1 (c (n "atsame70q20-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1y9y4zhdr4hd70xb76ip8k5fjh52xxqadn8fs76lsgq4cdvc9zfr") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20-pac-0.2.2 (c (n "atsame70q20-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qln1fhjlpd0f93w5m1l3pyps6xfr79plxgcpg0y2zmxsmixkd7d") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q20-pac-0.3.0 (c (n "atsame70q20-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1g6kyk1iibxal1yl22acsdiphi9vvy8jl4sz13rlx06lcvp5f4ll") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

