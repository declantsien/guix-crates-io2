(define-module (crates-io at sa atsam4sa16c-pac) #:use-module (crates-io))

(define-public crate-atsam4sa16c-pac-0.1.1 (c (n "atsam4sa16c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16gbfmpwqqpvfds9p8rqrazm0fz48a0plidydffvfl8j006ghwgj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.1.2 (c (n "atsam4sa16c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0hicsvxnq7vlykixvgdlyi9fa1z03zw2jm6rhf1d04zhaf54wzwk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.1.3 (c (n "atsam4sa16c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19pvgflgyyxadm02mkfgl37db0m9d9lxhh300h0hfd3rc9xgl0nk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.2.0 (c (n "atsam4sa16c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15lijwywngxxvrjdr78sahqhyj3zdpg01qn2j1lhapbn8b6ax2zq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.2.1 (c (n "atsam4sa16c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10lb0f5ck05pdkds62aad3bgf0gbdm3s9qdg06cpzjlsal9yzsm7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.2.2 (c (n "atsam4sa16c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s62kpfy4cs7sirh5dq35769rssj334s9avd6yiqj4yg401h5sid") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sa16c-pac-0.3.0 (c (n "atsam4sa16c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "07v6c0jca92nzrxsfyxxwjxqnrnjwwa71djphakcfmcj015a8fpj") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sa16c-pac-0.3.1 (c (n "atsam4sa16c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xwcq03vgb3b203bsg309zqrpwx7lsg3a7d3vd611rl48gzxpz42") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

