(define-module (crates-io at sa atsamd51n) #:use-module (crates-io))

(define-public crate-atsamd51n-0.8.0 (c (n "atsamd51n") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xcmb9j92k7lr6c62b3fqwlnki2n3d2ycppvdvvw7q51ny307jkk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n-0.9.0 (c (n "atsamd51n") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "119a3xjbb99qxgqknjdzw46s6dhijcn2k77xqdqha440wf671xgq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n-0.10.0 (c (n "atsamd51n") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lc4zzmaxv0qm2bssw7xhs69nmfasg9x6cgg898mxkqqwmwzl6n8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n-0.11.0 (c (n "atsamd51n") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pw1v1m72whqcz89dl9y3v71fmr6fpw323z39961rgjx0r25nf97") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n-0.12.0 (c (n "atsamd51n") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "01kqjc0zydknqd9x72wr9kxrxv7fbvnkahixzirjf5jaydak798h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n-0.13.0 (c (n "atsamd51n") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0lwlafjgf1cng1afl1x5m2n318k5fxyqsbx67alfqv5kqzvvsvgy") (f (quote (("rt" "cortex-m-rt/device"))))))

