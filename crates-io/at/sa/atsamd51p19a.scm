(define-module (crates-io at sa atsamd51p19a) #:use-module (crates-io))

(define-public crate-atsamd51p19a-0.7.0 (c (n "atsamd51p19a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1hd2qh1zz2kaqs8p3j55vrsc0r9pjadgm6p26ngf9n03rdg7wi6g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p19a-0.7.1 (c (n "atsamd51p19a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1d5gysagzsyf3xi3bcmpvchfbyh68ycybxr0qwbwhilhcfwzgi46") (f (quote (("rt" "cortex-m-rt/device"))))))

