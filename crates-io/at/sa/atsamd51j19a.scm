(define-module (crates-io at sa atsamd51j19a) #:use-module (crates-io))

(define-public crate-atsamd51j19a-0.4.0 (c (n "atsamd51j19a") (v "0.4.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xf1imawh550s8dn7yr5kh6k8hfjvvacs6irxz7q9s7lkimv350b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j19a-0.5.0 (c (n "atsamd51j19a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1jf2xghi5v8ldbb9q847zk9rzyayxmjyc8msvfcldna2n0v34gzp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j19a-0.6.0 (c (n "atsamd51j19a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0bd81g02w0l8mxfb817kp4lw6xi7mzcgvzh97259p0p6bcnf06s5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j19a-0.7.0 (c (n "atsamd51j19a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0nx040nd9pvn7jq6fy3jhqp38hwzk9z0a7hhmgbnzfrhfrwhywgk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j19a-0.7.1 (c (n "atsamd51j19a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "044zmq8bps4xnswsy5ly3vv2r6pl44srf2gf6xhp44af4andvyrm") (f (quote (("rt" "cortex-m-rt/device"))))))

