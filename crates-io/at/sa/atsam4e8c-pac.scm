(define-module (crates-io at sa atsam4e8c-pac) #:use-module (crates-io))

(define-public crate-atsam4e8c-pac-0.1.4 (c (n "atsam4e8c-pac") (v "0.1.4") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1y2fsfbni4dk5y7aa5zn3ac9a613fr9pb9mn290680iwwpmvrkv3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.1.5 (c (n "atsam4e8c-pac") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ri8plkl30h0jn99p3lrsw02q4gimi3pzk1ai1nfv4dxnm6zrzmr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.1.6 (c (n "atsam4e8c-pac") (v "0.1.6") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0nby5l25lcqpnzpdw42dzfwiskjir4zij55y7wp5k3rfk04gdwgk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.1.7 (c (n "atsam4e8c-pac") (v "0.1.7") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1xzq8xlbl2avmxbvwhy78wzpb402vphhpbr62jjrykkz8pzgrlgz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.1.8 (c (n "atsam4e8c-pac") (v "0.1.8") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1dxskfrb0b1p3xj4xr1bjd61d8hcl5s446r9l570whq7fks763ps") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.2.0 (c (n "atsam4e8c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gnr4n1kiblq9pnk1hk24ay9nsy67rlhlq41ihfiwalsaf18vf3v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.2.1 (c (n "atsam4e8c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mb0kncj44ja836n009rib69wp529s79s9ibqps1xapy0x0jzmdd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.2.2 (c (n "atsam4e8c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gs5hkmaxx9q3sjgfwhmfdvx2xpyk7d2g8jidiyc3wa8234sq4yc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e8c-pac-0.3.0 (c (n "atsam4e8c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bp7m71a7pn4pqy5gwhylriyh6v76d5y9v7013x04yfprhz43f1f") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4e8c-pac-0.3.1 (c (n "atsam4e8c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gcnsdzmnvvyj5ah3zajp3xwjf51sv8q08chn1rqpcd599gdvpjn") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

