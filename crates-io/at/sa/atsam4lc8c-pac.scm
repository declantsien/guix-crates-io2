(define-module (crates-io at sa atsam4lc8c-pac) #:use-module (crates-io))

(define-public crate-atsam4lc8c-pac-0.1.0 (c (n "atsam4lc8c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01yvcr8iczvkyd5hj5pq4mgr15pdllxdxs7323nq7dmn217smqv3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8c-pac-0.2.0 (c (n "atsam4lc8c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ri2bp6j1rr1pg6qqjmrx1bz2zdr8djrxz8vpgh68bxqw45ci8dh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8c-pac-0.2.1 (c (n "atsam4lc8c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k8fk5xw5apa74w65513v57cg0kvd2nlp4s6vh5izgh4lr4hkwxi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8c-pac-0.2.2 (c (n "atsam4lc8c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zvdxlbqq4w0rk8ag2zdpkh235yli05xnqp6hc9sfwxf5ccz4hqy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8c-pac-0.3.0 (c (n "atsam4lc8c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "169qyzphrlqbxng4j2k5khw1srjlv3kc9n32pdbyvsn93yqfv0dy") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8c-pac-0.3.1 (c (n "atsam4lc8c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xax1g7jp0ms8hzxriryd52khal8c6czz47kc5frb161816q6q3s") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8c-pac-0.3.2 (c (n "atsam4lc8c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0d3f3lr90j4iadim1ph7x07darxna0nhy77pcqki6qhixpi8dhpp") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

