(define-module (crates-io at sa atsame70j19-pac) #:use-module (crates-io))

(define-public crate-atsame70j19-pac-0.1.0 (c (n "atsame70j19-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1rvjyaas3zl9sy58mvw7q5fzkysj8jsc1q589v8mgymwpyc73g3y") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19-pac-0.2.0 (c (n "atsame70j19-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00bxb6ckqxick9sgrbixv4j3gaxs3jbbldh1rps9n3z1c85jkyhi") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19-pac-0.2.1 (c (n "atsame70j19-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1h4zanjjv8nhq3bczac3hxjdiaq8v84xs2jr1xwg0zs1702blwkv") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19-pac-0.2.2 (c (n "atsame70j19-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0szlyp6ai9h7il8z72d0zndfd0byzx6vnypdwfqwwxi1hw0ixdcq") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19-pac-0.3.0 (c (n "atsame70j19-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vkz1cgsmkh53ihp4mqn36d4cp7j28np0bhrsmmfib6qrvlnkma4") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

