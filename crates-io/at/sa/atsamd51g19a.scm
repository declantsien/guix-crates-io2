(define-module (crates-io at sa atsamd51g19a) #:use-module (crates-io))

(define-public crate-atsamd51g19a-0.4.0 (c (n "atsamd51g19a") (v "0.4.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "11vdvs1aby70a6nqbiajm3qf25l7j1rvam38xwp3p7qpx8mgjrpx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g19a-0.5.0 (c (n "atsamd51g19a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1kxsc88hxc9py8gnwi1gjz31m482jvz4hczay7lgmf2gdyjf14cv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g19a-0.6.0 (c (n "atsamd51g19a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0a8nc9vsx0f0ixpz6zgkrsmqdzc7gg5v3rihf945kc9w61d9wfcs") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g19a-0.7.0 (c (n "atsamd51g19a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0k4jfzf603mxbj3hilq9rjrvdm0f7mhwgd5h6ic1zk877yand70f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51g19a-0.7.1 (c (n "atsamd51g19a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "14g900kliw2bvx9j069yy9sxn6fhdzv9350py8xll07dkl024c7x") (f (quote (("rt" "cortex-m-rt/device"))))))

