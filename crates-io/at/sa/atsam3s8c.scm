(define-module (crates-io at sa atsam3s8c) #:use-module (crates-io))

(define-public crate-atsam3s8c-0.1.0 (c (n "atsam3s8c") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kvfiv2r3xw4yc3awrc1x8a5j5h5pgbwraqgc8150a818gfk5iaq") (f (quote (("rt" "cortex-m-rt/device"))))))

