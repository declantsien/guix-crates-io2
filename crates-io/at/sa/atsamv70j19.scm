(define-module (crates-io at sa atsamv70j19) #:use-module (crates-io))

(define-public crate-atsamv70j19-0.21.0 (c (n "atsamv70j19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1w80d5qmbmwq9i2m389cnzzqjjvmsccsa967zsabyxi2plbp3ycm") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

