(define-module (crates-io at sa atsam3s1a) #:use-module (crates-io))

(define-public crate-atsam3s1a-0.1.0 (c (n "atsam3s1a") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "104g07qapzizc7byq77ndarckl4kwq6m3ic7qvgn3g7q7ffsb2a3") (f (quote (("rt" "cortex-m-rt/device"))))))

