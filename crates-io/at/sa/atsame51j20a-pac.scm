(define-module (crates-io at sa atsame51j20a-pac) #:use-module (crates-io))

(define-public crate-atsame51j20a-pac-0.1.0 (c (n "atsame51j20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0vhh6l6fb412z1vahd29crzw801368fq0407fj86j1am4md62c5f") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j20a-pac-0.2.0 (c (n "atsame51j20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1iqvh168qvrfmzs1dlplsh0bk4n20zagjlfqpqpbzfm7d0k3hil6") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j20a-pac-0.2.1 (c (n "atsame51j20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1s99jyqqklw7h7i9jvi2fdfgvyy3pychk998lcamvy44xr5yrf14") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

