(define-module (crates-io at sa atsame54n20a-pac) #:use-module (crates-io))

(define-public crate-atsame54n20a-pac-0.1.0 (c (n "atsame54n20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0r6i48l6569fjbg8gl4982sdyv7p3k807aambqzgjfzp2hccx393") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54n20a-pac-0.2.0 (c (n "atsame54n20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jh90fg117n4r1x3l41dpz0ca9z7q07glc7vvw6lw6kvhy84mxvc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54n20a-pac-0.2.1 (c (n "atsame54n20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14x322pk174m6qyvx7mfpljdwykwizn0qwa07vb9anlxdbqnw0fc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

