(define-module (crates-io at sa atsam4n8a-pac) #:use-module (crates-io))

(define-public crate-atsam4n8a-pac-0.1.0 (c (n "atsam4n8a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lf72zrl7fwf5hgd8z4h13lm9pa41qim5bsxx0m13cm4rkjgm0vg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8a-pac-0.2.0 (c (n "atsam4n8a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0dkhypa68ng88hyyhiqcq14wqg79qyy7mrjz5yyjgnxqfqjig2kf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8a-pac-0.2.1 (c (n "atsam4n8a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0finv8sx489qd9mmi42gz260xjad69gsjnymz7mzwm3yy43zdgdj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8a-pac-0.2.2 (c (n "atsam4n8a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0llkldbln15j36qnx00jfm160c641ggs8x61pa6wfwnpvmin5gnr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8a-pac-0.3.0 (c (n "atsam4n8a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zcg092dgcxsiz1qg8azg7sv869fa15fksb4dp8cn72glbb8nwyd") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4n8a-pac-0.3.1 (c (n "atsam4n8a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k0c0l2yn1ir6v4hgsqw8l983ypgq4gls0xsk4hv8094d4jc3pzz") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

