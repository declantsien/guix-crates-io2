(define-module (crates-io at sa atsam4s2a-pac) #:use-module (crates-io))

(define-public crate-atsam4s2a-pac-0.1.1 (c (n "atsam4s2a-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "177md1hjspjd405k43lkwjnpywha37kskpgb0f6ny59dbzip0h7r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.1.2 (c (n "atsam4s2a-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1d7w63c9jyxwzwb02pnrdhj108w59z3ci53541qxb3gqk1zikl6v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.1.3 (c (n "atsam4s2a-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1v2ghbm4nmz34j4phjw4f3flz9vghxv08vcz86d2ycjlm9sahj9x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.2.0 (c (n "atsam4s2a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rj1vj6wj91qj5c95b879r5py96anirn924xdv5hs730f9v2755n") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.2.1 (c (n "atsam4s2a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0d4206wcsqm7bfs34bj0f652ib2rdw45ljnlglvw8bjx5x39ghy3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.2.2 (c (n "atsam4s2a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0b0zdrc4q5wgs0kxnx6iinlh5xqppwmw564gp9hi1628kra12fy7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s2a-pac-0.3.0 (c (n "atsam4s2a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0q3h12mbzs3ipz57xrv4ng7gwvz4kv65yal3z239rpjsw4z11a9j") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s2a-pac-0.3.1 (c (n "atsam4s2a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15h3nlgnmf2psaqqi6vm22q08m97zlykg3p9a3jk8jdhmblax056") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

