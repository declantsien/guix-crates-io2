(define-module (crates-io at sa atsamd51n20a) #:use-module (crates-io))

(define-public crate-atsamd51n20a-0.7.0 (c (n "atsamd51n20a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0zkzfixkk30ssxmdn4n9d2m0fd1q6v87125sydkks9sw1bj7ym0r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51n20a-0.7.1 (c (n "atsamd51n20a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1y6blj12y9x9vg3ddkk7sp68qmdnh3zb61b5gz96y4139k1f6r6f") (f (quote (("rt" "cortex-m-rt/device"))))))

