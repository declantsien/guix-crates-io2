(define-module (crates-io at sa atsams70n19) #:use-module (crates-io))

(define-public crate-atsams70n19-0.0.1 (c (n "atsams70n19") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0x59q7145yvi9wl7fa4mvxblh3mfksz7zn1cgnjjz1dj1sgknh2s") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70n19-0.0.2 (c (n "atsams70n19") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1kvgwkz92d7qb7zy7zhglldimmimf5yjym5y17q7dvm6v5xil7h7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70n19-0.21.0 (c (n "atsams70n19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lz6xny5imws0scyhdzv43c4yrkzs4mabmjrhprnzks906i7nnhp") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

