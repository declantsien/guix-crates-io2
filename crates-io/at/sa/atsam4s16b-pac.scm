(define-module (crates-io at sa atsam4s16b-pac) #:use-module (crates-io))

(define-public crate-atsam4s16b-pac-0.1.0 (c (n "atsam4s16b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gnafzjk7k6vw490w5wj2avhcbmd2v55plxzyzwgn289pa34635h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16b-pac-0.1.1 (c (n "atsam4s16b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1m4lmpnrj1w52n5xgm13mxqv0ffqjac0x4505jdy1g5bjdqckl1p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16b-pac-0.2.0 (c (n "atsam4s16b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03nj3956v474cck1svd641sbnxprg1pj6f02vpr4r8a9q797pkdf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16b-pac-0.2.1 (c (n "atsam4s16b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "049n1cvzvg25z4yghsrvhsqvi1cx070ldn88ngxcdaiycmlzjlfl") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16b-pac-0.2.2 (c (n "atsam4s16b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "15xad0q3zmndgl8a6ylrwdbbslxdzzn6jc52m9s3kydrwq2gjg54") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s16b-pac-0.3.0 (c (n "atsam4s16b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zpwp1pzxlba3a0h9ik1w47niaagp4455a3rfm38p5jbr5dfqk6h") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s16b-pac-0.3.1 (c (n "atsam4s16b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xv391qd7qwl8mv1agv7sx7pvim1yncf170lnihvlm14plcchndm") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

