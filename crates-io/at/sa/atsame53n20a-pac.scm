(define-module (crates-io at sa atsame53n20a-pac) #:use-module (crates-io))

(define-public crate-atsame53n20a-pac-0.1.0 (c (n "atsame53n20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1n9bjf8dzl6s51hshdrmhz6z1pnzxd5zavnpwmcz8ps0ak2bh90r") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53n20a-pac-0.2.0 (c (n "atsame53n20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0had9hb56iki2zmp3jcncm2irdc5g96l4gigplcnzmwjmnr0r9nv") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53n20a-pac-0.2.1 (c (n "atsame53n20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0a4xlqbnj9alsvprznswpg8y44sysca9sz9m5179wk587bmqir1w") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

