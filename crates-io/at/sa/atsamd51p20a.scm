(define-module (crates-io at sa atsamd51p20a) #:use-module (crates-io))

(define-public crate-atsamd51p20a-0.7.0 (c (n "atsamd51p20a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0112m97m18247sib1iswamz5pjvgzbp6gjh7rlwbnbp38iqnw7zc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p20a-0.7.1 (c (n "atsamd51p20a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0b7r1k8cr03cbzsfhphhwh56wi3rrmmhi4k55g42is11wh6w9vb9") (f (quote (("rt" "cortex-m-rt/device"))))))

