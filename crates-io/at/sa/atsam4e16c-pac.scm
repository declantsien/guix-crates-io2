(define-module (crates-io at sa atsam4e16c-pac) #:use-module (crates-io))

(define-public crate-atsam4e16c-pac-0.1.5 (c (n "atsam4e16c-pac") (v "0.1.5") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0i8g2rhv0in99w9sf3gw6bp51cc6ana0f2bncffp8sayy04fyky6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.1.6 (c (n "atsam4e16c-pac") (v "0.1.6") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1m5rcaqbcmygwaz8ffc5zd0i04shnyai40p2v1mwxf36rlk7cwjn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.1.7 (c (n "atsam4e16c-pac") (v "0.1.7") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0s5pzyfjrdrmp7qz059pfwyym0mras49p40r36szddxqm8f11bwa") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.1.8 (c (n "atsam4e16c-pac") (v "0.1.8") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14rw4qfmanb8pa7ra1y1bh08li84bypvzablwb3pf3ns8cysq569") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.2.0 (c (n "atsam4e16c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1kjn96cj0cfrs2a2qa7b2yjr1j038jdr631kfgd68g6sbf303b1g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.2.1 (c (n "atsam4e16c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1d170b8m62rrw5pw4zsa1phwgb7gz5zzd6pv76jqvp5f5jvfjfyx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.2.2 (c (n "atsam4e16c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1z6cf7jpk4vhrlzgpc8fnympsmmig8ki5kplbmvz2inkxw997ps7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4e16c-pac-0.3.0 (c (n "atsam4e16c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "131ijzl2h9vdg5nyzi0nac7vafsc7sddw0cgpm6cjz29fkzdwgkd") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4e16c-pac-0.3.1 (c (n "atsam4e16c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zy3cgn1y9jl17q05im3i6v8g3bnj50yksq2kxwp3fkdpkb4z4di") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

