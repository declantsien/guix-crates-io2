(define-module (crates-io at sa atsame51j19a-pac) #:use-module (crates-io))

(define-public crate-atsame51j19a-pac-0.1.0 (c (n "atsame51j19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "12337nqjkhagyd28c175vxzd3a8jr2wb550ra8c3vd9y6hsk3i13") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j19a-pac-0.2.0 (c (n "atsame51j19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "014qjb2xfkmcmb74z83k39mjd9kgdl3kiq1z9qi6m1qdng1kmxyq") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51j19a-pac-0.2.1 (c (n "atsame51j19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16f15hnbqdvz3z5d64lhdfpv864i31qirrxgisp8h7cmqchdns6l") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

