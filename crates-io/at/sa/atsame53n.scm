(define-module (crates-io at sa atsame53n) #:use-module (crates-io))

(define-public crate-atsame53n-0.8.0 (c (n "atsame53n") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0rgm14sx3ahfm8bjmh8qvmvxxkvs3slllx4hska4gc3f72x1pf4g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame53n-0.9.0 (c (n "atsame53n") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0bqv6q82hs5y5d95c58m9kfk6h5z0wdw50xjp417gfk6r9chazz5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame53n-0.10.0 (c (n "atsame53n") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rwqqs2izsd2c2i8ca9v707sikxygydngympd69m3lfsmqnjzcxb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame53n-0.11.0 (c (n "atsame53n") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0hfvibmnb9a0d18w7zaamiz0llp5cpia989fj90v9rj3ggbwp44v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame53n-0.12.0 (c (n "atsame53n") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1rcpapq9lydq1qzfhqavrnr4cdc3hc9jr3hc4k2bf7qlvysg77w5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame53n-0.13.0 (c (n "atsame53n") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "18d6gjjz32llipsmdwpj3qmbiln0x4yr3i468ikfbrg3inw2j799") (f (quote (("rt" "cortex-m-rt/device"))))))

