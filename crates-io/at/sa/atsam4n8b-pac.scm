(define-module (crates-io at sa atsam4n8b-pac) #:use-module (crates-io))

(define-public crate-atsam4n8b-pac-0.1.0 (c (n "atsam4n8b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gf9kbg1h6rmlffzns28h04zvfbxw9r53day50jnqzaccjkpp1ny") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8b-pac-0.2.0 (c (n "atsam4n8b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0891jf79cyxnhn2p02dj7nskj2dm7pc4afjc9j08h6hkrywgy50f") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8b-pac-0.2.1 (c (n "atsam4n8b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "13hzfjrgasmy0gv13l2fd9wmj8zrj15f8fy7h9qvdrlp7v2nfay9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8b-pac-0.2.2 (c (n "atsam4n8b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1937p7ammsnl4bbkkxih60b3rrl3mrq0p3b9qkd3j0vnqs1xwm10") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n8b-pac-0.3.0 (c (n "atsam4n8b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1acm5x6hzks9d8gqr82zv1xmmr1vixz9w7237hqyfc3vc8v5w5df") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4n8b-pac-0.3.1 (c (n "atsam4n8b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cr6vna97mx7nxbmrlcw5227awz9966hmq7zbv04m5giz5f89vsj") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

