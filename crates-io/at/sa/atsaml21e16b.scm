(define-module (crates-io at sa atsaml21e16b) #:use-module (crates-io))

(define-public crate-atsaml21e16b-0.1.1 (c (n "atsaml21e16b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1x3pg91v6ygpymrqvm5d66aijwgw9qc242zw98ml69gr1dkalclv") (f (quote (("rt" "cortex-m-rt/device"))))))

