(define-module (crates-io at sa atsam4sd32c-pac) #:use-module (crates-io))

(define-public crate-atsam4sd32c-pac-0.1.0 (c (n "atsam4sd32c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zpirvbckmszlclhz7p87xp2s181y7wvi7yib7myrzy9pqs6lw9s") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.1.1 (c (n "atsam4sd32c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "173sn0d0xhmgqphhjn26v74nmfm3iw62p31jylqd1p74p03agvwi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.1.2 (c (n "atsam4sd32c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "06c2diynca8wp7v2fyy17sp6wgj6vlb0lvc0afjs4jxa71vsppyf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.1.3 (c (n "atsam4sd32c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14m81sn7rn6lry17n9s7ayl6n6pw0pxmw8jiz2grfs3g2vqwc2sz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.2.0 (c (n "atsam4sd32c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0l603fsaxh10gvzasnsd4dxrvbkaal5xh445z6f3yi2gkrwbqgpa") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.2.1 (c (n "atsam4sd32c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0al60403zqj8a9sypmk5r16iv35avqbw0n85mg31xg7wbic5ym6z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.2.2 (c (n "atsam4sd32c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m9c6mj9k6nrnv60a3anqk27fvan0kfnjyx9mgrp4hiva565n5vr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4sd32c-pac-0.3.0 (c (n "atsam4sd32c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ydpsq4hfwglicv5y2b3zwfwp1nxxm228n6ljci9cch2nw39lfn0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4sd32c-pac-0.3.1 (c (n "atsam4sd32c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0l90nry96fprg2hxh0pgspxfcrf9xywmhhb8d1sj7p7gzlkphbvh") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

