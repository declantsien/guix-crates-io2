(define-module (crates-io at sa atsams70q21) #:use-module (crates-io))

(define-public crate-atsams70q21-0.0.1 (c (n "atsams70q21") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "03x89jmp1mi48n94d78rfpyabny636rqswbz9hm5p79yvzb39hjj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q21-0.0.2 (c (n "atsams70q21") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1pvxdlpdkgci3h54jaww7i0i17l79c08z1l4ra07gc124daxvj95") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q21-0.21.0 (c (n "atsams70q21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0cn43x17pwpjz2sp3rjxhwjs10dsryjqbf7dk4jnjp27pz357pg1") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

