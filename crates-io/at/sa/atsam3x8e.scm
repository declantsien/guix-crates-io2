(define-module (crates-io at sa atsam3x8e) #:use-module (crates-io))

(define-public crate-atsam3x8e-0.1.0 (c (n "atsam3x8e") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0bk8rjcp3zilgrslr8fcj6k7sx6pb07k7flxva1fl4lj03wncyhn") (f (quote (("rt" "cortex-m-rt/device"))))))

