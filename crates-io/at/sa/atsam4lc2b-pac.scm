(define-module (crates-io at sa atsam4lc2b-pac) #:use-module (crates-io))

(define-public crate-atsam4lc2b-pac-0.1.0 (c (n "atsam4lc2b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0j13p3ikmqhymmci3hg35gg9rpzn0agwmjj17xdi4047wymvyk6g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2b-pac-0.2.0 (c (n "atsam4lc2b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16b4a8q7yv97gr2x91ph7lmy9sa4hms4hqj4l1vqmppbpjynmsch") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2b-pac-0.2.1 (c (n "atsam4lc2b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0yd1ijaz8wxbjgllx4cgbghzgjhzra53r6smdggikva4k3z2j8la") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2b-pac-0.2.2 (c (n "atsam4lc2b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01vqf3fx0qfwanzby6zzxmxsa4bg0qd2b3yv57br5wfjw9jccrk3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc2b-pac-0.3.0 (c (n "atsam4lc2b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0a5gri0mrv95va3m18l6c0iagq5jd0gz0q35mvqa2ni35pd6920r") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2b-pac-0.3.1 (c (n "atsam4lc2b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1jifiwqd5kc0017jmg4y6b8k2zb3wbs9xwmdd73gsf8g6nqv0h5z") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc2b-pac-0.3.2 (c (n "atsam4lc2b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0b3sf7xmqcpdc9habl5avr93jjp08nmjr3q7a0sqb4pyjsgar0a8") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

