(define-module (crates-io at sa atsaml21j18bu) #:use-module (crates-io))

(define-public crate-atsaml21j18bu-0.1.1 (c (n "atsaml21j18bu") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0izz1v1wrdcgwz92ymqj7jri3ik49p800djdd86kzhklagpxn499") (f (quote (("rt" "cortex-m-rt/device"))))))

