(define-module (crates-io at sa atsamd11c14a) #:use-module (crates-io))

(define-public crate-atsamd11c14a-0.6.0 (c (n "atsamd11c14a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1khfclbj50is6wnvfp1v2dfc5nrj6qp95ayi7ij961af76d92nw6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c14a-0.7.0 (c (n "atsamd11c14a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1lxwkb90slkj9gq9l2ckd21sz656105j0fk55mgzprbn26ha921i") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c14a-0.7.1 (c (n "atsamd11c14a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0cwlwx1cpcrqz7272nqvzvw8kfw4s5a4k0qqr5i71xn0lbi0jb5p") (f (quote (("rt" "cortex-m-rt/device"))))))

