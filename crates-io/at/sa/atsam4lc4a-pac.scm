(define-module (crates-io at sa atsam4lc4a-pac) #:use-module (crates-io))

(define-public crate-atsam4lc4a-pac-0.1.0 (c (n "atsam4lc4a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "18xyip2nyzjdr1rlagpgsf72gbap5hqari9x4wdl41sb3wmdz5xg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4a-pac-0.2.0 (c (n "atsam4lc4a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0h4bc7cinwmsjsnkv7997x6nzafz12azivp9c21qmlg5p96h2g67") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4a-pac-0.2.1 (c (n "atsam4lc4a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1qjga33iqibzmkz0p32rv1v016h301bjy1pcgb4758m2gqr7pcrz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4a-pac-0.2.2 (c (n "atsam4lc4a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kawfm5fkav40jv7j0kw1hyfmhgfg3a5pw4wi0agbasbqfi95mpp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4a-pac-0.3.0 (c (n "atsam4lc4a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1v509yfw9p87f7avf4047md6vr5f58igf6sym7ibg1si07jamqaf") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4a-pac-0.3.1 (c (n "atsam4lc4a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12pjy7iz45c1b8cjrc7d7m9y7zg4jh1860sw8v40dmm00zhszd8z") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4a-pac-0.3.2 (c (n "atsam4lc4a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kxqdf5q623jvsvk1kxa3n4aimi6sn2ni8yzw6p6rfa50bk1zcch") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

