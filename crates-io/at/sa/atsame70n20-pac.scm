(define-module (crates-io at sa atsame70n20-pac) #:use-module (crates-io))

(define-public crate-atsame70n20-pac-0.1.0 (c (n "atsame70n20-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lf32kaj21qvlvg8wjabmnsqa6hnhiygi8nsdhi5al5qvixrjyar") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20-pac-0.2.0 (c (n "atsame70n20-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mz0q3rda0gzzxvgz8q15vap389xbwrfiay34srri4hdzbzazfzz") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20-pac-0.2.1 (c (n "atsame70n20-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0qb8id13vbfq1rsvqs6r0hvgd3wacm1l5ybpbqd11vl8bwjfi6vq") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20-pac-0.2.2 (c (n "atsame70n20-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16nwdyc9xrnsmx0nvqqh10qpvl1fjdj93v201vphg2drndwcirf6") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n20-pac-0.3.0 (c (n "atsame70n20-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08wm429px0si8gaqqi66d68pngdnayrklvnh8px1vrh2j02z6h31") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

