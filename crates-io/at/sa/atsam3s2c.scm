(define-module (crates-io at sa atsam3s2c) #:use-module (crates-io))

(define-public crate-atsam3s2c-0.1.0 (c (n "atsam3s2c") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1qgjhc6qcgah4gs26vym66bnswdn2126467gid3531f88z5x01sz") (f (quote (("rt" "cortex-m-rt/device"))))))

