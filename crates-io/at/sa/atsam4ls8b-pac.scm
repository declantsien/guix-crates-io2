(define-module (crates-io at sa atsam4ls8b-pac) #:use-module (crates-io))

(define-public crate-atsam4ls8b-pac-0.1.0 (c (n "atsam4ls8b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0qy8qawrz0k4nrihpc59md72r4gwffqcipx403283qnsgbzicgz8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8b-pac-0.2.0 (c (n "atsam4ls8b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1wbbngrlbbb9vf9viywsn0l468l267r7c32xxrh1dvx221731sdy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8b-pac-0.2.1 (c (n "atsam4ls8b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mw6w4r7f9mwbipzqj5ld233j8kl71y3c4igh3nxkim3sipvi5aj") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8b-pac-0.2.2 (c (n "atsam4ls8b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1g83g0czzycpbqcaznlmj97hsffcccb2gsmyva7amq174jp30pij") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8b-pac-0.3.0 (c (n "atsam4ls8b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00ihb0931bfka1i14jr2fij0qprrpskjxncbc67bida6bfka1k7y") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8b-pac-0.3.1 (c (n "atsam4ls8b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "18hlrvjg54g5gfwkc8p31c0fnv4alsvsf2jlwn8fmyjfskvzyqjv") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8b-pac-0.3.2 (c (n "atsam4ls8b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bhwpq5lm5x3sa7bgn7ya16zhsqg8a6xb60v6wmbvjq1v033wppn") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

