(define-module (crates-io at sa atsame70q19-pac) #:use-module (crates-io))

(define-public crate-atsame70q19-pac-0.1.0 (c (n "atsame70q19-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1kdbqcj04bv2m4djpkhyasxdf354ysm4vk5whcm6n25x5asgkvk1") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q19-pac-0.2.0 (c (n "atsame70q19-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0zk3lx7cbn8j8zbs2m69v61xvpp949rvicdpbscr472i9lccppyp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q19-pac-0.2.1 (c (n "atsame70q19-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0r2mif1n123x6wrnbsakgankraby5sayl4dffgynrpqsfaqcc57b") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q19-pac-0.2.2 (c (n "atsame70q19-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00n3af7hdj0m1vb0bp89zfrvqdmdbdgs1h04r48psrwdc8375jfg") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q19-pac-0.3.0 (c (n "atsame70q19-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1czzwkp049kjw3v3crkp3rhacl2gfx75ky2ka6ad5n0nlf8ql8kh") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

