(define-module (crates-io at sa atsamd11c) #:use-module (crates-io))

(define-public crate-atsamd11c-0.8.0 (c (n "atsamd11c") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1jd9g3a5lrg629vykdgi5fn121c13sv88v2cyajhfwcgwv5jymlz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c-0.9.0 (c (n "atsamd11c") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "11gpj1ranhfb131hk6621v5rrjg5krma1k57lcvi9dipf057r4bn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c-0.10.0 (c (n "atsamd11c") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1q2ks8zaaa3f0cf41ag2v46x3199f5d10ld7wqlfy91cg19hhikc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c-0.11.0 (c (n "atsamd11c") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0mp48gcxdyk12pxvg8spjc59pnr6hj0kswmjmflf63q1q6b88asq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c-0.12.0 (c (n "atsamd11c") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "00hz14f73lidsdlqiwcg49797hapylh0p9c76p8wrd74x7d6xdrl") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd11c-0.13.0 (c (n "atsamd11c") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "105wnkjylbrkz7zcghd6s9xzds67i2mvq530y8m7cpdhzijgn5gh") (f (quote (("rt" "cortex-m-rt/device"))))))

