(define-module (crates-io at sa atsaml21j16b) #:use-module (crates-io))

(define-public crate-atsaml21j16b-0.1.1 (c (n "atsaml21j16b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kjaj9ik24bfn46mdbrqnsivmzw192s36a5x4gp5snm4wyw83yf3") (f (quote (("rt" "cortex-m-rt/device"))))))

