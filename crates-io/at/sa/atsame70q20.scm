(define-module (crates-io at sa atsame70q20) #:use-module (crates-io))

(define-public crate-atsame70q20-0.21.0 (c (n "atsame70q20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1fwydw79d4pligqd3j3i5wjia7q1679lsq1i50swf1vb9m4n4h3b") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

