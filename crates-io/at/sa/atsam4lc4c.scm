(define-module (crates-io at sa atsam4lc4c) #:use-module (crates-io))

(define-public crate-atsam4lc4c-0.1.0 (c (n "atsam4lc4c") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "15ak95nhrl5xrqgjzsj3vp0wlc96yzmas1wn6aq1l1gd43hrmyv5") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsam4lc4c-0.1.1 (c (n "atsam4lc4c") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "141w8adzibzb8hxqmq6qcbp3p40jrp9z025bxjs18374hwd0csab") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

