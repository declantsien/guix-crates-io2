(define-module (crates-io at sa atsame70j21) #:use-module (crates-io))

(define-public crate-atsame70j21-0.21.0 (c (n "atsame70j21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "072k42w1zc839583vrh5h28nzkbavc7m7b9fy646401g55arl20x") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

