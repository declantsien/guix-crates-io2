(define-module (crates-io at sa atsam4n16c-pac) #:use-module (crates-io))

(define-public crate-atsam4n16c-pac-0.1.0 (c (n "atsam4n16c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "186fn908qg5fbwjhg3gx0ck9fgd0s77gf917dk6233j8hnid5vah") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16c-pac-0.2.0 (c (n "atsam4n16c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0n2fg5iqmvxxm5sdr6xpybp3fsmc6fda2jynx2gn547hkddjwh58") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16c-pac-0.2.1 (c (n "atsam4n16c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05a6akk5fyjccsg019hl7g0wrnsy4b0xrn7jk1dfi21j3dhzx72x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16c-pac-0.2.2 (c (n "atsam4n16c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06dhfwk31r2fic6282ww6sq3pw5gk74agb0cp14alra0s3pw0pnr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16c-pac-0.3.0 (c (n "atsam4n16c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10fkf6frasypdrhx6m6vd0fljk6lnv9bm2kmyzj9ialf3r4lsd8q") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4n16c-pac-0.3.1 (c (n "atsam4n16c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1g1lawy1xrwpl54476yv09sqfiy2238b4f86cc66fq0gz9jlw8mx") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

