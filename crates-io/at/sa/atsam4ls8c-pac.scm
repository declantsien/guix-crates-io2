(define-module (crates-io at sa atsam4ls8c-pac) #:use-module (crates-io))

(define-public crate-atsam4ls8c-pac-0.1.0 (c (n "atsam4ls8c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zszmw1pdxmy7588y411vqqn6lykjc4h5nshpb8f0kfigfbkbgmq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8c-pac-0.2.0 (c (n "atsam4ls8c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0647k5rk508f9c8rbbidif47livsxhkhcg87yxq9w967brwfbsvq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8c-pac-0.2.1 (c (n "atsam4ls8c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fiqxabc3ccadpisdy7w7p6wdfc5wad8mskwf2s3sb6fpx6spfqz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8c-pac-0.2.2 (c (n "atsam4ls8c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xasv0yiysvnsknj47vk5slszypgbywp38hzk1x4cdq8aqa5xm6g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls8c-pac-0.3.0 (c (n "atsam4ls8c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "10541hmcqmd8yi5qks4ar1s1wjiydvsssg272dpd3h5i0krvhn27") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8c-pac-0.3.1 (c (n "atsam4ls8c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0cjrc0wfm858bwypjps45nid9klc1i7lyqhh77ndvvdsm6hmzi6i") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls8c-pac-0.3.2 (c (n "atsam4ls8c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cgq242v4ac8s92gwvgrx2fwirw3dqcmn2yinw087pxghi3y27nx") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

