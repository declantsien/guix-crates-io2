(define-module (crates-io at sa atsame54p) #:use-module (crates-io))

(define-public crate-atsame54p-0.8.0 (c (n "atsame54p") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0dpcbhgs6svddl6pg3kbmzqp5axch6di75795n1rkfmnr42y5iij") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p-0.9.0 (c (n "atsame54p") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1ssjh31myyj6qaqc9ykg07aps20d5hnqxrr3b32nga70g2lz65rp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p-0.10.0 (c (n "atsame54p") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16g4zh1v0pnf12mb6bk8x6skq65ba79xw8xfyraqf47yvpz7vk02") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p-0.11.0 (c (n "atsame54p") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ynfd3zhxdpkh36hhjmwvfx92zzl41vjvb0qsgi2vb8gj2h0qf9z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p-0.12.0 (c (n "atsame54p") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kfb8bpx5w2x70fffgs3ia0k9jap22x7clvvv362am8rplndl6v1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p-0.13.0 (c (n "atsame54p") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00l9h06zn5r7wh7x80yy6692kbi0zd863mm4f2srli1rgsrj2nbj") (f (quote (("rt" "cortex-m-rt/device"))))))

