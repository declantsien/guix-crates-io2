(define-module (crates-io at sa atsamd21g) #:use-module (crates-io))

(define-public crate-atsamd21g-0.8.0 (c (n "atsamd21g") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0ply5brsi5rcp752qy9gxb5xf1987wz1imgc0zrkz12azgbwh0q5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g-0.9.0 (c (n "atsamd21g") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1iw33j4ygmqlnvkn3f72z26xinsafffdf6hr4bg489pq3iar4h0x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g-0.10.0 (c (n "atsamd21g") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "17ja9vfbnbc9l2hy0rjf935z7lp3004aypl8ha2rqwx59vywhmk2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g-0.11.0 (c (n "atsamd21g") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0k35hz706yl4ccwxdn2kcqhz1m4rvk24v2ijp2m8rl5wd6cd3j9x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g-0.12.0 (c (n "atsamd21g") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1bavq3mq7drv123p986wxp0317285sgisfjv5j2nyv0v7vh3rjaf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g-0.13.0 (c (n "atsamd21g") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "03f0r0q97xqgg6ckjvs671rg5r8k7d180s61in4kjl1zql1gmiwd") (f (quote (("rt" "cortex-m-rt/device"))))))

