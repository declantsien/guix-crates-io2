(define-module (crates-io at sa atsame53n19a-pac) #:use-module (crates-io))

(define-public crate-atsame53n19a-pac-0.1.0 (c (n "atsame53n19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0v18padw8r9av9vdr235n7hw6jbhqzzm02aa2falf16q9jbhsjab") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53n19a-pac-0.2.0 (c (n "atsame53n19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0cqjp8l4za8gpjrlnmbdyz7yy2jxw3jjfgw61zzxrfyb6v2a73kq") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53n19a-pac-0.2.1 (c (n "atsame53n19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "009h4pdkm7h3vc7nnn58p7s30rbms3mqzcbb0zzz4f4izdqmkcs8") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

