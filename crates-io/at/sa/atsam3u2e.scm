(define-module (crates-io at sa atsam3u2e) #:use-module (crates-io))

(define-public crate-atsam3u2e-0.1.0 (c (n "atsam3u2e") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "05bwr15s42m4wx2kyxjfipv293ad1c2ylsdd6g14f720b56b9wfw") (f (quote (("rt" "cortex-m-rt/device"))))))

