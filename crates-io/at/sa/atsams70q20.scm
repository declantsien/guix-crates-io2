(define-module (crates-io at sa atsams70q20) #:use-module (crates-io))

(define-public crate-atsams70q20-0.0.1 (c (n "atsams70q20") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1d4zbyncpzxn7acnx10fwqdk48k8py0vm0d49mpc8d7aw6d10xxk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q20-0.0.2 (c (n "atsams70q20") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1yc35ak4rb71xx7bdvv4sp66d47ln2fkb7m9ffayzap718rifdww") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q20-0.21.0 (c (n "atsams70q20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "14r24j04zwjkw0xk27wg2fkvi1fkf1s5llm9m274phz6ji20qv85") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

