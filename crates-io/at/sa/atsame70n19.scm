(define-module (crates-io at sa atsame70n19) #:use-module (crates-io))

(define-public crate-atsame70n19-0.21.0 (c (n "atsame70n19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1lj5wsm2x1q5gy82dxm657p62pn4pa3csqlshq6hv24h12g4rx5h") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

