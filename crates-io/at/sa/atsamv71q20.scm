(define-module (crates-io at sa atsamv71q20) #:use-module (crates-io))

(define-public crate-atsamv71q20-0.21.0 (c (n "atsamv71q20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0b23fdlxwzw15hp4cgj8z61z3vsi7dz5jl6pbw531psvys444y5h") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

