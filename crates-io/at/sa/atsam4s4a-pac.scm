(define-module (crates-io at sa atsam4s4a-pac) #:use-module (crates-io))

(define-public crate-atsam4s4a-pac-0.1.1 (c (n "atsam4s4a-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0zdqmcg6qhh3zxvniirx48m9r3lvabmwhaji7grihywcdz4gfsrk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.1.2 (c (n "atsam4s4a-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0h1r1aglbyacngnc9shdbh73m7fa7zrgn9bkfvc2ck3frjfc2jz4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.1.3 (c (n "atsam4s4a-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vaxm5whavh77qsqszsqawkjcmr4ad00langl5cp8kjyg95iw0r1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.2.0 (c (n "atsam4s4a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "018hbsvm1pi3nynm6sdxwpmchjagpi67gsxx3vbxargjw5sqrp3c") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.2.1 (c (n "atsam4s4a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "08hgri7wi061h2lpjq9lifwam4s7fdg5kvm5q5zz8xlilhjbklnh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.2.2 (c (n "atsam4s4a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1rlgha9d29y8b1sfffpv58rnv53qs5x3pgv906c4zfxn4z0qzsm9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4a-pac-0.3.0 (c (n "atsam4s4a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02vxk97daqy051fh01d2s53khpq7jicm3bsvgpcc6ag4mwfi1wsk") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s4a-pac-0.3.1 (c (n "atsam4s4a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00rg12riwrngdqknkl854mdi0bvdgmdw1767gs26c3z0yjgmcdhz") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

