(define-module (crates-io at sa atsamd21g18a) #:use-module (crates-io))

(define-public crate-atsamd21g18a-0.1.0 (c (n "atsamd21g18a") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "198fcgsj2snnz5srry8anqlxwl7sk8ywxirnp3mfpfs61fwym91q") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-atsamd21g18a-0.2.0 (c (n "atsamd21g18a") (v "0.2.0") (d (list (d (n "bare-metal") (r "~0.1") (d #t) (k 0)) (d (n "cortex-m") (r "~0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xzpssgnf37hfnk2nch7va6k1kb9r3ilbnjvwdnmlkfj85pshs90") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-atsamd21g18a-0.2.1 (c (n "atsamd21g18a") (v "0.2.1") (d (list (d (n "bare-metal") (r "~0.1") (d #t) (k 0)) (d (n "cortex-m") (r "~0.4") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.4") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xs5dww6ghgmw0r6jl7fyvgfxg351i4b99mh96l2yygi1z1w1pqn") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-atsamd21g18a-0.3.0 (c (n "atsamd21g18a") (v "0.3.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "07hslfyn5gvg8r71m3pz0c23rnna33xgqdrd1x8zwy7qspnpvp05") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.3.1 (c (n "atsamd21g18a") (v "0.3.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1xacw1v519hf2v0mmfmg9x5k3wbkyadhbijnr37bisn5rkqlbd20") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.4.0 (c (n "atsamd21g18a") (v "0.4.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0layyibvg8sp5qq5jl02x6gqv1xpqdh4ww81yiyfx47dvig7k24z") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.5.0 (c (n "atsamd21g18a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0sdsj4bqyfcycprbwgn1l0gwmgi0qnpsm730lr81603lp4nkgxr7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.6.0 (c (n "atsamd21g18a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1ss07lpcywdm2qf8mmad2gbb90nqf6yv9q29ywc2n0dh3acnnj55") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.7.0 (c (n "atsamd21g18a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1n65x314sv7fsjn4km82v6qj0vcjpqcq3x3ml6r24s0fybd14r64") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21g18a-0.7.1 (c (n "atsamd21g18a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0i9njnm1kq9fw5spjjxi334il5ivq696sazn4fakd0vy0v7jxdgr") (f (quote (("rt" "cortex-m-rt/device"))))))

