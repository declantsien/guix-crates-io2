(define-module (crates-io at sa atsaml21e15b) #:use-module (crates-io))

(define-public crate-atsaml21e15b-0.1.1 (c (n "atsaml21e15b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1ddvbabqb19ffhxcq1i5rwsg4iq6f817y0yhvv92pdfdjd5i9dmx") (f (quote (("rt" "cortex-m-rt/device"))))))

