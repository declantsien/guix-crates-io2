(define-module (crates-io at sa atsame70q21b-pac) #:use-module (crates-io))

(define-public crate-atsame70q21b-pac-0.1.0 (c (n "atsame70q21b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0g1kcqfp6viigkx01k8559ccpkad66rd00kzgmknikd21zp1agps") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21b-pac-0.2.0 (c (n "atsame70q21b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vklajsvi968bwphr7c8mj8ld9lampazsd7qfa1rdbjpcq7gmrwi") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21b-pac-0.2.1 (c (n "atsame70q21b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1kca505mv4mwy7i79i3cbkc970k3jc98x9lij8mpp8p56y1pipq4") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21b-pac-0.2.2 (c (n "atsame70q21b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xj3smw2dy6f2sjy3vyqlycygjl8id2kx0jvn8238gd8wqjdrkpj") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21b-pac-0.3.0 (c (n "atsame70q21b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16dbm55b93m07lcd3h60sqp7blnxrizvf2ilnzgjsj9r62siylq2") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

