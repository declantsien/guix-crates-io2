(define-module (crates-io at sa atsam4s8b-pac) #:use-module (crates-io))

(define-public crate-atsam4s8b-pac-0.1.1 (c (n "atsam4s8b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0lf3aascjzzq3fbxv1jq4dpbnzi98nhgg51ga157g97ry653slv4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.1.2 (c (n "atsam4s8b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "04m3wyiak2imjq0jqgl8p7wia2mhk5wyqv1p3xj7vlap6irpdysz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.1.3 (c (n "atsam4s8b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "09i7fqmk9qd59pncbymri2ihd4gjlv9pzdzwvn663y6a5lw2qqj7") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.2.0 (c (n "atsam4s8b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0x34d27x7lqc09minji3abc7gassjpgqp0z579qwrrw15cww0jvc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.2.1 (c (n "atsam4s8b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pkwb01gambdqvm7qijlpj1rqj28b0hcnjfbj6x7xxvkhvgnl3hd") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.2.2 (c (n "atsam4s8b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0mjf83380i5n6dnx2wmkmrsxmwkc6837nkf4dxfb6dkfq49sk73p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8b-pac-0.3.0 (c (n "atsam4s8b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1dvv0swwiwxbqdqgfvhl1ax4kyrn5pjpzz27lir9gi156x267q8y") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s8b-pac-0.3.1 (c (n "atsam4s8b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1b5f2qdj8q5ywvxn0hl7qq83wan6002vr8d6y9kq0vc8jjzrvv0l") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

