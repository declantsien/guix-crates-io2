(define-module (crates-io at sa atsaml21j18b) #:use-module (crates-io))

(define-public crate-atsaml21j18b-0.1.1 (c (n "atsaml21j18b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1z1k4mwss1g9a0d8m4dc1hvr0xm6mcw6jav5zrdckry60dnhhi8r") (f (quote (("rt" "cortex-m-rt/device"))))))

