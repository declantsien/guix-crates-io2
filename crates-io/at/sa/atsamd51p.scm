(define-module (crates-io at sa atsamd51p) #:use-module (crates-io))

(define-public crate-atsamd51p-0.8.0 (c (n "atsamd51p") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1kndbh3jl3b0zy2nkrbkfi6lchjzljw3ncddgj3bfan12b1vr380") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p-0.9.0 (c (n "atsamd51p") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "11ms1ww1dzycjifgcz6qy5j52ph8qvlhflssz2h38l51ydka5x88") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p-0.10.0 (c (n "atsamd51p") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "187bpika4c3hgxhqnaix08l0splza7w5yhffik4n9dz31ncj5iqz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p-0.11.0 (c (n "atsamd51p") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1x3prydpynd9p6h0l1fv90b4mlzyl8cld84fwcdipr3jymdlvw5y") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p-0.12.0 (c (n "atsamd51p") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1lnwk808sxy1fwj8vda20ppvddxx6bvidwbc39dl6blgxlzs86r6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51p-0.13.0 (c (n "atsamd51p") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1ph2zfh0gjzgmmyv3alpsmh3nmjzj2fk525iwwnak4gm1v1kzr73") (f (quote (("rt" "cortex-m-rt/device"))))))

