(define-module (crates-io at sa atsame51n19a-pac) #:use-module (crates-io))

(define-public crate-atsame51n19a-pac-0.1.0 (c (n "atsame51n19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vvs1fpvcwm0ms635n8r3r91m2bwinww87fkb88vg5wnf0idymjp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51n19a-pac-0.2.0 (c (n "atsame51n19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1s0wsmr8gwi80065nvd8xrw2bnfqp8v3qdqpla64qia3z0797r5z") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51n19a-pac-0.2.1 (c (n "atsame51n19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pwbq3hmimlq5fc3pwka6y0z1rb0fgg4h7kc47pjacml5cxg5d8x") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

