(define-module (crates-io at sa atsam4lc4c-pac) #:use-module (crates-io))

(define-public crate-atsam4lc4c-pac-0.1.0 (c (n "atsam4lc4c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0s2w3vnss9s5dbg6ir2bpsnsw7yn47p8cf3i9mfkfz4pjz140rm9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4c-pac-0.2.0 (c (n "atsam4lc4c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17rj20zilwdyssi1cknnv1jzdw1mkddd4v34rqyiii7c85m920jr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4c-pac-0.2.1 (c (n "atsam4lc4c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0acjhhckw6w7rm1b4npwymfv5l3javr7dmnl1dfdx6vzr8wwvdb9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4c-pac-0.2.2 (c (n "atsam4lc4c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1q1xqry9jmfdhfvcc12i14bzv53lqq06da9n7s8yxsgjkqaj2yi2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4c-pac-0.3.0 (c (n "atsam4lc4c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xsxfpqfjq42m04l82h49v2d4qpsd941y7b0pqclayasyqqsxls0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4c-pac-0.3.1 (c (n "atsam4lc4c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16c5p0zgk5lg3whxlyn172pa72gr5k6c59mmklgykmcc3xbcwm3c") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4c-pac-0.3.2 (c (n "atsam4lc4c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1crn3mykbk768ygv6330ri5rdp80wy63dq7vrls22i2fqkgmxlw3") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

