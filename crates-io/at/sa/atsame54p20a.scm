(define-module (crates-io at sa atsame54p20a) #:use-module (crates-io))

(define-public crate-atsame54p20a-0.6.0 (c (n "atsame54p20a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1l52cbc6lh3aq8z7clcmq3i1hd0jv6ahjwf1kbfz1ma3dp5h1wpy") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p20a-0.6.1 (c (n "atsame54p20a") (v "0.6.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "01mjpvxbyncvsix1dhq7kqxw7aigsmywyywljl5bzhkywc0i8d6w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p20a-0.7.0 (c (n "atsame54p20a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1pff4rrwaj6rfjhqf9ln2h58b0nwj9d385ixj22f3hy3y1wvjpz2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54p20a-0.7.1 (c (n "atsame54p20a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0v0qz6w6ykgmyqlmyzh97j485l8nxaa9z7fd5bqmf1m7i7aj2r91") (f (quote (("rt" "cortex-m-rt/device"))))))

