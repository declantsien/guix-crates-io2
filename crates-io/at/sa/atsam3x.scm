(define-module (crates-io at sa atsam3x) #:use-module (crates-io))

(define-public crate-atsam3x-0.1.0 (c (n "atsam3x") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.15, <0.8") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1m0bi4gap6fm3p3g6ngmij95whafnb03bsffmqjj0w7bk82nj8b2") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt") ("atsam3x8e") ("atsam3x8c") ("atsam3x4e") ("atsam3x4c"))))))

