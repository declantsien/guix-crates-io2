(define-module (crates-io at sa atsame70j19b-pac) #:use-module (crates-io))

(define-public crate-atsame70j19b-pac-0.1.0 (c (n "atsame70j19b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1vkvibzs05jr2drfvf54zghxd2hzii4jbsyagmyf9s9sg8f3xhfz") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19b-pac-0.2.0 (c (n "atsame70j19b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0f7k66mc6ih1gbg3rkz0jp47ykappx7kdnkpnvwdni58z0gjzlrc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19b-pac-0.2.1 (c (n "atsame70j19b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cql2xlv9dw04sg0pswjky5l0b2rh388kb2rvagqm9xbj39rhp0i") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19b-pac-0.2.2 (c (n "atsame70j19b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xki7lvrv24fmzc58i9sak8r9r2zaz2273wgp2h65b2dskzpgzyx") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70j19b-pac-0.3.0 (c (n "atsame70j19b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "185khmsgw5h09wpinzn4lb3mgl8f3izrh2qfgkkzbfc6nlwbdy7j") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

