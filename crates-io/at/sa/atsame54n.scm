(define-module (crates-io at sa atsame54n) #:use-module (crates-io))

(define-public crate-atsame54n-0.8.0 (c (n "atsame54n") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0m58lnx8bs2b5qxd4x9gvx7sv8qag3nf72fiyhfvdhmja1dlic9y") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54n-0.9.0 (c (n "atsame54n") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "0mnsgs0wlgfllmlgkhgs3g5703z2fyqzphxi03wkxn7wyj0mycvr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54n-0.10.0 (c (n "atsame54n") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0qzic66nwba7m8p6s6d86sblp5vn3zikr4f1ddrgygfxya6f2czw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54n-0.11.0 (c (n "atsame54n") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1kvb25gf1fwv1kdnp6r5pg0a0bl3q0zzkai26fbyhwrx5ws3lvp2") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54n-0.12.0 (c (n "atsame54n") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kmjn87qk5mzkflsaqs3y82l9x902gdzirkdd54nbpgisbcdi43v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame54n-0.13.0 (c (n "atsame54n") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1l0z4n3mrhl0kfr3zd51zgwvywflxa5zqbsb70kba3cz9p57h7ha") (f (quote (("rt" "cortex-m-rt/device"))))))

