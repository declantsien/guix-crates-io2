(define-module (crates-io at sa atsam4n16b-pac) #:use-module (crates-io))

(define-public crate-atsam4n16b-pac-0.1.0 (c (n "atsam4n16b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1x6dl6011my26wpd401pp82ajx182bj8q59rabw70bhpgaljp9ka") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16b-pac-0.2.0 (c (n "atsam4n16b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ph66hjyd4m7njciqyz5yh0msmhjbcbv9v2m9akjfqdgnbj0k6nq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16b-pac-0.2.1 (c (n "atsam4n16b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0yvgzq96qz3nannvk5a6f7ld3j4m7af520ylfic613cqsknl03ly") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16b-pac-0.2.2 (c (n "atsam4n16b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gjs3hvv5bipsb3pgm6c5y3s15lrv4ci7qhpv15kdwih3jdjk4ah") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4n16b-pac-0.3.0 (c (n "atsam4n16b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0x1bqhplajlf2ldbhjxqkf4lxiz86f5mb7czf7dplcrbaxkh97f5") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4n16b-pac-0.3.1 (c (n "atsam4n16b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1s7ggcarrlifhl4jw0k5f8n7ycfkicrxdbbs5vv5a4hz55sryzbx") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

