(define-module (crates-io at sa atsam4lc4b-pac) #:use-module (crates-io))

(define-public crate-atsam4lc4b-pac-0.1.0 (c (n "atsam4lc4b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0xjaysfnsx0zj7akmvdclp3kh6zp8nv14r1blapmxysi65xl73i5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4b-pac-0.2.0 (c (n "atsam4lc4b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1shhwghrkpr97azzgvcp44cji91krj8w692wz832yjszpy38n2lq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4b-pac-0.2.1 (c (n "atsam4lc4b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "026h655x5miqcs4capmcjbpk4bgyp1parmwbmc3fyapygm6k1wf4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4b-pac-0.2.2 (c (n "atsam4lc4b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "04xmknf10ak6798jq1840kkvsbggsfd0fm3qysfph69hk1hfqvbb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc4b-pac-0.3.0 (c (n "atsam4lc4b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "187vr7gi8fxcn215cfcfj7qvp84gll9vkklng79viaxxd26i24f9") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4b-pac-0.3.1 (c (n "atsam4lc4b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1pcgy4dzmi0cffcil3g2gzci1sfxnmq1y8wv57q9l4anh4c3zszx") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc4b-pac-0.3.2 (c (n "atsam4lc4b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "19wbm3ywdp4l4pkqbwiff0prq6z2xkl3f9fz8wmhaaz2if91fxyk") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

