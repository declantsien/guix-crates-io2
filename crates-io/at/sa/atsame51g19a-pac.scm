(define-module (crates-io at sa atsame51g19a-pac) #:use-module (crates-io))

(define-public crate-atsame51g19a-pac-0.1.0 (c (n "atsame51g19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0bd2xivzlx00i7j5cdij75cziz92rqixbir9xwg8yw8rxa9gjglz") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51g19a-pac-0.2.0 (c (n "atsame51g19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "17zwlg5c0ivldl1hxnfljgdk777m5qrhaj1yq7wa0cz0mypd8kvc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51g19a-pac-0.2.1 (c (n "atsame51g19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1iq42wizzh9dzcz0i55z9vaa3km3szqpmif492nxcywsnrl5nhwb") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

