(define-module (crates-io at sa atsamv70j20) #:use-module (crates-io))

(define-public crate-atsamv70j20-0.21.0 (c (n "atsamv70j20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0jkxh1g4zyzp086ga45vf4gfr34m7wf97drpcjl150ak8wdi8cb0") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

