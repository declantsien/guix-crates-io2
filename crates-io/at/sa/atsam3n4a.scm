(define-module (crates-io at sa atsam3n4a) #:use-module (crates-io))

(define-public crate-atsam3n4a-0.1.0 (c (n "atsam3n4a") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1zknrvp86flv22fds14i84380mkvhf7p946pl1d20pz9856vsg23") (f (quote (("rt" "cortex-m-rt/device"))))))

