(define-module (crates-io at sa atsam4ls2c-pac) #:use-module (crates-io))

(define-public crate-atsam4ls2c-pac-0.1.0 (c (n "atsam4ls2c-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1shvjlw2armwjfpbcyabs83brhjryq8zf2qj0ymgaqanglp7by8h") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2c-pac-0.2.0 (c (n "atsam4ls2c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1y036gqrhald1pp2nqwvwi5v3i72ag4cxf5acm2x171zq9gjk0ca") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2c-pac-0.2.1 (c (n "atsam4ls2c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gpxpkzkp34xqiifab9b6cvnazxxpkiw5qhs499i2d3cymmz7g4r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2c-pac-0.2.2 (c (n "atsam4ls2c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z5ncmy3ms1ap85kll5141h30firglmahij4wfl3mhpl5ix2l6c1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2c-pac-0.3.0 (c (n "atsam4ls2c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0pfsjshm9i449cpb2pivgvzpdcnvyqhl87fr9cab56mq80gigfx8") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2c-pac-0.3.1 (c (n "atsam4ls2c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bdjkiqfp1a0272nypwvzjyprdxikhkbxyjk9cs2j4s5x3jpim07") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2c-pac-0.3.2 (c (n "atsam4ls2c-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "111kxp24p6aj7m4gj7vqd3xd5fairhpc2n1giidw5lcr0pxl8rrw") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

