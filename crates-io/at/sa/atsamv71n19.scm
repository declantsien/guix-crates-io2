(define-module (crates-io at sa atsamv71n19) #:use-module (crates-io))

(define-public crate-atsamv71n19-0.21.0 (c (n "atsamv71n19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05im79cs509rc4vds0wg51s53qkyi8xwmvh2136nxga8qsw92b3g") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

