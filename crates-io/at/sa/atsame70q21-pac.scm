(define-module (crates-io at sa atsame70q21-pac) #:use-module (crates-io))

(define-public crate-atsame70q21-pac-0.1.0 (c (n "atsame70q21-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05cw6n1r75x7yf82bi7bn2yjvz1243rv7myfc5m36dnvwd2dw7k9") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21-pac-0.2.0 (c (n "atsame70q21-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1l9fqg61riis9c2nqn2z00dc2cg1ffi23axhwqpdikaxy73nq2dq") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21-pac-0.2.1 (c (n "atsame70q21-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1344301rk2kg55wr14l2vcnkkmw1ghncvbwd4d6jxslv926dn88v") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21-pac-0.2.2 (c (n "atsame70q21-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01gxsw24lxdjpvls9hnmbnv2vq86jx3a5pdflaf1q2903nzby23k") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70q21-pac-0.3.0 (c (n "atsame70q21-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0fa09803gsciac93cffg7x2l1l8r906xkgh3yi8zsdr16s3dgsad") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

