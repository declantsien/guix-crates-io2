(define-module (crates-io at sa atsame54p19a-pac) #:use-module (crates-io))

(define-public crate-atsame54p19a-pac-0.1.0 (c (n "atsame54p19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pm628d01ldskq52zgwpss1zk78qz3ybzcqic3sp8klzhjigda8n") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54p19a-pac-0.2.0 (c (n "atsame54p19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0xkmd7lfwskwmmch7lwl3zgjn7byky1yqsa313rxsqk695ki3dsf") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54p19a-pac-0.2.1 (c (n "atsame54p19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s7aljx2vcb040ykyd3c89mj78khsmwjpl9l976licq7fbqjdr8k") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

