(define-module (crates-io at sa atsams70j19) #:use-module (crates-io))

(define-public crate-atsams70j19-0.0.1 (c (n "atsams70j19") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "10dah28xdin02r99fh8wvsgxdfjnzqg78jq27ksiv7943pxd13y3") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j19-0.0.2 (c (n "atsams70j19") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1z8ddmh3f7zq8ddca1g5kjc79ik84m87cxjvqjiq77qsgbl1fw5g") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j19-0.21.0 (c (n "atsams70j19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0gh4sxc23xf0mzj0xddgjji9yi3zd1dq1sij8v3d0994bvsvh8c2") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

