(define-module (crates-io at sa atsamv70q20) #:use-module (crates-io))

(define-public crate-atsamv70q20-0.21.0 (c (n "atsamv70q20") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qcwpyz60dbh8zz6mfrb67s3s31r8x1hj317zs6ixn7s66swclsl") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

