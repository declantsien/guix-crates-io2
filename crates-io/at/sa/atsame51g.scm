(define-module (crates-io at sa atsame51g) #:use-module (crates-io))

(define-public crate-atsame51g-0.8.0 (c (n "atsame51g") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "065mqxmxv9bxnrsrqlclvmlwa2glkwli5g3w8svqc5gllwxr4421") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51g-0.9.0 (c (n "atsame51g") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "18ds889qzf4rmzyynj3sjg3k0dhcy17m4xv9vw1p3ic1nv03gnw0") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51g-0.10.0 (c (n "atsame51g") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "02x28qh5qckjsyz5937adpd91wj9lsa3g331c5c5sc85svmxzhzi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51g-0.11.0 (c (n "atsame51g") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0dnqyw3ylr3q5zwqvpbkwva8byfh1i50a4lp9i8zgkmnfrqz86g9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51g-0.12.0 (c (n "atsame51g") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "02fij95amkdc38d4cag82lkk9c41bscm2zvw79dfhfwg7iw7rnch") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsame51g-0.13.0 (c (n "atsame51g") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0qgrg8a5m8sg1psw16za8pmj6182qhrlanrncywkl506a9shp9w5") (f (quote (("rt" "cortex-m-rt/device"))))))

