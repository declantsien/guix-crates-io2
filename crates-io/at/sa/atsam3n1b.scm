(define-module (crates-io at sa atsam3n1b) #:use-module (crates-io))

(define-public crate-atsam3n1b-0.1.0 (c (n "atsam3n1b") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "03czxs7hs2ppm95iz8jpx58aysir8vrn4kggw1r37hnbbvg79053") (f (quote (("rt" "cortex-m-rt/device"))))))

