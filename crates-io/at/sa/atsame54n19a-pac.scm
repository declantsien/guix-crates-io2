(define-module (crates-io at sa atsame54n19a-pac) #:use-module (crates-io))

(define-public crate-atsame54n19a-pac-0.1.0 (c (n "atsame54n19a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "052gc73jv1ark9plqr7a48jxwj7vlxny7r3ak44zb7j6fvxjhxdx") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54n19a-pac-0.2.0 (c (n "atsame54n19a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "05kaspb86wlr3xpzwi40mkca7msnhls6sigmbzrd8fcc624n8bli") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54n19a-pac-0.2.1 (c (n "atsame54n19a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ch8zch0pi9067jw9y13mqbfww4ssbnfy00m6fyh5mh2wbrvmb0m") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

