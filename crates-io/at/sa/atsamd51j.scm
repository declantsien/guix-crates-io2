(define-module (crates-io at sa atsamd51j) #:use-module (crates-io))

(define-public crate-atsamd51j-0.8.0 (c (n "atsamd51j") (v "0.8.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0y77kqxdiwifj9x64h7cz9niz0lshnlm772fjc8jplmn0wbjxv8x") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j-0.9.0 (c (n "atsamd51j") (v "0.9.0") (d (list (d (n "bare-metal") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cortex-m") (r ">=0.6.0, <0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.6.12, <0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "04nam7p737xcxrvi6p6r710j8skg4lbalsc1z2pgfxglp5jc8anw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j-0.10.0 (c (n "atsamd51j") (v "0.10.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0n5vfj9qzrjbdx6xll35gx1jbm13q7lhqxv7lskjf01hf3rqqpai") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j-0.11.0 (c (n "atsamd51j") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "171xg7bnswjlqg8rlkz8pr9mhkh5xb3b029j40bad0b276vrsh5w") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j-0.12.0 (c (n "atsamd51j") (v "0.12.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0w2ybd24alxmv2ka6p69lc87dk4w1r9m1pp76wvm58kzsvz1zb34") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd51j-0.13.0 (c (n "atsamd51j") (v "0.13.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "12890sl8cddajq1wap81i2p26fgc7hr355ydb2nc9rmqmbidwzxs") (f (quote (("rt" "cortex-m-rt/device"))))))

