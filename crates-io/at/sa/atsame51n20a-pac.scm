(define-module (crates-io at sa atsame51n20a-pac) #:use-module (crates-io))

(define-public crate-atsame51n20a-pac-0.1.0 (c (n "atsame51n20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0mdz93n3d7zfmx3mm2484m5vys8d0y8nlga02h4l7w4ix2vndp5g") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51n20a-pac-0.2.0 (c (n "atsame51n20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0n8yj1ikc9hhf7ggakhhyz95r8hk45qyl7fx4cjb1717idhhvfcn") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51n20a-pac-0.2.1 (c (n "atsame51n20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1m5c94mwkgx75c36k44lqls1vw6wc093mw2wklv33ih41dn55kc3") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

