(define-module (crates-io at sa atsame53j20a-pac) #:use-module (crates-io))

(define-public crate-atsame53j20a-pac-0.1.0 (c (n "atsame53j20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ly0paa44dgls0d8f710ph97w4lr3bgna4wxqzjf7g5a0il9mrqd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j20a-pac-0.2.0 (c (n "atsame53j20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0wfyy1hr64jcd4whg0yxvcav6ql7z7nj8apnwgy5cykcp3zp2qzd") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame53j20a-pac-0.2.1 (c (n "atsame53j20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1mq1xwmaz4wibssy5jpr59j4w5bqwbla07zdw73a0zxn4qgr9pmi") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

