(define-module (crates-io at sa atsaml21j17bu) #:use-module (crates-io))

(define-public crate-atsaml21j17bu-0.1.1 (c (n "atsaml21j17bu") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1infyzv89a7yyirz0vda4v5ffh79pvnvav80gf05s8p5nah4qddc") (f (quote (("rt" "cortex-m-rt/device"))))))

