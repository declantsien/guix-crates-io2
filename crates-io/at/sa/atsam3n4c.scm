(define-module (crates-io at sa atsam3n4c) #:use-module (crates-io))

(define-public crate-atsam3n4c-0.1.0 (c (n "atsam3n4c") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1822i69ibs74ayb1ydkh6497w7pal6c3nmdx1rxzxsi0wgbmlm4p") (f (quote (("rt" "cortex-m-rt/device"))))))

