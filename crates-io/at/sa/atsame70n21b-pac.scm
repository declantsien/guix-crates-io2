(define-module (crates-io at sa atsame70n21b-pac) #:use-module (crates-io))

(define-public crate-atsame70n21b-pac-0.1.0 (c (n "atsame70n21b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0f223a3nvakilzig8375drgh95sr98il2b8rfssadrp5gpxp21kb") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21b-pac-0.2.0 (c (n "atsame70n21b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "11lpx7pqzx16b0wpcgzcvinc8n9mqkgnd3w70bkk9g105li6z699") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21b-pac-0.2.1 (c (n "atsame70n21b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1p5s7v5xayvnnz8941b1yc27l6vd344ig0a08d74j5qxrzp6jzzp") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21b-pac-0.2.2 (c (n "atsame70n21b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0nyaa9vf6195s2ka3ydmlaqkw9vk2lv7qbmgksj0gmxk1pbdgw52") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n21b-pac-0.3.0 (c (n "atsame70n21b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1hbm237hwcddpiv1cvfsmf636153zsdih2jgjlyzig9f7b21yhk9") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

