(define-module (crates-io at sa atsam4ls2a-pac) #:use-module (crates-io))

(define-public crate-atsam4ls2a-pac-0.1.0 (c (n "atsam4ls2a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1rsxwslzpy2arawicvkxj6sma6gxiv1czq5gj2w0v14h6xwmfx3a") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2a-pac-0.2.0 (c (n "atsam4ls2a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zqj3x6xv05rfbxrm50a7vc1gvky99bkzb3j3gqxbqgjgk5nk531") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2a-pac-0.2.1 (c (n "atsam4ls2a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0ivf6dlys50ka4b6c3fw39g8qlj7vqsdbg1af6hmjbw3xypipshr") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2a-pac-0.2.2 (c (n "atsam4ls2a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00gl9fk1z5qc7ridasps5pxm6shihjd6j8kk8nyzlcmmywq7x66m") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls2a-pac-0.3.0 (c (n "atsam4ls2a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0yxgwg37hxjlxj73fnsi4h9knwks3wrsg54rj3zqb6206kjj75r0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2a-pac-0.3.1 (c (n "atsam4ls2a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m6fz5s92n3zamdfqj7wgmpim4zrppfj6v5hxbsvzs9a8ihzkri5") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls2a-pac-0.3.2 (c (n "atsam4ls2a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0m49vjfwn5y91wkw0w6rp0yz2ci5jfrd6nhif8whvl0bdzknlfml") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

