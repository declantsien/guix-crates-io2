(define-module (crates-io at sa atsam4lc8b-pac) #:use-module (crates-io))

(define-public crate-atsam4lc8b-pac-0.1.0 (c (n "atsam4lc8b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0qsxrwl0i4s538g46s7krvnggnq0i4v4wr80667p1bwfh07g9miw") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8b-pac-0.2.0 (c (n "atsam4lc8b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0g2sgzd1jaql5frfrf2ky1wxjf3jg61llmfc8y8q5lbay51glp6q") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8b-pac-0.2.1 (c (n "atsam4lc8b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kci1sd538yj54srgw4j7q8a5rqnimaxmj6x8ifdd7mkk208zia9") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8b-pac-0.2.2 (c (n "atsam4lc8b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1a4jsw60nnn6sfbkjbrf560y1fikwyhffcfkry2gklq68f0g80xl") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4lc8b-pac-0.3.0 (c (n "atsam4lc8b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0cs3br8q0vpzlfhwb0xvzhqna8psww66zrd919yg4qp3jicvvaps") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8b-pac-0.3.1 (c (n "atsam4lc8b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0dpngbgz0b5y8z29mr8hlk7jmkgq6z88fysqv733k29v8cy9q3b9") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4lc8b-pac-0.3.2 (c (n "atsam4lc8b-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1509qvaxad8vx45j26r147ayblsb5qyycrss695vjsadd7zirjcd") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

