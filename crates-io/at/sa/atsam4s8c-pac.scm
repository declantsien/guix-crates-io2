(define-module (crates-io at sa atsam4s8c-pac) #:use-module (crates-io))

(define-public crate-atsam4s8c-pac-0.1.1 (c (n "atsam4s8c-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1rcc3fl8z5bcz361v4bg2lf5cmcw6n3rd1sldqai78z8jjcvfiri") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.1.2 (c (n "atsam4s8c-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00ywv8lslj37cpm47r281v76h4cgyphf8d9aa5rxfzm4yaxl0ynx") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.1.3 (c (n "atsam4s8c-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1pxyaahdm9ysx8lg594gk2wq3icn6s45795jhaji925khagfk6pf") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.2.0 (c (n "atsam4s8c-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "117clrnzz6mf9gx0jafl5f31l2rl8aw016hbbl2ms3hx25z4pkgg") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.2.1 (c (n "atsam4s8c-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gpwibjsp7wrxx6ckmqg8gr51rjb31c5zv5m57xs45llczi62dnh") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.2.2 (c (n "atsam4s8c-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rj7949p6l53vahnms46kz7052cpf9rsiwccxk0fm975mmxial52") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s8c-pac-0.3.0 (c (n "atsam4s8c-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1knssn6arr8g8nm22y8m1q2s31s17sd6c5yhbhy8w6j0sisdhljl") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s8c-pac-0.3.1 (c (n "atsam4s8c-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0vv1ppqa32acn11c07f4064svzahjq8ih8iz441825rxvcwqvrix") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

