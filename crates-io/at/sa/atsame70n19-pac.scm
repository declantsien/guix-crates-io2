(define-module (crates-io at sa atsame70n19-pac) #:use-module (crates-io))

(define-public crate-atsame70n19-pac-0.1.0 (c (n "atsame70n19-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gxhwrjiq0gnll7hcdnx71j2ii122adpsrc6yhfi45aqmx5c40yc") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19-pac-0.2.0 (c (n "atsame70n19-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0wjlw0ijfy23zwy65h8p1xrqdypdjkbkmyv8s4zslzdsqjlpiwrz") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19-pac-0.2.1 (c (n "atsame70n19-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12dacks696b8scn7yx4dz3xr9rcy56izwwc02fglwxrai9j1nr4y") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19-pac-0.2.2 (c (n "atsame70n19-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zi6anq5k7xzlia0xmaq5xdhp40hi0sin6lf41hxdw8finqx5ywm") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19-pac-0.3.0 (c (n "atsame70n19-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w5gzm5kizsq57p7liifvpflg1as7lav6c6ip19y97h9b8cqxzly") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

