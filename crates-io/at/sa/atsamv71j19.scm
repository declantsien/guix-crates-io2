(define-module (crates-io at sa atsamv71j19) #:use-module (crates-io))

(define-public crate-atsamv71j19-0.21.0 (c (n "atsamv71j19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0ply32wl6zmc4342cs7kkiz10gl5bvi6p8djpwb9y0dxz3szm34b") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

