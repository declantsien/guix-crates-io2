(define-module (crates-io at sa atsaml21e17b) #:use-module (crates-io))

(define-public crate-atsaml21e17b-0.1.1 (c (n "atsaml21e17b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0nj3sqw34cqipbiafhp02scz3dmd6j0wzyh0j74x31kpi9jzb1ac") (f (quote (("rt" "cortex-m-rt/device"))))))

