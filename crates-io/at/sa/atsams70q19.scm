(define-module (crates-io at sa atsams70q19) #:use-module (crates-io))

(define-public crate-atsams70q19-0.0.1 (c (n "atsams70q19") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "19anm055933haql86nz7va7jcllikxvyx67ah3yvjdcgqh9z1jdn") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q19-0.0.2 (c (n "atsams70q19") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "194x2dyya26b1bvipkwk1ln5j3681isckhdh4mlwnj9khp6hsp5l") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70q19-0.21.0 (c (n "atsams70q19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1q4rb56v15hvflk05dak80hz3k4dsvlbq8ds8a5vyxbb2wichw26") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

