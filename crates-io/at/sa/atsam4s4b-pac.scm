(define-module (crates-io at sa atsam4s4b-pac) #:use-module (crates-io))

(define-public crate-atsam4s4b-pac-0.1.1 (c (n "atsam4s4b-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1d48cpk29z4hfgg5yljwz2iq655qw0d2wqmmkndiidsaxhyiv0g6") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.1.2 (c (n "atsam4s4b-pac") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1nsnx1fzmq74596ndrz6mbghrz7xcxvxb05ji4b9nh969hpk7qrv") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.1.3 (c (n "atsam4s4b-pac") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "00pq83mcjhbmzn1yaqflg0146ik6hsj0yc6s5mb42h01512z6mji") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.2.0 (c (n "atsam4s4b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "01dmhdxsjq7safqifcddlyrxk5vf5l1x2fx1byzfd41jq1s2xpq8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.2.1 (c (n "atsam4s4b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1x64mxw27j519rxwqx02hlmnd5cvcf7nphqhs5j4hnd4ba2091c8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.2.2 (c (n "atsam4s4b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zlv4y7h797jmivszs0ccxjs4xskhbqbyidb77ab5dlskp2ka3yb") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4s4b-pac-0.3.0 (c (n "atsam4s4b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "14d29f4lky099an8b7m0wm99lcwnxl9gin3xns6hgsi2n3c1akp0") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4s4b-pac-0.3.1 (c (n "atsam4s4b-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "151q38wzzd24b1cqzq4skcxwhbwi87v50kwvrrnp92vzqcx24dvg") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

