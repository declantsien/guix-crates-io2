(define-module (crates-io at sa atsam4ls4a-pac) #:use-module (crates-io))

(define-public crate-atsam4ls4a-pac-0.1.0 (c (n "atsam4ls4a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1fj6v5sqsranpl19jmwkmivv8kkk4d9lrx0l961j8c9ans9gq0a1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4a-pac-0.2.0 (c (n "atsam4ls4a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1q2r2asbqkkdari4xksq1v2sj3kqrd33r2rkpxyv0b0y8wv3n24b") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4a-pac-0.2.1 (c (n "atsam4ls4a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0chasl6nq8lx1l4lxmq0a9wznpbaxif2n8hx5vdarmh60v4jzf0p") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4a-pac-0.2.2 (c (n "atsam4ls4a-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "16639g6dia784kbv7h08wjrln6c9kgrvxlhyf4hpaq0y7cnpzk0r") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsam4ls4a-pac-0.3.0 (c (n "atsam4ls4a-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ly5cr302nh4sfvmwwfpa1w15lpfgkizk5g5426bb01mglx101gz") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4a-pac-0.3.1 (c (n "atsam4ls4a-pac") (v "0.3.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1z2by8z99d814q6xr8nz1x6laacdlf7chlsl1gdxvylwky8anlbg") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-atsam4ls4a-pac-0.3.2 (c (n "atsam4ls4a-pac") (v "0.3.2") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vzdp4dq1v5ayzgn4h1rjw9n8mlz9kknxi81xbr2lkni4ggf9avx") (f (quote (("rt" "cortex-m-rt/device")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

