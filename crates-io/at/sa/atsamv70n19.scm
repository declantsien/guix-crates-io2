(define-module (crates-io at sa atsamv70n19) #:use-module (crates-io))

(define-public crate-atsamv70n19-0.21.0 (c (n "atsamv70n19") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0gwnzxjdy0z0mhjr6b7ysfdb68l7lfy6si9h9s9qbybc29kfdigf") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

