(define-module (crates-io at sa atsame70n19b-pac) #:use-module (crates-io))

(define-public crate-atsame70n19b-pac-0.1.0 (c (n "atsame70n19b-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "01dq8insafd2vx7igza38kka347is3yx2315m9wcs8257zx6dj5d") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19b-pac-0.2.0 (c (n "atsame70n19b-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0z38prv3pgrh631vbxpywnazdbvx72kqm3mdhfn1jkpvr0y4xsrh") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19b-pac-0.2.1 (c (n "atsame70n19b-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1cb9k5n0844h1y1rjf5jk8p3ang9dfbfdf2m952355wm33kp91cv") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19b-pac-0.2.2 (c (n "atsame70n19b-pac") (v "0.2.2") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1vjc2yip6bcxs16n2i2hpv5if6wjqs0wgh267n5hh2wrw7x9i67k") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame70n19b-pac-0.3.0 (c (n "atsame70n19b-pac") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0k1m500k2q231187h1w0n43pavjicflydxcif4yxwglbf03ch4d8") (f (quote (("rt" "cortex-m-rt/device")))) (y #t) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

