(define-module (crates-io at sa atsame51g18a-pac) #:use-module (crates-io))

(define-public crate-atsame51g18a-pac-0.1.0 (c (n "atsame51g18a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1c944jvl0iaab53py60ilad8xvnr7acx3sw4sy3v0jh5r7y18vmf") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51g18a-pac-0.2.0 (c (n "atsame51g18a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00bxy2qb6bbcgmm3y2f9ddmzm9viisr7apsrfsql8g51zh9gsr94") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame51g18a-pac-0.2.1 (c (n "atsame51g18a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0hsi9ymsbnl5yac0a60is6vpl8yyg2340b9s87j9i4srpfvk0935") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

