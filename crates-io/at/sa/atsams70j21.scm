(define-module (crates-io at sa atsams70j21) #:use-module (crates-io))

(define-public crate-atsams70j21-0.0.1 (c (n "atsams70j21") (v "0.0.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "000dp1qif3bd9xmab723vxdfsskay7832xi2zj71xsbj19s4sfs5") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j21-0.0.2 (c (n "atsams70j21") (v "0.0.2") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1kjngnrn9k1k76vpq8ldr90xspglzkphwk3s0smwjj2wjvbr16y1") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsams70j21-0.21.0 (c (n "atsams70j21") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "05k6v25gs97if52srqissjrbrx6c7jyam2ijiiansddn7x4zagml") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

