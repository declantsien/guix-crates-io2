(define-module (crates-io at sa atsaml21g16b) #:use-module (crates-io))

(define-public crate-atsaml21g16b-0.1.1 (c (n "atsaml21g16b") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0p2jbbbci0m9cik7hyxnx742a4b48fnl26nbsr68ak4vh7vxibgz") (f (quote (("rt" "cortex-m-rt/device"))))))

