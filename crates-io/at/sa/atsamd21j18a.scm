(define-module (crates-io at sa atsamd21j18a) #:use-module (crates-io))

(define-public crate-atsamd21j18a-0.5.0 (c (n "atsamd21j18a") (v "0.5.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "19w6sgr1fdcfwgmrdbrr9rzfnr86hj3gl1c6b48k5l2s6dsccfs8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j18a-0.6.0 (c (n "atsamd21j18a") (v "0.6.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0r0vabrbql6y6z8yij8mpa76zdr61z37zvnclhxfvlv15khv8hpk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j18a-0.7.0 (c (n "atsamd21j18a") (v "0.7.0") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1x2h5r7amm8i3bvmdl9dibfhpgizj7spj1dililpq64f8pr59qxk") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-atsamd21j18a-0.7.1 (c (n "atsamd21j18a") (v "0.7.1") (d (list (d (n "bare-metal") (r "~0.2") (d #t) (k 0)) (d (n "cortex-m") (r "~0.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0bxlp604x8ln346x8d3z16bjhp3rskdbv0kk6j6zbah7658h9a28") (f (quote (("rt" "cortex-m-rt/device"))))))

