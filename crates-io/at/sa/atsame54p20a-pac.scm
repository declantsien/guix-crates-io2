(define-module (crates-io at sa atsame54p20a-pac) #:use-module (crates-io))

(define-public crate-atsame54p20a-pac-0.1.0 (c (n "atsame54p20a-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1qxgsvwhgjnvhdqxhd5782zakibsy9xrr1imgkgaslyjlckivvpk") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54p20a-pac-0.2.0 (c (n "atsame54p20a-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "155dj04l5lsvmyzmz3m0hny2z69f3a22fkkgrz6wcmjgiarxja6z") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-atsame54p20a-pac-0.2.1 (c (n "atsame54p20a-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.5") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0gzbwagdz70g2npzdwqxn79irlpqjxvdjygk07nhr2gd3rivz0f8") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

