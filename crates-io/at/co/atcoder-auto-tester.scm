(define-module (crates-io at co atcoder-auto-tester) #:use-module (crates-io))

(define-public crate-atcoder-auto-tester-0.9.0 (c (n "atcoder-auto-tester") (v "0.9.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "inotify") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0m5vi02d2s3j01lypn0ms31cwgglm6399bv2mmrr1frvbc26f2vz")))

(define-public crate-atcoder-auto-tester-0.9.1 (c (n "atcoder-auto-tester") (v "0.9.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "inotify") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dw7w14a4c5y9l75921hjgdh520xn9bz5h6q8lm94i3gqp9kslhg")))

(define-public crate-atcoder-auto-tester-0.9.2 (c (n "atcoder-auto-tester") (v "0.9.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "inotify") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1hzv5k4hps8jw9sssgp13sf5lsriq835k8712vlw2ndsfv9mhh3x")))

