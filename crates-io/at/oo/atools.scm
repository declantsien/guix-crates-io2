(define-module (crates-io at oo atools) #:use-module (crates-io))

(define-public crate-atools-0.1.0 (c (n "atools") (v "0.1.0") (h "1123k3hi5nhmlhlwdnw4gwy14j7ndlghpb9kn1yh4rrhc9iij07y")))

(define-public crate-atools-0.1.1 (c (n "atools") (v "0.1.1") (h "1fc2a8dn6vbvbbk02gblrw2m6jmm3mzir57mzmcxf7vg0v3vqjnh")))

(define-public crate-atools-0.1.2 (c (n "atools") (v "0.1.2") (h "0mczvgrzvmqcxmqsl6811qx6pms4yz3l9zw18v4d2jpz4avfpp5n")))

(define-public crate-atools-0.1.3 (c (n "atools") (v "0.1.3") (h "1qps9zwyx4n065qcg0wsj4rn97ad20dfcm78ppds4djvkcj9kswq") (y #t)))

(define-public crate-atools-0.1.4 (c (n "atools") (v "0.1.4") (h "0rlgwqfyws2r085jik22il8yrfxjvkxmsm90nqn49cbmyb7nihyq")))

