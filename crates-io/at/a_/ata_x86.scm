(define-module (crates-io at a_ ata_x86) #:use-module (crates-io))

(define-public crate-ata_x86-0.1.0 (c (n "ata_x86") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "103krvkl50p0j2j7yl1391k10i5v3xz9gadfql60m5g1j1rcl359")))

(define-public crate-ata_x86-0.1.1 (c (n "ata_x86") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (k 0)) (d (n "spin") (r "^0.9.0") (d #t) (k 0)))) (h "1fgd1c0h6jlymp3r6isa4ars58xcdm74844508axv1v9jjk4g5nc")))

