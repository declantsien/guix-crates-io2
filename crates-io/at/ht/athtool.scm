(define-module (crates-io at ht athtool) #:use-module (crates-io))

(define-public crate-athtool-0.1.0 (c (n "athtool") (v "0.1.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "oath") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.1.27") (k 0)))) (h "0b03p7xsvaqdz4snnhkhbs56yp3bgp7m7hwidsmzmnvdnszpxpi8")))

