(define-module (crates-io at ro atrocious_sort) #:use-module (crates-io))

(define-public crate-atrocious_sort-0.1.0 (c (n "atrocious_sort") (v "0.1.0") (h "1i2d8w3p8b5c2nnx4bnbcgzzbfn8rj6lkqnxc4mnmpqps7ima4ag")))

(define-public crate-atrocious_sort-0.1.1 (c (n "atrocious_sort") (v "0.1.1") (h "174n3ysv5vvv9gp1zaf0sj7v9w7hin4kb3f8gkajifi44xlmskks")))

(define-public crate-atrocious_sort-0.1.2 (c (n "atrocious_sort") (v "0.1.2") (h "0qylxrgrn9hcsg98mwvssn4rhw73b2x9nrvkwzl8fbs8abzpfxfx")))

