(define-module (crates-io at tr attrsets) #:use-module (crates-io))

(define-public crate-attrsets-0.1.0 (c (n "attrsets") (v "0.1.0") (d (list (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0b9j5lmzzbj2gbbqw11zgjcsgjnicsqrkx6ygbhmm2jpdzd5df5p")))

(define-public crate-attrsets-0.1.1 (c (n "attrsets") (v "0.1.1") (d (list (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bkk2b28jbgnlz2f9hsylj891pxkmrq4hp9659xchwrj6yrl9sdz")))

(define-public crate-attrsets-0.1.2 (c (n "attrsets") (v "0.1.2") (d (list (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p7qzqg68kxf12bgl37yxdznvhc91km77a2bbgfkl39ycj70zdf4")))

