(define-module (crates-io at tr attr_alias) #:use-module (crates-io))

(define-public crate-attr_alias-0.1.0 (c (n "attr_alias") (v "0.1.0") (h "01qjyaijpn2f49rmy9i08p9jada842idg26vw3pipsy8d2fv5fm4") (f (quote (("nightly")))) (r "1.70.0")))

(define-public crate-attr_alias-0.1.1 (c (n "attr_alias") (v "0.1.1") (h "0n19q49zbhqyg4jczarl0d968wwm23snkn9kh2h6xmz5nyzvmfn1") (f (quote (("nightly")))) (r "1.70.0")))

(define-public crate-attr_alias-0.1.2 (c (n "attr_alias") (v "0.1.2") (h "1k2ra4qliyp1gkh6qc55l85kssnl64aad4jj6yrj4j5nvkyb66wi") (f (quote (("nightly")))) (r "1.70.0")))

