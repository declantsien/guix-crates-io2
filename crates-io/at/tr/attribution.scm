(define-module (crates-io at tr attribution) #:use-module (crates-io))

(define-public crate-attribution-0.0.0 (c (n "attribution") (v "0.0.0") (h "02y1czc7066nl7g0mi4pdqaq0wwxadcgzhjak5rf1gng5d0qghv3")))

(define-public crate-attribution-0.1.0 (c (n "attribution") (v "0.1.0") (d (list (d (n "attribution-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (d #t) (k 0)))) (h "1ifzg1zvshirrl9z6zmay5q9sfl8ki7cylvv7zwb1fwmn8i6w00r")))

(define-public crate-attribution-0.2.0 (c (n "attribution") (v "0.2.0") (d (list (d (n "attribution-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 2)) (d (n "shrinkwraprs") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.41") (d #t) (k 0)))) (h "1axk79g3z87axvjpsl6akfgrvd14c21nmi3aiw2vqi4qrca2lywj")))

(define-public crate-attribution-0.2.1 (c (n "attribution") (v "0.2.1") (d (list (d (n "attribution-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kzw3x3xjg1rv9pp3bhzm4gpfha8j4m11my5kr0njbymfa4bcxgy")))

(define-public crate-attribution-0.3.0 (c (n "attribution") (v "0.3.0") (d (list (d (n "attribution-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1miggaiy4xcm8h841g55g1fb0wd26ykm26ccn1jwi1skczzb2rij")))

(define-public crate-attribution-0.4.0 (c (n "attribution") (v "0.4.0") (d (list (d (n "attribution-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1pd8kbri9qc2dh2bhjqx2k1ksgw6rg7xad21faxagqg8ivgnfb0v")))

(define-public crate-attribution-0.5.0 (c (n "attribution") (v "0.5.0") (d (list (d (n "attribution-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0xddxn3zgfi01zi602ki3w77bmqicw2qp8cg03bki5vjfj0vjrdb")))

