(define-module (crates-io at tr attributes) #:use-module (crates-io))

(define-public crate-attributes-0.0.1 (c (n "attributes") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.71") (f (quote ("full"))) (d #t) (k 0)))) (h "0v2r9mxk4v21n4ws1svypn9gz0y6xczkpbpympzpai2mmgq3xk6a")))

(define-public crate-attributes-0.0.2 (c (n "attributes") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "13cwz26yzfq5xgj07hi47yi1spr5ja67r6269rrdr9l8fsyzqa3v") (y #t)))

(define-public crate-attributes-0.0.3 (c (n "attributes") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "0a54g89p5cm1wm08jjkvyc4wz77jkiljva4q87fh6ikf8nssn61v")))

(define-public crate-attributes-0.0.4 (c (n "attributes") (v "0.0.4") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "11i9fkc8rm9hyrbxbajkf75qqwafk1288hp6im0nn9dgs9rh0kix")))

(define-public crate-attributes-0.0.5 (c (n "attributes") (v "0.0.5") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "0h6z4w3pqdfggx4k8pyl8n1hf5w8rxm9va2n1qldwyif32ffnwry")))

(define-public crate-attributes-0.0.6 (c (n "attributes") (v "0.0.6") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "1j678wjxnk5id1fdlxyamjn0l6xc2xyvnfzxni199bc357h98sak")))

(define-public crate-attributes-0.0.7 (c (n "attributes") (v "0.0.7") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "02b2p6l9738mp6vax6jq2pz7v0cjsyz7hysh23gqlv3zx35313n1")))

(define-public crate-attributes-0.0.8 (c (n "attributes") (v "0.0.8") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "0zaqkxh6bl69gfcgzg6r7dr614m4gq79sfmkasn39vp0nykz8bsj")))

