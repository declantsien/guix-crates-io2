(define-module (crates-io at tr attribution-macros) #:use-module (crates-io))

(define-public crate-attribution-macros-0.1.0 (c (n "attribution-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1nz103iwp0ra0xlxdq93qfywlaibb24l5mc8c474yg5d3z150zbb")))

(define-public crate-attribution-macros-0.2.0 (c (n "attribution-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0llxqkfgg60zl4p2nf5hpa4z405mqikq9rm0im7jwzgngkawh7ah")))

(define-public crate-attribution-macros-0.2.1 (c (n "attribution-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dhnwdi7s4n3snysx9hy95r9kqq44x1k7yvnbc7s7m8zg1hg4kjz")))

(define-public crate-attribution-macros-0.3.0 (c (n "attribution-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10hq9mnlgygia7iv53xgh8smz7s6jlgshb2gwd5mb4c937m5l77j")))

(define-public crate-attribution-macros-0.4.0 (c (n "attribution-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "0rpvmys0xq35jxkz22ql679v3mazmsxmw3vch94fn9x7wwjp2ls4")))

(define-public crate-attribution-macros-0.5.0 (c (n "attribution-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("extra-traits" "full"))) (d #t) (k 2)))) (h "183ssccnyb1zyhyl6gzgp5ym5p7j0anx43kzkcdpz4v6hlyl7ips")))

