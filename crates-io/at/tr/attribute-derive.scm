(define-module (crates-io at tr attribute-derive) #:use-module (crates-io))

(define-public crate-attribute-derive-0.1.0 (c (n "attribute-derive") (v "0.1.0") (d (list (d (n "attribute-derive-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wzcj0j7cbhzbhj4xy6as72rrfglb71x3p3jv9qgykzk1ph1kjp1")))

(define-public crate-attribute-derive-0.1.1 (c (n "attribute-derive") (v "0.1.1") (d (list (d (n "attribute-derive-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1alaa4035qb5a1ns0p7mxykvhm77wxa0k6paiwvz6f0zas0bnjrm")))

(define-public crate-attribute-derive-0.2.1 (c (n "attribute-derive") (v "0.2.1") (d (list (d (n "attribute-derive-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1khsfpq5py8sq1zvkzw7dr3h9fnf1r7g7bxv9aax723i46mwjlnl")))

(define-public crate-attribute-derive-0.2.2 (c (n "attribute-derive") (v "0.2.2") (d (list (d (n "attribute-derive-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1f71fgbpkvg31wlnfgmrjrc8d2s9d9gq6vpjva4h3fd1yam72899")))

(define-public crate-attribute-derive-0.3.0 (c (n "attribute-derive") (v "0.3.0") (d (list (d (n "attribute-derive-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dx0gyi154v4hs3i2h5v79l8q64d9ldclcfgl9afdbq14dr86d8x")))

(define-public crate-attribute-derive-0.3.1 (c (n "attribute-derive") (v "0.3.1") (d (list (d (n "attribute-derive-macro") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1nv0vcp9cz8hnhf18lws27mpnbcdc9pm39q82rs3zmgzc83y9mz5")))

(define-public crate-attribute-derive-0.4.0 (c (n "attribute-derive") (v "0.4.0") (d (list (d (n "attribute-derive-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ly737iln0hb82v5q417fkmi9syhdy0m41ymi0m90xx3djbl7f51") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.5.0 (c (n "attribute-derive") (v "0.5.0") (d (list (d (n "attribute-derive-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xfx1xmg89gr8j0psbn444p636c62y0paxaxqy7xz1cmv66jjq1y") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.6.0 (c (n "attribute-derive") (v "0.6.0") (d (list (d (n "attribute-derive-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "186bqg8xnpqvxcsv41ya88x0xwn1bhkmip4l9j05b652j9sjfbi3") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.6.1 (c (n "attribute-derive") (v "0.6.1") (d (list (d (n "attribute-derive-macro") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1624ddi0bsk9ccrnbcn1rc5pkjd5s452swijn43h2rsfvqmg2961") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.7.0 (c (n "attribute-derive") (v "0.7.0") (d (list (d (n "attribute-derive-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xv8sf7qkq0dfhfwdcsyrz3vnwg1h8c0xqx02lag20ad5h72lpyp") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.7.1 (c (n "attribute-derive") (v "0.7.1") (d (list (d (n "attribute-derive-macro") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gsdmhvw3gbg8zfnycdx2l3zp3dk2xgyi4l6afx69sd5hkfm2s43") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.8.0 (c (n "attribute-derive") (v "0.8.0") (d (list (d (n "attribute-derive-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0c1yvb8dpl855jaja9lsmfi4631n6bw4yambbksf5vqdxiqqkhq6") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.8.1 (c (n "attribute-derive") (v "0.8.1") (d (list (d (n "attribute-derive-macro") (r "^0.8.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0yq797ac0rmd2687mflsshdsdpl59pwbyim0vsqxl9bgvqzg950c") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.9.0 (c (n "attribute-derive") (v "0.9.0") (d (list (d (n "attribute-derive-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "derive-where") (r "^1.2.5") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "175v972hm7qgdy0ahimm7gbnkv5cvn9xgf598hd5pnyfzk5nhn40") (f (quote (("syn-full" "syn/full"))))))

(define-public crate-attribute-derive-0.9.1 (c (n "attribute-derive") (v "0.9.1") (d (list (d (n "attribute-derive-macro") (r "^0.9.1") (d #t) (k 0)) (d (n "derive-where") (r "^1.2.5") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "manyhow") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "16rx5l5npd6d1q56fk925d72pzirw70czgprbz0p8svx6f5q0j4b") (f (quote (("syn-full" "syn/full"))))))

