(define-module (crates-io at tr attr-parser-fn) #:use-module (crates-io))

(define-public crate-attr-parser-fn-0.1.0 (c (n "attr-parser-fn") (v "0.1.0") (d (list (d (n "impl_variadics") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1an7jwxlmhyvbnpvqch9d7hdly9ak5j72w3bvzf2ak03lhp69sw5")))

(define-public crate-attr-parser-fn-0.2.0 (c (n "attr-parser-fn") (v "0.2.0") (d (list (d (n "impl_variadics") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k8yf6lm6xnby3v4qshw47044w273s5khj775nmgwak82jvzn4yg")))

