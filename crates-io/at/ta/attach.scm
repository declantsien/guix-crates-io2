(define-module (crates-io at ta attach) #:use-module (crates-io))

(define-public crate-attach-0.0.0 (c (n "attach") (v "0.0.0") (h "13s9ccj53vk7z6cmg3xan6w7xfs476ijr61bsiqp02n0s2jzsk7q")))

(define-public crate-attach-0.1.0 (c (n "attach") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Diagnostics_Debug" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (k 0)))) (h "1c30x2sikwmd5nwb6s0jh2zdlxpglgd2d6552hp4kbfsh1p4ik6r")))

