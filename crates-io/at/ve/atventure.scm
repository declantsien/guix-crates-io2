(define-module (crates-io at ve atventure) #:use-module (crates-io))

(define-public crate-atventure-0.1.2 (c (n "atventure") (v "0.1.2") (h "0ri7b8rasvnkhkiha9rs24c5bghbpzq8sipdkjawj5r46sbb6fxz")))

(define-public crate-atventure-0.1.3 (c (n "atventure") (v "0.1.3") (h "0d7fiz7jh1g2gn1df7pfaa5ld7yprkjz9vqs8aqmb6jxqn72w3r0")))

