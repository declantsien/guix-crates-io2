(define-module (crates-io at ri atri_macros) #:use-module (crates-io))

(define-public crate-atri_macros-0.1.0 (c (n "atri_macros") (v "0.1.0") (h "1c5x7qh407dczk2bkgxyy6h480zccags0i542zka1q73rv3qhgcw")))

(define-public crate-atri_macros-0.1.1 (c (n "atri_macros") (v "0.1.1") (h "1zaad4a4z7gi40farvy9rbi2p7l01y1byq8j6xqkmv7w8gjyb7cz")))

(define-public crate-atri_macros-0.1.2 (c (n "atri_macros") (v "0.1.2") (h "17rv2pn8xrfdgzvgmf02pjnj6ldnp21h3h2vcc0yccf5hr1s8n1x")))

(define-public crate-atri_macros-0.2.0 (c (n "atri_macros") (v "0.2.0") (h "13nlxr2azvk19kzbzmxna40mlfykikzp96jq2c4xfiji861dy5nf")))

(define-public crate-atri_macros-0.2.1 (c (n "atri_macros") (v "0.2.1") (h "0k2zxxf6ajq3ybbv19w9rszyi15w5r574li8yixdbwpgaiyanmp6")))

(define-public crate-atri_macros-0.3.0 (c (n "atri_macros") (v "0.3.0") (h "1dsrhr9wxwdnqg7hrv4bjgw1d29345xlhqr9n1wj23n9cw2d2n14")))

