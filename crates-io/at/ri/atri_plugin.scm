(define-module (crates-io at ri atri_plugin) #:use-module (crates-io))

(define-public crate-atri_plugin-0.1.0 (c (n "atri_plugin") (v "0.1.0") (d (list (d (n "atri_ffi") (r "^0") (d #t) (k 0)) (d (n "atri_macros") (r "^0") (d #t) (k 0)))) (h "1bl761qzxfsvz3ijc11rikzq1ksvy0vsycp6349ig533cf2wxmf6")))

(define-public crate-atri_plugin-0.2.0 (c (n "atri_plugin") (v "0.2.0") (d (list (d (n "atri_ffi") (r "^0.2") (d #t) (k 0)) (d (n "atri_macros") (r "^0") (d #t) (k 0)))) (h "16kds5lx7bz0jv11gyd0alvgizn0ylcb5q9hdwyy6awi7hdcr3m1")))

(define-public crate-atri_plugin-0.2.1 (c (n "atri_plugin") (v "0.2.1") (d (list (d (n "atri_ffi") (r "^0.2.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0") (d #t) (k 0)))) (h "1gnsls0w5j8mh9gbb58r3cga5ax3c84m60qv1kghn7aqxplpl9xk")))

(define-public crate-atri_plugin-0.2.4 (c (n "atri_plugin") (v "0.2.4") (d (list (d (n "atri_ffi") (r "^0.2.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0g85bf7hmz5zsyzkp3akf3xdf7pxxfff4kryawmqh3xhn7s87k9b")))

(define-public crate-atri_plugin-0.2.5 (c (n "atri_plugin") (v "0.2.5") (d (list (d (n "atri_ffi") (r "^0.2.2") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.0") (d #t) (k 0)))) (h "18pj3qlx5spxmfdvqwk9k7zhbld9xqslnkfqvcd2iv67zslsh8ra")))

(define-public crate-atri_plugin-0.2.6 (c (n "atri_plugin") (v "0.2.6") (d (list (d (n "atri_ffi") (r "^0.2.2") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0xj2nz85cpviqv10yq6cxzd03dnq8jyvlj6hfc3qhhgss9gcpm1h")))

(define-public crate-atri_plugin-0.2.7 (c (n "atri_plugin") (v "0.2.7") (d (list (d (n "atri_ffi") (r "^0.2.3") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1w3bxjwiqcg0n3flipwbk3p1bdn0lqjg1s4mihiiw6x1dvxqycml")))

(define-public crate-atri_plugin-0.2.8 (c (n "atri_plugin") (v "0.2.8") (d (list (d (n "atri_ffi") (r "^0.2.4") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1dz9wmlydgsjl6cpvll2d7gcwy97y58bijdz3fnmaj7w8byd33wp")))

(define-public crate-atri_plugin-0.2.9 (c (n "atri_plugin") (v "0.2.9") (d (list (d (n "atri_ffi") (r "^0.2.4") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0fxpacg5gzjqn8iqjwxldsziakygnm391q6a5hnq6kw670warayx")))

(define-public crate-atri_plugin-0.2.10 (c (n "atri_plugin") (v "0.2.10") (d (list (d (n "atri_ffi") (r "^0.2.4") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "02v3w8s14ws8wxj7fx858php59sjfili7dxmjw95c0b62ik0q8i0")))

(define-public crate-atri_plugin-0.2.11 (c (n "atri_plugin") (v "0.2.11") (d (list (d (n "atri_ffi") (r "^0.2.5") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1h11hcdnk476i6494ipnva7rvrym8mzp6259jji1f7mr4s215n0g")))

(define-public crate-atri_plugin-0.2.12 (c (n "atri_plugin") (v "0.2.12") (d (list (d (n "atri_ffi") (r "^0.2.5") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1y0fdbbvalwbxgr9rpg53l6z227yfr82il44ijw9b1yq5my41lb4")))

(define-public crate-atri_plugin-0.2.13 (c (n "atri_plugin") (v "0.2.13") (d (list (d (n "atri_ffi") (r "^0.2.5") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1n32gw6nwr5vv4qx4b452g7w06vag2aykikgfwwy5i9aj2fml7ia")))

(define-public crate-atri_plugin-0.2.14 (c (n "atri_plugin") (v "0.2.14") (d (list (d (n "atri_ffi") (r "^0.2.5") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0dyq9c6hq2931dka632a6jcz1x46y2nmlhlymn28nmm85rh1zshm")))

(define-public crate-atri_plugin-0.2.15 (c (n "atri_plugin") (v "0.2.15") (d (list (d (n "atri_ffi") (r "^0.2.7") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "07ny32h0hnx1sginmzmrdfiwc5rpb3i3bz1h3pwzbm48pa1wigpa")))

(define-public crate-atri_plugin-0.2.17 (c (n "atri_plugin") (v "0.2.17") (d (list (d (n "atri_ffi") (r "^0.2.7") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0jmisl7fp383dhr09ra1psm3xp15rs4sx3cbam03q40cchhaa215")))

(define-public crate-atri_plugin-0.2.18 (c (n "atri_plugin") (v "0.2.18") (d (list (d (n "atri_ffi") (r "^0.2.7") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "00c1ifa61kirfk3qz73ygzxb7bpc2jv9z215k656s7fmcir4iqdf")))

(define-public crate-atri_plugin-0.2.19 (c (n "atri_plugin") (v "0.2.19") (d (list (d (n "atri_ffi") (r "^0.2.7") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "07qifxnrqwynfqpip1qc1s4bhbviiq1ai63sh0d0wav7h9xnf5a7")))

(define-public crate-atri_plugin-0.2.21 (c (n "atri_plugin") (v "0.2.21") (d (list (d (n "atri_ffi") (r "^0.2.9") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0pxyyj80g62shz58nd0cj16lrm1fympkvyj81aakayrcz71sndjs")))

(define-public crate-atri_plugin-0.2.22 (c (n "atri_plugin") (v "0.2.22") (d (list (d (n "atri_ffi") (r "^0.2.9") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1iaa8cv11vx3b026r2yyycp0s8wphqb2ilzg58fmiyjj7ib518br")))

(define-public crate-atri_plugin-0.3.0 (c (n "atri_plugin") (v "0.3.0") (d (list (d (n "atri_ffi") (r "^0.2.9") (d #t) (k 0)) (d (n "atri_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0ra4xxzk0sbv57vfzgc9xjyyi3zh2hm4f30man80fnsfhajri340")))

(define-public crate-atri_plugin-0.4.0 (c (n "atri_plugin") (v "0.4.0") (d (list (d (n "atri_ffi") (r "^0.3.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.0") (d #t) (k 0)))) (h "15w4avcx158bn8bzri5rriqlifrq58a3kw9gi8vxn2vvv9sgpyx5")))

(define-public crate-atri_plugin-0.4.1 (c (n "atri_plugin") (v "0.4.1") (d (list (d (n "atri_ffi") (r "^0.3.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0r3r1q6h2fi20q15gfkaasbx2f4zs0r7vzhzg7zzjysp8zc95k6q")))

(define-public crate-atri_plugin-0.5.0 (c (n "atri_plugin") (v "0.5.0") (d (list (d (n "atri_ffi") (r "^0.3.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0d9xlw0dpji7975940nnnsmhqi593al1fz6s7jayh4326dmzfz36")))

(define-public crate-atri_plugin-0.5.1 (c (n "atri_plugin") (v "0.5.1") (d (list (d (n "atri_ffi") (r "^0.3.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1qb6iwavl3mhzrryfz1aqp2p3lh081k0gk6yyjqkcnz4nv8sj8aw")))

(define-public crate-atri_plugin-0.5.2 (c (n "atri_plugin") (v "0.5.2") (d (list (d (n "atri_ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "06kqbp79qqrkn20q7hrr1f6lshpjn1kc6a77gv78ikrk6dh9pjbv")))

(define-public crate-atri_plugin-0.5.3 (c (n "atri_plugin") (v "0.5.3") (d (list (d (n "atri_ffi") (r "^0.4.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0md4wrnipyb0r22caddn4lhfllhm4fs75yzv128ny5r5wd884jda")))

(define-public crate-atri_plugin-0.6.0 (c (n "atri_plugin") (v "0.6.0") (d (list (d (n "atri_ffi") (r "^0.5.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0z80gax95424bz8qsmyvkf5isnjp12fhijgy37y3fh5mmhkdw2w2")))

(define-public crate-atri_plugin-0.6.1 (c (n "atri_plugin") (v "0.6.1") (d (list (d (n "atri_ffi") (r "^0.5.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1m89m1pldm7ph2xp7g5cjqcxqpy8mg2szx8a12k4y0miryc7hahs")))

(define-public crate-atri_plugin-0.7.0 (c (n "atri_plugin") (v "0.7.0") (d (list (d (n "atri_ffi") (r "^0.6.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1z2mrnm5vivxsi4gjd0msddhx9n0rhpkw8gpd9lw44fyzigklijd") (y #t)))

(define-public crate-atri_plugin-0.7.1 (c (n "atri_plugin") (v "0.7.1") (d (list (d (n "atri_ffi") (r "^0.6.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0c7b6w1cjwxq5p1yznx1hdnl2n6xsfy2sa45nk8cd0z7lqmk43mg")))

(define-public crate-atri_plugin-0.7.2 (c (n "atri_plugin") (v "0.7.2") (d (list (d (n "atri_ffi") (r "^0.6.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1xbr6qnvdm776kwf2y2pb6vxxwzqax9qh832pzdwf6523lknhqrg")))

(define-public crate-atri_plugin-0.8.0 (c (n "atri_plugin") (v "0.8.0") (d (list (d (n "atri_ffi") (r "^0.8.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1yhyhm1s4bi46nx8kg5x4g3w34l5bgab6y6kzdvixilzsyg58lv8")))

(define-public crate-atri_plugin-0.8.1 (c (n "atri_plugin") (v "0.8.1") (d (list (d (n "atri_ffi") (r "^0.8.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0rhb1ck93rd6wv42bq5xjqn0a4kbbngns0zkxjkf0jp028i1pc6i")))

(define-public crate-atri_plugin-0.8.2 (c (n "atri_plugin") (v "0.8.2") (d (list (d (n "atri_ffi") (r "^0.8.1") (d #t) (k 0)) (d (n "atri_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1yncaiyzynvgrlxkmz5ibnxc9nh5spkricsnkv8985x74lm8k248")))

(define-public crate-atri_plugin-0.9.0 (c (n "atri_plugin") (v "0.9.0") (d (list (d (n "atri_ffi") (r "^0.9.0") (d #t) (k 0)) (d (n "atri_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1ma2yr10z63aw330kl2scpz7rnykcyyrk0dbg7jf6sns207xyw8l")))

