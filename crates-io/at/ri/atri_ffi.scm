(define-module (crates-io at ri atri_ffi) #:use-module (crates-io))

(define-public crate-atri_ffi-0.1.0 (c (n "atri_ffi") (v "0.1.0") (h "1zmg929v5nsibclmkl4wifqbdzii62gm0x0wyz4xpb42zkz6871y")))

(define-public crate-atri_ffi-0.2.0 (c (n "atri_ffi") (v "0.2.0") (h "1brmih117pls7mpansb4hz9fh1bb086c9v6ra58pdx1j0wy4kl6a")))

(define-public crate-atri_ffi-0.2.1 (c (n "atri_ffi") (v "0.2.1") (h "0qkpfjj5wjw40rah36wm29fxqw2bkf49hmh7bg03fqxvdskqqn3q")))

(define-public crate-atri_ffi-0.2.2 (c (n "atri_ffi") (v "0.2.2") (h "1x0vr89gyi6xw7r93ijj7yisvp72q5g0phmakpz5mdxzx6bl9sal")))

(define-public crate-atri_ffi-0.2.3 (c (n "atri_ffi") (v "0.2.3") (h "06dkpmd60mbljlr9wcwlkpz0ald4na3bxpjjvql23i2k9kwdmnhb")))

(define-public crate-atri_ffi-0.2.4 (c (n "atri_ffi") (v "0.2.4") (h "0w67kg6cbkx8fb0zh3xmi2rpb570f5xbwk75kkpm0307sy7dwk90")))

(define-public crate-atri_ffi-0.2.5 (c (n "atri_ffi") (v "0.2.5") (h "1wmskzx8xzr1q31a6rwn349jx1k5yvsi5f4l0kzd6yvngr883dim")))

(define-public crate-atri_ffi-0.2.6 (c (n "atri_ffi") (v "0.2.6") (h "0xmahcab7a93a2a7a69zpw25mvrjfpvqij8r08gjan9wyixd2xj2")))

(define-public crate-atri_ffi-0.2.7 (c (n "atri_ffi") (v "0.2.7") (h "1gmbfxhv9migw04snz8pidqhggvapx0xp3dzrsc3m51ahyrfdaqf")))

(define-public crate-atri_ffi-0.2.8 (c (n "atri_ffi") (v "0.2.8") (h "0rbbgpk08p9a1xhj8l791z3rjrky3q2gci6r1i6faj90ahh67k28")))

(define-public crate-atri_ffi-0.2.9 (c (n "atri_ffi") (v "0.2.9") (h "0girnqnchqkmhmc7xnyqyv8h77d9fwh242qkh7jh7h3ibxlikn1w")))

(define-public crate-atri_ffi-0.3.0 (c (n "atri_ffi") (v "0.3.0") (h "08d8jl0mq9780q1w9iyjrk4388cz071xg1frcwzzmlr1f2vj336c")))

(define-public crate-atri_ffi-0.4.0 (c (n "atri_ffi") (v "0.4.0") (h "18rij1j6f3j33vissyhv4brh7s1b4gq9ck6lgfw5dajxa67p858s")))

(define-public crate-atri_ffi-0.5.0 (c (n "atri_ffi") (v "0.5.0") (h "0hjb556h391skwjyamjjpnwgprmki0665jrvm9kac5w1arn9giam")))

(define-public crate-atri_ffi-0.5.1 (c (n "atri_ffi") (v "0.5.1") (h "139f44jhshgk95cl0blr6gw8raw141j6dd1i5lcv074bckjdwc2s")))

(define-public crate-atri_ffi-0.6.0 (c (n "atri_ffi") (v "0.6.0") (h "0pi8bilanma7jxzfqrkc72n0j7yzx4259hzjqr2n5snnxb458fkv")))

(define-public crate-atri_ffi-0.6.1 (c (n "atri_ffi") (v "0.6.1") (h "1sz4mirrxjq0ifd7zg4gl3fa1iyr8wdy35aw24f8270gglccycnh")))

(define-public crate-atri_ffi-0.7.0 (c (n "atri_ffi") (v "0.7.0") (h "1f0kibiqk44irirn59nbwxzh8zkl2kngqhv73x5lc9dgnh3qkwfw")))

(define-public crate-atri_ffi-0.8.0 (c (n "atri_ffi") (v "0.8.0") (h "111v1w8dhbisgpp5497ylpadbya745lpy77kd47r63zajysbg87h")))

(define-public crate-atri_ffi-0.8.1 (c (n "atri_ffi") (v "0.8.1") (h "0qqv3vfa7lfmc61vg55paljh6rl62bsfmdaaicr5spnc6814sk2c")))

(define-public crate-atri_ffi-0.9.0 (c (n "atri_ffi") (v "0.9.0") (h "0dk9j7j3qqs35fr6h2x6pjmdgwpggznrfbai49jhm8p59whdf5bd")))

