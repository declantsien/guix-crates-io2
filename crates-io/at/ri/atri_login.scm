(define-module (crates-io at ri atri_login) #:use-module (crates-io))

(define-public crate-atri_login-0.1.0 (c (n "atri_login") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0") (d #t) (k 0)) (d (n "qrcode") (r "^0") (k 0)) (d (n "ricq") (r "^0") (d #t) (k 0)) (d (n "rqrr") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)))) (h "1iq9xmkazkcychgdbf8svs033c1dyigz6j9psqjqrkpfiid3sm5r")))

