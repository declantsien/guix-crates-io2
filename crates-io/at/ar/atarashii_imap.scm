(define-module (crates-io at ar atarashii_imap) #:use-module (crates-io))

(define-public crate-atarashii_imap-0.2.0 (c (n "atarashii_imap") (v "0.2.0") (d (list (d (n "openssl") (r "^0.7.10") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "1788xrx188i2hiwi0cm95ywxllfyvgy7pjyr54ng4754k0p3mgr1")))

(define-public crate-atarashii_imap-0.2.1 (c (n "atarashii_imap") (v "0.2.1") (d (list (d (n "openssl") (r "^0.7.10") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)))) (h "05kkkjgcin7i4n225crpmcznbb24z2xxvng2cb3csnk4q2pli3yb")))

(define-public crate-atarashii_imap-0.3.0 (c (n "atarashii_imap") (v "0.3.0") (d (list (d (n "openssl") (r "^0.9.12") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0hqw7z6rji1gr03g9a9b3xngz36vavqkamirniay0xf4w5ikfx90")))

