(define-module (crates-io at ar ataraxy) #:use-module (crates-io))

(define-public crate-ataraxy-0.1.0 (c (n "ataraxy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "ataraxy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10") (f (quote ("builder" "cache" "client" "gateway" "model" "unstable_discord_api"))) (k 0)))) (h "0sdvj4q1n6vdbmk90jvlc4lsxz14zmnl51zny9mkyl40j2xp95lf") (f (quote (("default" "serenity/rustls_backend"))))))

(define-public crate-ataraxy-0.1.1 (c (n "ataraxy") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "ataraxy-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serenity") (r "^0.10") (f (quote ("builder" "cache" "client" "gateway" "model" "unstable_discord_api"))) (k 0)))) (h "0yv2cxs7i0c3mb858v68lz34122ia8l7ivhvrzfy10w1iypw9jnz") (f (quote (("default" "serenity/rustls_backend"))))))

