(define-module (crates-io at ar ataraxy-macros) #:use-module (crates-io))

(define-public crate-ataraxy-macros-0.1.0 (c (n "ataraxy-macros") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "01d0q6hql9drdxvggg493znbviyadygids22gw1nxm560z9i055q")))

(define-public crate-ataraxy-macros-0.1.1 (c (n "ataraxy-macros") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0n4fhh6xn3yx6a8li0hx80547nhzqny5mqg1vq3gb3bkcmfdlzad")))

