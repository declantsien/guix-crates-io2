(define-module (crates-io at ar atari-env-sys) #:use-module (crates-io))

(define-public crate-atari-env-sys-0.1.0 (c (n "atari-env-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1cw3nhqn8bvhd1h17qirmkqrjj9s07qck8zxx17scgwslg5jv4kq") (f (quote (("sdl") ("default" "sdl"))))))

