(define-module (crates-io at ad atadb) #:use-module (crates-io))

(define-public crate-atadb-0.1.0 (c (n "atadb") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06sf3fqljk6iz51zi1lsyaal07j8fhfgmn3mw3wrr1mfhvgjk34z")))

