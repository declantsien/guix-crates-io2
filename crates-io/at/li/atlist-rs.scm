(define-module (crates-io at li atlist-rs) #:use-module (crates-io))

(define-public crate-atlist-rs-0.1.0 (c (n "atlist-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1ppww7d23d9rqxm0rkm04jjs9jyw5ns7dhqs0fvxkpii3z3v9hqy")))

(define-public crate-atlist-rs-0.2.0 (c (n "atlist-rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "11xfkvl2l9s26nwiq74jnqnnp1phmzcg1rj535f40fgcm15grn5v")))

(define-public crate-atlist-rs-0.2.1 (c (n "atlist-rs") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0xajdm3d7hs2vy0drk6b24zq052035fbrvdz240q2qgbs8q2gq6b")))

