(define-module (crates-io at m- atm-io-utils) #:use-module (crates-io))

(define-public crate-atm-io-utils-0.1.0 (c (n "atm-io-utils") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.4") (d #t) (k 0)))) (h "01z3s4rikiv45hbxi8n2mblk9dw18b9cqfz6x2rpzwqg5sa1myi4")))

(define-public crate-atm-io-utils-0.2.0 (c (n "atm-io-utils") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0mhvhsciq7yyww00ya76p1gsmnbnc8ay10jqrz3hhn2v9aaaj7kx")))

(define-public crate-atm-io-utils-0.2.1 (c (n "atm-io-utils") (v "0.2.1") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0w65l4vdkpj3z1ijv5n02h8d6ws5ki45gqp20vgr83s0337nc0as")))

(define-public crate-atm-io-utils-0.2.2 (c (n "atm-io-utils") (v "0.2.2") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0lk9sssry3xrgldjxllqb7rkaf9r23cjmcsyx89xdadi0wrcalfh")))

(define-public crate-atm-io-utils-0.2.3 (c (n "atm-io-utils") (v "0.2.3") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)))) (h "09c5il1xylpn5dv1vw72hvdn4bg7xzi2z90gp2hys88ffxjip1is")))

(define-public crate-atm-io-utils-0.2.4 (c (n "atm-io-utils") (v "0.2.4") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1rj3k4c09mr1k09spr44rqw887zpb5m7ch3im47bmba8882044kj")))

(define-public crate-atm-io-utils-0.2.5 (c (n "atm-io-utils") (v "0.2.5") (d (list (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-io") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (o #t) (d #t) (k 0)))) (h "02cfm7sll982cp2chxmlk29c02ksjvaypmzz5m9ybqxdpd50jwga")))

