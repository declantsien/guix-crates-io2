(define-module (crates-io at m- atm-async-utils) #:use-module (crates-io))

(define-public crate-atm-async-utils-0.1.0 (c (n "atm-async-utils") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "08vgzfmvajyaz0qa00v9cwyxvxp0mxvbqfsgf0fx94ddm93sxvr1")))

(define-public crate-atm-async-utils-0.1.1 (c (n "atm-async-utils") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "0ankchdbnpchw04c4brfjjjq7wzdqjdsg52jf2jifa1dpgcs9dw7")))

(define-public crate-atm-async-utils-0.1.2 (c (n "atm-async-utils") (v "0.1.2") (d (list (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "quickcheck") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 2)))) (h "17fcd2lcslaxy7ax7scqkmkh1agf5nbip7750swzch8xy2kp8wfl")))

(define-public crate-atm-async-utils-0.2.0 (c (n "atm-async-utils") (v "0.2.0") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-channel") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-sink") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1mjil8c6mv28978byc780vaxz9x5s02shz6wj59nwg6lfwrjwbla")))

(define-public crate-atm-async-utils-0.2.1 (c (n "atm-async-utils") (v "0.2.1") (d (list (d (n "futures") (r "^0.2.0-alpha") (d #t) (k 2)) (d (n "futures-channel") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-core") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-sink") (r "^0.2.0-alpha") (d #t) (k 0)) (d (n "futures-util") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "1x99alawxflnj3s7nn9sirdy4l8n456j6cmmh90arkzivbr2qr4w")))

