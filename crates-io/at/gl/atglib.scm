(define-module (crates-io at gl atglib) #:use-module (crates-io))

(define-public crate-atglib-0.1.0 (c (n "atglib") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j937inwndqsxirg724hm5qbgpnslsf3y53ij5m4lw28q94lkvjg")))

(define-public crate-atglib-0.1.1 (c (n "atglib") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "167ymys8c6pd8by31ar3nnfg51gwl7wk6mk0ly51nxir7j9pz5iq")))

(define-public crate-atglib-0.1.2 (c (n "atglib") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m5ar9j7n1yg82xqs55h624gh98h0v80l6wb3yziwy3xiw8hhhv5")))

(define-public crate-atglib-0.1.3 (c (n "atglib") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "123jhgcahdpgpmm8j072kpb0m2vcn0kf33hy58x0hmsgpmr3afag")))

(define-public crate-atglib-0.1.4 (c (n "atglib") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15wlanq0k23dwwxkw2bk8w1h4pjw1zs8f6zf8ah4zxshyy814r23") (f (quote (("with-bench"))))))

(define-public crate-atglib-0.2.0 (c (n "atglib") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qylg6ri4nf481apca4ns9id6vz1zfk94qkc9zghjah9hwl651hc") (f (quote (("with-bench"))))))

(define-public crate-atglib-0.2.1 (c (n "atglib") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.156, <1.0.172") (f (quote ("derive"))) (d #t) (k 0)))) (h "001spnzrlbdw5bcc41my3rqp8xkx968qwnq41i3aj5sby7kdfjwn") (f (quote (("with-bench"))))))

(define-public crate-atglib-0.2.2 (c (n "atglib") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rh1al6hsmnqcdhi2qcrbcyg7j5w839pjhvjkrl2dn7yfck6j59b")))

(define-public crate-atglib-0.2.3 (c (n "atglib") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dfr786lzm1pfczgki3kw328jwdnidk0mffpr91qhaxqvzrz3faq")))

