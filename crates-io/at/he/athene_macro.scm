(define-module (crates-io at he athene_macro) #:use-module (crates-io))

(define-public crate-athene_macro-0.1.0 (c (n "athene_macro") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cpqzwgvw4v23x1n81nvjb59iq4i6q2dj6cli9r0rfdxxyr7j11g") (f (quote (("validate") ("full" "validate") ("default")))) (y #t)))

(define-public crate-athene_macro-0.1.1 (c (n "athene_macro") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h001qi5kgvi4b7v70wpym68skb2z54fk2n2xs8gdlfdhm9c7lry") (f (quote (("validate") ("full" "validate") ("default")))) (y #t)))

(define-public crate-athene_macro-0.1.2 (c (n "athene_macro") (v "0.1.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ypdmapfq8vwd4m9kmxx7j3whcm4pxsi510jabb1qh22488gla64") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.2.0 (c (n "athene_macro") (v "0.2.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qvczbycm744py584h9ybq8562m6kwjhc53w9b7272bdzbb9r8pn") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.3.0 (c (n "athene_macro") (v "0.3.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iygbf9bq0pgyc9qnbx6y20d701whr2hw7rvaisww8fclf039ymq") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.3.1 (c (n "athene_macro") (v "0.3.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "199wsy9rfh6jwl130z5mm5m1lsxrc4zbi2q380cj76nlxf4b1iv2") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.3.2 (c (n "athene_macro") (v "0.3.2") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jps7ml4dw4ykn8bxx6m9vzcnr4yzgi451px136kkmn2bywgy64i") (f (quote (("validate") ("full" "validate") ("default" "validate"))))))

(define-public crate-athene_macro-0.3.3 (c (n "athene_macro") (v "0.3.3") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zszfd6p80rg66hn0ka3jf5shn5b7xa5mzl3idpli9dpr2ihq1dq") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.3.4 (c (n "athene_macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q2mvwfciymg4pvaic3xpzc0m2ph888d5sq5941sr3zbdw6hmqgv") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.3.5 (c (n "athene_macro") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "166w8qxas62xsbiscfqypm2nz7lahck10fdaavxs8527lyyr10zk") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.2.1 (c (n "athene_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6qy4i4phb5iscni6g9g8h445yrmqzlqlpqaj29ljnj9v0csig8") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.2.2 (c (n "athene_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16bwsc35nzhbp226yvn67drlzsa7hj0mjl5lbw0z7ab40r0qhns4") (f (quote (("validate") ("full" "validate") ("default"))))))

(define-public crate-athene_macro-0.2.3 (c (n "athene_macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvivafng0zjpzkcg3fsz9j30z9fcn141ni11s5ldxiymng3c2pk") (f (quote (("validate") ("full" "validate") ("default"))))))

