(define-module (crates-io at he athenaengine) #:use-module (crates-io))

(define-public crate-AthenaEngine-0.1.0 (c (n "AthenaEngine") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1phiw1sqpkh8mvdrs9n2gfhn23sz2wck5qc95vsqzk1rbzaxq8i4") (y #t)))

(define-public crate-AthenaEngine-0.1.1 (c (n "AthenaEngine") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1pdmr8088hk2psyan0gbm8xipfyk4yp0v726d4wbmggxsnk3a0gw") (y #t)))

(define-public crate-AthenaEngine-0.1.2 (c (n "AthenaEngine") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1zl03ji6clb1jdxdaicx447fpgg3cswp0br4sjvhl2c874y6gh5l") (y #t)))

(define-public crate-AthenaEngine-0.1.3 (c (n "AthenaEngine") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "04jnmdvspvjwykbs2a6i0wdr9yk7xikylsd4g5c0rz7rylr8nafw")))

(define-public crate-AthenaEngine-0.1.4 (c (n "AthenaEngine") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1xs5x6vh6fzfb6sh16g3kpi2wxfkyg67adriz7l80qg84ghrbcyl")))

(define-public crate-AthenaEngine-0.1.5 (c (n "AthenaEngine") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "01jvak0bqwg23sc5pzg51j13yjhv90dj8gyj38wnnf7ccyi9ldc9")))

(define-public crate-AthenaEngine-0.1.6 (c (n "AthenaEngine") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0kv9w6lsz63jsyxwc8zgs4ap71glm0gbbx7jjsmq2khdvi9809m7")))

(define-public crate-AthenaEngine-0.1.7 (c (n "AthenaEngine") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "01kh0ls299y54ps8wmjy7d6lhah6p6lscpx9w3q93l7cpij71572")))

