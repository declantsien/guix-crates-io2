(define-module (crates-io at he athenna) #:use-module (crates-io))

(define-public crate-athenna-0.1.0 (c (n "athenna") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "07mh1bspyabm0pj45lv6l7lxpvkqdn2fc2nq32nw41ivzm3bf0j5")))

(define-public crate-athenna-0.1.1 (c (n "athenna") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0k7bd2j9sjg26ghfpkvydl21y10p66n8ahkjmq6sii8zb6mn3x21")))

(define-public crate-athenna-0.1.2 (c (n "athenna") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0y6p9184g4kgb52nwdf0pl4xynqyn9f2msm21spwnvca28ha4sr4")))

