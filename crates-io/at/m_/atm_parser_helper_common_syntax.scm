(define-module (crates-io at m_ atm_parser_helper_common_syntax) #:use-module (crates-io))

(define-public crate-atm_parser_helper_common_syntax-1.0.0 (c (n "atm_parser_helper_common_syntax") (v "1.0.0") (d (list (d (n "arbitrary") (r "^1.0.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "atm_parser_helper") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3.0") (d #t) (k 0)))) (h "1wizrmsxg3ax7n71d05ajb7qk8sqsn9cwwi139fflbp9fk6fsnqi") (f (quote (("default" "arbitrary"))))))

(define-public crate-atm_parser_helper_common_syntax-2.0.0 (c (n "atm_parser_helper_common_syntax") (v "2.0.0") (d (list (d (n "arbitrary") (r "^1.0.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "atm_parser_helper") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3.0") (d #t) (k 0)))) (h "1y9j21kgpzqvngys5df5b3w0l91qhxbx9ph9v2h204g16awqq626")))

