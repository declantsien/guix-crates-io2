(define-module (crates-io at at atat_derive) #:use-module (crates-io))

(define-public crate-atat_derive-0.1.1 (c (n "atat_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0sr1wmb2bdimcq6fpavnd4jwjzzhm4c490w716i3inhwywi1jmk8")))

(define-public crate-atat_derive-0.1.2 (c (n "atat_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0bwldl5kw16b6g52b6r2v3iwryy8cxblwfhx0x9gym2n5bin3wj7")))

(define-public crate-atat_derive-0.1.3 (c (n "atat_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0vx608v84v3cizhnsxzpipkrq10xp9r4im9bvdcqk4b684rwmnps")))

(define-public crate-atat_derive-0.1.4 (c (n "atat_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "1yixqgdm2r1z80b24hx28c1fmyjs4hbdrc7bg6vhh86vmpc7cnzz")))

(define-public crate-atat_derive-0.1.5 (c (n "atat_derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0qgb2rlql8gbqqw3d3kac95vyklzjgvcizdyx6sg7w60z0sd3vpx")))

(define-public crate-atat_derive-0.1.6 (c (n "atat_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "1d584smhqgw241hknz4xldwayc3l29c8gxji59727k49lipjlbkv")))

(define-public crate-atat_derive-0.1.7 (c (n "atat_derive") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0qrbijbwzz1snv7j7znljc86klrzgpv1nfz6jc11vs9ia3d33rxx")))

(define-public crate-atat_derive-0.3.0 (c (n "atat_derive") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_at") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)))) (h "0g7xmsxhs72cbsqmk6s2migz0207g1fflg6ra53pbn10682dbqy3")))

(define-public crate-atat_derive-0.4.1 (c (n "atat_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lljr3llvlnyds3wn3h5hyc5ak6581i6xp4lxkc40pgrwj7ymzrv")))

(define-public crate-atat_derive-0.4.2 (c (n "atat_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.4.2-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nv8kkyhs0rxwd54mamb8x8wx9v1394wlfnvdbp1ws6p6lc91hw8")))

(define-public crate-atat_derive-0.5.1 (c (n "atat_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8sl6nvjzps6g2srw5zpr0mfk0ipgi5pl4zhxy8jyqwjpw9lyk3")))

(define-public crate-atat_derive-0.6.0 (c (n "atat_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10whlg3cgq2mlsp16xgzr8zbb7fa489ncdmyy0gxfgp26h61bsjz")))

(define-public crate-atat_derive-0.7.0 (c (n "atat_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rh02w8k85s1839nnqf9b372p2yb79dbw4nfhn8jxc1gjz0kin4y")))

(define-public crate-atat_derive-0.7.1 (c (n "atat_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h2mb0v2rfckp08z9f7w3l2k0jcqc1nd15j4hp83kd70ghscdimh")))

(define-public crate-atat_derive-0.8.0 (c (n "atat_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlahyaw1pkg7ms10rj696gbi2rgjdrr0r3lifkn76vy8czbxdaz") (y #t)))

(define-public crate-atat_derive-0.8.1 (c (n "atat_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.8.1-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fvksfknywan0cn2nggp3122v2sn2k6cy2rzcrzx8sv0zwkyqbgr") (y #t)))

(define-public crate-atat_derive-0.8.2 (c (n "atat_derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.8.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wvbfj4nsdd1y9qh4gjch0v2ys0iqk0g7h5fcjx25lpfbmxzihrc") (y #t)))

(define-public crate-atat_derive-0.8.3 (c (n "atat_derive") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.8.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "124s9j8qz9l0zl1r6h8gg7bxgg20dkwsl2niz6spisz0vdsglbzw") (y #t)))

(define-public crate-atat_derive-0.8.4 (c (n "atat_derive") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1682rrinmb2lq5ph8wnc73ssc1k83qcf7vn39wzbvqszd28v7qyj")))

(define-public crate-atat_derive-0.9.0 (c (n "atat_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1aa982g30kcck8h7k1rficankzi5aqvffpzm85haz3mh31s34fp8")))

(define-public crate-atat_derive-0.10.0 (c (n "atat_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1brxiba374bx4hpi7l1mfs4q4cwqwx827zrq45i5fr17cb31v0i1")))

(define-public crate-atat_derive-0.11.0 (c (n "atat_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zgv7lhbdammrxlzysb6kdblikxlydjd8fkva9ni3p3hb3111gaf")))

(define-public crate-atat_derive-0.11.1 (c (n "atat_derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.11.1-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14g9xs98yw447b5ks0774ixm566f1nqnyq4rdw3lv6rdb21qadai")))

(define-public crate-atat_derive-0.12.0 (c (n "atat_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1riis7fs9zsma18dfjz16qh0fci9r4qilwlka6vh0h4z2bsdjwb5")))

(define-public crate-atat_derive-0.13.0 (c (n "atat_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x0hz272lrsnqljqfg72j5drdxpqvrjyfydm9x59ipsmr43ma6xc")))

(define-public crate-atat_derive-0.13.1 (c (n "atat_derive") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.13.1-alpha.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vjdxk42a8hpsavhj3dwa6gll5bawya3ivnwjc0lqzjba715yl0m")))

(define-public crate-atat_derive-0.14.0 (c (n "atat_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y2n37b7fcbwklr0lj1dyqz2jfy1f86lj2ci90b5kz6k48vm8am4")))

(define-public crate-atat_derive-0.15.0 (c (n "atat_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08vbp0kmcba0jxamhn4v8fpd36hr356g6m6mvwxnbmmx0rzakfw9")))

(define-public crate-atat_derive-0.16.0 (c (n "atat_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wblgrwbfr82z974w55z5gws5ddhndnz04c5ccny4mjsn4gljphj")))

(define-public crate-atat_derive-0.16.1 (c (n "atat_derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10kfyxd9xxsj0wb0ykscv9w8q2lzfwg0j4zgpxclwawak0c22cby")))

(define-public crate-atat_derive-0.17.0 (c (n "atat_derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.17.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09325s8y4518ian14s0kyq3pibf3s19jx3jjq8zml7855p7kg1rs")))

(define-public crate-atat_derive-0.18.0 (c (n "atat_derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.18.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "075anwzj1i54w97cqzc0pm6dabi91asmms5sypcjwafrrdkinh50")))

(define-public crate-atat_derive-0.19.0 (c (n "atat_derive") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.19.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "123jb1hxj0nagf5nvqqmpkizbkd810zzw2haxbmn3insrykrf3nw")))

(define-public crate-atat_derive-0.19.1 (c (n "atat_derive") (v "0.19.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.19.1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvcs06g795g9b7y1773l3555sxns6gc6mms8cvl5azms5zbrwf1")))

(define-public crate-atat_derive-0.20.0 (c (n "atat_derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.20.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "065kksk5czp7inpbmpfsrx9bglma2h177ff4b560wxpq4hr1qyjq")))

(define-public crate-atat_derive-0.21.0 (c (n "atat_derive") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.21.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ll4cw9v79pzz9vccrvyxvsl97bchvr52bq880kl7arx649p9jsj")))

(define-public crate-atat_derive-0.22.0 (c (n "atat_derive") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_at") (r "^0.22.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1f1xzw5sw1rkx95nyjsjcz5skhfz7gv50wmwzkrzi2nrp8l2k49a")))

