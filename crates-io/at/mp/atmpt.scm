(define-module (crates-io at mp atmpt) #:use-module (crates-io))

(define-public crate-atmpt-0.1.0 (c (n "atmpt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 1)) (d (n "directories-next") (r "^2.0.0") (d #t) (k 0)))) (h "1ydxcb9ld5fd0xy8bwxq0z2r8q4v0lb48102wh1ij3sxrjapsdcj") (y #t)))

(define-public crate-atmpt-0.2.0 (c (n "atmpt") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 1)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0bxbivb2cxzgb4lia2s7712n1hj2d8kzg42yhn6hc8dbv71mqkdy")))

