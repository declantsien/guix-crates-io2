(define-module (crates-io at pp atpp) #:use-module (crates-io))

(define-public crate-atpp-0.1.0 (c (n "atpp") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0m7q426p3nffdzphnnl3n0mqjg0yrs4a5240skqb01cm6060dfvl")))

(define-public crate-atpp-1.6.6 (c (n "atpp") (v "1.6.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "088mlpfvkbvbnhdra8mxl7x0y6dq9ykv9y9579dssdx535gqmiis")))

(define-public crate-atpp-1.6.7 (c (n "atpp") (v "1.6.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1zm07brj4d8d7jx7zawaw4jx1bbp2hd025l1r964fx64rcy8kfn5")))

