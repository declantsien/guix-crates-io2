(define-module (crates-io at me atmega32u4-hal) #:use-module (crates-io))

(define-public crate-atmega32u4-hal-0.1.0 (c (n "atmega32u4-hal") (v "0.1.0") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1isq4dlxa9rms88i29l9acdnqxmnchx7pbzkjxw6264gljg4avi8") (f (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1.1 (c (n "atmega32u4-hal") (v "0.1.1") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1jvfxadcc06zgf6d1rqs3a05yd5i30d0vpfw020diw5sw4brhcw8") (f (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1.2 (c (n "atmega32u4-hal") (v "0.1.2") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0zc7y8hq836zv613vy8zd9fz1x12slcysdqr68ffxampihxk79nq") (f (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1.3 (c (n "atmega32u4-hal") (v "0.1.3") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "13zvxgin40fqf3xnr6hg0q4j9x2cifpxkhn91bq4av01glhl5mi6") (f (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1.4 (c (n "atmega32u4-hal") (v "0.1.4") (d (list (d (n "atmega32u4") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1r4byhyhlv3bvkvjcs8q5cijvyfn4yhnpgynmhli23v34dnr1jib") (f (quote (("docs"))))))

