(define-module (crates-io at me atmega32u4) #:use-module (crates-io))

(define-public crate-atmega32u4-0.1.0 (c (n "atmega32u4") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0bczwzi1rhc9k7sa12ykgacfaz5f9l2lkxl88d037gswpkqpx0cj")))

(define-public crate-atmega32u4-0.1.1 (c (n "atmega32u4") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1aabswx4551vm994r0kayhcczqmcny924zdli5169qj6dgx4fivy")))

(define-public crate-atmega32u4-0.1.2 (c (n "atmega32u4") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0xzwlqpx5adfnq3d7g45wvzmfbs8x7lyxmj9rj7q561fmyiyhjg0")))

(define-public crate-atmega32u4-0.1.3 (c (n "atmega32u4") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "11f5cxsqwxbk9c45wli0dylqbq2xr3krd9hcsag731dn52ks9yxb")))

