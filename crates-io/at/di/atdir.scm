(define-module (crates-io at di atdir) #:use-module (crates-io))

(define-public crate-atdir-0.0.0 (c (n "atdir") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vra4jxplar9amnblx49sxhrhwh2rhw8jnbh9505n6n9r8s3sfai")))

(define-public crate-atdir-0.0.1-wip (c (n "atdir") (v "0.0.1-wip") (d (list (d (n "cbuffer") (r "^0.2.0-pre") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fbd16g9z0kxh6nkkgr7p1cywffn743jal22c6hvaz2gz0bylb45")))

