(define-module (crates-io at om atomic-file-install) #:use-module (crates-io))

(define-public crate-atomic-file-install-0.0.0 (c (n "atomic-file-install") (v "0.0.0") (d (list (d (n "reflink-copy") (r "^0.1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k1hz2xqqkfilwck41sapjk6s2ws79rnljzipvwzjrydfwpb5fvw") (r "1.65.0")))

(define-public crate-atomic-file-install-1.0.0 (c (n "atomic-file-install") (v "1.0.0") (d (list (d (n "reflink-copy") (r "^0.1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "windows") (r "^0.51.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b8q92v23di1nbhpc8d40hvg2hwbwfrdzn5ck66kbdf54kzb3nmv") (r "1.65.0")))

(define-public crate-atomic-file-install-1.0.1 (c (n "atomic-file-install") (v "1.0.1") (d (list (d (n "reflink-copy") (r "^0.1.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0b0rlg6jn63bz3rb8v4d4db7c18gf98w8h9q3dkl9gqmz1f8h68a") (r "1.65.0")))

(define-public crate-atomic-file-install-1.0.2 (c (n "atomic-file-install") (v "1.0.2") (d (list (d (n "reflink-copy") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k29cg650inq8ivsalkn05yl4l039vigfw9vw1z0xfxarm9k1grp") (r "1.65.0")))

(define-public crate-atomic-file-install-1.0.3 (c (n "atomic-file-install") (v "1.0.3") (d (list (d (n "reflink-copy") (r "^0.1.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Storage_FileSystem" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rrgixd0pxfjgn6kfidis1nxy8009cy4nqi0f7ydg2paq8fawxm1") (r "1.65.0")))

