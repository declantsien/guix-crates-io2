(define-module (crates-io at om atomic-enum-derive) #:use-module (crates-io))

(define-public crate-atomic-enum-derive-0.1.0 (c (n "atomic-enum-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s35djjq3k44hwyknf6a9vlzk0ngpqy06cxw4s35rgbh775h7rip")))

