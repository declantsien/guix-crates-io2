(define-module (crates-io at om atomic_value) #:use-module (crates-io))

(define-public crate-atomic_value-0.1.0 (c (n "atomic_value") (v "0.1.0") (d (list (d (n "loom") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.3") (d #t) (k 2)))) (h "1pafmcw5m3kqx5871rd8083vpq6y6x30hhd7hwc0kr93j5wsjd6x") (f (quote (("default") ("concurrent-test" "loom"))))))

(define-public crate-atomic_value-0.2.0 (c (n "atomic_value") (v "0.2.0") (d (list (d (n "loom") (r "^0.4") (d #t) (t "cfg(loom)") (k 0)))) (h "08h20f80yv8phvs48g40syyrg7gg62xfljsmxsdlfkzkwnqzjm09")))

