(define-module (crates-io at om atomic-take) #:use-module (crates-io))

(define-public crate-atomic-take-0.1.0 (c (n "atomic-take") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 2)))) (h "1x0q7731sc7dnazbh10m5gd8bympcfkgjmfgbiwmp38wyi8crsag")))

(define-public crate-atomic-take-1.0.0 (c (n "atomic-take") (v "1.0.0") (d (list (d (n "futures") (r "^0.1.29") (d #t) (k 2)))) (h "0zmn2pay3p94kcg9b8qz2kd26flzchlg1lcq685sixjznd7mxxpr")))

(define-public crate-atomic-take-1.1.0 (c (n "atomic-take") (v "1.1.0") (d (list (d (n "tokio") (r "^1.24.2") (f (quote ("sync"))) (d #t) (k 2)))) (h "1hzvfqiy0ixsawkh7ci9visn95kx2j6yvnqxz536x5wpzranpax8") (r "1.48.0")))

