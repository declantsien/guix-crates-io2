(define-module (crates-io at om atomic-interval) #:use-module (crates-io))

(define-public crate-atomic-interval-0.1.0 (c (n "atomic-interval") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "quanta") (r "^0.9") (d #t) (k 0)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "0xdyqhb3hyl16grgxyldkcrfasbrlz5a1z4qdaprfgzzyn07yl9k")))

(define-public crate-atomic-interval-0.1.1 (c (n "atomic-interval") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "quanta") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "11rasm25gnwnci82fqc1x7g6zdgy56wnk02fgqzi2y3d36gv96xm")))

(define-public crate-atomic-interval-0.1.2 (c (n "atomic-interval") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "quanta") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "test-case") (r "^1.2.1") (d #t) (k 2)))) (h "0p21fly5h48mlr2i010zxvrwf8hml8zv0pwcqf64sspv3jpvjsff")))

(define-public crate-atomic-interval-0.1.3 (c (n "atomic-interval") (v "0.1.3") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "quanta") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17daw17sgqqbkfqx08bc1lpvl6d6h77agjh5rviwsk895faly5bv")))

(define-public crate-atomic-interval-0.1.4 (c (n "atomic-interval") (v "0.1.4") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "quanta") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0a1n6qgdzwdwvf1qcxdk0g0ppg064mr6yllp416p73scp3k0d2dz")))

