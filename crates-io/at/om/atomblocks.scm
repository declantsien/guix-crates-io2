(define-module (crates-io at om atomblocks) #:use-module (crates-io))

(define-public crate-atomblocks-0.1.0 (c (n "atomblocks") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colored"))) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("parse"))) (k 0)) (d (n "x11rb") (r "^0.13") (d #t) (k 0)))) (h "0ad4f7mh389q15byflk3fpy25an32rk958122sknpw2cp7dn3bx8")))

(define-public crate-atomblocks-0.1.1 (c (n "atomblocks") (v "0.1.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colored"))) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("parse"))) (k 0)) (d (n "x11rb") (r "^0.13") (d #t) (k 0)))) (h "0srxv1g0kzk708cxmj3f2h9pkmvs3f0f03cazs5agi77mp247440")))

(define-public crate-atomblocks-0.2.0 (c (n "atomblocks") (v "0.2.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "simple_logger") (r "^4") (f (quote ("colored"))) (k 0)) (d (n "toml") (r "^0.8") (f (quote ("parse"))) (k 0)) (d (n "x11rb") (r "^0.13") (d #t) (k 0)))) (h "1jmiz51l5ixn1ywjxdqmnx12kd0zwxgyxb72dc0bfryjjg8985hc")))

