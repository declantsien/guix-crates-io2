(define-module (crates-io at om atomic_float) #:use-module (crates-io))

(define-public crate-atomic_float-0.1.0 (c (n "atomic_float") (v "0.1.0") (h "17f4vcw3qwqsqcp7snzmh8z5z7z4v3lxla35vjgg17ds8384dbv2") (f (quote (("default" "atomic_f64") ("atomic_f64"))))))

(define-public crate-atomic_float-1.0.0 (c (n "atomic_float") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (k 2)))) (h "1rx5v5yfyk12p19vh2lk1jng0irxsys0w6bw24h77zrhibnhhjrw") (f (quote (("default" "atomic_f64") ("atomic_f64")))) (r "1.60.0")))

