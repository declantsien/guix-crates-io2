(define-module (crates-io at om atomic-queue) #:use-module (crates-io))

(define-public crate-atomic-queue-0.1.0 (c (n "atomic-queue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1.2") (d #t) (k 2)))) (h "02v71f1s8kxsxyay9049an0v15z2h4d83vdqzv2hbvfmscjc5ylw") (y #t)))

(define-public crate-atomic-queue-1.0.0-alpha.1 (c (n "atomic-queue") (v "1.0.0-alpha.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "10dwk05anqx2y3z1q88d8cvj7n0amz7hsaz3dhphjxgcpx7vfm0r")))

(define-public crate-atomic-queue-1.0.0-alpha.2 (c (n "atomic-queue") (v "1.0.0-alpha.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1ysjmqriwxsbrnykrg06w1k12njcd1z7f4wy8nbczfzrkdbw6ir9")))

(define-public crate-atomic-queue-1.0.0-alpha.3 (c (n "atomic-queue") (v "1.0.0-alpha.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0iad14x6appd51sww4lm0jx9v1aghs2rr9x1ljhaws1nmwji8n49")))

(define-public crate-atomic-queue-1.0.0-alpha.4 (c (n "atomic-queue") (v "1.0.0-alpha.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1rjpaw8z50rq1y1cqvy3iw5gca6hz5iz204imcdad3giawr2imhb")))

(define-public crate-atomic-queue-1.0.1 (c (n "atomic-queue") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0m76msrvmb1z4slf48w9hv11d28z66m9mmbvw44cybmc060w5mpz")))

(define-public crate-atomic-queue-2.0.0 (c (n "atomic-queue") (v "2.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "0ql3921dd5fhamk53z3wxgs4ii0qnb3l41lgblknrfgd391csyav")))

(define-public crate-atomic-queue-2.1.0 (c (n "atomic-queue") (v "2.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 2)) (d (n "wisual-logger") (r "^0.1") (d #t) (k 2)))) (h "1ypw8rnfhzxgvjqscdxs48cv2cb53m4fql5p5g0p3j08w8gf2s02")))

