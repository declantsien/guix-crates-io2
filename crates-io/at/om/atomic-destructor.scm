(define-module (crates-io at om atomic-destructor) #:use-module (crates-io))

(define-public crate-atomic-destructor-0.0.0 (c (n "atomic-destructor") (v "0.0.0") (h "1wm87qz72h0llxx0lykp5s3vwdijbzg8qwnqclbg9r3n3yqv21za")))

(define-public crate-atomic-destructor-0.1.0 (c (n "atomic-destructor") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "082xi1c4nv15qw51i4wifpikb621jacgbzs7v2i8cfjryd7zszas") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-atomic-destructor-0.1.1 (c (n "atomic-destructor") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0gnzi90rifqv1ypv93zzmys4mbz3nh3f0li4x7aa2821y0ms8ls6") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-atomic-destructor-0.2.0-alpha.1 (c (n "atomic-destructor") (v "0.2.0-alpha.1") (d (list (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "0sggbxlgq679wqz7vzwhkpvp2b1pl29yv1l22nh6rmgbpipk1sn2") (f (quote (("default")))) (r "1.56.0")))

(define-public crate-atomic-destructor-0.2.0-alpha.2 (c (n "atomic-destructor") (v "0.2.0-alpha.2") (d (list (d (n "tracing") (r "^0.1") (o #t) (k 0)))) (h "1jkib51sc42pxn1bpawr1p9s52sii66cnvwxzsmj09sgqxmq00df") (f (quote (("default")))) (r "1.56.0")))

