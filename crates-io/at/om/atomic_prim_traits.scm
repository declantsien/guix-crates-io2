(define-module (crates-io at om atomic_prim_traits) #:use-module (crates-io))

(define-public crate-atomic_prim_traits-0.1.0 (c (n "atomic_prim_traits") (v "0.1.0") (h "1jafnxda3c4vxvii0k3ma3z0z8ggcbrp5ry21sazfl92c7ln1a2b") (f (quote (("nightly"))))))

(define-public crate-atomic_prim_traits-0.1.1 (c (n "atomic_prim_traits") (v "0.1.1") (h "139ydd9svnshkxgbg4siwjgnzz3vpn5cc25l13qhw533cmfaf2nd") (f (quote (("nightly"))))))

(define-public crate-atomic_prim_traits-0.2.0 (c (n "atomic_prim_traits") (v "0.2.0") (h "0zjmslm3gs99vv7y0cf0ggwf70g0xj6ya25i3y89yiq3hi5i708f") (f (quote (("nightly"))))))

