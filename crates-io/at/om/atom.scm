(define-module (crates-io at om atom) #:use-module (crates-io))

(define-public crate-atom-0.0.1 (c (n "atom") (v "0.0.1") (h "0m3a36kdy2nzwlwi0jbj7ag569p6np89fxc4ggqw8nqys603k778")))

(define-public crate-atom-0.0.2 (c (n "atom") (v "0.0.2") (h "02jfzqfr0hzf22jqq012msdrvysc5cmfw5jd9bsmfkiwv76y59lc")))

(define-public crate-atom-0.1.0 (c (n "atom") (v "0.1.0") (h "1p04h7rjc9dfi9x3z3728dqb7635zc2gqlfj03y54zypv10wrrp4")))

(define-public crate-atom-0.2.0 (c (n "atom") (v "0.2.0") (h "1agyz48x52y778pgjmyk5dxh8mjcnrskpwqdm29wzvda91djm015")))

(define-public crate-atom-0.2.1 (c (n "atom") (v "0.2.1") (h "00s21a2gbzbz7j6xfnay2w3j827ihylq5cyc98qpd1kkd77sw303")))

(define-public crate-atom-0.3.0 (c (n "atom") (v "0.3.0") (h "1i653flfilk392mdqlgsxkh7j5zh69ma1n3h4mgxk4c5za3dgliv")))

(define-public crate-atom-0.3.1 (c (n "atom") (v "0.3.1") (h "0qh7a5bz9a8v356dws0wz0w09wr6h1nhi473p4xgg4j5fbn8c9lp")))

(define-public crate-atom-0.3.2 (c (n "atom") (v "0.3.2") (h "1jpvld5flg16razh598py7nbzi3f3l3i08ssa5wykgjfxxczzx6n")))

(define-public crate-atom-0.3.3 (c (n "atom") (v "0.3.3") (h "1nlmw16d46dl18760h5258rzb71ydfks8n5fx3dzqc752jpvxvch")))

(define-public crate-atom-0.3.4 (c (n "atom") (v "0.3.4") (h "1bxc3sjrn1hbd4ni8kwi3gx1g77bwpr6mijskxkwdn89p86bimsc")))

(define-public crate-atom-0.3.5 (c (n "atom") (v "0.3.5") (h "1qig9fcdqf07mzzpkicm5wgxv0zpr28njdsqf708wxq27yf6k1iw")))

(define-public crate-atom-0.4.0 (c (n "atom") (v "0.4.0") (h "0ql596rfj0rhx6z8c0vfjkywsqqr9p6vgw5zcykkz94gdh1agkqx")))

(define-public crate-atom-0.3.6 (c (n "atom") (v "0.3.6") (h "1hzx48pl8r3ik8jm3d6fjfv6p198bg9n4s5kznn2a03qv6g19zy9")))

