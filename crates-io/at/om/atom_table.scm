(define-module (crates-io at om atom_table) #:use-module (crates-io))

(define-public crate-atom_table-1.0.0 (c (n "atom_table") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "typed-index-collections") (r "^3.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.14") (d #t) (k 2)))) (h "1wksl71w6w3rzynqrhwcd5z38c6c04hw3qv8blp36r49cfxldf7y") (f (quote (("default" "transform")))) (s 2) (e (quote (("transform" "dep:thiserror"))))))

(define-public crate-atom_table-1.1.0 (c (n "atom_table") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "typed-index-collections") (r "^3.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.14") (d #t) (k 2)))) (h "0icxq6d9l8yw7b24waf2rkijz1q6vxzdglcs80273wby1vgz1dlr") (f (quote (("default" "transform")))) (s 2) (e (quote (("transform" "dep:thiserror"))))))

