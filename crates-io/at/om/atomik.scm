(define-module (crates-io at om atomik) #:use-module (crates-io))

(define-public crate-atomik-1.0.0 (c (n "atomik") (v "1.0.0") (h "0h1030cx0s6qmj2lgj0lijqipc8yslg259pihi54idgryyknp4bn")))

(define-public crate-atomik-1.1.0 (c (n "atomik") (v "1.1.0") (d (list (d (n "atomic-polyfill") (r "^1") (o #t) (d #t) (k 0)))) (h "044sjgjy3gi0rz8arr5m8fhqq9sfa210rnmsrxkh8hvknbhrczds") (f (quote (("critical-section-polyfill" "atomic-polyfill"))))))

