(define-module (crates-io at om atomic_utilities) #:use-module (crates-io))

(define-public crate-atomic_utilities-0.1.0 (c (n "atomic_utilities") (v "0.1.0") (h "0cjnms4zyfdpid6b8if7g57xhsvryzqad0f0x18kx6q7f7ahgy74")))

(define-public crate-atomic_utilities-0.2.0 (c (n "atomic_utilities") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)))) (h "18dhg3v4sj3pk5h3wvwjdpibry5h1pp7z52fgs5i8zmvbs7fzx49")))

(define-public crate-atomic_utilities-0.3.0 (c (n "atomic_utilities") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)))) (h "1ic2nj0m531pybpqcaxs94pfslhvk16yf48bs5lggi0j383xd0w5") (y #t)))

(define-public crate-atomic_utilities-0.4.0 (c (n "atomic_utilities") (v "0.4.0") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)))) (h "0xgysc2myjbzgcida4hdwnyswbjjccwbrn4gq8r5rfkaf8qg71ws")))

(define-public crate-atomic_utilities-0.5.0 (c (n "atomic_utilities") (v "0.5.0") (d (list (d (n "rustc_version") (r "^0.1.4") (d #t) (k 1)))) (h "0p2029vr29wygcc2qpxgb8swpw600i0i1vpb1mb8qpkla64j8ncq")))

