(define-module (crates-io at om atomig-macro) #:use-module (crates-io))

(define-public crate-atomig-macro-0.1.0 (c (n "atomig-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "067ifdqmk2zy7wm4pji062zdy4607ihf0wnbnr2pbag765g0rjdc")))

(define-public crate-atomig-macro-0.2.0 (c (n "atomig-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0lk4zk6gimhb8496kv94k8lr79hvrajzniz2pvhayg92xcj57814")))

(define-public crate-atomig-macro-0.3.0 (c (n "atomig-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0hvgrip2nqqlgpfz8p9zrs6iyrv8nyz0c5bgvm5mxim4ik4wh44s")))

