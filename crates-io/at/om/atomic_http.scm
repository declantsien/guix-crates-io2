(define-module (crates-io at om atomic_http) #:use-module (crates-io))

(define-public crate-atomic_http-0.1.0 (c (n "atomic_http") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "0kbfb65jdx463a9b53cz001aq5kim7lnpbbnh9bfh480zysyjpca") (r "1.77")))

(define-public crate-atomic_http-0.1.1 (c (n "atomic_http") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "189gvzis51sxzdhp1pmxgskpxly9i7madymsnr1pf0mdk82ac2f6") (r "1.77")))

(define-public crate-atomic_http-0.1.2 (c (n "atomic_http") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "1d9d531hb9s4a08s37gykvj353hkxfcc36ym255w26sbah8kq4h6") (r "1.77")))

(define-public crate-atomic_http-0.1.3 (c (n "atomic_http") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "06sxy1zxq0shi4ivj14ph97xckh88w0fr3zz5zxx0zpxj7bgrrmb") (r "1.77")))

(define-public crate-atomic_http-0.1.4 (c (n "atomic_http") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "08y3l0x42lqvilnw34qmbq9klddy4c8cx3yzp6xxzxir1g302i8v") (r "1.77")))

(define-public crate-atomic_http-0.1.5 (c (n "atomic_http") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "13l4ffn25mr5h1g7y1zqmlazffy9a1agm301irldzq13x5s32y2d") (r "1.77")))

(define-public crate-atomic_http-0.1.6 (c (n "atomic_http") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "macros" "io-util" "fs"))) (d #t) (k 0)))) (h "0wri46j6jl86xim8i30yd88bgy6vg1hc37ga19pbhzbrlshrvbla") (r "1.77")))

