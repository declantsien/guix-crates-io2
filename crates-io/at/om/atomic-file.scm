(define-module (crates-io at om atomic-file) #:use-module (crates-io))

(define-public crate-atomic-file-0.1.0 (c (n "atomic-file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)))) (h "0hr7vrmvppg2wd6r9gq5ifsfpxcnzqfsqr8k4241mb72vpcf67ra")))

(define-public crate-atomic-file-0.2.0 (c (n "atomic-file") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)))) (h "10z3n9il8vzaba46angj2f9xms8ixj4lgkkdamwis5hxy8yb5ccc")))

(define-public crate-atomic-file-0.2.1 (c (n "atomic-file") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)))) (h "1118546xdg6vgjs43i02mhz316a7n0h3if6ir7d20hqjkwr2dczw")))

(define-public crate-atomic-file-0.3.0 (c (n "atomic-file") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)))) (h "1pvisjyc9vc572yjpi1zzl06dg86wihn731micrg63if0d8lj95i")))

(define-public crate-atomic-file-0.4.0 (c (n "atomic-file") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)))) (h "00k86s4bichg90kv57y05s3pyds0dicsph6xg1b1hblfvs02qbb0")))

(define-public crate-atomic-file-0.5.0 (c (n "atomic-file") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1x3yhyvazf5rskd27fynbplrfbnmmmm9z0w2allkvlsfmwq01y5j")))

(define-public crate-atomic-file-0.6.0 (c (n "atomic-file") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "12dd1nb6d3v35mg5wx260ahaisxsq73bm5qg2r405877cjnd41ms")))

(define-public crate-atomic-file-0.6.1 (c (n "atomic-file") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "177lbgg0klqad2r8kpr7mq13wk9b50wbniy1iabijbsm1qi47h3n")))

(define-public crate-atomic-file-0.7.3 (c (n "atomic-file") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cv1q8yya68ny95wjd8askmj58zzid0qyy55vkcs7y5zd87i1j9i")))

(define-public crate-atomic-file-0.8.3 (c (n "atomic-file") (v "0.8.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1wb7kybz6vikf0kys1a16g80qzs0sc7s5ishbnlp63g45q0rflxy")))

(define-public crate-atomic-file-0.9.3 (c (n "atomic-file") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1p6wlpl71q8bv3j0d798gzld3n9cc5r8df9s1f5y4a57yd8lgdig")))

(define-public crate-atomic-file-0.10.3 (c (n "atomic-file") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "15ky8ph5kknd01z845vk3636pdv0q35440gcv5yn4n8rx0fyqkfs")))

(define-public crate-atomic-file-1.0.0 (c (n "atomic-file") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "testdir") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "14ymp2yq50y91310a7cji2gabvadx7jci31lkjc8zr9j88ijp9b8")))

