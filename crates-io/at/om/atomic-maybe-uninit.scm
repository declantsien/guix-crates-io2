(define-module (crates-io at om atomic-maybe-uninit) #:use-module (crates-io))

(define-public crate-atomic-maybe-uninit-0.1.0 (c (n "atomic-maybe-uninit") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0vdn0jli5kjv6qswmb0c36g20f4gyjybii84kl0gxh677k92n8k5") (f (quote (("raw")))) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.0 (c (n "atomic-maybe-uninit") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 2)))) (h "04lsy4cv98c30qmi4a4fiim07yvag46j3jwc0qc8m2nb9mkmvy9s") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.1 (c (n "atomic-maybe-uninit") (v "0.2.1") (d (list (d (n "paste") (r "^1") (d #t) (k 2)))) (h "193ax9681w1j6xic98fq1brjy7i93vjfngdnh4vyp2ri8spnh78z") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.2 (c (n "atomic-maybe-uninit") (v "0.2.2") (d (list (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1ckcnjv7m45r6p5acvx941jm7vll3zjv5949zazqfcj19mzd1n9w") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.3 (c (n "atomic-maybe-uninit") (v "0.2.3") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "087ijs8kcsi8j156k3lsi5n3bhp9zzq36an3dqr6ywgl4xxaasgc") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.4 (c (n "atomic-maybe-uninit") (v "0.2.4") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "03xv8zg01cz6i5bkyl74qaff69d4kfmwg93qxh4233apvw7k87vc") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.5 (c (n "atomic-maybe-uninit") (v "0.2.5") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "03hqfa2bhs87ady1z4fc4qvwzfmxl8fzjkmp3f0jvifx5jqmgvvv") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.6 (c (n "atomic-maybe-uninit") (v "0.2.6") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1xrdv65l6zrxxxlpxij22ffcyakz9kf1vlkd9c5vds9vkczx5apg") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.7 (c (n "atomic-maybe-uninit") (v "0.2.7") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1zvnygjyjaxhspqbpvhfgzkz2ppiivqwx0vc198phraxwip2pczw") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.8 (c (n "atomic-maybe-uninit") (v "0.2.8") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1ha9i2gq2ivjz6v5v0d7h0ihk6kdzjj86pxrgc9i4x740p89dpyi") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.9 (c (n "atomic-maybe-uninit") (v "0.2.9") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "01qh7118ql5jbmjm8j1msfy48yrzzh31rq1swnbfhbs9xdnk3453") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.10 (c (n "atomic-maybe-uninit") (v "0.2.10") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0yb8pmkxfxma1yaif6g9vnf3928ag603kv943fsxxh4xmp8m6r6f") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.11 (c (n "atomic-maybe-uninit") (v "0.2.11") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0cn3p8zhfcvrgg5c7x45k286jzfrryb7sivb4zzssj1a7h360chv") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.12 (c (n "atomic-maybe-uninit") (v "0.2.12") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0kzrdvcwzqyng9kf822l7ldv2h2mnv70r3x5ai2885bvzkhsn38x") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.13 (c (n "atomic-maybe-uninit") (v "0.2.13") (d (list (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0glgg49xsz06vg8880rm5bds4hz0clfjmdhhmxm982m34ydn3ngf") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.14 (c (n "atomic-maybe-uninit") (v "0.2.14") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0z9hjmfg3nal1a3xqzpswr04ng32mzwa2l99aw4sxr2qg508qiyh") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.15 (c (n "atomic-maybe-uninit") (v "0.2.15") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1bg7jlhmsgcm1afczbwjf5klx2f27dy981grd2jy2aziywlyw453") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.16 (c (n "atomic-maybe-uninit") (v "0.2.16") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "1fis2rqsan61abzsqsmmy7vw1zyjnfc3ff1klwz2dx7qpcsxlzb6") (y #t) (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.17 (c (n "atomic-maybe-uninit") (v "0.2.17") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0zmmfl2g4s8lcbwymh1ip0y4jwk284sz3m0a6gizjbc8zg99md16") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.18 (c (n "atomic-maybe-uninit") (v "0.2.18") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "030c7b2n2gzq4h19mpkm0yzvinv73y4zm3n2xy3ga57161h89qld") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.19 (c (n "atomic-maybe-uninit") (v "0.2.19") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0wh3v6vq23zgy1zlsqjnv6qcsg4f6qs0gsc5jc1d8j7vr7k50af2") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.20 (c (n "atomic-maybe-uninit") (v "0.2.20") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0l30qmpk3424di58awras67i4hc9yv7xh4l9xyvhv8bz179d0x54") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.21 (c (n "atomic-maybe-uninit") (v "0.2.21") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0vbxdyf1mmgr04z3dcwdsq71rqjgk7bcws95lbv8xy1wvbxwsbnj") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.2.22 (c (n "atomic-maybe-uninit") (v "0.2.22") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0vlyiv5zvbbz5d3b1c98kasa80yyj67ama7qx04pxk02g42sv881") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.3.0 (c (n "atomic-maybe-uninit") (v "0.3.0") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1wldbzf4pbsl340bhnr8f5vv9fvcy80wsm5jcyknp57fyxz5rzrs") (r "1.59")))

(define-public crate-atomic-maybe-uninit-0.3.1 (c (n "atomic-maybe-uninit") (v "0.3.1") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0b76h8qn3wwxixv2x5vs5xnqkgqdqiqidnnd19pg2njq0h3fq1dh") (r "1.59")))

