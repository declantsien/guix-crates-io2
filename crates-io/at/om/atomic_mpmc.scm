(define-module (crates-io at om atomic_mpmc) #:use-module (crates-io))

(define-public crate-atomic_mpmc-0.1.0 (c (n "atomic_mpmc") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "sealed") (r "^0.4.0") (d #t) (k 0)))) (h "03pvshvjlr4cw0934j5vqs7qq7l7s6b314794mwmrjpslxnhpcwg") (y #t)))

(define-public crate-atomic_mpmc-0.1.1 (c (n "atomic_mpmc") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "sealed") (r "^0.4.0") (d #t) (k 0)))) (h "1kzjigqa7nqfr6m7fad3gdzgrcklp9mbs282hjfrgws8wg93ivay") (y #t)))

(define-public crate-atomic_mpmc-0.1.2 (c (n "atomic_mpmc") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "sealed") (r "^0.4.0") (d #t) (k 0)))) (h "099xj7l2rd195fsw5kr8q69v43vqhzlx428vhidclc0014cnzvq8") (y #t)))

(define-public crate-atomic_mpmc-0.2.0 (c (n "atomic_mpmc") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "sealed") (r "^0.4.0") (d #t) (k 0)))) (h "0qcsakxp7586jrv1iaxy4g1mb7rj0mk4nfyfsgw0wd6j7w6x911f") (y #t)))

