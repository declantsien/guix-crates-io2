(define-module (crates-io at om atom_syndication) #:use-module (crates-io))

(define-public crate-atom_syndication-0.1.0 (c (n "atom_syndication") (v "0.1.0") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "0s74lwmdh9scx3qhq3ibi7z7zym2g87zddax2dgqrjj19058xxk1")))

(define-public crate-atom_syndication-0.1.1 (c (n "atom_syndication") (v "0.1.1") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "1w5n4yh9hwq1qqndkcc8vlhzy1prlmi69fb2rwiakp53b1dcf1ga")))

(define-public crate-atom_syndication-0.1.2 (c (n "atom_syndication") (v "0.1.2") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "08dclxqsh54wgk8d8m3mp69cj9zq8sgmvpb3vzm4xfqrppf8c81z")))

(define-public crate-atom_syndication-0.1.3 (c (n "atom_syndication") (v "0.1.3") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "0xv6j5wjxzrw7sy2cvfn43cnkpffpqzqsx1qv6bpbpvq9vdf4yy9")))

(define-public crate-atom_syndication-0.1.4 (c (n "atom_syndication") (v "0.1.4") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "1b1j7z6wizsz95n30lqkddhd6ki82crf9ld1gdd1dssl1pga3c2j")))

(define-public crate-atom_syndication-0.2.0 (c (n "atom_syndication") (v "0.2.0") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "0f35zzbp5r4l44pw2079sjhfhsib8zpn66qfl9673y8d8kkr4sic")))

(define-public crate-atom_syndication-0.3.0 (c (n "atom_syndication") (v "0.3.0") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "13x761y50kkiy8cyrapihbwn7ipnbbnd975aqnjs7gcdxd3k8q1d")))

(define-public crate-atom_syndication-0.4.0 (c (n "atom_syndication") (v "0.4.0") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "0c69l6ak94ldfzy616x29xp96llylf7hksnl783qcjzsss2x3235")))

(define-public crate-atom_syndication-0.4.1 (c (n "atom_syndication") (v "0.4.1") (d (list (d (n "RustyXML") (r "^0.1") (d #t) (k 0)))) (h "17lbxgx2xsfi92c4kpq85y2yq14ab803syv5w2ijap5c7iznnd73")))

(define-public crate-atom_syndication-0.5.0 (c (n "atom_syndication") (v "0.5.0") (d (list (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "0wsmddxsz48wnys4f23ng4xx28311jbz3ax3whj8jbczpl0as731")))

(define-public crate-atom_syndication-0.5.1 (c (n "atom_syndication") (v "0.5.1") (d (list (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "1qgv5ihdwpcx0c6yf1jqa71jbz40zl278snf6wpv3s23dpi2z1s1")))

(define-public crate-atom_syndication-0.5.2 (c (n "atom_syndication") (v "0.5.2") (d (list (d (n "derive_builder") (r "^0.4") (f (quote ("private_fields"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "1rhxcwfvhlp0y0sfb4nhm98j197ra7b19m47ys6jrfx03hd8gkz1")))

(define-public crate-atom_syndication-0.5.3 (c (n "atom_syndication") (v "0.5.3") (d (list (d (n "derive_builder") (r "^0.4") (f (quote ("private_fields"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "1p2q9blcn85ir41l3qghdg0zb8dsmfd94xq4fmj0plasmahbmx58")))

(define-public crate-atom_syndication-0.5.4 (c (n "atom_syndication") (v "0.5.4") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "11jjb35zmlcl1j0dzqqdrv5kvxlh482b7jq6malm26vw42vaasly")))

(define-public crate-atom_syndication-0.5.5 (c (n "atom_syndication") (v "0.5.5") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.8") (d #t) (k 0)))) (h "0bi3m47inmxh8dghn04p7m7z0j1gyk7c9866za9fyyzvymgrkdjm")))

(define-public crate-atom_syndication-0.5.6 (c (n "atom_syndication") (v "0.5.6") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)))) (h "09mn5l51vngzlgfyj2kgapw98vx6jr5s4kgv51afc0q9z3y8q6qn")))

(define-public crate-atom_syndication-0.5.7 (c (n "atom_syndication") (v "0.5.7") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)))) (h "0h8vb6pf1c0vdlx4n5clyv5zr79hn1r93w2vyb8px8lrnfa96lhi")))

(define-public crate-atom_syndication-0.5.8 (c (n "atom_syndication") (v "0.5.8") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bw5wxlh145jynq94k6gwx1d2jm5g82x47cydsws7c8q27z95883")))

(define-public crate-atom_syndication-0.6.0 (c (n "atom_syndication") (v "0.6.0") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "037nsvk9wxjwzj8fgcbsjjdwrp14agcllvw50hxpmzrm6sw7m6ha")))

(define-public crate-atom_syndication-0.7.0 (c (n "atom_syndication") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13zy392igfzd9a7wxdms6pkhzwngd1p54sdcqfp8nqscwxn91b0h") (f (quote (("with-serde" "serde" "chrono/serde"))))))

(define-public crate-atom_syndication-0.8.0 (c (n "atom_syndication") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ix7rw1g1l97yz7milp0v3j5i8ydib182q9x01q7g3rqvywq8xk8") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder"))))))

(define-public crate-atom_syndication-0.9.0 (c (n "atom_syndication") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12q0y041rfvwilx7mwlp4fb7nvia8v0jb2ykw7xw8j6wmskjy2vx") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder"))))))

(define-public crate-atom_syndication-0.9.1 (c (n "atom_syndication") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18m3axxsxkx8jygnnpkx2d5sygdmjvnizv7kig93wkzzaazicl1d") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder"))))))

(define-public crate-atom_syndication-0.10.0 (c (n "atom_syndication") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pl64bpzz21awyh8fx00fz6lpawq9ab3j1fdbchy5z24ii1mbvsm") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

(define-public crate-atom_syndication-0.11.0 (c (n "atom_syndication") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "derive_builder") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hbmmlvll9hjxrrirbjdb91445y5adz17y26zvd7wlf6745nmyr1") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

(define-public crate-atom_syndication-0.12.0 (c (n "atom_syndication") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13537bvcqrx66sc5b6jfkgciyrklvkv61118j77ixgccw8p5za4i") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

(define-public crate-atom_syndication-0.12.1 (c (n "atom_syndication") (v "0.12.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0v73a1xfl8gj1iny6w7pgph1mg9hw5avqjx8fcany8yqwcwcp5na") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

(define-public crate-atom_syndication-0.12.2 (c (n "atom_syndication") (v "0.12.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "derive_builder") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19qm7qwbccnmhlxkl3fl05rmir9x936k0scf5rb2cpkpzzf3462p") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

(define-public crate-atom_syndication-0.12.3 (c (n "atom_syndication") (v "0.12.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "derive_builder") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "diligent-date-parser") (r "^0.1.3") (d #t) (k 0)) (d (n "never") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("encoding"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c4a5jk4wc6jk64ilvibhhs3y9n9ndniah02xzdwjcbzj09ldwzj") (f (quote (("with-serde" "serde" "chrono/serde") ("default" "builders") ("builders" "derive_builder" "never"))))))

