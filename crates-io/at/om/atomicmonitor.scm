(define-module (crates-io at om atomicmonitor) #:use-module (crates-io))

(define-public crate-atomicmonitor-0.1.0 (c (n "atomicmonitor") (v "0.1.0") (d (list (d (n "atomic") (r "^0.4.1") (d #t) (k 0)) (d (n "monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mjfz8q3b3hvsz1m3n2ww6h66bm2gd5sikrxqdz7dgjw98wp222p")))

(define-public crate-atomicmonitor-0.1.1 (c (n "atomicmonitor") (v "0.1.1") (d (list (d (n "atomic") (r "^0.4.1") (d #t) (k 0)) (d (n "monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08wz5bx7x9nrvb6n1yaskdswk2l77ngy71xzvn1sapibg4xjsl4v")))

(define-public crate-atomicmonitor-0.1.2 (c (n "atomicmonitor") (v "0.1.2") (d (list (d (n "atomic") (r "^0.4.1") (d #t) (k 0)) (d (n "monitor") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "06qknq9inaj7a7saw66ll4n3f03hx21khh9nskzr8bkijlrlmx5p")))

