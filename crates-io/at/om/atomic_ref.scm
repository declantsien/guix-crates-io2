(define-module (crates-io at om atomic_ref) #:use-module (crates-io))

(define-public crate-atomic_ref-0.1.0 (c (n "atomic_ref") (v "0.1.0") (h "0337y21226bm7m14j0s240yaml2l5nhb5d7kwlh0mjvl2hp8p6yq")))

(define-public crate-atomic_ref-0.1.1 (c (n "atomic_ref") (v "0.1.1") (h "054pri04y04zr6qfs8aghf4v7hhlg1i22ql6r369inqmj55ai1hq")))

(define-public crate-atomic_ref-0.1.2 (c (n "atomic_ref") (v "0.1.2") (h "0xkglv881d6wnp9w55csvasq2dv0zqa9b1jn76pnz6x8b8q98bv0")))

(define-public crate-atomic_ref-0.2.0 (c (n "atomic_ref") (v "0.2.0") (h "10x4slasq61mwjkiwmh8630kk6g3zkz1fdyd9wn2m6x7ykg0c5fm") (y #t)))

(define-public crate-atomic_ref-0.2.1 (c (n "atomic_ref") (v "0.2.1") (h "1q65ix6a0vaphyw28gf2ndcd7gncyr6p4k2hb31sp5m9idc0pym6")))

