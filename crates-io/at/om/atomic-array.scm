(define-module (crates-io at om atomic-array) #:use-module (crates-io))

(define-public crate-atomic-array-0.1.0 (c (n "atomic-array") (v "0.1.0") (h "0qimdapyx0ajaf8kmgs0b3x8ws1s6mm947cfdpi4kr853hpp56h4")))

(define-public crate-atomic-array-0.2.0 (c (n "atomic-array") (v "0.2.0") (h "05rcs9vjbjawr367qbb9y1958qq7k32kvxkp36qj9j7rg8d4z7f2")))

(define-public crate-atomic-array-0.2.1 (c (n "atomic-array") (v "0.2.1") (h "1rxv3v1hcynba2kdy0sqpczm2hcajccdy6104bq236vkd675lgfs")))

(define-public crate-atomic-array-0.3.0 (c (n "atomic-array") (v "0.3.0") (d (list (d (n "atomic-ref2") (r "^0.1") (d #t) (k 0)))) (h "0f2s7a7zlbyzdvny9j08j3vl84si37il8958cw9drl6nx9mn5jwz")))

(define-public crate-atomic-array-0.3.1 (c (n "atomic-array") (v "0.3.1") (d (list (d (n "atomic-ref2") (r "^0.2") (d #t) (k 0)))) (h "1gzfkfhg0iy8c2b86w2v10w4vvc0mpx86bvdh0m3mwqa667mf4yd")))

