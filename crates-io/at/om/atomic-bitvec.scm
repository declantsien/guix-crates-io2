(define-module (crates-io at om atomic-bitvec) #:use-module (crates-io))

(define-public crate-atomic-bitvec-0.1.0 (c (n "atomic-bitvec") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1pmc66rx4yga9qq620r7x8pjpq6fvx8kvg2llzh6ggfc7vdl7ia5")))

(define-public crate-atomic-bitvec-0.1.1 (c (n "atomic-bitvec") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1h237xys58sp7gdk7b5j51452kvhmd8wkgvd1azf9axf4m4s6vj1")))

