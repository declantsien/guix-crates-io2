(define-module (crates-io at om atomic_macro) #:use-module (crates-io))

(define-public crate-atomic_macro-0.1.0 (c (n "atomic_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q1r27afkdgggikqixv33sh9cfv4aqm1bchjiyfjz2rzl0pkh9zv")))

(define-public crate-atomic_macro-0.1.1 (c (n "atomic_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jqckqp31gbwfa04piy74w9b4qfqdz1gknhf5pkrq03qfgsaqnr9")))

