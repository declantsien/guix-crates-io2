(define-module (crates-io at om atomic_pingpong) #:use-module (crates-io))

(define-public crate-atomic_pingpong-0.1.0 (c (n "atomic_pingpong") (v "0.1.0") (h "05cy1pmmfqc48aza7n9g88pkbh1cxm71yvbkdd8anjwil8w54r7v")))

(define-public crate-atomic_pingpong-0.2.0 (c (n "atomic_pingpong") (v "0.2.0") (h "0zhhxvfi25qmq3xl4nqa1grj73mk30a2a98kq2r78jk5whwrxzhz")))

(define-public crate-atomic_pingpong-0.2.1 (c (n "atomic_pingpong") (v "0.2.1") (h "0in1yywx96zlikady30wcylbzm26bqc41ix414bys756nwnsc06h")))

(define-public crate-atomic_pingpong-0.2.2 (c (n "atomic_pingpong") (v "0.2.2") (h "05x65s1kqd19crjgdjn1s7j2qkffrwq0xlhyp82b9d0wi29fa51d")))

(define-public crate-atomic_pingpong-0.2.3 (c (n "atomic_pingpong") (v "0.2.3") (h "0cjc630545lfyz981dgr2hjqsyxsjgvrhq0w3jbdvj6ix2knsycm")))

