(define-module (crates-io at om atomic-waker) #:use-module (crates-io))

(define-public crate-atomic-waker-0.0.1 (c (n "atomic-waker") (v "0.0.1") (h "0f3skadpwrq6bkf4jyi36b9bbkf9arfqivr7bs4phda76c6mvzyi")))

(define-public crate-atomic-waker-1.0.0 (c (n "atomic-waker") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)))) (h "0ansiq5vlw684fhks2x4a4is2rqlbv50q5mi8x0fxxvx5q2p8lq6")))

(define-public crate-atomic-waker-1.1.0 (c (n "atomic-waker") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "16a5ivx4h92r69w00gja07y008ylkrimylkba13rx7z6wbfjkg6y") (r "1.36")))

(define-public crate-atomic-waker-1.1.1 (c (n "atomic-waker") (v "1.1.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)))) (h "1lr6gzsmxs49fp1kjjngz6asfz8ncxfpkbmi7nh9drzws7hf308i") (r "1.36")))

(define-public crate-atomic-waker-1.1.2 (c (n "atomic-waker") (v "1.1.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("cargo_bench_support"))) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)))) (h "1h5av1lw56m0jf0fd3bchxq8a30xv0b4wv8s4zkp4s0i7mfvs18m") (r "1.36")))

