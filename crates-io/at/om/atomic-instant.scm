(define-module (crates-io at om atomic-instant) #:use-module (crates-io))

(define-public crate-atomic-instant-0.1.0 (c (n "atomic-instant") (v "0.1.0") (d (list (d (n "quanta") (r "^0.7") (d #t) (k 0)))) (h "0s10a5ps0in2qfy3rpzaxw7fvyhjrv345d70hq3azsx4vs8cr0pc")))

(define-public crate-atomic-instant-0.1.1 (c (n "atomic-instant") (v "0.1.1") (d (list (d (n "quanta") (r "^0.7") (d #t) (k 0)))) (h "19ismkmxgwms61wzy5avlpnvsys7l546jjnm7ap66qv90szz4mww")))

