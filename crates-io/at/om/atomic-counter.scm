(define-module (crates-io at om atomic-counter) #:use-module (crates-io))

(define-public crate-atomic-counter-0.1.0 (c (n "atomic-counter") (v "0.1.0") (h "19q7ryjbykdj8ngbabhbaxad2q4q4j1k28rcpvah1m6g23slw8va")))

(define-public crate-atomic-counter-1.0.0 (c (n "atomic-counter") (v "1.0.0") (h "0vxhg98xjpzbyxlldva67wdj8lbg51lz7bqdbq47hr5xijbwca5d")))

(define-public crate-atomic-counter-1.0.1 (c (n "atomic-counter") (v "1.0.1") (h "06b0sl39a5mhkqy1bqbw3gnvar9av81sfqn8q6q9lnpsikb4gx32")))

