(define-module (crates-io at om atomic-arena) #:use-module (crates-io))

(define-public crate-atomic-arena-0.1.0 (c (n "atomic-arena") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0r98kr60im3z5h9qcxsmm55v69b9vbwh2ibjjg78cbaxcczpia3k")))

(define-public crate-atomic-arena-0.1.1 (c (n "atomic-arena") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "045d9qi8nwfan0308m9hc2lwq35kp9g9fzkj41azvg2srslfql2l")))

