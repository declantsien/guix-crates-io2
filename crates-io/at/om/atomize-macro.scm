(define-module (crates-io at om atomize-macro) #:use-module (crates-io))

(define-public crate-atomize-macro-0.1.0 (c (n "atomize-macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "15lmq5p86sz8li91sdcfl1mvnrnvlj40j5pwq1j1mwww0dnpfrhc")))

(define-public crate-atomize-macro-0.1.2 (c (n "atomize-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bmgq5ncqcala4v2mq3w41hy70iv6rvqfj00dwwjn0d07qf2hp3g")))

(define-public crate-atomize-macro-0.1.3 (c (n "atomize-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zjr9sfry3zpa0wm0krgnmkdsh8b4y0q8wqfmfdn77fk06r29nx0")))

