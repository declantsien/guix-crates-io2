(define-module (crates-io at om atomic_refcell) #:use-module (crates-io))

(define-public crate-atomic_refcell-0.1.0 (c (n "atomic_refcell") (v "0.1.0") (h "089g40fmc880q5wsq12saa6k92skiswlph1wjiv05wimdmpcnbgv")))

(define-public crate-atomic_refcell-0.1.1 (c (n "atomic_refcell") (v "0.1.1") (h "0di5wbr127dymp6rn20d66yi7avja6b0s52kadq9in5yldcrw86p")))

(define-public crate-atomic_refcell-0.1.2 (c (n "atomic_refcell") (v "0.1.2") (h "00ic5kxzkbhv2jn37hp349k2xps0gn5h6zfnhcv13rcy3r6in29d")))

(define-public crate-atomic_refcell-0.1.3 (c (n "atomic_refcell") (v "0.1.3") (h "13kmqvfv150cqygbmk5rarvfh626a6yzvdqk1wzhvk1dg7ji195m")))

(define-public crate-atomic_refcell-0.1.4 (c (n "atomic_refcell") (v "0.1.4") (h "09nq429n2yyfiw416qv31xjarpazc69d3m1fafd2y81awjyspj6g")))

(define-public crate-atomic_refcell-0.1.5 (c (n "atomic_refcell") (v "0.1.5") (h "07fqj268i5j4ng3fs16h374l3qvkja3fvx9phlqbcnf09hz09gry")))

(define-public crate-atomic_refcell-0.1.6 (c (n "atomic_refcell") (v "0.1.6") (h "17skn8zwz30zqdzmss2kggg0cy11ysayp7ash569gavy0v71vhrv")))

(define-public crate-atomic_refcell-0.1.7 (c (n "atomic_refcell") (v "0.1.7") (h "0zs2hpyjhpxxg36ng3bpxs39333ip0v05jmgzhh6pxz06q99f6v8")))

(define-public crate-atomic_refcell-0.1.8 (c (n "atomic_refcell") (v "0.1.8") (h "03cpdszm2vj3fhf9gv35pyh6anlr64pr7p6yablh8zwjigsfbdbk")))

(define-public crate-atomic_refcell-0.1.9 (c (n "atomic_refcell") (v "0.1.9") (h "1j7n2hr4wfi0j6k7mbdh86gfg5kaa0ayzw3kv47rvg97g0v56wl5")))

(define-public crate-atomic_refcell-0.1.10 (c (n "atomic_refcell") (v "0.1.10") (h "0caxcb4hhnbya1cwpsawmsp5sdbnh1jb4q1zaw3b14i75a9drmkr")))

(define-public crate-atomic_refcell-0.1.11 (c (n "atomic_refcell") (v "0.1.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01f9c4l5fz8g1ghlvvxh3bzjqja8m3vlwjbbbgybcg6bysrzcbhi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-atomic_refcell-0.1.12 (c (n "atomic_refcell") (v "0.1.12") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "087h5zn3fagd1sc6vs299akfrzppfj1dlcc4gd84a7flj7jbzwkn") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-atomic_refcell-0.1.13 (c (n "atomic_refcell") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z04ng59y22mwf315wamx78ybhjag0x6k7isc36hdgcv63c7rrj1") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

