(define-module (crates-io at om atomig) #:use-module (crates-io))

(define-public crate-atomig-0.1.0 (c (n "atomig") (v "0.1.0") (d (list (d (n "atomig-macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0ghy2w7q9h90z9800nc7947aravmzsk1y5l0am8wjl8ywqmkcfw0") (f (quote (("nightly") ("derive" "atomig-macro"))))))

(define-public crate-atomig-0.2.0 (c (n "atomig") (v "0.2.0") (d (list (d (n "atomig-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0fiqgixp1d3g5zny3w1v5p1dwdwng9cgfqyrfv0nz9nl4yjx80jq") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.3.0 (c (n "atomig") (v "0.3.0") (d (list (d (n "atomig-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0ag8v9qkzh44i0khfmh8d9xg9dsps486clfhp44m1vnp7dj9kggw") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.3.1 (c (n "atomig") (v "0.3.1") (d (list (d (n "atomig-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0k8r9l03hg71fxnsskvl8gshmyabym1vgz81mbcxv8dybpdcfga1") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.3.2 (c (n "atomig") (v "0.3.2") (d (list (d (n "atomig-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1nwzhijbasdfwzxr4sb9pwqnibdjy3iss8pbplng78r2vmg1garv") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.3.3 (c (n "atomig") (v "0.3.3") (d (list (d (n "atomig-macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "14vfq563gnzd0fqaxry3q16rkgbqjmshdpaq6gj3gpa8qhd5l30c") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.4.0 (c (n "atomig") (v "0.4.0") (d (list (d (n "atomig-macro") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "03ygasjgpiwily28zzphr89ylva38s0fx0w9ckkm9hr06w5g5v7w") (f (quote (("derive" "atomig-macro"))))))

(define-public crate-atomig-0.4.1 (c (n "atomig") (v "0.4.1") (d (list (d (n "atomig-macro") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "08f8mhww13dskj55rw37h9d2bwghqxvp7q70mg826y8zn4bjmbqf") (f (quote (("derive" "atomig-macro"))))))

