(define-module (crates-io at om atomicwrites) #:use-module (crates-io))

(define-public crate-atomicwrites-0.0.1 (c (n "atomicwrites") (v "0.0.1") (h "1wiz8fy0bvk3lz5hqjiiiljh8l47kkilz60pf566akdn57xqipsh") (y #t)))

(define-public crate-atomicwrites-0.0.2 (c (n "atomicwrites") (v "0.0.2") (h "18k5k0jm0p7ym195v7giv6q4sw0vs4xl4fnhdwk2h8si44j95bm2") (y #t)))

(define-public crate-atomicwrites-0.0.3 (c (n "atomicwrites") (v "0.0.3") (h "1001zibj88d5nd5bz3b2kikq6w63a14jc1x723xxqfxmjm0nflix")))

(define-public crate-atomicwrites-0.0.4 (c (n "atomicwrites") (v "0.0.4") (h "1l4l9k6d5nihsg931df9bc2nch235yhyyji7bxwgylz055nvhn7f")))

(define-public crate-atomicwrites-0.0.5 (c (n "atomicwrites") (v "0.0.5") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1swdinr16fzs99ly55bv1mx2ppd37ndl1psp5vx21g7dyz0d8chq")))

(define-public crate-atomicwrites-0.0.6 (c (n "atomicwrites") (v "0.0.6") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0q1f05zpl34d2965ivxfd81d9ydqa2ps73g8hjmjw8lw2vlmc4a4")))

(define-public crate-atomicwrites-0.0.7 (c (n "atomicwrites") (v "0.0.7") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0qr0ya1d02zndd524j2wk8869kdygx0wgmdzp8vpklvqhak7bzfp")))

(define-public crate-atomicwrites-0.0.8 (c (n "atomicwrites") (v "0.0.8") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0j99lhajqg65kh40wblg5zp6lwzm3maqkyaz0mbn5i380a1gwdc8")))

(define-public crate-atomicwrites-0.0.9 (c (n "atomicwrites") (v "0.0.9") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1qn27m606310dpgyqb66n3pl5z1b42a7rbyv0vklwlkggp49xzav")))

(define-public crate-atomicwrites-0.0.10 (c (n "atomicwrites") (v "0.0.10") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0flpr5k5vnqp7w7hmkid75wkrxqp0a139i0ll3nf2nqgw7gnrzdl")))

(define-public crate-atomicwrites-0.0.11 (c (n "atomicwrites") (v "0.0.11") (d (list (d (n "kernel32-sys") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0979855vl6gscv64ik4l5nc5qxwx29y9s6k97drgrv9wc8115rdg")))

(define-public crate-atomicwrites-0.0.12 (c (n "atomicwrites") (v "0.0.12") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "13iv0b7yf4albbk97q3asmzq2prsiq4zvs10jni2rpf9jzk44dzz")))

(define-public crate-atomicwrites-0.0.13 (c (n "atomicwrites") (v "0.0.13") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "1znrvya37w12n49s9f0nqihd30plx4pwjlhym27q1p5zz3zrqsqd")))

(define-public crate-atomicwrites-0.0.14 (c (n "atomicwrites") (v "0.0.14") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0mk5ns3sfwh1j7wf8778m64n35bw5gf6ggchxhzivql7hnb1f36b")))

(define-public crate-atomicwrites-0.1.0 (c (n "atomicwrites") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1w2zcq2v2bl201yyvmrzyqqqr42jpk4rn7bpa4qx2gfbi4jwdplf")))

(define-public crate-atomicwrites-0.1.1 (c (n "atomicwrites") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0nvjj4nx7pnq7ahyfxf89js4cy9rnvcyb7q3g5fsr489v8i121q8")))

(define-public crate-atomicwrites-0.1.2 (c (n "atomicwrites") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "02c77rsp1arw7j2ipn2y68fp2vmq2kn9py19by41i9szq3i2r988")))

(define-public crate-atomicwrites-0.1.3 (c (n "atomicwrites") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0vyihgwka1bwflyswz7aj0zwgv2dyyh7cyaqhb2ysjmglhzqscn4")))

(define-public crate-atomicwrites-0.1.4 (c (n "atomicwrites") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1kzijjxc04rhh0k44hpfsjajrmjalyb6yhi5isl8135dmm7dsq25")))

(define-public crate-atomicwrites-0.1.5 (c (n "atomicwrites") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1jiwvrbwisg0a3li0yjqhk9rfvd2jf0sbbjcrq0znmkbcvrz1pvs")))

(define-public crate-atomicwrites-0.2.0 (c (n "atomicwrites") (v "0.2.0") (d (list (d (n "nix") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vzchqlg44hkfrp7gpla07pa2w5wxmi5571sagymnq836yn4bz2r")))

(define-public crate-atomicwrites-0.2.1 (c (n "atomicwrites") (v "0.2.1") (d (list (d (n "nix") (r "^0.9.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rkx1ccla6ghwhlxhyzmgj07ikg6jja4bx2ibaiq0kyx3d5k6fbp")))

(define-public crate-atomicwrites-0.2.2 (c (n "atomicwrites") (v "0.2.2") (d (list (d (n "nix") (r "^0.11.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qxn3sgdiqv3qy9ffaqgxcsc0p60kxrj7p6x4frgxlzgrlrhnhm3")))

(define-public crate-atomicwrites-0.2.3 (c (n "atomicwrites") (v "0.2.3") (d (list (d (n "nix") (r "^0.14.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14v3pc04nmvnm965l8965r7r3paiklybxkq49cmj141wa6csmqf4")))

(define-public crate-atomicwrites-0.2.4 (c (n "atomicwrites") (v "0.2.4") (d (list (d (n "nix") (r "^0.14.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g73kf9xlsya1bqhsfjmxipq1dd5rpgmgc44y9m29dnxr4wf5p6v")))

(define-public crate-atomicwrites-0.2.5 (c (n "atomicwrites") (v "0.2.5") (d (list (d (n "nix") (r "^0.14.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07v5ai5sj8i53lrcxmvnrfhj18hlb57wrlbs7k2rj0l2xcpsyava")))

(define-public crate-atomicwrites-0.3.0 (c (n "atomicwrites") (v "0.3.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sw7rcl9p963cadwyxp5hh0mv9pa8y6d31kfy13hn796j330m0yl")))

(define-public crate-atomicwrites-0.3.1 (c (n "atomicwrites") (v "0.3.1") (d (list (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xizggh2md0m6m04aizbpv5y7lfvlzm9fgwx5873r99gjvb2r3zb")))

(define-public crate-atomicwrites-0.4.0 (c (n "atomicwrites") (v "0.4.0") (d (list (d (n "rustix") (r "^0.36.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1grqlqj2ihmcnf7469x3wdr5jkb1r3l1vdyx7qrp08zwq2zq1989")))

(define-public crate-atomicwrites-0.4.1 (c (n "atomicwrites") (v "0.4.1") (d (list (d (n "rustix") (r "^0.37.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kkkh0awmdjz515cwln72zgyj4cdi3lgavcxnyi53pjigjfks5n1")))

(define-public crate-atomicwrites-0.4.2 (c (n "atomicwrites") (v "0.4.2") (d (list (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fpdx49czh109by54qhssjba5mlgvv30wyz58i2i9v9544v5zm7l")))

(define-public crate-atomicwrites-0.4.3 (c (n "atomicwrites") (v "0.4.3") (d (list (d (n "rustix") (r "^0.38.0") (f (quote ("fs"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nvqwkbcq6bq7h9nqxqc47y73jfx1wc13s11z3q9l1b9j6z2syzw")))

