(define-module (crates-io at om atomic-try-update) #:use-module (crates-io))

(define-public crate-atomic-try-update-0.0.1 (c (n "atomic-try-update") (v "0.0.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "test-util"))) (d #t) (k 2)))) (h "1vp5xy72rar4r57iq0d5sj57w4g73a4imc6xa1jiif8f8zvk4aq6")))

(define-public crate-atomic-try-update-0.0.2 (c (n "atomic-try-update") (v "0.0.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.13") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("macros" "rt-multi-thread" "test-util"))) (d #t) (k 2)))) (h "0sy1q65nwvp9v2rv3hk6iwx0r5kn4fcia32v69sf4gmfx396blvk")))

