(define-module (crates-io at om atomic_hooks) #:use-module (crates-io))

(define-public crate-atomic_hooks-0.1.0 (c (n "atomic_hooks") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "atomic_hooks_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "illicit") (r "^0.9.2") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "topo") (r "=0.9.4") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0kz35y2mf2fbg9kq0i7pb58fl6yfld8myllm39jwj05pcxgjz7bm")))

