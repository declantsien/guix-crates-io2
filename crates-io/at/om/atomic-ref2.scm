(define-module (crates-io at om atomic-ref2) #:use-module (crates-io))

(define-public crate-atomic-ref2-0.1.0 (c (n "atomic-ref2") (v "0.1.0") (h "17vq0cag0aw2jbix7fi08837pviszdnhg9qz59gvdk8mqsvy0hgj")))

(define-public crate-atomic-ref2-0.2.0 (c (n "atomic-ref2") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1300ywwskx48123yigj575dw795dla625sylfl18gw5a67vi4r6i")))

(define-public crate-atomic-ref2-0.2.1 (c (n "atomic-ref2") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "08wkjn294x8irycnzwzi2ssdc4pkmh8qiyxrfr092x55a0bzs7wx")))

