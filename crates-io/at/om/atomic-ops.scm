(define-module (crates-io at om atomic-ops) #:use-module (crates-io))

(define-public crate-atomic-ops-0.1.0 (c (n "atomic-ops") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_flow") (r "^1.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0pd0iy4kqhyfwc05n5511rq2ixlyryminfc2hxwn700hahs14sxc")))

(define-public crate-atomic-ops-0.1.1 (c (n "atomic-ops") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_flow") (r "^1.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1q8as09ryd144my4xkkdkql7r309264pm7kza79kf7fjvdd0rrxw")))

