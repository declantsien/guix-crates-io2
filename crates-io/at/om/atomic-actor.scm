(define-module (crates-io at om atomic-actor) #:use-module (crates-io))

(define-public crate-atomic-actor-0.1.0 (c (n "atomic-actor") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1xa8nwmk8jz1sg3k4scxry67c35vz7y3s58r7jbknxmm3fszvcly")))

