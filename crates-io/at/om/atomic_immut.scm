(define-module (crates-io at om atomic_immut) #:use-module (crates-io))

(define-public crate-atomic_immut-0.1.0 (c (n "atomic_immut") (v "0.1.0") (h "0gm93bbi3yfkfr36jjn2xkzl86d9a44drwjglysl6lfx4crz4djv")))

(define-public crate-atomic_immut-0.1.1 (c (n "atomic_immut") (v "0.1.1") (h "1zlm137ph7wfbav09bpzmlv3aanrmll89gbcs6iig9zq29k6sbj5")))

(define-public crate-atomic_immut-0.1.2 (c (n "atomic_immut") (v "0.1.2") (h "0hndixnfsfm8j3kscip4p3whg82vfa1f2cl6gxamwclyxnjycc9k")))

(define-public crate-atomic_immut-0.1.3 (c (n "atomic_immut") (v "0.1.3") (h "1xhp1gmksvdb21m3w3v75cm1wi8sri4c05y3gxxd0f0g2b8badlc")))

(define-public crate-atomic_immut-0.1.4 (c (n "atomic_immut") (v "0.1.4") (h "1yp40g32mk9d4zmvywqirply81l9raf4j1l40n89i935dakcx7sb")))

