(define-module (crates-io at om atomic-write-file) #:use-module (crates-io))

(define-public crate-atomic-write-file-0.1.0 (c (n "atomic-write-file") (v "0.1.0") (d (list (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "07k3ck8jkb5ps3bnzjp4m12rwam6fka5njc88mxgw5hblmxifcn2") (f (quote (("unstable-unix_file_vectored_at") ("unstable-seek_stream_len") ("unstable-read_buf") ("unstable-can_vector") ("unnamed-tmpfile") ("default" "unnamed-tmpfile"))))))

(define-public crate-atomic-write-file-0.1.1 (c (n "atomic-write-file") (v "0.1.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "014ddlcjk84mc58fy7nh3lq85zs7bhw8rgyzqsxh85ihq6k69qws") (f (quote (("unstable-unix_file_vectored_at") ("unstable-seek_stream_len") ("unstable-read_buf") ("unstable-can_vector") ("unnamed-tmpfile") ("default" "unnamed-tmpfile"))))))

(define-public crate-atomic-write-file-0.1.2 (c (n "atomic-write-file") (v "0.1.2") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dl4x0srdwjxm3zz3fj1c7m44i3b7mjiad550fqklj1n4bfbxkgd") (f (quote (("unstable-unix_file_vectored_at") ("unstable-seek_stream_len") ("unstable-read_buf") ("unstable-can_vector") ("unnamed-tmpfile") ("default" "unnamed-tmpfile"))))))

(define-public crate-atomic-write-file-0.1.3 (c (n "atomic-write-file") (v "0.1.3") (d (list (d (n "nix") (r "^0.28") (f (quote ("fs" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02zv9i5hvcf3ji0nlkgd6a0rrcvq1y2dhnw4zrj8sr5zg6r4s858") (f (quote (("unstable-unix_file_vectored_at") ("unstable-seek_stream_len") ("unstable-read_buf") ("unstable-can_vector") ("unnamed-tmpfile") ("default" "unnamed-tmpfile"))))))

(define-public crate-atomic-write-file-0.1.4 (c (n "atomic-write-file") (v "0.1.4") (d (list (d (n "nix") (r "^0.28") (f (quote ("fs" "user"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1bkdi73d237zqwpl3piizh9720hfy9s9zsy3xg2xyavki12lvxfb") (f (quote (("unstable-unix_file_vectored_at") ("unstable-seek_stream_len") ("unstable-read_buf") ("unstable-can_vector") ("unnamed-tmpfile") ("default" "unnamed-tmpfile"))))))

