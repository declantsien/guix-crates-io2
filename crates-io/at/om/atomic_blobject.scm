(define-module (crates-io at om atomic_blobject) #:use-module (crates-io))

(define-public crate-atomic_blobject-0.1.0 (c (n "atomic_blobject") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "fancy_flocks") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1s0v4qp7j3ck6iy9nxakvacqxg3bgnqdyzlhsnny34iwrkskaacq")))

