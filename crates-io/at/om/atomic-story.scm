(define-module (crates-io at om atomic-story) #:use-module (crates-io))

(define-public crate-atomic-story-0.1.0 (c (n "atomic-story") (v "0.1.0") (d (list (d (n "loom") (r "^0.5.4") (d #t) (k 0)))) (h "0ava29lb497p8629kn8nll69354c080ifn54zw8fnv3zc0yl0gy1")))

(define-public crate-atomic-story-0.1.1 (c (n "atomic-story") (v "0.1.1") (d (list (d (n "loom") (r "^0.5.4") (d #t) (k 0)))) (h "0hwk5vlzj3smi5kn0f6j5h5y4z5icg4c4n5r8qwqd77800vcizaq")))

