(define-module (crates-io at om atomicell) #:use-module (crates-io))

(define-public crate-atomicell-0.1.0 (c (n "atomicell") (v "0.1.0") (h "1hr9afzbva4h4smbv21gb2g053h6h8c0wyg2afpjyad5d63nxlj0")))

(define-public crate-atomicell-0.1.1 (c (n "atomicell") (v "0.1.1") (h "0s2421f38v2d4xiqjxbd0cpqr5jafairmw5j4bdhvnwyhi11aqwq")))

(define-public crate-atomicell-0.1.2 (c (n "atomicell") (v "0.1.2") (h "033asxd494fa0ir8xdsdyxbsa4p37c2n5vvavwbyjzb02j5cssna")))

(define-public crate-atomicell-0.1.3 (c (n "atomicell") (v "0.1.3") (h "0v9ljmjrx21rgmjjpjfyjr0bnxjbpn8lzncdi0y4zdfb8d913xp8")))

(define-public crate-atomicell-0.1.4 (c (n "atomicell") (v "0.1.4") (h "0r23qcwlal6mwqyircwqfisi19wqccwwqb7zyv8nwh3is3wqwv1x")))

(define-public crate-atomicell-0.1.5 (c (n "atomicell") (v "0.1.5") (h "17516p91bjxsnnyv9psjdcirszafgcpi3x1p62awrmgs7hszdn6x")))

(define-public crate-atomicell-0.1.6 (c (n "atomicell") (v "0.1.6") (h "1r02mmcgcbpxv4izbb92yc92c4kkwir2f0da5xs8w1rl0sjd6kzh")))

(define-public crate-atomicell-0.1.7 (c (n "atomicell") (v "0.1.7") (h "0i7aaraiyxb3jciga7mf6iycwx1159rjknxamx0d51ihj2p8giyj")))

(define-public crate-atomicell-0.1.8 (c (n "atomicell") (v "0.1.8") (h "035g42zawynlnjg4fhh1ffiz5z5w3ml93gz540s40wx0krv98l93") (y #t)))

(define-public crate-atomicell-0.1.9 (c (n "atomicell") (v "0.1.9") (h "03w920wd3cwhqp1slqh3k63vhm6f5kxw25jbkf4icky6hkfl4wqm")))

(define-public crate-atomicell-0.2.0 (c (n "atomicell") (v "0.2.0") (h "0fqppqmkgi2rbbk69aizqskyrpfg3c460q4ds6jl5ym37v2dclxz")))

