(define-module (crates-io at om atomic-traits) #:use-module (crates-io))

(define-public crate-atomic-traits-0.1.0 (c (n "atomic-traits") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1rhxwnarkkfijycalpx6l05pkpbqbrqgkmabrwmp8l2kcwjrcb97") (f (quote (("nightly") ("doc") ("default"))))))

(define-public crate-atomic-traits-0.2.0 (c (n "atomic-traits") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1lnqdan3cfd052v7fv8aw4m4kd1jvr0ldpznpyx5xnb5kjwvy2g6")))

(define-public crate-atomic-traits-0.3.0 (c (n "atomic-traits") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "07w7smz9ml77dzfv8pkcsw1sjbqzh5i9vjsw4zdlzywnirwc77mj")))

(define-public crate-atomic-traits-0.4.0 (c (n "atomic-traits") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)))) (h "1p5y0f8vs5154dpqpkqcqri4lyn9wy7mzy6xz6f766xxjc5pazvh") (f (quote (("unstable" "atomic_from_mut" "atomic_bool_fetch_not") ("loom_atomics" "loom") ("integer_atomics") ("extended_compare_and_swap") ("default") ("atomic_mut_ptr") ("atomic_min_max") ("atomic_from_ptr") ("atomic_from_mut") ("atomic_bool_fetch_not") ("atomic_as_ptr") ("atomic_access"))))))

