(define-module (crates-io at om atomic-core) #:use-module (crates-io))

(define-public crate-atomic-core-0.0.1 (c (n "atomic-core") (v "0.0.1") (d (list (d (n "portable-atomic") (r "^1") (d #t) (k 0)))) (h "07z7ammqirgxn2m01fn6h45mp3b9y5x3f2310h6vmc64459fa7qh") (f (quote (("critical-section" "portable-atomic/critical-section"))))))

