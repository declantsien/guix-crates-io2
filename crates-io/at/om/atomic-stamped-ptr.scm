(define-module (crates-io at om atomic-stamped-ptr) #:use-module (crates-io))

(define-public crate-atomic-stamped-ptr-0.1.0 (c (n "atomic-stamped-ptr") (v "0.1.0") (h "1sxdgvfz12gj3qr71lwjxsg30y624fj09fs509zzzz94zvrimhd4")))

(define-public crate-atomic-stamped-ptr-0.1.1 (c (n "atomic-stamped-ptr") (v "0.1.1") (h "0afzyli86imdbhhj8w7g8fimjnsnl1i041czp919v6bm8aiq0avf")))

