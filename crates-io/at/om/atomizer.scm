(define-module (crates-io at om atomizer) #:use-module (crates-io))

(define-public crate-atomizer-0.0.1 (c (n "atomizer") (v "0.0.1") (h "0x0vzds0myvl534anhk0v6xzajlcb7v8ak9881bfymcj3p1h5klf")))

(define-public crate-atomizer-0.0.2 (c (n "atomizer") (v "0.0.2") (h "1qf6rdymlij9k5dw7sq01ij4vcw7ni06dqah6zm0wkgh7wrpl9fp") (f (quote (("nightly"))))))

