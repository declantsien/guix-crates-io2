(define-module (crates-io at om atomic_hooks_macros) #:use-module (crates-io))

(define-public crate-atomic_hooks_macros-0.1.3 (c (n "atomic_hooks_macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "illicit") (r "^0.9.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s997avnfvw609whk17m2l96ban7xwvxk1m06d943n0ia751gn2v")))

