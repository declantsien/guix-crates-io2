(define-module (crates-io at om atomic-write) #:use-module (crates-io))

(define-public crate-atomic-write-0.1.1 (c (n "atomic-write") (v "0.1.1") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "03ch5lfm4wrb1pdmxdds73b39n5baswhcx4j054mysdgnfzkj5lw")))

(define-public crate-atomic-write-0.1.2 (c (n "atomic-write") (v "0.1.2") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "19gbkzk1rvrk3363nbz0ywvj2fgz7bv8pxki0n74rzid2l45wnb1")))

(define-public crate-atomic-write-0.1.3 (c (n "atomic-write") (v "0.1.3") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "0vb1iywk2ak40xhnyb7hs9ybijdxpkfhwkhwbxzajxhc26rh8jfr")))

(define-public crate-atomic-write-0.2.0 (c (n "atomic-write") (v "0.2.0") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)))) (h "1rnkn9plh2c5mw97kc4z5k0ln2y08w0kvim8m850kc7sm18mniap")))

