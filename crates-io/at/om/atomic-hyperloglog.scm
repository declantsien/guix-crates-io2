(define-module (crates-io at om atomic-hyperloglog) #:use-module (crates-io))

(define-public crate-atomic-hyperloglog-0.1.0 (c (n "atomic-hyperloglog") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "seahash") (r "^4.1.0") (d #t) (k 2)) (d (n "tabled") (r "^0.12.2") (d #t) (k 2)))) (h "0d2fb0rp1khqp6sbly3c8dhlcxm9h7ybsrfjg8h27a4zkiz13srf")))

