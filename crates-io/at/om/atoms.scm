(define-module (crates-io at om atoms) #:use-module (crates-io))

(define-public crate-atoms-1.1.4 (c (n "atoms") (v "1.1.4") (h "0ymw3lprv62vfzdcsfa5wm6v3jb719brxc6g4bh1dbafvxkh1wnl")))

(define-public crate-atoms-2.0.0 (c (n "atoms") (v "2.0.0") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "02v3a364fgiycc1glm57pq39lr0bfshr01bfwvgfl985s4sqxbwh")))

(define-public crate-atoms-2.1.0 (c (n "atoms") (v "2.1.0") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "12bjf9njixpvgfj2nl02wxaivvrspd6wib2n79l18iaaf575pbxn")))

(define-public crate-atoms-2.1.1 (c (n "atoms") (v "2.1.1") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0s9j1pb6s7n489nr1cjhpsbl7fg1c9q0q05rr8zhrcby2b9xicbv")))

(define-public crate-atoms-2.1.2 (c (n "atoms") (v "2.1.2") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1na527w8lahvill2rn1azqhiz1mcy5y9mciywfh70cfrr9y5cr4w")))

(define-public crate-atoms-2.1.3 (c (n "atoms") (v "2.1.3") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1s17a1gxcfaxkk7i7lpbflwrin38fqj0j5j8kscvdzwgdkxvdv9m")))

(define-public crate-atoms-2.1.4 (c (n "atoms") (v "2.1.4") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1agpva7ckqnb69sswxkxnzl82ydihxsnjip5kg7hfi2cd6cmvi7q")))

(define-public crate-atoms-2.2.0 (c (n "atoms") (v "2.2.0") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0ndylybw3gqirmsbpxq8b5cv0afwy605csnwl5n3nl2fx2b6xjiw")))

(define-public crate-atoms-2.2.1 (c (n "atoms") (v "2.2.1") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1dh6v5lxkxygq7pvw38va7p49yyz2wcm7yahd1yw4r7ij5mphlaf")))

(define-public crate-atoms-2.2.2 (c (n "atoms") (v "2.2.2") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1jqq8jdms0nr5and7s1cih2pdqxyjsln7bsyciskia6hllzk9571")))

(define-public crate-atoms-2.2.3 (c (n "atoms") (v "2.2.3") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "04w249i3sramwi3iki0v52bdnjzih8bh2k0ca14zzvn8lqzrr7qm")))

