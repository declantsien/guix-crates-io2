(define-module (crates-io at om atomx) #:use-module (crates-io))

(define-public crate-atomx-0.1.0 (c (n "atomx") (v "0.1.0") (h "07sxx5p23ydnam8w9fy605pmwrmq3q3xc7sj5hm4jawhzhry0x17")))

(define-public crate-atomx-0.1.1 (c (n "atomx") (v "0.1.1") (h "1dphl6ng9mf3362sw0qwvwcr7d5lkd7ph35kx44k2c1vd1sqa04b")))

(define-public crate-atomx-0.2.0 (c (n "atomx") (v "0.2.0") (d (list (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1hr5gw5hhb25y0d3p3zmvl99m2w4qz2jxa7yhzpay0sp840nd41b")))

(define-public crate-atomx-0.2.1 (c (n "atomx") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "18j8ywpkh5l26wvzzsqk0s7spq617xn2lfs257989j0p8gf6nzc4")))

(define-public crate-atomx-0.2.2 (c (n "atomx") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1592vh1j4ynxkwdcqcf7wnzi9avf0bj58l65h07zpwb0h3apx1w3")))

(define-public crate-atomx-0.2.3 (c (n "atomx") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0rkzgy9fsfcncnqqimdm3g18ly860yf9x3vfrx5wmp04mscjwdnc")))

(define-public crate-atomx-0.2.4 (c (n "atomx") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1g7ys1z6k1njzaw2729ykj9p97m19szzn9r8gvajpgv98fvibch6")))

(define-public crate-atomx-0.2.5 (c (n "atomx") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "14gr035phijqqlpvnhfqak1p624hml4f20f4qj0n5fawf122f3dq")))

(define-public crate-atomx-0.2.6 (c (n "atomx") (v "0.2.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1rf8d75gn0gpys5h5zdkrj8ndvjvwyrmjj8lsn94qigjrz2066k7")))

