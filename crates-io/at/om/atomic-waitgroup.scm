(define-module (crates-io at om atomic-waitgroup) #:use-module (crates-io))

(define-public crate-atomic-waitgroup-0.1.0 (c (n "atomic-waitgroup") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync" "rt-multi-thread"))) (d #t) (k 2)))) (h "085y1g2hfhapapr4a3ppcagamjlq3l237g7kgilpgdm6qmpndmls")))

(define-public crate-atomic-waitgroup-0.1.1 (c (n "atomic-waitgroup") (v "0.1.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync" "rt-multi-thread"))) (d #t) (k 2)))) (h "00g6v3qjllg1d3rgspbb5vk3rdlchrv2sp6b041qgcvanlvd77lh")))

(define-public crate-atomic-waitgroup-0.1.2 (c (n "atomic-waitgroup") (v "0.1.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync" "rt-multi-thread"))) (d #t) (k 2)))) (h "06mys21mnhr78zxcgjszfyr5qznwcrvp6higi30j87crj7nvqbk4")))

(define-public crate-atomic-waitgroup-0.1.3 (c (n "atomic-waitgroup") (v "0.1.3") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("time" "sync" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f74gsf42gmfwfrp1nhbax1xzbs28r6hwrf1jg5r24yfyh3w061y")))

