(define-module (crates-io at om atomptr) #:use-module (crates-io))

(define-public crate-atomptr-1.0.0 (c (n "atomptr") (v "1.0.0") (h "1jz36mqhbjxf708vasna97md3fqv99ibwgcnc13f13nbicsd4aaa")))

(define-public crate-atomptr-1.0.1 (c (n "atomptr") (v "1.0.1") (h "0z6rj6im47vhjz33ir71whw6khpwblxpqh3b5z0hcg136ilwxpqy")))

(define-public crate-atomptr-1.0.2 (c (n "atomptr") (v "1.0.2") (h "1h38rxv2b0lidl1s6rf9wd2zslhy1695jcyn82kppbzzfnj9acj1")))

(define-public crate-atomptr-1.1.0 (c (n "atomptr") (v "1.1.0") (h "1m87ikg6zm5dc1b1yi5f8mkyw8ing1vw988j7wcfqszpmglsz38i")))

(define-public crate-atomptr-1.2.0 (c (n "atomptr") (v "1.2.0") (h "12xyxp0bjir9bq02ijzczqqvj04bs726jp13bj388f5zk1ya6jz0")))

(define-public crate-atomptr-1.2.1 (c (n "atomptr") (v "1.2.1") (h "13g2q0a85n2razk61qlpgng2l6k66wkmnbf4ss6i3yvzjraxf18z")))

(define-public crate-atomptr-1.3.0 (c (n "atomptr") (v "1.3.0") (h "1w4pms3bprlv85xrp8qbg4gq2r5lym37p6pdq6401q22smmmpzs6")))

(define-public crate-atomptr-1.4.0 (c (n "atomptr") (v "1.4.0") (h "1iyi3djbpy665v83899vk6kqsn3k7358mnrljyd6hzw57w33km93")))

(define-public crate-atomptr-1.4.1 (c (n "atomptr") (v "1.4.1") (h "0i6i0vpjdmgxxgra4zqi0z8v8n6x1sxm98z5rdl890h7gq8jj59a")))

