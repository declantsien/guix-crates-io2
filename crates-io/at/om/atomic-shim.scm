(define-module (crates-io at om atomic-shim) #:use-module (crates-io))

(define-public crate-atomic-shim-0.1.0 (c (n "atomic-shim") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (t "cfg(target_arch = \"mips\")") (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (t "cfg(target_arch = \"powerpc\")") (k 0)) (d (n "crossbeam") (r "^0.7.3") (o #t) (d #t) (k 0)))) (h "16yzir8xq6s6qzknkah6vl84i2smjn0q63lp1nis2yb72p3xl3yj") (f (quote (("mutex" "crossbeam"))))))

(define-public crate-atomic-shim-0.2.0 (c (n "atomic-shim") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (t "cfg(target_arch = \"mips\")") (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (t "cfg(target_arch = \"powerpc\")") (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0xvywhyjagc34wdzfsbwdly8s4j2yifi5s01qc0kbkq3sd8lpkb7") (f (quote (("mutex" "crossbeam-utils"))))))

