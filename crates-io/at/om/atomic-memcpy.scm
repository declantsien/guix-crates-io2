(define-module (crates-io at om atomic-memcpy) #:use-module (crates-io))

(define-public crate-atomic-memcpy-0.0.0 (c (n "atomic-memcpy") (v "0.0.0") (h "1lxkdp7ij8gl4jp92sxz60q4kywwhpf98sk2kg615bzp35ayhr8i") (y #t)))

(define-public crate-atomic-memcpy-0.1.0 (c (n "atomic-memcpy") (v "0.1.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "target-spec") (r "^1") (d #t) (k 2)))) (h "10icfjfp6ham4jxv7z93hzygxk0dg17rf0x00izkwwmazqpz5pzj") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.1.1 (c (n "atomic-memcpy") (v "0.1.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "target-spec") (r "^1") (d #t) (k 2)))) (h "0lg3nfay2ssnqbdqd6l3l2ja6skc7hzwg1sba95c74i2clbpsl41") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.1.2 (c (n "atomic-memcpy") (v "0.1.2") (d (list (d (n "no-panic") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "target-spec") (r "^1") (d #t) (k 2)))) (h "11jjpp0xw1kkpw307j6vhwcll2i47h57gqb8gscdbg834was3gcs") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.1.3 (c (n "atomic-memcpy") (v "0.1.3") (d (list (d (n "no-panic") (r "^0.1.11") (o #t) (d #t) (k 0)))) (h "0hwj2arb5ns8mj12s9ingmcay0ddlvaqr4lb2n9alqd5m1mcq547") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.2.0 (c (n "atomic-memcpy") (v "0.2.0") (d (list (d (n "portable-atomic") (r "^0.3.6") (t "cfg(target_os = \"none\")") (k 0)))) (h "0w8mflqdhf1abacfmqph4jdblrqw5qjk93ik8wl64vi4vlwcbnrq") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.2.1 (c (n "atomic-memcpy") (v "0.2.1") (d (list (d (n "portable-atomic") (r "^1") (t "cfg(target_os = \"none\")") (k 0)))) (h "0mnsivaia1qp4631rn044q7mm1py9abx9kmpkww6k81qy5fcrm0q") (f (quote (("inline-always")))) (r "1.36")))

(define-public crate-atomic-memcpy-0.2.2 (c (n "atomic-memcpy") (v "0.2.2") (d (list (d (n "build-context") (r "^0.1") (d #t) (k 2)) (d (n "portable-atomic") (r "^1") (t "cfg(target_os = \"none\")") (k 0)))) (h "16nlfaj88pnz6qnxm55mzz45fnkb53h1a0nidm26dmqghf64yw3c") (f (quote (("inline-always")))) (r "1.36")))

