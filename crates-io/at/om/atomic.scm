(define-module (crates-io at om atomic) #:use-module (crates-io))

(define-public crate-atomic-0.1.0 (c (n "atomic") (v "0.1.0") (h "0j76fjij5k64mabbmqlmn2k2inyk5cfjhf1ywf5xb8yay8jwkgwq") (f (quote (("nightly"))))))

(define-public crate-atomic-0.2.0 (c (n "atomic") (v "0.2.0") (h "0kmnzkhwpiyiimw1dvvclcz1kw7gkjy84zc4i020ryyl8fms5vs8") (f (quote (("nightly"))))))

(define-public crate-atomic-0.3.0 (c (n "atomic") (v "0.3.0") (h "1iiz7irynnhpx3bsjacwx1s04fqlxcmzrsbwrb73rnniqwgw642l") (f (quote (("nightly"))))))

(define-public crate-atomic-0.3.1 (c (n "atomic") (v "0.3.1") (h "0a0ilh2pmb0m1vbgvvf3krgzwzzl6s9kkmimy9bgnjizzj0v2wz8") (f (quote (("nightly"))))))

(define-public crate-atomic-0.3.2 (c (n "atomic") (v "0.3.2") (h "0rjgw3m183zkcw7npw6dizg4rbvss1ppnvl3mc3yjvdji6nan6dn") (f (quote (("nightly"))))))

(define-public crate-atomic-0.3.3 (c (n "atomic") (v "0.3.3") (h "01nlnzb50a726k0bysa6a1s2zrfiy2jic08n2zm4xpg9d9kgxr41") (f (quote (("nightly"))))))

(define-public crate-atomic-0.3.4 (c (n "atomic") (v "0.3.4") (h "0zn1khcdyxyqds0w9pric9mh3qhwwfllr0hjbqhpgwikhy9bnib9") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.0 (c (n "atomic") (v "0.4.0") (h "0diijlc3b8kxkv4dy3h1d03y3njc7kdphzlqwqyla9cgl7xr6wbp") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.1 (c (n "atomic") (v "0.4.1") (h "1q0a6f73fkgjlxsd4bap9hcbp6a8cwmcjcvz840dz2g9ax1fdc0i") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.2 (c (n "atomic") (v "0.4.2") (h "0209aw8sbmlhzfj9pgjn83rgfdbbygilh4akzw9akr1d24ys32fi") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.3 (c (n "atomic") (v "0.4.3") (h "01skxjlydskk7wa09ay2y79l276ywn5kfnfzb9srb8c1d0n02llz") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.4 (c (n "atomic") (v "0.4.4") (h "0ih1hzmnl272jlghp4167kr7ix6ylrbd22m2jgw6mlm63h68l875") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.5 (c (n "atomic") (v "0.4.5") (h "0a7w8gaknparcdvg2daqd509ck6qf82ifbb5gd3xm304vgsc2462") (f (quote (("nightly"))))))

(define-public crate-atomic-0.4.6 (c (n "atomic") (v "0.4.6") (h "0vm0d59h4xy1r66r4gamh5mkadk6qg4d2m07abqkfj6a3njnrx34") (f (quote (("std") ("nightly"))))))

(define-public crate-atomic-0.5.0 (c (n "atomic") (v "0.5.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "10dj429d1942cnjkkrigbl7ckh1vhf10z4xmxlxld318x0lhahf3") (f (quote (("std") ("fallback") ("default" "fallback"))))))

(define-public crate-atomic-0.5.1 (c (n "atomic") (v "0.5.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)))) (h "0k135q1qfmxxyzrlhr47r0j38r5fnd4163rgl552qxyagrk853dq") (f (quote (("std") ("fallback") ("default" "fallback"))))))

(define-public crate-atomic-0.5.3 (c (n "atomic") (v "0.5.3") (h "1fhc6ayg4d5vw1cibqwff15d45fc5448zg9i3drk42k5phsdp6y5") (f (quote (("std") ("nightly") ("fallback") ("default" "fallback"))))))

(define-public crate-atomic-0.6.0 (c (n "atomic") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 2)))) (h "15193mfhmrq3p6vi1a10hw3n6kvzf5h32zikhby3mdj0ww1q10cd") (f (quote (("std") ("nightly") ("fallback") ("default" "fallback"))))))

