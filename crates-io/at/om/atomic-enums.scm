(define-module (crates-io at om atomic-enums) #:use-module (crates-io))

(define-public crate-atomic-enums-0.1.1 (c (n "atomic-enums") (v "0.1.1") (d (list (d (n "atomic-enum-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0v8lfp89fvariz0502mkayhgczw5c7qv0r0ylk8jkwyabyd9ip3s") (y #t)))

(define-public crate-atomic-enums-0.2.0 (c (n "atomic-enums") (v "0.2.0") (d (list (d (n "paste") (r "^0.1.0") (d #t) (k 2)))) (h "12wzyjxp0p9mfq8a18fz21pbh7mng1sa6wwkg10m3lqxbm0jrjf1") (f (quote (("u64") ("default" "u64"))))))

