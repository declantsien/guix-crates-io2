(define-module (crates-io at om atomic_once_cell) #:use-module (crates-io))

(define-public crate-atomic_once_cell-0.1.0 (c (n "atomic_once_cell") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)))) (h "0qy6a6977apyk7ybr2mfb7szgc50xcx8jdairdynbs2vi8bdzywg")))

(define-public crate-atomic_once_cell-0.1.1 (c (n "atomic_once_cell") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)))) (h "1xd265r5mfij15qixm402if4cnkvn68y5vrpfghlxgan7qppinhw")))

(define-public crate-atomic_once_cell-0.1.2 (c (n "atomic_once_cell") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)))) (h "1ym3xxgs84kd65fv8q2hh097g8fxmbcvniih73yjrc44bsbsbvpm")))

(define-public crate-atomic_once_cell-0.1.3 (c (n "atomic_once_cell") (v "0.1.3") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)))) (h "0pz65x486z6zfdvk42zgwg8b1rgl302jigzjy54j508nh85l9m4p")))

(define-public crate-atomic_once_cell-0.1.5 (c (n "atomic_once_cell") (v "0.1.5") (d (list (d (n "atomic-polyfill") (r "^1.0.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.15") (k 0)) (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)))) (h "1psksn6gqppc3slqz6wwrrb60mf9wr36cbj927bz743379fj2qin")))

(define-public crate-atomic_once_cell-0.1.6 (c (n "atomic_once_cell") (v "0.1.6") (d (list (d (n "atomic-polyfill") (r "^1.0.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.15") (k 0)) (d (n "loom") (r "^0.7.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "00h2scqbf8n064zym123xr4bwh1midss8mspgxa7czn3772799p6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

