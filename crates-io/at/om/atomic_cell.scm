(define-module (crates-io at om atomic_cell) #:use-module (crates-io))

(define-public crate-atomic_cell-0.1.0 (c (n "atomic_cell") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)) (d (n "telemetry") (r "^0.1.1") (d #t) (k 2)))) (h "1gzcnbyl0c5zb6h6v6fbcv5ba7wfgfv8qzp7ags0vczdc5pfyy9m")))

(define-public crate-atomic_cell-0.2.0 (c (n "atomic_cell") (v "0.2.0") (d (list (d (n "const_panic") (r "^0.2.4") (d #t) (k 0)))) (h "1lpw2jwbxzyvzwgvndp0q5f7m4ydq8fvv5r19whg25gc7dbamw95") (f (quote (("default") ("const"))))))

