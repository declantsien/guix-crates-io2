(define-module (crates-io at om atomic-borrow) #:use-module (crates-io))

(define-public crate-atomic-borrow-0.1.0 (c (n "atomic-borrow") (v "0.1.0") (h "0kgnwdmy70409xl5kircbhy1sm7hlzfk5c27fcnd45chl2hqryf5")))

(define-public crate-atomic-borrow-0.1.1 (c (n "atomic-borrow") (v "0.1.1") (h "0wa6h2j2w9xhksj856f0l6i7h4d6lh2xz5c63iviirkc26663gk1")))

(define-public crate-atomic-borrow-0.1.2 (c (n "atomic-borrow") (v "0.1.2") (h "1d66lnkgbzy12sya722nzcglh9fak8d8c73fkvsf0xsd01ibpfyk")))

(define-public crate-atomic-borrow-0.1.3 (c (n "atomic-borrow") (v "0.1.3") (h "0vr8n72fwznsf51r2hhg1drks52ax873hf4702nd8d9s4qiiqc5r")))

