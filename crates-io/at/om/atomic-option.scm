(define-module (crates-io at om atomic-option) #:use-module (crates-io))

(define-public crate-atomic-option-0.0.1 (c (n "atomic-option") (v "0.0.1") (h "1yhmsv85yi3y0xbrcah36v5kc7qwm3msn05a08w41y267amffa6v")))

(define-public crate-atomic-option-0.0.2 (c (n "atomic-option") (v "0.0.2") (h "02v8fas1kv1q3ynqg0b71x0qf8qd7zbh6gclvhaj8k16x1yah5jz")))

(define-public crate-atomic-option-0.0.3 (c (n "atomic-option") (v "0.0.3") (h "1qycmm0k0rmz2fib8nh2dqf4ldq45fk2sph2pq5i8fjwfs6a0mkf")))

(define-public crate-atomic-option-0.0.4 (c (n "atomic-option") (v "0.0.4") (h "0cjagi4ayv7rjbb0hlrb6nd6m9vizflszlm13w4v4j1rzqb9r81b")))

(define-public crate-atomic-option-0.1.0 (c (n "atomic-option") (v "0.1.0") (h "1fywrcjyq6gz3iwk0lbd2bqlqn43m60iqfbh3rk1fp0ll2vqj8y8")))

(define-public crate-atomic-option-0.1.1 (c (n "atomic-option") (v "0.1.1") (h "0i4arjf6ljrbpq7qwxlh399hyh824h0s556m1f2xackc4662r95p")))

(define-public crate-atomic-option-0.1.2 (c (n "atomic-option") (v "0.1.2") (h "14zmzysgamw1pckp675k769f541kgmgzq95382n2bdb7nsn7idhd")))

