(define-module (crates-io at om atomic64) #:use-module (crates-io))

(define-public crate-atomic64-0.1.0 (c (n "atomic64") (v "0.1.0") (h "0zpxskq154ygxkkprp1xdrb7j2l4f5132q39gsq2lzxrqg1j0c07")))

(define-public crate-atomic64-0.1.1 (c (n "atomic64") (v "0.1.1") (h "00b6y1aqsfxb28aqd6jadmb7gyyh8v321fw14xin601rlish27rb")))

