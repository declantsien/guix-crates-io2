(define-module (crates-io at om atomic-pool) #:use-module (crates-io))

(define-public crate-atomic-pool-0.1.0 (c (n "atomic-pool") (v "0.1.0") (d (list (d (n "as-slice") (r "^0.1.4") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "0q8irrs4lh65kjy6b1c1ck2iyqqpb9bfbkmd6zr4qklyyqgqv0qp") (y #t)))

(define-public crate-atomic-pool-0.2.0 (c (n "atomic-pool") (v "0.2.0") (d (list (d (n "as-slice") (r "^0.1.4") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "0lj9pb0r8k9w7qwl662jjcllhk3m7kcwya59qmjygllafd07ik0l") (y #t)))

(define-public crate-atomic-pool-0.2.1 (c (n "atomic-pool") (v "0.2.1") (d (list (d (n "as-slice") (r "^0.1.4") (d #t) (k 0)) (d (n "atomic-polyfill") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "0qj0iax63v7zwjfmkq32js5rxzqkzk5zg55xn745npcacp0jn9ss") (y #t)))

(define-public crate-atomic-pool-0.2.2 (c (n "atomic-pool") (v "0.2.2") (d (list (d (n "as-slice-01") (r "^0.1.5") (d #t) (k 0) (p "as-slice")) (d (n "as-slice-02") (r "^0.2.1") (d #t) (k 0) (p "as-slice")) (d (n "atomic-polyfill") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "07wbbgl1x1wdadsnmn7mr2xxwmv8xi6x5z1jjigwmzi1szb7wh3v") (y #t)))

(define-public crate-atomic-pool-0.2.3 (c (n "atomic-pool") (v "0.2.3") (d (list (d (n "as-slice-01") (r "^0.1.5") (d #t) (k 0) (p "as-slice")) (d (n "as-slice-02") (r "^0.2.1") (d #t) (k 0) (p "as-slice")) (d (n "atomic-polyfill") (r "^0.1.1") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "1ga5f0077sp3clwk8v8ysm5dil8zcym9wxy3gfmcq98ghsynai7w")))

(define-public crate-atomic-pool-1.0.0 (c (n "atomic-pool") (v "1.0.0") (d (list (d (n "as-slice-01") (r "^0.1.5") (d #t) (k 0) (p "as-slice")) (d (n "as-slice-02") (r "^0.2.1") (d #t) (k 0) (p "as-slice")) (d (n "atomic-polyfill") (r "^1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "12bciknwm5xgb3x98hzkcd526jimspzx727mf99rs8xvc1kaw1rq")))

(define-public crate-atomic-pool-1.0.1 (c (n "atomic-pool") (v "1.0.1") (d (list (d (n "as-slice-01") (r "^0.1.5") (d #t) (k 0) (p "as-slice")) (d (n "as-slice-02") (r "^0.2.1") (d #t) (k 0) (p "as-slice")) (d (n "atomic-polyfill") (r "^1.0") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (k 0)))) (h "1xpm0iwaxh6rzbkbdll8i11cjy5jqxyk1gsqni6qihjyw0igriaq")))

