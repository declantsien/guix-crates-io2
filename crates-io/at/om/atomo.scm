(define-module (crates-io at om atomo) #:use-module (crates-io))

(define-public crate-atomo-0.0.0 (c (n "atomo") (v "0.0.0") (h "1938s5hk7z71vhx0zczjmhr4wl3gxjl2ni88hmhjb6ab2prkvljc")))

(define-public crate-atomo-0.0.1 (c (n "atomo") (v "0.0.1") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1vkb4r375z5qlhqha0jlch1mirdyfaw4zc4m7arhznypldkcbq9k")))

(define-public crate-atomo-0.0.2 (c (n "atomo") (v "0.0.2") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "08kxmf7iigi0j2nrkglw5k0qf9j53iiaq3zzp8sa3kwlgqgwb7kl")))

(define-public crate-atomo-0.0.3 (c (n "atomo") (v "0.0.3") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1mgz9x2nw3cmkvwisv4rwpwazb2800bmf46nnd2p9rlybmay2k4x")))

(define-public crate-atomo-0.0.4 (c (n "atomo") (v "0.0.4") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "seize") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0j8b323cnwbcrap872cmpkhn97k5j3w33vbf3b6zq92in9khq1wb")))

(define-public crate-atomo-0.0.5 (c (n "atomo") (v "0.0.5") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dashmap") (r "^5.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "seize") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1iw0ql9zk22va4z2wfrr1kdrvlcl1wavylg2rnk5b7fnpj410nij")))

