(define-module (crates-io at om atomicring) #:use-module (crates-io))

(define-public crate-atomicring-0.1.0 (c (n "atomicring") (v "0.1.0") (d (list (d (n "atomic64") (r "^0.1.1") (d #t) (k 0)))) (h "1gwp56h2j0mvbb1r1k0p140ifvay6hayzqnv4g17ck3a1h8g5035")))

(define-public crate-atomicring-0.1.1 (c (n "atomicring") (v "0.1.1") (d (list (d (n "atomic64") (r "^0.1.1") (d #t) (k 0)))) (h "0id63fg14lxzpiz1qn3afl6g7ybwicxjnpr2sjw152xd2azq8gkv")))

(define-public crate-atomicring-0.1.2 (c (n "atomicring") (v "0.1.2") (d (list (d (n "atomic64") (r "^0.1.1") (d #t) (k 0)))) (h "0iggfb0jqkh2a43xjnbz9i0nz9kalaazsisn1qcsv57kh88n5d87")))

(define-public crate-atomicring-0.2.0 (c (n "atomicring") (v "0.2.0") (h "1yqs8f58k92w23h276wj4wb3z5zlggnaw0gs7vaa03md167la3fb")))

(define-public crate-atomicring-0.2.1 (c (n "atomicring") (v "0.2.1") (h "0bfhz40fzgki1saliys605c791ki62qvj6sp1342l77qw4852rmn") (f (quote (("default" "compare_and_exchange_weak") ("compare_and_exchange_weak"))))))

(define-public crate-atomicring-0.2.2 (c (n "atomicring") (v "0.2.2") (h "1wajr1bapaw6paywc9427pwr6ranbpcarf92x1kmwdnzfyvy8dfd") (f (quote (("default" "compare_and_exchange_weak") ("compare_and_exchange_weak"))))))

(define-public crate-atomicring-0.3.0 (c (n "atomicring") (v "0.3.0") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "0qwxxrb1y1xmr46sn6pcdi8ic8lk78s8f9ql1kixz79mg5iixpl6") (f (quote (("force_32") ("default" "compare_and_exchange_weak") ("compare_and_exchange_weak"))))))

(define-public crate-atomicring-0.4.0-alpha (c (n "atomicring") (v "0.4.0-alpha") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "11z3cjlnynp1xh2cjab5fi3dqjglni2mck6911n2nxsh4rvw4vhf") (f (quote (("force_32"))))))

(define-public crate-atomicring-0.4.1 (c (n "atomicring") (v "0.4.1") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "1cmfhkvbpxychqw8z216ximz5v9bp4f1mgybj2wqyhasl30im94s") (f (quote (("force_32"))))))

(define-public crate-atomicring-0.4.2 (c (n "atomicring") (v "0.4.2") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "1aqnnjwancrlmr7m5awvf29rg5c10c37qsi0qkln6lb7lxfkzzjf") (f (quote (("force_32"))))))

(define-public crate-atomicring-0.4.3 (c (n "atomicring") (v "0.4.3") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "0h84jdf1nw2vnlpn0n5xd442nvcrzxs8rg88wlfampbsi00sa2s1")))

(define-public crate-atomicring-0.4.4 (c (n "atomicring") (v "0.4.4") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)))) (h "1w7bc3cz0nfjn4ks2zigss4b6x4zlxbxhpgk7hj6g7bnylrfsdr3") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.0 (c (n "atomicring") (v "0.5.0") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "0c8ixl4b23q7kh5pbsagwz7xviyv1ywfv580mpcq7m1sly3fd745") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.1 (c (n "atomicring") (v "0.5.1") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "06bq5ymp9jriw3r8a6z8h1qnb3fw831nrksb78cl61qczhvhsa7w") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.3 (c (n "atomicring") (v "0.5.3") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "1ipshv3047mdk73xy8mdbn47azxv5jsbrxnspp0qc4ncq3iyma7k") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.4 (c (n "atomicring") (v "0.5.4") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "15y3d8yy8zr2qdi8zn903l37r7gz1k9jc8qnxjcrv544p31g902w") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.5 (c (n "atomicring") (v "0.5.5") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "1ar11xbm9bgigq5j2a5yi214y0lpc7wxhd5yn1mxyjg56clcfz33") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-0.5.6 (c (n "atomicring") (v "0.5.6") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "07wvz90ddgzppbfhplg2q8wqslh01q0d46cpqzz7qxv9jdlgscf6") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.0 (c (n "atomicring") (v "1.0.0") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "1b5yg8j8vqhpp1xf5h24xgwg4cwwh4gyaclp14gnjc1ydwb51s2b") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.1 (c (n "atomicring") (v "1.0.1") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "1l37dfirkcqk5is9zhi2n72skbf62fhpm9nb7j3vpkxlb28p54in") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.2 (c (n "atomicring") (v "1.0.2") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.5.5") (d #t) (k 0)))) (h "0m34diy7hlpjr07vgmlyx3kcj4zbdih6qsqlichcchphs3fnssz7") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.3 (c (n "atomicring") (v "1.0.3") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1c6mqlr4bzbp04byzhk3ga8fhxclksp4a1asp0qqf1g7rmbp0qpx") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.4 (c (n "atomicring") (v "1.0.4") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0ddwciq40znqgjdb1qcppimh86js06m7dcrjmlllc18hlda2qfpd") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.0.5 (c (n "atomicring") (v "1.0.5") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0fqjb26m9cnqvk1b0czdglvygfw2wy372czb00h1zxqs7xlp1562") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.1.0 (c (n "atomicring") (v "1.1.0") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0qnhs00iqy6j8fp5pf7834fisfdwylkkb90q55z5sq1984cvdb95") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.1.1 (c (n "atomicring") (v "1.1.1") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "04yydphmwv4av1ly4i3jly68r9hpzmpirzj61bbcdvjn9aclrslq") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.1.2 (c (n "atomicring") (v "1.1.2") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1lbxcdrgakcfahbb1amh051g3dg2g1si2hz5wq12pa23gk9samg5") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.0 (c (n "atomicring") (v "1.2.0") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "07kcs2c58gm6d151b50pmf0xhij6nlmh50skn16k5vhf4hyky73z") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.1 (c (n "atomicring") (v "1.2.1") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "09l42ql935l3vhbrx3a33wi9fw1hdvq25ln12za82imyk6d6vbwx") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.2 (c (n "atomicring") (v "1.2.2") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1jxxylnbadpjin4xkd22cbsnhqbsaslz5g71yyrlwilm96ks1j15") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.3 (c (n "atomicring") (v "1.2.3") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1qd6wybgbxgsxrj36p24m3k9wlsbmwbjlap4fav830b9cdjflx7n") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.4 (c (n "atomicring") (v "1.2.4") (d (list (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0sk3xljm6pnp6lngwsaagjq39m11fk6jzfz4q5b3v54wxw3znkqw") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.5 (c (n "atomicring") (v "1.2.5") (d (list (d (n "crossbeam-queue") (r "^0.2.1") (d #t) (k 2)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "09z9hv84mfd3jxmmhwr4xncyp5l4f0qzpggnpb52kjav483w1g6p") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.6 (c (n "atomicring") (v "1.2.6") (d (list (d (n "crossbeam-queue") (r "^0.3.0") (d #t) (k 2)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "14jmp592c5y5psfj2z0wfrdb5002x3dz9m574b3j95rv9h07h6z3") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.7 (c (n "atomicring") (v "1.2.7") (d (list (d (n "crossbeam-queue") (r "^0.3.0") (d #t) (k 2)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0aqhzazlzj45hff82fjm0q2p3v8z77wlb89j95vbx3c5lngm08kv") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.8 (c (n "atomicring") (v "1.2.8") (d (list (d (n "crossbeam-queue") (r "^0.3.0") (d #t) (k 2)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1mmj3vr06x3d8yax0d52vp4i38y23qgvh0d0xzahbi3j7w9gcy3k") (f (quote (("index_access") ("default" "index_access"))))))

(define-public crate-atomicring-1.2.9 (c (n "atomicring") (v "1.2.9") (d (list (d (n "crossbeam-queue") (r "^0.3.0") (d #t) (k 2)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0kx2bqmhzjhzhxiv6194d174wxwrf5q9nq3r8j36cm17wsfx8bdw") (f (quote (("index_access") ("default" "index_access"))))))

