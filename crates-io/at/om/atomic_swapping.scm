(define-module (crates-io at om atomic_swapping) #:use-module (crates-io))

(define-public crate-atomic_swapping-0.1.0 (c (n "atomic_swapping") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "06c6xg07lpcv6dchlhdsx9znqi2q779d0hy9z3zgg4h3krrwi7xl") (f (quote (("never_unchecked") ("default"))))))

