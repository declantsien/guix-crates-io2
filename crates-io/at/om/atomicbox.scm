(define-module (crates-io at om atomicbox) #:use-module (crates-io))

(define-public crate-atomicbox-0.1.0 (c (n "atomicbox") (v "0.1.0") (h "0vrah60n3rs279mbpi0gsv8fp1kjazq9dinq6jdlya0rcagmn34n")))

(define-public crate-atomicbox-0.2.0 (c (n "atomicbox") (v "0.2.0") (h "1i61dblsrd2wpnhvcjc7r6lp3bna6dfhd5ihv812m0agiq6nwpxp")))

(define-public crate-atomicbox-0.3.0 (c (n "atomicbox") (v "0.3.0") (h "1l1mcz92yxf6iai38v6byknwjx9ajjgpvb86xzknnq9m56261dpd")))

(define-public crate-atomicbox-0.4.0 (c (n "atomicbox") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.7.1") (d #t) (k 2)))) (h "0xmdshqkdb08nsyyck15zzs9k1al1142m71qr1haz6lyphh3i6la")))

