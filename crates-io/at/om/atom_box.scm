(define-module (crates-io at om atom_box) #:use-module (crates-io))

(define-public crate-atom_box-0.1.0 (c (n "atom_box") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1vavn55pcqdg04ic42vxzknjqvkszxpcxdpzyqcz931gmzay5ak3")))

(define-public crate-atom_box-0.1.1 (c (n "atom_box") (v "0.1.1") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0cj0j3as48g9bph2w5vvq2xcywi9sxnkv5hywmls1j2hc5wbw14z")))

(define-public crate-atom_box-0.1.2 (c (n "atom_box") (v "0.1.2") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "1s6fiz3bama5jlkwccr6iz5s06vywpb3fi78kqxiiqz6ragm5nnv")))

(define-public crate-atom_box-0.2.0 (c (n "atom_box") (v "0.2.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "14c4wrz4fbbqd0v1d6bhqkx86q2z3735m64bbzs4ax5yyy9wnms0") (f (quote (("std") ("default" "std"))))))

