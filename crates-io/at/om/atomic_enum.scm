(define-module (crates-io at om atomic_enum) #:use-module (crates-io))

(define-public crate-atomic_enum-0.1.0 (c (n "atomic_enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1fzf8i4a5rh58fiashrhjj5rb9ji65xfmar8ik4r9jfyf6751j2w")))

(define-public crate-atomic_enum-0.1.1 (c (n "atomic_enum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "071sa3mda7x3kgr6yz35x2734m2wmmznnr9nj3p78z1m4255hmva")))

(define-public crate-atomic_enum-0.2.0 (c (n "atomic_enum") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "01qjbwycs6kfdwp1mwvdz9rwvrbrjl6lscf402qvqqmqzpbah9v2")))

(define-public crate-atomic_enum-0.3.0 (c (n "atomic_enum") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "0qd6wvdc59vrpfbw82z07w336xfpfanlmjchayc8jyza32ksrqcr") (f (quote (("default" "cas") ("cas"))))))

