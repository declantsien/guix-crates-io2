(define-module (crates-io at om atomic_bitfield) #:use-module (crates-io))

(define-public crate-atomic_bitfield-0.1.0 (c (n "atomic_bitfield") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0cican47qny6kkcd3llfnslrri3jikq8lnn1n3a3ycygqyp0d16x") (f (quote (("nightly") ("default"))))))

