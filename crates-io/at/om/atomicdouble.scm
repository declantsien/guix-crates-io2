(define-module (crates-io at om atomicdouble) #:use-module (crates-io))

(define-public crate-atomicdouble-0.1.0 (c (n "atomicdouble") (v "0.1.0") (h "0lagxla2w6ay7mglchcj5d64n1771nzfxpjqnwa1bnws5b5n1m73") (f (quote (("fallback") ("default" "fallback")))) (y #t)))

(define-public crate-atomicdouble-0.1.1 (c (n "atomicdouble") (v "0.1.1") (h "0qi55khg3z7hm7r85p6rnv64kkxqvfsy86p5ah1pvp6bn0rwwadk") (f (quote (("fallback") ("default" "fallback")))) (y #t)))

(define-public crate-atomicdouble-0.1.2 (c (n "atomicdouble") (v "0.1.2") (h "19lvc1s0sm6fl1wk96hwpagzkg518andfyh0hy8vlfdj0biadax5") (f (quote (("fallback") ("default" "fallback")))) (y #t)))

(define-public crate-atomicdouble-0.1.3 (c (n "atomicdouble") (v "0.1.3") (h "0f082i17npayz1384bp6cy3rmix23za2fkp3qmm23z1nd68h4zcw") (f (quote (("fallback") ("default" "fallback")))) (y #t)))

(define-public crate-atomicdouble-0.1.4 (c (n "atomicdouble") (v "0.1.4") (h "1afcnl1cgnp306p0faz3scj3sa0parpd6r6lgbv3igqhi99j7bnw") (f (quote (("fallback") ("default" "fallback"))))))

