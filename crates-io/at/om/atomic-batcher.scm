(define-module (crates-io at om atomic-batcher) #:use-module (crates-io))

(define-public crate-atomic-batcher-0.8.0 (c (n "atomic-batcher") (v "0.8.0") (d (list (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "128x2kkxg9w8wxqg9fl1vga8jy5kwlvzb49lfns9892fzj0ai98v")))

(define-public crate-atomic-batcher-0.8.2 (c (n "atomic-batcher") (v "0.8.2") (d (list (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0nm1f3nv6v9n3sdg2y1gdgw18k8b5yqc030vjdsm5rb11h9dx0zh")))

