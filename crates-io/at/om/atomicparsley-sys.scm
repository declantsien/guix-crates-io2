(define-module (crates-io at om atomicparsley-sys) #:use-module (crates-io))

(define-public crate-atomicparsley-sys-0.1.0 (c (n "atomicparsley-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1mzpa8vjnxn54gp6l8ahcgglnq4gmwl4w7v83y9yx6d44xs71a26") (l "atomicparsley")))

