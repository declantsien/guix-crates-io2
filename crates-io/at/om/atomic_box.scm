(define-module (crates-io at om atomic_box) #:use-module (crates-io))

(define-public crate-atomic_box-0.1.0 (c (n "atomic_box") (v "0.1.0") (h "1329saygpb38lndff4vh0d299039lhgb05qqzyxni4cxhhgzjlr0")))

(define-public crate-atomic_box-0.1.1 (c (n "atomic_box") (v "0.1.1") (h "1vhm2i11mmcz6vvhllb1qnc5qs70vqslcyslllwrqjz2c91isk28")))

