(define-module (crates-io at om atomize) #:use-module (crates-io))

(define-public crate-atomize-0.1.0 (c (n "atomize") (v "0.1.0") (d (list (d (n "atomize-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "0m99g6d19mqnjgc6cxvang2z3hhr5bb883hm9v5idk4ncdrxgack")))

(define-public crate-atomize-0.1.1 (c (n "atomize") (v "0.1.1") (d (list (d (n "atomize-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "00bk0n2cdshsjq7nlkprl8fpkm7sfc7899kqca41yvydgz1g86kx")))

(define-public crate-atomize-0.1.2 (c (n "atomize") (v "0.1.2") (d (list (d (n "atomize-macro") (r "^0.1.2") (d #t) (k 0)))) (h "11dslhrjma55dransf93qvv5035zzhvjaj2zy25qc99fxf9z3qm5")))

(define-public crate-atomize-0.1.3 (c (n "atomize") (v "0.1.3") (d (list (d (n "atomize-macro") (r "^0.1.3") (d #t) (k 0)))) (h "1yv7p297dfwgq3y6s2553z8afiy0ivsid4zvz1a5zfzzqn7hs8bd")))

