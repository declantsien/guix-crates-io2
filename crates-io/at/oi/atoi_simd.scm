(define-module (crates-io at oi atoi_simd) #:use-module (crates-io))

(define-public crate-atoi_simd-0.0.1 (c (n "atoi_simd") (v "0.0.1") (h "0ahpcs7smm36vfx1kavxlvwvgi3dqmvnj6dzlgp0rhk1szvixzg7") (y #t)))

(define-public crate-atoi_simd-0.0.2 (c (n "atoi_simd") (v "0.0.2") (h "1vdx8cjp3var98sh0cp8a42mwzp81sgsxdj2rs2dfsgvcwib5fhq")))

(define-public crate-atoi_simd-0.0.3 (c (n "atoi_simd") (v "0.0.3") (h "0ki835qlcgp2f8vw6h14cwl9267xkx89fgz9b6kmlxv7bnb9iwhw")))

(define-public crate-atoi_simd-0.1.0 (c (n "atoi_simd") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xwdb7papdcx0hn6zsg3dshhwz5sn38ndi5v6w6yjs987mhrnycz")))

(define-public crate-atoi_simd-0.1.1 (c (n "atoi_simd") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "158xrgbmlyid6wwksnmcycpl147vq60989kbzlbk4gq4frfc29z4")))

(define-public crate-atoi_simd-0.2.0 (c (n "atoi_simd") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1wadrhyrzz9k7psgj6rfl0nqbd8kpfghnmijki7bs1w48j29argy")))

(define-public crate-atoi_simd-0.2.1 (c (n "atoi_simd") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "041kq9q7bviax2gl3lh42vr4pqzqghcar431nnc2riskg2skanc4")))

(define-public crate-atoi_simd-0.3.0 (c (n "atoi_simd") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xq4y7y83jam5hnqvvknv55kc42rqcgic9533hkal0xjwqdyg1rg")))

(define-public crate-atoi_simd-0.4.0 (c (n "atoi_simd") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1bkn99cm10ak6wblpkbc2p512vpbmqq3701z6q1qbjpx8gafrjj5")))

(define-public crate-atoi_simd-0.5.0 (c (n "atoi_simd") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0213i78m4vby3br23jb05mcygm159dcfpb6jv34spqir8an90v8c") (y #t)))

(define-public crate-atoi_simd-0.5.1 (c (n "atoi_simd") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1m02xkqyjypbqpiln2azy6l5v3p6fb9q9y29w68pcb5szdp2rk91")))

(define-public crate-atoi_simd-0.6.0 (c (n "atoi_simd") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0wh80qm2w1wyf002hscvbb41g8xydm1w9pibqv7nhv8z88bpq3b0")))

(define-public crate-atoi_simd-0.6.1 (c (n "atoi_simd") (v "0.6.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "00is8yb97gpm416grfgj155xhsizn0dpi68jvyap1w9xf8kbz24b")))

(define-public crate-atoi_simd-0.7.0 (c (n "atoi_simd") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1dpikvrgfsz109fiyapa3zn52q6dasqwa9ddsaqi42431cpybpab")))

(define-public crate-atoi_simd-0.8.0 (c (n "atoi_simd") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1m7baas0g3ll9b4n3jxwj6ygpiph7dhxc29nyxn927m9y7szhkq4")))

(define-public crate-atoi_simd-0.9.0 (c (n "atoi_simd") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0snax8ivhzyb5pf3iy343jzw52b18q4nk5pa4h9kd9v8v111a6j5")))

(define-public crate-atoi_simd-0.9.1 (c (n "atoi_simd") (v "0.9.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0c538daaii95i9rdjcw1mml7vqc9zbjxx0s5wrll8mh2sd2bxc24")))

(define-public crate-atoi_simd-0.9.2 (c (n "atoi_simd") (v "0.9.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0kln5dapfvd03vw343gp1mz7yr92shh9v8rx0qdxsrrfnnfsd0c8")))

(define-public crate-atoi_simd-0.10.0 (c (n "atoi_simd") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0rz5jja9ibqy8vl0a7qsln48jybij4z4sdg3n9232mb9w4qv238k")))

(define-public crate-atoi_simd-0.10.1 (c (n "atoi_simd") (v "0.10.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "03rrrk2wily0nyrnl535n314pxhm2p68hzk4l0y74j23xb4zgizw")))

(define-public crate-atoi_simd-0.10.2 (c (n "atoi_simd") (v "0.10.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1nb39fpyy1lg13j33i2q6zjj6lw281n2kd6778v5gzif6y1vn6sv") (f (quote (("std") ("default" "std"))))))

(define-public crate-atoi_simd-0.10.3 (c (n "atoi_simd") (v "0.10.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1l2l4hcqzp1izx0fry5md9hgq13pbnxcrjzj4m50mwbk92aa7qpn") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.11.0 (c (n "atoi_simd") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wp90149ic0yfib1k6lgjnzij1syxp4fvrz8h76qpdgl8dn0gjg0") (f (quote (("std" "arrayvec/std") ("default" "std")))) (y #t)))

(define-public crate-atoi_simd-0.12.0 (c (n "atoi_simd") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0bxkldf242z67bdf927lngn0rih7qks9v9izwnkkggyzpj072x78") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.12.1 (c (n "atoi_simd") (v "0.12.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1323ii3xgp4ivqk487nmsfb2f6qclw1nn2hl0ah9n1ixnwb73gz8") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.12.2 (c (n "atoi_simd") (v "0.12.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1cazks428wk49hp3ay39yyazq5n0q1wvb6fbpqmfrii17h7211k5") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.0 (c (n "atoi_simd") (v "0.14.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "07y0j5l18ixjnhkas70ka9j1fkn54k73184cfsn1f1kw645dsm7s") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.1 (c (n "atoi_simd") (v "0.14.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "12mvnxbzz7rh1ksmdhjyw6njxmxwy8zspxp7m5nf2qld96s3b9xf") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.2 (c (n "atoi_simd") (v "0.14.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "11ggln4s504s5wd7znk2xbwzib1kgx1a4wp7b03m4gnnk71040b3") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.3 (c (n "atoi_simd") (v "0.14.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 2)))) (h "1h1pyv1viwhsqfcx2347k8lll52r7x7ybw5gisli1b0a67rkqvr3") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.4 (c (n "atoi_simd") (v "0.14.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 2)))) (h "1793lwpy30vsh8rgfp8zzmq4hhcqqwim46ndl0w561313iww94a5") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14.5 (c (n "atoi_simd") (v "0.14.5") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 2)))) (h "0ra7x11sixfqrkzq01ikriyk7iy8kmyccjyrq6dzjp9rs38pr766") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.0 (c (n "atoi_simd") (v "0.15.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "0y53xsbnq9d1lgv16raqrm4rx5mrkx8cckr7wwbiyb030y6323hr") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.1 (c (n "atoi_simd") (v "0.15.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "1lcgg789kvlcah50f0x6h1zm0jwy2h8kml7g4fkwrjcn6jaj38f2") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.2 (c (n "atoi_simd") (v "0.15.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "0rqpb6dbn62k0h1s0ivy73bsswbxyql5zf78228as72gk7qwzncl") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.3 (c (n "atoi_simd") (v "0.15.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "09jiwr7074j73viiafdzjq9mf39idd784hfpsbf1p1dn05gbchgw") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.4 (c (n "atoi_simd") (v "0.15.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "0dhzmg66j651wpnsy1l6z2kicdfa5sjj8f9gzyycv91bjcja7yck") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.5 (c (n "atoi_simd") (v "0.15.5") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "1l3z63dqliiyvahjw8wdwjyrngfyxhc9rfm775syakg3qgsi9z6c") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15.6 (c (n "atoi_simd") (v "0.15.6") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "1a98kvaqyhb1shi2c6qhvklahc7ckvpmibcy319i6g1i9xqkgq4s") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.16.0 (c (n "atoi_simd") (v "0.16.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2") (d #t) (k 2)))) (h "1sfvqhx7845j9629qhba9b7p71jhkd28agbqxcmi228jjvlgk427") (f (quote (("std" "arrayvec/std") ("default" "std"))))))

