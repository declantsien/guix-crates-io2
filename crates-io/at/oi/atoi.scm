(define-module (crates-io at oi atoi) #:use-module (crates-io))

(define-public crate-atoi-0.1.0 (c (n "atoi") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "0h1nanhhh49ylp2w63i74c16i9121k9xsppxi8p5p3fk3cybdm85")))

(define-public crate-atoi-0.2.0 (c (n "atoi") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "14ji2vgapih3a84kmspy3as047jsqkkzl9wclb7s6kijg5pv895b")))

(define-public crate-atoi-0.2.1 (c (n "atoi") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "1gkzq1dsjb121s56wdbzl89mm0aiq9ib6i8gdaxjnhir88g1qvjl")))

(define-public crate-atoi-0.2.2 (c (n "atoi") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "0z96cvg4sayyyhz14m35rfraqrabsgwwink274wn33bsgb0b8ln6")))

(define-public crate-atoi-0.2.3 (c (n "atoi") (v "0.2.3") (d (list (d (n "checked") (r "^0.5.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "146iblyx8i5csyrvp9v169g4lp89gjva0517gv4ch2ff540xr9ds")))

(define-public crate-atoi-0.2.4 (c (n "atoi") (v "0.2.4") (d (list (d (n "checked") (r "^0.5.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "0hqawdf05sbm54mh3jnmhsis3k5k1jxj4mqmwds4bkrvn1h3hb8h")))

(define-public crate-atoi-0.3.0 (c (n "atoi") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08kpzw2yb97xvc0vifcgfj86nmh29dcmk1zixy2v35dimk8pvfmi")))

(define-public crate-atoi-0.3.1 (c (n "atoi") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1jqs6kag1pw25d7550sfspw95ys9ks1sqj19l82v9j4z65ymliq4")))

(define-public crate-atoi-0.3.2 (c (n "atoi") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0izgx9vf20x6z931yy15sdcvxm6bj40y9iz5l1f5ymv8gclbgbz0")))

(define-public crate-atoi-0.3.3 (c (n "atoi") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "11aqg55yrj3rmi8469njxglyxgyxjcpzmn7rgnrjaz6mjzqpv2aw")))

(define-public crate-atoi-0.4.0 (c (n "atoi") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "19fdm7gxwp98y67ghyis841vy5ka7hc1afm9cfa69qn0bzh9cs31")))

(define-public crate-atoi-1.0.0 (c (n "atoi") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "13mycnr954w17lcvvbpzr4rmhl1h13cg8hq63j0rrx9g6497vifp")))

(define-public crate-atoi-2.0.0 (c (n "atoi") (v "2.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "0a05h42fggmy7h0ajjv6m7z72l924i7igbx13hk9d8pyign9k3gj") (f (quote (("std" "num-traits/std") ("default" "std")))) (r "1.57.0")))

