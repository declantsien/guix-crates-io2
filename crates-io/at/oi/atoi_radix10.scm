(define-module (crates-io at oi atoi_radix10) #:use-module (crates-io))

(define-public crate-atoi_radix10-0.0.1 (c (n "atoi_radix10") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "heapless") (r "^0.7") (d #t) (k 2)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)))) (h "08bidsna8vxqqxlb5v1hlbx1nnjh92n1ndxc5a38ysixlci0m5xn") (f (quote (("simd") ("nightly") ("default"))))))

