(define-module (crates-io at c_ atc_mi_thermometer) #:use-module (crates-io))

(define-public crate-atc_mi_thermometer-0.1.0 (c (n "atc_mi_thermometer") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.4") (d #t) (k 0)) (d (n "ccm") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("kv" "kv_std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "1zv6s47w9ga7676cfbsa678i6gj33a01dw0bcdr493j441by1f3m")))

