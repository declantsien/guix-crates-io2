(define-module (crates-io at #{42}# at42qt2120) #:use-module (crates-io))

(define-public crate-AT42QT2120-0.1.0 (c (n "AT42QT2120") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0swfdl27bwqix7834qwizfi7xhfbiiizx7fscyg3f1pkxxxyrgzi")))

(define-public crate-AT42QT2120-0.1.1 (c (n "AT42QT2120") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0i24vjm449lbkfaq6y7kgprh95ipzrkq1g4fvz59ajg6vqzy64y3")))

(define-public crate-AT42QT2120-0.1.2 (c (n "AT42QT2120") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0id1nimjqi2skqlyi8g04ggy6k6q1cs8hxadqcz7fhx4dvw7y8b1")))

