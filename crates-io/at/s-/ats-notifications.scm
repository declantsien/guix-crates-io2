(define-module (crates-io at s- ats-notifications) #:use-module (crates-io))

(define-public crate-ats-notifications-0.1.0 (c (n "ats-notifications") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "online") (r "^3.0.1") (f (quote ("sync"))) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "timer") (r "^0.2.0") (d #t) (k 0)))) (h "1b5jx4r0zb25q1ys8w9ybkahn51v1d0rm4llj83mrnxpb88zv13z")))

