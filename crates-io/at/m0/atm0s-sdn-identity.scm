(define-module (crates-io at m0 atm0s-sdn-identity) #:use-module (crates-io))

(define-public crate-atm0s-sdn-identity-0.1.0 (c (n "atm0s-sdn-identity") (v "0.1.0") (d (list (d (n "atm0s-sdn-multiaddr") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xxw0zlc3shf28zbz0pfs6zng1b63rqyhcvqc7yk3ix2h5wyz784")))

(define-public crate-atm0s-sdn-identity-0.1.1 (c (n "atm0s-sdn-identity") (v "0.1.1") (d (list (d (n "atm0s-sdn-multiaddr") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0knw4nzvds300wvx8vkbg0bp9km8zcbl7n9w356cgcz8kcp3rl3r")))

(define-public crate-atm0s-sdn-identity-0.1.2 (c (n "atm0s-sdn-identity") (v "0.1.2") (d (list (d (n "atm0s-sdn-multiaddr") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jh4hjwy9f4945jwpaqzjj9hs3xqi5qbvqarlb4b3b166c1cas6q")))

(define-public crate-atm0s-sdn-identity-0.2.0 (c (n "atm0s-sdn-identity") (v "0.2.0") (d (list (d (n "multiaddr") (r "^0.18.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nlc6qhaw8qbls9s8zhlsb2vjw190ibnnkiimlcyxf2jkn893sb3")))

