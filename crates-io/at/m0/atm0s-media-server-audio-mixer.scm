(define-module (crates-io at m0 atm0s-media-server-audio-mixer) #:use-module (crates-io))

(define-public crate-atm0s-media-server-audio-mixer-0.1.0 (c (n "atm0s-media-server-audio-mixer") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0id0jygxakmb779wm05xzd867rcnhlk2mf526kgdpdv46q3zdr4b")))

(define-public crate-atm0s-media-server-audio-mixer-0.1.1 (c (n "atm0s-media-server-audio-mixer") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bp0nlhmkrvhgwmjl6qhf9aj4x501ncnlwm337pm7qg3f6pxnqm9")))

