(define-module (crates-io at m0 atm0s-media-server-protocol) #:use-module (crates-io))

(define-public crate-atm0s-media-server-protocol-0.1.0 (c (n "atm0s-media-server-protocol") (v "0.1.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 1)))) (h "1bmv8h250dakn85xf0z3a13cpb0f92p45l8ac68c9bw8sxgzjcwf")))

(define-public crate-atm0s-media-server-protocol-0.1.1 (c (n "atm0s-media-server-protocol") (v "0.1.1") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-build") (r "^0.12") (d #t) (k 1)))) (h "1lwqbw28lyhca6ygp8q3zaj1h3fhfqzk1kqf5578wwp002i063vn")))

