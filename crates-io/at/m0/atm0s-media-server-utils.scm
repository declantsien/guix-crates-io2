(define-module (crates-io at m0 atm0s-media-server-utils) #:use-module (crates-io))

(define-public crate-atm0s-media-server-utils-0.1.0 (c (n "atm0s-media-server-utils") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "poem-openapi") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)))) (h "0imks0viih9lflx7ximp8k5k0zqh129w7948hykgcfhwwdp5rdxi")))

(define-public crate-atm0s-media-server-utils-0.2.0 (c (n "atm0s-media-server-utils") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.2") (d #t) (k 0)) (d (n "poem-openapi") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)))) (h "1gq6h1l0g882bm1srrn6z7ihqjgcgfnzva3b65zwav73yaa7zmql")))

(define-public crate-atm0s-media-server-utils-0.2.1 (c (n "atm0s-media-server-utils") (v "0.2.1") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11.2") (d #t) (k 0)) (d (n "poem-openapi") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)))) (h "06q1rvb4lsh6igvghcbqwdr8iix4rh7fwfilgla1myinikg45m7k")))

