(define-module (crates-io at m0 atm0s-sdn-transport-compose) #:use-module (crates-io))

(define-public crate-atm0s-sdn-transport-compose-0.1.0 (c (n "atm0s-sdn-transport-compose") (v "0.1.0") (h "0hgvi12r60wlhjms5ymj23kkzf7cc0k9c1sxchp35llqd47syi3h")))

(define-public crate-atm0s-sdn-transport-compose-0.1.1 (c (n "atm0s-sdn-transport-compose") (v "0.1.1") (h "16crfxcnhmcfrzg0az3i130pv1pwr9bp04pcxjv08vvrjhz15bdj")))

