(define-module (crates-io at m0 atm0s-sdn-utils) #:use-module (crates-io))

(define-public crate-atm0s-sdn-utils-0.1.0 (c (n "atm0s-sdn-utils") (v "0.1.0") (d (list (d (n "async-notify") (r "^0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zxbmxmmgjill6a3s77riyid01fxpfgl1kyjf3xdgchxm4x1lq7j") (f (quote (("auto-clear"))))))

(define-public crate-atm0s-sdn-utils-0.1.1 (c (n "atm0s-sdn-utils") (v "0.1.1") (d (list (d (n "async-notify") (r "^0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.12") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1c5mhmbdckjdmi9szmcy48k663g9fmv8smfsqq9mn63jqgn8yr1f") (f (quote (("auto-clear"))))))

