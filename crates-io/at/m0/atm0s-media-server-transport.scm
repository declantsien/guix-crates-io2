(define-module (crates-io at m0 atm0s-media-server-transport) #:use-module (crates-io))

(define-public crate-atm0s-media-server-transport-0.1.0 (c (n "atm0s-media-server-transport") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "media-utils") (r "^0.1.0") (d #t) (k 0) (p "atm0s-media-server-utils")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16wrmh70h0m3bc6pc0cnaknii8akljmhsvinmd47s2n7x214xa3b")))

(define-public crate-atm0s-media-server-transport-0.2.0 (c (n "atm0s-media-server-transport") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "media-utils") (r "^0.2.0") (d #t) (k 0) (p "atm0s-media-server-utils")) (d (n "poem-openapi") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01w1j3l6vd4690g52d2imkq6nxdmcs2rmmm0zg6j8mxikps8wiaq")))

(define-public crate-atm0s-media-server-transport-0.2.1 (c (n "atm0s-media-server-transport") (v "0.2.1") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "media-utils") (r "^0.2.1") (d #t) (k 0) (p "atm0s-media-server-utils")) (d (n "poem-openapi") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13d3lmin5w6dfch0bkw8xxzxdhdgs8vr1q1q1a3jpk7nw6kclq4z")))

