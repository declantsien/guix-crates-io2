(define-module (crates-io at m0 atm0s-media-server-cluster-local) #:use-module (crates-io))

(define-public crate-atm0s-media-server-cluster-local-0.1.0 (c (n "atm0s-media-server-cluster-local") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("tokio1" "unstable" "attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cluster") (r "^0.1.0") (d #t) (k 0) (p "atm0s-media-server-cluster")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "media-utils") (r "^0.1.0") (d #t) (k 0) (p "atm0s-media-server-utils")) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "transport") (r "^0.1.0") (d #t) (k 0) (p "atm0s-media-server-transport")))) (h "162n6y9f3n2kgzlccyiyjrdf4gb3af9xvqw8fdczv3rms7gkg7yi")))

