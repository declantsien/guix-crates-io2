(define-module (crates-io at m0 atm0s-sdn-router) #:use-module (crates-io))

(define-public crate-atm0s-sdn-router-0.1.0 (c (n "atm0s-sdn-router") (v "0.1.0") (d (list (d (n "atm0s-sdn-identity") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (o #t) (d #t) (k 0)))) (h "1xqhxv73dxvk4ir7q32idw1wg1n6r2wbd88c8y4s8k5akw4gv2jw") (f (quote (("mock" "mockall"))))))

(define-public crate-atm0s-sdn-router-0.1.1 (c (n "atm0s-sdn-router") (v "0.1.1") (d (list (d (n "atm0s-sdn-identity") (r "^0.1.0") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (o #t) (d #t) (k 0)))) (h "1xa6jwwn23b30mgqmp4d0n7kqc8w8k95ma3pqqkdcxysigykg2jz") (f (quote (("mock" "mockall"))))))

(define-public crate-atm0s-sdn-router-0.1.2 (c (n "atm0s-sdn-router") (v "0.1.2") (d (list (d (n "atm0s-sdn-identity") (r "^0.1.2") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (o #t) (d #t) (k 0)))) (h "0bxjz5mlnih30a2r1sj2b51frjyb998c0d6m4c8c47m7nzcy12li") (f (quote (("mock" "mockall"))))))

(define-public crate-atm0s-sdn-router-0.1.3 (c (n "atm0s-sdn-router") (v "0.1.3") (d (list (d (n "atm0s-sdn-identity") (r "^0.1.2") (d #t) (k 0)) (d (n "mockall") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "0y01wijp1k4fn47i550a19i44fjqi3a4mcp9vsx0181fri9hgs5j") (f (quote (("mock" "mockall"))))))

(define-public crate-atm0s-sdn-router-0.1.4 (c (n "atm0s-sdn-router") (v "0.1.4") (d (list (d (n "atm0s-sdn-identity") (r "^0.2.0") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "1zyvx7van9xdsy7a061danrxc1mnfi0h1xzkalhn9nfsb079i06m") (f (quote (("mock" "mockall"))))))

