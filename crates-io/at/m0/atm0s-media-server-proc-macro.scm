(define-module (crates-io at m0 atm0s-media-server-proc-macro) #:use-module (crates-io))

(define-public crate-atm0s-media-server-proc-macro-0.1.0 (c (n "atm0s-media-server-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1ml8wi7cp4pdimiznpk3qclqrj2ypvk01bq5ldvj2vbs9frw1cbp")))

(define-public crate-atm0s-media-server-proc-macro-0.1.1 (c (n "atm0s-media-server-proc-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0d98kfazdf7s3ma6kcl3g6vifq8na6agg63y9brmm2r30zdqwblx")))

