(define-module (crates-io at to atto) #:use-module (crates-io))

(define-public crate-atto-0.1.0 (c (n "atto") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "syntect") (r "^5.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "tui-logger") (r "^0.10.0") (d #t) (k 0)))) (h "1gk3yvd6axv5giwfk3m9j8644kvmv53pyvazc3b7blwbb3ri72yw")))

