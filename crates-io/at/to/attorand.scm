(define-module (crates-io at to attorand) #:use-module (crates-io))

(define-public crate-attorand-1.0.0 (c (n "attorand") (v "1.0.0") (h "0jsdns625f8mszgvf45af0ghrprgnd1m4qxhpinhqsdchm19xbyy")))

(define-public crate-attorand-1.0.1 (c (n "attorand") (v "1.0.1") (h "03ii6fnmj9mqikhaxch8k3idlmm3p18x1jhnn0ipyq6fv33n0b75")))

