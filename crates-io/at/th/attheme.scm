(define-module (crates-io at th attheme) #:use-module (crates-io))

(define-public crate-attheme-0.1.0 (c (n "attheme") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)))) (h "1izr9h7ryr5q489pl9yzcy4q9nya0ibf8dxblaby3bqk749c865v")))

(define-public crate-attheme-0.1.1 (c (n "attheme") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)))) (h "0wpzq5k2iqglslxfn5a1sk6zavg2j33fv2mn2m900k6zrrxxxh12")))

(define-public crate-attheme-0.2.0 (c (n "attheme") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)))) (h "0kiyv12rrhlfmzrp73ncg1yds70azhrasjldkj9zr2h1hxq47x1x")))

(define-public crate-attheme-0.2.1 (c (n "attheme") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)))) (h "0n977wvngiqx5448i3czfyr6ml189475pllszpjs7qr9mkx2m1h6")))

(define-public crate-attheme-0.2.2 (c (n "attheme") (v "0.2.2") (d (list (d (n "indexmap") (r "^1.0.1") (d #t) (k 0)))) (h "18kr41dz29md626016i4f024g3hnfxpmnk6k67cvzr4bkmwhhd1a")))

(define-public crate-attheme-0.3.0 (c (n "attheme") (v "0.3.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "079hkyckd3n9f06y5q4mdmziagfwk9cc8shmlkvbbzx5g9w848gz") (f (quote (("graphite-theme"))))))

