(define-module (crates-io at #{4_}# at4_protocol) #:use-module (crates-io))

(define-public crate-at4_protocol-0.1.0 (c (n "at4_protocol") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0iy0rkpky36qzb2hmmkxldlswvzqmhc6pc9sxclr73qp7y0y9v6s") (y #t)))

(define-public crate-at4_protocol-0.1.1 (c (n "at4_protocol") (v "0.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0l07rzskbsgwhnbqcfwwxg7rg988k7ziicsnagxi9bgm2l4jhgfp") (y #t)))

(define-public crate-at4_protocol-0.1.2 (c (n "at4_protocol") (v "0.1.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1iji8d4q5szjc4sl936mv4jn4ypvry7n7rjkv5xx2472x9h8dmrq") (y #t)))

(define-public crate-at4_protocol-0.1.3 (c (n "at4_protocol") (v "0.1.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "00wm0cbj7qch9b72lanqvrz99s1rkdhhczgnvc3d2vfsq7rv3w52") (y #t)))

(define-public crate-at4_protocol-1.0.0 (c (n "at4_protocol") (v "1.0.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "1azj3m86hgjb2xl9rvx4j6nbv2vxwwc9fic4c5vh7cbv6vb5dw09") (y #t)))

(define-public crate-at4_protocol-1.0.1 (c (n "at4_protocol") (v "1.0.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "19yaj2vw0rlg12xig78phdr295afjgl2ya5nci0z842zxy44hc9q") (y #t)))

(define-public crate-at4_protocol-2.0.0 (c (n "at4_protocol") (v "2.0.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "02rfkfnwl5ng6kbyjc021s8ipkmlk7xnd4yx84ffhfzly7dg06iy") (y #t)))

(define-public crate-at4_protocol-2.0.1 (c (n "at4_protocol") (v "2.0.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0d0mqr0jgq4nsr5ykv5dwzalknwlj2nxsj06kaslrqg41a9qyc1n") (y #t)))

(define-public crate-at4_protocol-2.1.0 (c (n "at4_protocol") (v "2.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0prqsn6xndf6pyzrf0j2ky6lz0y118c96i1qgpzigajy7ia0rd79") (y #t)))

(define-public crate-at4_protocol-2.1.1 (c (n "at4_protocol") (v "2.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0brbk6kgjav1xiify76nszcci6lz5q9hf9sscpkb9md6igmg66v2")))

