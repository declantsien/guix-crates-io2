(define-module (crates-io at df atdf2svd) #:use-module (crates-io))

(define-public crate-atdf2svd-0.1.2 (c (n "atdf2svd") (v "0.1.2") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "1q61l1mim3cr72mk5fwc7q7hdbg9mrkpm0656wfcs5c23d6ac71l")))

(define-public crate-atdf2svd-0.1.3 (c (n "atdf2svd") (v "0.1.3") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.11") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0b0jrh4p21ib0drb9dsp72jknm4nyzpa4if0w9axaff4spc19pxz")))

(define-public crate-atdf2svd-0.1.4 (c (n "atdf2svd") (v "0.1.4") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "1mf1bp5ni49l6qvnhrisd7k5j40pqz9555d0pfpkna2wx2yk71mh")))

(define-public crate-atdf2svd-0.2.0 (c (n "atdf2svd") (v "0.2.0") (d (list (d (n "colored") (r ">=1.8.0, <2.0.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.7.1, <0.8.0") (d #t) (k 0)) (d (n "gumdrop") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.6, <0.5.0") (d #t) (k 0)) (d (n "xmltree") (r ">=0.8.0, <0.9.0") (d #t) (k 0)))) (h "0823i4lz8z8bbswdrivn4z3hkfz0l7k24fzq2qpmjgqddqsim0ga")))

(define-public crate-atdf2svd-0.3.0 (c (n "atdf2svd") (v "0.3.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0nabxwhbsgf5b0gdhryr1r14xaxldk665cw6v09hvcf0ckzl2cnk")))

(define-public crate-atdf2svd-0.3.1 (c (n "atdf2svd") (v "0.3.1") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "1d3wj84j93db64yw4zs9g87qcxy90ldsi8hsb5x9810h77k01dhv")))

(define-public crate-atdf2svd-0.3.2 (c (n "atdf2svd") (v "0.3.2") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0pdins1xlf1h34wwsx80jiabvlj16v7fikswn4vqg8l7hgzxd7aj")))

(define-public crate-atdf2svd-0.3.3 (c (n "atdf2svd") (v "0.3.3") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "git-version") (r "^0.3.4") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "svd-encoder") (r "^0.14.1") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0lr6r96pa3n7jg669s2hvrlyx5kgianlwifbp5qp88xgabfyxw5x")))

(define-public crate-atdf2svd-0.4.0 (c (n "atdf2svd") (v "0.4.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "svd-encoder") (r "^0.14.2") (d #t) (k 0)) (d (n "svd-rs") (r "^0.14.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "115ra20pxa148rmxw9zsfsza1h9rk5bhskz9q0va2my7b32ls6dd")))

