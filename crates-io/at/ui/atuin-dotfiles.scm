(define-module (crates-io at ui atuin-dotfiles) #:use-module (crates-io))

(define-public crate-atuin-dotfiles-0.1.0 (c (n "atuin-dotfiles") (v "0.1.0") (d (list (d (n "atuin-client") (r "^18.1.0") (d #t) (k 0)) (d (n "atuin-common") (r "^18.1.0") (d #t) (k 0)) (d (n "crypto_secretbox") (r "^0.1.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0iqvz8nn7jzndc1zl1j0pxsgkqimx5k20p9ngljgkv0ddyrsjqlq") (r "1.67")))

(define-public crate-atuin-dotfiles-0.2.0 (c (n "atuin-dotfiles") (v "0.2.0") (d (list (d (n "atuin-client") (r "^18.2.0") (d #t) (k 0)) (d (n "atuin-common") (r "^18.2.0") (d #t) (k 0)) (d (n "crypto_secretbox") (r "^0.1.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iv0zjyqs4idqy8xkr9vcgqa0kcpn27cpm2vrca6f9bxznkl5ghx") (r "1.67")))

