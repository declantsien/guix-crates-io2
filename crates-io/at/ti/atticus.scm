(define-module (crates-io at ti atticus) #:use-module (crates-io))

(define-public crate-atticus-0.1.0 (c (n "atticus") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "133hjl08yz50ib826jjnf8k7dfh2d38msg76sy2a3fnj001hd0b0")))

(define-public crate-atticus-0.1.1 (c (n "atticus") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "0znrf6iwcbmwvi0nilam947q9mbnnrlr8vr1nyzzg6ci38z01073")))

(define-public crate-atticus-0.2.0 (c (n "atticus") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "016j62d34i89kqpryarhf37ln5r4cmwvf4f5g62k50q7wsdak10v")))

(define-public crate-atticus-0.2.1 (c (n "atticus") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "061xxsqh3s6f8320zqr2525hch8kfpqvg6k132wm723nsnmlnqjj")))

(define-public crate-atticus-0.2.2 (c (n "atticus") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "0cywww51h5fy8w07mkdj7gizdsq6rlz8gxw00c0m9i43799qiz6c") (r "1.63")))

(define-public crate-atticus-0.3.0 (c (n "atticus") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "test-util" "tokio-macros" "macros"))) (d #t) (k 2)))) (h "14imnac6xwmnhyz243wc2s379livdgc886qrrvyqw80f3f2csqhm") (r "1.63")))

