(define-module (crates-io at el atelier_assembler) #:use-module (crates-io))

(define-public crate-atelier_assembler-0.1.0 (c (n "atelier_assembler") (v "0.1.0") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_json") (r "~0.2") (d #t) (k 0)) (d (n "atelier_smithy") (r "~0.2") (d #t) (k 0)))) (h "026g6qnxwizc5i0szsqdf5dxhjnh5m2zk0cfj4l3d8aj32gxy7ff")))

(define-public crate-atelier_assembler-0.1.1 (c (n "atelier_assembler") (v "0.1.1") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_json") (r "~0.2") (d #t) (k 0)) (d (n "atelier_smithy") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0p92d23ngkmkw1l4yw0wzy7wdvxcmvkfcigygm918xalb443l9vl")))

(define-public crate-atelier_assembler-0.1.2 (c (n "atelier_assembler") (v "0.1.2") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_json") (r "~0.2") (d #t) (k 0)) (d (n "atelier_smithy") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1zaxzxkva7jycpvnmb2k7pvjjzffq70z0ax5j08qc5c8g2nh10ys")))

(define-public crate-atelier_assembler-0.1.3 (c (n "atelier_assembler") (v "0.1.3") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_json") (r "~0.2") (d #t) (k 0)) (d (n "atelier_smithy") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "069ygrh20lv5rk7hfn6kvmi2m3kq938nflk22mamjdj2wzf9vy3k")))

(define-public crate-atelier_assembler-0.1.4 (c (n "atelier_assembler") (v "0.1.4") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_json") (r "~0.2") (d #t) (k 0)) (d (n "atelier_smithy") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0sp3gg0m46hp3ndd5i8xm5b757g0pqd0vsi34c7wild7cj1wzvkz")))

