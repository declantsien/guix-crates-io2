(define-module (crates-io at el atelier) #:use-module (crates-io))

(define-public crate-atelier-0.0.1 (c (n "atelier") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1bb34sfg8q1772gihj0q7g9r0p1wskh4ipg4w5sc731f7bkj2lfc") (y #t)))

(define-public crate-atelier-0.1.0 (c (n "atelier") (v "0.1.0") (h "0f7wdcb14wx26ppx9azzw0fraljvh9vdaf7gzpmi6vlc8mdav4rq") (y #t)))

(define-public crate-atelier-0.0.2 (c (n "atelier") (v "0.0.2") (h "01bs80ni65hgdcax6s33z1xd7f753sx2hawnap7yv7pc70k3qzdn")))

