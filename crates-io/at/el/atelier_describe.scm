(define-module (crates-io at el atelier_describe) #:use-module (crates-io))

(define-public crate-atelier_describe-0.1.0 (c (n "atelier_describe") (v "0.1.0") (d (list (d (n "atelier_core") (r "^0.2.0") (d #t) (k 0)) (d (n "somedoc") (r "^0.2.4") (d #t) (k 0)))) (h "1asd4xwbfdh67rssxg43r156il0rafrixs8l8ma8zv3jp2asl3lj")))

(define-public crate-atelier_describe-0.1.1 (c (n "atelier_describe") (v "0.1.1") (d (list (d (n "atelier_core") (r "^0.2.0") (d #t) (k 0)) (d (n "somedoc") (r "^0.2.4") (d #t) (k 0)))) (h "0fmkzyzjiz111v37qhzip9hmhwif8r0pimzlvs7dc7dk2m47h84n")))

(define-public crate-atelier_describe-0.1.2 (c (n "atelier_describe") (v "0.1.2") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)))) (h "1g769zr76i4jm565nmsc7218spsiy8x198f0x7yx50j5c5zsayp4")))

(define-public crate-atelier_describe-0.1.3 (c (n "atelier_describe") (v "0.1.3") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)))) (h "0lwd1yds81fxcki6lwgfl6zm183q22k1kc17cw7acibj5j11b43s")))

(define-public crate-atelier_describe-0.1.4 (c (n "atelier_describe") (v "0.1.4") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)))) (h "0rpv91azynqbj30l525pcpfymrw349v07ikfin20gz8gyscvwn3p")))

(define-public crate-atelier_describe-0.1.5 (c (n "atelier_describe") (v "0.1.5") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)))) (h "1izgwn1wb60my0jby9jzm1g5bjfqgbslni8ilpzfn1xqjl8yl4vr")))

(define-public crate-atelier_describe-0.1.6 (c (n "atelier_describe") (v "0.1.6") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2.7") (d #t) (k 0)))) (h "136cmyarg7926vfh9gng67yx02y4q53w0wngax82i36ddrxyip2v")))

(define-public crate-atelier_describe-0.1.8 (c (n "atelier_describe") (v "0.1.8") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2") (d #t) (k 0)))) (h "025w3cmn0rffdss73pdqkplfixavxh0hgphiysbfm5dccxjyqp0d")))

(define-public crate-atelier_describe-0.1.9 (c (n "atelier_describe") (v "0.1.9") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2") (d #t) (k 0)))) (h "1zakm25fh3w9qqxh4bb3ysir661f9ik00dssz74ixiayzjwlxmpf")))

(define-public crate-atelier_describe-0.1.10 (c (n "atelier_describe") (v "0.1.10") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-fmt" "run-cargo-test"))) (k 2)) (d (n "somedoc") (r "^0.2") (d #t) (k 0)))) (h "0w1jkbdrn22ch05397rfpm5cw70yp2kwcbqbjd0sfn81xirnx6v1")))

