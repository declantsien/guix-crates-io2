(define-module (crates-io at el atelier_json) #:use-module (crates-io))

(define-public crate-atelier_json-0.1.0 (c (n "atelier_json") (v "0.1.0") (d (list (d (n "atelier_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hanjqgfhzlpmpqr9h3jrkyqvbrp0018p47qmsxcfhl084qzx4wi")))

(define-public crate-atelier_json-0.1.1 (c (n "atelier_json") (v "0.1.1") (d (list (d (n "atelier_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18j54nicy9rz8hzv82z6xp0a9xqj797cbbsv1cg010m32fr6wa7w")))

(define-public crate-atelier_json-0.1.2 (c (n "atelier_json") (v "0.1.2") (d (list (d (n "atelier_core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v996m30651xn3bl14s0qfgg4ncj5g8kqwm33a96hnl4mx94p7i1")))

(define-public crate-atelier_json-0.1.3 (c (n "atelier_json") (v "0.1.3") (d (list (d (n "atelier_core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00k63w15fcil0l8nh0g2fjnd32h53ih93cvv7fch3yvdfbinl1p5")))

(define-public crate-atelier_json-0.2.0 (c (n "atelier_json") (v "0.2.0") (d (list (d (n "atelier_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w4vc4lch49v1q10bnn84xpngmj286i0q1wr5fgkc5pzg4h0rwyy")))

(define-public crate-atelier_json-0.2.1 (c (n "atelier_json") (v "0.2.1") (d (list (d (n "assert-json-diff") (r "^1.1.0") (d #t) (k 2)) (d (n "atelier_core") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fpnc8sdf54gfh4p7c3n55c732a87lap5si7n5ykr6sanyjsj57s")))

(define-public crate-atelier_json-0.2.2 (c (n "atelier_json") (v "0.2.2") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15lymf6k7kal48g87n5f6gb5qnllr60cxq4fa87dqp2f7qx5gr1v")))

(define-public crate-atelier_json-0.2.3 (c (n "atelier_json") (v "0.2.3") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qjp9lk75kkiwbfvr0y2j4pa1hxxgsyvl4rns9lz8apjvslgnb65")))

(define-public crate-atelier_json-0.2.4 (c (n "atelier_json") (v "0.2.4") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04cxwh164153iw44d7cw4xa14zjsb9ggz1vv33883prfsqf4dci6")))

(define-public crate-atelier_json-0.2.5 (c (n "atelier_json") (v "0.2.5") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09jlvdhq2ry90568076psxsmm71zn4nm57x15c2yz563pgi8bwq1")))

(define-public crate-atelier_json-0.2.6 (c (n "atelier_json") (v "0.2.6") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1424c5nv688v0259xh7qxm88jmbnq8bzhipmfrm16ysgyqpp9jk8")))

(define-public crate-atelier_json-0.2.7 (c (n "atelier_json") (v "0.2.7") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "atelier_test") (r "^0.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gy4iq1a1v5jfvzzxpk7fz0p848apj1lwp0zxi6zlf1hyd8assxq")))

