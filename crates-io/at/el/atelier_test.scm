(define-module (crates-io at el atelier_test) #:use-module (crates-io))

(define-public crate-atelier_test-0.1.0 (c (n "atelier_test") (v "0.1.0") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)))) (h "0a1r8va47js4nb2izns3f9idk33dmgxp8zh6116z53fcy3dlngch")))

(define-public crate-atelier_test-0.1.1 (c (n "atelier_test") (v "0.1.1") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 0)))) (h "1x90l3njl9i8ik88wp8xfkf7p7hqccn9rjfw7ahys60ya83fsk72")))

(define-public crate-atelier_test-0.1.2 (c (n "atelier_test") (v "0.1.2") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 0)))) (h "0p82l443714wx79rh0j95pmx029zv9wqy9ns79l8x8rhl1rhcd6i")))

(define-public crate-atelier_test-0.1.3 (c (n "atelier_test") (v "0.1.3") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 0)))) (h "0k94hx6ivicn3baphmr2wypv26d0fqzvx84k4sgi1nj8jvdc2rkf")))

(define-public crate-atelier_test-0.1.4 (c (n "atelier_test") (v "0.1.4") (d (list (d (n "atelier_core") (r "~0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 0)))) (h "007vkgm564ikmmwydiifnxvnhc4c2ypsp2bhkg79y1wjxc3d67yq")))

