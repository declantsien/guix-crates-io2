(define-module (crates-io oa rs oars_proc_macro) #:use-module (crates-io))

(define-public crate-oars_proc_macro-0.1.0 (c (n "oars_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08rc730kps2z2vgws819bc4spqbqpxikzhf3mh49lcc6zf0qzdhc")))

(define-public crate-oars_proc_macro-0.1.1 (c (n "oars_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02zcf7bs0yqxng11k05aij4p9jz5zx15xij3z4f83r3rv39w4pdr")))

