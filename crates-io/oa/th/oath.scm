(define-module (crates-io oa th oath) #:use-module (crates-io))

(define-public crate-oath-0.0.3 (c (n "oath") (v "0.0.3") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1gkkczs6mcp14v0s4lc2rzqp5j66947lnbi8zbn77n8kq7di83gm")))

(define-public crate-oath-0.1.0 (c (n "oath") (v "0.1.0") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0blv555206rxn87nmxc685vvfq3k89k05gk1x6v3lf5sr9z433z4")))

(define-public crate-oath-0.1.1 (c (n "oath") (v "0.1.1") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1rvxapc7rwsfvagmmy9hlyb3cm8hgh0vppz2i9l9vmbidcq5h3dj")))

(define-public crate-oath-0.1.2 (c (n "oath") (v "0.1.2") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1h0ra124ba5nff14mwz5nr6j7dgn7cm30j4yifjg1rx61gw629jq")))

(define-public crate-oath-0.1.3 (c (n "oath") (v "0.1.3") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1fbzl7s5hr8cwcg5f9y1rsqbax3qkm1i14w84k6rxysmb22b676q")))

(define-public crate-oath-0.1.4 (c (n "oath") (v "0.1.4") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0vspi80hdbq4z54aff97hdy5p19kfa6ci20d3y6a9hlfly468lk1")))

(define-public crate-oath-0.10.1 (c (n "oath") (v "0.10.1") (d (list (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "hmac") (r "^0.1") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.5") (d #t) (k 0)))) (h "1mff7s0jhw005awgpq3g876krlhcqa8dyjz12fvxgv3as9jmh8fd")))

(define-public crate-oath-0.10.2 (c (n "oath") (v "0.10.2") (d (list (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "hmac") (r "^0.1") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.5") (d #t) (k 0)))) (h "1jl0pvjvsqz6grgwbqccphdijax1i3c9lpsk1l11k0pydbf0ar7c")))

