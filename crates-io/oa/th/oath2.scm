(define-module (crates-io oa th oath2) #:use-module (crates-io))

(define-public crate-oath2-0.9.0 (c (n "oath2") (v "0.9.0") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "06sfys6zsgd9jpqhwbc5j32rkih8h7ybd6ibn7xqkkqd5vmhhygb") (y #t)))

(define-public crate-oath2-0.9.1 (c (n "oath2") (v "0.9.1") (d (list (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1b8pmb8wvcb9dpg7v1rv2aglrdvpscqwp50g6ynnkvqdn5r4nr5x") (y #t)))

