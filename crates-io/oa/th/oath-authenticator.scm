(define-module (crates-io oa th oath-authenticator) #:use-module (crates-io))

(define-public crate-oath-authenticator-0.0.0-unreleased (c (n "oath-authenticator") (v "0.0.0-unreleased") (h "1ln5mfw75sjifwg6wd0byl8azfqqhinkr9ppl9sw0lsy02wqpc8q")))

(define-public crate-oath-authenticator-0.1.0 (c (n "oath-authenticator") (v "0.1.0") (d (list (d (n "apdu-dispatch") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "delog") (r "^0.1.2") (d #t) (k 0)) (d (n "flexiber") (r "^0.1.0") (f (quote ("derive" "heapless"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "heapless-bytes") (r "^0.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "interchange") (r "^0.2") (d #t) (k 0)) (d (n "iso7816") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "trussed") (r "^0.1") (d #t) (k 0)))) (h "0hw2yckvmgj7fgyfi0146h1f1kiy8z6vkiqi6aad381mfswhniqi") (f (quote (("log-warn") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default" "apdu-dispatch"))))))

