(define-module (crates-io oa l- oal-sys) #:use-module (crates-io))

(define-public crate-oal-sys-0.0.1 (c (n "oal-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)))) (h "1dn2r8bi4slraqsx7vs3dw95kgdqf8aynjc1wybmlp6wr8haqrqv") (f (quote (("generate" "bindgen")))) (y #t) (l "openal")))

(define-public crate-oal-sys-0.0.2 (c (n "oal-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)))) (h "1dzklycy31zfcbh8ilmlhk1g5qbnqh08wixsxm95y1z5xfa1lnmd") (f (quote (("generate" "bindgen")))) (l "openal")))

