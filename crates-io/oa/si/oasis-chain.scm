(define-module (crates-io oa si oasis-chain) #:use-module (crates-io))

(define-public crate-oasis-chain-0.0.0 (c (n "oasis-chain") (v "0.0.0") (h "123qdcylbwkvk9f6f7n6zjvk6k029nphfd6n8v5gkwlwv6zmqcz1") (y #t)))

(define-public crate-oasis-chain-0.0.1 (c (n "oasis-chain") (v "0.0.1") (h "161iacyf1zz5ig31s1m4i1y4x135316fasd779c0yk2ahsy8cszk") (y #t)))

(define-public crate-oasis-chain-0.0.2 (c (n "oasis-chain") (v "0.0.2") (h "0z1y14v480sci34hjd2mjkglr8d70jbxi2c3mw1mnfxxh06b3xmi") (y #t)))

