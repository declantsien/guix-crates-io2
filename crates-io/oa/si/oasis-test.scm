(define-module (crates-io oa si oasis-test) #:use-module (crates-io))

(define-public crate-oasis-test-0.2.0 (c (n "oasis-test") (v "0.2.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "memchain") (r "^0.2") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "1zg3bhx8gz1d66zw9hdggqmgjc2bjdmjgfja0vnb0b02bkwixpbv")))

(define-public crate-oasis-test-0.2.1 (c (n "oasis-test") (v "0.2.1") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "memchain") (r "^0.2") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "0z5dwql12xpc3i1chbb6qdbh43b1bs6vjn7x509db2a9w74dk6sm")))

(define-public crate-oasis-test-0.2.2 (c (n "oasis-test") (v "0.2.2") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "memchain") (r "^0.2") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.2") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "1vll3zbsxdhij79lg1v6qr1xgzm6siisjc6is55x48izsx61649g")))

(define-public crate-oasis-test-0.3.0 (c (n "oasis-test") (v "0.3.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "memchain") (r "^0.2") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.3") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "1apn2cpla39239y6ykn2hfbi7aq06j8djk97g06wiijqvi989rww")))

(define-public crate-oasis-test-0.3.1 (c (n "oasis-test") (v "0.3.1") (d (list (d (n "blockchain-traits") (r "^0.3") (d #t) (k 0)) (d (n "memchain") (r "^0.3") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.3") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "1jfrraq3q31w3n9p1579ggyamkafdrwl7mipa3ps50kl5skirhzl")))

(define-public crate-oasis-test-0.4.0 (c (n "oasis-test") (v "0.4.0") (d (list (d (n "blockchain-traits") (r "^0.4") (d #t) (k 0)) (d (n "memchain") (r "^0.4") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.3") (d #t) (k 0)) (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "15x2j9d03795g6slp6l6f03gaqx6d8r3214l97w67zvgx6q3zd8x")))

(define-public crate-oasis-test-0.4.1 (c (n "oasis-test") (v "0.4.1") (d (list (d (n "blockchain-traits") (r "^0.4") (d #t) (k 0)) (d (n "memchain") (r "^0.4") (d #t) (k 0)) (d (n "oasis-macros") (r "^0.3") (d #t) (k 0)) (d (n "oasis-types") (r "^0.4") (d #t) (k 0)))) (h "15xmiy2mlaxhqiwr4dyxbr75chf3swxs7sk1m1x53383644nrpwq")))

