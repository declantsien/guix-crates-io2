(define-module (crates-io oa si oasis-amqp-macros) #:use-module (crates-io))

(define-public crate-oasis-amqp-macros-0.1.0 (c (n "oasis-amqp-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "18r853nbgbr9qdfik1llpgyyn81v6zwwpmhxx8i0pimzpqdwpkn0")))

(define-public crate-oasis-amqp-macros-0.1.1 (c (n "oasis-amqp-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sks38nc2djk3ysivq1i0dsvmjsc1wcknn0rr1a9hw5pd0riwy4i")))

(define-public crate-oasis-amqp-macros-0.2.0 (c (n "oasis-amqp-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lshczpa1sgfnz38h2001yahlnfgrm95d4vkawc5sr59s2c3b2xm")))

