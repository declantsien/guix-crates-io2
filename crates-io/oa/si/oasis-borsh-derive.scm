(define-module (crates-io oa si oasis-borsh-derive) #:use-module (crates-io))

(define-public crate-oasis-borsh-derive-0.2.10 (c (n "oasis-borsh-derive") (v "0.2.10") (d (list (d (n "oasis-borsh-derive-internal") (r "^0.2.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "02vqmpymkn6bb53i2lxjn6l51l2d1vxsaknmxb22spw1anz3lv67")))

