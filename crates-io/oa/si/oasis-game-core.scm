(define-module (crates-io oa si oasis-game-core) #:use-module (crates-io))

(define-public crate-oasis-game-core-0.9.0 (c (n "oasis-game-core") (v "0.9.0") (d (list (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (f (quote ("arbitrary_precision"))) (d #t) (k 0)))) (h "1bhqif833dw49z255plkfvajcwfnydpzjidj0d52g9zwfakrv112")))

