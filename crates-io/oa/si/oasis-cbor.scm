(define-module (crates-io oa si oasis-cbor) #:use-module (crates-io))

(define-public crate-oasis-cbor-0.1.0 (c (n "oasis-cbor") (v "0.1.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0x7n8dlaxc8fqf1isd2xwqvfrxgsfxnn22d949d4n1mzc3wxh871")))

(define-public crate-oasis-cbor-0.1.1 (c (n "oasis-cbor") (v "0.1.1") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1j9wv84jd3qgqvglb7mgfbvyddzyiyslmag4cbf38w3m2fqid6vs")))

(define-public crate-oasis-cbor-0.1.2 (c (n "oasis-cbor") (v "0.1.2") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1q7qiwl2p95lcz5y6ixln37izpq9nm6pn431k3m3c76fgrbhc7j9") (y #t)))

(define-public crate-oasis-cbor-0.1.3 (c (n "oasis-cbor") (v "0.1.3") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "139l2s6dw06h0afb7lkn3w2sdmn29jmxf1998xr0ni2xfp59myxz")))

(define-public crate-oasis-cbor-0.1.4 (c (n "oasis-cbor") (v "0.1.4") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0vvsph4qym4ifjxq5jg5c713pyri36jpqs99p0m8fm0jhzx4wwbs")))

(define-public crate-oasis-cbor-0.1.5 (c (n "oasis-cbor") (v "0.1.5") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1f8g5a34120wrgjb1gn7560qv742mmc6mzq71kwb3zrwb48hkykr")))

(define-public crate-oasis-cbor-0.1.6 (c (n "oasis-cbor") (v "0.1.6") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1ph4jr8blzicdym6h6yz527bqdsnpfw36an1b91r9vya71x4fz5m")))

(define-public crate-oasis-cbor-0.1.7 (c (n "oasis-cbor") (v "0.1.7") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.7") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "04nl3kx48fdpv52sb6mp8lk5prfaah27lb5xkzxk80w3rdjyl3x9")))

(define-public crate-oasis-cbor-0.1.8 (c (n "oasis-cbor") (v "0.1.8") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.1.8") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0gvy64r64yvymjygr8xhig8j95p0xlci84m950chmnccvbby4d3i")))

(define-public crate-oasis-cbor-0.2.0 (c (n "oasis-cbor") (v "0.2.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "06qbx907m6n0v60yg37829c83s8q5zl5cmk34zwjjh3566s903v2")))

(define-public crate-oasis-cbor-0.2.1 (c (n "oasis-cbor") (v "0.2.1") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "00szkq7cr2f5ajdzc2gi2s9v0ibgqry348n79hipjcw1pasml0rq")))

(define-public crate-oasis-cbor-0.3.0 (c (n "oasis-cbor") (v "0.3.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "sk-cbor") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "15gkmc2dz6z0s3n4dx21ia61ms8iwgqb9a3h6lfm3zhimqalryx4")))

(define-public crate-oasis-cbor-0.4.0 (c (n "oasis-cbor") (v "0.4.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "oasis-cbor-value") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1gcn93xh7h0dq8xhwna96c63pfxnqqgspp4hh9wjrzq2yg9w5an3") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-oasis-cbor-0.5.0 (c (n "oasis-cbor") (v "0.5.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "oasis-cbor-value") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0raiq0rp2mkk9fl2g18ssbqwr6rbnf8pwiq52mmljbrzv8xd1rfs") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-oasis-cbor-0.5.1 (c (n "oasis-cbor") (v "0.5.1") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.1") (d #t) (k 0)) (d (n "oasis-cbor-derive") (r "^0.5.1") (d #t) (k 0)) (d (n "oasis-cbor-value") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0zfkxq1v53pwmmd1fmn1xdh3x1f7rq67d5phcygz4h53vhldjzck") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

