(define-module (crates-io oa si oasis-borsh-derive-internal) #:use-module (crates-io))

(define-public crate-oasis-borsh-derive-internal-0.2.10 (c (n "oasis-borsh-derive-internal") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1cznqhhpsc45zmgayk2wfbqzd4p7aw17qqm104faqv0kc29f5pi6")))

(define-public crate-oasis-borsh-derive-internal-0.2.11 (c (n "oasis-borsh-derive-internal") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ldi1c5d9qrfyzbz7aq73pvmrjpp5xr7dm0vg72bg1jc2kfk5cpv")))

(define-public crate-oasis-borsh-derive-internal-0.2.12 (c (n "oasis-borsh-derive-internal") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0z8pmnjzqlj3z923gpsxh6z1f7ghjvmck6c5n1x0bwbvingg6l44")))

(define-public crate-oasis-borsh-derive-internal-0.2.13 (c (n "oasis-borsh-derive-internal") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1lvh8a88krpndbfchf91h9aj4hnfd0s6hqg8x9n2agqyriwh775g")))

(define-public crate-oasis-borsh-derive-internal-0.2.14 (c (n "oasis-borsh-derive-internal") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1w8paxk2508j7i3fhb95bn9jfa1gf6cggm9300b42kfvr7nj3yrh")))

