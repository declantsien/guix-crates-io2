(define-module (crates-io oa si oasis-game-client-proxy) #:use-module (crates-io))

(define-public crate-oasis-game-client-proxy-0.9.0 (c (n "oasis-game-client-proxy") (v "0.9.0") (d (list (d (n "oasis-game-core") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.32") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1n9gbbidpjbn5ccrhv7akbxpvq1n7lbbha97a5ki8nn9wjsmwcra")))

