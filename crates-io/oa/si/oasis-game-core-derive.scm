(define-module (crates-io oa si oasis-game-core-derive) #:use-module (crates-io))

(define-public crate-oasis-game-core-derive-0.9.0 (c (n "oasis-game-core-derive") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "165b7ybcfdg5j7wfi6ypskwj9vr4shqfr82vjn23alkhhsr3wzs9")))

(define-public crate-oasis-game-core-derive-0.9.1 (c (n "oasis-game-core-derive") (v "0.9.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wnh79ikcs9myba8bjchcwffc268y0dmh5jyn1yfyq414cvhqrw7")))

