(define-module (crates-io oa si oasis-types) #:use-module (crates-io))

(define-public crate-oasis-types-0.2.0 (c (n "oasis-types") (v "0.2.0") (d (list (d (n "blockchain-traits") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "172a9an7pq41mzjvljxyllgmfzca8sgf1hi9qz0xlfakmfcdj9jp")))

(define-public crate-oasis-types-0.2.1 (c (n "oasis-types") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 2)))) (h "1c70vvl6wpki16slgsppzfxypbi52ysv9i2pccwcfb253c2l5ma2")))

(define-public crate-oasis-types-0.2.2 (c (n "oasis-types") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_cbor") (r "^0.10") (d #t) (k 2)))) (h "0m950091xh83apl9yip1gw20d6hw0zybs932sbxvfazkwpmnw6vm")))

(define-public crate-oasis-types-0.3.0 (c (n "oasis-types") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "borsh") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02hnpf5mpn0skrnhl89wpplpbps4pj42rrrbgdj3n4pg72q7qa6h") (f (quote (("serde" "borsh"))))))

(define-public crate-oasis-types-0.3.1 (c (n "oasis-types") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "borsh") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k05s0h5vc7zsb3n230qr35cqs2qvzld77skw3xqryfspzca1l23") (f (quote (("serde" "borsh"))))))

(define-public crate-oasis-types-0.3.2 (c (n "oasis-types") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "oasis-borsh") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wgfylx5b95q7k90vnxkfjxd1snzccqnj19hqd330gqa38lhnwlj") (f (quote (("serde" "oasis-borsh"))))))

(define-public crate-oasis-types-0.4.0 (c (n "oasis-types") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "derive_more") (r "^0.15") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "oasis-borsh") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q72lmqsvjdbyvyj0gm1f1qrr836a9qxr6jbck19d10w80zhda1p")))

