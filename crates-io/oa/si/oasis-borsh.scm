(define-module (crates-io oa si oasis-borsh) #:use-module (crates-io))

(define-public crate-oasis-borsh-0.2.10 (c (n "oasis-borsh") (v "0.2.10") (d (list (d (n "oasis-borsh-derive") (r "^0.2.10") (d #t) (k 0)))) (h "1c0ddaar5f0skxadak3f0vq2kay2i114wqii5zhrn9d98fsg9y3g") (f (quote (("std") ("default" "std"))))))

(define-public crate-oasis-borsh-0.2.11 (c (n "oasis-borsh") (v "0.2.11") (d (list (d (n "oasis-borsh-derive") (r "^0.2.10") (d #t) (k 0)))) (h "1chjmipi84i8ly0gii5r76vav68gqjg4g4abfd843yjbqr8bm4gv") (f (quote (("std") ("default" "std"))))))

(define-public crate-oasis-borsh-0.2.12 (c (n "oasis-borsh") (v "0.2.12") (d (list (d (n "oasis-borsh-derive") (r "^0.2.9") (d #t) (k 0)))) (h "1ss0wwvxbybvakdiwa1n87mc3rgbw98g9savspq9c8y4brjwd9ha") (f (quote (("std") ("default" "std"))))))

