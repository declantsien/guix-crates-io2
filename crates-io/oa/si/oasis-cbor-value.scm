(define-module (crates-io oa si oasis-cbor-value) #:use-module (crates-io))

(define-public crate-oasis-cbor-value-0.4.0 (c (n "oasis-cbor-value") (v "0.4.0") (h "10vpnj8wz61n84rhl8pajhblgwkvxd1g9qk22kgqfjb5y539bbfd")))

(define-public crate-oasis-cbor-value-0.5.0 (c (n "oasis-cbor-value") (v "0.5.0") (h "1q3l8dib9ki4ly3vnghdkikv630wfkrs69sn34wjmm32ms5gxvyb")))

(define-public crate-oasis-cbor-value-0.5.1 (c (n "oasis-cbor-value") (v "0.5.1") (h "0l10gwfxni4zgd4yvw4p97xg8chkr334cl44irzblpn5lzsxgq2z")))

