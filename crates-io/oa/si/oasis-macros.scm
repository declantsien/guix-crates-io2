(define-module (crates-io oa si oasis-macros) #:use-module (crates-io))

(define-public crate-oasis-macros-0.2.0 (c (n "oasis-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1alzxs97mlzrpw93s7q0a1gd5c47aa9zph0gw27j16m9pcr2yd38")))

(define-public crate-oasis-macros-0.2.1 (c (n "oasis-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "0d3grdhqbsacqzfzh5xn39h7sq3fsb1yv92bbhbgxdy9n8q7vgbj")))

(define-public crate-oasis-macros-0.2.2 (c (n "oasis-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "16l810ppnvfxh4fi2a2ghjxcx4a0wmvwgsqfjvzsxhsap8k8cv91")))

(define-public crate-oasis-macros-0.2.3 (c (n "oasis-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1wga2ajr7jmnz8acm87ffmhwz786f45z2bdbn1lswrwyig8fmydg") (y #t)))

(define-public crate-oasis-macros-0.3.0 (c (n "oasis-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "100n7030g8kqa4aqyvy6v5mzgx79lxsrd3dmxvlrqqkq9pvf8spr")))

