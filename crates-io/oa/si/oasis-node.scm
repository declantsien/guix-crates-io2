(define-module (crates-io oa si oasis-node) #:use-module (crates-io))

(define-public crate-oasis-node-0.0.0 (c (n "oasis-node") (v "0.0.0") (h "1i32aglrw7lyi4gm02cqvd76v8d764mqy2icvnhry7lxfnl98sx6")))

(define-public crate-oasis-node-0.0.1 (c (n "oasis-node") (v "0.0.1") (h "0firamlk7vn8mx25q8bpi3c0bc3l3vyjawkljgvfq2lpv0vk9rad")))

