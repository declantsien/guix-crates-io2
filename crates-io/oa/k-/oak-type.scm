(define-module (crates-io oa k- oak-type) #:use-module (crates-io))

(define-public crate-oak-type-0.1.0 (c (n "oak-type") (v "0.1.0") (h "03i84b1p94lxdippzj1h44jc2slakkd8g9dh7b544bghiv27ml8l") (y #t)))

(define-public crate-oak-type-0.1.0-alpha (c (n "oak-type") (v "0.1.0-alpha") (h "0g4bhag0hc27rkdx7xyl4cml677iin3s9slrnkxg33a3zih8s3f1")))

