(define-module (crates-io oa k- oak-http-server) #:use-module (crates-io))

(define-public crate-oak-http-server-0.1.0 (c (n "oak-http-server") (v "0.1.0") (h "0bm531bl0sydrr7kar68yz7x5r249jgiw2vnl2rjii6xw21md7ar")))

(define-public crate-oak-http-server-0.2.0 (c (n "oak-http-server") (v "0.2.0") (h "19i08k6s62zbar0zq6dyjwwz8wv57a7pnjm0rg1v9c2526j2llr9")))

(define-public crate-oak-http-server-0.3.0 (c (n "oak-http-server") (v "0.3.0") (h "0iqfdh9q5zs1j6d6yx5n0zpxdr56hj7207png00zfdakvk5xllx4")))

