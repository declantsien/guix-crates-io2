(define-module (crates-io oa nd oandars) #:use-module (crates-io))

(define-public crate-oandars-0.1.0 (c (n "oandars") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.9") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "ratelimit") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1rwd3vp2br2f8l348qlg9ha3vfglj8dyffm9if9flnkpc72vpyyi")))

(define-public crate-oandars-0.1.1 (c (n "oandars") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.9") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "ratelimit") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0c6blmrgq8q9r56b5jdl02m0y3n2hl0x8cdl4fy3szppfzdkr9ka") (y #t)))

(define-public crate-oandars-0.1.2 (c (n "oandars") (v "0.1.2") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.10.9") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "ratelimit") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "04sk1xy221i9mpgq28jclkpx6k9w3l8dv7a88i0vm17nb2lx1rwj")))

