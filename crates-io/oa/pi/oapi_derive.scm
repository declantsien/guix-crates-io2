(define-module (crates-io oa pi oapi_derive) #:use-module (crates-io))

(define-public crate-oapi_derive-0.1.0 (c (n "oapi_derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0msxz3ja6b74dsylh50s0p2s7xln3j8f86z5f04zqklr3q8l83zp")))

(define-public crate-oapi_derive-0.1.1 (c (n "oapi_derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1ia3zblxf58jjxln8aklf39d6gr8qcxx0iqaxla5zpsyj32jjydf")))

(define-public crate-oapi_derive-0.1.2 (c (n "oapi_derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1jhxddl2xg063c92r0bd2cycfdv3mb70knng15n2s2dizrs2pyc7")))

