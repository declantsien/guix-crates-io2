(define-module (crates-io oa ti oatie) #:use-module (crates-io))

(define-public crate-oatie-0.3.0 (c (n "oatie") (v "0.3.0") (d (list (d (n "either") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "ron") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.2") (d #t) (k 0)) (d (n "yansi") (r "^0.3.4") (d #t) (k 0)))) (h "12h6b7zk738pif3ddjs0cnad05y832a4ilg1q02hz26zc8mh6dg6")))

