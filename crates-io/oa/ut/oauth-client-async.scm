(define-module (crates-io oa ut oauth-client-async) #:use-module (crates-io))

(define-public crate-oauth-client-async-0.4.0 (c (n "oauth-client-async") (v "0.4.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.13") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "url") (r "^1.6.0") (d #t) (k 0)))) (h "0kbsn9b77rz4lkkb5dm977y3k1f4db0abcbswsi1h1ig9252bhxw")))

