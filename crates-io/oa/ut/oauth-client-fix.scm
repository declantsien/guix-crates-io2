(define-module (crates-io oa ut oauth-client-fix) #:use-module (crates-io))

(define-public crate-oauth-client-fix-0.1.4 (c (n "oauth-client-fix") (v "0.1.4") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1n33adijcd0zr6ciwpl3x6f0rajbxc2r9r0dck3aq2kzv852blhs")))

(define-public crate-oauth-client-fix-0.1.5 (c (n "oauth-client-fix") (v "0.1.5") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "004qyka185kd15pqjrlv2psxbs0ip160da4jcrcdgwq0rin705jv")))

