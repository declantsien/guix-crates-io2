(define-module (crates-io oa ut oauth2-apple) #:use-module (crates-io))

(define-public crate-oauth2-apple-0.0.0 (c (n "oauth2-apple") (v "0.0.0") (h "1ignnyzxrnlk2mjvaq9f08pm7j6zy7ndcvsx26r586phfm490lxk") (y #t)))

(define-public crate-oauth2-apple-0.1.0 (c (n "oauth2-apple") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1sjy8ra7d342rqghw4bh8adjfx6l1fnxdssa8y0fyl13n5cqa1fx")))

(define-public crate-oauth2-apple-0.1.1 (c (n "oauth2-apple") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1zkkb1b40689a7z247bpklsdlph2ajglrksfpnnbykdb2l4k8zmn")))

(define-public crate-oauth2-apple-0.1.2 (c (n "oauth2-apple") (v "0.1.2") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "03nzdxqgb588630avw4j09afrynfdx6ckgvnk0fh4hdik332hp50")))

(define-public crate-oauth2-apple-0.2.0 (c (n "oauth2-apple") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1jzaz69qvvmgf6xmgv9mx4zvdfcihp1mx9p4aniypr4id2d11919")))

