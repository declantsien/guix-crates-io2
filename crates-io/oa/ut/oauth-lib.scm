(define-module (crates-io oa ut oauth-lib) #:use-module (crates-io))

(define-public crate-oauth-lib-0.1.0 (c (n "oauth-lib") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "net"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt"))) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0q9x508zvvyppxwb1fz1z3vhzyjkl8qhcf456xdvbxnvcjc4sj71")))

(define-public crate-oauth-lib-0.1.1 (c (n "oauth-lib") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oauth2") (r "^4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("io-util" "net" "rt-multi-thread"))) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1394v5vk34hb3l3wkh75gh6y0jf2frd6wxqsa5pnbydxnd81zn1g")))

