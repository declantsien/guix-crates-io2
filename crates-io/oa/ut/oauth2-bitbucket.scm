(define-module (crates-io oa ut oauth2-bitbucket) #:use-module (crates-io))

(define-public crate-oauth2-bitbucket-0.0.0 (c (n "oauth2-bitbucket") (v "0.0.0") (h "01d2rby41qxi621wykasf1h1qw0s3kc5qimk928ayyw21gg6y3hy")))

(define-public crate-oauth2-bitbucket-0.1.0 (c (n "oauth2-bitbucket") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1s66f60wryphid0bwvhk2vkf6nfb5hqy28xli69dyglisf1n3acc")))

(define-public crate-oauth2-bitbucket-0.1.1 (c (n "oauth2-bitbucket") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1cmn4kfkkb06m9d6j5bgwpfclgjj92s2mvgj6i7dpg5yidhylnaj")))

(define-public crate-oauth2-bitbucket-0.2.0 (c (n "oauth2-bitbucket") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0qrgpvkp7v8hc9qf46gm9h988axfdb2x25vdnksjxdb5cr8d53nn")))

