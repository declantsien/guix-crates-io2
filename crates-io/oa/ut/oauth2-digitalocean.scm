(define-module (crates-io oa ut oauth2-digitalocean) #:use-module (crates-io))

(define-public crate-oauth2-digitalocean-0.0.0 (c (n "oauth2-digitalocean") (v "0.0.0") (h "0kfkjkmahlivvp9jj20jy8fyxsld1j0b69rba9fn4fgnycd3k4sc")))

(define-public crate-oauth2-digitalocean-0.1.0 (c (n "oauth2-digitalocean") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1ydwclklw53pkfmvzqis7wvflxjg47l1j598g2imlg4jzbcz0s2q")))

(define-public crate-oauth2-digitalocean-0.1.1 (c (n "oauth2-digitalocean") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0kpmjai3ff7i8xy859lr9z895i1rqr5idkfgsz9fwm6cqwn8hi8k")))

(define-public crate-oauth2-digitalocean-0.2.0 (c (n "oauth2-digitalocean") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "19v019ypnxjqfb36f5rrnzk5aki19sk6pc1bksq958kzcjjs9g49")))

