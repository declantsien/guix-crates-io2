(define-module (crates-io oa ut oauth2-mastodon) #:use-module (crates-io))

(define-public crate-oauth2-mastodon-0.0.0 (c (n "oauth2-mastodon") (v "0.0.0") (h "0rkl5456n6cxngzpg2r197bal2j1mfsc0c8lrdsic1xwkvssp1zy")))

(define-public crate-oauth2-mastodon-0.1.0 (c (n "oauth2-mastodon") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0jdrn3747h24lhfdvbsj371i4cxlg4zjrnj4pl55z6k5dcx24053")))

(define-public crate-oauth2-mastodon-0.1.1 (c (n "oauth2-mastodon") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1gh1qq64vrc1qm99x14848bq74ijlz7vgqwp9liiqsrsibkjh3vq")))

(define-public crate-oauth2-mastodon-0.2.0 (c (n "oauth2-mastodon") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0rp695mc026cj69vnw04bg9xk6zmc99ix40vrpd2pmyxirbqjfwi")))

