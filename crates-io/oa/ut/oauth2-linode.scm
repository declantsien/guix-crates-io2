(define-module (crates-io oa ut oauth2-linode) #:use-module (crates-io))

(define-public crate-oauth2-linode-0.1.0 (c (n "oauth2-linode") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0z0vn7av072j637ndsy7y6shg7gjhfd2qc86g7sypxwah4791g8r")))

(define-public crate-oauth2-linode-0.2.0 (c (n "oauth2-linode") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1i3rwri2v3ykgrs9iaxz9jydr3agyc37afd5sd5ss57p8m69fhal")))

