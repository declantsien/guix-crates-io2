(define-module (crates-io oa ut oauth2-tiktok) #:use-module (crates-io))

(define-public crate-oauth2-tiktok-0.0.0 (c (n "oauth2-tiktok") (v "0.0.0") (h "0bcgixpyi37g18m3sn41j2p8j3bslir0wzhvzmsxxfzqjgq0rpki")))

(define-public crate-oauth2-tiktok-0.1.0 (c (n "oauth2-tiktok") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1g3c5vy8cggv13xzrg3rh44rbb5654hr8k66n3lyqsdbwbl36s7p")))

(define-public crate-oauth2-tiktok-0.1.1 (c (n "oauth2-tiktok") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1fs0x808qa4ha9j64w98ffppxb0pjncyvys8f29aqjx423chwd5k")))

(define-public crate-oauth2-tiktok-0.2.0 (c (n "oauth2-tiktok") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1scmmd2h8yvlgjf42q4lyxifsdfr5fgs9y6wzm8142aby2i9ar57")))

