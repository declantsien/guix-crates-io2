(define-module (crates-io oa ut oauth_decap_github_lib) #:use-module (crates-io))

(define-public crate-oauth_decap_github_lib-0.0.1 (c (n "oauth_decap_github_lib") (v "0.0.1") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0kwrrf542p1q8m6i1c71pkfwmk3qjg5xcd2h0idwvxdx4011h24x") (y #t)))

(define-public crate-oauth_decap_github_lib-0.0.2 (c (n "oauth_decap_github_lib") (v "0.0.2") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "03zcfcg3nzkvgk4llm5j469am98xc0zwp556l1fpn8ir924851g4") (y #t)))

(define-public crate-oauth_decap_github_lib-0.0.3 (c (n "oauth_decap_github_lib") (v "0.0.3") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1dmlhavwcz624wy5ql99lxjm0j6bph22y5qrzy39sv5k5hsiwrzn") (y #t)))

(define-public crate-oauth_decap_github_lib-0.0.4 (c (n "oauth_decap_github_lib") (v "0.0.4") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1db5ah6rdmz5c232vhr5307ri1v83nzwj2g2hspcsjm1a0kyjrdk") (y #t)))

(define-public crate-oauth_decap_github_lib-0.0.5 (c (n "oauth_decap_github_lib") (v "0.0.5") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "06k0djxlzbihs99wpbgppfprrqk9dyxc4cpfqs9ads725ih0l903") (y #t)))

(define-public crate-oauth_decap_github_lib-0.0.6 (c (n "oauth_decap_github_lib") (v "0.0.6") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth2") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "072x6hi1338cca3l60pvv1qnk29xbslg158v0syj15cvhn1vvcpr") (y #t)))

