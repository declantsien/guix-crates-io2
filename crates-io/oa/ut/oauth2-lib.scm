(define-module (crates-io oa ut oauth2-lib) #:use-module (crates-io))

(define-public crate-oauth2-lib-0.3.0 (c (n "oauth2-lib") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (f (quote ("nightly"))) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)) (d (n "textnonce") (r "^0.3") (f (quote ("rust-nightly"))) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1vdynn4jyfz8dlqrm7msciafmfrpd8x35f1i07rbkfbxv5ibxffr")))

(define-public crate-oauth2-lib-0.4.0 (c (n "oauth2-lib") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7") (f (quote ("nightly"))) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)) (d (n "textnonce") (r "^0.3") (f (quote ("rust-nightly"))) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0f8jb2ylmlfayb8162m7bd4ggb64a48kql6ws02kl0i2kl8x5xcv")))

