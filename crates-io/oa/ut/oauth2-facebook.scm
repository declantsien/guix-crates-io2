(define-module (crates-io oa ut oauth2-facebook) #:use-module (crates-io))

(define-public crate-oauth2-facebook-0.0.0 (c (n "oauth2-facebook") (v "0.0.0") (h "0fzyimmb3rn0ry37mwny619mhyszpjqp28gp8gadigxycqcn4gcz")))

(define-public crate-oauth2-facebook-0.1.0 (c (n "oauth2-facebook") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde-aux") (r "^2.2") (k 0)))) (h "1cv239zh81zdvvz7sac5cq09xmv2xb17xjrbygy6ywb09jglcnh2")))

(define-public crate-oauth2-facebook-0.1.1 (c (n "oauth2-facebook") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde-aux") (r "^2.2") (k 0)))) (h "0x7vkk39nb05b1ksk8m5f2w4h5qfiw28zac46khidd08816znz1g")))

(define-public crate-oauth2-facebook-0.1.2 (c (n "oauth2-facebook") (v "0.1.2") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde-aux") (r "^4") (k 0)))) (h "125kx62ws0l8x0759sswwkvmzj3injp1dyjk1f8hgzqd735zc9b5")))

(define-public crate-oauth2-facebook-0.1.3 (c (n "oauth2-facebook") (v "0.1.3") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde-aux") (r "^4") (k 0)))) (h "0vay2wg7wcaw63mgycjjg4dj6jv8izpg6c1mmjp8h7n0yqnf7nfm")))

(define-public crate-oauth2-facebook-0.2.0 (c (n "oauth2-facebook") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde-aux") (r "^4") (k 0)))) (h "1f5mhxfl8vl2gfcck0cvryijf0l5vpchd5cfm9q8qbx4izgarm6m")))

