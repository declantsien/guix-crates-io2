(define-module (crates-io oa ut oauth2-pinterest) #:use-module (crates-io))

(define-public crate-oauth2-pinterest-0.0.0 (c (n "oauth2-pinterest") (v "0.0.0") (h "0x879alkn8q0ai3lg0asydgcvb5v7vnn6fnrigbk4mci6a46lc8y")))

(define-public crate-oauth2-pinterest-0.1.0 (c (n "oauth2-pinterest") (v "0.1.0") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0w4v90rbmzj1q9cfakrpdgqiybkv3qmwacpfik16fanp7wdbw46l")))

(define-public crate-oauth2-pinterest-0.1.1 (c (n "oauth2-pinterest") (v "0.1.1") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1xf11z3bz849y4rqh1a83c6bfgvlps1g5gi93sj2yypqc7z4djk3")))

(define-public crate-oauth2-pinterest-0.2.0 (c (n "oauth2-pinterest") (v "0.2.0") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1wd4q59svq1byz0n764lk2v8mmpwqawwfnw5k9sdfy3ymyy4xzw0")))

