(define-module (crates-io oa ut oauth2-wechat) #:use-module (crates-io))

(define-public crate-oauth2-wechat-0.0.0 (c (n "oauth2-wechat") (v "0.0.0") (h "1kx6c5583phwx5nnhbmww8rzzp9xb483vfd317ckpwzknalfhgai")))

(define-public crate-oauth2-wechat-0.1.0 (c (n "oauth2-wechat") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1nmxj7iv5cqs76wvixdn3djzwyapnhnfnkz8sn5dpc9wkjf6fjbk")))

(define-public crate-oauth2-wechat-0.1.1 (c (n "oauth2-wechat") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1kzld6x36ssyjjhxnw2pq3xllk9hi4234d4vj170hnidq4h8v32a")))

(define-public crate-oauth2-wechat-0.2.0 (c (n "oauth2-wechat") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0cccivcvkl4va6fggrp2drgv23g7c9h4b0nv9arwidvh8y4ifsvw")))

