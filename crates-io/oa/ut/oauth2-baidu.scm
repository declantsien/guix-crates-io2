(define-module (crates-io oa ut oauth2-baidu) #:use-module (crates-io))

(define-public crate-oauth2-baidu-0.1.0 (c (n "oauth2-baidu") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0n74dz43msqp4bcw9l58rkm4nkp6fqnr9naqhiwvidmmjsfls8yy")))

(define-public crate-oauth2-baidu-0.1.1 (c (n "oauth2-baidu") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0ihz4kmxv6k0k592ags0jlbpam7qrpm038biff53giq4yixkv6il")))

(define-public crate-oauth2-baidu-0.2.0 (c (n "oauth2-baidu") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0w0h974yy08f6c8rixcfr3kaaaysfh7f0ppazj9r462yxkp9rf2v")))

