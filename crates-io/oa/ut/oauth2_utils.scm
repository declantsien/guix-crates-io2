(define-module (crates-io oa ut oauth2_utils) #:use-module (crates-io))

(define-public crate-oauth2_utils-0.0.1 (c (n "oauth2_utils") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0mwcmp751fz3vzrmshd1hsdx00dr4fyx24fcnkw9akmc17fdj99d")))

(define-public crate-oauth2_utils-1.0.0 (c (n "oauth2_utils") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "10mssxxkq5nxssg5i1d78gpm6sckgn6fg65nq5k9mdc8xpyw1qjv")))

(define-public crate-oauth2_utils-1.0.1 (c (n "oauth2_utils") (v "1.0.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0rbqijs46vfwgq60h0vfzskxp0lsv3fsnxxqx47i13w57zngvpp3")))

(define-public crate-oauth2_utils-1.0.2 (c (n "oauth2_utils") (v "1.0.2") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1146z9nk84vy5k9kibvqz7ffpsfkmr23yg2v0147rcpw921vk21w")))

(define-public crate-oauth2_utils-2.0.0 (c (n "oauth2_utils") (v "2.0.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0ajk32qx7kshrrsw4c2m1a72k8kl39y1aqcrvw25hvsbxmzbqlpp")))

