(define-module (crates-io oa ut oauth2-noserver-rs) #:use-module (crates-io))

(define-public crate-oauth2-noserver-rs-0.1.0 (c (n "oauth2-noserver-rs") (v "0.1.0") (d (list (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.4.0") (d #t) (k 0)))) (h "0f4l8x5x7s1zqnjy24v53l4ylwx0v1ppvxqbs14qzkv91qliq8q2")))

