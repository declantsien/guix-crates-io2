(define-module (crates-io oa ut oauth-api) #:use-module (crates-io))

(define-public crate-oauth-api-0.2.0 (c (n "oauth-api") (v "0.2.0") (d (list (d (n "curl") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "0n3c25fpl7qm2ygnwqz6x6h404f4hwjfavqwjg801sshlq5w1j66")))

(define-public crate-oauth-api-0.2.1 (c (n "oauth-api") (v "0.2.1") (d (list (d (n "curl") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "08763ag06c43zsd9hk2kvrvgm1lpygdvp0nbyd2wzxdl3wv1dabl")))

(define-public crate-oauth-api-0.2.2 (c (n "oauth-api") (v "0.2.2") (d (list (d (n "curl") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "12hr1qkblvi2xsqciylvngn4q00sin6f6zsg3h4iq1l427kkkz1d")))

