(define-module (crates-io oa ut oauth_decap_github) #:use-module (crates-io))

(define-public crate-oauth_decap_github-0.0.2 (c (n "oauth_decap_github") (v "0.0.2") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth_decap_github_lib") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvl5qygjpn3xy95r2vpz8mwpcay7j0vg7pv2sq73p0val4maxq9") (y #t)))

(define-public crate-oauth_decap_github-0.0.3 (c (n "oauth_decap_github") (v "0.0.3") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth_decap_github_lib") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpsy17yyh5ghmz281zi2rv0kc68s9w966sd7v2rdpf6yvr7n25h") (y #t)))

(define-public crate-oauth_decap_github-0.0.4 (c (n "oauth_decap_github") (v "0.0.4") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth_decap_github_lib") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03k2qfhy7092w80i81c7dhkbl80yrc8isqij2gs9zv1kbqcr0r0j") (y #t)))

(define-public crate-oauth_decap_github-0.0.5 (c (n "oauth_decap_github") (v "0.0.5") (d (list (d (n "axum") (r "^0.7") (d #t) (k 0)) (d (n "oauth_decap_github_lib") (r "^0.0.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zny24a9c3w2alxw5s3y2s99jvrpvwclxvsaccz23v9y31mbgrxs") (y #t)))

