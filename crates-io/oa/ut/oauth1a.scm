(define-module (crates-io oa ut oauth1a) #:use-module (crates-io))

(define-public crate-oauth1a-0.1.0 (c (n "oauth1a") (v "0.1.0") (h "1ihyqzccj18fbhyafbmw1l41b3vx580djv0k727kkcra4kqm901h")))

(define-public crate-oauth1a-0.1.1 (c (n "oauth1a") (v "0.1.1") (h "1immdgbm6clgk46ld7vcm0bqpg63bvl9jnan409sc3yzcpqcb51s")))

