(define-module (crates-io oa ut oauth2-surf) #:use-module (crates-io))

(define-public crate-oauth2-surf-0.1.0 (c (n "oauth2-surf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "oauth2") (r "^4.0.0-alpha.2") (k 0)) (d (n "surf") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08sbcls8rb23hv3g5msw7v0ravb0k5mzijr6bw5zhk10f8zw3anx")))

(define-public crate-oauth2-surf-0.1.1 (c (n "oauth2-surf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "oauth2") (r "^4.0.0-alpha.2") (k 0)) (d (n "surf") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lzqqby17761qikgnbv99m7d92zcyyyrdyk2qsqc8622x6vfd5zq")))

(define-public crate-oauth2-surf-0.1.2 (c (n "oauth2-surf") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "oauth2") (r "^4.0.0") (k 0)) (d (n "surf") (r "^2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yyjxnnxkw3l9qvg5zlrcrpdwfs9q7pin0ldy1baqspvzkyl3858")))

