(define-module (crates-io oa ut oauth2-dropbox) #:use-module (crates-io))

(define-public crate-oauth2-dropbox-0.0.0 (c (n "oauth2-dropbox") (v "0.0.0") (h "0v1rv3ggpy255kqqygxlwbh8jabpl47kps2x69j1b3s7lbyjqfyn")))

(define-public crate-oauth2-dropbox-0.1.0 (c (n "oauth2-dropbox") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1xjqzdghdfz0bdl0yp9p8pv59ixy81sa741jfkc0nygi813b4gk7")))

(define-public crate-oauth2-dropbox-0.1.1 (c (n "oauth2-dropbox") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0fcv0rp01vfjw34zp4dlxnskbwab0dwl1nav1z9149fxyknkbik6")))

(define-public crate-oauth2-dropbox-0.2.0 (c (n "oauth2-dropbox") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1knrvs24fvbkn05hpcdsg4mj8x54r5cd4jjdg97syp7saym8zry2")))

