(define-module (crates-io oa ut oauth-axum) #:use-module (crates-io))

(define-public crate-oauth-axum-0.1.0 (c (n "oauth-axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 2)))) (h "04jbvhabjxygysi3q2n402vsc9ab7dw9kav9rshy97h05adid76q")))

(define-public crate-oauth-axum-0.1.1 (c (n "oauth-axum") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 2)))) (h "0l7dsax7sapn98cnmlz183y7w7v7jfdbilyrsaapaig81jj42g9c")))

(define-public crate-oauth-axum-0.1.2 (c (n "oauth-axum") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.80") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (f (quote ("macros"))) (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-postgres") (r "^0.7.10") (d #t) (k 2)))) (h "1qyp3iagasqgmm9i67gi0qm95d5gqa82nc6k5ma36zvg10s931ar")))

