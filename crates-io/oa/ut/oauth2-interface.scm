(define-module (crates-io oa ut oauth2-interface) #:use-module (crates-io))

(define-public crate-oauth2-interface-0.1.0 (c (n "oauth2-interface") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 2)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "0acmsdnhcpafj90sjjx69f93yj6xqg07ab4sybl292ywhmq4hrnq") (y #t)))

