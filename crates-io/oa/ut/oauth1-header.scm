(define-module (crates-io oa ut oauth1-header) #:use-module (crates-io))

(define-public crate-oauth1-header-0.1.0 (c (n "oauth1-header") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.2") (d #t) (k 0)))) (h "020vib7z10i6z6q9glarypa8clamwf9yld8zcf09inil9i2385gn")))

