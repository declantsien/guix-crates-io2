(define-module (crates-io oa ut oauth2_noserver) #:use-module (crates-io))

(define-public crate-oauth2_noserver-0.1.0 (c (n "oauth2_noserver") (v "0.1.0") (d (list (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.4.0") (d #t) (k 0)))) (h "1xggqdfwy4yw39pp2gmyz8f99zl3vq5c7vdy0k3rcvvhcbq4sxk3")))

(define-public crate-oauth2_noserver-0.1.1 (c (n "oauth2_noserver") (v "0.1.1") (d (list (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.4.0") (d #t) (k 0)))) (h "18wxndwxgs0q195c8x9b5y5saspb2v1aisn8sismygh9mb7rxiv0")))

(define-public crate-oauth2_noserver-0.1.2 (c (n "oauth2_noserver") (v "0.1.2") (d (list (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.4.0") (d #t) (k 0)))) (h "1qjpzybcrfna820pbrkc80q1ryx5q84wanjxsg2fn5c9mk1mv7dl")))

