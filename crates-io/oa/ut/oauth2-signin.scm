(define-module (crates-io oa ut oauth2-signin) #:use-module (crates-io))

(define-public crate-oauth2-signin-0.0.0 (c (n "oauth2-signin") (v "0.0.0") (h "1pc5q3rbf4a9vn3mp9gpr6mh1qa5kag8p1947154s903kfyjq97l")))

(define-public crate-oauth2-signin-0.1.0 (c (n "oauth2-signin") (v "0.1.0") (d (list (d (n "http-api-isahc-client") (r "^0.2") (k 2)) (d (n "oauth2-client") (r "^0.1") (f (quote ("with-flow"))) (k 0)) (d (n "oauth2-github") (r "^0.1") (d #t) (k 2)) (d (n "oauth2-google") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt"))) (k 2)))) (h "19fimzcrgiqcckxnhij3ksi1kzdz3rxdkk4l2r0glymby6bi9jaj")))

(define-public crate-oauth2-signin-0.1.1 (c (n "oauth2-signin") (v "0.1.1") (d (list (d (n "http-api-isahc-client") (r "^0.2") (k 2)) (d (n "oauth2-client") (r "^0.1") (f (quote ("with-flow"))) (k 0)) (d (n "oauth2-github") (r "^0.1") (d #t) (k 2)) (d (n "oauth2-google") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (k 2)))) (h "0avzwpb8myhgzk6ipsy31hlfnqqnlffi4yhps37hqbdas1qv1zgb")))

(define-public crate-oauth2-signin-0.2.0 (c (n "oauth2-signin") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (f (quote ("with-flow"))) (k 0)) (d (n "http-api-isahc-client") (r "^0.2") (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (k 2)))) (h "0g8zfswnfw32fsx1bvs4grzkiksmasvj0gcqzzfynni4n837shmw")))

