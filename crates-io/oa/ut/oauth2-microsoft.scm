(define-module (crates-io oa ut oauth2-microsoft) #:use-module (crates-io))

(define-public crate-oauth2-microsoft-0.0.0 (c (n "oauth2-microsoft") (v "0.0.0") (h "0ppya9p3nrl25vmckzal0m2hk2ab7vaxyc7vin30qpzba041hhci")))

(define-public crate-oauth2-microsoft-0.1.0 (c (n "oauth2-microsoft") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0c8jlrks00avb6aqdgj9rwivhby7qbaswwz2kpx6773byikaw32v")))

(define-public crate-oauth2-microsoft-0.1.1 (c (n "oauth2-microsoft") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0v0zyvzs7k2zmppz4d837fn01ya30bsqrpvi12lkqp6m8f1am8df")))

(define-public crate-oauth2-microsoft-0.2.0 (c (n "oauth2-microsoft") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0d05cila3dn65p6iz4h2smi9law342n7nb24nbf75zxc1jlk3zn2")))

