(define-module (crates-io oa ut oauth-credentials) #:use-module (crates-io))

(define-public crate-oauth-credentials-0.1.0 (c (n "oauth-credentials") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1jbx5jqrxl8jn121wiawqs7jql0js4vd2d3yfpqd5wrblpmmsa61") (f (quote (("std"))))))

(define-public crate-oauth-credentials-0.1.1 (c (n "oauth-credentials") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1m9mx4q84fd82n211iwz6sc90jjkvdpv0ky0150mz5xpmrxw5ybw") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-oauth-credentials-0.2.0 (c (n "oauth-credentials") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "118zagfslc0w9ill6kamkifq4bv29z40lqldp2yh8mv6z8qvjkpz") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-oauth-credentials-0.1.2 (c (n "oauth-credentials") (v "0.1.2") (d (list (d (n "oauth-credentials") (r "^0.2") (k 0)))) (h "0jz3qvp67haigw4r9gqlc1ldj7k5ycfmc39cwm6nd8nl988sgx5q") (f (quote (("std" "oauth-credentials/std") ("serde" "oauth-credentials/serde") ("alloc" "oauth-credentials/alloc"))))))

(define-public crate-oauth-credentials-0.2.1 (c (n "oauth-credentials") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "11bcjkzbq0cfdq5v0bfj9l23k6zxzjcs9mm6cjmxxqlijz4hszlr") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-oauth-credentials-0.3.0 (c (n "oauth-credentials") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)))) (h "1k5wipfssvdpivkkqr714wb63q40aqmml8n1w1px0ny99kp9svrd") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

