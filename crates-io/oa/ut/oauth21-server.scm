(define-module (crates-io oa ut oauth21-server) #:use-module (crates-io))

(define-public crate-oauth21-server-0.1.0 (c (n "oauth21-server") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "15daa6jix50n8c4n3ql2kk0j98cdr4lin0cz5rvilhaikhz1zxv1")))

(define-public crate-oauth21-server-0.1.1 (c (n "oauth21-server") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1l255hlzxv921wfx2z3nrirdzxmd78wwxqdjf4ia8mwsgp4151pr")))

(define-public crate-oauth21-server-0.1.2 (c (n "oauth21-server") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0hvqxafmjxrbz0bamdmycxdz2fckchm99jqzj88i27pqf2n0lvlh")))

