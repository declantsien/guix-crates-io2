(define-module (crates-io oa ut oauth2token) #:use-module (crates-io))

(define-public crate-oauth2token-0.1.0 (c (n "oauth2token") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("json"))) (d #t) (k 0)))) (h "19y9zgwd8dij4fr2c3wgqga8cx4jzfzfkcaicjr72x02fah6ddaw")))

(define-public crate-oauth2token-0.1.1 (c (n "oauth2token") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1cyw15wznw374i9vnkdd72sb53hcffkxm8kzn96n9l41r2m1fpx8")))

