(define-module (crates-io oa ut oauth2-twitter) #:use-module (crates-io))

(define-public crate-oauth2-twitter-0.0.0 (c (n "oauth2-twitter") (v "0.0.0") (h "1nr5pxivm8w7cd5hhzra9fw0shdriwfd87r320yibgxi7948cjwi")))

(define-public crate-oauth2-twitter-0.1.0 (c (n "oauth2-twitter") (v "0.1.0") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1b2py85n75w0l518xx2nfnz2q0iyvsbxyc1pgxqg6iwa213vghba")))

(define-public crate-oauth2-twitter-0.1.1 (c (n "oauth2-twitter") (v "0.1.1") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1zbsm0s1mhvryv2v758c4nhf1bp5k5mlqrsyn6d5d8r5ic4rx1rc")))

(define-public crate-oauth2-twitter-0.2.0 (c (n "oauth2-twitter") (v "0.2.0") (d (list (d (n "http-authentication") (r "^0.1") (f (quote ("scheme-basic"))) (k 0)) (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0cjj9kqh11d9j99wzlb5lhx2w37flmjmn8c8i4xhky66jxx2w2yx")))

