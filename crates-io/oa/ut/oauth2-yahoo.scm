(define-module (crates-io oa ut oauth2-yahoo) #:use-module (crates-io))

(define-public crate-oauth2-yahoo-0.0.0 (c (n "oauth2-yahoo") (v "0.0.0") (h "0b8bpq3r4j4yk6py6z3bzcd1y1j18wwjqld5l2z43kqkx3mb4v0k")))

(define-public crate-oauth2-yahoo-0.1.0 (c (n "oauth2-yahoo") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "16cbvcjb1ra2hnbjjxiyl8xmivz7j08qsh0vbd0c44gc2mdw35p3")))

(define-public crate-oauth2-yahoo-0.1.1 (c (n "oauth2-yahoo") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "08sr99hmr0i7ijp932ljxb97aw158fh7ar41kshv8flmd9hmxqhv")))

(define-public crate-oauth2-yahoo-0.2.0 (c (n "oauth2-yahoo") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "113jk0b60xxagh118qnmswcawrryiawbpvip97rgwlk0w757szss")))

