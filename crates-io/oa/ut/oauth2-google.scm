(define-module (crates-io oa ut oauth2-google) #:use-module (crates-io))

(define-public crate-oauth2-google-0.0.0 (c (n "oauth2-google") (v "0.0.0") (h "0zyibf7cgqyzr6185n7inyylxyhgyfhb871wmd0q0r7064df7n47")))

(define-public crate-oauth2-google-0.1.0 (c (n "oauth2-google") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0w4xbrlpsqwf8cwav756kn034vg78igzah2l7plqm3jj6l1y5023")))

(define-public crate-oauth2-google-0.1.1 (c (n "oauth2-google") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "00iw2vikbbb3kldx46f1hckrxf3k7vbwhi4a6y809m3l8ja2f8kc")))

(define-public crate-oauth2-google-0.2.0 (c (n "oauth2-google") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1qww0pq06825f34asvq25l8mrpvdx1587krxpgdhwgznjj796h9r")))

