(define-module (crates-io oa ut oauth2-doorkeeper) #:use-module (crates-io))

(define-public crate-oauth2-doorkeeper-0.0.0 (c (n "oauth2-doorkeeper") (v "0.0.0") (h "02d0jwkiv9wlk70yg3nayyyjqx6ay8vmsva6pskabhsc1q7ila0x")))

(define-public crate-oauth2-doorkeeper-0.1.0 (c (n "oauth2-doorkeeper") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1k5b7rbqgr81bw349hgqzfq2divjqim3x2v5z92smwjnw497pk3d")))

(define-public crate-oauth2-doorkeeper-0.1.1 (c (n "oauth2-doorkeeper") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)))) (h "055vi2yrrgwf36cskcp41q62ks0h01hykpryn9zhglfllzkf91r1")))

(define-public crate-oauth2-doorkeeper-0.2.0 (c (n "oauth2-doorkeeper") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)))) (h "026544zjkvnzbf44iw6rsjkd240621n41d0ln8230hs0wwl6wzbz")))

