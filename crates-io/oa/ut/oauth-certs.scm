(define-module (crates-io oa ut oauth-certs) #:use-module (crates-io))

(define-public crate-oauth-certs-0.1.0 (c (n "oauth-certs") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8") (k 0)) (d (n "jsonwebtoken") (r "^8") (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls-tls" "gzip" "json"))) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 1)))) (h "0qmq1g959j1ygs9jpqkd7kr3kzgzpjpxp7qih3dadb6x54kmd9mq")))

(define-public crate-oauth-certs-0.2.0 (c (n "oauth-certs") (v "0.2.0") (d (list (d (n "jsonwebtoken") (r "^8") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "gzip" "json" "blocking"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0snwxh9qq9w5vm48miha6xyl6pf1wv3fwkcpgk4aqqrvm6480vhr")))

(define-public crate-oauth-certs-0.3.0 (c (n "oauth-certs") (v "0.3.0") (d (list (d (n "jsonwebtoken") (r "^8") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "gzip" "json"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (k 0)))) (h "1r0y556lnkv2fdsfj2iqad0m88v17nch4ks5b6mqihwgmwclndk0")))

(define-public crate-oauth-certs-0.4.0 (c (n "oauth-certs") (v "0.4.0") (d (list (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "gzip" "json"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (k 0)))) (h "0bgqjighh3kwwj4gxz0jc5j55axqzgpbrcg326qvnxjsc55p2s31")))

(define-public crate-oauth-certs-0.5.0 (c (n "oauth-certs") (v "0.5.0") (d (list (d (n "cache_control") (r "^0.2.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9") (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "gzip" "json"))) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (k 0)))) (h "111pdhqz6h8vhgjl8jygzb8az0z4s8fjmhaqj4pd7x5s9nab23bs")))

