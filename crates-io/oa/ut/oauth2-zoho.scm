(define-module (crates-io oa ut oauth2-zoho) #:use-module (crates-io))

(define-public crate-oauth2-zoho-0.1.0 (c (n "oauth2-zoho") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0jngpyfrzcsi35c7i205frd8yqzy1p00p49fvl5kj5vlq0cswp08")))

(define-public crate-oauth2-zoho-0.1.1 (c (n "oauth2-zoho") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "07icqw87hd3mz4wz01m144fg9h2q6i1rk8jlyx4sbfx5v01l9gh1")))

(define-public crate-oauth2-zoho-0.1.2 (c (n "oauth2-zoho") (v "0.1.2") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1np8amhqiz5ag6bwl8b5nl1jbviak7rqc88krfzlp0lqsgmzjkk7")))

(define-public crate-oauth2-zoho-0.2.0 (c (n "oauth2-zoho") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "19rg8ia6hfm76kggfz58gyr188kpf7iy7qkb0w82fv8f2bhvh7f4")))

