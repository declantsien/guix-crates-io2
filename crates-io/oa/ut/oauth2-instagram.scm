(define-module (crates-io oa ut oauth2-instagram) #:use-module (crates-io))

(define-public crate-oauth2-instagram-0.0.0 (c (n "oauth2-instagram") (v "0.0.0") (h "0a769wdf8g5if80g8vahnw256r1v2dkbd9ighcqfap45mr9kbybs")))

(define-public crate-oauth2-instagram-0.1.0 (c (n "oauth2-instagram") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "13jiil0l0k7jm4w9mq97lqn807d9mhf899v6l1i5q5pj501y5w21")))

(define-public crate-oauth2-instagram-0.1.1 (c (n "oauth2-instagram") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0bbszvcg8gmm5ccvqvbym0rksjrlyar1id1kmcy45lx2dcg447j9")))

(define-public crate-oauth2-instagram-0.1.2 (c (n "oauth2-instagram") (v "0.1.2") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0hf1m7djpyw9l43z2m12ia5n4p4aqihvccarlg3gv2c6zrqbxdb6")))

(define-public crate-oauth2-instagram-0.2.0 (c (n "oauth2-instagram") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0g133ilbc0kxj7jbi1rgvdvss103f7j3iwdp4axw4c1kkx9yyja0")))

