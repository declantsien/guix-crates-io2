(define-module (crates-io oa ut oauth2-gitlab) #:use-module (crates-io))

(define-public crate-oauth2-gitlab-0.0.0 (c (n "oauth2-gitlab") (v "0.0.0") (h "1g4x5k55x1shs9iy6w0j2dnx50ahp59iwf09957ia7i8qs39n2kl")))

(define-public crate-oauth2-gitlab-0.1.0 (c (n "oauth2-gitlab") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1czjf11ii8wzvmvvz17g6w9pwp9zc3bw6ylwpfpdqlcjaw889ccw")))

(define-public crate-oauth2-gitlab-0.1.1 (c (n "oauth2-gitlab") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0jxr7ws0d51lnnvzi1nlg59dhwaz513ray5h6d5fci9y5h6mccjj")))

(define-public crate-oauth2-gitlab-0.2.0 (c (n "oauth2-gitlab") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "oauth2-doorkeeper") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "060x0ayhvnnghakr7b5c5550x0rbw9lc076wd2mc97623sq3hm8b")))

