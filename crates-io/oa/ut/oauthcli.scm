(define-module (crates-io oa ut oauthcli) #:use-module (crates-io))

(define-public crate-oauthcli-0.0.1 (c (n "oauthcli") (v "0.0.1") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1d9spr0kg0s1822l2szdqf9h977378q0fdlcv5k6d0nzx756dfms")))

(define-public crate-oauthcli-0.0.2 (c (n "oauthcli") (v "0.0.2") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "199h87b3xs81b99mn7alqk8vh06ac0cla10g03p0hjg4md4da1sc")))

(define-public crate-oauthcli-0.0.3 (c (n "oauthcli") (v "0.0.3") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "158pch6h5q7dgmh64vvayzmhhk06wflj2r4kcmsqmd3d1jahkzmm")))

(define-public crate-oauthcli-0.0.4 (c (n "oauthcli") (v "0.0.4") (d (list (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0vk7w87ya5m1pmr6cswar0hanpcyfnh1c0ic6kjz35p8hw1csz68")))

(define-public crate-oauthcli-0.0.5 (c (n "oauthcli") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "01bq6pnvwmww8sjv0mrh14rdyh572zk20082vkjh8h9qnjjaw1q9")))

(define-public crate-oauthcli-0.0.6 (c (n "oauthcli") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0n4avgfiryvv358aiskr7rd1l97ivyffs6m0mnxd7fqnlpq1hj45")))

(define-public crate-oauthcli-0.0.7 (c (n "oauthcli") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0k8hyr37v874kyp7rx14aynnw6bn4cah7zqzghv1kbbqihwjxx6d")))

(define-public crate-oauthcli-0.1.0 (c (n "oauthcli") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "1q45s038fhsldm6839cm3k0nymc3ysq01a97zrlrlb7xdri8mxil")))

(define-public crate-oauthcli-0.1.1 (c (n "oauthcli") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0w6vzzpvimvrx9bvzz8337ln7zym4zdh9lci78bs4si9l5dnks1s")))

(define-public crate-oauthcli-0.1.2 (c (n "oauthcli") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "151nlsb7yi1cdn0dkbxg0n19i1in4az5zdvnq3y0vp2f21nmnf2k")))

(define-public crate-oauthcli-1.0.0 (c (n "oauthcli") (v "1.0.0") (d (list (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0kv01fd0vd9xbmipd7z8wjcfi8q57znpzw59bnxbjk8b74in623p") (y #t)))

(define-public crate-oauthcli-1.0.1 (c (n "oauthcli") (v "1.0.1") (d (list (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "19y9032hyzgy8y705hhr3rmscl4an2hhslvdp0k2w95b68y7khb3")))

(define-public crate-oauthcli-1.0.2 (c (n "oauthcli") (v "1.0.2") (d (list (d (n "hyper") (r "^0.9") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0hc6f60qhlsfz9gjmxas6js7d8xslas21drmzq34z7zp2d5s8ys2")))

(define-public crate-oauthcli-1.0.3 (c (n "oauthcli") (v "1.0.3") (d (list (d (n "hyper") (r ">= 0.9, < 0.11") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1gmxjf487v66bqwf9prkab2r0cyyz5n46fs7gqlazs4v4va5vwci") (y #t)))

(define-public crate-oauthcli-1.0.4 (c (n "oauthcli") (v "1.0.4") (d (list (d (n "hyper") (r "^0.9") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "13q5jbnkfmh6jp34wmkch1wjq9iv9ljna4yinvaqjrqfl2gfd7gy")))

(define-public crate-oauthcli-1.0.5 (c (n "oauthcli") (v "1.0.5") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.8") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0gajb7pcbhwrjw2br3gnmy1ihg55hm1w6a9wmc6l31ac92msbfhj")))

(define-public crate-oauthcli-1.0.6 (c (n "oauthcli") (v "1.0.6") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.9.4") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "016vpykgvkms4w82mnspqphhmadh5vbb94rk2innbs8i0lglyv5i")))

(define-public crate-oauthcli-1.0.7 (c (n "oauthcli") (v "1.0.7") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (o #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1g5cw9ldlsllffcn8w4yngw4b6qin0x1y5592xzjhgqx1r4c2md9") (y #t)))

(define-public crate-oauthcli-2.0.0-beta-1 (c (n "oauthcli") (v "2.0.0-beta-1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 2)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.11") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1sa6kagip66limyc9w0js1abrn8gzibi8nmvm4pw376nz5dsi3pd")))

(define-public crate-oauthcli-2.0.0-beta-2 (c (n "oauthcli") (v "2.0.0-beta-2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 2)) (d (n "hyper") (r "^0.11") (d #t) (k 2)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.12") (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0hxvnhq2c0pz6k0psmm1173k278p4pgibpl1sf76jpvxfwqzbnin")))

