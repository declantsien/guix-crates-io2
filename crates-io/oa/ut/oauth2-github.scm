(define-module (crates-io oa ut oauth2-github) #:use-module (crates-io))

(define-public crate-oauth2-github-0.0.0 (c (n "oauth2-github") (v "0.0.0") (h "0kxin2qnida50wm8syai50ap1rdyk1fmywchk3rwyz6719shyyzc")))

(define-public crate-oauth2-github-0.1.0 (c (n "oauth2-github") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "17ixiaansgp6zag6kbb703l7jq8wqdjcp8zc9rh8bc26zp28gpip")))

(define-public crate-oauth2-github-0.1.1 (c (n "oauth2-github") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1wb8f06qb2r6236n9idf6nv2lmn8xjwz6rjyhgwc6yva6jvyf97g")))

(define-public crate-oauth2-github-0.2.0 (c (n "oauth2-github") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1sg1sik6qhpb8kblnfwhx03pcpl2v8r0j6slfzs64vjadh0gdzdy")))

