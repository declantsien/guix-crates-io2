(define-module (crates-io oa ut oauth2-okta) #:use-module (crates-io))

(define-public crate-oauth2-okta-0.0.0 (c (n "oauth2-okta") (v "0.0.0") (h "0gb5jj2y92ngssvj2ll6j7bcdpfardjhzq63jchd2sq98982jsy9")))

(define-public crate-oauth2-okta-0.1.0 (c (n "oauth2-okta") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0jpnhw73h3mwmzcvc4ky6af5spkrslfn8pswqwl1fgqbsld04ah2")))

(define-public crate-oauth2-okta-0.1.1 (c (n "oauth2-okta") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1203dfvxz3583hqn7z3c60fzkwraadq2k7vkvzimf2jiyh5bckfh")))

(define-public crate-oauth2-okta-0.2.0 (c (n "oauth2-okta") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0wig82nmhcgicfnqjsigc88agd9bwim3sppkwvk6lhzmj5pkgycz")))

