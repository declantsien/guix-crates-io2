(define-module (crates-io oa ut oauth2-amazon) #:use-module (crates-io))

(define-public crate-oauth2-amazon-0.0.0 (c (n "oauth2-amazon") (v "0.0.0") (h "1fj1d7dmrdfbdr6x6wsk83winkrvacrydz8wplmnag8zj6y5wr8y")))

(define-public crate-oauth2-amazon-0.1.0 (c (n "oauth2-amazon") (v "0.1.0") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "06hy3jchl9fjj6l3sk4cf7a1bcv926cikrrqqrk52cpr9bajy7m5")))

(define-public crate-oauth2-amazon-0.1.1 (c (n "oauth2-amazon") (v "0.1.1") (d (list (d (n "oauth2-client") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "18z7b86470lxcxx8ag4hyynldq96ss1zw1nhmfz0h623akhw9i09")))

(define-public crate-oauth2-amazon-0.2.0 (c (n "oauth2-amazon") (v "0.2.0") (d (list (d (n "oauth2-client") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "039wpnlzc7hif2d31ghzy0vxrgp0l2npfyqmlazy22nqa6gy69k9")))

