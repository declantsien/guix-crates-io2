(define-module (crates-io oa ut oauth1) #:use-module (crates-io))

(define-public crate-oauth1-0.1.0 (c (n "oauth1") (v "0.1.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "13rvgy7j19l73h1vbb43zcf7xf3rfnax4l16zxkrd2zyil5r9r27")))

(define-public crate-oauth1-0.2.0 (c (n "oauth1") (v "0.2.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "08lss1595rhfgkly4mfrlf250wgdp1iwdzjzbx3b4z3jg5v515xn")))

(define-public crate-oauth1-0.3.0 (c (n "oauth1") (v "0.3.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "1pcavyyxwd8m4m93yjkbrs8s459y5vcqracbhdkwhw0r6xry5jdp")))

(define-public crate-oauth1-1.0.0 (c (n "oauth1") (v "1.0.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2.2") (d #t) (k 0)))) (h "1c02hl5dakiwv9046qvpkwnzr5a19g2wkz84xiynp8y84wz6vs3s")))

