(define-module (crates-io oa ts oats) #:use-module (crates-io))

(define-public crate-oats-0.1.0 (c (n "oats") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1q44zbd3yrvc416snflh72qccz40wxg28axx8qmgxmb0gqsmakss")))

