(define-module (crates-io oa ts oats-rs) #:use-module (crates-io))

(define-public crate-oats-rs-0.1.0 (c (n "oats-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "1j330b2pv6ha71xx9abagplr449js7rm31b6ps3ammppvp839qz6")))

(define-public crate-oats-rs-0.1.1 (c (n "oats-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "1yfjd6y9ddkbgnqny5qzrwk598bqij2fj0ly23y90b04y6jc6pml")))

(define-public crate-oats-rs-0.1.2 (c (n "oats-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "16xbcga44dvgdiv1d7zinm88cadvh79gk5p40i54jzxmqiwahjfn")))

(define-public crate-oats-rs-0.2.0 (c (n "oats-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "0wkcsnfhz37vxqwbhcfh42nqcpp5xbj9b3zrff3gi5k59lk0bzfg")))

(define-public crate-oats-rs-0.2.1 (c (n "oats-rs") (v "0.2.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)))) (h "02258ycgkdarsl3r3inkxq5jslv3lb6hjfpc0yxxndpq2bnkrrcc")))

