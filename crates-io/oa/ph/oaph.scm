(define-module (crates-io oa ph oaph) #:use-module (crates-io))

(define-public crate-oaph-0.1.0 (c (n "oaph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0131h5rm2nwnlxfz70sjs4n9psbidzdhpxplki6lsy2wrxjrdvm0")))

(define-public crate-oaph-0.1.1 (c (n "oaph") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0136g5kk22bacsz82n4v6fd8ylvwlfdzb2lyl409f381pj2kb574")))

(define-public crate-oaph-0.2.0 (c (n "oaph") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0h35javifcw27awdjyg65ymimw460n5kbfrbnq4rl8vq4v4p2ms0")))

