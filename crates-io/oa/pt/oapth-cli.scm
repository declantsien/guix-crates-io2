(define-module (crates-io oa pt oapth-cli) #:use-module (crates-io))

(define-public crate-oapth-cli-0.1.0 (c (n "oapth-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("derive" "std"))) (k 0)) (d (n "oapth") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (k 0)))) (h "09pqsz90s9gnriysfk3z54461v73vy50vz4rnysvjr35nzwpfdk1") (f (quote (("sqlite" "oapth/with-rusqlite") ("postgres" "oapth/with-tokio-postgres") ("mysql" "oapth/with-mysql_async") ("mssql" "oapth/with-sqlx-mssql" "oapth/with-sqlx-runtime-tokio-rustls") ("dev-tools" "oapth/dev-tools") ("default"))))))

