(define-module (crates-io oa k_ oak_runtime) #:use-module (crates-io))

(define-public crate-oak_runtime-0.1.0 (c (n "oak_runtime") (v "0.1.0") (h "09826qqz7f74yjr7bz5icg2wsjkd6i88557ks7m8w4jfbah2r132")))

(define-public crate-oak_runtime-0.1.1 (c (n "oak_runtime") (v "0.1.1") (h "0xzcy6bp05a2d87gzhnmhfr15rik3mmnfrbvndsdsd1yi881kwjq")))

(define-public crate-oak_runtime-0.2.0 (c (n "oak_runtime") (v "0.2.0") (h "0s0q6c9q948cdxw3b77p79975d5y7rlz245z6sh1h3cb8pngqraf")))

(define-public crate-oak_runtime-0.3.0 (c (n "oak_runtime") (v "0.3.0") (h "0a2c9cpmbbzsn0bacc3lk1why21dym2lviaifxq7nfsdb6dm6gbm")))

(define-public crate-oak_runtime-0.3.1 (c (n "oak_runtime") (v "0.3.1") (h "142yz698579y7krcziqbbir1221wf9xciwql5r2j3mwawc67p87j")))

(define-public crate-oak_runtime-0.3.2 (c (n "oak_runtime") (v "0.3.2") (h "1ik5nzkkz5rb62bn0iiidf0qq85mw22ckx04v7df9gcd16g7ws8s")))

(define-public crate-oak_runtime-0.3.3 (c (n "oak_runtime") (v "0.3.3") (h "07ihpx14jfyqg870yqfw4irm8vxlyi6f59ji9jypywsbq58dacd6")))

(define-public crate-oak_runtime-0.3.4 (c (n "oak_runtime") (v "0.3.4") (h "0r30j496qzgs0ss4wp0ff4ps4hlmq3rnsnych9yzqia7lal41fbz")))

(define-public crate-oak_runtime-0.3.5 (c (n "oak_runtime") (v "0.3.5") (h "0fl6z9wp09si0p0xswm7mjivl5blhn96by6mjj2wyidkmszpmgi7")))

(define-public crate-oak_runtime-0.3.6 (c (n "oak_runtime") (v "0.3.6") (h "0fjwqmj1ygmcfkzg6jgj0dff8az80nifxalmrxdv7a74jyccydrw")))

(define-public crate-oak_runtime-0.3.7 (c (n "oak_runtime") (v "0.3.7") (h "0kncijqgvflvj5pdwrwqrbkkw15saagqj4zfl69xn4465vzib2n0")))

(define-public crate-oak_runtime-0.4.0 (c (n "oak_runtime") (v "0.4.0") (h "03czsd8lkgywpsjvr5h5kmarch7h2np0q6w8svq09310d7kx25pr")))

(define-public crate-oak_runtime-0.4.1 (c (n "oak_runtime") (v "0.4.1") (h "0qcfppdmkkdni4jgzcvcc024n4bl5nmrqkkmzdag0ibyr5vpficy")))

(define-public crate-oak_runtime-0.4.2 (c (n "oak_runtime") (v "0.4.2") (h "0z50yr5ji5gzj76jv14496ak7lbcgxdg2b9d6hm63r2s4nm7idw8")))

(define-public crate-oak_runtime-0.4.3 (c (n "oak_runtime") (v "0.4.3") (h "116j1fcghlarxbl9ahwham18rgc16apy8l7vfd72xkzpw3lj1cdi")))

(define-public crate-oak_runtime-0.5.0 (c (n "oak_runtime") (v "0.5.0") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)))) (h "10x17ng355fyz4nshic26j892fmmxgr9jab3wf0s37digqqkxfxh")))

(define-public crate-oak_runtime-0.5.1 (c (n "oak_runtime") (v "0.5.1") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)))) (h "1japp5l3zrxng163nyaqjc7i61hkwsvfvdshgy8690x5mh0jawk4")))

(define-public crate-oak_runtime-0.5.2 (c (n "oak_runtime") (v "0.5.2") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)))) (h "1l5j0b0lxbmksn19zhckh07csiqidzl51frgydlmjg1b7a35x41v")))

(define-public crate-oak_runtime-0.5.3 (c (n "oak_runtime") (v "0.5.3") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 2)))) (h "1pdn6vzhlj8g7ssg39lrgaylmvldxcqm4my5s04lmm07wa3ckxfc")))

(define-public crate-oak_runtime-0.5.4 (c (n "oak_runtime") (v "0.5.4") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 2)))) (h "07gq01nzy0vmky7iaqy81fj6myccl3hkmf6s5d70214rxfv6m0pr")))

(define-public crate-oak_runtime-0.5.5 (c (n "oak_runtime") (v "0.5.5") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 2)))) (h "0q5xk88yxiybq78zv4317f58k7dqa62zym5my9kfvifnfr0546j2")))

(define-public crate-oak_runtime-0.6.0 (c (n "oak_runtime") (v "0.6.0") (d (list (d (n "syntex_pos") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 2)))) (h "1xlc53jaazqkf05si5qa1fyqwg7g4ad5kz7jlbppg0a9hijkljg6")))

