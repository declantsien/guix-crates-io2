(define-module (crates-io uq ue uquery) #:use-module (crates-io))

(define-public crate-uquery-0.1.0 (c (n "uquery") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0vg05cy55gxibap81a8g30jqpvd5s5xpq8n9y7a4p4l4fxfhms9g")))

(define-public crate-uquery-0.1.1 (c (n "uquery") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0b6nagmnja4hi21ppc6znrmwhmb028icddnkx40mrk9r0cd8amvd")))

(define-public crate-uquery-0.1.2 (c (n "uquery") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "17046lbrkysz6ff22lswssf9wdp7nb9iv9a0y22lpg4vndxyyws3")))

(define-public crate-uquery-0.1.3 (c (n "uquery") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1hrli8w9vvbbkgdn70d472v2996f6vjkjy830k96dmda1rxax7dq")))

(define-public crate-uquery-0.2.0 (c (n "uquery") (v "0.2.0") (d (list (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0y04xmzki84dm8i464kiak4ggsfppgsmrjvk8gbzx3bsniyskvks")))

(define-public crate-uquery-0.2.1 (c (n "uquery") (v "0.2.1") (d (list (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1v894l3ff31bqi35jrsbys4zcgzy0ncxv80y40ii9ydmkqbs2rjr")))

(define-public crate-uquery-0.2.2 (c (n "uquery") (v "0.2.2") (d (list (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "0kxbgwvzynapp18620836s01hng1pqxkgqgwhyg4qsdmg06hl1yd")))

