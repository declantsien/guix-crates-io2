(define-module (crates-io bf cc bfcc) #:use-module (crates-io))

(define-public crate-bfcc-0.1.0 (c (n "bfcc") (v "0.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_ascii_tree") (r "^0.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (f (quote ("vendored"))) (d #t) (k 0)))) (h "1jn8f9amf3mq70x7zdj9cvkyr9gx80wmmhhwx93r3mxpkzfghiqj")))

(define-public crate-bfcc-0.1.1 (c (n "bfcc") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_ascii_tree") (r "^0.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "zmq") (r "^0.9") (f (quote ("vendored"))) (d #t) (k 0)))) (h "1dvcck2w2hhsmklgj51f7sx69dsds36frnd5pd2kgxbzp0111b7d")))

