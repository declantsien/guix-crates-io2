(define-module (crates-io bf li bflib) #:use-module (crates-io))

(define-public crate-bflib-0.1.0 (c (n "bflib") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15c5g28lmgj986785l5lyjvdpkh54yhv95ni07l0r4xr7a6c5kx5")))

(define-public crate-bflib-0.1.1 (c (n "bflib") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y0c7xs9abngn7h04w1s11jf55i1s24iyjncslsxnxavpbskdjw0")))

(define-public crate-bflib-0.1.2 (c (n "bflib") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ihkz5xg6sakl71jhlw5xpp57c5zxvzvjnl4any2bsaap924w8mx")))

(define-public crate-bflib-0.1.3 (c (n "bflib") (v "0.1.3") (d (list (d (n "bflib_proc_macro") (r "^0.1.3") (d #t) (k 0)))) (h "1hhdv5ryhiwrg2w6fj5achpls79p3fhf6xkbc413i399gjv5zizc")))

(define-public crate-bflib-0.1.4 (c (n "bflib") (v "0.1.4") (d (list (d (n "bflib_proc_macro") (r "^0.1.3") (d #t) (k 0)))) (h "04h0gm6ys0lv2i3wy3a2sax67kxz48fcdb5y1bwn0jjhv0iaglni")))

(define-public crate-bflib-0.1.5 (c (n "bflib") (v "0.1.5") (d (list (d (n "bflib_proc_macro") (r "^0.1.5") (d #t) (k 0)))) (h "1m1gh1a0cv4yx0nlmdaznvbwmphdq8sk95q1gh40wxrnvnp8a5ji")))

(define-public crate-bflib-0.1.6 (c (n "bflib") (v "0.1.6") (d (list (d (n "bflib_proc_macro") (r "^0.1.6") (d #t) (k 0)))) (h "180i29rsy0nwdgs91r3ckddd8yn4nxzimyragfrc83kw7f55ffqa")))

(define-public crate-bflib-0.1.7 (c (n "bflib") (v "0.1.7") (d (list (d (n "bflib_proc_macro") (r "^0.1.7") (d #t) (k 0)))) (h "0qvawnlr7zm6j10sxl08zrph4lc4kc4z8h3jm00gkvj9jsrc08c6")))

(define-public crate-bflib-0.1.8 (c (n "bflib") (v "0.1.8") (d (list (d (n "bflib_proc_macro") (r "^0.1.8") (d #t) (k 0)))) (h "1lgj016wa82kp02mqv021xzqfczvkdv47j81k83s1hcyhi6nahfm") (y #t)))

(define-public crate-bflib-0.1.9 (c (n "bflib") (v "0.1.9") (d (list (d (n "bflib_proc_macro") (r "^0.1.9") (d #t) (k 0)))) (h "0avdjlzz5j72gkf80m5yxhpibnl0zvixkp6p3y11ziw7fjwq280d")))

