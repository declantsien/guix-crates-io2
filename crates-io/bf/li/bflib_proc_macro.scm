(define-module (crates-io bf li bflib_proc_macro) #:use-module (crates-io))

(define-public crate-bflib_proc_macro-0.1.3 (c (n "bflib_proc_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yygpbg07j85b81fsg61bq6zzxjghkh5vw09bbcv1d6kcq2l2776")))

(define-public crate-bflib_proc_macro-0.1.4 (c (n "bflib_proc_macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07dm7651rladjxqhym9h09jb4167qxvbqb24prph78hrrz9xxsn3")))

(define-public crate-bflib_proc_macro-0.1.5 (c (n "bflib_proc_macro") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03jhzpnlpi9x967wylg4339b1nsan77jp1zp6nx9va9kl00gwjci")))

(define-public crate-bflib_proc_macro-0.1.6 (c (n "bflib_proc_macro") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12adbiwis9syrrwbl08zlf8qjs39g5skff1mdlkszz84izm9gyy7")))

(define-public crate-bflib_proc_macro-0.1.7 (c (n "bflib_proc_macro") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cd9jvanddixd81hdc8drick8c6xfdlcqzqwrjfvyyn99lpgnprh")))

(define-public crate-bflib_proc_macro-0.1.8 (c (n "bflib_proc_macro") (v "0.1.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dnm3zcfc5c41xkdsx1fc2yl6a83rpvx3y8nm3gcw674qpfrh06i")))

(define-public crate-bflib_proc_macro-0.1.9 (c (n "bflib_proc_macro") (v "0.1.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kvi1sl85966m5w12s0pb1dhgd8yianl51fzqxikdllsc2hx9zhq")))

