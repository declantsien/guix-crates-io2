(define-module (crates-io bf gs bfgs) #:use-module (crates-io))

(define-public crate-bfgs-0.1.0 (c (n "bfgs") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "00pzqi0m28f6x4dsqwj41lfcxha0xha73bayf2qkbwwdxd7f73r8")))

