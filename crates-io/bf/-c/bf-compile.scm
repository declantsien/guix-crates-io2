(define-module (crates-io bf -c bf-compile) #:use-module (crates-io))

(define-public crate-bf-compile-0.1.0 (c (n "bf-compile") (v "0.1.0") (h "19ykwkvs6fha6c0lhydl77gi6lnc6mfi011jcl9hx5znjayg1vgl")))

(define-public crate-bf-compile-0.1.1 (c (n "bf-compile") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0805pxypws8alziaxkiwp9na6pvn3gqhy3n3lsh84w1mbllqm8wx")))

