(define-module (crates-io bf fl bfflib) #:use-module (crates-io))

(define-public crate-bfflib-0.6.0 (c (n "bfflib") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("now"))) (k 0)) (d (n "file-mode") (r "^0.1.2") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)))) (h "1nk3zamsc7i7rw8yla51z46w69vcz6zdl392dflynrdnryd7fvzn")))

(define-public crate-bfflib-0.7.0 (c (n "bfflib") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("now"))) (k 0)) (d (n "file-mode") (r "^0.1.2") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1y0n1d501j3rcfsgmn0bcaxmj2vsi0w4r11rj2487drxazjq31bb")))

