(define-module (crates-io bf lo bflog-cli) #:use-module (crates-io))

(define-public crate-bflog-cli-0.1.0 (c (n "bflog-cli") (v "0.1.0") (d (list (d (n "bflog") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "2.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1qgfqs0rza9hl61s62spz3b61jlh215vf6bxshap53pza3qksr63")))

(define-public crate-bflog-cli-0.1.1 (c (n "bflog-cli") (v "0.1.1") (d (list (d (n "bflog") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "2.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1dv75ha3cy1s4fqxfyyi0dryxjmvb47c1m68ly1nkqp1yhdrqpsb")))

(define-public crate-bflog-cli-0.1.2 (c (n "bflog-cli") (v "0.1.2") (d (list (d (n "bflog") (r "0.1.*") (d #t) (k 0)) (d (n "clap") (r "2.3.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0d990m8g2bk4x6m9kpqr0lppkid4h083d6mqv7x249rlvj3l567a")))

(define-public crate-bflog-cli-0.2.0 (c (n "bflog-cli") (v "0.2.0") (d (list (d (n "bflog") (r "0.2.*") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0pwandb4qkc9mnq2cb6bnlrbd46qmgp3cs79zczqd22hn85qf912")))

(define-public crate-bflog-cli-0.3.0 (c (n "bflog-cli") (v "0.3.0") (d (list (d (n "bflog") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "013p2yzbpmfdc2v7339sh4nwh8y0v2v3myc0jc03mviyn9brgli1")))

(define-public crate-bflog-cli-0.3.1 (c (n "bflog-cli") (v "0.3.1") (d (list (d (n "bflog") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "2.32.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1rb773lys78lz9sc0wvv3bbm5y3d098iq10v7ab66fisf76sshmd")))

