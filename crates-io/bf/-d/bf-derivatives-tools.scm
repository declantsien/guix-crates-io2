(define-module (crates-io bf -d bf-derivatives-tools) #:use-module (crates-io))

(define-public crate-bf-derivatives-tools-0.3.0 (c (n "bf-derivatives-tools") (v "0.3.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1s5xc09xij0ifzpr4gqj19mc8j12dh72cixlv3yr29kk19lcp6y8")))

(define-public crate-bf-derivatives-tools-0.3.1 (c (n "bf-derivatives-tools") (v "0.3.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1f7lxg23r4siibq61qgf2qvb675cjqkngmz0h7xxpc98fyn6000x")))

(define-public crate-bf-derivatives-tools-0.3.2 (c (n "bf-derivatives-tools") (v "0.3.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "04sp46ay5s63lz53zyp357f9kl88djb50w802mmkwnb1syy3f0hz")))

(define-public crate-bf-derivatives-tools-0.4.0 (c (n "bf-derivatives-tools") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1rm6xyry6ah7z33bkqw49p06l8zs8lvc9d05vf8j1rvliybffq8r")))

