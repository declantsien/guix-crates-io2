(define-module (crates-io bf ki bfkit) #:use-module (crates-io))

(define-public crate-bfkit-0.1.2 (c (n "bfkit") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0zy2in63nz3iy82qil6xxpwjqpqsb73l5r1qqf5x0bwl8bl0im9r")))

(define-public crate-bfkit-0.1.3 (c (n "bfkit") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0x3s75yd3h3npf6vdrb1ig2qvgmz9ynwrin73bf1wb1af8d4inqg")))

(define-public crate-bfkit-0.1.4 (c (n "bfkit") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0g5v5x3j9icwmwm4n834yww4s3x5cbarv4zmyrsjziyqqp50bi55")))

