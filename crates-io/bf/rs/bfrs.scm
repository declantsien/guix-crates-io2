(define-module (crates-io bf rs bfrs) #:use-module (crates-io))

(define-public crate-bfrs-0.0.1 (c (n "bfrs") (v "0.0.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "winresource") (r "^0.1") (d #t) (k 1)))) (h "013fvr3a732rbhnk5vbdai3zh06nrxvydxibp3aq69wdli09h7p7")))

(define-public crate-bfrs-0.0.2 (c (n "bfrs") (v "0.0.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "winresource") (r "^0.1") (d #t) (k 1)))) (h "1mi29707z6qgwzxwpc3faby3bz12nhrj75w1252kq110pin1djl2")))

(define-public crate-bfrs-0.0.3 (c (n "bfrs") (v "0.0.3") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "winresource") (r "^0.1") (d #t) (k 1)))) (h "1hj5dks2ilk0xrylhbg77yx3fqs2n7q88yzgn5x5vg6b6386w12i")))

