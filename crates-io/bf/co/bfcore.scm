(define-module (crates-io bf co bfcore) #:use-module (crates-io))

(define-public crate-bfcore-0.1.0 (c (n "bfcore") (v "0.1.0") (h "0zngi7fx0chdm72qi6nmr0fby6n0k8z24b1zrxsnpf32hyrg1rsm")))

(define-public crate-bfcore-0.1.1 (c (n "bfcore") (v "0.1.1") (h "014nszbk5hvi8ianv96bqmngi1ymj09irsijdrry0lhpbncimsg1")))

(define-public crate-bfcore-0.1.2 (c (n "bfcore") (v "0.1.2") (h "0bir5rzqm121if3lq8fjdwp3363zih0djdh0mx1x0fgdym4rdbc2")))

(define-public crate-bfcore-0.2.0 (c (n "bfcore") (v "0.2.0") (h "0d828gqj8f9jaldjnzl9s6x8rbp5cffxjyia5pnq86xbbngkj7vd")))

