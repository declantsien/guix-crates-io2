(define-module (crates-io bf co bfcomp) #:use-module (crates-io))

(define-public crate-bfcomp-0.1.0 (c (n "bfcomp") (v "0.1.0") (d (list (d (n "bfcore") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0s720pxmbsc3wjnn2rz8rqnmjjn790qdmx506kj4d6c2z67ngl7d")))

(define-public crate-bfcomp-0.1.1 (c (n "bfcomp") (v "0.1.1") (d (list (d (n "bfcore") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "173iiia0y2h3dx7ic048gd41lzjij85cc2sr2d02ncc8ca0aijfh")))

(define-public crate-bfcomp-0.1.2 (c (n "bfcomp") (v "0.1.2") (d (list (d (n "bfcore") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1v2jmq2bsfrbd5kff1g1qcxfwffxw9mfp8smnhajza9fpmn0qq8h")))

