(define-module (crates-io bf t- bft-core) #:use-module (crates-io))

(define-public crate-bft-core-0.1.0 (c (n "bft-core") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 2)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0xr5gc2kyv4h1lbniprmmcr06an9wcq8vj7jz8zmpsc0ij7682gg") (f (quote (("default") ("async_verify"))))))

