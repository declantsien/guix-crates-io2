(define-module (crates-io bf -l bf-lib) #:use-module (crates-io))

(define-public crate-bf-lib-0.1.0 (c (n "bf-lib") (v "0.1.0") (h "0x7wb2qj8ji1c7j3gsqsjni0nl1y1552f0gx415lsm1xdfbwl9q0")))

(define-public crate-bf-lib-0.1.1 (c (n "bf-lib") (v "0.1.1") (h "03m3y6v93d8r2d610979y5i14zjd8q17wcrb6328iib8mip1915s")))

(define-public crate-bf-lib-0.2.0 (c (n "bf-lib") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0b8851c6mqq7x8nfy25pkm7bnqh1ha402bm2z2fn22pc7cjvh92m")))

(define-public crate-bf-lib-0.2.1 (c (n "bf-lib") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0b5za2kdx9q36wjzjnsw44iyd6jqmif4phphf3rvq67iqp7n8mmm")))

