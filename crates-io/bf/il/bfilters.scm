(define-module (crates-io bf il bfilters) #:use-module (crates-io))

(define-public crate-bfilters-0.1.2 (c (n "bfilters") (v "0.1.2") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)))) (h "0p38yn5xckca7jkgqy19rzz04ar1k853lbss6cayhl7q83v7nq7c")))

(define-public crate-bfilters-0.1.3 (c (n "bfilters") (v "0.1.3") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)))) (h "0m2684475x3ld5nr93xj1i73np27abmlyyabvzn2dny256lrrc74")))

(define-public crate-bfilters-0.1.4 (c (n "bfilters") (v "0.1.4") (d (list (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1m04y2d8k0hdznpwwlkkszm7kbrv17jccx6ad5i3dnzyamcqdjk8")))

(define-public crate-bfilters-0.2.0 (c (n "bfilters") (v "0.2.0") (d (list (d (n "bitarray-naive") (r "^0.1.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1625h10n6s38xg5my28n421ycyqjypgxpmy9vsyw6xndiajirlb2")))

