(define-module (crates-io bf #{-i}# bf-impl) #:use-module (crates-io))

(define-public crate-bf-impl-1.0.0 (c (n "bf-impl") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10b7m8rrljnxrr6war6kdqwlnzlbs5dxq8vnxlgq5slkz1cdcxbs") (f (quote (("use_std") ("default" "use_std"))))))

