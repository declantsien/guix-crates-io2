(define-module (crates-io bf -b bf-bot) #:use-module (crates-io))

(define-public crate-bf-bot-0.1.0 (c (n "bf-bot") (v "0.1.0") (d (list (d (n "bf-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.9.0") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1bkqavg8r9nw82fsj30c3cz3l9wv71i5ppac7dr77vbbk92px42y")))

(define-public crate-bf-bot-0.2.0 (c (n "bf-bot") (v "0.2.0") (d (list (d (n "bf-lib") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.9.0") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xsbnbjfwhqffb0cp8cg4k72r50h5ma8rxrlcfr31gwmaqrl516c")))

