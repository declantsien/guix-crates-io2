(define-module (crates-io bf om bfom) #:use-module (crates-io))

(define-public crate-bfom-0.1.0 (c (n "bfom") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vb1r2jblik6bs607z9fi4wl2fdpxlp11f0y6kkmf5hysyjrggx8")))

(define-public crate-bfom-0.1.1 (c (n "bfom") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jj7s5ssdi55zq7k0fp5v6d4wkyqk865wypvxh3dd8w257fimixv")))

(define-public crate-bfom-0.1.2 (c (n "bfom") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1mn5lp5brqxdlg11xgckhbpa3p3ybf3fqbay8drq70nxjrlxz20c")))

(define-public crate-bfom-0.1.13 (c (n "bfom") (v "0.1.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1cbw170k8n4wsagr5ly75vdqvfzpglp44cl90n0qvlls5i35vkk0")))

(define-public crate-bfom-0.1.14 (c (n "bfom") (v "0.1.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hk35kkn1b2g328rswhi35wjzxwql0sw527mj36fzj7nby9g3cbx")))

(define-public crate-bfom-0.1.17 (c (n "bfom") (v "0.1.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nvv2i2sl1n7jh0arb3qivqq7bivwcbgldb3140gnjxs5v0smkdy")))

(define-public crate-bfom-0.1.18 (c (n "bfom") (v "0.1.18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hfmdw9wzqcrq06clizlhsfc7sa1g3dlzxbl8cbfmgi3wrjlhnd7")))

(define-public crate-bfom-0.1.19 (c (n "bfom") (v "0.1.19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0zd8a31y4383iailmb0df53b75qjfgfwd5cw2skj80y0dh7znvrh")))

(define-public crate-bfom-0.1.20 (c (n "bfom") (v "0.1.20") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05ck8f0p8ggi0aky7943i5cm3021yh12v4swf0s3k7djrhjvpvf2")))

(define-public crate-bfom-0.1.21 (c (n "bfom") (v "0.1.21") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wrbyrc4awbq83c5dzm2q1hs5wl4wpwjhlzlddfqlnv5ax7bdig0")))

(define-public crate-bfom-0.1.22 (c (n "bfom") (v "0.1.22") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1s0aw2w3y8b8m2nxxrm99saz943id4gsvnh72ih0wivwgpyf9z81")))

(define-public crate-bfom-0.1.26 (c (n "bfom") (v "0.1.26") (d (list (d (n "bfom-lib") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1w14dg4lvvlzss1ldlpmyb4hxsqpsi51i6s38jghcvhbmccfnna4")))

(define-public crate-bfom-0.1.27 (c (n "bfom") (v "0.1.27") (d (list (d (n "bfom-lib") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0kcl1g9n6kndzv36rp3j81dcwjzqq97pyg0m2mncl9bl55maln45")))

(define-public crate-bfom-0.1.28 (c (n "bfom") (v "0.1.28") (d (list (d (n "bfom-lib") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ir9qq476ad362287fl3vx4v4s646rfwf0vwavykf72zrnmdhd58")))

(define-public crate-bfom-0.1.33 (c (n "bfom") (v "0.1.33") (d (list (d (n "bfom-lib") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ycdqdy1fk9fddwdpqwzywsa2qbq1z6rlfz43ayv6pcbw0iqiy5m")))

(define-public crate-bfom-0.1.34 (c (n "bfom") (v "0.1.34") (d (list (d (n "bfom-lib") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0la3cdxg68sxpwlli40aqj184wz4a5c5x89yv8j8n90rbn7nmj7z")))

(define-public crate-bfom-0.1.35 (c (n "bfom") (v "0.1.35") (d (list (d (n "bfom-lib") (r "^0.1.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "01d5nv26239a3kk45c5wkric3rb5npd608b9fmb3h42pqlp0ifpj")))

(define-public crate-bfom-0.1.36 (c (n "bfom") (v "0.1.36") (d (list (d (n "bfom-lib") (r "^0.1.37") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "053ykk49xlpfnf6a16v666s6jpsny645xrigbkbvacy13bdybwwx")))

(define-public crate-bfom-0.1.37 (c (n "bfom") (v "0.1.37") (d (list (d (n "bfom-lib") (r "^0.1.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hxrmb9hkcj2bgf6yws985l0kzw1rzv4d02kgc4y6jbw3cacqnb9")))

(define-public crate-bfom-0.1.38 (c (n "bfom") (v "0.1.38") (d (list (d (n "bfom-lib") (r "^0.1.44") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bkyxkd6d7qvrinmw10pzxjpiw8ybyzbjm2gjsck73zsxzjap3gd")))

(define-public crate-bfom-0.1.39 (c (n "bfom") (v "0.1.39") (d (list (d (n "bfom-lib") (r "^0.1.45") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lzgcp13z0488c0j86413rvhqypqxb5bbrqnrc0nqk40zyl62y1v")))

(define-public crate-bfom-0.1.40 (c (n "bfom") (v "0.1.40") (d (list (d (n "bfom-lib") (r "^0.1.47") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p3y6lxipbii27p9wvdn8f19rdjp0c3fs6p401j15fbx9g3b1i05")))

(define-public crate-bfom-0.1.41 (c (n "bfom") (v "0.1.41") (d (list (d (n "bfom-lib") (r "^0.1.48") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "19ayq39jw2zkqd2zwnvrgd32073xjcf9csrjqbzlmkwgg30m2276")))

(define-public crate-bfom-0.1.42 (c (n "bfom") (v "0.1.42") (d (list (d (n "bfom-lib") (r "^0.1.48") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vjznrqymzbh220w905w3jafvqz059z3hbcszpmjj3nm91k91gab")))

(define-public crate-bfom-0.1.43 (c (n "bfom") (v "0.1.43") (d (list (d (n "bfom-lib") (r "^0.1.48") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "13fpi5rqfd8ca4mz8yabgcpk2vazc1c2y2s5w63m7danzqiw5l5f")))

