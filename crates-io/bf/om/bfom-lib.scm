(define-module (crates-io bf om bfom-lib) #:use-module (crates-io))

(define-public crate-bfom-lib-0.1.24 (c (n "bfom-lib") (v "0.1.24") (h "11bdh5kxnr1d64ygksp1dmcklsvr42x6nfpc069igkdy5r6k3i1g")))

(define-public crate-bfom-lib-0.1.25 (c (n "bfom-lib") (v "0.1.25") (h "11k15m7my2z5k8k4hgdb2nvj6fmwn35lac32da95lyv0d25bflz6")))

(define-public crate-bfom-lib-0.1.26 (c (n "bfom-lib") (v "0.1.26") (h "08scs3wgp2gks49c6hvd6icpjl2bnl1pc2cxdxivyvb7df53h11x")))

(define-public crate-bfom-lib-0.1.27 (c (n "bfom-lib") (v "0.1.27") (h "1rdn76yabl542zwpy5n6jqq8rvb9jz00574bvxhqzf2476jldh3a")))

(define-public crate-bfom-lib-0.1.28 (c (n "bfom-lib") (v "0.1.28") (h "0s5351r8mrfvmcfsp32rv5cinhvvpdaf02psv21nwns5jin53sxi")))

(define-public crate-bfom-lib-0.1.31 (c (n "bfom-lib") (v "0.1.31") (h "1f8ppzp3x9cxgy5q68j21k67nyfrn96f90wn3q8jgln1np1c9rwn") (y #t)))

(define-public crate-bfom-lib-0.1.33 (c (n "bfom-lib") (v "0.1.33") (h "1k38afg52m512ramzxq0pjhgw9g4m0izxip5aicg20g9kr40k4yn")))

(define-public crate-bfom-lib-0.1.34 (c (n "bfom-lib") (v "0.1.34") (h "1a2nibmvh69djq0ag2wfb5dpn9a2lwr696f8ppaz6n5rnflpgv7i")))

(define-public crate-bfom-lib-0.1.35 (c (n "bfom-lib") (v "0.1.35") (h "15d15kaqzkirw3wcfd5pghnx4yv9lw3np42y52784av92kg9mbhj")))

(define-public crate-bfom-lib-0.1.36 (c (n "bfom-lib") (v "0.1.36") (h "01gjlqaislj7jl2n6lc6x685gy995mdmskyx90pa9fagkwnc9igf")))

(define-public crate-bfom-lib-0.1.37 (c (n "bfom-lib") (v "0.1.37") (h "1sm40w6n7rpzwyndx72cccdlg835vvkjjika853i001pnm545d1b")))

(define-public crate-bfom-lib-0.1.38 (c (n "bfom-lib") (v "0.1.38") (h "19b4p0nscq4c0w8kbmlnjvz45z8ifx3mz6j9rmnpnsmrdfaz1qfi")))

(define-public crate-bfom-lib-0.1.39 (c (n "bfom-lib") (v "0.1.39") (h "1dq0irxh538y4lz0mmz31mz2kvmmvsbm2966dkhlg9jwqs5q41ww")))

(define-public crate-bfom-lib-0.1.40 (c (n "bfom-lib") (v "0.1.40") (h "036cj7f8wcmdbx8zvq8r3i3c6wm13rnav3vdrxspaisqlr9ny8zp")))

(define-public crate-bfom-lib-0.1.41 (c (n "bfom-lib") (v "0.1.41") (h "1gplb0hpw5irvac0vxjsd4pfn3vcsalb4f1za7px3hfaldqyx1q9")))

(define-public crate-bfom-lib-0.1.42 (c (n "bfom-lib") (v "0.1.42") (h "05fjzlzsxpfsr0m6xyyrv0js3zrcbva10zps0psnccniygblg3si")))

(define-public crate-bfom-lib-0.1.43 (c (n "bfom-lib") (v "0.1.43") (h "18xdfb3xksjqc02a2h1n95xyfn13ys6nx7lm2k53f6w6zy3qa78d")))

(define-public crate-bfom-lib-0.1.44 (c (n "bfom-lib") (v "0.1.44") (h "1kms1yq0qwmhah48qsil9fjfbkdiik29x76zcainq7zjw2q6zcf1")))

(define-public crate-bfom-lib-0.1.45 (c (n "bfom-lib") (v "0.1.45") (h "0cs1k533k5ck2ya6rnh5nx5f247y9ifsaryb8307dssq684vcq0f")))

(define-public crate-bfom-lib-0.1.46 (c (n "bfom-lib") (v "0.1.46") (h "0ffbr1fglbwicvpxdbj8zbly7kvh0hrq3qpvjzwppfign02pxhxc")))

(define-public crate-bfom-lib-0.1.47 (c (n "bfom-lib") (v "0.1.47") (h "1p1644ga5khiiqghr4gl6galsxspnra0f04h7g4bkq1kfpahdrvc")))

(define-public crate-bfom-lib-0.1.48 (c (n "bfom-lib") (v "0.1.48") (h "0yy5jzhicmh1b9w9m24nj1a8vd2jq2g3y3f045ixp1fy73sfpdwm")))

(define-public crate-bfom-lib-0.1.49 (c (n "bfom-lib") (v "0.1.49") (h "1abhiiyxjz7mgyd2bdq5l5vgny8349ls3lz1dqlnmrkj6k4jxrvr")))

(define-public crate-bfom-lib-0.1.50 (c (n "bfom-lib") (v "0.1.50") (h "0ijgh3fnxgrknj2a212x4w5ns074q8g4rx7w79nydqsn1ggka2dg")))

