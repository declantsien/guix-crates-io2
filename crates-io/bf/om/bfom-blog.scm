(define-module (crates-io bf om bfom-blog) #:use-module (crates-io))

(define-public crate-bfom-blog-0.1.0 (c (n "bfom-blog") (v "0.1.0") (d (list (d (n "bfom-lib") (r "^0.1.36") (d #t) (k 0)))) (h "183yrf3jznkggc6ykcfqzl2jrivbya660pwkhwkj8x2lwmbad8pn")))

(define-public crate-bfom-blog-0.1.2 (c (n "bfom-blog") (v "0.1.2") (d (list (d (n "bfom-lib") (r "^0.1.37") (d #t) (k 0)))) (h "0wrz8mg2v52iid24vzflhjn2zjxmmavvsgy6j6q2gy4yjwp7nn4c")))

(define-public crate-bfom-blog-0.1.3 (c (n "bfom-blog") (v "0.1.3") (d (list (d (n "bfom-lib") (r "^0.1.38") (d #t) (k 0)))) (h "021aqc5q77mypkh1qvmlnyhgky2lj1036lh352cmhjhskimcnm6l")))

(define-public crate-bfom-blog-0.1.4 (c (n "bfom-blog") (v "0.1.4") (d (list (d (n "bfom-lib") (r "^0.1.40") (d #t) (k 0)))) (h "1i6xv9db1010k3az3bwn4hdahaqbqd5h9pcvdl47xpjisdln6d2x")))

(define-public crate-bfom-blog-0.1.5 (c (n "bfom-blog") (v "0.1.5") (d (list (d (n "bfom-lib") (r "^0.1.41") (d #t) (k 0)))) (h "0wk19w5w6dyjbwhanvcbqay8dypd4ib21d1gdmhgb5d8hn1va8i3")))

(define-public crate-bfom-blog-0.1.6 (c (n "bfom-blog") (v "0.1.6") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "0dfxdwrp3wny4lvj0966bj5rlbr15vfpdwp0c6j1gkxfn8qnigbx")))

(define-public crate-bfom-blog-0.1.7 (c (n "bfom-blog") (v "0.1.7") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "0jn5asrpskji0y6kbnj5cy6zmdcp6ji2bxmb1r336m4p348hq74s")))

(define-public crate-bfom-blog-0.1.8 (c (n "bfom-blog") (v "0.1.8") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "0vh610r2w7y0zvnln8cqq8xfybi7i0qn99d7xkh5m8mj985zmca9")))

(define-public crate-bfom-blog-0.1.9 (c (n "bfom-blog") (v "0.1.9") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "0y1pmf7p9g88c2ygqiyz8dr4vl5xnm2k41w32qwqs0i6hjz3vyi6")))

(define-public crate-bfom-blog-0.1.10 (c (n "bfom-blog") (v "0.1.10") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "1lz12i9c0zninn56f0xbdd0j1mw26dxgkhs125ws0ldsrxqkki41")))

(define-public crate-bfom-blog-0.1.11 (c (n "bfom-blog") (v "0.1.11") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "1s59wplz695i3kvm65km3vssvh0r45syk23kyha5scb5dcw109qf")))

(define-public crate-bfom-blog-0.1.12 (c (n "bfom-blog") (v "0.1.12") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "1s3szkyldabxc68wq41v4pkf5ahnsy5q2hz5yb8z844c2496n9kl")))

(define-public crate-bfom-blog-0.1.13 (c (n "bfom-blog") (v "0.1.13") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "1w060945ycjqirfyas122bpwp7jjg9qg72hr3anrnnxbijdhl5y9")))

(define-public crate-bfom-blog-0.1.14 (c (n "bfom-blog") (v "0.1.14") (d (list (d (n "bfom-lib") (r "^0.1.42") (d #t) (k 0)))) (h "0200rnpbf7zhmc2b7i6n475ps6pknyf3bcj0mfbyblhsk677265m")))

(define-public crate-bfom-blog-0.1.15 (c (n "bfom-blog") (v "0.1.15") (d (list (d (n "bfom-lib") (r "^0.1.44") (d #t) (k 0)))) (h "1pkrq6gzrmbwl7sqfhd0950lij7p7v6pyxhxmysk8mxivn2y69l6")))

(define-public crate-bfom-blog-0.1.16 (c (n "bfom-blog") (v "0.1.16") (d (list (d (n "bfom-lib") (r "^0.1.44") (d #t) (k 0)))) (h "0agq5cai3nyba2j53rpgbkfdq3dppa6pp0rr2nmhn3addrfsg23j")))

(define-public crate-bfom-blog-0.1.17 (c (n "bfom-blog") (v "0.1.17") (d (list (d (n "bfom-lib") (r "^0.1.45") (d #t) (k 0)))) (h "043yyhyaf1vb6nnq7iy74mpzrhhpjxyjcdq1hs0ck15z64gkyslp")))

(define-public crate-bfom-blog-0.1.18 (c (n "bfom-blog") (v "0.1.18") (d (list (d (n "bfom-lib") (r "^0.1.46") (d #t) (k 0)))) (h "1mdqjgx9ls2ricxs8npq6kvnlwwgby2m44ww9zp44kwkhv3zkara")))

(define-public crate-bfom-blog-0.1.19 (c (n "bfom-blog") (v "0.1.19") (d (list (d (n "bfom-lib") (r "^0.1.47") (d #t) (k 0)))) (h "1cj1hlr7pczfkii3cjb4n4l60hbqp1wf691zhs3f4gp4lvb071fr")))

(define-public crate-bfom-blog-0.1.20 (c (n "bfom-blog") (v "0.1.20") (d (list (d (n "bfom-lib") (r "^0.1.48") (d #t) (k 0)))) (h "1zr0zglarwjk5bfh098kq6l5vkay9knm46afkckr0nxf39nmlr2c")))

(define-public crate-bfom-blog-0.1.21 (c (n "bfom-blog") (v "0.1.21") (d (list (d (n "bfom-lib") (r "^0.1.50") (d #t) (k 0)))) (h "1awzscgwfsg971hb2k2zxrhpzdfs8xgfishvszll5hchjnf7v4hp")))

