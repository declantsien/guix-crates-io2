(define-module (crates-io bf v1 bfv12) #:use-module (crates-io))

(define-public crate-bfv12-0.1.0 (c (n "bfv12") (v "0.1.0") (d (list (d (n "probability") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "02qwfvimaj9xli1y69xsxafpvr5c762q7wrq8m0l1wkyh50iwyda")))

(define-public crate-bfv12-0.1.1 (c (n "bfv12") (v "0.1.1") (d (list (d (n "probability") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1c1d11x074zflz52ww3rq4gixi4lnrrx37kl4gr8i7z7fy23h6ar")))

