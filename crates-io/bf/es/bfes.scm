(define-module (crates-io bf es bfes) #:use-module (crates-io))

(define-public crate-bfes-0.1.0 (c (n "bfes") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "safer-ffi") (r "^0.0.10") (f (quote ("proc_macros"))) (d #t) (k 0)))) (h "18blf0sq079xplwznw06p0js6xl0j4ncwpw4m25j00xs1xz08p5g") (f (quote (("c-headers" "safer-ffi/headers"))))))

