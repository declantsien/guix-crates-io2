(define-module (crates-io bf ie bfield) #:use-module (crates-io))

(define-public crate-bfield-0.3.0 (c (n "bfield") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mmap-bitvec") (r "^0.4.1") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "17ksrky17cv4w6r0kbir63ksbmm4z0k3j4lhby71d9kzlssy3w13")))

