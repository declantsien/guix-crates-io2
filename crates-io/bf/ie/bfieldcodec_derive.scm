(define-module (crates-io bf ie bfieldcodec_derive) #:use-module (crates-io))

(define-public crate-bfieldcodec_derive-0.1.0 (c (n "bfieldcodec_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "15yfrj9vii9g3m2abbh8qv7v43r87ahz3p1pvkflfyy5iwsa8xap")))

(define-public crate-bfieldcodec_derive-0.2.0 (c (n "bfieldcodec_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1g4m8gyda5ikw8d0dxy60a1ackcmqycbj7ndrzfqpab9x06n4qnc")))

(define-public crate-bfieldcodec_derive-0.3.0 (c (n "bfieldcodec_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0ybwd1nnl3k76bhr2vbipfglc4yddvn2p5s181w56ckqlrfw158m")))

(define-public crate-bfieldcodec_derive-0.4.0 (c (n "bfieldcodec_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1jyvai9vhwqb2yfav1y01cqlrvfgn8ms9rhbh9ws77bpyln3xf4k")))

(define-public crate-bfieldcodec_derive-0.4.1 (c (n "bfieldcodec_derive") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1ivmyza92qi3zhxw9hc5lkvqwmlf6lxjlpsk9xqxc42cmxrs4ljq")))

(define-public crate-bfieldcodec_derive-0.4.2 (c (n "bfieldcodec_derive") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1v27dmf9wm2azqh5wkl6gjnvg6ifqqanf86pr44ppm2c1skm73dh")))

(define-public crate-bfieldcodec_derive-0.5.0 (c (n "bfieldcodec_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0rkng6x676i0n1gg6d4hh5dwxg92s6x0gas4mss4lhn6hmlzxj64")))

(define-public crate-bfieldcodec_derive-0.5.1 (c (n "bfieldcodec_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1p22xs86alrby9zs294b2f63kqh7ahzww65r8bf99mjmifcv7lg7")))

(define-public crate-bfieldcodec_derive-0.5.2 (c (n "bfieldcodec_derive") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0hk3ymzky2hrp4b2wj8mdhhkk95r9g0k321h549inzdrnkx3dclw")))

(define-public crate-bfieldcodec_derive-0.6.0 (c (n "bfieldcodec_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "14xvhd0alprdn8mla8y1alhz0ygmjj59xmdk8n948xxifspqfiga")))

(define-public crate-bfieldcodec_derive-0.7.0 (c (n "bfieldcodec_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "16vambszxgv62v0f97rbn6hqxmddiggn6s82jzq2n9ssy6lynra2")))

