(define-module (crates-io bf #{3d}# bf3d) #:use-module (crates-io))

(define-public crate-bf3d-0.1.0 (c (n "bf3d") (v "0.1.0") (h "0d8sbybinrzy3ic3dnq5nl4yg5w91i7pq4mr97drmi6ap77hasyf") (y #t)))

(define-public crate-bf3d-0.1.1 (c (n "bf3d") (v "0.1.1") (h "0rlkj4r6wgrwi3jfqp6maigfwdnajd8mz7jm8nmmp5a6lcxx0xig") (y #t)))

(define-public crate-bf3d-0.1.2 (c (n "bf3d") (v "0.1.2") (h "0zj76p3l0988q8dgvs60vf3dkb756j0dzg7lnfq6yhj62yfnr3if") (y #t)))

