(define-module (crates-io rm ux rmuxinator) #:use-module (crates-io))

(define-public crate-rmuxinator-0.1.0 (c (n "rmuxinator") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "071j6h14v4jxy9c4vpzh0zvjvjh0rjcf5krbx1g12gpnsw0frfbm")))

(define-public crate-rmuxinator-1.0.0 (c (n "rmuxinator") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1m05hka5gqplbpl8xx4v8wg21agwdxx0ygvayddrq1a1vmmwx4r1")))

(define-public crate-rmuxinator-1.1.0 (c (n "rmuxinator") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "mockall") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1jbjhc1gishf8vlrfffa7rsifa7wpdakvjwrr52sk72ksjn1xcgb")))

(define-public crate-rmuxinator-2.0.0 (c (n "rmuxinator") (v "2.0.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "mockall") (r "^0.10") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0a2miphl9nj08gf9fa4v7jal4sf0433ia5v5hg9cdy8kac44imrh")))

