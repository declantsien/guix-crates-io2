(define-module (crates-io rm #{-i}# rm-improved) #:use-module (crates-io))

(define-public crate-rm-improved-0.5.0 (c (n "rm-improved") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "07jl4g37mm24hij48nqnm7q8wv2wa28s7vn0j3w82lkkmyg49129")))

(define-public crate-rm-improved-0.5.1 (c (n "rm-improved") (v "0.5.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1wrmv8nlp3w91b2r1fx6ghr61j4p7sqjdzh6qqjc4kxf58zh73lq")))

(define-public crate-rm-improved-0.6.0 (c (n "rm-improved") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1s96s8pif0k6iciarpixvg76yl516109qmaz6lbyadzg0gq6lqi8")))

(define-public crate-rm-improved-0.6.1 (c (n "rm-improved") (v "0.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1phll4whrd25lfkr094iw389dr14y2jqcypjqcl22z5q8n8xyls4")))

(define-public crate-rm-improved-0.6.2 (c (n "rm-improved") (v "0.6.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1h6ckwr4vv5icxagz3szxcjbhvlqvdx5zpq2rirw4mrfaqvss7mv")))

(define-public crate-rm-improved-0.7.0 (c (n "rm-improved") (v "0.7.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1lwgmw2mhsgx7r4j24jqk7fmbngbyhxza2q9mdqaajvbvig15msm")))

(define-public crate-rm-improved-0.7.1 (c (n "rm-improved") (v "0.7.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1m0i05zln5r5kv0sy21wvnvm0a46gdkf3jmdk9hs91n97siaz3bh")))

(define-public crate-rm-improved-0.7.2 (c (n "rm-improved") (v "0.7.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0ml05mgq538hyy47h3g0dazk69dyy93b030797fbrp2ms8xfk26c")))

(define-public crate-rm-improved-0.8.0 (c (n "rm-improved") (v "0.8.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0faxkbjz1xiai3qvyzihlddhmffl8xxv8l6dimfl4624h8jap3km")))

(define-public crate-rm-improved-0.8.1 (c (n "rm-improved") (v "0.8.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "162azj2bkjf0prdmbzdyhd6g8p3asl0s0rrcrb9dqbb3ilyv79q7")))

(define-public crate-rm-improved-0.8.2 (c (n "rm-improved") (v "0.8.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0lsbjnpsb7x181lpmb96n1vras45869xymymhcs8wk3a5xn1r7cn")))

(define-public crate-rm-improved-0.9.1 (c (n "rm-improved") (v "0.9.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0ljwnikr1i7hj9abcqhfynbsa4lab2ab9pcbi73qbhxhy84xb88w")))

(define-public crate-rm-improved-0.9.3 (c (n "rm-improved") (v "0.9.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "196ccd0qc7jxwhq0w2pax872p23s14jjnk3icyn6gakrnkd4qaxb")))

(define-public crate-rm-improved-0.9.4 (c (n "rm-improved") (v "0.9.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "13fs40wqci2vjcplr3vrirya18l2zrn6a3c18va1qv2pdz3zm7xv")))

(define-public crate-rm-improved-0.9.5 (c (n "rm-improved") (v "0.9.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "171qf74kgd59xic0rc9wrpyi0i5x0dybhv4r97qmacdvcl53i4rv")))

(define-public crate-rm-improved-0.9.6 (c (n "rm-improved") (v "0.9.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0dws6rrh5ri859qsc582rj6y7gla1zy2fqfwhlgynp85w87fm8c4")))

(define-public crate-rm-improved-0.10.0 (c (n "rm-improved") (v "0.10.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1pnlmvqfccaj5xp9w34gqna0d0jw6sqbsca64hba948r92dcyy1p")))

(define-public crate-rm-improved-0.10.1 (c (n "rm-improved") (v "0.10.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0cqck6nyzhdaailbc42axynxh6mnhygxrz3mip55f0ylsvp7y575")))

(define-public crate-rm-improved-0.10.2 (c (n "rm-improved") (v "0.10.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "04w2lw0i0skdin8lay0wgnyp8a9qqs8y2ry4v2jdnklyb8xd25sr")))

(define-public crate-rm-improved-0.11.0 (c (n "rm-improved") (v "0.11.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "134wbvy10z1w3kn5zln4il3f4c679dsabr1im1vs73b98fivkrkl")))

(define-public crate-rm-improved-0.11.1 (c (n "rm-improved") (v "0.11.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "06i4yldzv57p7i7xq0v0x0ax7952f7vpxpw8sv67ls8yr3xyays3")))

(define-public crate-rm-improved-0.11.2 (c (n "rm-improved") (v "0.11.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1c3946ggjzilasxgraplkr9cyb759f6zkv3pgkfg6pa5y32q6z0m")))

(define-public crate-rm-improved-0.11.3 (c (n "rm-improved") (v "0.11.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1g235nva4xr110gfzgk46pya2i1787dhwcfc3vhvj811qfdwjlyf")))

(define-public crate-rm-improved-0.11.4 (c (n "rm-improved") (v "0.11.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "01cxxbk90pabhl2q0k35igvfmwlc6jnysr6z2p8349sqysxjdy7f")))

(define-public crate-rm-improved-0.12.0 (c (n "rm-improved") (v "0.12.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "004r9mplnhjrar5kyz55xyl0aabq80ld1mj3l6n34n4fivq72wsc")))

(define-public crate-rm-improved-0.12.1 (c (n "rm-improved") (v "0.12.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1gngdmgjqqq9c5d7a7vrbjczz0qmrpb7ag9bd5f9n9mhw64ly2ni")))

(define-public crate-rm-improved-0.13.1 (c (n "rm-improved") (v "0.13.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "08l703jdrdqgbqjnimkcwn2crd8v25fig21lm9hszcrk2vdvnr39")))

