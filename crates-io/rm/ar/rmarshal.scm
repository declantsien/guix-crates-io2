(define-module (crates-io rm ar rmarshal) #:use-module (crates-io))

(define-public crate-rmarshal-0.1.0 (c (n "rmarshal") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0dd5v7kbm7kgs1gwq1sf8wgjabipcplwcpy8iy3a0pnyblzpyaja") (f (quote (("debug"))))))

(define-public crate-rmarshal-0.1.1 (c (n "rmarshal") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "10c35g6jxn3f578fizbh0c5rjrhmbcbbx81y1jlfssr6567qx5cm") (f (quote (("debug"))))))

