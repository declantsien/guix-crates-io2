(define-module (crates-io rm p_ rmp_fs) #:use-module (crates-io))

(define-public crate-rmp_fs-0.1.0 (c (n "rmp_fs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0nm3flakfsajmqv5kmj4by63v4wgrya601zs1dadhj4s05nb7ggb") (y #t)))

(define-public crate-rmp_fs-0.2.0 (c (n "rmp_fs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1vv5wykylrafr64xa4in5yja2wqaax88x8cqq7z98b84p78i06z4") (y #t)))

(define-public crate-rmp_fs-0.2.1 (c (n "rmp_fs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1xlvkg1sljls1h8n71zzsnwv5dvm5m7r2z2a36xhk94ici1mglcd") (y #t)))

(define-public crate-rmp_fs-0.2.2 (c (n "rmp_fs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "114854cmn7v221drcq4ns5q3a028af0ldsakgd1vqw6nckmr2ajm")))

(define-public crate-rmp_fs-0.2.3 (c (n "rmp_fs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "04gnzzp9iwzv9bhkymiw0lynqys8zqwagbhbgxvll3pqp2fymbnk")))

(define-public crate-rmp_fs-1.0.0 (c (n "rmp_fs") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "1ibhfqnj8nr9s679jhb9whfmshv5vrx45msr0jkyg450d7lm9zgw")))

(define-public crate-rmp_fs-1.0.1 (c (n "rmp_fs") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 2)) (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "034xrj2r9jlxwyggkppr5clasg1pyrfk7pb2n6m327ydg6nssvv0")))

(define-public crate-rmp_fs-1.0.2 (c (n "rmp_fs") (v "1.0.2") (d (list (d (n "rmp-serde") (r "^0.15.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0vjq34dls08lay9j65fh3mj3pfqvdm4ck3gpwx4rnqnji45kczqq")))

(define-public crate-rmp_fs-1.0.3 (c (n "rmp_fs") (v "1.0.3") (d (list (d (n "rmp-serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "15q7r5vsx4vby8sc75sd525brgalqji6srjr27691xd5fvk0sh8k")))

(define-public crate-rmp_fs-1.0.4 (c (n "rmp_fs") (v "1.0.4") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1bifh67rhvrv2mgjdail3bzby4cydqvd8mdq3sq6lsk06dk86vyh")))

