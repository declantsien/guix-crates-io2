(define-module (crates-io rm al rmall) #:use-module (crates-io))

(define-public crate-rmall-0.1.0 (c (n "rmall") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("macros" "fs" "rt"))) (d #t) (k 0)))) (h "12gwpy1h3vbphp32qfhjqlhv8iyp4kc5zdr6y53xlr40wk3q6jj8")))

