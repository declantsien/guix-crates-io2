(define-module (crates-io rm al rmalloc) #:use-module (crates-io))

(define-public crate-rmalloc-1.0.0 (c (n "rmalloc") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "mersenne_twister") (r "^1.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "11bda32l6b6zwx2zfvcihvm4n98cgmbjnhln3h512f0dja35a1hj")))

(define-public crate-rmalloc-1.0.1 (c (n "rmalloc") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "mersenne_twister") (r "^1.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0mcab97pvbxz4haszix4nj5zinvvbg4hj43lxmk92kl99l9mf0br")))

(define-public crate-rmalloc-1.1.0 (c (n "rmalloc") (v "1.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "mersenne_twister") (r "^1.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "yaxpeax-arch") (r "^0.0.4") (o #t) (k 0)) (d (n "yaxpeax-x86") (r "^0.1.3") (o #t) (k 0)))) (h "0ka5y5rw0m09czv66a2mwyv43f0dq2i3nga601cf2vlnay863cmv") (f (quote (("safety-checks" "yaxpeax-x86" "yaxpeax-arch" "num-traits") ("default"))))))

