(define-module (crates-io rm cm rmcmc) #:use-module (crates-io))

(define-public crate-rmcmc-0.1.0 (c (n "rmcmc") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "nalgebra") (r "^0.16.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.3") (d #t) (k 0)) (d (n "rv") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "special") (r "^0.7.8") (d #t) (k 0)))) (h "0vdgijrkijy96mafbm4ryiwsmi8lk8v2xpzinq9l7dqccfgdkvv4") (f (quote (("serde_support" "serde" "serde_derive" "nalgebra/serde-serialize"))))))

