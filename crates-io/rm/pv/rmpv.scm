(define-module (crates-io rm pv rmpv) #:use-module (crates-io))

(define-public crate-rmpv-0.1.0 (c (n "rmpv") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0hk3rr4rnd1wmiwdd6yfr7kk4pj34n5q01fm3rrgqybq3wjc5hn5") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.2.0 (c (n "rmpv") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1hh582b732hws4p9c9lvc7nhawkfpy8hx5za2xnvza70dvzhhn8v") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.3.0 (c (n "rmpv") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1rl830hqw45h33sa19vaxcz7g2p22h43r4h255avvckc8872g8h5") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.3.1 (c (n "rmpv") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1ip9dlva18xnk9zsrcaaqppqk7g972mgjkrz5p2h8a5fzfqia0n3") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.3.2 (c (n "rmpv") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1jkhrvxp91wdcg4bpk7z7dlz6p06jpxcm0anpzmg1j693v5bf8q9") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.3.3 (c (n "rmpv") (v "0.3.3") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1rzhbk8m2n6xy5l7xv29n8qfv1402lm45anvslvczy5h8shnz4yf") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.3.4 (c (n "rmpv") (v "0.3.4") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1bz7hbw525nzh27wpgh9jwi41xkdaniccgl1v1v664b0djm8sdj5") (f (quote (("with-serde" "serde")))) (y #t)))

(define-public crate-rmpv-0.4.0 (c (n "rmpv") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (o #t) (d #t) (k 0)))) (h "09b3pz0gxcn0jq2krdmbxf8zfrbwcxbwdlx1jsimbsbxf02h5br9") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.1 (c (n "rmpv") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rmp") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0dfq5r2lzsah6hcrqad5d754flgbdqgnh2qs3n055a9xz99nsjad") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.2 (c (n "rmpv") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0gy44hawsc4b4jaz37x9sfidz3jlvy6kk5ffnsd1g6dxmi2zgjl3") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.3 (c (n "rmpv") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0nyz89wknb7i9821cj4d8ra4psq4q01xwim5955rzxcb3c9r4izz") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.4 (c (n "rmpv") (v "0.4.4") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "1qlz9rlpggw8ckqsd2xrhf7cczwg6mw2vgagn48hh6v9v26hgrdq") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.5 (c (n "rmpv") (v "0.4.5") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "14s2mi9pcqfxkqnkprwqw4d4y0w19301sl1yrmrv6fp89cy8ydgf") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.6 (c (n "rmpv") (v "0.4.6") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "0da2q1sjgcc042mz4kl6jwwbkqanlzxq89j8m8zxzyi9nmpk07k0") (f (quote (("with-serde" "serde" "serde_bytes")))) (y #t)))

(define-public crate-rmpv-0.4.7 (c (n "rmpv") (v "0.4.7") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "1mczy1ffpnwnimsk277zxbqc69l3h9mmnj1n3q91cplm27z0lxkw") (f (quote (("with-serde" "serde" "serde_bytes"))))))

(define-public crate-rmpv-1.0.0 (c (n "rmpv") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.119") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "0m4p668sh2h9rsl9n1gzdf6xhxa1g2wby9arzqw52p7rlari726y") (f (quote (("with-serde" "serde" "serde_bytes"))))))

(define-public crate-rmpv-1.0.1 (c (n "rmpv") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "1v01az3ahglmag94d75qgdygfcgwj9bh5921rvn49d52lha043if") (f (quote (("with-serde" "serde" "serde_bytes")))) (r "1.70")))

(define-public crate-rmpv-1.0.2 (c (n "rmpv") (v "1.0.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "1psh8cdjxchr4ing175ziycip1qvnxzrlln55g45c6bm24pjhh75") (f (quote (("with-serde" "serde" "serde_bytes")))) (r "1.70")))

(define-public crate-rmpv-1.3.0 (c (n "rmpv") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)) (d (n "rmp") (r "^0.8.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "1adjigqyrzbv71s18qz3sa77zqggqip0p8j4rrrk5scyrlihfiaq") (f (quote (("with-serde" "serde" "serde_bytes")))) (r "1.70")))

