(define-module (crates-io rm en rmenu) #:use-module (crates-io))

(define-public crate-rmenu-0.1.0 (c (n "rmenu") (v "0.1.0") (d (list (d (n "conrod") (r "0.54.*") (f (quote ("glium" "winit"))) (d #t) (k 0)))) (h "1n6jyd5csawq5hv9jpgnx50b3xjd6zapf1v47hfiapmqmfv4nq6q")))

(define-public crate-rmenu-0.1.1 (c (n "rmenu") (v "0.1.1") (d (list (d (n "conrod") (r "0.54.*") (f (quote ("glium" "winit"))) (d #t) (k 0)))) (h "0r51na3x1f5jgmxbqnwvibmamcd45cqyh9h74gq9d26hwlpbgayf")))

(define-public crate-rmenu-0.1.2 (c (n "rmenu") (v "0.1.2") (d (list (d (n "conrod") (r "0.56.*") (f (quote ("glium" "winit"))) (d #t) (k 0)))) (h "12ylc2bwnrx7kg0b28vhaj9wybhx0vv280qyjghmdv69c5bqdswv")))

(define-public crate-rmenu-0.1.3 (c (n "rmenu") (v "0.1.3") (d (list (d (n "conrod") (r "0.56.*") (f (quote ("glium" "winit"))) (d #t) (k 0)))) (h "1lds9q6dddn554bn1dyx5n7zscmfzpvk7r8pvc6pacajqsi054fq")))

(define-public crate-rmenu-0.1.4 (c (n "rmenu") (v "0.1.4") (d (list (d (n "conrod") (r "0.58.*") (f (quote ("glium" "winit"))) (d #t) (k 0)))) (h "07l3wvzdnhm0kv41cp72044m4i54chgc7ji8rj60qp6c60ilqxmk")))

