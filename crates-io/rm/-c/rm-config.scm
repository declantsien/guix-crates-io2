(define-module (crates-io rm -c rm-config) #:use-module (crates-io))

(define-public crate-rm-config-0.1.0 (c (n "rm-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "06lpbw6l5ipcvfkji7yqblvsg11jnb05jyqjya6damdmgp8zkafj")))

(define-public crate-rm-config-0.2.0 (c (n "rm-config") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "1xgmjry9y2hwdhs271352nxw7ngsziga9x9gn0ylh5gk9nyp2gvx")))

