(define-module (crates-io rm #{1a}# rm1asm) #:use-module (crates-io))

(define-public crate-rm1asm-1.0.1 (c (n "rm1asm") (v "1.0.1") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1h7iwrnlp0ma6h5xxv18yarar738m2q8925ql2syv0cmgzg1x1xx")))

(define-public crate-rm1asm-1.0.0 (c (n "rm1asm") (v "1.0.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "07gh1czbcvl77h9xdvn2xzsp1a9mgx46sxln79klkd6qs72nwhgd")))

(define-public crate-rm1asm-1.0.2 (c (n "rm1asm") (v "1.0.2") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0l3g4jk278c0np5sswg1rznn3v45pl9sl2svjwzj8mk96hvnax7i")))

(define-public crate-rm1asm-1.0.3 (c (n "rm1asm") (v "1.0.3") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1cfrjq2vasyh78m167m0qf6zksqpbcc3kki2li5nldczr43jfmji")))

