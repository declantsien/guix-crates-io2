(define-module (crates-io rm -f rm-frust5-api) #:use-module (crates-io))

(define-public crate-rm-frust5-api-0.0.1 (c (n "rm-frust5-api") (v "0.0.1") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)))) (h "0a5162j98cki878v0jigmn6npphg6h7f0hry4ilq8lr4lgd1392i") (r "1.58.0")))

(define-public crate-rm-frust5-api-0.0.2 (c (n "rm-frust5-api") (v "0.0.2") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)))) (h "0x2qlfmg8ryfcsa49ivhspsyxwmpjayzv7cabfnzh3h67h05dy0b") (r "1.58.0")))

(define-public crate-rm-frust5-api-0.0.3 (c (n "rm-frust5-api") (v "0.0.3") (d (list (d (n "hdf5") (r "^0.8") (d #t) (k 0)) (d (n "resolve-path") (r "^0.1.0") (d #t) (k 0)))) (h "1kq15iqy28s2qqa8p1lwryjarvidfwkq542azq6ipjnyn8dx5hw0") (r "1.58.0")))

