(define-module (crates-io rm at rmathlib) #:use-module (crates-io))

(define-public crate-rmathlib-1.0.0 (c (n "rmathlib") (v "1.0.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "01vqa9qk1ygfw8vsm669qcsysm8ni19yj6wi8n3hbx45wzal0bsa") (r "1.56.1")))

(define-public crate-rmathlib-1.1.0 (c (n "rmathlib") (v "1.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1p1cca0xjdckp5cbfzjh7y787q9symkr94pph0d49q7p9vxfd385") (r "1.56.1")))

