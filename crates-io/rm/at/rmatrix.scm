(define-module (crates-io rm at rmatrix) #:use-module (crates-io))

(define-public crate-rmatrix-0.1.0 (c (n "rmatrix") (v "0.1.0") (d (list (d (n "ncurses") (r "^5.99") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0lpvbh6q6ybda14ljhrvfcvyyr8yi6503f0cilqr07hviaaaih8f")))

(define-public crate-rmatrix-0.1.1 (c (n "rmatrix") (v "0.1.1") (d (list (d (n "ncurses") (r "^5.99") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0whc4b4c5bh8rn01lzbdrw8b3yxmc4y6y1hbf2mslbhpyprznzmv")))

