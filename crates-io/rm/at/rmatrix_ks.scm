(define-module (crates-io rm at rmatrix_ks) #:use-module (crates-io))

(define-public crate-rmatrix_ks-0.2.0 (c (n "rmatrix_ks") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06yy7iy9xb6skyraf2vc70vmlwzlbsvqik0gix4ryp0ykxshg44k")))

(define-public crate-rmatrix_ks-0.2.1 (c (n "rmatrix_ks") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bpqa8pkfvfc3nbq63yc5p8j8lnhd4a7np561bdlhpdsagffd7bi")))

