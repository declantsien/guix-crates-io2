(define-module (crates-io rm em rmem) #:use-module (crates-io))

(define-public crate-rmem-0.1.0 (c (n "rmem") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "procfs") (r "^0.8.1") (d #t) (k 0)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)))) (h "1pq3dkqck6mhq9wn3nxl227jdww3z9sdvic429k9xngr2vk5bnvz")))

(define-public crate-rmem-0.2.0 (c (n "rmem") (v "0.2.0") (d (list (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "getargs") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0i48g9pn7zz8m2ilfxgv127i6gzgxhwlldsbjsxy1hfcsw8nr52i")))

