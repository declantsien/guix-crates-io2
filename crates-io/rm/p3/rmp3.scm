(define-module (crates-io rm p3 rmp3) #:use-module (crates-io))

(define-public crate-rmp3-0.1.0 (c (n "rmp3") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pz4ys9d0g796vak4zrv845ms4prglls76gy8xplm7hy2b1mxzki") (f (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.2.0 (c (n "rmp3") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kqf6d339azcrrl90p1vkzblk6gm3cjq3q092iasvqbnfn72g7bw") (f (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.2.1 (c (n "rmp3") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03sj0zc4ik1h4i5w0fxkzmq05y6alpbr2crdn9dlqml8225b7l3w") (f (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.3.0 (c (n "rmp3") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vy6jnq4ls47gg3ibr607wlp3xsqsyjfgwmw1cai6wxs77mkr3xg") (f (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("float") ("default" "simd"))))))

(define-public crate-rmp3-0.3.1 (c (n "rmp3") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bdi3b11si0aadgxzqchwams3gxm9pyq4xq0axf686d8i4ngpnfk") (f (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("float") ("default" "simd"))))))

