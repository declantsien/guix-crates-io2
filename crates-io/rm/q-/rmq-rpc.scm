(define-module (crates-io rm q- rmq-rpc) #:use-module (crates-io))

(define-public crate-rmq-rpc-0.1.0 (c (n "rmq-rpc") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-amqp") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1a5jpq5jlnz4rfzh1c9ahlw47bw5ijyp1aacg4a4q7hwbnjk3sg9") (y #t)))

(define-public crate-rmq-rpc-0.1.1 (c (n "rmq-rpc") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-amqp") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pj3rylm7f5iyfp9q4y53yhi83hxzpm28anw32si31ipcf3baw78")))

(define-public crate-rmq-rpc-0.1.2 (c (n "rmq-rpc") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-amqp") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "07dqwa346mhgix018izgimc2nzdc87aj30a166wxnknxp55cwn09")))

