(define-module (crates-io rm q- rmq-rs-admin) #:use-module (crates-io))

(define-public crate-rmq-rs-admin-0.1.0 (c (n "rmq-rs-admin") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "185mvnj9bnasc18l4slim99znz140r1vqimgn5wzmkws4shpsiys")))

(define-public crate-rmq-rs-admin-0.1.1 (c (n "rmq-rs-admin") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j6160g8w596q44l5r8m263120fvhh2p0bdvzchingng5hv057f4")))

