(define-module (crates-io rm rf rmrf) #:use-module (crates-io))

(define-public crate-rmrf-0.1.0 (c (n "rmrf") (v "0.1.0") (d (list (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "1qvjabr0868wz1nfbi284l2vdrd2k1i89hhzsjzfkcky9xxa5jmb")))

(define-public crate-rmrf-0.1.1 (c (n "rmrf") (v "0.1.1") (d (list (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "0m2mzlq8n1l4xjc6c2a2678pzvkaczawg19p8k31h2x0cr1yb5vb")))

(define-public crate-rmrf-0.1.2 (c (n "rmrf") (v "0.1.2") (d (list (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "1bx68xqihy94qpzi59nd4p3rnfa4l7b8ds0kda48fpnl7zr1277v")))

(define-public crate-rmrf-0.1.3 (c (n "rmrf") (v "0.1.3") (d (list (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "04g1pzf3y0z19v05mfsws7s32i17xhfqrsax35j8cvqb37nvvbny")))

(define-public crate-rmrf-0.1.4 (c (n "rmrf") (v "0.1.4") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "0gc0zcph0apbifin1spas3as2nnvis7gzydwb40r8cgzfjpifjjz")))

(define-public crate-rmrf-0.1.5 (c (n "rmrf") (v "0.1.5") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "1am22l736ix5b09q6c3vzf6k90hy7d3bmxlbzdsbps5qxkfvna2h")))

(define-public crate-rmrf-0.1.6 (c (n "rmrf") (v "0.1.6") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "trash") (r "^3.0") (f (quote ("coinit_speed_over_memory"))) (d #t) (k 0)))) (h "07p32z9yl1sslwk7n3f6x3rmx3virl5zlca56ranz5py3n076xmm")))

