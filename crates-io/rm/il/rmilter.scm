(define-module (crates-io rm il rmilter) #:use-module (crates-io))

(define-public crate-rmilter-0.1.0 (c (n "rmilter") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "charset") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "quoted_printable") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1wnaainm9irlyr1cv0cipfj6x6i3s6pzcg1g9mhchrx1wpqa26ap")))

(define-public crate-rmilter-0.2.0 (c (n "rmilter") (v "0.2.0") (d (list (d (n "base64") (r ">=0.13.0, <0.14.0") (d #t) (k 0)) (d (n "bitflags") (r ">=1.2.0, <2.0.0") (d #t) (k 0)) (d (n "charset") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "quoted_printable") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "regex") (r ">=1.4.0, <2.0.0") (d #t) (k 0)))) (h "187dwr4zj4ak02kk88lm1rcli75g671d82v7m4nj1xdpbr5g3qa2")))

