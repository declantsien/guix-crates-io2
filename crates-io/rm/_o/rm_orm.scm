(define-module (crates-io rm _o rm_orm) #:use-module (crates-io))

(define-public crate-rm_orm-0.1.0 (c (n "rm_orm") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "mongodb") (r "^2.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rm_orm_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "10v2a3z6pvm7vgy7fhagxnxvfnr0i0npdwrsi5w1zmii6f3axs9d") (y #t)))

