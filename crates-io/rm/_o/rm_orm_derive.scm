(define-module (crates-io rm _o rm_orm_derive) #:use-module (crates-io))

(define-public crate-rm_orm_derive-0.1.0 (c (n "rm_orm_derive") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0gxq86rmwbw4pq6anffnlklf2b9aqipmwi7kxjli4krxl8hwny4k") (y #t)))

