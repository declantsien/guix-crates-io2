(define-module (crates-io rm k- rmk-config) #:use-module (crates-io))

(define-public crate-rmk-config-0.1.0 (c (n "rmk-config") (v "0.1.0") (d (list (d (n "embassy-nrf") (r "^0.1.0") (f (quote ("unstable-pac" "time"))) (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s6xq8gilm1b2vlzasp6qvxh7w9ybb7ch9gsvm9r0z70nz00s26p") (f (quote (("toml" "serde" "serde_derive") ("default") ("_nrf_ble" "embassy-nrf") ("_esp_ble"))))))

