(define-module (crates-io rm js rmjs) #:use-module (crates-io))

(define-public crate-rmjs-0.1.0 (c (n "rmjs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "119w3nrflgigm5spj7k9g69nx4kqn5b8kfk2fjy4s7slgy742fgx")))

