(define-module (crates-io rm p- rmp-serialize) #:use-module (crates-io))

(define-public crate-rmp-serialize-0.7.0 (c (n "rmp-serialize") (v "0.7.0") (d (list (d (n "rmp") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1yi3ncmvlh2zryckpb1haf5zhl6zxs511appff19lk5dg8y3rf19") (y #t)))

(define-public crate-rmp-serialize-0.8.0 (c (n "rmp-serialize") (v "0.8.0") (d (list (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1fhw30kw8algjp7wx2p8c2xf9h5m1jhsyv7sh5fqv893afbgmnal") (y #t)))

(define-public crate-rmp-serialize-0.8.1 (c (n "rmp-serialize") (v "0.8.1") (d (list (d (n "rmp") (r "^0.8.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0isb9hd7ra8qi0dhs45kvmcbimqb12vd18cl0b8jsicszpkknxwy")))

