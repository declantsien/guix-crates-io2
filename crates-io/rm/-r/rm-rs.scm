(define-module (crates-io rm -r rm-rs) #:use-module (crates-io))

(define-public crate-rm-rs-0.2.0 (c (n "rm-rs") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "09ncvywvib6nf1z1fdsrgl7hzv3s03zps6lbznnfngv9sk8wl2qx")))

(define-public crate-rm-rs-0.2.1 (c (n "rm-rs") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "1npz3g9jn66fgj9c8sj3dmrmcqqgd1s45rrjik95ncaq2xaw51f5")))

(define-public crate-rm-rs-0.2.2 (c (n "rm-rs") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.1") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "0ym1cjdzsgkdbypxcv4h62vbxmbxrqczrdzbn33m8qx798amxvkc")))

