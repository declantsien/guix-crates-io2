(define-module (crates-io rm bg rmbg) #:use-module (crates-io))

(define-public crate-rmbg-0.1.0 (c (n "rmbg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "fast_image_resize") (r "^3.0.4") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ort") (r "^2.0.0-rc.1") (f (quote ("ndarray"))) (k 0)) (d (n "ort") (r "^2.0.0-rc.1") (f (quote ("download-binaries"))) (k 2)))) (h "01bwjp0nknnraxcg4783ynin9lj9gxk6xznvpxx26bdyswx66mfj")))

