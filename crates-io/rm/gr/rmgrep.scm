(define-module (crates-io rm gr rmgrep) #:use-module (crates-io))

(define-public crate-rmgrep-1.1.0 (c (n "rmgrep") (v "1.1.0") (h "1dblm9sd1i8y9k433pz0i2fxaqqgs3aw6py65r90glj515440yvk")))

(define-public crate-rmgrep-1.1.1 (c (n "rmgrep") (v "1.1.1") (h "0kn1dw58qsbmzw3kw3g290jqilnp86n6gwl28ixg7j1xs33g4c1n")))

