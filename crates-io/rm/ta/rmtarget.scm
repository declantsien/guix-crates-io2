(define-module (crates-io rm ta rmtarget) #:use-module (crates-io))

(define-public crate-rmtarget-0.1.0 (c (n "rmtarget") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1h1x6bhy7c5hg1bd4p74md37l66jdv9llhk4r1pm8psir7wi5fmz")))

