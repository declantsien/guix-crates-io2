(define-module (crates-io rm on rmonitor) #:use-module (crates-io))

(define-public crate-rmonitor-0.1.0 (c (n "rmonitor") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "tcp" "rt-core" "macros" "dns"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "10d03rgsh6l4689ad22r619vz6g6j0ly9bfqh2k4h1g0i2sjdfzj")))

(define-public crate-rmonitor-0.1.1 (c (n "rmonitor") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "tcp" "rt-core" "macros" "dns"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "026dl0v6zr6gll4nrmv0xcbh93rr9a72lb6df9cgq7jfhqlljsk4")))

(define-public crate-rmonitor-0.2.0 (c (n "rmonitor") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time" "tcp" "rt-core" "macros" "dns"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "09mllbahg4hlq7bp1irjsqjjmzvllz45708x24i9g33cqyn0vn7b")))

(define-public crate-rmonitor-0.3.0 (c (n "rmonitor") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "io-util" "time" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1ypxfz0qlsmkdwm0iq5yk8s6j75r4ip2hkafy56yqqq5sscl1iy2")))

