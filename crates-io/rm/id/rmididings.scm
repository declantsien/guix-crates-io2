(define-module (crates-io rm id rmididings) #:use-module (crates-io))

(define-public crate-rmididings-0.1.0 (c (n "rmididings") (v "0.1.0") (d (list (d (n "alsa") (r "^0.5.0") (d #t) (k 0)))) (h "0bh3awlix57ka0rq4qgp4fcnhw4rs563v9z5mal9mg3yiyh5m428")))

(define-public crate-rmididings-0.1.1 (c (n "rmididings") (v "0.1.1") (d (list (d (n "alsa") (r "^0.5.0") (d #t) (k 0)))) (h "0x5nbdnn7x5mfpz3hlym5b4srics945kxcdyj78013xal8fcf6pb")))

(define-public crate-rmididings-0.1.2 (c (n "rmididings") (v "0.1.2") (d (list (d (n "alsa") (r "^0.5.0") (d #t) (k 0)))) (h "0xr0j50vqfgxniv2ww8kb5x4mqr7fcwb4zsv040q2g3nvssaw35y")))

(define-public crate-rmididings-0.2.0 (c (n "rmididings") (v "0.2.0") (d (list (d (n "alsa") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "dbus") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "rosc") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "1dqg0b49akmc64lh481p1hkg35q4x1p6l2zjp73s8fjpyjxsj8pp") (f (quote (("osc" "rosc") ("default" "alsa" "osc"))))))

(define-public crate-rmididings-0.2.1 (c (n "rmididings") (v "0.2.1") (d (list (d (n "alsa") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "dbus") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "rosc") (r "^0.5.2") (o #t) (d #t) (k 0)))) (h "0g8y2djl8drawmjwj1axddaid40dk5bpmwqk7h6h9b7v2nlr90n6") (f (quote (("osc" "rosc") ("default" "alsa" "osc"))))))

