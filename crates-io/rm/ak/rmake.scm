(define-module (crates-io rm ak rmake) #:use-module (crates-io))

(define-public crate-rmake-0.1.0 (c (n "rmake") (v "0.1.0") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "15xkc6ak664aymlvjvw6c3rj0gg5si4209pz8sf3rsgafvfzbcgj")))

(define-public crate-rmake-0.1.1 (c (n "rmake") (v "0.1.1") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "07kis0w9prpi2il3m0x5302hl2nq2yjfp4bknvssm9bv40iy2wbh")))

(define-public crate-rmake-0.1.2 (c (n "rmake") (v "0.1.2") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1b8m15a09wng58rvy31qcd26z3cd47sr2zgf668qw99najl3fvjm")))

(define-public crate-rmake-0.1.3 (c (n "rmake") (v "0.1.3") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "03sqyzfxvr3axgwkkl9m34n4xgn1dgbdkydf7cs5hf2xrkyaz4rx")))

(define-public crate-rmake-0.1.4 (c (n "rmake") (v "0.1.4") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1fpfq4ljqkshf6ifk9h5d5pvs89fxw9zh7hbiy2xpvbnbhrs1smc")))

(define-public crate-rmake-0.1.5 (c (n "rmake") (v "0.1.5") (d (list (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0ldpm604mc3x9fmvcvykkv12fi8gfn53fljn8ykkrk6jqk2pxwy5")))

(define-public crate-rmake-0.1.6 (c (n "rmake") (v "0.1.6") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1cgw68jdgpn7s9nkzvd9aqhs2xp02c7j4lp36glvnh9wvv2c4sl0")))

(define-public crate-rmake-0.1.7 (c (n "rmake") (v "0.1.7") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0fc68s5p9yzd6820k5w58kp50wcx0z88rhhqayf1d5lygpq8czjl")))

(define-public crate-rmake-0.1.8 (c (n "rmake") (v "0.1.8") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1yhdymi5sfiija8qyj96bvkk86d34s4281nbndy0dym17c4jyfrv")))

(define-public crate-rmake-0.1.9 (c (n "rmake") (v "0.1.9") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "122972pa5xk9arqk6w52k3v3qbnwywsm7v1b56w1z7pa9pavx39z")))

(define-public crate-rmake-0.1.10 (c (n "rmake") (v "0.1.10") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1ax0j4wj00wll7lzszh9zc03kmx13x9nsi9v2ih48h4lkw032r0s")))

(define-public crate-rmake-0.1.11 (c (n "rmake") (v "0.1.11") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0xil0z9y0agqjzagj69ipyygr1mnfcq8i6j98rr2lx9xzijxa2ll")))

(define-public crate-rmake-0.1.12 (c (n "rmake") (v "0.1.12") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0ab2dpiidns0kfsj3ds0vplmpxw1rivhlikyla5zmvgssi57jxn9")))

(define-public crate-rmake-0.1.13 (c (n "rmake") (v "0.1.13") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1rhxhhrx9n1zp27l0gnahmhbdymccyx4wv7id7x68ia5kq232ivy")))

(define-public crate-rmake-0.1.14 (c (n "rmake") (v "0.1.14") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "18l69qia08ldvyrnxri2gniapnys6xngyzpdh8qgyrqlgpm6ajy4")))

(define-public crate-rmake-0.1.15 (c (n "rmake") (v "0.1.15") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1cbbiqii8mbk86an34mzk57vayv5gahnndpx78jpgcz70av29wm9")))

(define-public crate-rmake-0.1.16 (c (n "rmake") (v "0.1.16") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1imnbwk2lh6h74lh83vd0mq4n9qv881wrc35g1iwqwbd759kxij9")))

(define-public crate-rmake-0.1.17 (c (n "rmake") (v "0.1.17") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1l70rn6l5jjanyrqalggh2wwscn95zxk0wwcy2midhvjni34yfbn")))

(define-public crate-rmake-0.1.18 (c (n "rmake") (v "0.1.18") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "06rgwha7d8ck6wqgpi9f9v0408aak1vyvib7zazw2bydyd2pg3si")))

(define-public crate-rmake-0.1.19 (c (n "rmake") (v "0.1.19") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0i4hy53m11r16kcwj0r5dx5lzywzyprvz2vxny7fyhv6z4hx6kdb")))

(define-public crate-rmake-0.1.20 (c (n "rmake") (v "0.1.20") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "02xack48kigdq1s8amv2p7srbh9p4ka4cccv18xgzi4lc83q38rn")))

(define-public crate-rmake-0.1.21 (c (n "rmake") (v "0.1.21") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1nxrai0vmwyfq46cs9l03k40q502s6r1spyg9828cjsd8q1fjmsr")))

(define-public crate-rmake-0.1.22 (c (n "rmake") (v "0.1.22") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "04f0rb6yc0kb2axsbyfv2gsax1shp5cswzp95lmi30l0p7bwifv6")))

(define-public crate-rmake-0.1.23 (c (n "rmake") (v "0.1.23") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "068w8z7c4bihjyn4wz9xrrkixczjrp15j757aixbbyhhw795h855")))

(define-public crate-rmake-0.1.24 (c (n "rmake") (v "0.1.24") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0arhzn7lnrjhih0l4apjq42i5b3rkbprs1sr1nfgiwz8qlbw4ad2")))

(define-public crate-rmake-0.1.25 (c (n "rmake") (v "0.1.25") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "14y2p5bljxkzd29awq3q0nrssfj678qw03rwynz1ni7lk4717agh")))

(define-public crate-rmake-0.1.26 (c (n "rmake") (v "0.1.26") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06b18zdx3d53rmdb0k93w5bfbc8gdhh662lqdhv284bcsi07wv90")))

(define-public crate-rmake-0.1.27 (c (n "rmake") (v "0.1.27") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "05bkxjvpsc5habf2cskfq1ip3vf25f3mcyv3ajihgh7rb84dbf7q")))

(define-public crate-rmake-0.1.28 (c (n "rmake") (v "0.1.28") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ka8fz22ds0ccs4ldkmxh22a7qjp4yxrrdlz60n9i3lq7307ahd3")))

(define-public crate-rmake-0.1.29 (c (n "rmake") (v "0.1.29") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lwdgvhnsizpczvqqbcnwjxfqsq2d13h7xibyzwfcsdz9rb0gxj9")))

(define-public crate-rmake-0.1.30 (c (n "rmake") (v "0.1.30") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0949c09jc7x67rhbls6i1r7yd82mgjycb2s9024370074dzsfgsg")))

(define-public crate-rmake-0.1.31 (c (n "rmake") (v "0.1.31") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sg9m91wmrwqidcjq7ycj6s6ajj615jzwhd1550asgm5hcgdq33q")))

(define-public crate-rmake-0.1.32 (c (n "rmake") (v "0.1.32") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0jlfcq44pd0w59bqqwp2lvbl58h8is9lq54dzxz3hys17bakc08i")))

(define-public crate-rmake-0.1.33 (c (n "rmake") (v "0.1.33") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0l24a51khl60kxvnj95wgvd7yabvr32xb0b26ydfqmbbzf6kipgw")))

(define-public crate-rmake-0.1.34 (c (n "rmake") (v "0.1.34") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0aj7k0r9ynrqqmvwd1admmd87jrcfzbs5yk6yn3nnhqqa9niqcc0")))

(define-public crate-rmake-0.1.35 (c (n "rmake") (v "0.1.35") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ad3dm9ada1plad1vq0x738x6x6jpvz92vcnma3gkri623ddqy8v")))

(define-public crate-rmake-0.1.36 (c (n "rmake") (v "0.1.36") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "09g7x74hafg7pdcdsni7c7jmzfiwzka89a2mrb6gab6g28h1pm91")))

(define-public crate-rmake-0.1.37 (c (n "rmake") (v "0.1.37") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0y38h2p7rxg5m1f3s7ryainczsn7mkxj130msa365v0wlqrqnqy3")))

(define-public crate-rmake-0.1.38 (c (n "rmake") (v "0.1.38") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14qmhgbzcq133pzk1xzglvyxnb7pxcf24jdn50qisk4fyqa10h3q")))

