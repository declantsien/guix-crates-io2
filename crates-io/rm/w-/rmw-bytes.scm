(define-module (crates-io rm w- rmw-bytes) #:use-module (crates-io))

(define-public crate-rmw-bytes-0.1.12 (c (n "rmw-bytes") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)))) (h "14s8rj6wk1rnmy35hwvn77i3r4syn647k2hsk1d6m374xibkv6w7")))

(define-public crate-rmw-bytes-0.1.13 (c (n "rmw-bytes") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)))) (h "0n9syy7sd8hq9zwap6p723zzza8py75l2lr3iq322lvyggy7kiza")))

(define-public crate-rmw-bytes-0.1.14 (c (n "rmw-bytes") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)))) (h "0ih18p1ajk4b33y0r6bg241j9jhmdy2gail5kcyankxmkc8h4i6v")))

