(define-module (crates-io rm w- rmw-upnp) #:use-module (crates-io))

(define-public crate-rmw-upnp-0.1.0 (c (n "rmw-upnp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0xdncpssm9b4kkazs5nws2cphmn6x1kjs3wnrifaiwixdfpr8w4n")))

(define-public crate-rmw-upnp-0.1.1 (c (n "rmw-upnp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "097jc3mbk5q3lwa6kj33sky2f6pjjs1qjdcng1cr5rz6msbv2z1s")))

(define-public crate-rmw-upnp-0.1.4 (c (n "rmw-upnp") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "140gv7s5mj6c64ix48friqg5ip812g0b3p9qy75wjxkn2jr1c4ji")))

(define-public crate-rmw-upnp-0.1.5 (c (n "rmw-upnp") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jxpy4xalcap763w9k3kvilh5pi61m8fp2zhlbvjv4nx4mhvm9f4")))

(define-public crate-rmw-upnp-0.1.6 (c (n "rmw-upnp") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12rl7z3qywinfxg85bbfz872vpsdvy72bbj4ih7cr4bvv8q6h0ks")))

(define-public crate-rmw-upnp-0.1.7 (c (n "rmw-upnp") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1w9j15nad1580nb84idp5zk0kbv6rcb55m3yxcghyl0bb8vwwljx")))

(define-public crate-rmw-upnp-0.1.8 (c (n "rmw-upnp") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0996n31vwzr4ggz0wv4sxj1hypx3i1wf9fh13nv9ykxsrg8x0lzd")))

(define-public crate-rmw-upnp-0.1.9 (c (n "rmw-upnp") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0yb25nh3ls2hwm50jkf8kxqfb4hbviv4g072mi2pyz30j7v92kaz")))

(define-public crate-rmw-upnp-0.1.10 (c (n "rmw-upnp") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1xf7lhv2i2n6f9avg2mpd0bibjnwz66f47w3s52k6drs548lw3bs")))

(define-public crate-rmw-upnp-0.1.11 (c (n "rmw-upnp") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("tokio1" "attributes"))) (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (f (quote ("aio"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "023lp0qjk7mdvnnhbxr3rgl6vqmkh0y4z55k0n4pscl9cmmjk5jx")))

(define-public crate-rmw-upnp-0.2.1 (c (n "rmw-upnp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fern") (r "^0.6.1") (d #t) (k 2)) (d (n "igd") (r "^0.12.4") (f (quote ("aio"))) (d #t) (k 0) (p "igd_async_std")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1riqvkx665yvfh97xvskc3mq9y5yczy7x5s2980fmd26rca25jpp") (f (quote (("default"))))))

(define-public crate-rmw-upnp-0.2.2 (c (n "rmw-upnp") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fern") (r "^0.6.1") (d #t) (k 2)) (d (n "igd") (r "^0.12.4") (f (quote ("aio"))) (d #t) (k 0) (p "igd_async_std")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1wr5ank32dp197zd3lrdp6apwc99dgzy6kvrzp7qhk6mh8lf3ajd") (f (quote (("default"))))))

(define-public crate-rmw-upnp-0.2.3 (c (n "rmw-upnp") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fern") (r "^0.6.1") (d #t) (k 2)) (d (n "igd") (r "^0.12.4") (f (quote ("aio"))) (d #t) (k 0) (p "igd_async_std")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lg4mlcblinr5hv4nzgm3ibal56gdawzvamjqjw7d11n3qsx8mpb") (f (quote (("default"))))))

(define-public crate-rmw-upnp-0.2.4 (c (n "rmw-upnp") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "fern") (r "^0.6.1") (d #t) (k 2)) (d (n "igd") (r "^0.12.4") (f (quote ("aio"))) (d #t) (k 0) (p "igd_async_std")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dbkvdrnj6g4njfm5dpj13zp5r7wxr82jlpnqiiaikhm7p50y9qa") (f (quote (("default"))))))

