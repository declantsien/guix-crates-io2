(define-module (crates-io rm w- rmw-toml-fmt) #:use-module (crates-io))

(define-public crate-rmw-toml-fmt-0.1.0 (c (n "rmw-toml-fmt") (v "0.1.0") (d (list (d (n "toml") (r "^0.5.8") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0g6lcwcir5h8xjjb9xqv2dfbikglwrgdjgh76k42zfwpckb4faq5")))

