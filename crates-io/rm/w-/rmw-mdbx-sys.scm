(define-module (crates-io rm w- rmw-mdbx-sys) #:use-module (crates-io))

(define-public crate-rmw-mdbx-sys-0.9.3 (c (n "rmw-mdbx-sys") (v "0.9.3") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)))) (h "0npqf1xzad7jn8x102hz2kikf7r35pjgw4jid1finr382yx3zyka")))

