(define-module (crates-io rm #{1m}# rm1masm) #:use-module (crates-io))

(define-public crate-rm1masm-1.0.0 (c (n "rm1masm") (v "1.0.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0a3nc6c4igzhx63j0aklmqmpmzy23caa3p9fnvvax34la3qk23yl")))

(define-public crate-rm1masm-1.0.1 (c (n "rm1masm") (v "1.0.1") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0br5mnzikrzrr041x38hfh4wj1b4xbz47p8bw5vpb423klzi6n3x")))

(define-public crate-rm1masm-1.0.2 (c (n "rm1masm") (v "1.0.2") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0fd9k0mwb8vadni9xx1y6q9f941327bw7idnc2mq5w7l3kbjvday")))

(define-public crate-rm1masm-1.0.3 (c (n "rm1masm") (v "1.0.3") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0zixrhs9pybcajcz93hq180zrvfdmrisl967by44rxk1zzjm4a50")))

