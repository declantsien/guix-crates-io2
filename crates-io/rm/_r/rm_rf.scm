(define-module (crates-io rm _r rm_rf) #:use-module (crates-io))

(define-public crate-rm_rf-0.1.0 (c (n "rm_rf") (v "0.1.0") (h "1va7nphxfk98hjl9a6d4ix2d82jj6r60jjyv282s8kgki84yakdm")))

(define-public crate-rm_rf-0.1.1 (c (n "rm_rf") (v "0.1.1") (h "1k1pfjjlrhkpr5qwn4v2n5fhxzwqylf6d6s9rcqx79bkivg3bjh0")))

(define-public crate-rm_rf-0.2.0 (c (n "rm_rf") (v "0.2.0") (h "0prz576ydnibw0rbaqs14cm6fcjzm7zmmwcc4hz2p0kxy04wmw0d")))

(define-public crate-rm_rf-0.2.1 (c (n "rm_rf") (v "0.2.1") (d (list (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "13gy03qbr2w1f41m0jcy45cxx9x0xs3kdqgkipl78l492m7pwrv1")))

(define-public crate-rm_rf-0.2.2 (c (n "rm_rf") (v "0.2.2") (d (list (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1lry4ypya4q62dg7fji192f9xpvv55k29m194ili8amckdida723")))

(define-public crate-rm_rf-0.2.3 (c (n "rm_rf") (v "0.2.3") (d (list (d (n "stacker") (r "^0.1.5") (d #t) (k 0)))) (h "01ny6qaa3gb50kkb9scmixqs95abl0rrjnm1vr6b93ff3h6w11zv")))

(define-public crate-rm_rf-0.2.4 (c (n "rm_rf") (v "0.2.4") (d (list (d (n "stacker") (r "^0.1.5") (d #t) (k 0)))) (h "0jxhgwghgbfmkwha8b4aqdcbrfrcg0g1ychakcp3qk6xj3lzbxbb")))

(define-public crate-rm_rf-0.3.0 (c (n "rm_rf") (v "0.3.0") (d (list (d (n "stacker") (r "^0.1.5") (d #t) (k 0)))) (h "01ywxmm2y46yivy8mk65g0jgkml1qrzx7dx9ib1bisvwi73fy266")))

(define-public crate-rm_rf-0.4.0 (c (n "rm_rf") (v "0.4.0") (d (list (d (n "stacker") (r "^0.1.6") (d #t) (k 0)))) (h "10hihzyq0ivvxmi9kgx1sjy4aj97j1dp3fpxl0bj8r6iqhdf49kp")))

(define-public crate-rm_rf-0.4.1 (c (n "rm_rf") (v "0.4.1") (d (list (d (n "stacker") (r "^0.1.6") (d #t) (k 0)))) (h "1bj4m80awcfgwd5g4bkm6vdx24hgm58pgdkjkilj41dxv54mjr7v")))

(define-public crate-rm_rf-0.5.0 (c (n "rm_rf") (v "0.5.0") (d (list (d (n "stacker") (r "^0.1.6") (d #t) (k 0)))) (h "0nfj87alyprywxcn2xnjml4avwb6dc8v6d9jnq2fv7ajqsminxls")))

(define-public crate-rm_rf-0.5.1 (c (n "rm_rf") (v "0.5.1") (d (list (d (n "stacker") (r "^0.1.6") (d #t) (k 0)))) (h "0ywr1d2ac8d9qcrzz5a8nfgagnkwszkjsz411sfwhf96565j15h3")))

(define-public crate-rm_rf-0.6.0 (c (n "rm_rf") (v "0.6.0") (d (list (d (n "stacker") (r "^0.1.6") (d #t) (k 0)))) (h "08fpfb2b3k8vks1p7fxkc1icc7wl3varhf87dl70428xb8avcyh4")))

(define-public crate-rm_rf-0.6.1 (c (n "rm_rf") (v "0.6.1") (d (list (d (n "stacker") (r "^0.1.12") (d #t) (k 0)))) (h "0n66d1zkc59bibw5qh31pzwa2dif3bsvli9szl5a9pln9v6a4xlv")))

(define-public crate-rm_rf-0.6.2 (c (n "rm_rf") (v "0.6.2") (d (list (d (n "stacker") (r "^0.1.14") (d #t) (k 0)))) (h "07ppl6pa3pw1k7i87k05hf2b8lvazszfrh6zkvlx4bm1baivfhrl")))

