(define-module (crates-io rm -n rm-node) #:use-module (crates-io))

(define-public crate-rm-node-0.1.0 (c (n "rm-node") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "delete") (r "^1.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "0xim9186759g69n1kk1y2zgv1jfsimp89fn7y7nb1lyh0dzcq6d6")))

(define-public crate-rm-node-1.0.0 (c (n "rm-node") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("default"))) (d #t) (k 0)) (d (n "delete") (r "^1.0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1x67wswm4wnmjaj63sjd94ia40r0sgxd7z0z1ban71rkrlw1ck0f")))

