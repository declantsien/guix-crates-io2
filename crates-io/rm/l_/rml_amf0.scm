(define-module (crates-io rm l_ rml_amf0) #:use-module (crates-io))

(define-public crate-rml_amf0-0.1.0 (c (n "rml_amf0") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1ynw0dfscabylc8svhg9wkwymdv21d323ykf9cgw1ibmpjssrb4f")))

(define-public crate-rml_amf0-0.1.1 (c (n "rml_amf0") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "11xpabqm32ab89yb187s8xlxxiwjx32bnzk3kwssii43wmmnqad2")))

(define-public crate-rml_amf0-0.1.2 (c (n "rml_amf0") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "125ahnc2jxb792cg184vlgjg2a2f9jslzvkjcbqsx62ksjmh5vs9")))

(define-public crate-rml_amf0-0.2.0 (c (n "rml_amf0") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "199xwwj2qi4gx8j7xxaf9c2h9s4fcnc2g17jbr992fbll9wwvp47")))

(define-public crate-rml_amf0-0.3.0 (c (n "rml_amf0") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h62r89am117yvxrl3a1fg5ymcb8sa6vbr4hq4rjgx6isky1qmb3")))

