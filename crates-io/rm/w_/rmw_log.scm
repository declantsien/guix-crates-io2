(define-module (crates-io rm w_ rmw_log) #:use-module (crates-io))

(define-public crate-rmw_log-0.0.8 (c (n "rmw_log") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01xsa2i67gcql65jrsmg2fn5hh9q4zz3rdwk3wx6vfh1vx65pixv") (f (quote (("default"))))))

(define-public crate-rmw_log-0.0.9 (c (n "rmw_log") (v "0.0.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "10b0gyqdcz95mcp1pkxsflk8iaxnh2vgwdgqias4z6ypbxdyh1jb") (f (quote (("default"))))))

