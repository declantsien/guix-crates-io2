(define-module (crates-io rm w_ rmw_str) #:use-module (crates-io))

(define-public crate-rmw_str-0.0.1 (c (n "rmw_str") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)))) (h "1q2x4p98gjjsg89qqan211dl40z5vb9815rfpiqk6zvpc3z5d2xd")))

(define-public crate-rmw_str-0.0.2 (c (n "rmw_str") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)))) (h "1zs3p9jmdi8mx0nwc71451mvm5dqp2kzabvxk2bbs5fbzbnnddbb")))

(define-public crate-rmw_str-0.0.5 (c (n "rmw_str") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)))) (h "0nkg732wid928qgax657dkf04zbgjm2as8zp5hv0kgda4aac4160")))

(define-public crate-rmw_str-0.0.6 (c (n "rmw_str") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)))) (h "0x84fbxfnwcjcrjchm1w7m0fw24s2qsgh7ryg2j14lskidiz3683")))

(define-public crate-rmw_str-0.0.7 (c (n "rmw_str") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)))) (h "1kbmv8q60p2gncvlw5ryj7jswp0s23lv27mzm77362cbk2l207dy")))

