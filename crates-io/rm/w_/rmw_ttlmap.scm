(define-module (crates-io rm w_ rmw_ttlmap) #:use-module (crates-io))

(define-public crate-rmw_ttlmap-0.2.2 (c (n "rmw_ttlmap") (v "0.2.2") (d (list (d (n "async-lock") (r "^2.4.0") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-timer") (r "^0.7.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 2)) (d (n "smol") (r "^1.2.5") (d #t) (k 2)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0nlmwlb46d1wv5ns5zanp9w7ag5dgckpfn879fkvz9v8q3f6hhv4")))

