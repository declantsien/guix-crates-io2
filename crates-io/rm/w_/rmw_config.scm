(define-module (crates-io rm w_ rmw_config) #:use-module (crates-io))

(define-public crate-rmw_config-0.0.7 (c (n "rmw_config") (v "0.0.7") (d (list (d (n "const-str") (r "^0.4.3") (d #t) (k 0)) (d (n "env_dir") (r "^0.0.7") (d #t) (k 0)) (d (n "err") (r "^0.0.5") (d #t) (k 0) (p "rmw_err")) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rmw_str") (r "^0.0.7") (d #t) (k 0)))) (h "1qxl8hk7dg1v5qicmf1kz7pizprk13dpj8m271n938zxxnyhk8s6") (f (quote (("default"))))))

