(define-module (crates-io rm w_ rmw_err) #:use-module (crates-io))

(define-public crate-rmw_err-0.0.4 (c (n "rmw_err") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "04g68zizdapdsw4d9l61m191bmaqabxiakqsxrdm9qs6vmpryzb9")))

(define-public crate-rmw_err-0.0.5 (c (n "rmw_err") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1nxhp4n0ys9n8vsaz0pfrw5k1zvclbf9j3nnpgwrkmy5qflk63hl")))

