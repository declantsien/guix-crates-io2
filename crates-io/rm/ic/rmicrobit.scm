(define-module (crates-io rm ic rmicrobit) #:use-module (crates-io))

(define-public crate-rmicrobit-0.4.0 (c (n "rmicrobit") (v "0.4.0") (d (list (d (n "cortex-m-rtfm") (r "^0.4.3") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nrf51-hal") (r "^0.7.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "tiny-led-matrix") (r "^1.0") (d #t) (k 0)))) (h "02iiz2rj24a3b9b7g148vzd1qm9437zjbq4w9kgkx2bfla8kp39g")))

(define-public crate-rmicrobit-1.0.0 (c (n "rmicrobit") (v "1.0.0") (d (list (d (n "cortex-m-rtfm") (r "^0.5.0") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nrf51-hal") (r "^0.7.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "tiny-led-matrix") (r "^1.0") (d #t) (k 0)))) (h "15grlj6v9s250zwjib9nv1cx39a7pd8llcwh1lqd55hi83ks56a2")))

(define-public crate-rmicrobit-1.0.1 (c (n "rmicrobit") (v "1.0.1") (d (list (d (n "cortex-m-rtfm") (r "^0.5.0") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.5") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nrf51-hal") (r "^0.7.0") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.3") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "tiny-led-matrix") (r "^1.0") (d #t) (k 0)))) (h "0wvsaz5hzslbwrd1zjnrd14ajj780khm30h229n88njv2v21mc2w")))

