(define-module (crates-io rm -b rm-backup) #:use-module (crates-io))

(define-public crate-rm-backup-0.1.1 (c (n "rm-backup") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "figment") (r "^0.10.12") (f (quote ("toml" "env"))) (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "merkle_hash") (r "^3.5.0") (f (quote ("sha"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.7") (f (quote ("xxh3"))) (d #t) (k 0)))) (h "0x5l2xplvdx5cdkx9f8blp8hdbf0j0xjyr694q87f7y655y77az7")))

