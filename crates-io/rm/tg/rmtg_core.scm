(define-module (crates-io rm tg rmtg_core) #:use-module (crates-io))

(define-public crate-rmtg_core-0.1.0 (c (n "rmtg_core") (v "0.1.0") (h "0zk02059w53k0imjp2xfyqrvbbc73hyrsxrdczak7g7ddaw9774k")))

(define-public crate-rmtg_core-0.1.1 (c (n "rmtg_core") (v "0.1.1") (h "0n3fi85hdn2g3cwcqiycf9gvz3vjmhpf1k1v4sa16i2rprijmdb5")))

(define-public crate-rmtg_core-0.1.2 (c (n "rmtg_core") (v "0.1.2") (h "0bg90xq98izqzvgxdig3b9aa5ckgndcba6rqd4iqhrcx7yxl27ss")))

