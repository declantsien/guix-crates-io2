(define-module (crates-io rm tg rmtg_server) #:use-module (crates-io))

(define-public crate-rmtg_server-0.1.0 (c (n "rmtg_server") (v "0.1.0") (d (list (d (n "rmtg_core") (r "^0.1.0") (d #t) (k 0)))) (h "1vrxaphzijwm95xlx1vcfmw9y5bwy2fbdwb2jw4bpjsszxm049a6")))

(define-public crate-rmtg_server-0.1.1 (c (n "rmtg_server") (v "0.1.1") (d (list (d (n "rmtg_core") (r "^0.1.2") (d #t) (k 0)))) (h "0pqbcbszx57qwxkbwg8la8rknzbp14wivs6ylpb5n2x351zc9jw2")))

(define-public crate-rmtg_server-0.1.2 (c (n "rmtg_server") (v "0.1.2") (d (list (d (n "rmtg_core") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "132mm0k2nyi7xff9vh6f530p3jkw66h5av8mr1y1fka98bz9nac5")))

