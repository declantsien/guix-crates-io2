(define-module (crates-io rm es rmessenger) #:use-module (crates-io))

(define-public crate-rmessenger-0.0.1 (c (n "rmessenger") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "multipart") (r "^0.8.1") (d #t) (k 0)))) (h "004lp01jgbw2fmwbclicqi4zvp9a2m3jhgngpkwl843hq6s3l9fg")))

(define-public crate-rmessenger-0.0.2 (c (n "rmessenger") (v "0.0.2") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "multipart") (r "^0.8.1") (d #t) (k 0)))) (h "1c0134i7b60i9wrf69b7piy00da07237vaclhlwm3yira4p9rjdq")))

(define-public crate-rmessenger-0.0.3 (c (n "rmessenger") (v "0.0.3") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "multipart") (r "^0.8.1") (d #t) (k 0)))) (h "1s4szg0mz4dsml6sdc6ymy4yyvszhwmcqg32f4gqn2yf4vbbp3xj")))

(define-public crate-rmessenger-0.0.4 (c (n "rmessenger") (v "0.0.4") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "multipart") (r "^0.8.1") (d #t) (k 0)))) (h "13zz2kbn791g69ldj468g8fjyd0d4k7p5bhdzgnhidd8bi8r33dd")))

