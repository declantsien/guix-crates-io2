(define-module (crates-io rm es rmesh) #:use-module (crates-io))

(define-public crate-rmesh-0.1.0 (c (n "rmesh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1n8x5qjb4nany7is9j8b6355g3dlz29yw7ws4xvnx146d23pqzf1")))

(define-public crate-rmesh-0.1.1 (c (n "rmesh") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0mvqdbb0xgxvhzsca3kh7zffcssnbfayzvl6z2iivh9cxympld5p")))

(define-public crate-rmesh-0.1.2 (c (n "rmesh") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jzr2d66d1x5bxpbyccmfmmlmhzc4zfxzz9gn5ykrl6wxpbpa1jz")))

(define-public crate-rmesh-0.2.0 (c (n "rmesh") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1z1r980n40q3d220x5birc0pl2r5dbsy91xrrk9kdg5xph8f6kl5")))

(define-public crate-rmesh-0.3.0 (c (n "rmesh") (v "0.3.0") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "three-d") (r "^0.14.0") (d #t) (k 2)))) (h "1cmjmz7jz409am4cj8p69anjkmdk8lp1klhgmqrs56dhjyfigja1")))

(define-public crate-rmesh-0.3.1 (c (n "rmesh") (v "0.3.1") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1a55p04l226k54qsjl1ggr5wbx476h8dngrazfzpz1y0m83q7ijg") (y #t)))

(define-public crate-rmesh-0.3.2 (c (n "rmesh") (v "0.3.2") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0pyjly8r12g8adam41il9pwb9hwz4az537vhpnrnmbfwwidizvz0")))

(define-public crate-rmesh-0.3.3 (c (n "rmesh") (v "0.3.3") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1nasfkn0vvg8zlimlcldipf81s6n8cj2w2p0v6j1kgnxp3wh4fwz")))

(define-public crate-rmesh-0.3.4 (c (n "rmesh") (v "0.3.4") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0vxla28pjklhlmxazx6a765cwf2smcppqh6kzssj87l6fgs62f4i")))

(define-public crate-rmesh-0.3.5 (c (n "rmesh") (v "0.3.5") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1zfn2n6n7z2lxycjyjvgkiaskipvrwff04fq8j6kn458ar479q23")))

