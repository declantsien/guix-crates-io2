(define-module (crates-io ov -c ov-config) #:use-module (crates-io))

(define-public crate-ov-config-0.1.0 (c (n "ov-config") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qyq9p8ax3kgdyd0p3hvgaxpj7s3pxm29fz3d3vinkbc073qjicm")))

(define-public crate-ov-config-0.1.1 (c (n "ov-config") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07a7mf0fmrzby8vr33win6p8szwly770fjg6acgwmr2vmnhx2b6p")))

