(define-module (crates-io ov er overwatch) #:use-module (crates-io))

(define-public crate-overwatch-0.1.0 (c (n "overwatch") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cgqyj6fj1w54s8pnvxfad94g2lia10mnj9kp41bdv3i3812w198")))

