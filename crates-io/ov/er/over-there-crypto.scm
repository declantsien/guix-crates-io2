(define-module (crates-io ov er over-there-crypto) #:use-module (crates-io))

(define-public crate-over-there-crypto-0.1.0-alpha.1 (c (n "over-there-crypto") (v "0.1.0-alpha.1") (d (list (d (n "aead") (r "^0.2.0") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.5.0") (d #t) (k 0)) (d (n "aes-gcm-siv") (r "^0.4.1") (d #t) (k 0)) (d (n "aes-siv") (r "^0.2.0") (d #t) (k 0)) (d (n "lru") (r "^0.4.3") (d #t) (k 0)) (d (n "over-there-derive") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.2.0") (d #t) (k 0)))) (h "08mggxcd5sy3jf56li4g5f31bdaknh764cdhvpn6y1lgbv5nvdad") (y #t)))

