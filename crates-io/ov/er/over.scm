(define-module (crates-io ov er over) #:use-module (crates-io))

(define-public crate-over-0.1.0 (c (n "over") (v "0.1.0") (d (list (d (n "fraction") (r "^0.3.6") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "1bmvq8lyb1j30rvdf2rvis1dy36yp1c7kx6dzf96apq0kwx7l97r")))

(define-public crate-over-0.2.0 (c (n "over") (v "0.2.0") (d (list (d (n "fraction") (r "^0.3.6") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0xq45scr8a3al0wrilrlbjr4bglc5zf0q9h1bg7qs7gy7rx1pyy9")))

(define-public crate-over-0.3.0 (c (n "over") (v "0.3.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "00gczgkk2x95yy4v1lyd9yzymskcifn1wlwm5sgbjrwwhd0j7jc8")))

(define-public crate-over-0.4.0 (c (n "over") (v "0.4.0") (d (list (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "01n0jaq08v8kg3giqpngx7xfhpc3s292rvma7smhwq12l5pxw0kr")))

(define-public crate-over-0.4.1 (c (n "over") (v "0.4.1") (d (list (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "01mj3cjfr9ywcq0cb0vjicp2qv0yxaafcrn352d5kkzvwxxw6h5c")))

(define-public crate-over-0.5.0 (c (n "over") (v "0.5.0") (d (list (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "084mmpcl2k3fhj0mkfl2h1g4si2iy5ji6r9n9gdpf67hrms37yhw")))

(define-public crate-over-0.5.1 (c (n "over") (v "0.5.1") (d (list (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "02pp388l5rmjzwl0dbl0dymvpcpi4zx0sp913988pfjdcsgb9nk1")))

(define-public crate-over-0.5.2 (c (n "over") (v "0.5.2") (d (list (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "1vb3rkagzaj9mpd09i22mc1qg03crf2v58gmpj3n1ahidd54k0rw")))

(define-public crate-over-0.6.0 (c (n "over") (v "0.6.0") (d (list (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "0b4b1h8q73ghlbzb0mws8k8qhdcwxy3lx67ggvc3mdkvn0pahifs")))

(define-public crate-over-0.6.1 (c (n "over") (v "0.6.1") (d (list (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "0c6akmj3kmnyrj4khi9lg93nfd1r2qbkbkbq1h2pfwfq2isf30ix")))

(define-public crate-over-0.6.2 (c (n "over") (v "0.6.2") (d (list (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "114pm2r5flii6mifcnfkva46xh71k8chi6rz9lyjq2sbrg0ii2h1")))

(define-public crate-over-0.6.3 (c (n "over") (v "0.6.3") (d (list (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "num-bigint") (r "0.1.*") (d #t) (k 0)) (d (n "num-rational") (r "0.1.*") (d #t) (k 0)) (d (n "num-traits") (r "0.1.*") (d #t) (k 0)))) (h "122y1kbammw4i8krhrvxbhi7cacvinwc960xgbpc17by28frjxyi")))

(define-public crate-over-0.6.4 (c (n "over") (v "0.6.4") (d (list (d (n "lazy_static") (r "1.0.*") (d #t) (k 0)) (d (n "num-bigint") (r "0.2.*") (d #t) (k 0)) (d (n "num-rational") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 0)))) (h "0lcqmssshb9fxzzy7p170sci5qykcqf3hzjwln34nhs9lc47gncj")))

(define-public crate-over-0.6.5 (c (n "over") (v "0.6.5") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "03rd2is7q08h9cwsfc3dpsw25fxhycrn51yyn653m7yq8xn6jzmy")))

