(define-module (crates-io ov er overworld_dice) #:use-module (crates-io))

(define-public crate-overworld_dice-0.3.0 (c (n "overworld_dice") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jhhr05kn35l6xz8j6j3fa8c2mxk79wxqb1daz69fwdw0z2q04m5") (f (quote (("default" "serde"))))))

(define-public crate-overworld_dice-0.4.0 (c (n "overworld_dice") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0iai8qrndy9czx11ya5qd4jy53y3k5impghdg246asgllxsd2djc") (f (quote (("default" "serde"))))))

(define-public crate-overworld_dice-0.5.0 (c (n "overworld_dice") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01q5xx64hjwpn9gp19l4fq65allicbb5d0gqi8kxlmwdgikpjd6x") (f (quote (("default" "serde"))))))

