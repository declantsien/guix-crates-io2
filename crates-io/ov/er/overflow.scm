(define-module (crates-io ov er overflow) #:use-module (crates-io))

(define-public crate-overflow-0.1.0 (c (n "overflow") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1gwypbmy3l4m4hzaj4xszj976jc6fm6ibxkf9453p42flvkpxsas")))

