(define-module (crates-io ov er overdose) #:use-module (crates-io))

(define-public crate-overdose-0.1.0 (c (n "overdose") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.1") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n0p2abpfq95mixhqzryy22ha2da761i6vmb7vx98wj9aij160fl")))

