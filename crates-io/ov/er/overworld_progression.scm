(define-module (crates-io ov er overworld_progression) #:use-module (crates-io))

(define-public crate-overworld_progression-0.4.0 (c (n "overworld_progression") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gbrkz8wa7pczl009jp6a0wj1dv25yxa4bz39d38yqzhrgac4gpb") (f (quote (("default" "serde"))))))

(define-public crate-overworld_progression-0.5.0 (c (n "overworld_progression") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1slwi19q6spqr49j0n7p11rhy8m4g6v7r960ji3gan429hsil6x0") (f (quote (("default" "serde"))))))

