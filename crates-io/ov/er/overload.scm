(define-module (crates-io ov er overload) #:use-module (crates-io))

(define-public crate-overload-0.1.0 (c (n "overload") (v "0.1.0") (h "0sw6vp7xz66fxpghm1pm5dh53v7laynx16la2ndf1si73qg8pawg")))

(define-public crate-overload-0.1.1 (c (n "overload") (v "0.1.1") (h "0fdgbaqwknillagy1xq7xfgv60qdbk010diwl7s1p0qx7hb16n5i")))

