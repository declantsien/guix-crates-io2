(define-module (crates-io ov er overloading) #:use-module (crates-io))

(define-public crate-overloading-0.1.0 (c (n "overloading") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)))) (h "1cqssgv3h8p35v0mmva4j8wpvmacnprpvq9cv6wanpkl06hxc2xz")))

(define-public crate-overloading-0.1.1 (c (n "overloading") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)))) (h "18ji1pahh54b5cw7jradln9a466dxljh2l0qp020a77p4h905lgj")))

(define-public crate-overloading-0.1.2 (c (n "overloading") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)))) (h "0bnx9xclvbczxc5jvc7hiw5iwr4fjwmh849hmhqb8n3vxcjh1qaa")))

