(define-module (crates-io ov er overloaded_literals_macro) #:use-module (crates-io))

(define-public crate-overloaded_literals_macro-0.5.0 (c (n "overloaded_literals_macro") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1q21af112y25fxza76q12whl42lmi47wgpxl6rv7prwsf79hzc7z")))

(define-public crate-overloaded_literals_macro-0.5.1 (c (n "overloaded_literals_macro") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1y56ci3b37ka5811z42nvdi503fi3854ca83lpc9j3361p93w6l3")))

(define-public crate-overloaded_literals_macro-0.6.0 (c (n "overloaded_literals_macro") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0qyhmmpqxh2vly3x9mxdsvyfxb6m3zy65jl50r0vd2v60lzln9fb")))

(define-public crate-overloaded_literals_macro-0.7.0 (c (n "overloaded_literals_macro") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0xvg4ll8zxywxi17bjgmg8w17gdkibw5zc3n3a71ax3wcr901xa9")))

(define-public crate-overloaded_literals_macro-0.7.1 (c (n "overloaded_literals_macro") (v "0.7.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1kp2aczpizikfm2r51zi77vzbgf09dmdwjkn17cwmizgkm4sxv1y")))

(define-public crate-overloaded_literals_macro-0.8.0 (c (n "overloaded_literals_macro") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "166qpsssqdv05vv4nkcqfrlddah8h6x1y7nrhq9fm5c1az1668kv")))

(define-public crate-overloaded_literals_macro-0.8.1 (c (n "overloaded_literals_macro") (v "0.8.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0bnwznmc8d1ibjhmg4g3p9qlpdh6k03dcj87vbv88f4r29plqm2w")))

(define-public crate-overloaded_literals_macro-0.8.2 (c (n "overloaded_literals_macro") (v "0.8.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0j673bbicj1zxlcd38aa5k6h20g9qab8yn03qap2kvx9rs1nvyla")))

(define-public crate-overloaded_literals_macro-0.8.3 (c (n "overloaded_literals_macro") (v "0.8.3") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "001fn2al19vvb6yrr7q51fph1f9pj4q88jv0qf13jnirpkxv4m8c")))

