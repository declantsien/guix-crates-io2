(define-module (crates-io ov er overrider_build) #:use-module (crates-io))

(define-public crate-overrider_build-0.3.1 (c (n "overrider_build") (v "0.3.1") (d (list (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0llilq307066c02x5nh9rcjv4zkjm81wx8dfays3d48jk3ii8c16")))

(define-public crate-overrider_build-0.3.2 (c (n "overrider_build") (v "0.3.2") (d (list (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gg6b2p7l8nz1b3f5mzdg342v4xh6m9d8anhmcmsrys2zrw2i883")))

(define-public crate-overrider_build-0.4.0 (c (n "overrider_build") (v "0.4.0") (d (list (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vdkdfm9sjbp4iqzy5y90gc8brfk2787z7wmcs9znpz1pw9na5rs")))

(define-public crate-overrider_build-0.5.0 (c (n "overrider_build") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pw8lp9qxgr9bq0k8b2g0qwi7108jb1z1kgbh2rfnnc15ngglyl0")))

(define-public crate-overrider_build-0.5.1 (c (n "overrider_build") (v "0.5.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k8pw03yfk5c3iwnvawg8pkcahym79593bv0m8bdycjmr1dbwgx5")))

(define-public crate-overrider_build-0.7.0 (c (n "overrider_build") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k7vfhh42n0sdjk2gf5af78cz3jw7q90py7akl42nq4vlgmhk5qx")))

