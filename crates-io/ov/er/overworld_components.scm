(define-module (crates-io ov er overworld_components) #:use-module (crates-io))

(define-public crate-overworld_components-0.1.0 (c (n "overworld_components") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l5p92lszm1gbz9bx2lwag6kpc206bkkm6fmck301x82r23l9rw9") (f (quote (("default" "serde")))) (y #t)))

(define-public crate-overworld_components-0.2.0 (c (n "overworld_components") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fqzlp723bd350wi9n0d0fxxdx77r320a7284vvy44kw29hk09rb") (f (quote (("default" "serde"))))))

(define-public crate-overworld_components-0.2.1 (c (n "overworld_components") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03jv1wry5lw5aagz1r1kbz3kl2b4c28cm13vqppl070a4c68m90z") (f (quote (("default" "serde"))))))

(define-public crate-overworld_components-0.2.2 (c (n "overworld_components") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14bncrfym5ac24akycml01af9dhykh3ydizyas79qjqkqzzn155n") (f (quote (("default" "serde"))))))

(define-public crate-overworld_components-0.3.0 (c (n "overworld_components") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "137jqb8c5akjilg6n2i6gvymhzlv04zll2w2r4k4cih94c7awmlh") (f (quote (("default" "serde"))))))

