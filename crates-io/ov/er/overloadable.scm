(define-module (crates-io ov er overloadable) #:use-module (crates-io))

(define-public crate-overloadable-0.1.0 (c (n "overloadable") (v "0.1.0") (h "06pdllzaiaxrn49ccr0bbfzxcwfblspgy4nrxjzj6dk86gz17xgs")))

(define-public crate-overloadable-0.1.1 (c (n "overloadable") (v "0.1.1") (h "0mbxcmih48fd6gzvai9b7bgyqby6aszn3f5ys4a434ycnx7gv5ar")))

(define-public crate-overloadable-0.2.0 (c (n "overloadable") (v "0.2.0") (h "1ch3k1gzpwibscwskd0v4arqcpizk10gbwq1ibg45k61g1c9bhhc")))

(define-public crate-overloadable-0.3.0 (c (n "overloadable") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9d1i7dpaqa9ql2cjgaqvrmly4qj8k7xmj0liiahx47c41c2hmr")))

(define-public crate-overloadable-0.3.1 (c (n "overloadable") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0p7l2fmxfnvjm85kxyi571a5p7afkq5qmf50yxnmlnn4rjdw7ark")))

(define-public crate-overloadable-0.4.0 (c (n "overloadable") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1qnf5py77f2cm6wmn3nw1jq4xm416z1vafii5pq0nv7sap55cwb2")))

(define-public crate-overloadable-0.4.1 (c (n "overloadable") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("full"))) (d #t) (k 0)))) (h "15b42lz9fcsb00fmbva6848sj3976c8c18r3ggb0xxckj2wkbavs")))

