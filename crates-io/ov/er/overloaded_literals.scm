(define-module (crates-io ov er overloaded_literals) #:use-module (crates-io))

(define-public crate-overloaded_literals-0.5.0 (c (n "overloaded_literals") (v "0.5.0") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "tlist") (r "^0.6.2") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "1mx6909q8bkjqmkvlmp5w77ssl2l3snswg0jvhjqkhxba2bbgp59")))

(define-public crate-overloaded_literals-0.5.1 (c (n "overloaded_literals") (v "0.5.1") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "tlist") (r "^0.6.2") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "0n2ns7j1lwq1shiibwx14lly0ycf866frkcpyby8am80d97rqn4j")))

(define-public crate-overloaded_literals-0.5.2 (c (n "overloaded_literals") (v "0.5.2") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "^0.5.0") (d #t) (k 0)) (d (n "tlist") (r "^0.6.2") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "03zvciwlmngqbg0wikrk6rmhlinbh9raai66prbm8p1szfwgfrw5")))

(define-public crate-overloaded_literals-0.6.0 (c (n "overloaded_literals") (v "0.6.0") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "^0.6.0") (d #t) (k 0)) (d (n "tlist") (r "^0.6.2") (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "1pg6zhvk1pvjx33s44rk5rq0sjlkcrhcq7fs4f0gahwkqg361nam")))

(define-public crate-overloaded_literals-0.7.0 (c (n "overloaded_literals") (v "0.7.0") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.7.0") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "01cln42dc51yxapzipgjw88f4f5bqjsg9f0qiipa0aa83zvk0b2r")))

(define-public crate-overloaded_literals-0.7.1 (c (n "overloaded_literals") (v "0.7.1") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.7.1") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "0lgv5zq2ddpda77clmb6l075n9ajqk0hjvp7pln2f0pv4i1s69fa")))

(define-public crate-overloaded_literals-0.8.0 (c (n "overloaded_literals") (v "0.8.0") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.8.0") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "0aywfjhfgvkj7yvlhsbp7nkz26m3d257ss5lx7cw4k16ywgbn75h")))

(define-public crate-overloaded_literals-0.8.1 (c (n "overloaded_literals") (v "0.8.1") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.8.1") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "0j5m5dfs4sdvwzaq5qn2gjnrqflgcci55jmidhiqis537cb4n8pj")))

(define-public crate-overloaded_literals-0.8.2 (c (n "overloaded_literals") (v "0.8.2") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.8.2") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "0rr8sq60ibz9kb1rsphc1if9abpnwjlmjal5l179cy1asp4wzdsk")))

(define-public crate-overloaded_literals-0.8.3 (c (n "overloaded_literals") (v "0.8.3") (d (list (d (n "const-str") (r "^0.5.4") (d #t) (k 2)) (d (n "overloaded_literals_macro") (r "=0.8.3") (d #t) (k 0)) (d (n "tlist") (r "^0.7.0") (d #t) (k 0)))) (h "11angp26cdafvihlg5wli5i13v3qgc67ij0idggrh5cffm1bm0pc")))

