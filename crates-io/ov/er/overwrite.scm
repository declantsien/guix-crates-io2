(define-module (crates-io ov er overwrite) #:use-module (crates-io))

(define-public crate-overwrite-0.1.0 (c (n "overwrite") (v "0.1.0") (h "122hpr6b1jsxmkmnlv8jzcaasj81nkbkc5i30g9h6nprk7w3kzkb")))

(define-public crate-overwrite-0.2.0 (c (n "overwrite") (v "0.2.0") (h "0jhlsrzm6pnvxhzswpgk1s5jrsfwfanfjmsvf3p73zzmfqahf0rg") (f (quote (("overwrite_options") ("default" "overwrite_options"))))))

