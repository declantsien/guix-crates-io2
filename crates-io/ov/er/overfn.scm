(define-module (crates-io ov er overfn) #:use-module (crates-io))

(define-public crate-overfn-0.1.0 (c (n "overfn") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.24") (f (quote ("full"))) (d #t) (k 0)))) (h "0valm4d95lq8iqkcj6pss37i2nqcl63nx675k79hjrfwbq826lhf")))

(define-public crate-overfn-0.1.1 (c (n "overfn") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.24") (f (quote ("full"))) (d #t) (k 0)))) (h "0pj0whjsz0rzb840ikfj9ykv41ikp3s9ckya956kzxm3igjhifw5")))

(define-public crate-overfn-0.1.2 (c (n "overfn") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.24") (f (quote ("full"))) (d #t) (k 0)))) (h "0hgklg5vshc0rd2mwvf0wlcm272cyniaf5pj2l5qwmjqpnnkss2v")))

