(define-module (crates-io ov er overworld_affinity) #:use-module (crates-io))

(define-public crate-overworld_affinity-0.1.0 (c (n "overworld_affinity") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06n9d55w1qjmv3wkq1pyc0g211h5vn54kcxykgvx0mxr2cm4yz88") (f (quote (("default" "serde") ("common_three_elements") ("common_rps") ("common_elements") ("common" "common_rps" "common_three_elements" "common_elements") ("color_hints"))))))

