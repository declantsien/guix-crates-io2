(define-module (crates-io ov er overloadf_derive) #:use-module (crates-io))

(define-public crate-overloadf_derive-0.1.0 (c (n "overloadf_derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nqjbxixm0f5xyy227nw3b7n2spzrcpjbhm7g2nlh77vr31az1wa")))

(define-public crate-overloadf_derive-0.1.1 (c (n "overloadf_derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.20") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jhwi0n50gigp8lm8s6f71117ql22284m89jv4id3y6s21gsm9y9")))

(define-public crate-overloadf_derive-0.1.2 (c (n "overloadf_derive") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.20") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pi21f6rw6rql30md4s1s9r5jkrkxmra0ybmdz6hygvf9vkva98c")))

(define-public crate-overloadf_derive-0.1.3 (c (n "overloadf_derive") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0r3z7zsfsf5ddgj690nllqdv2hvqirap7vy72mn8r5irjranxjql")))

(define-public crate-overloadf_derive-0.1.4 (c (n "overloadf_derive") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b3n7z3rzwjjhfsz20hlci77v20299xl16rzk7hvvz4qkz2rgi7n")))

(define-public crate-overloadf_derive-0.1.5 (c (n "overloadf_derive") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.21") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02gliqxmv30r6amss0xmfs47masy238hk39ni2r1dks8g39w73x2")))

(define-public crate-overloadf_derive-0.1.6 (c (n "overloadf_derive") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.25") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lzmzq3yqc67409y02z3mzxa5i548yixw6s0yk6bazy05aa3rvjz")))

(define-public crate-overloadf_derive-0.1.7 (c (n "overloadf_derive") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.25") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16v1byavwssfsaanygrgywndwih3x5dbarivrxbmqxqpv97r9vgb")))

(define-public crate-overloadf_derive-0.1.8 (c (n "overloadf_derive") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ys0sahnhl0a4qqv6b86jzq55v06vfxiys24a40byws3fjgwfb4p")))

