(define-module (crates-io ov er overtone) #:use-module (crates-io))

(define-public crate-overtone-0.1.0 (c (n "overtone") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "uid") (r "^0.1.7") (d #t) (k 0)))) (h "18l8xvv0v23blfwbmsvr32rjrf5y6sjbinz8yh5xm8msrzx3m3rm") (r "1.74.0")))

(define-public crate-overtone-0.1.1 (c (n "overtone") (v "0.1.1") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0qa9n7zk1wlgyhvm6n46pbnnbzmc2hrqqn2vbk2m5za5xyb7hdi4") (r "1.74.0")))

