(define-module (crates-io ov er overflower) #:use-module (crates-io))

(define-public crate-overflower-0.1.0 (c (n "overflower") (v "0.1.0") (d (list (d (n "overflower_support") (r "^0.1.0") (d #t) (k 0)))) (h "16sksx65vnzkn4sakrb878gjy3g3dilnblgq2pk2fv9q21vgw4bm")))

(define-public crate-overflower-0.1.1 (c (n "overflower") (v "0.1.1") (d (list (d (n "overflower_support") (r "^0.1.0") (d #t) (k 0)))) (h "0ga3ahwq63l37la3kkqqwr5h1xwzb3npn7p724l9w4xmsc4xycjq")))

(define-public crate-overflower-0.1.2 (c (n "overflower") (v "0.1.2") (d (list (d (n "overflower_support") (r "^0.1.2") (d #t) (k 0)))) (h "1q6nx63rnpg9wnqivkjpwmq99kp8rn8h2pdhkcqbivm68277xapm")))

(define-public crate-overflower-0.1.3 (c (n "overflower") (v "0.1.3") (d (list (d (n "overflower_support") (r "^0.1.3") (d #t) (k 0)))) (h "0kcc7097ccwwb71kyvby66zw9nj5j0zyxfdyg74avy5wpczpz1nh")))

(define-public crate-overflower-0.1.5 (c (n "overflower") (v "0.1.5") (d (list (d (n "overflower_support") (r "^0.1.4") (d #t) (k 0)))) (h "0bcr7lfbgcj66r85zqydmpxlm9zgcsxn2ih4sgmkw00a1kw1aw8y")))

(define-public crate-overflower-0.2.0 (c (n "overflower") (v "0.2.0") (d (list (d (n "overflower_support") (r "^0.1.4") (d #t) (k 0)))) (h "1vpcbi6kwffi1sm6hgamcrv0p2s7k9s2kmypifdqjqkd7zfalarv")))

(define-public crate-overflower-0.3.1 (c (n "overflower") (v "0.3.1") (d (list (d (n "overflower_support") (r "^0.1.4") (d #t) (k 0)))) (h "1l3mw1nrn5rw8b15nvlgbr6nz7xngmpkl093n0r1v81c1ija6a4s")))

(define-public crate-overflower-0.3.3 (c (n "overflower") (v "0.3.3") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "14kg6vgpcl32dym6hcwf9w3g2wipxz8i6bb8h33f3dyx6c348ryw")))

(define-public crate-overflower-0.4.0 (c (n "overflower") (v "0.4.0") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "059x6l8mczh4llmsh8vzwn706yn1w1laykkzpvx2bkqag2xlk3r3")))

(define-public crate-overflower-0.4.1 (c (n "overflower") (v "0.4.1") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "14cbbn9jbvwdxa9715y5xdrr9mbsz2dqyk7kdjml3nalvbkv07y1")))

(define-public crate-overflower-0.4.2 (c (n "overflower") (v "0.4.2") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "00gnn53xhr7i010cbba5a67nxn2p8jp0f6skph87d83dsq1zjq3p")))

(define-public crate-overflower-0.4.3 (c (n "overflower") (v "0.4.3") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "0g3lk5kfx1mldxps0rx39axr1d2qfms29gcmicx9ch9as94dixj6")))

(define-public crate-overflower-0.4.4 (c (n "overflower") (v "0.4.4") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "0r6rlics1yv1a63n4i5p6i0s4s8i81jwvwx0sp7125jy0613c4bb")))

(define-public crate-overflower-0.4.5 (c (n "overflower") (v "0.4.5") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "0hdymjlvldcf6pnbpsmfs1d5hsinmchcf6lnd815n0r333pwzhxh")))

(define-public crate-overflower-0.4.6 (c (n "overflower") (v "0.4.6") (d (list (d (n "overflower_support") (r "^0.1.5") (d #t) (k 0)))) (h "1wb2jpxnmks8yrp55bvh8ls9fxsjnvm5bqpr6674jlb0i620lnv1")))

