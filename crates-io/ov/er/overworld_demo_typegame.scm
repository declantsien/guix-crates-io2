(define-module (crates-io ov er overworld_demo_typegame) #:use-module (crates-io))

(define-public crate-overworld_demo_typegame-0.1.0 (c (n "overworld_demo_typegame") (v "0.1.0") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "overworld") (r "^0.2.2") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "0ghw5v3jq83jv7sm9vim7b7fn8nbjk6ypy95rg6mhbbsr968ila9")))

(define-public crate-overworld_demo_typegame-0.2.0 (c (n "overworld_demo_typegame") (v "0.2.0") (d (list (d (n "iced") (r "^0.10.0") (d #t) (k 0)) (d (n "overworld") (r "^0.4.2") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.4") (d #t) (k 0)))) (h "0b1dj964vy9686bxzrkzf55l1knvgrj7qp0b0h7xral44rjbg1xf")))

