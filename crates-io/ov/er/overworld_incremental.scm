(define-module (crates-io ov er overworld_incremental) #:use-module (crates-io))

(define-public crate-overworld_incremental-0.4.0 (c (n "overworld_incremental") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dknv7wgqyscqi4xv3z13arpy9kvyg1b88hymzwfrc6w2dgjq8y0") (f (quote (("default" "serde"))))))

(define-public crate-overworld_incremental-0.5.0 (c (n "overworld_incremental") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16rf3s86ih10sr37n498y9qkn6v1q4980y8m8vqb8282xkiznmah") (f (quote (("default" "serde"))))))

