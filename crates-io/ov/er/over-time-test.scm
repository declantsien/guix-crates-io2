(define-module (crates-io ov er over-time-test) #:use-module (crates-io))

(define-public crate-over-time-test-0.1.0 (c (n "over-time-test") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.3") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.1.3") (d #t) (k 0)) (d (n "cw-multi-test") (r "^0.16.2") (d #t) (k 2)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "cw2") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0c4hxmbbidhwigdirib72i9xk8r3hjdb3kfbn3cq2jlbwrmfjg6n") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

