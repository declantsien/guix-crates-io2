(define-module (crates-io ov er overload-strings) #:use-module (crates-io))

(define-public crate-overload-strings-0.1.0 (c (n "overload-strings") (v "0.1.0") (h "02w25y7y978z3pkpg4rfxfbxlj8zw257g54d4n80fln59a4rnd5s")))

(define-public crate-overload-strings-0.1.1 (c (n "overload-strings") (v "0.1.1") (h "0s23blfh7lyyac81f6hva00mnj54rz790j7hdcvdmz1417yw1pp4")))

