(define-module (crates-io ov er over-there-utils) #:use-module (crates-io))

(define-public crate-over-there-utils-0.1.0-alpha.1 (c (n "over-there-utils") (v "0.1.0-alpha.1") (d (list (d (n "over-there-derive") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z9bjq09dzwn4b8xd2ly8akhpabij1dx0czyfq4qmvlgphm96vd6") (y #t)))

