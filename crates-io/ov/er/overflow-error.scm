(define-module (crates-io ov er overflow-error) #:use-module (crates-io))

(define-public crate-overflow-error-0.1.0 (c (n "overflow-error") (v "0.1.0") (h "04wfrd2q79sqbp4ysvp9nc4528galss4z808417i8cbsy3rgpzfl") (f (quote (("std") ("default" "std"))))))

(define-public crate-overflow-error-0.2.0 (c (n "overflow-error") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (k 0)))) (h "0bf3ccz0lzn1w4qrqk9x3zh70lb1kab72cxnpydmsl47agjxbfyn") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-overflow-error-0.3.0 (c (n "overflow-error") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (k 0)))) (h "0mrqgynx51601yx8y10wp4bpn7d79pwnw2vl3w5xx6i28r8ak35l") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-overflow-error-0.4.0 (c (n "overflow-error") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (k 0)))) (h "1558h3gxxinmjxgpkb0ggkddy8zqa5sn26jjpjfjkg4008jwv843") (f (quote (("std" "failure/std") ("default" "std"))))))

(define-public crate-overflow-error-0.4.1 (c (n "overflow-error") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.1") (k 0)))) (h "11sd1zi2qqm6226y265s0l0anqxr2d9zfphrxq63g2hhdagiwkpv") (f (quote (("std" "failure/std") ("default" "std"))))))

