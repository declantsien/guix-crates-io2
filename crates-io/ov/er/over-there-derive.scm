(define-module (crates-io ov er over-there-derive) #:use-module (crates-io))

(define-public crate-over-there-derive-0.1.0-alpha.1 (c (n "over-there-derive") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "016r3235cg0i5qwgb6rhdiq5cj8hphkaf7ymd41irn37lma9ggr2") (y #t)))

