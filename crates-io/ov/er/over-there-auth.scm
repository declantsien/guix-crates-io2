(define-module (crates-io ov er over-there-auth) #:use-module (crates-io))

(define-public crate-over-there-auth-0.1.0-alpha.1 (c (n "over-there-auth") (v "0.1.0-alpha.1") (d (list (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "over-there-utils") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0z7zkx6hkrdyrnk74gbwdhdvn11qpxx5yx6g0x9bkz7kq9df5spz") (y #t)))

