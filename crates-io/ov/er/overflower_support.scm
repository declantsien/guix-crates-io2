(define-module (crates-io ov er overflower_support) #:use-module (crates-io))

(define-public crate-overflower_support-0.1.0 (c (n "overflower_support") (v "0.1.0") (h "1aqldj1cd71341nhcn86vwg2y874r7zzdqi3nv3gvb9cffjqmsib")))

(define-public crate-overflower_support-0.1.2 (c (n "overflower_support") (v "0.1.2") (h "1arn1c87j42h5ns0ab66abj1hrcdkpyskpcwm01z0c3mplayih5c")))

(define-public crate-overflower_support-0.1.3 (c (n "overflower_support") (v "0.1.3") (h "13ixfkv49nwpndijaqd4fcn004qqnz90p6njhj0zg88y3fbkfs0f")))

(define-public crate-overflower_support-0.1.4 (c (n "overflower_support") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1djxbv44kyx5hs4v3mjjvrmpj75wd0ry8j9hckbx9sl9bc089nv4")))

(define-public crate-overflower_support-0.1.5 (c (n "overflower_support") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1d6zivkv0xx8aqp0b7dy42isf7c1jpyrqncfivrwf5x42iz65kav")))

