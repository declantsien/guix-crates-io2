(define-module (crates-io ov er overlap) #:use-module (crates-io))

(define-public crate-overlap-0.0.1 (c (n "overlap") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "01y1z6bfpmd9vqzw3rk5gwl0w6f5g05sb25bbapmbc7i2jg9h4l3")))

(define-public crate-overlap-0.0.2 (c (n "overlap") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0vc5vi05c6p0gbadf3ga4qw3iby9imh3xx1iwpi2qlcsfl7nibzk")))

