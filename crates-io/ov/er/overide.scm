(define-module (crates-io ov er overide) #:use-module (crates-io))

(define-public crate-overIDE-0.0.1 (c (n "overIDE") (v "0.0.1") (h "0fyp017dk5crl24rw058mqd07168hqrnld2m1m3dsgljrgdn9y92") (y #t)))

(define-public crate-overIDE-0.0.2 (c (n "overIDE") (v "0.0.2") (h "10mgymdnhr37nghfna28grkibyafrii22k4p128l1wa9hbh233q9") (y #t)))

(define-public crate-overIDE-0.0.5 (c (n "overIDE") (v "0.0.5") (h "05yjxzasrrmb20wp20fjw382akr7qshc4w220diskzl82y5ivmh6") (y #t)))

(define-public crate-overIDE-0.0.7 (c (n "overIDE") (v "0.0.7") (h "144nwz2lg6xnw0in1f3ad52i4825qwbqasp9q264qcyklljxf0fv") (y #t)))

(define-public crate-overIDE-0.1.1 (c (n "overIDE") (v "0.1.1") (h "0d6kwww8rp2vdknz9n82x1mq4n7yy5kjrw6zgq9s3ss9linfiv1w") (y #t)))

(define-public crate-overIDE-0.1.3 (c (n "overIDE") (v "0.1.3") (h "0vkcnxa10jkkj1walgvak9i3ks0dcvhfxfim30fd10dz7hvxi6qr") (y #t)))

(define-public crate-overIDE-0.1.4 (c (n "overIDE") (v "0.1.4") (h "1prz39hfs5jhdx6mv8ssbjf8fwhsjlaqacs31fr3zlgz63bmg027") (y #t)))

(define-public crate-overIDE-0.1.77 (c (n "overIDE") (v "0.1.77") (h "040gbp17jhp1731pqgkkqj1x1f1xd3gj6rx5l7f8rcsnlk36j7jg") (y #t)))

(define-public crate-overIDE-0.77.0 (c (n "overIDE") (v "0.77.0") (h "00r3v19vp6r835lksi40i5bd3wipz9rhn0zz75xfkwwc79ny3b5f") (y #t)))

(define-public crate-overIDE-7.7.18 (c (n "overIDE") (v "7.7.18") (h "1y9c1chhhvsm8mcs4kdp2c6ldvy87xg31l9g4v9ajwk7416ghzw5") (y #t)))

(define-public crate-overIDE-2018.7.7 (c (n "overIDE") (v "2018.7.7") (h "1gh2ykmzj8hd5qw7x5icxgx368m5053z38hdch7czavamj8yrb3r") (y #t)))

(define-public crate-overIDE-2019.12.13 (c (n "overIDE") (v "2019.12.13") (h "0cngigbmnaglr9bv16ssnyijy8b7bdavy6f8d53hxzwpr2yjsw4k") (y #t)))

(define-public crate-overIDE-9999.999.99 (c (n "overIDE") (v "9999.999.99") (h "0gqpjsz7jxhk3lq66sgxkhh7mc35fb66acrz5g95z0xx5h7wac8g") (y #t)))

(define-public crate-overIDE-9.9.9 (c (n "overIDE") (v "9.9.9") (h "10h66r7cr4mz8hidgsl470cfak9dkp9lf2bm1lx4xmzxdzv7kymr") (y #t)))

(define-public crate-overIDE-99999.99999.99999 (c (n "overIDE") (v "99999.99999.99999") (h "18avfhs43ds21jx1g56pzb15jnib7n5f9pwgb4l5fqpw3md5mh9y") (y #t)))

(define-public crate-overIDE-9999999.9999999.9999999 (c (n "overIDE") (v "9999999.9999999.9999999") (h "1ps4hg9hy77bhkncdlf0zlva77xpfk0gn3nrkaj26yff8bzz0blb") (y #t)))

(define-public crate-overIDE-999999999.999999999.999999999 (c (n "overIDE") (v "999999999.999999999.999999999") (h "02mqlzy6av54dmz2jhh8kfigw3dl8a0dx7zb51xlphmm2id402zn")))

