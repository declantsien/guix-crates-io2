(define-module (crates-io ov er overpass) #:use-module (crates-io))

(define-public crate-overpass-0.1.0 (c (n "overpass") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1ypwlk5rkv7k5ddhhwd7vangyyskrx9wwfnq7fsa2j0lz3dabv4p")))

