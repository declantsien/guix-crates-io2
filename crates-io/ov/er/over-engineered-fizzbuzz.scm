(define-module (crates-io ov er over-engineered-fizzbuzz) #:use-module (crates-io))

(define-public crate-over-engineered-fizzbuzz-0.1.0 (c (n "over-engineered-fizzbuzz") (v "0.1.0") (h "1z0kc0x3j4pd4q2yp5b9ddg14js48drlmbmzz4f6v0psrxg9w3r5")))

(define-public crate-over-engineered-fizzbuzz-0.1.1 (c (n "over-engineered-fizzbuzz") (v "0.1.1") (h "0gc4jb0wai7r5aygqssh2a73fl21jcy8ilmxr4pmf7b7rx081fyq")))

