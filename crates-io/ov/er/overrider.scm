(define-module (crates-io ov er overrider) #:use-module (crates-io))

(define-public crate-overrider-0.3.1 (c (n "overrider") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vpxgan5m7iyydjkasx0lsygi3fni5w3aqpc5j6c667vycn00lb5")))

(define-public crate-overrider-0.3.2 (c (n "overrider") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0b17vwnkmr0xhdgfi2wy4jy2v282bmqpnzb81m3215y7jr2aslg3")))

(define-public crate-overrider-0.4.0 (c (n "overrider") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14n9jf6gv419v366dgb193kakdscyhksrmb2cblzlwc37660czl5")))

(define-public crate-overrider-0.5.0 (c (n "overrider") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w43gp7rbzknlnwmzifnf8ys39xh6w7mivlai7nyvgfj72ia62q8")))

(define-public crate-overrider-0.5.1 (c (n "overrider") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yvafvwxmndld51lbc7kddr3mbindsvhgbvrm3ib7rjl6hwjd32k")))

(define-public crate-overrider-0.6.0 (c (n "overrider") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ymdldwk8b5mvgv5jgfjr937ssgrjrrmjxadqphxk4mqi5ignnd6")))

(define-public crate-overrider-0.6.1 (c (n "overrider") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wkj4i76gkigxwbyr3j8m54719mwxblr2rq7a01pb4rhqm17d0c4")))

(define-public crate-overrider-0.7.0 (c (n "overrider") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "051i57xaf2nh4xa2wwfjm7l81k1cqiqpbmasyvzzkbdncy6gpfam")))

