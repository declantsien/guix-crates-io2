(define-module (crates-io ov r_ ovr_overlay_sys) #:use-module (crates-io))

(define-public crate-ovr_overlay_sys-0.0.0 (c (n "ovr_overlay_sys") (v "0.0.0") (d (list (d (n "autocxx") (r "^0.21") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.21") (d #t) (k 1)) (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "normpath") (r "^0.3") (d #t) (k 1)))) (h "1aybiykw45b7hklyqqz0iplbz0qhfnm06wflqgc7v9l2in4f9d8n") (r "1.58")))

