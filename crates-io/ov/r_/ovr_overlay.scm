(define-module (crates-io ov r_ ovr_overlay) #:use-module (crates-io))

(define-public crate-ovr_overlay-0.0.0 (c (n "ovr_overlay") (v "0.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "ovr_overlay_sys") (r "=0.0.0") (d #t) (k 0)) (d (n "slice-of-array") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "056fpngkz2j5rqsr9hz7jjb1wgn8s5hxrdc0jilypmws8rf53jhb") (r "1.58")))

