(define-module (crates-io ov r- ovr-evm-precompile-ed25519) #:use-module (crates-io))

(define-public crate-ovr-evm-precompile-ed25519-2.0.0 (c (n "ovr-evm-precompile-ed25519") (v "2.0.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0") (f (quote ("alloc" "u64_backend"))) (k 0)) (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")))) (h "1jjx1p0x893s4iki9qy1kwsilil03xg6i0gf3b8sp9xlnyfyjckw")))

