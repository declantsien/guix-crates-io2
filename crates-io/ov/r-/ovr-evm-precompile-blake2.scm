(define-module (crates-io ov r- ovr-evm-precompile-blake2) #:use-module (crates-io))

(define-public crate-ovr-evm-precompile-blake2-2.0.0 (c (n "ovr-evm-precompile-blake2") (v "2.0.0") (d (list (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")) (d (n "ovr-evm-test-vector-support") (r "^1.0.0") (d #t) (k 2)))) (h "0f116829p02zxivm4xlyph7ab8jfxi6pad9wbnlv7cg6vx8q31f8")))

