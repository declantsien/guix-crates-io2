(define-module (crates-io ov r- ovr-mobile-sys) #:use-module (crates-io))

(define-public crate-ovr-mobile-sys-0.1.0 (c (n "ovr-mobile-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)))) (h "1jlzxrimms989lq63ha78zjpdjzdp978fvx9ij75ydaj5mabycr4")))

(define-public crate-ovr-mobile-sys-0.2.0 (c (n "ovr-mobile-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)))) (h "19454bqlq6hby5f28gqnjb0hb1a5gf4a01xnlsnnarmn150dx35j")))

(define-public crate-ovr-mobile-sys-0.3.0 (c (n "ovr-mobile-sys") (v "0.3.0") (h "09hv48q5cj2yqqx46i9r4wvg7mnjg7gq3fzrljqh3hx3grja46xp")))

(define-public crate-ovr-mobile-sys-0.3.1 (c (n "ovr-mobile-sys") (v "0.3.1") (h "1619misd4b43f7aqayzxx3i3arz9yf4c8rh51wsgh590kcwgkddp")))

(define-public crate-ovr-mobile-sys-0.4.0 (c (n "ovr-mobile-sys") (v "0.4.0") (h "1gjckjzjlg06ccg919ki52rvr2wrl3pmsqkr0q7n9z66x9zm36x6")))

