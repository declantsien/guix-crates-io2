(define-module (crates-io ov r- ovr-ruc) #:use-module (crates-io))

(define-public crate-ovr-ruc-1.0.8 (c (n "ovr-ruc") (v "1.0.8") (d (list (d (n "nix") (r "^0.23.1") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "time") (r "^0.3.6") (f (quote ("formatting"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1l3z9byfcrxrz8hxvfq5hg34lwh14w356q3npf6h0kp66h7l7871") (f (quote (("uau" "nix" "rand") ("rich" "uau" "cmd") ("default" "ansi") ("compact") ("cmd") ("ansi"))))))

