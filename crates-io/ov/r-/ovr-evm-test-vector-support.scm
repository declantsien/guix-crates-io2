(define-module (crates-io ov r- ovr-evm-test-vector-support) #:use-module (crates-io))

(define-public crate-ovr-evm-test-vector-support-1.0.0 (c (n "ovr-evm-test-vector-support") (v "1.0.0") (d (list (d (n "evm") (r "^0.33.1") (f (quote ("with-codec"))) (k 0)) (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nnn1sz56awzz10igxibl4z9qgnlhgrpgb0s48zvak0c4fi2d1xf")))

