(define-module (crates-io ov r- ovr-evm-precompile-simple) #:use-module (crates-io))

(define-public crate-ovr-evm-precompile-simple-2.0.0 (c (n "ovr-evm-precompile-simple") (v "2.0.0") (d (list (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")) (d (n "ovr-evm-test-vector-support") (r "^1.0.0") (d #t) (k 2)) (d (n "ripemd") (r "^0.1") (k 0)) (d (n "sp-io") (r "^6.0.0") (d #t) (k 0)))) (h "0k3sdk39l716iyi6x7nslbgdxnxv743s785rrdzv765z1sywnksg")))

