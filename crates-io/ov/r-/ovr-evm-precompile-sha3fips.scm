(define-module (crates-io ov r- ovr-evm-precompile-sha3fips) #:use-module (crates-io))

(define-public crate-ovr-evm-precompile-sha3fips-2.0.0 (c (n "ovr-evm-precompile-sha3fips") (v "2.0.0") (d (list (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")) (d (n "tiny-keccak") (r "^2.0") (f (quote ("fips202"))) (d #t) (k 0)))) (h "0sflvh914jqrr081abw5rx0pn76g4kf4cgd8qmqi8bzk9wv4r1gc")))

