(define-module (crates-io ov r- ovr-evm-precompile-curve25519) #:use-module (crates-io))

(define-public crate-ovr-evm-precompile-curve25519-1.0.0 (c (n "ovr-evm-precompile-curve25519") (v "1.0.0") (d (list (d (n "curve25519-dalek") (r "^4.0.0-pre.2") (f (quote ("u64_backend" "alloc"))) (k 0)) (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")))) (h "0yazfxa7dq2qv343rjwcqc82smgc7qy348napgskpyikym1wzgr0") (y #t)))

(define-public crate-ovr-evm-precompile-curve25519-1.0.1 (c (n "ovr-evm-precompile-curve25519") (v "1.0.1") (d (list (d (n "curve25519-dalek") (r "^4.0.0-pre.1") (f (quote ("u64_backend" "alloc"))) (k 0)) (d (n "fp-evm") (r "^3.0.0") (d #t) (k 0) (p "ovr-fp-evm")))) (h "1g0ni8cvnvz4f4qn0hspwr94glwbn5i4wb8qm9r1ynf5g1pfnv1z")))

