(define-module (crates-io ov r- ovr-sys) #:use-module (crates-io))

(define-public crate-ovr-sys-0.1.0 (c (n "ovr-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0x6ydajp8dz0kngss9pkdc5kda3h5f685wkmx6pp0z6lp7qk8z0l") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.1.1 (c (n "ovr-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "16rbfkl4dd1prddwwbxc9630mbz0720142prz7ak9pg0yl10ifxi") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.1.2 (c (n "ovr-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "11i8jy7p4w61jwi4f23b99pi1gq9nzwh3qxj3iwn5l19j7kvz5hk") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.1.3 (c (n "ovr-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0hx69csy47w8d5j3r0asfqkshrjp87y8jn3qgy3dnifk6r5fvrbw") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.2.0 (c (n "ovr-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1749lzq8a48h5qlw0mk8cd8kgpaar43xqb4b7cwcz7vc64hynwxg") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.2.1 (c (n "ovr-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0grmbfq0m63skjkji329l9hq78ig53myj5yam2qnf4h94zvy5mry") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.2.2 (c (n "ovr-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vk-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1csqbz898xczh9cixd4f7zvsgqgn16yc0d6qzlbk6dn4lw1ikdl4") (f (quote (("vulkan" "vk-sys") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

(define-public crate-ovr-sys-0.3.0 (c (n "ovr-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("core_1_0_3"))) (o #t) (k 0)) (d (n "vks") (r "^0.17") (f (quote ("khr_swapchain_67"))) (k 2)) (d (n "winapi") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0d17k3915vqjjaqc61q3aw3wy5bmlf5fcabm4bhs1s04zz6ry53x") (f (quote (("vulkan" "vks") ("opengl") ("directx" "winapi") ("default" "opengl") ("audio" "winapi"))))))

