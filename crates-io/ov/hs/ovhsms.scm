(define-module (crates-io ov hs ovhsms) #:use-module (crates-io))

(define-public crate-ovhsms-0.1.0 (c (n "ovhsms") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l22wpr4yrwhx1bbrdpb3c7zbk75iniqrhwi16wwibmxhbvrgyva") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/default-tls") ("default" "rustls-tls"))))))

