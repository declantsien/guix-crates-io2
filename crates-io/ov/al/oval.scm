(define-module (crates-io ov al oval) #:use-module (crates-io))

(define-public crate-oval-1.0.0 (c (n "oval") (v "1.0.0") (d (list (d (n "bytes") (r ">=0.0.1, <0.6") (o #t) (d #t) (k 0)))) (h "1f51q5ycc96s9h13nnk0r95djjc26x82r51n410q2lbmljc5wgas")))

(define-public crate-oval-1.0.1 (c (n "oval") (v "1.0.1") (d (list (d (n "bytes") (r ">=0.0.1, <0.6") (o #t) (d #t) (k 0)))) (h "02xqzzahx2jh5iy7cyw5jxwz7z10rjy6vjhi6vl7zb8r2wlqavw9")))

(define-public crate-oval-2.0.0 (c (n "oval") (v "2.0.0") (d (list (d (n "bytes") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0a78bm0zw4yzhd2ryp7qysxs5jwvnsq9024i1m2lcrqcf8rfyp0k")))

