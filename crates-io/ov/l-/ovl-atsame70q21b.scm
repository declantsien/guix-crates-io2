(define-module (crates-io ov l- ovl-atsame70q21b) #:use-module (crates-io))

(define-public crate-ovl-atsame70q21b-0.21.0 (c (n "ovl-atsame70q21b") (v "0.21.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "16imcj8p074cxjfz7jacd93nmgc2bkhlfmahz4ilsdcpx6gs0sv7") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.46.0")))

