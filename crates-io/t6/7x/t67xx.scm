(define-module (crates-io t6 #{7x}# t67xx) #:use-module (crates-io))

(define-public crate-t67xx-0.1.0 (c (n "t67xx") (v "0.1.0") (d (list (d (n "device-driver") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "09q000lppbaxz22i7m924g064hqamrlbr4djj9wljaq9bhc98d4c")))

(define-public crate-t67xx-0.1.1 (c (n "t67xx") (v "0.1.1") (d (list (d (n "device-driver") (r "^0.3.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "1zb9x33d0fiqbm1wpsfx9k66w283qkraiz1m9y90fbdwrqvwk1i4")))

