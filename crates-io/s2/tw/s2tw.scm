(define-module (crates-io s2 tw s2tw) #:use-module (crates-io))

(define-public crate-s2tw-1.0.1 (c (n "s2tw") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.5") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.1") (d #t) (k 0)))) (h "036sq436rvsangf1hw7wy8csba1j3kl1alvv4bf782va9a0pqn62")))

(define-public crate-s2tw-1.0.2 (c (n "s2tw") (v "1.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.7") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.3") (d #t) (k 0)))) (h "1hrf4b24ckhw94h7s9cq2m895jppb3dz5nfnpvx8mlmjisb66pn3")))

(define-public crate-s2tw-1.0.3 (c (n "s2tw") (v "1.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.0.7") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^1.1.3") (d #t) (k 0)))) (h "1azgskd280lf7nwwbbc6m17kh3wiy5na9psw5nwdxzwbfbl8kkvz")))

(define-public crate-s2tw-1.0.4 (c (n "s2tw") (v "1.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "08i83w0iq7fsgy7cfv8lsnvmq414bzlybsy95ryb0jnan2k4c2ml")))

(define-public crate-s2tw-1.0.5 (c (n "s2tw") (v "1.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "008735yra2w5973nw16igda217lhi9lavc95whfassi19fbpkf3y")))

(define-public crate-s2tw-1.0.6 (c (n "s2tw") (v "1.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1w3p0s8nizr1ss98lirz6p25jywm96hcvgvj988rzdjq6bpfqhn0")))

(define-public crate-s2tw-1.0.7 (c (n "s2tw") (v "1.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^2") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1pg9zmncqw18gwkfxcq5fsbb65w5xysajg5ajkwp9nbp5crwjsca")))

(define-public crate-s2tw-1.0.8 (c (n "s2tw") (v "1.0.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0v21a1ik7bnahh8ywf7vfbcaadz6nskp68ym7iain1lm2wg5n0zm")))

(define-public crate-s2tw-1.0.9 (c (n "s2tw") (v "1.0.9") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "070jmxyifw1m55j2fk6ir89s19bfc8fzihf608ylzwlkwakax39j")))

(define-public crate-s2tw-1.0.10 (c (n "s2tw") (v "1.0.10") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "191ldfp44y6xns2njyr72lkalj2aqi4b7qszcbls3mjrbil05ihr")))

(define-public crate-s2tw-1.0.11 (c (n "s2tw") (v "1.0.11") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "14vq5bz058yi50acpl1fpqwf1kylqxf39qsfjxk2cgdbjpbl8av1")))

(define-public crate-s2tw-1.0.12 (c (n "s2tw") (v "1.0.12") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0r0xqnazr34x0i93cg63ml776s3ybrkcqqzl47b1w91ibp8nq3h8")))

(define-public crate-s2tw-1.0.13 (c (n "s2tw") (v "1.0.13") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "0jksfn1zl1v4srjryn6q8vlh1f1snbfrc787nxpvvmnimz0q35rw")))

(define-public crate-s2tw-1.0.14 (c (n "s2tw") (v "1.0.14") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)))) (h "1qljncjckysclz5jdmbcfzdwqahwknnizjxyrw6d889gc6qavdyv")))

(define-public crate-s2tw-1.0.15 (c (n "s2tw") (v "1.0.15") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "1p5qhfg2r64nbc5wj40y66az5mgy3prjgsydpzppi9m3kqy8n6cc")))

(define-public crate-s2tw-1.0.16 (c (n "s2tw") (v "1.0.16") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3") (f (quote ("lazy_static_cache"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.2") (d #t) (k 0)))) (h "111nkkaam8ynzni6czw0iq4cp8hj8mwp1nm1s9fhcpb9w53103jn") (r "1.60")))

(define-public crate-s2tw-1.0.17 (c (n "s2tw") (v "1.0.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "1b4z7qywbp2r4gzx4pfrpkz0mnr04gdi1394k2l6hqpwblsa2jpz") (r "1.70")))

(define-public crate-s2tw-1.0.18 (c (n "s2tw") (v "1.0.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "1fp6q18in89i9r3nl2h1sq09h81xd1n7s35lp4sp0a7kybmnwh19") (r "1.70")))

(define-public crate-s2tw-1.0.19 (c (n "s2tw") (v "1.0.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "concat-with") (r "^0.2") (d #t) (k 0)) (d (n "opencc-rust") (r "^1.1") (f (quote ("static-dictionaries"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "07xffpap91m85wn4x6hmnh5n20kp8h2xacm14f58yknpp12syrfx") (r "1.70")))

