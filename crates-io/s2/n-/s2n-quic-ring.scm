(define-module (crates-io s2 n- s2n-quic-ring) #:use-module (crates-io))

(define-public crate-s2n-quic-ring-0.0.0 (c (n "s2n-quic-ring") (v "0.0.0") (h "0r2kvp9ggsxsnj90p8xh08l599zj8qmghpjqvsf9mr0cjp679knq")))

(define-public crate-s2n-quic-ring-0.1.0 (c (n "s2n-quic-ring") (v "0.1.0") (d (list (d (n "bolero") (r "^0.6") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (k 0)) (d (n "ring") (r "^0.16") (k 0)) (d (n "s2n-codec") (r "^0.1.0") (k 0)) (d (n "s2n-quic-core") (r "^0.1.0") (k 0)) (d (n "s2n-quic-core") (r "^0.1.0") (f (quote ("testing"))) (d #t) (k 2)) (d (n "zeroize") (r "^1") (k 0)))) (h "0z02pmkx66ya2iw2lh0p9ylhndsrcy0xq9vgi4d9cc1zaazh0kg9")))

(define-public crate-s2n-quic-ring-0.1.1 (c (n "s2n-quic-ring") (v "0.1.1") (d (list (d (n "bolero") (r "^0.6") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (k 0)) (d (n "ring") (r "^0.16") (k 0)) (d (n "s2n-codec") (r "^0.1.0") (k 0)) (d (n "s2n-quic-core") (r "=0.1.2") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1w8w2i2vmf7m226x8a8qx0zwv8kb31vx4qy2a75nbn35sdinlnhb")))

