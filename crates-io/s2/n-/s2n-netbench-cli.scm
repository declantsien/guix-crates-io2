(define-module (crates-io s2 n- s2n-netbench-cli) #:use-module (crates-io))

(define-public crate-s2n-netbench-cli-0.1.0 (c (n "s2n-netbench-cli") (v "0.1.0") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "063kn2ngrjbgjapwaj2w7d0cqiq8qgrqim409v7vxi2jcm0xqz2l") (r "1.74")))

(define-public crate-s2n-netbench-cli-0.1.1 (c (n "s2n-netbench-cli") (v "0.1.1") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0cdghwwnx938n75cdlakkn3gj7vi5jz2wz7l21w1yndfpw055bcw") (r "1.74")))

