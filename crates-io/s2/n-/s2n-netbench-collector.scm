(define-module (crates-io s2 n- s2n-netbench-collector) #:use-module (crates-io))

(define-public crate-s2n-netbench-collector-0.1.0 (c (n "s2n-netbench-collector") (v "0.1.0") (d (list (d (n "handlebars") (r "^4") (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (k 0)))) (h "0b8vyzw9q7ap2rjhmrnizknp2mqpaghiwvbyylf2rvzy3n16llha") (r "1.74")))

