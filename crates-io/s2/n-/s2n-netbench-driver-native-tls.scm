(define-module (crates-io s2 n- s2n-netbench-driver-native-tls) #:use-module (crates-io))

(define-public crate-s2n-netbench-driver-native-tls-0.1.0 (c (n "s2n-netbench-driver-native-tls") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "netbench-driver") (r "^0.1") (d #t) (k 0) (p "s2n-netbench-driver")) (d (n "openssl-sys") (r "^0.9") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-native-tls") (r "^0.3") (d #t) (k 0)))) (h "0mlq8rk3viqd8pxqarrgja3yghx5g159g2w9r81jkmskq3wbw37n") (r "1.74")))

