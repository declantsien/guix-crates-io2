(define-module (crates-io s2 n- s2n-quic-tls-default) #:use-module (crates-io))

(define-public crate-s2n-quic-tls-default-0.0.0 (c (n "s2n-quic-tls-default") (v "0.0.0") (h "1spjnl51d1497idsvn03mysnqy87v7bi25dmjc1hqrk3glmh2r4f")))

(define-public crate-s2n-quic-tls-default-0.1.0 (c (n "s2n-quic-tls-default") (v "0.1.0") (d (list (d (n "s2n-quic-rustls") (r "^0.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1qlvf10bd58n32aiwc5hvxk1v8y7qjd5a3mlcy8misgq5zqin75c")))

(define-public crate-s2n-quic-tls-default-0.1.1 (c (n "s2n-quic-tls-default") (v "0.1.1") (d (list (d (n "s2n-quic-rustls") (r "=0.1.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.1.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1lwv8w3px44sbxn3pkihxr38k9zk173snbdf049mvgp0msynpnhw")))

(define-public crate-s2n-quic-tls-default-0.1.2 (c (n "s2n-quic-tls-default") (v "0.1.2") (d (list (d (n "s2n-quic-rustls") (r "=0.1.2") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.1.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0kasr13ibcyvzl4v99a6ri31gaxjx6w1cdmj8651sya34d8hh2i0")))

(define-public crate-s2n-quic-tls-default-0.2.0 (c (n "s2n-quic-tls-default") (v "0.2.0") (d (list (d (n "s2n-quic-rustls") (r "=0.2.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.2.0") (d #t) (t "cfg(unix)") (k 0)))) (h "16r0wp5kgn7nfc1xmbr5hm36wgmnajc4l4x6cfpxrkhxsna45zm2")))

(define-public crate-s2n-quic-tls-default-0.2.1 (c (n "s2n-quic-tls-default") (v "0.2.1") (d (list (d (n "s2n-quic-rustls") (r "=0.2.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.2.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1kr8l5nzhirrk6v483n81yi8f0s2n3mall4n37505cd5nlq1pk1b")))

(define-public crate-s2n-quic-tls-default-0.3.0 (c (n "s2n-quic-tls-default") (v "0.3.0") (d (list (d (n "s2n-quic-rustls") (r "=0.3.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.3.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1n7ydjj6nrqiwbxxqhs38s6ijlya7g61bz0xhgilma31zilzk1qy")))

(define-public crate-s2n-quic-tls-default-0.4.0 (c (n "s2n-quic-tls-default") (v "0.4.0") (d (list (d (n "s2n-quic-rustls") (r "=0.4.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.4.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0praq43x4x29kcrmp5dhf935ahk1zj7p91l0ajshxawks10akhrl")))

(define-public crate-s2n-quic-tls-default-0.5.0 (c (n "s2n-quic-tls-default") (v "0.5.0") (d (list (d (n "s2n-quic-rustls") (r "=0.5.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.5.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0287kq6278m96g00smx8ld5jcgds96nkygw2masr2plfn1ahspha")))

(define-public crate-s2n-quic-tls-default-0.6.0 (c (n "s2n-quic-tls-default") (v "0.6.0") (d (list (d (n "s2n-quic-rustls") (r "=0.6.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.6.0") (d #t) (t "cfg(unix)") (k 0)))) (h "16g1px4iqfy4vq7v2d1mf3ql75a1pwfmf641shpbvfjpcj5pipyy")))

(define-public crate-s2n-quic-tls-default-0.7.0 (c (n "s2n-quic-tls-default") (v "0.7.0") (d (list (d (n "s2n-quic-rustls") (r "=0.7.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.7.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1281qcs4ldk6v0ms1wgcv3shb5a6lgrwlw1vq4gclsqvnk4qvv1k") (y #t)))

(define-public crate-s2n-quic-tls-default-0.7.1 (c (n "s2n-quic-tls-default") (v "0.7.1") (d (list (d (n "s2n-quic-rustls") (r "=0.7.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.7.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0khfippsqwghfjsi2cg5s5xab65hqq2lnfjx34pwqzjgd8vfa01f")))

(define-public crate-s2n-quic-tls-default-0.8.0 (c (n "s2n-quic-tls-default") (v "0.8.0") (d (list (d (n "s2n-quic-rustls") (r "=0.8.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.8.0") (d #t) (t "cfg(unix)") (k 0)))) (h "12syi1223qbngdrjlf9jxnsbv4ah6gn9bvk8m6xnzmy4jn5mzpdh") (r "1.56")))

(define-public crate-s2n-quic-tls-default-0.9.0 (c (n "s2n-quic-tls-default") (v "0.9.0") (d (list (d (n "s2n-quic-rustls") (r "=0.9.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.9.0") (d #t) (t "cfg(unix)") (k 0)))) (h "03zrl7fwvc89fzldcx9nqj9p2cz4nxc9ndar293zbv8vzj4k5qb8") (r "1.56")))

(define-public crate-s2n-quic-tls-default-0.10.0 (c (n "s2n-quic-tls-default") (v "0.10.0") (d (list (d (n "s2n-quic-rustls") (r "=0.10.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.10.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1rxggdsg22xdrs95w6qlwa7cxx5af35zgvis7fzmf0gcwyy92747") (r "1.56")))

(define-public crate-s2n-quic-tls-default-0.10.1 (c (n "s2n-quic-tls-default") (v "0.10.1") (d (list (d (n "s2n-quic-rustls") (r "=0.10.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.10.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1miixyjwm6h17fnszxndd8x25lbzxb5rg8jwgx2diijaq1ii2nyy") (r "1.56")))

(define-public crate-s2n-quic-tls-default-0.10.2 (c (n "s2n-quic-tls-default") (v "0.10.2") (d (list (d (n "s2n-quic-rustls") (r "=0.10.2") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.10.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1v2mz87aw41nndgj4s8yz88dlj3yfwlrs47aw38162aa4lqcw61a") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.11.0 (c (n "s2n-quic-tls-default") (v "0.11.0") (d (list (d (n "s2n-quic-rustls") (r "=0.11.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.11.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1s7zjnlkgwlhkn498ff4zvsfsp6agvmy6f517jq3994am8x78i7b") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.12.0 (c (n "s2n-quic-tls-default") (v "0.12.0") (d (list (d (n "s2n-quic-rustls") (r "=0.12.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.12.0") (d #t) (t "cfg(unix)") (k 0)))) (h "17jdghc58r89c5dgnrcfmhzw4xj5pb5lp88zlrh0yp49kf4yd6ci") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.13.0 (c (n "s2n-quic-tls-default") (v "0.13.0") (d (list (d (n "s2n-quic-rustls") (r "=0.13.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.13.0") (d #t) (t "cfg(unix)") (k 0)))) (h "17narvlzj3y2wbwb412vk49ngklx0g88vbrkc00c722mndykxz0d") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.14.0 (c (n "s2n-quic-tls-default") (v "0.14.0") (d (list (d (n "s2n-quic-rustls") (r "=0.14.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.14.0") (d #t) (t "cfg(unix)") (k 0)))) (h "09p55bgzds6kjbic0pdi2hcmnf4zrpcmcnkf4vn3vbk48wyvml8z") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.15.0 (c (n "s2n-quic-tls-default") (v "0.15.0") (d (list (d (n "s2n-quic-rustls") (r "=0.15.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.15.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0pj0mfy0lafbkl30cknbcn48jz7gjp2184fh4p2ax7z1vq0ql74j") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.16.0 (c (n "s2n-quic-tls-default") (v "0.16.0") (d (list (d (n "s2n-quic-rustls") (r "=0.16.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.16.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0v72bgkq0icwn52xz64njfa0pvh7zh5qv15nbgb52pkcr2jll3sm") (r "1.57")))

(define-public crate-s2n-quic-tls-default-0.17.0 (c (n "s2n-quic-tls-default") (v "0.17.0") (d (list (d (n "s2n-quic-rustls") (r "=0.17.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.17.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0f9x03i3y6llj1lrc1cbqf2xn1qb7n0d2d5gklk02m5b6dsfy09d") (r "1.60")))

(define-public crate-s2n-quic-tls-default-0.18.1 (c (n "s2n-quic-tls-default") (v "0.18.1") (d (list (d (n "s2n-quic-rustls") (r "=0.18.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.18.1") (d #t) (t "cfg(unix)") (k 0)))) (h "06zcvgsr5vnfhg70a8ywpzbr08aklh7kr2cig9wy52lhwql69bk6") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.18.2 (c (n "s2n-quic-tls-default") (v "0.18.2") (d (list (d (n "s2n-quic-rustls") (r "=0.18.2") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.18.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0si5wbqghmi4f6infgzqxsgahd8h1ap7dn5wlcmk2jg37a6m22xc") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.19.0 (c (n "s2n-quic-tls-default") (v "0.19.0") (d (list (d (n "s2n-quic-rustls") (r "=0.19.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.19.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0gxriqj4lcxd2wad6dd20k0ka3707y8480fl7j5c4bqyycva2ijs") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.20.0 (c (n "s2n-quic-tls-default") (v "0.20.0") (d (list (d (n "s2n-quic-rustls") (r "=0.20.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.20.0") (d #t) (t "cfg(unix)") (k 0)))) (h "00v9zxyz2hzzj3b23ysb67ac1c3l3q1ffaj50ds8y68aswpp735r") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.21.0 (c (n "s2n-quic-tls-default") (v "0.21.0") (d (list (d (n "s2n-quic-rustls") (r "=0.21.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.21.0") (d #t) (t "cfg(unix)") (k 0)))) (h "160wc3ssc8d9j1i34vajhh3k9hhgvf0wb0wkypbwpikbv15l3h48") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.22.0 (c (n "s2n-quic-tls-default") (v "0.22.0") (d (list (d (n "s2n-quic-rustls") (r "=0.22.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.22.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1gl4gsbzgdiv0219z2a3r95lrapb6shfa6z2zvfqq7lg6v0w6vi1") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.23.0 (c (n "s2n-quic-tls-default") (v "0.23.0") (d (list (d (n "s2n-quic-rustls") (r "=0.23.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.23.0") (d #t) (t "cfg(unix)") (k 0)))) (h "13h3ndh0zw8kmpp1qzn9v3nhvmqj1bp7nklg19yqp2qivak2gg4n") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.24.0 (c (n "s2n-quic-tls-default") (v "0.24.0") (d (list (d (n "s2n-quic-rustls") (r "=0.24.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.24.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0diivpiv51aw96v12fz8xdz8j4prcap4zgq68ydb0x0cqcyx1xxd") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.25.0 (c (n "s2n-quic-tls-default") (v "0.25.0") (d (list (d (n "s2n-quic-rustls") (r "=0.25.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.25.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0qi7wf9svh4s0fsh2w6lf12jnsppmmm0h9pb2x1hqh56nijmpsxj") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.26.0 (c (n "s2n-quic-tls-default") (v "0.26.0") (d (list (d (n "s2n-quic-rustls") (r "=0.26.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.26.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1883hb3hrnk99ppmxp6dj3vqwc4m4142lh3870zxpwl277165df2") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.27.0 (c (n "s2n-quic-tls-default") (v "0.27.0") (d (list (d (n "s2n-quic-rustls") (r "=0.27.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.27.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1fn2wy5iw48h27hnyksijvgk0h4i7gh99v428wq4s93921a5ppyw") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.28.0 (c (n "s2n-quic-tls-default") (v "0.28.0") (d (list (d (n "s2n-quic-rustls") (r "=0.28.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.28.0") (d #t) (t "cfg(unix)") (k 0)))) (h "148w8n3543bbc9k2dbaix6va2r4261dm5qq956cgfq7yw6lk4pkr") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.29.0 (c (n "s2n-quic-tls-default") (v "0.29.0") (d (list (d (n "s2n-quic-rustls") (r "=0.29.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.29.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1xnqawf6f4za6snghhvahsma7yynfmw8wj119ai03rmir7a0djmr") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.30.0 (c (n "s2n-quic-tls-default") (v "0.30.0") (d (list (d (n "s2n-quic-rustls") (r "=0.30.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.30.0") (d #t) (t "cfg(unix)") (k 0)))) (h "12rixp43w6np63xysyazh8ydvllk0cv40d4c9xqk54d00dshrvjg") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.31.0 (c (n "s2n-quic-tls-default") (v "0.31.0") (d (list (d (n "jobserver") (r "=0.1.26") (d #t) (t "cfg(unix)") (k 2)) (d (n "s2n-quic-rustls") (r "=0.31.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.31.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1i65zw8db3cc80x7chhxbrnz76brsflx53nzjpibkl0y0pxsx6wd") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.32.0 (c (n "s2n-quic-tls-default") (v "0.32.0") (d (list (d (n "jobserver") (r "=0.1.26") (d #t) (t "cfg(unix)") (k 2)) (d (n "s2n-quic-rustls") (r "=0.32.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.32.0") (d #t) (t "cfg(unix)") (k 0)))) (h "07wyr8vmg91gl2q3xj1zdfwlc88qg9d8w6gwjvzbrl0ws1bycyv3") (r "1.63")))

(define-public crate-s2n-quic-tls-default-0.33.0 (c (n "s2n-quic-tls-default") (v "0.33.0") (d (list (d (n "s2n-quic-rustls") (r "=0.33.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.33.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0sp67y2kvz1l70yz0qqj1j7gasd5kmqhx5bz6z7s78fbsrnjvpns") (r "1.71")))

(define-public crate-s2n-quic-tls-default-0.34.0 (c (n "s2n-quic-tls-default") (v "0.34.0") (d (list (d (n "s2n-quic-rustls") (r "=0.34.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.34.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0r92vw24hbbdag7rk18m8ss7w4h2ddrcglhwpqwqkb392wxbr9wb") (r "1.71")))

(define-public crate-s2n-quic-tls-default-0.35.0 (c (n "s2n-quic-tls-default") (v "0.35.0") (d (list (d (n "s2n-quic-rustls") (r "=0.35.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.35.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1pvalpa248zxi9f8gkhnq91mhkd3v766lq4dibpmz3hxvx0mnk3y") (y #t) (r "1.71")))

(define-public crate-s2n-quic-tls-default-0.35.1 (c (n "s2n-quic-tls-default") (v "0.35.1") (d (list (d (n "s2n-quic-rustls") (r "=0.35.1") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.35.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0rf4nkmsivqxbh3ghcdr1yfc18zhzsqlj9nd86n6qwx7304a7q3p") (r "1.71")))

(define-public crate-s2n-quic-tls-default-0.36.0 (c (n "s2n-quic-tls-default") (v "0.36.0") (d (list (d (n "s2n-quic-rustls") (r "=0.36.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.36.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1pvrcmdykrsij138sli82idl132ywi9b6qrhghamix587a1cv8zy") (r "1.71")))

(define-public crate-s2n-quic-tls-default-0.37.0 (c (n "s2n-quic-tls-default") (v "0.37.0") (d (list (d (n "s2n-quic-rustls") (r "=0.37.0") (d #t) (t "cfg(not(unix))") (k 0)) (d (n "s2n-quic-tls") (r "=0.37.0") (d #t) (t "cfg(unix)") (k 0)))) (h "1qyrcrwkpgv0r2vkkw57szzw162kw8y9knaxf699rvisxfmmii83") (r "1.71")))

