(define-module (crates-io s2 n- s2n-netbench-driver-s2n-quic) #:use-module (crates-io))

(define-public crate-s2n-netbench-driver-s2n-quic-0.1.0 (c (n "s2n-netbench-driver-s2n-quic") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "netbench-driver") (r "^0.1") (d #t) (k 0) (p "s2n-netbench-driver")) (d (n "s2n-quic") (r "^1") (d #t) (k 0)) (d (n "s2n-quic-core") (r "^0.32") (f (quote ("testing"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "1k5lyblydr1g4787vcgj56xi8g5hagv6gs2kw61slah9a50b7qy6") (r "1.74")))

