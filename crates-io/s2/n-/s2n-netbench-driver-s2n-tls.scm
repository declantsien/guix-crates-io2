(define-module (crates-io s2 n- s2n-netbench-driver-s2n-tls) #:use-module (crates-io))

(define-public crate-s2n-netbench-driver-s2n-tls-0.1.0 (c (n "s2n-netbench-driver-s2n-tls") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "netbench-driver") (r "^0.1") (d #t) (k 0) (p "s2n-netbench-driver")) (d (n "s2n-tls") (r "=0.0.39") (d #t) (k 0)) (d (n "s2n-tls-tokio") (r "=0.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "0yygyvsih1f2mq6jmdhqgyq2y2skvy13hiwrgirydznimh8x5ykh") (r "1.74")))

