(define-module (crates-io s2 n- s2n-netbench-scenarios) #:use-module (crates-io))

(define-public crate-s2n-netbench-scenarios-0.1.0 (c (n "s2n-netbench-scenarios") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("color" "suggestions"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")))) (h "0p074s1y8q91wyf79rplzwa9dcmd75h2zc78k8d1izaji01zixvn") (r "1.74")))

(define-public crate-s2n-netbench-scenarios-0.1.1 (c (n "s2n-netbench-scenarios") (v "0.1.1") (d (list (d (n "clap") (r "^2") (f (quote ("color" "suggestions"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "netbench") (r "^0.1.1") (f (quote ("builder"))) (d #t) (k 0) (p "s2n-netbench")))) (h "0kf2s1idqgah8ki71gmjcfh88lki4r5bppnmhh78dh538g8dgwyg") (r "1.74")))

