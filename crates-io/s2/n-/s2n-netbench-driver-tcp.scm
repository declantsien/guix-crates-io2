(define-module (crates-io s2 n- s2n-netbench-driver-tcp) #:use-module (crates-io))

(define-public crate-s2n-netbench-driver-tcp-0.1.0 (c (n "s2n-netbench-driver-tcp") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "netbench-driver") (r "^0.1") (d #t) (k 0) (p "s2n-netbench-driver")) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "1q79664mhbngn6544db8q91gp1hryacjkg1byky8zzwk8rv3n703") (r "1.74")))

