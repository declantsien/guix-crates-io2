(define-module (crates-io s2 n- s2n-tls-sys) #:use-module (crates-io))

(define-public crate-s2n-tls-sys-0.0.1 (c (n "s2n-tls-sys") (v "0.0.1") (h "0y3dmpkhll6rrivjxm75cxqa6b588z078z45i5xwjzv171ddf7xd")))

(define-public crate-s2n-tls-sys-0.0.2 (c (n "s2n-tls-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (d #t) (k 0)))) (h "1sz5c47128fydi48vcr35zjv41sn9fq29y4pyq36lsaz98vkrzrj") (f (quote (("quic") ("pq") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.3 (c (n "s2n-tls-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "05c17ycljm2b54zvbgzqnf9s5v219adw8mgbcry45aaq3azr481c") (f (quote (("quic") ("pq") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.4 (c (n "s2n-tls-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "1277vf69n0b8g7kq46xn9ddzxp07x4izaj25k7k8pff2s0z78ff3") (f (quote (("quic") ("pq") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.5 (c (n "s2n-tls-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)))) (h "096my5c1vz47wdy5d79hn7wb5dharych08k29rwbm2fwl8k1r98g") (f (quote (("quic") ("pq") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.6 (c (n "s2n-tls-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 1)))) (h "1aiyzja9hm4r2kj3j17q1kl29pxkp4j51xc4aw72zw7xiwfi0p0c") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.7 (c (n "s2n-tls-sys") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 1)))) (h "1ga6nv4xqj5n4nvswjnz6a74320qfp5bddmjj5i1mpgdbgqimdyv") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.8 (c (n "s2n-tls-sys") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "14xfigcqwgawjpfynhi1dm3pa7kwmibbfrgkwpav6cp6dgahky4x") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.9 (c (n "s2n-tls-sys") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0w29dqaj4wx325jh2qm6ipvzjfzhznr5h69gr6lqapg74jmyqxj9") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.10 (c (n "s2n-tls-sys") (v "0.0.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "082nrdwj59jxic1zx96kajw078dvivcng87favz0qh8r1sj6fmpl") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.11 (c (n "s2n-tls-sys") (v "0.0.11") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0kb9bg0hmlz90w9qigx6h6rvap1m3ilmwsxfin0jkbknh003zl1r") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.12 (c (n "s2n-tls-sys") (v "0.0.12") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1849zsi2xiqzkvj7hzqgz07ng2ins4xa8lql4rw9bj9kv40l8mwp") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.13 (c (n "s2n-tls-sys") (v "0.0.13") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1cby4mv0sw1js3i7x8lglfxgr5kbk0524m8ilb0xlc5i98isn2ax") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.14 (c (n "s2n-tls-sys") (v "0.0.14") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "176lpk9mk2xhlafgbhxnw19kbbyxgnir3ln4smzmml7jzqdz74xl") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.15 (c (n "s2n-tls-sys") (v "0.0.15") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "14ncmfjbrb888l2g89s831p381rh78k2r758agsjicjdpkq4z6jw") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.16 (c (n "s2n-tls-sys") (v "0.0.16") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "01a90hbnm2xg3h0qmc9q928fv7z5zk6gqrbiqa9rcinwarws9qgn") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.17 (c (n "s2n-tls-sys") (v "0.0.17") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0nfnwqqnhal8ypb95f2vxjhfsybh80r90as56xhxrl9f6551b90z") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.18 (c (n "s2n-tls-sys") (v "0.0.18") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0gh6n9mh50sfmh40p2fhz2zqbdn0j8vkv1j8g1x4zlgsk8791crn") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.19 (c (n "s2n-tls-sys") (v "0.0.19") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "046gfkp3dgq8i4gp08l88l3hpdnnz28fs25ka6hfm7v5zxyak2gb") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.20 (c (n "s2n-tls-sys") (v "0.0.20") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1llj0rbw9v90l9gfqa7x8jh9kaasxcgb7kfisa345hvid6r6mh4b") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.21 (c (n "s2n-tls-sys") (v "0.0.21") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1jyin8i30b7wm0apqj5vvdp74p0s3cb4n5ygs9xcb79ax412rnn2") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.22 (c (n "s2n-tls-sys") (v "0.0.22") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1fw1fz9izhmapwsbh1kq18mxv7xz7s1k61chlcjvgv82k2p0iyxd") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.23 (c (n "s2n-tls-sys") (v "0.0.23") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1l8vi9jj1wjrz2mr995b86lqm5dxvkwayfflyl5s3yicn0if7qy4") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.24 (c (n "s2n-tls-sys") (v "0.0.24") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1z3fy6cfv1fm0pjrd43skvnb2vwp375cd3ij22qlak03ps1g5cch") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.25 (c (n "s2n-tls-sys") (v "0.0.25") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9.68") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1dkxx1yjw9akbca08m6f2qk6vp2hvy8r4k68fj7hm2jpnmkybqgd") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls")))

(define-public crate-s2n-tls-sys-0.0.26 (c (n "s2n-tls-sys") (v "0.0.26") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1v26c11q3jvfg33vv7769s8hp1k8yfav3inqkpa0948fysn7bw64") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63")))

(define-public crate-s2n-tls-sys-0.0.27 (c (n "s2n-tls-sys") (v "0.0.27") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "01nxz309fxdmvsd77j97cvk16g3lf3j9yh6qbjbbmnv9vdj1h2hm") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.28 (c (n "s2n-tls-sys") (v "0.0.28") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0xm61r96sbzrsqgg7wjf5z3ibn4mhvdvzr2kbl0iagk8ip5n86z6") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.29 (c (n "s2n-tls-sys") (v "0.0.29") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1ac0ibz8kq7kp8w98sn5n7p6mlwqyn9vlc7843ng80zxz1h456rd") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.30 (c (n "s2n-tls-sys") (v "0.0.30") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1i6ajf1bww673mnw07srfd86lkk6dj8d843h007yvg27mkpl6fvh") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.31 (c (n "s2n-tls-sys") (v "0.0.31") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0hf04q4ss80rnq7ay34f3k5b2hfwg0lz9mn4jw94xvbk24psczl5") (f (quote (("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.32 (c (n "s2n-tls-sys") (v "0.0.32") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "152hwa2ijkl7fdl0rymsn6l6bm47gqws9ahzi02gkw1baxcxk57h") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.33 (c (n "s2n-tls-sys") (v "0.0.33") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1zx9dpn2aviaiccld8biynsvxi4q64fhwaaah2w98wxafrzc602r") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.34 (c (n "s2n-tls-sys") (v "0.0.34") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0ifcrs8kf2xi214qpillxwjl94jyanhid9mijv1vpwcrddjhl3c9") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.35 (c (n "s2n-tls-sys") (v "0.0.35") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0pkhqpm8m1rf6gl4pnh00kb4dj4k9f6f69p6b78ayi16iav4ya2w") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.36 (c (n "s2n-tls-sys") (v "0.0.36") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0rzrpf9ddzfwy5pnbarbky4s6kxlcm71xncci2d88rkfcr6cvy86") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.37 (c (n "s2n-tls-sys") (v "0.0.37") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0l2wzb5m77vdpqg0b2y93c7faml3jygs9cii65bdv3rf42jkxq43") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.38 (c (n "s2n-tls-sys") (v "0.0.38") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1pr6xxc0xbsznp1mw8crlgzg2x29cqm2afi8nc8mkvpgwkd1wrph") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.39 (c (n "s2n-tls-sys") (v "0.0.39") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "04f0pv1y6mc282rrs1cka2rvy8m5gr95mpkljid1gbxj498gywa8") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.40 (c (n "s2n-tls-sys") (v "0.0.40") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0hcbqc4bj4b71mfdd1fsb4vh69ks91q0b5mg1f7221917h0xrzxm") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.0.41 (c (n "s2n-tls-sys") (v "0.0.41") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "openssl-sys") (r "<=0.9") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0n3j4ixnqyam6cmj185lp2k5c8d0smm0rasmjx3a59qyfm788chz") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.0 (c (n "s2n-tls-sys") (v "0.1.0") (d (list (d (n "aws-lc-sys") (r "^0.12") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0daqpi3cbqmh0p162k7fa3rl6v1al8wp7bp8f3vn2y8yd644nc22") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.1 (c (n "s2n-tls-sys") (v "0.1.1") (d (list (d (n "aws-lc-sys") (r "^0.12") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cpv7d63pf1cvcmfa24ni7f0x5ilwf46pmav3jdyb2jh1p15h2pw") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq" "cmake") ("internal") ("default")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.2 (c (n "s2n-tls-sys") (v "0.1.2") (d (list (d (n "aws-lc-sys") (r "^0.12") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ydbbcqbp2rkiwizd6rw7xhq7bpnqnid9vfs137drh5xhwryypz5") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.3 (c (n "s2n-tls-sys") (v "0.1.3") (d (list (d (n "aws-lc-sys") (r "^0.13") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "1ganzcjp9szsq3z7y7vsi6vkqmc0cs0cjydlwjq93i3xhxayrgkh") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.4 (c (n "s2n-tls-sys") (v "0.1.4") (d (list (d (n "aws-lc-sys") (r "^0.13") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "1y4bpsndxp8cf4k5rjss784r61fn88sg6w8q7dbkai0j3ll7rawa") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.5 (c (n "s2n-tls-sys") (v "0.1.5") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "195c5kli5vjh6gb8n5jvjflfz99byb2nmgns0xw8mqpppdv3wd79") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.6 (c (n "s2n-tls-sys") (v "0.1.6") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "1wiya5cbi1c0kznqprq1ngsvd2i6ww7srbpbg3n0l4aqbh5m8lkj") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.1.7 (c (n "s2n-tls-sys") (v "0.1.7") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "1z7vlij88vp5863bmpykcfnkm23gsjwfcryi32i1k8d7dvasxhd0") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.0 (c (n "s2n-tls-sys") (v "0.2.0") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "05ky6pc1j2hv0pahrh5xsh42v6dv1r9j3vfwripx7pk0n17qhj79") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.1 (c (n "s2n-tls-sys") (v "0.2.1") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "1yrmhnfmqkz1zycs9z8ppzxrnggqz0zc2zprj2avfqvfc6a1z54h") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.2 (c (n "s2n-tls-sys") (v "0.2.2") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "0y46hwidhh5x4pl3xwq2yaizw57ydrglrgmfcmc2l5s14b1ff7cg") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.3 (c (n "s2n-tls-sys") (v "0.2.3") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)))) (h "066p7iasrf2ax45nhf7762vckar6s7r2zi0ykq83vk0vnbl2pc13") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.4 (c (n "s2n-tls-sys") (v "0.2.4") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)) (d (n "zeroize") (r "=1.7.0") (d #t) (k 2)))) (h "17frl90m4jn6ml4qckqwil1syngh6zz9a7nhk44ifyw4i45vk3gm") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.5 (c (n "s2n-tls-sys") (v "0.2.5") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)) (d (n "zeroize") (r "=1.7.0") (d #t) (k 2)))) (h "0rklilcp3wagdmr228jhrmrias5wcvava7i1p2bk88bvn37j2n13") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("fips" "aws-lc-rs/fips") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

(define-public crate-s2n-tls-sys-0.2.6 (c (n "s2n-tls-sys") (v "0.2.6") (d (list (d (n "aws-lc-rs") (r "^1.6.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 2)) (d (n "jobserver") (r "=0.1.26") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "=1.9.6") (d #t) (k 2)) (d (n "zeroize") (r "=1.7.0") (d #t) (k 2)))) (h "0pb2dhsd6a35idq7w0cpz4hg86swzm144f22br07pz9m3sl7rfkn") (f (quote (("unstable-renegotiate") ("unstable-npn") ("unstable-ktls") ("unstable-fingerprint") ("unstable-crl") ("stacktrace") ("quic") ("pq") ("internal") ("fips" "aws-lc-rs/fips") ("default") ("cmake")))) (l "s2n-tls") (r "1.63.0")))

