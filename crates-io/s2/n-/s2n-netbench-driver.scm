(define-module (crates-io s2 n- s2n-netbench-driver) #:use-module (crates-io))

(define-public crate-s2n-netbench-driver-0.1.0 (c (n "s2n-netbench-driver") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)) (d (n "netbench") (r "^0.1") (d #t) (k 0) (p "s2n-netbench")) (d (n "probe") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "12lrvdv0jq9chzggwc67cz2w2y3gcj77n3mvcyvz0pyypm4xj059") (r "1.74")))

