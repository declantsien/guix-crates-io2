(define-module (crates-io s2 pa s2pac_ch32v103) #:use-module (crates-io))

(define-public crate-s2pac_ch32v103-0.1.0 (c (n "s2pac_ch32v103") (v "0.1.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.12.2") (o #t) (d #t) (k 0)) (d (n "svd2rust") (r "^0.31.5") (k 1)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1w7f06syiixh5xhiy6s907ihlhmwg11dfg8w9nrfqbi76x4c2gm1") (f (quote (("default" "rt")))) (s 2) (e (quote (("rt" "dep:riscv-rt"))))))

