(define-module (crates-io s2 pn s2png) #:use-module (crates-io))

(define-public crate-s2png-0.11.0 (c (n "s2png") (v "0.11.0") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0nl564lj4vg9z23h8fn3qf84qpmv45kxlsxppdbkxjyknb3sdv2j")))

(define-public crate-s2png-0.11.1 (c (n "s2png") (v "0.11.1") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1f4mhm0xpji320kx6rsn1kknw2bir0k5hcmjwk3jv1yfsjxwi5fl")))

