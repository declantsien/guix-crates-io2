(define-module (crates-io s2 rs s2rs-derive) #:use-module (crates-io))

(define-public crate-s2rs-derive-0.1.0 (c (n "s2rs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "12g8sz0xbjc4gfkjcmn1ycclxp7paqcmzi4aq35s5z47lh09dfbr")))

(define-public crate-s2rs-derive-0.1.1 (c (n "s2rs-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1l01d00j70y0pyajcihxr1jyh8lkysdw1adzvkccz3nnf7sryify")))

(define-public crate-s2rs-derive-0.1.2 (c (n "s2rs-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjpvgz8a6zp5p6l3h81vgw2d2f3hh2p81xra6157ikjw76mpy1b")))

