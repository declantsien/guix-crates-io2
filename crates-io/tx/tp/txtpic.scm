(define-module (crates-io tx tp txtpic) #:use-module (crates-io))

(define-public crate-txtpic-1.0.0 (c (n "txtpic") (v "1.0.0") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "03r8ycpf5qjkxalrf7y11xnqhkanrz5l4lrrp8p8v7w3b24hzrdp")))

(define-public crate-txtpic-1.0.1 (c (n "txtpic") (v "1.0.1") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1bcac31nmj98n4hm934s18mn10vhxb0q4hqmrpsla3x28anf7xr0")))

(define-public crate-txtpic-1.2.0 (c (n "txtpic") (v "1.2.0") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "01apbgppysr9n9v48aha53ljzvjjx6jdkrasgjqmydyfmrf1ph7z")))

(define-public crate-txtpic-1.2.1 (c (n "txtpic") (v "1.2.1") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1lyw01pwqysc444b70kb5szi1frvr2pjrdw8zd3jl66ipq2ril99")))

(define-public crate-txtpic-1.2.2 (c (n "txtpic") (v "1.2.2") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1jihi6w75x19ry0a5fgpkq3kkjbdd0g6x8sja6wyahxvc0klnpmn")))

(define-public crate-txtpic-1.2.3 (c (n "txtpic") (v "1.2.3") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1pi772354rzyksvkpq71szm74j67nz6mfiq8jhr0gi7abaaykzrx")))

(define-public crate-txtpic-1.2.4 (c (n "txtpic") (v "1.2.4") (d (list (d (n "bmp") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.22.2") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1q73y3h4drb9viyda0i726qzbrwgnfz7fvqy0n4i7jf2z884d5hj")))

