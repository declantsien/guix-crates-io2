(define-module (crates-io tx t_ txt_writer) #:use-module (crates-io))

(define-public crate-txt_writer-0.1.0 (c (n "txt_writer") (v "0.1.0") (h "144xij6hwqkwkqkg6qxp38li77h7wnbpgmmcvaw2zjdmb7ph7scs")))

(define-public crate-txt_writer-0.1.1 (c (n "txt_writer") (v "0.1.1") (h "0dfb2zp52b3n55yv0g225r3y4p2mqacgxys475y3jsigb42xm8sj")))

(define-public crate-txt_writer-0.1.2 (c (n "txt_writer") (v "0.1.2") (h "1h114m4mpnx28y1y2li057y1sw3s2025xx9m82sv0y83viay6jny")))

(define-public crate-txt_writer-0.1.3 (c (n "txt_writer") (v "0.1.3") (h "0y2197dkhyrqyhf3127wzrsifg0g75v5fp2hmz0d5xcgxsqx5ams")))

(define-public crate-txt_writer-0.1.4 (c (n "txt_writer") (v "0.1.4") (h "0l5y0gjfbz2dljd4cr71x37b6ik0s7a00vwgfz4lp06bnqcclyhs")))

