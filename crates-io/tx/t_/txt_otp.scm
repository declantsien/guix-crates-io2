(define-module (crates-io tx t_ txt_otp) #:use-module (crates-io))

(define-public crate-txt_otp-1.0.2 (c (n "txt_otp") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1szapp8n3lpmkh1qzkwmh4mp78yj2smprs7vl6slsn1kxavgkyqp")))

(define-public crate-txt_otp-2.0.0 (c (n "txt_otp") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "11fp9sk8pi9imkv68icnff82hr7ncbqn9k97cn3dcq64kix7bmqv")))

