(define-module (crates-io tx t_ txt_processor) #:use-module (crates-io))

(define-public crate-txt_processor-0.1.1 (c (n "txt_processor") (v "0.1.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1q74xkacjii7jz2a5sciwxbl1fzs82cm320j0xmcd0l50xn7k8y8")))

(define-public crate-txt_processor-0.1.2 (c (n "txt_processor") (v "0.1.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1dnrf42kcmnxiadkbc98wnbazvwacizmld01y4rmy0h1l0bbzz86")))

(define-public crate-txt_processor-0.1.3 (c (n "txt_processor") (v "0.1.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "13cnm6ivzc7jbwjkpjdddfhxfpgn0qnqaddc0ahidmmdqmw7c57s")))

(define-public crate-txt_processor-0.1.4 (c (n "txt_processor") (v "0.1.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1hl12vkn0dapg2j2qsdwx0nhlyssb02q17kncbx562pw9wcf8k8s")))

