(define-module (crates-io tx tf txtframe) #:use-module (crates-io))

(define-public crate-txtframe-0.1.0 (c (n "txtframe") (v "0.1.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "1iznhn4q4fb8r3xy64zkvrlsid53h09liv944kkihf7cgcih33f9")))

(define-public crate-txtframe-0.1.1 (c (n "txtframe") (v "0.1.1") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "0ldr01lm1m2aa1zcxkg2czjxjf0hlsjsm2r0k9ksy5qa7cndal30")))

(define-public crate-txtframe-0.1.2 (c (n "txtframe") (v "0.1.2") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "1hz6mqlpk66bk0ig83gy8mzaj9crnzi2ipz9hw8jylzf9hxnqkr3")))

(define-public crate-txtframe-0.2.0 (c (n "txtframe") (v "0.2.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "01b4gfr60d22k4z5p4daj7ivlb35b440rn2bisshy8jfpa92z6ma") (f (quote (("default" "color") ("color"))))))

(define-public crate-txtframe-0.3.0 (c (n "txtframe") (v "0.3.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)))) (h "09zbr225dhpiinx43k594h18cmxdpvj5rlhqi541n7f1g3i1fbw3") (f (quote (("newline") ("default" "color") ("color"))))))

(define-public crate-txtframe-0.4.0 (c (n "txtframe") (v "0.4.0") (d (list (d (n "smallstr") (r "^0.3.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)))) (h "0wg7yj0f1hlmdlahqj203x2dyig1dbpf9ysyxwzyiijl28vsbv3v") (f (quote (("newline") ("esc") ("default" "color") ("color"))))))

