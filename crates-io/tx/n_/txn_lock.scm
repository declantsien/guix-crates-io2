(define-module (crates-io tx n_ txn_lock) #:use-module (crates-io))

(define-public crate-txn_lock-0.1.0 (c (n "txn_lock") (v "0.1.0") (d (list (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "086qlq7c3h5f5xk5cdiqk9m977hdy7f1wmcs10xnzji7y6mmkrls") (y #t)))

(define-public crate-txn_lock-0.1.1 (c (n "txn_lock") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "0by6dbkdf0prdinmb41xjh7dhm04a65pghaz9n652vsbfcv507yw")))

(define-public crate-txn_lock-0.2.0 (c (n "txn_lock") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "058mh62jd5i7ddbr800w86iq1ygly7jra00szhqxgg1n1jr65nm0")))

(define-public crate-txn_lock-0.3.0 (c (n "txn_lock") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "1qxbmds4gxyx96z1gwda620sg9lqxl1z408zi68v8bziyvdqk0i3") (f (quote (("logging" "log")))) (y #t)))

(define-public crate-txn_lock-0.3.1 (c (n "txn_lock") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "16fp41ws5wkcfs79c8sd7cwxfd0j58g3w7ikwhrmg7gys2m2bc3l") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.3.2 (c (n "txn_lock") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "0jyj3lf8fgcsknrxkrsvcyd0dg9w6rl4qi0f6kphdas7dlbpa7r0") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.3.3 (c (n "txn_lock") (v "0.3.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "10yhlkah9c2s60piby38kw9szgrrxkfw2s64z3kgn6661ccznd85") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.4.0 (c (n "txn_lock") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "0f5a5d5a32f1lxnnbwrvdnqm7m42z1bvwyr7x44zc4i2siwk4w2q") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.5.0 (c (n "txn_lock") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("sync"))) (d #t) (k 0)))) (h "1wrqm75cq9hmnabipd4zh4xzhhc9lz4blj8ml39y3iqk3ranm1g5") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.5.1 (c (n "txn_lock") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("sync"))) (d #t) (k 0)))) (h "0l753bym45kghb2lxmymcqbwb8dkkdqaa3rh0ah3g1gjvnc9maq2") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.6.0 (c (n "txn_lock") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("sync"))) (d #t) (k 0)))) (h "0sn1cbiqkpgrp1w62dgy2yc2cy0ikpcqww8n7a20c48dbq4kxpcy") (f (quote (("logging" "log"))))))

(define-public crate-txn_lock-0.7.0 (c (n "txn_lock") (v "0.7.0") (d (list (d (n "collate") (r "^0.3") (d #t) (k 0)) (d (n "ds-ext") (r "~0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hr-id") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "0vr3kgy9c0xs0i50ww8fyfdps6yphyfpgdm6i8z7p2x2l525mbnh") (f (quote (("logging" "log") ("id" "hr-id") ("all" "hr-id" "logging"))))))

(define-public crate-txn_lock-0.7.1 (c (n "txn_lock") (v "0.7.1") (d (list (d (n "collate") (r "^0.3") (d #t) (k 0)) (d (n "ds-ext") (r "~0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hr-id") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("sync"))) (d #t) (k 0)))) (h "0wnj98sc9h8dasja3s9f2nlzpqa1rhjl9pkdcg1sf22k1fpikmmb") (f (quote (("logging" "log") ("id" "hr-id") ("all" "hr-id" "logging"))))))

(define-public crate-txn_lock-0.8.0 (c (n "txn_lock") (v "0.8.0") (d (list (d (n "collate") (r "^0.4") (d #t) (k 0)) (d (n "ds-ext") (r "~0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hr-id") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("sync"))) (d #t) (k 0)))) (h "1f0jwsz49m34bca6ck6zp2z0hwjh635rqvp67h4s9vm4icbig67h") (f (quote (("logging" "log") ("id" "hr-id") ("all" "hr-id" "logging"))))))

(define-public crate-txn_lock-0.8.1 (c (n "txn_lock") (v "0.8.1") (d (list (d (n "collate") (r "^0.4") (d #t) (k 0)) (d (n "ds-ext") (r "~0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hr-id") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("sync"))) (d #t) (k 0)))) (h "0jvsq0df7pdvxrc2s7nsf4w3d91sngyn7s1glx7n8ryb4dlgb2qz") (f (quote (("logging" "log") ("id" "hr-id") ("all" "hr-id" "logging"))))))

(define-public crate-txn_lock-0.9.0 (c (n "txn_lock") (v "0.9.0") (d (list (d (n "collate") (r "^0.4") (d #t) (k 0)) (d (n "ds-ext") (r "~0.1.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "hr-id") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("time"))) (d #t) (k 2)))) (h "1ny35dfy78m7y8z4ld1f0j3lxdpi2qi7jyfswir6dvhc1q8iwd0m") (f (quote (("logging" "log") ("id" "hr-id") ("all" "hr-id" "logging"))))))

