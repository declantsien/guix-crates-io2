(define-module (crates-io tx tu txture) #:use-module (crates-io))

(define-public crate-txture-0.1.0 (c (n "txture") (v "0.1.0") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "18wm1k2hgid9wmr3l6mkfa5qynjig5iwk7i0ga6fiaz7242ay9gr")))

(define-public crate-txture-0.1.1 (c (n "txture") (v "0.1.1") (d (list (d (n "bmp") (r "^0.5.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0rig838q9b7x3xg1jnzva3i5v42kbsncqxamvqgzy8k2s7vp8v4r")))

