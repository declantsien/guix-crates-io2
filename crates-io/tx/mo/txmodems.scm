(define-module (crates-io tx mo txmodems) #:use-module (crates-io))

(define-public crate-txmodems-0.1.0 (c (n "txmodems") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (k 0)) (d (n "core2") (r "^0.4.0") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "11r6r1l0pg7gr75zmxlldhm1dijgqqhxl7vi51sbg5lbj0abn0ad") (f (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1.2 (c (n "txmodems") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (k 0)) (d (n "core2") (r "^0.4.0") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "0yhinb88kdwpf23dpb68z1dgmrfr1l3fdfm88c4m64skilw0vq5y") (f (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1.1 (c (n "txmodems") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (k 0)) (d (n "core2") (r "^0.4.0") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "038j70h4lchnnndi0sn07nwhiy7gazki77wmvxps50v8vlmvp8yv") (f (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1.3 (c (n "txmodems") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (k 0)) (d (n "core2") (r "^0.4.0") (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "17n0690ml6k12hamhs8jz61l0zlgp9ndw5gjszblpingqqqk78z1") (f (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

