(define-module (crates-io tx #{5-}# tx5-online) #:use-module (crates-io))

(define-public crate-tx5-online-0.0.1-alpha.1 (c (n "tx5-online") (v "0.0.1-alpha.1") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.1-alpha.3") (d #t) (k 0)))) (h "0rhaja8cwmcvjb8rx7gc7lbq5vlqlv8yyy4cb9ffxkfqz5m56w4x")))

(define-public crate-tx5-online-0.0.1-alpha.2 (c (n "tx5-online") (v "0.0.1-alpha.2") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.1-alpha.4") (d #t) (k 0)))) (h "1wq934d5ygj6ar84nqxg1r9f4zmb3vvvhf4vskg5fk9mvf3kp5za")))

(define-public crate-tx5-online-0.0.1-alpha.3 (c (n "tx5-online") (v "0.0.1-alpha.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.1-alpha.5") (d #t) (k 0)))) (h "1shwc610wwhkppp6526mcvda8difzkq7sm2r1chwjwzjq110r3fs")))

(define-public crate-tx5-online-0.0.1-alpha.4 (c (n "tx5-online") (v "0.0.1-alpha.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.1-alpha.6") (k 0)))) (h "093fpss7hgfc9in5h0v716mmlj791pv01hyajfpqa883k5r80v8z")))

(define-public crate-tx5-online-0.0.1-alpha.5 (c (n "tx5-online") (v "0.0.1-alpha.5") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.1-alpha.7") (k 0)))) (h "00wfblhp34n3dq9w7nh083y755sgsr62pwsyylky8p5h0frdk4sl")))

(define-public crate-tx5-online-0.0.2-alpha (c (n "tx5-online") (v "0.0.2-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.2-alpha") (k 0)))) (h "11s8xhb3w3pj923mh2vl3c8h1lxbgzvk8q3cb6n5gwv42ycbdfl3")))

(define-public crate-tx5-online-0.0.3-alpha (c (n "tx5-online") (v "0.0.3-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.3-alpha") (k 0)))) (h "00fyj4vb6mrwnina057jld0z12cjnhifky0278inz4vn45g27a06")))

(define-public crate-tx5-online-0.0.4-alpha (c (n "tx5-online") (v "0.0.4-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.4-alpha") (k 0)))) (h "1mh8lns9kgaqszbb5rwwq6av5siivf562g2ssxpifnnhihj267rc")))

(define-public crate-tx5-online-0.0.5-alpha (c (n "tx5-online") (v "0.0.5-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.5-alpha") (k 0)))) (h "063rcknmcrvbv10z3nqs8c51r6ss448a48s1fgljcla0j03lr2d6")))

(define-public crate-tx5-online-0.0.6-alpha (c (n "tx5-online") (v "0.0.6-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.6-alpha") (k 0)))) (h "0w0cndh0zkbz96hm7kdljmac05pdkb7cgic8psz0js4ibp2s681r")))

(define-public crate-tx5-online-0.0.7-alpha (c (n "tx5-online") (v "0.0.7-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.7-alpha") (k 0)))) (h "17ymzxhpsdg2xq6g47s1xf773l0zjkmcwxbfgjq73zymqyngnp6v")))

(define-public crate-tx5-online-0.0.8-alpha (c (n "tx5-online") (v "0.0.8-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.8-alpha") (k 0)))) (h "1z7qqxnskp6vzf9xkzjijg548l1x6j664p8dhqhi6sj8fp6fb93a")))

(define-public crate-tx5-online-0.0.9-alpha (c (n "tx5-online") (v "0.0.9-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.9-alpha") (k 0)))) (h "01mhb4shdgfxlkrfmps20v8wkih2bjvp2qjbmhyhi4045avql6mb")))

(define-public crate-tx5-online-0.0.10-alpha (c (n "tx5-online") (v "0.0.10-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.10-alpha") (k 0)))) (h "1d6qii8njwck9xacwwjvfvf0r7ikikkw9r6ifvhakz0yslr10vli")))

(define-public crate-tx5-online-0.0.11-alpha (c (n "tx5-online") (v "0.0.11-alpha") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "tx5-core") (r "^0.0.11-alpha") (k 0)))) (h "1b95jwlc1yh3lvd4anm9mxh38iajy8cgdscwffskpybx7hi8pv7c")))

