(define-module (crates-io tx to txtools) #:use-module (crates-io))

(define-public crate-txtools-0.1.0 (c (n "txtools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "rfd") (r "^0.13.0") (d #t) (k 0)))) (h "0g98xk0k187b4gbb0i2x4xac5kasc3nlgvl5213nd8mrgnp7mzc6")))

(define-public crate-txtools-0.1.1 (c (n "txtools") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "rfd") (r "^0.13.0") (d #t) (k 0)))) (h "1zxb1izxqjlzfp7vx4sldk0pj4iq3ixs1rdizkxhjdipp3sisp3q")))

