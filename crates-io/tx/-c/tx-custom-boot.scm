(define-module (crates-io tx -c tx-custom-boot) #:use-module (crates-io))

(define-public crate-tx-custom-boot-0.1.0 (c (n "tx-custom-boot") (v "0.1.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0carjp74rqlcmrcwmf9lfnwmynidvys5hypgjndqb1wghsgd0clh")))

(define-public crate-tx-custom-boot-0.2.0 (c (n "tx-custom-boot") (v "0.2.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1a7hcqvfj8i1srihzsbdfdx13br8rm85ygpd1k5522553plmly5f")))

(define-public crate-tx-custom-boot-0.3.0 (c (n "tx-custom-boot") (v "0.3.0") (d (list (d (n "binwrite") (r "^0.2.1") (d #t) (k 0)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0idffxfgsmdh89blf6mi4x21p2p7hx6agfrbwnxdjl2s368whlzx")))

