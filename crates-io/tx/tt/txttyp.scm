(define-module (crates-io tx tt txttyp) #:use-module (crates-io))

(define-public crate-txttyp-0.1.0 (c (n "txttyp") (v "0.1.0") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "057axmxv0zlxr7zl98d5di2l09vp9kvvk4k0nrl4xqa4x3pjvw3m")))

(define-public crate-txttyp-0.1.1 (c (n "txttyp") (v "0.1.1") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "03gs15i70kwrjm9hl9mc2v56krqwnsqywrgrqj7hgmxsklr95lq6")))

(define-public crate-txttyp-0.1.2 (c (n "txttyp") (v "0.1.2") (d (list (d (n "ansistr") (r "^0.1.1") (d #t) (k 0)))) (h "0c1f1r3h5yc88x215n811qdw61xpza7qmbiyz7w2m6x1w2rdc582")))

