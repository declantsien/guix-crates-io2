(define-module (crates-io tx ts txts-rs) #:use-module (crates-io))

(define-public crate-txts-rs-0.1.1 (c (n "txts-rs") (v "0.1.1") (d (list (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies" "brotli"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (d #t) (k 0)))) (h "0h2vyy5hprcw36z3386qz1rb14fk3c8a273wi188rbdhlbg12jb6")))

