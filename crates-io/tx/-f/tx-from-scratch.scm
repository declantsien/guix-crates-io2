(define-module (crates-io tx -f tx-from-scratch) #:use-module (crates-io))

(define-public crate-tx-from-scratch-0.1.0 (c (n "tx-from-scratch") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.14.1") (d #t) (k 2)) (d (n "jsonrpsee") (r "^0.16.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "rlp") (r "^0.5.2") (d #t) (k 0)) (d (n "secp256k1") (r "^0.25.0") (f (quote ("default" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "web3") (r "^0.18.0") (f (quote ("bytes"))) (d #t) (k 2)))) (h "1pn0dyiqpmnr9n12rfhr3hlsqn7lkz56jrcn1jwkbgs6rpxm5mqx")))

