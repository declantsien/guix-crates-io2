(define-module (crates-io tx t- txt-cluster) #:use-module (crates-io))

(define-public crate-txt-cluster-0.1.0 (c (n "txt-cluster") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "strsim") (r "^0.9") (d #t) (k 0)))) (h "0cs2ikgfcfzifv4pn7by9nnh65q012040g2sz3nsywngl576y8wm")))

