(define-module (crates-io tx l- txl-rs) #:use-module (crates-io))

(define-public crate-txl-rs-0.0.1 (c (n "txl-rs") (v "0.0.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.28") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1z7rwq8jxrsj419zdvkpfd5w5gyh8k1skaixlrr6219pq0wfqqa1")))

(define-public crate-txl-rs-0.0.2 (c (n "txl-rs") (v "0.0.2") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.28") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1d8qa5xc1z3qg5bnh5wyg37br164s6ad4r1pqb4yrfrignmlxqnq")))

(define-public crate-txl-rs-0.0.3 (c (n "txl-rs") (v "0.0.3") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tar") (r "^0.4.28") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1m1v55rqpdzfyh74priwm42hjmqi93s4lps9a56q23zxqdnp1y72")))

