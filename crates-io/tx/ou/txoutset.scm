(define-module (crates-io tx ou txoutset) #:use-module (crates-io))

(define-public crate-txoutset-0.1.0 (c (n "txoutset") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)))) (h "15409s4p1x857vjgpqliv39jncqq525chx53jy48ih2aq1c5i97y")))

(define-public crate-txoutset-0.2.0 (c (n "txoutset") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)))) (h "1xnv61f3yzqfhv3qh912h2n2xzxwqnfnk6yi97f37994nrw7pw4g")))

(define-public crate-txoutset-0.3.0 (c (n "txoutset") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.31.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1447jylrsjqjkk5gfwcc3bvwkv4nx9qv7n608nia3wnm170d6vh9")))

