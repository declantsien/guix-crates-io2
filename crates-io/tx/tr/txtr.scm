(define-module (crates-io tx tr txtr) #:use-module (crates-io))

(define-public crate-txtr-0.1.0 (c (n "txtr") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1ps9cg6amprbys2gir271xzck05fmxx0kyc8z6nz2qwy712kwzjx")))

(define-public crate-txtr-0.2.0 (c (n "txtr") (v "0.2.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "05v4gp3fr58grm9maqgqk0025n70lg5s8jasqa57vh77qcisx65j")))

(define-public crate-txtr-1.0.0 (c (n "txtr") (v "1.0.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15l863frr6yfig4ypp1wb7fmnqf9c7q5vms5h8afysx2nfaizlk2")))

