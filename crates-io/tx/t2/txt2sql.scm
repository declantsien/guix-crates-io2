(define-module (crates-io tx t2 txt2sql) #:use-module (crates-io))

(define-public crate-txt2sql-0.1.0 (c (n "txt2sql") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rb89680zrl2a212f27130lxjyw3lrh87jn542rd8azk405y54za")))

