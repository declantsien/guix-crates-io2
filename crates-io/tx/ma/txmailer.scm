(define-module (crates-io tx ma txmailer) #:use-module (crates-io))

(define-public crate-txmailer-0.0.1 (c (n "txmailer") (v "0.0.1") (h "1x95z2w8g3jppbl9rh6lca02kp8x42z9nq4k1yr3yjalg28sganr")))

(define-public crate-txmailer-0.0.2 (c (n "txmailer") (v "0.0.2") (h "1gjbaqgfwv1gr6r8q01zs47vsl2zxy1g6wwm98d6abxq2510rg6i")))

