(define-module (crates-io tx #{4-}# tx4-go-pion) #:use-module (crates-io))

(define-public crate-tx4-go-pion-0.0.1 (c (n "tx4-go-pion") (v "0.0.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "tx4-go-pion-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1grhbjak4jynrmqyg9ycyqa7p43vw2schwvaqp8fnbpx5wxwkcbl") (y #t)))

