(define-module (crates-io tx #{4-}# tx4-go-pion-sys) #:use-module (crates-io))

(define-public crate-tx4-go-pion-sys-0.0.1 (c (n "tx4-go-pion-sys") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "ouroboros") (r "^0.15.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tx4-core") (r "^0.0.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 1)))) (h "1jy92n64zhymp0n6s5vxhqky8djyc6fnyvh0srg4lbwvp0j5sasz") (y #t)))

