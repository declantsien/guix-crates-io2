(define-module (crates-io tx li txlib) #:use-module (crates-io))

(define-public crate-txlib-0.1.1 (c (n "txlib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "open") (r "^4.0.2") (d #t) (k 0)))) (h "0a95r8x1cjmi4wgq32hvf6hipz81n597kf8kxxxq0rabzr6szdqc")))

(define-public crate-txlib-0.1.2 (c (n "txlib") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "024fgx55hml6v2gafnfbmbxz9ddbj97x5p2qkxpwc8li1lnpxsnm")))

(define-public crate-txlib-0.1.3 (c (n "txlib") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0brhp2gwb9w24hycbh9abjd57sx39q7ry0bgyr8d2fbxp7rd84h1")))

