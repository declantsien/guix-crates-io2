(define-module (crates-io tx n- txn-core) #:use-module (crates-io))

(define-public crate-txn-core-0.1.0 (c (n "txn-core") (v "0.1.0") (d (list (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j8v7a369w8g67zgcp8p2vkphmwivsffj4rwy2w7a5v1ih1f19q8") (f (quote (("std") ("default" "std"))))))

(define-public crate-txn-core-0.1.1 (c (n "txn-core") (v "0.1.1") (d (list (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mbirbj2w74rpayyi1651fb8llyphhb8bqp0n7mpqbp9xrs9zfbx") (f (quote (("std") ("default" "std"))))))

(define-public crate-txn-core-0.1.2 (c (n "txn-core") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (o #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "11sn19vjhghrc06wxz3wjsn6npzl4g1wpaw72qxs090r9z5igp3d") (f (quote (("std" "alloc" "smallvec-wrapper/std" "indexmap/default" "thiserror") ("default" "std") ("alloc" "indexmap"))))))

(define-public crate-txn-core-0.1.3 (c (n "txn-core") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "cheap-clone") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "indexmap") (r "^2") (o #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)))) (h "1iai6b7y3l0pmknlg805kxavv62v15l0d0fyf2wqhlj527l4b5n0") (f (quote (("std" "alloc" "smallvec-wrapper/std" "indexmap/default" "thiserror") ("default" "std") ("alloc" "indexmap"))))))

