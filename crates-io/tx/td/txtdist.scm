(define-module (crates-io tx td txtdist) #:use-module (crates-io))

(define-public crate-txtdist-0.1.0 (c (n "txtdist") (v "0.1.0") (h "0h883sqa6h9v8di8sjdwsp2ywag714an8bf4wqzmvzngsxrbxnbb")))

(define-public crate-txtdist-0.2.0 (c (n "txtdist") (v "0.2.0") (h "0bic6bfq1bi7z1w4jgvgi5bs2x9dc7b9fx8kxdm8hm48zp5bqxgh")))

(define-public crate-txtdist-0.2.1 (c (n "txtdist") (v "0.2.1") (h "1zs27w1irmlx73mi6yaz5x7fnpb3zp5x9bk4y2k9qcln936niw4i")))

