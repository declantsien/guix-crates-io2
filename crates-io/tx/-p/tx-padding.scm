(define-module (crates-io tx -p tx-padding) #:use-module (crates-io))

(define-public crate-tx-padding-0.1.0 (c (n "tx-padding") (v "0.1.0") (d (list (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)) (d (n "typenum") (r "^1.12") (k 0)))) (h "1kq5igigc5y84rlci4dlkfh0p5mzsap11hjhcm76c5kmbpnzhq65") (f (quote (("thread_rng"))))))

(define-public crate-tx-padding-0.1.1 (c (n "tx-padding") (v "0.1.1") (d (list (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)) (d (n "typenum") (r "^1.12") (k 0)))) (h "19yjydmbaipzy3ng9xvbk6skahyk6dqns036c1h30w7zl9b8lwp0") (f (quote (("thread_rng"))))))

(define-public crate-tx-padding-0.1.2 (c (n "tx-padding") (v "0.1.2") (d (list (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)) (d (n "typenum") (r "^1.12") (k 0)))) (h "0pkbdngs9p7fbpai58cigp2k2r1f40wjcbbd3syc46050bphrm8g") (f (quote (("thread_rng"))))))

