(define-module (crates-io mm ar mmarinus) #:use-module (crates-io))

(define-public crate-mmarinus-0.1.0 (c (n "mmarinus") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07x1ws4hp7d7nl6n4akb1yd5wq7k4b9zf9m6jphcxlbz7izqcb67")))

(define-public crate-mmarinus-0.2.0 (c (n "mmarinus") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0znqixvpz4zm0rm8flq6saqzi3zj0pnhqgb4prv9zbg9dv4gv4j2")))

(define-public crate-mmarinus-0.2.1 (c (n "mmarinus") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b463v3rqmqb70fg1avv00m6frlggy50p2i4v0i5nxiy68n1q03g")))

(define-public crate-mmarinus-0.3.0 (c (n "mmarinus") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19n9kainf7lfysmjafckxfrl8rw9sdhpckgqfsnplpn70fi0n0kl")))

(define-public crate-mmarinus-0.4.0 (c (n "mmarinus") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wwlgwsydf90qbq94k753958kwrs1l0nk5gg6yi3z0m3wzk9i01b")))

