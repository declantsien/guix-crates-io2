(define-module (crates-io mm r- mmr-macro) #:use-module (crates-io))

(define-public crate-mmr-macro-0.0.1 (c (n "mmr-macro") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2i7gnq12m8mxi68w882c33sisfavimcyllv478zk147w25gy1v")))

(define-public crate-mmr-macro-0.0.2 (c (n "mmr-macro") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1gl5ri63lccj22dn0qpcxw37w02ypdf13nna5jxj2cqpvs54n55y")))

(define-public crate-mmr-macro-0.0.3 (c (n "mmr-macro") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0hlgs4vmz8nz6i9kaq3icygmzyzzp00vzm19j18ry453iav0rwfm")))

(define-public crate-mmr-macro-0.0.4 (c (n "mmr-macro") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0db8k16crk0ipm6f6wvl6ka7461xy9s6j1p1c6f5y53iha0k12fb")))

(define-public crate-mmr-macro-0.0.5 (c (n "mmr-macro") (v "0.0.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9vzsc92cdmzfw7fm85kpjqjl0953dihs37zi9616ak7w3659bk")))

(define-public crate-mmr-macro-0.0.6 (c (n "mmr-macro") (v "0.0.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0csn6s684ys8fvi7lxmz4pj2i6la8q5a992m8czgwin7mapbs3n2")))

(define-public crate-mmr-macro-0.0.7 (c (n "mmr-macro") (v "0.0.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "05vd5v622jdapgprlzav312x8y8hww9kpddl9m4hfxcrg917c0iy")))

