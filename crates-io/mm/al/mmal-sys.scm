(define-module (crates-io mm al mmal-sys) #:use-module (crates-io))

(define-public crate-mmal-sys-0.0.0 (c (n "mmal-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.32.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x4q9n8ygmf6gxfjzzbl75xj6gw8h0xc83s0r41709yfcyrap79a")))

(define-public crate-mmal-sys-0.1.0-1 (c (n "mmal-sys") (v "0.1.0-1") (d (list (d (n "bindgen") (r "^0.32.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "172f29z440f99mc1wd2sk3gkck4fqlfv6g5dav1rjm6rqfxrwm5m") (f (quote (("generate_bindings" "bindgen") ("default"))))))

(define-public crate-mmal-sys-0.1.0-2 (c (n "mmal-sys") (v "0.1.0-2") (d (list (d (n "bindgen") (r "^0.40.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05ggh31pflwi6yq57l2c69pjibrf0wba1rnlv10qspv4ns0zxisr") (f (quote (("generate_bindings" "bindgen") ("default")))) (l "mmal")))

(define-public crate-mmal-sys-0.1.0-3 (c (n "mmal-sys") (v "0.1.0-3") (d (list (d (n "bindgen") (r "^0.52.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "185a2sds01jjwfy3yyyzws1h5a98095rx1kx4xfixhfb1k43wg58") (f (quote (("generate_bindings" "bindgen") ("default")))) (l "mmal")))

