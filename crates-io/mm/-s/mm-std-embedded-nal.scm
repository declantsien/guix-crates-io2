(define-module (crates-io mm -s mm-std-embedded-nal) #:use-module (crates-io))

(define-public crate-mm-std-embedded-nal-0.3.0 (c (n "mm-std-embedded-nal") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12") (o #t) (d #t) (k 0)) (d (n "embedded-nal") (r "^0.8") (d #t) (k 0)) (d (n "embedded-nal-tcpextensions") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-ext"))) (d #t) (k 2)))) (h "1nnrfaws7vrpgsf9q03cgrrmv1pybphanigpl90idv8nchznpk9x")))

