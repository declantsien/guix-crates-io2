(define-module (crates-io mm i- mmi-parser) #:use-module (crates-io))

(define-public crate-mmi-parser-0.1.0 (c (n "mmi-parser") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10phillxcazdf3ki32hx6p5fml1sjhcc602a423p8y8qp06nl0jz") (f (quote (("dumb_terminal" "colored/no-color"))))))

(define-public crate-mmi-parser-0.1.2 (c (n "mmi-parser") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "018wacg9jza1j3vcpp0kxgz71pwgp01lcgpz59sxq8699rczdj6v") (f (quote (("dumb_terminal" "colored/no-color"))))))

(define-public crate-mmi-parser-1.0.0 (c (n "mmi-parser") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xigkv0h5cymygvfhjqrjs6ifk1lkml4rggwhshlp4qz5a6wli6x")))

(define-public crate-mmi-parser-1.0.1 (c (n "mmi-parser") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fgpk6fav4kx2d5jvi5y2i1fry0rcc09r758x1352b7x7c9sjggm")))

(define-public crate-mmi-parser-1.1.0 (c (n "mmi-parser") (v "1.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dmhqqmryw3ryw5v9vv4xxgbfl4h7r2ywihgnf7r9wnc0l2cc7qq")))

(define-public crate-mmi-parser-2.0.0 (c (n "mmi-parser") (v "2.0.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1sj71av66gbs9lz41jlxwz474v5nym65gsdrb8m9mafhddqgzka9")))

