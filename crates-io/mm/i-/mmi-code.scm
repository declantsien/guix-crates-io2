(define-module (crates-io mm i- mmi-code) #:use-module (crates-io))

(define-public crate-mmi-code-0.1.0 (c (n "mmi-code") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i88445szyvwqjax9v2ay3qqksv85c7ynyx0qhnjd6s5g5b2gvp3")))

(define-public crate-mmi-code-0.1.1 (c (n "mmi-code") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pbj44ipdjbz9xvif96iryzl60gj80bw8zc1j2kn7jdbz0acxhjg")))

(define-public crate-mmi-code-0.2.0 (c (n "mmi-code") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b37a4myr1aw18lr22xnifiwzj4rzs2phrkbqp4pxck80msrmn89")))

