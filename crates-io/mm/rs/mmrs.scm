(define-module (crates-io mm rs mmrs) #:use-module (crates-io))

(define-public crate-mmrs-0.1.0 (c (n "mmrs") (v "0.1.0") (d (list (d (n "mockito") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1m410jn5aswrj84n1l70025xrz98rn04173vqri57j36bsmr0cfq")))

(define-public crate-mmrs-0.1.1 (c (n "mmrs") (v "0.1.1") (d (list (d (n "mockito") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1v6k9p4r7g6wvba83fvg46k5pi64frqn7zck4bgdbvwrnj6mrjjx")))

(define-public crate-mmrs-0.1.2 (c (n "mmrs") (v "0.1.2") (d (list (d (n "mockito") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (f (quote ("json" "rustls-tls" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1sd80b0bf4lhdp390jy3qxlslkfcqcgdnah4vbpcg4ydx669w9xy")))

