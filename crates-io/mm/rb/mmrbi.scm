(define-module (crates-io mm rb mmrbi) #:use-module (crates-io))

(define-public crate-mmrbi-0.0.1 (c (n "mmrbi") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1vp5l9iv23cjrsb8f3wsdawh18pgsd2ii79an5i73zirlzbl2fd8")))

(define-public crate-mmrbi-0.0.2 (c (n "mmrbi") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1vwvw2fgs18isrjbh8ddbgqdc3dqz3axsglc089m45577iji209k")))

(define-public crate-mmrbi-0.0.3 (c (n "mmrbi") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1bbdnjmjgg0g2aw430ip405lfyrrr9nbvvqjamnyfqh4f3kw2qhk")))

(define-public crate-mmrbi-0.0.4 (c (n "mmrbi") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06bkikbn76rariy1h37bm768w7bn5v3bh6z7aa6dsz15al22p2j6")))

(define-public crate-mmrbi-0.0.5 (c (n "mmrbi") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0vnvx08b7v1zzrg2f6cd7bd6aw5d124nilj08zdsqy9m9g6k817l")))

(define-public crate-mmrbi-0.0.6 (c (n "mmrbi") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "182fd60wphylgk2z8c08rpmpx07lia6qppq7gkf4j3crvdl38zg8")))

(define-public crate-mmrbi-0.0.7 (c (n "mmrbi") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0n6n6n6gz3a2i9wby06nx98kvhzzbcmm2sbb6pjwpz8csd4m2bfl")))

(define-public crate-mmrbi-0.0.8 (c (n "mmrbi") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1l57xqwfi3q4qgzabdpd0sx7vvj4333491qgw1qg2vkvpysl9sxz")))

(define-public crate-mmrbi-0.0.9 (c (n "mmrbi") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lh22wdqrbhywk1z670fr3q0fk2zyqg4cwlywz7s0ja8y078m3aq")))

(define-public crate-mmrbi-0.0.10 (c (n "mmrbi") (v "0.0.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1k6b0nd97b4yakw9ccr7bwfda76bq49w2fjr2iixa5mypg8w9508")))

(define-public crate-mmrbi-0.0.11 (c (n "mmrbi") (v "0.0.11") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0999wk0103jsi2q6nzwh23xnrwmkkfsrd5jfv8f1p9yql12gc7r8")))

(define-public crate-mmrbi-0.0.12 (c (n "mmrbi") (v "0.0.12") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "semver") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1n8l4ljbkipa6xb2yf2w2p6c1y1c514ihd4c6lb1nfnp87q230hw")))

(define-public crate-mmrbi-0.0.13 (c (n "mmrbi") (v "0.0.13") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1ws4m9ami0iz7sr82k283chrcx5zc1m423zwqb1z1rrfypzmdnc2") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.14 (c (n "mmrbi") (v "0.0.14") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0q6g5pn2gimnbidv1r55lssdbzi5nan0pjm2slljv5l4bb2fcfwn") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.15 (c (n "mmrbi") (v "0.0.15") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1f1dpafw26pqlj2pkrmyq3ybpr4pjnwvl2fiwjj7xyk6sd8k9m40") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.16 (c (n "mmrbi") (v "0.0.16") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0209qy9knrqh0w43bajh0j2fmrcaqmaxls3qpp230cwfkrxvrywv") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.17 (c (n "mmrbi") (v "0.0.17") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02fmzip107vl40hqhasbndqngycwh1y9ihdgjj0xnp8qh4z2m12a") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.18 (c (n "mmrbi") (v "0.0.18") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02yanlbxqfvxjkaz256hp6k1mbqrzgw551w8cxffmxvrcvjls1rp") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.19 (c (n "mmrbi") (v "0.0.19") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "01frgn04wm7ajakqx8hvljhzb69kfdm7inyhvq8f553006l8z305") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

(define-public crate-mmrbi-0.0.20 (c (n "mmrbi") (v "0.0.20") (d (list (d (n "alphanumeric-sort") (r "=1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0fyb8wfw8msssmyvlyll6rvrf7d2v49agy6yqixy2h7mf2q6jmni") (f (quote (("version" "semver") ("default") ("all" "serde" "toml" "version"))))))

