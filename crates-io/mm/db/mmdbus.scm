(define-module (crates-io mm db mmdbus) #:use-module (crates-io))

(define-public crate-mmdbus-1.6.14 (c (n "mmdbus") (v "1.6.14") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "19z0ndbpsgz6jdg08krg4iva3ks8a4sz6055mdn03cqpl70d3s1b")))

(define-public crate-mmdbus-1.10.8 (c (n "mmdbus") (v "1.10.8") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "1yicrmrs6nibn2xix8br55qvwqypwx1mr3155lpx33cjvphi3vii")))

(define-public crate-mmdbus-1.14.12 (c (n "mmdbus") (v "1.14.12") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "1fqk9hx28f7b61f5assfiy0vgpgdiy9r2gffqm8qnipgzgpppb5a")))

(define-public crate-mmdbus-1.16.10 (c (n "mmdbus") (v "1.16.10") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "14mzdcafdwdm7fx0qxh05w5a6cbd3i830mnkw2ynm36wf66agdij")))

(define-public crate-mmdbus-1.17.900 (c (n "mmdbus") (v "1.17.900") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "1nfm80w51zlkz639yvn968ad9kb1cjl0v0h9g0n498cayn5ajbi8")))

(define-public crate-mmdbus-1.18.6 (c (n "mmdbus") (v "1.18.6") (d (list (d (n "dbus") (r "^0.9") (d #t) (k 0)))) (h "074aahc90wsw3f3kfrg2p1viz039d0ps0jhs3qqy43jczwasj79k")))

