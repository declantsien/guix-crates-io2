(define-module (crates-io mm db mmdb_trie_db) #:use-module (crates-io))

(define-public crate-mmdb_trie_db-0.1.0 (c (n "mmdb_trie_db") (v "0.1.0") (d (list (d (n "hash-db") (r "^0.16.0") (d #t) (k 0)) (d (n "mmdb") (r "^1.0.0") (k 0)) (d (n "mmdb_hash_db") (r "^0.1.0") (k 0)) (d (n "parity-scale-codec") (r "^3.6.12") (d #t) (k 0)) (d (n "ruc") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trie-db") (r "^0.29.1") (d #t) (k 0)) (d (n "trie-root") (r "^0.18.0") (d #t) (k 0)))) (h "0zhiwc5l6vavbmjdb8y5wkv915sp6l59x66zgm4yz8fa0cdwckwm") (f (quote (("rocks_backend" "mmdb/rocks_backend") ("parity_backend" "mmdb/parity_backend") ("msgpack_codec" "mmdb/msgpack_codec") ("json_codec" "mmdb/json_codec") ("default" "rocks_backend"))))))

