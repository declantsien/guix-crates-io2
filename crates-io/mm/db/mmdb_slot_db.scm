(define-module (crates-io mm db mmdb_slot_db) #:use-module (crates-io))

(define-public crate-mmdb_slot_db-0.1.0 (c (n "mmdb_slot_db") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "mmdb") (r "^1.0.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ruc") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sdi4a9rqcb4h09v5jjb3b02gh14xgq453r27ffkc0ayh2ka7iqp") (f (quote (("rocks_backend" "mmdb/rocks_backend") ("parity_backend" "mmdb/parity_backend") ("msgpack_codec" "mmdb/msgpack_codec") ("json_codec" "mmdb/json_codec") ("default" "rocks_backend" "compress" "msgpack_codec") ("compress" "mmdb/compress"))))))

