(define-module (crates-io mm db mmdb_trie_map) #:use-module (crates-io))

(define-public crate-mmdb_trie_map-0.1.0 (c (n "mmdb_trie_map") (v "0.1.0") (d (list (d (n "mmdb_trie_db") (r "^0.1.0") (k 0)))) (h "0pg605ypinhwldyxmwcwkgp76jl89w72h0d7062ghwnqzbjnk59n") (f (quote (("rocks_backend" "mmdb_trie_db/rocks_backend") ("parity_backend" "mmdb_trie_db/parity_backend") ("msgpack_codec" "mmdb_trie_db/msgpack_codec") ("json_codec" "mmdb_trie_db/json_codec") ("default" "rocks_backend"))))))

