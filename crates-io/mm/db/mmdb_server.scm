(define-module (crates-io mm db mmdb_server) #:use-module (crates-io))

(define-public crate-MMDB_Server-0.1.0 (c (n "MMDB_Server") (v "0.1.0") (d (list (d (n "configster") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0s7c4p7h9cwr6cxm75hik3i72v39f5s8r04r6271pl9w76yxfnxj") (y #t)))

(define-public crate-MMDB_Server-0.1.1 (c (n "MMDB_Server") (v "0.1.1") (d (list (d (n "configster") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "118hb3q3dkqfr2i8f37jkw82bbx8xnm6fzlr7hw9v89wxspp2ri5") (y #t)))

(define-public crate-MMDB_Server-0.1.2 (c (n "MMDB_Server") (v "0.1.2") (d (list (d (n "configster") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0h0w2a9vjlk9k1mi4nq3m9zgjnrfj6x2z479idgphq0ahv1jy1d5") (y #t)))

(define-public crate-MMDB_Server-0.1.3 (c (n "MMDB_Server") (v "0.1.3") (d (list (d (n "configster") (r "^0.1.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03rrp6nnsbgdi90darv8082c6sahrx7s2i7xrb7qina9rh4i528g")))

