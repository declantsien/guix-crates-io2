(define-module (crates-io mm db mmdb_hash_db) #:use-module (crates-io))

(define-public crate-mmdb_hash_db-0.1.0 (c (n "mmdb_hash_db") (v "0.1.0") (d (list (d (n "hash-db") (r "^0.16.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.16.0") (d #t) (k 0)) (d (n "mmdb") (r "^1.0.0") (k 0)) (d (n "msgpack") (r "^1.1.1") (d #t) (k 0) (p "rmp-serde")) (d (n "ruc") (r "^5.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vg1ma0pm5xki8ldy4m5w1rcgmrwb1wk0f6kfdpqrn5pmszngzw8") (f (quote (("rocks_backend" "mmdb/rocks_backend") ("parity_backend" "mmdb/parity_backend") ("msgpack_codec" "mmdb/msgpack_codec") ("json_codec" "mmdb/json_codec") ("default" "rocks_backend"))))))

