(define-module (crates-io mm ox mmoxi) #:use-module (crates-io))

(define-public crate-mmoxi-0.1.0 (c (n "mmoxi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)))) (h "1yr77axg6j9m1ap62r9j3n9vda22lq0ccya8s83zczm7hf78pf9v")))

(define-public crate-mmoxi-0.1.1 (c (n "mmoxi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)))) (h "0d94n483bydapd7s55jbz30yqpl31yksczpqlwagkg0dx541fclf")))

(define-public crate-mmoxi-0.1.2 (c (n "mmoxi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)))) (h "0i186g8p1xr7vqhsval00dcg1f8g6cpibq7j1yg0d8sn4cgfsi40")))

(define-public crate-mmoxi-0.2.0 (c (n "mmoxi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0vbh86jlv5yvw6kkqnsk1ndrmxwxrch2xyi6gkqaxdfrw70x0dg8")))

(define-public crate-mmoxi-0.2.1 (c (n "mmoxi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1jshbyfc78s21wyxlljsqnb8nffjwdkxq0wk7ginrqdix57yjij8")))

(define-public crate-mmoxi-0.2.2 (c (n "mmoxi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "wrap_help"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1lydr5l59ldc66f6bzgq86nyfrz1xm8p0lgdl7pibb1s2li7qgcy") (r "1.70")))

