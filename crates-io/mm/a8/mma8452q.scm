(define-module (crates-io mm a8 mma8452q) #:use-module (crates-io))

(define-public crate-mma8452q-0.1.0 (c (n "mma8452q") (v "0.1.0") (d (list (d (n "accelerometer") (r "^0.11") (d #t) (k 0)) (d (n "cast") (r "^0.2") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)))) (h "157cniszv9k25prbbii6mkahvas9pbam6n98ggvh8ba8fi4qrrgl") (f (quote (("out_f32") ("default"))))))

