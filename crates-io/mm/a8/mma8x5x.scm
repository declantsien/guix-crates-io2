(define-module (crates-io mm a8 mma8x5x) #:use-module (crates-io))

(define-public crate-mma8x5x-0.1.0 (c (n "mma8x5x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1xappkg4pabp5ai108yd5wfrp48hi34x2vzqk8mvjxrwd1pz0y8f")))

(define-public crate-mma8x5x-0.1.1 (c (n "mma8x5x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1f4warh5arab2anl5bkpq2hdgf0skafgs1xw5nrgpin8sqwsk6ri")))

