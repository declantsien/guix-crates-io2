(define-module (crates-io mm le mmledger) #:use-module (crates-io))

(define-public crate-mmledger-0.1.0 (c (n "mmledger") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "1vn78q2wyxbd9cfjdds1j280m0q6s4khq4rk07w778r3hyf60qw5")))

(define-public crate-mmledger-0.1.1 (c (n "mmledger") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "0ai9hyj5yd4kbhz33q7jyvkchbxrd1y3qdrvw7aqbpkx24ryl3pb")))

(define-public crate-mmledger-0.1.2 (c (n "mmledger") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0j358dwr89pvmcrn38w3cgk5ngm94zgr2z2mzqyn8j3pxg19p27d")))

(define-public crate-mmledger-0.1.3 (c (n "mmledger") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1s1g8ji2k1p0p3vv7rcwkas0w5s8c2fqbikm7c4nbf1ds04zm60p")))

(define-public crate-mmledger-0.1.4 (c (n "mmledger") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1n7pc7bbr185hz74m2lkvvaj5ci6bmxjc83gm5wr4cyghf68dxpp")))

(define-public crate-mmledger-0.1.5 (c (n "mmledger") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1125vlwjwsq3h2y8jd8b0c2i77qdwjsybfk0cdnsrqvhyngdfdlf")))

(define-public crate-mmledger-0.1.6 (c (n "mmledger") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1fq7lfb04sm07sddf9225vc789kcszdqabn9f1i72n9fm80s6667")))

(define-public crate-mmledger-0.1.7 (c (n "mmledger") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0swy6jsrh18ff2dsy7fbfnqlpkd1bdis48nmvshaz6ycfa8hp19g")))

(define-public crate-mmledger-0.1.8 (c (n "mmledger") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "1sl7q00mr0p1bwdl6qyys9w1p2qrvlc7lr5hfpyfwkf5dnrn3282")))

(define-public crate-mmledger-0.1.9 (c (n "mmledger") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "197fvfs177xycl174r0sapjpzj12mp2hr593xhdnvjgndp5fsazx")))

(define-public crate-mmledger-0.1.10 (c (n "mmledger") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "0j4dypqlr0y8bzqzh9z7hbisb6qdzwpcp13a91wfpd4yja2g6s1f")))

(define-public crate-mmledger-0.1.11 (c (n "mmledger") (v "0.1.11") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)))) (h "032h8hfndhx4afh1p02ljwz5a2w6fryjz4kwkfjd6j1hvqz0c9nf")))

(define-public crate-mmledger-0.2.0 (c (n "mmledger") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lset") (r "^0.3.0") (d #t) (k 0)) (d (n "primordial") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)))) (h "1kizyz70fvaci535v48ald98i0inip5li2p0r7xl7vrghz7clvq2") (r "1.57")))

(define-public crate-mmledger-0.3.0 (c (n "mmledger") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "lset") (r "^0.3.0") (d #t) (k 0)) (d (n "primordial") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)))) (h "08q0y5d9vp49fbcqmwxn1nnzsscy09kv7d0d46yrx9028ha9gs05") (r "1.57")))

(define-public crate-mmledger-0.4.0 (c (n "mmledger") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "const-default") (r "^1.0.0") (d #t) (k 0)) (d (n "lset") (r "^0.3.0") (d #t) (k 0)) (d (n "primordial") (r "^0.5.0") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1vxpg2bfkw7mbp7qs1db4xzja9rshj6ii2z65kq76dyrjinz7syx") (r "1.57")))

