(define-module (crates-io mm de mmdeploy) #:use-module (crates-io))

(define-public crate-mmdeploy-0.2.1 (c (n "mmdeploy") (v "0.2.1") (d (list (d (n "mmdeploy-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)))) (h "14jawdd9dvhj18l6py4ylxqd6l78wwj7pw4qy4amv3h6ap92byfc")))

(define-public crate-mmdeploy-0.3.0 (c (n "mmdeploy") (v "0.3.0") (d (list (d (n "mmdeploy-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)))) (h "0432876dqd2mnygwx3bl7v3b3pzzb6b2wlr2276q3bpnh2dbg0bs")))

(define-public crate-mmdeploy-0.4.0 (c (n "mmdeploy") (v "0.4.0") (d (list (d (n "mmdeploy-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04fv4xlww9az58bkam2gdz6gf79nbmkcbpgdsz9n5h7y84g183a9")))

(define-public crate-mmdeploy-0.5.0 (c (n "mmdeploy") (v "0.5.0") (d (list (d (n "mmdeploy-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yq1h0hhgqjim71s83zisky3aqk2l4v4n49a0cpf7vayjssnw1m9")))

(define-public crate-mmdeploy-0.6.0 (c (n "mmdeploy") (v "0.6.0") (d (list (d (n "mmdeploy-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pp07llmzgcaw2y6hazxscq0hwdaxnd73l2y84g3m18skfhvzyjw")))

(define-public crate-mmdeploy-0.7.0 (c (n "mmdeploy") (v "0.7.0") (d (list (d (n "mmdeploy-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02g6f3mrzxbrk7sn6l3chx3s9px0pcgw2hhqdk42ih138gd7nlgp")))

(define-public crate-mmdeploy-0.8.0 (c (n "mmdeploy") (v "0.8.0") (d (list (d (n "mmdeploy-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y2pl09hgq15jvwycgcsx409zd5m62dfh1zr7vaxscplz6ylilyr")))

(define-public crate-mmdeploy-0.8.2 (c (n "mmdeploy") (v "0.8.2") (d (list (d (n "mmdeploy-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07p0s2wa9ankzkd0ddbzanx4wmjsmb23s603fd0266xs4iwfh2kk")))

(define-public crate-mmdeploy-0.9.0 (c (n "mmdeploy") (v "0.9.0") (d (list (d (n "mmdeploy-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "opencv") (r "^0.68") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07ngf2nya0m9ypnd8xqgbkc4l01b62fyr327lmi51y4l2379n4bs")))

