(define-module (crates-io mm de mmdeploy-sys) #:use-module (crates-io))

(define-public crate-mmdeploy-sys-0.1.0 (c (n "mmdeploy-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1v0lz7505bcl1gbsf887pqbbdk7vhh0lw2kvbnlrz2wlcfqnjyhn")))

(define-public crate-mmdeploy-sys-0.2.0 (c (n "mmdeploy-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "10026wr48njkbagfml3k5l8sd8ax7ldzx0m4qdl1kqadzgrwcf5s") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.3.0 (c (n "mmdeploy-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1yz5dchlxwfv23cch0bafm2zxwdcqiizfklnsyxi5svklziq4a76") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.3.1 (c (n "mmdeploy-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "182x031bi7lyhn3n7r6av44gfpki4bqmkw072zj1xb9yhy67crqg") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.4.0 (c (n "mmdeploy-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0r470bs3cpl3xdsgd66d67ba900vb2j3ps3b7cka6s87k69nv998") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.5.0 (c (n "mmdeploy-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "194ypghfmay0hpa74nax428jsz35jr8v9nhp75k9jw41qk3m3pys") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.6.0 (c (n "mmdeploy-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1cx5y5di87qg8rab3xh57sd0bbwz1hb2sjpanjb9flz5y1dp5whp") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.7.0 (c (n "mmdeploy-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1hf13h48lswm4wkcfj604c29kgp957pq3mz7bx7gq2l5w4asj5db") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.8.0 (c (n "mmdeploy-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0gvrdp99snk5i6864wjmpfy9pwkn8wrnmgp7pk6chn3sj3vn7pad") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.8.1 (c (n "mmdeploy-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1rdcic5ixwml0ddlvrj391k911bm7aq4rgn5qa2cnr1ngkbqw9p9") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.9.0 (c (n "mmdeploy-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "006bz2ws451smzcv7y8kdr3a0qhxbcp08fq5k62kkwfazy1y85z2") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.9.1 (c (n "mmdeploy-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0xhh2g6sd4cc45hwp0966sdyxvv2jgaizvv0d8r89w509iaa72cp") (f (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-1.0.0 (c (n "mmdeploy-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0srhs9azbixb6343xsx94363wx164xdz2r7fzrxdb246xc1m3a6d") (f (quote (("static") ("build" "static"))))))

