(define-module (crates-io mm #{0_}# mm0_deepsize_derive) #:use-module (crates-io))

(define-public crate-mm0_deepsize_derive-0.1.1 (c (n "mm0_deepsize_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1phvq840hjldjiln4adjng6q632nixxqa4igqiy7imq9p6mqgkwy") (f (quote (("nodummy") ("default" "nodummy"))))))

(define-public crate-mm0_deepsize_derive-0.1.2 (c (n "mm0_deepsize_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cbc4kazmfp6d4nc6myjd4rbgk5wq2waqbshvvsvhbj7jlrpyd4d") (f (quote (("nodummy") ("default" "nodummy"))))))

