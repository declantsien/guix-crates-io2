(define-module (crates-io mm #{0b}# mm0b_parser) #:use-module (crates-io))

(define-public crate-mm0b_parser-0.1.0 (c (n "mm0b_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "mm0_util") (r "^0.1.1") (k 0)) (d (n "zerocopy") (r "^0.4") (d #t) (k 0)))) (h "1fnfn8wvnhyfhcbvmf7hh6f95nihk9r19swwf5cz7na1gm01nayh")))

(define-public crate-mm0b_parser-0.1.1 (c (n "mm0b_parser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "mm0_util") (r "^0.1.1") (k 0)) (d (n "zerocopy") (r "^0.5") (d #t) (k 0)))) (h "03b86626i1ydfgykah3r6lndm54aqkb2w7m81myy50p5i6dq0ya1")))

(define-public crate-mm0b_parser-0.1.2 (c (n "mm0b_parser") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "mm0_util") (r "^0.1.4") (k 0)) (d (n "zerocopy") (r "^0.5") (d #t) (k 0)))) (h "14mv6lxdv2w6a6kh7c53wls5g7jk20kx5pap7m6909ydazw3v5v6")))

(define-public crate-mm0b_parser-0.1.3 (c (n "mm0b_parser") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "mm0_util") (r "^0.1.4") (k 0)) (d (n "zerocopy") (r "^0.5") (d #t) (k 0)))) (h "0svn62syyq7icl0cidygbqck9c7c8j6z3n209sa4bl4k1yc53z1v")))

(define-public crate-mm0b_parser-0.1.4 (c (n "mm0b_parser") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "mm0_util") (r "^0.1.4") (k 0)) (d (n "zerocopy") (r "^0.5") (d #t) (k 0)))) (h "1db5v8zif3kmd8afz18i50snxap47a3wfwhy4g2sv8jpfqw2g0wh")))

