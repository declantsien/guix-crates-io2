(define-module (crates-io mm px mmpx) #:use-module (crates-io))

(define-public crate-mmpx-0.1.0 (c (n "mmpx") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (f (quote ("jpeg" "png"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0mray79m5ibag278imi1l4czpqagf97lvzbjgsa3mj17vax08vyd")))

