(define-module (crates-io mm _e mm_example_proc_macro) #:use-module (crates-io))

(define-public crate-mm_example_proc_macro-0.1.1 (c (n "mm_example_proc_macro") (v "0.1.1") (d (list (d (n "macro_magic") (r "^0.1.1") (d #t) (k 0)) (d (n "mm_example_crate") (r "^0.1.1") (d #t) (k 0)))) (h "14qh4fhk5gnh3scd5slv2g47dx2rysxfk2h22k3dr1bbrfdj4ywv") (f (quote (("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

(define-public crate-mm_example_proc_macro-0.1.3 (c (n "mm_example_proc_macro") (v "0.1.3") (d (list (d (n "macro_magic") (r "^0.1.3") (d #t) (k 0)) (d (n "mm_example_crate") (r "^0.1.3") (d #t) (k 0)))) (h "1a2kkg9cq1k7168k565dywldgmwqvqwbzhyr28i15a1lilzlg583") (f (quote (("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

(define-public crate-mm_example_proc_macro-0.1.4 (c (n "mm_example_proc_macro") (v "0.1.4") (d (list (d (n "macro_magic") (r "^0.1.4") (d #t) (k 0)) (d (n "mm_example_crate") (r "^0.1.4") (d #t) (k 0)))) (h "06qx8gwckaia74fp8rcbx2hxbp5rliiwcviqghqjqdg5q0kyl933") (f (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

