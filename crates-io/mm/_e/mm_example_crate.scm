(define-module (crates-io mm _e mm_example_crate) #:use-module (crates-io))

(define-public crate-mm_example_crate-0.1.1 (c (n "mm_example_crate") (v "0.1.1") (d (list (d (n "macro_magic") (r "^0.1.1") (d #t) (k 0)))) (h "1czsiwb8y3kk4vmna050rrivhzsyj20lch8q00izix1kgi6hqgb9") (f (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate-0.1.3 (c (n "mm_example_crate") (v "0.1.3") (d (list (d (n "macro_magic") (r "^0.1.3") (d #t) (k 0)))) (h "1x6fgi3gmcjkxcbfbq2nw9a2kmrzgh2x2gd01vpwvmvpj3i4v5wi") (f (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate-0.1.4 (c (n "mm_example_crate") (v "0.1.4") (d (list (d (n "macro_magic") (r "^0.1.4") (d #t) (k 0)))) (h "0byw22bmnyfdp5za4mxhkcbknzfpwgnmblhaw52178yd1nybnrdy") (f (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect") ("default"))))))

