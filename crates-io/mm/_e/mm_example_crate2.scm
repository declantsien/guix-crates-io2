(define-module (crates-io mm _e mm_example_crate2) #:use-module (crates-io))

(define-public crate-mm_example_crate2-0.1.1 (c (n "mm_example_crate2") (v "0.1.1") (d (list (d (n "macro_magic") (r "^0.1.1") (d #t) (k 0)))) (h "085rkdwrzhy83b9krflb35r9p5bihhy1j13klfvvfvzx6kc66g66") (f (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate2-0.1.3 (c (n "mm_example_crate2") (v "0.1.3") (d (list (d (n "macro_magic") (r "^0.1.3") (d #t) (k 0)))) (h "0k3135g8hqradn35ws2cx7fc4gnv6g7s0wh6hyna7fpiwmhhrn7x") (f (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate2-0.1.4 (c (n "mm_example_crate2") (v "0.1.4") (d (list (d (n "macro_magic") (r "^0.1.4") (d #t) (k 0)))) (h "004kldb52vbg57brsgfjyarj9mzn56h541l5ip3mynxvab8dh0gp") (f (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect") ("default"))))))

