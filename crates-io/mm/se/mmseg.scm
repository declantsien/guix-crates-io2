(define-module (crates-io mm se mmseg) #:use-module (crates-io))

(define-public crate-mmseg-0.0.1 (c (n "mmseg") (v "0.0.1") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0ggqp7jh5qzzym5nzpfvln125va5qxr9cwakvgghikswab4ax52g") (f (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.1.0 (c (n "mmseg") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0xv02adgfc5kfpy900g186driyvdrkgf485402ahl2p5lbwwzwhg") (f (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.1.1 (c (n "mmseg") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)))) (h "1rq5zsjjiydpj9zjmxkp13jd76rjxg3jkrik0456xhp1981xgz95") (f (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.2.0 (c (n "mmseg") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)))) (h "1v0nnbchlf6iqn4x68j34r92nargmp6qx4vrv10820yihzkbhskw") (f (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.3.0 (c (n "mmseg") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)))) (h "142jkq47la69yc4snahi5jf3px5jydgrrd3aj5hvvjrxqwa863wv") (f (quote (("embed-dict") ("default" "embed-dict"))))))

