(define-module (crates-io mm #{32}# mm32f5) #:use-module (crates-io))

(define-public crate-mm32f5-0.1.0 (c (n "mm32f5") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0qd9l73dinv6kdnk568s2s1fwyphyhms3g3l0yj61g4ck0mv8vwq") (f (quote (("rt" "cortex-m-rt/device") ("default" "critical-section"))))))

