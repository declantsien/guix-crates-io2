(define-module (crates-io mm _i mm_image) #:use-module (crates-io))

(define-public crate-mm_image-0.0.1 (c (n "mm_image") (v "0.0.1") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1f9gp63nphp1diq62syh6vhkpz818vy1q9fyy5qyxnxr05zwhbm2")))

(define-public crate-mm_image-0.0.2 (c (n "mm_image") (v "0.0.2") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "0xz2k7nb5zd018x77ryfx3mid20k4g8g1y28yab66cgxj005lw0r")))

(define-public crate-mm_image-0.0.3 (c (n "mm_image") (v "0.0.3") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1707llxpawz739ycn60lzciyls20lp2j63272nwyi814z5nvxnl1")))

(define-public crate-mm_image-0.1.0 (c (n "mm_image") (v "0.1.0") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1ys4y2i1lvg1xhdgqsi6ml73m5zlf16xx6sqiabnjabv248sl8hc")))

(define-public crate-mm_image-0.1.1 (c (n "mm_image") (v "0.1.1") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1z36jlvca9f9mys4v9bhh9g4rhn0wq67lifp7i5ai7wlarf6nr49") (y #t)))

(define-public crate-mm_image-0.1.2 (c (n "mm_image") (v "0.1.2") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "0xx51c92y6vwyn9ffdh8x4mgix4rzf5jqnmwxqga52p7ksj0sqci") (y #t)))

(define-public crate-mm_image-0.1.3 (c (n "mm_image") (v "0.1.3") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1rxj462zcv1jxd7xzz2qk9yq4z3vnrjpw1si9la5kjq478jyvkfy") (y #t)))

(define-public crate-mm_image-0.1.4 (c (n "mm_image") (v "0.1.4") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1i4b837460fv2x2d615ksqfn3czs31dr8yfv5h8kqlsmzs5rn1f3") (y #t)))

(define-public crate-mm_image-0.1.5 (c (n "mm_image") (v "0.1.5") (d (list (d (n "mm_math") (r "*") (d #t) (k 0)))) (h "1vcljc6w18f2qiqgycs88p3srqc02rlmqdkpvwxsbwl7aiq5bslv") (y #t)))

