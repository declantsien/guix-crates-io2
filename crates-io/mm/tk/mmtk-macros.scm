(define-module (crates-io mm tk mmtk-macros) #:use-module (crates-io))

(define-public crate-mmtk-macros-0.12.0 (c (n "mmtk-macros") (v "0.12.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k573s5vgpdj5ffihnrhdk95bm3ij4pvzv9vlydrsarzszif88lf")))

(define-public crate-mmtk-macros-0.13.0 (c (n "mmtk-macros") (v "0.13.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1q7fdbsiqrmpnzarh21bbx4nr609jz1p2dbx7j1fd1flkcpd829w")))

(define-public crate-mmtk-macros-0.14.0 (c (n "mmtk-macros") (v "0.14.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w1sx1rjkbalg1ndrn19nyjnsydc4m4rmn7fv1nz2vvzi1sj7vyf")))

(define-public crate-mmtk-macros-0.15.0 (c (n "mmtk-macros") (v "0.15.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nq1q7pwd8ygildxx23rmnhk12y9g0x31s73mh70v5jxaic4j8ga")))

(define-public crate-mmtk-macros-0.16.0 (c (n "mmtk-macros") (v "0.16.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12ywszm483rydgqb35lz1wql4819pvsg3wd5rjk9canpp6dzafmy")))

(define-public crate-mmtk-macros-0.17.0 (c (n "mmtk-macros") (v "0.17.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hd5z3zzwzp7zi7kyx8my7pnz851jfgjkgfda392j85xkyqvq67n")))

(define-public crate-mmtk-macros-0.18.0 (c (n "mmtk-macros") (v "0.18.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03519zm6d51g3sm6sh6lqygl7bbmay9mmrcj15gmr6zankasihfb")))

(define-public crate-mmtk-macros-0.19.0 (c (n "mmtk-macros") (v "0.19.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p08qvg012sfldlm89lyb4hrwmy4k2y4svaqhfxyss4niij7iwfp")))

(define-public crate-mmtk-macros-0.20.0 (c (n "mmtk-macros") (v "0.20.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mh4rn04bj5xxs6h479fx70c9jqi76m7kzk7j4s99srvr8b2d9d6")))

(define-public crate-mmtk-macros-0.21.0 (c (n "mmtk-macros") (v "0.21.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0w9qc0a2myyx5c6axz6zc0kph6npcjr47m3668kpimjs31w5fkpf")))

(define-public crate-mmtk-macros-0.22.0 (c (n "mmtk-macros") (v "0.22.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xqs33ddsvcr2n96ddiq15n64618004g7yxqi6bra0mkh82ys8nq")))

(define-public crate-mmtk-macros-0.22.1 (c (n "mmtk-macros") (v "0.22.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n7ki6fflvfsjjigfnszdw6rf5lm3sskzqz253c9h4n0m23i1wkg")))

(define-public crate-mmtk-macros-0.23.0 (c (n "mmtk-macros") (v "0.23.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kr9ma0a8c3755q81iqzsf2hg87cmxry7lgyyfy12ayca0jnwf0h")))

(define-public crate-mmtk-macros-0.24.0 (c (n "mmtk-macros") (v "0.24.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bw22l4z57a0a738ky2pn8g0x2fdk779cpqihrkkl49gxdbc60xi")))

(define-public crate-mmtk-macros-0.25.0 (c (n "mmtk-macros") (v "0.25.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14whmkggpx9shig80ad11rc72sli3lplv1hd27zd82iw944f38ja")))

