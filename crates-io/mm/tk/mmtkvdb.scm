(define-module (crates-io mm tk mmtkvdb) #:use-module (crates-io))

(define-public crate-mmtkvdb-0.0.1 (c (n "mmtkvdb") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "15fr4pa40gw68ycsjzxgh4rba91b9jd0zi8k28dnirl6dy3aqwrs") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.2 (c (n "mmtkvdb") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0gxi078kjaa17clxkln1n9b2r8s8xw6bi8dpx1pnqjvzznfw325w") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.3 (c (n "mmtkvdb") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0v96jdghgyi1js1r9ssljw98j032qmi6lpm17l79g60zy5hxj4bc") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.4 (c (n "mmtkvdb") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1vbbv697kgqv4sfwkdx7qzdsc72h5ic3xy36qh4q19x8ziwyn08q") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.5 (c (n "mmtkvdb") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1qyqxwf3cx2hg7762cks1fsrpcm6c1gfcikxkmm546g19p86ck7s") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.6 (c (n "mmtkvdb") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0ck341kd84vcv9bissgsc0cacvy8p8v3bl0cs4r7z2naj29629q4") (l "lmdb")))

(define-public crate-mmtkvdb-0.0.7 (c (n "mmtkvdb") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1h37432jlwf8ra3r0js2dsr1y6s4jxphjbkd8267mxi3i16fnq4n") (l "lmdb")))

(define-public crate-mmtkvdb-0.1.0 (c (n "mmtkvdb") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1gbpfh90079dhxnx01q6dibq59vbg8gmwh16pbrqrybcnlqbhy6c") (l "lmdb")))

(define-public crate-mmtkvdb-0.2.0 (c (n "mmtkvdb") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "14iim5syysvf2mlrz8nbnzgm8jbs1372mcvy1zn47db5gwsvz1yx") (l "lmdb")))

(define-public crate-mmtkvdb-0.3.0 (c (n "mmtkvdb") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0vcn2vvqjb14ks1nsshs7kzjqk7a4zcsnjgjkxyw6a6slb0xa4jp") (l "lmdb")))

(define-public crate-mmtkvdb-0.3.1 (c (n "mmtkvdb") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1r7rg1n4lpfgnahyvyj195d9qqkwignyyigcj8yj36kdpznmzarn") (l "lmdb")))

(define-public crate-mmtkvdb-0.4.0 (c (n "mmtkvdb") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0dpvcx1c95nn5pkxpwlnym976h788kask1nrxg6j147bzfim0r0g") (l "lmdb")))

(define-public crate-mmtkvdb-0.4.1 (c (n "mmtkvdb") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "09bxykmaqf96478nakqm59g744zmbpz4apgl7hz5jjllzdx3nw9a") (l "lmdb")))

(define-public crate-mmtkvdb-0.5.0 (c (n "mmtkvdb") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.6.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1bmvzr9cv1gv3f092v7yr1x4jgilcnm6j22c6pc3rf21sc0y84bi") (l "lmdb")))

(define-public crate-mmtkvdb-0.6.0 (c (n "mmtkvdb") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0n7vql9icyi2hy0xi0grbxalrlva3788s57qp670c9v8nqwiakck") (l "lmdb")))

(define-public crate-mmtkvdb-0.7.0 (c (n "mmtkvdb") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0wvhja86rm5in9r86682v0hvbd1rnlf0fw59r1spaij3dabm5vdz") (l "lmdb")))

(define-public crate-mmtkvdb-0.8.0 (c (n "mmtkvdb") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0dz7md4i2k5km9qrkignp07wlqarp8k44a4l5dijflhr4caapjkk") (l "lmdb")))

(define-public crate-mmtkvdb-0.8.1 (c (n "mmtkvdb") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0wg916b07ah48z0pwhr7hy6m16zaf9vijqq39clw83cxpq795j5v") (l "lmdb")))

(define-public crate-mmtkvdb-0.8.2 (c (n "mmtkvdb") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1d8wh9j2siwyq1m0id64d1gyzp1lbqrcqlz8q0rkq8jbx6if9lna") (l "lmdb")))

(define-public crate-mmtkvdb-0.9.0 (c (n "mmtkvdb") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1i9qhp9qi7n9izxcc106ls4jj6dm2gdacrlrfwdm4h9xgp9773z5") (l "lmdb")))

(define-public crate-mmtkvdb-0.9.1 (c (n "mmtkvdb") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1a671xay9jaxabzvqwk8cp3h5wr7hn1j5qsmkpyn230b8flq5ri0") (l "lmdb")))

(define-public crate-mmtkvdb-0.9.2 (c (n "mmtkvdb") (v "0.9.2") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1cqp5pzyxqg9paizdj2dv3666sn3ncj2pmd6667cx3wc3sm0hk60") (l "lmdb")))

(define-public crate-mmtkvdb-0.9.3 (c (n "mmtkvdb") (v "0.9.3") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1fm6i6p06m963mh2ki3kaxzwgvc8x5zx1v18np4i3j8am3bjbzzm") (l "lmdb")))

(define-public crate-mmtkvdb-0.9.4 (c (n "mmtkvdb") (v "0.9.4") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "0zwjlzbfcjmfh47xdhangsk0cxhm27pash4zs05yq9qx487nj6yd") (l "lmdb")))

(define-public crate-mmtkvdb-0.10.0 (c (n "mmtkvdb") (v "0.10.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1nc193jji0pskx7yjka0qz6xmyrgrxkkysx9wdapzlm2zv7kk6h8") (l "lmdb")))

(define-public crate-mmtkvdb-0.11.0 (c (n "mmtkvdb") (v "0.11.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "0s9cq0bair3zn29dxpvpj65gdsd97dbpbdfjgscpmazvsvfckm6j") (l "lmdb")))

(define-public crate-mmtkvdb-0.12.0 (c (n "mmtkvdb") (v "0.12.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1gb4dqmzqbm8mnhk1spvhpksf64jrckn4mf4m283xx8q3kq3kgak") (l "lmdb")))

(define-public crate-mmtkvdb-0.12.1 (c (n "mmtkvdb") (v "0.12.1") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1lrc0l2wlmg47ldpg5qdj2hwzv8q3j5m47m7bb9527q33696kgdk") (l "lmdb")))

(define-public crate-mmtkvdb-0.13.0 (c (n "mmtkvdb") (v "0.13.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "0yhkyzd13c308yxvz0cj6b6m6v72r2sy3i9lqv7m6g8gfrnhl5gh") (l "lmdb")))

(define-public crate-mmtkvdb-0.14.0 (c (n "mmtkvdb") (v "0.14.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "0rwhn0i581x73bszwyvc3smjggzx9sc6l37zhggq93aazgajvxq2") (l "lmdb")))

(define-public crate-mmtkvdb-0.14.1 (c (n "mmtkvdb") (v "0.14.1") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1i9ibh61jk8bycdkndi4rk4yr46rw51qhr6zsgyjsh1skiv8ykkv") (l "lmdb")))

(define-public crate-mmtkvdb-0.15.0 (c (n "mmtkvdb") (v "0.15.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1zf876b2cgz0szp43ahylf039v19ykwd76vwbbg2rjf6nfbcbg64") (l "lmdb")))

(define-public crate-mmtkvdb-0.16.0 (c (n "mmtkvdb") (v "0.16.0") (d (list (d (n "deref_owned") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1mq7wx2rchwi1007sc7vl0l4zin9lcmbpwi9vhi24hr147yh2xac") (l "lmdb")))

