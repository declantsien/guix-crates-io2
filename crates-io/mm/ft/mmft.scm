(define-module (crates-io mm ft mmft) #:use-module (crates-io))

(define-public crate-mmft-0.1.0 (c (n "mmft") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bio") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xnvfp7hzz5f980hf7qjzrf7ic62g6xlbjwypj4ydcxdq3zql6x3")))

