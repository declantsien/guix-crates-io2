(define-module (crates-io mm li mmlib) #:use-module (crates-io))

(define-public crate-mmlib-0.1.0 (c (n "mmlib") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)))) (h "16k3jhag09j93amfmixgwicss1cj8zakij2d560dnf1az475xngg")))

(define-public crate-mmlib-0.1.1 (c (n "mmlib") (v "0.1.1") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)))) (h "0dgmrnnl0chflply1jmh3xg8nri2k61md6v942lbk4qx27lrc9mv")))

(define-public crate-mmlib-0.1.2 (c (n "mmlib") (v "0.1.2") (d (list (d (n "euclid") (r "^0.22.9") (d #t) (k 0)))) (h "1nw5072psb7c1asapn89zz8h2abv0jzn5l97yc549df7879i0z95")))

