(define-module (crates-io mm a7 mma7660fc) #:use-module (crates-io))

(define-public crate-mma7660fc-0.1.0 (c (n "mma7660fc") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "0nachy63xbgp072yj9n5a1x8rsw1grxnk3fc1wd4sld6sc276svz")))

(define-public crate-mma7660fc-0.1.1 (c (n "mma7660fc") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "067g9kpz0w8gqpg3g3y4x45ym201d58v4nddkbk76vhf409zsxkn")))

(define-public crate-mma7660fc-0.1.2 (c (n "mma7660fc") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "1v3m95w1wciyiy4h1pm7yrwwgdjylpdizmyhhp1km4asw36811wc")))

