(define-module (crates-io mm s- mms-rs) #:use-module (crates-io))

(define-public crate-mms-rs-1.0.0 (c (n "mms-rs") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1falzz5zm94k1x3pf8k09dr6xz31i536jmp7nd8hnkzglpijgfa8") (f (quote (("use_results") ("default" "use_results"))))))

(define-public crate-mms-rs-2.0.0 (c (n "mms-rs") (v "2.0.0") (d (list (d (n "cbindgen") (r "^0.24.5") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.5") (o #t) (d #t) (k 1)) (d (n "csbindgen") (r "^1.7.3") (o #t) (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04niqnxx0126gkx3dyzmiakxrm34chfj6qb0z0088rdsxf6p93vy") (f (quote (("use_panics") ("dotnet" "c_api_internal" "csbindgen") ("default" "c_api" "cxx_api") ("cxx_api" "cpp_api") ("cpp_api" "c_api_internal" "cbindgen") ("c_api_internal" "use_panics") ("c_api" "c_api_internal" "cbindgen"))))))

