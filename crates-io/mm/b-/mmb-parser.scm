(define-module (crates-io mm b- mmb-parser) #:use-module (crates-io))

(define-public crate-mmb-parser-0.1.0 (c (n "mmb-parser") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1qzw8aff49mxhy1g48vprr3x77072zw304aldba3wjxa0smsilgk")))

(define-public crate-mmb-parser-0.2.0 (c (n "mmb-parser") (v "0.2.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1cr4j0d6gn354cvynfc697yyn6gv6g7d0jxg14w3yhb161hvp9nw")))

(define-public crate-mmb-parser-0.3.0 (c (n "mmb-parser") (v "0.3.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1clkcjv8vxnf2fr991p7x9lqkzbapv6k1g82i85s5y59kyl76pr8")))

(define-public crate-mmb-parser-0.4.0 (c (n "mmb-parser") (v "0.4.0") (d (list (d (n "mmb-types") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1407d08vr7jihnvmy0mjrgpjl9z4sxkl9rhh6xqa7izz54570f00")))

(define-public crate-mmb-parser-0.5.0 (c (n "mmb-parser") (v "0.5.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1mbpbz482pcv09yky7wyv0iyczjjhwk3qpg8n2ryrqa6x3xvbl52")))

(define-public crate-mmb-parser-0.5.1 (c (n "mmb-parser") (v "0.5.1") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "15fzwg3gsvli8symc09cny7xaxawrnknca3via3wjbhjcslxc9sd")))

(define-public crate-mmb-parser-0.6.0 (c (n "mmb-parser") (v "0.6.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1isya56nxwc7ja466darxdbay1jdp5acnpa1rpnm25gvybbxjkdm")))

(define-public crate-mmb-parser-0.7.0 (c (n "mmb-parser") (v "0.7.0") (d (list (d (n "mmb-types") (r "^0.3.0") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "1f7l03nki8ayml30wr9hbj0v3ip511mhkmd4lsnvl9ws3f19hf8x")))

