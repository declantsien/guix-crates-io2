(define-module (crates-io mm b- mmb-types) #:use-module (crates-io))

(define-public crate-mmb-types-0.1.0 (c (n "mmb-types") (v "0.1.0") (h "074fljkdywhcs12kv9fkvd8zclhcn6day03m4dx1ia2f43g8vjx4")))

(define-public crate-mmb-types-0.1.1 (c (n "mmb-types") (v "0.1.1") (h "1f1axb3w452pa1dcfq3hp24444af64v1ip7y4hwkvbkp7idqwppa") (y #t)))

(define-public crate-mmb-types-0.1.2 (c (n "mmb-types") (v "0.1.2") (h "17picdhz734i06s0w23krl6r4chv8s0lkgqxp5anqjixx1977q2d") (y #t)))

(define-public crate-mmb-types-0.2.0 (c (n "mmb-types") (v "0.2.0") (h "0kipj6gapx15bfwmrsx8vfn9bpcycam9hwnrzghp6mvhi6d8fc40")))

(define-public crate-mmb-types-0.3.0 (c (n "mmb-types") (v "0.3.0") (h "0j7i5q86ygf059nf3zjwqqkbzm0fj46w6f0ks7fkms61zswg1055")))

(define-public crate-mmb-types-0.3.1 (c (n "mmb-types") (v "0.3.1") (h "0qs0gnbxk4mvvlz958bknsqi1q1jc1baqqa0lmli36shia7h118i")))

