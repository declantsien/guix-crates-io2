(define-module (crates-io mm kv mmkv-proc-macro-lib) #:use-module (crates-io))

(define-public crate-mmkv-proc-macro-lib-0.0.1 (c (n "mmkv-proc-macro-lib") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xvyp71b7ssxx3blh7a24gp9lflmf8cfs4h17vc3x61ykhpdzgpq")))

(define-public crate-mmkv-proc-macro-lib-0.0.2 (c (n "mmkv-proc-macro-lib") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zyqmcghqhbiv2jrzri1yf42brpb84vqvc3sad6dhy9kk2j94hka")))

(define-public crate-mmkv-proc-macro-lib-0.0.3 (c (n "mmkv-proc-macro-lib") (v "0.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l7pf6wgad172an5abdwk0644m1vdl1md4iznb012r8gpmcgz17b")))

