(define-module (crates-io mm f_ mmf_parser) #:use-module (crates-io))

(define-public crate-mmf_parser-0.1.0 (c (n "mmf_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0i48iibywbbkp80jls6qy3k8rrwnvy40sg581bdw52z6v6bac1r0")))

(define-public crate-mmf_parser-0.1.1 (c (n "mmf_parser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1mmi47yvzc9rymyvh7z9zmwx960jkjl3af2gdjndckxha932mhl6")))

(define-public crate-mmf_parser-0.1.2 (c (n "mmf_parser") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "104c812i4fs456jjk3hibrzpj5q8nd5q9d01h1y50qfh88hdc9bl")))

