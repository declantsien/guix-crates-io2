(define-module (crates-io mm io mmio) #:use-module (crates-io))

(define-public crate-mmio-0.0.0 (c (n "mmio") (v "0.0.0") (h "14vg4j3hgbj594lfi94snvr8jjl6cnmqagn57br755zy40j9rpcj")))

(define-public crate-mmio-0.1.0 (c (n "mmio") (v "0.1.0") (h "01z6r6dyfkvh5zfy5bdgr860zqs8077ybdygc2xhyzna9jb6jk73")))

(define-public crate-mmio-1.0.0 (c (n "mmio") (v "1.0.0") (h "11vhp4gbmhaacgqj1qfib7f8llyibidyx263avkqj7w98zcqx4xf")))

(define-public crate-mmio-1.0.1 (c (n "mmio") (v "1.0.1") (d (list (d (n "array-macro") (r "^2.1.0") (d #t) (k 0)))) (h "09lvjjp2z61yzrcd5qklrvhd0k45pxyv9pqjrfyq9jnjpmmkm1h3")))

(define-public crate-mmio-2.0.0 (c (n "mmio") (v "2.0.0") (h "08gskwdy6777q8wr7sfkx161bg17k79j4g9b80sai3m2b7f6gc37")))

(define-public crate-mmio-2.0.1 (c (n "mmio") (v "2.0.1") (h "06nwypp840aqw5igl39s1yn6psjs6wp47qg7qkphswc95wl5v1aq")))

(define-public crate-mmio-2.1.0 (c (n "mmio") (v "2.1.0") (h "022l5xkq3d5v6ryy8l6fl9wj2aki9fyyg3np0wslyf9p1gypp1gf")))

