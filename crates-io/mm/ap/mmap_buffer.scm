(define-module (crates-io mm ap mmap_buffer) #:use-module (crates-io))

(define-public crate-mmap_buffer-0.1.0 (c (n "mmap_buffer") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04rn651m05rgl1djfmgn7v12ayyxcjq8jnwbgrkilgzi92bl08dy") (y #t)))

(define-public crate-mmap_buffer-0.1.1 (c (n "mmap_buffer") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pwqqkyc3lwh0lqyh8x95y90cw9s56y8xh22bqg4dvcq77ng6czg") (y #t)))

(define-public crate-mmap_buffer-0.1.2 (c (n "mmap_buffer") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "00bp3h3aadyh7n2vw31fckihqa45s5w7416w2ljg44hhjgva302x")))

(define-public crate-mmap_buffer-0.1.3 (c (n "mmap_buffer") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1awz8mcy8g0kyfn4698wdwv1iya92nyq8ppmg1asn9iqdy4pyaxh")))

(define-public crate-mmap_buffer-0.1.4 (c (n "mmap_buffer") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("extern_crate_std"))) (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0cr3anhhsp8ykx6rpbjkfp6sisnxfwp2j9ccr290mwv6w58nkmdw")))

