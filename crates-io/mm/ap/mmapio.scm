(define-module (crates-io mm ap mmapio) #:use-module (crates-io))

(define-public crate-mmapio-0.9.0 (c (n "mmapio") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03z0lpdqjgnaqri3kdg5fv6gn44qwa55kvfsczf1s7bn6ml71x1n")))

(define-public crate-mmapio-0.9.1 (c (n "mmapio") (v "0.9.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "handleapi" "memoryapi" "minwindef" "std" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p83pfcxlm7fnqq6l1nqv9w8xrgn0vcvb37cnwsjwnwgqv5f4102")))

