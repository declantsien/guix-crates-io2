(define-module (crates-io mm ap mmap_bytey) #:use-module (crates-io))

(define-public crate-mmap_bytey-0.1.0 (c (n "mmap_bytey") (v "0.1.0") (d (list (d (n "mmap_bytey_byte_buffer") (r "^0.1.0") (d #t) (k 0)) (d (n "mmap_bytey_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lznn31zn6icfsim2m5bc7fqs67k7bcna3zqzs996k2vzijm911i")))

(define-public crate-mmap_bytey-0.1.1 (c (n "mmap_bytey") (v "0.1.1") (d (list (d (n "mmap_bytey_byte_buffer") (r "^0.1.0") (d #t) (k 0)) (d (n "mmap_bytey_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0wjxqpwfcw64v6ac393dil9y2xa41k2kkx0i8wkdp41a30pdvxay")))

(define-public crate-mmap_bytey-0.1.2 (c (n "mmap_bytey") (v "0.1.2") (d (list (d (n "mmap_bytey_byte_buffer") (r "^0.1.0") (d #t) (k 0)) (d (n "mmap_bytey_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1j913v951qdm9i2m2kqngl7f1b6x6dv6yf85n6d4c66a3h8rwlma")))

(define-public crate-mmap_bytey-0.1.3 (c (n "mmap_bytey") (v "0.1.3") (d (list (d (n "mmap_bytey_byte_buffer") (r "^0.1.0") (d #t) (k 0)) (d (n "mmap_bytey_derive") (r "^0.1.0") (d #t) (k 0)))) (h "005fk3agvh39am1wljfsnqsjj2qi0lbcb3g9cjqbki649n87zy19")))

(define-public crate-mmap_bytey-0.2.0 (c (n "mmap_bytey") (v "0.2.0") (d (list (d (n "mmap_bytey_byte_buffer") (r "^0.2.0") (d #t) (k 0)) (d (n "mmap_bytey_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0hm2l4c3kwiy5jswyzrsk72v722ynpnwyfdf3fbb2x6vldclijkk")))

