(define-module (crates-io mm ap mmap_bytey_derive) #:use-module (crates-io))

(define-public crate-mmap_bytey_derive-0.1.0 (c (n "mmap_bytey_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0ih0hcd1igll0sck7zmmf93w9p9f4ky222hy3kf8p560xvmr990s")))

(define-public crate-mmap_bytey_derive-0.2.0 (c (n "mmap_bytey_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "05mlz0rqy9ggbahg6yf6744cdywxil207s0njrghdv82fnrj4df0")))

