(define-module (crates-io mm ap mmap-simple) #:use-module (crates-io))

(define-public crate-mmap-simple-0.1.0 (c (n "mmap-simple") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)))) (h "1k84gn6qv5h1jyqa8k6nkwbp8v4scdqfx8607cvbh0aqnkbqfmmd")))

(define-public crate-mmap-simple-0.1.1 (c (n "mmap-simple") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)))) (h "077a55nlpwmdj6iq0x1am4hsh7l0hc8i2ydxfgypizasypkblf8i")))

(define-public crate-mmap-simple-0.2.0 (c (n "mmap-simple") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 0)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)))) (h "0py8vqpczjv5x97y6bcvwvp15vj9xjdh91x8wfrnjcnx9h94j4a3")))

