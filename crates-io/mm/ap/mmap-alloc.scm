(define-module (crates-io mm ap mmap-alloc) #:use-module (crates-io))

(define-public crate-mmap-alloc-0.1.0 (c (n "mmap-alloc") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "object-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "sysconf") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1rv7ic71dsqsk8fdmx1xgin9qphi7f6mxyjkj2xgxksx8g5f3vk8") (f (quote (("test-no-std"))))))

(define-public crate-mmap-alloc-0.2.0 (c (n "mmap-alloc") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "object-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "sysconf") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1iyz1bfqj3w6pg6w4qkp72na9nmp68ka3x9gk9cgrn36j6biy757")))

