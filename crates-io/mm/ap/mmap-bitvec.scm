(define-module (crates-io mm ap mmap-bitvec) #:use-module (crates-io))

(define-public crate-mmap-bitvec-0.4.0 (c (n "mmap-bitvec") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1h7gxawfb8wxpczyqfhf13x7g29j304ra45l7av2jybg8g0gqdsf")))

(define-public crate-mmap-bitvec-0.4.1 (c (n "mmap-bitvec") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "murmurhash3") (r "^0.0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "067f8wis5rk3wj3wakhk4255lr8la74sc51nngxql0z6ik0asxsz")))

