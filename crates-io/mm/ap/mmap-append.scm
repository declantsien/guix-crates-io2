(define-module (crates-io mm ap mmap-append) #:use-module (crates-io))

(define-public crate-mmap-append-0.1.0 (c (n "mmap-append") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0q42prlr2ypdg49kja2ihswyxbmfa5aj0ldk167k2w4h59p9zya5")))

(define-public crate-mmap-append-0.2.0 (c (n "mmap-append") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0dv28kavdhxwdqf6xh7p2lnydh70vcqaa7fk5fbjb97bsi5zfbnz")))

