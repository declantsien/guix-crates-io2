(define-module (crates-io mm ap mmap) #:use-module (crates-io))

(define-public crate-mmap-0.1.0 (c (n "mmap") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1vncv3zdg6dlaf2m6dsffz790f2mb4p885pk4pcc5al3830080dx")))

(define-public crate-mmap-0.1.1 (c (n "mmap") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "08xqhvr4l3rf1fkz2w4cwz3z5wd0m1jab1d34sxd4v80lr459j0b")))

