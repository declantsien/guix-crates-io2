(define-module (crates-io mm ap mmap-storage) #:use-module (crates-io))

(define-public crate-mmap-storage-0.1.0 (c (n "mmap-storage") (v "0.1.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "1wcsf3rcbvx6qn3dx5y65dbj8ldw04hpx4xj76fx95590ysxv2f1")))

(define-public crate-mmap-storage-0.2.0 (c (n "mmap-storage") (v "0.2.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "0l9xawybgp04lfxbnhp0agcmhh7n4ksnsg35i0hcx2q1s8gffr4m")))

(define-public crate-mmap-storage-0.3.0 (c (n "mmap-storage") (v "0.3.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0p0v237v8rf0pllkgsb69nxn8l9vygc4bggfn75f22k324dxzh8b") (f (quote (("serialization" "serde") ("ser_toml" "toml"))))))

(define-public crate-mmap-storage-0.4.0 (c (n "mmap-storage") (v "0.4.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0pr9zxxv2ksqhdnmw1kgbs3v0a63a1mkbq8fmqcfdrpax1dngd0i") (f (quote (("serialization" "serde") ("ser_toml" "toml"))))))

(define-public crate-mmap-storage-0.4.1 (c (n "mmap-storage") (v "0.4.1") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0y35w688vkn6mdw56xkmaxpzfal7cr4qj62wlffsvaaszwhzc3ij") (f (quote (("serialization" "serde") ("ser_toml" "toml"))))))

(define-public crate-mmap-storage-0.5.0 (c (n "mmap-storage") (v "0.5.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "1wwpmhqn9vs4xbf9abqz53hw085hd5v9csdk48hjnf6xq0l9pf9x") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json"))))))

(define-public crate-mmap-storage-0.5.1 (c (n "mmap-storage") (v "0.5.1") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "1aqn0am37kh829m5xz68jaw0cjynas85nzkyf6rdfg44146bhbpv") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json"))))))

(define-public crate-mmap-storage-0.5.2 (c (n "mmap-storage") (v "0.5.2") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "15fgpvrbm91a58x95lva4vw4pfxyifssa7d91g42rw2hvg71am19") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json"))))))

(define-public crate-mmap-storage-0.6.0 (c (n "mmap-storage") (v "0.6.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "1ra2n8ifbax646a08c275s6c8r2z4n8ynkg01s1lbxnrlqgw1h72") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json"))))))

(define-public crate-mmap-storage-0.7.0 (c (n "mmap-storage") (v "0.7.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0gsii5qr3p4a7ar21ck0mfvys427pnhjna9bhqmybfqxhq3sip3z") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json"))))))

(define-public crate-mmap-storage-0.8.0 (c (n "mmap-storage") (v "0.8.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0wfbld8gx0d9zjn8la7dx4xxmb6wp5kz0zv0035iajphnh7mfagw") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json") ("ser_bincode" "bincode"))))))

(define-public crate-mmap-storage-0.9.0 (c (n "mmap-storage") (v "0.9.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "1m2jyxg67gk2mlndm25c3rq81klv1k2nrqpgyz1l1wmxznlkzv8q") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json") ("ser_bincode" "bincode"))))))

(define-public crate-mmap-storage-0.9.1 (c (n "mmap-storage") (v "0.9.1") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0iq35vy60bdhxa4s8zq5rp5b3gqrzy8rkkrk7k3fdv34c2k5bhgq") (f (quote (("serialization" "serde") ("ser_toml" "toml") ("ser_json" "serde_json") ("ser_bincode" "bincode"))))))

(define-public crate-mmap-storage-0.10.0 (c (n "mmap-storage") (v "0.10.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0") (o #t) (d #t) (k 0)))) (h "0hpjlrh699mpgzhs3sfck120y8fkr7qfnxdn3l54wfgzzg68hgsb") (f (quote (("serializer" "serde") ("json" "serde_json"))))))

