(define-module (crates-io mm ap mmap-vec) #:use-module (crates-io))

(define-public crate-mmap-vec-0.1.0 (c (n "mmap-vec") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z92gs0c6ndb01fxk1q5y9c0h1bifxpdz9i0v2i13bbsk1wbxki2")))

(define-public crate-mmap-vec-0.1.1 (c (n "mmap-vec") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02wq57br7x55p7mh1kxifaiv507pj5nlrbi25j0r29cmffbyn1zs") (f (quote (("default" "cache-dir") ("cache-dir" "dirs"))))))

(define-public crate-mmap-vec-0.1.2 (c (n "mmap-vec") (v "0.1.2") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vhblvpi6p1n2gh6v3msi9wqp2rwnlrqblhaj3llwahz5ai3f0kl") (f (quote (("default" "cache-dir") ("cache-dir" "dirs"))))))

(define-public crate-mmap-vec-0.1.3 (c (n "mmap-vec") (v "0.1.3") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1r1wyk886q5zaah4aw7xk1qww5x7lv8bk0dp7807rwq55g0rxiwq") (f (quote (("default" "cache-dir") ("cache-dir" "dirs"))))))

(define-public crate-mmap-vec-0.1.4 (c (n "mmap-vec") (v "0.1.4") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wj7cdf5yzgb4p7im0jqnd5rs1c0qvf6jfg44capam3d9blq4iw7") (f (quote (("default" "cache-dir") ("cache-dir" "dirs"))))))

(define-public crate-mmap-vec-0.2.0 (c (n "mmap-vec") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l260a1yxmyw412zmpnpfpvyff18q23rrdf7mdaprz8k5snsmqy7") (f (quote (("default" "cache-dir" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("cache-dir" "dep:dirs"))))))

