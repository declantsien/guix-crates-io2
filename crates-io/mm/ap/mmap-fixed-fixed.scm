(define-module (crates-io mm ap mmap-fixed-fixed) #:use-module (crates-io))

(define-public crate-mmap-fixed-fixed-0.1.0 (c (n "mmap-fixed-fixed") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "sysinfoapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f3x8d1zar40ry8q7npsg9ivlpvyw279y38m13v7pldpjms978nv")))

(define-public crate-mmap-fixed-fixed-0.1.1 (c (n "mmap-fixed-fixed") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "sysinfoapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qdy0wd7p8h62jj5lqd9dmnjj20i1c22kn9477jxx9lmqxla5nd8")))

(define-public crate-mmap-fixed-fixed-0.1.3 (c (n "mmap-fixed-fixed") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "sysinfoapi" "handleapi" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k0a8nph4fbk34xsm817xv7jpwzsf8v88bi5viilf7l0j4w8b086")))

