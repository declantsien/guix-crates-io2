(define-module (crates-io mm ap mmap-safe) #:use-module (crates-io))

(define-public crate-mmap-safe-0.0.1 (c (n "mmap-safe") (v "0.0.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "11y5v4pbfg2kij6gv7asafc73is1rp2msfk82m29x15ixhnnrv3i")))

(define-public crate-mmap-safe-0.0.2 (c (n "mmap-safe") (v "0.0.2") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "195azbflimzz9j8dzygmq11pk05ldkc91iwvzpj9y6mjgamld9v6")))

