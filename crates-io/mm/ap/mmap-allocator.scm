(define-module (crates-io mm ap mmap-allocator) #:use-module (crates-io))

(define-public crate-mmap-allocator-0.1.0 (c (n "mmap-allocator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)))) (h "1awk1ldnxcsjg0bs3wzqxnf79vv22fy4x8m69g6i2qnn3m7akalk")))

(define-public crate-mmap-allocator-0.2.1 (c (n "mmap-allocator") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)))) (h "0kqqzfrhs2s9jjyhn9rq3q1iv99m45kzz35hlp9vm606i9zwnysb")))

(define-public crate-mmap-allocator-0.3.0 (c (n "mmap-allocator") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "011vlsh828lxqvm7s1qhjzx6ddmwrg0xz88ghacqxy6syb1cnqp1")))

(define-public crate-mmap-allocator-0.3.1 (c (n "mmap-allocator") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1ix6nrncjv0ql8hlh83dxf19lkg7s1gv78l6dbakl1gjmpiis3cz")))

