(define-module (crates-io mm ap mmap-sync) #:use-module (crates-io))

(define-public crate-mmap-sync-1.0.0 (c (n "mmap-sync") (v "1.0.0") (d (list (d (n "bytecheck") (r "~0.6.8") (k 0)) (d (n "memmap2") (r "^0.7.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.40") (f (quote ("validation" "strict"))) (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16cq66azwrvc5f508zgv49bqkv2kzhi0dilqnzlbwlix59g968s2")))

(define-public crate-mmap-sync-1.0.1 (c (n "mmap-sync") (v "1.0.1") (d (list (d (n "bytecheck") (r "~0.6.8") (k 0)) (d (n "memmap2") (r "^0.7.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.40") (f (quote ("validation" "strict"))) (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06v0f5kh32m7dr2qdfllis73rvkcblgjsw7a3vsrb3arbkgm8hj5")))

(define-public crate-mmap-sync-1.0.2 (c (n "mmap-sync") (v "1.0.2") (d (list (d (n "bytecheck") (r "~0.6.8") (k 0)) (d (n "memmap2") (r "^0.7.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rkyv") (r "^0.7.40") (f (quote ("validation" "strict"))) (d #t) (k 0)) (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0bvc22q2j608j0hcsdn9q3gp8iph9m3pslvi9443jy76kdqa85yn")))

