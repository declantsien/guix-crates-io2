(define-module (crates-io mm ap mmap-fixed) #:use-module (crates-io))

(define-public crate-mmap-fixed-0.1.2 (c (n "mmap-fixed") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1906f6w1rdaah5v8xflld53pb8jbkx3arz0rjss6gkm09zy1m9ir")))

(define-public crate-mmap-fixed-0.1.3 (c (n "mmap-fixed") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1xpjzkr64y7c35hzjba5xx89rrzs7crma5krvm6zc8jf4iir1qa9")))

(define-public crate-mmap-fixed-0.1.4 (c (n "mmap-fixed") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1s8l738c1hfwn3v5frr16q709ha7i6nj6wh7aln6war6rhyqrskl")))

(define-public crate-mmap-fixed-0.1.5 (c (n "mmap-fixed") (v "0.1.5") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "08bvs08wxijkfx635cbc2324s1vdksygcjcm0ysd6hv39lkaxh97")))

(define-public crate-mmap-fixed-0.1.6 (c (n "mmap-fixed") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "sysinfoapi" "handleapi" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "106lbziidb8bwi3bs4hx5znjlwkh7x6x08qdg0914rpcd2w1qw1v")))

