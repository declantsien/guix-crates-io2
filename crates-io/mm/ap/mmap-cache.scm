(define-module (crates-io mm ap mmap-cache) #:use-module (crates-io))

(define-public crate-mmap-cache-0.1.0 (c (n "mmap-cache") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.9") (d #t) (k 2)) (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)))) (h "1i07w5vra517w99djrhippf05rbl37b09gwm1jv4cw8cq04h8hqz")))

