(define-module (crates-io mm ap mmap_bytey_byte_buffer) #:use-module (crates-io))

(define-public crate-mmap_bytey_byte_buffer-0.1.0 (c (n "mmap_bytey_byte_buffer") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.9.6") (d #t) (k 0)) (d (n "mmap-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0qmqar500yb8rrh6748vv25xy0i5izqjh0zbpg4w0bya81anc9vv")))

(define-public crate-mmap_bytey_byte_buffer-0.2.0 (c (n "mmap_bytey_byte_buffer") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.9.6") (d #t) (k 0)) (d (n "mmap-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "16l1bnx08c3jnmlwgmxg8zhw0lqam2nfqzn002aiaiywvlsafpna")))

(define-public crate-mmap_bytey_byte_buffer-0.2.1 (c (n "mmap_bytey_byte_buffer") (v "0.2.1") (d (list (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.9.6") (d #t) (k 0)) (d (n "mmap-rs") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1fj76wjfsflillvah7nwgdx3aha8g9qb84fzn7yzyj3gzk6y1z94")))

