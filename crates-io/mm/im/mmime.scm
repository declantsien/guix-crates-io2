(define-module (crates-io mm im mmime) #:use-module (crates-io))

(define-public crate-mmime-0.1.0 (c (n "mmime") (v "0.1.0") (d (list (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "13q1h3x4gxr9cwx1i5hqfczkjcfn2j928yydhy85vqhxvnidx1hd")))

(define-public crate-mmime-0.1.1 (c (n "mmime") (v "0.1.1") (d (list (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "14q53qwcfrp6zm5yhgvnyz1c9yv34jw7sl51zkqkc34482iny951")))

(define-public crate-mmime-0.1.2 (c (n "mmime") (v "0.1.2") (d (list (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "14cb05fmp5davxnk3vgxifkjcd8vziin98v8qbal8hrsp3n17yxy")))

