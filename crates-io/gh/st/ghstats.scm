(define-module (crates-io gh st ghstats) #:use-module (crates-io))

(define-public crate-ghstats-0.1.0 (c (n "ghstats") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (f (quote ("reqwest-blocking"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "upon") (r "^0.7.1") (d #t) (k 0)))) (h "0zr6fcmk06v9iba7sknfjnsyhm4kax4fl4ryll2y10alavc8llj5")))

