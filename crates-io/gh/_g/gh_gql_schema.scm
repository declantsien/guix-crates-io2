(define-module (crates-io gh _g gh_gql_schema) #:use-module (crates-io))

(define-public crate-gh_gql_schema-0.1.0 (c (n "gh_gql_schema") (v "0.1.0") (d (list (d (n "cynic") (r "^3") (f (quote ("rkyv"))) (d #t) (k 0)) (d (n "cynic-codegen") (r "^3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0l86snglsvxqk5mk60q3rf0wr64xbzwc7x93ppa14jg5n0pgqk2a")))

(define-public crate-gh_gql_schema-0.1.1 (c (n "gh_gql_schema") (v "0.1.1") (d (list (d (n "cynic") (r "^3") (f (quote ("rkyv"))) (d #t) (k 0)) (d (n "cynic-codegen") (r "^3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10xa2w24il1xrf9gb6s8j89i3gpxdi11haspck5jbwa3x31qqwna")))

(define-public crate-gh_gql_schema-0.2.0 (c (n "gh_gql_schema") (v "0.2.0") (d (list (d (n "cynic") (r "^3") (f (quote ("rkyv"))) (d #t) (k 0)) (d (n "cynic-codegen") (r "^3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1fi4gqf749gfmijdj1ajn5k1ridda4brzxy7yxdw1fd69pjah02c")))

