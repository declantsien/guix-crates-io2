(define-module (crates-io gh _a gh_api) #:use-module (crates-io))

(define-public crate-gh_api-0.1.0 (c (n "gh_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.0") (d #t) (k 0) (p "reqwest-wasm")))) (h "0wnx2gqs5g4m6r6nvrdwx4rhb4ry1b6mvfx7vn1pdgqfghx9byxj") (f (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.2.0 (c (n "gh_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "1m3qyrxw1kcw2fyy818fqic38gagq7hjpyp9qifalr8b9r27fjpq") (f (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.3.0 (c (n "gh_api") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)))) (h "1iflz6r4kc5mz5swlvlbhrmyisk94ic98pnjnhgicks8lyz53ljv") (f (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.3.1 (c (n "gh_api") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.11.12") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0) (p "reqwest-wasm")))) (h "07mq9b7mvrrv4rh7zgpfb8kpqdrpf0b5934kb0ngqk0nr9wma7gi") (f (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

