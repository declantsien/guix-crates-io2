(define-module (crates-io gh _a gh_actions) #:use-module (crates-io))

(define-public crate-gh_actions-0.0.2 (c (n "gh_actions") (v "0.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.2") (d #t) (k 0)))) (h "0b5fvrblf1l397lcqib00xxkb7ql9gs4ac3wks3h5p0nbp7fi9pq") (r "1.56")))

(define-public crate-gh_actions-0.0.3 (c (n "gh_actions") (v "0.0.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "hubcaps") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.2") (d #t) (k 0)))) (h "0x84wdckh91kzrck6lwsdh07bj9000nkp0c9p5nyqfbfaiy0cvqg") (r "1.56")))

