(define-module (crates-io gh ee ghee-cli) #:use-module (crates-io))

(define-public crate-ghee-cli-0.6.1 (c (n "ghee-cli") (v "0.6.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ghee-lang") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0d1xzc0rrhmplg8c5d44svqnn9iw0fq6bkxnc58m2xlnqqxvyakp")))

(define-public crate-ghee-cli-0.6.2 (c (n "ghee-cli") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 1)) (d (n "ghee-lang") (r "^0.6.1") (d #t) (k 0)) (d (n "ghee-lang") (r "^0.6.1") (d #t) (k 1)) (d (n "nom") (r "^7.1.3") (d #t) (k 1)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 1)))) (h "177k5ycqv8fgbhxldxg80spm4g1drddycgpm51dis0b7fvn4jqqh")))

