(define-module (crates-io gh ee ghee-lang) #:use-module (crates-io))

(define-public crate-ghee-lang-0.6.1 (c (n "ghee-lang") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1fb67jn9snjj0cvzaqfin9x5y2z99sbyjqj6477d1qvzafvmm0ny")))

