(define-module (crates-io gh -f gh-file-curler) #:use-module (crates-io))

(define-public crate-gh-file-curler-1.0.0 (c (n "gh-file-curler") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1yazrsazcirs3qf2h03653llkrgipapbg6877hdlyz31dfyh8n0r")))

(define-public crate-gh-file-curler-1.1.0 (c (n "gh-file-curler") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0wjcw5r9mr2c68iy8a24z2cnhq5lrkaqhbgqqwnbqb52vi0f98xa")))

(define-public crate-gh-file-curler-1.2.0 (c (n "gh-file-curler") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1i8n57i3v3vrf9rb57iyfk4wf424aypqiz2gg094f7zimq1af9iz")))

(define-public crate-gh-file-curler-1.3.0 (c (n "gh-file-curler") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0x8lpfxy9ch9ygxalq8gkz27p7n05fp9ay51178m9ll7qnfg6qib")))

(define-public crate-gh-file-curler-1.4.0 (c (n "gh-file-curler") (v "1.4.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1vldv2hiasjvgqxb6mjr26fp8nyrr8dvx8y55yqq2ccr8jhic010")))

(define-public crate-gh-file-curler-1.5.0 (c (n "gh-file-curler") (v "1.5.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1rdy774x0ppa1yps3hn0s001d1myaxf2d88nm43ldy90m5s19a7m")))

(define-public crate-gh-file-curler-2.0.0 (c (n "gh-file-curler") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1abdjidd41w5zjnghgx88nmp0hlaqm60iv0pv8f2pa7bn6j55bal")))

(define-public crate-gh-file-curler-2.0.1 (c (n "gh-file-curler") (v "2.0.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0m5bd64gddplb6nwmf9lz8ciqgym50rl3fb2vw3nc32brcxp1m9n")))

(define-public crate-gh-file-curler-2.0.2 (c (n "gh-file-curler") (v "2.0.2") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0yzms7hzxgvgg129dbb05xz5p06c0pbs90zbb9vjlr1pjy16w171")))

(define-public crate-gh-file-curler-2.1.0 (c (n "gh-file-curler") (v "2.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0xf3f1ssxvf9m6wsmz0bsmmb7sfhwkf94hf4gfgxigba618kini8")))

(define-public crate-gh-file-curler-2.1.1 (c (n "gh-file-curler") (v "2.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "10xwpc4b5whqg0k3frasrydbxsaxx7m6vjh2qbk3k6qwyrn58p2r")))

(define-public crate-gh-file-curler-2.2.0 (c (n "gh-file-curler") (v "2.2.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "11yi814lpcc3hcnmhbcy5jdv7948pjfanlxv2xs7hiyc5k7l1y93")))

(define-public crate-gh-file-curler-2.3.0 (c (n "gh-file-curler") (v "2.3.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1aswha4r0f92v1s8avj6j7yy0fjd1pj073dh3c0h2bankax8swma")))

(define-public crate-gh-file-curler-2.4.0 (c (n "gh-file-curler") (v "2.4.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1rgkkpqck99004mdf4jc0ijv4k5yxy5da7dy5z4s1z9w0i04waf0")))

