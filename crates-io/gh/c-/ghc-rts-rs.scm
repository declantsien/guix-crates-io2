(define-module (crates-io gh c- ghc-rts-rs) #:use-module (crates-io))

(define-public crate-ghc-rts-rs-0.0.1 (c (n "ghc-rts-rs") (v "0.0.1") (d (list (d (n "cabal-rs") (r "^0.0.1") (d #t) (k 1)) (d (n "duct") (r "^0.9.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 1)))) (h "032lqw9iyabxl7zhsc5izdda14z997gkdslizff7c1yfw6g56pwm") (y #t)))

