(define-module (crates-io gh _r gh_release) #:use-module (crates-io))

(define-public crate-gh_release-0.1.0 (c (n "gh_release") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "serde_json" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10rwfzw07jiyai937k86n0mbp9b06y1arjs7jkbbd4r9qdnznzrm")))

(define-public crate-gh_release-0.1.1 (c (n "gh_release") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "serde_json" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m8azsqavhbnh88nl52q79iwldrw39wjc1nnzrmnf96kk94rwsnd") (f (quote (("git"))))))

