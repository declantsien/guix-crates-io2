(define-module (crates-io gh ar gharial) #:use-module (crates-io))

(define-public crate-gharial-0.1.2 (c (n "gharial") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "040gbhinmd4xchn6gxiypx91h2fhg5523vs5w2h13wdmzrzplv88")))

(define-public crate-gharial-0.1.3 (c (n "gharial") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "07dkr5xz50isvpc82blz8iqlml6c8xgady4f59ly6anr270ygqhd")))

(define-public crate-gharial-0.2.0 (c (n "gharial") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1fwi9fs61lwd4yg0iqriv523xf2f7zk646h7vmi5v4vam3ls4lpy")))

(define-public crate-gharial-0.3.0 (c (n "gharial") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0prkbw3izjzgv17mjd4gj5q4gj5nknqj79s5v4gfhdg602dlva1z")))

(define-public crate-gharial-0.3.1 (c (n "gharial") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1a0cgmzv5i9xpn09ypiga6b5zy7zciah0jk96md6y8w4bvin3dpc")))

(define-public crate-gharial-0.3.2 (c (n "gharial") (v "0.3.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "15hcjhx6mfxbfk2wah8jclrdkqvwxf3mybdvs3n16faznw07v9p1")))

