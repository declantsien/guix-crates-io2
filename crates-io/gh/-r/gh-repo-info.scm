(define-module (crates-io gh -r gh-repo-info) #:use-module (crates-io))

(define-public crate-gh-repo-info-0.1.0 (c (n "gh-repo-info") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "06y3b1ly3yb53gwqlxihbjl53irh184sm9f7s2hv5pcqxwq2qw99") (f (quote (("blocking" "reqwest/blocking"))))))

