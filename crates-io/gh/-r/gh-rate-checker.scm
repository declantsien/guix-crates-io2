(define-module (crates-io gh -r gh-rate-checker) #:use-module (crates-io))

(define-public crate-gh-rate-checker-0.1.0 (c (n "gh-rate-checker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1lv7jb6ic5y8rkc6n0hxw5a3sh3i6rcb5z6dzkwm0j83l8n53wwn")))

(define-public crate-gh-rate-checker-0.2.0 (c (n "gh-rate-checker") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "105wxx4gz0p17g63dda53n6l9l7c1lsnqybmcjfz51fk4waia2rq")))

