(define-module (crates-io gh bu ghbu) #:use-module (crates-io))

(define-public crate-ghbu-0.1.0 (c (n "ghbu") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0y7rx928lvzcqdk5hh1mrcnhn9iv8vg8jhxgyr1y1953ic96nhid")))

(define-public crate-ghbu-0.1.1 (c (n "ghbu") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "05hyrxnvxy133dmkn1ydm78bbfgq95ggm8bmrknbp8fwfc18q0a1")))

(define-public crate-ghbu-0.1.2 (c (n "ghbu") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0q6bisa7489yv8rsyznpkwhlqby17sxdx88wmssa2p3wkfh9r63r")))

(define-public crate-ghbu-0.1.3 (c (n "ghbu") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "118ivygv7na2cz8mfbh9xwmk24dadjgvi34qrprs90ciksr57658")))

(define-public crate-ghbu-0.1.4 (c (n "ghbu") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "013ap30pdlji9yhg50cqrkjyfv0iz35n3ykdhfswd58br5vl2mjm")))

(define-public crate-ghbu-0.1.5 (c (n "ghbu") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0xxvnhf7r7yddab9gfgjjyrwpnwx2ybrzs68ylkvf0rn5iwdjq8p")))

(define-public crate-ghbu-0.1.6 (c (n "ghbu") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "172a61xjp3c08hg75731nfidx85q3zpig0dk547p929l4iazb8zn")))

(define-public crate-ghbu-0.1.7 (c (n "ghbu") (v "0.1.7") (d (list (d (n "akshually") (r "^0.2.3") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "16017xpsh02vz58fyl4aw0pmvngi6p31flqx7s4ify7ak4kbq5r1")))

