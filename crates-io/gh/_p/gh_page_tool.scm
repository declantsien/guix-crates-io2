(define-module (crates-io gh _p gh_page_tool) #:use-module (crates-io))

(define-public crate-gh_page_tool-0.1.0 (c (n "gh_page_tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1dwnzr891jg38nlz4l81gibk6kifg3vm0l6pa3db1jz05r4jkbjz")))

(define-public crate-gh_page_tool-0.2.0 (c (n "gh_page_tool") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0071z32iki1pwfym97c108xf3mwjzrg1ihdb277l3cgin3wnqrrp")))

(define-public crate-gh_page_tool-0.4.0 (c (n "gh_page_tool") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1sy5zr0cs34q7asjk72kn181h9gbhqgv5m2k8rscb93bj0i0jg3d")))

