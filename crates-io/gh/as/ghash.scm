(define-module (crates-io gh as ghash) #:use-module (crates-io))

(define-public crate-ghash-0.0.0 (c (n "ghash") (v "0.0.0") (h "0v6yqaf4xg82cqngnlkzl5273r00pk95bxd3c5biciv0x2ypaqqf")))

(define-public crate-ghash-0.1.0 (c (n "ghash") (v "0.1.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.1") (d #t) (k 0)))) (h "1wczr9kvrpa4syqwfai9q9a0lr3krxh4znm3l0sg44fp7zr78csn") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.2.0 (c (n "ghash") (v "0.2.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.2") (d #t) (k 0)))) (h "1m5snz10l2swx3qczg9pq6q7fqk675rwnp60v45pfv6qnwgf0z22") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.2.1 (c (n "ghash") (v "0.2.1") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "17q6wh7amjiyaz6vr0hx81zwwg4q91nlr0d77jb65n8crvgj20my") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.2.2 (c (n "ghash") (v "0.2.2") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "0288cqg090q1nj8s83dyczlpkqxkgr91dr18i52wnv1s7qky93dp") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.2.3 (c (n "ghash") (v "0.2.3") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0lijv1y6qcysnxv45ny5fjvc4v9gmpggxlj6xa4l065737nk02cz") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.3.0 (c (n "ghash") (v "0.3.0") (d (list (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "polyval") (r "^0.4") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0c957q9sk1q93pqqfvhcmflfm1zvbr14aznfpm25kqd6i437zqnn") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.3.1 (c (n "ghash") (v "0.3.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.4.4") (f (quote ("mulx"))) (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0xd362xh17hadc2194dd6kjjq0ak1j4x7kkmfmpq9hw2s564wc4p") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.4.0 (c (n "ghash") (v "0.4.0") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.5") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1kq8zv5xc61p3f50hqkapvf1kk17a7zkpk3r2dcsc6ypchlrrpwh") (f (quote (("std" "polyval/std"))))))

(define-public crate-ghash-0.4.1 (c (n "ghash") (v "0.4.1") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.5") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0kn62isnkghwb82q8czka9wlhzp6r6nfkj4blrld5syjdnib4vsz") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft"))))))

(define-public crate-ghash-0.4.2 (c (n "ghash") (v "0.4.2") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0lvqvayb32fg5wbmbajzy0zdp0xljisvmnr7i587s8qilg561gbv") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft") ("armv8" "polyval/armv8"))))))

(define-public crate-ghash-0.4.3 (c (n "ghash") (v "0.4.3") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r "=1.3") (o #t) (k 0)))) (h "1hd5a71c8nl4ac156jl2bqbjp488x4j7s90mj9hxx1316qww8hml") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft") ("armv8" "polyval/armv8"))))))

(define-public crate-ghash-0.4.4 (c (n "ghash") (v "0.4.4") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r ">=1, <1.4") (o #t) (k 0)))) (h "169wvrc2k9lw776x3pmqp76kc0w5717wz01bfg9rz0ypaqbcr0qm") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft") ("armv8" "polyval/armv8"))))))

(define-public crate-ghash-0.5.0-pre (c (n "ghash") (v "0.5.0-pre") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "=0.6.0-pre") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "00mlc44igdq0xyps5mwzf6rhjlci23zdfq1vv75x19yc2398mccb") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft") ("armv8" "polyval/armv8")))) (r "1.56")))

(define-public crate-ghash-0.5.0-pre.1 (c (n "ghash") (v "0.5.0-pre.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "=0.6.0-pre.1") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1xb836zwcwhyb4azxaff09zix9j6cbhch26jnvid7nwarfff2bc8") (f (quote (("std" "polyval/std") ("force-soft" "polyval/force-soft") ("armv8" "polyval/armv8")))) (r "1.56")))

(define-public crate-ghash-0.5.0-pre.2 (c (n "ghash") (v "0.5.0-pre.2") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "=0.6.0-pre.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1aap7rllmd2qz4i78s1lgy6d947rv3fac5jwmad7a6nga582paxb") (f (quote (("std" "polyval/std")))) (r "1.56")))

(define-public crate-ghash-0.5.0 (c (n "ghash") (v "0.5.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.6") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0h1y3v3kj8xxkf2snv1yly0lr20fdh3jrm60p382szbiwl6pac6r") (f (quote (("std" "polyval/std")))) (r "1.56")))

(define-public crate-ghash-0.5.1 (c (n "ghash") (v "0.5.1") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "^0.6.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1wbg4vdgzwhkpkclz1g6bs4r5x984w5gnlsj4q5wnafb5hva9n7h") (f (quote (("std" "polyval/std")))) (r "1.56")))

(define-public crate-ghash-0.6.0-pre.0 (c (n "ghash") (v "0.6.0-pre.0") (d (list (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "polyval") (r "=0.7.0-pre.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1dr7mhn2qx415qnwrybaw98m2z5p03qvrmigvlgswiynpfbm1wj3") (f (quote (("std" "polyval/std")))) (r "1.65")))

