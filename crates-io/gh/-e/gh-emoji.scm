(define-module (crates-io gh -e gh-emoji) #:use-module (crates-io))

(define-public crate-gh-emoji-1.0.0 (c (n "gh-emoji") (v "1.0.0") (d (list (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "01y8yya9i8igag4w624kidfl82gyvafvggq30nqwmf80hwpcmsap")))

(define-public crate-gh-emoji-1.0.2 (c (n "gh-emoji") (v "1.0.2") (d (list (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1x1v1rv2qamyyv82bh7yyxc8bi1arsxr25ls0ak41ncakl4f9l1l")))

(define-public crate-gh-emoji-1.0.3 (c (n "gh-emoji") (v "1.0.3") (d (list (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)))) (h "1g6wkpfyx7hidmp4xdwqkv3q9dg892v1vkz18hrma85lgq5haym1")))

(define-public crate-gh-emoji-1.0.6 (c (n "gh-emoji") (v "1.0.6") (d (list (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std"))) (k 0)))) (h "1pvskjla5wbv5apf6z5wj53rr90v71a583rpnfap37b7kb7kkbyn")))

(define-public crate-gh-emoji-1.0.7 (c (n "gh-emoji") (v "1.0.7") (d (list (d (n "phf") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (f (quote ("std"))) (k 0)))) (h "17krrnwzcc8azilccml9xjg5ksqn70ibmrar01f79ha87ns69b9h")))

(define-public crate-gh-emoji-1.0.8 (c (n "gh-emoji") (v "1.0.8") (d (list (d (n "phf") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (f (quote ("std"))) (k 0)))) (h "0dwc7kr1dsb4bn8n87x7363c2c6vc4pi9fy6s74nk0r8vxhxz2pb")))

