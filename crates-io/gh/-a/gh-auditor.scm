(define-module (crates-io gh -a gh-auditor) #:use-module (crates-io))

(define-public crate-gh-auditor-0.1.0 (c (n "gh-auditor") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "hyperx") (r "^0.15.2") (d #t) (k 0)) (d (n "log") (r "= 0.4.8") (f (quote ("kv_unstable"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "snafu") (r "^0.6.0") (f (quote ("backtraces"))) (d #t) (k 0)))) (h "15mcajl72q2fdww514gwmrb12qrkjkarp8vzaqpvhwyx2n1nclh2")))

