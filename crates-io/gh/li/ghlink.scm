(define-module (crates-io gh li ghlink) #:use-module (crates-io))

(define-public crate-ghlink-0.2.10 (c (n "ghlink") (v "0.2.10") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1jnlq3xikk9lkya91maga0qih14nmnjdlclfg0ih3bi5rizkbbav")))

(define-public crate-ghlink-0.2.11 (c (n "ghlink") (v "0.2.11") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1358yqw5kwsin5m8a2hkykfq5b6c2z3mzhf9fc6philp7bdvpc3b")))

(define-public crate-ghlink-0.2.12 (c (n "ghlink") (v "0.2.12") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "0rphy8bvkfpi4fkfc5j7fzy5dq9npayfm5dnfg18bz742h8g1nvp")))

(define-public crate-ghlink-0.2.13 (c (n "ghlink") (v "0.2.13") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1w94fxbnc7gqgmk96c3g4vn7xwf63c8km0qz6vkrl1fm8pjfwbp4")))

(define-public crate-ghlink-0.2.14 (c (n "ghlink") (v "0.2.14") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1f7sb7jv80azl73rrnggvkvjkjdf373g3hmivprdp4l2id4v517f")))

(define-public crate-ghlink-0.2.15 (c (n "ghlink") (v "0.2.15") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1xw7snlzs6iv1b2cf300ryb37hhf8wmy1lham76v9llaii9k06pz")))

(define-public crate-ghlink-0.2.16 (c (n "ghlink") (v "0.2.16") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)))) (h "1ly1fzllj37q5hbn70fvwndrj99z1x2jk9yd5w8gl78j314c8qzb")))

(define-public crate-ghlink-0.2.17 (c (n "ghlink") (v "0.2.17") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)) (d (n "gix-testtools") (r "^0.13.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0nnin52dfhxspvcha1c6ynrdld592x869vrg746z7mdfp8x5addc")))

(define-public crate-ghlink-0.2.18 (c (n "ghlink") (v "0.2.18") (d (list (d (n "gix") (r "^0.62.0") (d #t) (k 0)) (d (n "gix-testtools") (r "^0.13.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 2)) (d (n "serial_test") (r "^3.1.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1w2nr5n71ag0fbq1vw20n0q9n3fsybj973j4a6gyk38wljin6j8x")))

