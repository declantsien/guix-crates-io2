(define-module (crates-io gh os ghost-lite) #:use-module (crates-io))

(define-public crate-ghost-lite-0.1.0 (c (n "ghost-lite") (v "0.1.0") (h "0vpddi17fkcja5i5lzz3hnnxs2disfclr7a1ds4xwqc5wyka146a")))

(define-public crate-ghost-lite-0.1.1 (c (n "ghost-lite") (v "0.1.1") (h "1il47aqvn8cl2b162cr5sx9bl9vbqwqbnjvdlnlji1mzrwczh6z7")))

(define-public crate-ghost-lite-0.1.2 (c (n "ghost-lite") (v "0.1.2") (h "1brn35qc3apnijcw43jrxhjq7knb6a4mghvdrbl36jja80xblfjw")))

