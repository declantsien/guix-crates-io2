(define-module (crates-io gh os ghost) #:use-module (crates-io))

(define-public crate-ghost-0.1.0 (c (n "ghost") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0x6abgi865nn7xvzfybzidwibqhf2j61f6r4lckfm7yw8ccvg5sj")))

(define-public crate-ghost-0.1.1 (c (n "ghost") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mpkhyipwcbvjgrm0k5d59lc8iabqqrizfw6vi05caskd1m60dia")))

(define-public crate-ghost-0.1.2 (c (n "ghost") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yalg3g1g3cz63n3phy7cdhh7p2qd220mrpxy96alwxbpqdwynqs")))

(define-public crate-ghost-0.1.3 (c (n "ghost") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "13cpx9kwcf828xzjzqj869k6jh864dzhyfam26sfiyj3z0qhsn13") (r "1.31")))

(define-public crate-ghost-0.1.4 (c (n "ghost") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jpr9df3xq5zvi20nkfyq8m63iwjlcgcrhqsdzgx73rynvzi7j3n") (r "1.31")))

(define-public crate-ghost-0.1.5 (c (n "ghost") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0plphkhqzyn47zspxlgy4lp6hd1fzwgj3wjhlf4wb0hp1dar0d5r") (r "1.31")))

(define-public crate-ghost-0.1.6 (c (n "ghost") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0fj42hmlgcj1rbqcshlvk65npsks4ba7vdzphb9202gawf6zw6gb") (r "1.31")))

(define-public crate-ghost-0.1.7 (c (n "ghost") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cwrmfjm9zrwcr97zza17636sh4xr5y4b8rvfpw5m8zp8m63v5s1") (r "1.31")))

(define-public crate-ghost-0.1.8 (c (n "ghost") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sy5snym5cagvsgdxdaavksc6p7cjrmjgk57ddff4dw9k65cvq39") (r "1.31")))

(define-public crate-ghost-0.1.9 (c (n "ghost") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0my3zrl95vn0c9dl2s6y5c8fm8h13i5yzz1p2wji6qwf3fswfyp7") (r "1.56")))

(define-public crate-ghost-0.1.10 (c (n "ghost") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "08hd4r3hdv7ly2cjcrk27kb5yj84g900xvxf2ygywhykrmbm2fc8") (r "1.56")))

(define-public crate-ghost-0.1.11 (c (n "ghost") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1301rxjyb6117wm69vmg3zwx2ybc8n7sj0ap060dfhx4wqhv7vrl") (r "1.56")))

(define-public crate-ghost-0.1.12 (c (n "ghost") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1dm9lq68i9b3x4can05sg2b1a9mg831wlmg423q4dz13lffy1rzs") (r "1.56")))

(define-public crate-ghost-0.1.13 (c (n "ghost") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0myj270d0piccv1rg5z72y2zxk80zmsvv235pa54pia8ijmjrxjm") (r "1.56")))

(define-public crate-ghost-0.1.14 (c (n "ghost") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yd8dwk29nf2lx8lz324kifsppcpxsm0bqmq633kn79llmq0ncxs") (r "1.56")))

(define-public crate-ghost-0.1.15 (c (n "ghost") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0kfwdf9i78v9afm99zd05mgkbi9s6inl7hgcacvnz639iw7css5c") (r "1.56")))

(define-public crate-ghost-0.1.16 (c (n "ghost") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "05zz13rpxnrp0sza201jw0pi739c8h641cy7bp6m9az6vk7fg0gg") (r "1.56")))

(define-public crate-ghost-0.1.17 (c (n "ghost") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m78l65f90m9sh4fixwnvpvp9i2ljwdr4h3b2wr7q9piv7g8bq5h") (r "1.56")))

