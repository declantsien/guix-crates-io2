(define-module (crates-io gh os ghosts-proc_macros) #:use-module (crates-io))

(define-public crate-ghosts-proc_macros-0.1.0-rc1 (c (n "ghosts-proc_macros") (v "0.1.0-rc1") (h "1v4z75yap0fwbpr5pgxc1fbdwja1vk1vkv3sb50izdlnwq1bq1id")))

(define-public crate-ghosts-proc_macros-0.1.0-rc2 (c (n "ghosts-proc_macros") (v "0.1.0-rc2") (h "1pvk6r1h7q74snlvdk3waj00igb4m5mhydhjz4s3a8zp9gk01p7q")))

(define-public crate-ghosts-proc_macros-0.1.0-rc3 (c (n "ghosts-proc_macros") (v "0.1.0-rc3") (h "1jh6fj14hl59krdcgd0npg2lajilmis1qv7s5qs3ajaknb44i8qg")))

(define-public crate-ghosts-proc_macros-0.1.0 (c (n "ghosts-proc_macros") (v "0.1.0") (h "1vpycqx5288y18kisv4ka9ivgbngwk1c15a2nb2bdqk22km4c07f")))

