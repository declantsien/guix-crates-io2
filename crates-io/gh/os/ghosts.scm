(define-module (crates-io gh os ghosts) #:use-module (crates-io))

(define-public crate-ghosts-0.1.0-rc1 (c (n "ghosts") (v "0.1.0-rc1") (d (list (d (n "ghosts-proc_macros") (r "^0.1.0-rc1") (d #t) (k 0)) (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "17p49wlqp0v7xzqw92yffifg67k212ijx9w21ndzpg9s441gw3kr") (f (quote (("ui-tests" "better-docs") ("better-docs")))) (r "1.56.0")))

(define-public crate-ghosts-0.1.0-rc2 (c (n "ghosts") (v "0.1.0-rc2") (d (list (d (n "ghosts-proc_macros") (r "^0.1.0-rc2") (d #t) (k 0)) (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "1d6rd75kh40sjmb7a07xq9sw7h00w8pfkwrj7i5jmh0qf50xrxic") (f (quote (("ui-tests" "better-docs") ("better-docs")))) (r "1.56.0")))

(define-public crate-ghosts-0.1.0-rc3 (c (n "ghosts") (v "0.1.0-rc3") (d (list (d (n "ghosts-proc_macros") (r "^0.1.0-rc3") (d #t) (k 0)) (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "1swynwwc6gnb97aa2bxb00s2s58ck0ghgrl4c7fg9xqpgs6hlcv7") (f (quote (("ui-tests") ("better-docs")))) (r "1.56.0")))

(define-public crate-ghosts-0.1.0 (c (n "ghosts") (v "0.1.0") (d (list (d (n "ghosts-proc_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "0jw3fh2g7pd50mnmm66bagwpy5a11dkrc5br4jpdv3yp00zg4dfb") (f (quote (("ui-tests") ("better-docs")))) (r "1.56.0")))

