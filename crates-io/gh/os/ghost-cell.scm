(define-module (crates-io gh os ghost-cell) #:use-module (crates-io))

(define-public crate-ghost-cell-0.1.0 (c (n "ghost-cell") (v "0.1.0") (h "0g5rws9n7zz7hwgqkk7wccsc4j6v7r6l60mq1dbcfx9drhwz24ba")))

(define-public crate-ghost-cell-0.2.0 (c (n "ghost-cell") (v "0.2.0") (h "0wmv7fxa62pxdwklkpbfhmzs1yqij8kp24vz8pr7g3rhjf615qsv") (f (quote (("experimental-ghost-cursor"))))))

(define-public crate-ghost-cell-0.2.1 (c (n "ghost-cell") (v "0.2.1") (h "02l9gncrdysbxxdv1hf5ziixa510h5adk7b2cl8vgv2dz4sxc0kf") (f (quote (("experimental-ghost-cursor"))))))

(define-public crate-ghost-cell-0.2.2 (c (n "ghost-cell") (v "0.2.2") (h "14rvc5jza20cbx8gmp7sxvyz31s8af8dfyxf6y4qka31lax9glj3") (f (quote (("experimental-ghost-cursor"))))))

(define-public crate-ghost-cell-0.2.3 (c (n "ghost-cell") (v "0.2.3") (h "1rk4s5vq6cp5v8wp7z7qfnkl9s1h5jczhnm2jwnasprd33rd7zd5") (f (quote (("experimental-multiple-mutable-borrows") ("experimental-ghost-cursor"))))))

(define-public crate-ghost-cell-0.2.4 (c (n "ghost-cell") (v "0.2.4") (h "1spvnv4dwsn4nqbhwilrb017x1vhdhdmvpxhbsa4lipcyw8vjnwc") (f (quote (("experimental-multiple-mutable-borrows") ("experimental-ghost-cursor"))))))

(define-public crate-ghost-cell-0.2.5 (c (n "ghost-cell") (v "0.2.5") (h "114fjl702j48gi8z5wnrw018b13hzs4vfx0bqp2q0chjap9pnjha") (f (quote (("experimental-multiple-mutable-borrows") ("experimental-ghost-cursor")))) (y #t)))

(define-public crate-ghost-cell-0.2.6 (c (n "ghost-cell") (v "0.2.6") (h "1dsi4f6wr917pins20930qq7zcmrnzg72bp9d68z8rqw5cs9si6q") (f (quote (("experimental-multiple-mutable-borrows") ("experimental-ghost-cursor"))))))

