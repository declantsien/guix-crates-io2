(define-module (crates-io gh os ghostwriter) #:use-module (crates-io))

(define-public crate-ghostwriter-0.1.0 (c (n "ghostwriter") (v "0.1.0") (h "18isczwnqxwqc95i2pw2arbl68hhf2insxgj44980nfwj0vq3jk2") (y #t)))

(define-public crate-ghostwriter-0.2.0 (c (n "ghostwriter") (v "0.2.0") (h "031qzh6h49r4kmd0yc1wyyxw6h116v157fx7b17jbzsk609z11f5") (y #t)))

