(define-module (crates-io gh os ghost-rs) #:use-module (crates-io))

(define-public crate-ghost-rs-0.1.0 (c (n "ghost-rs") (v "0.1.0") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0riy0sgf6zg0ilq949lh2jarqnshnc209qqb68xj4s189h67ma9v") (f (quote (("debug")))) (y #t)))

(define-public crate-ghost-rs-0.1.1 (c (n "ghost-rs") (v "0.1.1") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1nl0is675xnxmzr68wcd3b0d08dczc8r22fsj51hnaq230hxzvzb") (f (quote (("debug"))))))

