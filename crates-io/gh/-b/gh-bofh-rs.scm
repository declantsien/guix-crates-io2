(define-module (crates-io gh -b gh-bofh-rs) #:use-module (crates-io))

(define-public crate-gh-bofh-rs-1.0.0-next.2 (c (n "gh-bofh-rs") (v "1.0.0-next.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nfazwsj6gjpbavgzyr26xlbd2fxlnslkfmsp0vp786z4gp3ivd3") (r "1.64.0")))

(define-public crate-gh-bofh-rs-1.0.0-next.3 (c (n "gh-bofh-rs") (v "1.0.0-next.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y3m1jw8vf32ww39yazwv45zgnmxhiraa82q685c8lw4bkfdd8bh") (r "1.64.0")))

(define-public crate-gh-bofh-rs-1.0.0-next.4 (c (n "gh-bofh-rs") (v "1.0.0-next.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hpsx5x4fhx9p0w0rnqbk8igfif9wa4g1808aprp5v6gards4q2v") (r "1.64.0")))

(define-public crate-gh-bofh-rs-1.0.0-next.5 (c (n "gh-bofh-rs") (v "1.0.0-next.5") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1422mag18fz8x5vqz18wa155rnlk3vk18cnarz2k3sd9in9ad7ci") (r "1.64.0")))

(define-public crate-gh-bofh-rs-1.0.0-next.6 (c (n "gh-bofh-rs") (v "1.0.0-next.6") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p8rqpsdxi92x4lxcdjv78pm95yz2gm1d5sf8nywxj4h1rl71h2p") (r "1.64.0")))

(define-public crate-gh-bofh-rs-1.0.0-next.7 (c (n "gh-bofh-rs") (v "1.0.0-next.7") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1rw1rdi4jwib4ii23y4wxiywma5nysr0nmcpg0kfx7aa1cq74vfi") (r "1.70.0")))

(define-public crate-gh-bofh-rs-1.0.0 (c (n "gh-bofh-rs") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "02g0nchc4jzy0kclvpjpqcfigxdd9cfcrg9l876v0k9f3qmq3nhx") (r "1.70.0")))

(define-public crate-gh-bofh-rs-1.1.0 (c (n "gh-bofh-rs") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0jwg4aw6a3708cvzfnpkxa4s0i3r0jmap7pnxpx9y9k93klb0nq8") (r "1.70.0")))

(define-public crate-gh-bofh-rs-1.1.1-next.1 (c (n "gh-bofh-rs") (v "1.1.1-next.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0wsz330hwpsx4jcpds82q1hm666hjaan6rh8k63pxcbqkb0bwf75") (r "1.70.0")))

