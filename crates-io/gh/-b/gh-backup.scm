(define-module (crates-io gh -b gh-backup) #:use-module (crates-io))

(define-public crate-gh-backup-0.1.0 (c (n "gh-backup") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sq2kz8mbwb7xi5syl9cvp29vr01jlmlc69fqlam2x0c9phnkwlh")))

(define-public crate-gh-backup-0.1.1 (c (n "gh-backup") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.25") (d #t) (k 0)) (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07vhw3yzd65kvhhfh6y8cz68pajag65qvb26mmffdzk7da7jv2sq")))

(define-public crate-gh-backup-0.1.2 (c (n "gh-backup") (v "0.1.2") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.18") (f (quote ("https"))) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qysmfx3yp7k1i8kmc2fyvx9cc9caqgjhmb1gya4g3cmdv0wf2ir")))

