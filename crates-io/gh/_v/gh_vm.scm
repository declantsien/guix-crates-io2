(define-module (crates-io gh _v gh_vm) #:use-module (crates-io))

(define-public crate-gh_vm-0.0.1-alpha (c (n "gh_vm") (v "0.0.1-alpha") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0giysw0f9614356sfz7mdaxjrfmb9bjmb86a41qy5j80bik52wwd") (y #t)))

