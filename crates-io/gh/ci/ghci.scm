(define-module (crates-io gh ci ghci) #:use-module (crates-io))

(define-public crate-ghci-0.1.0 (c (n "ghci") (v "0.1.0") (d (list (d (n "nix") (r "0.26.*") (f (quote ("poll"))) (k 0)) (d (n "nonblock") (r "0.2.*") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.2") (d #t) (k 0)) (d (n "version-sync") (r "0.9.*") (d #t) (k 2)))) (h "0rgblv5d5h28fw11d5ld32rlray1m90hvj5klcig3abg3lir8fn8")))

