(define-module (crates-io gh -h gh-hex) #:use-module (crates-io))

(define-public crate-gh-hex-0.0.0 (c (n "gh-hex") (v "0.0.0") (d (list (d (n "pager") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1rpg4fz22nw3jl19w3gvx8pp1nh80ahpgx9ilwj1ah0ri81wdl44")))

