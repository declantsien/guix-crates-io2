(define-module (crates-io gh -l gh-labelx) #:use-module (crates-io))

(define-public crate-gh-labelx-0.1.0 (c (n "gh-labelx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gh-config") (r "^0.1") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hbphg3q5ljzb85n04bz6z76nk1v50al5b37h7va70h67w0fqqdq")))

(define-public crate-gh-labelx-0.1.1 (c (n "gh-labelx") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gh-config") (r "^0.1") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "12zn5k78lvf163fkywnyynz1km9hx7wgkcpvqf32a329cdlfb035")))

