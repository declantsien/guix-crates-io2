(define-module (crates-io gh la ghlast) #:use-module (crates-io))

(define-public crate-ghlast-0.1.0 (c (n "ghlast") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.0") (f (quote ("https-rustls" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d988xin8x1328rvvf696y33qigbc5qns13p70yipc3zpryvpidp")))

(define-public crate-ghlast-0.1.1 (c (n "ghlast") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8") (f (quote ("https-rustls" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nif5imfpah5dxffvm02b3rh2ijkffmsjjhkb4aa3yb23yknc1i9")))

(define-public crate-ghlast-0.1.2 (c (n "ghlast") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8") (f (quote ("https-rustls" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fv9ra87hgncgnb8f1vnjwlyrhqapgrjdrz647j9j3cwh80ccc7j")))

(define-public crate-ghlast-0.1.3 (c (n "ghlast") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8") (f (quote ("https-rustls" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0231c05pyi84g9s9b2sqgxbkslbddg8r1hfka8dz9hd5mnbq9yqi")))

(define-public crate-ghlast-0.1.4 (c (n "ghlast") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8") (f (quote ("https-rustls" "json-using-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0928g9ikj0908k24f8min5j6bg3gi1k990jhr3b28kr64ihw5lz5")))

