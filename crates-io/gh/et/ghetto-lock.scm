(define-module (crates-io gh et ghetto-lock) #:use-module (crates-io))

(define-public crate-ghetto-lock-0.1.0 (c (n "ghetto-lock") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memcache") (r "^0.13.1") (d #t) (k 0)))) (h "0wbkr5m6r4dv7rr7v32n1kc8h9dxlb2rbcpmv572mgyjzp2z2gwa")))

(define-public crate-ghetto-lock-0.1.1 (c (n "ghetto-lock") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memcache") (r "^0.13.1") (d #t) (k 0)))) (h "176yrrcf0w6cp8g1x5jhcwv273lf4c8lfsncgy6nm2vgs1cslv0b")))

(define-public crate-ghetto-lock-0.2.0 (c (n "ghetto-lock") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memcache") (r "^0.14.0") (d #t) (k 0)))) (h "0mavf5dayxql282qc283fzb79xnld0lni62ys3v5i7bf4pyi8s93")))

(define-public crate-ghetto-lock-0.2.1 (c (n "ghetto-lock") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "memcache") (r "^0.14.0") (d #t) (k 0)))) (h "1cqkgixif2gbw37ibxqydypl1bq6xb1gz5q08aaq6ky6l9b1m6ci")))

