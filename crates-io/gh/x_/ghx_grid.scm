(define-module (crates-io gh x_ ghx_grid) #:use-module (crates-io))

(define-public crate-ghx_grid-0.1.0 (c (n "ghx_grid") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.0") (o #t) (k 0)))) (h "0j1ln41xgks8y744p9s0k2vsjanhr2s0z8hw0ap0ppnybxapcacm") (f (quote (("reflect" "bevy")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

(define-public crate-ghx_grid-0.2.0 (c (n "ghx_grid") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (o #t) (k 0)))) (h "1hvhdkxbnvvlxlqqhkbv0g103kx2x844r068p6pxsd59z12zr57f") (f (quote (("reflect" "bevy")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

