(define-module (crates-io gh re ghrel) #:use-module (crates-io))

(define-public crate-ghrel-0.0.0 (c (n "ghrel") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "134n2m6a9g8g2kxw86g3hfqxksvpz0nk6hl7bpcnb5i5b4wk9jxn")))

