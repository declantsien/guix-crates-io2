(define-module (crates-io gh -x gh-xplr) #:use-module (crates-io))

(define-public crate-gh-xplr-0.2.1 (c (n "gh-xplr") (v "0.2.1") (h "03aqpzk933cj4n1a5b376xcf2h896kp432yn70y9fdvb6pjr5jax") (y #t)))

(define-public crate-gh-xplr-0.2.2 (c (n "gh-xplr") (v "0.2.2") (h "0s4fw08wawg6yqm7iiicvgfxpw0sskxydd01gf2fl4fvygk798nd")))

