(define-module (crates-io gh im ghimlink) #:use-module (crates-io))

(define-public crate-ghimlink-0.1.0 (c (n "ghimlink") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "git2") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.7") (d #t) (k 0)))) (h "0wcq5ykgsg8gq4bgpfy5ij9nnm217644bnr9lml6v43r7p8jky99")))

(define-public crate-ghimlink-0.2.0 (c (n "ghimlink") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "17mr441rgkrpj3r8fyjj9mak352032ya4s41wz3ncjc1wzia83av")))

