(define-module (crates-io gh op ghopac) #:use-module (crates-io))

(define-public crate-ghopac-0.1.0 (c (n "ghopac") (v "0.1.0") (d (list (d (n "hubcaps") (r "^0.3.12") (d #t) (k 0)) (d (n "hyper") (r "^0.10.12") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.1") (d #t) (k 0)) (d (n "slog-term") (r "^2.1") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "1gwz05ppwqivn1zlvrsqw7q0ahcx90s3kis494vrx8c1jarl898h")))

