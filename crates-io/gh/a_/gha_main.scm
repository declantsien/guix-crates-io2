(define-module (crates-io gh a_ gha_main) #:use-module (crates-io))

(define-public crate-gha_main-0.0.1 (c (n "gha_main") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "gha_main-proc_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "temp-env") (r "^0.3.4") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 2)))) (h "190dplk93iwikg4ypvrrnmp7jihmv7m1hs589iafxx7iixmnb0sh")))

(define-public crate-gha_main-0.0.2 (c (n "gha_main") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "gha_main-proc_macro") (r "^0.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)) (d (n "temp-env") (r "^0.3.4") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "012h5h9nw7zkb8r99h4p53ccd4jmr0qyl418drwcpbn3v3y5sq45")))

(define-public crate-gha_main-0.0.5 (c (n "gha_main") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "gha_main-proc_macro") (r "^0.0.5") (d #t) (k 0)) (d (n "gha_main_core") (r "^0.0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.3") (d #t) (k 2)) (d (n "temp-env") (r "^0.3.4") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 2)))) (h "0jr1mnfg95y2i69vx9k6ljnlg20w2fggka90k8p2q6wwhh78i1kx")))

