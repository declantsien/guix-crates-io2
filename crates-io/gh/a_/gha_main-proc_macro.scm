(define-module (crates-io gh a_ gha_main-proc_macro) #:use-module (crates-io))

(define-public crate-gha_main-proc_macro-0.0.1 (c (n "gha_main-proc_macro") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1060z1sj50lq2gkj395ksi9hmvavckrnj5gv5gfyamixjqrigiih")))

(define-public crate-gha_main-proc_macro-0.0.2 (c (n "gha_main-proc_macro") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "16cpfgxbxakpp4c1qf7xc2cdzx5qqkqggx1gmswfnydh534q5syn")))

(define-public crate-gha_main-proc_macro-0.0.5 (c (n "gha_main-proc_macro") (v "0.0.5") (d (list (d (n "gha_main_core") (r "^0.0.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)))) (h "1kqhjnbv7mchfkqb9zzkwha69gs5hb1iq8wbz1qq3j64y14mz7y2")))

