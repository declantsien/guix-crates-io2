(define-module (crates-io gh -t gh-token) #:use-module (crates-io))

(define-public crate-gh-token-0.1.0 (c (n "gh-token") (v "0.1.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1z825lds49vfxz4dj2bcci9sr9qz1d67s795mn8sqkh0x5kkyqmw")))

(define-public crate-gh-token-0.1.1 (c (n "gh-token") (v "0.1.1") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "03fi3whmaayz1bff54lh7jflqinidraigpj047smspqm7faz7xvl")))

(define-public crate-gh-token-0.1.2 (c (n "gh-token") (v "0.1.2") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0ihdg27qkqs5xq0yn9036l7fw7qlmln9580lxhwqrdjqdgdzh7yg")))

(define-public crate-gh-token-0.1.3 (c (n "gh-token") (v "0.1.3") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1aj4d2yham8cgpz2qalbjkjrvany02n7lxhivapyc4ys4rjghj5g")))

(define-public crate-gh-token-0.1.4 (c (n "gh-token") (v "0.1.4") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "02dgbh2aaxa4g7gdiw6k122n2s4fl6bzz52rri7rjqh9k9ard5y2")))

(define-public crate-gh-token-0.1.5 (c (n "gh-token") (v "0.1.5") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "115h94y3zvkq3zm5nsxvgvd8i81l8qdcqyb9hs7xm01zlzg0pfjv")))

(define-public crate-gh-token-0.1.6 (c (n "gh-token") (v "0.1.6") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.28") (d #t) (k 0)))) (h "1rnz3nrplahc1wfp5pv3ssas1pxfa68ckg4npa2hi4vjr6wnnxpk")))

(define-public crate-gh-token-0.1.7 (c (n "gh-token") (v "0.1.7") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.194") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "16jb9zbypilcd4i6zakpvapms56di2f7sl98xanqzbnvsg32l00a")))

