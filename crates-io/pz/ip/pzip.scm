(define-module (crates-io pz ip pzip) #:use-module (crates-io))

(define-public crate-pzip-0.1.0 (c (n "pzip") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "hex") (r "~0.4.2") (d #t) (k 0)) (d (n "ring") (r "~0.16.14") (d #t) (k 0)))) (h "0ypzslahakqb1ykl94x59x476rvfkfd2av4ydnm03v1r7p091a2k")))

(define-public crate-pzip-0.2.0 (c (n "pzip") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)))) (h "0ij3j51irs6lj0wqacvjglr3r0yajapyl8jf1rk68i86wbhygy2h")))

(define-public crate-pzip-0.2.1 (c (n "pzip") (v "0.2.1") (d (list (d (n "clap") (r "~3") (d #t) (k 0)) (d (n "hex") (r "~0.4") (d #t) (k 2)) (d (n "libdeflater") (r "~0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.11") (d #t) (k 0)))) (h "0ifrjx9kk8nyr844p85vg8xsw9783gr7af7gnq2prbz2p0wvprpr")))

