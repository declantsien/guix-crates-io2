(define-module (crates-io pz ma pzmacro) #:use-module (crates-io))

(define-public crate-pzmacro-0.1.0 (c (n "pzmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "170ffv9qqrlv0dfdbzfbw3f757vindd60h21c5ns0y7bffvnciyp")))

(define-public crate-pzmacro-0.2.0 (c (n "pzmacro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vr3r8k3zf0af85ik4mkqlp1lk1vir9zgdiqc9xvvd1f06pinpcp")))

(define-public crate-pzmacro-0.3.0 (c (n "pzmacro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0acbxw5wi3ax3dwb0sknisdb2nnq0higrrqc9pr9q6qi2cr7d52h")))

(define-public crate-pzmacro-0.4.0 (c (n "pzmacro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "12ss899956r3csvn917ard1jy5vzyxa34zj7gklsd17ib514h4n1")))

(define-public crate-pzmacro-0.5.0 (c (n "pzmacro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "00f3wm1nvn6f4a68yfq8dpxil2aqyx6qcsga0y4mqkw4l2yjnv27")))

(define-public crate-pzmacro-0.6.0 (c (n "pzmacro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "118i25zglsrlqyi10isb2yyy2jzdd8w57azxs9w26ds4ixsv32l1")))

