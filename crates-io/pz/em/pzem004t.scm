(define-module (crates-io pz em pzem004t) #:use-module (crates-io))

(define-public crate-pzem004t-0.1.0 (c (n "pzem004t") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "15siy043k9v771psksfwinjd296908rgc38b12yv3d8hyfpqv5w7")))

(define-public crate-pzem004t-0.1.1 (c (n "pzem004t") (v "0.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "10mxzibiw8lbxywmlrrvpnqdk4c6vy0v92ikm1wwjas7ss514bzz")))

(define-public crate-pzem004t-0.1.2 (c (n "pzem004t") (v "0.1.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1y4g3z857ric936634kw3wa9l9bhiw7n3x9azwjwd8cikza6s33i")))

(define-public crate-pzem004t-0.1.3 (c (n "pzem004t") (v "0.1.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0np1wckkhzmxqzgr5ixvxi180axfjs3khd5b8r8zds9hq8kbmahr")))

(define-public crate-pzem004t-0.1.4 (c (n "pzem004t") (v "0.1.4") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1yhx8c4if6gldb58fi5x5x12hy0qipd1rsv5gm0bprvp9rmm0m4c")))

(define-public crate-pzem004t-0.1.5 (c (n "pzem004t") (v "0.1.5") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0sl2pvk381k4kh2i64qja549y84yskp5z94m72094lvvlxmjjwls")))

(define-public crate-pzem004t-0.1.6 (c (n "pzem004t") (v "0.1.6") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1rl1crr42w960h4md32drrd577daw7a31wf7ap7pyjalyswkvgwi")))

(define-public crate-pzem004t-0.1.7 (c (n "pzem004t") (v "0.1.7") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "18q7jrvyw5wqnppdm54jfwhmchbl25mc978yr59hmq8gq2ylgj82")))

