(define-module (crates-io zr -a zr-alshasha) #:use-module (crates-io))

(define-public crate-zr-alshasha-0.1.0 (c (n "zr-alshasha") (v "0.1.0") (d (list (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.1") (d #t) (k 0)))) (h "162vfjq40kynzz20zkyzmn9q21ndcy16442yr95agn00a9bym8b8")))

(define-public crate-zr-alshasha-0.1.1 (c (n "zr-alshasha") (v "0.1.1") (d (list (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.1") (d #t) (k 0)))) (h "04d0zwsxmprrv2hijds4bqnbvdfgnfv2jb19yjw9hxpbvn7myk75")))

(define-public crate-zr-alshasha-0.1.2 (c (n "zr-alshasha") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0h5fgkjhjm5rb7gfbwpar428g0mhr0pg2kjs94s5wk86f29jfak2")))

(define-public crate-zr-alshasha-0.2.0 (c (n "zr-alshasha") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "rdev") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "09j5mdi317zd45sdb8zsay0nwa10m9rgcb8zf3i8drf9pvxmb30j")))

