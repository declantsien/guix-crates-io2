(define-module (crates-io my -l my-little-eval) #:use-module (crates-io))

(define-public crate-my-little-eval-0.1.0 (c (n "my-little-eval") (v "0.1.0") (h "006b0bi45j5yfw8an9mj91h8cin0mn5960cbm16x2lwcs9yc9852")))

(define-public crate-my-little-eval-0.1.1 (c (n "my-little-eval") (v "0.1.1") (h "08bn49cw08h17rwq1dgvxm89z70yqypdairpga0wjb31cxd7yfnm")))

