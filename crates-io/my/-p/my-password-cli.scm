(define-module (crates-io my -p my-password-cli) #:use-module (crates-io))

(define-public crate-my-password-cli-0.1.0 (c (n "my-password-cli") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "my-password") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1svxgglzw4bi1vc24yvikikjzp2l4080a4ibgyzmsa92x35bf1sk")))

