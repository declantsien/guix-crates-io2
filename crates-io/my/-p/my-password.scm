(define-module (crates-io my -p my-password) #:use-module (crates-io))

(define-public crate-my-password-0.1.0 (c (n "my-password") (v "0.1.0") (d (list (d (n "bs58") (r "^0.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "0bk1wjvp6bpgb2m5yyxfpcmvs9mv993nxxl2lrydykr3x9f118hw")))

