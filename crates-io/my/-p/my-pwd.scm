(define-module (crates-io my -p my-pwd) #:use-module (crates-io))

(define-public crate-my-pwd-0.1.0 (c (n "my-pwd") (v "0.1.0") (h "0416dsmax57z8zkaqrinzdpi7cdcfhixm2c6hbzl28564b787k8r")))

(define-public crate-my-pwd-0.1.1 (c (n "my-pwd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ga965vzz0acxk99fkqw0562fwvy6mvjxwzkzj8l9mapa279vdfg")))

(define-public crate-my-pwd-0.1.11 (c (n "my-pwd") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r0fvmagxlf5xy24fr6v47jbpxlr0abjlscs7qxz1z0x28lqy1wi")))

(define-public crate-my-pwd-0.1.12 (c (n "my-pwd") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "097xcv9ay04plvpn7fjwdghkxd1gkym4q2aldcjh2snlsppzr698")))

