(define-module (crates-io my -p my-pretty-failure) #:use-module (crates-io))

(define-public crate-my-pretty-failure-0.1.0 (c (n "my-pretty-failure") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "182p0k314kg8kswh4dq3gbwxm4zdjvfg5vsdss79g70g5ciraclx")))

(define-public crate-my-pretty-failure-0.1.1 (c (n "my-pretty-failure") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1drik87k4j4z5x252ni1f2bbrcp4csl1n10wi3zryfn3xa27kmz4")))

(define-public crate-my-pretty-failure-0.1.2 (c (n "my-pretty-failure") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0gp1cjxyjsxrm9h0c60nzbzpdp1vhfd3wjsfq879bipimvlfm0p3")))

