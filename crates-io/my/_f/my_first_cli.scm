(define-module (crates-io my _f my_first_cli) #:use-module (crates-io))

(define-public crate-my_first_cli-0.1.0 (c (n "my_first_cli") (v "0.1.0") (h "16xzwypih7516374fc2x69lrbphrgim5sqzara7pb7ncz7bs52nx")))

(define-public crate-my_first_cli-0.1.1 (c (n "my_first_cli") (v "0.1.1") (h "1jjh16rv3l60xja7nc2yqvmzlihhwzixki9pv5cy7r0g5gpjlx1w")))

(define-public crate-my_first_cli-0.1.2 (c (n "my_first_cli") (v "0.1.2") (h "1iinrp28xmy4kmfyxard4vrnsq9kfxksiaphrnbilhqcza15yj31")))

(define-public crate-my_first_cli-0.1.3 (c (n "my_first_cli") (v "0.1.3") (h "11lyqchqymgncmw3yl8y4si6x01kjrq8wk0m6qvzs4x6mxf84884")))

(define-public crate-my_first_cli-0.1.4 (c (n "my_first_cli") (v "0.1.4") (h "096y1jn9256brh8gy1wp4mc2yamvidy8g9ca22rycb6w404xyb6x")))

(define-public crate-my_first_cli-0.2.0 (c (n "my_first_cli") (v "0.2.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "05y03j9sam9g8n72yzs1g1ds2l6mdjd2i063v5r19k69yg4k6qjg")))

(define-public crate-my_first_cli-0.2.1 (c (n "my_first_cli") (v "0.2.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0cd884azynhxg84wl3j6f3ff9c3b8xws3abxvcwnwsf6ap8ms6hp")))

