(define-module (crates-io my _f my_file_crate) #:use-module (crates-io))

(define-public crate-my_file_crate-0.1.0 (c (n "my_file_crate") (v "0.1.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "my_lib_crate") (r "^0.1") (d #t) (k 0)) (d (n "my_macro_crate") (r "^0.1") (d #t) (k 0)))) (h "0fy9y58zjljnqc5534c15a27b1qwfcgq2kq618f8z0pjx3cj617c")))

(define-public crate-my_file_crate-0.1.1 (c (n "my_file_crate") (v "0.1.1") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "my_lib_crate") (r "^0.1") (d #t) (k 0)) (d (n "my_macro_crate") (r "^0.1") (d #t) (k 0)))) (h "04ji4zyqh3d08rfh8sppwg1zm7xxk86k3qddf2b4f2csvvazfz76")))

