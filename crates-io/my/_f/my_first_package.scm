(define-module (crates-io my _f my_first_package) #:use-module (crates-io))

(define-public crate-my_first_package-0.1.0 (c (n "my_first_package") (v "0.1.0") (h "09yf7k2kj89gv0fb7lb9qx5wxxwf8bfqbmxvd9si5rbic00nd4rv") (y #t)))

(define-public crate-my_first_package-1.1.0 (c (n "my_first_package") (v "1.1.0") (h "1nvyz6zf9809w6s3kwpdk6yn7qsshcnxv5fr5lcp7mh01hb2ls8c") (y #t)))

