(define-module (crates-io my lo mylogger) #:use-module (crates-io))

(define-public crate-mylogger-0.1.0 (c (n "mylogger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)))) (h "0c5v49982hpx8g60fh428a9gvxmiwqwfb5xswzfk2hwbdqf8zn79")))

(define-public crate-mylogger-0.2.0 (c (n "mylogger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1nnijwwsfvxxcbnzmi4yrwmyysrif7rjdf2b3r6qblsv9c3kacmh")))

(define-public crate-mylogger-0.2.1 (c (n "mylogger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "00xqhrfmg9j0h5d7hm8vxcfhlk6q19kpkp78jhr9p43ky0hqzdm5")))

(define-public crate-mylogger-0.2.2 (c (n "mylogger") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11270h3ajwsiw2j7753cq491v0jjwxjns8x74qj3j13szja2kpsi")))

(define-public crate-mylogger-0.2.3 (c (n "mylogger") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16vf7hkhr1cmkz7gxppp4wj9w5js1qxh362k6wi4ll1l8yb9izqi")))

(define-public crate-mylogger-0.2.4 (c (n "mylogger") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vqaib0xh95z54m3mmg3giyqmhspszimf803rmnd56zb8sf95wib")))

(define-public crate-mylogger-0.2.5 (c (n "mylogger") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0xan13qqax5w7ckp48r4cgfz7zqxdy4l6gsi1dxas096lc2bm2f5")))

