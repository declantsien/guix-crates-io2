(define-module (crates-io my lo myloginrs) #:use-module (crates-io))

(define-public crate-myloginrs-0.1.0 (c (n "myloginrs") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.29") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "0hjxm91w760sdmbqkbxxdrj11rwbbj468brdfvcxj1r134bc5gaa")))

(define-public crate-myloginrs-0.1.1 (c (n "myloginrs") (v "0.1.1") (d (list (d (n "mysql") (r "^18.0.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.29") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.2") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "1cskm1nav1sg6b9skd38naqy76fim7yzsslgm3wh6zdqmnjk0ia2")))

(define-public crate-myloginrs-0.1.2 (c (n "myloginrs") (v "0.1.2") (d (list (d (n "mysql") (r ">=20.1.0, <21.0.0") (d #t) (k 2)) (d (n "openssl") (r ">=0.10.29, <0.11.0") (d #t) (k 0)) (d (n "rust-ini") (r ">=0.15.2, <0.16.0") (d #t) (k 0)) (d (n "shellexpand") (r ">=2.0.0, <3.0.0") (d #t) (k 0)))) (h "1wskjb691lil7fcf60qpkcrar37vwjw1v1zp7fxswindxdwyqswb")))

(define-public crate-myloginrs-0.1.3 (c (n "myloginrs") (v "0.1.3") (d (list (d (n "mysql") (r "^22.1.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.29") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "0wsshnv09a4jg8rhls4gmgn25nfvkzqx6cl6iz6dfna4nwrcvvlk")))

(define-public crate-myloginrs-0.1.4 (c (n "myloginrs") (v "0.1.4") (d (list (d (n "mysql") (r "^23.0.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.29") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)))) (h "15avi01nk36g59jvmmy1kkc4hp3hx6k6yz9cnnvb68qiyq5gk8zr")))

(define-public crate-myloginrs-0.1.5 (c (n "myloginrs") (v "0.1.5") (d (list (d (n "mysql") (r "^23.0.0") (d #t) (k 2)) (d (n "openssl") (r "^0.10.29") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)))) (h "1gn8pwh3grcnjsirv900bm094xk070zd0y8bpll9wrcxjqf5jc33")))

