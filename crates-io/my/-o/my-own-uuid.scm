(define-module (crates-io my -o my-own-uuid) #:use-module (crates-io))

(define-public crate-my-own-uuid-0.1.0 (c (n "my-own-uuid") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "16dpzwmzkw413y81dbpifhklsipj5fcwbfj6gnv04ksn21yqwiz7")))

(define-public crate-my-own-uuid-0.1.1 (c (n "my-own-uuid") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1qvwg7d4l6kr6qvbijmghvzvvy55km9kypcv0cjj90yv8mnjidqq")))

(define-public crate-my-own-uuid-0.1.2 (c (n "my-own-uuid") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "15p0b7r7q5hyrk6lizv1gl4704v8hy4350sx2ixs5b5fkagfk8ac")))

