(define-module (crates-io my -f my-first-crate) #:use-module (crates-io))

(define-public crate-my-first-crate-0.1.0 (c (n "my-first-crate") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "maud") (r "^0.20.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.4.1") (d #t) (k 0)))) (h "00a7224x7al25qzh6xjad2sdj9jj24ky4daf3ndz8k22i9fp3fa6")))

