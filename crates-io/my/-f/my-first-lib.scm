(define-module (crates-io my -f my-first-lib) #:use-module (crates-io))

(define-public crate-my-first-lib-0.1.0 (c (n "my-first-lib") (v "0.1.0") (h "0b048kiky9sdmclkg4n2r7djdsfnmlgv78akhgm3hbdr5j4w9lnz") (y #t)))

(define-public crate-my-first-lib-0.0.1 (c (n "my-first-lib") (v "0.0.1") (h "1w6zqpwlc4wnrraabqizvmsknhpvr2fk1qmxhjhk8ibc4mfc0drr")))

