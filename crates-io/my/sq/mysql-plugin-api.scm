(define-module (crates-io my sq mysql-plugin-api) #:use-module (crates-io))

(define-public crate-mysql-plugin-api-0.1.0 (c (n "mysql-plugin-api") (v "0.1.0") (h "0p4z9ia2csqxrvvczlp36gsw8qi9ld361v5zf68pwrch4g8pbxwz")))

(define-public crate-mysql-plugin-api-0.1.1 (c (n "mysql-plugin-api") (v "0.1.1") (h "09qd8sbp4nkh15b39plvxzwv9zc003f5nw4yqibmj19f3w573miy")))

(define-public crate-mysql-plugin-api-0.1.2 (c (n "mysql-plugin-api") (v "0.1.2") (h "0smf91wk1jq730s8yvpbj6qzklcdqnxsl6q1q20gy5746ilpmxzf")))

(define-public crate-mysql-plugin-api-0.1.3 (c (n "mysql-plugin-api") (v "0.1.3") (h "10bq3f8n8v01z65596i26yvzqz0m9dr51rlbjg3y6q7nf4bakc9d")))

