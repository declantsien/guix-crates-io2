(define-module (crates-io my sq mysql_roaring) #:use-module (crates-io))

(define-public crate-mysql_roaring-0.1.0 (c (n "mysql_roaring") (v "0.1.0") (d (list (d (n "mysql_roaring_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "roaring") (r "^0.10.1") (d #t) (k 0)) (d (n "udf") (r "^0.5.3") (d #t) (k 0)))) (h "0mnhfzlvwfr804kjcxvk9pjvdsy6g5pj8gddpvdi538i7q3nsh16")))

