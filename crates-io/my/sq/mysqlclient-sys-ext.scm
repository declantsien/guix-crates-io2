(define-module (crates-io my sq mysqlclient-sys-ext) #:use-module (crates-io))

(define-public crate-mysqlclient-sys-ext-0.2.5 (c (n "mysqlclient-sys-ext") (v "0.2.5") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0cbg1gxf4q1md113xavgmqbh5h5snnhg0zj7q7gca3cqx6zk9mgs")))

