(define-module (crates-io my sq mysql_fetcher) #:use-module (crates-io))

(define-public crate-mysql_fetcher-0.1.0 (c (n "mysql_fetcher") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "mysql") (r "^12.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "015s522xlccdcfl02i0kxan6fpzmxcq4nlf626qk99aycdkkng22")))

(define-public crate-mysql_fetcher-0.1.1 (c (n "mysql_fetcher") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "mysql") (r "^12.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0zm1l3ir0y76zkl80ia77hy5cg47pyj6iks334qig4ifmca7v66n")))

(define-public crate-mysql_fetcher-0.1.2 (c (n "mysql_fetcher") (v "0.1.2") (d (list (d (n "log") (r "^0.3") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "mysql") (r "^12.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0fnp1lnlcjh1vqaf6iaynjp9r9wg4s505rjpr1332i2jppzfyyds")))

(define-public crate-mysql_fetcher-0.1.3 (c (n "mysql_fetcher") (v "0.1.3") (d (list (d (n "log") (r "^0.3") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "mysql") (r "^12.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0vx8sksz6nxfhdmnlzsjpnjl9ijd8x1h9waby19qdi6kd1ha78m4")))

