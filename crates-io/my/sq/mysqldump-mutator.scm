(define-module (crates-io my sq mysqldump-mutator) #:use-module (crates-io))

(define-public crate-mysqldump-mutator-0.0.1 (c (n "mysqldump-mutator") (v "0.0.1") (d (list (d (n "bigdecimal") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "simple_logger") (r "^1.5.0") (d #t) (k 2)) (d (n "utf8-chars") (r "^0.1.2") (d #t) (k 0)))) (h "1jglckx33vabp0nv3915p5wx6qki4273qnac833x5rcabllnj5vz")))

