(define-module (crates-io my sq mysql-proxy) #:use-module (crates-io))

(define-public crate-mysql-proxy-0.1.0 (c (n "mysql-proxy") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "19df1walal6np7arzfprv1ckq10djmz2ssqi4rqbarw8ww6b9jhr")))

(define-public crate-mysql-proxy-0.1.1 (c (n "mysql-proxy") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1d4l9pnmw6zmwd5pp4j6c6ls7hxwyrh7cxzmbbmisjx90c77696x")))

(define-public crate-mysql-proxy-0.1.2 (c (n "mysql-proxy") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0q47ln0fcpsfy6dpq4xx96l7s1s25bcg0kqbyrjxwpc5s4v63l11")))

(define-public crate-mysql-proxy-0.1.3 (c (n "mysql-proxy") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0514qyrfhhpzl8ia3l5vq3wp57rd3za7n4v5amdj5cq70ihk6zj9")))

(define-public crate-mysql-proxy-0.1.4 (c (n "mysql-proxy") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "017a9vki0wnh7mm7gc80xr6ry3pmw0b2p20q6fkn55q9r5ag0n2k")))

(define-public crate-mysql-proxy-0.1.5 (c (n "mysql-proxy") (v "0.1.5") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0ms4igf57pwavl2f561k4f0z90f4p3k4q5rakqqiw89wsi815dlf")))

(define-public crate-mysql-proxy-0.1.6 (c (n "mysql-proxy") (v "0.1.6") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1w458lyqcqsvn1q7mxidvfsxz5k3az1cmf3d271rji8f6ndz2270")))

(define-public crate-mysql-proxy-0.1.7 (c (n "mysql-proxy") (v "0.1.7") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "01n0zf2ls4gmssmq750g6chj6aas8545pincbv8188d1bqwgxypb")))

(define-public crate-mysql-proxy-0.1.8 (c (n "mysql-proxy") (v "0.1.8") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0g7vsc4rv76fb5nckq581sm0qnifkik86vaxw50cyh4181110a2l")))

(define-public crate-mysql-proxy-0.1.9 (c (n "mysql-proxy") (v "0.1.9") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "0bmcs3g9gnrz69y9sa3iz9gvkcd1y1hzwzj2xahw815vyln82dhw")))

(define-public crate-mysql-proxy-0.2.0 (c (n "mysql-proxy") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1lr8dddhd8p2wx1sq64y1i55fah57lgxklpfps58hhlff113aars")))

(define-public crate-mysql-proxy-0.2.1 (c (n "mysql-proxy") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "curl") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "04bwfd3nhfy09av6mqwxr7wf0knds406nw2lakplma2l6hqhn0g3")))

