(define-module (crates-io my sq mysql_crypt) #:use-module (crates-io))

(define-public crate-mysql_crypt-0.1.0 (c (n "mysql_crypt") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1f1grl21a8v3a3m0hc59z69i3nbz46bplhyk0823b445q1fzxwbg")))

