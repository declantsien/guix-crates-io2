(define-module (crates-io my sq mysql_async_support) #:use-module (crates-io))

(define-public crate-mysql_async_support-0.1.0 (c (n "mysql_async_support") (v "0.1.0") (d (list (d (n "mysql_async_support_model") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql_async_support_rt") (r "^0.1.0") (d #t) (k 0)))) (h "08pnq9dnvciyn5ny3gih85wx0pi92jynpshn485hrya4ip95m2xs")))

(define-public crate-mysql_async_support-0.1.1 (c (n "mysql_async_support") (v "0.1.1") (d (list (d (n "mysql_async_support_model") (r "^0.1.0") (d #t) (k 0)) (d (n "mysql_async_support_rt") (r "^0.1.0") (d #t) (k 0)))) (h "03ndlydjhxpd5l8rj4wwqkab7bgw2gnadnjw7k7aq0w7q6syxdgz")))

(define-public crate-mysql_async_support-0.2.0 (c (n "mysql_async_support") (v "0.2.0") (d (list (d (n "mysql_async_support_model") (r "^0.2.0") (d #t) (k 0)) (d (n "mysql_async_support_rt") (r "^0.2.0") (d #t) (k 0)))) (h "0nrkbrjnjswq1468d72jfh5z098hs5b1272wlrpcswwfda3n4bdh")))

(define-public crate-mysql_async_support-0.2.1 (c (n "mysql_async_support") (v "0.2.1") (d (list (d (n "mysql_async_support_model") (r "^0.2.0") (d #t) (k 0)) (d (n "mysql_async_support_rt") (r "^0.2.0") (d #t) (k 0)))) (h "19lihav9al3w3yyj4n1i8jf941w3djflnf4byqisc3fd5bajxkb2")))

(define-public crate-mysql_async_support-0.3.0 (c (n "mysql_async_support") (v "0.3.0") (d (list (d (n "mysql_async_support_model") (r "^0.3.0") (d #t) (k 0)) (d (n "mysql_async_support_rt") (r "^0.3.0") (d #t) (k 0)))) (h "03h51jgq6vrzc4f7h70bvvvj38gy5xgryv2ygzpplrbq4m9nkawl")))

