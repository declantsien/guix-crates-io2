(define-module (crates-io my sq mysql_helper) #:use-module (crates-io))

(define-public crate-mysql_helper-0.1.0 (c (n "mysql_helper") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1pn5x8h1rf4kx9smjixqh374j8hi98p48v61h1apg30fx5sfpjnw") (y #t)))

(define-public crate-mysql_helper-0.1.1 (c (n "mysql_helper") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1vanikgk546l1p8k3h5fdgbv8cj0cmys9gp27vzryck4rir0f127") (y #t)))

(define-public crate-mysql_helper-0.1.2 (c (n "mysql_helper") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "11xvc55fpdmxj7a2hwa031w54i832mwq1jfwsbzsyy7lplm8776q") (y #t)))

(define-public crate-mysql_helper-0.1.3 (c (n "mysql_helper") (v "0.1.3") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0pp9xcjaw2gg5n1vz7k6sg0ir3nf5b87v1cz01xv5idfxszs491x") (y #t)))

(define-public crate-mysql_helper-0.1.4 (c (n "mysql_helper") (v "0.1.4") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0z1pd4rmjz076mi909alh7y2bpgnlqsax3vp8p7vhmr2j4idvmvr") (y #t)))

(define-public crate-mysql_helper-0.1.5 (c (n "mysql_helper") (v "0.1.5") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1wjk1cihrwpgk171csxbhxsharmbv83hbwcawjkmajhnhf547iz1") (y #t)))

(define-public crate-mysql_helper-0.1.6 (c (n "mysql_helper") (v "0.1.6") (d (list (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1jwwyfgla1yzxgizv6bw77ip4knc422z62hmbwy81ymfagvk0gk9")))

