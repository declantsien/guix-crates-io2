(define-module (crates-io my sq mysql_macros) #:use-module (crates-io))

(define-public crate-mysql_macros-0.1.0 (c (n "mysql_macros") (v "0.1.0") (h "0yw56z12i1qzi8c4cl49rg53qnxdzmcl8vlw0g4w6h50zg33cza6")))

(define-public crate-mysql_macros-0.1.1 (c (n "mysql_macros") (v "0.1.1") (h "10m6m8l479f3czffz79rrrwpym06vxzpjfk6476mvsd8wlilkx77")))

(define-public crate-mysql_macros-0.1.2 (c (n "mysql_macros") (v "0.1.2") (h "0js9wiis7jl2zgrg1c008g2w8pwwqzmhc4xsm5ylxsgfw2yyqbib")))

