(define-module (crates-io my sq mysqlclient-src) #:use-module (crates-io))

(define-public crate-mysqlclient-src-0.0.1 (c (n "mysqlclient-src") (v "0.0.1") (h "18h23kzbaj627xjz3xvfxkwhbiabhwnkx3xbsf5fgi8s9jdfdrgn")))

(define-public crate-mysqlclient-src-0.1.0 (c (n "mysqlclient-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "link-cplusplus") (r "^1.0") (d #t) (k 0)) (d (n "openssl-src") (r "^300.0.0") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (f (quote ("vendored"))) (d #t) (k 0)))) (h "1qnsq2wzabwa4vcwwbwgf1nxxl6c5r0im4kc0ah0jv08jq9q5ndv") (f (quote (("with-asan") ("default")))) (l "mysqlclient_sys_src")))

