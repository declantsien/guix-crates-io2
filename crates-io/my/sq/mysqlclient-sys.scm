(define-module (crates-io my sq mysqlclient-sys) #:use-module (crates-io))

(define-public crate-mysqlclient-sys-0.1.0 (c (n "mysqlclient-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0aggi07kd5d9wf0k5i949w0w91achjgmw5d20fsscqmcn3jdpa5k")))

(define-public crate-mysqlclient-sys-0.1.1 (c (n "mysqlclient-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "18va79k2llqqgz4g372zj04wwvr7sawkn8lzcr2yyfcarmwn4q3c")))

(define-public crate-mysqlclient-sys-0.1.2 (c (n "mysqlclient-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.22.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1vjlh8slaxkjgjwf0lsc53532b1kmmsy1fm5j8wkz2q8k25chnrp")))

(define-public crate-mysqlclient-sys-0.2.0 (c (n "mysqlclient-sys") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1zlxaisks1spcc2lbq2zaxby9llfl61hkydrbwxzhvj36rsc22ix")))

(define-public crate-mysqlclient-sys-0.2.1 (c (n "mysqlclient-sys") (v "0.2.1") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1xciidq059w6i23r2bzsslis86nln0zbzhpccn128wq4ddfzy7p8")))

(define-public crate-mysqlclient-sys-0.2.2 (c (n "mysqlclient-sys") (v "0.2.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "021bnww3binmkkkx4rqgrnsmx3inbrm4gzz99p7clcyjqkaay9w3")))

(define-public crate-mysqlclient-sys-0.2.3 (c (n "mysqlclient-sys") (v "0.2.3") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1ds2k6fpg2gq6qm182pljnhf4kq0fsj350vzggc5973k727f1747")))

(define-public crate-mysqlclient-sys-0.2.4 (c (n "mysqlclient-sys") (v "0.2.4") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "11ggkcbfnmp81amc9g0j98dk17fnmqcp9smgm9w401286kckg5ky") (l "mysqlclient")))

(define-public crate-mysqlclient-sys-0.2.5 (c (n "mysqlclient-sys") (v "0.2.5") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "16wndr59cbpc2wgli45zfgi0hi837pbrsh1aqh2k0ads50akh6zn") (l "mysqlclient")))

(define-public crate-mysqlclient-sys-0.3.0 (c (n "mysqlclient-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "mysqlclient-src") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.4") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0bdd29ivayjk254dc3g6n0vh26lv0653wl43714dyx39h0sc0ndx") (f (quote (("default")))) (l "mysqlclient") (s 2) (e (quote (("bundled" "dep:mysqlclient-src") ("buildtime_bindgen" "dep:bindgen"))))))

