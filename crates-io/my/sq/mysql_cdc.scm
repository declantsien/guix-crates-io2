(define-module (crates-io my sq mysql_cdc) #:use-module (crates-io))

(define-public crate-mysql_cdc-0.1.0 (c (n "mysql_cdc") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1njn4nk8wpa356y95468hjc7nq38gmgyb2ill2rmfhqfy0mpqf21")))

(define-public crate-mysql_cdc-0.1.1 (c (n "mysql_cdc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0197ssf01jrj67cwjzz3zldbwb2bgqq2zlc4ry04sm1h9rzv19ff")))

(define-public crate-mysql_cdc-0.1.2 (c (n "mysql_cdc") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "0fgdq7r1ppmjh8h01b1znryakllx65b5vcxdkbfhm50as9s0wmm7")))

(define-public crate-mysql_cdc-0.1.3 (c (n "mysql_cdc") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "11008rqk8m8px8khbb2azbn89b7byfjp73vy2y5rkmd825wnbbk6")))

(define-public crate-mysql_cdc-0.1.4 (c (n "mysql_cdc") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "1a7fdpkhzql86v91jnnlbr3l7wmv6yx5lqaq28av4hgiwk2x1fc3")))

(define-public crate-mysql_cdc-0.1.5 (c (n "mysql_cdc") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.36") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9.8") (d #t) (k 0)))) (h "06x9z0cdwd98d5bw7j92aqhxr8frysdyizaz915pw10c3fr72wkc")))

(define-public crate-mysql_cdc-0.1.6 (c (n "mysql_cdc") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)) (d (n "sha-1") (r "^0.10.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "16pipqn2kjj19jqp39ps463jkfq6zjlr3ffxm2k7gixz0h5zcvmb")))

(define-public crate-mysql_cdc-0.1.7 (c (n "mysql_cdc") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0f5gdgprvmpsqnsgysvizch7smwzq6vz36rrrjwl89ffg55a5jm8") (y #t)))

(define-public crate-mysql_cdc-0.2.0 (c (n "mysql_cdc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "176djcq8bvxqkiqlx4m41wxjdrhbxqgaw91g4waq4pjs1k4fhxmp")))

(define-public crate-mysql_cdc-0.2.1 (c (n "mysql_cdc") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0ixq5pj5325y8siwr0gm1zlswrm71i2waiiki01z7p2z61axyylh")))

