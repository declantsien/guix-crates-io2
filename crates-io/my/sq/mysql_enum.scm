(define-module (crates-io my sq mysql_enum) #:use-module (crates-io))

(define-public crate-mysql_enum-0.1.0 (c (n "mysql_enum") (v "0.1.0") (d (list (d (n "mysql") (r "^15") (d #t) (k 0)) (d (n "mysql_enum_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.14") (d #t) (k 2)) (d (n "strum_macros") (r "^0.14") (d #t) (k 2)))) (h "1xsky5pdf2ghpjfnk5arr6r481f9zw92b82jm4ps949mivcj5ndx")))

(define-public crate-mysql_enum-0.1.1 (c (n "mysql_enum") (v "0.1.1") (d (list (d (n "mysql") (r "^15") (d #t) (k 0)) (d (n "mysql_enum_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.14") (d #t) (k 2)) (d (n "strum_macros") (r "^0.14") (d #t) (k 2)))) (h "1p715cv7s5gfdrp5rralay9z6299yh3a0cfcdx0zskvl83gbiajm")))

(define-public crate-mysql_enum-0.1.2 (c (n "mysql_enum") (v "0.1.2") (d (list (d (n "mysql") (r "^14") (d #t) (k 0)) (d (n "mysql_enum_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.14") (d #t) (k 2)) (d (n "strum_macros") (r "^0.14") (d #t) (k 2)))) (h "1zjb4bvv166j8y9viiq2rsvzp34zxzzrcd1zz9r7zphj55ajvqhk")))

(define-public crate-mysql_enum-0.1.3 (c (n "mysql_enum") (v "0.1.3") (d (list (d (n "mysql") (r "^14") (d #t) (k 0)) (d (n "mysql_enum_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.14") (d #t) (k 2)) (d (n "strum_macros") (r "^0.14") (d #t) (k 2)))) (h "1yjykgrh2rmddwzwzh7mzgjh85psjvrywp7yv1ki19sb8rm6c14d")))

