(define-module (crates-io my sq mysqlbinlog-network) #:use-module (crates-io))

(define-public crate-mysqlbinlog-network-1.0.0 (c (n "mysqlbinlog-network") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1s3bdgavg6si79aprs2n4ycassv2znl3ri9hv65gcbs2lfs5ixlh") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.1 (c (n "mysqlbinlog-network") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1idqi73h7ixdjd3zqgva9g5jjj85jw3afgr6swkwmxc9jzkc6dy3") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.2 (c (n "mysqlbinlog-network") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "060paacq0dx2l2gpsk2zr5nq974h536i45cw5027v5sxhlyxqrmj") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.3 (c (n "mysqlbinlog-network") (v "1.0.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1n45p1cpih9s4hf7n51g64rmanm0s99ymw4w98gypn05fgvfvpb6") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.4 (c (n "mysqlbinlog-network") (v "1.0.4") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "05650x082gxk3fnsijsiw5qywzwickq63j08rxxdximrgdxs3gyd") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.5 (c (n "mysqlbinlog-network") (v "1.0.5") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "155q5r2dq7j8ckgcwnyimnfbgzlkc9afkx2hhkpvw63y1yx9wfsc") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.6 (c (n "mysqlbinlog-network") (v "1.0.6") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0fvvicijz5ckxw29bkyfj31v1kmzw68fqwlrh7qhh39kcr9bgd1x") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.7 (c (n "mysqlbinlog-network") (v "1.0.7") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0iajxfv8air7f6ywyv9pn7ra2agpbfsm9bpv4by9az10vh7fgvq7") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.8 (c (n "mysqlbinlog-network") (v "1.0.8") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "mysql_binlog") (r "^0.3.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1pnnn34z85gdy3lzx2m5syx9pgz3v301h0wrwp8kvx6kwmvx0x84") (y #t)))

(define-public crate-mysqlbinlog-network-1.0.9 (c (n "mysqlbinlog-network") (v "1.0.9") (d (list (d (n "assert_matches") (r "^1") (d #t) (k 2)) (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "bigdecimal") (r "^0.1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "sys-info") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0bzxvn1j2hhxrfwgrs6wibzbv6s2clc2vh513lyxgg3zbqs3drgx")))

