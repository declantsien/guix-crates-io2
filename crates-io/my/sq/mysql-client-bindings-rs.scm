(define-module (crates-io my sq mysql-client-bindings-rs) #:use-module (crates-io))

(define-public crate-mysql-client-bindings-rs-0.1.0 (c (n "mysql-client-bindings-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1arscmnz64rhgkjd9kn3y1dh0rnyiabk2i5mc192p3lw4n52dxg5") (f (quote (("mysql") ("mariadb") ("default" "mysql")))) (y #t) (l "mysql")))

(define-public crate-mysql-client-bindings-rs-0.1.1 (c (n "mysql-client-bindings-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1kxhq8hwwv98qcnnjyxbb0kphkc8717zgjwdxdr06qyha4rv8kss") (f (quote (("mysql") ("mariadb") ("default" "mysql")))) (y #t) (l "mysql")))

(define-public crate-mysql-client-bindings-rs-0.1.2 (c (n "mysql-client-bindings-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "18hkpmkas01c7qdlharmpxz7bil0fhhcar4vadsph6c11av6fa3h") (f (quote (("mysql") ("mariadb") ("default" "mysql")))) (y #t) (l "mysql")))

(define-public crate-mysql-client-bindings-rs-0.2.0 (c (n "mysql-client-bindings-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.1") (o #t) (d #t) (k 1)))) (h "1dac3x68s8rqx3266xpwcydmi2i7dfmhzg1am1g914snkvh4dq74") (f (quote (("mysql") ("mariadb") ("fresh_bindings" "bindgen") ("default" "mysql")))) (y #t) (l "mysql")))

