(define-module (crates-io my sq mysql_enum_derive) #:use-module (crates-io))

(define-public crate-mysql_enum_derive-0.1.0 (c (n "mysql_enum_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0v05l72s21vz9a7fyl29vhhdzpfw1nyrylhlq4ynjrqavql5ib30")))

(define-public crate-mysql_enum_derive-0.1.1 (c (n "mysql_enum_derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1nsz70qlc77x1w2xsa017m3hkxsrh3nyj221pkf6n4sdlr2gg420")))

(define-public crate-mysql_enum_derive-0.1.2 (c (n "mysql_enum_derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1rqv9zfvdh35ldcrhd7z1bislrk7cyjbvk3jps97vgvf2iixnm2n")))

