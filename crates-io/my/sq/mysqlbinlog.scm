(define-module (crates-io my sq mysqlbinlog) #:use-module (crates-io))

(define-public crate-mysqlbinlog-0.1.0 (c (n "mysqlbinlog") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 2)))) (h "0ijxic8fhzdws7mcz49k8ibnj2nryglq2ak417yxyac8c8pxn3z7")))

(define-public crate-mysqlbinlog-0.1.1 (c (n "mysqlbinlog") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 2)))) (h "1z9s97s70wzbyaycsacsmg4awa3a35pwvshsz7j397rm0ws0m90i")))

(define-public crate-mysqlbinlog-0.1.3 (c (n "mysqlbinlog") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 2)))) (h "06n9vc68g08blv7x8v46g6s5yi62wgylc0ly5ha7inxb5rb4i8hc")))

(define-public crate-mysqlbinlog-0.1.4 (c (n "mysqlbinlog") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 2)))) (h "07izmqwishb6jhvyq7n1rndj629976zlyaa6fm3q8y4jhwgvbpin")))

(define-public crate-mysqlbinlog-0.1.5 (c (n "mysqlbinlog") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 2)))) (h "15wsbza1465s9yjmjkiq82gk0mhx5qgxyf7qvmf60ai05d3ndb80")))

