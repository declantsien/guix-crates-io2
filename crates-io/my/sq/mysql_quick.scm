(define-module (crates-io my sq mysql_quick) #:use-module (crates-io))

(define-public crate-mysql_quick-0.1.0 (c (n "mysql_quick") (v "0.1.0") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0r8hs4z1sqmlv0vxai72vqyhl9v2md6h83bxgq4c6n7xpv98p8sd")))

(define-public crate-mysql_quick-0.2.0 (c (n "mysql_quick") (v "0.2.0") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "10md35303g7wdvvlvry549m8hphjh6dln7bgf0pl58b21bdc7zdq")))

(define-public crate-mysql_quick-0.3.2 (c (n "mysql_quick") (v "0.3.2") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "19rhh97lcjr4i18qw2qx4l7ihamsgd0mgwj6mj0g3pvkvd8z0vrc")))

(define-public crate-mysql_quick-0.3.3 (c (n "mysql_quick") (v "0.3.3") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1bk1lp2bx35yz8ngghsxcfk31ig5yfsnjl5x9rslnka5dknbvizy")))

(define-public crate-mysql_quick-0.3.8 (c (n "mysql_quick") (v "0.3.8") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1718rs9d7a6k4x20wy1fdvz3vqw1qkpb61msc424qx7cgas52b00")))

(define-public crate-mysql_quick-0.4.0 (c (n "mysql_quick") (v "0.4.0") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0m27y9ivn1msij82rhhx6xs8cy937g4hxkdc5yw0s0i4wd9jyk73")))

(define-public crate-mysql_quick-0.4.1 (c (n "mysql_quick") (v "0.4.1") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0acrayp9hzkl57rivrkk2pi9rd1kxcmr6jax55204vl9bzxphlgl")))

(define-public crate-mysql_quick-0.4.2 (c (n "mysql_quick") (v "0.4.2") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1fwa5r4pgribhdglhax2mm8h380bb1da81k6506jvnpvg79s5qqf")))

(define-public crate-mysql_quick-0.5.0 (c (n "mysql_quick") (v "0.5.0") (d (list (d (n "mysql") (r "^23.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.29") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1f8r0mhq779ggj5vi0ysmls831izawxggb412bsc8s8arwg4dbld")))

(define-public crate-mysql_quick-1.0.0 (c (n "mysql_quick") (v "1.0.0") (d (list (d (n "mysql") (r "^24.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0992ng0disfw5agnz6rw2x8jxasj81806h59s5ivb0rbxh9346dq")))

(define-public crate-mysql_quick-1.0.1 (c (n "mysql_quick") (v "1.0.1") (d (list (d (n "mysql") (r "^24.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "0li9pm990cy6p7i3njz8whyj9sqcz6k7dyiqbg4wj62czx1bbwfb")))

(define-public crate-mysql_quick-1.2.1 (c (n "mysql_quick") (v "1.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mysql") (r "^24.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1jyvdxbb4lk619fnz294bnhfqlj1nkf68fdyisrzik0q35gs3g1g")))

(define-public crate-mysql_quick-1.2.2 (c (n "mysql_quick") (v "1.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mysql") (r "^25") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "032scw2slxl82n4pxciq1d2zyz8gwkm9apfkqxn8q1y04x38fnys")))

(define-public crate-mysql_quick-1.2.3 (c (n "mysql_quick") (v "1.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "mysql") (r "^25") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ij77h7nwifmhkbvkdg88p93y2wc0a9mz6sm83jlzkd12dp8m3kl")))

