(define-module (crates-io my _m my_minigrep321) #:use-module (crates-io))

(define-public crate-my_minigrep321-0.1.0 (c (n "my_minigrep321") (v "0.1.0") (h "1z9qx615zj3q5mj5wijhgzmbp3l3cv9546gfjd8yiz66qcwanpg2")))

(define-public crate-my_minigrep321-0.1.1 (c (n "my_minigrep321") (v "0.1.1") (h "1147fr2lrss3i8pw29di2lvc07dl5glkncch0yiz9mqp0mb0llhp") (y #t)))

(define-public crate-my_minigrep321-0.1.2 (c (n "my_minigrep321") (v "0.1.2") (h "1490q6kbv9rgmlw4aaf0xx6hi2qs3d1cij1yqpv5s996cbhk1h1q")))

