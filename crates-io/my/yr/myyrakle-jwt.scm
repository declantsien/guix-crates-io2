(define-module (crates-io my yr myyrakle-jwt) #:use-module (crates-io))

(define-public crate-myyrakle-jwt-0.1.0 (c (n "myyrakle-jwt") (v "0.1.0") (d (list (d (n "epoch-timestamp") (r "^1.0.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "011hxm296xlppv3iw76i8w7h21x8hd4x91sxg91pmnkp84bj8bs6")))

