(define-module (crates-io my ho myhome) #:use-module (crates-io))

(define-public crate-myhome-0.1.0 (c (n "myhome") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vf5wvsdrd8rm7agakz3phmpyk1bjawwraa36jfb1qiiysaln3ka")))

(define-public crate-myhome-0.1.1 (c (n "myhome") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (f (quote ("unstable-locales"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jy44bqkjrb9lh0s0m1zyvh921r7b6grz26id1lnl4lnl1w9fz63")))

