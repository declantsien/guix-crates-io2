(define-module (crates-io my -c my-cargo-project2) #:use-module (crates-io))

(define-public crate-my-cargo-project2-0.1.0 (c (n "my-cargo-project2") (v "0.1.0") (h "1qan0c1snc0zpczcs774kldg514rxq5qgcf9hdh81fmqjn01y64x")))

(define-public crate-my-cargo-project2-0.2.0 (c (n "my-cargo-project2") (v "0.2.0") (h "08bcmpkz73d7pgwy00ph4hbb8q5pfws3qmikjsxjpygy2mra6wb6")))

