(define-module (crates-io my -e my-essentials) #:use-module (crates-io))

(define-public crate-my-essentials-0.1.0 (c (n "my-essentials") (v "0.1.0") (h "1f2drah9ykkhpbyb4xmwr5j9wn5yiqaj5dzrb0hrwnc33i56v1pc")))

(define-public crate-my-essentials-0.1.1 (c (n "my-essentials") (v "0.1.1") (h "1vwqjdvgdr58jdaxj4dha0bkly8k5iik0qjmxi2fdcdji5mi4dnm")))

