(define-module (crates-io my _i my_internet_ip) #:use-module (crates-io))

(define-public crate-my_internet_ip-0.0.1 (c (n "my_internet_ip") (v "0.0.1") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1jzz02b7h59fcbk7rxax3rgks74hcqga51k8ab937kaxhqp8axii")))

(define-public crate-my_internet_ip-0.0.2 (c (n "my_internet_ip") (v "0.0.2") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "19lczy3qmp4wxv2yq6vidji6161w0ggiqg0h0796c3sg27yga0z6")))

(define-public crate-my_internet_ip-0.0.3 (c (n "my_internet_ip") (v "0.0.3") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "0gb3bwslvgahcsq84r6hd7ygqpa4q17r1pa0a3xarimh5kb4dl52")))

(define-public crate-my_internet_ip-0.1.0 (c (n "my_internet_ip") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)))) (h "14p4rs0hyc4sj16f87ygch7lmb3hbkf82cd3aw97l6z222ahjkav")))

(define-public crate-my_internet_ip-0.1.1 (c (n "my_internet_ip") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)))) (h "05gn9dgyqv540dln71rhgjqvl9ki5jlsnaabqkffwffkj8vxic6d")))

