(define-module (crates-io my be mybench) #:use-module (crates-io))

(define-public crate-mybench-0.1.0 (c (n "mybench") (v "0.1.0") (h "1r05kc9bfvjij4gpwbfay9jpv8ahizlg9lih6xfyc4q2lnxm9451") (y #t)))

(define-public crate-mybench-0.1.1 (c (n "mybench") (v "0.1.1") (h "0gcviykvsmxvn751x3dhxpg0kdc6qxaaz4mp6czc4yp9gl0pdghp")))

(define-public crate-mybench-0.1.2 (c (n "mybench") (v "0.1.2") (h "1630syl31296s76zrz24fb4chfm7vbflh93hfblxwyff4hy3a7lk")))

(define-public crate-mybench-0.1.3 (c (n "mybench") (v "0.1.3") (h "0jgkww5c81gxv5qwr5k52n95nj25g0ylny88m2igk2s8fbpvsr2y")))

(define-public crate-mybench-0.1.4 (c (n "mybench") (v "0.1.4") (h "0xkw1r3md19msq58348s5d7i458bca1mpwnjnj7qlwrrgbjli1ry")))

(define-public crate-mybench-0.1.5 (c (n "mybench") (v "0.1.5") (h "10kk3pv4459ihpypzxr0gm39naci6ap9295pm4pjiwajl4hqnd8q")))

(define-public crate-mybench-0.1.6 (c (n "mybench") (v "0.1.6") (h "1hhnavl9z3b60c9723l94f24458mhljcp4sp1jv3cw54ff6bnqz1")))

(define-public crate-mybench-0.1.7 (c (n "mybench") (v "0.1.7") (h "1q89df0mi5xz230riprxm789nx1fq8klfjzxiplv9h52q3f2d1zp")))

(define-public crate-mybench-0.1.8 (c (n "mybench") (v "0.1.8") (h "0ffrs4plx9dp2hf34ardhcc0ddpx81na28ddyar5yb2c630d82v7")))

