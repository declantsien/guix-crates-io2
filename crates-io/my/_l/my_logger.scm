(define-module (crates-io my _l my_logger) #:use-module (crates-io))

(define-public crate-my_logger-0.1.0 (c (n "my_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)))) (h "14mpfz9fw603qq2xbb4h3kgcxph722bg7v3q2ncn4y1yqs2xqw71") (y #t)))

(define-public crate-my_logger-0.1.1 (c (n "my_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)))) (h "1dpi7zg7q65z0kyyxlcn9i2fb29yff1crv12h9kmlm8hcczd2mq2") (y #t)))

(define-public crate-my_logger-0.1.2 (c (n "my_logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "1a8bi5p5z4y6rqsgqja7rbc0phb87f8vbcvckl20a43gyw9mk1my") (y #t)))

(define-public crate-my_logger-0.1.3 (c (n "my_logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "1jipzwhj1ra02428ab5rzx8ivsbkv4kva8bd0km9xjqsnb03pifi") (y #t)))

(define-public crate-my_logger-0.1.4 (c (n "my_logger") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0f8l0r8f4q2nvxdzxy4a5yc682iqghzjb65wyby8fxxf0n1cn9xi")))

(define-public crate-my_logger-0.1.5 (c (n "my_logger") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "0d556x99d510p4xamkala8rwvj7k2qqjf7b91553x2x6r0vnfmaw")))

(define-public crate-my_logger-0.2.0 (c (n "my_logger") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "12rfmfampn7ricry6b7gc2bkbsz766ivq7f20zc5644p1w775j8r")))

(define-public crate-my_logger-0.2.1 (c (n "my_logger") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.7.0") (d #t) (k 2)))) (h "1ivdyhxzrnji7hwnj2alz79xk8i34ik593rh1m5pfhs3w80z2mg6")))

