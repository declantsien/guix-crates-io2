(define-module (crates-io my -b my-bfgs) #:use-module (crates-io))

(define-public crate-my-bfgs-0.1.0 (c (n "my-bfgs") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "1wsgwv9p2r8jwd789mcg4pbrydvajp7g1mgma6q79dx6rs5aqw8i")))

(define-public crate-my-bfgs-0.1.1 (c (n "my-bfgs") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "1kzksl60xvcl2bms4plfqwj0rkhz94i12lngddv4d66xxm40wld5")))

(define-public crate-my-bfgs-0.1.2 (c (n "my-bfgs") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "spectral") (r "^0.6") (d #t) (k 2)))) (h "012lgiwvacgdcb8cbcqgk900icl4qd0wpxh25nrjimm1bqflmb84")))

