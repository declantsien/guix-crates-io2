(define-module (crates-io my cr mycroft) #:use-module (crates-io))

(define-public crate-mycroft-0.0.1 (c (n "mycroft") (v "0.0.1") (d (list (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0ilm88hvvdyjslp4x02rgcf1dlj2vivjzclv94xv0r49bfaidpqn")))

