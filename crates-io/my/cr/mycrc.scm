(define-module (crates-io my cr mycrc) #:use-module (crates-io))

(define-public crate-mycrc-0.1.0 (c (n "mycrc") (v "0.1.0") (h "1b50qvnpd1bys89zvv113hqps8ccy6vmwg2dnhi4sp625q87wynk")))

(define-public crate-mycrc-0.2.0 (c (n "mycrc") (v "0.2.0") (h "0gsrqdks43yqfbn4izb26dq3h06s9ykl4p5phrhgdf2llkadym23")))

(define-public crate-mycrc-0.3.0 (c (n "mycrc") (v "0.3.0") (h "09yh67xyrv8mvjip0s00yjdrf8pv8qcrxzw1ynv5a5k1s7q9vmp6")))

(define-public crate-mycrc-0.3.1 (c (n "mycrc") (v "0.3.1") (h "0aibia47dgbzk0vgqik319fq53p6i13fahmdibblm9zs1d6axiy3")))

