(define-module (crates-io my cr mycroft-macros) #:use-module (crates-io))

(define-public crate-mycroft-macros-0.0.1 (c (n "mycroft-macros") (v "0.0.1") (d (list (d (n "mycroft-macros-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1jcivh8l21a0n5sc6kn56vrckgab2hdl4g7a5bvy23zjmzl92y7j")))

