(define-module (crates-io my cr mycraft) #:use-module (crates-io))

(define-public crate-mycraft-0.1.0 (c (n "mycraft") (v "0.1.0") (d (list (d (n "async-codec") (r "^0.4.0-alpha.4") (d #t) (k 0)) (d (n "async-std") (r "^1.6.3") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "1zz5v2aiv3cc62q1w8bfjl87fma2j9if57186y5l3l55iniq657v")))

