(define-module (crates-io my cr mycroft-macros-impl) #:use-module (crates-io))

(define-public crate-mycroft-macros-impl-0.0.1 (c (n "mycroft-macros-impl") (v "0.0.1") (d (list (d (n "combine") (r "^2.5") (d #t) (k 0)) (d (n "mycroft") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full" "printing" "parsing"))) (d #t) (k 0)))) (h "0ckcbzdy7jazasjp8ah618s20l5x5p7s68kiyisr558qrhqqnm58")))

