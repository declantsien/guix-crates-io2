(define-module (crates-io my -w my-wgsl-macros) #:use-module (crates-io))

(define-public crate-my-wgsl-macros-0.0.1 (c (n "my-wgsl-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1dyq38k4z9wa7vm0s6lprhy9pfwwwjpxmik6w0qlksp9sxp8dwxz")))

