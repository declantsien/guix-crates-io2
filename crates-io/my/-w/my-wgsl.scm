(define-module (crates-io my -w my-wgsl) #:use-module (crates-io))

(define-public crate-my-wgsl-0.0.1 (c (n "my-wgsl") (v "0.0.1") (d (list (d (n "my-wgsl-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.11.2") (d #t) (k 0)))) (h "0518hnhxa8qvmnq1pnwdn97a767lfzmr92qk3xc2vwmy42nsxbby")))

