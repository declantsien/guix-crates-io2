(define-module (crates-io my -r my-rust-test) #:use-module (crates-io))

(define-public crate-my-rust-test-0.1.0 (c (n "my-rust-test") (v "0.1.0") (h "1vsfh6qdvssb6n0wqzkmndbn7576vf3m7lr7ga587bj2ahmx0za5")))

(define-public crate-my-rust-test-0.1.1 (c (n "my-rust-test") (v "0.1.1") (h "1p6d0ccdhqacgxfmgm52i8l08h4srbp4xqms34wn5f7njk34hlpa")))

