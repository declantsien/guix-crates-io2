(define-module (crates-io my -r my-rust-lib2) #:use-module (crates-io))

(define-public crate-my-rust-lib2-0.1.0 (c (n "my-rust-lib2") (v "0.1.0") (h "06jlvr9mawnp7i4smv7sszihjm9i4bhmfvrz9ds6ssms6952a7cr")))

(define-public crate-my-rust-lib2-0.1.1 (c (n "my-rust-lib2") (v "0.1.1") (h "0y12xgy17jyg31ij1dzk3qks0857vh6y3fn1wygpfh23xfzajzw8")))

(define-public crate-my-rust-lib2-0.1.2 (c (n "my-rust-lib2") (v "0.1.2") (h "105cnxwb6ivkkqkfm57v2f0wgc5cd04cgfcy2shzjh4vmy951pys")))

(define-public crate-my-rust-lib2-0.1.3 (c (n "my-rust-lib2") (v "0.1.3") (h "0vmilkdsc9mxd9104c0qh8fv6hxigk4fka440xsm1f1jhdkgmcgq")))

(define-public crate-my-rust-lib2-0.1.4 (c (n "my-rust-lib2") (v "0.1.4") (h "0hw3sxbp83n6an79wf5r9fga69fl9kb40m8z55fhn8847jp8a7cx")))

(define-public crate-my-rust-lib2-1.0.0 (c (n "my-rust-lib2") (v "1.0.0") (h "1fhd2gq5hp8aqm7wkan4bcwmx0wrdp5p2330c8ln85nc4raszcyh")))

(define-public crate-my-rust-lib2-1.0.1 (c (n "my-rust-lib2") (v "1.0.1") (h "0807wnfihcmmsqjr9i0zxc6mqzw4z9s6yylzz78a0xn73b4idbr1")))

(define-public crate-my-rust-lib2-1.0.3 (c (n "my-rust-lib2") (v "1.0.3") (h "1w1g8pjqmr4msszd2jnr29mmb6inm5m85kgbrkjhb82587qambn8")))

