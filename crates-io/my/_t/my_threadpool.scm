(define-module (crates-io my _t my_threadpool) #:use-module (crates-io))

(define-public crate-my_threadpool-0.1.0 (c (n "my_threadpool") (v "0.1.0") (h "0wwgmp5icfcldzxn6wkspqjba5c9wrs0g05pn6fvhlkfd0d27y31")))

(define-public crate-my_threadpool-1.0.0 (c (n "my_threadpool") (v "1.0.0") (h "0s6zkjnjc89p2njmbgpx895ns8dihfjz1qi5sc5hb31lyjhk1cj4")))

(define-public crate-my_threadpool-1.0.1 (c (n "my_threadpool") (v "1.0.1") (h "1qp988f9a7zrsxkxxmrfb456k6z4p228jjcnpf0n22g0j8ja9qh4")))

