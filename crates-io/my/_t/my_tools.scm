(define-module (crates-io my _t my_tools) #:use-module (crates-io))

(define-public crate-my_tools-0.1.0 (c (n "my_tools") (v "0.1.0") (h "1j1304q213kzz6v3v7ajf61ym1xnla1cvw4gvajc51klv2fbsdzy")))

(define-public crate-my_tools-0.1.1 (c (n "my_tools") (v "0.1.1") (h "16gscnb7vil2wj3ald6p7b0nnaissi9nwhpfk2nd82dyw07wfa9s")))

