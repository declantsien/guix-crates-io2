(define-module (crates-io my _t my_test_crate) #:use-module (crates-io))

(define-public crate-my_test_crate-0.1.0 (c (n "my_test_crate") (v "0.1.0") (h "1dspaaffnhdlcidlgd6dmp09vkg798q8mqgvkbjgwwi9x4cx4i20")))

(define-public crate-my_test_crate-0.1.1 (c (n "my_test_crate") (v "0.1.1") (h "1jgwr1vfyksq5n8nknc85zp85k3shyjghqwbmc105lmx0b37p0mk")))

