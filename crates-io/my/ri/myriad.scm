(define-module (crates-io my ri myriad) #:use-module (crates-io))

(define-public crate-myriad-0.1.0 (c (n "myriad") (v "0.1.0") (h "074cpim6l22j4hmkkjqnbfsi64hp8y4x66zf8lw04v79bvv091p5")))

(define-public crate-myriad-0.1.1 (c (n "myriad") (v "0.1.1") (h "0ga55xjfspdpmpcwp9hccddvh65vlic5djil71p0aicv8cg0pq7b")))

