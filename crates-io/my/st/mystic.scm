(define-module (crates-io my st mystic) #:use-module (crates-io))

(define-public crate-mystic-0.1.10 (c (n "mystic") (v "0.1.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "forky_ai") (r "0.*") (d #t) (k 0)) (d (n "forky_core") (r "0.*") (d #t) (k 0)) (d (n "forky_fs") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)) (d (n "sweet") (r "0.*") (d #t) (t "cfg(windows)") (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0sf2qaikb8v2llzy8xy4x8hv7cjhn04y1r0qqdzc34fxfs1lg21z")))

