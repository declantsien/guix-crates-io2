(define-module (crates-io my st mystem) #:use-module (crates-io))

(define-public crate-mystem-0.1.0 (c (n "mystem") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "142qybql1f042mmcvzgzwr6bwwl5z71d9c5vl1463nnha36lnnxz")))

(define-public crate-mystem-0.2.0 (c (n "mystem") (v "0.2.0") (d (list (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "07xvh8fakwhx5964zcyxqf5n5bcj5cg9m0qz20c6bps4xn1hfp05")))

(define-public crate-mystem-0.2.1 (c (n "mystem") (v "0.2.1") (d (list (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "0pw1qdbjh454azsqqqwm496hv70ll58r4jfdfz78y9k0p0b9pina")))

(define-public crate-mystem-0.2.2 (c (n "mystem") (v "0.2.2") (d (list (d (n "log") (r "^0.4.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.6") (d #t) (k 0)))) (h "1sz0rqc5rh1wvnjinv0c16bc5s5l1n9yxz09c1ajn4qcdn4fnrr5")))

