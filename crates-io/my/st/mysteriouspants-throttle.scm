(define-module (crates-io my st mysteriouspants-throttle) #:use-module (crates-io))

(define-public crate-mysteriouspants-throttle-0.1.0 (c (n "mysteriouspants-throttle") (v "0.1.0") (h "06zmbd98pvm0h384dxkla6zrvgq0ip637lcqhr74gsj0w2bflki9")))

(define-public crate-mysteriouspants-throttle-0.2.0 (c (n "mysteriouspants-throttle") (v "0.2.0") (h "1lds3g5b8q32jmmis6mahrkzp7kixjm45gadkclgf09zjjybv64b")))

(define-public crate-mysteriouspants-throttle-0.2.1 (c (n "mysteriouspants-throttle") (v "0.2.1") (h "1iang2l6qgylqm9h2gjvmmlpw9krv3jycr7ry1arbcnmajfygx14")))

(define-public crate-mysteriouspants-throttle-0.2.2 (c (n "mysteriouspants-throttle") (v "0.2.2") (h "0r5jmzpgj8xia31llb95cdycry92dy7j0va7z7cabpddf0pdwmwh")))

(define-public crate-mysteriouspants-throttle-0.2.3 (c (n "mysteriouspants-throttle") (v "0.2.3") (h "1ldn0sjyzv45k9b8bdb5lxwqvjpz92hdidr8lqwaxs12n35pmjvn")))

(define-public crate-mysteriouspants-throttle-0.2.4 (c (n "mysteriouspants-throttle") (v "0.2.4") (h "0nzrsz3l05ym2z7cw1mr7z9zx2mi00kmajy9g31b1dn7aqabfr8p")))

(define-public crate-mysteriouspants-throttle-0.2.5 (c (n "mysteriouspants-throttle") (v "0.2.5") (h "19smaxpnf50xc4zm0c85wnr3xxnabhazhxlsp6rmdnhh6w4nm3c2")))

