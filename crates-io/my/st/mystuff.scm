(define-module (crates-io my st mystuff) #:use-module (crates-io))

(define-public crate-mystuff-0.1.0 (c (n "mystuff") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)))) (h "01r6hrnan9v0a1mfn6kcnsrzg5fg0yw9zq46xkcg8vpyp70n3afx")))

(define-public crate-mystuff-0.1.1 (c (n "mystuff") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)))) (h "1plc51xbfjyxfjz9gndy53jgjhgh3nhqwpr6xlkkv6nvwkpnpyak")))

(define-public crate-mystuff-0.1.2 (c (n "mystuff") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)))) (h "0bd8x69mblr1cd6k31ihk1gq1nkglr0ispzach5drss8n7xav6wc")))

