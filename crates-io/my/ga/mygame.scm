(define-module (crates-io my ga mygame) #:use-module (crates-io))

(define-public crate-mygame-0.1.0 (c (n "mygame") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)))) (h "0y6hdmkyn5qs9l3bmi7w4slywb90vqz1x9bkcw7mbz03d3i7i727")))

(define-public crate-mygame-0.1.1 (c (n "mygame") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)))) (h "17hrcfr5j6vkjy2rpp6k9143lhpv18ffijinvfknmymq70m1x3nb")))

(define-public crate-mygame-0.1.2 (c (n "mygame") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)))) (h "1541ag9naygrj9k80a8wf4jd1zfs3lrrs0vpnj0qqm5llxnf88cc")))

(define-public crate-mygame-0.1.3 (c (n "mygame") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)))) (h "0ikp9f548y4bznphxl4pxg5riwwn92y174ny9h8jiila4r2zyikw")))

