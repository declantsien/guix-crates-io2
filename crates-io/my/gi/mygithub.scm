(define-module (crates-io my gi mygithub) #:use-module (crates-io))

(define-public crate-mygithub-0.1.0 (c (n "mygithub") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1d5fhp02ybavz5qmski8d1rnmb922jbbgc5kw5672ywj4da20w45")))

