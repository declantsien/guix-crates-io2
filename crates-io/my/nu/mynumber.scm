(define-module (crates-io my nu mynumber) #:use-module (crates-io))

(define-public crate-mynumber-0.1.0 (c (n "mynumber") (v "0.1.0") (h "055pvdggbqygyid7y5kw7x9na8svljl5hadl3znzrr955qdsan67")))

(define-public crate-mynumber-0.2.0 (c (n "mynumber") (v "0.2.0") (h "04yi55p3zr9mmcazm1zv8547h9mrswm7cmlpx2jdj97zcpmkaans")))

