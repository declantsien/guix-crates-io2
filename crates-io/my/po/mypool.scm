(define-module (crates-io my po mypool) #:use-module (crates-io))

(define-public crate-mypool-0.1.0 (c (n "mypool") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.8") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)))) (h "031627wyy8mrpk445vjpdk6kc3illhcp81547w295hrqp8bdivx7")))

