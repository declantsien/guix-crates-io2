(define-module (crates-io my -t my-tea-dinf) #:use-module (crates-io))

(define-public crate-my-tea-dinf-0.1.2 (c (n "my-tea-dinf") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01sk13cjl71cfzs7hxwg6n67w5jhk9dixpazm2g3p3rf8ydx7kj0")))

