(define-module (crates-io my -t my-tool) #:use-module (crates-io))

(define-public crate-my-tool-0.1.0 (c (n "my-tool") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "html5ever") (r "^0.22") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "soup") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1vxp41p6gsvsb891glfgcsds14ygcsj5ich1bbavdfql2ajm8a0b") (y #t)))

