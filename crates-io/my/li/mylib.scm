(define-module (crates-io my li mylib) #:use-module (crates-io))

(define-public crate-mylib-0.1.0 (c (n "mylib") (v "0.1.0") (h "0mj41d7cdmfjqc682a182pgi15c2ixxa0l7z44y9a8zpprr9pyjs")))

(define-public crate-mylib-0.1.1 (c (n "mylib") (v "0.1.1") (h "0w5485yzm887hvjjxhznpvwhfh0v90jzw5a536vgdsvd78v8gg1i")))

