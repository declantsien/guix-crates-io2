(define-module (crates-io my li mylibrary) #:use-module (crates-io))

(define-public crate-mylibrary-1.0.4+dev1 (c (n "mylibrary") (v "1.0.4+dev1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "python3-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0pdny220s1mql27r2m07x90zzxx6p2pp46yb0581x93ixmyc0py9")))

(define-public crate-mylibrary-1.0.4+dev2 (c (n "mylibrary") (v "1.0.4+dev2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "python3-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0pfkbcw930cln9659r86pcx36avhnicjxy2wjw4gam4hhvi4yc4v")))

