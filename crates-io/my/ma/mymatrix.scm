(define-module (crates-io my ma mymatrix) #:use-module (crates-io))

(define-public crate-mymatrix-0.1.0 (c (n "mymatrix") (v "0.1.0") (d (list (d (n "pyinrs") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1qnkpbzlzcjqdg4gff5871l7zn1hhsb921rf39y9qysbisib6x4z")))

(define-public crate-mymatrix-0.2.0 (c (n "mymatrix") (v "0.2.0") (d (list (d (n "pyinrs") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "08prqqb2gd0h282b7qxgix9gnm5f7x2ki8msvqqfawlybvzphpl3")))

(define-public crate-mymatrix-0.3.0 (c (n "mymatrix") (v "0.3.0") (d (list (d (n "pyinrs") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "045i21sx76p0nr5pvd14s1g1l6yykwj0ix07h43sny0c9sgvax5i")))

