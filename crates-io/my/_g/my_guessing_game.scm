(define-module (crates-io my _g my_guessing_game) #:use-module (crates-io))

(define-public crate-my_guessing_game-0.1.0 (c (n "my_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1pynnlqbd0jn96nl17cy7zy4y3qapvr157rmhnj96ifk40z7b0i4")))

(define-public crate-my_guessing_game-0.1.1 (c (n "my_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0156bq9rpd2ggpccnl107q0n9g14abg6xsxnr7mamcp4hgrdwjcr")))

(define-public crate-my_guessing_game-0.1.2 (c (n "my_guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lhvhr5gh52sbyxrdpz06gdpjahypf82f452b7hzfjq7rbhgz91r")))

