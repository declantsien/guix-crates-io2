(define-module (crates-io my ex myexe) #:use-module (crates-io))

(define-public crate-myexe-0.1.0 (c (n "myexe") (v "0.1.0") (d (list (d (n "my3") (r "^0.1.0") (d #t) (k 0)))) (h "11wz0gnfy4znm0i7v940fz3za1jc1dy55az9c2dnd37ncirbwj31")))

(define-public crate-myexe-0.1.1 (c (n "myexe") (v "0.1.1") (d (list (d (n "my3") (r "^0.1.0") (d #t) (k 0)))) (h "0yd6fd8vm51mzjwk6g97984mf4c5vgfvnz0l40im586bkrlsj47v")))

(define-public crate-myexe-0.1.5 (c (n "myexe") (v "0.1.5") (d (list (d (n "my3") (r "^0.1.2") (d #t) (k 0)))) (h "11vs1irqzm0rp0c5cjckc17655v7bjf01i50sx85hkjin8agzd52")))

(define-public crate-myexe-0.1.3 (c (n "myexe") (v "0.1.3") (d (list (d (n "my3") (r "^0.1.3") (d #t) (k 0)))) (h "1bb3ww4mjws8x6m7s9vgqn9nxn67f27dgnlc77dyvwz0vkbb3hh9")))

(define-public crate-myexe-0.1.4 (c (n "myexe") (v "0.1.4") (d (list (d (n "my3") (r "^0.1.4") (d #t) (k 0)))) (h "1cksj4gk72gpjmghsznn3v9wyxnqg64nlpkfmlzm26bnv2mwqh39")))

(define-public crate-myexe-0.1.6 (c (n "myexe") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "1f11glr785ihf9l451z2axxykrnfi17fv36ssawx8ka9x06gzsyf")))

(define-public crate-myexe-0.1.7 (c (n "myexe") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "057zvnnf7jj5jbrb8gg8n1ws2w257i8mq7jg5pnv1a6cwcb4c6nd")))

(define-public crate-myexe-0.1.8 (c (n "myexe") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "144mqchqa6xkypllgqymir6hzqi4c5qrp8h974hsb7q7gdlvpqym")))

(define-public crate-myexe-0.1.9 (c (n "myexe") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "08rklsl15zp27jl10qhybn2ipk5s195jlca09ac5v5kx4w9s9m4i")))

(define-public crate-myexe-0.1.10 (c (n "myexe") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "0a7868ddzdwvzvpm896gw2giy4n0zs4zqwp0w12bam9i79c1ygi2")))

(define-public crate-myexe-0.1.11 (c (n "myexe") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "1s62yfviwkf707hcj5bxlyhnnkjfc4y6lisykmgb4cfr977bcn9f")))

(define-public crate-myexe-0.1.12 (c (n "myexe") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "my3") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (k 0)))) (h "036lw490yqdr9rnnmyx2zspz6glylqrn51zp70sdamnh36n6bcz5")))

