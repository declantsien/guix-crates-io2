(define-module (crates-io my _p my_parser_kma_group_test_1) #:use-module (crates-io))

(define-public crate-my_parser_kma_group_test_1-0.1.0 (c (n "my_parser_kma_group_test_1") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1qisxpxdmswdca3m1dkfgam8wmwgwnqz8gss631pflyms6i9hmba")))

(define-public crate-my_parser_kma_group_test_1-0.1.1 (c (n "my_parser_kma_group_test_1") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "04az89507c3zbr2hzv44qnrgrsw6mxb1li7w4v5kg2ad5c3ayvx1")))

(define-public crate-my_parser_kma_group_test_1-0.1.2 (c (n "my_parser_kma_group_test_1") (v "0.1.2") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1sfhs6h87bksvrbgk3p38ckq4wnwbibdhmk0rkg5dfpcdrqvli0i")))

(define-public crate-my_parser_kma_group_test_1-0.1.3 (c (n "my_parser_kma_group_test_1") (v "0.1.3") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0w4qk511dz4m7xbl5d34bzkh1422ndshsxx0c1346ml2c3xswbf3")))

(define-public crate-my_parser_kma_group_test_1-0.1.4 (c (n "my_parser_kma_group_test_1") (v "0.1.4") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0dy99ia40f8j9vn7n44hvpw615l3zja3qgi9kxvyfbnfq8nm7l8g")))

(define-public crate-my_parser_kma_group_test_1-0.1.5 (c (n "my_parser_kma_group_test_1") (v "0.1.5") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "02vywc8d1z6nkdc0q507h2v0l035d0j8ijljmfz44raf0hi5i5cv")))

(define-public crate-my_parser_kma_group_test_1-0.1.6 (c (n "my_parser_kma_group_test_1") (v "0.1.6") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1khs7sr43i12cq2ng0zil05krpb52pcy9xwxiy4jy676hflbdf43")))

(define-public crate-my_parser_kma_group_test_1-0.1.7 (c (n "my_parser_kma_group_test_1") (v "0.1.7") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "01cg958h5qwc76x5i826sc0pslnx1rp90kcn1fhikc65y356sh98")))

(define-public crate-my_parser_kma_group_test_1-0.1.8 (c (n "my_parser_kma_group_test_1") (v "0.1.8") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0fwgln5miqk8937sim74fb57j5aa3s2pyzh65g09ny70w5g3a1yp")))

(define-public crate-my_parser_kma_group_test_1-0.1.9 (c (n "my_parser_kma_group_test_1") (v "0.1.9") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0jaziqfjx4q6bq6n9d1j8cvc8syf0w0a9hwlp2jr5m27n2v47s84")))

(define-public crate-my_parser_kma_group_test_1-0.1.10 (c (n "my_parser_kma_group_test_1") (v "0.1.10") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0vi3nwy93j3b6bmh87iyz0hihb30xb88fmpfyixqs4grza4yxj1x")))

(define-public crate-my_parser_kma_group_test_1-0.1.11 (c (n "my_parser_kma_group_test_1") (v "0.1.11") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0019sdfj8lg5gwji5q20b47kzsm502gi0za63r9y8ycxxd412bzn")))

(define-public crate-my_parser_kma_group_test_1-0.1.12 (c (n "my_parser_kma_group_test_1") (v "0.1.12") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1ckvvwnx963pmf21f5njigv5c609w66scphnaswwbpllcq3a27zq")))

