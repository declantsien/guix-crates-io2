(define-module (crates-io my _p my_parser_kma_group_test_2) #:use-module (crates-io))

(define-public crate-my_parser_kma_group_test_2-0.1.13 (c (n "my_parser_kma_group_test_2") (v "0.1.13") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "074n39g25qzq4cni77f5qxl595jxzi4f2vj56gnbwdywcih6zr0r")))

(define-public crate-my_parser_kma_group_test_2-0.1.14 (c (n "my_parser_kma_group_test_2") (v "0.1.14") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1a3z2pb7h2zn7fz6barkkd6pnclbqpbq57xx2f2nyz36v4s26cwa")))

(define-public crate-my_parser_kma_group_test_2-0.1.15 (c (n "my_parser_kma_group_test_2") (v "0.1.15") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "06ii5vd9085hisl9x8kag4rk8h14bzmdw6b8fhbq4mxdrrldn9pc")))

(define-public crate-my_parser_kma_group_test_2-0.1.16 (c (n "my_parser_kma_group_test_2") (v "0.1.16") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0smh15yxjksz8x3mmynnkzm4cbgf47yl16zwgj1zr0xikdq3skaq")))

(define-public crate-my_parser_kma_group_test_2-0.1.17 (c (n "my_parser_kma_group_test_2") (v "0.1.17") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1s6pp2qf0n1s3nv0qlkk8khwf0rs1bp7m7pc8zxar18g9plgd0hi")))

(define-public crate-my_parser_kma_group_test_2-0.1.18 (c (n "my_parser_kma_group_test_2") (v "0.1.18") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1imiiym1jzvzxl9ga67878n05rviwi8jy2p45p7shq1wigawk3kg")))

(define-public crate-my_parser_kma_group_test_2-0.1.19 (c (n "my_parser_kma_group_test_2") (v "0.1.19") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0z5py4d9l54dal1p4v8g8yympxgl3gch33m5mxpr1qprmbl1s088")))

