(define-module (crates-io my _p my_parser) #:use-module (crates-io))

(define-public crate-my_parser-0.1.0 (c (n "my_parser") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1ck3z4ifb7zbzrg0d5v9sq2yv3mbxakhwxcwr9rbn06krimwnk66")))

(define-public crate-my_parser-0.1.1 (c (n "my_parser") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1xfw6dfp158z05crxbxir4cfxsp1wxmbighj4lp0virmwxdh88yz")))

