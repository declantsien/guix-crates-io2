(define-module (crates-io my _p my_public_ip) #:use-module (crates-io))

(define-public crate-my_public_ip-0.1.0 (c (n "my_public_ip") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.10") (d #t) (k 0)))) (h "1aj7f07l7b7hcvn71h3p2hjffmb512a3sj3ym7l0svraa9d14xcm")))

