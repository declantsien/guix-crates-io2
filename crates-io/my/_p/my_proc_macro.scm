(define-module (crates-io my _p my_proc_macro) #:use-module (crates-io))

(define-public crate-my_proc_macro-0.1.0 (c (n "my_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1nyghzxsbrshjra3lx6jynmsfyxqbg0xdwm53vvypf11gg5q505q")))

