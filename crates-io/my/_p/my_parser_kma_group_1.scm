(define-module (crates-io my _p my_parser_kma_group_1) #:use-module (crates-io))

(define-public crate-my_parser_kma_group_1-0.1.0 (c (n "my_parser_kma_group_1") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "04pxzz0k0wkcf220pbvqv9ar2rbyf6lfplkd21jrx55aqr7mnqld")))

(define-public crate-my_parser_kma_group_1-0.2.0 (c (n "my_parser_kma_group_1") (v "0.2.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0nhq1izv5r2f76cx88w92z3s26rsx130ax7n8c17x770ihg63l9p")))

