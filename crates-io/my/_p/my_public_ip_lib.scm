(define-module (crates-io my _p my_public_ip_lib) #:use-module (crates-io))

(define-public crate-my_public_ip_lib-0.1.0 (c (n "my_public_ip_lib") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qr6q90ibin4bbayyg3lfhmxxj1s0y2a2ds4l0zkv9lc1k4d11ib")))

(define-public crate-my_public_ip_lib-0.4.0 (c (n "my_public_ip_lib") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r9gmglw9ix4bfs4r8qjjri4ck9ahdk166g54zcyvgv862rmw6vy")))

