(define-module (crates-io my _p my_parser_yaroslav_fetisov) #:use-module (crates-io))

(define-public crate-my_parser_yaroslav_fetisov-0.1.0 (c (n "my_parser_yaroslav_fetisov") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0lv4rdizp0giskh2rzrx3q16699989h0mia4fcvv42gcqmwjy00d")))

(define-public crate-my_parser_yaroslav_fetisov-0.1.1 (c (n "my_parser_yaroslav_fetisov") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "160x0g2gzfrzfyizggwp7x38ld9nkvi4aq61vvlh90fca4qxp4x3")))

