(define-module (crates-io my _p my_parser_hvpo32dds) #:use-module (crates-io))

(define-public crate-my_parser_hVpo32Dds-0.1.0 (c (n "my_parser_hVpo32Dds") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0ynfl3lp3p81hsc2hs63y4vgc6m7q14d5l5pd19g4qdkdpli0jls")))

(define-public crate-my_parser_hVpo32Dds-0.1.1 (c (n "my_parser_hVpo32Dds") (v "0.1.1") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1g25njkibr9zagzfmm8c6g4kx82kp7k6xg3h5z0dd7mq4ham0xvg")))

(define-public crate-my_parser_hVpo32Dds-0.1.2 (c (n "my_parser_hVpo32Dds") (v "0.1.2") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1xbmilw9s38ninzad0k784szwhmhampdf423ilcqd9ipd5aknazx")))

