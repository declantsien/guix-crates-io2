(define-module (crates-io my _p my_proc_marco) #:use-module (crates-io))

(define-public crate-my_proc_marco-0.0.1 (c (n "my_proc_marco") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0xaqbzr6axdsb3r3b5kpvnm2h1fa2kng6kf1zzf11fzzjbvii8")))

