(define-module (crates-io my _p my_parser_ishevchyk) #:use-module (crates-io))

(define-public crate-my_parser_ishevchyk-0.1.0 (c (n "my_parser_ishevchyk") (v "0.1.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "1mcl649xxvrwp8gil6pajq4x4c40kpvb2rkyp6lfhaadrdr756br")))

(define-public crate-my_parser_ishevchyk-0.2.0 (c (n "my_parser_ishevchyk") (v "0.2.0") (d (list (d (n "peg") (r "^0.8.2") (d #t) (k 0)))) (h "0d8c3qzk5r2fwjapvsv28857p9yvvk7lpicbzdhypcg6mfk4qa81")))

