(define-module (crates-io my _p my_project0) #:use-module (crates-io))

(define-public crate-my_project0-0.1.0 (c (n "my_project0") (v "0.1.0") (d (list (d (n "test0") (r "^0.2.0") (d #t) (k 0)))) (h "1xl03j6p4dkcjmvc20yckpfkx62z8qiqipwx3g8al0yqkqwp6rm3")))

(define-public crate-my_project0-0.2.0 (c (n "my_project0") (v "0.2.0") (d (list (d (n "test0") (r "^0.2.0") (d #t) (k 0)))) (h "10ajqnxbkf123x8yl32ydxvadjnb5frrzxa9ihc5pvfcq62s2q0c")))

(define-public crate-my_project0-0.3.0 (c (n "my_project0") (v "0.3.0") (d (list (d (n "test0") (r "^0.4.0") (d #t) (k 0)))) (h "0sxd3cv15qxy1b6han5ppfs9226xh2hrv2d21k64lnlgwrgd2ghp")))

