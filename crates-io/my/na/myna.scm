(define-module (crates-io my na myna) #:use-module (crates-io))

(define-public crate-myna-0.2.0 (c (n "myna") (v "0.2.0") (d (list (d (n "der-parser") (r "^3.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 0)) (d (n "rsa") (r "^0.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1k2dyz4vsccmmmk7n0py11mi58mnl4axl3ffvggc38a0s4jvd874")))

(define-public crate-myna-0.3.0 (c (n "myna") (v "0.3.0") (d (list (d (n "der-parser") (r "^3.0.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 0)) (d (n "rsa") (r "^0.2.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.7.0") (d #t) (k 0)))) (h "066k29i81ifvv57r5b0bsbxgnb1gxbfkzld86vaw4dl55npvky29")))

