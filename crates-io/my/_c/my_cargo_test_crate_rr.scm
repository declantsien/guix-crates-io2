(define-module (crates-io my _c my_cargo_test_crate_rr) #:use-module (crates-io))

(define-public crate-my_cargo_test_crate_rr-0.1.0 (c (n "my_cargo_test_crate_rr") (v "0.1.0") (h "0qxm36jj1xgksygqygbljrq6pc1zrb4sakwn3vyr6xs9my90cd90") (y #t)))

(define-public crate-my_cargo_test_crate_rr-0.1.1 (c (n "my_cargo_test_crate_rr") (v "0.1.1") (h "0p8yr0b6zjr07c83xj0kgvxmg1an317bwqllcb7h6ff0acsnayql")))

