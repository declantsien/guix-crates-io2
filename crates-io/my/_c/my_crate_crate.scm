(define-module (crates-io my _c my_crate_crate) #:use-module (crates-io))

(define-public crate-my_crate_crate-0.1.0 (c (n "my_crate_crate") (v "0.1.0") (h "1ws7vvnpwczsda580xz48wyrjmqvn5d8hrfli5pfi87jj7vdspip")))

(define-public crate-my_crate_crate-0.2.0 (c (n "my_crate_crate") (v "0.2.0") (h "07bx9naz6iv6vll32jpic6i5kgxc08rk5w290za986s1irs81li1")))

