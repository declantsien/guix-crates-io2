(define-module (crates-io my _c my_crate_inside_a_repo_with_a_long_name1) #:use-module (crates-io))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.1.0 (c (n "my_crate_inside_a_repo_with_a_long_name1") (v "0.1.0") (h "1q0yxy93camzj6pwpcbby595a0fvdpr2b8likdzmqp7qqi56js8r")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.2.0 (c (n "my_crate_inside_a_repo_with_a_long_name1") (v "0.2.0") (h "0ihgx9csvlic5qwds1xf8f9il3w2sllkk5q5p687zgy2f5n5qzyl")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.3.0 (c (n "my_crate_inside_a_repo_with_a_long_name1") (v "0.3.0") (h "0h269j5dnb7v1gj0yqilhzm05vqihapn20p3rh0v9kb711hcwi9z")))

