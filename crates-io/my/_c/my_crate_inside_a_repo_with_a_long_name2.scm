(define-module (crates-io my _c my_crate_inside_a_repo_with_a_long_name2) #:use-module (crates-io))

(define-public crate-my_crate_inside_a_repo_with_a_long_name2-0.1.0 (c (n "my_crate_inside_a_repo_with_a_long_name2") (v "0.1.0") (d (list (d (n "my_crate_inside_a_repo_with_a_long_name1") (r "^0.1") (d #t) (k 0)))) (h "1i1an4npwb0kla141hmnh9xbmcvci824khfbd8l717yvqmp4gr38")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name2-0.2.0 (c (n "my_crate_inside_a_repo_with_a_long_name2") (v "0.2.0") (d (list (d (n "my_crate_inside_a_repo_with_a_long_name1") (r "^0.2") (d #t) (k 0)))) (h "19b13mvwdbz5j0rx21jlb4yyw7mgfayydxyyynh8kppbj1nw5b46")))

