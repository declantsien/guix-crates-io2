(define-module (crates-io my _c my_crate_demo) #:use-module (crates-io))

(define-public crate-my_crate_demo-0.1.1 (c (n "my_crate_demo") (v "0.1.1") (h "1cq72c87l0v8dm3z384pbpnz0lgjvcny3cji77wnya2j9r0ah83c") (y #t)))

(define-public crate-my_crate_demo-0.1.2 (c (n "my_crate_demo") (v "0.1.2") (h "14gpihirmgmm1m4vx7cc7cwzns27y4g7vm910aark8x669izndfl")))

