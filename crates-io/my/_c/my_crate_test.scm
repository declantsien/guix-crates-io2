(define-module (crates-io my _c my_crate_test) #:use-module (crates-io))

(define-public crate-my_crate_test-0.1.0 (c (n "my_crate_test") (v "0.1.0") (h "1kxaask9006jv0h3ia7w0p935k6ci23aaz2b1mcmmh30mq6cc33c")))

(define-public crate-my_crate_test-0.1.1 (c (n "my_crate_test") (v "0.1.1") (h "09bgjfzr9iyk7lly8jk7hqj3xlc8i4mximcl9ihgrn7969795rpk") (y #t)))

