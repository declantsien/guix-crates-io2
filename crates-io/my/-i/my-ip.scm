(define-module (crates-io my #{-i}# my-ip) #:use-module (crates-io))

(define-public crate-my-ip-0.1.0 (c (n "my-ip") (v "0.1.0") (d (list (d (n "dinglebit-log") (r "^0.1.0") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "15h0wdi1qs31594dpp1z7j8mgkpgchydiycgvyy7ajzi95y6b5gg")))

(define-public crate-my-ip-1.0.0 (c (n "my-ip") (v "1.0.0") (d (list (d (n "dinglebit-log") (r "^0.1.0") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "06ksxd1q2va20sj5xrndibr4y5m6ljxcqjn39hw24xcxfky2jd4z")))

(define-public crate-my-ip-1.1.0 (c (n "my-ip") (v "1.1.0") (d (list (d (n "dinglebit-log") (r "^0.1.0") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0favp42vhpyqqh25vxd7ywp6z7wj7jccx6zn5q86frhva93cln71")))

