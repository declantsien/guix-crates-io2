(define-module (crates-io my _e my_example) #:use-module (crates-io))

(define-public crate-my_example-0.1.0 (c (n "my_example") (v "0.1.0") (h "1cdfq2ail9ippp3vxd64dc9y2yd9139w6l3cdxknm126baqrr70c") (y #t)))

(define-public crate-my_example-0.1.1 (c (n "my_example") (v "0.1.1") (h "1ckac02x3bj2hbnkl278lr2k0jir3nvx162cw4qxzkcyznmbwmhm") (y #t)))

(define-public crate-my_example-0.1.2 (c (n "my_example") (v "0.1.2") (h "01yxc2n5idmr2xxjpwg1s2srm0xnmc439lsb4z2fn4hxx2cgzmpa")))

