(define-module (crates-io my bl myblog-proto-rust) #:use-module (crates-io))

(define-public crate-myblog-proto-rust-0.0.1 (c (n "myblog-proto-rust") (v "0.0.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "0jp3a4grmqpc5ch2wkmz5i4lnaa9srf9qy3j0hfw2iymrgl44cld") (y #t)))

(define-public crate-myblog-proto-rust-0.0.2 (c (n "myblog-proto-rust") (v "0.0.2") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "0rlhm63j2pad5r1scmyd0pvgjzzjhd4c3yfqrrc8why4m4bkdrxr") (y #t)))

(define-public crate-myblog-proto-rust-0.0.3 (c (n "myblog-proto-rust") (v "0.0.3") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "0lhpnqc26bqhhz4qha2205n3lz2ml0zwq8y19dj31nrdbqs246dv") (y #t)))

(define-public crate-myblog-proto-rust-0.0.4 (c (n "myblog-proto-rust") (v "0.0.4") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "1jm53rpyhzbr1rffnf9znyakzjfpaprbh4l2qnkj357v146bzrc0") (y #t)))

