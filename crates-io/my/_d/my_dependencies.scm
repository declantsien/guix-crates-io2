(define-module (crates-io my _d my_dependencies) #:use-module (crates-io))

(define-public crate-my_dependencies-0.1.0 (c (n "my_dependencies") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wl0i5ziy4fqvnraw8d7p4c36mcqxdqhcyshq0k3cgq2bx66m6hk")))

