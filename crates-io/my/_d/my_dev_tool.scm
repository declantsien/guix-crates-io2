(define-module (crates-io my _d my_dev_tool) #:use-module (crates-io))

(define-public crate-my_dev_tool-0.1.0 (c (n "my_dev_tool") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0730ipm3pki922q14hgq70mmjw3yzzxlkmgs825d1qkiqparwmfh")))

(define-public crate-my_dev_tool-0.2.0 (c (n "my_dev_tool") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.0") (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "1fn5vnd5kwnd6wli8z11m2rykhx62v1glw95vq70xx80kzswivdj")))

