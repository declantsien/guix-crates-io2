(define-module (crates-io my ru myrust_utils) #:use-module (crates-io))

(define-public crate-myrust_utils-0.1.0 (c (n "myrust_utils") (v "0.1.0") (h "19frq8rmv76kriz8sk3bsr88832bczkb0ifhkhsrbkg7qyz8lcfv") (y #t)))

(define-public crate-myrust_utils-0.2.0 (c (n "myrust_utils") (v "0.2.0") (h "0fx8wy0mq97aw4hx9nkq954j7ql66xgj0jdzr6m8navikz2lqv2i") (y #t)))

(define-public crate-myrust_utils-0.3.0 (c (n "myrust_utils") (v "0.3.0") (h "1nyr7lr84dayhl5zffi9s86zpm7p8q0p2r3mw5jhb609nqva5m4c") (y #t)))

