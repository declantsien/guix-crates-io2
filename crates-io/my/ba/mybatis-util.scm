(define-module (crates-io my ba mybatis-util) #:use-module (crates-io))

(define-public crate-mybatis-util-1.0.0 (c (n "mybatis-util") (v "1.0.0") (d (list (d (n "mybatis-drive") (r "^1.0.0") (f (quote ("debug_mode"))) (d #t) (k 0)) (d (n "rbson") (r "^2.0.3") (f (quote ("uuid-0_8" "chrono-0_4"))) (d #t) (k 0)))) (h "12y87c0x7brsh90vg6zchyp4nfg58rkaz52ak473lzig7m7kf3d8") (r "1.59.0")))

(define-public crate-mybatis-util-1.0.3 (c (n "mybatis-util") (v "1.0.3") (d (list (d (n "mybatis-core") (r "^1.0.3") (d #t) (k 0)) (d (n "rbson") (r "^2.0.3") (f (quote ("uuid-0_8" "chrono-0_4"))) (d #t) (k 0)))) (h "0735vvn1rwwx289vsyvnnj9fd10hdx72r605ky8x7qng50gb2vpp") (r "1.59.0")))

(define-public crate-mybatis-util-1.0.8 (c (n "mybatis-util") (v "1.0.8") (d (list (d (n "mybatis-core") (r "^1.0.3") (d #t) (k 0)) (d (n "rbson") (r "^2.0.3") (f (quote ("uuid-0_8" "chrono-0_4"))) (d #t) (k 0)))) (h "1i3fi6f61c7c46qg9awrja37saq4r95iyfqv426i8r3c2qm5sgdh") (r "1.59.0")))

(define-public crate-mybatis-util-2.0.0 (c (n "mybatis-util") (v "2.0.0") (d (list (d (n "mybatis-core") (r "^2.0.0") (d #t) (k 0)) (d (n "rbson") (r "^2.0.3") (f (quote ("uuid-0_8" "chrono-0_4"))) (d #t) (k 0)))) (h "0agvihqpa6dkrlnsr5lr7wndibd6h5d57f4hn838rz9xi4mznpxd") (r "1.59.0")))

