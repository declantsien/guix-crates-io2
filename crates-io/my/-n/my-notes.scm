(define-module (crates-io my -n my-notes) #:use-module (crates-io))

(define-public crate-my-notes-1.0.0 (c (n "my-notes") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "0cf7r9sc1rhcgili29f3sk7di5i76gmgwdg59zqjndd3ahpz6aj5")))

(define-public crate-my-notes-1.1.0 (c (n "my-notes") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "09cr860llgwxprxl7qw03wnn7r669l8r6i3v4jbsj8p0qzdyrg1k")))

(define-public crate-my-notes-1.1.2 (c (n "my-notes") (v "1.1.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "1yqm41n4h7869j240ilp4gryh206fl6gb193hwi0v1w8lp5fa5la")))

(define-public crate-my-notes-1.2.0 (c (n "my-notes") (v "1.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "05m3m3dhqb0bfhcvgdl0gf7hcr2bl0q7ikls8wifai6s1mghgyip")))

(define-public crate-my-notes-1.2.1 (c (n "my-notes") (v "1.2.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "1kjs6z9p0jgfpjas73hl1vw84w4kq426w32l7scix34060a726gr")))

(define-public crate-my-notes-1.2.2 (c (n "my-notes") (v "1.2.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "1a3vx39i9i8rx0h8zg6m7vkmf9fh69a6fx9np189jvm28hnrk1i3")))

(define-public crate-my-notes-1.3.0 (c (n "my-notes") (v "1.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "magic_string_search") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "termimad") (r "^0.23.0") (d #t) (k 0)))) (h "012acirs0jc4hp5wrwkkjzbjri8fxbn5nv0fyzczh58cqni1kdsv")))

