(define-module (crates-io my _h my_hello_world) #:use-module (crates-io))

(define-public crate-my_hello_world-0.1.0 (c (n "my_hello_world") (v "0.1.0") (h "0hpn4qrq0gk4pjmyhzngpngb816874n0whsx7hs41y1vfb4l7gv5") (y #t)))

(define-public crate-my_hello_world-0.1.1 (c (n "my_hello_world") (v "0.1.1") (h "05kp4xn3kp0cdg38yvbq8zgy608cnj95fbzy3iw6yaxpc11nf656") (y #t)))

