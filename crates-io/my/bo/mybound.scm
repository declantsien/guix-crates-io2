(define-module (crates-io my bo mybound) #:use-module (crates-io))

(define-public crate-mybound-0.1.0 (c (n "mybound") (v "0.1.0") (h "0b48c0429ygxkdf9nqlh6vll26xnp47zgnz3h9rhpysyz6idajzg")))

(define-public crate-mybound-0.1.1 (c (n "mybound") (v "0.1.1") (h "1ng9w1w8lzhd3n079cq7x76a4chww4hysy7g0bmsfd8lkwizm12m")))

(define-public crate-mybound-0.1.2 (c (n "mybound") (v "0.1.2") (h "06zy4jp8rzw72g5c8zfar9dj7jlbdpgv7bqaxnh0nhg1dsyfiqgx")))

