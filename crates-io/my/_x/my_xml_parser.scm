(define-module (crates-io my _x my_xml_parser) #:use-module (crates-io))

(define-public crate-my_xml_parser-0.1.2 (c (n "my_xml_parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "12gmvw9604qqqynv3bgnkrn4sipypw95jm39gyn6m15njqy3rds0")))

