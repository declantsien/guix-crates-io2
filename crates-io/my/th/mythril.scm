(define-module (crates-io my th mythril) #:use-module (crates-io))

(define-public crate-mythril-0.1.0 (c (n "mythril") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "num_enum") (r "^0.4.2") (k 0)) (d (n "uefi") (r "^0.3.2") (d #t) (k 0)) (d (n "uefi-services") (r "^0.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.7.5") (d #t) (k 0)))) (h "0hz68b37ipcz12knkgyql5kdnpnh4vqr66x3a4w9lvyr8lrklfkb")))

