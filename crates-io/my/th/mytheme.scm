(define-module (crates-io my th mytheme) #:use-module (crates-io))

(define-public crate-mytheme-0.0.2 (c (n "mytheme") (v "0.0.2") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "prql-compiler") (r "^0.8.1") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "07gd7883i9wff6vjkfjzzslsa7n66dzqrsyixm91xqx2qcdb7d1y") (y #t)))

