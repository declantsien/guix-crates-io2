(define-module (crates-io my th mythoji) #:use-module (crates-io))

(define-public crate-mythoji-0.1.0 (c (n "mythoji") (v "0.1.0") (d (list (d (n "strum") (r "^0.24.1") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (o #t) (d #t) (k 0)))) (h "1mv1p3ma8k8icyjj1w7lanyymim53jb9ns08y78xfi9404bx42f0") (f (quote (("iter" "strum" "strum_macros"))))))

