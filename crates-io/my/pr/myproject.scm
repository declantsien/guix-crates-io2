(define-module (crates-io my pr myproject) #:use-module (crates-io))

(define-public crate-myproject-0.1.0 (c (n "myproject") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.4") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)))) (h "16g7psb0ryfsnjr5kqcx1lbpqcrhk4bllwx3i1wdwnqqwb90fg17") (y #t)))

