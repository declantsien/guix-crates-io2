(define-module (crates-io my ls myls) #:use-module (crates-io))

(define-public crate-myls-0.1.3 (c (n "myls") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "0j66libdbl0b56472yvi5sl6an4inyyicgbkp152i20gi5vc3qac")))

(define-public crate-myls-0.1.4 (c (n "myls") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "size") (r "^0.1.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.9.1") (d #t) (k 0)))) (h "1s4h8j37n4sw6rn4y5jfv358id48hs7844nhgkbppjzyhnm8ngzr")))

