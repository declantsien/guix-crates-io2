(define-module (crates-io my -s my-stack) #:use-module (crates-io))

(define-public crate-my-stack-0.1.0 (c (n "my-stack") (v "0.1.0") (h "0dghj8dba1185f0pzq0xdw5p3f7f9i8mb650zkmp77p0mj8zrvjs")))

(define-public crate-my-stack-0.1.1 (c (n "my-stack") (v "0.1.1") (h "1lm1ac5jrwr6izp74qinqsq3h8fvjzsc0r2dwnidi7za4fb7c219")))

