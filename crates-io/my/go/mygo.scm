(define-module (crates-io my go mygo) #:use-module (crates-io))

(define-public crate-mygo-0.1.0 (c (n "mygo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "0xl1h5y9wbfpvxycv8zwyvc79vky9yn8k2n1dc476vkgz1qmqz0m")))

(define-public crate-mygo-0.1.1 (c (n "mygo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("net" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.13") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (f (quote ("fs"))) (d #t) (k 0)))) (h "11zmnc6km5ggpvgva0i5664jc84n5xh0al05w9apycwm433iihsb")))

