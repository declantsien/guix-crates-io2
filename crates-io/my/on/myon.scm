(define-module (crates-io my on myon) #:use-module (crates-io))

(define-public crate-myon-0.1.0 (c (n "myon") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "0nzkmrk7z1k325bhr8d7rvshlj9z9gclqsm9zlbrm2ik94bxpvsf") (y #t)))

