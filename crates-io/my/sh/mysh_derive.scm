(define-module (crates-io my sh mysh_derive) #:use-module (crates-io))

(define-public crate-mysh_derive-0.1.0 (c (n "mysh_derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0mzarvf2yck5gmpa4dq3565za6b8bcq5ijk4lkwc6nipjqd5s7kp") (r "1.77")))

(define-public crate-mysh_derive-0.1.6 (c (n "mysh_derive") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde-reflection") (r "^0.3.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "150ybsd0542ix679dwr680jv8a0xq1z8v26dpq9ir4b76lca7b2f") (r "1.77")))

