(define-module (crates-io my um myumberbar) #:use-module (crates-io))

(define-public crate-myumberbar-0.0.1 (c (n "myumberbar") (v "0.0.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "umberbar") (r "^0.7.5") (d #t) (k 0)))) (h "16vvngsd6s5rgjc59nbi2higgkb8csyi7vhya14ady2fknlzcfn1")))

(define-public crate-myumberbar-0.0.2 (c (n "myumberbar") (v "0.0.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "umberbar") (r "^0.7.6") (d #t) (k 0)))) (h "1m16wgbds3gypn8iqqz7fcmxishf520r4g15ndi0ni0pcc7gkw8v")))

