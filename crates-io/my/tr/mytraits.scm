(define-module (crates-io my tr mytraits) #:use-module (crates-io))

(define-public crate-mytraits-0.1.0 (c (n "mytraits") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1982vls7b2v1fnmd0vibv39nd4viy9lcvl8vsqyxagaxrj7i0aa7") (y #t)))

