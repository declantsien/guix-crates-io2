(define-module (crates-io my tr mytrie) #:use-module (crates-io))

(define-public crate-mytrie-0.1.0 (c (n "mytrie") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04q06928xd9farvhip7riffky3mily19g1g2z7y2f5zawlpxkfqm")))

(define-public crate-mytrie-0.2.0 (c (n "mytrie") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10bjmh36xxrh7dzm2m1xv2xygz3skcpc1qhwaxr9bk4z1n8c926v")))

(define-public crate-mytrie-0.2.1 (c (n "mytrie") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00w6xv0c6j548w5xw3s1mjqa2hz50b8ibj1va53hn1yxhmba8pik")))

(define-public crate-mytrie-0.2.2 (c (n "mytrie") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00pfxyl6c2hlssj79m1lmb3867yrz0hmrii2n5k91j4siiny6q35")))

