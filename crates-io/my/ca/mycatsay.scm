(define-module (crates-io my ca mycatsay) #:use-module (crates-io))

(define-public crate-mycatsay-0.1.0 (c (n "mycatsay") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1z3nq1lrvyv2db0zxmy7b8bbigk693mppkl1qvc0gs733f649nvk")))

