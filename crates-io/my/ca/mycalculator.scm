(define-module (crates-io my ca mycalculator) #:use-module (crates-io))

(define-public crate-mycalculator-0.1.0 (c (n "mycalculator") (v "0.1.0") (h "12f8zr2ygbz38kgc4r2s1ny9p991sifkg5n684x951xxibb0s5n6") (r "1.74")))

(define-public crate-mycalculator-0.2.0 (c (n "mycalculator") (v "0.2.0") (h "0lbrah8x3b34sf1wzbw0z1r85zxbdc7sjhfvi94f1lca2v4qx24g") (r "1.74")))

