(define-module (crates-io my gp mygpoclient) #:use-module (crates-io))

(define-public crate-mygpoclient-0.1.0 (c (n "mygpoclient") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0azh1nnc1y2h96hsfksnsr218chr355kd01cllcf5314f044ihcl") (y #t)))

(define-public crate-mygpoclient-0.1.1 (c (n "mygpoclient") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0mkvy460dh5d90zwyxxrx6b5ql91nky7rdzybb0h9mpjn8yirg43")))

(define-public crate-mygpoclient-0.2.0 (c (n "mygpoclient") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "06k89yrq5vqzh7pcij03nbgrfpi3m8mmccb1vzlxvwmi4kb7d1nm")))

