(define-module (crates-io my le mylex) #:use-module (crates-io))

(define-public crate-mylex-0.1.0 (c (n "mylex") (v "0.1.0") (h "0ccm1crf8vcwvhpbr2pvsa3p23w8pcvi241y8a5ljdi1dz3wy8ns")))

(define-public crate-mylex-0.1.1 (c (n "mylex") (v "0.1.1") (h "019lvvvk843ps6md6yzc9fwj8gmd0xwp7ylkwfkibgpszag5z7m3")))

