(define-module (crates-io my el myelin-geometry) #:use-module (crates-io))

(define-public crate-myelin-geometry-1.0.0 (c (n "myelin-geometry") (v "1.0.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "181qdrh6vls8gkil6l2mkywi55zf2drcf5ciwdh8jr66c5w20xx8")))

(define-public crate-myelin-geometry-2.0.0 (c (n "myelin-geometry") (v "2.0.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08sm0rqdj8w4gy4v5i6mg58w6wzzbvqamhgpz7k6mxhwfq6sxrzk")))

(define-public crate-myelin-geometry-2.1.0 (c (n "myelin-geometry") (v "2.1.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16qlk2z015p45i2dmc1ajhb3g24hp95y5znl417qpk56igw6xka8")))

(define-public crate-myelin-geometry-2.2.0 (c (n "myelin-geometry") (v "2.2.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1lzb2mi50rdhmw0s8pijzslbjj5g6nwyaxz16kqyqiyzxgd4p0cm")))

(define-public crate-myelin-geometry-2.3.0 (c (n "myelin-geometry") (v "2.3.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1q6zxl36liwpnvhvf2z3k62ns3ajk4jmqvdzf5l3rpdlldlgxb7p")))

(define-public crate-myelin-geometry-2.4.0 (c (n "myelin-geometry") (v "2.4.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ni5p7ibai16srh0xas995b285yhgpyq0wfc9njprj5fx5g6jsqj")))

(define-public crate-myelin-geometry-2.4.1 (c (n "myelin-geometry") (v "2.4.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08l8b13nrxqhk2cv77k4ci8haiirvwh6rg7gz2f4isqn8dw94565")))

(define-public crate-myelin-geometry-2.4.2 (c (n "myelin-geometry") (v "2.4.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nearly_eq") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "157kvhzmnyparapbkh0l7l4g9zx8df88pgx5c2vn0gqhqm8704dd")))

