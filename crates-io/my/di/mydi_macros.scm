(define-module (crates-io my di mydi_macros) #:use-module (crates-io))

(define-public crate-mydi_macros-0.1.0 (c (n "mydi_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0d56dr1lmkdnisxmjvnm65692yp575dnpihqdx966hgcwy0c11pz")))

(define-public crate-mydi_macros-0.1.1 (c (n "mydi_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8ggr18k9sy6vzci4f671x4r8m3zfh6xir8xgnd2c5lbq17a7kq")))

(define-public crate-mydi_macros-0.1.2 (c (n "mydi_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1zjf3as05mrhxfbx06qj5diji25xswrq4ick6d2hv2sdm7iwhyjl")))

(define-public crate-mydi_macros-0.2.0 (c (n "mydi_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0vvkmjzyigb1hvirszjmmsiz19mcnd6hhkpwdr51s8fnyzgjysj1")))

(define-public crate-mydi_macros-0.2.1 (c (n "mydi_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1441iq8k1pwl4jdzb86yxqzc58s2bs896vjx7l1wqld515yz698m")))

(define-public crate-mydi_macros-0.2.2 (c (n "mydi_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0yd6gkxqiffhw98cxxzhn9khcq54dq6yblvn0kmyqpxinydjsxz4")))

