(define-module (crates-io my di mydi) #:use-module (crates-io))

(define-public crate-mydi-0.1.0 (c (n "mydi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "18n2ykjb7hrcskr1hf87mr1r8q105js93c0hzm85yqww7m80z62z")))

(define-public crate-mydi-0.1.1 (c (n "mydi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "06kvklr6wrakgkwgn3mjdlc0qp4f8nyylyqski809nlnvc220vrb")))

(define-public crate-mydi-0.1.2 (c (n "mydi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "05vpzw2xfggz58v6v6gmn4mpl3ykyvxdl0f91p1jpapdq3r0jmyg")))

(define-public crate-mydi-0.2.0 (c (n "mydi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "176yfggmfrwnwfw2a2ap6q7g4mkwqzfaxd3b7q9d7qbnqa69ys6q")))

(define-public crate-mydi-0.2.1 (c (n "mydi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1ilsj66pgrps1aa5y7hhaldarh88iv410vv2lpgd524a3q2d3c88")))

(define-public crate-mydi-0.2.2 (c (n "mydi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 2)) (d (n "mydi_macros") (r "^0.2.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1i5i7kg80dhj9d77q2ywljs0kzwyhb3gf2zhx2z9sf4v98kk26c6")))

