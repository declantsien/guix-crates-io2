(define-module (crates-io my op myopic-board) #:use-module (crates-io))

(define-public crate-myopic-board-1.0.0 (c (n "myopic-board") (v "1.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b4igy0i9r3kf86z7iylshw4qvcy1q2winx0558lbfi3211lgwmq")))

(define-public crate-myopic-board-1.0.1 (c (n "myopic-board") (v "1.0.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kzzrsa57msvjcyqz77p8xpq3jhdx4ag2gq2nslv93vid17wny7w")))

(define-public crate-myopic-board-1.1.0 (c (n "myopic-board") (v "1.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b4zmvlvyhls43ndmfvpmbzl4z5vnr2z7aznj0wz8v9gs7zg2kgc")))

(define-public crate-myopic-board-1.1.1 (c (n "myopic-board") (v "1.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qzqdscxnma1ds08sdxw5pn4d7v0wxcqj9dfpvr7d75gyhdmcv47")))

(define-public crate-myopic-board-1.1.2 (c (n "myopic-board") (v "1.1.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0qj2lmd481k5qqfzhvpwhjqj58fw2ywb35z1fs093cp0pvsxxxzh")))

(define-public crate-myopic-board-1.2.0 (c (n "myopic-board") (v "1.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ank1grzpv32fa5nvvq2k1ly8ik1clli05fsd7v3v4m3k6y67yxa")))

(define-public crate-myopic-board-1.2.1 (c (n "myopic-board") (v "1.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1d4ccgjkmp76hnnr78y804b53a52yy24bc1xnxiw7hkd4zwgk1c1")))

(define-public crate-myopic-board-1.3.0 (c (n "myopic-board") (v "1.3.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k0j9m9gz8lylgnbcjp59d6mwpipbxkas221bh6hkzgyc1387azx")))

(define-public crate-myopic-board-1.3.1 (c (n "myopic-board") (v "1.3.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05hpi59m15ljgp3x5vs0d9qfzhwjv0dwdf5fi801cmgqax7vkras")))

(define-public crate-myopic-board-1.3.2 (c (n "myopic-board") (v "1.3.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kh5s4gzg0vmyzg4gzgq25n6gxci1qqvialacaj3r61ccsad8fw3")))

(define-public crate-myopic-board-1.3.3 (c (n "myopic-board") (v "1.3.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1g67522qc8lqckayd1h5g7zhcqaclhbi9bf8krh8zj6sj9w3zgrc")))

(define-public crate-myopic-board-1.3.4 (c (n "myopic-board") (v "1.3.4") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sj9c5s7m5m2d1zb61ygfb0iis4pny4f1khwqhkyzrk8231qcvxn")))

(define-public crate-myopic-board-2.0.0 (c (n "myopic-board") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18zdpr5gnll53zcdarq5lx9bn9nf76x11fzz0rh590z76618lhb7")))

(define-public crate-myopic-board-2.0.1 (c (n "myopic-board") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dcy3mzady66yq37v0irapxf299r4icbl6sfxa9q64mdl6kn5p4d")))

(define-public crate-myopic-board-2.1.0 (c (n "myopic-board") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04nbadiz01zyqaqjvlrcri6kq1qi63hlw17v1y4k4hbw8b8p7w7r")))

(define-public crate-myopic-board-2.1.1 (c (n "myopic-board") (v "2.1.1") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s0qx6wp9mlbc9p8cc09ldg26gka52jha5l0gfwklmg3qn0787l8") (y #t)))

(define-public crate-myopic-board-2.1.2 (c (n "myopic-board") (v "2.1.2") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18yng2jh8mag96krd63cccxv3q58nlyhbmky5ck390xn7lp18k02")))

(define-public crate-myopic-board-2.1.3 (c (n "myopic-board") (v "2.1.3") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.2.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02bym4lw4mvwk9k541cqixq2bl5bc639qkqh3bl29r9bgz7hbb4z")))

(define-public crate-myopic-board-2.2.0 (c (n "myopic-board") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "myopic-core") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p546ys2zm0jrxd89y1l21xlapqc2ga32qgnj69kssx83nnb6840")))

