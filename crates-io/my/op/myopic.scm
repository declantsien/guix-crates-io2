(define-module (crates-io my op myopic) #:use-module (crates-io))

(define-public crate-myopic-0.1.0 (c (n "myopic") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wm2fjmdx6pnzmbf6l2dnzfim58n5cz5pfd618nrhmqa4bw6l8n9")))

(define-public crate-myopic-0.1.1 (c (n "myopic") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jrj6lpgyrnagggf699cn6fmkqznlrkk41zy9i9aj3lchj0q7b2c")))

(define-public crate-myopic-0.1.2 (c (n "myopic") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0a120ki3ab67kp8bwscac6phrmarcnyh7xz9x45ddyjy2r9v1nkg")))

