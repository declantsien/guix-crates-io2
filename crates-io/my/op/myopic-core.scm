(define-module (crates-io my op myopic-core) #:use-module (crates-io))

(define-public crate-myopic-core-1.0.0 (c (n "myopic-core") (v "1.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "0l7hrbbw25b84r1v7a15m4d8yl0zfqif0jdym80sn7gdbw16ggcy")))

(define-public crate-myopic-core-1.0.1 (c (n "myopic-core") (v "1.0.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "0d2i7mrfm6yzz0qzmfiaaq8ik0q3b3n4lmqjn5jpq0ns8jhfxfz9")))

(define-public crate-myopic-core-1.1.0 (c (n "myopic-core") (v "1.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1wkn8g3nh983idxqb1v03r6q4qhayd3hakxgpy3dzihh8r2c21p5")))

(define-public crate-myopic-core-1.1.1 (c (n "myopic-core") (v "1.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1b32d4g089y217bzfh4il5r5lxmzqcpjsrbpp0lgmgfnl5wg8rgw") (y #t)))

(define-public crate-myopic-core-1.1.2 (c (n "myopic-core") (v "1.1.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "003vq881m120ir2carmzsnx8mkzhrfvviibikza4a4v338sav64i")))

(define-public crate-myopic-core-1.2.0 (c (n "myopic-core") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "188pjsrvqwmp6fmw39cij6szg1f36s6hqag3nsi7qf32van4pdrp")))

(define-public crate-myopic-core-1.2.1 (c (n "myopic-core") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1qlxsfd06d0xzszwigqba0gzl4720n12nl5bfmkpmw1zl5l9x720")))

(define-public crate-myopic-core-1.2.2 (c (n "myopic-core") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1cdaxvq9hjm195fl4b86va1q2phggs2ijvdgaff8j5kjda8b4bbf")))

(define-public crate-myopic-core-1.2.3 (c (n "myopic-core") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1mfbx0aq38fzyjay8s46nbxifqxqbbxk3zrgjy3nl7wx2hzqx6ps")))

(define-public crate-myopic-core-1.2.4 (c (n "myopic-core") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "1hsdi6vfjdj76hrqczs04l9m0rnv840cdy4r0mfc7r2czlzh63fm")))

(define-public crate-myopic-core-1.3.0 (c (n "myopic-core") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0.35") (d #t) (k 0)) (d (n "enumset") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.1.2") (d #t) (k 0)))) (h "07nz9965yws1dccf4mvhri5m4hdxn7njn90sap6s29ys1xrzwzmy")))

