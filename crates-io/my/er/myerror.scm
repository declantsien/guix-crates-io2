(define-module (crates-io my er myerror) #:use-module (crates-io))

(define-public crate-myerror-0.1.0 (c (n "myerror") (v "0.1.0") (h "013avkqh6phkbqwxxy4rcg3njfh0z8zbi2i1qg8748brhy7mqfhc")))

(define-public crate-myerror-0.1.1 (c (n "myerror") (v "0.1.1") (h "1bn3bdb9py39bj66icsa8gm88bllmgdbaz5p3xvqjfj4grrvcj24")))

