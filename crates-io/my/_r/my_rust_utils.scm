(define-module (crates-io my _r my_rust_utils) #:use-module (crates-io))

(define-public crate-my_rust_utils-0.1.0 (c (n "my_rust_utils") (v "0.1.0") (h "16sv1zl00zizal41sr5xxq54wlk8gh8ilj7vx8fr6jsfiybih76j")))

(define-public crate-my_rust_utils-0.1.1 (c (n "my_rust_utils") (v "0.1.1") (h "03ndpqfc772j43rvigjvanv3aj48ia7yidrcma7dy2yshzc1blb7")))

