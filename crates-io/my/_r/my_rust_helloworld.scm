(define-module (crates-io my _r my_rust_helloworld) #:use-module (crates-io))

(define-public crate-my_rust_helloworld-0.1.0 (c (n "my_rust_helloworld") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "ferris-says") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vj7h5lzl7sy6nvqs36xk9xwc2bs6jdhz5gyy6vhvc97g8aarkgx")))

(define-public crate-my_rust_helloworld-0.1.1 (c (n "my_rust_helloworld") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "ferris-says") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0yg1nfplik1wim2vwc4gz97i75wbj3sd0ihavskcp424lfjd9jgc")))

