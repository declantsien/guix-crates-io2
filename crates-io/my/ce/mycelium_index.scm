(define-module (crates-io my ce mycelium_index) #:use-module (crates-io))

(define-public crate-mycelium_index-0.1.0 (c (n "mycelium_index") (v "0.1.0") (d (list (d (n "mycelium_command") (r "^0.1.0") (d #t) (k 0)) (d (n "mycelium_core") (r "^0.1.0") (d #t) (k 0)) (d (n "mycelium_experimental") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "15c5zvy533h0ir2k3ikhw3b05rq2774vm5vqsyy4mspf7gm8wds1")))

(define-public crate-mycelium_index-0.1.1 (c (n "mycelium_index") (v "0.1.1") (d (list (d (n "mycelium_command") (r "^0.1.1") (d #t) (k 0)) (d (n "mycelium_core") (r "^0.1.1") (d #t) (k 0)) (d (n "mycelium_experimental") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1n4cd9p7w5zmf0q07m85a8grbjcwscc452gljnab31y270cahba9")))

