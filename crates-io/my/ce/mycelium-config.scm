(define-module (crates-io my ce mycelium-config) #:use-module (crates-io))

(define-public crate-mycelium-config-4.15.1 (c (n "mycelium-config") (v "4.15.1") (d (list (d (n "mycelium-base") (r "^4.15.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1vd2xpnxm94nfik0fgxcw3gnssgzd176qcyx3w71qdf193cdif6y")))

(define-public crate-mycelium-config-4.15.3 (c (n "mycelium-config") (v "4.15.3") (d (list (d (n "mycelium-base") (r "^4.15.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "08l0xbl4zxdp8md7q3dspg23wsmrhdccpdnw88xk6l6k2j417ys7")))

(define-public crate-mycelium-config-4.16.0 (c (n "mycelium-config") (v "4.16.0") (d (list (d (n "mycelium-base") (r "^4.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xhzn7c0cd0lvhmh60d00wbkacjmrnj2swdnnbna9qy9rijjj844")))

(define-public crate-mycelium-config-5.0.0 (c (n "mycelium-config") (v "5.0.0") (d (list (d (n "mycelium-base") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0l61f3d6j0xyvf4xp1nn9idc4g076ckliqm1r5md9d02f8yii5cv")))

(define-public crate-mycelium-config-5.0.1 (c (n "mycelium-config") (v "5.0.1") (d (list (d (n "mycelium-base") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11qzb6x2v5i9rbqxgg9c0q96bsah3rskb16qh1gb997f707w49ka")))

(define-public crate-mycelium-config-5.0.2 (c (n "mycelium-config") (v "5.0.2") (d (list (d (n "mycelium-base") (r "^5.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11blhjk00q7hxyvb450gam1h4cwxgssfm7zzxsbbw7r74rr4nygb")))

(define-public crate-mycelium-config-5.0.8 (c (n "mycelium-config") (v "5.0.8") (d (list (d (n "mycelium-base") (r "^5.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0rr8jlpqm4q51mrizw613r33d6w98w02ps0v066jqsa1yx3rds3d")))

