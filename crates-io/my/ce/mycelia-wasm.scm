(define-module (crates-io my ce mycelia-wasm) #:use-module (crates-io))

(define-public crate-mycelia-wasm-0.1.0 (c (n "mycelia-wasm") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "mycelial-crdt") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1xbfaw4r4mmx2jq74qphrwzb9mmcsmz1nd06rvfm4awdaznp00sz") (y #t)))

