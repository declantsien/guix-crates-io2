(define-module (crates-io my ce mycelial-crdt) #:use-module (crates-io))

(define-public crate-mycelial-crdt-0.1.0 (c (n "mycelial-crdt") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1by13cba3h1z2ban2w1grlik7k1218kysjl2kmhvm6zgx7q4jzxj")))

(define-public crate-mycelial-crdt-0.1.1 (c (n "mycelial-crdt") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1654jd97vk5qg7ilqbk2carbrnd1r3xpqb0j0aifz7rgd0m0kzlg")))

(define-public crate-mycelial-crdt-0.2.0 (c (n "mycelial-crdt") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ia7z0p3ibvha98cp9q2403sn49fdjr5x2ymhcsnc8m4rdplwis4")))

(define-public crate-mycelial-crdt-0.2.1 (c (n "mycelial-crdt") (v "0.2.1") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ih6kq288arbgi0n1s69qqhxhzbi7qyq0vx3pjniqlacb03cf664")))

(define-public crate-mycelial-crdt-0.2.2 (c (n "mycelial-crdt") (v "0.2.2") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mpyfhzxg1sdds3jgjdja3wrb6p4cgmjsbvqg1cf1j15w2mc234j")))

(define-public crate-mycelial-crdt-0.2.3 (c (n "mycelial-crdt") (v "0.2.3") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y3khwwi7hvv5rrazck6xg9yl707is47wvgzxdnmlik1m2j3fazy")))

(define-public crate-mycelial-crdt-0.2.4 (c (n "mycelial-crdt") (v "0.2.4") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0canf7pxzcshp9pzhpwffzd2a5igiyi3k36vq58wzji6yazbgwyh")))

(define-public crate-mycelial-crdt-0.2.5 (c (n "mycelial-crdt") (v "0.2.5") (d (list (d (n "num") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06ppsmqs1628zxb8dwgcwhk9clllm8bd96c3vcwwf14w0yj7391a")))

