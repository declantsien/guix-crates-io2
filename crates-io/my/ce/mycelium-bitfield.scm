(define-module (crates-io my ce mycelium-bitfield) #:use-module (crates-io))

(define-public crate-mycelium-bitfield-0.1.0 (c (n "mycelium-bitfield") (v "0.1.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0vwf2kg1l9563j8qz1d296qxf34c5rx8hrnczrwsm1r97x9n5rx2") (r "1.61.0")))

(define-public crate-mycelium-bitfield-0.1.1 (c (n "mycelium-bitfield") (v "0.1.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0xad5cphfm6ap0raa970byp6ypqcwrygqa0w9jchjfhdvm83f1ng") (r "1.61.0")))

(define-public crate-mycelium-bitfield-0.1.2 (c (n "mycelium-bitfield") (v "0.1.2") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0r5l18mi7jp5ibq1an9dpk4lvy1nwrd22b7c3n50pwrqgkyv9wwn") (r "1.61.0")))

(define-public crate-mycelium-bitfield-0.1.3 (c (n "mycelium-bitfield") (v "0.1.3") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "119x41yah6ws3lnajbh0zswa574kb454d5riplxqqsyj75za6y0i") (r "1.61.0")))

(define-public crate-mycelium-bitfield-0.1.4 (c (n "mycelium-bitfield") (v "0.1.4") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "03r9ka6lz9g38r235ghc208lfwzbgbb95d7zndvqqd5p1wl6s6sa") (r "1.61.0")))

(define-public crate-mycelium-bitfield-0.1.5 (c (n "mycelium-bitfield") (v "0.1.5") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1k21szpzbi0i6f3laczl2d9ls7qyyzgi3sawbk8wnnjq5igcrq14") (r "1.61.0")))

