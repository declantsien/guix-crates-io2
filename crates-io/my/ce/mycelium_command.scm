(define-module (crates-io my ce mycelium_command) #:use-module (crates-io))

(define-public crate-mycelium_command-0.1.0 (c (n "mycelium_command") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hsv0y057api1nq14l0zw53rwzplrbcxly5850idmhskh0p7bm1m")))

(define-public crate-mycelium_command-0.1.1 (c (n "mycelium_command") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "13dp0a3a56iy14wcy8ci3j6hqg228yyajch5xz3bdahj8c4plhyx")))

