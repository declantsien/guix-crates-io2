(define-module (crates-io my ce mycelium) #:use-module (crates-io))

(define-public crate-mycelium-0.1.0 (c (n "mycelium") (v "0.1.0") (h "0p2k5wf9phi82qv9h0li3d17l2z5cc3vbvs94n1c7avv12q6njiv")))

(define-public crate-mycelium-0.1.1 (c (n "mycelium") (v "0.1.1") (d (list (d (n "mycelium_lib") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)))) (h "07isaz7ylfrjzjvf61im6psbqax9axysq6gvhw0j5aag8027wi4y")))

