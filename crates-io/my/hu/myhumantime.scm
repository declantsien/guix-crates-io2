(define-module (crates-io my hu myhumantime) #:use-module (crates-io))

(define-public crate-myhumantime-1.0.0 (c (n "myhumantime") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1nfcr18iwzzj48b0lph08iqq550hpgp1mfbc7jmf83qk6qrmzzhi")))

