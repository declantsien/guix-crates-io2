(define-module (crates-io my fi myfind) #:use-module (crates-io))

(define-public crate-myfind-0.1.0 (c (n "myfind") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s014wjsdy0m2f0mn8fnxfyrlbgaf2pjfg3cr16p86mj0az8aiaa")))

(define-public crate-myfind-0.1.1 (c (n "myfind") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a82136r7qfz6r71q41fz57915l8aj73jwmrbrcl6fi58306ms27")))

(define-public crate-myfind-0.1.2 (c (n "myfind") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "0si0ml141zwv2vgl0yzbmk0pv9q3lirmwfb9i741vjbadmx15pag")))

(define-public crate-myfind-0.1.3 (c (n "myfind") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pbw3n82x78qr1kh1y7d0w2mh4c7f66kjrbpipmd9jaaqc56z6rn")))

(define-public crate-myfind-0.1.4 (c (n "myfind") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "03yzr2bcb3mb95bpqnnn2bhv43wy13f8my48dy6zkjmafni24b9h")))

(define-public crate-myfind-0.2.0 (c (n "myfind") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.8.1") (d #t) (k 0)))) (h "02d4c0fmm4lspj0nrljjp4dbvzafgcy7h7i42i3j7dq7i4r777hz")))

