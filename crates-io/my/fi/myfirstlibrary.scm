(define-module (crates-io my fi myfirstlibrary) #:use-module (crates-io))

(define-public crate-myfirstlibrary-0.1.0 (c (n "myfirstlibrary") (v "0.1.0") (h "0sbnvvgq0xz4zc7qk11d3qi35zm64lnlsrycp1m1bfclz2vy8q0g")))

(define-public crate-myfirstlibrary-0.1.2 (c (n "myfirstlibrary") (v "0.1.2") (h "1k6v7xfi6d1xpvbh1r0yqj1nzmrbn6b1yskvyc83zssbvxs6awn7")))

