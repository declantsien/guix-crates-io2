(define-module (crates-io my co mycodee-project_manager) #:use-module (crates-io))

(define-public crate-mycodee-project_manager-0.1.0 (c (n "mycodee-project_manager") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1cd7zin79n58s304gz9n4mnw3cs0rdl2bb91iha08y44w3brrsfr")))

(define-public crate-mycodee-project_manager-0.1.1 (c (n "mycodee-project_manager") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "05cyw7165900ps4xfl0bb9wm0j2c9dx11d2qrijskrsgqgla5na1")))

