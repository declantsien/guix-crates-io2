(define-module (crates-io my co mycon) #:use-module (crates-io))

(define-public crate-mycon-0.1.0 (c (n "mycon") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0k3r1rny943f7fqrki64ipddqhmp40dk229g13nin5d34vy2hvjf")))

(define-public crate-mycon-0.2.0 (c (n "mycon") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0wfbmg0khyhg89jh4qlcm482cipz5z3j2zmk0nfbcwg23qpkqmvh")))

