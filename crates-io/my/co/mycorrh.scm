(define-module (crates-io my co mycorrh) #:use-module (crates-io))

(define-public crate-mycorrh-0.1.0 (c (n "mycorrh") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c1zbp96pbkqlvr6icdd9pk6avkjvgy3mwgavfdbwh9gc6pigxzj")))

(define-public crate-mycorrh-0.1.1 (c (n "mycorrh") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f31j707cazf4i70fg19riaszcswf7xqmqfwag787w7v7l9f35p3")))

