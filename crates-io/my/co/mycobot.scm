(define-module (crates-io my co mycobot) #:use-module (crates-io))

(define-public crate-mycobot-0.1.0 (c (n "mycobot") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1zy28fc1zbfrfwad89d62irybk67kpvcn7lrgjn7m1mnsbwjb1v9")))

(define-public crate-mycobot-0.2.0 (c (n "mycobot") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1bv6xh0xqha7522pbp35skim88qcz3dv4l888nqxf0kg14mjcr95")))

(define-public crate-mycobot-0.3.0 (c (n "mycobot") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "15xcw58l1l3wn4gf1w0f0z5rrdzki352g1zap4g9d7fnvd47bsmp")))

(define-public crate-mycobot-0.4.0 (c (n "mycobot") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "1i4vk7kjjpd16an0c43zjg4dpc72gfgrmfm7id45xamgf28n4zhq")))

