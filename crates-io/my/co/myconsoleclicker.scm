(define-module (crates-io my co myconsoleclicker) #:use-module (crates-io))

(define-public crate-MyConsoleClicker-0.1.0 (c (n "MyConsoleClicker") (v "0.1.0") (h "0jxcxvgq8305nip9gsl309in4azd9azppkjxyq2zm73w4jgqxd0h")))

(define-public crate-MyConsoleClicker-0.1.1 (c (n "MyConsoleClicker") (v "0.1.1") (h "1hhkv0bhgq54fz5x7i2spp2xmdjws3p55mwslvvk9id6lz208fdm")))

