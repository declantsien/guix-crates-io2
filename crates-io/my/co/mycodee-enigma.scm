(define-module (crates-io my co mycodee-enigma) #:use-module (crates-io))

(define-public crate-mycodee-enigma-0.1.0 (c (n "mycodee-enigma") (v "0.1.0") (h "0h0iisxg0pqf952nwny59h1461l3pf5pz7lji1lmsdj8h858a44k") (y #t)))

(define-public crate-mycodee-enigma-0.2.0 (c (n "mycodee-enigma") (v "0.2.0") (h "0rh4ypdyhl4w8hvb6bx21q26kpifdf68cfh9397cxrbiq5ifa4ar") (y #t)))

(define-public crate-mycodee-enigma-0.3.0 (c (n "mycodee-enigma") (v "0.3.0") (h "1jdy84zv0j1ik87dw3wcqjy0qw7fhv2ic8z8nn4yc7rgjxq1v6bx") (y #t)))

