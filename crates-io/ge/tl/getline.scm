(define-module (crates-io ge tl getline) #:use-module (crates-io))

(define-public crate-getline-0.1.0 (c (n "getline") (v "0.1.0") (h "0h8x70qz0xxikwynkh07qj14ywjn04lm0vkyhwjri9c0nr3qw45c")))

(define-public crate-getline-0.1.1 (c (n "getline") (v "0.1.1") (h "05c2h1p035mwwwcv045iya8qywr41k49l7wdvcv3vkh8xrgfx2fm")))

