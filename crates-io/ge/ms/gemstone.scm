(define-module (crates-io ge ms gemstone) #:use-module (crates-io))

(define-public crate-gemstone-0.1.0 (c (n "gemstone") (v "0.1.0") (d (list (d (n "gem-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0jx4dnqjjv33lpi2bz6fb2kjhcs5icrnhf5z4rg696qm61q99kd7")))

(define-public crate-gemstone-0.2.0 (c (n "gemstone") (v "0.2.0") (d (list (d (n "gem-macros") (r "^0.1.0") (d #t) (k 0)))) (h "124cfxlyxq4463p7hjpjkkjkv006da0233qir9cqq08f76dwyf7m")))

(define-public crate-gemstone-0.3.0 (c (n "gemstone") (v "0.3.0") (d (list (d (n "gem-macros") (r "^0.1.0") (d #t) (k 0)))) (h "13cn6nja680105adwplrgsv993hs2anv8kb4gsdkfl5l1rh15m4d")))

(define-public crate-gemstone-0.3.1 (c (n "gemstone") (v "0.3.1") (h "09ywy6wv07wr0phn5shav118k5fxqk0rdjcadqv1gabypzylfbc1")))

(define-public crate-gemstone-0.4.1 (c (n "gemstone") (v "0.4.1") (h "02pm4w1489jhiak1169x2hvj1apmw8nd9j0phkhg3437d5lzisdy")))

(define-public crate-gemstone-0.4.2 (c (n "gemstone") (v "0.4.2") (h "13bjvhxna68xn9258npgbyysid482646z05fhnxjkfr3j1w0693z")))

(define-public crate-gemstone-0.4.3 (c (n "gemstone") (v "0.4.3") (h "1wfdylwz3v8bb6xwyafwqf92jxkipxd1v5wg052ag8kw52lr7952")))

(define-public crate-gemstone-0.4.4 (c (n "gemstone") (v "0.4.4") (h "1mf66qrfk611qks7caca3w87k9pmk032hhavi9n5bczla0nlpl2g")))

(define-public crate-gemstone-0.4.5 (c (n "gemstone") (v "0.4.5") (h "1rf0qab94gb032yqiv08yxwjji2hswqycxlnnr6nvdjpr8rcml7p")))

