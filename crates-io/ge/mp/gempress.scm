(define-module (crates-io ge mp gempress) #:use-module (crates-io))

(define-public crate-gempress-0.1.0 (c (n "gempress") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1shi8ys0sim1kpbv5v6v0r0hkhy69s6q670wk5xxr6vbcaqmibyw")))

(define-public crate-gempress-0.1.1 (c (n "gempress") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.2.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0dhaafdml59v3fjc7pfifv36gj8d5i0d4735zzrmikmm69hrqk0j")))

