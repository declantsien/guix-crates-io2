(define-module (crates-io ge og geogetter) #:use-module (crates-io))

(define-public crate-geogetter-0.1.0 (c (n "geogetter") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11q48z1sc4l7abq24yh2r669jm9j83hpv6z9i3kll2w6k0s0ak9a")))

(define-public crate-geogetter-0.1.1 (c (n "geogetter") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p02j9f3l1six20myzs65iwmzq5b2nllh0krsrbg225gcyn664jx")))

(define-public crate-geogetter-0.1.2 (c (n "geogetter") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15ihjziqy6yc30i8cai5bdyrdbmcs8ivrd959ygg65h7vni89f71")))

(define-public crate-geogetter-0.1.3 (c (n "geogetter") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i5bxcs6ajfi14q2wdnh3i0r1n2jlla1nrvnmdqc8w5sl821lxhh")))

(define-public crate-geogetter-0.1.4 (c (n "geogetter") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0npd9jaj9spgb9vzsaaa1yrnyhskrjl5xcrh6zyr8lly6rkq70li")))

(define-public crate-geogetter-0.1.5 (c (n "geogetter") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13nrlch7gjfsmqynimqgfid4plvd7mc7bnvis56jvs4lhj7djkyp")))

(define-public crate-geogetter-0.1.6 (c (n "geogetter") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3nnm6w4jnkkcnf72sv6rjs480f04gbvfsiqzdr5bm9x9gnrnyy")))

(define-public crate-geogetter-0.1.7 (c (n "geogetter") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1141v330f1apdhw4wymwz3dpr83brxakrw6whbnc51zddavkwic3")))

(define-public crate-geogetter-0.1.8 (c (n "geogetter") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0937b9v8jlqh94p02fd61xamh1fvbln86928jwm5by3q96iz56ji")))

