(define-module (crates-io ge og geographiclib-rs) #:use-module (crates-io))

(define-public crate-geographiclib-rs-0.1.0 (c (n "geographiclib-rs") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1df3y7x24rdnnam6mzj5blb449ivsqbv6pmsk0xzgaizj8vrzgyr")))

(define-public crate-geographiclib-rs-0.2.0 (c (n "geographiclib-rs") (v "0.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qd4gn8c2s82a30y642qc0d7h7i6vys0q5x8h90jryk8v3aj13mp")))

(define-public crate-geographiclib-rs-0.2.1 (c (n "geographiclib-rs") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12famzbfx7q5s2zf4izfy29h16rc3ikq89rxfqv7gjjn33f3rggx") (f (quote (("test_short") ("test_full"))))))

(define-public crate-geographiclib-rs-0.2.2 (c (n "geographiclib-rs") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07hx3m5khpygiky8zh7dafr78c5i3fc7781ijg9d2hwzx74fz904") (f (quote (("test_short") ("test_full"))))))

(define-public crate-geographiclib-rs-0.2.3 (c (n "geographiclib-rs") (v "0.2.3") (d (list (d (n "accurate") (r "^0.3") (o #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1glj4p22il2hnvif3wyksj0qlyjm659a7pqyl2k4qsiwppkh9a4f") (f (quote (("test_short") ("test_full") ("default" "accurate"))))))

(define-public crate-geographiclib-rs-0.2.4 (c (n "geographiclib-rs") (v "0.2.4") (d (list (d (n "accurate") (r "^0.3") (o #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geographiclib") (r "^0.1.0") (d #t) (k 2)) (d (n "libm") (r "^0.2.8") (k 0)))) (h "0q0rg3wm4q06qqvhvp1dc5qkcwywnvnhm3ha9cip1708z22fvrg6") (f (quote (("test_short") ("test_full") ("default" "accurate")))) (r "1.70.0")))

