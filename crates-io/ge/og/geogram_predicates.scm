(define-module (crates-io ge og geogram_predicates) #:use-module (crates-io))

(define-public crate-geogram_predicates-0.1.0 (c (n "geogram_predicates") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1kz83s19nm68jkgzlivvn7vai7h2sknsvvixybcj21386jc6bi6g")))

(define-public crate-geogram_predicates-0.1.1 (c (n "geogram_predicates") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0apdbl79ivvjg2mgmqv38w5bskq2s2i3264jj0cfn9g0hi5iq8c0")))

(define-public crate-geogram_predicates-0.1.2 (c (n "geogram_predicates") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "19qr0xvpg1p6mvyga5f72vi43cykppsdg2nnd2719q2fncirqbg8")))

(define-public crate-geogram_predicates-0.1.3 (c (n "geogram_predicates") (v "0.1.3") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0avk0ch4dkvgzz1zrx1c2rcsnfdkm68vx5wz8igl498dh75cy2bi")))

(define-public crate-geogram_predicates-0.1.5 (c (n "geogram_predicates") (v "0.1.5") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)) (d (n "png") (r "^0.17.13") (d #t) (k 2)))) (h "00wjs6xw6mmaij04b2dxgrcbbcxqh1wiazvwyzclvgypbyg3wrsb")))

(define-public crate-geogram_predicates-0.1.6 (c (n "geogram_predicates") (v "0.1.6") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)))) (h "1j1nv1bbj0v7s3fjv69w5jlrn9j26kvapg3yhryj8328v3hndjpp")))

(define-public crate-geogram_predicates-0.1.7 (c (n "geogram_predicates") (v "0.1.7") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)))) (h "1mzabzh9mdnavsvvn7xw764nsprbds5bkf2824a69pmd21gzb0cp")))

(define-public crate-geogram_predicates-0.1.8 (c (n "geogram_predicates") (v "0.1.8") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "float_extras") (r "^0.1.6") (d #t) (k 2)))) (h "0904zw5yh0mwip41b5pzzhm1ljvk5v33palr9zq6c8lyy5rwkpcb")))

