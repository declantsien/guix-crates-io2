(define-module (crates-io ge ol geolocate_bin) #:use-module (crates-io))

(define-public crate-geolocate_bin-3.0.1 (c (n "geolocate_bin") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "geolocate_lib") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "08dzanq3qxcz69sjxlw2fq4i9ndrlq6g6v0qgkaw42c97pkl14wm") (y #t)))

