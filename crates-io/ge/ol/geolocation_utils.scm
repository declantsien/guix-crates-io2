(define-module (crates-io ge ol geolocation_utils) #:use-module (crates-io))

(define-public crate-geolocation_utils-0.1.0 (c (n "geolocation_utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1qnqvjf8j9bim03h9n1jn4056qby7jgr2xchsgiqwibrjycnvhjm") (f (quote (("serde"))))))

(define-public crate-geolocation_utils-0.1.1 (c (n "geolocation_utils") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0lfplrip6r7cyxrknjgifd28apxwdhzg1nxryf6mxpzv268nl2y0") (f (quote (("serde"))))))

(define-public crate-geolocation_utils-0.1.2 (c (n "geolocation_utils") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0mf30pzlmxc7kw1aagdwdmp7yjkwixfgljnby0ijvah326ajbwjc") (f (quote (("serde")))) (y #t)))

(define-public crate-geolocation_utils-0.1.3 (c (n "geolocation_utils") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "02sq52k0kvfxbzlh5drlpyw7sr456kmwzj1cz30989mijq3355nz") (f (quote (("serde" "serde/derive"))))))

(define-public crate-geolocation_utils-0.1.4 (c (n "geolocation_utils") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1l0m71jiq8kka7yjck5rnkc7gxwj632vckk1y25mjzmhi7ddlaxf") (f (quote (("serde" "serde/derive")))) (y #t)))

(define-public crate-geolocation_utils-0.1.5 (c (n "geolocation_utils") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "04fa1x8p6a0gfyg6axg9cd969zx3g8mhmivv75dih681830dacs5") (f (quote (("serde" "serde/derive"))))))

(define-public crate-geolocation_utils-0.2.0 (c (n "geolocation_utils") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0v04qib0h67zii02pd37fkqaw3il5wywzzwnjwdnkwf4dacx2zj4") (f (quote (("serde" "serde/derive"))))))

(define-public crate-geolocation_utils-0.2.1 (c (n "geolocation_utils") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "09l6kmjr5f14gp5w7w10bqjjj1whljviq2cqzrdh9z76hlz2yxk7") (f (quote (("serde" "serde/derive")))) (y #t)))

(define-public crate-geolocation_utils-0.2.2 (c (n "geolocation_utils") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.188") (d #t) (k 0)))) (h "0gx0z1v2wk7bq6i4lb1i6b0qiygh3l8f8rz3pmqm5ldx16pmci7z") (f (quote (("serde" "serde/derive"))))))

