(define-module (crates-io ge ol geolocation) #:use-module (crates-io))

(define-public crate-geolocation-0.1.0 (c (n "geolocation") (v "0.1.0") (d (list (d (n "isahc") (r "^1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i0bym4mv8ng7q72rqzf20jswvxi37mrm3adna31zcc8m03wqybc") (y #t)))

(define-public crate-geolocation-0.1.1 (c (n "geolocation") (v "0.1.1") (d (list (d (n "isahc") (r "^1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ga3r6fiiybzziw35msc8fsfi3fd0ndvir1s2amy9adx0rb9xd3v") (y #t)))

(define-public crate-geolocation-0.1.2 (c (n "geolocation") (v "0.1.2") (d (list (d (n "isahc") (r "^1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rrbyzi8pqfhaq2v8lyaz14m4pcp9yrl6kmp5rjyby9v77z48q4g") (y #t)))

(define-public crate-geolocation-0.2.0 (c (n "geolocation") (v "0.2.0") (d (list (d (n "isahc") (r "^1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qh4wla4k2swhimvf8b72a2llp3m707f70zc7jkaxvnwycqn0w40") (y #t)))

(define-public crate-geolocation-0.2.1 (c (n "geolocation") (v "0.2.1") (d (list (d (n "isahc") (r "^1.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l2sgrf3z925z75h4sadiny13ly7jir083gi5zdq1vnnlbw3f16h")))

