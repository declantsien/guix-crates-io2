(define-module (crates-io ge ol geologic) #:use-module (crates-io))

(define-public crate-geologic-0.0.1 (c (n "geologic") (v "0.0.1") (h "1nxbg3112ly4n5r4q8i8c0p8x2j1a0q3c4bvxx62q6aqkxqda81m")))

(define-public crate-geologic-0.0.2 (c (n "geologic") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1sh9951vckq0vqcz36dwh40bparqmb12kip7hw65ykiwyya732zg")))

(define-public crate-geologic-0.0.3 (c (n "geologic") (v "0.0.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "12qdzx9pmjaf1dyncsq70ycc8isfqb2bwjaz0wglrgmqpi5r57gn")))

