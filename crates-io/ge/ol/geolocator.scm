(define-module (crates-io ge ol geolocator) #:use-module (crates-io))

(define-public crate-geolocator-0.1.0 (c (n "geolocator") (v "0.1.0") (d (list (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y9vrh5kav67g9hmmsdsn611azxs7fgrzf3apmfsg6b8mjpsnbc3")))

(define-public crate-geolocator-0.1.1 (c (n "geolocator") (v "0.1.1") (d (list (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qh96avnrh8yr999mcbvpmgwm3q2w69ffy8m9vp1na73hiz7w711")))

(define-public crate-geolocator-0.2.0 (c (n "geolocator") (v "0.2.0") (d (list (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hg4ppziw53cssyz30dxw9fdazdkj1bzf0sz4swv2qakq1ddwv9f")))

(define-public crate-geolocator-0.2.1 (c (n "geolocator") (v "0.2.1") (d (list (d (n "ipgeolocate") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^1.19.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vklhfyfvppxaclifp8c6kzhjyn1gsyprjd4sndgcf8329lni4kh")))

