(define-module (crates-io ge dv gedv) #:use-module (crates-io))

(define-public crate-gedv-0.1.3 (c (n "gedv") (v "0.1.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r ">=0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v9lvpsbd8hqqcb5d661wa7mfz204rhav7f9x1yn2fj38725bycn") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gedv-0.1.4 (c (n "gedv") (v "0.1.4") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.53.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qnr6saqdybwjly9hdyk7h2vrlpa0xahfibkv8wfkblaklzndr3a") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gedv-0.2.0 (c (n "gedv") (v "0.2.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.53.0") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17x6af7in4b89jwj4rcgqkqph2xhw5vpdzkn4rz0q217g6c2wn2w") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gedv-0.2.1 (c (n "gedv") (v "0.2.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_Foundation" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hda2i8qqi9g1j3wc78hm578fdsydw2n7gk0pl2vf3sdkfpx2vg3") (s 2) (e (quote (("serde" "dep:serde"))))))

