(define-module (crates-io ge rl gerlib) #:use-module (crates-io))

(define-public crate-gerlib-0.1.0 (c (n "gerlib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.25") (d #t) (k 0)) (d (n "http") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "serde_url_params") (r "^0.2.0") (d #t) (k 0)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1yxcbhv5d8wdqvha03c529ji6xbn9lqd4v5sf24bspdkwzh6r1jz") (y #t)))

