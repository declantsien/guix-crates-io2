(define-module (crates-io ge en geen) #:use-module (crates-io))

(define-public crate-geen-0.0.1 (c (n "geen") (v "0.0.1") (h "1dgd2363a9gfzrrj8b5ghsp4is90n0xzxgsm9rgh75naljqybvgk")))

(define-public crate-geen-0.0.2 (c (n "geen") (v "0.0.2") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12w17pv39w7y3mgb4k9w889qg6g5lf5djwax8kxz74410zbxigck")))

(define-public crate-geen-0.0.3 (c (n "geen") (v "0.0.3") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.3.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mv9szlfjklfyljfs6gcfyjya7nbc0c6ifqqa0bd1p91axawkmsk")))

(define-public crate-geen-0.0.4 (c (n "geen") (v "0.0.4") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.3.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "097gpi837x01ndg4flih4fyfmxcjvvgq2kicyw0szbidjsq2w9a7")))

(define-public crate-geen-0.0.5 (c (n "geen") (v "0.0.5") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.3.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pbz5zbzi80zqs8wk3g3mlz29vyck3svh8dlphfgw01nqcbj5ajs")))

(define-public crate-geen-0.0.6 (c (n "geen") (v "0.0.6") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.3.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lhh92j3nbgvxgfsrmqcbhn795vwwn7751in7axnbdmpmyrq8h06")))

(define-public crate-geen-0.0.7 (c (n "geen") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "croaring") (r "^0.3.9") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1msxj6m6da7rxal4g9rzcgpjzsacznlc1gwmac55c2p84bx5rqah")))

