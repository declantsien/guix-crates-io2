(define-module (crates-io ge en geeny-api) #:use-module (crates-io))

(define-public crate-geeny-api-0.2.0 (c (n "geeny-api") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ny93aj5zwplvy73n1m3c2fm52y17dggjv2phxn22n1g69whfkfh")))

(define-public crate-geeny-api-0.2.1 (c (n "geeny-api") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "09czzn0va1w6d78sip1swkf655qsp90h99n6zmjv0bb4yi31vhh8")))

(define-public crate-geeny-api-0.2.2 (c (n "geeny-api") (v "0.2.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0a303b2ha29f2nvwg8dr9i5q2x3r8imizx9ln4crpabj8h136y9w")))

(define-public crate-geeny-api-0.3.0 (c (n "geeny-api") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1z559wg43v55ddzi384r2i7giyv3v311741bzc6snnr3k8h80n86")))

