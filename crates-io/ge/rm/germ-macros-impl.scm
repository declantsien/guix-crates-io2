(define-module (crates-io ge rm germ-macros-impl) #:use-module (crates-io))

(define-public crate-germ-macros-impl-0.1.0 (c (n "germ-macros-impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "1xmnbxblg5qingawxh8z76v4412rdqcamvvxxlyg2gw4v23n36hn")))

(define-public crate-germ-macros-impl-0.1.1 (c (n "germ-macros-impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0z6vjwzjrhjghflblmyln1j0ys4mqgvwaq9djmrg9kvnl00rlz9z")))

(define-public crate-germ-macros-impl-0.1.2 (c (n "germ-macros-impl") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)))) (h "0xsh1nbffg97rx62ks31bwv0s1z66g4zys1n0c3cjpx1z2zyp1l1")))

