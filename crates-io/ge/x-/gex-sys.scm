(define-module (crates-io ge x- gex-sys) #:use-module (crates-io))

(define-public crate-gex-sys-0.1.0 (c (n "gex-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0iwwhz37f6qsqa3p1givmsfxfhkiyrjgikz5bd4zs807gcl6sbja") (f (quote (("udp") ("mpi") ("ibv") ("default" "udp"))))))

