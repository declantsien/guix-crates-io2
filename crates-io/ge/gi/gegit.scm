(define-module (crates-io ge gi gegit) #:use-module (crates-io))

(define-public crate-gegit-1.0.0 (c (n "gegit") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pndpacx7wq8jgqajcxgvzbqai3fg7jyc3nfpq8r59v8s6dp058i")))

(define-public crate-gegit-1.1.0 (c (n "gegit") (v "1.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jkm1npmvfl1bgnx02v8la8mfizp3vb2w6xjyxgk37bf0r4d4ci6")))

