(define-module (crates-io ge oi geoint) #:use-module (crates-io))

(define-public crate-geoint-0.1.0 (c (n "geoint") (v "0.1.0") (d (list (d (n "geo") (r "^0.15.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.12.3") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1rk4n8lv23d32fnxvs3f0ixry2bd4m8cihgpkpf4ig0p2gybhf8l")))

