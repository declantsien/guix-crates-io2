(define-module (crates-io ge oi geoiplocation) #:use-module (crates-io))

(define-public crate-geoiplocation-0.2.0 (c (n "geoiplocation") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "185hnk70n4ccyf52yljd7dbzwbgk4fsmyajmxjkw41xb0s8yqmw6") (y #t)))

(define-public crate-geoiplocation-0.3.0 (c (n "geoiplocation") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08i7p0alsjlcccaj8pgfcwpk2ifldrmswqz9zm9s7yj6bkjf44nj")))

