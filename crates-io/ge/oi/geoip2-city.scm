(define-module (crates-io ge oi geoip2-city) #:use-module (crates-io))

(define-public crate-geoip2-city-0.1.0 (c (n "geoip2-city") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (o #t) (d #t) (k 0)))) (h "1r0n0zxhznr3i408i0y3q9cyq6rsq1ci91ppp18nqfdgxl8jss2x") (f (quote (("serde_support" "serde/derive"))))))

