(define-module (crates-io ge oi geoip-sys) #:use-module (crates-io))

(define-public crate-geoip-sys-0.0.2 (c (n "geoip-sys") (v "0.0.2") (h "1lnnlrgzaxbfj27xiiymqna5apk1xkifv5mdysanjr34693qg3d3")))

(define-public crate-geoip-sys-0.0.3 (c (n "geoip-sys") (v "0.0.3") (h "1cnahmmkhg7rsc5c4p3pksy0927a7xwa1lnmwgqv26hqclwac8xr")))

(define-public crate-geoip-sys-0.0.4 (c (n "geoip-sys") (v "0.0.4") (h "0ir0ciyx2yscxd9mkxp1wl4b3s30h8gzkn4jjqrdwf1g8q5zn2mq")))

(define-public crate-geoip-sys-0.0.5 (c (n "geoip-sys") (v "0.0.5") (h "165i7jhgx57cl980rslvgynygwjqrfk7i80n4x5lfai1krzms8zm")))

(define-public crate-geoip-sys-0.0.6 (c (n "geoip-sys") (v "0.0.6") (h "0p9k1z9q00xnz10gf5c726pvk9pwhnp2anmfymkb0na8hjav35v9")))

(define-public crate-geoip-sys-0.0.7 (c (n "geoip-sys") (v "0.0.7") (h "0l36an7rs1hl90rvbhn5sdwjrlckhzd832axcvlia2nnqzz0k0ir")))

(define-public crate-geoip-sys-0.0.8 (c (n "geoip-sys") (v "0.0.8") (h "1b69psr956w4ffdbl0b8b51py58i648hr2hv50k340mbwfq80p7n")))

(define-public crate-geoip-sys-0.0.9 (c (n "geoip-sys") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0mjk2562bwlmljys1mckapwzhk5dwxsw721yxqidniprs0asydrf")))

(define-public crate-geoip-sys-0.0.10 (c (n "geoip-sys") (v "0.0.10") (d (list (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "1y34r1vs7s5dz85cbk0zcgzzf533234zqh5v7mx96xqhq0jwpiir")))

(define-public crate-geoip-sys-0.0.12 (c (n "geoip-sys") (v "0.0.12") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)))) (h "1y35mb7xahw2vj51ljpyz6j0hmvnbr8fn3v0bw9g13mwl39gqlkd")))

(define-public crate-geoip-sys-0.0.13 (c (n "geoip-sys") (v "0.0.13") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1hhpq48z6w0a0p1n93qsmhanmq0qs9abl9aymdmlcmxsaq6idpbr")))

(define-public crate-geoip-sys-0.0.14 (c (n "geoip-sys") (v "0.0.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cycbqkwihchzyzhlhkjllq8j8zfvvk7l85ggvdmfsxpdjz5cmhb")))

