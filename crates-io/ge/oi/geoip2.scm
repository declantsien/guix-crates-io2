(define-module (crates-io ge oi geoip2) #:use-module (crates-io))

(define-public crate-geoip2-0.0.1 (c (n "geoip2") (v "0.0.1") (d (list (d (n "geoip2-codegen") (r "^0.0.1") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "1d1g3070xcq1sc96dsww3bm3bfyh11lngss3070i6sdj0ng8y0bi")))

(define-public crate-geoip2-0.1.0 (c (n "geoip2") (v "0.1.0") (d (list (d (n "geoip2-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "0za586yd65a0klzqpz7ljsqsymwwz8f446pzkdc21cr5cm9z4z10")))

(define-public crate-geoip2-0.1.1 (c (n "geoip2") (v "0.1.1") (d (list (d (n "geoip2-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "01m6hz6s1mk3rw3gna2qffvb5xvn0f2aaq0vxcphx8xv8b1dmc2m")))

(define-public crate-geoip2-0.1.2 (c (n "geoip2") (v "0.1.2") (d (list (d (n "geoip2-codegen") (r "^0.1.2") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "0kg6pdgvmpachsp29rfplkwr3gfjd7hk919rl2iyla38gzpa028h")))

(define-public crate-geoip2-0.1.3 (c (n "geoip2") (v "0.1.3") (d (list (d (n "geoip2-codegen") (r "^0.1.2") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "1psfmkhjv42x5dsmlq6g8j4x26kjwi76qfrg1f8i17bn4x26ji6f") (f (quote (("unsafe-str") ("default"))))))

(define-public crate-geoip2-0.1.4 (c (n "geoip2") (v "0.1.4") (d (list (d (n "geoip2-codegen") (r "^0.1.4") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "009x4p42p2byx89v3qrx38ssxd7qyn0689958w1aijzs5izv5940") (f (quote (("unsafe-str") ("default"))))))

(define-public crate-geoip2-0.1.5 (c (n "geoip2") (v "0.1.5") (d (list (d (n "geoip2-codegen") (r "^0.1.4") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "0p7wjyc53fq6n7krnl9yd37yc52bl69nfsf65k7lnvg3dgcvz74l") (f (quote (("unsafe-str") ("default"))))))

(define-public crate-geoip2-0.1.6 (c (n "geoip2") (v "0.1.6") (d (list (d (n "geoip2-codegen") (r "^0.1.4") (d #t) (k 0)) (d (n "maxminddb") (r "^0.21.0") (d #t) (k 2)))) (h "1i18295w24l6d3vx7afpkfqkwjzcqmw5llwywp92jas4khafxj0a") (f (quote (("unsafe-str") ("default"))))))

(define-public crate-geoip2-0.1.7 (c (n "geoip2") (v "0.1.7") (d (list (d (n "geoip2-codegen") (r "^0.1.5") (d #t) (k 0)) (d (n "maxminddb") (r "^0.24.0") (d #t) (k 2)))) (h "1xmngggy93fhp1w5pnd41pgairwfvs3ivjyhziaxd9b4a4xr0535") (f (quote (("unsafe-str") ("default"))))))

