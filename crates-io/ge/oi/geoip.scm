(define-module (crates-io ge oi geoip) #:use-module (crates-io))

(define-public crate-geoip-0.0.1 (c (n "geoip") (v "0.0.1") (h "0fwiclm89d1npb7gcknphas9ff4y5qx7wm9rb28hxn7l4wkld6l0")))

(define-public crate-geoip-0.0.2 (c (n "geoip") (v "0.0.2") (h "18ygf6mbbhy8d35vkc66d26sfh4zgc1ffw7mn614ji9gzh20ii0m")))

(define-public crate-geoip-0.0.3 (c (n "geoip") (v "0.0.3") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)))) (h "1gd049davy2livxiak33r2wd8dvr21hfdjkiiacx72nfkbggxh3q")))

(define-public crate-geoip-0.0.4 (c (n "geoip") (v "0.0.4") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1w5x8www65ybv87jzkgz7x2bzm11vrgzpv5rmb9gcfqsk3ligfz3")))

(define-public crate-geoip-0.0.5 (c (n "geoip") (v "0.0.5") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1d0wjnk9058yydm4larn7kbh4jn0f34fxjnd6k2raf5lbd6sfw2h")))

(define-public crate-geoip-0.0.6 (c (n "geoip") (v "0.0.6") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0shbca1rhwd6msbhiyb12zdhk51ll1jz11ash5ygl2f40l1gllkd")))

(define-public crate-geoip-0.0.7 (c (n "geoip") (v "0.0.7") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0a8gh8wx0jyr9v5h36k3zh9kprzz2zdbr0wblrg81bjpgw5p4jqx")))

(define-public crate-geoip-0.0.8 (c (n "geoip") (v "0.0.8") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1v1m3x5jw729zxh46nsp24k0frfss395l9gbpgikallx09xdzgp2")))

(define-public crate-geoip-0.0.9 (c (n "geoip") (v "0.0.9") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dnvf846y4v9pbsdgk42w85jb3wz0mdzdxjl4i116n8dfrmdjnkq")))

(define-public crate-geoip-0.0.10 (c (n "geoip") (v "0.0.10") (d (list (d (n "geoip-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0rg4xgvghzh5kgvm86nyy6fv8j8c2ryfiq5n4gadp532v8c94ax9")))

(define-public crate-geoip-0.0.11 (c (n "geoip") (v "0.0.11") (d (list (d (n "geoip-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "110vm83fnqiv0wrbiqjqzjcsmvqgsi2xq3krnbxaz85a995569mn")))

(define-public crate-geoip-0.0.12 (c (n "geoip") (v "0.0.12") (d (list (d (n "geoip-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0w9mgkydmv4pdz4mhsq903vsyrqf389kpgryafq8d8l1d2c4cjlw")))

(define-public crate-geoip-0.0.13 (c (n "geoip") (v "0.0.13") (d (list (d (n "geoip-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0m480vgc98h64n7d2d830p8ajha6jkk4s56s5av3gd350kvrlj62")))

(define-public crate-geoip-0.0.14 (c (n "geoip") (v "0.0.14") (d (list (d (n "geoip-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "00svj720ig2k0frng9glzg9iinapf0fj4p834a6l6nxmx0044yzi")))

