(define-module (crates-io ge oi geoip2-codegen) #:use-module (crates-io))

(define-public crate-geoip2-codegen-0.0.1 (c (n "geoip2-codegen") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyyclxfanwwc1pfspsmdd1xx1m747mlixj7flbaifb36w20b565")))

(define-public crate-geoip2-codegen-0.1.0 (c (n "geoip2-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full"))) (d #t) (k 0)))) (h "147arhncak9lq3l4zzr2p1d82273jg4is7fbwfjvgbrfxsz9ki5n")))

(define-public crate-geoip2-codegen-0.1.1 (c (n "geoip2-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02w03s2rivb7r50dm8i7djss3fglr1sii3l97f20z4mflkb95kws")))

(define-public crate-geoip2-codegen-0.1.2 (c (n "geoip2-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0aq7h5fxwfd2k6i1yffdv6c5nzxhr5y0dr00wmm4x2fdcqg08s47")))

(define-public crate-geoip2-codegen-0.1.3 (c (n "geoip2-codegen") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lh04z9l6gc3q2izyw70xv07zzs3ka2c30dii6156i9v104vvqi5")))

(define-public crate-geoip2-codegen-0.1.4 (c (n "geoip2-codegen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0179wgqmc0f1s3z06xdi758fbw56wsl4w69p800hk9nzyrf9g04x")))

(define-public crate-geoip2-codegen-0.1.5 (c (n "geoip2-codegen") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jv9qmsvj1yb0rx21rg7csm42fhl34x4a8i9nryhbqz4xdcaqb0z")))

