(define-module (crates-io ge on geonode) #:use-module (crates-io))

(define-public crate-geonode-0.1.0 (c (n "geonode") (v "0.1.0") (h "0lsqaikd1clcd95fwdw57mqk9dd4p8xgnv6dczdv0kji5xyn993m")))

(define-public crate-geonode-0.1.1 (c (n "geonode") (v "0.1.1") (h "0i8cf4kibplrxwvh5w7mxvm0dgx8q2xrmgrn5rz6fxy3zzvmdvgz")))

