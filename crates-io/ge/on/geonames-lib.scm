(define-module (crates-io ge on geonames-lib) #:use-module (crates-io))

(define-public crate-geonames-lib-0.1.0 (c (n "geonames-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1py5rvxv07ilsgb9hv5d2bnpincq4cal7w3knq4zswi2hrmn8ci9")))

(define-public crate-geonames-lib-0.2.0 (c (n "geonames-lib") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1495mf66m052cyw61pz9mqcgc4fb08zn2xc34yhy4r861jwnw8s5")))

(define-public crate-geonames-lib-0.3.0 (c (n "geonames-lib") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zzidfyfm520iy6d1kf3s0y2jg8njas0h5w2adzr7jy61a56xbqn")))

