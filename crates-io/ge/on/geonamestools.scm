(define-module (crates-io ge on geonamestools) #:use-module (crates-io))

(define-public crate-geonamestools-0.1.0 (c (n "geonamestools") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "000my4gby11f0izkrv1wbs8i1rr7gc1hbsp1q6rdvv7zn7x2p289")))

