(define-module (crates-io ge ti geticons) #:use-module (crates-io))

(define-public crate-geticons-0.1.0 (c (n "geticons") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "0kycfc5mik6510i2lhdvdnl52xq75prvdhw9a0ayzdyf9lbcpbiz")))

(define-public crate-geticons-1.0.0 (c (n "geticons") (v "1.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "0i94nlpj6iqh9cci633661y8nw6bwzjcn2bdmsfy2pr590b5ifhn")))

(define-public crate-geticons-1.1.0 (c (n "geticons") (v "1.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "08259l7sg9rfsa9g18wk1d1l9zsra68y1y89411vjfg9bjzxgm84")))

(define-public crate-geticons-1.2.0 (c (n "geticons") (v "1.2.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "1q70j75cppg8z9cyfcpqgxrfazzcwn4xxhgxd5864s98dghvx4pf")))

(define-public crate-geticons-1.2.1 (c (n "geticons") (v "1.2.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (k 0)))) (h "1qmmfp41sznblqjlp0x8697vvdzzqf3va4ls841xwnccg2w0rgmy")))

(define-public crate-geticons-1.2.2 (c (n "geticons") (v "1.2.2") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "linicon") (r "^2.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (k 0)))) (h "0pim6wn0yg6dva7yjcyhxgj2saa6vahw8mwpvly3zkqxh9v3g2kv")))

