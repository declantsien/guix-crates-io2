(define-module (crates-io ge ti getignore) #:use-module (crates-io))

(define-public crate-getignore-0.1.0 (c (n "getignore") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)))) (h "1dzx5qhx0nj7b6vvx2swa4zj5hywjazr9q4drkbzsf69b3vrcly2") (y #t)))

(define-public crate-getignore-0.1.1 (c (n "getignore") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)))) (h "1agv7r8gkl39bpacy0gl0m9car6j7nkm7g1gnz2bk08500ww1mq6") (y #t)))

(define-public crate-getignore-0.1.2 (c (n "getignore") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)))) (h "1hgsjlaqjra2jxwy9sis92c5xdbjsi1wmmrlvki5f3h9lh29p44r") (y #t)))

