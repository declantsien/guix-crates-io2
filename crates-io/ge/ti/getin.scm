(define-module (crates-io ge ti getin) #:use-module (crates-io))

(define-public crate-getin-0.1.0 (c (n "getin") (v "0.1.0") (h "0szgj7b2k982yxyx6k15js3as400vqxxxfg8qcm11spfddbv99hb")))

(define-public crate-getin-0.1.1 (c (n "getin") (v "0.1.1") (h "0p4h78wddvmhrmiziqvydvdwfn40wy40cjic5y7c4r05z2ihgp4a")))

