(define-module (crates-io ge ti getid) #:use-module (crates-io))

(define-public crate-getid-0.1.0 (c (n "getid") (v "0.1.0") (d (list (d (n "cuid") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0zh0n5wky39v3v05f7qixfjmgn0famz2hxdras8p7gxgmrym0gli")))

(define-public crate-getid-0.2.0 (c (n "getid") (v "0.2.0") (d (list (d (n "cuid") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lah7m7wb3zhiqk5ay64ivxpml4s83ij0s3imdfl4qhg7k03v0fl")))

(define-public crate-getid-0.3.0 (c (n "getid") (v "0.3.0") (d (list (d (n "cuid") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dd9xs87kjm3fxkh13ik8m4h0gz0bcfxcddjcnwvb9zrzfvnys9h")))

(define-public crate-getid-0.4.0 (c (n "getid") (v "0.4.0") (d (list (d (n "cuid") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1i9cynrl60r1lxs8nzyvhjw7awiiq94a528xfvhjln9pav097wgp")))

(define-public crate-getid-0.4.1 (c (n "getid") (v "0.4.1") (d (list (d (n "cuid") (r "^1.2.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (f (quote ("eq-separator"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lapp56aisw9zy61ycr8r5y6bg9pszj2g8javc3cc25vsv3q9pmr")))

