(define-module (crates-io ge t_ get_files_macro) #:use-module (crates-io))

(define-public crate-get_files_macro-0.1.0 (c (n "get_files_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0s1kk66rb1njda3dm0vycfs5wls1s8y3hmzxbfl15b2zmq997ggl") (y #t)))

(define-public crate-get_files_macro-0.1.1 (c (n "get_files_macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "05iwsyq21arw6f9y9wbq9j0s17y77zsg6a1fvvwd54ka359hj1im") (y #t)))

(define-public crate-get_files_macro-0.1.2 (c (n "get_files_macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "09q0r5gsqibzm2clfjfflyyazwc4wr017a6i2jp1z3a5fx1jc164")))

(define-public crate-get_files_macro-0.1.3 (c (n "get_files_macro") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "00hn7gsrg3pl9afj4y9x94lf9x0makbl2ldyh5jq61bdx3x443cp")))

