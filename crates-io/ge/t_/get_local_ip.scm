(define-module (crates-io ge t_ get_local_ip) #:use-module (crates-io))

(define-public crate-get_local_ip-0.1.0 (c (n "get_local_ip") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1sh8anbj8gc1l866nvs4q1l72ygfn0b69mh1w97r4iszz92isdb6")))

(define-public crate-get_local_ip-0.1.1 (c (n "get_local_ip") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "12642xd9bqhbvknb9bx8wk6c4144lywqd5ilsir9vxkgbb2f0kg4")))

(define-public crate-get_local_ip-0.1.2 (c (n "get_local_ip") (v "0.1.2") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1dhl6msa9fcivcicjsmmf86a2r6l9z553lc4k3nax76l5kqii6aa")))

(define-public crate-get_local_ip-0.1.3 (c (n "get_local_ip") (v "0.1.3") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0rdxymr9487dsil4bwhjz3sh1x40sc4nsssr7j34c7i549zxm1nq")))

(define-public crate-get_local_ip-0.1.4 (c (n "get_local_ip") (v "0.1.4") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1v6pn6cwbbdcwxd3g5srjg7bn3f6x0kx2g8gqwkwqvnyw698ykz7")))

(define-public crate-get_local_ip-0.1.5 (c (n "get_local_ip") (v "0.1.5") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1qnw1lz5q01ha6yrlm9mv1cy12ns9w5gjr5fjnyc737akpi567z7")))

(define-public crate-get_local_ip-0.1.6 (c (n "get_local_ip") (v "0.1.6") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0lp4gbgz57ddsv1g13psdwar9j16iwn5a4cs7m7m8vyx1dbbbj9z")))

(define-public crate-get_local_ip-0.1.7 (c (n "get_local_ip") (v "0.1.7") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0d1nv6n1gd0lixlzwmckxrivzxiyjj8v5z95fpq1l58yzxi04vxb")))

(define-public crate-get_local_ip-0.1.8 (c (n "get_local_ip") (v "0.1.8") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1wq3dqr7n8kp12v6mc9mz961h22hkqb31wzflb9flldcmh547v2l")))

(define-public crate-get_local_ip-0.1.9 (c (n "get_local_ip") (v "0.1.9") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1cnvi6r6jdim6y9xpn4dj62hsmkhqnbiqjf0bmscm6glqsimy8v3")))

(define-public crate-get_local_ip-0.2.0 (c (n "get_local_ip") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0lllzxy8rb1phy8mz5vvgxknq78sz1rkslc2dabq7r8f76k1p5sf")))

(define-public crate-get_local_ip-0.2.1 (c (n "get_local_ip") (v "0.2.1") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1jw3jm6hrgcn55d3waxcn1zlc869q39zwqibr8jndr0mzmpsxcja")))

