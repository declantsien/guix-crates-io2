(define-module (crates-io ge t_ get_ftc) #:use-module (crates-io))

(define-public crate-get_ftc-0.0.0 (c (n "get_ftc") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i5qs2b151dhzjaqg1rfpni68zz44yrac5l6wwc0ldkh2560q5i0")))

