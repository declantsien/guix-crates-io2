(define-module (crates-io ge t_ get_if_addrs-sys) #:use-module (crates-io))

(define-public crate-get_if_addrs-sys-0.1.0 (c (n "get_if_addrs-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2.28") (d #t) (k 0)))) (h "02fmm0p5x8c06axlsiaccp1h9rvksgiqbkw39vz2spw0j7b8xxkw")))

(define-public crate-get_if_addrs-sys-0.1.1 (c (n "get_if_addrs-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "~0.2.28") (d #t) (k 0)))) (h "0j5bypizbk59jhkaw1abkx7ydj79pplfvwq03hcnpwvcfkxzj10d") (l "ifaddrs")))

