(define-module (crates-io ge t_ get_user_agent) #:use-module (crates-io))

(define-public crate-get_user_agent-0.1.0 (c (n "get_user_agent") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01ypgvhyn970nn35cc2kw1d0iy4x6bq5y3f0675ldxll42n0sr98")))

(define-public crate-get_user_agent-0.1.1 (c (n "get_user_agent") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bzi0ahdiqdc524g162lzyfz6qrs5l0sj02an58jq7q0jypcfgnw")))

