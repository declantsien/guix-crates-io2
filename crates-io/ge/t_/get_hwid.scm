(define-module (crates-io ge t_ get_hwid) #:use-module (crates-io))

(define-public crate-get_hwid-0.1.0 (c (n "get_hwid") (v "0.1.0") (d (list (d (n "machineid-rs") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winnt" "debugapi"))) (d #t) (k 0)) (d (n "windows") (r "^0.44.0") (d #t) (k 0)) (d (n "wmi") (r "^0.12.0") (d #t) (k 0)))) (h "0fjbn93plmfc7ayzd1b772p5awy57qkcvbmp5b6mqp2psjiy574i")))

