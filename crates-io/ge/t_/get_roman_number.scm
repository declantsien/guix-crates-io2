(define-module (crates-io ge t_ get_roman_number) #:use-module (crates-io))

(define-public crate-get_roman_number-0.1.0 (c (n "get_roman_number") (v "0.1.0") (h "0w0a7br9k5yfmnrcx4nd8hmpl2qw8xf2by422c78mw97nnml1zg3")))

(define-public crate-get_roman_number-0.1.1 (c (n "get_roman_number") (v "0.1.1") (h "1hh5fawz0w6rzqr7pxhwqgpba0mq9pbj2hfz3ggqambprhv5v7gj")))

