(define-module (crates-io ge t_ get_move) #:use-module (crates-io))

(define-public crate-get_move-0.1.0 (c (n "get_move") (v "0.1.0") (h "0wd9ydjzh1h2qb5gv38hrkijmn3nch2cddhbjsrn68c113fwclbx")))

(define-public crate-get_move-0.2.0 (c (n "get_move") (v "0.2.0") (h "1bczx0sv9vdzlhvm5dq7yxkhkj798i12gi0p9qsdz5ygb34jm2cg")))

(define-public crate-get_move-0.3.0 (c (n "get_move") (v "0.3.0") (h "14iflrxcsh8a1a87fxyw1krcdkg20ww2q879il57cpjkxw83czgz")))

