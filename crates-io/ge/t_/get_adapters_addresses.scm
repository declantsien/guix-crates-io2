(define-module (crates-io ge t_ get_adapters_addresses) #:use-module (crates-io))

(define-public crate-get_adapters_addresses-0.1.0 (c (n "get_adapters_addresses") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0fra97c0p04yi9fchqi1c9pxzn65hx5sxij3japm7pvxihlr8ya3")))

(define-public crate-get_adapters_addresses-0.1.1 (c (n "get_adapters_addresses") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_NetworkManagement_Ndis" "Win32_Networking_WinSock"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "14z4n114vyws5miq7sza0yl8y3x5c0bgbwy3ynaqkk0iiy5pcvda")))

