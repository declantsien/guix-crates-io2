(define-module (crates-io ge t_ get_errno) #:use-module (crates-io))

(define-public crate-get_errno-0.0.1 (c (n "get_errno") (v "0.0.1") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "1mhwg6a8vg5rdjd1z0c12rmi4wfkpz95ibli452dq5s7yf50zi0r")))

(define-public crate-get_errno-0.0.2 (c (n "get_errno") (v "0.0.2") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "1byx3jk8zmgsrm3vdf5xmmfybcd3h5lc8lghypvhgri7iy918vn3")))

(define-public crate-get_errno-0.0.3 (c (n "get_errno") (v "0.0.3") (d (list (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)))) (h "0vxr1wj8knq0xmhdq96cwjy5pnzyvyp2myjrmbfw132z154v0paa")))

