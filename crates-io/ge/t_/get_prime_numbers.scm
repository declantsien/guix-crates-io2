(define-module (crates-io ge t_ get_prime_numbers) #:use-module (crates-io))

(define-public crate-get_prime_numbers-0.1.0 (c (n "get_prime_numbers") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0hk793piwlgswm7naj0q7m8v1xjyh87yhmzqq3splgvrv531gg6l")))

