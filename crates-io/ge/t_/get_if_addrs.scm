(define-module (crates-io ge t_ get_if_addrs) #:use-module (crates-io))

(define-public crate-get_if_addrs-0.1.0 (c (n "get_if_addrs") (v "0.1.0") (d (list (d (n "ip") (r "~1.0.0") (d #t) (k 0)) (d (n "libc") (r "~0.1.12") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "net2") (r "^0.2.20") (d #t) (k 0)))) (h "17mzj0hgkg1kyl4xygiwbbcc2f7gdqpv16d1zjsnwk6gybgvb1wd")))

(define-public crate-get_if_addrs-0.1.1 (c (n "get_if_addrs") (v "0.1.1") (d (list (d (n "ip") (r "~1.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.1.12") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "net2") (r "^0.2.20") (d #t) (k 0)))) (h "1cw6b2iva00q1yxri5sz1vpw4dxn285fpa509x7w615b89hb8dcm")))

(define-public crate-get_if_addrs-0.3.0 (c (n "get_if_addrs") (v "0.3.0") (d (list (d (n "c_linked_list") (r "~0.1.0") (d #t) (k 0)) (d (n "ip") (r "~1.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.1.12") (d #t) (k 0)))) (h "0v08d6bfn3sxga34q9x0phzfxyhvmnn4z77gx3i4jypy1rkp2lh4")))

(define-public crate-get_if_addrs-0.3.1 (c (n "get_if_addrs") (v "0.3.1") (d (list (d (n "c_linked_list") (r "~0.1.0") (d #t) (k 0)) (d (n "ip") (r "~1.1.0") (d #t) (k 0)) (d (n "libc") (r "~0.1.12") (d #t) (k 0)))) (h "0ncs6i581ib9czrmjr0vsz6619fpdiwv7944aj477j3gs4j98lc8")))

(define-public crate-get_if_addrs-0.4.0 (c (n "get_if_addrs") (v "0.4.0") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.45") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.1.12") (d #t) (k 0)))) (h "08a87xv5aq1k6adrygc2fg1k3j8yr3xx13s8ldh7b6pgyn2d5phn")))

(define-public crate-get_if_addrs-0.4.1 (c (n "get_if_addrs") (v "0.4.1") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.45") (o #t) (d #t) (k 0)) (d (n "get_if_addrs-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.28") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0g3akkn52adw08lhhia0xp4jc7xak8q4l3yjk8984l74yk1868ys")))

(define-public crate-get_if_addrs-0.5.0 (c (n "get_if_addrs") (v "0.5.0") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.175") (o #t) (d #t) (k 0)) (d (n "get_if_addrs-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.34") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1qn5wm55kdga7x4sdvirpp56i9yii3yn3r77c9icwckqcml972iq")))

(define-public crate-get_if_addrs-0.5.1 (c (n "get_if_addrs") (v "0.5.1") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.175") (o #t) (d #t) (k 0)) (d (n "get_if_addrs-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.34") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1hrx2h2ywb1z0zzq1632hp7dp3h960mrqpwhbwgpn8jfgksca7y1")))

(define-public crate-get_if_addrs-0.5.2 (c (n "get_if_addrs") (v "0.5.2") (d (list (d (n "c_linked_list") (r "~1.1.0") (d #t) (k 0)) (d (n "clippy") (r "~0.0.175") (o #t) (d #t) (k 0)) (d (n "get_if_addrs-sys") (r "^0.1.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.34") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0p0gyhsf2zgrjx32b608f5y49kkm3438y8jin8jgfmd5j7v46k55")))

(define-public crate-get_if_addrs-0.5.3 (c (n "get_if_addrs") (v "0.5.3") (d (list (d (n "c_linked_list") (r "~1.1.1") (d #t) (k 0)) (d (n "clippy") (r "~0.0.175") (o #t) (d #t) (k 0)) (d (n "get_if_addrs-sys") (r "~0.1.1") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "libc") (r "~0.2.34") (d #t) (k 0)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1xsfxq3ga63vllw5k6pxpdlfp3m6fh8jiga865gr4cldi5dbbpdb")))

