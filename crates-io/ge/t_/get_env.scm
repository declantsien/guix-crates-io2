(define-module (crates-io ge t_ get_env) #:use-module (crates-io))

(define-public crate-get_env-0.1.0 (c (n "get_env") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0w3sxh4wmi0n8y6c5j1rnq4c8zykk6cnmq7rcfhp2bjb28lfkppd")))

