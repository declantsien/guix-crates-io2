(define-module (crates-io ge t_ get_config) #:use-module (crates-io))

(define-public crate-get_config-0.1.0 (c (n "get_config") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0kj4mknxmd69jwa86mjfamqnvbc73xisq52qy8kkazfbj5siyvbn")))

(define-public crate-get_config-0.1.1 (c (n "get_config") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "169j2bb2di4zjyw5fbann693xd0djmwr9x7j95nz8c1gmdcimfbj")))

