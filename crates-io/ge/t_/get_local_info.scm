(define-module (crates-io ge t_ get_local_info) #:use-module (crates-io))

(define-public crate-get_local_info-0.1.0 (c (n "get_local_info") (v "0.1.0") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)))) (h "0izjc9hj1jd0a2mdl1pfj1k72sbc6jzb4ysh3ly7imvjb1krs5qv")))

(define-public crate-get_local_info-0.1.1 (c (n "get_local_info") (v "0.1.1") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)))) (h "0kc9nsmakyddmhj6xd18pf8x72y42pk7s3h00517s1i8p13bzily")))

(define-public crate-get_local_info-0.1.2 (c (n "get_local_info") (v "0.1.2") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)))) (h "15i0qifiniqzwvncwzi72997ipyxhvkb7ckbk7glxc5j1bvmhk25")))

(define-public crate-get_local_info-0.1.3 (c (n "get_local_info") (v "0.1.3") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "1ds2wj8kcn6jw5jcbdbxx2v6mmpabadc3nqa27zb6wlarbyfnmj2")))

(define-public crate-get_local_info-0.1.4 (c (n "get_local_info") (v "0.1.4") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "018z6703ci0swfm43k23lfrqq8cxm67hnmmvxn5br2k71jpbyfbc")))

(define-public crate-get_local_info-0.1.5 (c (n "get_local_info") (v "0.1.5") (d (list (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "1870igx1xdhvgpj6nxbnlwr9c3yb03a96ji050w8v7apknhpy67k")))

(define-public crate-get_local_info-0.1.6 (c (n "get_local_info") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "03vb486zdld0k2df8m5b1drh73iqkyx8zynd89izvxcfi7zfsjyw")))

(define-public crate-get_local_info-0.1.7 (c (n "get_local_info") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "16z2bgh4ndz2g0y3m2vzshjbqidf49rlqkf3mw8jny5vpda91wly")))

(define-public crate-get_local_info-0.1.8 (c (n "get_local_info") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "0xxfrp5rrjj3hq67fpsiy4vhxrlhhcs04b81m65a6r7zh4dn4svf")))

(define-public crate-get_local_info-0.1.9 (c (n "get_local_info") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "04bmjm07xgd8r3sf69pny8nmxv1w72vl6b4v0qk0a4kd66x6dqrx")))

(define-public crate-get_local_info-0.2.0 (c (n "get_local_info") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "0icgbwhxd88wdr2xmaha6kg3dqijdbqidg87ji2hgry3rbj98hlr")))

(define-public crate-get_local_info-0.2.1 (c (n "get_local_info") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "0y53avhyvvw72s8g4y0nbi4lbgf0f49ip4f7fwfgdylwlix7jmr0")))

(define-public crate-get_local_info-0.2.2 (c (n "get_local_info") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "1bmnzzrs3b5p7svs28853dxfq6zyff3v39kb8pk13ydj69zpg0bm")))

(define-public crate-get_local_info-0.2.3 (c (n "get_local_info") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "0n1a6s7pr1b9lmg23l9dwaffx905cab8ryxrfgr4x1zyhja8dxi1")))

(define-public crate-get_local_info-0.2.4 (c (n "get_local_info") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "dns-lookup") (r "^2.0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.34.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rust-ini") (r "^0.19") (d #t) (k 0)))) (h "0f8jh2znjdilg8sxyg7wjp6y34318abkjwiwc8ncdaqanxfjnq0s")))

