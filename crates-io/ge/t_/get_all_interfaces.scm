(define-module (crates-io ge t_ get_all_interfaces) #:use-module (crates-io))

(define-public crate-get_all_interfaces-0.1.0 (c (n "get_all_interfaces") (v "0.1.0") (d (list (d (n "pnet") (r "^0.30") (d #t) (k 0)))) (h "12hs0mss09afnndyls1f6j6hwq1817c5nglrjcrdm7wljr19g8wn") (y #t)))

(define-public crate-get_all_interfaces-0.1.1 (c (n "get_all_interfaces") (v "0.1.1") (d (list (d (n "pnet") (r "^0.30") (d #t) (k 0)))) (h "0kgjn781w3b01ra9wxb42qcairk6llnqlxws8zvgjs7yzvpa6r5f")))

