(define-module (crates-io ge t_ get_input) #:use-module (crates-io))

(define-public crate-get_input-0.1.0 (c (n "get_input") (v "0.1.0") (h "13p05v3aib6hxn8hfg6506abjs0jypffq2cy4j1kcw10frykfc9g")))

(define-public crate-get_input-0.1.1 (c (n "get_input") (v "0.1.1") (h "0wn49b6raj5n2z1ccdcc0y4fickfs9sfy3rgsc2zp0mypvy4cf9v")))

