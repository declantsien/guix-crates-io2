(define-module (crates-io ge t_ get_bing_image) #:use-module (crates-io))

(define-public crate-get_bing_image-0.1.0 (c (n "get_bing_image") (v "0.1.0") (d (list (d (n "http_req") (r "^0.9.0") (d #t) (k 0)))) (h "0d7m2jpw9z4xdfhz6kvsw392lln1121kcvw5fi9i65cajagcxq4n") (y #t)))

(define-public crate-get_bing_image-0.1.1 (c (n "get_bing_image") (v "0.1.1") (d (list (d (n "http_req") (r "^0.9.0") (d #t) (k 0)))) (h "1crlkiv545ac48xwkz1yhm12d06wkb0vfdidsv69sc2xr6cdkf6j")))

