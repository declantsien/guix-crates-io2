(define-module (crates-io ge t_ get_user_input) #:use-module (crates-io))

(define-public crate-get_user_input-0.0.2 (c (n "get_user_input") (v "0.0.2") (h "19z5pi92n4n0zp0gy53qdwmypgs68ki035q1cnns1jb9m8zq1myw")))

(define-public crate-get_user_input-0.1.0 (c (n "get_user_input") (v "0.1.0") (h "1mnihcx9sbbkzwrd8d8spl580h48193bx2skw4ayplmgxa6km4nv")))

(define-public crate-get_user_input-0.1.1 (c (n "get_user_input") (v "0.1.1") (h "0m66c1d4q56ql1wq4pq4dgk8qkh4m6mdj59jj0nazwn47m45qdpd")))

