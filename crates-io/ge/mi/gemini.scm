(define-module (crates-io ge mi gemini) #:use-module (crates-io))

(define-public crate-gemini-0.0.1 (c (n "gemini") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "clone-impls" "visit-mut"))) (d #t) (k 0)))) (h "1pjg8mknhyckqj0dr7xzqskw929vf0a8cl1krsbjmzxsyr9bq1p4") (f (quote (("sync") ("default")))) (y #t)))

(define-public crate-gemini-0.0.2 (c (n "gemini") (v "0.0.2") (d (list (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1zkfimcm30r83h8500cki8llpp81rfbjc5ffpaf2xjjs8fawf7fz")))

(define-public crate-gemini-0.0.3 (c (n "gemini") (v "0.0.3") (d (list (d (n "nom") (r "^6.0.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "url") (r "^2.2.0") (d #t) (k 0)))) (h "1yviji78f104k0kn1wxdvf6fwm84qid75jkcjrbkqvrwjrjb7ds7") (f (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

(define-public crate-gemini-0.0.4 (c (n "gemini") (v "0.0.4") (d (list (d (n "nom") (r "^6.1.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "075hhvxbcz06cd4lkilylz3ssvnv3hc404ikzpgxqzlglc5qa4hn") (f (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

(define-public crate-gemini-0.0.5 (c (n "gemini") (v "0.0.5") (d (list (d (n "nom") (r "^6.1.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1q6lphyiawxqfcpy33sw4w4m17bmgg8whkcl6hxl6vxcm7sj1a0j") (f (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

