(define-module (crates-io ge mi gemini-engine) #:use-module (crates-io))

(define-public crate-gemini-engine-0.1.0 (c (n "gemini-engine") (v "0.1.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0xics3xmzycw2i5asfxa9qpgfvxz6ri1m9r6mskkpbpsxvbin9p9")))

(define-public crate-gemini-engine-0.1.1 (c (n "gemini-engine") (v "0.1.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1hcbdzadja227wky6w93xpbng1bc9zzmqazj1jxc56446hzr16fz")))

(define-public crate-gemini-engine-0.2.0 (c (n "gemini-engine") (v "0.2.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1vdmkw7ls471lc59qkzg7ypq80bp36wkg77p9x4b99lcyqcp1nlb")))

(define-public crate-gemini-engine-0.2.1 (c (n "gemini-engine") (v "0.2.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "11clsc5738nfidgc3van5ka1953gdhb91zanm9xf03njf968ymz6")))

(define-public crate-gemini-engine-0.3.0 (c (n "gemini-engine") (v "0.3.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0znj8psnqril37c6zz3kpa2plzcgzzhrnlrzq6dqfkmj2j0zk9r4")))

(define-public crate-gemini-engine-0.3.1 (c (n "gemini-engine") (v "0.3.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0arnnrddws97yh5c1jmhjpkhq56w9f9c4vcxb7plnfvmim6mi54l")))

(define-public crate-gemini-engine-0.3.2 (c (n "gemini-engine") (v "0.3.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0whpng1jw5b8yryl9rjj2p8r9fdp3cs54lfks35sh9092p7swrcb")))

(define-public crate-gemini-engine-0.3.3 (c (n "gemini-engine") (v "0.3.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "04cc8p7b5ivi94dg778v9lf7sq0h76fz0ndbgdddfl2m1rv8c53q")))

(define-public crate-gemini-engine-0.4.0 (c (n "gemini-engine") (v "0.4.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "09rjk1hxw5ag9j37kszs5k1nisxpmlmmfds70rfsdr03cxk1ibyl")))

(define-public crate-gemini-engine-0.4.1 (c (n "gemini-engine") (v "0.4.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "137x15107p23p38q8rp5a51gx63qbh1mck94kbnx3bfg24kf18gp")))

(define-public crate-gemini-engine-0.4.2 (c (n "gemini-engine") (v "0.4.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0276fg0y1vrcqg91p4i9gklj5h3axs2r66ayqd6v6i0qdwyx59v9")))

(define-public crate-gemini-engine-0.5.0 (c (n "gemini-engine") (v "0.5.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0bz1n0gw52diq7hzm5b82xf562lrl5g2wsw5a3r1jsw4v57hp92p")))

(define-public crate-gemini-engine-0.5.1 (c (n "gemini-engine") (v "0.5.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1y5ygvdkr865brfpqcjv9psyfnksm0wkcnkjvli2h0xkg7payi0n")))

(define-public crate-gemini-engine-0.6.0 (c (n "gemini-engine") (v "0.6.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1fm7d2nhb5lnfi9z1x8sp0gankfv9hc52myfh52gpsgcs6k74bwb")))

(define-public crate-gemini-engine-0.6.1 (c (n "gemini-engine") (v "0.6.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0f981iygd9v0a1y414dnhn6203412r4pq1p26vs2qasvxllj5bpc")))

(define-public crate-gemini-engine-0.6.2 (c (n "gemini-engine") (v "0.6.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0yh15fj7sm82lb595dalz0jx5zy6wfygfmpsqnqbk18sm59hwkam")))

(define-public crate-gemini-engine-0.6.3 (c (n "gemini-engine") (v "0.6.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0jjcvwlph4d4pwzjsji2wgqnjyxz2qrmm98crfd8vg16ns884z04")))

(define-public crate-gemini-engine-0.6.4 (c (n "gemini-engine") (v "0.6.4") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "03wpyfbxpmm68asvpk8jc18hcxv898h94jjp9fmldqx6zy9wl61a")))

(define-public crate-gemini-engine-0.7.0 (c (n "gemini-engine") (v "0.7.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1i1sfwaqf6dqn7z3wm2cz6f6djga3i88mdk84l2acj7796zjjhpp")))

(define-public crate-gemini-engine-0.7.1 (c (n "gemini-engine") (v "0.7.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0rhnj5zj3kbv7y922zv7dk9pz6sk03xxc1b58ys5swsn22l2cl3g")))

(define-public crate-gemini-engine-0.7.2 (c (n "gemini-engine") (v "0.7.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "10mz21vnjp1y3zp92gjn2884gvjxqpjdk91wa0whzlqlihflqswj")))

(define-public crate-gemini-engine-0.7.3 (c (n "gemini-engine") (v "0.7.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1cpb9f2inh0x7bm384rgx6azp0qsrca3mamfcp8c14gfnwmyag78")))

(define-public crate-gemini-engine-0.7.4 (c (n "gemini-engine") (v "0.7.4") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1v5cjxz9gr6dbfg40z6jz0282inkaal9vb3a1hvmnn7syv3nps7q")))

(define-public crate-gemini-engine-0.7.5 (c (n "gemini-engine") (v "0.7.5") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "04dkl84jbx8s7mjk8945xr0csn39dxgzd0v3ddgcyjfin50hbcc3")))

(define-public crate-gemini-engine-0.8.0 (c (n "gemini-engine") (v "0.8.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "07shiki2xavza4659hr81mzj1amr8b9lxzd994n014m68d44kpya")))

(define-public crate-gemini-engine-0.8.1 (c (n "gemini-engine") (v "0.8.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "07b8p8rwxmwhx3pbwnlxpghhnswn0wmr3c8n94w31bqpsh70ldkh")))

(define-public crate-gemini-engine-0.8.2 (c (n "gemini-engine") (v "0.8.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1dbj0a8d95r48bblvs702g3mypgcyags7n6kv7aqpzjhvh44zbch")))

(define-public crate-gemini-engine-0.8.3 (c (n "gemini-engine") (v "0.8.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "055prymclmfdsi3k3ipvygx847s4vhl8j9fpnrrcv8l3mqhhxkm2")))

(define-public crate-gemini-engine-0.8.4 (c (n "gemini-engine") (v "0.8.4") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1z4kh26nmkl1d6lbs2sj8mm5917cbmmvv07sskjis3fv3sicw874")))

(define-public crate-gemini-engine-0.9.0 (c (n "gemini-engine") (v "0.9.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "130rw62gna7m2pm473dcaja3x6g3z0s6csbwyl4vcj49b2dvdzsb")))

(define-public crate-gemini-engine-0.9.1 (c (n "gemini-engine") (v "0.9.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1h0ysqz2yz7vi3nx75yvyr97w6i2k4gicc1hn0yh3xkm89c9x6p3") (y #t)))

(define-public crate-gemini-engine-0.9.2 (c (n "gemini-engine") (v "0.9.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1n80d3k7d902qdyhy9xa5si3wh900h650n9b9dbmi5x4k7a0slg2")))

(define-public crate-gemini-engine-0.10.0 (c (n "gemini-engine") (v "0.10.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0jglrjx3z95wypipf9d3sz2qnj6v28ymhrnlqhffl220cxlnl3mw")))

(define-public crate-gemini-engine-0.10.1 (c (n "gemini-engine") (v "0.10.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "158z5fafs5a6p4z6h7y70915h644v39dfm8hpa86skphjamjyxnj")))

(define-public crate-gemini-engine-0.10.2 (c (n "gemini-engine") (v "0.10.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1zlskjpsc23dq8arb1da65m1ccc67r9vfanwzkwd8xshaa3j5941")))

(define-public crate-gemini-engine-0.10.3 (c (n "gemini-engine") (v "0.10.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1y0n7wjvm0x8gcayx675kl3a5w00ky8hm6gc9yi2jdnapalw5848")))

(define-public crate-gemini-engine-0.10.4 (c (n "gemini-engine") (v "0.10.4") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "04j15vji3wfarrsb1nv6s1l01gv48gsq3jy4cy62bkrvr344q8jk")))

(define-public crate-gemini-engine-0.10.5 (c (n "gemini-engine") (v "0.10.5") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1ydsax36rklwid5sa5sj33qdxg18pm3caybbj9g8ar3hd3gpa8qm")))

(define-public crate-gemini-engine-0.10.6 (c (n "gemini-engine") (v "0.10.6") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1mjyisbrzxdslrl1np17hby36mrv1j5a0hxig5pcspd3qmhh8kkk")))

(define-public crate-gemini-engine-0.11.0 (c (n "gemini-engine") (v "0.11.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1fz6v35c0j2iqcqa4qd2jcg2wfaw476a0w5gky59syi479l0r26x") (y #t)))

(define-public crate-gemini-engine-0.11.1 (c (n "gemini-engine") (v "0.11.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "170r4fs9ilzmjgygcq5i1v4niffxvr514d847mkfx15d6afmijws") (y #t)))

(define-public crate-gemini-engine-0.11.2 (c (n "gemini-engine") (v "0.11.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1sqi9ji05dby9ccqc3chpfj2lmgn8swr88yhfv2k7rhjynwmgyf2") (y #t)))

(define-public crate-gemini-engine-0.11.3 (c (n "gemini-engine") (v "0.11.3") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0a4pacxqdsjg5s1zza7nvfrjvrb7lx81vf5bgbn93g5wzqalkik9")))

(define-public crate-gemini-engine-0.12.0 (c (n "gemini-engine") (v "0.12.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1kqix42q2fkfzxgnb8gykpriiqf431i2d6aqsrqhx9fsgvd1rppv")))

(define-public crate-gemini-engine-0.13.0 (c (n "gemini-engine") (v "0.13.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "03wk1dfr8k9qkjsnvy44qv2749axxlwh2nwhfhfgbhrzsk514294")))

(define-public crate-gemini-engine-0.13.1 (c (n "gemini-engine") (v "0.13.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "0s4clb3iijivyf9qvifpab00s60r2z73ls25a2bz08afinbgnjxy")))

(define-public crate-gemini-engine-0.13.2 (c (n "gemini-engine") (v "0.13.2") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "02sz40hrr81qai5aynqjj5g6606605hbhrmimfpnqdxkbs2mmpq7")))

(define-public crate-gemini-engine-0.14.0 (c (n "gemini-engine") (v "0.14.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "1y5sjsq0p5sik1l139cm70ar7slfw7bdzr53nxj0gm44pbpbk3jz")))

(define-public crate-gemini-engine-0.14.1 (c (n "gemini-engine") (v "0.14.1") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)))) (h "18p28jg4l53ylfm5h36xmlx0mgzss2az7q7dh4fhpp8blzfd15cn") (f (quote (("default" "3D") ("3D"))))))

(define-public crate-gemini-engine-0.14.2 (c (n "gemini-engine") (v "0.14.2") (d (list (d (n "terminal_size") (r "^0.3.0") (d #t) (k 0)))) (h "1yvk9vmc02dmf1w2y8zg8hlq3hr86zaazl251w4nl457bin7yyjb") (f (quote (("default" "3D") ("3D"))))))

