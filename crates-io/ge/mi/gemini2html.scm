(define-module (crates-io ge mi gemini2html) #:use-module (crates-io))

(define-public crate-gemini2html-0.1.0 (c (n "gemini2html") (v "0.1.0") (h "0qjhvz9bcpahg7rakzw1zaw6aqyidm5n98686j8wsfx81szggsl0")))

(define-public crate-gemini2html-0.2.0 (c (n "gemini2html") (v "0.2.0") (h "00r6nfmv71di016a0jibxa5wcfzbq809grzmqcn78lj09bb09bpi")))

(define-public crate-gemini2html-0.2.1 (c (n "gemini2html") (v "0.2.1") (h "03gc8dgy4bgh3ik2ama9lbmc8vddagnmqzsavjfzxim9zf4hjmc7")))

(define-public crate-gemini2html-0.2.2 (c (n "gemini2html") (v "0.2.2") (h "0d4m41ydgwk5v4zqs8qn34i6vnarimj9kcw2cvxmjj1arcz9r9jz")))

