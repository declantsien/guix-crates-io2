(define-module (crates-io ge mi gemina) #:use-module (crates-io))

(define-public crate-gemina-0.1.0 (c (n "gemina") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1nrnnilrl5q9jgspwsc9bfg7gi62dy5wi0hxyp8cmsbhslvfmiqj") (y #t)))

(define-public crate-gemina-0.1.1 (c (n "gemina") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "13h388n707q8w9zggg6yjhpli69lar4sk7gxknb2ipf0nr0da0la") (y #t)))

(define-public crate-gemina-0.2.0 (c (n "gemina") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.11") (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0xd839klm7502ag1v90gjr70b7g9ay0l324i565k7s7hdzfjcdix")))

