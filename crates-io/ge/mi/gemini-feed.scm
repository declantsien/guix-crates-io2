(define-module (crates-io ge mi gemini-feed) #:use-module (crates-io))

(define-public crate-gemini-feed-0.1.0 (c (n "gemini-feed") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gemini-fetch") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03ixrkd0qivx203phjz3d8a95avbn38fn0pdp7fhvmw397di7r29")))

