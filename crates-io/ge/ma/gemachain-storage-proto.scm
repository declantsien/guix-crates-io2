(define-module (crates-io ge ma gemachain-storage-proto) #:use-module (crates-io))

(define-public crate-gemachain-storage-proto-1.8.0 (c (n "gemachain-storage-proto") (v "1.8.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "gemachain-account-decoder") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-transaction-status") (r "=1.8.0") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.2") (d #t) (k 1)))) (h "1b34g8dx8hrzv9ifymrsfip12a798kkqhqv6qbnan5px1ax63vwk")))

