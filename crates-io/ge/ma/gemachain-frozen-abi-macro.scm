(define-module (crates-io ge ma gemachain-frozen-abi-macro) #:use-module (crates-io))

(define-public crate-gemachain-frozen-abi-macro-1.8.1 (c (n "gemachain-frozen-abi-macro") (v "1.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mnl0clwmywd6k06ddh62vbdlpal450rrvkz24iqaj2n0f4s8nic")))

(define-public crate-gemachain-frozen-abi-macro-1.8.0 (c (n "gemachain-frozen-abi-macro") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ih5yn06mmfx2bzmlks2r21hqj6dvpa0pkwp32z32nl0xykvj86c")))

(define-public crate-gemachain-frozen-abi-macro-1.8.2 (c (n "gemachain-frozen-abi-macro") (v "1.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cs7jnb7l4rbfnnw9zfl0d14k1d5kjrgnsadqnjqhsiiz34ngxyv")))

