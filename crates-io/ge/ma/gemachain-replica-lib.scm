(define-module (crates-io ge ma gemachain-replica-lib) #:use-module (crates-io))

(define-public crate-gemachain-replica-lib-1.8.0 (c (n "gemachain-replica-lib") (v "1.8.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "gemachain-runtime") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.5.0") (f (quote ("tls" "transport"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.1") (d #t) (k 1)))) (h "0q1d05iapzq75cqspvviggl03a7jz60mqa2kr4zni7nw0dv24r7s")))

