(define-module (crates-io ge ma gemachain-send-transaction-service) #:use-module (crates-io))

(define-public crate-gemachain-send-transaction-service-1.8.0 (c (n "gemachain-send-transaction-service") (v "1.8.0") (d (list (d (n "gemachain-logger") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-metrics") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-runtime") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1hbw1w3nm0prnd7rbgq4f9c6x1i0ydl0vzlqrf6lhw0qxhnivmk1")))

(define-public crate-gemachain-send-transaction-service-1.8.2 (c (n "gemachain-send-transaction-service") (v "1.8.2") (d (list (d (n "gemachain-logger") (r "=1.8.2") (d #t) (k 0)) (d (n "gemachain-metrics") (r "=1.8.2") (d #t) (k 0)) (d (n "gemachain-runtime") (r "=1.8.2") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1f2gfkbis4f8nrn9dkcfgdn0lkz47nma3159710mnwq5clh532xf")))

