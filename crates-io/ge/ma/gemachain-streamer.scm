(define-module (crates-io ge ma gemachain-streamer) #:use-module (crates-io))

(define-public crate-gemachain-streamer-1.8.0 (c (n "gemachain-streamer") (v "1.8.0") (d (list (d (n "gemachain-logger") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-metrics") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-perf") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.102") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05in7c7p8yh2izqiiq1adk2wvpvpavndkjl23qfx4cw4d97x4zfj")))

