(define-module (crates-io ge ma gematria_rs) #:use-module (crates-io))

(define-public crate-gematria_rs-0.1.0 (c (n "gematria_rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q03pirpgc44b2q9njh975gl1jr76scq2vc5pbqjiir52sx3p1lw")))

(define-public crate-gematria_rs-0.1.1 (c (n "gematria_rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "18rglz9nff2zz4fzhjchw75afi3sfhp60i3c485z3ig4rzgqaqd2")))

