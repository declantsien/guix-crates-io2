(define-module (crates-io ge ma gemachain-merkle-tree) #:use-module (crates-io))

(define-public crate-gemachain-merkle-tree-1.8.0 (c (n "gemachain-merkle-tree") (v "1.8.0") (d (list (d (n "fast-math") (r "^0.1") (d #t) (k 0)) (d (n "gemachain-program") (r "=1.8.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (t "bpfel-unknown-unknown") (k 0)))) (h "1167gbillnr6hm5cari893a1pj9cq2bqzj62a7j1jn8h1bbvjqcd")))

