(define-module (crates-io ge ma gemachain-rayon-threadlimit) #:use-module (crates-io))

(define-public crate-gemachain-rayon-threadlimit-1.8.0 (c (n "gemachain-rayon-threadlimit") (v "1.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "0w3l92nj408rzx22bqslxy9hyrvwyzfaw9asr3r3305bqvad4453")))

(define-public crate-gemachain-rayon-threadlimit-1.8.2 (c (n "gemachain-rayon-threadlimit") (v "1.8.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)))) (h "1s0gdxmwqq3jkncbsngqp6ifl2idqpsk8c23fj0l6l1s2zm7pwgi")))

