(define-module (crates-io ge ma gemachain-secp256k1-program) #:use-module (crates-io))

(define-public crate-gemachain-secp256k1-program-1.8.0 (c (n "gemachain-secp256k1-program") (v "1.8.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "gemachain-logger") (r "=1.8.0") (d #t) (k 2)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1ddfizb3mxhcp5jvns44dfx9s5fx93b477a5pziyqnjjpwwy0qsx")))

(define-public crate-gemachain-secp256k1-program-1.8.2 (c (n "gemachain-secp256k1-program") (v "1.8.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "gemachain-logger") (r "=1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.6.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1qxn06ihcxqsk25257ydcfw30fdvhlny9vmb36alfb9ydwz4vfy2")))

