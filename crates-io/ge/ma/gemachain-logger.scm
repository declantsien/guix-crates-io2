(define-module (crates-io ge ma gemachain-logger) #:use-module (crates-io))

(define-public crate-gemachain-logger-1.8.1 (c (n "gemachain-logger") (v "1.8.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1qrqhsf0kmxs1sqyfvyw6vf2d9qhmkwwh2fxilwbb82i7mqm1izv")))

(define-public crate-gemachain-logger-1.8.0 (c (n "gemachain-logger") (v "1.8.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0bclc6y911z7lqn7ynmr8vnmir0rdn9mmaxa8ghzf9hkzl57abky")))

(define-public crate-gemachain-logger-1.8.2 (c (n "gemachain-logger") (v "1.8.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "00609fmk7mhp1p8a6m2f8m6csrdigyba90ydz284dibmv1yzlsq6")))

