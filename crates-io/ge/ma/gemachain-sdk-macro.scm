(define-module (crates-io ge ma gemachain-sdk-macro) #:use-module (crates-io))

(define-public crate-gemachain-sdk-macro-1.8.1 (c (n "gemachain-sdk-macro") (v "1.8.1") (d (list (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00iwwdx30sniacl5avqx1rsddxgmg96ag51mgb3flq94761vag68")))

(define-public crate-gemachain-sdk-macro-1.8.0 (c (n "gemachain-sdk-macro") (v "1.8.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1adlbzlydh5kxyx7rlvbp06yd84cmkqq5b4s9fadbwjdy3gw6qmm")))

(define-public crate-gemachain-sdk-macro-1.8.2 (c (n "gemachain-sdk-macro") (v "1.8.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13s5p2icfwcwnwvwp4zh0ln4hi30mrbwl3s9x3kzlyiq9zmkhhid")))

