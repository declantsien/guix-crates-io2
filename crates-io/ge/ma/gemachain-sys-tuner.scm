(define-module (crates-io ge ma gemachain-sys-tuner) #:use-module (crates-io))

(define-public crate-gemachain-sys-tuner-1.8.0 (c (n "gemachain-sys-tuner") (v "1.8.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "gemachain-logger") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-version") (r "=1.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.102") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "sysctl") (r "^0.4.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "unix_socket2") (r "^0.5.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "users") (r "^0.10.0") (d #t) (t "cfg(unix)") (k 0)))) (h "0mij0c71d0lzjkc7hylcahwhsirx6j5psd94n9ccd5z26d5p2v1p")))

