(define-module (crates-io ge ma gemachain-banks-interface) #:use-module (crates-io))

(define-public crate-gemachain-banks-interface-1.8.1 (c (n "gemachain-banks-interface") (v "1.8.1") (d (list (d (n "gemachain-sdk") (r "=1.8.1") (d #t) (k 0)) (d (n "mio") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.24.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gy1mcg8hc9dwh886dwbjiq5lfr6s90lsp365bcphqkjspzcf49w")))

(define-public crate-gemachain-banks-interface-1.8.0 (c (n "gemachain-banks-interface") (v "1.8.0") (d (list (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.26.2") (f (quote ("full"))) (d #t) (k 0)))) (h "00ap3s07m32dqyv9h5yp202pr4zdkskwgzl004kc374zabc65qni")))

(define-public crate-gemachain-banks-interface-1.8.2 (c (n "gemachain-banks-interface") (v "1.8.2") (d (list (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tarpc") (r "^0.26.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vvi2798y66xrmzzmggads9xm1dcqa769kpglh2s4jc4rv5phh51")))

