(define-module (crates-io ge ma gemachain-version) #:use-module (crates-io))

(define-public crate-gemachain-version-1.8.0 (c (n "gemachain-version") (v "1.8.0") (d (list (d (n "gemachain-frozen-abi") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-frozen-abi-macro") (r "=1.8.0") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)))) (h "0kxm8xlhimc7ks9af1piimzfbiwnfkgrpac5cgcd3v7nf5dk2xqb")))

(define-public crate-gemachain-version-1.8.2 (c (n "gemachain-version") (v "1.8.2") (d (list (d (n "gemachain-frozen-abi") (r "=1.8.2") (d #t) (k 0)) (d (n "gemachain-frozen-abi-macro") (r "=1.8.2") (d #t) (k 0)) (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)))) (h "0bjlrya810amyr8n43ckfhbq3ard9yqmilc02hnh00pcgmpi6699")))

