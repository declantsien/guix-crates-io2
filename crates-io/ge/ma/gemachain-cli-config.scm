(define-module (crates-io ge ma gemachain-cli-config) #:use-module (crates-io))

(define-public crate-gemachain-cli-config-1.8.0 (c (n "gemachain-cli-config") (v "1.8.0") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0dm012dqz89l3jd25z9ap2h8jwf9qgwl7mihhxfb4jdwynf8bf31")))

(define-public crate-gemachain-cli-config-1.8.2 (c (n "gemachain-cli-config") (v "1.8.2") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "16ll5134vf68m0dkwaabbnbsxv22mj7jrxjxs06hrzv493b9ib5y")))

