(define-module (crates-io ge ma gemachain-compute-budget-program) #:use-module (crates-io))

(define-public crate-gemachain-compute-budget-program-1.8.1 (c (n "gemachain-compute-budget-program") (v "1.8.1") (d (list (d (n "gemachain-sdk") (r "=1.8.1") (d #t) (k 0)))) (h "1abpf333xlda0cranxhyfvlkdsfjbxjr5wx8qqwj7i6289nxhsid")))

(define-public crate-gemachain-compute-budget-program-1.8.0 (c (n "gemachain-compute-budget-program") (v "1.8.0") (d (list (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)))) (h "07g2n8vhwlr1zdy7gixn8sqmdjmc9w7jqmrf91ryzh6bgimsphy8")))

(define-public crate-gemachain-compute-budget-program-1.8.2 (c (n "gemachain-compute-budget-program") (v "1.8.2") (d (list (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)))) (h "1xpqc6s007kjid4l1p66cqs1xgcj5v5a7v0rzijnkfzw8w1n8jsg")))

