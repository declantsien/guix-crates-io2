(define-module (crates-io ge ma gemachain-measure) #:use-module (crates-io))

(define-public crate-gemachain-measure-1.8.0 (c (n "gemachain-measure") (v "1.8.0") (d (list (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0i8kwa69icdcnh3b76slb8ifmxqfih3mcfivc14m9zks4c12a0f3")))

(define-public crate-gemachain-measure-1.8.2 (c (n "gemachain-measure") (v "1.8.2") (d (list (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1ffs73klplg46wxsn9bfxkkja6gxc12ziiy76hqvwqya4pfwd8l5")))

