(define-module (crates-io ge ma gemachain-ed25519-program) #:use-module (crates-io))

(define-public crate-gemachain-ed25519-program-1.8.1 (c (n "gemachain-ed25519-program") (v "1.8.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 2)) (d (n "gemachain-logger") (r "=1.8.1") (d #t) (k 2)) (d (n "gemachain-sdk") (r "=1.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0imcj52ris69s4lxxqs4vi950rffm894acx9zh86i0rpb32qb8ni")))

(define-public crate-gemachain-ed25519-program-1.8.0 (c (n "gemachain-ed25519-program") (v "1.8.0") (d (list (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 2)) (d (n "gemachain-logger") (r "=1.8.0") (d #t) (k 2)) (d (n "gemachain-sdk") (r "=1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "06q143iz7wpj8cs8yh1bbscqcvill0lj1y20z0jfs7h603bbnll4")))

(define-public crate-gemachain-ed25519-program-1.8.2 (c (n "gemachain-ed25519-program") (v "1.8.2") (d (list (d (n "ed25519-dalek") (r "=1.0.1") (d #t) (k 2)) (d (n "gemachain-logger") (r "=1.8.2") (d #t) (k 2)) (d (n "gemachain-sdk") (r "=1.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1mln85v8l8g82xl41qx4vfp7lj0qii36agcsiqm3asahpnzkc2l1")))

