(define-module (crates-io ge ra gerasdb) #:use-module (crates-io))

(define-public crate-gerasdb-0.0.1-dev (c (n "gerasdb") (v "0.0.1-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1nli31fdpd94bwqm5szims288c4y64dlw6fl8rf0l07lwm2iay29")))

(define-public crate-gerasdb-0.0.2-dev (c (n "gerasdb") (v "0.0.2-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0dpjlwmh7ipnx6628n7mx37a4sf56w678zdsajnlr9dq4fv34dzf")))

(define-public crate-gerasdb-0.0.3-dev (c (n "gerasdb") (v "0.0.3-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1nfldd0lmb448596n0xm2kc0ml58fsza1av7h1v852ry8gdlj1xg")))

(define-public crate-gerasdb-0.0.4-dev (c (n "gerasdb") (v "0.0.4-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1nn9lvlgacsads8fqzv8wk8gy1sjk2zncdf850rvvsq13y1jazpr")))

(define-public crate-gerasdb-0.0.5-dev (c (n "gerasdb") (v "0.0.5-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "163rmirvs27jj4gvl2zvvs0cw36cds01f3lxpzgrf8qrz7sk0vxm")))

(define-public crate-gerasdb-0.0.6-dev (c (n "gerasdb") (v "0.0.6-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.23.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "112r94xk2bjdw0n5bk1bnx1il3y4vgagmz8z5sbfawgl7qli719a")))

(define-public crate-gerasdb-0.0.7-dev (c (n "gerasdb") (v "0.0.7-dev") (d (list (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "r2d2_sqlite") (r "^0.17.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0in2dj0h5kvbwmcsjg5ad6h6wmq8ff79f4i33z6z3b8ax1ir5wfi")))

