(define-module (crates-io ge rr gerrymander) #:use-module (crates-io))

(define-public crate-gerrymander-0.1.0 (c (n "gerrymander") (v "0.1.0") (h "1zagb0wghjlkc4p9wrjiy17rapwwcf2w9ij6d3vww5vdaf72n0fa")))

(define-public crate-gerrymander-0.2.0 (c (n "gerrymander") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)))) (h "1qasxy9l7h44ribhq0b9730z571anagi4gdl4jry06f3ahdsh2d7") (s 2) (e (quote (("serde" "dep:serde"))))))

