(define-module (crates-io ge th gethostname) #:use-module (crates-io))

(define-public crate-gethostname-0.1.0 (c (n "gethostname") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "0pxirqbr5lz7j4r2b7gylf0yypj82kmj5qakrqv6xwz1gjlsavvl")))

(define-public crate-gethostname-0.2.0 (c (n "gethostname") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07g0wbx45qrbah6c36mnblya8nday763fn5i835bc7m3l8y2gayl")))

(define-public crate-gethostname-0.2.1 (c (n "gethostname") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a609j9dhk816il2f2a01avvi5sqzxh0p38nxwrja7dcpybf54p6")))

(define-public crate-gethostname-0.2.2 (c (n "gethostname") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lals1bchjd9ghd9xqrr5cnp853rvfxmsh2cfxkd0li8jdjc3paa")))

(define-public crate-gethostname-0.2.3 (c (n "gethostname") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bl6wf7zclzmjriipzh98rr84xv2ilj664z8ffxh0vn46m7d7sy1")))

(define-public crate-gethostname-0.3.0 (c (n "gethostname") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xy1wbx5k2bzi4cbaqj9wqgqsbn4f8pm6nsm1d86mibk66xd8rdv")))

(define-public crate-gethostname-0.4.0 (c (n "gethostname") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gw6v5r25dm7xss8y8ymxwcv1dhlziangzrxdm4x3l1y71kyrqhm") (r "1.64")))

(define-public crate-gethostname-0.4.1 (c (n "gethostname") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0114bh2p97xi9ygxgpq0wgm0m5bv7nia8ff6s8sqpmvdhqi9wcla") (r "1.64")))

(define-public crate-gethostname-0.4.2 (c (n "gethostname") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.141") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pc72k1ivpg438v6jb8xlw9h6dmms95qqpki81v48axgf2x2758r") (r "1.64")))

(define-public crate-gethostname-0.4.3 (c (n "gethostname") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.141") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "windows-targets") (r "^0.48") (d #t) (t "cfg(windows)") (k 0)))) (h "063qqhznyckwx9n4z4xrmdv10s0fi6kbr17r6bi1yjifki2y0xh1") (r "1.64")))

