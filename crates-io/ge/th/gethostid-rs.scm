(define-module (crates-io ge th gethostid-rs) #:use-module (crates-io))

(define-public crate-gethostid-rs-0.1.0 (c (n "gethostid-rs") (v "0.1.0") (d (list (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)))) (h "0xlhj9mqz74alb6p5nr82hfm27cml0x0iv9f9x9q58nnr0bqn4fs")))

(define-public crate-gethostid-rs-0.1.1 (c (n "gethostid-rs") (v "0.1.1") (d (list (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)))) (h "1fzmdykgzfjj89b0dms6qq32x1w87bajx7hcbdxmrna015xif419")))

