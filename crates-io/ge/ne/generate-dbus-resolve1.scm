(define-module (crates-io ge ne generate-dbus-resolve1) #:use-module (crates-io))

(define-public crate-generate-dbus-resolve1-0.2.0 (c (n "generate-dbus-resolve1") (v "0.2.0") (d (list (d (n "dbus") (r "^0.9.1") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9.1") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9.0") (d #t) (k 0)))) (h "0ibgx2sdypayvjc7zpniichqzn244vdb04xpdvhd1arilcnk2m3f")))

(define-public crate-generate-dbus-resolve1-0.3.0 (c (n "generate-dbus-resolve1") (v "0.3.0") (d (list (d (n "dbus") (r "^0.9.1") (d #t) (k 0)) (d (n "dbus-codegen") (r "^0.9.1") (d #t) (k 1)) (d (n "dbus-tree") (r "^0.9.0") (d #t) (k 0)))) (h "1kqx7b57i6cm80qwqhjhdp5v9bgqlcp02mclg0mvw6h29m852p2v")))

(define-public crate-generate-dbus-resolve1-1.0.0 (c (n "generate-dbus-resolve1") (v "1.0.0") (d (list (d (n "dbus") (r "^0.9.6") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus-tree") (r "^0.9.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus-codegen") (r "^0.9.1") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1xk6yc254k394h5qbpibj081cxlfvaqm6rssi134ii6bvzm8fbvm")))

(define-public crate-generate-dbus-resolve1-1.1.0 (c (n "generate-dbus-resolve1") (v "1.1.0") (d (list (d (n "dbus-codegen") (r "^0.10.0") (k 1)) (d (n "dbus") (r "^0.9.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dbus-tree") (r "^0.9.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "05dg4ic7f1yj5s7rlz1dn5vq7jvq9fz2l1akg3v347b2q828g7bl")))

