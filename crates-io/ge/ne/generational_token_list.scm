(define-module (crates-io ge ne generational_token_list) #:use-module (crates-io))

(define-public crate-generational_token_list-0.1.1 (c (n "generational_token_list") (v "0.1.1") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1gwni0ljqjk7w1ahjvibwnwxygd5pjzcck6ypvx1ci57q7fn17p6")))

(define-public crate-generational_token_list-0.1.2 (c (n "generational_token_list") (v "0.1.2") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "181llkm1w9pd0ilysv7hiv3ph3av2a20xyf4k024cdr4kvp7g6g6") (f (quote (("iter-mut") ("default"))))))

(define-public crate-generational_token_list-0.1.3 (c (n "generational_token_list") (v "0.1.3") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "0hz78279y85sn6h53gndfb8d07il8wxkinx21s7isgrl114m4v71") (f (quote (("iter-mut") ("default"))))))

(define-public crate-generational_token_list-0.1.4 (c (n "generational_token_list") (v "0.1.4") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1x20ds0s0li45ywf7ysr3vrzg6lmq0aqik84piv9kqqg5imdkrvx") (f (quote (("iter-mut") ("default"))))))

(define-public crate-generational_token_list-0.1.5 (c (n "generational_token_list") (v "0.1.5") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0wzf5vb26qrkw05qw1lxsvf07gwwmpcq2h33wfpspp3ay9fxqkzd") (f (quote (("iter-mut") ("default"))))))

