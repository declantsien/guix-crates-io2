(define-module (crates-io ge ne generate-random) #:use-module (crates-io))

(define-public crate-generate-random-0.1.0 (c (n "generate-random") (v "0.1.0") (d (list (d (n "generate-random-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "152lkf7qirnryvaxmnr1jzl2f56d682lf50l39ff39r531rhqljn")))

