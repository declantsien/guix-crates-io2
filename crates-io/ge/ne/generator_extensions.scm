(define-module (crates-io ge ne generator_extensions) #:use-module (crates-io))

(define-public crate-generator_extensions-0.1.0 (c (n "generator_extensions") (v "0.1.0") (h "1vs7w5al7rq44ds8hkcqj5jpiwwf4lq8azy3s4slhjy7cmc4fw6a")))

(define-public crate-generator_extensions-0.1.1 (c (n "generator_extensions") (v "0.1.1") (h "19wkq9na7nk06izdnxf15ap76mrz2f8zwqg1bg2wjc6c028b6s2f")))

