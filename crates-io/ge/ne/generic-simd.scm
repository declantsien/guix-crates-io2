(define-module (crates-io ge ne generic-simd) #:use-module (crates-io))

(define-public crate-generic-simd-0.1.0 (c (n "generic-simd") (v "0.1.0") (d (list (d (n "generic-simd-macros") (r "^0.1") (k 0)) (d (n "multiversion") (r "^0.6.1") (k 0)) (d (n "num-complex") (r "^0.3") (o #t) (k 0)))) (h "1mxkdxi3jxs75yyqy8raf8gc285q09gz1i8rid2fbyk6m2rb4ms1") (f (quote (("std" "multiversion/std") ("nightly" "generic-simd-macros/nightly") ("default" "std" "complex") ("complex" "num-complex") ("alloc"))))))

