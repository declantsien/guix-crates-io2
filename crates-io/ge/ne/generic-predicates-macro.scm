(define-module (crates-io ge ne generic-predicates-macro) #:use-module (crates-io))

(define-public crate-generic-predicates-macro-0.1.0 (c (n "generic-predicates-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a3ci04c1qpli0p2nzzakw7fyfxk8mhv2vm6d45pdza7ba5nhvck")))

