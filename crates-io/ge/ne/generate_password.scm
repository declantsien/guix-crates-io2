(define-module (crates-io ge ne generate_password) #:use-module (crates-io))

(define-public crate-generate_password-0.1.0 (c (n "generate_password") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1y6wgqykp0s3p78r394x4qbpy0mf0kh8c5fmagdl5b4k6na9d93g")))

(define-public crate-generate_password-0.1.1 (c (n "generate_password") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "09l14ajp68rqpp1a6pfm1xw0fnd9dh46vnsxcfw5l0kr13n93697")))

(define-public crate-generate_password-0.1.2 (c (n "generate_password") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1q6pc92fk72q5xh9wsk34pnzxm0ksw3r5idfplrs20rp944a1rn8")))

(define-public crate-generate_password-0.1.3 (c (n "generate_password") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fk2mki0sz1x9jknz1f4br39r0ph8iwnqdd3jbf0x9qqxmdhr40a")))

(define-public crate-generate_password-0.1.4 (c (n "generate_password") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "013jjdy4044vymh0z1nymhm854q121lj5341sjx7bv011yh2nn9n")))

