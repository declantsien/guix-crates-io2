(define-module (crates-io ge ne generate-readme) #:use-module (crates-io))

(define-public crate-generate-readme-0.1.0 (c (n "generate-readme") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0vf55ihlpw705an6v000xz72a0k0rlabqlfar7dd5kr4vq7vpah1")))

(define-public crate-generate-readme-0.1.1 (c (n "generate-readme") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1q1mqm1fyjdvxyfi8s99klf986ggl0r9fixcymfysly1p75np1vf")))

(define-public crate-generate-readme-0.1.2 (c (n "generate-readme") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.9") (d #t) (k 0)))) (h "0frnjf5z2zyqvg8c9798p98dx9ajii8f2hav28pdd9md5bv1hmsw")))

(define-public crate-generate-readme-0.1.3 (c (n "generate-readme") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.9") (d #t) (k 0)))) (h "15720ba9xaqj4g7vb0gsablsmlq8pwllha9mchhra5yvri8d8qlf")))

