(define-module (crates-io ge ne generic-simd-macros) #:use-module (crates-io))

(define-public crate-generic-simd-macros-0.1.0 (c (n "generic-simd-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0si50lh3cxag8vfjm2av95bckgm5narnbxi9y8dlbhmvvbfhga54") (f (quote (("nightly") ("default"))))))

