(define-module (crates-io ge ne genesys) #:use-module (crates-io))

(define-public crate-genesys-0.1.0 (c (n "genesys") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0wqpy1azri9acnw3ay49fq187ckl0gl7lra1l3bs20rn7ab31n4s")))

(define-public crate-genesys-0.1.1 (c (n "genesys") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0lmncvihsmapfi0665pfmqc465jaqv9w5v9zx636abkhk6vdfwzv")))

(define-public crate-genesys-0.1.2 (c (n "genesys") (v "0.1.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1hf206wmw2asyxvd30p1jj6j4frb3xxjzr8nd09rgkrjvh40w664")))

