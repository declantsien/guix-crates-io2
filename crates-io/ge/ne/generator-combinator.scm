(define-module (crates-io ge ne generator-combinator) #:use-module (crates-io))

(define-public crate-generator-combinator-0.1.0 (c (n "generator-combinator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1fklxig42gy5lhvkllcjrl45kqajyad05v0wdnjwhda7yfai6jhc")))

(define-public crate-generator-combinator-0.2.0 (c (n "generator-combinator") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "03bmkyzz3lmcaxjlqrn25ks8g5das0gb9ilcd56r45x3sfg7ga5c")))

(define-public crate-generator-combinator-0.3.0 (c (n "generator-combinator") (v "0.3.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "01n55q7d8nmsfkzjnqlwv1851bzbj2sz5xq9ways39pnhk7rq1r5")))

(define-public crate-generator-combinator-0.4.0 (c (n "generator-combinator") (v "0.4.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1mnm3rbggginsh5i018nklk6wa8xnf18mp0vrmrklckm42ylv2rb") (f (quote (("with_rand" "rand") ("default"))))))

