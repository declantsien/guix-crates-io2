(define-module (crates-io ge ne genetic_optimization) #:use-module (crates-io))

(define-public crate-genetic_optimization-0.1.0 (c (n "genetic_optimization") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qm55rkbbkay4cg6a9xlll4px9j0y378p3y6d0aqnczzkgcrz8ss")))

(define-public crate-genetic_optimization-0.2.0 (c (n "genetic_optimization") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0424163prz7zw4ngqgf2crqrzs6lf2wz6wqxsbihcir5ha41gszs")))

(define-public crate-genetic_optimization-0.3.0 (c (n "genetic_optimization") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "16p728h6jgwzc4awfig26xaysad2my2s4gqikbh8a61jziskkqav")))

(define-public crate-genetic_optimization-0.4.0 (c (n "genetic_optimization") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0f41bn8d24ca6qnlhn2i6s5mgr7pdx9sbqvjrac7dcr7zw4m498q")))

(define-public crate-genetic_optimization-0.4.1 (c (n "genetic_optimization") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0prfdgdfkmyf3vvk2b80vcr0zv20377v9v1h37zpfyivmwzhvija")))

(define-public crate-genetic_optimization-0.5.0 (c (n "genetic_optimization") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1jffdjiybx2pd926cizl95zawb3vncq1n4cnq2ihxjzpq8cbdapz")))

(define-public crate-genetic_optimization-0.5.1 (c (n "genetic_optimization") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0hm3asd0a3pd3dr9r74013piyixr3mh8ijn32c6h4jzdl389pbgv")))

