(define-module (crates-io ge ne genesis2000) #:use-module (crates-io))

(define-public crate-genesis2000-0.1.0 (c (n "genesis2000") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1j39klqggvd4pl4rivkf6jgzai6xkyr0sa2ijm5jxalxa8n4djdy")))

(define-public crate-genesis2000-0.2.0 (c (n "genesis2000") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1idn3ig91widfl9a9mmrl06khwybii5x4xh443ikrqvsn6b7bg1w")))

