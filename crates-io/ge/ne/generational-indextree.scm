(define-module (crates-io ge ne generational-indextree) #:use-module (crates-io))

(define-public crate-generational-indextree-1.0.0 (c (n "generational-indextree") (v "1.0.0") (d (list (d (n "generational-arena") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hc4bcilyi20xainxj46fzqsgmsr1hfhaknq2s7a2f1mn11x9al7") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.0.1 (c (n "generational-indextree") (v "1.0.1") (d (list (d (n "generational-arena") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i617z65kzdaa40qflyf6k39db28vv9fqhrfvbwbdbfbvl6fqk2s") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.0.2 (c (n "generational-indextree") (v "1.0.2") (d (list (d (n "generational-arena") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1albws74g25bsrii5ijzlg4pi4xr5b43pxlrcc26868lkna8dns4") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.0.3 (c (n "generational-indextree") (v "1.0.3") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05mxk852dypqhnw5ksjzk77qck8pldsawllzshxhx9sck5lza5j0") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.1.0 (c (n "generational-indextree") (v "1.1.0") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06p7sazbgxww527di5xd0qrllldrc86nwhh69ixc4hgh2v0hkr60") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.1.1 (c (n "generational-indextree") (v "1.1.1") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0g5yql58ap9v1b5l1fcdpaikx57alh414iib8hiqq63554x0agxs") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.1.2 (c (n "generational-indextree") (v "1.1.2") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zarmdnzx5snav248nah5887dpnnsfj8dvqfs66kmcxn5pzapr6w") (f (quote (("std") ("deser" "serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.1.3 (c (n "generational-indextree") (v "1.1.3") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jvim7ch2alqaxbn3znxii860mf4pxy2mwrvykvpjckga5ah9rby") (f (quote (("std") ("deser" "serde" "generational-arena/serde") ("default" "std"))))))

(define-public crate-generational-indextree-1.1.4 (c (n "generational-indextree") (v "1.1.4") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kw8n1a5lhq71hgyxj3zw9098jbj72s5xmplvpmql1h8ajdrirlj") (f (quote (("std") ("deser" "serde" "generational-arena/serde") ("default" "std"))))))

