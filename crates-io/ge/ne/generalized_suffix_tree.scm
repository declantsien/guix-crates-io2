(define-module (crates-io ge ne generalized_suffix_tree) #:use-module (crates-io))

(define-public crate-generalized_suffix_tree-0.1.0 (c (n "generalized_suffix_tree") (v "0.1.0") (h "1riy69l4f36ja9dqp4ys0yfrrqf00bgd7p0vqkpbbmkwfg8xwfp9")))

(define-public crate-generalized_suffix_tree-0.1.1 (c (n "generalized_suffix_tree") (v "0.1.1") (h "11x7spkpv1qdazr8amd56xi79kidrr7id2cnh34cxm3ijk117hrq")))

(define-public crate-generalized_suffix_tree-0.1.2 (c (n "generalized_suffix_tree") (v "0.1.2") (h "1rmlpijq5fx85jhxf0v0rp934n8in638zlsfa8zvvx9cy8bra9xk")))

(define-public crate-generalized_suffix_tree-0.2.0 (c (n "generalized_suffix_tree") (v "0.2.0") (h "04ca5f3bs86klfkraz1b144yn3s640cwnsr0z119kl3cwa1w3gsb")))

(define-public crate-generalized_suffix_tree-0.2.1 (c (n "generalized_suffix_tree") (v "0.2.1") (h "0sh7yjvv7ibf5ybva3gcasi2k80r70kqn3zgncnaxbm9kfvxfkqw")))

(define-public crate-generalized_suffix_tree-1.0.0 (c (n "generalized_suffix_tree") (v "1.0.0") (h "1gxsf9j3w4cvdq5smdbmnmg3ijy2j5p34xl4444ys0rii2rrdd1q")))

(define-public crate-generalized_suffix_tree-1.1.0 (c (n "generalized_suffix_tree") (v "1.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0n5x4ax1an0g8ix5kjqhn3bn06wraam31yx1f3jx0xi3rb1nv3nx")))

(define-public crate-generalized_suffix_tree-1.1.1 (c (n "generalized_suffix_tree") (v "1.1.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1fdbkvk58pj8g5d2j7n7l21nkl5z5mn2mb2lg01mqa0sdgxkn9yn")))

(define-public crate-generalized_suffix_tree-1.2.1 (c (n "generalized_suffix_tree") (v "1.2.1") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1ka4152qsycx70arxrwpdcx8gm2af2jl9n06k9f4lqafljnlwc10")))

