(define-module (crates-io ge ne general_storage_web) #:use-module (crates-io))

(define-public crate-general_storage_web-0.1.0 (c (n "general_storage_web") (v "0.1.0") (d (list (d (n "general_storage") (r "^0.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (k 0)))) (h "1fiji7qni8lh6w01p4hnhb3bcrc958jwa0wnqafbl4zsk4wb8pcb")))

(define-public crate-general_storage_web-0.1.1 (c (n "general_storage_web") (v "0.1.1") (d (list (d (n "general_storage") (r "^0.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (k 0)))) (h "0m8am85djr0mwfscbd1v2qs5fb6cggmarpn7cc9x88qpih41jcrb")))

(define-public crate-general_storage_web-0.2.0 (c (n "general_storage_web") (v "0.2.0") (d (list (d (n "general_storage") (r "^0.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (k 0)))) (h "0n9win8j8l52dyddyx0a6yal053y1ysidpp11ag1hr42ycnild33")))

(define-public crate-general_storage_web-0.3.0 (c (n "general_storage_web") (v "0.3.0") (d (list (d (n "general_storage") (r "^0.3") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Storage"))) (d #t) (k 0)))) (h "0ym38mq7yz0nnps98cq5cmjbnr81y6nd3vspalwjpvcvflhhdn6r")))

