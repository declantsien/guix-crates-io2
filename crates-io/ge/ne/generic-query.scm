(define-module (crates-io ge ne generic-query) #:use-module (crates-io))

(define-public crate-generic-query-0.1.0 (c (n "generic-query") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (f (quote ("staking" "stargate"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)))) (h "1qybkw2ynr5kby4r7l8vyv099yy0xpjm4rk6wjj6lrw0v71xczl3")))

(define-public crate-generic-query-0.1.1 (c (n "generic-query") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (f (quote ("staking" "stargate"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-cw-value") (r "^0.7.0") (d #t) (k 0)))) (h "0yhdkw7s56plz8x0h1p7j9glw2x0y8366hh72pa7rqzkrpj8pchi")))

