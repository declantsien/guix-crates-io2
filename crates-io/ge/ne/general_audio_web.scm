(define-module (crates-io ge ne general_audio_web) #:use-module (crates-io))

(define-public crate-general_audio_web-0.1.0 (c (n "general_audio_web") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlAudioElement"))) (d #t) (k 0)))) (h "05jhjsri8yzp14fc98cgf6bs4zz2driqjjfrjafykgkp0l1gqvn5")))

(define-public crate-general_audio_web-0.1.1 (c (n "general_audio_web") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlAudioElement"))) (d #t) (k 0)))) (h "19pm1rggg2pmfhsjvgvbgfvnv99vq8xq1m9vv6zdafz869bqr41j")))

