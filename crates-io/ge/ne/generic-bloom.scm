(define-module (crates-io ge ne generic-bloom) #:use-module (crates-io))

(define-public crate-generic-bloom-0.1.0 (c (n "generic-bloom") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wgixkwmxw0lnmi14lh1wi1aivpa3vnpsiclj3vn9zdq1qf8ywlf")))

