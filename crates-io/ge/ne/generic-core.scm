(define-module (crates-io ge ne generic-core) #:use-module (crates-io))

(define-public crate-generic-core-0.0.1 (c (n "generic-core") (v "0.0.1") (h "1h7imp1q5kalqcy984j0aah9rcvrig1i75q5lzykg7j3yr9bbcyd")))

(define-public crate-generic-core-0.0.2 (c (n "generic-core") (v "0.0.2") (d (list (d (n "uuid") (r "^0.7") (d #t) (k 0)))) (h "085b7d2w2ip4zm9b8mg3wabdsmk6npbpysq9j75jw5qzwnx5yr63")))

