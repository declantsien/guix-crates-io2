(define-module (crates-io ge ne generate_sql) #:use-module (crates-io))

(define-public crate-generate_sql-0.1.0 (c (n "generate_sql") (v "0.1.0") (h "13bjwbcbccgl4d6x7jcpnklznmifmvc12wr6dmxi88p1mdfy4zmi")))

(define-public crate-generate_sql-0.1.1 (c (n "generate_sql") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k6v3q8nn5j5psmbsrnfkaqlbyym4y1872fsgr6vmyf9hxcripnx")))

(define-public crate-generate_sql-0.1.2 (c (n "generate_sql") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13n4xmkzf7ag3czilpp9vjqij0w88j7lxbk12ii8ihrxw0bfy2sa")))

(define-public crate-generate_sql-0.1.3 (c (n "generate_sql") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jbk86d58abm21k8mm7lmfylmb5fx0f11c6l1130zga5wpsrir88")))

(define-public crate-generate_sql-0.1.4 (c (n "generate_sql") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vvg9ck1w2h7xiy3a7lkixl34zcsi3hcbh9p6yd87lf79flvcd83")))

(define-public crate-generate_sql-0.1.5 (c (n "generate_sql") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1y1d1f1hi9034vrg62z452kvlfrdy37wvnjnf5faakhf0syw8q4p")))

(define-public crate-generate_sql-0.1.51 (c (n "generate_sql") (v "0.1.51") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0xwdzyfmgx5nk9s0lpighb5rzhw2ps03fkq3vigcbrwjx657rb2g")))

(define-public crate-generate_sql-0.1.6 (c (n "generate_sql") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0hy6h7wnbmj4qh14q3ax441wckwavmykn03rjvz9gdmd6x55pr0n")))

(define-public crate-generate_sql-0.1.61 (c (n "generate_sql") (v "0.1.61") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "139yx87nbc9xb5vpx4prkwm9nw81nqh6zy06ql9gby6lg750wmrs")))

(define-public crate-generate_sql-0.1.62 (c (n "generate_sql") (v "0.1.62") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0qbigalxhc9xyfqb7sz1w208rzf5ika321m67dsjniw91y9kj67v")))

(define-public crate-generate_sql-0.1.63 (c (n "generate_sql") (v "0.1.63") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "13klk93b41mfxbl213qz3d6rkpyvxiwf5m002hkj7dsx35yiavh0")))

(define-public crate-generate_sql-0.1.631 (c (n "generate_sql") (v "0.1.631") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ahllxzypc3fy2a2bawm0mw45p6g8d9mn4ajpinbwzykxhg6s0r5")))

