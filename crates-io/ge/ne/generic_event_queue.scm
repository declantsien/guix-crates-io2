(define-module (crates-io ge ne generic_event_queue) #:use-module (crates-io))

(define-public crate-generic_event_queue-0.1.0 (c (n "generic_event_queue") (v "0.1.0") (h "1mwq63g7g1z0sy3l52nrjasgwyzzg8s96l36ir2fw3di0ncv5449")))

(define-public crate-generic_event_queue-0.2.0 (c (n "generic_event_queue") (v "0.2.0") (h "1fvcp96rfwm32qdrkmhcvrs7a34n2dx2mg1yk2ajv07j2jsc6xxc")))

