(define-module (crates-io ge ne genetic) #:use-module (crates-io))

(define-public crate-genetic-0.1.0 (c (n "genetic") (v "0.1.0") (h "1lnrzfyfnqssskssx84ak0pwn5ip2kwyhn3y47b78w20h2ciq628")))

(define-public crate-genetic-0.1.1 (c (n "genetic") (v "0.1.1") (h "1b1lxb26p3ady26micd2jn201lrp82k18zjglp2rynbyczn27m80")))

(define-public crate-genetic-0.2.0 (c (n "genetic") (v "0.2.0") (h "18af64xx57w8azim98gbxs6gppj7ynlvx3j7aiky1g9kz6zvvml7")))

(define-public crate-genetic-0.2.1 (c (n "genetic") (v "0.2.1") (h "1lfxlzw2qzc5rvafipvj7csmgy5hwi4g4pb2sa6dj23lggy2w6jr")))

