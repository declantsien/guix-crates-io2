(define-module (crates-io ge ne generic-vec) #:use-module (crates-io))

(define-public crate-generic-vec-0.1.0-alpha (c (n "generic-vec") (v "0.1.0-alpha") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "02zg6v1was311zq5i1vyh53a7lb4r2l6qczvys6i0wpq2w0c5k8h") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-generic-vec-0.1.1 (c (n "generic-vec") (v "0.1.1") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "0crc85d96nvd80kqbmb6csncnjfy05cbhmqcbaf31l1s71xxlikn") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-generic-vec-0.1.2 (c (n "generic-vec") (v "0.1.2") (d (list (d (n "mockalloc") (r "^0.1") (d #t) (k 2)) (d (n "static-alloc") (r "^0.2") (d #t) (k 2)))) (h "0yns3mlw6ck98k3ypc3621wlr3gxbx1w38fs8hdhccylkw6q81bn") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

