(define-module (crates-io ge ne generic-static-cache) #:use-module (crates-io))

(define-public crate-generic-static-cache-0.1.0 (c (n "generic-static-cache") (v "0.1.0") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "1plyf6cyylzaf7zs57lbcykb6xk02h7ddwrcb1k83qw91nralcnc")))

(define-public crate-generic-static-cache-0.1.1 (c (n "generic-static-cache") (v "0.1.1") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "1lifk880fkxjnmac29hs2i9rqb18xr4x002rg48agqpbhw2vyigv")))

(define-public crate-generic-static-cache-0.1.2 (c (n "generic-static-cache") (v "0.1.2") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "0di96rqij7i8q2is0yzdis89pbmdi13cd7bz61lkri96hmg544zd")))

(define-public crate-generic-static-cache-0.1.3 (c (n "generic-static-cache") (v "0.1.3") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "0iz0nizxzf6iba6w8qfsmpkb7fd2373sibj6lrqcf1l91jkp56hg")))

(define-public crate-generic-static-cache-0.1.4 (c (n "generic-static-cache") (v "0.1.4") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "1maqzb8y6bvvizzx3l533aqbz8vpyfwjnlsnbhbhib6775x9cfxc")))

(define-public crate-generic-static-cache-0.1.5 (c (n "generic-static-cache") (v "0.1.5") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "1y4wak5khplfvb5yzrr6jvfl2ypajr59ndb5vy827ysqnbqvzyc1")))

(define-public crate-generic-static-cache-0.1.6 (c (n "generic-static-cache") (v "0.1.6") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "0brzpzqkyf2mm9xx2mxmy02rr1kqml3gf5rccmm27jjl93pbmhxq")))

(define-public crate-generic-static-cache-0.1.7 (c (n "generic-static-cache") (v "0.1.7") (d (list (d (n "bytemuck") (r ">=1.13") (f (quote ("zeroable_atomics" "zeroable_maybe_uninit"))) (d #t) (k 0)))) (h "002zg175a3dr660b6py2kna75mvaz83v2hd237gqsaq75d9zaq7n")))

