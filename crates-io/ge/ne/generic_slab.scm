(define-module (crates-io ge ne generic_slab) #:use-module (crates-io))

(define-public crate-generic_slab-0.1.0 (c (n "generic_slab") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0k7548q4gr4vmc57l61s1pfi572qwimvh9yy8dgz3p3gklchlvch") (f (quote (("std") ("range") ("default" "std")))) (r "1.31")))

(define-public crate-generic_slab-0.1.1 (c (n "generic_slab") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 2)) (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "099fx028w5w35nr8ihh55pzhb884j9p9spglaxvrhmwa2y73mdv1") (f (quote (("std") ("range") ("default" "std")))) (r "1.31")))

