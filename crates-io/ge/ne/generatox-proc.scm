(define-module (crates-io ge ne generatox-proc) #:use-module (crates-io))

(define-public crate-generatox-proc-0.1.0 (c (n "generatox-proc") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "16i5lf2ap65mgbmy8fz5v2ph398qcrmnz0xsz5ny4kaikw7wvfdj") (f (quote (("nightly"))))))

(define-public crate-generatox-proc-0.1.1 (c (n "generatox-proc") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "07qn7gb850r0q41vfhfhqliajy8wlx2chsmckh1n5gyzl785sgbz") (f (quote (("nightly"))))))

(define-public crate-generatox-proc-0.1.2 (c (n "generatox-proc") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "1jw93vh4ma5mm4cmis5rmxzg8ii4sxp94aasj7dyl5sfmscn4h8x") (f (quote (("nightly"))))))

(define-public crate-generatox-proc-0.1.3 (c (n "generatox-proc") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits" "fold"))) (d #t) (k 0)))) (h "19dwbjx26bi3dm1bgjxr8qi39sqczss59awpy424gina1ngf6j55") (f (quote (("nightly"))))))

