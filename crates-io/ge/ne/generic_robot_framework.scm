(define-module (crates-io ge ne generic_robot_framework) #:use-module (crates-io))

(define-public crate-generic_robot_framework-0.1.0 (c (n "generic_robot_framework") (v "0.1.0") (d (list (d (n "jsonschema") (r "^0.17.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "14x2yg1hvqba5h8qjvxksarf7ay3ywzzqkj9ndx0rlpn50j2yr1h")))

(define-public crate-generic_robot_framework-0.1.1 (c (n "generic_robot_framework") (v "0.1.1") (d (list (d (n "jsonschema") (r "^0.17.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0qyya9yndnnkyndm76jcl24cin7cdnx4clx26qc0f3ra9ill1izf")))

(define-public crate-generic_robot_framework-0.1.2 (c (n "generic_robot_framework") (v "0.1.2") (d (list (d (n "jsonschema") (r "^0.17.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0bllvygvl9lrhi06c97anl58a7l6d35mj9w7vgql0zb5fr2dbf6f")))

(define-public crate-generic_robot_framework-0.1.3 (c (n "generic_robot_framework") (v "0.1.3") (d (list (d (n "jsonschema") (r "^0.17.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0x1gg7dywmg91k67jzybyl2lr6fs043c7sckh6pjfx4gblji3bl0")))

