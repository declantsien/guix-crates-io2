(define-module (crates-io ge ne generic-token-faucet) #:use-module (crates-io))

(define-public crate-generic-token-faucet-0.1.2 (c (n "generic-token-faucet") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.1.1") (d #t) (k 0)))) (h "058z4cqmvmf5yr632yn2h7bgrx2p04ah4sd2ifaplq0pmnwdawap") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

