(define-module (crates-io ge ne generic_graph) #:use-module (crates-io))

(define-public crate-generic_graph-0.1.0 (c (n "generic_graph") (v "0.1.0") (h "1713jyj3n7pgmk70f3p4b4ymm3fs0xmcj8g25cackhdar2nkglxs") (y #t)))

(define-public crate-generic_graph-0.1.1 (c (n "generic_graph") (v "0.1.1") (h "0da891l003j5cphf9ajl8lf4gyy7g6nfhgfdsha154xg7vgyz3vc") (y #t)))

(define-public crate-generic_graph-0.1.2 (c (n "generic_graph") (v "0.1.2") (h "185i74f6j0nmxzx2yl9h2iy3g4nmi86h8q9rdv45aaplg5m96a1x")))

(define-public crate-generic_graph-0.2.2 (c (n "generic_graph") (v "0.2.2") (h "01wxwkr9yycyagbr3mziazgdkimbh16j1c72vswmsr4q0y68a1c8")))

(define-public crate-generic_graph-0.3.0 (c (n "generic_graph") (v "0.3.0") (h "0d5p1gjb576cbfjd201v9xwqlkirw4r306izg5xf4xz2j3pyhbdl")))

