(define-module (crates-io ge ne genetic_algorithms) #:use-module (crates-io))

(define-public crate-genetic_algorithms-0.1.0 (c (n "genetic_algorithms") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zaz6mbdknwk23fyblshcq726yxj3c3yfplksbhbrkxqqsi5r28s") (y #t)))

(define-public crate-genetic_algorithms-0.1.1 (c (n "genetic_algorithms") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1crjcqwszqr3bzvvcrry92nwjrnr50yryw92pb1rgsqswvql5vpg")))

(define-public crate-genetic_algorithms-0.2.0 (c (n "genetic_algorithms") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c12myjnrxi0ngclqy8r9gxraqwy5j6r78n1nf1s9jshp4y72s8y") (y #t)))

(define-public crate-genetic_algorithms-0.2.1 (c (n "genetic_algorithms") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kdjxzsyr34xkagswzkbld4zby9rgbcfng8zs1nvamlcgggsfnwg")))

(define-public crate-genetic_algorithms-0.3.0 (c (n "genetic_algorithms") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s4ayl9sdggl40n8qgggafbwsaxpgyr429gglps6qm4imrsz9p19")))

(define-public crate-genetic_algorithms-0.4.0 (c (n "genetic_algorithms") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vhs02hdbwshz0bgzldvajl53hfailhsdzbqlxy7wk7qfg88hi5w")))

(define-public crate-genetic_algorithms-0.5.0 (c (n "genetic_algorithms") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zsx67cnrv8b8caflk3lfzcgi86ima1yrl2iqj392s64qnc9f0kz") (y #t)))

(define-public crate-genetic_algorithms-0.5.1 (c (n "genetic_algorithms") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dvkar2brhmbf272dpyhzm9a2dawkaajzv2y3gfa04rks3c0qrah")))

(define-public crate-genetic_algorithms-0.6.0 (c (n "genetic_algorithms") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0brn6k9q2zjdxbwhf8yjzrlmyhpm3drd2gbcm08anph48p99zgdr")))

(define-public crate-genetic_algorithms-0.7.0 (c (n "genetic_algorithms") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vdrj63l4w7a6kipss31m0nkr6v6ay6c7f9a7bg8qyp0d0qy5fn2") (y #t)))

(define-public crate-genetic_algorithms-0.7.1 (c (n "genetic_algorithms") (v "0.7.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fal4q9b34nki5jaqpry1d96kgcp2dymcli46vp3hvidbqqzg348") (y #t)))

(define-public crate-genetic_algorithms-0.7.2 (c (n "genetic_algorithms") (v "0.7.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19lhi6nc493bmnmcimwjmpl03nz4qzgnch4wx2rd3nx1bzhk5k7l") (y #t)))

(define-public crate-genetic_algorithms-0.7.3 (c (n "genetic_algorithms") (v "0.7.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04gynhg5nk1kibwcb7fc60b92dwimip2ic04rknmh7n6ly77ka7r")))

(define-public crate-genetic_algorithms-0.8.0 (c (n "genetic_algorithms") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0scb7i4ghscns6qdq7srxk6sndwqcs3pl4yzcaamhnvrfwsbi1nl")))

(define-public crate-genetic_algorithms-0.8.1 (c (n "genetic_algorithms") (v "0.8.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07q07py7gr4a9h3cg8yi45fchvwffgiqaw3iigb1d5jnjplkqicp") (r "1.60")))

(define-public crate-genetic_algorithms-0.8.2 (c (n "genetic_algorithms") (v "0.8.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10wawwvj63cawbcfcnsdshyvk2h27fyjgalh3gbikilg057iqfr5") (r "1.60")))

(define-public crate-genetic_algorithms-0.8.3 (c (n "genetic_algorithms") (v "0.8.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13v4i4wyk8smiplrx00b0iccywfqfca3ab4x6mjcmhkyq3fwn67y") (r "1.60")))

(define-public crate-genetic_algorithms-0.8.4 (c (n "genetic_algorithms") (v "0.8.4") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "149xi1gmc3xmvlrqc77xscg8wlhbnjf39jy6cvdy1yk6pj3qgj7d") (r "1.60")))

(define-public crate-genetic_algorithms-0.9.0 (c (n "genetic_algorithms") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11l22mkmxm0b25c4dvkz031c9msyfrif5dfd7q8lqcp0zhq9axsy") (r "1.60")))

(define-public crate-genetic_algorithms-0.9.1 (c (n "genetic_algorithms") (v "0.9.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1060wkq7026vzy4q1asg3ab1ssswg6xxymcnz280ki740cc8jyl5") (r "1.60")))

(define-public crate-genetic_algorithms-0.9.2 (c (n "genetic_algorithms") (v "0.9.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0amhsimln4djlfyh5mvzhv6jsq79s2y6fr6kdq97ymjkqzq17a72") (r "1.60")))

(define-public crate-genetic_algorithms-1.0.0 (c (n "genetic_algorithms") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "045xk2srf6bl1b2pqx78krlxcfwzg588f8vdv3y0m7iz0aj8r8pl") (r "1.60")))

(define-public crate-genetic_algorithms-1.0.1 (c (n "genetic_algorithms") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rq35hahliahhd6j5hvw8hpyq79xmmjkns39na68k2ayrssbqcsd") (r "1.60")))

(define-public crate-genetic_algorithms-1.1.0 (c (n "genetic_algorithms") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vw829k3rrhj57w0rs5yqsgz8mi0c0i2kwfdr01ybdnpy49jas7l") (r "1.60")))

(define-public crate-genetic_algorithms-1.2.0 (c (n "genetic_algorithms") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06rggx0sc948n72ly4la7hs9026hczzc1206smlczip4vgb06fr5") (r "1.60")))

(define-public crate-genetic_algorithms-1.3.0 (c (n "genetic_algorithms") (v "1.3.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w320w24a9k9jnd3wiqk1ff38khdsvsvdgsr6hxzwx0kif6ricwc") (r "1.60")))

(define-public crate-genetic_algorithms-1.3.1 (c (n "genetic_algorithms") (v "1.3.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ng5jmyfr7sdfh0m0c57wlf3zvsvpb74bc9kidlbhzq0fz63rxgv") (r "1.60")))

(define-public crate-genetic_algorithms-1.3.2 (c (n "genetic_algorithms") (v "1.3.2") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde" "kv_unstable"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ynv55n4h27jqlzhihpgkzcp4kxxg5s05rmf6qmp6hyrw8xgzfic") (r "1.60")))

