(define-module (crates-io ge ne general_audio) #:use-module (crates-io))

(define-public crate-general_audio-0.1.0 (c (n "general_audio") (v "0.1.0") (h "0z8byq8y9a3bl5imayphs5xnaz9mhq81j58l2ggfmlrzn6hgl367")))

(define-public crate-general_audio-0.1.1 (c (n "general_audio") (v "0.1.1") (h "03qm0qqr684sjbs0j4668ffb03g9cnrmgvrfy54mjr42fy4j2gyy")))

