(define-module (crates-io ge ne genere) #:use-module (crates-io))

(define-public crate-genere-0.0.1 (c (n "genere") (v "0.0.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1391w7l8x63rhdhqv8c0nns1paiqnhrnib6vvcs8kw6in31bc9q8")))

(define-public crate-genere-0.1.0 (c (n "genere") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17cwhjzs26nn6bp1admljzpbwpk1g9fvf6g3y8ngy40zbzlybh1q")))

(define-public crate-genere-0.1.1 (c (n "genere") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xq35bgljsc34lpmkjnbzwbwqa5az3haqx8rnd3np3hh8npbdmgz")))

(define-public crate-genere-0.1.2 (c (n "genere") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hd5mxbhm56s929ihq1pimf9ysavs0xvvfcyzp5dmlf00c5jjg5z")))

