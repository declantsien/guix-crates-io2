(define-module (crates-io ge ne generic-bytes) #:use-module (crates-io))

(define-public crate-generic-bytes-0.1.0 (c (n "generic-bytes") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14.3") (d #t) (k 0)))) (h "0fvpy0payvk4a6ic5wv4sgdlbmg0hbflhlwc1mjfl75xkf1qsqyn")))

(define-public crate-generic-bytes-0.2.0 (c (n "generic-bytes") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)))) (h "02clhp09567857s3wpfbcgg634rnjlf3iyf0kmgx5718nv9lc23r") (y #t)))

(define-public crate-generic-bytes-0.2.1 (c (n "generic-bytes") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.14.4") (d #t) (k 0)))) (h "1i36p9wh54a7lmj48x4ikq2zj15bdwa7cp9pmk614g2zazxvh9lg")))

(define-public crate-generic-bytes-0.2.2 (c (n "generic-bytes") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.14.6") (d #t) (k 0)))) (h "0alamqa0psrl82xb5qya69js1f0vphplz51gf69ck2spib1k8k30")))

