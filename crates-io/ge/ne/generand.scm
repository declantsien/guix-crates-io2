(define-module (crates-io ge ne generand) #:use-module (crates-io))

(define-public crate-generand-1.0.0 (c (n "generand") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (o #t) (d #t) (k 0)))) (h "1jar3pwxw8g7p7dkjkz0hjyq3grrp19smadia8s9vd7f8imy7idg") (f (quote (("default") ("bin" "clap" "terminal_size"))))))

(define-public crate-generand-1.0.1 (c (n "generand") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.17") (o #t) (d #t) (k 0)))) (h "090bqdc9crpqhdqw6dz3kzlp42kngy5flsxsz70srnmwfksvlj20") (f (quote (("default") ("bin" "clap" "terminal_size"))))))

