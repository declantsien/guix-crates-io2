(define-module (crates-io ge ne generational-cache) #:use-module (crates-io))

(define-public crate-generational-cache-0.1.0 (c (n "generational-cache") (v "0.1.0") (h "1pvczvswcz0zfbwpbd7gr0y3r2jidqfjmk6v20z1wmpx4zyc5a5g")))

(define-public crate-generational-cache-0.1.1 (c (n "generational-cache") (v "0.1.1") (h "0mhjcdxmnykl8yilzbhz1wfhcnwb3kya3192vkypxih1hgmgibpc")))

(define-public crate-generational-cache-0.1.2 (c (n "generational-cache") (v "0.1.2") (h "1h57f7p08dqj2x2cbc8zmwn1ixcdgjyw5mwgvblwhmj4dzc3g3nf")))

(define-public crate-generational-cache-0.2.0 (c (n "generational-cache") (v "0.2.0") (h "1r1k0x1lgqbddlxr5zny1x2zlmhg0nmjykxny26a7x1ivic6gjb0")))

(define-public crate-generational-cache-0.2.1 (c (n "generational-cache") (v "0.2.1") (h "1ll6ph8bk7sa9645d24dnjwajh7p66i14b10zk3007kn81f6z09d")))

(define-public crate-generational-cache-0.2.2 (c (n "generational-cache") (v "0.2.2") (h "0hs8k0wlan8qdcn2jnv324f8i4ig3164n3j7l59ffnw3cs8150cg")))

