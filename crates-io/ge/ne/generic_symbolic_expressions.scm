(define-module (crates-io ge ne generic_symbolic_expressions) #:use-module (crates-io))

(define-public crate-generic_symbolic_expressions-5.0.3 (c (n "generic_symbolic_expressions") (v "5.0.3") (h "1x1hirdb3gjybqhdsw8j101fdgc113l37qgk6lw8pc0p5wqh0ahn")))

(define-public crate-generic_symbolic_expressions-5.0.4 (c (n "generic_symbolic_expressions") (v "5.0.4") (h "0jqb9idcffzxr2cl5nm9vxc5mhrz8n561hsg54sikzbwzf2bazjr")))

