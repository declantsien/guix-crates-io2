(define-module (crates-io ge ne generic_singleton) #:use-module (crates-io))

(define-public crate-generic_singleton-0.1.0 (c (n "generic_singleton") (v "0.1.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)))) (h "0v5rhazw8kckxnmp9r5fmhb12jgra48fqm5jjs19cjqgigl1430v") (y #t)))

(define-public crate-generic_singleton-0.1.1 (c (n "generic_singleton") (v "0.1.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)))) (h "0p4mvz5gvh91jpl4qlv71j625mgxjk3vrnzzx86n8dk1pja8p12m") (y #t)))

(define-public crate-generic_singleton-0.1.2 (c (n "generic_singleton") (v "0.1.2") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)))) (h "01fd6xs0jh3006infdw8fbb9ydh293pyl74ddk3xp5866klp59f5") (y #t)))

(define-public crate-generic_singleton-0.1.3 (c (n "generic_singleton") (v "0.1.3") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)))) (h "0pr7g4gfgrjwlswgaf3ifr2cs33c53jphqyfnyzsh63gi2352aq7")))

(define-public crate-generic_singleton-0.2.0 (c (n "generic_singleton") (v "0.2.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)))) (h "07liclg1gr4iz9himcmvc9rk9257vnfavqmk77pvfbsm7dghq368") (y #t)))

(define-public crate-generic_singleton-0.3.0 (c (n "generic_singleton") (v "0.3.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0hia1w8kb2rngg2kddx9164sh7brz2yiwf14wax7c04z7q3nqhx1")))

(define-public crate-generic_singleton-0.3.1 (c (n "generic_singleton") (v "0.3.1") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1r1dsgiq0kgd0v7ajdign5rfjy8c4q5hl9rq7h7fg5q7bq1sfn73")))

(define-public crate-generic_singleton-0.3.2 (c (n "generic_singleton") (v "0.3.2") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0sg8qc092k8m6i9fid9cf53fawsf0nvkrl8i859q4ms19drj3mm6")))

(define-public crate-generic_singleton-0.4.0 (c (n "generic_singleton") (v "0.4.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "06nxmyzz35pcgvll31jlk4xvf9ikzy4qrxzpjnfvl63jwsky28nm")))

(define-public crate-generic_singleton-0.4.1 (c (n "generic_singleton") (v "0.4.1") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0id84i29gr167dcjkv4m9816085lr2f5xg18kw30b6qaxaidpwfv")))

(define-public crate-generic_singleton-0.5.0 (c (n "generic_singleton") (v "0.5.0") (d (list (d (n "anymap") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0a2q1xyha77k2lcinfd78wrcgk5mfypzwymbwjkyzmahb1qy6ryg")))

