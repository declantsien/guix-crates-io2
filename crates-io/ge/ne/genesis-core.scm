(define-module (crates-io ge ne genesis-core) #:use-module (crates-io))

(define-public crate-genesis-core-0.0.0 (c (n "genesis-core") (v "0.0.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "secp256k1") (r "^0.7.1") (d #t) (k 0)))) (h "0gib8j8l369sbs12jh00h24cb78q5vrwa07p9wxb826k24l7fa0w")))

