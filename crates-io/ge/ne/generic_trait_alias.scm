(define-module (crates-io ge ne generic_trait_alias) #:use-module (crates-io))

(define-public crate-generic_trait_alias-0.1.0 (c (n "generic_trait_alias") (v "0.1.0") (h "10qk31z55hd1gpnrc7wrpha5gz6x5wrhn28xib42f3rvpk6cxhga")))

(define-public crate-generic_trait_alias-0.1.1 (c (n "generic_trait_alias") (v "0.1.1") (h "12g43a102pkrz34j652f7fbmh12nwkks2qimcf60xadbr4niip4l")))

(define-public crate-generic_trait_alias-0.1.2 (c (n "generic_trait_alias") (v "0.1.2") (h "0z80rlq2qlkqyw9kjwxn5bz2afl78g826z0f17c60k6dpbr34xwl")))

