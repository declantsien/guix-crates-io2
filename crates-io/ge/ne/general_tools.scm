(define-module (crates-io ge ne general_tools) #:use-module (crates-io))

(define-public crate-general_tools-0.0.1 (c (n "general_tools") (v "0.0.1") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0pi1byvd6hi6dncrx2n7ab1ksyhlay3s3znxc89ldzqqmam0lxsf") (r "1.56.0")))

(define-public crate-general_tools-0.0.5 (c (n "general_tools") (v "0.0.5") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "0r84fmw7rz8jmnihl0v1rdgv9qvkaxmsrvwlfirqzswlmlvnp5ki") (r "1.56.1")))

(define-public crate-general_tools-0.0.6 (c (n "general_tools") (v "0.0.6") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "0y89vbfr66h7ld117cfcq2g0wb4jcydpavs96mqsij7h4s1k3l4j") (r "1.56.1")))

(define-public crate-general_tools-0.0.7 (c (n "general_tools") (v "0.0.7") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "1r1v003nankra4vxwjvvd4jn2kd82kdq493z6ksmxjdbfgfyc7l5") (r "1.56.1")))

(define-public crate-general_tools-0.0.75 (c (n "general_tools") (v "0.0.75") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "1rj7frw2l9mblw5m7x1cdk1s1z791gw07s5ii9r9qy8rnynn5g6a") (r "1.56.1")))

(define-public crate-general_tools-0.0.85 (c (n "general_tools") (v "0.0.85") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "0jprx7qwxbdxz50g17ybv087v4yhv3f2y437nynkp0fw6g0dbjjg") (r "1.56.1")))

(define-public crate-general_tools-0.1.0 (c (n "general_tools") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uint") (r "^0.9.1") (d #t) (k 0)))) (h "1bsfkdw8fidvj5hixwa5wzjknjz1c6zpj69y3yp5sji0imv4nnqg") (r "1.56.1")))

(define-public crate-general_tools-0.1.1 (c (n "general_tools") (v "0.1.1") (h "0qajwf4hzzq78cyyv7fx5fw8fkvfkxib7ihli2f0d49n70ibz4ja") (r "1.56.1")))

(define-public crate-general_tools-0.1.25 (c (n "general_tools") (v "0.1.25") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "14i5jrgiwzq40kfy2v5qng3sjpd33qvh1gf8sz4f4rv0nxcq81px") (r "1.56.1")))

