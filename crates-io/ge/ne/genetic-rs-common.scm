(define-module (crates-io ge ne genetic-rs-common) #:use-module (crates-io))

(define-public crate-genetic-rs-common-0.4.0 (c (n "genetic-rs-common") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0nsp6g1s5v4c6kg11235hl1lshbc2m0x628459csxqrnhyiw10s5") (f (quote (("speciation" "crossover") ("genrand") ("default" "builtin" "genrand") ("crossover" "builtin") ("builtin")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-genetic-rs-common-0.5.0 (c (n "genetic-rs-common") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "19v5lzparv8cs0ism5rb64vckpr0wgigi4z9znj2whda8p5q71d5") (f (quote (("speciation" "crossover") ("genrand") ("default" "builtin" "genrand") ("crossover" "builtin") ("builtin")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-genetic-rs-common-0.5.1 (c (n "genetic-rs-common") (v "0.5.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)))) (h "0jiizlvkcyq6dfvnfg7hhl6fh6ibw3a714gwww06mdncyviv0hag") (f (quote (("speciation" "crossover") ("genrand") ("default" "builtin" "genrand") ("crossover" "builtin") ("builtin")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

