(define-module (crates-io ge ne generational_indexlist) #:use-module (crates-io))

(define-public crate-generational_indexlist-0.1.0 (c (n "generational_indexlist") (v "0.1.0") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1kzj75vfwh19aqbss32hwx58vaa1y03yd17mbnhk0gkafhp9f60f") (y #t)))

(define-public crate-generational_indexlist-0.1.1 (c (n "generational_indexlist") (v "0.1.1") (d (list (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1psj14yighk4hqj7x62xd5c9mn6ymlgn5knj90smnzcx1a80cwxz") (y #t)))

