(define-module (crates-io ge ne generic-channel) #:use-module (crates-io))

(define-public crate-generic-channel-0.1.0 (c (n "generic-channel") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)))) (h "1p6k5xj18icn7k94y3kfy4hrdknp9m53ljzac5xlql9pqr980dks") (f (quote (("all" "futures" "crossbeam-channel"))))))

(define-public crate-generic-channel-0.1.1 (c (n "generic-channel") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)))) (h "1kf4waavp5q6mv2sbz312qnch875ajwlixpcw8g1gl8a4d6kafm5") (f (quote (("all" "futures" "crossbeam-channel"))))))

(define-public crate-generic-channel-0.2.0 (c (n "generic-channel") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)))) (h "1868dxpmfadqflx7gyxp2gmy00g5j32b8rah09hziq08fx10l6ld") (f (quote (("all" "futures" "crossbeam-channel"))))))

