(define-module (crates-io ge ne genenv) #:use-module (crates-io))

(define-public crate-genenv-1.0.0 (c (n "genenv") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.1") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0fc2q07z684lw7vb77r8sjfh9qq38wka34fc0pwra2vsl5kdp1g9")))

(define-public crate-genenv-1.1.0 (c (n "genenv") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.1") (d #t) (k 0)) (d (n "path-clean") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1rfz938c7a7ii23ksc6iq4bm580929z4a7ff6fi08vhzv2w4930s")))

