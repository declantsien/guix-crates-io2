(define-module (crates-io ge ne generatox) #:use-module (crates-io))

(define-public crate-generatox-0.1.0 (c (n "generatox") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "generatox-proc") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "1g09w4xcjfq1xyfwcm61xr45hlyyw4l5hpni66qwcpsikr620dxb") (f (quote (("nightly" "generatox-proc/nightly"))))))

(define-public crate-generatox-0.1.1 (c (n "generatox") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "generatox-proc") (r "^0.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "1kxm8qqzsipn15416fvhhrjxw61772ipgd227rcqjlf54lyfaq6l") (f (quote (("nightly" "generatox-proc/nightly"))))))

(define-public crate-generatox-0.1.2 (c (n "generatox") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "generatox-proc") (r "^0.1.2") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "0spwnplxrqlcbk8d6fbfhg667xbg2d5w74vw4s8k3pi42p4aqin6") (f (quote (("nightly" "generatox-proc/nightly"))))))

(define-public crate-generatox-0.1.3 (c (n "generatox") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "generatox-proc") (r "^0.1.3") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)))) (h "02nkdmkqkkc54kkfmfxabp41viv0fwk9xdm3zqpc39qyyywgv0kg") (f (quote (("nightly" "generatox-proc/nightly"))))))

