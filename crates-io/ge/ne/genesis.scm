(define-module (crates-io ge ne genesis) #:use-module (crates-io))

(define-public crate-genesis-0.0.0 (c (n "genesis") (v "0.0.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)))) (h "1q591sy8ynxwrnfj3b0dawni8g6wfyg2h20lfiisaby1rb24f6d0") (y #t)))

(define-public crate-genesis-0.0.1 (c (n "genesis") (v "0.0.1") (h "1pkynmw7pck5n2166fcws4apm6p9k2knf7d3br1b1pgqaxxhl1dv")))

(define-public crate-genesis-0.1.0 (c (n "genesis") (v "0.1.0") (d (list (d (n "genesis-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0npavmx23wqwma8yh2rv1faf1l5wl0q2fbmszimds29k5rfjwvgn")))

(define-public crate-genesis-0.1.1 (c (n "genesis") (v "0.1.1") (d (list (d (n "genesis-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z440pw2wpsb8y5rim9aiyf3xa6bicwj31rvi3dzw8h1sm7m5y68")))

(define-public crate-genesis-0.2.0 (c (n "genesis") (v "0.2.0") (d (list (d (n "genesis-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hzmpqddn2n4wvkjxyrh15x1n6zjfgg1v9s6l4li15fhrmxp05ps")))

(define-public crate-genesis-0.2.1 (c (n "genesis") (v "0.2.1") (d (list (d (n "genesis-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l99mj016r1iffwws8q982avcx4h9hagklxd1k7rypsr097q6dlj")))

(define-public crate-genesis-0.2.2 (c (n "genesis") (v "0.2.2") (d (list (d (n "genesis-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vj7mz6gky0axlz4sjgzk9lbhgbap54qmgk7g7dqw4zimi2qkz88")))

