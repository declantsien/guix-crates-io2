(define-module (crates-io ge ne genetic-files) #:use-module (crates-io))

(define-public crate-genetic-files-1.0.0 (c (n "genetic-files") (v "1.0.0") (d (list (d (n "clap") (r "^2.1.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4.11") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0cdglqk3084k8q9488yih16rb34abccq5x1spqim3i7i39ynvh0a")))

