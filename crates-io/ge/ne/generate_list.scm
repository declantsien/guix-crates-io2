(define-module (crates-io ge ne generate_list) #:use-module (crates-io))

(define-public crate-generate_list-0.1.1 (c (n "generate_list") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "19932rq5bpzv9n9zg1g0imjdg171s31cxir2ll4q384hbvx0k0pi")))

(define-public crate-generate_list-0.2.0 (c (n "generate_list") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1k4vzbvqjjwavzcbc1s8cx7nvh7285hjpxzd341l8s76l2br8vvk")))

(define-public crate-generate_list-0.2.1 (c (n "generate_list") (v "0.2.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1hcan3bckhjkc5mbmv8v5r01w4k15pnzfh4cd3bhq1mz7gckpj9v")))

