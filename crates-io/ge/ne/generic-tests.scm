(define-module (crates-io ge ne generic-tests) #:use-module (crates-io))

(define-public crate-generic-tests-0.1.0-beta.1 (c (n "generic-tests") (v "0.1.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0kxh8g57i5qajpzdiz6s3k6xvjzc0qc7hkf3ylk045dp66h73647") (f (quote (("test-compile-fail"))))))

(define-public crate-generic-tests-0.1.0 (c (n "generic-tests") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "169i8lpzbc2mz571zlgsbwi9qh1p5mvixfmcgn6bwg37n9a2w6wv") (f (quote (("test-compile-fail"))))))

(define-public crate-generic-tests-0.1.1 (c (n "generic-tests") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "04nardj45ajfyy7wckyd152gcbz1m9n57qqyfyz4kljkvfcafb29") (f (quote (("test-tokio" "tokio/macros" "tokio/rt-threaded" "tokio/io-util"))))))

(define-public crate-generic-tests-0.1.2 (c (n "generic-tests") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit" "visit-mut" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2") (d #t) (k 2)))) (h "0l6h7xv3fr0c16xqlqkq83abgxlpksmibn6fdqdm92fcvb09xczf") (f (quote (("test-tokio" "tokio/macros" "tokio/rt-multi-thread" "tokio/io-util"))))))

