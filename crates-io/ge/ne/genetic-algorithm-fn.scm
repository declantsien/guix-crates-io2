(define-module (crates-io ge ne genetic-algorithm-fn) #:use-module (crates-io))

(define-public crate-genetic-algorithm-fn-0.1.0 (c (n "genetic-algorithm-fn") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 0)) (d (n "genetic-algorithm-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01s87crndarhycywhiyadd09738ymgkq8m1mj7bz966qi2h0wgmh")))

