(define-module (crates-io ge ne generate-app-icons) #:use-module (crates-io))

(define-public crate-generate-app-icons-0.1.0 (c (n "generate-app-icons") (v "0.1.0") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "133yqmwia2fmrzzxrfn237vk1wz2da0w4bif86axdnsg6vn71x62")))

(define-public crate-generate-app-icons-0.1.1 (c (n "generate-app-icons") (v "0.1.1") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "12kwlb0nd41pq1x2dida44g598avdkj8p81bp0f55kg33glxivnh")))

(define-public crate-generate-app-icons-0.1.2 (c (n "generate-app-icons") (v "0.1.2") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "1nbaxpbl3r9irm3wgj6aclszr1gdrsv2wvlhasv77iraw0rkra3j")))

(define-public crate-generate-app-icons-0.1.3 (c (n "generate-app-icons") (v "0.1.3") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0hz8rfvyillqmlmwnbcjac9nliyni1fyq5ccq8c358qiggb538m0")))

(define-public crate-generate-app-icons-1.0.0 (c (n "generate-app-icons") (v "1.0.0") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1cy618wwz6s1az8ad1zn04lzzflhh5pcm19z0qkw92pc21j4fvmi")))

(define-public crate-generate-app-icons-1.0.1 (c (n "generate-app-icons") (v "1.0.1") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1kxfzwadgdpsqrv40qw2s196xm4pmnba36ia6wkxilwgn9sl0gyb")))

(define-public crate-generate-app-icons-1.0.2 (c (n "generate-app-icons") (v "1.0.2") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "061jqcznjzddv1kf7blhmmflxs0jmixxrhfyywlyffs80znq9wbw")))

(define-public crate-generate-app-icons-1.0.4 (c (n "generate-app-icons") (v "1.0.4") (d (list (d (n "doe") (r "^0.2.26") (d #t) (k 0)) (d (n "icns") (r "^0.3.1") (d #t) (k 0)) (d (n "ico") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "156n5a2lzc5gh2ryhny21l1pry2229k2rd8xc8lkyfmdi3948mx3")))

