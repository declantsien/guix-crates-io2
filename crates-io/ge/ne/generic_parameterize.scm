(define-module (crates-io ge ne generic_parameterize) #:use-module (crates-io))

(define-public crate-generic_parameterize-0.1.0 (c (n "generic_parameterize") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04ykdzav74ja8vmqsy767bv2iqvkgx32smq20vfcg44dkf868myr")))

(define-public crate-generic_parameterize-0.2.0 (c (n "generic_parameterize") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qw52qy9w38alvdq95lir22ycf8g3v3ji51girlqnybfxq37ynv6")))

(define-public crate-generic_parameterize-0.2.1 (c (n "generic_parameterize") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cns9xrkra0fl2gcv0462d7cp9viygrv8d9lw4rzbp7g0mk2wldd")))

(define-public crate-generic_parameterize-0.2.2 (c (n "generic_parameterize") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jlly7sdgwj5p2k88mid6mpg8nlc7mg7ghlbjs4ynx75bnnc54v7")))

