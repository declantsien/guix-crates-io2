(define-module (crates-io ge ne genetic-rs-macros) #:use-module (crates-io))

(define-public crate-genetic-rs-macros-0.4.0 (c (n "genetic-rs-macros") (v "0.4.0") (d (list (d (n "genetic-rs-common") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "09whbpzbmzi6kcks9fbngkawca6pyzlcxzvlsifzd0w4yjiv5zsd") (f (quote (("genrand") ("default" "genrand") ("crossover" "genetic-rs-common/crossover"))))))

(define-public crate-genetic-rs-macros-0.5.0 (c (n "genetic-rs-macros") (v "0.5.0") (d (list (d (n "genetic-rs-common") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "14rb695r9c36zr05kxyqx8svjkl08nvw3nhcnxnqw9jc3kfyqypx") (f (quote (("genrand") ("default" "genrand") ("crossover" "genetic-rs-common/crossover"))))))

(define-public crate-genetic-rs-macros-0.5.1 (c (n "genetic-rs-macros") (v "0.5.1") (d (list (d (n "genetic-rs-common") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (d #t) (k 0)))) (h "02hj5i2yp7vn83nv7xq8j6sf08k35sbsd3qc1yxkcs4swsww6pid") (f (quote (("genrand") ("default" "genrand") ("crossover" "genetic-rs-common/crossover"))))))

