(define-module (crates-io ge ne general-structs) #:use-module (crates-io))

(define-public crate-general-structs-0.1.0 (c (n "general-structs") (v "0.1.0") (h "0xc28y24g40sf56ravb71ikbv1y124ghi50gj309hfli8277wjj8") (r "1.68")))

(define-public crate-general-structs-0.2.0 (c (n "general-structs") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (d #t) (k 0)))) (h "0qrixwx92mynd80jg6pcf45f1cvrflw742bg3814wyqdlm2h2zav") (r "1.68")))

