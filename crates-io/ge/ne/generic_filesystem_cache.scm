(define-module (crates-io ge ne generic_filesystem_cache) #:use-module (crates-io))

(define-public crate-generic_filesystem_cache-0.1.0 (c (n "generic_filesystem_cache") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0i84zvfsbal4iyf8bik6ymjb3f5xvv5vr84dnmp6jjxdc7z49dk9")))

