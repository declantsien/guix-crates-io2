(define-module (crates-io ge ne generic_error) #:use-module (crates-io))

(define-public crate-generic_error-0.1.0 (c (n "generic_error") (v "0.1.0") (h "1kxfaf3r0cn50wvs6qzgzjq39qhf0ma7l9lwzn4jxaj7gqpcad8j")))

(define-public crate-generic_error-0.2.0 (c (n "generic_error") (v "0.2.0") (h "0w6b4gp24lxaw21gvcghr5hr1dv0arx8q31x50r4x0yga367k165")))

