(define-module (crates-io ge ne geneva) #:use-module (crates-io))

(define-public crate-geneva-0.1.0 (c (n "geneva") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.0.2") (d #t) (k 0)) (d (n "mio") (r "^0.8") (f (quote ("os-poll" "net"))) (d #t) (k 0)))) (h "0l4avwpgndxpjn03akjc6kymz1jbsqpyb26fyn32nj5l6m19axbz")))

