(define-module (crates-io ge ne generic-tls) #:use-module (crates-io))

(define-public crate-generic-tls-0.1.0 (c (n "generic-tls") (v "0.1.0") (d (list (d (n "rustls-pemfile") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 0)))) (h "0sh0b7fqnqziy55g22gqvy3r9djyilly1dda7r6zmb5smllp6k38")))

(define-public crate-generic-tls-0.1.1 (c (n "generic-tls") (v "0.1.1") (d (list (d (n "rustls-pemfile") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.23.4") (d #t) (k 0)))) (h "0ypwwdbvc3v2ls7yh7wbqh5f9dkj9w4sgvh3nc261fraycnaykln")))

