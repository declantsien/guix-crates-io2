(define-module (crates-io ge ne genesis-impl) #:use-module (crates-io))

(define-public crate-genesis-impl-0.0.0 (c (n "genesis-impl") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sbgbads59qc2mlczazjnzb9syc46b3invdy07712b58jg5cw128")))

(define-public crate-genesis-impl-0.1.0 (c (n "genesis-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "197qfqp068fzgjkzvqcxv80b70lc3j5ic5vvy9p6r4hk7g2g6xzw")))

(define-public crate-genesis-impl-0.1.1 (c (n "genesis-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wajddscq6p39kigzcs1alswqj8s1cn7pq55mb361ikxjmwlcavz")))

(define-public crate-genesis-impl-0.2.0 (c (n "genesis-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "119yfsvamlrs4jbnfji5jmb5dwf4yw7y6nziwzzwhgpw7kl4aqyq")))

(define-public crate-genesis-impl-0.2.2 (c (n "genesis-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d2f1h2f9vyxmgk6qq4zv2pmalmrfm28kim7zy2bg1rl9zwijxdh")))

