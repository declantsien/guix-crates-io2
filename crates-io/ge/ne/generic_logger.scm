(define-module (crates-io ge ne generic_logger) #:use-module (crates-io))

(define-public crate-generic_logger-0.1.0 (c (n "generic_logger") (v "0.1.0") (h "1hqiy0n0ywfis5inyj574s6z523rbavc4p4yn8gd5kn4v8wvsfr7")))

(define-public crate-generic_logger-0.2.0 (c (n "generic_logger") (v "0.2.0") (h "14pm79wx66bk7waz7gz2vvzjs92hy1shrfxb38xr5xzr0nr1jlch")))

(define-public crate-generic_logger-0.2.1 (c (n "generic_logger") (v "0.2.1") (h "0mvf95b0ds738bpgm8wj5qzgyxnx5klhfxgfpj50fbdmh5isymrr")))

(define-public crate-generic_logger-0.3.0 (c (n "generic_logger") (v "0.3.0") (h "1r6qas8x6yk8hr0amvq6g0vxrrfk3imx4v56zky2p2p4mim060s8")))

(define-public crate-generic_logger-0.4.0 (c (n "generic_logger") (v "0.4.0") (h "1bmhl2xms6fq5vpxzirmpgg1h5kj5lrvx8vvm09a9hqm0bf5yyfj")))

