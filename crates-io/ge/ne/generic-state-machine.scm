(define-module (crates-io ge ne generic-state-machine) #:use-module (crates-io))

(define-public crate-generic-state-machine-0.1.0 (c (n "generic-state-machine") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.13") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "time" "macros"))) (o #t) (d #t) (k 0)))) (h "1viz5aw71kr1r515mg4793x6qqvnd6yjsrhh6rh3xjn78xj2a785") (f (quote (("async" "tokio" "anyhow"))))))

