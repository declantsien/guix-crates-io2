(define-module (crates-io ge ne generativity) #:use-module (crates-io))

(define-public crate-generativity-1.0.0 (c (n "generativity") (v "1.0.0") (d (list (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "0pa36vfy5kgsz34526y90lh5cjiczaqsvv0q9wkhvw2ndbc3rdl2")))

(define-public crate-generativity-1.0.1 (c (n "generativity") (v "1.0.1") (d (list (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "1bn5gbb47arhsvqiyv79lk7jbbxnqjp7pdk5prss2x06qg3sb2am")))

(define-public crate-generativity-1.1.0 (c (n "generativity") (v "1.1.0") (d (list (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "1v7r2d7bmn9kgk16haw6fj129m29blmzv75ibf8f8gs3qb1y90aq") (r "1.56")))

