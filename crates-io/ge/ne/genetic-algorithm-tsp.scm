(define-module (crates-io ge ne genetic-algorithm-tsp) #:use-module (crates-io))

(define-public crate-genetic-algorithm-tsp-0.1.0 (c (n "genetic-algorithm-tsp") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 0)) (d (n "genetic-algorithm-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1c53skixpm7xwng4smayy7lv736drr7w0vf4c1f747yjx5xlnpmk")))

(define-public crate-genetic-algorithm-tsp-0.1.1 (c (n "genetic-algorithm-tsp") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 0)) (d (n "genetic-algorithm-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0hl2ywsrpzz69ka65lbjy6hrm3i57892akjah49g948c4s22rkcz")))

(define-public crate-genetic-algorithm-tsp-0.1.2 (c (n "genetic-algorithm-tsp") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 0)) (d (n "genetic-algorithm-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mcj8hq8ic08a04qdkpbid26l0cm6qa59ika8v1zpz7yb075h7mb")))

(define-public crate-genetic-algorithm-tsp-0.1.3 (c (n "genetic-algorithm-tsp") (v "0.1.3") (d (list (d (n "crossbeam-utils") (r "^0.8.6") (d #t) (k 0)) (d (n "fasthash-fork") (r "^0.4.1") (d #t) (k 0)) (d (n "genetic-algorithm-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1hmgpgqvmb1kb8xxxn468qdyp7mcfh104m3z1avglmkbwvl4mrja")))

