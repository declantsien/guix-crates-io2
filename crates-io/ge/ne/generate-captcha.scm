(define-module (crates-io ge ne generate-captcha) #:use-module (crates-io))

(define-public crate-generate-captcha-0.1.0 (c (n "generate-captcha") (v "0.1.0") (d (list (d (n "captcha") (r "^0.0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "09xbdwcdh36aylk2m7cgqb5xrda76c9h46jmxbr95hxxj7gl07fs")))

