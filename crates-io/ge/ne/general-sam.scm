(define-module (crates-io ge ne general-sam) #:use-module (crates-io))

(define-public crate-general-sam-0.1.0 (c (n "general-sam") (v "0.1.0") (h "05rp64ri8gdyp6lvaf9y28ycwylp4glz1rdxyc3x0asp06h1628p")))

(define-public crate-general-sam-0.1.1 (c (n "general-sam") (v "0.1.1") (h "1g26vrxp94i91pm87nrr88ac8s1pbhsxxkaw4hph5xh0cf9pgjf6")))

(define-public crate-general-sam-0.2.0 (c (n "general-sam") (v "0.2.0") (h "19g9zw5ziqs27vvl0jdgv1c81y2h4w1kvshmf74bvwvzvwr581ab")))

(define-public crate-general-sam-0.3.0 (c (n "general-sam") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0q2g0bwzldivi5j2shlsk89pc9h56lb7x5aii86gm3ii9vpl8hp7")))

(define-public crate-general-sam-0.4.0 (c (n "general-sam") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0n58v8hbzhvyrm50hipv4jmm19dqa52riki0831q2wy78k90ihpg") (f (quote (("trie") ("all" "trie"))))))

(define-public crate-general-sam-0.4.1 (c (n "general-sam") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17nllmrhrn3q0lzvz2qh07fjjg8pq0c5zw82zhpi4lv0kr61cwfq") (f (quote (("trie") ("all" "trie"))))))

(define-public crate-general-sam-0.4.2 (c (n "general-sam") (v "0.4.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07i21gsivcqijdwn7ggf9lin1vb43bmj5vmfgz4chvhaksr0c866") (f (quote (("trie") ("all" "trie"))))))

(define-public crate-general-sam-0.5.0 (c (n "general-sam") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.14.1") (d #t) (k 2)))) (h "168xqgpdl2avc6sw3zz8p6cn02rs0dpyl1wm64y5gnh1fpl44ii1") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.5.1 (c (n "general-sam") (v "0.5.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.14.1") (d #t) (k 2)))) (h "0p1wd4xamirh0q5l29ma45zg3mms4wzfsn2wgh204kwzaj42s6i7") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.5.2 (c (n "general-sam") (v "0.5.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.14.1") (d #t) (k 2)))) (h "1c103jk23wizvx5mmsin19agzdlyk51kan3dkh4cl30k7h4ayjgs") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.5.3 (c (n "general-sam") (v "0.5.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.14.1") (d #t) (k 2)))) (h "0dxdnnimh4mxf1mqfxg8fjdasrkjsr3z078zz8428271527790ni") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.5.4 (c (n "general-sam") (v "0.5.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.15.0") (d #t) (k 2)))) (h "0qkq933n5hhn0yjzxdld6x97ms01wd9mm5bw324kb8c0il1adgmi") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.6.0 (c (n "general-sam") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.15.0") (d #t) (k 2)))) (h "1mkfl8c9drg505lj6xy1yi4h98xn7gysdfm6qag8pckrzgg1wzy7") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.6.1 (c (n "general-sam") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.15.0") (d #t) (k 2)))) (h "0drqk9xal41g6hnijhapvlaiv4mkhc3d2zfqr1asf4sjb7lf6jp9") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-0.7.0 (c (n "general-sam") (v "0.7.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.15.2") (d #t) (k 2)))) (h "13gd8igqp3ici5si88wvzjlcvzjqjn93fk06fh1flvbnvljnra7q") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

(define-public crate-general-sam-1.0.0 (c (n "general-sam") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokenizers") (r "^0.15.2") (d #t) (k 2)))) (h "1i4gif4kgshhkbh7ndhq849407slln3lagjcdl3df1dkijnackmj") (f (quote (("utils" "rand") ("trie") ("all" "trie" "utils"))))))

