(define-module (crates-io ge ne generate-kml-for-images) #:use-module (crates-io))

(define-public crate-generate-kml-for-images-0.1.0 (c (n "generate-kml-for-images") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1g6jwvw2iwdp13an5mpn3sp71jizxpc2i4xlx2c0qdl5wy88nxx2")))

(define-public crate-generate-kml-for-images-0.1.1 (c (n "generate-kml-for-images") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1607yfmd9y2mjj6ccmpam4lf43mvrirsp83w0wjw3zb0zc80qd2f")))

(define-public crate-generate-kml-for-images-0.1.2 (c (n "generate-kml-for-images") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1kg7srixiqla81fihw381qmm99pqp5rpij4f8sfazslj58398ifa")))

(define-public crate-generate-kml-for-images-0.1.3 (c (n "generate-kml-for-images") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1r9vhij8n9lw6vv1syk416835p3vkhwc4mcm5ymjqmwcdg1xkaym")))

(define-public crate-generate-kml-for-images-0.1.4 (c (n "generate-kml-for-images") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "127818vghm1n8ashfi26p7w586ml9k57023rai18s5ngdnhf25ad")))

