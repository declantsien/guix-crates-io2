(define-module (crates-io ge ne generic-bnp) #:use-module (crates-io))

(define-public crate-generic-bnp-0.1.0 (c (n "generic-bnp") (v "0.1.0") (d (list (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "compare") (r "^0.1.0") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "gurobi") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "01dqlpl6rb5l4msjcjm0zw0ig2g8zc3zyhwzwa37gy8h6klh7ycz") (f (quote (("validity_assertions") ("locked_out") ("disable_ui") ("default" "gurobi") ("buffered_out") ("branch-graphviz"))))))

