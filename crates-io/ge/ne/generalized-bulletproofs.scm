(define-module (crates-io ge ne generalized-bulletproofs) #:use-module (crates-io))

(define-public crate-generalized-bulletproofs-0.1.0 (c (n "generalized-bulletproofs") (v "0.1.0") (d (list (d (n "ciphersuite") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "multiexp") (r "^0.4") (f (quote ("std" "batch"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "transcript") (r "^0.3") (d #t) (k 0) (p "flexible-transcript")) (d (n "zeroize") (r "^1.5") (f (quote ("zeroize_derive"))) (k 0)))) (h "13d73f5qc8rxdq5n2cdy9kgwfpbync0smmshxi8zjcmg38g3m970")))

