(define-module (crates-io ge ne generics-util) #:use-module (crates-io))

(define-public crate-generics-util-0.1.0 (c (n "generics-util") (v "0.1.0") (d (list (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a7wmxzfi9z1w7vlxhbx7bcds30l1hs98q0pnqj831lgzkcqpv3p")))

