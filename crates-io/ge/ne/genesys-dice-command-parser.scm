(define-module (crates-io ge ne genesys-dice-command-parser) #:use-module (crates-io))

(define-public crate-genesys-dice-command-parser-0.1.0 (c (n "genesys-dice-command-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0jbsfag127cl1sd4cflqjb7mkz28j3a47r6wafdi67g06rjdmpfy")))

(define-public crate-genesys-dice-command-parser-0.1.1 (c (n "genesys-dice-command-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "05x1i55iyzyg3cbi4cgmwipagi4vxs1lx4j654cr0wnd99z6qm12")))

