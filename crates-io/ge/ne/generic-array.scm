(define-module (crates-io ge ne generic-array) #:use-module (crates-io))

(define-public crate-generic-array-0.0.1 (c (n "generic-array") (v "0.0.1") (h "1xniakvrbw5wmmflpkrc5yh9d073xm87ndzhf2dgmijk1ywmfjn9")))

(define-public crate-generic-array-0.0.2 (c (n "generic-array") (v "0.0.2") (d (list (d (n "typenum") (r "^0.1.0") (d #t) (k 0)))) (h "0nhwzdqlwnxr6zrp8g34llfgb1g8mgyka8mmb8vfzsf1fsv7fizh")))

(define-public crate-generic-array-0.0.3 (c (n "generic-array") (v "0.0.3") (d (list (d (n "typenum") (r "^1.0.1") (d #t) (k 0)))) (h "0fckhrdi46q1s3bv680lbc0vhbzxb6wh12sj1fx1gra3nzhfdby3")))

(define-public crate-generic-array-0.1.0 (c (n "generic-array") (v "0.1.0") (d (list (d (n "typenum") (r "1.*") (d #t) (k 0)))) (h "1jh2ksjkb7scymjscyr47591wjnr9jv2dlss2kh5l08cc5snskz6")))

(define-public crate-generic-array-0.1.1 (c (n "generic-array") (v "0.1.1") (d (list (d (n "typenum") (r "1.*") (d #t) (k 0)))) (h "1qrw1zhzl8x3pvcy7zxp43hqdsmrdv5nrn4baanmc4z9z0x1dm05")))

(define-public crate-generic-array-0.1.2 (c (n "generic-array") (v "0.1.2") (d (list (d (n "typenum") (r "1.*") (d #t) (k 0)))) (h "10fnyf19f3rkdywmic2bssv588632gxm4w50s2q23hw33xxk58az") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.2.0 (c (n "generic-array") (v "0.2.0") (d (list (d (n "typenum") (r "1.*") (d #t) (k 0)))) (h "18sl2zjjdclpiq5y34wnx474w445pkkj23cfhjyj1wzjhp3firvw") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.2.1 (c (n "generic-array") (v "0.2.1") (d (list (d (n "typenum") (r "1.*") (d #t) (k 0)))) (h "0rvkla5pr6icjyj4yw64nvs13zx0jqrdar3rbgcgsi69bfbs61il") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.3.0 (c (n "generic-array") (v "0.3.0") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "0pycm3qrwbhiqrr65jpyk775vjm26kw475yxyrl6ziw6bwnv70m2") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.3.1 (c (n "generic-array") (v "0.3.1") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "1a6h7an2ajjs2r66j38kddn8b7ps4am7325zl8w288fsk7g47jj8") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.3.2 (c (n "generic-array") (v "0.3.2") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "11vi77ax5ynhwxzkj997777x2wr2xmadzv1d706r44dl3ipybn39") (f (quote (("no_std" "typenum/no_std"))))))

(define-public crate-generic-array-0.4.0 (c (n "generic-array") (v "0.4.0") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "1mdikw4ljxk8rg5wfwwsbdqi540zkwxn5yqsi323qr95yc58sf1r") (f (quote (("no_std" "typenum/no_std")))) (y #t)))

(define-public crate-generic-array-0.4.1 (c (n "generic-array") (v "0.4.1") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "1iybnhm7h03zh7p5mfvbl1s016q6k7z5cbaxsrqyv5a7mmg9jdjp") (f (quote (("no_std"))))))

(define-public crate-generic-array-0.5.0 (c (n "generic-array") (v "0.5.0") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "1xfp1zhrabf9xw1d3llgjmn70rwxfnf9baaz1wgylh6620b9dxmd")))

(define-public crate-generic-array-0.5.1 (c (n "generic-array") (v "0.5.1") (d (list (d (n "nodrop") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "0pmc6l9jk07f52y6d1cqzjnq3jpkrd0v9w6lbk4hzgrzzvgvhgss")))

(define-public crate-generic-array-0.6.0 (c (n "generic-array") (v "0.6.0") (d (list (d (n "nodrop") (r "^0.1.6") (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (d #t) (k 2)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "03605nzzwz5y4b01pks5p60hcd5m7abhl49abw6y16z7awkdhabj")))

(define-public crate-generic-array-0.7.0 (c (n "generic-array") (v "0.7.0") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "~0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 2)) (d (n "typenum") (r "~1.5") (d #t) (k 0)))) (h "05rcam07rshlyh7xnw8mfswbkngh1hq770s9p6dzifwdk6w2z6r2")))

(define-public crate-generic-array-0.7.1 (c (n "generic-array") (v "0.7.1") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "~0.9") (o #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 2)) (d (n "typenum") (r "~1.5") (d #t) (k 0)))) (h "0xnhkfcg8phjm8xzqh2abkzv563fmvxiyvyh9d32x32mcmsci1jm")))

(define-public crate-generic-array-0.7.2 (c (n "generic-array") (v "0.7.2") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "~0.9") (o #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 2)) (d (n "typenum") (r "~1.5") (d #t) (k 0)))) (h "0q663prkb3spbn4k9gs575vyxf7h1l7hz919075a3s160zv2029k")))

(define-public crate-generic-array-0.8.0 (c (n "generic-array") (v "0.8.0") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "~0.9") (o #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 2)) (d (n "typenum") (r "~1.7") (d #t) (k 0)))) (h "1fbkldvjps4vb2bqjm9bdc9dzgzvhzpa43vhkmcvy8dk7ik38f9a")))

(define-public crate-generic-array-0.8.1 (c (n "generic-array") (v "0.8.1") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "028s6fg0r6cn2kwc3hqnr27by4vbxk9kgz8j69bvdyx5fs6llx3v")))

(define-public crate-generic-array-0.8.2 (c (n "generic-array") (v "0.8.2") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "17cd5agm18vjcssgclqbfmbjijv5firkcy71sgsaqnlfqmwb70b1")))

(define-public crate-generic-array-0.8.3 (c (n "generic-array") (v "0.8.3") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1wi6rlx3dmrvl26yxm4z5n68kyj2ikk4nllk1kazw2ik9scnkszw")))

(define-public crate-generic-array-0.7.3 (c (n "generic-array") (v "0.7.3") (d (list (d (n "nodrop") (r "^0.1") (k 0)) (d (n "serde") (r "^0.9") (o #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 2)) (d (n "typenum") (r "^1.5") (d #t) (k 0)))) (h "19mnvzjmshs53d3lsbdcw631cdlhyn0xx1vhi4vpjac5kg7l7q6v")))

(define-public crate-generic-array-0.9.0 (c (n "generic-array") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "17avshwh41d1w9p75mw2k6pflmbaj9ra7svxplmhqmv76xlca9gg")))

(define-public crate-generic-array-0.10.0 (c (n "generic-array") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0i3rmwxd0v38gsp2xm8sw6v7qr229j1wfk2izvhibpj0wf5v2kqn")))

(define-public crate-generic-array-0.11.0 (c (n "generic-array") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0b58cjils09a9h853qvxgvpags85kyp3ixh3mzwdwyx7y6nbv48m")))

(define-public crate-generic-array-0.11.1 (c (n "generic-array") (v "0.11.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0sgkh1rgdizc6zq9lpqs7ad60n2a9fak60dnf65q8368g3xdl1w1")))

(define-public crate-generic-array-0.12.0 (c (n "generic-array") (v "0.12.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "14lmhgcspg7zjv43sd1q6xlh12f1lwnpli5gfl0rdddzyp12h3rw")))

(define-public crate-generic-array-0.12.1 (c (n "generic-array") (v "0.12.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "14hmkiykcw955v0g4x0ryfzzf2fx13z26p3ibq3v9bmxqvhwr1zp") (y #t)))

(define-public crate-generic-array-0.12.2 (c (n "generic-array") (v "0.12.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "10psk92mhkq7bvj7jvjlw0zymdcp7xx11b0d7zvh21ga8vyi19yb") (y #t)))

(define-public crate-generic-array-0.13.0 (c (n "generic-array") (v "0.13.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1xn51659qkdvv78nrsy4sdjngi1d1k0blkcyxh3hsixd1x108f8x")))

(define-public crate-generic-array-0.13.1 (c (n "generic-array") (v "0.13.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1prbl4jssh6x3qjbvr1i3z4dm2id4d8pycrpqrsp8mnd3r88558x")))

(define-public crate-generic-array-0.12.3 (c (n "generic-array") (v "0.12.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1v5jg7djicq34nbiv1dwaki71gkny002wyy9qfn3y0hfmrs053y6")))

(define-public crate-generic-array-0.13.2 (c (n "generic-array") (v "0.13.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1kddwxpd58y807y1r3lijg7sw3gxm6nczl6wp57gamhv6mhygl8f") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.0 (c (n "generic-array") (v "0.14.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0ybxi0waf41193mb6wk9wfrk8y4rvjw5b1vs4jcn2yilj8zc401d") (f (quote (("more_lengths")))) (y #t)))

(define-public crate-generic-array-0.14.1 (c (n "generic-array") (v "0.14.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1srd08b3z28n4knm4vyql5wi6ry3d96b058hycv90108rz1689kd") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.2 (c (n "generic-array") (v "0.14.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "107r1fpm8zcab3lzci4x9par6ik8bra390c60rhxvnmz7dgnlx5c") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.3 (c (n "generic-array") (v "0.14.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0qry0358gdy0zg7x4b5j8ypw0v02glxrlk96f6j7hbx5pfv4pyv0") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.4 (c (n "generic-array") (v "0.14.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "05qqwm9v5asbil9z28wjkmpfvs1c5c99n8n9gwxis3d3r3n6c52h") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.12.4 (c (n "generic-array") (v "0.12.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1gfpay78vijl9vrwl1k9v7fbvbhkhcmnrk4kfg9l6x24y4s9zpzz")))

(define-public crate-generic-array-0.13.3 (c (n "generic-array") (v "0.13.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "02g3zhqc086zmsb6kcmjs2fiprz8gq12g0xbm9g23215ydxfd5zp") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.11.2 (c (n "generic-array") (v "0.11.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0a7w8w0rg47nmcinnfzv443lcyb8mplwc251p1jyr5xj2yh6wzv6")))

(define-public crate-generic-array-0.10.1 (c (n "generic-array") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "165a0rrpsrwzj490svqryyjca63y7d3cbzigsjgg8zfc1zal7ra4")))

(define-public crate-generic-array-0.9.1 (c (n "generic-array") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "0zmv3zdfih05yjzzksr76316yx9hdb520miyd0ffiifaxn63403d")))

(define-public crate-generic-array-0.8.4 (c (n "generic-array") (v "0.8.4") (d (list (d (n "nodrop") (r "~0.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.9") (d #t) (k 0)))) (h "1gkyr94q67gq31xa3qzmkpsjhh4jlgf54jx21lw2wlgawfq7yadj")))

(define-public crate-generic-array-0.14.5 (c (n "generic-array") (v "0.14.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "00qqhls43bzvyb7s26iw6knvsz3mckbxl3rhaahvypzhqwzd6j7x") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.6 (c (n "generic-array") (v "0.14.6") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1fgi07v268jd0mr6xc42rjbq0wzl8ngsgp5b8wj33wwpfaa9xx5z") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-0.14.7 (c (n "generic-array") (v "0.14.7") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "16lyyrzrljfq424c3n8kfwkqihlimmsg5nhshbbp48np3yjrqr45") (f (quote (("more_lengths"))))))

(define-public crate-generic-array-1.0.0-alpha.0 (c (n "generic-array") (v "1.0.0-alpha.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "const-default") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.16") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "048pyqaxricbh02brvnc6h9zyq9jkrs2dfmzwsjqqhqy5klx14g8") (f (quote (("internals") ("alloc")))) (y #t)))

(define-public crate-generic-array-1.0.0-alpha.1 (c (n "generic-array") (v "1.0.0-alpha.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "const-default") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.16") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0v32ypfphkxkfc5r273srl7kqmvnfaa81yhg17sy44is133i4b5m") (f (quote (("internals") ("alloc")))) (y #t)))

(define-public crate-generic-array-1.0.0-beta.2 (c (n "generic-array") (v "1.0.0-beta.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "const-default") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.16") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "06hih43w2lbsfkjw5p0ygh6rm3vgznvafs0dnz2lf2f6x1wma9s1") (f (quote (("internals") ("alloc")))) (y #t)))

(define-public crate-generic-array-1.0.0 (c (n "generic-array") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "const-default") (r "^1") (o #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "faster-hex") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typenum") (r "^1.16") (f (quote ("const-generics"))) (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "1c1nb06dzirrqq3m838nkw5i71anv6nvdk7dh3h28r20lm29jwzy") (f (quote (("internals") ("alloc"))))))

