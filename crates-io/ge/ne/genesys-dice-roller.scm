(define-module (crates-io ge ne genesys-dice-roller) #:use-module (crates-io))

(define-public crate-genesys-dice-roller-0.1.0 (c (n "genesys-dice-roller") (v "0.1.0") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0cysckxav3rpy6as76r3h7vg2qj18s4hbxwr5w3748z67bygxm9d")))

(define-public crate-genesys-dice-roller-0.1.1 (c (n "genesys-dice-roller") (v "0.1.1") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1c9hcla5jr4aw7qhv1bbzzi1k4820zafmwzgd8d6yy7r1d8bqi6f")))

(define-public crate-genesys-dice-roller-0.2.0 (c (n "genesys-dice-roller") (v "0.2.0") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19sq8d799jfhjgg9w52prds7c9xzmjqmkprkdw9x2nm3437d8kk9")))

(define-public crate-genesys-dice-roller-0.2.1 (c (n "genesys-dice-roller") (v "0.2.1") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1z62laz53b17xaipxwil5xzzh3hyzr6mv3hc1a8shislak3wkb25")))

(define-public crate-genesys-dice-roller-0.2.2 (c (n "genesys-dice-roller") (v "0.2.2") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1drs8wbhdw883s3wcdyw2lg2isnz295gwhns8ap3dz0fcyqqd141")))

(define-public crate-genesys-dice-roller-0.2.3 (c (n "genesys-dice-roller") (v "0.2.3") (d (list (d (n "genesys-dice-command-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0v5hdjrpn9a470m1fmapidx3bbjvlam6wylxp2cjyfdfyxbdb8zr")))

