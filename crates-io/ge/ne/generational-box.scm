(define-module (crates-io ge ne generational-box) #:use-module (crates-io))

(define-public crate-generational-box-0.1.0 (c (n "generational-box") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "04274yd23ahap6ijs1lwi04h35yzpnccbh1g6g785ydbnpi399w2") (f (quote (("default" "check_generation") ("check_generation"))))))

(define-public crate-generational-box-0.4.3 (c (n "generational-box") (v "0.4.3") (d (list (d (n "bumpalo") (r "^3.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1sab4gln6ysyh9fc58akmil2j2bbahys8bk7j3yy01ycb6b8k5ki") (f (quote (("default" "check_generation") ("check_generation"))))))

(define-public crate-generational-box-0.5.0-alpha.0 (c (n "generational-box") (v "0.5.0-alpha.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ahj2vb1crgclcykdixn55kfn0j1ga5zmmyada3clgqiqh8vpyj6") (f (quote (("default" "check_generation") ("debug_ownership") ("debug_borrows") ("check_generation"))))))

(define-public crate-generational-box-0.5.0-alpha.1 (c (n "generational-box") (v "0.5.0-alpha.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1gjj78daal4izzwqn3j9qwifiil081cfqyyjmhydywslxq7lkd02") (f (quote (("default" "check_generation") ("debug_ownership") ("debug_borrows") ("check_generation"))))))

(define-public crate-generational-box-0.5.0-alpha.2 (c (n "generational-box") (v "0.5.0-alpha.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1va396jvfxgh972ws0x8m8dczkd9n9zvzdpz1rf3q62qsdm0q3l9") (f (quote (("default" "check_generation") ("debug_ownership") ("debug_borrows") ("check_generation"))))))

(define-public crate-generational-box-0.5.0 (c (n "generational-box") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1p8a616ayis89hzd947k8n0z6gfnkin7g058snykzag2sxmll7w7") (f (quote (("default" "check_generation") ("debug_ownership") ("debug_borrows") ("check_generation"))))))

(define-public crate-generational-box-0.5.1 (c (n "generational-box") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0swn9lgikb4m9cwcfm9g3q8mgkhasmgrdbnfcyh04p5qjsf92cpi") (f (quote (("default" "check_generation") ("debug_ownership") ("debug_borrows") ("check_generation"))))))

