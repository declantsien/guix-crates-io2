(define-module (crates-io ge ne generating-proc-macro-crate) #:use-module (crates-io))

(define-public crate-generating-proc-macro-crate-0.1.0 (c (n "generating-proc-macro-crate") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0xi8rzfbfx5kxsh880kyyxdvqhkfs4v12vhhhxdaq6v01ap2dy3i")))

(define-public crate-generating-proc-macro-crate-0.1.1 (c (n "generating-proc-macro-crate") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1gy7swxa85wx75w8zml822m89lisvfx25wqpc5n7g7rnilanpg15")))

(define-public crate-generating-proc-macro-crate-0.1.2 (c (n "generating-proc-macro-crate") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1n5w3g0i3npwr88gm7kis39lcfkbcw2q0dp97mpxzx993xlb6iy6")))

(define-public crate-generating-proc-macro-crate-0.1.3 (c (n "generating-proc-macro-crate") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05ff1xf963z3713l55dzy38l3pdy0kzpkwvdglhgm154qlm577aq")))

