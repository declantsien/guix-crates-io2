(define-module (crates-io ge ne generational_array) #:use-module (crates-io))

(define-public crate-generational_array-0.1.0 (c (n "generational_array") (v "0.1.0") (h "0n2byz6l49amnl90f5pwgcsqkjnaxxm7dmd3ywlzwz6fq37g0x7j")))

(define-public crate-generational_array-0.1.1 (c (n "generational_array") (v "0.1.1") (h "16di494v3xph7qm18ldikviiv22ypksck6rxk24k34b9adw6swk4")))

