(define-module (crates-io ge ne genelf) #:use-module (crates-io))

(define-public crate-genelf-0.1.0 (c (n "genelf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "096ax9fliw4bv28zic8k0qv10bmqb0jm8qzfxzv1dnn3kim3sa1b")))

(define-public crate-genelf-0.1.1 (c (n "genelf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "16s97j6skd82lhyw45q96nh11ld2wglb90fg4fyay35r09csfid8")))

(define-public crate-genelf-0.1.2 (c (n "genelf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1qncacrs26h3wmixl9mshqjaz9nc5archx3bchq4mqd4s1x1bidx")))

(define-public crate-genelf-0.1.3 (c (n "genelf") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1giq8002rb7bdkrqr350ys1z1fky0h6hjidl61jsfyw9mv3c7iln")))

(define-public crate-genelf-0.1.4 (c (n "genelf") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1fxvvpjn494a354szm8sgaqj81vqd4ww8as59hx0g3x913if6ra2")))

(define-public crate-genelf-0.1.5 (c (n "genelf") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1mac2gs7na0vm5h641m07s36iwjy4rqs4lcyizhzxg07hb3ic584")))

(define-public crate-genelf-0.1.6 (c (n "genelf") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "00dwlg4kvy5c53yrv2b5wlyaw45ykmbivjwhrfxrbk85g9sb6d8y")))

(define-public crate-genelf-0.1.7 (c (n "genelf") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0i8ylh84d5pais240hs0lrhwghpml6v0mfa0m0cik7aw3fb252qv")))

(define-public crate-genelf-0.1.8 (c (n "genelf") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "139k7n5q37155127yw5y39pzy0ypqxvxmazy25mc6wcnzq3v94mn")))

(define-public crate-genelf-0.1.9 (c (n "genelf") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0wijvgcrwa6hc3i30a78ysa6m04r5g8qc00cq9i4fhpynk9zbw09")))

(define-public crate-genelf-0.2.0 (c (n "genelf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1qbdb9f993qrgy1vamf6r2443bs0n03kfh3053vcjnd52x8rqb7a")))

(define-public crate-genelf-0.2.1 (c (n "genelf") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1av6xqc4pm50kq982kw2jrckp642hiklg3ry8jn152sdqn9kmww0")))

(define-public crate-genelf-0.2.2 (c (n "genelf") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "06as5z96yllh3dsx5kjhh8h1s3fcd95vpkfzvh2kzgzm43gk6x6w")))

(define-public crate-genelf-0.2.3 (c (n "genelf") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "0cc1shyavx7wgbxrl81zyl5wxslbvqiysshsp6h69ydh71h4k51y")))

(define-public crate-genelf-0.2.4 (c (n "genelf") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1jskm7j1v20i51q9fk0dy7wp4hrh8v9hci78wj47xqni1k5mn1bq")))

