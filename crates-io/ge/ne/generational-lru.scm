(define-module (crates-io ge ne generational-lru) #:use-module (crates-io))

(define-public crate-generational-lru-0.1.0 (c (n "generational-lru") (v "0.1.0") (h "116bypr8gzjdsyswfdwx1830w30x1w6nwm3wlc1gfpq3kcsvzqbv")))

(define-public crate-generational-lru-0.1.1 (c (n "generational-lru") (v "0.1.1") (h "1b8x7fn3n1mpkyd3jk0f20sgx11wrj2bhk7fvaacwqg2apxv7m1s")))

(define-public crate-generational-lru-0.1.2 (c (n "generational-lru") (v "0.1.2") (h "1hk20240zdcnr9a8m3bg0f6b1976cxda3660s7rlrdq88vz554ad")))

