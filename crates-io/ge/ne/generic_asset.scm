(define-module (crates-io ge ne generic_asset) #:use-module (crates-io))

(define-public crate-generic_asset-0.1.0 (c (n "generic_asset") (v "0.1.0") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0779dg2ys54nrm8rcmwawwwnpf1vh804zk1fkahaldqxk4jqdvjb")))

(define-public crate-generic_asset-0.1.1 (c (n "generic_asset") (v "0.1.1") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0714g3x20d1xh9mq3yjagvy0krcxwqq99qm4i0cffar9bz93q99f")))

(define-public crate-generic_asset-0.1.11 (c (n "generic_asset") (v "0.1.11") (d (list (d (n "bevy") (r "^0.12.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "18z0s8p1qhpivl19xp45wvf09j2bnh88n76az5i0a7wi31kh747x")))

(define-public crate-generic_asset-0.1.12 (c (n "generic_asset") (v "0.1.12") (d (list (d (n "bevy") (r "^0.12.1") (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1w6gm8y410a194h3532lvar4rs25kj60kzq9vyqznjdraj40zs1w")))

(define-public crate-generic_asset-0.1.13 (c (n "generic_asset") (v "0.1.13") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "0dy9qdvmssqn55phqi5ndpmiyg1ggna04xn6vx14nllmbvy0vjm5")))

(define-public crate-generic_asset-0.1.14 (c (n "generic_asset") (v "0.1.14") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1wbn1rnjpkgfdn648av9nyzyzvbznvdgradf1rw2c9m6y64wh494")))

(define-public crate-generic_asset-0.1.15 (c (n "generic_asset") (v "0.1.15") (d (list (d (n "bevy") (r "^0.12.1") (f (quote ("bevy_asset"))) (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)))) (h "1pqnawjdbzg90hhfwgnixjavyjyfkhxn3mr3kqs54ibg9nqgd2wr")))

