(define-module (crates-io ge ne generational_vector) #:use-module (crates-io))

(define-public crate-generational_vector-0.1.0 (c (n "generational_vector") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "15hrpl0fas4hbldxq5c979wi1pxvm2k7rjiiy3qg372dbjrxzini")))

(define-public crate-generational_vector-0.2.0 (c (n "generational_vector") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "11lr3s8sbf3lp48372bcvrbw2jy5mzw1y6arjzgijg23863iz775")))

(define-public crate-generational_vector-0.2.1 (c (n "generational_vector") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0w46bja5dp9jlnxvlbq96ypvxmd9dz8ylmvyipzdg4m9k86acx05")))

(define-public crate-generational_vector-0.3.0 (c (n "generational_vector") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("alloc"))) (o #t) (d #t) (k 0)))) (h "1wi7zamlxq198nzd8wmvhr64y8kyl5s2ja6lv7zdx5gzimk37vfn") (s 2) (e (quote (("tinyvec" "dep:tinyvec") ("smallvec" "dep:smallvec"))))))

