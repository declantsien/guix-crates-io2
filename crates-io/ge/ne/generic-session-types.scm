(define-module (crates-io ge ne generic-session-types) #:use-module (crates-io))

(define-public crate-generic-session-types-0.1.0 (c (n "generic-session-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y6dj38d2aj9yilvm3p9m67aqz6zs076fpp0hcd2c9l70nbyj33z") (y #t)))

(define-public crate-generic-session-types-0.1.1 (c (n "generic-session-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17kxazxfmf2xmlqv0pni75qgxr4d06fis81qgjlpq7vyr2pxpm41")))

