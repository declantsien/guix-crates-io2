(define-module (crates-io ge ne generate-random-macro) #:use-module (crates-io))

(define-public crate-generate-random-macro-0.1.0 (c (n "generate-random-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1karl0ff365a927wf6wz1zccf9lpjzv0yzy0h4niidzml0njv331")))

