(define-module (crates-io ge ne generic_cong_avoid) #:use-module (crates-io))

(define-public crate-generic_cong_avoid-0.2.0 (c (n "generic_cong_avoid") (v "0.2.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "portus") (r "^0.4") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1r3v7znpbjq3d5cglzr71i87scrcvch3ma7nic029wgzbqfa5k56")))

(define-public crate-generic_cong_avoid-0.2.1 (c (n "generic_cong_avoid") (v "0.2.1") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "portus") (r "^0.5") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0xhd5pdh3q3g4sz5hcr7y5k7n44y0jf4spxa43llklcy2raxk44d")))

(define-public crate-generic_cong_avoid-0.3.0 (c (n "generic_cong_avoid") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "portus") (r "^0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1ga4l7lbnzw54vaa5x3l3irxrksfwrm2azg8rsknsr9vqlyk37zb") (f (quote (("bin" "tracing-subscriber"))))))

(define-public crate-generic_cong_avoid-0.4.0 (c (n "generic_cong_avoid") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "portus") (r "^0.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0aakw3hzdrahp82a8jr6945mnma7x0h6fqv4x493qf6g9ck4fay2") (f (quote (("bin" "tracing-subscriber"))))))

