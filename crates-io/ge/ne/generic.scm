(define-module (crates-io ge ne generic) #:use-module (crates-io))

(define-public crate-generic-0.0.1 (c (n "generic") (v "0.0.1") (d (list (d (n "generic-core") (r "^0.0.1") (d #t) (k 0)) (d (n "generic-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0v1y7m8bnyxj4d2czjkajm4y0qdmg9n2lxsjfp7nhayflm0s2qg3")))

(define-public crate-generic-0.0.2 (c (n "generic") (v "0.0.2") (d (list (d (n "generic-core") (r "^0.0.2") (d #t) (k 0)) (d (n "generic-derive") (r "^0.0.2") (d #t) (k 0)))) (h "1akfikkp2dmqx914y9qphpj8s5w8ha6hq4rbsg7liqj8shby40xk")))

