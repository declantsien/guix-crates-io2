(define-module (crates-io ge ne generic-arrayvec) #:use-module (crates-io))

(define-public crate-generic-arrayvec-0.0.1 (c (n "generic-arrayvec") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.1") (d #t) (k 0)))) (h "10wqb3sikr6x97rmzlavq21g94rvqsqlp3iib8hbfgbdp0pqgax0")))

(define-public crate-generic-arrayvec-0.0.2 (c (n "generic-arrayvec") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.1") (d #t) (k 0)))) (h "0w99q74fg66q7q8d9n8xyw61ncp0glsy71wfy32nhab7rbji2x77")))

(define-public crate-generic-arrayvec-0.1.0 (c (n "generic-arrayvec") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)))) (h "1wrq1nz7p263nf2bv41wxrm3rw6gqca0sdfa3han43sm093if5ia")))

(define-public crate-generic-arrayvec-0.2.0 (c (n "generic-arrayvec") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.7") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.0") (d #t) (k 0)))) (h "1aakrvl5pnb1q4yf0ida9z7srhvb3jn37w45mw6qd4pmvzyyr8l8")))

(define-public crate-generic-arrayvec-0.3.0 (c (n "generic-arrayvec") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0xf64aq2b91xgafj4fbr0xcp9cvmj6smac54yliffz7s1nsw4z9g") (f (quote (("generic-array-more-lengths" "generic-array/more_lengths") ("arrayvec-sizes-33-128" "arrayvec/array-sizes-33-128") ("arrayvec-sizes-129-255" "arrayvec/array-sizes-129-255"))))))

(define-public crate-generic-arrayvec-0.3.1 (c (n "generic-arrayvec") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0yxv59l6zcbvx5nl7xz2ibq076p5ln38ab3dwygxm0xhyyd4d5cr") (f (quote (("generic-array-more-lengths" "generic-array/more_lengths") ("arrayvec-sizes-33-128" "arrayvec/array-sizes-33-128") ("arrayvec-sizes-129-255" "arrayvec/array-sizes-129-255"))))))

(define-public crate-generic-arrayvec-0.4.0 (c (n "generic-arrayvec") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.5") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cy5zrryyshsabsh53migxln83vasc808d8g0pqw02i1w01hscwh") (f (quote (("std" "arrayvec/std") ("serde" "arrayvec/serde") ("generic-array-more-lengths" "generic-array/more_lengths") ("default" "std") ("arrayvec-sizes-33-128" "arrayvec/array-sizes-33-128") ("arrayvec-sizes-129-255" "arrayvec/array-sizes-129-255"))))))

