(define-module (crates-io ge ne generic-floyd-warshall) #:use-module (crates-io))

(define-public crate-generic-floyd-warshall-0.1.0 (c (n "generic-floyd-warshall") (v "0.1.0") (h "1gjyq3381h2dn038crwslnzyyr88q0z2mb83vrbsd4a2vvshrd56")))

(define-public crate-generic-floyd-warshall-0.2.0 (c (n "generic-floyd-warshall") (v "0.2.0") (h "07wj3r2cydf4z96f4hdjc0v74bh4pnybyj5m5waxpys1zwdpbz68")))

