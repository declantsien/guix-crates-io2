(define-module (crates-io ge ne generator_ext) #:use-module (crates-io))

(define-public crate-generator_ext-0.1.0 (c (n "generator_ext") (v "0.1.0") (h "1mqv4hmrg2mia0a36rh2ybnbr9czvr9kbdvyg92zjfm82jbbfk26")))

(define-public crate-generator_ext-0.1.1 (c (n "generator_ext") (v "0.1.1") (h "0bcw7q5q96lllj8rn9j236lni3d98pg01nlh7xy12ldbhmkywc72")))

(define-public crate-generator_ext-0.1.2 (c (n "generator_ext") (v "0.1.2") (d (list (d (n "futures") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1hilrxl9rw8savqxx2b3ifn7gvn0a99dl76zfdp2bla5n0kfq0gp") (f (quote (("extfutures" "futures"))))))

