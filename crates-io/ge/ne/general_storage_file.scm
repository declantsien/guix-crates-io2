(define-module (crates-io ge ne general_storage_file) #:use-module (crates-io))

(define-public crate-general_storage_file-0.1.0 (c (n "general_storage_file") (v "0.1.0") (d (list (d (n "general_storage") (r "^0.1") (d #t) (k 0)))) (h "1w10g99r52xsw7lv5wzcwrl0ncn8rr3y7laxlilgb93x8hidvngb") (f (quote (("yaml" "general_storage/yaml") ("toml" "general_storage/toml") ("json" "general_storage/json") ("compress" "general_storage/compress") ("bincode" "general_storage/bincode"))))))

(define-public crate-general_storage_file-0.1.1 (c (n "general_storage_file") (v "0.1.1") (d (list (d (n "general_storage") (r "^0.1") (d #t) (k 0)))) (h "0jdsxw8h0h8ix2w7k0qnany9v5h8annl96b8wzis0ypdgp6qc635") (f (quote (("yaml" "general_storage/yaml") ("toml" "general_storage/toml") ("json" "general_storage/json") ("compress" "general_storage/compress") ("bincode" "general_storage/bincode"))))))

(define-public crate-general_storage_file-0.2.0 (c (n "general_storage_file") (v "0.2.0") (d (list (d (n "general_storage") (r "^0.2") (d #t) (k 0)))) (h "1i3hh831fz8ks5vc7f3b0hd0xnsw68s9nl59ajkm020jxz009ifb") (f (quote (("yaml" "general_storage/yaml") ("toml" "general_storage/toml") ("json" "general_storage/json") ("compress" "general_storage/compress") ("bincode" "general_storage/bincode"))))))

(define-public crate-general_storage_file-0.2.1 (c (n "general_storage_file") (v "0.2.1") (d (list (d (n "general_storage") (r "^0.2") (d #t) (k 0)))) (h "0fpfvk6awdsdh1g38z9mlycpbqwdjwiv1mlqz8iaq158rsf0rjdx") (f (quote (("yaml" "general_storage/yaml") ("toml" "general_storage/toml") ("json" "general_storage/json") ("compress" "general_storage/compress") ("bincode" "general_storage/bincode"))))))

(define-public crate-general_storage_file-0.3.0 (c (n "general_storage_file") (v "0.3.0") (d (list (d (n "general_storage") (r "^0.3") (d #t) (k 0)))) (h "1d2xfdr8a9z6nqncp8bhni792zfznv35jvxhxf01yqnfmcx05jms") (f (quote (("yaml" "general_storage/yaml") ("toml" "general_storage/toml") ("json" "general_storage/json") ("compress" "general_storage/compress") ("bincode" "general_storage/bincode"))))))

