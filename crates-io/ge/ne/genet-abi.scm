(define-module (crates-io ge ne genet-abi) #:use-module (crates-io))

(define-public crate-genet-abi-0.1.0 (c (n "genet-abi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0abxcxr9mcp8b1r9ibp8vwy9va1dmqqlgz4xn5ay3zs28cm7psz8")))

(define-public crate-genet-abi-0.2.0 (c (n "genet-abi") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p90k48k4higxi4hs5wbc8djp0mfw60ay704fzaicmxa77xfiw2c")))

(define-public crate-genet-abi-0.3.0 (c (n "genet-abi") (v "0.3.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m9q08xlnml86pdr59ybiiiz5gp5w5hynigiw727a5qc8sd81q4q")))

(define-public crate-genet-abi-0.4.0 (c (n "genet-abi") (v "0.4.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04b6dphh6crvxw8w0y1g901qxk44liny6l5dl9hkycdlqnc99dgh")))

(define-public crate-genet-abi-0.5.0 (c (n "genet-abi") (v "0.5.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "08fk1v5152laiz3n5q0sn8mymh3xp7ygr37xw96rlcr10xlhp91m")))

