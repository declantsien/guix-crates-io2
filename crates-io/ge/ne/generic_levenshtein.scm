(define-module (crates-io ge ne generic_levenshtein) #:use-module (crates-io))

(define-public crate-generic_levenshtein-0.1.0 (c (n "generic_levenshtein") (v "0.1.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "eddie") (r "^0.3.2") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 2)) (d (n "txtdist") (r "^0.2.1") (d #t) (k 2)))) (h "1p2v109lg1yirdzl7y1n7255bihy4wpcas2jyrflfi9g45ajjgc4")))

(define-public crate-generic_levenshtein-0.1.1 (c (n "generic_levenshtein") (v "0.1.1") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "eddie") (r "^0.3.2") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 2)) (d (n "txtdist") (r "^0.2.1") (d #t) (k 2)))) (h "1gx10xs2776a5has7wx0fi0ik8zm4mfk5sb5ahs9j81v9mgx8f09")))

(define-public crate-generic_levenshtein-0.2.0 (c (n "generic_levenshtein") (v "0.2.0") (d (list (d (n "distance") (r "^0.4.0") (d #t) (k 2)) (d (n "eddie") (r "^0.3.2") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 2)) (d (n "txtdist") (r "^0.2.1") (d #t) (k 2)))) (h "1ql4vdj65lmskqf8dzmraq8kg8nidx1gba7m3zl6l25g6v3vk63a")))

