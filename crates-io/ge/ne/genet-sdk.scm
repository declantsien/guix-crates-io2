(define-module (crates-io ge ne genet-sdk) #:use-module (crates-io))

(define-public crate-genet-sdk-0.1.0 (c (n "genet-sdk") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "genet-abi") (r "^0.1") (d #t) (k 0)))) (h "1nwy6nd9wmkq37ska47cpgqcwx3sw87qsr8c61d0709cqmzpm6dp")))

(define-public crate-genet-sdk-0.2.0 (c (n "genet-sdk") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "genet-abi") (r "^0.2.0") (d #t) (k 0)))) (h "1xixlxd17birklx7qpjpmb9p5wj46s4vary5a10dl8myy3x2ap08")))

(define-public crate-genet-sdk-0.3.0 (c (n "genet-sdk") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "genet-abi") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1jq93fkp8kgv9qz5hp0rwq2i84y31pjrwxyz0dw0rs67vdnf94n0")))

(define-public crate-genet-sdk-0.4.0 (c (n "genet-sdk") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "genet-abi") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0djxqs0w6l7kb00qq6hdgnqvna6xs5w1wlix4zzm5hgx6zswd07h")))

(define-public crate-genet-sdk-0.5.0 (c (n "genet-sdk") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "genet-abi") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "08pkvd8cjwcayxyz66pmn7g29hhm94v3jjx0vy4f7qvybgxqzjc5")))

