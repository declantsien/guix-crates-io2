(define-module (crates-io ge ne generate-derive) #:use-module (crates-io))

(define-public crate-generate-derive-0.0.1 (c (n "generate-derive") (v "0.0.1") (h "12pd34v64bi2y0v572r4pnbcz1hw0vmqdiay8x8z6rnl6jg8fynj")))

(define-public crate-generate-derive-1.0.0 (c (n "generate-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0y9w9h16dbk32248jb2ana3xwpwgmad5rv9iypy89lnd8sxcbbwq") (f (quote (("nightly")))) (y #t)))

(define-public crate-generate-derive-1.2.0 (c (n "generate-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0hk4z7ria9a0h8rm38sbgwfzi48aj20qnddzqwjcczwrwrzgwijh") (f (quote (("nightly")))) (y #t)))

(define-public crate-generate-derive-1.3.0 (c (n "generate-derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0zxg8xrrd78msqlahri3j3w14k3z5z780gd9h55fqmbmz5ixh335") (f (quote (("nightly"))))))

(define-public crate-generate-derive-1.4.0 (c (n "generate-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1d00a5grqym6v7zpigsp38k4rvdfdkyx7pw7fgwwbm3kp0w3df1p") (f (quote (("nightly"))))))

(define-public crate-generate-derive-1.5.0 (c (n "generate-derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "03a0f70ypdrsg63an91wni9dvhck4gh92nff5fh94pbz3x00q53y") (f (quote (("nightly"))))))

