(define-module (crates-io ge ne generic_static) #:use-module (crates-io))

(define-public crate-generic_static-0.1.0 (c (n "generic_static") (v "0.1.0") (h "10c441hz8fk78pk6wy0dfiynw2v1wwxl0hvyqc4ad10qdmnqlxyb")))

(define-public crate-generic_static-0.2.0 (c (n "generic_static") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "0q1snh4za11vala4fff5i1pc5zqasbkaw2fvf5v32w40klbzzk18")))

