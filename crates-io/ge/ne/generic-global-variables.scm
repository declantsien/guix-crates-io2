(define-module (crates-io ge ne generic-global-variables) #:use-module (crates-io))

(define-public crate-generic-global-variables-0.1.0 (c (n "generic-global-variables") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0pvkk8ga8cw3pwi0jb1jxg0cwsv565q41sh04jlynnw4l2g1a1cf")))

(define-public crate-generic-global-variables-0.1.1 (c (n "generic-global-variables") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0ax1s1sad0cp6kdqcsxwbd7fykz4h18k0q41alqk0bqrhibd7yxl")))

(define-public crate-generic-global-variables-0.1.2 (c (n "generic-global-variables") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1fpz5iacbp74sak3lry09mnq84wc1r9a7rk9lwng2sz0p8klj9g8")))

