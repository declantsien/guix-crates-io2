(define-module (crates-io ge ne generic-rcsp) #:use-module (crates-io))

(define-public crate-generic-rcsp-0.1.0 (c (n "generic-rcsp") (v "0.1.0") (d (list (d (n "binary-heap-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.2") (d #t) (k 0)))) (h "0y8qlwqm9mxr8xrn015mx36kl8p6ab18rbvl3ggzclbad3jz1i7v")))

