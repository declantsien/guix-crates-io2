(define-module (crates-io ge ne generic-ec-core) #:use-module (crates-io))

(define-public crate-generic-ec-core-0.1.0 (c (n "generic-ec-core") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1wpnxmdshhs5mvr6ljicm4nnssr93xsc4szjl0a003s82y3vzwv6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-generic-ec-core-0.1.1 (c (n "generic-ec-core") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0n36dkrxrmvspmn091xz4m4a38b8r1swm5fsyzwfi8gcnns6fygz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-generic-ec-core-0.1.2 (c (n "generic-ec-core") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0gk6hx5zvyfkjapk55qg7v9yaf3m1wika1nvsyjlwcph2w6794qi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-generic-ec-core-0.1.3 (c (n "generic-ec-core") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0gwjxh8cz8nhkv8fwsgyifsimx76v8gd4sbfq4bw1gw8zh1b3ji2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-generic-ec-core-0.1.4 (c (n "generic-ec-core") (v "0.1.4") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "0dr1dv5hq59wzm5998byqinm3v1p6m5n89apvmk77sn99xjs7s45") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-generic-ec-core-0.2.0 (c (n "generic-ec-core") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "subtle") (r "^2.4") (k 0)) (d (n "zeroize") (r "^1") (k 0)))) (h "1sjb7l9nbpmxzy9ibx6rfh245kx918d6p0h7m1nlaixar165cmpi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

