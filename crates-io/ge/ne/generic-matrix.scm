(define-module (crates-io ge ne generic-matrix) #:use-module (crates-io))

(define-public crate-generic-matrix-0.0.1 (c (n "generic-matrix") (v "0.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "06i6qs9dig0z90i8gykhxxdpqsy9ggixznslp7zryqhng0rs5b7i")))

(define-public crate-generic-matrix-0.0.2 (c (n "generic-matrix") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1rph8fdxd3y6afg87jqrr61286xx4ail1fh42k8jldy99yn7f7ki")))

(define-public crate-generic-matrix-0.0.4 (c (n "generic-matrix") (v "0.0.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0a5criv75h0xl9p24z4jl484566yiv3qxbblmq5w36iwgxpb68hq")))

(define-public crate-generic-matrix-0.0.5 (c (n "generic-matrix") (v "0.0.5") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "078qbsf0vj5zlcrp4qakhywl07w0drh6yvczbx093scjphz94mj6")))

(define-public crate-generic-matrix-0.0.6 (c (n "generic-matrix") (v "0.0.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1z842224vshblrir2qsrh3a6w4kbaq69kjahpaima9l53plsk4lp")))

(define-public crate-generic-matrix-0.0.7 (c (n "generic-matrix") (v "0.0.7") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1b2whcz0vnqzpqapyg1kjcdf8b7ghy9c8sli3xcc0q454qk4f8l6")))

(define-public crate-generic-matrix-0.0.8 (c (n "generic-matrix") (v "0.0.8") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "08rnlz8fkxlp7ir705c6pwjxs1pgriaqdm5c39fcq0llsckkkc5v")))

(define-public crate-generic-matrix-0.1.0 (c (n "generic-matrix") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1cwxynd55asqw3fliyl1iga11j09jlqdvjlpv0zqvm6yppi4sq64")))

(define-public crate-generic-matrix-0.2.0 (c (n "generic-matrix") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "11kxfq2i84x6x6i5f3niivzdcanfrv03p07sx1clbipyj5bhaivb") (r "1.39.0")))

(define-public crate-generic-matrix-0.2.1 (c (n "generic-matrix") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02lzal6ibh0sl22r3m66gz7dcdc5fwppcbsmhw3dr13kk5ca076y") (r "1.39.0")))

(define-public crate-generic-matrix-0.2.2 (c (n "generic-matrix") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cg9c3q0mc3s48hgcl6i2vlxr7hhnzslh63j10aqa3pa1b8iwdfr") (r "1.39.0")))

