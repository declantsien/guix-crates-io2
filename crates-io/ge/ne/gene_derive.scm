(define-module (crates-io ge ne gene_derive) #:use-module (crates-io))

(define-public crate-gene_derive-0.1.0 (c (n "gene_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "09qlqq48p6amgay97x3z9ik8drnraxvd039sncxy2xr86np79wdb") (r "1.65.0")))

(define-public crate-gene_derive-0.1.1 (c (n "gene_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "15iihcgakld3majgbx3b150361h37xxz5af09n3v94n68lq0qlhx") (r "1.65.0")))

(define-public crate-gene_derive-0.1.2 (c (n "gene_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0vj12dw2vlfvff8w4ld4r66360s2f3zckkh229gxcfxvlfznggrr") (r "1.65.0")))

