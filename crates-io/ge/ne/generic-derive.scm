(define-module (crates-io ge ne generic-derive) #:use-module (crates-io))

(define-public crate-generic-derive-0.0.1 (c (n "generic-derive") (v "0.0.1") (d (list (d (n "generic-core") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r18rcqhaxk1k08y8sgsc4acfr16ap3zi3sk6zl5czc8wg5v3hxj")))

(define-public crate-generic-derive-0.0.2 (c (n "generic-derive") (v "0.0.2") (d (list (d (n "generic-core") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04xb8ad4cys8aqcgyf145h827pgcd0c7q3hb9fniycpgi6qxz0in")))

