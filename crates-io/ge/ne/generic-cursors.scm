(define-module (crates-io ge ne generic-cursors) #:use-module (crates-io))

(define-public crate-generic-cursors-0.0.1 (c (n "generic-cursors") (v "0.0.1") (h "11g6g3r79qdd24r1h49rv2lmvw259fw85jvs5hz8c630iy32znfh")))

(define-public crate-generic-cursors-0.0.2 (c (n "generic-cursors") (v "0.0.2") (h "0a52dlk8w2dvd6rrlah5kra5906yscg21nk964pzg6jvjirjbyjf")))

(define-public crate-generic-cursors-0.0.3 (c (n "generic-cursors") (v "0.0.3") (h "10f3yr3r2frjv56a109a5l1qyvi5lglx00249gn5nibkynwpaqdx")))

