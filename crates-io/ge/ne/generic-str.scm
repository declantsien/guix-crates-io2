(define-module (crates-io ge ne generic-str) #:use-module (crates-io))

(define-public crate-generic-str-0.2.1 (c (n "generic-str") (v "0.2.1") (d (list (d (n "generic-vec") (r "^0.1.4") (f (quote ("nightly" "alloc"))) (d #t) (k 0) (p "cl-generic-vec")))) (h "123l4ximy4a2d7ln9lapwfnk8akp3aj87dly5wjavchzxbv6vc19")))

(define-public crate-generic-str-0.2.2 (c (n "generic-str") (v "0.2.2") (d (list (d (n "generic-vec") (r "^0.1.4") (f (quote ("nightly"))) (k 0) (p "cl-generic-vec")))) (h "0lgw4i3rdimgmwhxsx70y5i3fqk4aa2nsygjfhnznm9p5vadg0x3") (f (quote (("std" "alloc" "generic-vec/std") ("default" "std") ("alloc" "generic-vec/alloc"))))))

(define-public crate-generic-str-0.3.0 (c (n "generic-str") (v "0.3.0") (d (list (d (n "generic-vec") (r "^0.3.1") (f (quote ("nightly"))) (k 0) (p "cl-generic-vec")))) (h "12idwdmmiqs37qc684idn53cwhg102kwwjkm5pv6a0mji67sxis5") (f (quote (("std" "alloc" "generic-vec/std") ("default" "std") ("alloc" "generic-vec/alloc"))))))

(define-public crate-generic-str-0.3.1 (c (n "generic-str") (v "0.3.1") (d (list (d (n "generic-vec") (r "^0.3.3") (f (quote ("nightly"))) (k 0) (p "cl-generic-vec")))) (h "004cc14d23v4v7cnf8yfqn3jz13i1ma43v44da1rv0lrds4xslyv") (f (quote (("std" "alloc" "generic-vec/std") ("default" "std") ("alloc" "generic-vec/alloc"))))))

