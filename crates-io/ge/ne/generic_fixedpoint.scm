(define-module (crates-io ge ne generic_fixedpoint) #:use-module (crates-io))

(define-public crate-generic_fixedpoint-0.1.0 (c (n "generic_fixedpoint") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0q1wlxzclrgql7m8mrbvpizmn6l2w9y9lxzi2jdilikid63jsp7n")))

(define-public crate-generic_fixedpoint-0.1.1 (c (n "generic_fixedpoint") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "07vl9ijwmpsvxcf14l9255rn73laan02mr5mh1mjck22zdwvjag2")))

