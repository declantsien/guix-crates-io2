(define-module (crates-io ge ne generate_model) #:use-module (crates-io))

(define-public crate-generate_model-0.1.0 (c (n "generate_model") (v "0.1.0") (d (list (d (n "fake") (r "^2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "09naay6by48xfym8glvcwmdyr94kcxk354x3aswwm0yhx6qdrksv") (y #t)))

(define-public crate-generate_model-0.2.0 (c (n "generate_model") (v "0.2.0") (d (list (d (n "fake") (r "^2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "18izxsj1i0wv44k0ysyf5ai7hx2dd71lnrvrhrg949zgqhidngj2") (y #t)))

(define-public crate-generate_model-0.2.1 (c (n "generate_model") (v "0.2.1") (d (list (d (n "fake") (r "^2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "01ss8888sy6yzrgl3cfhzkkk2q7xwdd76dk6dlrydh6wldc71105") (y #t)))

(define-public crate-generate_model-0.2.2 (c (n "generate_model") (v "0.2.2") (d (list (d (n "fake") (r "^2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "01g85159gv24kp96jglivpwdhpny1iv1d4s6m5fda1cadbk5fhcx") (y #t)))

