(define-module (crates-io ge ne genetic_planner) #:use-module (crates-io))

(define-public crate-genetic_planner-0.1.0 (c (n "genetic_planner") (v "0.1.0") (d (list (d (n "rand") (r ">= 0.3") (d #t) (k 0)))) (h "1m7l49jvqw0jaaqby6q6cc6xg2ildyib29ch2syx86xn2q7vmb2c")))

(define-public crate-genetic_planner-0.1.1 (c (n "genetic_planner") (v "0.1.1") (d (list (d (n "rand") (r ">= 0.3") (d #t) (k 0)))) (h "0kcz8xmhfq9xah277i4w2jc88v0nmx6827w8xg47b63xmcb0zccr")))

(define-public crate-genetic_planner-0.1.5 (c (n "genetic_planner") (v "0.1.5") (d (list (d (n "rand") (r ">= 0.3") (d #t) (k 0)))) (h "0s3bra1229dvbq82dmk9avr88zypj42h8rn9nq1mv7fjf952fyv6")))

(define-public crate-genetic_planner-0.2.0 (c (n "genetic_planner") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "0yjm7414nj74dzak1s6il22z9xmj3v2vsibq3pkd6lpgpw2nixf8")))

(define-public crate-genetic_planner-0.3.0 (c (n "genetic_planner") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "19mpplpq2cq0za8rxzi36qpv9vsb91fz3a73vgwrr12y200xwf94")))

(define-public crate-genetic_planner-0.4.0 (c (n "genetic_planner") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "threadpool") (r "^1.3.2") (d #t) (k 0)))) (h "13kjwgydhj1h4p14w3lqjzrykpvvsxp8h6ypncx369b53rpbbpzy")))

