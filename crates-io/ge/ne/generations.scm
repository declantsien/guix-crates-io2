(define-module (crates-io ge ne generations) #:use-module (crates-io))

(define-public crate-generations-1.0.0 (c (n "generations") (v "1.0.0") (h "1d31fc4khpxlwx1sc0pq2g6j72xv55w3816xh34y1kk6yw66dfbd")))

(define-public crate-generations-1.0.1 (c (n "generations") (v "1.0.1") (h "1xci4pda8y2agap27p9mynlal5b05pa7z762smyp3wpmljz3503f")))

(define-public crate-generations-2.0.0 (c (n "generations") (v "2.0.0") (h "00i0vpws4dak5l8bsd1gm41xh5zdl7z20xmb2r62zl3b9fz1fjmv")))

