(define-module (crates-io ge ne general_audio_native) #:use-module (crates-io))

(define-public crate-general_audio_native-0.1.0 (c (n "general_audio_native") (v "0.1.0") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (d #t) (k 0)))) (h "0gb4c6mxj4aci5a8dz7ff3b2lblqx11hk8kxlacqa7gc2m56965g") (f (quote (("force_dedicated_audio_thread"))))))

(define-public crate-general_audio_native-0.2.0 (c (n "general_audio_native") (v "0.2.0") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (k 0)))) (h "1ipinhb504qrr0xgqa073jl8s2x69lv04h61plzab5lap6n4hwf1") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.2.1 (c (n "general_audio_native") (v "0.2.1") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.13") (k 0)))) (h "1z8p1zm7g2wszbl768skilyx0188j79v9bcvrnnypckmjb5is385") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.0 (c (n "general_audio_native") (v "0.3.0") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.13") (k 0)))) (h "1l3vbqrczrdm61dc0f7ma2brpcb9z4n653ykc45mma2gcm7gbnfv") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.2.2 (c (n "general_audio_native") (v "0.2.2") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.11") (k 0)))) (h "04cplzfnkvxcia6yz3lb3ji55bkf76xin65mr2aq5ng1rqciz4j3") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.1 (c (n "general_audio_native") (v "0.3.1") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (k 0)))) (h "0svxj35rc2avvi2yfkq8qcgyx63wa11c8vgm6k7klp2b582clks7") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.2 (c (n "general_audio_native") (v "0.3.2") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.14") (k 0)))) (h "1xl884ra3adm6glgpmrmr19rh1ma24vkbcbadjg6lj6mgwwm25zv") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.3 (c (n "general_audio_native") (v "0.3.3") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (k 0)))) (h "0203nglb82dl0mcy0qkr9i3dxs0c05vawkfh2xc5dlm77rsal3iy") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.4 (c (n "general_audio_native") (v "0.3.4") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.16") (k 0)))) (h "0sv9p7kk229rhqgppcn3a64xc0crsfqhijl2qja8i42gawv87xip") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

(define-public crate-general_audio_native-0.3.5 (c (n "general_audio_native") (v "0.3.5") (d (list (d (n "general_audio") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (k 0)))) (h "0vpvi8ysmaf14mdwysb1ad4hpkqd5kc2wfrgm6f0zrjks66xklf6") (f (quote (("wav" "rodio/wav") ("vorbis" "rodio/vorbis") ("mp3" "rodio/mp3") ("force_dedicated_audio_thread") ("flac" "rodio/flac"))))))

