(define-module (crates-io ge ne generic-uvarint) #:use-module (crates-io))

(define-public crate-generic-uvarint-0.0.0 (c (n "generic-uvarint") (v "0.0.0") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.3") (d #t) (k 0)))) (h "0w1iprhpg3xik1bijn0c3hh1gaasgfpcwaxzchkffpqkhdh9rkid") (y #t)))

(define-public crate-generic-uvarint-0.0.1 (c (n "generic-uvarint") (v "0.0.1") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.3") (d #t) (k 0)))) (h "0h2r5l2w8xxb1adkq1rsszcags4si0wzd96rlmy795lij4gdwmn7")))

(define-public crate-generic-uvarint-0.1.0 (c (n "generic-uvarint") (v "0.1.0") (d (list (d (n "generic-array") (r "^1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)))) (h "0c4izr1azk4kmczaim0cb428z9q4pc40bdxvqc08mib14kk8zic1")))

