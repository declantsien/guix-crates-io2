(define-module (crates-io ge ne generate) #:use-module (crates-io))

(define-public crate-generate-0.0.1 (c (n "generate") (v "0.0.1") (d (list (d (n "generate-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0gj5jj6gvspp9x6gp9d8iyg0zaq63p6gvg0sjv2px3sh57h2w018")))

(define-public crate-generate-1.0.0 (c (n "generate") (v "1.0.0") (d (list (d (n "generate-derive") (r "^1.0.0") (d #t) (k 0)))) (h "0cgpd0gcpadc25fg58llv776x7cnygb7xm09l7fa5rpfh4ys7xx3") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro")))) (y #t)))

(define-public crate-generate-1.1.0 (c (n "generate") (v "1.1.0") (d (list (d (n "generate-derive") (r "^1.0.0") (d #t) (k 0)))) (h "05fsg87q8xgg56dg3n96sj8j1xsm233ksvd0f40dgpr3wqczkcdj") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro")))) (y #t)))

(define-public crate-generate-1.2.0 (c (n "generate") (v "1.2.0") (d (list (d (n "generate-derive") (r "^1.0.0") (d #t) (k 0)))) (h "059jazrf0x5yinxwd9h5bjh8n77x0ff06fqzqainan752ip1l4iz") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro")))) (y #t)))

(define-public crate-generate-1.3.0 (c (n "generate") (v "1.3.0") (d (list (d (n "generate-derive") (r "^1.3.0") (d #t) (k 0)))) (h "1hp5sh2kpzl49v81l8s83rshcqad3p6jb2i81szggcfs0qgzss31") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro"))))))

(define-public crate-generate-1.4.0 (c (n "generate") (v "1.4.0") (d (list (d (n "generate-derive") (r "^1.4.0") (d #t) (k 0)))) (h "0d0wvr7qncb2gmjfbsq8w3h1nbcg3g2g1r49pdayfnfgnsks06yv") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro"))))))

(define-public crate-generate-1.5.0 (c (n "generate") (v "1.5.0") (d (list (d (n "generate-derive") (r "^1.5.0") (d #t) (k 0)))) (h "08cl8yqp8js0nb4h9jnp8f7fdh1fcg1mzkqx6dp99h8nq6j1cq3j") (f (quote (("nightly" "generate-derive/nightly") ("macro") ("default" "macro"))))))

