(define-module (crates-io ge ne generics) #:use-module (crates-io))

(define-public crate-generics-0.0.1 (c (n "generics") (v "0.0.1") (h "1fj7dga6chvzvn2acxb6gpgj65fj481g0xg47jgwsdpmxpnsm8zr")))

(define-public crate-generics-0.1.0 (c (n "generics") (v "0.1.0") (h "0bs7yhpp9hxsy8xa1sxbp4sp0jdbc4cjwgpvnidczac95jinvzsp")))

(define-public crate-generics-0.1.1 (c (n "generics") (v "0.1.1") (h "1ski1ydjdvgyrqq9i2cf8v6nlrxry6x1sa38b2mjhbzccg9rnwjm")))

(define-public crate-generics-0.1.2 (c (n "generics") (v "0.1.2") (h "0z74ndbk53gpk32lgwcjzqc478mcvzhlmhq7g52yk89z9gmpr2g0")))

(define-public crate-generics-0.1.3 (c (n "generics") (v "0.1.3") (h "1nq44vj8y4wv3ph72rw0zym23vwm3xmm1fpbyqrk61wk8pbm3nai")))

(define-public crate-generics-0.2.0 (c (n "generics") (v "0.2.0") (h "03lqyyyr2bss00fy003489rgjlbd34s8wa9kzr482b9iy5xjxfq5")))

(define-public crate-generics-0.2.1 (c (n "generics") (v "0.2.1") (h "1ywg4r7z8iim6snzm2rm50535kgah0v8jr13ihdzrv6wcjysbfd3")))

(define-public crate-generics-0.2.2 (c (n "generics") (v "0.2.2") (h "1ikx1nxs1486zdqk2dqkzdngnriirjdfb5ykmvvl52wpjjp0azvs")))

(define-public crate-generics-0.2.3 (c (n "generics") (v "0.2.3") (h "0m30mnph14g33z3c6c4gm6v11ap7299a2jna0zyrha0jmsfmlkjf")))

(define-public crate-generics-0.2.4 (c (n "generics") (v "0.2.4") (h "0bm8fhk57kmfg1jz5x5cm6wh772ika1y612q3y8cz9839ni8vr9y")))

(define-public crate-generics-0.2.5 (c (n "generics") (v "0.2.5") (h "0y1asl0zc4ch11wiiwgmb52179bbkaialz1agv49825vmqb5xllk")))

(define-public crate-generics-0.2.6 (c (n "generics") (v "0.2.6") (h "1mcjh9jccs27dxkjav9wiz1v902n5rlzq9y2xyqbzhm47p809mzj")))

(define-public crate-generics-0.2.7 (c (n "generics") (v "0.2.7") (h "0nb0bpjllhj3g3rxarrldp2qjwp99fkd488rk1s86av7dvgqfjr9")))

(define-public crate-generics-0.2.8 (c (n "generics") (v "0.2.8") (h "1cs9wcagdq0dh9ggq33x52w5szrkrz3pz4mwzxq5cggxw60kfxfg")))

(define-public crate-generics-0.3.0 (c (n "generics") (v "0.3.0") (h "054jiac0hb21c1xq267pprl50z8v1w1b7z3k7pkfgy4cwwg8amw1")))

(define-public crate-generics-0.3.1 (c (n "generics") (v "0.3.1") (h "0yxcz1j3v4nwm95snfpkicycwxam67vc2c6v6q97xkbx2y7a0ii8")))

(define-public crate-generics-0.3.2 (c (n "generics") (v "0.3.2") (h "1cmyjidkr1lgi4vcahdmxpsch6fs9jiwii2nnkgnmk2rkgwh2kvl")))

(define-public crate-generics-0.4.0 (c (n "generics") (v "0.4.0") (h "1wskrfjbwv173z8w1ryz3v32c3iydfa36lf9bwlilz6na4pnisqm") (r "1.60")))

(define-public crate-generics-0.4.1 (c (n "generics") (v "0.4.1") (h "04fm1zz32liai1z2yvji3a1mabzdhgznj2n6js6w41lx4kjqh5r9") (r "1.60")))

(define-public crate-generics-0.4.2 (c (n "generics") (v "0.4.2") (h "0xq20kiikyg50855jdksxx7sbwvwy9vhzvgr1wf9m75l59wpadav") (r "1.60")))

(define-public crate-generics-0.4.3 (c (n "generics") (v "0.4.3") (h "1pdhs7x9r5dwy9b1zyc1nfknsyvwz4hfnsz38m24d0k8ci91nbss") (r "1.60")))

(define-public crate-generics-0.4.4 (c (n "generics") (v "0.4.4") (h "12zdswb8by161fql03niqqjahs13pg04ff7s4i2nx8i2rp0chs4v") (r "1.60")))

(define-public crate-generics-0.5.0 (c (n "generics") (v "0.5.0") (h "18l16fwllagnnf24996j2vi8ynqls24wk622w2lnp9gzblvqvy6w") (r "1.71")))

(define-public crate-generics-0.5.1 (c (n "generics") (v "0.5.1") (h "1a8srl7vs8i63piv10a7rp88djw8sfrqd7cgxzvir3mxpgv054f3") (r "1.71")))

(define-public crate-generics-0.5.2 (c (n "generics") (v "0.5.2") (h "06l034fbb9wjl4l5wq6f96im7dcwa3cvypi34g8343xg7igivsk2") (r "1.71")))

(define-public crate-generics-0.5.3 (c (n "generics") (v "0.5.3") (h "1032b81njfgzcd0502fv2asfjyaii3i7fz2dx21f7w6lnk17brg7") (r "1.71")))

