(define-module (crates-io ge ne general_pub_sub) #:use-module (crates-io))

(define-public crate-general_pub_sub-0.0.1 (c (n "general_pub_sub") (v "0.0.1") (h "0sk9h0znxpfj9yl2hblpvvh6m54pqpv6msg8d26369fsd7a85csp")))

(define-public crate-general_pub_sub-0.0.2 (c (n "general_pub_sub") (v "0.0.2") (h "0lpw1102faza0pw0v8ycccn9wc3k3dlq9sfxzaahmhn04jncj1xg")))

(define-public crate-general_pub_sub-0.0.3 (c (n "general_pub_sub") (v "0.0.3") (h "03s7llgyhna0slchfmrmgqpshr9v9v230z8sxg5j4r8h6xn46paq")))

(define-public crate-general_pub_sub-0.0.4 (c (n "general_pub_sub") (v "0.0.4") (d (list (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0qws7rz9vgrprvgv23ni4viavbfzcpglyda2n8yrn9ggphbhj7sb")))

(define-public crate-general_pub_sub-0.0.5 (c (n "general_pub_sub") (v "0.0.5") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "19qgz3mfijyqhp90agg4d9jhn3jc3v510r1mn48lppj4g6j620dv")))

(define-public crate-general_pub_sub-0.0.6 (c (n "general_pub_sub") (v "0.0.6") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0hsg6azwrq2vcmaqpsklz9hhz4hq6ggfxsni2fb2dfrgd1n762xg")))

(define-public crate-general_pub_sub-0.0.7 (c (n "general_pub_sub") (v "0.0.7") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0rd07xpvdqz07ciz4j8yjz12h330xhw2gc4mlrzazpliv32wardm")))

(define-public crate-general_pub_sub-0.0.8 (c (n "general_pub_sub") (v "0.0.8") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0c0bbkkbqzizfsd7apd4531a8ym0p7j05rl12rd8z5rlqxhmmlkq")))

(define-public crate-general_pub_sub-0.1.0 (c (n "general_pub_sub") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "1xkw79sjas3diwl6712gvk0wygdl53rahwsd02za182mqb05v5lj")))

