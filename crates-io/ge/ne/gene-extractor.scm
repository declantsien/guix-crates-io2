(define-module (crates-io ge ne gene-extractor) #:use-module (crates-io))

(define-public crate-gene-extractor-0.1.0 (c (n "gene-extractor") (v "0.1.0") (d (list (d (n "ansi_term") (r "0.*") (d #t) (k 0)) (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "glob") (r "0.*") (d #t) (k 0)) (d (n "indicatif") (r "0.*") (d #t) (k 0)) (d (n "log") (r "0.*") (d #t) (k 0)) (d (n "log4rs") (r "1.*") (d #t) (k 0)) (d (n "num_cpus") (r "1.13.*") (d #t) (k 0)) (d (n "rayon") (r "1.*") (d #t) (k 0)) (d (n "sysinfo") (r "^0.21.1") (d #t) (k 0)))) (h "0i97njhjal0zlbjavwrb53hv2svxzmckb0k0z1qjs9q3r34yaq5y")))

