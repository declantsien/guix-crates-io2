(define-module (crates-io ge od geode) #:use-module (crates-io))

(define-public crate-geode-0.1.0 (c (n "geode") (v "0.1.0") (h "1p2qyhablnpi230y99va32a2zkkrsszl5r7bpgh982sip731lxy3")))

(define-public crate-geode-0.1.1 (c (n "geode") (v "0.1.1") (d (list (d (n "windows") (r ">=0.51.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation"))) (d #t) (k 0)))) (h "1w72a1r40dzxpxcgyzy7h180khya8gc3p57z612xybnddgsnpy3s")))

(define-public crate-geode-0.1.2 (c (n "geode") (v "0.1.2") (d (list (d (n "windows") (r ">=0.51.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation"))) (d #t) (k 0)))) (h "1j27k53jgng06gdxykyg2z381v7ak7zqvrjp2a02b49q3181pmxi")))

(define-public crate-geode-0.1.3 (c (n "geode") (v "0.1.3") (d (list (d (n "windows") (r ">=0.51.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation"))) (d #t) (k 0)))) (h "1fb4svygfanvbqkxsmfzmd3n7yxhl3msnzr0xpwscinmaczwsjc1")))

(define-public crate-geode-0.1.4 (c (n "geode") (v "0.1.4") (d (list (d (n "windows") (r ">=0.51.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation"))) (d #t) (k 0)))) (h "1mvpvw1p4aanfjwx8bpk34lyp3fmbrx9wz2d80w37a1ddvx91h8r")))

