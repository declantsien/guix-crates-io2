(define-module (crates-io ge od geod) #:use-module (crates-io))

(define-public crate-geod-0.1.0 (c (n "geod") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y07lj99gzjd11szlm3g2yiv87ghl5wnjl5y9g1z1mjfviginl1j") (y #t)))

(define-public crate-geod-0.1.1 (c (n "geod") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rpn82ra197d20j9hb86pvxn3yrfdkdsajcypxhgzhqy8nlp5hcr")))

