(define-module (crates-io ge od geodate) #:use-module (crates-io))

(define-public crate-geodate-0.1.0 (c (n "geodate") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nvsz8qza779i878d36d2v5d147z2nrj5ylifl79d7clvg15xvwa")))

(define-public crate-geodate-0.1.1 (c (n "geodate") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "160c4h16lafhw8jvjqxfc8l1shdr6562nwg1c170snnjh8gv4307")))

(define-public crate-geodate-0.2.0 (c (n "geodate") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1vlwpcmijplav5lfb47z75hzpy914mx9zcrg9bb7cf3r6cjxz238")))

(define-public crate-geodate-0.2.1 (c (n "geodate") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0a0l39dk5m9z0fypc3h207ngdyqgi88hpj3lj6cn8rvxq9s3fxcf")))

(define-public crate-geodate-0.3.0 (c (n "geodate") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0kh88pjgx4g6zbl1rgd14hflnfk8p7xaabb2b64r870887b01gkz")))

(define-public crate-geodate-0.4.0 (c (n "geodate") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "16m9k89s9lxapya1j8fhc9fxgyc33pb0y9c6ryhwp4ln099685sp")))

