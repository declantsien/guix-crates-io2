(define-module (crates-io ge od geode-finder) #:use-module (crates-io))

(define-public crate-geode-finder-1.0.0 (c (n "geode-finder") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "java-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "0m43477ddid9h854kmwc0g48mkljn6lhhh4145llwv8271csac5n")))

(define-public crate-geode-finder-1.0.1 (c (n "geode-finder") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "java-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "19b0mqhylp69ha4hi7i80gggfs8ky8vzf0s9nbpgy19r2ka2mz3i")))

(define-public crate-geode-finder-1.0.2 (c (n "geode-finder") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "java-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "14b2ma9vrs550jbnvrarcxfgavbd7hbbj5xryp4wgfgh56xngl48")))

(define-public crate-geode-finder-1.0.3 (c (n "geode-finder") (v "1.0.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0xz1qn9qjm3wp569khqp6yw1ixxgnl31gil3fzdjxmhrdshv7f8z")))

(define-public crate-geode-finder-1.0.4 (c (n "geode-finder") (v "1.0.4") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)))) (h "0a8s7smnq85ldxm380hfxq0yi6bz1kb1ypyyics7p5f30jcrmwcr")))

