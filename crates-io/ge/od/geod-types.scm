(define-module (crates-io ge od geod-types) #:use-module (crates-io))

(define-public crate-geod-types-0.1.1 (c (n "geod-types") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pms54yv4aj8sc59k8lqcbcsbrzg063wvfp14vn2v5g8fmdijhgs")))

