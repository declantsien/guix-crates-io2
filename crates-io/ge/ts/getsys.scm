(define-module (crates-io ge ts getsys) #:use-module (crates-io))

(define-public crate-getsys-1.0.1 (c (n "getsys") (v "1.0.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "16665sq4yg8w1bf35s37dq6413j6jq5lhlinkvbsg3s4bspz48a4")))

(define-public crate-getsys-1.1.0 (c (n "getsys") (v "1.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1zbr3kp2ma0yqgzsxxrgghshqpyxflc759vdjd9423sbfnwjziqz")))

(define-public crate-getsys-1.1.1 (c (n "getsys") (v "1.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "02vkb95c5f81zmdj980fs72q17kyq6vbd50cq0r533482ccqgfcp")))

(define-public crate-getsys-1.1.2 (c (n "getsys") (v "1.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0d2r2ra04k8f9qyxbyqm29wkx4rjza5n7j7jvy6n80g0z53jq00q")))

(define-public crate-getsys-1.1.3 (c (n "getsys") (v "1.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "11mfd6ag1v1657df4aj9lj74sdhjhq87049rkph65lrshfkgwfcj")))

(define-public crate-getsys-1.2.0 (c (n "getsys") (v "1.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "19ibvyx29hfbij548db0w6ddfqcpl3alrd6ac49y2c2vs5cmz4n0")))

