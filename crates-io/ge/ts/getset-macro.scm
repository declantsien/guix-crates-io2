(define-module (crates-io ge ts getset-macro) #:use-module (crates-io))

(define-public crate-getset-macro-1.0.0 (c (n "getset-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.35") (d #t) (k 0)))) (h "0ds8ac6pnipy6rsagngi1fnh4z1qax0y8b4akyy9b4q09mqswskg")))

(define-public crate-getset-macro-1.0.1 (c (n "getset-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.35") (d #t) (k 0)))) (h "1iv660c4xfbv8fif9xpiw1pxxdi9lc7zi7nsbp26pvksamc9f285")))

