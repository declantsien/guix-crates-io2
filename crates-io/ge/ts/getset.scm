(define-module (crates-io ge ts getset) #:use-module (crates-io))

(define-public crate-getset-0.0.1 (c (n "getset") (v "0.0.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "17nc8n0kww22cwqb602yx4fkrwklks1g5163rlyb3gk121qlqnzg")))

(define-public crate-getset-0.0.2 (c (n "getset") (v "0.0.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1imiyzq6f6z7qdqcki1qxdknr4z25yw68h8y3hsncza6g0hc7iax")))

(define-public crate-getset-0.0.3 (c (n "getset") (v "0.0.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "03i6203jw6yw0vcnbxnnh39b7aapnkaxrg343spl7df71aj37m6j")))

(define-public crate-getset-0.0.4 (c (n "getset") (v "0.0.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0wiy9d8py3wfksrs1ip5qa811a1nm170j112pmbcbg0cykfglh63")))

(define-public crate-getset-0.0.5 (c (n "getset") (v "0.0.5") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "16c2kvcr28j4hz5786kdc48flm73pn7jvrv5h0p3m0q9pv526q1w")))

(define-public crate-getset-0.0.6 (c (n "getset") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^0.3") (k 0)) (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.10") (d #t) (k 0)))) (h "10h740vb4sqdz15qr4fkdirzbxi8pcyjnsjdj0jvnf2p4dmg7isl")))

(define-public crate-getset-0.0.7 (c (n "getset") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^0.4.24") (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "1jz2ark3zi0rpp9fcx2gyzr0jax2kdf1yjv9fja1y70cml7xxyqr")))

(define-public crate-getset-0.0.8 (c (n "getset") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rmq79q5wpb4wkc928vchd98b3l9v8nv5zmk3cb10kndmq9mnyhi")))

(define-public crate-getset-0.0.9 (c (n "getset") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0aaldwfs2690rjqg2ygan27l2qa614w2p6zj7k99n36pv2vzbcsv")))

(define-public crate-getset-0.1.0 (c (n "getset") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18047rasp9491asd7sa039yrkwb8mhmg34n3chwnr15fb6f16apn")))

(define-public crate-getset-0.1.1 (c (n "getset") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "016590lxhlqga016z1qnavl0zavk59b97aix2zcd4wad3b02icr4")))

(define-public crate-getset-0.1.2 (c (n "getset") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1f8yc83hm5b7vzscxq20ivdv7wlfvabn79j653zh9k3m1qjjfmz4")))

