(define-module (crates-io ge ts getsb) #:use-module (crates-io))

(define-public crate-getsb-0.1.0 (c (n "getsb") (v "0.1.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "knock") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0z8gk4li0h8c4c2gaxzy3i8nhfcxz2x4szdkgwik44iqjw23xxln")))

(define-public crate-getsb-0.1.1 (c (n "getsb") (v "0.1.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "knock") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0q8004969lyl554qvk6f8gkfpx5cjq4qlh1wvac0csldbqs50yqq")))

(define-public crate-getsb-0.1.2 (c (n "getsb") (v "0.1.2") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "knock") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0kwplfni9w8hgcsznz4bnfaadvyza024nw74x2m1f7wsqx3yvg1n") (f (quote (("default"))))))

