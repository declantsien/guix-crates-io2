(define-module (crates-io ge mg gemgen) #:use-module (crates-io))

(define-public crate-gemgen-0.1.0 (c (n "gemgen") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gemblockchain") (r "^0.1.1") (d #t) (k 0)))) (h "1d02z649y4rpw0rph54spf90qpgyxf7z183nsbsmxd2lq00ljdbi")))

(define-public crate-gemgen-0.1.1 (c (n "gemgen") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gemblockchain") (r "^0.1.1") (d #t) (k 0)))) (h "0yn2mlr6fyjvvcs4xzaq80dhlfvws4awhpvcb76gv8bg4qa070qs")))

(define-public crate-gemgen-0.2.0 (c (n "gemgen") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gemblockchain") (r "^0.2.0") (d #t) (k 0)))) (h "0d9iqvfyvx785xw5iscykz3smk7806k2ggzi89mbi42qibzjvqxj") (y #t)))

(define-public crate-gemgen-0.2.1 (c (n "gemgen") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "gemblockchain") (r "^0.2.1") (d #t) (k 0)))) (h "1j82jfn43iv8kkgb9q97iai6wss3vaw14gghxn5bfm1m48z7w8kd") (y #t)))

(define-public crate-gemgen-0.3.0 (c (n "gemgen") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gemblockchain") (r "^0.3") (d #t) (k 0)))) (h "1pbahlvi0k5psd8yc09vx71yw5bqk6gvzm96vw9akfqdc4jxw9zb")))

