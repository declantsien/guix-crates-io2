(define-module (crates-io ge dc gedcomx_file) #:use-module (crates-io))

(define-public crate-gedcomx_file-0.1.0-alpha.0 (c (n "gedcomx_file") (v "0.1.0-alpha.0") (d (list (d (n "gedcomx") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "11004klcdqm532cwbgfa4z7wcsbwr4cvp2lgdmkx1g0afl3ijr7d")))

