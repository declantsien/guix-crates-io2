(define-module (crates-io ge dc gedcomx_date) #:use-module (crates-io))

(define-public crate-gedcomx_date-0.0.1 (c (n "gedcomx_date") (v "0.0.1") (d (list (d (n "clippy") (r "~0.0.46") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.2.0") (d #t) (k 0)))) (h "0zlbs0zsqxanljgrhvni9dih4xy47s17i0ld73zwcqncpcv3nhri") (f (quote (("dev" "clippy") ("default") ("bench"))))))

(define-public crate-gedcomx_date-0.0.2 (c (n "gedcomx_date") (v "0.0.2") (d (list (d (n "clippy") (r "~0.0.46") (o #t) (d #t) (k 0)) (d (n "nom") (r "^1.2.0") (d #t) (k 0)))) (h "1qsilp0fznx825iy1vlddmzpsy87yfq8hgwha6a5jz9q9gysfspp") (f (quote (("dev" "clippy") ("default") ("bench"))))))

(define-public crate-gedcomx_date-0.1.0 (c (n "gedcomx_date") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1gnyw0i0ji25m551nhfzlr11sfb9hldyq0ffvsp3qzb25dkpw6xn")))

