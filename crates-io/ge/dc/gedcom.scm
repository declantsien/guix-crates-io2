(define-module (crates-io ge dc gedcom) #:use-module (crates-io))

(define-public crate-gedcom-0.1.0 (c (n "gedcom") (v "0.1.0") (h "0bdr3r233aa3k3zna2jpr1l8ii5s3akk57n6pg3gyxmrbnry939m")))

(define-public crate-gedcom-0.1.1 (c (n "gedcom") (v "0.1.1") (h "02xgsi415dz077d8l4z1b3790bdr4dxjlbvfb0k8w4943d53f803")))

(define-public crate-gedcom-0.2.0 (c (n "gedcom") (v "0.2.0") (h "0djs9ibpqz0xa5n60bnmmyfp5iwdcc989g4fcs214x7l36k3ma4c")))

(define-public crate-gedcom-0.2.1 (c (n "gedcom") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "06grmbph2p4igmh86l0n15h5icmlh5g8cm8z1kngij9x9ha1953m") (f (quote (("json" "serde" "serde_json") ("default"))))))

(define-public crate-gedcom-0.2.2 (c (n "gedcom") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "01326617sqss013pqp7x38pwakfgq0skp6zm4xrz36r72y3kjip4") (f (quote (("json" "serde" "serde_json") ("default"))))))

