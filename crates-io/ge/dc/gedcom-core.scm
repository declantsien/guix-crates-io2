(define-module (crates-io ge dc gedcom-core) #:use-module (crates-io))

(define-public crate-gedcom-core-0.0.1 (c (n "gedcom-core") (v "0.0.1") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 2)) (d (n "insta") (r "^1.8.0") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "0938q3r2ig280qg1fjgwfix0q6kpa47mj4a9w5b134y3l92hl1sv")))

