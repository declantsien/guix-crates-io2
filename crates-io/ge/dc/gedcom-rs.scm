(define-module (crates-io ge dc gedcom-rs) #:use-module (crates-io))

(define-public crate-gedcom-rs-0.1.0 (c (n "gedcom-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "winnow") (r "^0.5.28") (d #t) (k 0)))) (h "1agz62ywfg542n4xxsnsy92wkkrl41r518ixj3mlld7n4lcam7vc")))

