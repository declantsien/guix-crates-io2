(define-module (crates-io ge ns genserver) #:use-module (crates-io))

(define-public crate-genserver-0.1.7 (c (n "genserver") (v "0.1.7") (d (list (d (n "genserver_codegen") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1xqa75695cjb808bnrxjj1gmhn6wvaw6n07f41sfxasrzy9b6jr9")))

(define-public crate-genserver-0.1.8 (c (n "genserver") (v "0.1.8") (d (list (d (n "genserver_codegen") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "02ackb2grjxq6fgmywl2s5ah43k4d457sj6svcxhdwvgxlcj7bis")))

(define-public crate-genserver-0.1.9 (c (n "genserver") (v "0.1.9") (d (list (d (n "genserver_codegen") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "00ikjvdi9nr4a1dn3pbi1m87hg7svmv3qxr6945dk2f78faislba")))

(define-public crate-genserver-0.1.10 (c (n "genserver") (v "0.1.10") (d (list (d (n "genserver_codegen") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "09b5sfcdh5c77k9lca3kf2qpw9mdjxamsv2ly0gy5g8bm03kxy8p")))

(define-public crate-genserver-0.1.11 (c (n "genserver") (v "0.1.11") (d (list (d (n "genserver_codegen") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0kycjy2iky8w2b223h96ypgnsd8g3r842kx9cm1rqgzvif6v57w0")))

(define-public crate-genserver-0.1.12 (c (n "genserver") (v "0.1.12") (d (list (d (n "genserver_codegen") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "06aclcfp2sk5bc6xzxyi18bf2ha64ss1l3lszp8fi99ikpr79v1c")))

(define-public crate-genserver-0.1.13 (c (n "genserver") (v "0.1.13") (d (list (d (n "genserver_codegen") (r "^0.1.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "16r0mhpfi8zyw6j2574nmmi4rb5rlkxai14b1bxaphlrzivr06ad")))

(define-public crate-genserver-0.1.14 (c (n "genserver") (v "0.1.14") (d (list (d (n "genserver_codegen") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0rrs49p3i97d4rhrrb30k4hd8qbk8n2x6alqy5lln6dx9iyi0ir4")))

(define-public crate-genserver-0.1.15 (c (n "genserver") (v "0.1.15") (d (list (d (n "genserver_codegen") (r "^0.1.15") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "13k1dbyfjkbj22w7pfsrrc9vnzfg95qp30r625c3rwlp8pkxif1a")))

(define-public crate-genserver-0.2.2 (c (n "genserver") (v "0.2.2") (d (list (d (n "genserver_codegen") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0nnpq45dr4k9j906kfgv98hd3j8dv5pgpxmawy6fn682i1s0v93r") (r "1.65")))

