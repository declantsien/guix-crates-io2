(define-module (crates-io ge ns genserver_codegen) #:use-module (crates-io))

(define-public crate-genserver_codegen-0.1.4 (c (n "genserver_codegen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14c1lap38rwvdb9b276v0xbb0h9qr0pldn7hsmb5679ikmzwbg2d")))

(define-public crate-genserver_codegen-0.1.5 (c (n "genserver_codegen") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18jakprha7w1vhjqig6xirn6b9x1yqvprh0jbk1cpvw385sqdbbf")))

(define-public crate-genserver_codegen-0.1.6 (c (n "genserver_codegen") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0is4lvqzp744c2f5f6kdjwklfi0dfgw1fgyc5b8hh0sr4gaf3sj3")))

(define-public crate-genserver_codegen-0.1.7 (c (n "genserver_codegen") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yqmpw4kag08v36k6mvh4knkarxm6zw4inb1nq90zx39vpg9v3xn")))

(define-public crate-genserver_codegen-0.1.8 (c (n "genserver_codegen") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0bh2yp6hza67bldx675r59yfa4v7gw8d2k5p88ack5lzlahmjb")))

(define-public crate-genserver_codegen-0.1.9 (c (n "genserver_codegen") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07snvgib659qlxql0j39wqv08sdhd3sgdlmfz4d7zjcgq35fz1pr")))

(define-public crate-genserver_codegen-0.1.10 (c (n "genserver_codegen") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10nwzlgc7w5qz7c7grcq0j90q095svj4gj9z9n4fbiycifmvh45s")))

(define-public crate-genserver_codegen-0.1.11 (c (n "genserver_codegen") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hw108nks29ljfw1k1phnyiln151g6cd79f15hkj7vy0vj70z81y")))

(define-public crate-genserver_codegen-0.1.12 (c (n "genserver_codegen") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vp5lpqkvxnma5an6l33i39rm5w61vcrlimw08i1qwz8gpmhm4ws")))

(define-public crate-genserver_codegen-0.1.13 (c (n "genserver_codegen") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04far12m7391w2mxibgvb94y651m9kncnr3x5j60bgdyn4lpy6nc")))

(define-public crate-genserver_codegen-0.1.14 (c (n "genserver_codegen") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02h3nb6azd47zi05nsd9lq7gz4i37a4d133ky3cdmnx3xf3xhkl7")))

(define-public crate-genserver_codegen-0.1.15 (c (n "genserver_codegen") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08qc63b94i7cgg1ny6bkvl6akr333y7y75a8pja5wcb819yw7kwi")))

(define-public crate-genserver_codegen-0.2.2 (c (n "genserver_codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nijl9wffyn5m6szsc5i5z6dz83iqq5qpsl9hd71skp7dvk2fc4k") (r "1.65")))

