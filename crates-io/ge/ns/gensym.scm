(define-module (crates-io ge ns gensym) #:use-module (crates-io))

(define-public crate-gensym-0.1.0 (c (n "gensym") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1kfcc9clq596chsqbmlldkrb8jcvwdgbnmryia0pbw6b4pz2iczw")))

(define-public crate-gensym-0.1.1 (c (n "gensym") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "10iwpbjapcdg9wjhrsc32dmzr2dcfzvhd30pzi0fmhh6bx6cwgci")))

