(define-module (crates-io ge ns genson-rs) #:use-module (crates-io))

(define-public crate-genson-rs-0.1.0 (c (n "genson-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codspeed-criterion-compat") (r "^2.6.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mimalloc") (r "^0.1.41") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 0)))) (h "0dn0pwrx8bx49n2w3jfzw7axlg393zzgg7gr0wbwgyzz958jaf2r")))

(define-public crate-genson-rs-0.2.0 (c (n "genson-rs") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codspeed-criterion-compat") (r "^2.6.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "mimalloc") (r "^0.1.41") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "simd-json") (r "^0.13.10") (d #t) (k 0)))) (h "0qd56lf2x7w6pil8m9bvcmm54mvdfjjsm6v2pl7vmm2bcm641qpp")))

