(define-module (crates-io ge ns gensoquote) #:use-module (crates-io))

(define-public crate-gensoquote-0.4.1 (c (n "gensoquote") (v "0.4.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c9y9sm6i9zvb8cv3adx5id53sb3bkpfd4dck469dq2abqk6nk97")))

