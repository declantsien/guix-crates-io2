(define-module (crates-io ge ns gensokyo_engine) #:use-module (crates-io))

(define-public crate-gensokyo_engine-0.1.0 (c (n "gensokyo_engine") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0f2kc7ayr0df994gcgm0pk9dnj0iyirjwk4ylypqyz89dkc7vvaz") (y #t)))

