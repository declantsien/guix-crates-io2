(define-module (crates-io ge to getopts-long) #:use-module (crates-io))

(define-public crate-getopts-long-0.1.0 (c (n "getopts-long") (v "0.1.0") (h "0drbxaf58ygjg5fgri5xc24kjxlv9sis0ba1nm38ch5mnhj1srrj")))

(define-public crate-getopts-long-0.1.1 (c (n "getopts-long") (v "0.1.1") (h "02ji8hf22lnbg60agjc9vmpzysv3cak3kh9bghga6fqjhqwm17h9")))

