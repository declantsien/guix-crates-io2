(define-module (crates-io ge to getopt-long) #:use-module (crates-io))

(define-public crate-getopt-long-0.1.0 (c (n "getopt-long") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "002ks8p8nh5cxss2npjyrh57dvq3jcvrlj01apljs9j74ghv3xsw")))

(define-public crate-getopt-long-0.1.1 (c (n "getopt-long") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "0hdv6dx2gxxwlgp2393zdwf8h00m789bk1lxjf35ca15sbxgi02i")))

(define-public crate-getopt-long-0.1.2 (c (n "getopt-long") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "1bfws1cxkqqpg52yjhdywddiaas6ml0bkglhmi8lr5rqicpnxqrk")))

(define-public crate-getopt-long-0.1.3 (c (n "getopt-long") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "0aaaxr7zsi4k4gd09vlhddzv37679nr3xjlpsc7vykfwv9239c92")))

(define-public crate-getopt-long-0.1.5 (c (n "getopt-long") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "01w5q9hpxdg2wi9nccrjrvrvh09fh0mfv4dfnqf19asm00fqdg2w")))

(define-public crate-getopt-long-0.2.0 (c (n "getopt-long") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "19xw5b64maf8bvw1zkki79ic1p3irbx81nfcs9d2dyi7796vlv3s")))

(define-public crate-getopt-long-0.2.1 (c (n "getopt-long") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "1zl7yy8i040blnaai3dwick2slnxcibnfi4yc20jz8k0ydh5grwm")))

(define-public crate-getopt-long-0.2.2 (c (n "getopt-long") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "1bfnr42ing3i9rwk1jp42ns61l3f82hm3fg2sq8y532fqj8xzid3")))

(define-public crate-getopt-long-0.3.0 (c (n "getopt-long") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "0jk78rzkw945vmn8hpai1zfvz1njzrr244kziri8n5wcjxniy626")))

