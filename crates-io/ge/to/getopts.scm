(define-module (crates-io ge to getopts) #:use-module (crates-io))

(define-public crate-getopts-0.1.0 (c (n "getopts") (v "0.1.0") (h "0ma8hcswnq9mm6xf6p6qbqdkgnk8xqn0lbhy61lg9wyz312k13i2")))

(define-public crate-getopts-0.1.1 (c (n "getopts") (v "0.1.1") (h "0r7cc9xmivh0y4dwjs0wlbh2lnhk29xscccid1k277xhhxg5vvd7")))

(define-public crate-getopts-0.1.2 (c (n "getopts") (v "0.1.2") (h "0frhsq1sbxlmmlz14cp9rnpchnnm9z0nqia8nv3xgafq0h86b6mc")))

(define-public crate-getopts-0.1.3 (c (n "getopts") (v "0.1.3") (h "1wdslw603vqg96csch9nrggkaimnx2msy96bjixmp9h6fs5x4jhg")))

(define-public crate-getopts-0.1.4 (c (n "getopts") (v "0.1.4") (h "1cydr3q45sl98c6hcx8np9l070c9r2prb9bhfdpzxzvpzja2pzpa")))

(define-public crate-getopts-0.2.0 (c (n "getopts") (v "0.2.0") (h "1zgvaqpg3rc0kvwicscx3w5pj0dn00ds86ddgdv80d4r7banygni")))

(define-public crate-getopts-0.2.1 (c (n "getopts") (v "0.2.1") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "1fhnd0xbhj14v9jf5ln890v5mfwr6d0ds0nnljca6m6w3pb9yw95")))

(define-public crate-getopts-0.2.2 (c (n "getopts") (v "0.2.2") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "14hysvyslgqvbznc8d5g0b6qnd4yxfhylnipz632q67p9yqvry7b")))

(define-public crate-getopts-0.2.3 (c (n "getopts") (v "0.2.3") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "14c1fhgz7l4jvszmlq5v8yvkzhx1jhjhs2xhdl4xnw5m0mrzpq0w")))

(define-public crate-getopts-0.2.4 (c (n "getopts") (v "0.2.4") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "03gdblzazk01rbbap7jagx1w3pwmikc4lrpqs5dnsk4w6wl6lv34")))

(define-public crate-getopts-0.2.5 (c (n "getopts") (v "0.2.5") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)))) (h "0yyf02axlwnhkk29656j0rdhscr2ppwa46dnf3d0773r7gbim60c")))

(define-public crate-getopts-0.2.6 (c (n "getopts") (v "0.2.6") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1r444fdqqa3l2yw46arafykxmw4fbjzdsv0wbgs0swy1mai881ai")))

(define-public crate-getopts-0.2.7 (c (n "getopts") (v "0.2.7") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0j499c6kafjd5lyncg583dhj8366hhp273q9dcjwfyx41m27q81f")))

(define-public crate-getopts-0.2.8 (c (n "getopts") (v "0.2.8") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0222xy9bhmi386d1anq3krfvl0x1ssi01camlanvd5nsxwmqawm1")))

(define-public crate-getopts-0.2.9 (c (n "getopts") (v "0.2.9") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0zgpjaw1y1ip2nmw2jmiawdi21xwc8gy6wmfaz4f9kj6mxg9rarv")))

(define-public crate-getopts-0.2.10 (c (n "getopts") (v "0.2.10") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1ipivajw7wmq98mkycf5sfzi7d2zqx4x24mw0j4igh25nzfs9glc")))

(define-public crate-getopts-0.2.11 (c (n "getopts") (v "0.2.11") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "05g67hi28wgsvjspifqsgspxqihy7zwaiszp6pzzw7is8h9qsya5")))

(define-public crate-getopts-0.2.12 (c (n "getopts") (v "0.2.12") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "11ixqrx5mdpb79sgqdc65zm4ig9yjnail8pyfs1dspgcn74qhw29")))

(define-public crate-getopts-0.2.13 (c (n "getopts") (v "0.2.13") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1lrny7zmyl9ys6m6fvylg9i3zzg57fjraps987frjvrylddg0fn1")))

(define-public crate-getopts-0.2.14 (c (n "getopts") (v "0.2.14") (d (list (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "11fnh4awj227c85ax1sixbl5yb25c3qmxlv3nd870hwas3xpq16r")))

(define-public crate-getopts-0.2.15 (c (n "getopts") (v "0.2.15") (d (list (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "1yggwpnqcpqbaa1dvhyrgal593k6cvfbmbhfxfi03wfjmdqji4k5")))

(define-public crate-getopts-0.2.16 (c (n "getopts") (v "0.2.16") (d (list (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "0zwzlb6y98mm9wzg92d9mqgnida8m1qhbcqrj8bw57axwjvbbbak")))

(define-public crate-getopts-0.2.17 (c (n "getopts") (v "0.2.17") (d (list (d (n "log") (r "^0.3") (d #t) (k 2)))) (h "018yhq97zgcrcxwhj3pxh31h83704sgaiijdnpl0r1ir366c005r")))

(define-public crate-getopts-0.2.18 (c (n "getopts") (v "0.2.18") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "15xpa5ljczsylavlnm7y2v2y91iaa41drxalncj59yrj079r4wha")))

(define-public crate-getopts-0.2.19 (c (n "getopts") (v "0.2.19") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0l74ldicw6gpkly3jdiq8vq8g597x7akvych2cgy7gr8q8apnckj")))

(define-public crate-getopts-0.2.20 (c (n "getopts") (v "0.2.20") (d (list (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "09vkkz6flm0hb5bpmx8dqlxg6h46vb25acdncpwc4wfx6f7lz990") (f (quote (("rustc-dep-of-std" "unicode-width/rustc-dep-of-std" "std"))))))

(define-public crate-getopts-0.2.21 (c (n "getopts") (v "0.2.21") (d (list (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "std") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-std")) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1mgb3qvivi26gs6ihqqhh8iyhp3vgxri6vwyrwg28w0xqzavznql") (f (quote (("rustc-dep-of-std" "unicode-width/rustc-dep-of-std" "std" "core"))))))

