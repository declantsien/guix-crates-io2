(define-module (crates-io ge to getopt3) #:use-module (crates-io))

(define-public crate-getopt3-1.2.0 (c (n "getopt3") (v "1.2.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "06441xn8akayj5w90vdywif73kbv850sbblw6rbrxsg8a197vfnl")))

(define-public crate-getopt3-1.2.1 (c (n "getopt3") (v "1.2.1") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "085yzfdmg86b0kbzls2hc4kvfq9mz5nd33zpm146v6cddkxwbixg")))

(define-public crate-getopt3-2.0.0 (c (n "getopt3") (v "2.0.0") (d (list (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1737iy6gmwb4h7xzmnx6yhzlliv5kg131n55a3bpm6hyjjhrakmy")))

(define-public crate-getopt3-2.0.1 (c (n "getopt3") (v "2.0.1") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "01hchvpqfkfgz4ydz59b1kmnkrf840jhwr8syinfaql0wxavgri9") (f (quote (("no_std") ("default"))))))

