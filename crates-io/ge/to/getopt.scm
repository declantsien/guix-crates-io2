(define-module (crates-io ge to getopt) #:use-module (crates-io))

(define-public crate-getopt-0.1.0 (c (n "getopt") (v "0.1.0") (h "0fr8wcavaqkcill7dysz8vdav79jg9v2z97246q60g8rfy3rq18d")))

(define-public crate-getopt-0.2.0 (c (n "getopt") (v "0.2.0") (h "03rvjm5xnqyca6r8ddpd0j8rijps54dj8wzdmi9fbvnrq97gz1rp")))

(define-public crate-getopt-0.2.1 (c (n "getopt") (v "0.2.1") (h "08bv0jccy73krxwhfgv8lalvy00yzi0ib4b2hwy1xfrdlkizvkpi") (y #t)))

(define-public crate-getopt-0.2.2 (c (n "getopt") (v "0.2.2") (h "0fx35phlrsmr3syjwj272jql0mqva3q706br0fy8vyxz1d7g1rsn")))

(define-public crate-getopt-0.3.0 (c (n "getopt") (v "0.3.0") (h "0l0smqwnyxavysmyc5vbdlnsazwn7lfp424bcv5xkz11i6wn1igx")))

(define-public crate-getopt-0.3.1 (c (n "getopt") (v "0.3.1") (h "131i657s4cxii1v7w3jrsrzzxhr0nq3n6kz9kmdzhbndrp1xvacv")))

(define-public crate-getopt-0.3.2 (c (n "getopt") (v "0.3.2") (h "160ialipmpzj1gp3pkw7d4p5c76wsj2c42l07mf7xwvj87asbajq")))

(define-public crate-getopt-0.3.3 (c (n "getopt") (v "0.3.3") (h "181yccsnilxwhmc7r72cvzng20x9s9p7szqm0hj087vgs3d6w538")))

(define-public crate-getopt-0.3.4 (c (n "getopt") (v "0.3.4") (h "1f3pqcp7lb1z1lkcip7b38pcbi2k3vd9q3d2alxsyvxz6nhzwx4x")))

(define-public crate-getopt-0.3.5 (c (n "getopt") (v "0.3.5") (h "0cnliqyir4b483zgpz94fcdh1n7dwihys357n79h3czbqlzvp3mj")))

(define-public crate-getopt-0.3.6 (c (n "getopt") (v "0.3.6") (h "1ki2w1sawffxnn5bsb97v0hg6ndl5a3g1xaff85j44ymb8aggvbp")))

(define-public crate-getopt-0.3.8 (c (n "getopt") (v "0.3.8") (h "18q276vmj44fw4qihdp18h2s1s6ffbj3b9v79plsixwj5rvvi53g")))

(define-public crate-getopt-0.3.9 (c (n "getopt") (v "0.3.9") (h "1ykyysrspdwgqcp8rdx6fkqmx79rnafal1iyrb4109kws42m63am")))

(define-public crate-getopt-0.4.0 (c (n "getopt") (v "0.4.0") (h "0dl0dv6bcqfkm9xj7l0q9yq5sngai4fpvgp8h4qk8bb0mbyi6b9y")))

(define-public crate-getopt-0.4.1 (c (n "getopt") (v "0.4.1") (h "0v1rpplirhbjaf45lh0dyqxx7kl5rzgcv7zb9hi0rz49jyqv30gg")))

(define-public crate-getopt-0.4.2 (c (n "getopt") (v "0.4.2") (h "0xva00848bjf1mwmb82c6v09vl3dm2hyzsgdyk1irr323nfyskvv")))

(define-public crate-getopt-0.4.3 (c (n "getopt") (v "0.4.3") (h "08pw5r3khnrpn14mad1v4b8w6fsajznjhx66p9l3l0n0vym2alm9")))

(define-public crate-getopt-1.0.0 (c (n "getopt") (v "1.0.0") (h "0fkqf193r9gfkyj1pna8k2qg3fml9b4zmbpzzyqbvnaa1ns2062q")))

(define-public crate-getopt-1.0.1 (c (n "getopt") (v "1.0.1") (h "0h11nl11d9jgxq8wmklj643jz9sigvpj5gn7q35sdy6n4fz94mav")))

(define-public crate-getopt-1.0.2 (c (n "getopt") (v "1.0.2") (h "00n3dqrxh1qm282c25ziwgg093llk2db5gnp7rkxzspij0j7z5h9")))

(define-public crate-getopt-1.0.3 (c (n "getopt") (v "1.0.3") (h "0kldz60xww36bykmhfzc4bm8pph2d03wfp723hkny2qn1xagg5h8")))

(define-public crate-getopt-1.1.0 (c (n "getopt") (v "1.1.0") (h "18j9mjbgg668zqqk5wm66r9l40ns7fracifxfh7a00wcpiqvjp18")))

(define-public crate-getopt-1.1.1 (c (n "getopt") (v "1.1.1") (h "02shdxcn8a7bqg7qv1hsc621q7n0sd16zf6bkbbwswmsm0ywkihn")))

(define-public crate-getopt-1.1.2 (c (n "getopt") (v "1.1.2") (h "0d4dvbajfsfpcmfd3wvi7cdqbkhbpqqzh1ix5jigzc7nldy24gqm")))

(define-public crate-getopt-1.1.3 (c (n "getopt") (v "1.1.3") (h "1iz3lm6sl3m2irx2bzv517133ywakxcvsxlpkf5c5rqsrgk8j4pf")))

(define-public crate-getopt-1.1.4 (c (n "getopt") (v "1.1.4") (h "0yn4iz04y1l8ljmh20zjjlfm6ma6kg1ayy7155qca32y78jrjdlz")))

(define-public crate-getopt-1.1.5 (c (n "getopt") (v "1.1.5") (h "1hw3d54gphrmhw1ym1vk8hd617zckp56jswz3l9n1qjx807qr1xb") (r "1.30.0")))

(define-public crate-getopt-1.1.6 (c (n "getopt") (v "1.1.6") (h "01r8shfvh7ai2svqcs91s2h2n4hyv569kjrq2nzyvp5yz4ilwyym") (r "1.30.0")))

