(define-module (crates-io ge to getopt_rs) #:use-module (crates-io))

(define-public crate-getopt_rs-0.1.0 (c (n "getopt_rs") (v "0.1.0") (h "12w7gwjz3pyjgaz4rgr24q7ipvbznzz9bswjpgz24gsbqyzfz0nx")))

(define-public crate-getopt_rs-0.1.1 (c (n "getopt_rs") (v "0.1.1") (h "0ygc27c58f5ivw1mzhhx52bf75wa17x5mra411zn4ww91ahlaxsk")))

(define-public crate-getopt_rs-0.1.2 (c (n "getopt_rs") (v "0.1.2") (h "1bxcgi3hnagiykyi6rx8s8jz70hqndi36bbw3h7lpr654csq0lzm")))

(define-public crate-getopt_rs-0.1.3 (c (n "getopt_rs") (v "0.1.3") (h "1dlgmyqzbr584dxn9p3l8xlc517vxmcb0k8wlxz573zq0ldzmcnx")))

