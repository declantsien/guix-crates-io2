(define-module (crates-io ge ef geefr-ppm) #:use-module (crates-io))

(define-public crate-geefr-ppm-0.1.0 (c (n "geefr-ppm") (v "0.1.0") (h "0qg4cf8rqfar1mjd719c66rcjggzab6kwhz8q3z7lasbp8jdjr3p")))

(define-public crate-geefr-ppm-0.1.1 (c (n "geefr-ppm") (v "0.1.1") (h "1j4zham44a6s2xlzmqcafrpb6dhwa6h8c6kix44im488avwr8mhi")))

(define-public crate-geefr-ppm-0.1.2 (c (n "geefr-ppm") (v "0.1.2") (h "07iszsy82pqma3yrxmlsxvjdd9xj5jw8n2f8ccih7zipzypz0000")))

(define-public crate-geefr-ppm-0.1.3 (c (n "geefr-ppm") (v "0.1.3") (h "15if4sbvbnzfipd8h7sngykq7bvmj7pnhsyigay9gbh97jz8rmk0")))

(define-public crate-geefr-ppm-0.2.0 (c (n "geefr-ppm") (v "0.2.0") (h "17z2nfwdnhh9ak7x91bzcw0afqrki0n1vwnjaxcprrhxnygyd262")))

