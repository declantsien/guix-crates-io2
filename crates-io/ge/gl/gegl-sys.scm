(define-module (crates-io ge gl gegl-sys) #:use-module (crates-io))

(define-public crate-gegl-sys-0.1.0 (c (n "gegl-sys") (v "0.1.0") (d (list (d (n "babl-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0ag7h86g4z7rw52dr4sm0jbm14n8yb1va33jb42h48myabyjgl60") (f (quote (("v0_4_2") ("v0_4_14" "v0_4_2"))))))

