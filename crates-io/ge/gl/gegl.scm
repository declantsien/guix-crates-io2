(define-module (crates-io ge gl gegl) #:use-module (crates-io))

(define-public crate-gegl-0.1.0 (c (n "gegl") (v "0.1.0") (d (list (d (n "babl") (r "^0.1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "gegl-sys")) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0afgi9qpnr6xcjsvasy0pajpy3ak2hrfsghs5vm2hklkfjccj6jr") (f (quote (("v0_4_2" "ffi/v0_4_2") ("v0_4_14" "ffi/v0_4_14" "v0_4_2"))))))

(define-public crate-gegl-0.1.1 (c (n "gegl") (v "0.1.1") (d (list (d (n "babl") (r "^0.1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "gegl-sys")) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hm743b80bh41jp02qxgwr28vaqxrn2nc5pp5ix35r6c886a1gjj") (f (quote (("v0_4_2" "ffi/v0_4_2") ("v0_4_14" "ffi/v0_4_14" "v0_4_2"))))))

