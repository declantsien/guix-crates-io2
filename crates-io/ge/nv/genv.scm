(define-module (crates-io ge nv genv) #:use-module (crates-io))

(define-public crate-genv-0.1.1 (c (n "genv") (v "0.1.1") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0042n7mqlmipcvdl5j8psjzrq85y3jqj1p16riws3picjzrp8br8")))

(define-public crate-genv-0.1.2 (c (n "genv") (v "0.1.2") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "16rgpdxybh6vxd7lwv5gdzal08xv5rm1ykz5gsv1nvckph3pkcv7")))

(define-public crate-genv-0.1.3 (c (n "genv") (v "0.1.3") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04jgpblgq90c21sdb6dm3ik91vhkcxg3ix4fhcgc49z3iwlxf00k")))

(define-public crate-genv-0.1.5 (c (n "genv") (v "0.1.5") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1iaan1c5a1s25hlb53hg1w0lmzf6fza9k3z9qzbi4s6w67zmlkj4")))

(define-public crate-genv-0.1.6 (c (n "genv") (v "0.1.6") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0pccca49s3yav32mqr7cni9gymmfdqmi6w0dmpdn9j64cj222yb2")))

(define-public crate-genv-0.1.7 (c (n "genv") (v "0.1.7") (d (list (d (n "const-str") (r "^0.5.6") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0s24h9msb682g61fshzalmp4l20cq2q910njypyh5q9g0gsazf64")))

(define-public crate-genv-0.1.8 (c (n "genv") (v "0.1.8") (d (list (d (n "const-str") (r "^0.5.7") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0ckk5ajqmxm36hrhphysgp87x3s23xvyqcmzvqzjcsqkhqbd9sjm")))

