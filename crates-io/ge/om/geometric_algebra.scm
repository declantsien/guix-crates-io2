(define-module (crates-io ge om geometric_algebra) #:use-module (crates-io))

(define-public crate-geometric_algebra-0.1.0 (c (n "geometric_algebra") (v "0.1.0") (h "1wqa80kni5101nw9pgvqbxmpcnz9g215smfzhvhpfgqnqly0p5qc")))

(define-public crate-geometric_algebra-0.1.1 (c (n "geometric_algebra") (v "0.1.1") (h "1hm6cmakv2idjf8ifa36ch3gsbqjbbnb5vrw5gz87ly5aa1qgg66")))

(define-public crate-geometric_algebra-0.1.2 (c (n "geometric_algebra") (v "0.1.2") (h "1liwgdwmb4mizx1bwywkd7ac4ghr6pzw7hsgbb51rjfkfwjq893h")))

(define-public crate-geometric_algebra-0.1.3 (c (n "geometric_algebra") (v "0.1.3") (h "0ygqccb26k3y14y9xs4qkfjlbc2c5nzbbfqm2iggrswkz75950k0")))

(define-public crate-geometric_algebra-0.2.0 (c (n "geometric_algebra") (v "0.2.0") (h "1y6zrkdzcygj9034dl92jcm9nblhkxcl38jvzzy8ibwzwipjba2m")))

(define-public crate-geometric_algebra-0.2.1 (c (n "geometric_algebra") (v "0.2.1") (h "0sih8xr586pigilh9x7big8ip6cy7s39wmn2y7m6djwf1wd1y457")))

(define-public crate-geometric_algebra-0.3.0 (c (n "geometric_algebra") (v "0.3.0") (h "0n89sad5wcavjcyydww07mq1jafwqiw95gfn5rjbi1196v7im96y")))

