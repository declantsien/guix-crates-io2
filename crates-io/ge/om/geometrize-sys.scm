(define-module (crates-io ge om geometrize-sys) #:use-module (crates-io))

(define-public crate-geometrize-sys-0.1.0 (c (n "geometrize-sys") (v "0.1.0") (h "0hkdkj590wy7cy0fwb32lb0afs1zc2m33bx2szbjqdk21a01hrdh")))

(define-public crate-geometrize-sys-0.1.1 (c (n "geometrize-sys") (v "0.1.1") (h "12lzi9v9h0s61fxb8raxhi31b3gpx2mpdhnvnj4bpv440drihb3v")))

