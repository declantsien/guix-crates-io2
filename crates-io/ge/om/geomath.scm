(define-module (crates-io ge om geomath) #:use-module (crates-io))

(define-public crate-geomath-0.2.0 (c (n "geomath") (v "0.2.0") (h "0h1lvapkhawajxj9zazhwnbnkxf561s45c8dwczgx7i1wcpygd8a")))

(define-public crate-geomath-0.2.1 (c (n "geomath") (v "0.2.1") (h "0vr7733sy67s7kay6s7y94dw37kgpggggbb14sad8gh4xyqwzzqw")))

(define-public crate-geomath-0.2.2 (c (n "geomath") (v "0.2.2") (h "1k15zmsmcn4hc7ly4ramjv9a9kdjhdbi9ilafdk56204hi7nnfic")))

(define-public crate-geomath-0.2.3 (c (n "geomath") (v "0.2.3") (h "0c3qihriqr7yk317k86n7wacmm46hbhp2vw0l5l8im5sagy47yx7")))

(define-public crate-geomath-0.2.4 (c (n "geomath") (v "0.2.4") (h "0an9phpidrif3hv2f18wkzhz0dw4ds3rdmis9kjjgkf2nbk8xrij")))

