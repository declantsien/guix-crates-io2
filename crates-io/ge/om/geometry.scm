(define-module (crates-io ge om geometry) #:use-module (crates-io))

(define-public crate-geometry-0.0.0 (c (n "geometry") (v "0.0.0") (d (list (d (n "range") (r "^0.0.6") (d #t) (k 0)))) (h "1xa3rd3jcyi7vls3pyf13izc9chhbs2czs5iacar4mycnj9zb7d4")))

(define-public crate-geometry-0.1.0 (c (n "geometry") (v "0.1.0") (d (list (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "wavefront_obj") (r "^2.0.2") (d #t) (k 0)))) (h "1m82aiw0iqf60h1mskfhncc10a2q6wi34wkx03z1yrv5fv1rzvqp")))

(define-public crate-geometry-0.2.0 (c (n "geometry") (v "0.2.0") (d (list (d (n "range") (r "^0.2.0") (d #t) (k 0)) (d (n "wavefront_obj") (r "^2.0.3") (d #t) (k 0)))) (h "17a8av57aabcjx0icad6nz4mm3gilqxs1kvr59a61qy52rf4mdqn")))

