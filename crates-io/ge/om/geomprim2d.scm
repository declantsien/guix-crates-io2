(define-module (crates-io ge om geomprim2d) #:use-module (crates-io))

(define-public crate-geomprim2d-0.1.0 (c (n "geomprim2d") (v "0.1.0") (d (list (d (n "scalar") (r "^0.1.0") (d #t) (k 0)))) (h "024wncqb8l2g1xw81b567463ikqmpcmd22d4qq7zsfy15a9vl58d")))

(define-public crate-geomprim2d-0.1.1 (c (n "geomprim2d") (v "0.1.1") (d (list (d (n "scalar") (r "^0.1.8") (d #t) (k 0)))) (h "0z9kv8xr9wk8qbszsgahxvx9rq29hc1j8r57rwd4f1d9b4524srr")))

