(define-module (crates-io ge om geom) #:use-module (crates-io))

(define-public crate-geom-0.1.0 (c (n "geom") (v "0.1.0") (h "0ngwxcj419d2b78cfws7sk86z6789lfb9nd14hl0mip6rq7cvpdq")))

(define-public crate-geom-0.2.0 (c (n "geom") (v "0.2.0") (h "0c1bnwn039wa24rv3w0v8kcmw6lqxqr7dk9q0l1yqjhqgnzz2ly0")))

(define-public crate-geom-0.2.1 (c (n "geom") (v "0.2.1") (h "14w39s1b5xqbidr0n8pw1hlpm35rmpwqmknp0y116n56wvchrmi8")))

(define-public crate-geom-0.2.2 (c (n "geom") (v "0.2.2") (h "0gkvacfmsb38k8wvqb6hjc4scbj5nnf3j97vlkvx9cazml6xnmch")))

(define-public crate-geom-0.2.3 (c (n "geom") (v "0.2.3") (h "03qqi46rpkvmvp9vzg6nbkygz2pg3bb0i19dl8f8rfh896sqnq5i")))

