(define-module (crates-io ge om geometry_tools) #:use-module (crates-io))

(define-public crate-geometry_tools-0.1.0 (c (n "geometry_tools") (v "0.1.0") (d (list (d (n "glam") (r "^0.15") (d #t) (k 0)))) (h "0yxhrmfr125l6f9bjsb1n489113isljz2443qmn1h0nf7190m2rh")))

(define-public crate-geometry_tools-0.2.0 (c (n "geometry_tools") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.15") (d #t) (k 0)))) (h "05i5gfvyfyfhmxcd53dy2r6bd2xmf7fr6rdnyv1xcnhfqrn8cph1")))

(define-public crate-geometry_tools-0.3.0 (c (n "geometry_tools") (v "0.3.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03q0mv62zdy37qb0gvpwhc48y63iin95y19qiknvrfain64rjdar")))

(define-public crate-geometry_tools-0.3.1 (c (n "geometry_tools") (v "0.3.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rljqmdpcvfil1sqzwflax6f0kyic9n3wkjpyqyxh2i8s95jhgja")))

(define-public crate-geometry_tools-0.4.0 (c (n "geometry_tools") (v "0.4.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0689h1vhqacihakgibrx5vh72j5skd46ks185cb7ifzvirrmjavz")))

(define-public crate-geometry_tools-0.4.1 (c (n "geometry_tools") (v "0.4.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g7f8kp2zjhvi7za9s6bli3b7vkfva3p37r321l5xd94s1hwmp8z")))

(define-public crate-geometry_tools-0.4.2 (c (n "geometry_tools") (v "0.4.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01ycw409d6wn8nb3nz4ghlnl1jv8zf4dc5xqfrkzlnc9cn3mm4kf")))

(define-public crate-geometry_tools-0.5.0 (c (n "geometry_tools") (v "0.5.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b0kfk0crsqvhkpdjd614i4hhwxqh3ybrigf9v6l840qz7hdddaw")))

