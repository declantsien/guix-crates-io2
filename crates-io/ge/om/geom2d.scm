(define-module (crates-io ge om geom2d) #:use-module (crates-io))

(define-public crate-geom2d-0.1.0 (c (n "geom2d") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "08wm5gzlcvwv9v2l8ikj6gjnmzfgzp4n5nkdpzdxk0prdcd1ng7x")))

(define-public crate-geom2d-0.1.1 (c (n "geom2d") (v "0.1.1") (d (list (d (n "fast_inv_sqrt") (r "^1.0.1") (d #t) (k 0)))) (h "18zj4c3rzplzmglv707bl7sm3y1gmmbdg1sa3fd910dki2nd88f8")))

(define-public crate-geom2d-0.1.2 (c (n "geom2d") (v "0.1.2") (d (list (d (n "fast_inv_sqrt") (r "^1.0.1") (d #t) (k 0)))) (h "0z14f842pzf9jj07q8s8hp774jmwgwgfxyzfqgv7dc464dfjscnw")))

(define-public crate-geom2d-0.1.3 (c (n "geom2d") (v "0.1.3") (h "1if8sms0yx3bzcrzig9kmhnrvvbpn5hamml5jkpkjxsic8fhsm1w")))

(define-public crate-geom2d-0.1.4 (c (n "geom2d") (v "0.1.4") (h "07ffxbg0rn7h7mxddpyyzf0c87bq18dq5jmf7ibbg31qa8j7nwiq")))

(define-public crate-geom2d-0.1.5 (c (n "geom2d") (v "0.1.5") (h "0g3a4gxfrps3alx8l2s9ngd5bzzq1586axmh2qaxc4my3qy71maa")))

(define-public crate-geom2d-0.1.6 (c (n "geom2d") (v "0.1.6") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "1rl1spqq3ii2lrrb4qbr6iql3jbxi1ylxws2plh3pbxh4g4l4hrw")))

(define-public crate-geom2d-0.1.7 (c (n "geom2d") (v "0.1.7") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "09940vb3s6d74hfbp4028q42j7yxndkv1r0kz4gyvlig10dghvb4")))

(define-public crate-geom2d-0.1.8 (c (n "geom2d") (v "0.1.8") (d (list (d (n "auto_ops") (r "^0.3.0") (d #t) (k 0)))) (h "0ba3rgi2ix4iydx7j9jq8y2q2z473xq07bhlx05wqla01nrb5zqb")))

