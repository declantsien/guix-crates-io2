(define-module (crates-io ge om geom_2d) #:use-module (crates-io))

(define-public crate-geom_2d-0.1.0 (c (n "geom_2d") (v "0.1.0") (h "1l34xfcqihbcfagbz4a1s4l7jaxa38gnnw1l5j2xv52kr37r1i85")))

(define-public crate-geom_2d-0.1.1 (c (n "geom_2d") (v "0.1.1") (h "0hiap19zdw509ndaj920ghsj0wcz9dn0x03bwljxl2ki2w6px1f0")))

(define-public crate-geom_2d-0.2.0 (c (n "geom_2d") (v "0.2.0") (h "16f9j1pmi767x40piiwmp3y4imjggn46yspn3mg33yrb7b2ryv3g")))

