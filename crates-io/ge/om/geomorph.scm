(define-module (crates-io ge om geomorph) #:use-module (crates-io))

(define-public crate-geomorph-0.1.0 (c (n "geomorph") (v "0.1.0") (h "0673cmjd2vf5ypzs6xd2fa8f46d12mzbn1n8qwmwjq2hlzh3iyx1")))

(define-public crate-geomorph-0.2.0 (c (n "geomorph") (v "0.2.0") (h "19r7jwbjgv2pbais948mlpxp613zw0mqb03r2zqm21ns060i6zzn")))

(define-public crate-geomorph-0.2.1 (c (n "geomorph") (v "0.2.1") (h "0w7br9ig5k62y4dzcdzb0l4dhfsax1qsdkzhsniy0mn0pl72ggka")))

(define-public crate-geomorph-0.2.2 (c (n "geomorph") (v "0.2.2") (h "1pnpxb6wfl9c2h6w9navld33yjl56rngpvxgnl9mk91503jfrnck")))

(define-public crate-geomorph-0.3.0 (c (n "geomorph") (v "0.3.0") (h "0ckvrv7ag3nd6168ya63rhbiaah9p3vp9v4rrdaf9spksnjrjxq4")))

(define-public crate-geomorph-0.4.0 (c (n "geomorph") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "08sx4vzpagmh7n341m7padnbkps318pmcph8skjrz61qf19vv86x")))

(define-public crate-geomorph-1.0.0 (c (n "geomorph") (v "1.0.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0ksayg4l3v7kgf907bdn2k6d4lw9j0acalmsx1nyggzhc7csz4ji")))

(define-public crate-geomorph-1.1.0 (c (n "geomorph") (v "1.1.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "01cv70rrcavjbxnyarn1y3rxjb7j0q60idnbmsyvgj6qbdlvp5gv")))

