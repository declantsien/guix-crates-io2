(define-module (crates-io ge om geometrydash) #:use-module (crates-io))

(define-public crate-geometrydash-0.1.0 (c (n "geometrydash") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "minhook") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("libloaderapi" "memoryapi"))) (d #t) (k 0)))) (h "18x3h3limdv5zq98yh6cxh9p7bxdr79zhd83k1dw725f56bcz2wj") (f (quote (("fmod"))))))

(define-public crate-geometrydash-0.2.0 (c (n "geometrydash") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_System_Memory"))) (d #t) (k 0)))) (h "1sl538v5i1ji1igrkvdys20qj9qfcrqhrb5y2mphj2bsnmj17ffm") (f (quote (("fmod"))))))

(define-public crate-geometrydash-0.2.1 (c (n "geometrydash") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_System_Memory"))) (d #t) (k 0)))) (h "1iaij1d3rwsa1317m97pbphbhl9fpjw2iqwdw17crrmsmk6a94iw") (f (quote (("fmod"))))))

(define-public crate-geometrydash-0.2.2 (c (n "geometrydash") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_Threading" "Win32_System_Diagnostics" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "1f5m8ng7x95rld2nq3jdzrxkcb7x0vdhpi27894kvj4f6s78vclg") (f (quote (("fmod"))))))

(define-public crate-geometrydash-0.2.3 (c (n "geometrydash") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_System" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_Threading" "Win32_System_Diagnostics" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "15sx07dh9pd6alp91pv3qagvf4vriry01h68hgzslkp3bg3kwrxc") (f (quote (("fmod"))))))

