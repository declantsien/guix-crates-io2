(define-module (crates-io ge om geometric) #:use-module (crates-io))

(define-public crate-geometric-0.1.0 (c (n "geometric") (v "0.1.0") (d (list (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1f95jrws4blf4j5iabmy3sb3mql89dnfg235824d1rg9jak7rw7s") (y #t)))

(define-public crate-geometric-0.2.0 (c (n "geometric") (v "0.2.0") (d (list (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "1fhh3qq71spdhql3c3jaggy3ncz0ysj41mbjkk54j88pgbqzxjn9") (y #t)))

(define-public crate-geometric-0.2.1 (c (n "geometric") (v "0.2.1") (d (list (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "02s42zf7a2ccn11hcil39q085gvdg3ly97vkd5bjz2xfkl9q7gh9")))

