(define-module (crates-io ge om geoms) #:use-module (crates-io))

(define-public crate-geoms-0.0.0 (c (n "geoms") (v "0.0.0") (h "1gmmfkxdc8hsjkn2qwhbqp6kg5qasdf7pyra483qnymrx7ald7xp")))

(define-public crate-geoms-0.0.1 (c (n "geoms") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "windows") (r "^0.42.0") (o #t) (d #t) (k 0)))) (h "0nyng82cc4m551r02nprnkhabqv1bgwpakvgfyc35knpqzzq675z") (f (quote (("win32" "windows/Win32_Foundation") ("nightly") ("default") ("d2d" "windows/Win32_Graphics_Direct2D_Common"))))))

