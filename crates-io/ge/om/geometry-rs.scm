(define-module (crates-io ge om geometry-rs) #:use-module (crates-io))

(define-public crate-geometry-rs-0.1.2 (c (n "geometry-rs") (v "0.1.2") (d (list (d (n "float_next_after") (r "^0.1.5") (d #t) (k 0)))) (h "1wf6v680hn1yp9vl4mzzfc5r0x89mqnf01fvk1q7acnlmq8wh8y6")))

(define-public crate-geometry-rs-0.2.0 (c (n "geometry-rs") (v "0.2.0") (d (list (d (n "float_next_after") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rtree_rs") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01l3innp2v13a5xk58f88jnny8v6bf7xwnc09zms2yazz68c98s3")))

(define-public crate-geometry-rs-0.2.1 (c (n "geometry-rs") (v "0.2.1") (d (list (d (n "float_next_after") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rtree_rs") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1xdjqyylx69j0pbwc77n1vzj738dx1vfh1dk2cdzbjjfsdp5vi8c")))

