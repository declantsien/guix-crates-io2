(define-module (crates-io ge nd gender_identification) #:use-module (crates-io))

(define-public crate-gender_identification-0.1.2 (c (n "gender_identification") (v "0.1.2") (h "1v7iiawjy0k5wbrxn82v717alk8k3s8p41nccrlv1g4vsw482xx2")))

(define-public crate-gender_identification-0.1.3 (c (n "gender_identification") (v "0.1.3") (h "11aldaisjg3w3s72ic1yh2i8pa6gkq64a9smy5rs8pj4mf45j929")))

(define-public crate-gender_identification-0.1.4 (c (n "gender_identification") (v "0.1.4") (h "13jzh5l1dwdr81ffsqjzn6yhidskprqcdx790sghs3p2gskz0xjl")))

(define-public crate-gender_identification-0.2.1 (c (n "gender_identification") (v "0.2.1") (h "0y3mh96sdv35byvp2d1ynqw36rirgbrnx5y7q2zp1r5v850mi8y1")))

(define-public crate-gender_identification-0.2.2 (c (n "gender_identification") (v "0.2.2") (h "1hr3q8cabwy1s47afsg4xq3fdq0kmgr44y2hj72kwcaw41hc69jn")))

