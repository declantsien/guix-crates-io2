(define-module (crates-io ge nd gender_guesser) #:use-module (crates-io))

(define-public crate-gender_guesser-0.1.0 (c (n "gender_guesser") (v "0.1.0") (h "1g1ykc37hlsjdjc4v26gbhp6gkm8bzk4qq3br7p5x5d7jmbfk56x") (y #t)))

(define-public crate-gender_guesser-0.1.1 (c (n "gender_guesser") (v "0.1.1") (h "1k3zmy9bcjmlrqbgybrnbfr9naz95a8ab7777g5k9jjnbqz305m7") (y #t)))

(define-public crate-gender_guesser-0.1.2 (c (n "gender_guesser") (v "0.1.2") (h "05wcczkv03yh3f45ww2rpfzf34yi9z8ik0507b2sl331imdhdlwm") (y #t)))

(define-public crate-gender_guesser-0.1.3 (c (n "gender_guesser") (v "0.1.3") (h "0ir0n3ahggylji13v8hyppirz2r83yf2a22qrqbqd663041rig9j") (y #t)))

(define-public crate-gender_guesser-0.1.4 (c (n "gender_guesser") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "1kd309vqvdig2iqdgr01pp1p8rcdymcgi7d8rr32mig3xv0brcpf")))

(define-public crate-gender_guesser-0.1.5 (c (n "gender_guesser") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "16v4l82z7l81vc55pqwa99p3kkvj6i7jm7al1jbqikn0p7cywn8w")))

(define-public crate-gender_guesser-0.1.6 (c (n "gender_guesser") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "1ia32701cmi0fw1m3fhm6a6br52lndxwjn80h1gx739bbjigmmf4")))

(define-public crate-gender_guesser-0.2.0 (c (n "gender_guesser") (v "0.2.0") (h "0gh3f0v0rfdipzcrv1r4rzbpawaqx3m469a6yi7z7xwqi8is94vv")))

