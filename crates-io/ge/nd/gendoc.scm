(define-module (crates-io ge nd gendoc) #:use-module (crates-io))

(define-public crate-gendoc-0.1.0 (c (n "gendoc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1fzn06bnh8qpk3l79vb1wxkdg6ghbjl7ll23rzbq9lhqb4lip9g9")))

(define-public crate-gendoc-0.2.0 (c (n "gendoc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1yf3r76j8bbbyca7n6lq3vhi69hlh8r46wsqp3q5n1z6h25myxpv")))

(define-public crate-gendoc-0.3.0 (c (n "gendoc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kbsm30xl7dhy1dxi3xi548jn57mn80xr9hiix79kb3pb40lyf8x")))

(define-public crate-gendoc-0.4.0 (c (n "gendoc") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vy8xfgwj89g7pz7qims3hxsq7ykvyyix8ak115mv9q13hs01syz")))

(define-public crate-gendoc-0.3.1 (c (n "gendoc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "12zzsc458midif43npynbg6xxcr51qz8bx5yyabgmjwd9qsncihs") (y #t)))

(define-public crate-gendoc-0.4.1 (c (n "gendoc") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0wl4y1mcb9ih45p3lpydglk0i18cfxnwlhdf0kv28r1i1n5ajf6s")))

