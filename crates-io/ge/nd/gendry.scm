(define-module (crates-io ge nd gendry) #:use-module (crates-io))

(define-public crate-gendry-0.1.0 (c (n "gendry") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tiberius") (r "^0.12") (f (quote ("async-std"))) (d #t) (k 0)))) (h "04ipqfgf2lfrszjdr7pd7b5dklyz55p2hn2b6wab8w34bv8ppsc2")))

(define-public crate-gendry-0.1.1 (c (n "gendry") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tiberius") (r "^0.12") (f (quote ("async-std"))) (d #t) (k 0)))) (h "02d1i3p2wc5mhak33aszxk0r74716xify4as90gqanigp6ajrrf3")))

(define-public crate-gendry-0.1.2 (c (n "gendry") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tiberius") (r "^0.12") (f (quote ("async-std"))) (d #t) (k 0)))) (h "16ivz3g26v1mf163h2spqd47yjh3cpl44zqwd94bjhrclfl42ai8")))

