(define-module (crates-io ge ob geobacter-amd-std) #:use-module (crates-io))

(define-public crate-geobacter-amd-std-1.0.0 (c (n "geobacter-amd-std") (v "1.0.0") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "hsa-rt") (r "^0.1.0") (d #t) (k 0)))) (h "1j52603f1y91zzk9q2zmf4mzrmd44bh2dfnfwc44ipfx4i6wvmc4")))

