(define-module (crates-io ge ob geobacter-vk-intrinsics) #:use-module (crates-io))

(define-public crate-geobacter-vk-intrinsics-1.0.0 (c (n "geobacter-vk-intrinsics") (v "1.0.0") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-intrinsics-common") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-rustc-help") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-vk-core") (r "^1.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)))) (h "06dd1fyhhd9v452zs41jnfn7bas2cawq02rswfxfi9l2jbrc5gy5")))

