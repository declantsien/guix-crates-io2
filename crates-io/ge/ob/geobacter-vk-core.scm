(define-module (crates-io ge ob geobacter-vk-core) #:use-module (crates-io))

(define-public crate-geobacter-vk-core-1.0.0 (c (n "geobacter-vk-core") (v "1.0.0") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-shared-defs") (r "^1.0.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.11.1") (d #t) (k 0)))) (h "0qd366gzzq3fsn3c6pdjw0zw64mkr3dgjxj9vzk1c8j9bqncicxw")))

(define-public crate-geobacter-vk-core-1.0.1 (c (n "geobacter-vk-core") (v "1.0.1") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-shared-defs") (r "^1.0.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)))) (h "0215jyiqqlmwd3blshpcq4l0gj9i8c8i5j5qvbbk1pw07h260gkn")))

