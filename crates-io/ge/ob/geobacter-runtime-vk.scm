(define-module (crates-io ge ob geobacter-runtime-vk) #:use-module (crates-io))

(define-public crate-geobacter-runtime-vk-0.1.0 (c (n "geobacter-runtime-vk") (v "0.1.0") (d (list (d (n "geobacter-intrinsics-common") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-runtime-core") (r "^1.0.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.16.0") (d #t) (k 0)))) (h "1shfm55i8x9k1q9kr39vr874a12qad0myr9kj2ky7xn8w2fhg5xs")))

