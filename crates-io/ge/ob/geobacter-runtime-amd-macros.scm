(define-module (crates-io ge ob geobacter-runtime-amd-macros) #:use-module (crates-io))

(define-public crate-geobacter-runtime-amd-macros-1.0.0 (c (n "geobacter-runtime-amd-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v669cqdxy999vw1fvwsl9ji5a4rda44w7hpp3d9n0zachbfg4ld")))

