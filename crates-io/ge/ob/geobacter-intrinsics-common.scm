(define-module (crates-io ge ob geobacter-intrinsics-common) #:use-module (crates-io))

(define-public crate-geobacter-intrinsics-common-1.0.0 (c (n "geobacter-intrinsics-common") (v "1.0.0") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-rustc-help") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "seahash") (r "^3.0.6") (d #t) (k 0)))) (h "1wnqh843r6lzw8xd2a8q35aryd41hj4mw3kd36zarm3wfazp1fxj")))

