(define-module (crates-io ge ob geobacter-amdgpu-intrinsics) #:use-module (crates-io))

(define-public crate-geobacter-amdgpu-intrinsics-1.0.0 (c (n "geobacter-amdgpu-intrinsics") (v "1.0.0") (d (list (d (n "geobacter-core") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-intrinsics-common") (r "^1.0.0") (d #t) (k 0)) (d (n "geobacter-rustc-help") (r "^1.0.0") (d #t) (k 0)))) (h "0dcwih0xs2w32v25a1w11h7mw3kfng8qxr7rcqpqdqlaql79xbi6")))

