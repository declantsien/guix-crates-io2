(define-module (crates-io ge nt gentrix) #:use-module (crates-io))

(define-public crate-gentrix-0.1.0 (c (n "gentrix") (v "0.1.0") (h "0mqj3a1gbk0cwnhwr33v1q4jiw1pxxcm7w3mrp3vy6qc64zz19gj")))

(define-public crate-gentrix-0.1.1 (c (n "gentrix") (v "0.1.1") (h "1syz69kyls6nkp1rb0kd3g595877lxq50781dlbv60sz8kc7nzs5")))

(define-public crate-gentrix-0.1.2 (c (n "gentrix") (v "0.1.2") (h "1c8a40zv666ajwrxi8868bn4kay3zdw3lmqxr56dr9zgj1263hvh")))

(define-public crate-gentrix-0.1.3 (c (n "gentrix") (v "0.1.3") (h "0m5xx1if0xid1yyrjgiad6bkxngsrvaw28pnwpb3bvnbj8zmgg60")))

(define-public crate-gentrix-0.1.4 (c (n "gentrix") (v "0.1.4") (h "113pqz4vssbly7rb6mzr7hl3md1diryfyarqmsarp10fyl4r54lc")))

