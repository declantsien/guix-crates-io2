(define-module (crates-io ge nt gentian) #:use-module (crates-io))

(define-public crate-gentian-0.1.0 (c (n "gentian") (v "0.1.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1dd27ij00wcdjrlha0ps76lkc55h5hxndhkdzq7cjbv4i95jq20h") (y #t)))

(define-public crate-gentian-0.1.1 (c (n "gentian") (v "0.1.1") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0w2jmd9xxgybmv6xbvshchj4yq9dd8pxy8mmi6h3s45ip5zly8jv")))

(define-public crate-gentian-0.1.2 (c (n "gentian") (v "0.1.2") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0pbx00di14q0b2kpqhf5sfwm6gzm376qsmcgfckrn7nbgm2cfmgj")))

(define-public crate-gentian-0.1.3 (c (n "gentian") (v "0.1.3") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1iklf7dbq69yjk6ml4gz8l6ly078a0wykmqjr9hrwi97i4wcwyy0")))

(define-public crate-gentian-0.1.4 (c (n "gentian") (v "0.1.4") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "04p3rs6i4gn0rlijqxpr2854yvkcmc77szk2lkiap5kvc0k180cp")))

(define-public crate-gentian-0.1.5 (c (n "gentian") (v "0.1.5") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0mi0da5z7nbaq5v3wfwnbg0akcpjvsrcrwxagis575kkynb1r64c")))

(define-public crate-gentian-0.1.6 (c (n "gentian") (v "0.1.6") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1igg8xvlxnzdlf57qlzba198xsd72jvc4grq0c6sgibylrnkvr6f")))

(define-public crate-gentian-0.1.7 (c (n "gentian") (v "0.1.7") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "053dj8cb7f6k8d4s881qyplhjj8jp1150az55q32hbbdirddkd7d")))

(define-public crate-gentian-0.1.8 (c (n "gentian") (v "0.1.8") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "066fvy4w6mfc1rdffxcicmryqnlqfdmpxxar4ka0b2fxmnsqia23") (f (quote (("default" "co_await") ("co_await"))))))

