(define-module (crates-io ge nt gentoo-cruft) #:use-module (crates-io))

(define-public crate-gentoo-cruft-1.0.0 (c (n "gentoo-cruft") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.10") (f (quote ("yaml"))) (k 0)) (d (n "fs-tree") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0hckx4ks2iz58imqyf41jj0ab0zhfkswrq8mrw7r06gl7nbrbr9v")))

(define-public crate-gentoo-cruft-1.0.1 (c (n "gentoo-cruft") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.10") (f (quote ("yaml"))) (k 0)) (d (n "fs-tree") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "1kcj899ni2rsiik8xj85akpr73krk76wd9mvbdq6sahly5a2d476")))

(define-public crate-gentoo-cruft-1.0.2 (c (n "gentoo-cruft") (v "1.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.10") (f (quote ("yaml"))) (k 0)) (d (n "fs-tree") (r "^0.1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "059dxljjxrrh04csp0rhb3r33iv829vs2a11xi0gjhld9k7ziz33")))

(define-public crate-gentoo-cruft-1.0.3 (c (n "gentoo-cruft") (v "1.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.10") (f (quote ("yaml"))) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0gpb8d8hvdfwdriyrrgcgfa2wivhl8nay75i7jihzc3va9iaw3kj")))

(define-public crate-gentoo-cruft-1.0.4 (c (n "gentoo-cruft") (v "1.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "config") (r "^0.10") (f (quote ("yaml"))) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0zifi2nkn4wsrdij7bs4g09r1p32l66nv1n2jqp1hpps0jgxqpix")))

(define-public crate-gentoo-cruft-1.0.5 (c (n "gentoo-cruft") (v "1.0.5") (d (list (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("yaml"))) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0xqy6w8i7kgknwlya2p9h9bjdrgw5x39ib861kmg16dkllpkk4a8")))

