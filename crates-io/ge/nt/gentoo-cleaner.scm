(define-module (crates-io ge nt gentoo-cleaner) #:use-module (crates-io))

(define-public crate-gentoo-cleaner-0.1.0 (c (n "gentoo-cleaner") (v "0.1.0") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("acct"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive" "strum_macros"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)))) (h "1cachvcwhip0yrljlhckvwmmg61g0vbm2830hp35rbaqxlfqxhdk")))

