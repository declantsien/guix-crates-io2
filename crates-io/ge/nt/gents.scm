(define-module (crates-io ge nt gents) #:use-module (crates-io))

(define-public crate-gents-0.1.0 (c (n "gents") (v "0.1.0") (d (list (d (n "gents_derives") (r "^0.1.0") (d #t) (k 0)))) (h "0y3h8pnah3p3418knrngnf4hv5in4fbbxziic4mga9af675gcl2f")))

(define-public crate-gents-0.1.1 (c (n "gents") (v "0.1.1") (d (list (d (n "gents_derives") (r "^0.1.0") (d #t) (k 0)))) (h "02hw8yizwcz347bsmasfd6bzlfs0p2j4r8jr1ayglqfvn340g5vv")))

(define-public crate-gents-0.1.2 (c (n "gents") (v "0.1.2") (d (list (d (n "gents_derives") (r "^0.1.0") (d #t) (k 0)))) (h "01r9cgkpywhyj6m3xk0ra67p8zsnp7wavgbbjng2wyd9y8i74h8s")))

(define-public crate-gents-0.2.0 (c (n "gents") (v "0.2.0") (d (list (d (n "gents_derives") (r "^0.1.0") (d #t) (k 0)))) (h "09qi4p4xzcsx24f1bhxvbyhna2pf2fi41ifvsb1z9ciqgjy1vxzl")))

(define-public crate-gents-0.3.0 (c (n "gents") (v "0.3.0") (d (list (d (n "gents_derives") (r "^0.3.0") (d #t) (k 2)))) (h "05cm0ayn4621xhk88l5hmr9ws0dgrjlxgsnns2qyzp63qwmvaslw")))

(define-public crate-gents-0.3.1 (c (n "gents") (v "0.3.1") (d (list (d (n "gents_derives") (r "^0.3.1") (d #t) (k 2)))) (h "1yh6bx75yi9p87llg42b8fq6zdzrh1khfv4i4pwcn3drmhgi940g")))

(define-public crate-gents-0.3.2 (c (n "gents") (v "0.3.2") (d (list (d (n "gents_derives") (r "^0.3.2") (d #t) (k 2)))) (h "1q2x14grbkjg4xivcgpjjd04sxqkaj6mngd7bhz1x7hl7n1bziyi")))

(define-public crate-gents-0.4.0 (c (n "gents") (v "0.4.0") (d (list (d (n "gents_derives") (r "^0.4.0") (d #t) (k 2)))) (h "15hrrxg7yzsgdwj74k8k7ljzz7k8bmvkv6jiw4ssbmwp7yw4j4lf")))

(define-public crate-gents-0.4.1 (c (n "gents") (v "0.4.1") (d (list (d (n "gents_derives") (r "^0.4.0") (d #t) (k 2)))) (h "0gq68k520rsjzgis2x7agm4dkp0qdcrkkc5zpl3yxc55adkb6jb1")))

(define-public crate-gents-0.5.0 (c (n "gents") (v "0.5.0") (d (list (d (n "gents_derives") (r "^0.5.0") (d #t) (k 2)))) (h "0i7n9jhwagrv7d1b33rk1l10ayrfq16sy68fkzwbdzvjwj0pm5l3")))

(define-public crate-gents-0.6.0 (c (n "gents") (v "0.6.0") (d (list (d (n "gents_derives") (r "^0.6.0") (d #t) (k 2)))) (h "040mhgdm6422kfli5zr9bwggjbgfpv763drrgsni6c86zjghb9yn")))

(define-public crate-gents-0.7.0 (c (n "gents") (v "0.7.0") (d (list (d (n "gents_derives") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0613ixp5kijs8rw5924zcmfp9hmk62n80kk5mgzbjmn0q3cxnch2")))

(define-public crate-gents-0.8.0 (c (n "gents") (v "0.8.0") (d (list (d (n "gents_derives") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0n7cqmflh094g32mz7cz180r6bh77jaiki9kx49p3cmlnvwz5czp")))

