(define-module (crates-io ge oh geohash-tools) #:use-module (crates-io))

(define-public crate-geohash-tools-0.0.0 (c (n "geohash-tools") (v "0.0.0") (d (list (d (n "phf") (r "^0.11.1") (d #t) (k 0)))) (h "06ykak3nplkxkfphyxadzdh8zhkdqb653h24k8yxbwijpzzc613r")))

(define-public crate-geohash-tools-0.1.0 (c (n "geohash-tools") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "05v66n0q6v7spqvzydkb94nmbp005mgvrb09gj3y4apmhdm9563b")))

(define-public crate-geohash-tools-0.1.1 (c (n "geohash-tools") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nyi6854dhfhfwdnws3fpbw32cmx78iahwm2n7zsbq9yz2hcng9f")))

(define-public crate-geohash-tools-0.1.2 (c (n "geohash-tools") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "14b6mjsvqjx2y5pbicdivn9f2nxs8kkkgl60f8bvi4i6yzp458wa")))

(define-public crate-geohash-tools-0.1.3 (c (n "geohash-tools") (v "0.1.3") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1wg5k1qv5yxiwh7xhlsfyx0n41iyw79iglj1pzdddghq8lzjnn14")))

