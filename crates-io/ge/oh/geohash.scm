(define-module (crates-io ge oh geohash) #:use-module (crates-io))

(define-public crate-geohash-0.1.0 (c (n "geohash") (v "0.1.0") (h "10cigl33vmrjd10swl9z0xknp0xgmfq3022j050l4lx2mbif9l26")))

(define-public crate-geohash-0.1.1 (c (n "geohash") (v "0.1.1") (h "0zirmlf8x0jz24dwsdwxfgxfyhqvpnj30a3jps2kpzr5j3yasrzr")))

(define-public crate-geohash-0.1.2 (c (n "geohash") (v "0.1.2") (h "0lc0lsbnwib4vay9mv2hg4bqb0kx5vwmkxagzsb6m65j2sc5ynp2")))

(define-public crate-geohash-0.2.0 (c (n "geohash") (v "0.2.0") (d (list (d (n "geo") (r "*") (d #t) (k 0)))) (h "1lkwgya8g67v1177mfzmpf3mlq82fzv9ws8lcdnl90n5pp6wis3s")))

(define-public crate-geohash-0.2.1 (c (n "geohash") (v "0.2.1") (d (list (d (n "geo") (r "*") (d #t) (k 0)))) (h "0r45zjjhjrjwjl8vs97j2iqawz8ix03aw769q3481gmrgjm3n3yx")))

(define-public crate-geohash-0.2.2 (c (n "geohash") (v "0.2.2") (d (list (d (n "geo") (r "*") (d #t) (k 0)))) (h "1ci0fwjddig4hfl9cspvj32r02p9grfk9idn31fp8l3xprdd9qgb")))

(define-public crate-geohash-0.2.3 (c (n "geohash") (v "0.2.3") (d (list (d (n "geo") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "1vjnizy2fjv3mwy8j6fkg9zbfbmd46rywib89gnv3vi6cpdrd1ja")))

(define-public crate-geohash-0.2.4 (c (n "geohash") (v "0.2.4") (d (list (d (n "geo") (r "^0.0.5") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "0yprda3kjipbp7isbflqcwwmc763n50i9dzaax21dhk1bf15jzaf")))

(define-public crate-geohash-0.2.5 (c (n "geohash") (v "0.2.5") (d (list (d (n "geo") (r "^0.0.5") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 2)))) (h "1my0lvl81fxiq8lhplxagx0f91kqglrz0frq22w8mvgrf9dx09lj")))

(define-public crate-geohash-0.2.6 (c (n "geohash") (v "0.2.6") (d (list (d (n "geo") (r "^0.0.5") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "1w21w4sm1ndy027n87j3jsm18l5gqb1p2indmngl28rrzdglpz2m")))

(define-public crate-geohash-0.2.7 (c (n "geohash") (v "0.2.7") (d (list (d (n "geo") (r "~0.0.5") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "1cw633lmxrpc9kvfd5q3cfvhmdvjhcs9md709r5rwi41c52a5x1i")))

(define-public crate-geohash-0.3.0 (c (n "geohash") (v "0.3.0") (d (list (d (n "geo") (r "~0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "1j5cik0bfy76d94mzr4b3kz3p3p56qq0gkhjcq3jfj5n2s2cjmk5")))

(define-public crate-geohash-0.3.1 (c (n "geohash") (v "0.3.1") (d (list (d (n "geo") (r "~0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "0pg4njw67x4zzvzvbdc4kidsh2hz54l6if3b7n9hs4swn7l7bssk")))

(define-public crate-geohash-0.4.0 (c (n "geohash") (v "0.4.0") (d (list (d (n "geo") (r "~0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "1rjl9qc6vzz7fy2aswj158wj004nzqpl2prrh5dpvi32an1ybbyp")))

(define-public crate-geohash-0.5.0 (c (n "geohash") (v "0.5.0") (d (list (d (n "geo") (r "~0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 2)))) (h "0ahywwr3l8by9vhfkxdkfnwkj87zxxbvwfw3bhly537vs7k71r1f")))

(define-public crate-geohash-0.6.0 (c (n "geohash") (v "0.6.0") (d (list (d (n "geo") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1dp6nzrplx03d38d7xc94ki073b582ifvl5rnx5n83al1s3s5i2l")))

(define-public crate-geohash-0.7.0 (c (n "geohash") (v "0.7.0") (d (list (d (n "geo-types") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1xcwrjgsda4aa56vf44ff1v7hsx5b51pi4rfkdg92995623469iw")))

(define-public crate-geohash-0.7.1 (c (n "geohash") (v "0.7.1") (d (list (d (n "geo-types") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "0grf00qhsmjcas7ly1pflnj9igyqsdamcx39m4bdrqp9s4yh9rlh")))

(define-public crate-geohash-0.8.0 (c (n "geohash") (v "0.8.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "0pa6m8n2fbgl79vb6w0cng827nihp80djzszi28gl12pyrlmm13r")))

(define-public crate-geohash-0.9.0 (c (n "geohash") (v "0.9.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "11mjfjyq993fa1hb71jv0rcp18pcgb9va2lzkjs5i3nhvcb77jd4")))

(define-public crate-geohash-0.10.0 (c (n "geohash") (v "0.10.0") (d (list (d (n "geo-types") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1kx0cfw4n3w3n2wfla4i0q6j40mb85ihrfs3n5g1hiv9a5kvfsg8")))

(define-public crate-geohash-0.11.0 (c (n "geohash") (v "0.11.0") (d (list (d (n "geo-types") (r ">=0.6.0, <0.8.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "19qg0nc11fnvvm8prgj089l19yl0vmqigjh8bk8c47kd61pf0w66")))

(define-public crate-geohash-0.12.0 (c (n "geohash") (v "0.12.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "geo-types") (r ">=0.6.0, <0.8.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0kq0iacnpl7bl2vl02grki82f5fwb6jjsrzjs4pn1kys5s7gnc95")))

(define-public crate-geohash-0.13.0 (c (n "geohash") (v "0.13.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "geo-types") (r ">=0.6.0, <0.8.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0il53q682g9c89h52sw52wsil0i9qjf1g57xrk4z391zn44vh64a")))

(define-public crate-geohash-0.13.1 (c (n "geohash") (v "0.13.1") (d (list (d (n "csv") (r "^1.2") (d #t) (k 2)) (d (n "geo-types") (r ">=0.6.0, <0.8.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1rmibl9jrc9qwnz43y3lcv114j1nm90912lm4aznq7a0cld4pf8g")))

