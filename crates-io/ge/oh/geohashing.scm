(define-module (crates-io ge oh geohashing) #:use-module (crates-io))

(define-public crate-geohashing-0.1.0 (c (n "geohashing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (o #t) (d #t) (k 0)))) (h "108mxhl7paxsp10iap8vwwmjphr2wiyif8pa4g2bsly2f9rbfg11") (f (quote (("online" "reqwest" "regex") ("offline") ("default" "offline"))))))

