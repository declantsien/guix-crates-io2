(define-module (crates-io ge oh geohashrust) #:use-module (crates-io))

(define-public crate-geohashrust-0.0.1 (c (n "geohashrust") (v "0.0.1") (h "1pjk804l7bcyd7p035jl7pl2wpvb45liddn67sw7d4vcrx7aribf")))

(define-public crate-geohashrust-0.0.2 (c (n "geohashrust") (v "0.0.2") (h "1b4k6av5kmlg7v18xpc4hm3zkyaxwbfg049crgww2hnnyaiwldwf")))

