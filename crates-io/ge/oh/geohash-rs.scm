(define-module (crates-io ge oh geohash-rs) #:use-module (crates-io))

(define-public crate-geohash-rs-0.1.0 (c (n "geohash-rs") (v "0.1.0") (h "17gi2hylqfbkjq2zxj79mrla6mcl9k8ivfr76bba6xcp25lxjj89")))

(define-public crate-geohash-rs-0.1.1 (c (n "geohash-rs") (v "0.1.1") (h "182qbi0hqhj9kg6hqd029jqk49bff719dn08qi0y92hd1w895pws")))

(define-public crate-geohash-rs-0.1.2 (c (n "geohash-rs") (v "0.1.2") (h "1awm7nmd2xhqq174r4xvg98mdbqkv0nhmkfwgxmzm0h95bdvf308")))

(define-public crate-geohash-rs-0.1.3 (c (n "geohash-rs") (v "0.1.3") (h "164cb19pn7arps9s00c9b395wma627a95pcs2piwwn9ny0m0qxc6")))

