(define-module (crates-io ge ta getaddrs) #:use-module (crates-io))

(define-public crate-getaddrs-0.1.0 (c (n "getaddrs") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bf9l3x9mr6r74lbsayw4nwmrmv6v6gvbqmasffn78a5zi21vfpg")))

