(define-module (crates-io ge ta getargs-os) #:use-module (crates-io))

(define-public crate-getargs-os-0.1.0 (c (n "getargs-os") (v "0.1.0") (d (list (d (n "argv") (r "~0.1.6") (d #t) (k 2)) (d (n "getargs") (r "~0.5.0") (d #t) (k 0)))) (h "10lhpvjb103d6qigf3d6zdjbrrdv4yk584rk68rbk219gd0hkxay") (r "1.56")))

(define-public crate-getargs-os-0.1.1 (c (n "getargs-os") (v "0.1.1") (d (list (d (n "argv") (r "~0.1.6") (d #t) (k 2)) (d (n "getargs") (r "~0.5.0") (d #t) (k 0)))) (h "0qnxb52migxdd2qm7c4knlzkrpzlhcm3x4rfkwmpikvk2a9mfl2g") (f (quote (("os_str_bytes")))) (r "1.56")))

