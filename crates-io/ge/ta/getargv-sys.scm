(define-module (crates-io ge ta getargv-sys) #:use-module (crates-io))

(define-public crate-getargv-sys-0.1.0 (c (n "getargv-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "14gyhhnqqkf0sjl21p1x83rcishah8jy9x8gafgml2zg4nzmxb8z") (l "getargv")))

(define-public crate-getargv-sys-0.2.0 (c (n "getargv-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "0vm1xcjfa84q3mcpvhwk1n5dzwxxl9icb6pln0c7ngz0ikhp0av4") (l "getargv")))

(define-public crate-getargv-sys-0.3.0 (c (n "getargv-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "11368sdsysppa2flvfffsjcljqym390441lk6s5w49wf4a076z7z") (l "getargv")))

(define-public crate-getargv-sys-0.4.0 (c (n "getargv-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "~0.63.0") (d #t) (k 1)))) (h "1w90l1nkj16rp1ibc7fag0vphjqgwrddr49pjrmivhilvf9943pl") (l "getargv")))

(define-public crate-getargv-sys-0.4.1 (c (n "getargv-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "~0.63.0") (d #t) (k 1)))) (h "0q5m5vjldri7zgmiblyc7i62vrjjmvnmg07jlf1kxjgd514mvg9i") (l "getargv")))

(define-public crate-getargv-sys-0.5.0 (c (n "getargv-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "~0.63.0") (d #t) (k 1)) (d (n "goblin") (r "~0.6.1") (f (quote ("mach64" "std"))) (d #t) (k 1)))) (h "1z2w6br8xfb9xi5zcswlllc0qipvj37y652pwapd110lnrz21as2") (l "getargv")))

(define-public crate-getargv-sys-0.5.1 (c (n "getargv-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "~0.63.0") (d #t) (k 1)) (d (n "goblin") (r "~0.6.1") (f (quote ("mach64" "std"))) (d #t) (k 1)))) (h "19fi87xzxhlg7m9i9fdaxd8y1chvbhwn4gbg9k5gnskn8v5jp3w5") (l "getargv")))

(define-public crate-getargv-sys-0.5.2 (c (n "getargv-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "~0.69.1") (d #t) (k 1)) (d (n "goblin") (r "~0.7.1") (f (quote ("mach64" "std"))) (d #t) (k 1)))) (h "09ib650b0vj1ssmlkg7l0dfxzp6wc0alww5k3psc5q61miz6n9ql") (l "getargv")))

(define-public crate-getargv-sys-0.5.3 (c (n "getargv-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "~0.69.4") (d #t) (k 1)) (d (n "goblin") (r "~0.8.0") (f (quote ("mach64" "std"))) (d #t) (k 1)))) (h "0y5byswb222l8kw4cx7ina8b22gsv305rvgn6x0kcpadf7bcad2p") (l "getargv")))

