(define-module (crates-io ge ta getargs) #:use-module (crates-io))

(define-public crate-getargs-0.1.0 (c (n "getargs") (v "0.1.0") (h "1366vljimn52g8zrk3xprj0dybiz92jdmcs0y5idsr55h71qd7wv") (y #t)))

(define-public crate-getargs-0.2.0 (c (n "getargs") (v "0.2.0") (h "0mh2zz22y9ivxcjnvzrdj1v0il3mzqyz1gl492x2zjagb2ga647z")))

(define-public crate-getargs-0.3.0 (c (n "getargs") (v "0.3.0") (h "1y32a9l77h0ma135phsl6bnplaimm5ql0iiilphs10vpzn1aphsc")))

(define-public crate-getargs-0.4.0 (c (n "getargs") (v "0.4.0") (h "0v33pmc8pwkvb67vhbln057f20ziyijrw20mzr5x0cl2nwijrgv2")))

(define-public crate-getargs-0.4.1 (c (n "getargs") (v "0.4.1") (h "1wacn0n596z2l78n7bsj2y600h8v5wb41fgaya3zvnjnrjnwmylw")))

(define-public crate-getargs-0.5.0 (c (n "getargs") (v "0.5.0") (d (list (d (n "argv") (r "~0.1.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 2)))) (h "0vp6f2h82i1a7268fkq75gcv1aari36bk1i9y57wbph15cc6nk08") (f (quote (("std") ("default" "std"))))))

