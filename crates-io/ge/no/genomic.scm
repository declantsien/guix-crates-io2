(define-module (crates-io ge no genomic) #:use-module (crates-io))

(define-public crate-genomic-0.1.0 (c (n "genomic") (v "0.1.0") (d (list (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1p5c6w2v5a36j24crqii58nwkv87f2av9y96fd3n8qm23iyl55zl")))

(define-public crate-genomic-0.2.0 (c (n "genomic") (v "0.2.0") (d (list (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "16hkjf5m9z1g3pfys62jyj519ki2h9q94n8fcm3amndc4yb73wya")))

