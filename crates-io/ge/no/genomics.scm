(define-module (crates-io ge no genomics) #:use-module (crates-io))

(define-public crate-genomics-0.1.0 (c (n "genomics") (v "0.1.0") (h "1qd0knl3g6iilply32s5k8qvnspl5z0ll4zcq94rm00lrgqjvfl6") (y #t)))

(define-public crate-genomics-0.1.1 (c (n "genomics") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)))) (h "15bvxr7wfhm8g9s7xbkyddcglg2bmqm49m2qh6y73fsw94jwgpb2") (y #t)))

