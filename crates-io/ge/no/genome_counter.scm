(define-module (crates-io ge no genome_counter) #:use-module (crates-io))

(define-public crate-genome_counter-0.2.0 (c (n "genome_counter") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "packed_simd") (r "^0.3.5") (d #t) (k 0) (p "packed_simd_2")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0lxlyb7lfjn9n0c3a4i7ykc7vdwdm5lm3m8h7sv9844kllyg2k3h")))

