(define-module (crates-io ge no genotype) #:use-module (crates-io))

(define-public crate-genotype-0.1.0 (c (n "genotype") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pq3fmfb4k3snxarifc1pj824lgmidqqsg7kzp0dfnk6c4jjgwgw") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-genotype-0.1.1 (c (n "genotype") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0xaghklv40hsysk2yi6mc7k3ad80ac0cjxm4vk0wb5hawf5vgcs0") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-genotype-0.1.2 (c (n "genotype") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mikk3bxn3x379sg2yk9fhfc4f6j0z1s3gv18fwsr1jznn6g1crr") (f (quote (("serialize" "serde" "serde_derive"))))))

