(define-module (crates-io ge no genome) #:use-module (crates-io))

(define-public crate-genome-0.1.0 (c (n "genome") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "0gjxq459agicmf5pd03263dzimb2x8cpzvxc6mrrsbsd7d19l9wb")))

(define-public crate-genome-0.2.0 (c (n "genome") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "107in8g6wkwx1vip4a9h1cnmnb0dby55h0q2iwzsbf0dbzd2f8dk")))

(define-public crate-genome-0.2.1 (c (n "genome") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "193jnysz1hd7w86an0zvysdh3vkscpkjzicmwdy48crz91a54ac0")))

(define-public crate-genome-0.3.0 (c (n "genome") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1069g3bzvwagfrqvh06zb47cn50dzxabya89lqypqcqifaz7x61m")))

