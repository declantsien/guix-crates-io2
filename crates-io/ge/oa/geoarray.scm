(define-module (crates-io ge oa geoarray) #:use-module (crates-io))

(define-public crate-geoarray-0.0.0 (c (n "geoarray") (v "0.0.0") (h "05b5mbmhqbr70mn6r9zasidd15z84c8j6rbywq80x47wq2vbdsjs") (y #t)))

(define-public crate-geoarray-0.0.1 (c (n "geoarray") (v "0.0.1") (h "1hbqhp7b87x6lj0rpnjaa22brjcp514i12ymjg46hl3nwfr4ylk9") (y #t)))

(define-public crate-geoarray-0.0.2 (c (n "geoarray") (v "0.0.2") (h "09342d8b3013l2qfrnjmxpw3g681lanq2zq2bm3hmssqf2xfad7x")))

