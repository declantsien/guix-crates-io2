(define-module (crates-io ge nz genztools) #:use-module (crates-io))

(define-public crate-genztools-0.1.0 (c (n "genztools") (v "0.1.0") (h "1iw9z2aiip53fay7bm13pjsb9n8snhrsdcqhvg5nvp10326a7ig3")))

(define-public crate-genztools-0.1.1 (c (n "genztools") (v "0.1.1") (h "0d57m7j7n6llll85qv2zi8vq1gykd7jlhqnq42d525fh8kcmschg")))

(define-public crate-genztools-0.1.2 (c (n "genztools") (v "0.1.2") (h "1m12wpijvkqbdn1szpsg8f9xd4ib44cf51pn921ksmnskbbfi552")))

(define-public crate-genztools-0.1.3 (c (n "genztools") (v "0.1.3") (h "10xk5zgs68jykb0wyxpnqz8412br5vi84x2mwwb8ybjrdw6jka9m")))

(define-public crate-genztools-0.1.4 (c (n "genztools") (v "0.1.4") (h "1alqr2151vd7v4vyjw8qwzj348dlgy498iznvhf6ypgwqbhc201w")))

(define-public crate-genztools-0.1.5 (c (n "genztools") (v "0.1.5") (h "1wnrdsgr8f9c1db56lyxwcbjxyhg8byixwa6axna6rx4chslk69c")))

(define-public crate-genztools-0.1.6 (c (n "genztools") (v "0.1.6") (h "122cj8jq04i6jbv9f9nwlyxvssz66c7hk0ckf2l1dqllimy1nana")))

