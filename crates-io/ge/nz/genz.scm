(define-module (crates-io ge nz genz) #:use-module (crates-io))

(define-public crate-genz-0.1.0 (c (n "genz") (v "0.1.0") (h "15kljqznfmlfyxvvclxy0458ksf963pn3inpg9cv64p3mcklpic6")))

(define-public crate-genz-0.2.0 (c (n "genz") (v "0.2.0") (h "0y4f81zvvxqp3qh42djswjb0s4kqc5dkkr3m666rx1fa8ri513pq") (y #t)))

(define-public crate-genz-0.3.0 (c (n "genz") (v "0.3.0") (h "0hgibqyn0qcdqwbx6axl463a1v58wkwqga3imyaqmsfv7bikkxla")))

(define-public crate-genz-0.4.0 (c (n "genz") (v "0.4.0") (h "1xkpl5b1wrblv2f1ig2w4nrhqrvnv9r2c5xyrf1dyhrv24bqgxnx")))

