(define-module (crates-io ge op geoplaces) #:use-module (crates-io))

(define-public crate-geoplaces-1.0.0 (c (n "geoplaces") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1kjf0qsg30gaqpbyqs07z5bw7skwbkms8sdm3ak704qpai1p7gb8")))

