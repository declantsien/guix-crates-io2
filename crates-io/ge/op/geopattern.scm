(define-module (crates-io ge op geopattern) #:use-module (crates-io))

(define-public crate-geopattern-0.1.0 (c (n "geopattern") (v "0.1.0") (d (list (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.10") (d #t) (k 0)))) (h "1a3xy58sa1xcajj43vwaiwbjr0df2bbsbp34n7hvnmxgmbvjnxzw")))

(define-public crate-geopattern-0.2.0 (c (n "geopattern") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.10") (d #t) (k 0)))) (h "0ns30wg1fzvid6l4sxak4r3088wg7k6d4bc2yx52fqf5qhzibzbz")))

(define-public crate-geopattern-0.3.0 (c (n "geopattern") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1phav93p0p2g8j0qfcysm5d30fk8p29d4cg2532zr82414r0i0v4")))

(define-public crate-geopattern-0.4.0 (c (n "geopattern") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "0gxhaqmv41hny3acikdfbs03lc15dr683458fcm80xc6i8crs0zw")))

(define-public crate-geopattern-0.5.0 (c (n "geopattern") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "sha-1") (r "^0.9.1") (d #t) (k 2)) (d (n "svg") (r "^0.8.0") (d #t) (k 0)))) (h "022mj2gpkmsdjl17r3hwrq9qhb8d3f301r61xgygdlc2ix2x8h8l")))

