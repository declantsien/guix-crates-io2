(define-module (crates-io ge la gelato-sdk) #:use-module (crates-io))

(define-public crate-gelato-sdk-0.1.0-alpha (c (n "gelato-sdk") (v "0.1.0-alpha") (d (list (d (n "ethers-core") (r "^0.6.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (k 0)) (d (n "serde_repr") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1qz3pbl94623pzcm1mycy9yzb94jngrcwd719amhfi066a07q3ci")))

