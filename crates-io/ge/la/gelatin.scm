(define-module (crates-io ge la gelatin) #:use-module (crates-io))

(define-public crate-gelatin-0.1.0 (c (n "gelatin") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1f20hbv2j7d287x3paq21q6kcsmqwqlkbjcw3k83cx09knqaqx23")))

(define-public crate-gelatin-0.2.0 (c (n "gelatin") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1fm5w2qlr6x82k1y8rwxzw4i243q3ifh528iabkg4amfywjmf2fk")))

(define-public crate-gelatin-0.3.0 (c (n "gelatin") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "~0.23.4") (d #t) (k 0)))) (h "0x4a3vqfxi9mvanx2mqkny42cl0khqhb772cp0hx514ma968mlff")))

(define-public crate-gelatin-0.4.0 (c (n "gelatin") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "winit") (r "=0.22.0") (d #t) (k 0)))) (h "1mk0b0szrdi71hh6lzsbazrjjyngid09513wqvs71bd43k4kvw0s")))

(define-public crate-gelatin-0.5.0 (c (n "gelatin") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "winit") (r "=0.22.0") (d #t) (k 0)))) (h "05k44153whij03849klx6jvcklrfirx4msnd89213v2g5zbipd8s")))

(define-public crate-gelatin-0.6.0 (c (n "gelatin") (v "0.6.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "glium") (r "^0.27") (d #t) (k 0)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "winit") (r "=0.22.0") (d #t) (k 0)))) (h "0c7mw3qmpa1fycnv81wl6rqxbqia7vnqxxpqqsii9q3x1l446yz5")))

(define-public crate-gelatin-0.7.0 (c (n "gelatin") (v "0.7.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "0r3ws24mz6zqfvm1wdwr3mc8pjnwax1pmh8dp8v3kz5xj2gbkrwq")))

(define-public crate-gelatin-0.8.0 (c (n "gelatin") (v "0.8.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "17iv8qfysknjpgkpnhsfw5pa9yn07p152f3jsxisl0cdm0ydlyl5")))

(define-public crate-gelatin-0.9.0 (c (n "gelatin") (v "0.9.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.0") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "1cl40dy0jar2brad93zkiyy0w1fysy6b5psqy9bg1x8k3prnmil7")))

