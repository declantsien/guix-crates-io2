(define-module (crates-io ge re gerencianet-api) #:use-module (crates-io))

(define-public crate-gerencianet-api-0.1.0 (c (n "gerencianet-api") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gwlpqnxv7k8jv1sm11c1ivy3kwx2l0gmp2ajk0w2bjpc9p0a143")))

