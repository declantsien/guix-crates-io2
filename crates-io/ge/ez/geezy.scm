(define-module (crates-io ge ez geezy) #:use-module (crates-io))

(define-public crate-geezy-0.1.0 (c (n "geezy") (v "0.1.0") (h "1d9ixq1hxn40pppgxcaamdcy3rjph6mpa3szg32wnrjpmmh54fim")))

(define-public crate-geezy-0.1.1 (c (n "geezy") (v "0.1.1") (h "1agdbd8acvwj7wc7fs4n3h9xrpbi1qjm8xnya9pn014rarzl89iq")))

