(define-module (crates-io ge nl genlib) #:use-module (crates-io))

(define-public crate-genlib-0.1.0 (c (n "genlib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "0zl52i9wlimha7qlba587vs6gk5qkg3bj4wacc5h83l638pm2d0p") (y #t)))

