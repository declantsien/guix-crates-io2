(define-module (crates-io ge na genawaiter-macro) #:use-module (crates-io))

(define-public crate-genawaiter-macro-0.99.0 (c (n "genawaiter-macro") (v "0.99.0") (h "0xvvpvz912haydwc3rbm78zvdj723b8nm323av5zdkjg9z559zrq") (f (quote (("strict") ("proc_macro"))))))

(define-public crate-genawaiter-macro-0.99.1 (c (n "genawaiter-macro") (v "0.99.1") (h "1g6zmr88fk48f1ksz9ik1i2mwjsiam9s4p9aybhvs2zwzphxychb") (f (quote (("strict") ("proc_macro"))))))

