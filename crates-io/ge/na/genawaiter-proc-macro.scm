(define-module (crates-io ge na genawaiter-proc-macro) #:use-module (crates-io))

(define-public crate-genawaiter-proc-macro-0.99.0 (c (n "genawaiter-proc-macro") (v "0.99.0") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "04r811w3ir69wmfrdsy10x4qmv99drlgdnda7nwg0zb60i958s8n") (f (quote (("strict"))))))

(define-public crate-genawaiter-proc-macro-0.99.1 (c (n "genawaiter-proc-macro") (v "0.99.1") (d (list (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit-mut" "full"))) (d #t) (k 0)))) (h "0f0pcaln4wrpi35nwxs9g516ysiax373m32a3hjiavinpkp88kvq") (f (quote (("strict"))))))

