(define-module (crates-io ge na genawaiter_iterator) #:use-module (crates-io))

(define-public crate-genawaiter_iterator-0.1.0 (c (n "genawaiter_iterator") (v "0.1.0") (d (list (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)))) (h "1pmdbkyh94yxfx3nfks9b177bblj5wnrxa13hicgs0b8wqhg8m0i")))

(define-public crate-genawaiter_iterator-0.1.1 (c (n "genawaiter_iterator") (v "0.1.1") (d (list (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)))) (h "0gfqkfm6sawl69z8jcbs5d23v617lj6afwmz1lnmzkk9caxhn72m")))

(define-public crate-genawaiter_iterator-0.1.2 (c (n "genawaiter_iterator") (v "0.1.2") (d (list (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)))) (h "0kznvhda9mrfxfb58r742qhsc7njc4n46jxv7zx5p2azpvd8sb7g")))

(define-public crate-genawaiter_iterator-0.1.3 (c (n "genawaiter_iterator") (v "0.1.3") (d (list (d (n "genawaiter") (r "^0.99.1") (d #t) (k 0)))) (h "15m00ks671mcgcybvlfpzz3vd44mpklk0a2bqzz38kd1w2w5jk83")))

