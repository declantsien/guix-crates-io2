(define-module (crates-io ge na genawaiter) #:use-module (crates-io))

(define-public crate-genawaiter-0.1.0 (c (n "genawaiter") (v "0.1.0") (h "16yf0ap85p3rlki6yhs9v3gdsa58w7z1sni69q8vr008z4mxfw6n") (f (quote (("strict") ("nightly"))))))

(define-public crate-genawaiter-0.1.1 (c (n "genawaiter") (v "0.1.1") (h "1c5mnyp6c49zqi3gvlcd6pslq0l145llwf1byzpzy1l8l0ha879l") (f (quote (("strict") ("nightly"))))))

(define-public crate-genawaiter-0.2.0 (c (n "genawaiter") (v "0.2.0") (h "1vlmznib1g07cs8cyb5fxlg0v0psp0484s794847fq5p0b6pwhlh") (f (quote (("strict") ("nightly"))))))

(define-public crate-genawaiter-0.2.1 (c (n "genawaiter") (v "0.2.1") (h "0dwr6ac7s8sn8ydr0q2ybmwmfprkirhy5wjk6ny4q207lch4348h") (f (quote (("strict") ("nightly"))))))

(define-public crate-genawaiter-0.2.2 (c (n "genawaiter") (v "0.2.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0cidw5wgwgc17xaalf7hi3kiqd7c1x7745p3s7hzkfhjx2f2adhj") (f (quote (("strict") ("nightly") ("futures03" "futures-core"))))))

(define-public crate-genawaiter-0.99.0 (c (n "genawaiter") (v "0.99.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "genawaiter-macro") (r "^0.99.0") (d #t) (k 0)) (d (n "genawaiter-proc-macro") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1p45k1mch0fyd3pxnxdpnm04kfxvjwbg6pw00ss4m8wvg1nf29ar") (f (quote (("strict") ("proc_macro" "genawaiter-proc-macro" "proc-macro-hack" "genawaiter-macro/proc_macro") ("nightly") ("futures03" "futures-core") ("default" "proc_macro"))))))

(define-public crate-genawaiter-0.99.1 (c (n "genawaiter") (v "0.99.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "genawaiter-macro") (r "^0.99.1") (d #t) (k 0)) (d (n "genawaiter-proc-macro") (r "^0.99.1") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1861a6vy9lc9a8lbw496m9j9jcjcn9nf7rkm6jqkkpnb3cvd0sy8") (f (quote (("strict") ("proc_macro" "genawaiter-proc-macro" "proc-macro-hack" "genawaiter-macro/proc_macro") ("nightly") ("futures03" "futures-core") ("default" "proc_macro"))))))

