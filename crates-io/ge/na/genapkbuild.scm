(define-module (crates-io ge na genapkbuild) #:use-module (crates-io))

(define-public crate-genapkbuild-1.4.0 (c (n "genapkbuild") (v "1.4.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5.10") (d #t) (k 0)))) (h "0724i0fczq4v51n8c7l7zkqn6nknhjx992wl3nwcs3bcq6zy0fxl")))

(define-public crate-genapkbuild-1.5.0 (c (n "genapkbuild") (v "1.5.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.5.10") (d #t) (k 0)))) (h "13msw6p5pfgg40afsn0kspp0capvnirk5vrgmjwvfd0skgrcnk2j")))

