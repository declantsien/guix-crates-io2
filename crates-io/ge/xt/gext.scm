(define-module (crates-io ge xt gext) #:use-module (crates-io))

(define-public crate-gext-0.1.0 (c (n "gext") (v "0.1.0") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0f62n10lqycm0985xiqydb6ycqzinr3lchgyhwgs3557nrsbc8bs")))

(define-public crate-gext-0.1.1 (c (n "gext") (v "0.1.1") (d (list (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1m6k2qvrb673md5s9qf5ynihcp1qbbhrbcf5imxkpwsrcffc9mkz")))

