(define-module (crates-io ge ot geotiff) #:use-module (crates-io))

(define-public crate-geotiff-0.0.1 (c (n "geotiff") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)))) (h "0zynzgrl7jlsfvqvwkvia6aw4c34mnsbbyljmha19bjcfllap458")))

(define-public crate-geotiff-0.0.2 (c (n "geotiff") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)))) (h "1mlslz2aicis2211s8w738l76qrnq21b61w7s31mqncn92lsjl8h")))

