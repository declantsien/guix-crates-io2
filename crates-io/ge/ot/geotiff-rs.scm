(define-module (crates-io ge ot geotiff-rs) #:use-module (crates-io))

(define-public crate-geotiff-rs-0.1.0 (c (n "geotiff-rs") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "weezl") (r "^0.1") (d #t) (k 0)))) (h "15x8g9706yjvdrh601i04gfs9i5l24g66s0681qamzkd5fck3n8p")))

(define-public crate-geotiff-rs-0.1.1 (c (n "geotiff-rs") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "weezl") (r "^0.1") (d #t) (k 0)))) (h "0rd0cawfxllp0q33k4plsryd39cbwg79bc6pdwyfsli2pbzyspl0")))

