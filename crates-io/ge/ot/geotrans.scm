(define-module (crates-io ge ot geotrans) #:use-module (crates-io))

(define-public crate-geotrans-0.1.0 (c (n "geotrans") (v "0.1.0") (h "0zy5r4fxg5dhl6cpqbs7sds3023ni0wm6nipl9vh3cc96py14y4q")))

(define-public crate-geotrans-0.1.1 (c (n "geotrans") (v "0.1.1") (h "0kw74j5r3s21d7i8s0cq7j4c67nh65sjdwgl410nc4gnp9yjfdrj")))

(define-public crate-geotrans-0.2.0 (c (n "geotrans") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "018rl2ncjxx82rsi24s5q32c246051j61iix9z3k6by8m9rv16np")))

(define-public crate-geotrans-0.2.1 (c (n "geotrans") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0m24yad25bsn3cc522kgam4n3hqbnaaxhyl5ir259ggwrczay9b1")))

(define-public crate-geotrans-0.2.2 (c (n "geotrans") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ymrj4j6v1660m7f3pm5xljdh8sa2gs7mj3wf7rhisqpclhiwyax")))

