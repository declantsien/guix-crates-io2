(define-module (crates-io ge os geos-sys) #:use-module (crates-io))

(define-public crate-geos-sys-1.0.0 (c (n "geos-sys") (v "1.0.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gvzwv8hadl9mv7jxgn2344k9rpfx1iqajms1n9dagscs49shpg5")))

(define-public crate-geos-sys-1.0.1 (c (n "geos-sys") (v "1.0.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08wkzk28972bvxy9xjw1lnhn9jdkrd6iskj1rqblp21cjc77vrgj")))

(define-public crate-geos-sys-1.0.2 (c (n "geos-sys") (v "1.0.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06pbrl3j9galg22rcffdf9iz4bac10klnbx8f2qpkjy7a4ksw6fd")))

(define-public crate-geos-sys-1.0.3 (c (n "geos-sys") (v "1.0.3") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09p6n69srgcib4kg7ics765q16fva0hx1mg8y7pwjn08ykkm4kgr")))

(define-public crate-geos-sys-1.0.4 (c (n "geos-sys") (v "1.0.4") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dibgqpqjz53pl2c42fgv744vxbwv9490gz2qpshb360jbblp08w")))

(define-public crate-geos-sys-1.0.5 (c (n "geos-sys") (v "1.0.5") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0cjs0fvlf6qbjwkzfx607ds0hgvnavxvmrxgsyl8j0ggywa2h263")))

(define-public crate-geos-sys-1.0.6 (c (n "geos-sys") (v "1.0.6") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0g0swqddfz324nqgzlkqxf2p9nllnk91qmhr90gkli6vfk9cpqgh") (f (quote (("v3_7_0") ("default"))))))

(define-public crate-geos-sys-1.0.7 (c (n "geos-sys") (v "1.0.7") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ljrigfp07ac7f85qhak6jg99bn8q89lff7h9x8gc6sqhk7a07wj") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0") ("default"))))))

(define-public crate-geos-sys-1.0.8 (c (n "geos-sys") (v "1.0.8") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "077bp9h00mg2019yk6gnbjppsikfhpl47cscjr0s7d39wd2a7sin") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.9 (c (n "geos-sys") (v "1.0.9") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rlvrd182j1qs50rvw26flb33cd87qia8cw4azcwy1bk54sl1c2f") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.10 (c (n "geos-sys") (v "1.0.10") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0cd4cw2favjv6r3i21b8qf844cz4h4zx58ylmvrv6ay3hc0mz29k") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.11 (c (n "geos-sys") (v "1.0.11") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18khsv1na76kihj8h253bf7ll65hb7pixvljmaarjkbpm2x843im") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.12 (c (n "geos-sys") (v "1.0.12") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18gxsghjn6hjcp4cld9gh47f6hpri2n0580jk9gxp3w4bk0his6x") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.13 (c (n "geos-sys") (v "1.0.13") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cs9lv6rxnvngi0nwq5v5p2mjljdki10jgwbr6djfqrk25qx7bj1") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.14 (c (n "geos-sys") (v "1.0.14") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n2ir91ai8bhrqhg9qnkrwpj1yn7f0fiymlv38a71khj25rk7pin") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.0.15 (c (n "geos-sys") (v "1.0.15") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "154ylzay8i9gnkkx08g12hw2gnya9dnxiww7fxj1q887zxw89hhc") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.1.0 (c (n "geos-sys") (v "1.1.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1anjj61kyj7g300p33jkf6p049f7l0jxzpzryzfjs5hdvbg4fpf5") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-1.1.1 (c (n "geos-sys") (v "1.1.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d1xcch6ivb4b13ixg25kb4wwma2vrq0brqp297mvswlglpjqyxj") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-2.0.0 (c (n "geos-sys") (v "2.0.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jshjlanqcxs5l67pyklx2x4rxcsqk7640wywwmsn2lw2k0n2yba") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-2.0.1 (c (n "geos-sys") (v "2.0.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w2wsac0cjjcyax033vwrydnwyqv4ymlmvf42x4bb0w23ldal8pk") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("default"))))))

(define-public crate-geos-sys-2.0.2 (c (n "geos-sys") (v "2.0.2") (d (list (d (n "geos-src") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0inlsrng2cg1i9i1wf4kr5gcan90y8d4l4dlvsl351ymrwmba381") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("static" "geos-src" "link-cplusplus") ("default"))))))

(define-public crate-geos-sys-2.0.3 (c (n "geos-sys") (v "2.0.3") (d (list (d (n "geos-src") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12gb90wqy02cadl59b9cirshlp8riphwjbaqcnzsxk3y8y28apv5") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("static" "geos-src" "link-cplusplus") ("default"))))))

(define-public crate-geos-sys-2.0.4 (c (n "geos-sys") (v "2.0.4") (d (list (d (n "geos-src") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0gnvf11r8iphqwf426zjdygkh7fzwiqbhiv406hr1am8i5r2bkhf") (f (quote (("v3_8_0" "v3_7_0") ("v3_7_0" "v3_6_0") ("v3_6_0") ("static" "geos-src" "link-cplusplus") ("default"))))))

(define-public crate-geos-sys-2.0.5 (c (n "geos-sys") (v "2.0.5") (d (list (d (n "geos-src") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0n9qzg5c19vfkwhl4d0i656d62dbvqf3irm1xi94hq8d5g0i7n6j") (f (quote (("v3_9_0") ("v3_8_0") ("v3_7_0") ("v3_11_0") ("v3_10_0") ("static" "geos-src" "link-cplusplus") ("dox") ("default"))))))

(define-public crate-geos-sys-2.0.6 (c (n "geos-sys") (v "2.0.6") (d (list (d (n "geos-src") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "link-cplusplus") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "0k45z7z8bxg1q132f016b6zbggl2zcd2a71w9jljmizg9b977j2d") (f (quote (("v3_9_0") ("v3_8_0") ("v3_7_0") ("v3_11_0") ("v3_10_0") ("static" "geos-src" "link-cplusplus") ("dox") ("default"))))))

