(define-module (crates-io ge os geoscoin) #:use-module (crates-io))

(define-public crate-GeosCoin-0.1.0 (c (n "GeosCoin") (v "0.1.0") (h "13qqih54jv9d8jjrqqazgxrkv41mhqxds2ghfn488kri7qi41lkj") (y #t)))

(define-public crate-GeosCoin-0.1.1 (c (n "GeosCoin") (v "0.1.1") (h "011qq49fiampj2i2r67yi74rvv08iwayqspzy6wj9vq5finmwlfw")))

