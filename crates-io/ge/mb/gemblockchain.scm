(define-module (crates-io ge mb gemblockchain) #:use-module (crates-io))

(define-public crate-gemblockchain-0.1.0 (c (n "gemblockchain") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.7.1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1.2.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "=1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.3") (d #t) (k 0)))) (h "1ijy90x40g6rmmxswzskpsc50ihzdfqmxyhpw7gaa11aldchh2dw")))

(define-public crate-gemblockchain-0.1.1 (c (n "gemblockchain") (v "0.1.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.7.1") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^1.2.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "=1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.3") (d #t) (k 0)))) (h "1iycfpsv0mdjpfmw5y3y994sgz9za738pg5vbirid8hd2q093yhy")))

(define-public crate-gemblockchain-0.2.0 (c (n "gemblockchain") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.1") (d #t) (k 0)))) (h "0d0hz6ppgwn84rn0prhvg9xnn4n3309g6xkfxgwgvhjgviqh40bc") (y #t)))

(define-public crate-gemblockchain-0.2.1 (c (n "gemblockchain") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.1") (d #t) (k 0)))) (h "0p2ivp67b70if48y2xbmcwxv81dz8d0p2rbw6lxnppx2k5h80hs8") (y #t)))

(define-public crate-gemblockchain-0.3.0 (c (n "gemblockchain") (v "0.3.0") (d (list (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "blake2") (r "^0.9.2") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^2.1.3") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "sha3") (r "^0.8.0") (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "07f32v34wvrdnsi8z7h551xmh46nfc821m2fxvwbin5r82ljvcy3")))

