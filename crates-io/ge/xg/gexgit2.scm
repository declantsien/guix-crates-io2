(define-module (crates-io ge xg gexgit2) #:use-module (crates-io))

(define-public crate-gexgit2-0.0.1 (c (n "gexgit2") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1cvqzjjp39d548g3k19jbvjzqf1pah1g4r1504gdrbbyxw5y3xjh") (y #t)))

(define-public crate-gexgit2-0.0.2 (c (n "gexgit2") (v "0.0.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1inlpmhqlskcdg8zhrbq5sq9qi3czqcs3xqnh3hzw6423f2qrhpv") (y #t)))

(define-public crate-gexgit2-0.0.3 (c (n "gexgit2") (v "0.0.3") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1laxrcr2ppyz7z0qn5wjx6yc5kvrs0cff2hf5xp448aj0g22v4f0")))

(define-public crate-gexgit2-0.0.5 (c (n "gexgit2") (v "0.0.5") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "00rzkq45mfs870x0m0bj1g3c7cdm4b360ml78g7dlpsyf1pi94ci")))

(define-public crate-gexgit2-0.0.6 (c (n "gexgit2") (v "0.0.6") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0wz4mnnrgby47zds5k419syzrwsm9ryya297sysarax8brmy7sv1")))

(define-public crate-gexgit2-0.0.7 (c (n "gexgit2") (v "0.0.7") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "06y5bd3yy047qkdjrw0vk3nnkkl5ppgf6fz0mr4kxq1hcx3mc07r")))

(define-public crate-gexgit2-0.0.8 (c (n "gexgit2") (v "0.0.8") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1mmgs1k7i4zasr4xmj4my8f4nyfc476vl96ppw449c72nv7j40zp")))

(define-public crate-gexgit2-0.0.9 (c (n "gexgit2") (v "0.0.9") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "19p8knxzgqf88mhql1s9pb6a0sky2a7lyvrld10jp430f269p4kk")))

