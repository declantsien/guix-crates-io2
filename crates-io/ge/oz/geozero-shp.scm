(define-module (crates-io ge oz geozero-shp) #:use-module (crates-io))

(define-public crate-geozero-shp-0.1.0 (c (n "geozero-shp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geozero") (r "^0.6.0") (d #t) (k 0)) (d (n "geozero-core") (r "^0.6.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dphmb03zqbvcv50x0lf7zqrznqgfrd5h3wsr4v6pypjaq84bqlk")))

(define-public crate-geozero-shp-0.2.0 (c (n "geozero-shp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "dbase") (r "^0.0.4") (d #t) (k 0)) (d (n "geozero") (r "^0.7.1") (k 0)) (d (n "geozero") (r "^0.7.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jqhwipz8plw1dyrnrlszw2yjw9zxv1lm03b7dqyxyxalqi7fmhc")))

(define-public crate-geozero-shp-0.3.0 (c (n "geozero-shp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "dbase") (r "^0.2.3") (d #t) (k 0)) (d (n "geozero") (r "^0.8") (k 0)) (d (n "geozero") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1snvd3x09fspzr3m7dnr3ri22b0xzf6hk26af4sdmflpzwvzcf72")))

(define-public crate-geozero-shp-0.3.1 (c (n "geozero-shp") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "dbase") (r "^0.2.3") (d #t) (k 0)) (d (n "geozero") (r "^0.8") (k 0)) (d (n "geozero") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jkxfd95h7qnqdqqjx6wq8rrw6r31bi312j1qvc609dd1lyqy9ci")))

(define-public crate-geozero-shp-0.4.0 (c (n "geozero-shp") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "dbase") (r "^0.3") (d #t) (k 0)) (d (n "geozero") (r "^0.9.7") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "geozero") (r "^0.9.7") (d #t) (k 2)))) (h "1pgk70drid5bcm9jfg9j7ngyh1pxxlfywkf7265qc9r21rh6yz7n")))

