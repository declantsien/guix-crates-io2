(define-module (crates-io ge ig geiger) #:use-module (crates-io))

(define-public crate-geiger-0.1.0 (c (n "geiger") (v "0.1.0") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1jzr6pg9mdakv283c0pwsnmd4wwg68qp48gp058b2rz7vx1jdzr0")))

(define-public crate-geiger-0.2.0 (c (n "geiger") (v "0.2.0") (d (list (d (n "syn") (r "^0.15.26") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1jm10x2p4ih1cbx2mc1sr5prw3w7266q25dwlv4n2inzrlfqhvic")))

(define-public crate-geiger-0.3.0 (c (n "geiger") (v "0.3.0") (d (list (d (n "syn") (r "^0.15.34") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1balf4capglp1sy6sl718002hhid6vral5v3sxcjzkjlf93gd40w")))

(define-public crate-geiger-0.3.1 (c (n "geiger") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "09p71hk18bmf27flpkix7gjci1zki2f0rs2dqmbll5d93r7h5pm8")))

(define-public crate-geiger-0.3.2 (c (n "geiger") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "0x731cppwk4qghyajyjf2wmn292lb1arff89j072yrjcac5bpc1z")))

(define-public crate-geiger-0.3.3 (c (n "geiger") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1q0jlnxjn1vm09hsmckr9b2019pjndl8f4g1sc50xrwl8zr67c8m")))

(define-public crate-geiger-0.4.0 (c (n "geiger") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.2") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1kimnwhg0cqc4lwly3mxs222swih7ppzc3z0ram8p8mj12s1adv1")))

(define-public crate-geiger-0.4.1 (c (n "geiger") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "173101mvi2p41n13ignq3namwwwbhbk56wqrfkk5dw7pyn6q3cb2")))

(define-public crate-geiger-0.4.2 (c (n "geiger") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0jsqcy2894hn1kcakgnim708n2nw968rh2nbbw70dvqw784pi7ai")))

(define-public crate-geiger-0.4.3 (c (n "geiger") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1rgbdbg50zpvf88i3nkxsjay934zf3ynfbn2ja887x85ja7jbzfd")))

(define-public crate-geiger-0.4.4 (c (n "geiger") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0qll2xq4lrxd20i3qk8f1ngns4scql97i6pkiw9dfa18nn3k1f56")))

(define-public crate-geiger-0.4.5 (c (n "geiger") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.34") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1q8q0h4qxqab3kj72vq4k3f2i7dfg14ykfy03ffxsc42cdyja4zd")))

(define-public crate-geiger-0.4.6 (c (n "geiger") (v "0.4.6") (d (list (d (n "cargo-geiger-serde") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "rstest") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.67") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0dkaqq9xzz9ag0aii4kmrac2mg9fipyr22fas1lpfihzq0g7lpqd")))

(define-public crate-geiger-0.4.7 (c (n "geiger") (v "0.4.7") (d (list (d (n "cargo-geiger-serde") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "rstest") (r "^0.7.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.67") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "123zwznhva2k6xnfkxgfsag4xcpyszr6i5py9lksn5hhq8pm7dcz")))

(define-public crate-geiger-0.4.8 (c (n "geiger") (v "0.4.8") (d (list (d (n "cargo-geiger-serde") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.83") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1m8gyrgclsmwq582jdq93i1m0xvbsxzp22qxaahy1k6y7wq1g368")))

(define-public crate-geiger-0.4.9 (c (n "geiger") (v "0.4.9") (d (list (d (n "cargo-geiger-serde") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "rstest") (r "^0.13.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.95") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0k8bjs9kvsx00284b7cbj37131bq5ig9qlj2141zc9fkifrz497y")))

(define-public crate-geiger-0.4.10 (c (n "geiger") (v "0.4.10") (d (list (d (n "cargo-geiger-serde") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0iqs843x9cdjq2j6csgqhr4lmfjv0d1y64n9mxdxfqn5r23r0wrl")))

(define-public crate-geiger-0.4.11 (c (n "geiger") (v "0.4.11") (d (list (d (n "cargo-geiger-serde") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0h17x11mgdmy1dyr0wcfpg7c9js2ih3fban74v53r8ssi1h956qn")))

(define-public crate-geiger-0.4.12 (c (n "geiger") (v "0.4.12") (d (list (d (n "cargo-geiger-serde") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("parsing" "printing" "clone-impls" "full" "extra-traits" "visit"))) (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1wfagf4pkr73djhld6mpsffx32ngfis9kdpszial8hvvmbvkdris")))

