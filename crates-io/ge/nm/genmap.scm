(define-module (crates-io ge nm genmap) #:use-module (crates-io))

(define-public crate-genmap-1.0.0 (c (n "genmap") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "handy") (r "^0.1") (d #t) (k 2)) (d (n "slotmap") (r "^0.4") (d #t) (k 2)))) (h "05dmag00rrq039akjx5ly4rch5ak9gjflwzxpmsv0bi5pj75zigz")))

(define-public crate-genmap-1.0.1 (c (n "genmap") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "handy") (r "^0.1") (d #t) (k 2)) (d (n "slotmap") (r "^0.4") (d #t) (k 2)))) (h "0q0rpvxcylm7c2aa72xyf7ra6kgbrfg912v6k1b9ayagp529wkmn")))

(define-public crate-genmap-1.0.2 (c (n "genmap") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "handy") (r "^0.1") (d #t) (k 2)) (d (n "slotmap") (r "^0.4") (d #t) (k 2)))) (h "1br5gcwbzspz89qq4xshy88x55rbxnnchsma0m6h0waidmbrf2xj")))

(define-public crate-genmap-1.0.3 (c (n "genmap") (v "1.0.3") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "generational-arena") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "handy") (r ">=0.1.0, <0.2.0") (d #t) (k 2)) (d (n "slotmap") (r ">=0.4.0, <0.5.0") (d #t) (k 2)))) (h "1ly0shzg8bmxgs3ly55ivry1y7rylp52gj773jpxzk5pip82myi1")))

