(define-module (crates-io ge nm genmesh) #:use-module (crates-io))

(define-public crate-genmesh-0.0.1 (c (n "genmesh") (v "0.0.1") (h "1f2ab3izr4c58kmy3qljb9vy77q6s5a7gsib8zblwbbi2nraqcmj")))

(define-public crate-genmesh-0.0.2 (c (n "genmesh") (v "0.0.2") (h "0nakjkl8lr0mnavwj7hgavskb7xp89rsyq7ivmdbnj2s5klhj7zs")))

(define-public crate-genmesh-0.0.3 (c (n "genmesh") (v "0.0.3") (h "1b2jy52hz7c69qxh8yqas1x5sfarfn362bl29yg45izna2r7lp2a")))

(define-public crate-genmesh-0.0.4 (c (n "genmesh") (v "0.0.4") (h "0hsa6dcn2sdmwy7sx07mvghrnczx2g22rhvqvhnzhqcykgmrl8sz")))

(define-public crate-genmesh-0.0.5 (c (n "genmesh") (v "0.0.5") (h "05j4c429m0d9k48bjgbhrdr211rkz2n51znijzpvnfny6qh5mjy7")))

(define-public crate-genmesh-0.0.6 (c (n "genmesh") (v "0.0.6") (h "0rgmh8rkxwpww6d5i48jnn9if63sci4nx5aw9nblj2r8w8qqjdv5")))

(define-public crate-genmesh-0.0.7 (c (n "genmesh") (v "0.0.7") (h "1jldq7ipaw0983s56cjrlsxgphzf195y3qzrdg1zj720q0dhkxlg")))

(define-public crate-genmesh-0.0.8 (c (n "genmesh") (v "0.0.8") (h "1i5xqpw7ihc69xbx9q6hhxdc2n15a1gc18w1721zp5v4am5dinzw")))

(define-public crate-genmesh-0.0.9 (c (n "genmesh") (v "0.0.9") (h "11h4qigpa5sckiv5ldfc307b1az9ff4545q0yjw8xkb7g2z4n23j")))

(define-public crate-genmesh-0.0.10 (c (n "genmesh") (v "0.0.10") (h "18bwz2xs0abkc356y2fir4b0607s10361r729grd4jrhd6kvqlzi")))

(define-public crate-genmesh-0.0.11 (c (n "genmesh") (v "0.0.11") (h "148kzfgv1k4s675bpmsc3ck826vygbq2y341cgv8ijj4svnmhl9n")))

(define-public crate-genmesh-0.0.12 (c (n "genmesh") (v "0.0.12") (h "11dgigrzrq0r49773rz7q85mfa7g2rl2b7q8ps78wx0jsjym6flx")))

(define-public crate-genmesh-0.0.13 (c (n "genmesh") (v "0.0.13") (h "0j5afj2q93a3fk2mhwdq82cmgpmsrgix32id77scbq54y1sa4a8p")))

(define-public crate-genmesh-0.1.0 (c (n "genmesh") (v "0.1.0") (h "0dj2x9sqmlp1kvwa3a19fz2cgx6qgilklj53w73z903lgiinflj2")))

(define-public crate-genmesh-0.2.0 (c (n "genmesh") (v "0.2.0") (h "1z32y5lcx511bg9mj6v1lvgnz9s2iajnaxy1xdjg4ljm6mj9i2xw")))

(define-public crate-genmesh-0.2.1 (c (n "genmesh") (v "0.2.1") (h "1kkjl2afqfc5jw9f2amhgddh9lwy78znpc4q9i3yl1466syzkd7j")))

(define-public crate-genmesh-0.3.0 (c (n "genmesh") (v "0.3.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)))) (h "0jy6dg3xpfc0ynsigh0c810n82zb2cbd733cz7rrg2vx05dcg9di")))

(define-public crate-genmesh-0.3.1 (c (n "genmesh") (v "0.3.1") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)))) (h "1mwzm40ba7vi4wr1l37vsw0qnzbcjvgrzlh21qz14wpghg4k5py3")))

(define-public crate-genmesh-0.3.2 (c (n "genmesh") (v "0.3.2") (d (list (d (n "cgmath") (r "*") (d #t) (k 0)))) (h "07cfn4b4fc4k5i729q2hj6igm0rm8mbw7d9g8zgxxw44k4imsnd8")))

(define-public crate-genmesh-0.3.3 (c (n "genmesh") (v "0.3.3") (d (list (d (n "cgmath") (r "^0.3") (d #t) (k 0)))) (h "1k67l8b296iac35wkg2pkc9day96g3kx78lpqsfs6vzhc4wpaxq7")))

(define-public crate-genmesh-0.4.0 (c (n "genmesh") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.7") (d #t) (k 0)))) (h "1ncf8v3b43zmqdzyd43b64n6ad0s0pbrc2pima4dy8h17ds5lgpr")))

(define-public crate-genmesh-0.4.1 (c (n "genmesh") (v "0.4.1") (d (list (d (n "cgmath") (r "^0.7") (d #t) (k 0)))) (h "14mlmyah9n83ffbjwdnvryknrqk07cqbgqmz5ab2fawhn4pkcyg4")))

(define-public crate-genmesh-0.4.2 (c (n "genmesh") (v "0.4.2") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)))) (h "1634jn2py3czpaymd9n89wh2rj6hp6y5jyngfyxbbmvpmxvvjydp")))

(define-public crate-genmesh-0.4.3 (c (n "genmesh") (v "0.4.3") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)))) (h "06yvllg11qa58jbfq7wrplmdh4lv4ji5y8l6m0q09fq7w0cacx54")))

(define-public crate-genmesh-0.5.0 (c (n "genmesh") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)) (d (n "mint") (r "^0.2") (d #t) (k 0)))) (h "0rhvaqqqg3zqwsb58fqzkrg52bf98fdxcmg9wdsm86sy763wcj23")))

(define-public crate-genmesh-0.6.0 (c (n "genmesh") (v "0.6.0") (d (list (d (n "cgmath") (r "^0.16") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "10sqydcgc37i95abwmq6kiwxr7nbhzp37ad1glfha1hwgj901zd5")))

(define-public crate-genmesh-0.6.1 (c (n "genmesh") (v "0.6.1") (d (list (d (n "cgmath") (r "^0.16") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "0c09n7pa3bzxl4dq80s2h2j58nnbaqcbfbiv155pcwapd6xxbhip")))

(define-public crate-genmesh-0.6.2 (c (n "genmesh") (v "0.6.2") (d (list (d (n "cgmath") (r "^0.16") (f (quote ("mint"))) (d #t) (k 0)) (d (n "mint") (r "^0.5") (d #t) (k 0)))) (h "17qybydyblf3hjiw7mq181jpi4vrbb8dmsj0wi347r8k0m354g89")))

