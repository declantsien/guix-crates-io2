(define-module (crates-io ge nr genref) #:use-module (crates-io))

(define-public crate-genref-0.7.0 (c (n "genref") (v "0.7.0") (h "1d9hn1cl0lq451avp9hpq4zznc6s31axjmqapjwxjfbivsyvh30g") (y #t)))

(define-public crate-genref-0.8.0 (c (n "genref") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "lock_api") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "1glxdk604iw1ykgnnbsac44sja3shllapxvprbicl07dcsxkdyhr") (f (quote (("default" "global")))) (s 2) (e (quote (("global" "dep:lazy_static" "dep:parking_lot" "dep:lock_api"))))))

