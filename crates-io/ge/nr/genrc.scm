(define-module (crates-io ge nr genrc) #:use-module (crates-io))

(define-public crate-genrc-0.1.0 (c (n "genrc") (v "0.1.0") (h "1mqbczw8kgk5k3s00aq49snjj646c9rvhqfsfmhgn4m7a8gcria7")))

(define-public crate-genrc-0.2.0 (c (n "genrc") (v "0.2.0") (h "0dgyziihcvp457y102h7yw4akin3pngb7y86h27wi24ajnsmb04w") (f (quote (("allocator_api"))))))

(define-public crate-genrc-0.2.1 (c (n "genrc") (v "0.2.1") (h "10drn83zgyc2mw4rzz446ydjc5b1s344b0iiglc2s2dsxsglqj89") (f (quote (("allocator_api"))))))

(define-public crate-genrc-0.3.0 (c (n "genrc") (v "0.3.0") (d (list (d (n "bumpalo") (r "^3.13") (d #t) (k 2)))) (h "14943kl7y7ca36gwfs5qqnx5xmib2mv4r3mxnjn99m8wswn668xm") (f (quote (("allocator_api" "bumpalo/allocator_api"))))))

