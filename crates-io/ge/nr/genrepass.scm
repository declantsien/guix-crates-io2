(define-module (crates-io ge nr genrepass) #:use-module (crates-io))

(define-public crate-genrepass-1.0.0 (c (n "genrepass") (v "1.0.0") (d (list (d (n "clipboard-ext") (r "^0.2.0") (d #t) (k 0)) (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0qlr2xx3d4dixgp6hdb66vvp74yr08zkj3jcxas72qkksnn6b77s") (y #t)))

(define-public crate-genrepass-1.0.1 (c (n "genrepass") (v "1.0.1") (d (list (d (n "copypasta-ext") (r "^0.3.2") (d #t) (k 0)) (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1h59dlyf0ibhnvxhgagv18c4165r9pir5ddzrc4v8i7qav3i2s9r") (y #t)))

(define-public crate-genrepass-1.1.0 (c (n "genrepass") (v "1.1.0") (d (list (d (n "deunicode") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "06f84lym1m42lvizr4qwv7i6i2188p6mmv4wj73rclcmaj96qsfb") (y #t)))

(define-public crate-genrepass-1.1.1 (c (n "genrepass") (v "1.1.1") (d (list (d (n "deunicode") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "10jc81s4jdwd5ya5bfnwsr1db5mnwml83x9r72jzc9jz0xdywms3") (y #t)))

(define-public crate-genrepass-1.1.2 (c (n "genrepass") (v "1.1.2") (d (list (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)))) (h "0az8gbb8b2xjnf1ms3nczb290qn44b067k8a7lbdz3hvfydv6whs") (y #t)))

(define-public crate-genrepass-1.1.3 (c (n "genrepass") (v "1.1.3") (d (list (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.9") (d #t) (k 0)))) (h "1859cbwvif9sih9pyp4ximss5y6izq8wxhc2vj8n6m4517fpfdzp")))

(define-public crate-genrepass-1.1.4 (c (n "genrepass") (v "1.1.4") (d (list (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0bychk4f1rjzy5wxr48kmf231n3hxnpip448p4q6d8jk9z18adiz")))

