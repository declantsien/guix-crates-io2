(define-module (crates-io ge nr genrepass-cli) #:use-module (crates-io))

(define-public crate-genrepass-cli-0.0.0 (c (n "genrepass-cli") (v "0.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3") (d #t) (k 0)) (d (n "genrepass") (r "^1.1") (d #t) (k 0)))) (h "01pvr2r960rvi1844y7id6nyag3v7c0qfw7rdxkf815lkzax4sxn")))

(define-public crate-genrepass-cli-1.1.0 (c (n "genrepass-cli") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3") (d #t) (k 0)) (d (n "genrepass") (r "^1.1.3") (d #t) (k 0)))) (h "0k5z7a4x7gvf2pjvpfz5s1q5pl12r6vkzlzmxcrfq3qbi574md45")))

(define-public crate-genrepass-cli-1.1.1 (c (n "genrepass-cli") (v "1.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3") (d #t) (k 0)) (d (n "genrepass") (r "^1.1.4") (d #t) (k 0)))) (h "143cb3rc6830975grv7841v5c071ixrsjaidsgfigy4k5z6iz7ax")))

