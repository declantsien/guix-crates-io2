(define-module (crates-io ge cl gecl) #:use-module (crates-io))

(define-public crate-gecl-0.0.1 (c (n "gecl") (v "0.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wmp1w4jm2869k31cdqkq87pw03fw6slji36mr5k8kllna7cllqc")))

(define-public crate-gecl-0.0.2 (c (n "gecl") (v "0.0.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mkwnm5l1y5xlr6yj2dy8nbnypwx1c45pvxrk6n9xa9rf0iacsz2")))

(define-public crate-gecl-0.1.0 (c (n "gecl") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s8lvmxq1x63zzpvdmzigyrm7zr89jcrh26pmm2jh3n833s24ah9")))

(define-public crate-gecl-0.1.1 (c (n "gecl") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pyak2qk4p6726367jixm402wjdzdhjk9ggn8s617309n1kiylkb")))

(define-public crate-gecl-0.1.2 (c (n "gecl") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cxm38gl7rs1r9j8bz6rmh9bg218770vyi3h6i936xr13yad2zi6")))

(define-public crate-gecl-0.2.0 (c (n "gecl") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rav5c2iw5j43granhwnnbjglmldsy4hj8d91kzc5acwk8gp7gd3")))

(define-public crate-gecl-0.2.1 (c (n "gecl") (v "0.2.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14xbfdh1daz3002mgr3z43yr4qgwkkw03kcim5y3bnlj983ybxnm")))

