(define-module (crates-io ge cs gecs) #:use-module (crates-io))

(define-public crate-gecs-0.0.1 (c (n "gecs") (v "0.0.1") (d (list (d (n "gecs_macros") (r "^0.0.1") (d #t) (k 0)))) (h "0viw8lnpnp85995yclbb6slfa9nqymkmwwqaxsysrpdr6lbh32rh")))

(define-public crate-gecs-0.0.2 (c (n "gecs") (v "0.0.2") (d (list (d (n "gecs_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0plj5y9hvmai93cnypric5k8csgl3ny9yc83l8anmga2r75dlrjy")))

(define-public crate-gecs-0.0.3 (c (n "gecs") (v "0.0.3") (d (list (d (n "gecs_macros") (r "^0.0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "09fgmj2v1drk34is4hdjm9r86kjb8p5nx08cszhm861wakis8za8")))

(define-public crate-gecs-0.0.4 (c (n "gecs") (v "0.0.4") (d (list (d (n "gecs_macros") (r "^0.0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0dkdalva0a2wibm073hglypafpf5h0ap7k3na1j7g96wnd1zccmd")))

(define-public crate-gecs-0.0.5 (c (n "gecs") (v "0.0.5") (d (list (d (n "gecs_macros") (r "^0.0.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1nrz3np8prf318l74zrs7i5naxlkbzhm04ic7rhb1w0g5170dc1p")))

(define-public crate-gecs-0.0.6 (c (n "gecs") (v "0.0.6") (d (list (d (n "gecs_macros") (r "^0.0.6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "05dsddfl3cl1gkdm37hy12bfij3p7br1bs5l1r1kz52cw2p6alar")))

(define-public crate-gecs-0.0.7 (c (n "gecs") (v "0.0.7") (d (list (d (n "gecs_macros") (r "^0.0.7") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0vrmvfn7qa4hqvnh37lw3cs9dzkm3ww8rsg06yci8bc9k89349h3")))

(define-public crate-gecs-0.0.8 (c (n "gecs") (v "0.0.8") (d (list (d (n "gecs_macros") (r "^0.0.8") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1f3kdhdqiw2rbm0w2gw7c5qkkd4pshjz4057b1vmyhj7jcznqmyf")))

(define-public crate-gecs-0.0.9 (c (n "gecs") (v "0.0.9") (d (list (d (n "gecs_macros") (r "^0.0.9") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "19g425d6ldp0nz61b4grjb6b9w34v7pp3vbi2dndjx1hfqvgjg3s")))

(define-public crate-gecs-0.1.0 (c (n "gecs") (v "0.1.0") (d (list (d (n "gecs_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1ga8d4znjp3rhk6wb4j6bn2c73i1cgxa9h0pr595mnb0kcw3ah9a")))

(define-public crate-gecs-0.1.1 (c (n "gecs") (v "0.1.1") (d (list (d (n "gecs_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0p6lichkgcyknix18w1386msjnfk6r1p8ij8rj3y3ba2ickdxnk5")))

(define-public crate-gecs-0.1.2 (c (n "gecs") (v "0.1.2") (d (list (d (n "gecs_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0q3c2kya5yi99zr39kzjal5f1yq2d87k893679ajhf1zq9v3f9n6")))

(define-public crate-gecs-0.2.0 (c (n "gecs") (v "0.2.0") (d (list (d (n "gecs_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1dlhkmbqyxp6hqrzv8zw6axwsfck30i2m5k3jr4df4sff310icgk") (f (quote (("default") ("32_components"))))))

(define-public crate-gecs-0.2.1 (c (n "gecs") (v "0.2.1") (d (list (d (n "gecs_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1xgz250c8d3j00dsnprq4rskjxygynrfg1gbrlbqp2dxz4n5n6gz") (f (quote (("default") ("32_components"))))))

(define-public crate-gecs-0.2.2 (c (n "gecs") (v "0.2.2") (d (list (d (n "gecs_macros") (r "^0.2.2") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "1rkq6i753spsn7z94lrc5vxd5pwmhhxd58l4vl0hhvv9mbmzw7kg") (f (quote (("default") ("32_components"))))))

(define-public crate-gecs-0.3.0 (c (n "gecs") (v "0.3.0") (d (list (d (n "gecs_macros") (r "^0.3.0") (k 0)) (d (n "seq-macro") (r "^0.3") (d #t) (k 0)))) (h "0kjqdrl6yaiy6jrjg87r6071m84nhbrqax6ddlmy5029p9s7hrmf") (f (quote (("wrapping_entity_version") ("wrapping_entity_raw_version") ("default") ("32_components"))))))

