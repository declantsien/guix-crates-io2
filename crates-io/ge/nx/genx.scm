(define-module (crates-io ge nx genx) #:use-module (crates-io))

(define-public crate-genx-0.1.0 (c (n "genx") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1xqdp4mjkl00499av3j3gnmnmkqkxs0ih225922gpy05i3miccaj")))

(define-public crate-genx-0.2.0 (c (n "genx") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0wb3ndfv8s2cvwf7xk1mgmcsczhh97y2k1a97y0j89sgqcrya6w0")))

(define-public crate-genx-0.2.1 (c (n "genx") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "19h550jdf3y43jaml1r48hpy18kabbxbnh39cw17qb6ly7xpzm8x")))

(define-public crate-genx-0.2.2 (c (n "genx") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1c3jj0gss06gncs2qdhgklcix64ffmijij13y2bp37d0wrzwanfb")))

(define-public crate-genx-0.2.3 (c (n "genx") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1143y6ksiwcfyaknn7csr0m4kjprk208hqsd09lsf1jiygkxkwn3")))

(define-public crate-genx-0.3.0 (c (n "genx") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0r55i3hjgh40z52gw85zhgdxmwk68pni1yi6i8g0kgh8wb52y5hj")))

(define-public crate-genx-0.3.1 (c (n "genx") (v "0.3.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1js1g2nzj592116lg0vqn3ankc9sn69d59dhmbqkk8z2b31flmnh")))

(define-public crate-genx-0.3.2 (c (n "genx") (v "0.3.2") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08dbffrziv1pjs39fvlsa6n1xwgg8vqn80fvpamfg2z8rj43ihh2")))

(define-public crate-genx-0.3.3 (c (n "genx") (v "0.3.3") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0rcmsc908655vwkrj37c36fmlk5afdmmryrc9r816kk67dz4s615")))

(define-public crate-genx-0.3.4 (c (n "genx") (v "0.3.4") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "10d13cgpwmw5cr9m9m4v5za44hqsi9b5wqkd2dcn35rhzcxpv281")))

(define-public crate-genx-0.4.0 (c (n "genx") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0qwymgmh3sr9j97sa9s51wfnpl0bdjrn9rm78h593qfbyfzy9pmz")))

