(define-module (crates-io ge st gestalt) #:use-module (crates-io))

(define-public crate-gestalt-0.1.0 (c (n "gestalt") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "dirs") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.17") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.24") (f (quote ("Storage" "Window"))) (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1c5h711m7j1rbi96m84fgvwj3q1is2flaadnjlw7r1v5vk7r91iq")))

