(define-module (crates-io ge st gestetner) #:use-module (crates-io))

(define-public crate-gestetner-0.1.0 (c (n "gestetner") (v "0.1.0") (d (list (d (n "governor") (r "^0.3.2") (f (quote ("dashmap"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nonzero_ext") (r "^0.2.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rouille") (r "^3.1.1") (d #t) (k 0)))) (h "0p20sczpxg6l3a6caff20fgny3ahzmfqy3ci3sisx7r9yx9l5540")))

