(define-module (crates-io ge st gestalt_ratio) #:use-module (crates-io))

(define-public crate-gestalt_ratio-0.1.0 (c (n "gestalt_ratio") (v "0.1.0") (h "0b7gp8n04rsai2n9d6378qfd7yph1yb1g9nlykwx14y4yq4yxjls")))

(define-public crate-gestalt_ratio-0.2.0 (c (n "gestalt_ratio") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0kf0r3jfqrji47zcs9wcq0n4j1r0mqqvyzpx7xaa43b0qw11fc04")))

(define-public crate-gestalt_ratio-0.2.1 (c (n "gestalt_ratio") (v "0.2.1") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1va8iy8h619g1fn13z1c9kmxsldlfky7w9rcg7hls2x9r6cw7cyn")))

