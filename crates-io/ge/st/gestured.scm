(define-module (crates-io ge st gestured) #:use-module (crates-io))

(define-public crate-gestured-0.0.0 (c (n "gestured") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "input") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "1l0xi7wdb47mvynyvh8c7ydslydp6xvm5r7mqr4y5nzgbmcc9rm8")))

