(define-module (crates-io ge oj geojsonseq) #:use-module (crates-io))

(define-public crate-geojsonseq-0.1.0 (c (n "geojsonseq") (v "0.1.0") (d (list (d (n "geojson") (r "^0.19.0") (d #t) (k 0)) (d (n "jsonseq") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a0pgqd2qg1iy9zivd868vsm63wap79k1zklv5i1n12nc8brvbzg")))

(define-public crate-geojsonseq-0.1.1 (c (n "geojsonseq") (v "0.1.1") (d (list (d (n "geojson") (r "^0.19") (f (quote ("geo-types"))) (d #t) (k 0)) (d (n "jsonseq") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11czpzrm45fyg30bxvqzaf9jnxsxwcmh2mn0xx9x854af4wz7i38")))

