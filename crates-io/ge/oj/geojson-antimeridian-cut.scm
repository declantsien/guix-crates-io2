(define-module (crates-io ge oj geojson-antimeridian-cut) #:use-module (crates-io))

(define-public crate-geojson-antimeridian-cut-0.1.0-alpha.1 (c (n "geojson-antimeridian-cut") (v "0.1.0-alpha.1") (d (list (d (n "geojson") (r "^0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09h95rjwqb551ds0g4bsxcyg0q3s2f15bp254pdq9yy23ligzwvb")))

