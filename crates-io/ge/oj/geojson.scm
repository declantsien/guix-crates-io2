(define-module (crates-io ge oj geojson) #:use-module (crates-io))

(define-public crate-geojson-0.0.1 (c (n "geojson") (v "0.0.1") (h "055dqd12pkr3adfmisvc7z8f6crmx7qi30sp0d3j87p866zcn4s5")))

(define-public crate-geojson-0.0.2 (c (n "geojson") (v "0.0.2") (h "1l8sdzvnfbk1v6vwki90r3x7g8ms3jmnsxd41cp1avk1nw8w52xr")))

(define-public crate-geojson-0.0.3 (c (n "geojson") (v "0.0.3") (h "1yr6dnczfij7ckr2176xc7bzrf4msqvm40gxx0mfn0nyzf2arw1v")))

(define-public crate-geojson-0.0.4 (c (n "geojson") (v "0.0.4") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "0v7b2mx1c8pb50lwwawnlvq5r1ycx7b1nzs5hidx1487y8b4c6z3")))

(define-public crate-geojson-0.0.5 (c (n "geojson") (v "0.0.5") (d (list (d (n "rustc-serialize") (r "^0.2") (d #t) (k 0)))) (h "09mv2vpqxjfw76yr0pv0qnwadbbylpz4vkm06dd9cij6anmm3p56")))

(define-public crate-geojson-0.0.6 (c (n "geojson") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0340mqva6byppdy4l911fshq5m1c0i9yji6s88zpa6ibmwv7w5rd")))

(define-public crate-geojson-0.0.7 (c (n "geojson") (v "0.0.7") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1q0zixgmch7d14wayqwmpg9ns8d7il1wih5mbajz8hmf7qsh6160")))

(define-public crate-geojson-0.0.8 (c (n "geojson") (v "0.0.8") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zvimm72228zskris0ans5sjx2k915afvxv4mkwyfdgndhxnz4aj")))

(define-public crate-geojson-0.0.9 (c (n "geojson") (v "0.0.9") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1gmifzwxd4psli4wl5cyrzpg7aqxsq7zl51vfq9d8f0bmbr7l8ca")))

(define-public crate-geojson-0.1.0 (c (n "geojson") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0li6a7d7bca7sflv2fp77d6piz3yay9d116bfydf1nxqkdc7nwla")))

(define-public crate-geojson-0.1.1 (c (n "geojson") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kx14wwyrg3jyimv085829z0b7f9n04v2qfk75ackb9cqnaz436k")))

(define-public crate-geojson-0.2.0 (c (n "geojson") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06rjbj9way9488dcndrhvz94r5ibanxs3382zgs8phjphniw53vj")))

(define-public crate-geojson-0.2.1 (c (n "geojson") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (o #t) (d #t) (k 0)))) (h "178nxjrcs0x3yr991jldapdzfk5a9r9a5yh6k78745wmwjqs38lm") (f (quote (("with-serde" "serde" "serde_json") ("default" "rustc-serialize"))))))

(define-public crate-geojson-0.3.0 (c (n "geojson") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.7") (o #t) (d #t) (k 0)))) (h "15k2ks5dh43cc905a2vgbvn7yjx3axa3g350wgai321ygr8r0irf") (f (quote (("with-serde" "serde" "serde_json") ("default" "rustc-serialize"))))))

(define-public crate-geojson-0.4.0 (c (n "geojson") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (o #t) (d #t) (k 0)))) (h "0nf4k2r92vijl6116rrij3lg92ac6fqswc1shnawr43c2phmj583") (f (quote (("with-serde" "serde" "serde_json") ("default" "rustc-serialize"))))))

(define-public crate-geojson-0.4.1 (c (n "geojson") (v "0.4.1") (d (list (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (o #t) (d #t) (k 0)))) (h "0hgyk0wlkiyw1n5hf566v3gayphcrfk14z67a2q8bbvrxcy5384w") (f (quote (("with-serde" "serde" "serde_json") ("default" "rustc-serialize"))))))

(define-public crate-geojson-0.4.2 (c (n "geojson") (v "0.4.2") (d (list (d (n "geo") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (o #t) (d #t) (k 0)))) (h "0r0pfxkgazxvr0i6g177s1rbbw8hkzbilk76xbian7hkj4rijbvz") (f (quote (("with-serde" "serde" "serde_json") ("geo-interop" "geo" "num") ("default" "rustc-serialize" "geo-interop"))))))

(define-public crate-geojson-0.4.3 (c (n "geojson") (v "0.4.3") (d (list (d (n "geo") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~0.8") (o #t) (d #t) (k 0)))) (h "0r4fmanwri2p0yyx2ckl3ckrbz5r48ggi1shg2kp3rr04s5g54nx") (f (quote (("with-serde" "serde" "serde_json") ("geo-interop" "geo" "num") ("default" "rustc-serialize" "geo-interop"))))))

(define-public crate-geojson-0.5.0 (c (n "geojson") (v "0.5.0") (d (list (d (n "geo") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "1fyflx0lg6wkazjvjqdfxwkqqq5jmy79igv34c6lwsiqyls7265n")))

(define-public crate-geojson-0.6.0 (c (n "geojson") (v "0.6.0") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~0.9") (d #t) (k 0)) (d (n "serde_json") (r "~0.9") (d #t) (k 0)))) (h "0vb5a73q6pzfhhwm7c0jp97mp5123ifyam3x6lkdkm0yv50jy4l0")))

(define-public crate-geojson-0.7.0 (c (n "geojson") (v "0.7.0") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0842fawr556a2g69v68i2sbr787g8j23dfw8rs9120k6x3r503f8")))

(define-public crate-geojson-0.7.1 (c (n "geojson") (v "0.7.1") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1pkqky1i0bw9110cvdpbibxrhb4gpjhz1dambgd6h6bc0r69r97y")))

(define-public crate-geojson-0.8.0 (c (n "geojson") (v "0.8.0") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1jagcp9mrkw8hzdw3qbzk4mlamb73b76kphjjf41sxvd7x14x8nb")))

(define-public crate-geojson-0.8.1 (c (n "geojson") (v "0.8.1") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1vfv65n1s2af6hydvbj0n3qwzn8q6bbpg7vb6g7lbgdnfypr4ziv")))

(define-public crate-geojson-0.9.0 (c (n "geojson") (v "0.9.0") (d (list (d (n "geo") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0f6nnvkv75d52vm31xzmiavgzfbci8hxsvnw5dh451c45775gk8y")))

(define-public crate-geojson-0.9.1 (c (n "geojson") (v "0.9.1") (d (list (d (n "geo") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1lf3jykmcb5l7wcjzb1xzj2zgnzj5kz5ss7yvzjyf2snx7llqj5k")))

(define-public crate-geojson-0.10.0 (c (n "geojson") (v "0.10.0") (d (list (d (n "geo") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1ry5xmh1186yhwm80ysvih7sdqadzb79xvz23wrx8w8qy9wnm6dv")))

(define-public crate-geojson-0.11.0 (c (n "geojson") (v "0.11.0") (d (list (d (n "geo-types") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0p22nzpbgsprsv7cfm6b567xyl55pahrhv7b4giagjihaqfpfxqz")))

(define-public crate-geojson-0.11.1 (c (n "geojson") (v "0.11.1") (d (list (d (n "geo-types") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0fc5ci5fn80pn0svvzanjv2272a0ssc3bk0a4wnk59m44hma8dpv")))

(define-public crate-geojson-0.12.0 (c (n "geojson") (v "0.12.0") (d (list (d (n "geo-types") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "0vckx82kvj5dr8yc7papdzqglsbpj5n6fkvhffjb5qjrnd5vakzs")))

(define-public crate-geojson-0.13.0 (c (n "geojson") (v "0.13.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1bl3rwpa0jkd98h96zfb7p52baxfawgxkxjpc9n8d1bqra1fs5ds")))

(define-public crate-geojson-0.14.0 (c (n "geojson") (v "0.14.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1h451dq7131c3kfmjwh7h1sxl4njq3sq2w5f9xx4vqxhyswvf9c1")))

(define-public crate-geojson-0.15.0 (c (n "geojson") (v "0.15.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1p8bkqylifznkxjih8adhc0v44g4pz8vkfjl2wj5fpwbb4lpmpq5")))

(define-public crate-geojson-0.16.0 (c (n "geojson") (v "0.16.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "17pgqg8yn6ymh97qnzg8awsi62xdxf4s85h16s0vx67lqxbpxcr2")))

(define-public crate-geojson-0.17.0 (c (n "geojson") (v "0.17.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1fcw84w04ikssjykzi71fzpr233wdk3s1ssn2xzzqxijid107b74")))

(define-public crate-geojson-0.18.0 (c (n "geojson") (v "0.18.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1bs3dz8h67kc8h06b43ygjmnw0i97g0wnbr3xaj0fy5p6q5rgdn2")))

(define-public crate-geojson-0.19.0 (c (n "geojson") (v "0.19.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)))) (h "1ayj3znwzwkzrg0dmz4ykqwzmw7vdhb7hz4myd3dz61jr9mni66s")))

(define-public crate-geojson-0.20.0 (c (n "geojson") (v "0.20.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1b3sp11iwm8k3j9mblx3c56pkdp4hdylrn1ci774ckyia7y4m4g3")))

(define-public crate-geojson-0.20.1 (c (n "geojson") (v "0.20.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1xishn67h3rsl350nx68h8l4x8i98lynb94jyqr5nwkxwy67cj6z")))

(define-public crate-geojson-0.21.0 (c (n "geojson") (v "0.21.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1gj61yzfmryqjcsmg5lr3k8ynqdvd1xxkrqdr39xgckamyjp60pn")))

(define-public crate-geojson-0.22.0 (c (n "geojson") (v "0.22.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wqc7n5nbr52s8xbrlq9a0361iivmqw8hhqc0f70bi5w3yms8455")))

(define-public crate-geojson-0.22.1 (c (n "geojson") (v "0.22.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0505ad8wm9bmc1k5h5q5kyp5p3vj19g5xz47mdhg84sv6z8fck2z") (y #t)))

(define-public crate-geojson-0.22.2 (c (n "geojson") (v "0.22.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1k4ww2zprsh7jj6cshkv9s98a51axy5q09rin8pi9n98dm29w84w")))

(define-public crate-geojson-0.22.3 (c (n "geojson") (v "0.22.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0pdz9qqrip1619hp4am32z09lvxw51gvq5bdns14vf8bhixhmnbj")))

(define-public crate-geojson-0.22.4 (c (n "geojson") (v "0.22.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0z7hq3z8fzskfm37whw0pgi7by374qz71lk4dqf3sgaz4wbhvn85")))

(define-public crate-geojson-0.23.0 (c (n "geojson") (v "0.23.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "192yi4qz5q74yh54v3sjb992x1xbmgj46z1qwi6jhrigw9xi9hgk") (f (quote (("default" "geo-types"))))))

(define-public crate-geojson-0.24.0 (c (n "geojson") (v "0.24.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1dvj4bf5hzzpcxvwdhqchi9zhy9pw9v3s8jfa6jv4mssandc6b7z") (f (quote (("default" "geo-types"))))))

(define-public crate-geojson-0.24.1 (c (n "geojson") (v "0.24.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "06r4r4ibssfx3acfliikh7p7jvsqn1ngw7hmfj6k5gqzvz0jimx5") (f (quote (("default" "geo-types"))))))

