(define-module (crates-io ge co geco-consul-connector) #:use-module (crates-io))

(define-public crate-geco-consul-connector-1.0.0 (c (n "geco-consul-connector") (v "1.0.0") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.1") (f (quote ("transport" "codegen" "tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.1") (d #t) (k 1)))) (h "1w3qs1s7ij0yciaav7hqn0ws2y9fgfsl80p2bgkpwvmd29pfh5iv")))

(define-public crate-geco-consul-connector-1.0.1 (c (n "geco-consul-connector") (v "1.0.1") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tonic") (r "^0.3.1") (f (quote ("transport" "codegen" "tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.1") (d #t) (k 1)))) (h "191qzpymwy40n6bka965xx1r3m9kiiyihmcc7lmph09hk5qd3yrq")))

(define-public crate-geco-consul-connector-1.1.0 (c (n "geco-consul-connector") (v "1.1.0") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "tonic") (r "^0.4.0") (f (quote ("transport" "codegen" "tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.4.0") (d #t) (k 1)))) (h "0qhsf0g22ky8nyms8j142bsjcb8ffr30izr97byqk25087zj44l7")))

