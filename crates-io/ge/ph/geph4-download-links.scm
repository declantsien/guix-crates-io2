(define-module (crates-io ge ph geph4-download-links) #:use-module (crates-io))

(define-public crate-geph4-download-links-0.1.0 (c (n "geph4-download-links") (v "0.1.0") (h "1xfdincnpsyr50i9x45svr0axcqn7i293js8gfygp4jav8z58bha")))

(define-public crate-geph4-download-links-4.6.2 (c (n "geph4-download-links") (v "4.6.2") (h "1iiajfbc4hid665pd4914fan0dqh2c202a3c2bp3a8qnkkzf7r2w")))

