(define-module (crates-io ge vl gevlib) #:use-module (crates-io))

(define-public crate-gevlib-0.1.0 (c (n "gevlib") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0mw9mxpnssc3cs0n2jnln0ncygaabk87w10yawa1kp53v6516si7")))

(define-public crate-gevlib-0.1.1 (c (n "gevlib") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "1lg4jxvmg86kjwjj54vv9wd5xwl76dlb0g7ipkvlkd84yilqq9cp")))

(define-public crate-gevlib-0.1.2 (c (n "gevlib") (v "0.1.2") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "031kwca6g3cpxfnglisy9k9jkchgqpykshf5qd7fl0nh9krxqqki") (y #t)))

(define-public crate-gevlib-0.1.3 (c (n "gevlib") (v "0.1.3") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0vfdxn4v2mjdh9246f3arggvplxw0l7v79009k4pd3n18xa6ff3z")))

(define-public crate-gevlib-0.1.4 (c (n "gevlib") (v "0.1.4") (d (list (d (n "libm") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0x2pvyldq9ybl0y4sn3p094ysv90ppl22r63vbzk668hfsyz2hyz")))

