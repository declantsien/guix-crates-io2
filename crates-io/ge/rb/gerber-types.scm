(define-module (crates-io ge rb gerber-types) #:use-module (crates-io))

(define-public crate-gerber-types-0.1.0 (c (n "gerber-types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "~0.0.138") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "10idkrmllif2x4i28w5jrxvj1ahkj1n4qkhl2n5la8yy1yf4aqgb") (f (quote (("nightly" "clippy"))))))

(define-public crate-gerber-types-0.1.1 (c (n "gerber-types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clippy") (r "~0.0.138") (o #t) (d #t) (k 0)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "0xdw3nkvkk27nbpwa3h3l7cgszf2ndd7dzh9k2c67g3z2y6y788k") (f (quote (("nightly" "clippy"))))))

(define-public crate-gerber-types-0.2.0 (c (n "gerber-types") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1lqh73k51pm8381hh1nwna3paqnrjcrbf03nc2v7w82vqljxvfn7")))

(define-public crate-gerber-types-0.3.0 (c (n "gerber-types") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1g6q781fmh3n1a1lv3ymyx2bphajiyqp80gkacwzgd6pd6i7ikxf")))

