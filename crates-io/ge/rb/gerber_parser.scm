(define-module (crates-io ge rb gerber_parser) #:use-module (crates-io))

(define-public crate-gerber_parser-0.1.1 (c (n "gerber_parser") (v "0.1.1") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)))) (h "1zsfd2592ijrpwx8gqr30mv09c43x43ags2zhhm7zlia9pw2jp79")))

(define-public crate-gerber_parser-0.1.2 (c (n "gerber_parser") (v "0.1.2") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 2)))) (h "1rbgbsc0f39g5rn6bmyyrghbf2l11pcd7ppjd3q7zgs4p5bakfh5")))

