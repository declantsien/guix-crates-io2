(define-module (crates-io ge rb gerber2svg) #:use-module (crates-io))

(define-public crate-gerber2svg-0.1.0 (c (n "gerber2svg") (v "0.1.0") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "gerber_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "0iv3g8aa61bh917w90vmbp5d6dz8fwsxqf32lp3sqy3zfzyazlp9")))

(define-public crate-gerber2svg-0.2.0 (c (n "gerber2svg") (v "0.2.0") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "gerber_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "0xkwvwjy17mvfm1204k4av5d5091nv2nzhllifpks1z6v9khq6kd")))

(define-public crate-gerber2svg-0.2.1 (c (n "gerber2svg") (v "0.2.1") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "gerber_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "1ydyslv5fllm69njp5r3q7mldirk1sf9y40rw8xi1ad89bjlfv8w") (y #t)))

(define-public crate-gerber2svg-0.2.2 (c (n "gerber2svg") (v "0.2.2") (d (list (d (n "gerber-types") (r "^0.2.0") (d #t) (k 0)) (d (n "gerber_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "simple_logger") (r "^4.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "svg") (r "^0.13.1") (d #t) (k 0)))) (h "1g9xlzvzhcq32jm7w53cjwnsmk62kh1nlby88ns99maivdf2z2pl")))

