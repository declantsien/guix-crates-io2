(define-module (crates-io ge n_ gen_eval_table) #:use-module (crates-io))

(define-public crate-gen_eval_table-0.1.0 (c (n "gen_eval_table") (v "0.1.0") (d (list (d (n "bytepack") (r "^0.4.1") (d #t) (k 0)))) (h "1cp1swhq8dkf1ahvxl46rv4h264j45zm9iy8gwa50xx2qxvjyqn7")))

(define-public crate-gen_eval_table-0.1.1 (c (n "gen_eval_table") (v "0.1.1") (d (list (d (n "read_write") (r "^0.1.1") (d #t) (k 0)))) (h "1cnz39hj76spqh1r05pwjfnfjc7984lifhqcdapqqaab0jmlqsbq")))

(define-public crate-gen_eval_table-0.1.2 (c (n "gen_eval_table") (v "0.1.2") (d (list (d (n "read_write") (r "^0.1.1") (d #t) (k 0)))) (h "18vqrfy0skanl42xqrcnjjri0azlzc9bbh9qbg0fvwhji2sagnmi")))

