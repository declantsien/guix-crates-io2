(define-module (crates-io ge n_ gen_lsp_server) #:use-module (crates-io))

(define-public crate-gen_lsp_server-0.1.0 (c (n "gen_lsp_server") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.51.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "04fs35gh1zqipxng5zzifqvfcy6228c41q75rhdl1fhkl60s0ifn")))

(define-public crate-gen_lsp_server-0.2.0 (c (n "gen_lsp_server") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "lsp-types") (r "^0.57.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "0yfdfh0bai8yzzn097dr798h6arx7vj0zndmjxjpjlyjbqj0qdrd")))

