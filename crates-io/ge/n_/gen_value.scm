(define-module (crates-io ge n_ gen_value) #:use-module (crates-io))

(define-public crate-gen_value-0.1.0 (c (n "gen_value") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ngdy9gbsx38qjy3wa6as2ddbh9r89b5n5mhk7m98483sc5p6sc0") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.2.0 (c (n "gen_value") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fbfvy0m3x4c0wzkbkzylr9flcxd6zxib0cyn1cnsnfinqdj6g36") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.3.0 (c (n "gen_value") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x98qy1bkr1lk1dzb6y35dv8r538bhyrz3gjx0rq8hbfdmj71v7m") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.4.0 (c (n "gen_value") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rswj8d4d48b96n3k3czizca9fpiw49fh4bl9mjkj61dhx1xai5j") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.5.0 (c (n "gen_value") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wlzkpig0h93ncd1190r937nvx03w1z5l19l3fx5zgzdj37c23qw") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.6.0 (c (n "gen_value") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fixasxsria6lyi9fnndvd1fxmpnrgqrlcb6w7226piifn2yi8r0") (f (quote (("std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-gen_value-0.7.0 (c (n "gen_value") (v "0.7.0") (h "0cp5fjhwlw81jqvay3f0lb4f2f2lr9wkd9mp0f1r54lchinkxhvj") (f (quote (("std") ("default" "std") ("alloc")))) (r "1.56.0")))

