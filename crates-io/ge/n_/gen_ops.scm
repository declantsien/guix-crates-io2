(define-module (crates-io ge n_ gen_ops) #:use-module (crates-io))

(define-public crate-gen_ops-0.1.0 (c (n "gen_ops") (v "0.1.0") (h "033szxsgjdy68x8rscy07s7dikb59ac2nlzvz6zad46axwczs1cy")))

(define-public crate-gen_ops-0.1.1 (c (n "gen_ops") (v "0.1.1") (h "16gwhhdj3i92f16hcc4pw1qw50c88n066vqsz412p4q22p7daf08")))

(define-public crate-gen_ops-0.1.2 (c (n "gen_ops") (v "0.1.2") (h "12k49frvh0hj02l7cmb3hivn8yglr861rfbcwnf4lx2779x62vng")))

(define-public crate-gen_ops-0.1.3 (c (n "gen_ops") (v "0.1.3") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0x38saxdyrl5jbbvp3v14plv87ixrqm2x8mbw0ckw9ik8k5qsiqi")))

(define-public crate-gen_ops-0.2.0 (c (n "gen_ops") (v "0.2.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "12q81dc8fwlg6ljkwikpk812caminvvdm1lvak1868d39xzk8h9z")))

(define-public crate-gen_ops-0.3.0 (c (n "gen_ops") (v "0.3.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "0nl255b6a8crsycxfqvrgqajb2lng2nz82z48zahk0g7isnnrig7")))

(define-public crate-gen_ops-0.4.0 (c (n "gen_ops") (v "0.4.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 2)))) (h "02hfbxyz79z284g4l9gdqhw99rn8pgyb0si9babj1102nyfy2k9h")))

