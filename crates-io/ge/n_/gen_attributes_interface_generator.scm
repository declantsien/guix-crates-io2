(define-module (crates-io ge n_ gen_attributes_interface_generator) #:use-module (crates-io))

(define-public crate-gen_attributes_interface_generator-0.1.0 (c (n "gen_attributes_interface_generator") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "derive"))) (d #t) (k 0)))) (h "02s207jd87k5m70cw0h6xah3zrdnscikv6rn35w5c5iva19m78pf")))

