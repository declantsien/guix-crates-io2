(define-module (crates-io ge n_ gen_layouts_sys) #:use-module (crates-io))

(define-public crate-gen_layouts_sys-0.1.0 (c (n "gen_layouts_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 1)) (d (n "quote") (r "^0.6") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "1nsp5dc1cvk4hcfrnlxh2jrbsizgsp2jl0qhr51lbs9pkdwv9p9j")))

(define-public crate-gen_layouts_sys-0.2.0 (c (n "gen_layouts_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 1)) (d (n "quote") (r "^0.6") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "0n6wx4l3s2wfmly511y2nb0c67ri5w9sb3xjn8b2pl49pq3k896r")))

(define-public crate-gen_layouts_sys-0.2.1 (c (n "gen_layouts_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 1)) (d (n "quote") (r "^0.6") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 1)))) (h "0wxvv9j4rh6yrnz9wxwpkh8324kj5b8gg2n3qy73syrz02k1vyjn")))

(define-public crate-gen_layouts_sys-0.3.0 (c (n "gen_layouts_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.46") (o #t) (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (o #t) (d #t) (k 1)) (d (n "quote") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (o #t) (d #t) (k 1)))) (h "0zbkhmwirayyrx0179kp92qxnicxc8s6wj45sm860q72hyc22bx6") (f (quote (("generate" "bindgen" "quote" "syn" "regex" "proc-macro2"))))))

