(define-module (crates-io ge n_ gen_random) #:use-module (crates-io))

(define-public crate-gen_random-0.1.0 (c (n "gen_random") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02j3mx922zr5g97pigk10wi10s49l1ic9ip06xcwczphdi4x3wnc") (y #t)))

(define-public crate-gen_random-0.1.1 (c (n "gen_random") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m0yzyhldxfifxx9njwrnya2sss5ps307384bv2rqp27a2kqj4vl") (y #t)))

(define-public crate-gen_random-0.1.2 (c (n "gen_random") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "050md2r10zdvak7zrq7p2k20qs20wz3gmvp1s78c962yfz4kc8cm")))

(define-public crate-gen_random-0.1.3 (c (n "gen_random") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1py3x4d856b8rc4w22c29y31sag8z0rx051y70qpzdj1gzj9hkdf")))

(define-public crate-gen_random-0.1.4 (c (n "gen_random") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hkp6d2wl8wxvd2azi81l25kssib3bkfl5m68l113qx0xd3kqfsq")))

