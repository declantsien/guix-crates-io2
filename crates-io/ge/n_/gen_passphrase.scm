(define-module (crates-io ge n_ gen_passphrase) #:use-module (crates-io))

(define-public crate-gen_passphrase-0.1.0 (c (n "gen_passphrase") (v "0.1.0") (d (list (d (n "nanorand") (r "^0.7") (f (quote ("chacha"))) (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 2)))) (h "1gvy2ib8i24743jixb2ivw7fypqnsixj23cpq9mp4w0hlxlzwhby") (f (quote (("eff_short_2") ("eff_short_1") ("eff_large") ("default")))) (y #t)))

(define-public crate-gen_passphrase-0.1.1 (c (n "gen_passphrase") (v "0.1.1") (d (list (d (n "nanorand") (r "^0.7") (f (quote ("chacha"))) (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 2)))) (h "11w2smzs1izdblqzx3hwwnmp0zxs6jc0mz3lqpjjz8f2zcraw2zj") (f (quote (("eff_short_2") ("eff_short_1") ("eff_large") ("default"))))))

