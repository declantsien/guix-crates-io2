(define-module (crates-io ge n_ gen_prime) #:use-module (crates-io))

(define-public crate-Gen_Prime-0.1.0 (c (n "Gen_Prime") (v "0.1.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1gwjvjfrysbp2srx6gi6nxcj6lpqmr11k5vf4k8yp9c9xri4xyl5")))

(define-public crate-Gen_Prime-0.1.1 (c (n "Gen_Prime") (v "0.1.1") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0dwv8s9chgg60s7n3m1rq9qxga5rwxf5wxxjp2fy0vcy5jk0hm15")))

(define-public crate-Gen_Prime-0.1.2 (c (n "Gen_Prime") (v "0.1.2") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1qgycp7y0554j52bbaiqh461539i95cp62a2fyzg9l0iaayr8k05")))

(define-public crate-Gen_Prime-1.0.0 (c (n "Gen_Prime") (v "1.0.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "01vl659cz4va5c4ar06bp8z2mvfj71brn5gbvjnbqci1ps40d6l0")))

(define-public crate-Gen_Prime-1.1.0 (c (n "Gen_Prime") (v "1.1.0") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "06zyxkw7aqx0b92mg7p78aih0zxw6ijnfljddnr536c1407pbkwm")))

(define-public crate-Gen_Prime-1.1.1 (c (n "Gen_Prime") (v "1.1.1") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "101yymhfya9p8sl4nmk1747h6x6jrdw6rkch3yjwxifxf7nw42hq")))

(define-public crate-Gen_Prime-1.1.2 (c (n "Gen_Prime") (v "1.1.2") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1ayg7n1rlgbydacg2bdpnk9x0lhx11ppi6gz06m1bbkfs813v6da")))

(define-public crate-Gen_Prime-1.1.3 (c (n "Gen_Prime") (v "1.1.3") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0lsmayadbrbmavp8rgjd8dprzgqw642lkjsxc6bvzwlg664mkshb")))

(define-public crate-Gen_Prime-1.1.4 (c (n "Gen_Prime") (v "1.1.4") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0qklvgjcnjwcfli8q36pwj6ncdwb6mcnyx4qg24giixyrpybih9b")))

(define-public crate-Gen_Prime-1.1.5 (c (n "Gen_Prime") (v "1.1.5") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "05pikrvz9pd6qs5x6fyw1w35zixlmbm53bb066w586ap9j5qjlb1")))

(define-public crate-Gen_Prime-1.1.6 (c (n "Gen_Prime") (v "1.1.6") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "10vnvdwqqj9zlxkbvh4xqmlcbw6ymxnjv10prqi36ibzvcv2mvkd")))

(define-public crate-Gen_Prime-1.1.7 (c (n "Gen_Prime") (v "1.1.7") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0zbdc1fdis3rnzr4afjcfays2ffyjya11fbkwia3pxfq7jid2sky")))

(define-public crate-Gen_Prime-1.1.8 (c (n "Gen_Prime") (v "1.1.8") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "14c8zva81immbrgb9djrcxml42v0lp796v0qsdh2bldzymjqyzls")))

(define-public crate-Gen_Prime-1.1.9 (c (n "Gen_Prime") (v "1.1.9") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "1gi4f84x6f1rhc2b342zrxzd7bsxv2riwn62602w9p406p4pb13j")))

