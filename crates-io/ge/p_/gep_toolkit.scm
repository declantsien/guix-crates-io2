(define-module (crates-io ge p_ gep_toolkit) #:use-module (crates-io))

(define-public crate-gep_toolkit-0.1.0 (c (n "gep_toolkit") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "01q9daj6agjj427w4d6dy54m201ni8d6vcrzzjy9jc7k8rnb2pz7")))

(define-public crate-gep_toolkit-0.2.0 (c (n "gep_toolkit") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "0822mz95a4zd1498hpvr8j1b2ys4a0xyq1p9v4c0yqz77q3iqg9y")))

(define-public crate-gep_toolkit-0.2.1 (c (n "gep_toolkit") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r "^0.2.3") (d #t) (k 0)))) (h "01gkv4s8s8mfrcl6zlljpjgizqxl2l5kgnglxi88nzhkqjx1v7j1")))

(define-public crate-gep_toolkit-0.2.2 (c (n "gep_toolkit") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)))) (h "13ymfwp1mpfg5ny869q60v8yc5l1yv2dmdsilfhs4zqgqia05fdw")))

(define-public crate-gep_toolkit-0.2.3 (c (n "gep_toolkit") (v "0.2.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ky6mabl92kfr77dx2npsk8in50h238qzrrxvaddl2rf50g2a35n")))

