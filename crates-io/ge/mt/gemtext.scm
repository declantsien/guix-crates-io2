(define-module (crates-io ge mt gemtext) #:use-module (crates-io))

(define-public crate-gemtext-0.1.0 (c (n "gemtext") (v "0.1.0") (d (list (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "05060fcyyj8n91vvn7i99myhdxpld45wjcw8g346gw2v6g0xi394")))

(define-public crate-gemtext-0.2.0 (c (n "gemtext") (v "0.2.0") (d (list (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "16994mvf7rqppsagp4m0xkp2f3m0276zg02naigm537nsssxvqk3")))

(define-public crate-gemtext-0.2.1 (c (n "gemtext") (v "0.2.1") (d (list (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0nsbbj5j6sk30i7jwbb9anss8a42ggfr625adhw0ni4qnl26mdrf")))

