(define-module (crates-io ge qs geqslib) #:use-module (crates-io))

(define-public crate-geqslib-0.1.0 (c (n "geqslib") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "gmatlib") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "14b1mk99b695zakxpfk7zxm32mqc4jaz87cnylcmzd3s2fvsc6nx")))

(define-public crate-geqslib-0.1.1 (c (n "geqslib") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "gmatlib") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1nssxxwfhphlnxbhzp5f2vrg3kcwz1hxwrq15a4pdd1k3a641l9x")))

(define-public crate-geqslib-0.1.2 (c (n "geqslib") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "gmatlib") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0klkh8z1zrm82ymjnvm2iyk6wxvn55gda3ay5xqk77bfwfcwnws4")))

(define-public crate-geqslib-0.1.3 (c (n "geqslib") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "gmatlib") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1sjhclarzhgzykag486gwwj73l9h7b4w8icyam0s83q5py642ldl")))

