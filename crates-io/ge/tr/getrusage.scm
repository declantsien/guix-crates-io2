(define-module (crates-io ge tr getrusage) #:use-module (crates-io))

(define-public crate-getrusage-0.1.0 (c (n "getrusage") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "0lbvm42dizgvpv3q7iw622w56razgxnr1zia8kglvqpb6yq1z44x")))

