(define-module (crates-io ge tr getrandom-runtime-seeded) #:use-module (crates-io))

(define-public crate-getrandom-runtime-seeded-0.1.0 (c (n "getrandom-runtime-seeded") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (k 0)))) (h "1g21ac0hzw7lrp7phibfqdxhzk1qj7bbqhjfpfm8ksady8zgd7qq")))

(define-public crate-getrandom-runtime-seeded-1.0.0 (c (n "getrandom-runtime-seeded") (v "1.0.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (k 0)))) (h "0acrnpzafacdvfyshqwblmlzngwl2ybvj23rw1wvpnnhlphbk992")))

