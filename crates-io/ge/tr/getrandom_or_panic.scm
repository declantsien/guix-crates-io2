(define-module (crates-io ge tr getrandom_or_panic) #:use-module (crates-io))

(define-public crate-getrandom_or_panic-0.0.1 (c (n "getrandom_or_panic") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (k 0)))) (h "0hpsh0175k07gisy4zfximwprmz55cv3v9jy1cjxiqyr6if2mf1h") (f (quote (("std" "alloc" "getrandom" "rand") ("getrandom" "rand_core/getrandom") ("default" "std" "getrandom") ("alloc" "rand_core/alloc")))) (y #t)))

(define-public crate-getrandom_or_panic-0.0.2 (c (n "getrandom_or_panic") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (k 0)))) (h "12jnqa1xakaagmbn070zm9qgykmm6fz8gz4l6rq0pcyqj69j675v") (f (quote (("std" "alloc" "getrandom" "rand") ("getrandom" "rand_core/getrandom") ("default" "std" "getrandom") ("alloc" "rand_core/alloc"))))))

(define-public crate-getrandom_or_panic-0.0.3 (c (n "getrandom_or_panic") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "1f8h7dvjjkc14akdacmi5jbrvy4a1kjwyc62iml6nqbhb9dh38bf") (f (quote (("std" "alloc" "getrandom" "rand") ("getrandom" "rand_core/getrandom") ("default" "std" "getrandom") ("alloc" "rand_core/alloc"))))))

