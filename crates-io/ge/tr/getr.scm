(define-module (crates-io ge tr getr) #:use-module (crates-io))

(define-public crate-getr-0.1.0 (c (n "getr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0b6r1vrpnmz2fxrslzh3xp2zc1bz7zng3i2w7ycm7xk3d2l9j7n0")))

(define-public crate-getr-0.1.1 (c (n "getr") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "07rkmxgvss6dbfsx11dk14aygy8l4zn4jjxby90slzwb374y864l")))

