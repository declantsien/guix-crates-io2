(define-module (crates-io ge tr getrandom_package) #:use-module (crates-io))

(define-public crate-getrandom_package-0.1.0 (c (n "getrandom_package") (v "0.1.0") (h "0vv0nsb6dj8p8qzchi4rj29zf6wh1zhkbgi5cn1bnd4r5mxnhrpz")))

(define-public crate-getrandom_package-0.1.2 (c (n "getrandom_package") (v "0.1.2") (h "0ra27rami44c6is21d9dr5vgarddc08iasgji4gz6p79lcs2p506") (f (quote (("wasm-bindgen") ("stdweb")))) (y #t)))

(define-public crate-getrandom_package-0.1.20 (c (n "getrandom_package") (v "0.1.20") (h "13wcbz5hxwmc668dfdlhrq5w253v1nbl0hvll4gwav2y4c1hnla5") (f (quote (("wasm-bindgen") ("stdweb"))))))

