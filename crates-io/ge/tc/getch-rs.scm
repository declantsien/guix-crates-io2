(define-module (crates-io ge tc getch-rs) #:use-module (crates-io))

(define-public crate-getch-rs-0.1.0 (c (n "getch-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "11rhpn5l337zlicpzsspcq0by5mmpf68z3fjlx3ygf7rp0g55qp7")))

(define-public crate-getch-rs-0.1.1 (c (n "getch-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12xi1zxr6syls6iq3y9viwxhvzfdiv5rzvmwg4q2h9j0298rvsgf")))

(define-public crate-getch-rs-0.1.2 (c (n "getch-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cg1j7krv4b6n3m2mhxmnq44m7gscsij1kqg4jsdrsdc4igin080")))

(define-public crate-getch-rs-0.1.3 (c (n "getch-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17jspqiiwyy1balkk2nx64f3x6jn78jkirg8j8l0bpdzqr0g885s")))

(define-public crate-getch-rs-0.1.4 (c (n "getch-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sjwyild6q91fyx1gz9g3bqnbkkw84cvw24v8kwpxb8qrd0lc7wp")))

(define-public crate-getch-rs-0.2.0 (c (n "getch-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase" "wincon" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "003fdy01kaxaky3fd3q3r7rvkfnjn9wzv4dcfph6kk1pdfkkhi10")))

