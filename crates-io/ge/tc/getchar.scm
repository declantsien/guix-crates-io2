(define-module (crates-io ge tc getchar) #:use-module (crates-io))

(define-public crate-getchar-0.1.0 (c (n "getchar") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0qirz5mjpyajv5vb0czdgivflj6r5vj30984zx7zf8151f6x5g6i") (y #t)))

(define-public crate-getchar-0.1.1 (c (n "getchar") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1rf6h7rs2mg0yi97d7dfiqywigk3r2k0q3pn7sqqf5q3gns3za30") (y #t)))

(define-public crate-getchar-0.1.2 (c (n "getchar") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0inmc0f2dwvbr72ba7r7xqcc7yg4q5h2qsvmlyadpihdgghzya18")))

