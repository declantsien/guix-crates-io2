(define-module (crates-io ge tc getch) #:use-module (crates-io))

(define-public crate-getch-0.1.0 (c (n "getch") (v "0.1.0") (d (list (d (n "termios") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0ihk3npa3l4snq23qkm58nc5kjvq4rg43c8ms9gcgx6ax85bsg59")))

(define-public crate-getch-0.1.1 (c (n "getch") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "13bvcpzk5zm41k0bnld6zdvjari0snvpjs25mix3znxma0zvvy7x")))

(define-public crate-getch-0.2.0 (c (n "getch") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1avnsxcm8pc9b6yy6q3faibhyhvg7nv1arma6gxblc1gaj24imx5")))

(define-public crate-getch-0.2.1 (c (n "getch") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "00in8q95qi8a5q3zn2zcaqp5avj79f5myd2a4zfdy2m24ycvbc5v")))

(define-public crate-getch-0.3.0 (c (n "getch") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (t "cfg(unix)") (k 0)))) (h "131pci5b9ymlifagbjdpjvhr0m8bbfmkzqv7ldadmh0w732c9784")))

(define-public crate-getch-0.3.1 (c (n "getch") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (t "cfg(unix)") (k 0)))) (h "1a0v3ck9w65zcgg055laiyyvkb6lxaph1h3zvw6pgq99bcnhx68k")))

