(define-module (crates-io ge tc getc) #:use-module (crates-io))

(define-public crate-getc-0.1.1 (c (n "getc") (v "0.1.1") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.5") (d #t) (k 0)))) (h "0i2h7mzpqfbv0frxyg690v3jpch6v7gw0bx4d295j8sklw5jrhgp")))

(define-public crate-getc-0.1.2 (c (n "getc") (v "0.1.2") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.5") (d #t) (k 0)))) (h "1nx3jishd8whzfq1n9pd6hp0xignpjzf26c3k08hjv0mmisfh4b1")))

(define-public crate-getc-0.1.3 (c (n "getc") (v "0.1.3") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.5") (d #t) (k 0)))) (h "0bk8rl5dwr65jxaq5qaksg11r902kcf7gdfygwb3lk2kjfvaa0b3")))

(define-public crate-getc-0.1.4 (c (n "getc") (v "0.1.4") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.5") (d #t) (k 0)))) (h "16qsfgmwsfxr1lsj5akfwxa3vyc5n740ag88h47yjxk6by123gwd")))

(define-public crate-getc-0.1.5 (c (n "getc") (v "0.1.5") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.13") (f (quote ("mut"))) (d #t) (k 0)))) (h "10shqp03cx8rmsmjajq0lwqhf33smb7rg7hz61vjn46nl50rhp31")))

(define-public crate-getc-0.1.6 (c (n "getc") (v "0.1.6") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "tp") (r "^0.1.14") (f (quote ("mut"))) (d #t) (k 0)))) (h "0zsqibihbswmk315gxsxqs82syv1dzwlmy3vxrjhzqprfkxrsxpy")))

