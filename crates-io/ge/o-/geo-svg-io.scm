(define-module (crates-io ge o- geo-svg-io) #:use-module (crates-io))

(define-public crate-geo-svg-io-0.1.0 (c (n "geo-svg-io") (v "0.1.0") (d (list (d (n "flo_curves") (r "^0.3.1") (d #t) (k 0)) (d (n "geo-booleanop") (r "^0.2.1") (d #t) (k 0)) (d (n "geo-normalized") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.2") (d #t) (k 0)))) (h "061pb86iygv01jnjm1kscr5m2zfll3jjzmb22k3h5szmg342snz5")))

(define-public crate-geo-svg-io-0.1.1 (c (n "geo-svg-io") (v "0.1.1") (d (list (d (n "flo_curves") (r "^0.3.1") (d #t) (k 0)) (d (n "geo-booleanop") (r "^0.2.1") (d #t) (k 0)) (d (n "geo-normalized") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.2") (d #t) (k 0)))) (h "0lbcrc446h787iy8z7byi43sb94zwvwln3d8mv6f4mfd5szzbpib")))

