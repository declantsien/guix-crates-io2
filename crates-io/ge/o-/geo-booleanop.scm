(define-module (crates-io ge o- geo-booleanop) #:use-module (crates-io))

(define-public crate-geo-booleanop-0.1.3 (c (n "geo-booleanop") (v "0.1.3") (d (list (d (n "geo-types") (r "^0.2.0") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "14wingym79xhaml8j2lcydln6rzj0nh981cp5g4sm4zzn6af62ij")))

(define-public crate-geo-booleanop-0.1.4 (c (n "geo-booleanop") (v "0.1.4") (d (list (d (n "geo-types") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0rq7a8wm6g1qx6kxwspcp3lblw5jyss7hn17y2489kfdblxqapgg")))

(define-public crate-geo-booleanop-0.2.0 (c (n "geo-booleanop") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0wd87hxq7990cf37zl7prq9zlqkqbx3nd5lkbi49143c209h95kh")))

(define-public crate-geo-booleanop-0.2.1 (c (n "geo-booleanop") (v "0.2.1") (d (list (d (n "float_next_after") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "robust") (r "^0.1") (d #t) (k 0)))) (h "0s0i1x19x1k1xwwhbimw0917scq2gqyp732lm6jzcksbqw4ry7x8") (f (quote (("debug-booleanop"))))))

(define-public crate-geo-booleanop-0.2.2 (c (n "geo-booleanop") (v "0.2.2") (d (list (d (n "float_next_after") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "robust") (r "^0.1") (d #t) (k 0)))) (h "0fn05rablwykwrn4z67376jq9jw8xkqnvips5436zzc2vg3c59wp") (f (quote (("debug-booleanop"))))))

(define-public crate-geo-booleanop-0.3.0 (c (n "geo-booleanop") (v "0.3.0") (d (list (d (n "float_next_after") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.5.0") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "robust") (r "^0.1") (d #t) (k 0)))) (h "13swi5qv5k5g9yc52n30sf7s3fv7yywghxbq7pmzshz822v78qgi") (f (quote (("debug-booleanop"))))))

(define-public crate-geo-booleanop-0.3.2 (c (n "geo-booleanop") (v "0.3.2") (d (list (d (n "float_next_after") (r "^0.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.6.0") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "robust") (r "^0.1") (d #t) (k 0)))) (h "0nzlq7ydh1vf13q9a2l714dzfdzbynyl3vly1rhrd4l1gkqqzdnl") (f (quote (("debug-booleanop"))))))

