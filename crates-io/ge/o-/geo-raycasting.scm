(define-module (crates-io ge o- geo-raycasting) #:use-module (crates-io))

(define-public crate-geo-raycasting-0.1.0 (c (n "geo-raycasting") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mqqhry8fk90nyhi82rdn7k4lj2bx6vs6qrfbgrr9pcvypn714dq")))

(define-public crate-geo-raycasting-0.1.1 (c (n "geo-raycasting") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0b82x09js1ac15p3pc0pk1zwblhnb4wdp33bg0cmgq8m375rfmxb")))

(define-public crate-geo-raycasting-0.1.2 (c (n "geo-raycasting") (v "0.1.2") (d (list (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0l61nissldrlkasqf9kjgrz01gj6p9dgk4nlqqc8nfjnsjyryakp")))

(define-public crate-geo-raycasting-0.2.0 (c (n "geo-raycasting") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vqq123g81hrkkzy1r3gxsyccz3gm1y0s7b3bk4xaxgx39v64ss7")))

(define-public crate-geo-raycasting-0.3.0 (c (n "geo-raycasting") (v "0.3.0") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)))) (h "1cw9rnviq9lym12iyibxw81aq28kj17jb497m81mdh11sadzxvxf")))

