(define-module (crates-io ge o- geo-clipper) #:use-module (crates-io))

(define-public crate-geo-clipper-0.1.0 (c (n "geo-clipper") (v "0.1.0") (d (list (d (n "clipper-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)))) (h "0rxi8mbm263qbncjxvm7d5zvmzgw9b72sl46zp2xpybqrfpg6kyl")))

(define-public crate-geo-clipper-0.2.0 (c (n "geo-clipper") (v "0.2.0") (d (list (d (n "clipper-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)))) (h "1yfmrmxgg88sq6kis30562657h6his04csbd5v414v90z36n51z4")))

(define-public crate-geo-clipper-0.3.0 (c (n "geo-clipper") (v "0.3.0") (d (list (d (n "clipper-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)))) (h "0sbhf9x21ssp0wvhy884sxqwvkm783p7vzlqc3prxyrssh74li49")))

(define-public crate-geo-clipper-0.4.0 (c (n "geo-clipper") (v "0.4.0") (d (list (d (n "clipper-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.6.0") (d #t) (k 0)))) (h "18sc417bwg57dlblm42m1zqcqpb5yzp8xxqf1wfqwj66v4r83vaj")))

(define-public crate-geo-clipper-0.5.0 (c (n "geo-clipper") (v "0.5.0") (d (list (d (n "clipper-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)))) (h "1k9kfmn9bl6gnpq450m8vdxp64cwpjllgp0lkf64vh0fs1mkn99i")))

(define-public crate-geo-clipper-0.6.0 (c (n "geo-clipper") (v "0.6.0") (d (list (d (n "clipper-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)))) (h "0yqjxp8paimv8fq7hfm6ka4fv0cq6x8i34k7c5nv5ajj8pn050m3")))

(define-public crate-geo-clipper-0.7.0 (c (n "geo-clipper") (v "0.7.0") (d (list (d (n "clipper-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)))) (h "06j45k7c0v19ck32fv07m28zpm8icvmrhz8ssxhhjrrzprixpprd")))

(define-public crate-geo-clipper-0.7.1 (c (n "geo-clipper") (v "0.7.1") (d (list (d (n "clipper-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)))) (h "0wsrfjiqvhkk8rf8qhjp82acm9vngg026ny4h226an28vrx1p98b")))

(define-public crate-geo-clipper-0.7.2 (c (n "geo-clipper") (v "0.7.2") (d (list (d (n "clipper-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.3") (d #t) (k 0)))) (h "0iywyn7qw4s4ijlz2gj4ylxibm94bnm5xk50pbg5vmyx29r3lxls")))

(define-public crate-geo-clipper-0.7.3 (c (n "geo-clipper") (v "0.7.3") (d (list (d (n "clipper-sys") (r "^0.7.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.3") (d #t) (k 0)))) (h "0p083h88wl095yavqcw15db6nz0l0sg7ycaignz0if8ag0labaks")))

(define-public crate-geo-clipper-0.8.0 (c (n "geo-clipper") (v "0.8.0") (d (list (d (n "clipper-sys") (r "^0.7.2") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.8") (d #t) (k 0)))) (h "1aqbhb1bagzm1xd9i3nbjagi1yjnvc00shmvqg8smvlbpw7wm060")))

