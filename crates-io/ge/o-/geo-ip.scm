(define-module (crates-io ge o- geo-ip) #:use-module (crates-io))

(define-public crate-geo-ip-0.1.0 (c (n "geo-ip") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0p4cxvrlhj3dygslnj16h58lqg58xrc6jx156rkfhf32cvrjqwfw")))

