(define-module (crates-io ge o- geo-postgis) #:use-module (crates-io))

(define-public crate-geo-postgis-0.1.0 (c (n "geo-postgis") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "postgis") (r "^0.7") (d #t) (k 0)))) (h "05y0w1w819cl2lc9z4284bnwv6bqlj3gbby50rsxynwdiqbmh6qc")))

(define-public crate-geo-postgis-0.2.0 (c (n "geo-postgis") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "postgis") (r "^0.7") (d #t) (k 0)))) (h "0766lpblmllcn2blx8h3niw33j9rqip3nsh3ixpg3z9nsxbmmg2b")))

(define-public crate-geo-postgis-0.2.1 (c (n "geo-postgis") (v "0.2.1") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "postgis") (r ">=0.7.0, <0.9.0") (d #t) (k 0)))) (h "1sk60qjai8mlmc7m20mrijk59zfsg8p5wxsl5mrqdy0cvxcli07r")))

(define-public crate-geo-postgis-0.2.2 (c (n "geo-postgis") (v "0.2.2") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "postgis") (r ">=0.7.0, <0.10.0") (d #t) (k 0)))) (h "1f2jl62b38h8ir5skr6kpa1hnlm5v26516z6p28lr7vypnrwizch")))

