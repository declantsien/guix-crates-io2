(define-module (crates-io ge o- geo-rand) #:use-module (crates-io))

(define-public crate-geo-rand-0.1.0 (c (n "geo-rand") (v "0.1.0") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 2)))) (h "0xmj5mi74mx0yn00x0ndbx1436kwiasgchqlr764iqj29mn4sy8g")))

(define-public crate-geo-rand-0.2.0 (c (n "geo-rand") (v "0.2.0") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 2)))) (h "07gnffld438h2lsa50q50il2g219bvkvgcv57i2hmcjmqmipls9c")))

(define-public crate-geo-rand-0.2.1 (c (n "geo-rand") (v "0.2.1") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 2)))) (h "1pm7hpk54yvapga9fq5p6kfv0w8qgwx93p0b8yigbbh8pbpdwnnm")))

(define-public crate-geo-rand-0.2.2 (c (n "geo-rand") (v "0.2.2") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2.0") (d #t) (k 2)))) (h "0xxi6gl4jylxrp7clf7fqbic96brwpcsvgpyqd1946a0driwyi3k")))

(define-public crate-geo-rand-0.3.0 (c (n "geo-rand") (v "0.3.0") (d (list (d (n "geo") (r "^0.17.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 2)))) (h "1ijs102ah79kafr6dyikp0py266qvanw7fka9y0cqr26a56zrfyf")))

