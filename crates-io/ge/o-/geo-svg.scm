(define-module (crates-io ge o- geo-svg) #:use-module (crates-io))

(define-public crate-geo-svg-0.1.0 (c (n "geo-svg") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "0sxbki013b3cmgyr4xkbpc3h2adja6r65d1p5hb2h62xmxkjr65d")))

(define-public crate-geo-svg-0.1.1 (c (n "geo-svg") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "00gwlsn4vibz3dgp8hqjx7rvbqwpmzbr6n3c60w9fq3ym6pdxlvv")))

(define-public crate-geo-svg-0.1.2 (c (n "geo-svg") (v "0.1.2") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "0yj5r0q0v99d2ja46a95g7gipv77g2miii5l4a4i8mpd2qikgfv2")))

(define-public crate-geo-svg-0.2.0 (c (n "geo-svg") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0q7r8hyqg12mcgi7bx1py3zpc6li4ssdm6js62366hld4bzx6732")))

(define-public crate-geo-svg-0.2.1 (c (n "geo-svg") (v "0.2.1") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0rlaipiza7yvxiiz8jq1h16rq45nf7pz52w3lhhqa4jf4wnw59qc")))

(define-public crate-geo-svg-0.2.2 (c (n "geo-svg") (v "0.2.2") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1w234cbqiby6vflm4509ky3wagmk3rp6f79njg39n2sc6pczdmxn")))

(define-public crate-geo-svg-0.2.3 (c (n "geo-svg") (v "0.2.3") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0fykfiqlym4n0c0fyyjvli0xfmbpxk223cvvcaxgac303fsdiwdw")))

(define-public crate-geo-svg-0.3.0 (c (n "geo-svg") (v "0.3.0") (d (list (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "18dgd13dr3ry9a70nln89bww9xd47qlmvp6ibh75gvb3hi1ihvgm")))

(define-public crate-geo-svg-0.4.0 (c (n "geo-svg") (v "0.4.0") (d (list (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1sv4prqzlgdpcghkqng3d6cn1nqi2l5jh2485yl3yggf8knjf3w1")))

(define-public crate-geo-svg-0.5.0 (c (n "geo-svg") (v "0.5.0") (d (list (d (n "geo-types") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1awh1kmn9js1c3cy172nil4lwf2bw1z17gn7m0hxcxqvibz3q9vx")))

(define-public crate-geo-svg-0.6.0 (c (n "geo-svg") (v "0.6.0") (d (list (d (n "geo-types") (r "^0.7.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1hpwxrjvlv5df9lcxplnfqjfx07wjnkswkwj2y5b38sarrinwqk1")))

(define-public crate-geo-svg-0.6.1 (c (n "geo-svg") (v "0.6.1") (d (list (d (n "geo-types") (r "^0.7.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "13s6svmpras02in4955xcfxd6rwjilcay4vml90v4khhimwrxry5")))

(define-public crate-geo-svg-0.6.2 (c (n "geo-svg") (v "0.6.2") (d (list (d (n "geo") (r "^0.27.0") (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0a28flw0snh5azg9b738gs8qik08iy6fpd03ngzxlnpgmdbdi9yz")))

(define-public crate-geo-svg-0.6.3 (c (n "geo-svg") (v "0.6.3") (d (list (d (n "geo") (r "^0.28.0") (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0pfkvxw8bxvl5nk2r75phvyn9fj0k2xq18xav23vrcqwhpf2ip7w")))

