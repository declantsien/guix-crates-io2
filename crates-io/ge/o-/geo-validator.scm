(define-module (crates-io ge o- geo-validator) #:use-module (crates-io))

(define-public crate-geo-validator-0.1.0 (c (n "geo-validator") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1wv46w9qpwk2q4kb3qyz5h2z4kwx0kv1cznkn7nbnvz0x98mcmiw")))

(define-public crate-geo-validator-0.1.1 (c (n "geo-validator") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1a3fkxxgrkccg73pfx0f5gnd1w317aypyfjfy4z6yx9mc2rx6pkx")))

(define-public crate-geo-validator-0.1.2 (c (n "geo-validator") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0506lfms9ynq6rg91vp2jbmcgc6lfykddrqflha5ynsglihgcvi3")))

(define-public crate-geo-validator-0.1.3 (c (n "geo-validator") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "004gg6hr1v5mpy63xmb6nlfb9fk6bl8kf8j9rb8dpmx7mfv2gsq3")))

