(define-module (crates-io ge o- geo-normalized) #:use-module (crates-io))

(define-public crate-geo-normalized-0.1.0 (c (n "geo-normalized") (v "0.1.0") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0711c2zw2pv8gp55baiysd8jmj5nbfk42rhg81ar69f9fsw5hdh3")))

(define-public crate-geo-normalized-0.1.1 (c (n "geo-normalized") (v "0.1.1") (d (list (d (n "geo") (r "^0.12.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0fd963201ld2imrkgy6ikdri2wjw87dzdwbzqmvf8qqsx8qmjkdc")))

