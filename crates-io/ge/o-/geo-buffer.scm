(define-module (crates-io ge o- geo-buffer) #:use-module (crates-io))

(define-public crate-geo-buffer-0.1.0 (c (n "geo-buffer") (v "0.1.0") (d (list (d (n "geo") (r "^0.24.1") (d #t) (k 0)) (d (n "geo-svg") (r "^0.5.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)) (d (n "wkt") (r "^0.10.3") (d #t) (k 2)))) (h "07g63bc1nqymrpjyhf5lk695g0zlvngm8x4hazi1p7901cq7s6fn")))

(define-public crate-geo-buffer-0.1.1 (c (n "geo-buffer") (v "0.1.1") (d (list (d (n "geo") (r "^0.24.1") (d #t) (k 0)) (d (n "geo-svg") (r "^0.5.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)) (d (n "wkt") (r "^0.10.3") (d #t) (k 2)))) (h "0093x7zdfw81rmyaz75zp6xxz71w281sylgrav4r5phmd6wf7jqj")))

(define-public crate-geo-buffer-0.2.0 (c (n "geo-buffer") (v "0.2.0") (d (list (d (n "geo") (r "^0.24.1") (d #t) (k 0)) (d (n "geo-svg") (r "^0.5.0") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.9") (d #t) (k 0)) (d (n "wkt") (r "^0.10.3") (d #t) (k 2)))) (h "0c610k78zzng5d2bj34yw4hx5k4p9g4c1sv5a2qb1w7j7lvz0yr6")))

