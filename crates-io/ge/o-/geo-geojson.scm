(define-module (crates-io ge o- geo-geojson) #:use-module (crates-io))

(define-public crate-geo-geojson-0.1.0 (c (n "geo-geojson") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "geojson") (r "^0.16.0") (f (quote ("geo-types"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "once_cell") (r "^0.2.1") (d #t) (k 2)))) (h "05vkd5q2cx0y5ijzgnx6181vcxd0dsa7x6dyqa54wfiz27brgr1a") (y #t)))

