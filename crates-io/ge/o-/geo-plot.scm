(define-module (crates-io ge o- geo-plot) #:use-module (crates-io))

(define-public crate-geo-plot-0.1.0 (c (n "geo-plot") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.33") (d #t) (k 0)))) (h "046v6himxc044v96kv16cr9n49byl9dv3rwxms3iyijj8cyjvra8")))

(define-public crate-geo-plot-0.1.1 (c (n "geo-plot") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.33") (d #t) (k 0)))) (h "143c8z9d51px14gd6a3bql1hbmp8w2y6vsxp06dbm75j7qjs4nbf")))

(define-public crate-geo-plot-0.1.2 (c (n "geo-plot") (v "0.1.2") (d (list (d (n "geo-types") (r "^0.4.3") (d #t) (k 0)) (d (n "gnuplot") (r "^0.0.33") (d #t) (k 0)))) (h "0f4dmzh4aw0ydp253fqqxhrnvk7r048y788jlj4wqyp22sg62g88")))

