(define-module (crates-io ge o- geo-validity-check) #:use-module (crates-io))

(define-public crate-geo-validity-check-0.1.0 (c (n "geo-validity-check") (v "0.1.0") (d (list (d (n "float_next_after") (r "^1.0.0") (d #t) (k 0)) (d (n "geo") (r "^0.24") (d #t) (k 0)) (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "robust") (r "^1.0.0") (d #t) (k 0)) (d (n "geos") (r "^8.2.0") (f (quote ("geo"))) (d #t) (k 2)))) (h "1fkhv9dg7zqqvvvcsgarbaymh8ls5h6win6fj353k5q6ylh278pg")))

