(define-module (crates-io ge o- geo-nd) #:use-module (crates-io))

(define-public crate-geo-nd-0.1.0 (c (n "geo-nd") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "0xzs8lp6jrcxzgv0dqdyxh4c1lyfk9v8340lmnvyf82w63kpxmfg")))

(define-public crate-geo-nd-0.1.1 (c (n "geo-nd") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "0dnpfhr7fdf8j9fkz5j5fx9fn4d441abw89q4f056bycj1vnpnk3") (y #t)))

(define-public crate-geo-nd-0.1.2 (c (n "geo-nd") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "08h06y320p4fys2zx93hzl491jqwc725k5gjlnrb4102f5lqmn8q") (y #t)))

(define-public crate-geo-nd-0.1.3 (c (n "geo-nd") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1iwi4q5r7f3npq2xx8v3iyxz789jf8ma2sjszpyxn22p8aqmcxz3") (y #t)))

(define-public crate-geo-nd-0.1.4 (c (n "geo-nd") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1aa1by36lymmnfr2hj8g8alkn71cq37j7384d3gii3klyw77lbkh")))

(define-public crate-geo-nd-0.1.5 (c (n "geo-nd") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1f79r4nr95b000rkb737iq0pdjspa5kglbvhnvdksc3c4slmlzv7")))

(define-public crate-geo-nd-0.5.0 (c (n "geo-nd") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nc5iczpalq7z3cg79mp0d69pr0k1ywisp5acybvdzgjqibggh43")))

