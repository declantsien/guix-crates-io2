(define-module (crates-io ge o- geo-wkt-writer) #:use-module (crates-io))

(define-public crate-geo-wkt-writer-0.1.0 (c (n "geo-wkt-writer") (v "0.1.0") (d (list (d (n "geo-normalized") (r "^0.1.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1q29vagnr57rg65nblz8y8hg7xy9k26rhbzl45ma61g889lqpc4d")))

(define-public crate-geo-wkt-writer-0.1.1 (c (n "geo-wkt-writer") (v "0.1.1") (d (list (d (n "geo") (r "^0.12") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1ar9cgqizs78pbjd7hxn13vyylyn7vyb1q4mshdpy0x71a3nfg44")))

