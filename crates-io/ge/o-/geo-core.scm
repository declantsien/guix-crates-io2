(define-module (crates-io ge o- geo-core) #:use-module (crates-io))

(define-public crate-geo-core-0.1.0 (c (n "geo-core") (v "0.1.0") (h "1w04ailxrpr0bn0zjssk6nbb0z4j0x4airnxm2vjz8vc2sk00bmk")))

(define-public crate-geo-core-0.1.1 (c (n "geo-core") (v "0.1.1") (h "1nl5xx9h2vlxab8cqaxhn815clkxd3vjlkxjwcjy9pab2vjbwdf1")))

(define-public crate-geo-core-0.1.2 (c (n "geo-core") (v "0.1.2") (h "11r6czg981jvszd3kc2vk51x5x5sd7xi0gi8damrn8ap4bb67pvn")))

(define-public crate-geo-core-0.1.3 (c (n "geo-core") (v "0.1.3") (h "0kzyahn9hvc3239m7nxmlzqrdxlfng3k5kxs10b4nwal4wrgjsbf")))

(define-public crate-geo-core-0.1.4 (c (n "geo-core") (v "0.1.4") (h "1fvlkxh09k542yqbwxrs805aqa2y3czg1j6zf95qpipm4lb8124a")))

