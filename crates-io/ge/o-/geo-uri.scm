(define-module (crates-io ge o- geo-uri) #:use-module (crates-io))

(define-public crate-geo-uri-0.1.0 (c (n "geo-uri") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1izkcj98rm5w15rypwzhaq7dx1whj8vim0rqisywbjdrld9mcwl3")))

(define-public crate-geo-uri-0.1.1 (c (n "geo-uri") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1jjmjaqsyaxcba5zjg5xkyvj5ggplhh96d7hkbkxxmmn2s5i7j4p")))

(define-public crate-geo-uri-0.2.0 (c (n "geo-uri") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.145") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "1fa92ba1nsigmjvfar5vqv9112k732lxf31qp5v7dxnfz6q2hnx7") (s 2) (e (quote (("url" "dep:url") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-geo-uri-0.2.1 (c (n "geo-uri") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.145") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "0wczv2bdkaidafrsqhy467rf4cg2p1fx1hk6nfss7d4w0wm83f56") (s 2) (e (quote (("url" "dep:url") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-geo-uri-0.2.2 (c (n "geo-uri") (v "0.2.2") (d (list (d (n "derive_builder") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.145") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (o #t) (d #t) (k 0)))) (h "1klragbx8xqx9cairhgcc97ypyf62v41rhaq83zfxmvjrrvyndds") (s 2) (e (quote (("url" "dep:url") ("serde" "dep:serde")))) (r "1.60.0")))

