(define-module (crates-io ge o- geo-quadkey-rs) #:use-module (crates-io))

(define-public crate-geo-quadkey-rs-0.1.0 (c (n "geo-quadkey-rs") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.0") (d #t) (k 0)))) (h "1bd4zh17k41z6qcw1jnqc89pnskz8fvaap5dbi7ywp0n6p5sib63")))

(define-public crate-geo-quadkey-rs-0.1.1 (c (n "geo-quadkey-rs") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.0") (d #t) (k 2)))) (h "1wbvbrqfjq63bpysn61w3rmiwczzzqpzhhnykdla0f5a5y0dqm6q")))

