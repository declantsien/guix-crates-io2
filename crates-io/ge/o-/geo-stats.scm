(define-module (crates-io ge o- geo-stats) #:use-module (crates-io))

(define-public crate-geo-stats-0.1.0 (c (n "geo-stats") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "geo") (r "^0.23.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.7") (d #t) (k 0)) (d (n "geo-weights") (r "^0.1.0") (d #t) (k 0)) (d (n "geojson") (r "^0.24.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.2") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0v9fas59lkir5d1b4n43345d9xfd2mm5vyv5vrr0sydrmfcw8yqn")))

