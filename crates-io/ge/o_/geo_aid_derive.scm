(define-module (crates-io ge o_ geo_aid_derive) #:use-module (crates-io))

(define-public crate-geo_aid_derive-0.1.0 (c (n "geo_aid_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "188x69m9g8acnd22abpvakz9kbkz0qxcbq645pjl1m3nrn7k5gv4")))

(define-public crate-geo_aid_derive-0.1.1 (c (n "geo_aid_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1kryz6bk6g2j9zhkh0qr1bmx6wlz2yxfi9xk224d240hhwhz3bkh")))

(define-public crate-geo_aid_derive-0.1.2 (c (n "geo_aid_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1g97x21gsyfvh7j0rxcpnxmwdn6lxzlxzv2rm6vx8b3d57sm96hx")))

(define-public crate-geo_aid_derive-0.1.3 (c (n "geo_aid_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0z10i02qfplxqa4bkqvmnmsdwq7dz65mk806gakhlwvscx2m6wmn")))

(define-public crate-geo_aid_derive-0.1.4 (c (n "geo_aid_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0swhaccalqgajsxv54pk1d3q82baj8h7nlpvrj1g9h6jqiyj0cgp")))

