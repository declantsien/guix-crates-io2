(define-module (crates-io ge o_ geo_filters) #:use-module (crates-io))

(define-public crate-geo_filters-0.0.1 (c (n "geo_filters") (v "0.0.1") (h "0nscb54qpcj8gxh6cqyvnf5v61n05jfc7dh2b9fqgi2amgfnfn4g")))

(define-public crate-geo_filters-0.1.0 (c (n "geo_filters") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hyperloglogplus") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "11vbkqndsy6fqbglk2c3yn92zfyc1nbjv7dp596826apxf5pd44n") (f (quote (("default")))) (s 2) (e (quote (("evaluation" "dep:clap" "dep:hyperloglogplus" "dep:rand" "dep:rayon" "dep:regex"))))))

