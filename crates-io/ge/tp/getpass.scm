(define-module (crates-io ge tp getpass) #:use-module (crates-io))

(define-public crate-getpass-0.0.1 (c (n "getpass") (v "0.0.1") (h "1pvzfxy9fh2jz4q76yy6linmxh84a7qiq8rbay7mqanfcp57k8rx")))

(define-public crate-getpass-0.0.2 (c (n "getpass") (v "0.0.2") (h "0fx4gxr5pz87dwrz88xbpmvllx4mlwdi4pyysr1pjvddjpadws7k")))

