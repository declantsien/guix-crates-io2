(define-module (crates-io ge tp getpdb) #:use-module (crates-io))

(define-public crate-GetPDB-0.1.0 (c (n "GetPDB") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1gw0r54fpgjk07anv5jc2f453fh0ibqax1nd7cg0s36iqcrd8nvr")))

(define-public crate-GetPDB-1.0.0 (c (n "GetPDB") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0wbg1z3b717zn59v1j6x49p05xfkiaghfipvi55v8vgqpc8zgavx")))

(define-public crate-GetPDB-1.0.1 (c (n "GetPDB") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1ljj1jix005i8f3pphz8jgb4p1p558fm210brwg7crdxnyl9cyrr")))

