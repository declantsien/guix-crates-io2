(define-module (crates-io ge ng geng-asset-derive) #:use-module (crates-io))

(define-public crate-geng-asset-derive-0.15.0 (c (n "geng-asset-derive") (v "0.15.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y57iigdkl22bmsjfy1advzddh4kd3k1lz1c2pgnb5b2fpaxwdbv")))

(define-public crate-geng-asset-derive-0.16.0 (c (n "geng-asset-derive") (v "0.16.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11al731vgz7hpvj3hkgwinck5yg3iwvxqnib8mhmrqdjr86n7i2g")))

(define-public crate-geng-asset-derive-0.17.0 (c (n "geng-asset-derive") (v "0.17.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "036sv4m4nsz0jvxccllifsjhdjqlnlfyl62r03m71z83sgch68c3")))

(define-public crate-geng-asset-derive-0.17.1 (c (n "geng-asset-derive") (v "0.17.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0hbd15mhbcdgmhlwjfgnxfh7kmfh5wpfaah2x1hxkzgwqqgyp5k4")))

(define-public crate-geng-asset-derive-0.17.2 (c (n "geng-asset-derive") (v "0.17.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16aav0cdfnpdq2jcnwb557hjymvljag630fpry90mzajj52rcy7g")))

