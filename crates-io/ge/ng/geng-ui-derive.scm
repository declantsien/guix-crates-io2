(define-module (crates-io ge ng geng-ui-derive) #:use-module (crates-io))

(define-public crate-geng-ui-derive-0.1.0 (c (n "geng-ui-derive") (v "0.1.0") (d (list (d (n "batbox") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "184k6ga8kphh88nnagrlxjbr71xwfzy2wxamil2yxzlkhd3f3mn6")))

(define-public crate-geng-ui-derive-0.2.0 (c (n "geng-ui-derive") (v "0.2.0") (d (list (d (n "batbox") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04qd8lscam9v5p39l462qkn86kwgq40sc5300fvn0zd7q8p6man4")))

(define-public crate-geng-ui-derive-0.3.0 (c (n "geng-ui-derive") (v "0.3.0") (d (list (d (n "batbox") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "04b11b08vng7x80w1gapdqz2z1y24578pxqlb7mxfchz6fk7if0s")))

(define-public crate-geng-ui-derive-0.4.0 (c (n "geng-ui-derive") (v "0.4.0") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0raq9g3486b0h25pig9nrq3lywvpl629wz0xxi0x2k472k2rkjjl")))

(define-public crate-geng-ui-derive-0.5.0 (c (n "geng-ui-derive") (v "0.5.0") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15dah3c4mvzz6jslh7mv4m77wmvm9w8yi8x15fd4mj2hs1vx0akq")))

(define-public crate-geng-ui-derive-0.6.0-alpha.0 (c (n "geng-ui-derive") (v "0.6.0-alpha.0") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08i7nqwyilck775yraarffsskpq9k5n3fcwcngwjps6w5qmwfb4y")))

(define-public crate-geng-ui-derive-0.6.0-alpha.1 (c (n "geng-ui-derive") (v "0.6.0-alpha.1") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "126iry7n7hpgkmd7p8bs7l29r28a28li9zc6wsib7b98hlhfkyw6")))

