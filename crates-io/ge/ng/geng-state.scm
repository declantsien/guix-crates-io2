(define-module (crates-io ge ng geng-state) #:use-module (crates-io))

(define-public crate-geng-state-0.15.0 (c (n "geng-state") (v "0.15.0") (d (list (d (n "geng-ui") (r "^0.15") (d #t) (k 0)) (d (n "geng-window") (r "^0.15") (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "07fr7rgg37i72bbcqyv4q5qly4n25zchjzx6pxcc4iig629nlsjq")))

(define-public crate-geng-state-0.16.0 (c (n "geng-state") (v "0.16.0") (d (list (d (n "geng-ui") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-window") (r "^0.16.0") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "1cwb390r0w7h2zswr0yff52kyjy6marcnh9xj73c3w8qfvgdwi5x")))

(define-public crate-geng-state-0.17.0 (c (n "geng-state") (v "0.17.0") (d (list (d (n "geng-ui") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0p03kkl45n6z22yz0s199qq7ynsrk3mxm9j1gszpx9w9kgb8891j")))

(define-public crate-geng-state-0.17.2 (c (n "geng-state") (v "0.17.2") (d (list (d (n "geng-ui") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "04c8vmp9303nxg7yj19aywz4slfyynqd35hrh6qnhk7aqa5riw7k")))

