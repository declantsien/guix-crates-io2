(define-module (crates-io ge ng geng-ecs) #:use-module (crates-io))

(define-public crate-geng-ecs-0.8.0 (c (n "geng-ecs") (v "0.8.0") (d (list (d (n "geng-ecs-derive") (r "^0.8.0") (d #t) (k 0)))) (h "1zr584xvx5wlsyf6mcgyvhc6jmfb1i1mhxpvxgzcxl96gz0ybs6f")))

(define-public crate-geng-ecs-0.9.0 (c (n "geng-ecs") (v "0.9.0") (d (list (d (n "geng-ecs-derive") (r "^0.9.0") (d #t) (k 0)))) (h "0ggbzk34zd078xzna1mr2ps6sqiqkbg107nrqf88r10gjdcsv41g")))

(define-public crate-geng-ecs-0.11.0 (c (n "geng-ecs") (v "0.11.0") (d (list (d (n "geng-ecs-derive") (r "^0.11.0") (d #t) (k 0)))) (h "0ibi8svrxnay7cgr31nw3abmy9aih9mlxfjg3v5ql0yaywmkl1jq")))

