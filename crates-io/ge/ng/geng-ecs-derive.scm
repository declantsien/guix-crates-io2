(define-module (crates-io ge ng geng-ecs-derive) #:use-module (crates-io))

(define-public crate-geng-ecs-derive-0.8.0 (c (n "geng-ecs-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mr4zir56wqk465p6zw9w00wgl5n19b1pf1sr2zfb4clh2cis2ha")))

(define-public crate-geng-ecs-derive-0.9.0 (c (n "geng-ecs-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1pcyl0ydmmcwparcw9wzwaxrmaayl32vgpv6gq74163g7dq9qzkg")))

(define-public crate-geng-ecs-derive-0.11.0 (c (n "geng-ecs-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0970fnbx4dkdn94hay12hsp098x4zvfjbd3zmpcwcbpgbcrklgl7")))

