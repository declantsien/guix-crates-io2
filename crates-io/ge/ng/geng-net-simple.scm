(define-module (crates-io ge ng geng-net-simple) #:use-module (crates-io))

(define-public crate-geng-net-simple-0.15.0 (c (n "geng-net-simple") (v "0.15.0") (d (list (d (n "batbox-cli") (r "^0.15") (d #t) (k 0)) (d (n "geng") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1mpwia42696j7qqk28vak0i96r04dypfvz4ivhcki4kqw6ccgymg")))

(define-public crate-geng-net-simple-0.16.0 (c (n "geng-net-simple") (v "0.16.0") (d (list (d (n "batbox-cli") (r "^0.16") (d #t) (k 0)) (d (n "geng") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0q42zd0pkhn5awi81y3gkf50wnj8m7gvgpnnhb8jdhgf2gcgx35x")))

