(define-module (crates-io ge ng geng-egui) #:use-module (crates-io))

(define-public crate-geng-egui-0.9.0 (c (n "geng-egui") (v "0.9.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "geng") (r "^0.10.0") (d #t) (k 0)))) (h "0abrx8x5b25znx4xdl55196qhapmmkgyyy5xrnbs1zp5xa6mwm95")))

(define-public crate-geng-egui-0.11.0 (c (n "geng-egui") (v "0.11.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "geng") (r "^0.11.0") (d #t) (k 0)))) (h "0p1x02mvmyqxz9fpvzk29s8n8idg9y15sb9145n0l79sj2570177")))

(define-public crate-geng-egui-0.12.0 (c (n "geng-egui") (v "0.12.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "geng") (r "^0.12.0") (d #t) (k 0)))) (h "141sa1hww44xn5habggsz4jfxkbljcb2dzfr6czgfw9d3hs56dfa")))

