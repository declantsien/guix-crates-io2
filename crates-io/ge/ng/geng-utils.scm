(define-module (crates-io ge ng geng-utils) #:use-module (crates-io))

(define-public crate-geng-utils-0.1.0 (c (n "geng-utils") (v "0.1.0") (d (list (d (n "geng") (r "^0.15") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wangx3ywm6r7xglr9rglsq3zwd35rscarr5gsrq2zaxz9mx2kx3")))

(define-public crate-geng-utils-0.2.0 (c (n "geng-utils") (v "0.2.0") (d (list (d (n "geng") (r "^0.16") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "163lhhv3b04n7zlgvj0igybxw9l50jj83158m927bgqsr17d05gg")))

(define-public crate-geng-utils-0.3.0 (c (n "geng-utils") (v "0.3.0") (d (list (d (n "geng") (r ">=0.15") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nk8783hz5vd2qcp8l5fg1yb3hm65f9mja7m9pz2qca4zb88q12l")))

