(define-module (crates-io ge ng geng-texture-atlas) #:use-module (crates-io))

(define-public crate-geng-texture-atlas-0.15.0 (c (n "geng-texture-atlas") (v "0.15.0") (d (list (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "1dlw5jcwi3y6x6sx5jx7fscg6xxv17kw7gc5b32np4mad3yzxk90")))

(define-public crate-geng-texture-atlas-0.16.0 (c (n "geng-texture-atlas") (v "0.16.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "13ykfkm2r2gvxz2mg8gfhpxj57dvc0jbnhii22py5ccabjicf4yg")))

(define-public crate-geng-texture-atlas-0.17.0 (c (n "geng-texture-atlas") (v "0.17.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "1f5ji78ac4yxfr0ggfp9cxg5dhd446srphlzl89767m4fcv4idi3")))

(define-public crate-geng-texture-atlas-0.17.2 (c (n "geng-texture-atlas") (v "0.17.2") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "02x7263gcz0jxb0bsl5cmi69im4qgn5h5iixn4xdxlp3zc6sa1mw")))

