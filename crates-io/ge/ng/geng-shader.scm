(define-module (crates-io ge ng geng-shader) #:use-module (crates-io))

(define-public crate-geng-shader-0.15.0 (c (n "geng-shader") (v "0.15.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "1f4f46jp4rcg0ggb0gf978gv72xpsj2ckw1rykbp6g9pvx8p9pcv")))

(define-public crate-geng-shader-0.16.0 (c (n "geng-shader") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "1hs0y7anjgjw8ayi6x3gih8a4h2zzz828fw5hgb7zj3bcaxb72q8")))

(define-public crate-geng-shader-0.17.0 (c (n "geng-shader") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0iicwnik2b91dz0k52p7mdrk4p8cih55fj3q6kp3siy3j2b99gyy")))

(define-public crate-geng-shader-0.17.2 (c (n "geng-shader") (v "0.17.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0wzwc6a6w5yw493xjhmjj7yn2pwkyd2g1kcmnhi2j0iyhlrbphj5")))

