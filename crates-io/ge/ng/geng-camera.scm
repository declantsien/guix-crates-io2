(define-module (crates-io ge ng geng-camera) #:use-module (crates-io))

(define-public crate-geng-camera-0.15.0 (c (n "geng-camera") (v "0.15.0") (d (list (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "batbox-lapp") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "0qak6avyj2yb57is8k34ks08gljzgkfgwwdhxzlga34k09wy7rcw")))

(define-public crate-geng-camera-0.16.0 (c (n "geng-camera") (v "0.16.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-lapp") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "1dvnv0qrj48m8iyq5kyphyrj0w04f8j8grlgqq2g3140388j4dx8")))

(define-public crate-geng-camera-0.17.0 (c (n "geng-camera") (v "0.17.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-lapp") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0zzbpxi459044jvx1y3jha3zbficrbbqilqk5k61zyl8vhdafbcm")))

(define-public crate-geng-camera-0.17.2 (c (n "geng-camera") (v "0.17.2") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-lapp") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "1m0bcficj4jalb9ffvbhpm8b0j0ccbrvr8xgaknbxrgdf2phgymw")))

