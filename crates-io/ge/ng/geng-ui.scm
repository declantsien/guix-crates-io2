(define-module (crates-io ge ng geng-ui) #:use-module (crates-io))

(define-public crate-geng-ui-0.1.0 (c (n "geng-ui") (v "0.1.0") (d (list (d (n "geng-core") (r "^0.1.0") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "07fkfwx4h9c20dnhvc2fdc2nc8hyj9qrf8b7iw0pf4wr46xjw9fd")))

(define-public crate-geng-ui-0.2.0 (c (n "geng-ui") (v "0.2.0") (d (list (d (n "geng-core") (r "^0.2") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "14jvpn2fnv2qfjblbsy3j3rj9k2cdh1cin3f1ryrj32vyy4hnf6s")))

(define-public crate-geng-ui-0.3.0 (c (n "geng-ui") (v "0.3.0") (d (list (d (n "geng-core") (r "^0.3") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1imjjkjxf22ladx5i403kfxfygdsj6fdhk16xyks9v9iy5kh8zdi")))

(define-public crate-geng-ui-0.4.0 (c (n "geng-ui") (v "0.4.0") (d (list (d (n "geng-core") (r "^0.4") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1wr2gqaaqs2sxgyiz3bc4gf8c13nnz9hmw5ndji4x6aiddcfaaqg")))

(define-public crate-geng-ui-0.4.1 (c (n "geng-ui") (v "0.4.1") (d (list (d (n "geng-core") (r "^0.4") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "04f9gvzpdnvvdjviv28j5cnvi1nz5dzp19py9r21rafln4xpj59f")))

(define-public crate-geng-ui-0.5.0 (c (n "geng-ui") (v "0.5.0") (d (list (d (n "geng-core") (r "^0.5") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1nir1p10bzv6jx1d6isxlqilv23jhgifzi4k67gr12jqx8yadlyd")))

(define-public crate-geng-ui-0.6.0-alpha.0 (c (n "geng-ui") (v "0.6.0-alpha.0") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1chyda6im5hzqw74zlvnr20fvnjlnnnpl6nxm7k169kmqgm997w5")))

(define-public crate-geng-ui-0.6.0-alpha.2 (c (n "geng-ui") (v "0.6.0-alpha.2") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "17d23sqfh6pymh1ynqall6hm413hapfjvdvfqdjblng61n0i3gxp")))

(define-public crate-geng-ui-0.6.0-alpha.3 (c (n "geng-ui") (v "0.6.0-alpha.3") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui-derive") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "12z6v8m3770ji1mdq467mv9scqkcw8w5jkpy7grcda5qyl8d8sim")))

(define-public crate-geng-ui-0.15.0 (c (n "geng-ui") (v "0.15.0") (d (list (d (n "batbox-cmp") (r "^0.15") (d #t) (k 0)) (d (n "batbox-color") (r "^0.15") (d #t) (k 0)) (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "batbox-tuple-macros") (r "^0.15") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "geng-camera") (r "^0.15") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.15") (d #t) (k 0)) (d (n "geng-font") (r "^0.15") (d #t) (k 0)) (d (n "geng-window") (r "^0.15") (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "08vaiqygvnljnzf05dcjg5fcjhypgd0iy9rqjgm06dwlw2cm2kkb")))

(define-public crate-geng-ui-0.16.0 (c (n "geng-ui") (v "0.16.0") (d (list (d (n "batbox-cmp") (r "^0.16") (d #t) (k 0)) (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-tuple-macros") (r "^0.16") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "geng-camera") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-font") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-window") (r "^0.16.0") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "08xbg8yj3kg72n5g1xsa9pl5mamgks5kgjbsaiigyy96p5kxsp13")))

(define-public crate-geng-ui-0.17.0 (c (n "geng-ui") (v "0.17.0") (d (list (d (n "batbox-cmp") (r "^0.16") (d #t) (k 0)) (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-tuple-macros") (r "^0.16") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "geng-camera") (r "^0.17") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.17") (d #t) (k 0)) (d (n "geng-font") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "194scv1b28zrlvzshx9mi0qwbd1l5f4cvj985f0wgxq7w2h3qi6i")))

(define-public crate-geng-ui-0.17.2 (c (n "geng-ui") (v "0.17.2") (d (list (d (n "batbox-cmp") (r "^0.16") (d #t) (k 0)) (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-tuple-macros") (r "^0.16") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "geng-camera") (r "^0.17") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.17") (d #t) (k 0)) (d (n "geng-font") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0zgrj2cwpc03lw9i494jj2bcjvfp4bmbacav6p30xck8kfccnwm2")))

