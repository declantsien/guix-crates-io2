(define-module (crates-io ge ng geng-debug_overlay) #:use-module (crates-io))

(define-public crate-geng-debug_overlay-0.1.0 (c (n "geng-debug_overlay") (v "0.1.0") (d (list (d (n "geng-core") (r "^0.1.0") (d #t) (k 0)) (d (n "geng-ui") (r "^0.1.0") (d #t) (k 0)))) (h "0ywgyj1p0g9ylh4j5n0cz69sv46vyxkw7gcrdpyz1j9sxm2v90kb")))

(define-public crate-geng-debug_overlay-0.2.0 (c (n "geng-debug_overlay") (v "0.2.0") (d (list (d (n "geng-core") (r "^0.2") (d #t) (k 0)) (d (n "geng-ui") (r "^0.2") (d #t) (k 0)))) (h "0gficrz0gkrwsi6sb23d04l3k8mbl6l7sc3bypf3514x2frxbi0l")))

(define-public crate-geng-debug_overlay-0.3.0 (c (n "geng-debug_overlay") (v "0.3.0") (d (list (d (n "geng-core") (r "^0.3") (d #t) (k 0)) (d (n "geng-ui") (r "^0.3") (d #t) (k 0)))) (h "0kw4kd8i3mb0s725xdpfq6qygyic64p6988yalzmh5fi9sd41fza")))

(define-public crate-geng-debug_overlay-0.4.0 (c (n "geng-debug_overlay") (v "0.4.0") (d (list (d (n "geng-core") (r "^0.4") (d #t) (k 0)) (d (n "geng-ui") (r "^0.4") (d #t) (k 0)))) (h "0kpgxxlvdhlnmlw9xsr0hx5jgl3s82n6qi6fd2hjh8hij4rsyrxy")))

(define-public crate-geng-debug_overlay-0.4.1 (c (n "geng-debug_overlay") (v "0.4.1") (d (list (d (n "geng-core") (r "^0.4") (d #t) (k 0)) (d (n "geng-ui") (r "^0.4") (d #t) (k 0)))) (h "11fa6qmcvvk73980mkj7s746bjpjr1q6m8bldmb6b470jg2vbiyi")))

(define-public crate-geng-debug_overlay-0.5.0 (c (n "geng-debug_overlay") (v "0.5.0") (d (list (d (n "geng-core") (r "^0.5") (d #t) (k 0)) (d (n "geng-ui") (r "^0.5") (d #t) (k 0)))) (h "0yrv0ygcmf5ia2k9wwd4sc6v13hhc95ajjjvlmwhwgxjxxih16b6")))

(define-public crate-geng-debug_overlay-0.6.0-alpha.0 (c (n "geng-debug_overlay") (v "0.6.0-alpha.0") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui") (r "^0.6.0-alpha") (d #t) (k 0)))) (h "1r2d26gc3wwx4kn9k7sp22h6jbmb2ndw6npbrc9qpx53srck4hvw")))

(define-public crate-geng-debug_overlay-0.6.0-alpha.1 (c (n "geng-debug_overlay") (v "0.6.0-alpha.1") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui") (r "^0.6.0-alpha") (d #t) (k 0)))) (h "0cn93p1jxn3i8ssv3pfxx8x6p81kjaidjvnfgrz2jbiqbycjn41q")))

(define-public crate-geng-debug_overlay-0.6.0-alpha.2 (c (n "geng-debug_overlay") (v "0.6.0-alpha.2") (d (list (d (n "geng-core") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "geng-ui") (r "^0.6.0-alpha") (d #t) (k 0)))) (h "0afa4fifl0cqss63mrdm3dqp76i5hhjdljd4xwyfdyga32k8qc4p")))

(define-public crate-geng-debug_overlay-0.15.0 (c (n "geng-debug_overlay") (v "0.15.0") (d (list (d (n "batbox-color") (r "^0.15") (d #t) (k 0)) (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "batbox-logger") (r "^0.15") (d #t) (k 0)) (d (n "batbox-time") (r "^0.15") (d #t) (k 0)) (d (n "geng-camera") (r "^0.15") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.15") (d #t) (k 0)) (d (n "geng-ui") (r "^0.15") (d #t) (k 0)) (d (n "geng-window") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "0mkbn2dw75ii05hs2b6kib73ihcqjc1bps37fwk6sn3a30an9z5c")))

(define-public crate-geng-debug_overlay-0.16.0 (c (n "geng-debug_overlay") (v "0.16.0") (d (list (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-logger") (r "^0.16") (d #t) (k 0)) (d (n "batbox-time") (r "^0.16") (d #t) (k 0)) (d (n "geng-camera") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-ui") (r "^0.16.0") (d #t) (k 0)) (d (n "geng-window") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "00hd8hw657z6njrgj39z2nnzb6qlb44bvbssmkzaicq6gs5fsiam")))

(define-public crate-geng-debug_overlay-0.17.0 (c (n "geng-debug_overlay") (v "0.17.0") (d (list (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-logger") (r "^0.16") (d #t) (k 0)) (d (n "batbox-time") (r "^0.16") (d #t) (k 0)) (d (n "geng-camera") (r "^0.17") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.17") (d #t) (k 0)) (d (n "geng-ui") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0wk0x0x9jzwvmyh22ldi3w62bm98541kf4b6cdckm2g8a4znp24a")))

(define-public crate-geng-debug_overlay-0.17.2 (c (n "geng-debug_overlay") (v "0.17.2") (d (list (d (n "batbox-color") (r "^0.16") (d #t) (k 0)) (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "batbox-logger") (r "^0.16") (d #t) (k 0)) (d (n "batbox-time") (r "^0.16") (d #t) (k 0)) (d (n "geng-camera") (r "^0.17") (d #t) (k 0)) (d (n "geng-draw2d") (r "^0.17") (d #t) (k 0)) (d (n "geng-ui") (r "^0.17") (d #t) (k 0)) (d (n "geng-window") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "01p8vkdp0x1rncvb9jbv6finryvbhzk571imwscn0v7jdcwfjb9g")))

