(define-module (crates-io ge ng geng-derive) #:use-module (crates-io))

(define-public crate-geng-derive-0.1.0 (c (n "geng-derive") (v "0.1.0") (d (list (d (n "batbox") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l08c04kz90lmpshydifam3j44v004k9x8r4gfmf6byfba4ndfpm")))

(define-public crate-geng-derive-0.2.0 (c (n "geng-derive") (v "0.2.0") (d (list (d (n "batbox") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqwm8d0hfjl0sr85agqa3x5x76wfyzhvhxs547291dq1rn47vb4")))

(define-public crate-geng-derive-0.3.0 (c (n "geng-derive") (v "0.3.0") (d (list (d (n "batbox") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2g7m72yvbzmn8bwmw2l4g5h6h9jwzym3zikkgi798dhia48gvm")))

(define-public crate-geng-derive-0.4.0 (c (n "geng-derive") (v "0.4.0") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dc7xyfpszaipcmm34ydcskv3dfjgmaz2zlgk2qj709f6y5w9p07")))

(define-public crate-geng-derive-0.4.1 (c (n "geng-derive") (v "0.4.1") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09l1ysmnmzhjsayq4c8w1lb1ngvpc8gwf1vkpi5xjx96d86hkwiw")))

(define-public crate-geng-derive-0.5.0 (c (n "geng-derive") (v "0.5.0") (d (list (d (n "batbox") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07sdwr501d57dg63qv0zdi1iizw1gfz20k0j3lsa25403x26wj70")))

(define-public crate-geng-derive-0.6.0-alpha.0 (c (n "geng-derive") (v "0.6.0-alpha.0") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0r5g20i77djk6w0zs924nsf14aqarra52mzcv8742b7sr6dzrf8n")))

(define-public crate-geng-derive-0.6.0-alpha.1 (c (n "geng-derive") (v "0.6.0-alpha.1") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p0s3s5v9zwaq4wa399c628pz8pd6j5karv53j6d0wxff4ivm8b8")))

(define-public crate-geng-derive-0.6.0-alpha.2 (c (n "geng-derive") (v "0.6.0-alpha.2") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5irfdd0rmw9i1i3vh2hhjxc302xzy1n64cxa8pjw57gybqi627")))

(define-public crate-geng-derive-0.6.0-alpha.3 (c (n "geng-derive") (v "0.6.0-alpha.3") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s5sv8607nl4b7yb6q1fawccf8rnzmcq8y52nb65hk3aj28h4a2s")))

(define-public crate-geng-derive-0.6.0-alpha.4 (c (n "geng-derive") (v "0.6.0-alpha.4") (d (list (d (n "batbox") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14h1gcjg8z22i5smgm4qm11l5bqk5p6gdk1lm3c6amzaczq6mild")))

(define-public crate-geng-derive-0.6.0 (c (n "geng-derive") (v "0.6.0") (d (list (d (n "batbox") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nna3zvimqa6hfyhm2lf6qdd2n65n7k1wsdnzv3lj3s543ihlrb8")))

(define-public crate-geng-derive-0.7.0-alpha.0 (c (n "geng-derive") (v "0.7.0-alpha.0") (d (list (d (n "batbox") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xlrvg2wfplkhds9wmvdsfmpjnlmsnl3rcv8nxk9g35nrr8k4vk4")))

(define-public crate-geng-derive-0.7.0-alpha.1 (c (n "geng-derive") (v "0.7.0-alpha.1") (d (list (d (n "batbox") (r ">=0.6.0-alpha, <0.7.0") (d #t) (k 0)) (d (n "itertools") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c89xi07i3h6m51gw2ah3ngk1y9q461s6b67ziq67z1v95bwyn7l")))

(define-public crate-geng-derive-0.7.0 (c (n "geng-derive") (v "0.7.0") (d (list (d (n "batbox") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09gkahp71cfw6nhb6cfmjxzx21411nfzf1rv8330057axarnmziv")))

(define-public crate-geng-derive-0.8.0-alpha.0 (c (n "geng-derive") (v "0.8.0-alpha.0") (d (list (d (n "batbox") (r "^0.7.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1apq06cv5w4ihdfp2gvawzjs4h6wsh8pfn1m5rpm9bn0awddhw4k")))

(define-public crate-geng-derive-0.8.0-alpha.1 (c (n "geng-derive") (v "0.8.0-alpha.1") (d (list (d (n "batbox") (r "^0.7.0-alpha") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zasslm8gm06b92hi47l7wskqiz1ka50mi93j0zcsyhdcwaw4ism")))

(define-public crate-geng-derive-0.8.0 (c (n "geng-derive") (v "0.8.0") (d (list (d (n "batbox") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05g80lyjs9hfi0y3a9pq726jkfh71jbyj4ff3biy4byywpz0an2g")))

(define-public crate-geng-derive-0.8.1 (c (n "geng-derive") (v "0.8.1") (d (list (d (n "batbox") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1viy38sw13jhgf2q54xfwh2p8pvlrkj8136k922i4pacp71pw93w")))

(define-public crate-geng-derive-0.9.0 (c (n "geng-derive") (v "0.9.0") (d (list (d (n "batbox") (r "^0.9.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1macs4r96l0fxij9k4mcl1iy3gcbl1jlj1ck3f9zmb2gwx3lxzzv")))

(define-public crate-geng-derive-0.10.0 (c (n "geng-derive") (v "0.10.0") (d (list (d (n "batbox") (r "^0.10.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d02qc7yzi5mcwwk2wmbn2p0ja6wzmk5mmzkwd0xk11bs0idqdfd")))

(define-public crate-geng-derive-0.11.0 (c (n "geng-derive") (v "0.11.0") (d (list (d (n "batbox") (r "^0.11.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y4wkiy0xbzjxyjhg0lqs3fisr0ynq08n34sws2f5fpcyd7zv599")))

(define-public crate-geng-derive-0.12.0 (c (n "geng-derive") (v "0.12.0") (d (list (d (n "batbox") (r "^0.12.0") (d #t) (k 0)) (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1grbip0bbzb5zgnlnzbmkvnyhaahs0qw2490yqmifam2qph82lbj")))

(define-public crate-geng-derive-0.13.0 (c (n "geng-derive") (v "0.13.0") (d (list (d (n "batbox") (r "^0.13.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w19lx5fsmdix99c90virpk9fyqx0vilcbnl4li3zg5dgprsqy8b")))

(define-public crate-geng-derive-0.14.0 (c (n "geng-derive") (v "0.14.0") (d (list (d (n "batbox") (r "^0.14.0") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q9gypq1sxsszmkpzbd9vzdkn5mnz3g28i0qhi3l4acpfsj43nx3")))

