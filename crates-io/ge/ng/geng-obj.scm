(define-module (crates-io ge ng geng-obj) #:use-module (crates-io))

(define-public crate-geng-obj-0.15.0 (c (n "geng-obj") (v "0.15.0") (d (list (d (n "batbox-la") (r "^0.15") (d #t) (k 0)) (d (n "ugli") (r "^0.15") (d #t) (k 0)))) (h "13vay9nfk6azikxr9j7c2dfq1c3mj8zciaaf1zf7arr3x5016n4c")))

(define-public crate-geng-obj-0.16.0 (c (n "geng-obj") (v "0.16.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.16.0") (d #t) (k 0)))) (h "1jg8yzj5gh2kjk6m4y2wg16ifxyg3442ral8lk9fwfb7imckvbr2")))

(define-public crate-geng-obj-0.17.0 (c (n "geng-obj") (v "0.17.0") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "0vy2gw6xk1scvbjxrya5z5rrpzgf3wyjsca8mrzdfhrknaabny4g")))

(define-public crate-geng-obj-0.17.2 (c (n "geng-obj") (v "0.17.2") (d (list (d (n "batbox-la") (r "^0.16") (d #t) (k 0)) (d (n "ugli") (r "^0.17") (d #t) (k 0)))) (h "1h3s5sb49jw8hy6z9q49a6lzm1jv88f1wr3f35f9k15k71jn882c")))

