(define-module (crates-io ge nc genco-derive) #:use-module (crates-io))

(define-public crate-genco-derive-0.5.0-alpha.1 (c (n "genco-derive") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1qgzkddfv3ja9vcq0qmbw8c48k0mxg3f8phpziaf1fmi4fkmwfs6")))

(define-public crate-genco-derive-0.5.0-alpha.2 (c (n "genco-derive") (v "0.5.0-alpha.2") (d (list (d (n "genco") (r "=0.5.0-alpha.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0nw3wzlffigsdwma2ixdldf87pxb2yg8hi8prhhc0108lmh4b9vp")))

(define-public crate-genco-derive-0.5.0-alpha.3 (c (n "genco-derive") (v "0.5.0-alpha.3") (d (list (d (n "genco") (r "=0.5.0-alpha.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1w72nnsw6cdkp9zxhi5bf44w10hbqw5f6p2bgspdkqb7ss2xpm73")))

(define-public crate-genco-derive-0.5.0-alpha.4 (c (n "genco-derive") (v "0.5.0-alpha.4") (d (list (d (n "genco") (r "=0.5.0-alpha.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1sx3151lf2asr342lvglyf1hd4cxh6k62sz08xnfndxs2gg1n8cf")))

(define-public crate-genco-derive-0.5.0-alpha.5 (c (n "genco-derive") (v "0.5.0-alpha.5") (d (list (d (n "genco") (r "=0.5.0-alpha.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ms72n7vfadhd92kk6v35ma29iz70bfikys4d4axnpwy6y1bf3s2")))

