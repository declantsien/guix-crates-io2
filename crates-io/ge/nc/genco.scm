(define-module (crates-io ge nc genco) #:use-module (crates-io))

(define-public crate-genco-0.1.0 (c (n "genco") (v "0.1.0") (h "03dqjy5kcahrn4zs2f50rsxshizxd4z7la28szzqryhg0682wrkm")))

(define-public crate-genco-0.1.1 (c (n "genco") (v "0.1.1") (h "02qh1gj3mdnqda91q7m4g2kwjfll0dgwp03q8nxbbnpprndpiijy")))

(define-public crate-genco-0.1.2 (c (n "genco") (v "0.1.2") (h "029ygmxnzmbcfrirh5cy0yzadnz9mb49kkslvpv09pzi4br11yvv")))

(define-public crate-genco-0.1.3 (c (n "genco") (v "0.1.3") (h "0zlvhicc5l8hz8y5fxajvhwsx95qagk7m2l8b6b11gjw8njpdzga")))

(define-public crate-genco-0.1.4 (c (n "genco") (v "0.1.4") (h "002b9mkn44xqg7ym5a18a1axgslcmqrl2hrx75cwzbj167mr5bbz")))

(define-public crate-genco-0.1.5 (c (n "genco") (v "0.1.5") (h "0y9dqy47jamynjrbyczkdcjy16farmv23vyczhwi5y0myyy820q7")))

(define-public crate-genco-0.1.6 (c (n "genco") (v "0.1.6") (h "1hkgdy67g21zniqnk50v0d4pzl50r2f0mxwk0gwd5xs0rlszqwr2")))

(define-public crate-genco-0.1.7 (c (n "genco") (v "0.1.7") (h "12mhj77gvi1bdbjmawlh88lycy3mfm3i6zqpg0fvidryj1c1vib1")))

(define-public crate-genco-0.1.8 (c (n "genco") (v "0.1.8") (h "0alvdbknsfdnldvwj9zl9hrhgr8hmmych0valx78mcbmy3iv8y9b")))

(define-public crate-genco-0.1.9 (c (n "genco") (v "0.1.9") (h "0nlkbad52jf3cnfzpk2cg74rzx1yzjwxsxyiss9s5mm2cygl40lv")))

(define-public crate-genco-0.1.10 (c (n "genco") (v "0.1.10") (h "1pdrw3j615xmc33j0bb90535r918y7bx661w6hw1iz5yz33qljx5")))

(define-public crate-genco-0.1.11 (c (n "genco") (v "0.1.11") (h "1gfpi8swsigil7lpxxcgqy79jv6km2wbscpg6rdpidvjnv4y51aj")))

(define-public crate-genco-0.2.0 (c (n "genco") (v "0.2.0") (h "1ibip1cwrnc4l8dlpv9l8wikcnisfn348sw4hygazvlpvigdv2xm")))

(define-public crate-genco-0.2.1 (c (n "genco") (v "0.2.1") (h "1i6j2s1bqrnv1a4wc6zjv9sybrlh2kvfs45mz8pkdwddpq3n0q3z")))

(define-public crate-genco-0.2.2 (c (n "genco") (v "0.2.2") (h "0q6vlp8sy43xvf40qd4fclmimxf4zh4k9qinbyvzvnb1i2mhaslb")))

(define-public crate-genco-0.2.3 (c (n "genco") (v "0.2.3") (h "0vkysn5681jn5bnp4rl22psclc8b7x6xc9hr8aai9j58viasc2jx")))

(define-public crate-genco-0.2.4 (c (n "genco") (v "0.2.4") (h "03ij9ni867rf0aiqvhnn9ib7av9lrfa4kal6w89irmjkvzijl6nw")))

(define-public crate-genco-0.2.5 (c (n "genco") (v "0.2.5") (h "175366kiacpavimiw2rxzzfs2xkx2b9yq4qwhmk9flv73nijgx09")))

(define-public crate-genco-0.2.6 (c (n "genco") (v "0.2.6") (h "08cpa1xaffyjr6nvfawzfji0w0rv0ijvvybwnh4v4s65mc8lyiji")))

(define-public crate-genco-0.2.7 (c (n "genco") (v "0.2.7") (h "1nz40g2871g7dvjl5nv3w5f826icdliglfmfvs79l4g4s2469l13")))

(define-public crate-genco-0.3.0 (c (n "genco") (v "0.3.0") (h "0xk8f5lyqqlr4h31vlj0x2fbrs7zmbna40hk9zmgsc4ij51ymsnm")))

(define-public crate-genco-0.3.1 (c (n "genco") (v "0.3.1") (h "0fdpr35qx14k0maanqgy9dw0x290vphm67y7icjypmy37k7sv6gi")))

(define-public crate-genco-0.3.2 (c (n "genco") (v "0.3.2") (h "02fipxp3x1d0b5lifw8n8sahnrggvrbsv7ac8c8ifyc1krs0r1bf")))

(define-public crate-genco-0.3.3 (c (n "genco") (v "0.3.3") (h "06hx4f1kw5j6p17nqb6zs6m5gaf6vyipr3fi6z5qh4z4vbqp592q")))

(define-public crate-genco-0.3.4 (c (n "genco") (v "0.3.4") (h "1v3ky1h9870ajkqycww54sf8a1h0w9pp3487z4zqvxsmghr2sh6f")))

(define-public crate-genco-0.3.5 (c (n "genco") (v "0.3.5") (h "13vy8328icgkc7mn1jph520x44g1pvazw3y4sfi79n42pqa7pv2n")))

(define-public crate-genco-0.3.6 (c (n "genco") (v "0.3.6") (h "0w6mdc1ifpfcxskjv9gj6kvxy13a404xb3sv03628mqbwfwk0pk4")))

(define-public crate-genco-0.3.7 (c (n "genco") (v "0.3.7") (h "05mch0m24j8bx53wimjbjci30684ij0j4whwk77qgq89n47chd5h")))

(define-public crate-genco-0.3.8 (c (n "genco") (v "0.3.8") (h "18qvydk6a3z1l0jdnlg5cpmwg089gxspqy2l125j55m6zz62cmd7")))

(define-public crate-genco-0.3.9 (c (n "genco") (v "0.3.9") (h "036vmaz0rh7a5s2ffz1w28dmbbw63azb84080ln7pkfclv7q88rm")))

(define-public crate-genco-0.3.10 (c (n "genco") (v "0.3.10") (h "0ivr0ad3lzy4kpx8v22xa3qc45225n6hxk1xwbqkppxzhqizw0ya")))

(define-public crate-genco-0.3.11 (c (n "genco") (v "0.3.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0y4la8lyshlay1j6xg4l6ayqn54gglqaiibgm73fdv1majrj8d7c")))

(define-public crate-genco-0.3.12 (c (n "genco") (v "0.3.12") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1b65dr8i1b7q5m2751snpzs34nq1zbphabknd5cwhg2h1wrx0jly")))

(define-public crate-genco-0.3.13 (c (n "genco") (v "0.3.13") (h "0whnhcgswql6fi6cvlakbyn31zn13g1bxgjvyqv7yif6bscc092w")))

(define-public crate-genco-0.3.14 (c (n "genco") (v "0.3.14") (h "1rx9bz86b2lsas44vvb06rqdnq20in8kql9vbznvnqhy4737bafd")))

(define-public crate-genco-0.3.15 (c (n "genco") (v "0.3.15") (h "0xgy16ifz3ykg47wfqfnrcrs0im1w3c9hnmny8jflr6d9l0wkrnm")))

(define-public crate-genco-0.3.16 (c (n "genco") (v "0.3.16") (h "0x53gkm0x7m21p3c9b6hqdngh9c71na6wq9yv4v32mx0kp07m9ih")))

(define-public crate-genco-0.3.17 (c (n "genco") (v "0.3.17") (h "07q55hrgz3x9hlc6qlx9izi81lmkjm9hlkjfs07zzn6r2xh3m1gr")))

(define-public crate-genco-0.3.18 (c (n "genco") (v "0.3.18") (h "10y59zznv6mwmpcmkbi35hwkpxkfb975g0jxyvrwdhh98cxfgi7r")))

(define-public crate-genco-0.3.19 (c (n "genco") (v "0.3.19") (h "0pw9xnmqw8bqsvx00vclnswl3fpsx72aq9v0kyqqkmjpp338yclr")))

(define-public crate-genco-0.3.20 (c (n "genco") (v "0.3.20") (h "0vaq0mm1syfss48yv94067vsnvgyns8pnz3fzip1avi0y0wi54mb")))

(define-public crate-genco-0.3.21 (c (n "genco") (v "0.3.21") (h "0ri9r1nnssrkqcqw5y0ch6ddjkw1bwyxxjiakwl429jmxzvzimgx")))

(define-public crate-genco-0.3.22 (c (n "genco") (v "0.3.22") (h "1glvzz4cm72sscd58rzz0g94wzi9dzvk5pp9zxl93hxs229z9xww")))

(define-public crate-genco-0.3.23 (c (n "genco") (v "0.3.23") (h "00cb56sd69l6ixkw4pkg6fsj1j8jfqhji98gxsb9z5p9fqmnc533")))

(define-public crate-genco-0.3.24 (c (n "genco") (v "0.3.24") (h "0xzmhzq7y30g9qb8r5xrwd28v3x67ng1b92bkpm8v1rmk15sdk4s")))

(define-public crate-genco-0.3.25 (c (n "genco") (v "0.3.25") (h "0p81y9b8ly5x51qd1w409wp99sk349sah8rmkwbivsv4iajyq065")))

(define-public crate-genco-0.3.26 (c (n "genco") (v "0.3.26") (h "0sxwiwf77qzv7lspcwfvgd0m8830smy1mg38sjlbwl9ncplrdml7")))

(define-public crate-genco-0.3.27 (c (n "genco") (v "0.3.27") (h "0lbzm2f42lra5q0x3sip6nxf9jiwyjs4d3a3nh422lzlk9ywm61l")))

(define-public crate-genco-0.3.28 (c (n "genco") (v "0.3.28") (h "1s6lcggd8r4bfc0b4gi8966iph0kczs8fbrl3q0k99vgif1x3cvd")))

(define-public crate-genco-0.3.29 (c (n "genco") (v "0.3.29") (h "0wmhfvz5rp073cjmvjhm0kxraw3qqpff6q8xqfw14zchssbhzy60")))

(define-public crate-genco-0.3.30 (c (n "genco") (v "0.3.30") (h "1x817avjvyhhm7fvny0rszw5bghnq0mygc9qid8gk4zbzaql4w39")))

(define-public crate-genco-0.4.0 (c (n "genco") (v "0.4.0") (h "1rip282vwnz2kgz6jsyz3qpf1k4z2cv59p1fy4zzq137kra0yzh1")))

(define-public crate-genco-0.5.0-alpha.1 (c (n "genco") (v "0.5.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-derive") (r "=0.5.0-alpha.1") (d #t) (k 0)))) (h "19phirda98r4xxbnvgzysdpw5p8ypb287a46cdv63p7kim3lkk0f")))

(define-public crate-genco-0.5.0-alpha.2 (c (n "genco") (v "0.5.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-derive") (r "=0.5.0-alpha.2") (d #t) (k 0)))) (h "0v8psixgig7pda53dpa9pp68ms6lhkqcly3gma1kbxhh5ri3xmiz")))

(define-public crate-genco-0.5.0-alpha.3 (c (n "genco") (v "0.5.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-derive") (r "=0.5.0-alpha.3") (d #t) (k 0)))) (h "0zafdl5488xjs241iv2kpbg8k16cwhi6sfgpghyl41gfgxghdmwy")))

(define-public crate-genco-0.5.0-alpha.4 (c (n "genco") (v "0.5.0-alpha.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-derive") (r "=0.5.0-alpha.4") (d #t) (k 0)))) (h "0a5kh31r6wwfbpsi5sc0bwcpdf2z1vnbz1pq2j2xl2d97l16wnww")))

(define-public crate-genco-0.5.0-alpha.5 (c (n "genco") (v "0.5.0-alpha.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-derive") (r "=0.5.0-alpha.5") (d #t) (k 0)))) (h "1ypman3lcj7dlfv6yrrws6arhd94vwyr3iczkkvwhhn8718bffby")))

(define-public crate-genco-0.5.0-alpha.8 (c (n "genco") (v "0.5.0-alpha.8") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "15wlb7437lgyds3c2lkkqg6g46jgg0da44klq4n88vm4mzzvhyx3")))

(define-public crate-genco-0.5.0-alpha.9 (c (n "genco") (v "0.5.0-alpha.9") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0dhj3ws1qyvxzi3k0fhgn9grw46zirmqbbn5qkssdsngp5bc1n8k")))

(define-public crate-genco-0.5.0-alpha.10 (c (n "genco") (v "0.5.0-alpha.10") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0hra9wszhxl0m0iphjvcamns3m8y8820maamf749p9rs8qipm84i")))

(define-public crate-genco-0.5.0-alpha.11 (c (n "genco") (v "0.5.0-alpha.11") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0q0d8siybrzs1rdpzqb0j54839a9q0nn4vmzniapg81bwi3mqclw")))

(define-public crate-genco-0.5.0-alpha.12 (c (n "genco") (v "0.5.0-alpha.12") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1c26cjl13vykwcnjcylk5icrff8r8j5xg7ddffr9ygknyhs9jd8s")))

(define-public crate-genco-0.5.0-alpha.13 (c (n "genco") (v "0.5.0-alpha.13") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.13") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "111q2zf4cd83kmwpc30xwqjmlbk5ddx94abwbqai13359yz6kjca")))

(define-public crate-genco-0.5.0-alpha.14 (c (n "genco") (v "0.5.0-alpha.14") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.14") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13j12pqmd32s7dk9747qh83rw6vdknxvwj2g5mshvgn6d9l6q7qi")))

(define-public crate-genco-0.5.0-alpha.15 (c (n "genco") (v "0.5.0-alpha.15") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1w56yhwqhndsycvcy5srv3dhmc2z09ccrs1s6sm5h6z3qf8nvhly")))

(define-public crate-genco-0.5.0-alpha.16 (c (n "genco") (v "0.5.0-alpha.16") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.5.0-alpha.16") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0rawvd21vfa2kjgy4wmqyx05pafgkqdk1bq52c8g0f6i04r0g7nc")))

(define-public crate-genco-0.5.0 (c (n "genco") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0fkang6lbmhsx5kz1hz5siaj8pkl9wddq1akyz7gv2fsnrgsc5mk")))

(define-public crate-genco-0.6.0 (c (n "genco") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1ma5rafdim1s50dx39iyvinvsg2jjsxdszz3sxwk8qczcacxsam4")))

(define-public crate-genco-0.7.0 (c (n "genco") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1ybd7xgih3vaajs7h596cqdr137nqrqbbsx0aixizlbq5zlk2ynh")))

(define-public crate-genco-0.7.1 (c (n "genco") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1fyigkwd6g598vhld598l01chak5vk9awghx8vw2b98j2fb2kyss")))

(define-public crate-genco-0.7.2 (c (n "genco") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "028bvj71mw4a3yda91fzl0sccnyd267bc30p149a90l1sni5q2pp")))

(define-public crate-genco-0.8.0 (c (n "genco") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0858vaw8b0cx6f199a1k2y1mppd5ndfi57p19lf954h2scpnq01v") (y #t)))

(define-public crate-genco-0.8.1 (c (n "genco") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0p13n50fk7hwsd46fmkv571i8maa9wd9qd43msh5m9wn1bvjdxn1")))

(define-public crate-genco-0.9.0 (c (n "genco") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0lvrrl3p8a11hlw9k63v5awrldy70r90szdca8kh0jzy1zylh519")))

(define-public crate-genco-0.10.0 (c (n "genco") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1kf4v9xfk73zy0b44wpvk01drp86ln0xrh7mzxjp1facmbib7nqh")))

(define-public crate-genco-0.10.1 (c (n "genco") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "02cvvywgsirx872cprjwrlg63019d2d1ri7g4xiz98r2bn78mamc")))

(define-public crate-genco-0.10.2 (c (n "genco") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "18azr0iygr9cxbj7xvwir67qrmn26kbiaf5f8djiw088s44c5h0i")))

(define-public crate-genco-0.10.3 (c (n "genco") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "06q0y1b8ybs0vb81k8q8ci5irmd98fpp2vz7552hfmm9di5g3r79")))

(define-public crate-genco-0.10.4 (c (n "genco") (v "0.10.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0jmw84v82pw086b3a14bwkpy49xkvmspsm9q9gn41gdhmqw58n73")))

(define-public crate-genco-0.10.5 (c (n "genco") (v "0.10.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1xpnk6wglrzpg1pvkpjw89r8ydzc6w56n6nbcpvqpfdlvpacic9g")))

(define-public crate-genco-0.10.6 (c (n "genco") (v "0.10.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0l6ba1pmlc2b1b6q65hrfbym15k02hv5q18vbw7h6ybj2wl55irw")))

(define-public crate-genco-0.10.7 (c (n "genco") (v "0.10.7") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1gl5rs6pzmb5bswzdc4lhx6qnj9f13kh3dx7c6x92xhiz1hadhc5")))

(define-public crate-genco-0.10.8 (c (n "genco") (v "0.10.8") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1bv0zy8sa1387p5kv27dpkzz1fxxsbyhym9gvmq4yz2rbnbri940")))

(define-public crate-genco-0.10.9 (c (n "genco") (v "0.10.9") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1606wz4x1ddk0virxs54b9r8xwlmi83449chaihwzhsalag4lz8p")))

(define-public crate-genco-0.10.10 (c (n "genco") (v "0.10.10") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.10.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1jbgxb8lq05d5a0fqc3bfilrxnkfcrxvicxqjkdc4iqs4pbai0md")))

(define-public crate-genco-0.11.0 (c (n "genco") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "08dny27j5k9blza4ifarlwd932mxhiansjzcbg8x3395zq0b7zbv")))

(define-public crate-genco-0.11.1 (c (n "genco") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "144080crmq5mvsgvn9bg42kvfrpnzv1zba58z98m9kci5hc302i2")))

(define-public crate-genco-0.12.0 (c (n "genco") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "150vaph41hpi644nv419iig2f8i5a38hdphiv0lgvmjibap3ggpb")))

(define-public crate-genco-0.13.0 (c (n "genco") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1dw3fr3w1n0djp0x5c2g5bdh4sxmdi2i0bmn1d5f077d3ir0is0l")))

(define-public crate-genco-0.14.0 (c (n "genco") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "03qg7gqzzmqbhhvykkfpfx80ih6svlfza0sm487qv0yhidsvl7bk")))

(define-public crate-genco-0.14.1 (c (n "genco") (v "0.14.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.14.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0alqp3zpqjz4np3mdqzbzsc0q0li741i84gfrk534701krjr6hf4")))

(define-public crate-genco-0.14.2 (c (n "genco") (v "0.14.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.14.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1xqp06mhldlla974kpw9wwjgdv83i21mhfd116dyj1fsrw3c5nyj")))

(define-public crate-genco-0.15.0 (c (n "genco") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1kpib03j02jpl47zy8i1kimabsc37wz2pc3wq46vh5fg99kihlhy")))

(define-public crate-genco-0.15.1 (c (n "genco") (v "0.15.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.15.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1l4vy6r90la33ad7ma9bqjaqw5ic42i6lrbcnywkcarxpr066gka")))

(define-public crate-genco-0.16.0 (c (n "genco") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "13i5qpzzrj1gvzrv52nfpnmi9zv8np9pmxmb0c3hrsmfqj7vhzd3")))

(define-public crate-genco-0.16.1 (c (n "genco") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.16.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1wwspjgm1p3fzn96rlrh0b6dw3yirlvsp28mv4aya42gk67p4fz7")))

(define-public crate-genco-0.17.0 (c (n "genco") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "00pqwwp9myf1g8svldgklz1bcw47c9nbzrlbkgj8m3nfi2zg6z9d")))

(define-public crate-genco-0.17.1 (c (n "genco") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0bfdjpbp2gyyzypmfwq0qr6xfyh245jx1vsvq72j7ri1ddbycr27")))

(define-public crate-genco-0.17.2 (c (n "genco") (v "0.17.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1x713l59awrc3iv6ivk2gddm3673qp0jgdwyrwqm5g1dg3q8ynfq")))

(define-public crate-genco-0.17.3 (c (n "genco") (v "0.17.3") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "^0.17.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "085dmd8q2pm9xa888yb861vzcjv0xzjb67xil62arm8yrv443pj3")))

(define-public crate-genco-0.17.4 (c (n "genco") (v "0.17.4") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "02nf4jrpynwhz6c31mlv86vcibs8bzrkj3pgxhnxk3h90wy3iv03") (r "1.58")))

(define-public crate-genco-0.17.5 (c (n "genco") (v "0.17.5") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "12mw960n1wm7q3wxhsvaai2qa233nnjzca7l0ka732h6322wwwv9") (r "1.58")))

(define-public crate-genco-0.17.6 (c (n "genco") (v "0.17.6") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "11vpfsvmh9m45nmax7yq2w5rh8r1afwrk4ilrdshfih4psfzk5rm") (r "1.58")))

(define-public crate-genco-0.17.7 (c (n "genco") (v "0.17.7") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "0k9hjdrblz694pmc7flm9nwyxqmv44fvnki2h5dwzsgzjd427zf4") (r "1.58")))

(define-public crate-genco-0.17.8 (c (n "genco") (v "0.17.8") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1bawc0kscby961zrp70v7ckn38qg6rlyc9k4c7p8ywwhhxcszmwq") (r "1.58")))

(define-public crate-genco-0.17.9 (c (n "genco") (v "0.17.9") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "genco-macros") (r "=0.17.9") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "relative-path") (r "^1.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.4.0") (d #t) (k 0)))) (h "1ij2mm0glvj34flq2npq4yjsffbym3c61nwwxygsqsfv2jxkrb5g") (r "1.66")))

