(define-module (crates-io ge ar gear-lazy-pages-common) #:use-module (crates-io))

(define-public crate-gear-lazy-pages-common-0.0.0 (c (n "gear-lazy-pages-common") (v "0.0.0") (h "1y7bv5b7yyxixswhr4pk1ph49g0qaapbgdrcn4y93c6rmszcmhaw")))

(define-public crate-gear-lazy-pages-common-1.0.3-dev (c (n "gear-lazy-pages-common") (v "1.0.3-dev") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.3-dev") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0ib5h8x92a7g9jf0453xf8c52abwb1gr5ag704iz440bbg7pfabg")))

(define-public crate-gear-lazy-pages-common-1.0.3-dev.0 (c (n "gear-lazy-pages-common") (v "1.0.3-dev.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.3-dev.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1z3pxcfilc2bx47a21qj8by584rbwx12sh8x4810axwqy8isq3pd")))

(define-public crate-gear-lazy-pages-common-1.0.3 (c (n "gear-lazy-pages-common") (v "1.0.3") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0f9yrcqmk8zi9vls84ac8fgbw2s052lm49m56qi4f7lx8385z4bz")))

(define-public crate-gear-lazy-pages-common-1.0.4-rc.0 (c (n "gear-lazy-pages-common") (v "1.0.4-rc.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.4-rc.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0yxdp5v0rljp6ri5sib3a3anwvxf1gj2s68wkq6jlz7xs6xj17ym")))

(define-public crate-gear-lazy-pages-common-1.0.4 (c (n "gear-lazy-pages-common") (v "1.0.4") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.4") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1r0lsz75aj1mzk36k0108d5h5hxzpd85jfhf8j736qp43l076ij8")))

(define-public crate-gear-lazy-pages-common-1.0.5 (c (n "gear-lazy-pages-common") (v "1.0.5") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.0.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1j2fqknzg3b220laapfdcmwgs76qmbklbnkbqmiw7kzjf09j0w9a")))

(define-public crate-gear-lazy-pages-common-1.1.0 (c (n "gear-lazy-pages-common") (v "1.1.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.1.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "09y2s7iz4gfv3b3mrwj4672kkbjbxcaj6598lqjfwm5vmys8py4l")))

(define-public crate-gear-lazy-pages-common-1.1.1-rc.0 (c (n "gear-lazy-pages-common") (v "1.1.1-rc.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.1.1-rc.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0xnk7m9519wikfa7gyd65g3b5svcn5g3965iadh0s6gnq05a28yb")))

(define-public crate-gear-lazy-pages-common-1.1.1 (c (n "gear-lazy-pages-common") (v "1.1.1") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.1.1") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0522p1xdh5az17drw23v7cjisvma8bm1snwhbcq945bksqzmrwg3")))

(define-public crate-gear-lazy-pages-common-1.2.0-pre.2 (c (n "gear-lazy-pages-common") (v "1.2.0-pre.2") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.2.0-pre.2") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1hshwj16lpqg3slhp40cgwppp3njn6x23zpsnqhfpgsmsj2airv3")))

(define-public crate-gear-lazy-pages-common-1.3.1-pre.2 (c (n "gear-lazy-pages-common") (v "1.3.1-pre.2") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.3.1-pre.2") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1xk9zz7f4jmjmcvq7v1psig37gd45ki9j5gxq1viphsm01h0ifgw")))

(define-public crate-gear-lazy-pages-common-1.3.1-pre.3 (c (n "gear-lazy-pages-common") (v "1.3.1-pre.3") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.3.1-pre.3") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0g1hg5sjc70mai1nd1dgdgwry9bh29ncrh8xbx0hxrhwd2qlm0f2")))

(define-public crate-gear-lazy-pages-common-1.2.0-pre.3 (c (n "gear-lazy-pages-common") (v "1.2.0-pre.3") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.2.0-pre.3") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0saa7gb7k1lcl8pckpkn7vglqq5nsfahxlclp1lknvdpbrjpyjnq")))

(define-public crate-gear-lazy-pages-common-1.2.0 (c (n "gear-lazy-pages-common") (v "1.2.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.2.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1sbwqb4p1rabskxfp8hv08aqbj0vp0vvwc0jragwhp21n3zl5i4x")))

(define-public crate-gear-lazy-pages-common-1.2.1 (c (n "gear-lazy-pages-common") (v "1.2.1") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.2.1") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "14yhsvs35lbw82rbaw98qggbvh0ld9vfc559fjdcyhr8x3df5j9a")))

(define-public crate-gear-lazy-pages-common-1.3.0-pre.1 (c (n "gear-lazy-pages-common") (v "1.3.0-pre.1") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.3.0-pre.1") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "13c5k7mnyiz39famxgp6hc8b0g27jkm9bq05xc8461qkm5fciznc")))

(define-public crate-gear-lazy-pages-common-1.3.0 (c (n "gear-lazy-pages-common") (v "1.3.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.3.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "04my517c5vns84za8iig016kqmnx8kh176q500gmrjmb5pkpwyfw")))

(define-public crate-gear-lazy-pages-common-1.3.1 (c (n "gear-lazy-pages-common") (v "1.3.1") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.3.1") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0hc4ary76j1w9q1gsb7dwdhr761zxqhkwfp1sxhfrw7bygwrlpfy")))

(define-public crate-gear-lazy-pages-common-1.4.0 (c (n "gear-lazy-pages-common") (v "1.4.0") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.4.0") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1fphvzyi66q100wb2f44f5dmr5c5s4shcyd77xn5yfldc5kd24il")))

(define-public crate-gear-lazy-pages-common-1.4.1 (c (n "gear-lazy-pages-common") (v "1.4.1") (d (list (d (n "codec") (r "^3.6.4") (k 0) (p "parity-scale-codec")) (d (n "gear-core") (r "^1.4.1") (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "0vg4il2mzci0qrrxd6hpbr4bivglx6qfz9w6k6z8z1g5gxkypiqc")))

