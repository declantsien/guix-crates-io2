(define-module (crates-io ge ar gear-common-codegen) #:use-module (crates-io))

(define-public crate-gear-common-codegen-0.0.0 (c (n "gear-common-codegen") (v "0.0.0") (h "19gfwc177c0x5q5alyimkk0w4g6lkv8wy5l9jjh5cr9lqsp6pn2v")))

(define-public crate-gear-common-codegen-0.1.0 (c (n "gear-common-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1czay1vmars03c79p9mgpsdn88qncwixmfakz5m0bpw0bf2ksaib")))

(define-public crate-gear-common-codegen-0.2.1-alpha.0 (c (n "gear-common-codegen") (v "0.2.1-alpha.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0qqhm05v0ibnwwqgm9h1z93h7p1za9g155kcgklxrni6pvxyc2mr")))

(define-public crate-gear-common-codegen-0.2.2-alpha.0 (c (n "gear-common-codegen") (v "0.2.2-alpha.0") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "16853laqbcl2bcnxvlak76kin1rs2dksc7cgrbly60pm8kmz1yay")))

(define-public crate-gear-common-codegen-0.2.1-alpha.1 (c (n "gear-common-codegen") (v "0.2.1-alpha.1") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1105md802igzb9i91v6s6npfx93p2603kjdqkzqgdcfa0b53y8qi")))

(define-public crate-gear-common-codegen-0.2.1-alpha.2 (c (n "gear-common-codegen") (v "0.2.1-alpha.2") (d (list (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "08b7gnd81r5l1sa0lhcl6k3zpjcc28grlvgldjqnp2px48qyzylc")))

(define-public crate-gear-common-codegen-0.3.1-alpha.0 (c (n "gear-common-codegen") (v "0.3.1-alpha.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "02qw3jb2l1b71zrajhv3a54bm2bll5p3h58glvn8xi40lg7z4kry")))

(define-public crate-gear-common-codegen-0.3.1-alpha.1 (c (n "gear-common-codegen") (v "0.3.1-alpha.1") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1zp1x2caa0lhz1gpgkljyf2rpinz31ch85hlyz3zyxslf0azn511")))

(define-public crate-gear-common-codegen-0.3.1-alpha.2 (c (n "gear-common-codegen") (v "0.3.1-alpha.2") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "06imnm0nr5psr0fjyfglprj66z6m5kqz08f3hrw34b3j34pw7pqz")))

(define-public crate-gear-common-codegen-0.3.2 (c (n "gear-common-codegen") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0yn8r50g5b0kxkg0mmcav82cihxkz0fbdqwby4pjnmsbmhyhscmn")))

(define-public crate-gear-common-codegen-0.3.2-alpha.1 (c (n "gear-common-codegen") (v "0.3.2-alpha.1") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "182qsww8yrbzs66c47hqcipm6gjvm21ynf5ir26lm7n0n50f4nk4")))

(define-public crate-gear-common-codegen-0.3.2-alpha-2 (c (n "gear-common-codegen") (v "0.3.2-alpha-2") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "0vmb6b4i8nmj9lxkd4gn1a20a6v5xckap70snsy05g7n0l3dxv1k")))

(define-public crate-gear-common-codegen-0.3.2-alpha.3 (c (n "gear-common-codegen") (v "0.3.2-alpha.3") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "04ind8ly9jf3yd8bmb6qhggfn5wvh9wp30wzgj971m6bn7f5jhsy")))

(define-public crate-gear-common-codegen-0.3.2-alpha.4 (c (n "gear-common-codegen") (v "0.3.2-alpha.4") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "1ifslrap9s2jsys8izw1q0g9x0f180f71ha7nl8y6qcp4b0b5hhb")))

(define-public crate-gear-common-codegen-1.0.2-pre.0 (c (n "gear-common-codegen") (v "1.0.2-pre.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1jidbj4ndr5cfvc2mnis4q1xyxajkbz1agf3593885cywpg6vcvb")))

(define-public crate-gear-common-codegen-1.0.2-pre.3 (c (n "gear-common-codegen") (v "1.0.2-pre.3") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0djylbq0f1nxq6mgwk8p0p88mabkj94zzc0if1a1ghri36ffa56m")))

(define-public crate-gear-common-codegen-1.0.2-pre.4 (c (n "gear-common-codegen") (v "1.0.2-pre.4") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0s15m9in1xipkjm3i7aryvr5rnqxfavimb3j2x9r4j2ci865c5df")))

(define-public crate-gear-common-codegen-1.0.2-pre.5 (c (n "gear-common-codegen") (v "1.0.2-pre.5") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0bi8q3y17anl2kh9s491dkcz45424i1nrk99wa46ipvgw34f0pqx")))

(define-public crate-gear-common-codegen-1.0.2-pre.6 (c (n "gear-common-codegen") (v "1.0.2-pre.6") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1ri0rlfqpj03nmpx8v125ibpxf6s8wf118406k09h6fmqybs54ra")))

(define-public crate-gear-common-codegen-1.0.2-pre.7 (c (n "gear-common-codegen") (v "1.0.2-pre.7") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1cyc1pkjnkaa01nandbilj5x9m1q31285w07pp9b5mjl1kx0q29c")))

(define-public crate-gear-common-codegen-1.0.2-pre.8 (c (n "gear-common-codegen") (v "1.0.2-pre.8") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0657r2nvz1lmh9jnmz1952a4yqnhi0438a9gpg86wcl73j5s4nlk")))

(define-public crate-gear-common-codegen-1.0.2-pre.9 (c (n "gear-common-codegen") (v "1.0.2-pre.9") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1jqxi5rfic31na4c9xbic4sj3yk67cprlm3jcn5d537lrp928wi7")))

(define-public crate-gear-common-codegen-1.0.2-pre.10 (c (n "gear-common-codegen") (v "1.0.2-pre.10") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0bblk24qpc2q6smzp872rc0wq0m275nliam6p0gc9jkwirqlpfdp")))

(define-public crate-gear-common-codegen-1.0.2-pre.11 (c (n "gear-common-codegen") (v "1.0.2-pre.11") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1bgw196x9viygypx2qcmnbd2qfhirxwp0csnsfq32sgqc0624i3b")))

(define-public crate-gear-common-codegen-1.0.2-pre.12 (c (n "gear-common-codegen") (v "1.0.2-pre.12") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "07v6ijf0nv94br21hzzgpbb4aih8zdrycyvyiqasxi7dqr02acn2")))

(define-public crate-gear-common-codegen-1.0.2-pre.13 (c (n "gear-common-codegen") (v "1.0.2-pre.13") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "17b5mfi0prj9ng92z2jygll1mxwydqj1x4wxb03bss2zivnc0z2n")))

(define-public crate-gear-common-codegen-1.0.2-pre.14 (c (n "gear-common-codegen") (v "1.0.2-pre.14") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0qa6c1y7lai2qw9c6djmbf9aincqgw98ir9bl65zfhiwlrjlwc5b")))

(define-public crate-gear-common-codegen-1.0.2-pre.15 (c (n "gear-common-codegen") (v "1.0.2-pre.15") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1dpshhf2gh1m5pw6xvc1frs0mvl6f974fnnmr9yfzr463k5lp6m1")))

(define-public crate-gear-common-codegen-1.0.2-354d660.0 (c (n "gear-common-codegen") (v "1.0.2-354d660.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "190h9lw165z2rqnzsfigncnmkdyfzf1kc7is0abq5k0xm4c48h20")))

(define-public crate-gear-common-codegen-1.0.2-354d660.1 (c (n "gear-common-codegen") (v "1.0.2-354d660.1") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1b8l3vh9r58ffvrd2qasphbzji1id88q99xvn3ykn6flr40ir8rh")))

(define-public crate-gear-common-codegen-1.0.2-354d660.3 (c (n "gear-common-codegen") (v "1.0.2-354d660.3") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0x882pf1ik15b6v5nym57swnlxnrq6b0pwhmnz5p6isdg65i1sj5")))

(define-public crate-gear-common-codegen-1.0.2-354d660.4 (c (n "gear-common-codegen") (v "1.0.2-354d660.4") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0184jr1qkr0q1a6sgfx26j85hqbpga8fhyrpf2flcqkrqp3yrsmi")))

(define-public crate-gear-common-codegen-1.0.2-354d660.6 (c (n "gear-common-codegen") (v "1.0.2-354d660.6") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "02qm6qx9613xnq1nj7rlj7fdrnrsdw8856m27sg274c6ysyy8v02")))

(define-public crate-gear-common-codegen-1.0.2-354d660.5 (c (n "gear-common-codegen") (v "1.0.2-354d660.5") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0d31qdvwwxpgksivzhcd474gha8sb56qwbk5xlr9m9lpdh0bibl0")))

(define-public crate-gear-common-codegen-1.0.2-354d660.8 (c (n "gear-common-codegen") (v "1.0.2-354d660.8") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0vzsgggjbv6fb4k1hsjnwbkq2plyb4mpgqq89d1z8lnj8al2paba")))

(define-public crate-gear-common-codegen-1.0.2-354d660.9 (c (n "gear-common-codegen") (v "1.0.2-354d660.9") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1sdb7nl7qr0v5jkc44l03cq57965g0wc290mz46xw1jynyn4ixlb")))

(define-public crate-gear-common-codegen-1.0.2-354d660.10 (c (n "gear-common-codegen") (v "1.0.2-354d660.10") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0lwchzjq1j35iqr3n61l89wl0qr7k4mca733gl1d2glbr9s41hjf")))

(define-public crate-gear-common-codegen-1.0.2 (c (n "gear-common-codegen") (v "1.0.2") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "09mawrkcszjpy9jq012kylswfingkyh1gz1rgdg34raxcvsal2wk")))

(define-public crate-gear-common-codegen-1.0.2-gtest-dev (c (n "gear-common-codegen") (v "1.0.2-gtest-dev") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.40") (d #t) (k 0)))) (h "02m7k542bilqf81zq1rfinl0vy69hpgv4v2afzk61m5ydr95srbz")))

(define-public crate-gear-common-codegen-1.0.2-dev.0 (c (n "gear-common-codegen") (v "1.0.2-dev.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0006fvwb4ksihmbp0is0v556zhjsw65yvj4z381fj977i2ixifp6")))

(define-public crate-gear-common-codegen-1.0.3-dev.0 (c (n "gear-common-codegen") (v "1.0.3-dev.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0fyam8mwffvh0hw3w0rd12s0r5b19kzg2wj6fb9011ridcv92mxj")))

(define-public crate-gear-common-codegen-1.0.3 (c (n "gear-common-codegen") (v "1.0.3") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0rp52v868065mll9nln6qiqsp530nfjgwbh4acrzh2lny3ivfmkv")))

(define-public crate-gear-common-codegen-1.0.4-rc.0 (c (n "gear-common-codegen") (v "1.0.4-rc.0") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0ynrd4q7g1vw68jfjh1z5l17ywfajlwpvp29mqhbcdjw4s0v71kn")))

(define-public crate-gear-common-codegen-1.0.4 (c (n "gear-common-codegen") (v "1.0.4") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0gcwizpai5gqx17ydn6frzvwlzj0rp451l9k7jy5yfngvi5586y4")))

(define-public crate-gear-common-codegen-1.0.5 (c (n "gear-common-codegen") (v "1.0.5") (d (list (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "11j9qn07gyws43lasyx1jfvgz2kij1lymmdrw55n57bb19q7xlqf")))

(define-public crate-gear-common-codegen-1.1.0 (c (n "gear-common-codegen") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "11r5ya5gypwryriab2q24fj9a14cvcyczbyjlni69lx2szp6812a")))

(define-public crate-gear-common-codegen-1.1.1-rc.0 (c (n "gear-common-codegen") (v "1.1.1-rc.0") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "1gjxf3lf37y3hzd5wyz5b9db70n48xlx6vk8bf8l1lg6l7nws13x")))

(define-public crate-gear-common-codegen-1.1.1 (c (n "gear-common-codegen") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0j0kd5pbxsdkyjq05x17ql328985gqlynyi4hkb1gw0xy4fvyi5f")))

(define-public crate-gear-common-codegen-1.2.0 (c (n "gear-common-codegen") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "13rwvg76c48ji1plnybxzjmbi5bjf747260axh8vm6yyh6j1jbcd")))

(define-public crate-gear-common-codegen-1.3.0 (c (n "gear-common-codegen") (v "1.3.0") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "1ddz24f4iqc7arng1ihx0nrgma6knl47q1n0z8c3j5w0l6h4nh0v")))

(define-public crate-gear-common-codegen-1.2.0-pre1 (c (n "gear-common-codegen") (v "1.2.0-pre1") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "1j33alnxma74l0awamsgk70rqbcdp8ajs70dffm4pmjd192yrf8l")))

(define-public crate-gear-common-codegen-1.2.0-pre.2 (c (n "gear-common-codegen") (v "1.2.0-pre.2") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "1dkf99pqag1px9nnhr4k28b35xd2ga8vvp4vprahln8xhk1lij87")))

(define-public crate-gear-common-codegen-1.3.1-pre.1 (c (n "gear-common-codegen") (v "1.3.1-pre.1") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "09hxfri9zmgwyl26z6rn0jqrn1w1kfnjn36j1q43vcjknw5ji49h")))

(define-public crate-gear-common-codegen-1.3.1-pre.2 (c (n "gear-common-codegen") (v "1.3.1-pre.2") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "1k7gnhpdz24d4fjbrq6jbxiclr0k0522ds4w7sr3vi52l1rd2djv")))

(define-public crate-gear-common-codegen-1.3.1-pre.3 (c (n "gear-common-codegen") (v "1.3.1-pre.3") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "0x91w70idml4haqj9l6mqiyn58har0i93azr1afyclbx16mn0jwz")))

(define-public crate-gear-common-codegen-1.2.0-pre.3 (c (n "gear-common-codegen") (v "1.2.0-pre.3") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "06lf3ykdbxz5ywg62i462i36lz163591k8dp5xdmndxr14kkd6ss")))

(define-public crate-gear-common-codegen-1.2.1 (c (n "gear-common-codegen") (v "1.2.1") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.49") (d #t) (k 0)))) (h "00i1r95jdfnxzxz4z3krhz7i65byxzhna0nxw9bq1dkry5rgz4g8")))

(define-public crate-gear-common-codegen-1.3.0-pre.1 (c (n "gear-common-codegen") (v "1.3.0-pre.1") (d (list (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.57") (d #t) (k 0)))) (h "04h598s06nm3gf4q2i16sq65papk1353xjn66n4mhc1spqpjjrgr")))

(define-public crate-gear-common-codegen-1.3.1 (c (n "gear-common-codegen") (v "1.3.1") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1lpw3xk1zacwkid1n6k6zb40bai4v7nivdxqxyif6d3cyx9l8rmx")))

(define-public crate-gear-common-codegen-1.4.0 (c (n "gear-common-codegen") (v "1.4.0") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1g8xmgmskwyryzksiqf1pnxx7mixd7hqdm0znz6jl5j1y1xv82mv")))

(define-public crate-gear-common-codegen-1.4.1 (c (n "gear-common-codegen") (v "1.4.1") (d (list (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0frgln2szib9x03xcr0qai1xrl3njwhzqanm5sfb36lkhfdz4lj9")))

