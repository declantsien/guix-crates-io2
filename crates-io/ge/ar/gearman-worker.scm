(define-module (crates-io ge ar gearman-worker) #:use-module (crates-io))

(define-public crate-gearman-worker-0.1.0 (c (n "gearman-worker") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.6.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "0xwcbjm3qd8kifnlc4v5zy51n5c2mclydjw8m99s8w5z782m2rm9")))

(define-public crate-gearman-worker-0.2.0 (c (n "gearman-worker") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "uuid") (r "^0.6.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "075nvk2ldcgjdbbcnz3xb35fpb9a92y5q8hxvwwil80w00czmvq4")))

(define-public crate-gearman-worker-0.3.0 (c (n "gearman-worker") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ka0qigi5l8rg830w87vx19r3zzrrnjmafcw985iy8gjgb10dcm7")))

