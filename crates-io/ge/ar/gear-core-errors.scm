(define-module (crates-io ge ar gear-core-errors) #:use-module (crates-io))

(define-public crate-gear-core-errors-0.0.0 (c (n "gear-core-errors") (v "0.0.0") (h "0giqfdjds4dyqk6h7fbsmk0r6slqlaha9hzcbqs5csghmds6w0rv")))

(define-public crate-gear-core-errors-0.1.0 (c (n "gear-core-errors") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0xclqifv3cds6gknfsfajc02jbch3smsn6ncy2sk8f2jb6ljyw7x") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.2.1-alpha.0 (c (n "gear-core-errors") (v "0.2.1-alpha.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1xyqq54wwsqzsdw0g6pqymsrl5zs6g3nf2p89w09024srkw6g85f") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.2.1-alpha.1 (c (n "gear-core-errors") (v "0.2.1-alpha.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1gm5fp47zkgkvm8hgf4qfq4yr1xsvdqibjig4g6ilai7836007ih") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.2.1-alpha.2 (c (n "gear-core-errors") (v "0.2.1-alpha.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1lwl6w1l9lj95jh4i4qqdfcpwvbzh7zvzz1nbw09ksxbg247f8dn") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.1-alpha.0 (c (n "gear-core-errors") (v "0.3.1-alpha.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "17sww3p7s5pv63gqhc661bibw3pilbwv6673n5ya6wsngm82fi1v") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.1-alpha.1 (c (n "gear-core-errors") (v "0.3.1-alpha.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0x87mq7dgb2g0p47zk84ibva0jf8lha78dq2yar3w82bj5ynrn3v") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.1-alpha.2 (c (n "gear-core-errors") (v "0.3.1-alpha.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0w5rxxcz8f8fnfrs19pkxldi4xq4g2z2i5zmdrx5y745kgr8g10h") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.2 (c (n "gear-core-errors") (v "0.3.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0dv5bnxad6ra6dfddpsbsqs5yavvw03hj659k91cwr5f0kmf8jzh") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.2-alpha.1 (c (n "gear-core-errors") (v "0.3.2-alpha.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0c8p0dzc2j7fzim34s0r3hx28bbwxy7jb8w1kc51qghkkkympqsi") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.2-alpha-2 (c (n "gear-core-errors") (v "0.3.2-alpha-2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1pmw3nvb4jmgdikjnz35g956f6i3g5iai2jmdim1m342q1cbrvh4") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.2-alpha.3 (c (n "gear-core-errors") (v "0.3.2-alpha.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1g8p7ih5h8mh5q01vfq1qxp8ch0r8dl96mfdk7wvqyi7jwxz24kn") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-0.3.2-alpha.4 (c (n "gear-core-errors") (v "0.3.2-alpha.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0aczrvdxnwqil439491hc7py6nygv141vl182jaj873mi8khnmx4") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.0 (c (n "gear-core-errors") (v "1.0.2-pre.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "04qplwjz31d0hgvq8dyfwxnj6w4ml6z6hd77yjlxz5azycm5nbn4") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2 (c (n "gear-core-errors") (v "1.0.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0mffpy5sq6f532n4g4hc6gwfnblv21fafkyn25yc8cc0d8c98b2a") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.1 (c (n "gear-core-errors") (v "1.0.2-pre.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "16ba8m5pydyh9vmnkq23d5602aymp1r8nip7sk6i0mnqkqs3zipk") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.2 (c (n "gear-core-errors") (v "1.0.2-pre.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1qpq685jvwxkaip675zgz7arklri8j7ymcs2c910wi30p834iwdr") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.3 (c (n "gear-core-errors") (v "1.0.2-pre.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1z625d8mkwf6dcjvqh8cgxlvl74mqb9s7w2brr4nvls6xpp7yx5s") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.4 (c (n "gear-core-errors") (v "1.0.2-pre.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0wl45bgkgbk7w9mfpcn917b836r9m7dib32dzzwg1bf74baai083") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.5 (c (n "gear-core-errors") (v "1.0.2-pre.5") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0nadm9w1g9f69l48fm1ll5rcznsjqwdj4jazb25likbrxwly17sj") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.6 (c (n "gear-core-errors") (v "1.0.2-pre.6") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "12r3yv0qpc34fmqrs4mwqlg63zp3avz2nxvfz3yf2vk3b6nyhyrc") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.7 (c (n "gear-core-errors") (v "1.0.2-pre.7") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "14pfnzis6jvqkywxa79pxp37zy37163pv4wyk836qy6ax81w0qcq") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.8 (c (n "gear-core-errors") (v "1.0.2-pre.8") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0a94bwc0na9lkq7z2dq7cmbxs43n4snnzx5njfbyp38fc93k2b1w") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.9 (c (n "gear-core-errors") (v "1.0.2-pre.9") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0yhiddsizmav4p32pzr4rjfpq5c2y13fhx6j4nn9q8wkb2j8hz1a") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.10 (c (n "gear-core-errors") (v "1.0.2-pre.10") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "15nyjwj3wxjsz7h9a6rba65bg8f8rriaj497wz2d2yy2hfsyazjn") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.12 (c (n "gear-core-errors") (v "1.0.2-pre.12") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0d88rbxfnjmpf057i8fvpx5zwfy3ld3mm1czzzrgalqnyfz970dr") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.13 (c (n "gear-core-errors") (v "1.0.2-pre.13") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1vdz3rhd68wn88ggk5xjs7jl4pirbjgm9szqbn0m7dx1g8pxz9z6") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.14 (c (n "gear-core-errors") (v "1.0.2-pre.14") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1phb2m2mwmr7hb4bggqn9a5p3ybjw9bbaigc98vdasciz7z6p6k9") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-pre.15 (c (n "gear-core-errors") (v "1.0.2-pre.15") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "12nf74k9560in89kbxr2bkhhmaggivfzzn8srsm2mjp5gk1a7rrl") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.0 (c (n "gear-core-errors") (v "1.0.2-354d660.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "10jcw7y0xdsm89f011i7r43mjxfgmdxshwyg4vg2yil0r8h7c73i") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.1 (c (n "gear-core-errors") (v "1.0.2-354d660.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0qls4iklrliq9cypxyqp655cb2h4c6j0826bn9r37xif277n7hy9") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.3 (c (n "gear-core-errors") (v "1.0.2-354d660.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1l97n0lr7gvlfjd3cz8xivj4mmrfc6n8aym1llp8x2npc53g41jc") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.4 (c (n "gear-core-errors") (v "1.0.2-354d660.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "12lfhf7idws6cv12bnwa81h25sd14pls0r00pdh05cz00iy4wf8a") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.6 (c (n "gear-core-errors") (v "1.0.2-354d660.6") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "060nry938hb4485wnhqf20mq7c7smkb7bhmhiv72b8glmmqpxfdg") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.5 (c (n "gear-core-errors") (v "1.0.2-354d660.5") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1bh5bdpnzh41qz0qwrp6467ndl8pkgjspc2kvjr1dsf90h14j9la") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.8 (c (n "gear-core-errors") (v "1.0.2-354d660.8") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0a00hgjyiddcv28b3asr5j5jjl8llpw82rni2f59hxddxi74cd6l") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.9 (c (n "gear-core-errors") (v "1.0.2-354d660.9") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0xvg8g5p6s0vpiiqj88n5hl90x2xf5xl0f4vbsf67rpy41cm3a76") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-354d660.10 (c (n "gear-core-errors") (v "1.0.2-354d660.10") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1k37h5brx7pzpysqabqmhss32ffqfgzg5jgsfps8g6pam39jh15i") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-gtest-dev (c (n "gear-core-errors") (v "1.0.2-gtest-dev") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0ihzvl4586hn4an3p2l3hwd4ala321v2p2cjzvxghpvczsip5agg") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.2-dev.0 (c (n "gear-core-errors") (v "1.0.2-dev.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "09g6mzjkblpgrh921z1i0d0n45bspwhm4h64w40n66qzcsjxygrk") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.3-dev (c (n "gear-core-errors") (v "1.0.3-dev") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0688h5ppj5bycmp3748aid34lfp8dk9s3ndap3fqhhr24man0agn") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.3-dev.0 (c (n "gear-core-errors") (v "1.0.3-dev.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "11092hgnb5bvpkbsgj4acyzpvj9az38k8f8700vb9dhqkg5as4q7") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.3 (c (n "gear-core-errors") (v "1.0.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0mjpm6lmmnm216x2vi7sbz4yn5wm3kgabrbhrpf5c3r9x0bpkx46") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.4-rc.0 (c (n "gear-core-errors") (v "1.0.4-rc.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "13axg02z3lfzjlgycyn2hqiz449bjsgz32a49m67id5b2ayjn3cb") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.4 (c (n "gear-core-errors") (v "1.0.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1z6ywa8ml1sqiskcb3j8dfisy9bp8khlj1jqanyqhvzwy3zrsw8q") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.0.5 (c (n "gear-core-errors") (v "1.0.5") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "18rc83p1q2mljvafhyw0ilpiaviskdrrqz2d8b66shs98k55yvaa") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.1.0 (c (n "gear-core-errors") (v "1.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0d14m7wxr28pjm0sj4wkvczfw5b9rdpxa1gy1rx711l1i73bmp0c") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.1.1-rc.0 (c (n "gear-core-errors") (v "1.1.1-rc.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0wkirmc0kq5yhcriz565nhky8h7vnvg2rkhfql5h5lx1881px8vy") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.1.1 (c (n "gear-core-errors") (v "1.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0982w0lbjlwjgd85f57vav37ipzqikrhi7c7j3y34h94rq5jiz4y") (f (quote (("codec" "scale-info"))))))

(define-public crate-gear-core-errors-1.2.0 (c (n "gear-core-errors") (v "1.2.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "108sac27xjp7dc7g2zg7qd83bik3kpckqfa7fnap6ccyg080m4kx") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.0 (c (n "gear-core-errors") (v "1.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1fvdfr13lpi1qmgjqg3ij0bh66ahsb17hqvkhr0r6wrjh7l3wa9n") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.2.0-pre1 (c (n "gear-core-errors") (v "1.2.0-pre1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1irrf2pcrfgfapkiy05khw4v8x2q2yyyfkidhczwr75r4509ggfb") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.2.0-pre.2 (c (n "gear-core-errors") (v "1.2.0-pre.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "0vwqhikrapfpv4d439ky6saxk25qyzp652yzy35ka888ssxwhrlb") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.1-pre.1 (c (n "gear-core-errors") (v "1.3.1-pre.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1946ad1sjnv8cdwppydxiik4qyawww8n0d70pjyg8vfzkih16vbx") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.1-pre.2 (c (n "gear-core-errors") (v "1.3.1-pre.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "16byqp82113pr87n1as29djdjph652yyy6ic6izc0yqww81l0xkr") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.1-pre.3 (c (n "gear-core-errors") (v "1.3.1-pre.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "09mf1w0xknynyacs5ycxii2ncgrcm69mndxrn3k37kpx3kbk5y9s") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.2.0-pre.3 (c (n "gear-core-errors") (v "1.2.0-pre.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "11caq7fx6hx7kjw47xv5w6halln0pzqn6z9zxm0hx0bm6bq1yqch") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.2.1 (c (n "gear-core-errors") (v "1.2.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1k57rpmnr7apxic92xza6zkvmxiwv51c5vhj10b2yr0bq4rahmzs") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.0-pre.1 (c (n "gear-core-errors") (v "1.3.0-pre.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1xw52v5fy9yr44bzwaawjqljqdcb2jfgrk85jqhjpgn7hsvymx1n") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.3.1 (c (n "gear-core-errors") (v "1.3.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "1b0df4hii0bs4x9lh5j12gj2gi2wima8dxa5fv96wfl0lgnsk3wn") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.4.0 (c (n "gear-core-errors") (v "1.4.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "18ihl9jlklx349gz6w660b7m38hfm4fj7yhsr8ps1np4rdzswrr0") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-gear-core-errors-1.4.1 (c (n "gear-core-errors") (v "1.4.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (o #t) (d #t) (k 0)))) (h "05i48fyx4l5q1d12abzp1panqnx36657hpq18m89v0kw725alfzf") (f (quote (("codec" "scale-info")))) (s 2) (e (quote (("serde" "dep:serde"))))))

