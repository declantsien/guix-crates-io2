(define-module (crates-io ge ar gearhash) #:use-module (crates-io))

(define-public crate-gearhash-0.1.0 (c (n "gearhash") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1f1pphvz0dsbm5cvjnaaq09idin0h9c7bhalcz3dcr9l7d6pficp") (f (quote (("bench"))))))

(define-public crate-gearhash-0.1.1 (c (n "gearhash") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0k5bdd59wk62rr9bkb4l7cnpam05nr0s6ljagrdcgzwpqfrxnghr") (f (quote (("bench"))))))

(define-public crate-gearhash-0.1.2 (c (n "gearhash") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0g1myw80jj7mkdl5dac69fmzh5v65gfqhwnyclkqfzikp6n71mny") (f (quote (("bench"))))))

(define-public crate-gearhash-0.1.3 (c (n "gearhash") (v "0.1.3") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "04yj8ni60v8756qfc2qsky671kkmqxvi6ni9arg4h5ndfv7q5ky8") (f (quote (("bench"))))))

