(define-module (crates-io ge ar gears-cli) #:use-module (crates-io))

(define-public crate-gears-cli-0.1.1 (c (n "gears-cli") (v "0.1.1") (d (list (d (n "clap") (r "~2.25.0") (f (quote ("suggestions" "color" "wrap_help"))) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "gears") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "18jgpagmdfq41hvkq8n00kdp71jmr06qa9wsxi2597fiihpfc80k")))

