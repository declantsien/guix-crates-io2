(define-module (crates-io ge ar gear-core-backend-codegen) #:use-module (crates-io))

(define-public crate-gear-core-backend-codegen-1.0.5 (c (n "gear-core-backend-codegen") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0sq51lhvxmgnn4dzascqp8ma301si38ff376clkci86b7x0bc81k")))

