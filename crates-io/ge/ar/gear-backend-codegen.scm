(define-module (crates-io ge ar gear-backend-codegen) #:use-module (crates-io))

(define-public crate-gear-backend-codegen-0.0.0 (c (n "gear-backend-codegen") (v "0.0.0") (h "0cl85148bnvbxchw6hrh0yqgkbpj0zbwz7943fdm2bkn13xnaacd")))

(define-public crate-gear-backend-codegen-0.1.0 (c (n "gear-backend-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1lsj36scw1slxw1sxzglhkwawmmjrka52chh637776igc1mg9npf")))

(define-public crate-gear-backend-codegen-0.2.1-alpha.0 (c (n "gear-backend-codegen") (v "0.2.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "11bpp168i79njljm6idbh5b8yms31v4bvv37a5rbidd6kpcqv8wr")))

(define-public crate-gear-backend-codegen-0.2.2 (c (n "gear-backend-codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "16mmxlrl7g7qhl3wj5h35v40v8694hzm3hcfb3a7nxx1r53i9fr0")))

(define-public crate-gear-backend-codegen-0.2.1-alpha.1 (c (n "gear-backend-codegen") (v "0.2.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "08dgra32k67mjbnaw6qlkbqa94fz6l1hs0x2csd4q7pxzcsm6jrw")))

(define-public crate-gear-backend-codegen-0.2.1-alpha.2 (c (n "gear-backend-codegen") (v "0.2.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "0c2jngm6pnjlpg5zvckd4nwv2da3iwwfv4iv9a46w2fgr3g4l81y")))

(define-public crate-gear-backend-codegen-0.3.1-alpha.0 (c (n "gear-backend-codegen") (v "0.3.1-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "0cj33jb57aa36djcpasdlly03xkajc99vgslqsh02i2dfbkzpwc4")))

(define-public crate-gear-backend-codegen-0.3.1-alpha.1 (c (n "gear-backend-codegen") (v "0.3.1-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "1drpalms5d29yrfgfn4jmr46m2a2jpa7cv8iccqi4rsp0wlp0smv")))

(define-public crate-gear-backend-codegen-0.3.1-alpha.2 (c (n "gear-backend-codegen") (v "0.3.1-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "0lwv26gzw10613cpyvsjc5qh359vv9rwc2gprqsk3lpdfn4apccs")))

(define-public crate-gear-backend-codegen-0.3.2 (c (n "gear-backend-codegen") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("default" "full" "fold"))) (k 0)))) (h "078ybq7639zmscxn1k82hvqgmlahqxjb2l8b4b9s6y7xknpb696a")))

(define-public crate-gear-backend-codegen-0.3.2-alpha.1 (c (n "gear-backend-codegen") (v "0.3.2-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full" "fold"))) (k 0)))) (h "1f7i7s3yrnlxvk8g4abs7m21l036q7m83izfcghgl2q1qack7hb3")))

(define-public crate-gear-backend-codegen-0.3.2-alpha-2 (c (n "gear-backend-codegen") (v "0.3.2-alpha-2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full" "fold"))) (k 0)))) (h "1iwr2fq6pdr84ff60dpwi4vfy4dhw3x0gnajs62sb1ywnqsh9nvj")))

(define-public crate-gear-backend-codegen-0.3.2-alpha.3 (c (n "gear-backend-codegen") (v "0.3.2-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full" "fold"))) (k 0)))) (h "16akbjlnyd13azc6gsksgawnpxnpc566w326ydjazqk5m1sr8d8x")))

(define-public crate-gear-backend-codegen-0.3.2-alpha.4 (c (n "gear-backend-codegen") (v "0.3.2-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("default" "full" "fold"))) (k 0)))) (h "03fpxffbjfkj5ql6ydyhwsidx0brw845av7k6wq1rgj8i55sli5q")))

