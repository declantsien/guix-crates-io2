(define-module (crates-io ge ar gear-wasm) #:use-module (crates-io))

(define-public crate-gear-wasm-0.45.0 (c (n "gear-wasm") (v "0.45.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1j95q7jg39cplpdsz6sn4gfl7v2jvcvlwmnm06gm1x33n6gfsigp") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

(define-public crate-gear-wasm-0.45.1 (c (n "gear-wasm") (v "0.45.1") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "0nyhkb1psqr48z8pmx5xmhs2dwnfdc24zcigrlznhpn63mqgmyxv") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

