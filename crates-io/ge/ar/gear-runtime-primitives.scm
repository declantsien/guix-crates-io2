(define-module (crates-io ge ar gear-runtime-primitives) #:use-module (crates-io))

(define-public crate-gear-runtime-primitives-0.0.0 (c (n "gear-runtime-primitives") (v "0.0.0") (h "00k9izkxs6g5nk82plcy869ql2fhcry1329pg2s5blrxjzc46w2h")))

(define-public crate-gear-runtime-primitives-1.0.3 (c (n "gear-runtime-primitives") (v "1.0.3") (d (list (d (n "sp-core") (r "^7.0.0") (k 0)) (d (n "sp-runtime") (r "^7.0.0") (k 0)))) (h "0gsp9j1awjxvy26rfra4yw146ga1sqwj2j66lxmh13b705gj49z5") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.0.4-rc.0 (c (n "gear-runtime-primitives") (v "1.0.4-rc.0") (d (list (d (n "sp-core") (r "^21.0.0") (k 0)) (d (n "sp-runtime") (r "^24.0.0") (k 0)))) (h "12zhk75bh86d4sirbcql2227gbmfsv00ifqfi39wimrx22iqnbhg") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.0.4 (c (n "gear-runtime-primitives") (v "1.0.4") (d (list (d (n "sp-core") (r "^21.0.0") (k 0)) (d (n "sp-runtime") (r "^24.0.0") (k 0)))) (h "1ikjmsxcbkajdxvynl3851x41fsl72anbwx8yb2bxl9nn6z7z5ms") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.0.5 (c (n "gear-runtime-primitives") (v "1.0.5") (d (list (d (n "sp-core") (r "^21.0.0") (k 0)) (d (n "sp-runtime") (r "^24.0.0") (k 0)))) (h "05k45zxsl9ak1865w46whnmz4s8qdamljx7l7wg12kvykalxm5l4") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.1.0 (c (n "gear-runtime-primitives") (v "1.1.0") (d (list (d (n "sp-core") (r "^21.0.0") (k 0)) (d (n "sp-runtime") (r "^24.0.0") (k 0)))) (h "1rjycmxrvyd5h3177w24z5haw3dg4b792n45m3j987f9lg9jzmvy") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.1.1-rc.0 (c (n "gear-runtime-primitives") (v "1.1.1-rc.0") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "0q6ph04b6sql4y0bkdy7vbmzls1vnkp17aaaiwhi8pgrj6klj56p") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.1.1 (c (n "gear-runtime-primitives") (v "1.1.1") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "02wh48vac6rbn9f129dglk3iqv02ksn2i9rs8ks0abjz8pvdkidl") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.2.0-pre.2 (c (n "gear-runtime-primitives") (v "1.2.0-pre.2") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "1liq1chgbgrq2l965yvqi0w4f79ps9x6cvb7gkacp3qalq9a4wzf") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.3.1-pre.2 (c (n "gear-runtime-primitives") (v "1.3.1-pre.2") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "0c9ylcx1km7imjkb1a74fyiqx04spk4l7scnwcrjfvj3b9qjf396") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.3.1-pre.3 (c (n "gear-runtime-primitives") (v "1.3.1-pre.3") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "1zxj15h347cd8s12gzkslba5zx0zz5z6szqgniklbp8i6nb656xi") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.2.0-pre.3 (c (n "gear-runtime-primitives") (v "1.2.0-pre.3") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "1js1d2aajbk63mkjrba0zdmpvkqan9h4vgkcs7mcvwnkpad1waaj") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.2.0 (c (n "gear-runtime-primitives") (v "1.2.0") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "1il4q6n4vsqdrkzzd2blg26zali48xrgw4jscjpy9dwzw4w6nnkr") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.2.1 (c (n "gear-runtime-primitives") (v "1.2.1") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "19x24bwjk223q4zwr6xmlfhlmdslfszlznrnzp48i1bf0lpb47ni") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.3.0-pre.1 (c (n "gear-runtime-primitives") (v "1.3.0-pre.1") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "174a2d3317ki32v7c2szxmcfy7fm4y52zj021i5cy6miz54fza20") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.3.0 (c (n "gear-runtime-primitives") (v "1.3.0") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "1mn4m7y4287f1y8ppxk01y5lhg6j3k832njdc48v9ws7ml17lvl6") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.3.1 (c (n "gear-runtime-primitives") (v "1.3.1") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "0z92bwvlacpmh9pk6hpsb0vjlz64qgx9b3blsl6pdy4qhgagp8hs") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.4.0 (c (n "gear-runtime-primitives") (v "1.4.0") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "024s03flngr26f5xg9jfsyy51a4m4h4rjnc6anbnwdirap7c20vf") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-gear-runtime-primitives-1.4.1 (c (n "gear-runtime-primitives") (v "1.4.1") (d (list (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "12h7d51r5wvq5cdk6pdqh5jzfffn9jzhvk5fb14isdn05lfidqv1") (f (quote (("std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

