(define-module (crates-io ge ar gear-objects) #:use-module (crates-io))

(define-public crate-gear-objects-0.1.0 (c (n "gear-objects") (v "0.1.0") (d (list (d (n "arraystring") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)))) (h "078q0aq3afq94d6l0gqrancvs041k14apqz3zjmxabd5lbj53kv5")))

(define-public crate-gear-objects-0.2.0 (c (n "gear-objects") (v "0.2.0") (d (list (d (n "arraystring") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)))) (h "0hjhvrw90f0b7pj9cs9pysnmvswqsjj8zvbd77nj01bwzc9vvbgy")))

(define-public crate-gear-objects-0.3.0 (c (n "gear-objects") (v "0.3.0") (d (list (d (n "arraystring") (r "^0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "colored") (r "^2") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)))) (h "12rd6vxajks92c09r77lbhx7rrczcj552j4pfa1qiz655mbfhp9p")))

