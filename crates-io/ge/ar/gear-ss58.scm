(define-module (crates-io ge ar gear-ss58) #:use-module (crates-io))

(define-public crate-gear-ss58-1.4.0 (c (n "gear-ss58") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "bs58") (r "^0.5.1") (f (quote ("alloc"))) (k 0)))) (h "0kz8g2bhx68x435fmny9s96bc9sy5232g6mbl10qabbwix460qrn")))

(define-public crate-gear-ss58-1.4.1 (c (n "gear-ss58") (v "1.4.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "bs58") (r "^0.5.1") (f (quote ("alloc"))) (k 0)))) (h "0n1qj1by0rlbq2lj4cv6scl2553z6cas7l0gg8v1qjb874bq52mf")))

