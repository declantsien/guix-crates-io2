(define-module (crates-io ge ar gear-fingerprinter) #:use-module (crates-io))

(define-public crate-gear-fingerprinter-0.0.0 (c (n "gear-fingerprinter") (v "0.0.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 2)))) (h "1iy7xfc8nr0vvvc6ig9dl9375mfiapg9k2rav2baq9yxp1rxbga0")))

