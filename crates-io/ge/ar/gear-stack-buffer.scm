(define-module (crates-io ge ar gear-stack-buffer) #:use-module (crates-io))

(define-public crate-gear-stack-buffer-1.0.2-354d660.8 (c (n "gear-stack-buffer") (v "1.0.2-354d660.8") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "16fnf3dadrj6kgixlglzy89gwasjn3jqphkyq4fhl4nlcj77m15x") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.2-354d660.9 (c (n "gear-stack-buffer") (v "1.0.2-354d660.9") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1hia1fafzkg96g8xdkqi87vl3wdys57xvzqwh342q9bspksxfh9j") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.2-354d660.10 (c (n "gear-stack-buffer") (v "1.0.2-354d660.10") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1hd58hqx64fj3fn6ismpfw8f9sw7qn8i58cksnndr5xrqdi7n9k6") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.2 (c (n "gear-stack-buffer") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1ypafinb2brpn01zh93x46zgh5qcshwd964r1478g9gpdcar20vj") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.2-gtest-dev (c (n "gear-stack-buffer") (v "1.0.2-gtest-dev") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "19vn5ff3rfrlvik77zbf2yxy2pw0nhal26v3ixqrpscsk6px9bii") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.2-dev.0 (c (n "gear-stack-buffer") (v "1.0.2-dev.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0f8xj3ixvcqm4ag0v6l1iws0f03v5gjb1lgfq8hah3vx3fci57az") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.3 (c (n "gear-stack-buffer") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0xhjv9bxrkszmp0g4r12n2n3f59xhacy2fx5lw38j43p6rsmmas0") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.4-rc.0 (c (n "gear-stack-buffer") (v "1.0.4-rc.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0ckn61d1x2mpspk9r97l4malxcpknc27y1c26kasanfr6dyyc690") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.4 (c (n "gear-stack-buffer") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "04kynxlj0y9apn5ralp2arjggqh7fcjnfrsbz89x5y43bn92a2k0") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.0.5 (c (n "gear-stack-buffer") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1hg8yp7gkawjs2p3m9cazzhlyx1syhxgjihpmxm3640y1gfb025c") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.1.0 (c (n "gear-stack-buffer") (v "1.1.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0yn0sgkx7v20ck6073prv2xx69y52c8ndb392wsh1b7dgl421pnr") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.1.1-rc.0 (c (n "gear-stack-buffer") (v "1.1.1-rc.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0ncxspp0v5ygsj6ws10bbyblww0qzyf3n3azk425bd60zakcj2za") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.1.1 (c (n "gear-stack-buffer") (v "1.1.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1i468dvngpr7cvqyc2mx1jqzckm2bxsj969ls8q0zd483f7awwai") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.2.0 (c (n "gear-stack-buffer") (v "1.2.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1jirbgzjc5k5853akwfdpg1lqkf4ng2kk3qkdl4h6yj6iz1b5zxd") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.0 (c (n "gear-stack-buffer") (v "1.3.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1v41c0il7ak32hgyqswi0ji11wljvxfj55b9cjzap8d9miimpa5x") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.2.0-pre1 (c (n "gear-stack-buffer") (v "1.2.0-pre1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0azidns02lbjbhvm9cqh3ww3ak19446jmv7aq2i82l1mjw97xngl") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.2.0-pre.2 (c (n "gear-stack-buffer") (v "1.2.0-pre.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "00sc9fpr2nhhch7wz84wxmnlxbgd4gafhpbkzzhwsmb32ghxg0id") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.1-pre.1 (c (n "gear-stack-buffer") (v "1.3.1-pre.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1k5laj3r8vdfr0smfh4an9yqyx99j4r9v2b2r1y2bw3nvvskgm8n") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.1-pre.2 (c (n "gear-stack-buffer") (v "1.3.1-pre.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0hmrmbz721gygm1f02vhq0wyszx0f677cya6l8h6faz4p6h4ix14") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.1-pre.3 (c (n "gear-stack-buffer") (v "1.3.1-pre.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1g1g11rr9snk1gj9fqh05q1s30wzn46rrislvmx4m6w715xm2gyl") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.2.0-pre.3 (c (n "gear-stack-buffer") (v "1.2.0-pre.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "09xx6m76bdgpb1slqr08id2s2v5ml93i0f9xv8j6vxlf8nz2lfpx") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.2.1 (c (n "gear-stack-buffer") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "19vq6jv4r8skf5sk418i7cwqdg80hjxk780vzbhz5fsw3rdnqb6a") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.0-pre.1 (c (n "gear-stack-buffer") (v "1.3.0-pre.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1y6izi9i06b5zhv5wilfc08av6fha4sksvq8wq8ad1gkzwsqsvqz") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.3.1 (c (n "gear-stack-buffer") (v "1.3.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1x9n0l20wma2j29jm1lxwy3rr3g3wlmwjy98n0xysdkc1q4kd2qn") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.4.0 (c (n "gear-stack-buffer") (v "1.4.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "0spbkak6kk1vpaw8fr4zvpqj3bq6bb5rhcapz77glc6adwhl8k2f") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

(define-public crate-gear-stack-buffer-1.4.1 (c (n "gear-stack-buffer") (v "1.4.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)))) (h "1khglp1nb34jmvl4wvjl3n9r8d6bw3f3wv16jscr9wi6mgs9zi3q") (f (quote (("stack-clash-protection") ("compile-alloca" "cc"))))))

