(define-module (crates-io ge ar gear_combos) #:use-module (crates-io))

(define-public crate-gear_combos-1.0.0 (c (n "gear_combos") (v "1.0.0") (h "18hhg5mr5kkizhy40r0ckyx04bvghgbng66rrllga5xm4kgh18gk") (y #t)))

(define-public crate-gear_combos-1.0.1 (c (n "gear_combos") (v "1.0.1") (h "0qq6ynp6pxrsx1m85p4ykvg1p39ixkq0s6pqhlkjjrjwvydkkjc6") (y #t)))

(define-public crate-gear_combos-1.0.2 (c (n "gear_combos") (v "1.0.2") (h "0bgnxjhvj3isnlgpi161wvhd0azwgyjh1xlvpynn6ps3hkqxw3mg") (y #t)))

(define-public crate-gear_combos-1.0.3 (c (n "gear_combos") (v "1.0.3") (h "1fm9hvwnh22v3qsxb22k6x8mifx6b6m60ldsqyfyiz9l56mcx6l0") (y #t)))

(define-public crate-gear_combos-1.0.4 (c (n "gear_combos") (v "1.0.4") (h "13j2krzppj19q1cylnhidz45asr30jhi0dxsbyg360kxqwjkwrdg")))

