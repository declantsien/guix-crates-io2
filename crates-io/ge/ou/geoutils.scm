(define-module (crates-io ge ou geoutils) #:use-module (crates-io))

(define-public crate-geoutils-0.1.0 (c (n "geoutils") (v "0.1.0") (h "0ik3bj20djl16r4pnngpaazvxd2nwgf830bcykm2a4hf7bsh0s6m")))

(define-public crate-geoutils-0.2.0 (c (n "geoutils") (v "0.2.0") (h "0fm9fkvvrz09nj70iml2wk7nrdfh3b7wp8nafrca5iywy5psvzrb")))

(define-public crate-geoutils-0.3.0 (c (n "geoutils") (v "0.3.0") (h "0qc35ylkrpazgvizxzwgplxk3dd100jy9cpv5lrarxh48w6j58xx")))

(define-public crate-geoutils-0.4.0 (c (n "geoutils") (v "0.4.0") (h "1gq013x065pffmwjcql7c0bsaxlkmkqv6s8gzra2wz92g70cada6")))

(define-public crate-geoutils-0.4.1 (c (n "geoutils") (v "0.4.1") (h "07nx8mv8v4i9s6dhg65hp2fihwzd8czvnbhxrrm3jza0d9hny04y")))

(define-public crate-geoutils-0.5.0 (c (n "geoutils") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q57qssrdzkg0ms6dlxiz0k7147wjsrw081kzpccyj5cyw3m5h9n") (f (quote (("default"))))))

(define-public crate-geoutils-0.5.1 (c (n "geoutils") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bd6nzrk85y1qdx7wvf4xqajswr5g65jpbdbpig9nc8kh6h49lin") (f (quote (("default"))))))

