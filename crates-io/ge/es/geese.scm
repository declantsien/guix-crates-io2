(define-module (crates-io ge es geese) #:use-module (crates-io))

(define-public crate-geese-0.1.0 (c (n "geese") (v "0.1.0") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1l3k1x6f6rzdc0cmvfr5dcpxcblpzyaqkw0l7zws4h2rbz79rmgp")))

(define-public crate-geese-0.1.1 (c (n "geese") (v "0.1.1") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1ypgwglnhzmcq8gfrv0cgk7ia36rfarzy46q99ss2azkjf04fr63")))

(define-public crate-geese-0.1.2 (c (n "geese") (v "0.1.2") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "12ll9yvg32n9m3indbcw7vr4ydh0wjjwnjd1by82r4mvkamg1hv9")))

(define-public crate-geese-0.1.3 (c (n "geese") (v "0.1.3") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1qygnjmsy7r6ykazg1i5pvnarxap328728z8fadyvsfg4fql897k")))

(define-public crate-geese-0.1.4 (c (n "geese") (v "0.1.4") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0kccsz2ljm3wixidmjb5v9vg9zp3rnn044v77r2iqnsw3phb0nmp")))

(define-public crate-geese-0.1.5 (c (n "geese") (v "0.1.5") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1n7rs7mh67cya2ad61kviqdvls4xl617px7pb1krdqlpl9gl1ji4")))

(define-public crate-geese-0.1.6 (c (n "geese") (v "0.1.6") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0b16srl7rhrgc5rnb0gfdqpaajxxd4d8wfmws9lxf5v4nblysj3l")))

(define-public crate-geese-0.1.7 (c (n "geese") (v "0.1.7") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1jclyjvsqspx2q7mb9xy3pla5s89pg8fsjyypb0klk9z7qbidgji")))

(define-public crate-geese-0.1.8 (c (n "geese") (v "0.1.8") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1k2b9xd8crd9jjd720xjz6p20mkwyx2y7gz3y7wbaw0f71yzsl24")))

(define-public crate-geese-0.1.9 (c (n "geese") (v "0.1.9") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1i7aaah11pa78sr1dn8vfrcwzav8zylxybmzlgr3hpv0dazbja6r")))

(define-public crate-geese-0.1.10 (c (n "geese") (v "0.1.10") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0caclphy894m5bv0yi360lrrifc84qbxdgr4jqml43mlrgnnk8sr")))

(define-public crate-geese-0.1.11 (c (n "geese") (v "0.1.11") (d (list (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "1lvhp0m42g6315j6im26bp40nknky5jhzal5n9xzgl2aa2p36mzp")))

(define-public crate-geese-0.1.12 (c (n "geese") (v "0.1.12") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0p8hpi75wyz9qqmrzirxdqw3abx4q654nw0l13gkpp91s1n9idh8")))

(define-public crate-geese-0.1.13 (c (n "geese") (v "0.1.13") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "0wdqxxwmrcq1l8bbha4vcrxib6xnqrv9wmmvlhzdi0xykwwdfxx8")))

(define-public crate-geese-0.1.14 (c (n "geese") (v "0.1.14") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "13nk9snp161nf0j24wzyi2rx5ahfz9a1d5737gqlkslcym841f6x")))

(define-public crate-geese-0.1.15 (c (n "geese") (v "0.1.15") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "00v0pwvgsrgw7nhrxmri1v9880nyakdfhfsnq1jdqn0qg0i7iywb")))

(define-public crate-geese-0.2.0 (c (n "geese") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r ">=1.16") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)))) (h "008x7g6p2na19640agd3iwj820q6s0p6b2jas1szrdy24b9yjq4j")))

(define-public crate-geese-0.3.0 (c (n "geese") (v "0.3.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0r99fx2bjcvqxkg68vdgkcw9l83aql9rdcv2ili9ks438b7hi3qa")))

(define-public crate-geese-0.3.1 (c (n "geese") (v "0.3.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "09md13ss1r2ncpzaypqr6l0jvmar0i40k5ncljgvxcqxh0mnwwwl")))

(define-public crate-geese-0.3.2 (c (n "geese") (v "0.3.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "00sqk4zv9fa49f80mnparb6kpbc2y0sh1c4d7scjfjz0f0cyp7hp")))

(define-public crate-geese-0.3.3 (c (n "geese") (v "0.3.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0vmfpkyfjh5n3igx5s7a9hwvdz4qdnrc4scd7gc2akr3f7dfzl5x")))

(define-public crate-geese-0.3.4 (c (n "geese") (v "0.3.4") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "036qmy69l1nahryix3581ndhmpwydanx4d0j408f1rwvqhhik67x")))

(define-public crate-geese-0.3.5 (c (n "geese") (v "0.3.5") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "const_list") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2.2") (d #t) (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (d #t) (k 0)) (d (n "wasm_sync") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm_thread") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0a5qibqzg8r989bl545ns047mrjyh3y7310w8ccfj9jwll86wgj6")))

(define-public crate-geese-0.3.6 (c (n "geese") (v "0.3.6") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "macroquad") (r "^0.4.2") (d #t) (k 2)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (k 0)) (d (n "topological-sort") (r "^0.2.2") (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (k 0)) (d (n "wasm_sync") (r "^0.1.0") (k 0)) (d (n "wasm_thread") (r "^0.2.0") (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0vpaw0y6ic7jqlqg6xhspid7d7a0dvmilww2db39iwz6ayygi7i3")))

(define-public crate-geese-0.3.7 (c (n "geese") (v "0.3.7") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "macroquad") (r "^0.4.2") (d #t) (k 2)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (k 0)) (d (n "topological-sort") (r "^0.2.2") (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (k 0)) (d (n "wasm_sync") (r "^0.1.0") (k 0)) (d (n "wasm_thread") (r "^0.2.0") (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1r7i8yrdq78gqvcxad5qbjrnd5ii678n5q5dh2074xviq8jjdd66")))

(define-public crate-geese-0.3.8 (c (n "geese") (v "0.3.8") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "macroquad") (r "^0.4.2") (d #t) (k 2)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (k 0)) (d (n "topological-sort") (r "^0.2.2") (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (k 0)) (d (n "wasm_sync") (r "^0.1.0") (k 0)) (d (n "wasm_thread") (r "^0.2.0") (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1gliylavkfj875gabwmjl6n5mw3gpzi937fzlh36fzd44clzn73n")))

(define-public crate-geese-0.3.9 (c (n "geese") (v "0.3.9") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "macroquad") (r "^0.4.2") (d #t) (k 2)) (d (n "old_geese") (r "^0.2.0") (d #t) (k 2) (p "geese")) (d (n "rustversion") (r "^1.0.14") (d #t) (k 1)) (d (n "smallvec") (r "^1.11.0") (k 0)) (d (n "topological-sort") (r "^0.2.2") (k 0)) (d (n "vecdeque-stableix") (r "^1.1.1") (k 0)) (d (n "wasm_sync") (r "^0.1.0") (k 0)) (d (n "wasm_thread") (r "^0.2.0") (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1lzhiipbip5y26nyrgzcyj6ql0m8nm32vy44z11w1izvzlqxjqsp")))

