(define-module (crates-io ge es geese_executor) #:use-module (crates-io))

(define-public crate-geese_executor-0.1.0 (c (n "geese_executor") (v "0.1.0") (d (list (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 0)) (d (n "geese") (r "^0.1.12") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "1rmc4r36xy2rwc2r4dz1jsd61ch8zzmvh1qfgdyblw6xbqc32x3i")))

(define-public crate-geese_executor-0.1.1 (c (n "geese_executor") (v "0.1.1") (d (list (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 0)) (d (n "geese") (r "^0.2.0") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "128c097br1s16q80b7rxkw2d2flmqwzp30jq04fhxqz5qjyhaazc")))

(define-public crate-geese_executor-0.1.2 (c (n "geese_executor") (v "0.1.2") (d (list (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 0)) (d (n "geese") (r "^0.2.0") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "0fl8c8c20v0glpa0c9lb2yqf85fc11y5b2zn480mgkaxh43gxlmg")))

(define-public crate-geese_executor-0.1.3 (c (n "geese_executor") (v "0.1.3") (d (list (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 0)) (d (n "geese") (r "^0.3.2") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "1812v6fmnl1235ihc6flsr829lqmcknvy0d2s89mfsmm03q58wlj")))

