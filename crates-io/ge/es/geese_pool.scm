(define-module (crates-io ge es geese_pool) #:use-module (crates-io))

(define-public crate-geese_pool-0.1.0 (c (n "geese_pool") (v "0.1.0") (d (list (d (n "geese") (r "^0.1.10") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "1h93w66zw9c6ppm8z10ima43llc08ajwhll50lsm7d4x6f36ahk9")))

(define-public crate-geese_pool-0.1.1 (c (n "geese_pool") (v "0.1.1") (d (list (d (n "geese") (r "^0.2.0") (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "0xrq9ngczmpgjj4a7cqbgvvg83sha9snaii8nylfsanv22s2j395")))

(define-public crate-geese_pool-0.2.0 (c (n "geese_pool") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "geese") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "02x836796yxxk12vmrigxs9g2dnf7b8rdwq4vbnxqcbdd6n1liyk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-geese_pool-0.3.0 (c (n "geese_pool") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "geese") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "takecell") (r "^0.1.1") (d #t) (k 0)))) (h "18326z56l0agflr9fkb9ipwikm0vfh3vdzbjxq9kvr0ly0sz0dlh") (s 2) (e (quote (("serde" "dep:serde"))))))

