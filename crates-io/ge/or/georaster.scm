(define-module (crates-io ge or georaster) #:use-module (crates-io))

(define-public crate-georaster-0.1.0 (c (n "georaster") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "http-range-client") (r "^0.7.2") (d #t) (k 2)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "tiff") (r "^0.9.1") (d #t) (k 0)))) (h "0p0xxcawqfws1wxr80shq790f4lv40bsvd1gsrjc1175mn5i9wjv")))

