(define-module (crates-io ge ck gecko) #:use-module (crates-io))

(define-public crate-gecko-0.1.0 (c (n "gecko") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08wiwlf8yvkvbjzd52cph6s8a3az1wzydrhq9pkm59b4jc0h1lg0") (y #t)))

(define-public crate-gecko-0.1.1 (c (n "gecko") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0734qlvfnm8rp2ms5lk7xb1h7r2vnhni2aarv6zkk15c9qpya74f") (y #t)))

(define-public crate-gecko-0.1.2 (c (n "gecko") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03200h215zvcava984mnha8xcnv929k1rqjk6vg9wbwn2a6iy3j6")))

(define-public crate-gecko-0.1.3 (c (n "gecko") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1igm2wasv5rq87hzi90p8pmyxpsm8vrmgj6rgfr2p8hpzcj429rj")))

(define-public crate-gecko-0.1.4 (c (n "gecko") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wbwsmzz6iamfifkk5mfyd38vhizp7q7bp50lllbvbmbicx1a2bn")))

(define-public crate-gecko-0.1.5 (c (n "gecko") (v "0.1.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gznz8nnzd7ffn6dxss72248k8iaf5mp3r3aqkjly3l1imlghva0")))

(define-public crate-gecko-0.1.6 (c (n "gecko") (v "0.1.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06fb302v1vqlw35554lqfv0nq38b7dxy1g7sg9yzvrgkb6706l18")))

