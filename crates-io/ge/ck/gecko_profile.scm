(define-module (crates-io ge ck gecko_profile) #:use-module (crates-io))

(define-public crate-gecko_profile-0.1.0 (c (n "gecko_profile") (v "0.1.0") (d (list (d (n "debugid") (r "^0.7.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0g36jk93c29r4lz5q0maw4kzi6xv33bjhd21llfv60lcma76lgir")))

(define-public crate-gecko_profile-0.1.1 (c (n "gecko_profile") (v "0.1.1") (d (list (d (n "debugid") (r "^0.7.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "17bqm41hbcxwfab2irnazsac0sdzwad9v648jy4myljbaslj6p0l")))

(define-public crate-gecko_profile-0.2.0 (c (n "gecko_profile") (v "0.2.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zyabx68spziss74i5bvi1p910mh9axzx2ba3xjaqizfajinb75s")))

(define-public crate-gecko_profile-0.3.0 (c (n "gecko_profile") (v "0.3.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d7z1m0jbmadg7q1rs1ainx2mxra5vqnjhmx143z5rv7nlv9zxfs")))

(define-public crate-gecko_profile-0.4.0 (c (n "gecko_profile") (v "0.4.0") (d (list (d (n "assert-json-diff") (r "^2.0.1") (d #t) (k 2)) (d (n "debugid") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sjc9gjmnwflwif5hbs93grgpr1n08g6np9jirswcaz0w73m4249")))

