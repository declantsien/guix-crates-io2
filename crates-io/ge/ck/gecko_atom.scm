(define-module (crates-io ge ck gecko_atom) #:use-module (crates-io))

(define-public crate-gecko_atom-1.0.0 (c (n "gecko_atom") (v "1.0.0") (d (list (d (n "heapsize") (r "^0.3.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18knyxms441fxxz5zcgmpad78fn8irsf5lc07y3hz55az269fw1a") (f (quote (("unstable") ("log-events") ("heap_size"))))))

