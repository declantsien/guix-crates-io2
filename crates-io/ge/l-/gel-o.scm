(define-module (crates-io ge l- gel-o) #:use-module (crates-io))

(define-public crate-gel-o-0.0.1 (c (n "gel-o") (v "0.0.1") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "inotify") (r "^0.8.2") (k 0)))) (h "1hgv4ngp47cjayn74yrb7na4ngcs4iivmfs07zgpmvnrvrbxm1mp")))

(define-public crate-gel-o-0.0.2 (c (n "gel-o") (v "0.0.2") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "inotify") (r "^0.8.2") (k 0)))) (h "1is15malfa2gssxf8nab869pbf55nlkmwirr7hfrqm7j8m0lflzg")))

(define-public crate-gel-o-0.0.3 (c (n "gel-o") (v "0.0.3") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "inotify") (r "^0.8.2") (k 0)))) (h "1zfjc1hm3w7m6lkrn2wb6xz2l1icgaf6vyz0qv7827wnnq1yb64f")))

