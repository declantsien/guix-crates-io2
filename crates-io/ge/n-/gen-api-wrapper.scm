(define-module (crates-io ge n- gen-api-wrapper) #:use-module (crates-io))

(define-public crate-gen-api-wrapper-0.1.2 (c (n "gen-api-wrapper") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_urlencoded") (r "~0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "15y4ymwcj9ynqd7z8zhs4jc0n24ja29vamj6nnn7kwlks2bp22br") (r "1.65.0")))

