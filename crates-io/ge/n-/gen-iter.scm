(define-module (crates-io ge n- gen-iter) #:use-module (crates-io))

(define-public crate-gen-iter-0.1.0 (c (n "gen-iter") (v "0.1.0") (h "0s6avxdsb6g3lrgsw2msxzhb2y2d02ga3sc1bzf319gapymig6xi")))

(define-public crate-gen-iter-0.1.1 (c (n "gen-iter") (v "0.1.1") (h "0xwsrp28nm0ncjnc4bflh877m0n31wqkyc0yjrpldzxqnyrmii8f")))

(define-public crate-gen-iter-0.1.2 (c (n "gen-iter") (v "0.1.2") (h "1yrzsxa5qf4llpa63g9lw9pj8md39i3jjab194cfxm4d2q80h79j")))

(define-public crate-gen-iter-0.2.0 (c (n "gen-iter") (v "0.2.0") (h "19i4cg94km4di3rr4ay7pfpy8c0vrvy7v2bw1kzxjjckfrzaiq4l")))

(define-public crate-gen-iter-0.2.1 (c (n "gen-iter") (v "0.2.1") (h "0jbh3mp8rdr50xi8nl3zpn0imakhinfm1vb52piz3iccgcyaqs0n")))

(define-public crate-gen-iter-0.3.0 (c (n "gen-iter") (v "0.3.0") (h "1gvpkzhvb414fvm89h39spv31ic28jrjb1sckxqrqggdy763dark")))

