(define-module (crates-io ge n- gen-stream) #:use-module (crates-io))

(define-public crate-gen-stream-0.1.0 (c (n "gen-stream") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0p8lbqp048r6nwzra892gqlfbzbczlbzvkwxpddkyjnbms9bgilm") (y #t)))

(define-public crate-gen-stream-0.1.1 (c (n "gen-stream") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1x4s2h2jlqsm6535q3d6l9fnixkvn50vg60paly9brmybgmb18mh") (y #t)))

(define-public crate-gen-stream-0.2.0 (c (n "gen-stream") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "14i8zdyq7w0jirrsaxdg3r5isjcyhiy32im05dq05827yqdycxq2") (y #t)))

(define-public crate-gen-stream-0.2.1 (c (n "gen-stream") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.13") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.13") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "1sks8sn71mvcl8niaga8ij6dgfrpk37pdcvkp4klsl5dkaxfxs0g")))

(define-public crate-gen-stream-0.2.2 (c (n "gen-stream") (v "0.2.2") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.14") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.14") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0rvc6294i7wlzvykihyh188f1p3z427icsrhgsyhgz4img68d349")))

(define-public crate-gen-stream-0.2.3 (c (n "gen-stream") (v "0.2.3") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.15") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.15") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "18dm5jykhzn8b0rh5d25xvps7f06c72aqw32bf03rbypz9dkac7v")))

(define-public crate-gen-stream-0.2.4 (c (n "gen-stream") (v "0.2.4") (d (list (d (n "futures-core-preview") (r "^0.3.0-alpha.16") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("compat"))) (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0nshh6vhznmy8cfzpsn7qjjsh1aq6gqvdcx4xrns7qa2bbc255db")))

