(define-module (crates-io ge n- gen-nested-iter-yield) #:use-module (crates-io))

(define-public crate-gen-nested-iter-yield-0.1.0 (c (n "gen-nested-iter-yield") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99.1") (f (quote ("futures03"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1a8ssi9gxfbfp1ai7q3sff3svgxfiaz857z58bz743xxcir6l59l") (f (quote (("futures03" "genawaiter/futures03"))))))

(define-public crate-gen-nested-iter-yield-0.1.2 (c (n "gen-nested-iter-yield") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99.1") (f (quote ("futures03"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0w8aghxm2p62njh61ygnzb6n98dspj575m960is9r6a538lfc5cy")))

(define-public crate-gen-nested-iter-yield-0.1.3 (c (n "gen-nested-iter-yield") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "genawaiter") (r "^0.99.1") (f (quote ("futures03"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1r9gfl1vhjl2n259pwv2prbg16dxy4ky9r4xb2l2zwhv8yjyi1a8")))

