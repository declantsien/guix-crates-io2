(define-module (crates-io ge n- gen-readme) #:use-module (crates-io))

(define-public crate-gen-readme-0.1.0 (c (n "gen-readme") (v "0.1.0") (h "1vc884kfkywnri0lnhlif2h29mfvpv5vcjjbxj0803if4bjffxb9")))

(define-public crate-gen-readme-0.1.1 (c (n "gen-readme") (v "0.1.1") (h "01w90fdz7h5rdrh8jdxxwp6jgff2a93pag2q5gpq9shbm3pp7rpf")))

(define-public crate-gen-readme-1.0.0 (c (n "gen-readme") (v "1.0.0") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "0kzza6zz6iyws4bcil4pidvr0biiza46gmzn2n0sf4fjf75f2wd2")))

(define-public crate-gen-readme-1.0.1 (c (n "gen-readme") (v "1.0.1") (d (list (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "0i1yzq7ywjhjlh2dpmn5sahq7nv1q8v81ng88w6d4381bdkmkf6x")))

