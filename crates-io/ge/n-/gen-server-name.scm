(define-module (crates-io ge n- gen-server-name) #:use-module (crates-io))

(define-public crate-gen-server-name-1.0.2 (c (n "gen-server-name") (v "1.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0hq14lcy8hbyhfd9dqljldhr2g07rsq2virb3lzmnvanjda8xg8k")))

(define-public crate-gen-server-name-1.0.3 (c (n "gen-server-name") (v "1.0.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "19yv3rv7gz02yrfml8w7fp61n5byhb039piiq4nhlvwbxmvm2v25")))

(define-public crate-gen-server-name-1.0.4 (c (n "gen-server-name") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0zfn9z0l30z3cjlw4gj2lgqgz81f1wv8p0v3cmyvc8jlzcrwabya")))

(define-public crate-gen-server-name-1.0.5 (c (n "gen-server-name") (v "1.0.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1mz3xnhalzscgfb4sa22c36scw98im0j3nhn02spng9r1p6xng2a")))

(define-public crate-gen-server-name-1.0.6 (c (n "gen-server-name") (v "1.0.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1wpgksqsa6gqjg5xnsyl83qdlhihm37g42fdsf4965agh5vc179y")))

(define-public crate-gen-server-name-1.0.7 (c (n "gen-server-name") (v "1.0.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "deunicode") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0bqkhk7i5whg058lclq7nzzxp7bpmals8vhs4q4f33l741abkh34")))

(define-public crate-gen-server-name-1.0.8 (c (n "gen-server-name") (v "1.0.8") (d (list (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "deunicode") (r "^1.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0awwbf02hy35db8704qc01pkcfb23sy5fjzhqpspb35ya1hh7n7j")))

