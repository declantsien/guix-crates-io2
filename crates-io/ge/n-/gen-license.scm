(define-module (crates-io ge n- gen-license) #:use-module (crates-io))

(define-public crate-gen-license-0.1.0 (c (n "gen-license") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1sm1hvhzcwhgdr52fvc8rp07n0bvbsd2wg4p0d6ws4bjacz1z09x")))

(define-public crate-gen-license-0.1.1 (c (n "gen-license") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "18m5xsri7gyx05b785d0qf94zzwsj1s80qmwdkaivkq5j8lgs0y0")))

(define-public crate-gen-license-0.1.2 (c (n "gen-license") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "09cl8q8ka9jg5iywpdxkjfb6bjr5s0wpjmk1lfv56x35si8s7pqp")))

(define-public crate-gen-license-0.1.3 (c (n "gen-license") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0d8w2l8i9v5w4gfvi577h399q3jnns2g8hg4zps203206z4cnrbl")))

