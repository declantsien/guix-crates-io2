(define-module (crates-io ge n- gen-vec) #:use-module (crates-io))

(define-public crate-gen-vec-0.2.0 (c (n "gen-vec") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w18hiivfnlbvdi3szi6b17wd8d5wj2bj2l58va8z01rjqy7skw4")))

(define-public crate-gen-vec-0.3.0 (c (n "gen-vec") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "190zvv64c7f72fxq621qdd6ww2ilidv3nw9lr9m6q11h8vykaq6f")))

