(define-module (crates-io ge rg gerg_ui) #:use-module (crates-io))

(define-public crate-gerg_ui-0.1.0 (c (n "gerg_ui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "04hyx1yg5jnzll8vm2cwf68gvh7naxd4db4dg10qcrfl6kg73b9a") (y #t)))

(define-public crate-gerg_ui-0.1.1 (c (n "gerg_ui") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "0grpvyv8fm9d5flh34cgpdd8pzc0say8v2ijclha9rx7n71zpf9x") (y #t)))

(define-public crate-gerg_ui-0.1.2 (c (n "gerg_ui") (v "0.1.2") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "0w6vqm3l1h9fmzhr553gf4vc0q03yhbbnkm4ivsjrb59yz2lp1h0") (y #t)))

(define-public crate-gerg_ui-0.1.3 (c (n "gerg_ui") (v "0.1.3") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "0blfvzwx5hd4l3lry7857kyh4m1m3flwdq97d305vyzj8cc2dq8w") (y #t)))

(define-public crate-gerg_ui-0.1.4 (c (n "gerg_ui") (v "0.1.4") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)))) (h "1rxb93slk5sz65ziwxc45pab8fqws2ll7067rsp92c7f9drf8hmr") (y #t)))

(define-public crate-gerg_ui-0.1.5 (c (n "gerg_ui") (v "0.1.5") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0khgdh79mw1w2ja7hbqny34w987wnjvhk1qq5963ar4992ibfb7n")))

(define-public crate-gerg_ui-0.1.6 (c (n "gerg_ui") (v "0.1.6") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "112r6nx027sx0qg24qyd136z0bi9840xrd9r43i4nzplmahsjmww")))

(define-public crate-gerg_ui-0.1.7 (c (n "gerg_ui") (v "0.1.7") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_mod_debug_console") (r "^0.0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0ajm927i0f6l2v7j4xl5ws7rzf7j0n0x8ynawjhj8g832nsmbxzj")))

(define-public crate-gerg_ui-0.1.8 (c (n "gerg_ui") (v "0.1.8") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_mod_debug_console") (r "^0.0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0k8drbwm7r4d25rjnk3i9in3dwysirm3djxlsg81sfvm8b622jnn")))

(define-public crate-gerg_ui-0.1.9 (c (n "gerg_ui") (v "0.1.9") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_mod_debug_console") (r "^0.0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "166dfh30q3n7l0n0lgdjv35612sps0kb1yzq9rbri3fx0kzsq9ax")))

(define-public crate-gerg_ui-0.1.10 (c (n "gerg_ui") (v "0.1.10") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_mod_debug_console") (r "^0.0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "009pyql4maiwa6lgkbvv7df0cwdl6sgzvj8a48g3wfhwflsiqhm4")))

