(define-module (crates-io ge rg gerg_geometric_shapes) #:use-module (crates-io))

(define-public crate-gerg_geometric_shapes-0.1.0 (c (n "gerg_geometric_shapes") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "glam") (r "^0.13.0") (d #t) (k 0)))) (h "1i0kx3mn9x504z7m2v2n28qxchljpvhbswklp68lvl6hcs284n9v")))

