(define-module (crates-io ge ys geyser-gpa) #:use-module (crates-io))

(define-public crate-geyser-gpa-0.1.0 (c (n "geyser-gpa") (v "0.1.0") (d (list (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "solana-account-decoder") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-client") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "0dla4j61jqqcn917hb62wqaiw57h46fmpwhbh9557hkcayvrnmvj")))

(define-public crate-geyser-gpa-0.1.1 (c (n "geyser-gpa") (v "0.1.1") (d (list (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "solana-account-decoder") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-client") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "0imcr08gxxr718v7bb6767kw0j8qbsya3s9s6cix0lacm2xi2zdm")))

(define-public crate-geyser-gpa-0.1.2 (c (n "geyser-gpa") (v "0.1.2") (d (list (d (n "redis") (r "^0.23.3") (d #t) (k 0)) (d (n "solana-account-decoder") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-client") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.16") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.14.16") (d #t) (k 0)))) (h "0gpfw7i0q0jscv537566flzvkb3ni6bxb1315np3rmc8dz2l5vk9")))

