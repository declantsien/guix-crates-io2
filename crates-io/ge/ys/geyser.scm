(define-module (crates-io ge ys geyser) #:use-module (crates-io))

(define-public crate-geyser-0.1.0 (c (n "geyser") (v "0.1.0") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)))) (h "1a1slvd23ic0sk8ydz2q8k361g21grln1f4f2q0li502d55rd86q")))

(define-public crate-geyser-0.2.0 (c (n "geyser") (v "0.2.0") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)))) (h "014ms8zdj99h01dh8ysv1zcgyj72h42j2gb71rby051m2daivfrj")))

(define-public crate-geyser-0.2.1 (c (n "geyser") (v "0.2.1") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)))) (h "0ahk4crfx65dsr0mk0ar8m8j0km32c3q7jnij1qyhkvrrb19sdh9")))

(define-public crate-geyser-0.2.2 (c (n "geyser") (v "0.2.2") (d (list (d (n "vulkano") (r "^0.14.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.14.0") (d #t) (k 0)))) (h "1h119bl44xd511mrcmcl4fj5sf5mf8vfnpzv4n393pjldg8w324r")))

