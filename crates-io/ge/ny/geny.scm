(define-module (crates-io ge ny geny) #:use-module (crates-io))

(define-public crate-geny-0.1.0 (c (n "geny") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1q6igfvbh462i4q36dgdfhq74868wbrw79mjjm18iw897lahjh6h")))

(define-public crate-geny-0.2.0 (c (n "geny") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0566rbmp95x5jwr7x8j89r19ibpfv4a8lmirlxk8k2ig3l2aqjd4")))

