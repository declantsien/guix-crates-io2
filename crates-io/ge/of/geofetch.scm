(define-module (crates-io ge of geofetch) #:use-module (crates-io))

(define-public crate-geofetch-1.0.0 (c (n "geofetch") (v "1.0.0") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "os_info") (r "^3.7") (k 0)) (d (n "si-scale") (r "^0.2") (d #t) (k 0)) (d (n "systemstat") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)) (d (n "textflow") (r "^0.2") (d #t) (k 0)))) (h "0zdc8qj9zj6xgcppicwqg9hyljhf0di5nq9b1h1kclhffkxx793m")))

(define-public crate-geofetch-1.0.2 (c (n "geofetch") (v "1.0.2") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "os_info") (r "^3.7") (k 0)) (d (n "si-scale") (r "^0.2") (d #t) (k 0)) (d (n "systemstat") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)) (d (n "textflow") (r "^0.2") (d #t) (k 0)))) (h "0pw8k3rp7pjm9kr1v36ahwpphwz1pnkhdzbvz9akkxk20f125j5z")))

(define-public crate-geofetch-1.0.3 (c (n "geofetch") (v "1.0.3") (d (list (d (n "figlet-rs") (r "^0.1") (d #t) (k 0)) (d (n "os_info") (r "^3.7") (k 0)) (d (n "si-scale") (r "^0.2") (d #t) (k 0)) (d (n "systemstat") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)) (d (n "textflow") (r "^0.2") (d #t) (k 0)))) (h "184jmqwx83arhi7jambqkvziga2svyqr8zz8ki04sv9ljwljw53l")))

