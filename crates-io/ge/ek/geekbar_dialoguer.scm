(define-module (crates-io ge ek geekbar_dialoguer) #:use-module (crates-io))

(define-public crate-geekbar_dialoguer-0.1.0 (c (n "geekbar_dialoguer") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1xi6vncgz6yyr67sis7a7ya7zxmhai8hnbf47simii52xbmx5c23") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher" "unicode-segmentation") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

