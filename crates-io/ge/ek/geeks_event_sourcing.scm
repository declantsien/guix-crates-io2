(define-module (crates-io ge ek geeks_event_sourcing) #:use-module (crates-io))

(define-public crate-geeks_event_sourcing-0.1.0 (c (n "geeks_event_sourcing") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v6z1fzgmf7ivj1sh1qjy5cbjfhpgak5xwvxiyq38f7dqfn4ib2g")))

(define-public crate-geeks_event_sourcing-0.2.0 (c (n "geeks_event_sourcing") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "04fr6iamhaqqv21zdqmkx46cwm6bpbqaqd7zsb9213svlcnslak8")))

(define-public crate-geeks_event_sourcing-0.3.0 (c (n "geeks_event_sourcing") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qlg9dxvl20y5pdrbvz60pra45a9acqb0dirdd63pr4yicc16y7n")))

(define-public crate-geeks_event_sourcing-0.3.1 (c (n "geeks_event_sourcing") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8ml581yib62v00kvlk9hw00pj07304jd8973s405iiy8qrl4hc")))

