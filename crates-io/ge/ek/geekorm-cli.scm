(define-module (crates-io ge ek geekorm-cli) #:use-module (crates-io))

(define-public crate-geekorm-cli-0.1.0 (c (n "geekorm-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "geekorm") (r "^0.1") (d #t) (k 0)))) (h "1bl391k7hhnhw22b2lfyfjw938xwhrls90f3jr5b9kchh27sz5x4") (r "1.74")))

(define-public crate-geekorm-cli-0.2.0 (c (n "geekorm-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "1l1l3iyv7sxjzkihqbh9z30pmxg7337yn98pckq2y4whq1i6f55z") (r "1.74")))

(define-public crate-geekorm-cli-0.2.1 (c (n "geekorm-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "19idpka1rchjx2pngsz1ns6g93hqcw0xj133v5cbv87klvvl1fck") (r "1.74")))

(define-public crate-geekorm-cli-0.2.2 (c (n "geekorm-cli") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "188lg3ik8zgj8dahk0dvk3kl35k9mqbr3qql37nbw3rjpgzq8z78") (r "1.74")))

(define-public crate-geekorm-cli-0.2.3 (c (n "geekorm-cli") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "0vkdknwssf96cld36r6b7z49amnv89bbyrmlqmmcmfva0wm10zbh") (r "1.74")))

(define-public crate-geekorm-cli-0.2.4 (c (n "geekorm-cli") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "0m55y6my14qmm3sgawyz0g5hhp3fw9jmv0481w4dbjaq4jmwyghq") (r "1.74")))

(define-public crate-geekorm-cli-0.2.5 (c (n "geekorm-cli") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "12pbgpc0li2ls7lqr46x4pc4jkdbcdffmf8w1xvraw0gmrccvzf1") (r "1.74")))

(define-public crate-geekorm-cli-0.2.6 (c (n "geekorm-cli") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "1crqmx7i2fi3akwnw1yzqc4qrqxsvx11h9ajs0b1f8jgcdawipwd") (r "1.74")))

(define-public crate-geekorm-cli-0.2.7 (c (n "geekorm-cli") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "19i0n6xccz44sz2190xdp46sln96i30hnzgkxlcac7k0w70n3jfr") (r "1.74")))

(define-public crate-geekorm-cli-0.2.8 (c (n "geekorm-cli") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.2") (d #t) (k 0)))) (h "14pxq0y6zhmgdx60phqz7yxmjwrjflzs3y91rf5nzdm0221hx3ip") (r "1.74")))

(define-public crate-geekorm-cli-0.3.0 (c (n "geekorm-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "geekorm") (r "^0.3") (d #t) (k 0)))) (h "1aamyv7nm1f4h30xs6xkym3dyl1spxamblw2ay79g87fhpwf9wp5") (r "1.74")))

(define-public crate-geekorm-cli-0.3.1 (c (n "geekorm-cli") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "console") (r "^0.15") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "geekorm") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0ixf8s0izjilfms3inwfq7qradlvwndwv4wlwpp5vyk2ibi40nxf") (r "1.74")))

