(define-module (crates-io ge ek geeks_git) #:use-module (crates-io))

(define-public crate-geeks_git-0.1.0 (c (n "geeks_git") (v "0.1.0") (d (list (d (n "git2") (r "^0.14.3") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 2)) (d (n "run_script") (r "^0.9.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19xr8x97v31dyjl4lv3fni8hirgbg093rbzbx9k9k29jxzbhf1ff")))

(define-public crate-geeks_git-0.2.0 (c (n "geeks_git") (v "0.2.0") (d (list (d (n "git2") (r "^0.14.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hmpd1mvdh7drwzh93niklxhw5wym2ix3i97x7ia360x508na28j")))

(define-public crate-geeks_git-0.2.1 (c (n "geeks_git") (v "0.2.1") (d (list (d (n "git2") (r ">=0.14.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1pfz6hcl63hmqvlw4fm39yw5jislx4scg02zn6zfxgx4d9mlkh1f")))

