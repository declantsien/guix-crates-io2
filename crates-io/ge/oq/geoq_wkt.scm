(define-module (crates-io ge oq geoq_wkt) #:use-module (crates-io))

(define-public crate-geoq_wkt-0.2.0 (c (n "geoq_wkt") (v "0.2.0") (d (list (d (n "geo-types") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rdlpwf5cfjag2qi3q1gn9lps4ab6533pcz00agvp8l6hcf0rz56") (f (quote (("default" "geo-types"))))))

