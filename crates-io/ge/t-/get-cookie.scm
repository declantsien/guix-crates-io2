(define-module (crates-io ge t- get-cookie) #:use-module (crates-io))

(define-public crate-get-cookie-0.2.0 (c (n "get-cookie") (v "0.2.0") (d (list (d (n "aes") (r "^0") (d #t) (k 0)) (d (n "cbc") (r "^0") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "keyring") (r "^1") (d #t) (k 0)) (d (n "ring") (r "^0") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a0s8qi9sjbrbriq05356nzj9vad8ayn84wa235pqqf15zy008ml")))

