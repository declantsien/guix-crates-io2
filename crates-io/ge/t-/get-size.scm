(define-module (crates-io ge t- get-size) #:use-module (crates-io))

(define-public crate-get-size-0.1.0 (c (n "get-size") (v "0.1.0") (d (list (d (n "get-size-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jz5y5hmxgmjjszkjfx32skm9p8rgzz4arcasz371nj6442gdz94") (f (quote (("derive" "get-size-derive") ("default"))))))

(define-public crate-get-size-0.1.1 (c (n "get-size") (v "0.1.1") (d (list (d (n "get-size-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01n3sshl2r7y2jv0zzz7mgrb76l5lwqrrbk901p6px02fibppzzz") (f (quote (("derive" "get-size-derive") ("default"))))))

(define-public crate-get-size-0.1.2 (c (n "get-size") (v "0.1.2") (d (list (d (n "get-size-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "06xw978zx9qkcr2wbnc06z65md99h9jkfvv0vwzqzlywkch74szj") (f (quote (("derive" "get-size-derive") ("default"))))))

(define-public crate-get-size-0.1.3 (c (n "get-size") (v "0.1.3") (d (list (d (n "get-size-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "1w7gdmq2w6kvydwfw3v7l7v1a32x6w3v6875wi0wgaas8v2rhv81") (f (quote (("derive" "get-size-derive") ("default"))))))

(define-public crate-get-size-0.1.4 (c (n "get-size") (v "0.1.4") (d (list (d (n "get-size-derive") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "08115g93dy0z042fg8y5p8bgywwqj65ldcrsm29wxvbymcnixdj7") (f (quote (("derive" "get-size-derive") ("default"))))))

