(define-module (crates-io ge t- get-size-derive) #:use-module (crates-io))

(define-public crate-get-size-derive-0.1.0 (c (n "get-size-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0zw14gqb1nsk44v5mdv6l0y8fa4hlzyj57qsq3j6m63bfj947yb0")))

(define-public crate-get-size-derive-0.1.1 (c (n "get-size-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0w1zjd62jls06fkxjwvkhm0wqm0igfrkqgvrllahjv93rm6afv9i")))

(define-public crate-get-size-derive-0.1.2 (c (n "get-size-derive") (v "0.1.2") (d (list (d (n "get-size") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xh9qmrys1xnhnmwywh7ijc31904zbw446w2qg0r39v9za29vrc")))

(define-public crate-get-size-derive-0.1.3 (c (n "get-size-derive") (v "0.1.3") (d (list (d (n "attribute-derive") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1hny6lj675w2j8i91rvxymb1qnpj6qp59aqkb46k87swhpxvr88k")))

