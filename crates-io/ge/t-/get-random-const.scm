(define-module (crates-io ge t- get-random-const) #:use-module (crates-io))

(define-public crate-get-random-const-1.0.0 (c (n "get-random-const") (v "1.0.0") (d (list (d (n "getrandom") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0m4m3ka67jfpxlikhgiz83yz1cibgjxzjcwrmc9dpj9ci9vpm9cg")))

(define-public crate-get-random-const-1.0.1 (c (n "get-random-const") (v "1.0.1") (d (list (d (n "getrandom") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "19zgbynrzj3danlhf2cd6qn6hisc9dpcpx9rfr34cvkr82bkhmyn")))

(define-public crate-get-random-const-2.0.0 (c (n "get-random-const") (v "2.0.0") (d (list (d (n "getrandom") (r "^0") (d #t) (k 0)))) (h "1p8yncpslga8kw7dzp7nxyn1svif9sx9zdg9lc8hy5lkn4jjjiyx")))

(define-public crate-get-random-const-2.0.1 (c (n "get-random-const") (v "2.0.1") (d (list (d (n "getrandom") (r "^0") (d #t) (k 0)))) (h "1b6p6mynvvm9wynh3mz6b8bxg1n8ki9djvcgxbzmc5p0r5asc2ka")))

(define-public crate-get-random-const-2.0.2 (c (n "get-random-const") (v "2.0.2") (d (list (d (n "getrandom") (r "^0") (d #t) (k 0)))) (h "116ahc07dd6w5q00qljqbck2gkyx9xgc7amifxwipjncv8ny1gr2")))

(define-public crate-get-random-const-2.0.3 (c (n "get-random-const") (v "2.0.3") (d (list (d (n "getrandom") (r "^0.2.9") (d #t) (k 0)))) (h "0hjxp92282hly06prdg0gkjd95vmkmh1j497g62554av6r308znm")))

