(define-module (crates-io ge t- get-field-by-type-derive) #:use-module (crates-io))

(define-public crate-get-field-by-type-derive-0.0.1 (c (n "get-field-by-type-derive") (v "0.0.1") (d (list (d (n "syn-helpers") (r "^0.4.3") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "1cryhjc6y3vlyn6b9hsj1d4djq7jkasz1zhpbn0vmagp7r7x2v4m")))

(define-public crate-get-field-by-type-derive-0.0.2 (c (n "get-field-by-type-derive") (v "0.0.2") (d (list (d (n "syn-helpers") (r "^0.4.3") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "1i5p8fln7cplnjln4c52kq4bjslx2id2ag5pmksq7nwzr76mjsmc")))

(define-public crate-get-field-by-type-derive-0.0.3 (c (n "get-field-by-type-derive") (v "0.0.3") (d (list (d (n "syn-helpers") (r "^0.4.5") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "1svzpag7m5m6k89h21d90nj7c5q58nrfrrzdgsgj457c0pz7f5qw")))

(define-public crate-get-field-by-type-derive-0.0.4 (c (n "get-field-by-type-derive") (v "0.0.4") (d (list (d (n "syn-helpers") (r "^0.5") (f (quote ("syn-extra-traits"))) (d #t) (k 0)))) (h "0p1215i76mn7rn2isnzsvvq7kd5s3y1ard4gc3b1w9wi24f3fc9r")))

