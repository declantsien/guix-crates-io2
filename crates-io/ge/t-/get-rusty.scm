(define-module (crates-io ge t- get-rusty) #:use-module (crates-io))

(define-public crate-get-rusty-0.1.0 (c (n "get-rusty") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dvzgdvdchvx789zrh041ap45rwik1s0w0cyvk9kld07yc0xmgsi")))

