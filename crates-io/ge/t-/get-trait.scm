(define-module (crates-io ge t- get-trait) #:use-module (crates-io))

(define-public crate-get-trait-0.1.0 (c (n "get-trait") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "iter-trait") (r "^0.3.1") (d #t) (k 0)))) (h "1bqvh4cxa3xpp6i27rbjdba3hxzw7g0n9i4bip4y49iwi266m3ap") (f (quote (("std" "alloc") ("nightly") ("default" "std" "nightly") ("alloc"))))))

