(define-module (crates-io ge t- get-hash) #:use-module (crates-io))

(define-public crate-get-hash-1.0.2 (c (n "get-hash") (v "1.0.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0msy7wf6v6h8xclwq8symg8yjzi7k6j01hibi92wsclagigx8j7g") (y #t)))

(define-public crate-get-hash-1.0.4 (c (n "get-hash") (v "1.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "04l86g4a1zjl9nn4y3slgsb0z8lncp586kk83vw8jwfd06zbzdf4") (y #t)))

(define-public crate-get-hash-1.1.0 (c (n "get-hash") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "01anaacfjsvvyc48k597jwdqb2cvz1f19mf9ym46im6sflw6dg1d") (y #t)))

(define-public crate-get-hash-1.1.1 (c (n "get-hash") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0vmbhiibdj12xfkqmpijsmwdfjvdzi9qvgs6ani3x2zsrqkpnbkp") (y #t)))

(define-public crate-get-hash-1.1.2 (c (n "get-hash") (v "1.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1lwa6vdrnrsvjf34n04kaa13ljib2lg1cki11hr241l5n2vbnp70") (y #t)))

(define-public crate-get-hash-1.1.9 (c (n "get-hash") (v "1.1.9") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1axzgjlnf7zvqk1jc9lq0g91mf6fghnbgrr5r6ksrk9qsimwc3wa")))

