(define-module (crates-io ge t- get-all-images) #:use-module (crates-io))

(define-public crate-get-all-images-0.1.0 (c (n "get-all-images") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "0xi4h88avh0d5ld7ilsas9x04wg11j8cp7vmwbyccdz4m93kaqv3")))

(define-public crate-get-all-images-0.1.1 (c (n "get-all-images") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "095ahc218f1qpv1l5jf58a9dl11hx961y595hbbgz170lqf776ig")))

