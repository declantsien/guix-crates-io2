(define-module (crates-io ge t- get-last-error) #:use-module (crates-io))

(define-public crate-get-last-error-0.1.0 (c (n "get-last-error") (v "0.1.0") (d (list (d (n "rust_win32error") (r "^0.8") (d #t) (k 2)) (d (n "w32-error") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "18z0gsaxy9zcr7abm8425xhqmpc5nqh8k2v8x9138hqx1wg5wiam") (f (quote (("std" "winapi/std") ("default" "std"))))))

(define-public crate-get-last-error-0.1.1 (c (n "get-last-error") (v "0.1.1") (d (list (d (n "rust_win32error") (r "^0.8") (d #t) (k 2)) (d (n "w32-error") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0b5v62yk6pdxa0vl7q89n13iwavpnnplbr3axgk3x9mnmzrcv1ff") (f (quote (("std" "winapi/std") ("default" "std"))))))

