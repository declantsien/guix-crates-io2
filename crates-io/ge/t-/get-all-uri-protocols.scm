(define-module (crates-io ge t- get-all-uri-protocols) #:use-module (crates-io))

(define-public crate-get-all-uri-protocols-0.1.0 (c (n "get-all-uri-protocols") (v "0.1.0") (d (list (d (n "winreg") (r "^0.10.1") (d #t) (k 0)))) (h "08lji7789r5xbx0n28qnvimksphmsv3xwp013w39gpq08ym0374g")))

(define-public crate-get-all-uri-protocols-0.1.1 (c (n "get-all-uri-protocols") (v "0.1.1") (d (list (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0p64nwwg2vr4z2zx4y4bdp5m74wf8zisvpii0yq9f27pw3j6nipg")))

(define-public crate-get-all-uri-protocols-0.1.2 (c (n "get-all-uri-protocols") (v "0.1.2") (d (list (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)))) (h "14nprg9pz3d1vwfymbg5yj5gdzls49336vnzf28dshib8zx7h04i")))

(define-public crate-get-all-uri-protocols-0.1.3 (c (n "get-all-uri-protocols") (v "0.1.3") (d (list (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)))) (h "13hgbf10n4yjqfnbhqlnpfipjbwcynzplsa72jwx5z0dpjdsvkcn")))

