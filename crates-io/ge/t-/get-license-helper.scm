(define-module (crates-io ge t- get-license-helper) #:use-module (crates-io))

(define-public crate-get-license-helper-0.1.0 (c (n "get-license-helper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1bdi29xwzk56qsy5dvidvgkil8j3sgcibwrv0dqi8zc5v17yp56i")))

(define-public crate-get-license-helper-0.1.1 (c (n "get-license-helper") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wcv9dkr9l64fs9klz0nm1mz2xymw099galqgg2dxvg0qlw5ya8p")))

