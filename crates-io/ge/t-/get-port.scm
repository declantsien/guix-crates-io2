(define-module (crates-io ge t- get-port) #:use-module (crates-io))

(define-public crate-get-port-0.1.0 (c (n "get-port") (v "0.1.0") (h "180y1cw3f93k9991m9058z7as0yhj68113s4r3cg3akgi10hz0la")))

(define-public crate-get-port-1.0.0 (c (n "get-port") (v "1.0.0") (h "0h9bws7vilq5shr6d1x9y8b07mbpa39ynk7x2kjk4k08h8mic2jy")))

(define-public crate-get-port-1.0.1 (c (n "get-port") (v "1.0.1") (h "156f24z7wl74c6ph9wykhiw2hlqzjcjv32w6mdd04nsa037d3c0m")))

(define-public crate-get-port-1.1.0 (c (n "get-port") (v "1.1.0") (h "1aamfadha2ldirx2y98lpqp236gjb23bzqgmb1752bpb142mla0c")))

(define-public crate-get-port-1.1.1 (c (n "get-port") (v "1.1.1") (h "0yyqky0fmabq6cyh5zg45ndif4w534qjd4bfcbhmjsa46faqb25k")))

(define-public crate-get-port-1.2.0 (c (n "get-port") (v "1.2.0") (h "0r8m1wdqzmx2zkzqagz13sgqkygqv7vvbh63h700sv79cs3399wz")))

(define-public crate-get-port-1.3.0 (c (n "get-port") (v "1.3.0") (h "1m69sqkbr96h80wfyb1r18js7w1g66swrr950a9shz9m7wlmj6n4")))

(define-public crate-get-port-1.3.1 (c (n "get-port") (v "1.3.1") (h "1cv5lgm9f00vz13aplm4qn9in5q4sx5w1i8l5n44m4bx8rm1ws72")))

(define-public crate-get-port-2.0.0 (c (n "get-port") (v "2.0.0") (h "025sba0y3gjc3f6xmqj0cgj35lmxhl0hhdwbljv2kw5nlnri9vvd")))

(define-public crate-get-port-3.0.0 (c (n "get-port") (v "3.0.0") (h "115wprv4apxv15dwdnpwn15ixylhrr3h68hfpz2izbk0kjil2v5c")))

(define-public crate-get-port-4.0.0 (c (n "get-port") (v "4.0.0") (h "1qkhymc2bdn05kjkvd3c3m9svysd6r37sd2sbgqvykxkgl0270c8")))

