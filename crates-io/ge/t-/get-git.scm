(define-module (crates-io ge t- get-git) #:use-module (crates-io))

(define-public crate-get-git-0.1.0 (c (n "get-git") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "1z0x0vhrcymid4d0smna4zfkz99x70b0jmiqihk5v22j4wik00q7")))

(define-public crate-get-git-0.1.1 (c (n "get-git") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "00hdwy8kn7aammggzpgb75l3fr3px7d0ws6q46w25n0n3pg5gvdp")))

(define-public crate-get-git-0.1.2 (c (n "get-git") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0183702wa2wlcpiyw1vx91xiv7z9g9jz1hwfliways9z3ikrs83z")))

