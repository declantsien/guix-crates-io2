(define-module (crates-io ge ni genius-invokation) #:use-module (crates-io))

(define-public crate-genius-invokation-0.0.0 (c (n "genius-invokation") (v "0.0.0") (h "0pamkjz70q4s3xfgi9d7x6g0yxlcx5vm1sd3ad0waa5f500frv80")))

(define-public crate-genius-invokation-0.1.0 (c (n "genius-invokation") (v "0.1.0") (h "00cz7ifgcrfgnza0m9rrjql4hl8rfkgpwln4ymli7ssfw1030il3") (f (quote (("deck-url"))))))

(define-public crate-genius-invokation-0.2.0 (c (n "genius-invokation") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1k2glgdll7nza52mplrrlzdx5kdz5iqx5la944dsj2hjv7zhk50a") (f (quote (("deck-url"))))))

