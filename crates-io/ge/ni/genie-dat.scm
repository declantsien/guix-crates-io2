(define-module (crates-io ge ni genie-dat) #:use-module (crates-io))

(define-public crate-genie-dat-0.1.0 (c (n "genie-dat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (f (quote ("rust_backend"))) (k 0)) (d (n "genie-support") (r "^1.0.0") (d #t) (k 0)) (d (n "jascpal") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0g037ad87ymi8796qfm18q2qhdxhi1rh53j41c5nz70yxb0z06ms")))

