(define-module (crates-io ge ni genie-cpx) #:use-module (crates-io))

(define-public crate-genie-cpx-0.1.0 (c (n "genie-cpx") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "genie-scx") (r "^1.0.0") (d #t) (k 0)))) (h "15yyqm622y8hfd0hq0g8i0m5h3s629xz8hfaz48j9rk9k02v82ly")))

(define-public crate-genie-cpx-0.2.0 (c (n "genie-cpx") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "genie-scx") (r "^1.0.0") (d #t) (k 0)))) (h "11b0bghdn1y4v3jn3y6fgwk74xlv88gv09cw4fngb3sqjbvf4xv2")))

(define-public crate-genie-cpx-0.3.0 (c (n "genie-cpx") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "genie-scx") (r "^2.0.0") (d #t) (k 0)))) (h "1f5d8k7228vb3qrwsrc4gja6yyf343mgwk43cnhqw20xjsralksx")))

(define-public crate-genie-cpx-0.4.0 (c (n "genie-cpx") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "genie-scx") (r "^3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "10s2rzp1qn3yv2bc4lddgrj3z5zaipdchzd87g9m383g7n40rc84")))

(define-public crate-genie-cpx-0.5.0 (c (n "genie-cpx") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "genie-scx") (r "^4.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "1hgm1pjqwmy6c0w2s9p54hkps7ivbza48nw6glsdd2n10ikn4glv")))

