(define-module (crates-io ge ni genius_lyrics) #:use-module (crates-io))

(define-public crate-genius_lyrics-1.0.0 (c (n "genius_lyrics") (v "1.0.0") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19dphha78wi00vj7hxqzpb9d09bfxn6a2jqjnm96a9qpqczk91rq")))

(define-public crate-genius_lyrics-1.0.1 (c (n "genius_lyrics") (v "1.0.1") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qf1sjzizwg21n14pf43310hxn4012z1ashifyfn0qx74nwp4r7a")))

(define-public crate-genius_lyrics-1.0.2 (c (n "genius_lyrics") (v "1.0.2") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a34hkbn9yvkl0ql1mv9zwzss46wzmif0bqg8pird3pm4xmlzh87")))

(define-public crate-genius_lyrics-1.0.3 (c (n "genius_lyrics") (v "1.0.3") (d (list (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jwv9k3xqi7z6wz0rfyp2yxvfsc0rs83c5m7j2wnf834n93mmcb3")))

(define-public crate-genius_lyrics-1.0.4 (c (n "genius_lyrics") (v "1.0.4") (d (list (d (n "html2text") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "006d5p1w9505law87yhlwv0qjdlnwp9nmabhdhf373ir5m6p5xsq")))

