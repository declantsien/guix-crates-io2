(define-module (crates-io ge ni genies_derive) #:use-module (crates-io))

(define-public crate-genies_derive-1.0.0 (c (n "genies_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0z9ivqbz5316g7i0bj70msp17rbv2mrlgrp6xrbdfcpnz8q471hk") (f (quote (("debug_mode"))))))

