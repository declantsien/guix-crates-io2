(define-module (crates-io ge ni genio) #:use-module (crates-io))

(define-public crate-genio-0.1.0 (c (n "genio") (v "0.1.0") (d (list (d (n "void") (r "^1") (d #t) (k 0)))) (h "1r5236hydwxshqkjxrh70fx3vysb7k6g85g9ddfw301j0wbcdk0a") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-genio-0.2.0 (c (n "genio") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "18adyyknbpf0daawal57yj7kp089yvvasrp7rnz7v8r4bd8s3k0w") (f (quote (("use_std" "std") ("std" "void/std") ("default" "std"))))))

(define-public crate-genio-0.2.1 (c (n "genio") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "1j1di22ih183q9m7lqxfxn8ymbv6jcy7w37jhcxaizq8m1cniqpl") (f (quote (("use_std" "std") ("std" "void/std") ("default" "std"))))))

