(define-module (crates-io ge ni genie-drs) #:use-module (crates-io))

(define-public crate-genie-drs-0.1.1 (c (n "genie-drs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "1481j1sg0ncj20fwhsmrqrh5a8l65p72hyixwp2jlswhza5bp84y")))

(define-public crate-genie-drs-0.2.0 (c (n "genie-drs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.3.0") (d #t) (k 0)))) (h "00vf4giyghsvq1pwnbxci32szjshx8w4nam7q9x4072jg6qahpvq")))

(define-public crate-genie-drs-0.2.1 (c (n "genie-drs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0l8m6jdrh0lgflklfpf271hkrwr7xxvfl4gxk6hmyv33spisy5dz")))

