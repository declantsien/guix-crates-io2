(define-module (crates-io ge ni genie-lang) #:use-module (crates-io))

(define-public crate-genie-lang-0.1.0 (c (n "genie-lang") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.5") (d #t) (k 0)) (d (n "pelite") (r "^0.7.1") (d #t) (k 0)))) (h "198xwz5sm3mizz1wxxbvsg439l9ddqcv4mkplik0kndxhrix9mbb")))

(define-public crate-genie-lang-0.2.0 (c (n "genie-lang") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.5") (d #t) (k 0)) (d (n "pelite") (r "^0.7.1") (d #t) (k 0)))) (h "1izcnda1ryzflp6nbf3rcpqxsglck4hnrmm11g500sg4pfg84bp9")))

(define-public crate-genie-lang-0.2.1 (c (n "genie-lang") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.5") (d #t) (k 0)) (d (n "genie-support") (r "^1.0.0") (d #t) (k 0)) (d (n "pelite") (r "^0.8.0") (f (quote ("mmap"))) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "17k2507mqqz9ijzxia1x3ahw701q5gy113pfnxpxw5zv9npn2yld")))

