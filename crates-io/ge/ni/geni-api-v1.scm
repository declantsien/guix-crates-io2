(define-module (crates-io ge ni geni-api-v1) #:use-module (crates-io))

(define-public crate-geni-api-v1-0.1.0 (c (n "geni-api-v1") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "03ns6i4f5h01bwkvac16m5mh6np6g3sj915qnfqkf5xcxvghd5db")))

(define-public crate-geni-api-v1-0.1.1 (c (n "geni-api-v1") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "09sm3gm9br5jh5c2drdxfc6pds87zn1r4gmr5mhp907cj5liawmp")))

