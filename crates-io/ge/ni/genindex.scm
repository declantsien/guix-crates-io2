(define-module (crates-io ge ni genindex) #:use-module (crates-io))

(define-public crate-genindex-0.2.0 (c (n "genindex") (v "0.2.0") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i22j9m4m5w4j9jks0m7pnvndxcdvsfbh4gc4hbhcd2zvb09v4hc") (f (quote (("default"))))))

(define-public crate-genindex-0.2.1 (c (n "genindex") (v "0.2.1") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wxk2wsgc3sv5rajnr6cynngpwhhqhn2vhnj820k4xf98169i24p") (f (quote (("default"))))))

(define-public crate-genindex-0.2.2 (c (n "genindex") (v "0.2.2") (d (list (d (n "num") (r "^0.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d5w4nl8k7aljzlk8pi9xwih5cm6gjgaqpr353c4c5838sw3zvbi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

