(define-module (crates-io ge ni genie-hki) #:use-module (crates-io))

(define-public crate-genie-hki-0.1.0 (c (n "genie-hki") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (f (quote ("rust_backend"))) (k 0)))) (h "1zqiw67mvjnk97f92hvhp96fd5wqmzd2zvdsgpsyx4dyf11y9ywk")))

(define-public crate-genie-hki-0.2.0 (c (n "genie-hki") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (f (quote ("rust_backend"))) (k 0)) (d (n "genie-lang") (r "^0.2.0") (d #t) (k 0)))) (h "1959wpq4jgjxcz29rigdglfdbbsa8ncr6yyjp2gzx1d3aljmlphi")))

(define-public crate-genie-hki-0.2.1 (c (n "genie-hki") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (d #t) (k 0)) (d (n "genie-lang") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "10hc03999hv5b51ccsybw34y4yph4j8ijm824rbm9yyb727w8hfg")))

