(define-module (crates-io ge ni genie-support) #:use-module (crates-io))

(define-public crate-genie-support-1.0.0 (c (n "genie-support") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0zsrzd8ywp1l29sp98576w87lnlm64hjz1p8mpx9w3dzlglsrrpf") (f (quote (("strings" "encoding_rs"))))))

