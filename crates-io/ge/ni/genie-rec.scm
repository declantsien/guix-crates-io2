(define-module (crates-io ge ni genie-rec) #:use-module (crates-io))

(define-public crate-genie-rec-0.1.0 (c (n "genie-rec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (f (quote ("rust_backend"))) (k 0)) (d (n "genie-dat") (r "^0.1.0") (d #t) (k 0)) (d (n "genie-scx") (r "^3.0.0") (d #t) (k 0)) (d (n "genie-support") (r "^1.0.0") (f (quote ("strings"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0d0390qi5ri87fwgw87sjkn2188vdskc44zslk2xcqb2mqhvl553")))

(define-public crate-genie-rec-0.1.1 (c (n "genie-rec") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.0") (f (quote ("rust_backend"))) (k 0)) (d (n "genie-dat") (r "^0.1.0") (d #t) (k 0)) (d (n "genie-scx") (r "^4.0.0") (d #t) (k 0)) (d (n "genie-support") (r "^1.0.0") (f (quote ("strings"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0jhy265lmbg6b1k9kaflg49pn0s80k3d9lc87mk5dan516jjy9kv")))

