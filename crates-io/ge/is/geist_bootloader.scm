(define-module (crates-io ge is geist_bootloader) #:use-module (crates-io))

(define-public crate-geist_bootloader-0.1.0 (c (n "geist_bootloader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "shiplift") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yzfdsslfyzkz7pppzd3h72b1lghx2x69z3hdgkd71wqqijqzibk")))

(define-public crate-geist_bootloader-0.1.1 (c (n "geist_bootloader") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "shiplift") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sshc806y1y7dr5lvn9fgkh95rgpv0bnlfcjdgwdqizc8xdmjf5n")))

(define-public crate-geist_bootloader-0.1.2 (c (n "geist_bootloader") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "shiplift") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "0p2asfn7wnqlbx7zgy14vf9m87q4q8spz7ba9qjmgmws0zkpai3a")))

(define-public crate-geist_bootloader-0.1.3 (c (n "geist_bootloader") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0ff263grjmkklmd5hrgrvigzv6xby9xvr30dzgj2kdv8rqsm4qbb")))

(define-public crate-geist_bootloader-0.1.4 (c (n "geist_bootloader") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "12nyqy6aygaqk65ckj06l8zsjh6qszmjbm5q65zmimy4jxmyrm4c")))

(define-public crate-geist_bootloader-0.1.5 (c (n "geist_bootloader") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1bi8ypj37ragy9q2x1cc9yhp3zwdqlcw26yjjlmcgdyzvphw2b0g")))

(define-public crate-geist_bootloader-0.1.6 (c (n "geist_bootloader") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1g4i0jwxh8s675zkmn7wv4vv2zd87anhmlrpmci27xy5rz1gd6a7")))

(define-public crate-geist_bootloader-0.1.7 (c (n "geist_bootloader") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1ig4vw8fx5pln466nj0bj7v0ncszdmnf7cx7dm9d9bbqpc45m3mr")))

(define-public crate-geist_bootloader-0.1.8 (c (n "geist_bootloader") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0vbsidjd5sqm273v7lqnag2z69l5cm9x6fa7cxs68rpk55xig82s")))

(define-public crate-geist_bootloader-0.1.9 (c (n "geist_bootloader") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1akrjd0p650qyrqa9gm60l5hgw06c7d4ac8fnyxhfn81jnkmkpg7")))

(define-public crate-geist_bootloader-0.1.10 (c (n "geist_bootloader") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "04zbd3999gfsv7dbv1n1b9cqivll5xzs3c3d123pdpl1ss00dbwl")))

