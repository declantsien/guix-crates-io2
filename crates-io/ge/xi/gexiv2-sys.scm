(define-module (crates-io ge xi gexiv2-sys) #:use-module (crates-io))

(define-public crate-gexiv2-sys-0.1.0 (c (n "gexiv2-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d0k3s9iavmibid5b1pbdrhmai3008a7h6b1kjq4ffbmb083yqf0")))

(define-public crate-gexiv2-sys-0.1.1 (c (n "gexiv2-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "076p5ppvpwfi8vq8baf9ylzjrzyy2n200wrbxj16n3rxv4sbrrq8")))

(define-public crate-gexiv2-sys-0.2.0 (c (n "gexiv2-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05plr0qs6mnm364kj1agpmxkh67x55lzcf2pbzs9spprhhzv1blf")))

(define-public crate-gexiv2-sys-0.3.0 (c (n "gexiv2-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12cighxkhx7wsm04i98gpc2wpgvpligsva02n1b3r4rzp857i22p")))

(define-public crate-gexiv2-sys-0.4.0 (c (n "gexiv2-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hn7bd1y91a8gr3nxvvwx82hjcpx6i4dznbfhnksikvqb5lhr5ms")))

(define-public crate-gexiv2-sys-0.5.0 (c (n "gexiv2-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w0fbq96kq6z9rzpz9if3r46x0k38g3x5pkchwzz4ph11354zw3a")))

(define-public crate-gexiv2-sys-0.6.0 (c (n "gexiv2-sys") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "105m9cm8rr3j2bqw07mb5wsnkrgqsv46myc7lx8h0mbnf8z8cmpv") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-0.6.1 (c (n "gexiv2-sys") (v "0.6.1") (d (list (d (n "bitflags") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "165a22ar4sw5mj80c7h7b5gfvf75d8m4v576g5h3hv7925j0dnmg") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-0.7.0 (c (n "gexiv2-sys") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1gw8vny1y2hkdnxffs98k0xzmmj2g703593a7ya0yrh99zi9cxv6") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-0.7.1 (c (n "gexiv2-sys") (v "0.7.1") (d (list (d (n "bitflags") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2, < 0.2.34") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1nz71v36j2ss8h7xbhfl5srgdj500j62vx592ir7a1sg2fpnvmqw") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-0.8.0 (c (n "gexiv2-sys") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0b4vqps1lkprrb186f143njr3hpr6q24ry7a838mh4s2lvjsqgm2") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-1.0.0 (c (n "gexiv2-sys") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1s329b81l9g08r5bp1izp64lr13h644gp53smywylcsm40w13nyq") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys"))))))

(define-public crate-gexiv2-sys-1.1.0 (c (n "gexiv2-sys") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1izxrams687dqijac1nbsmw0i3b4xjv3q2zx8nihch8sbrmxc8j6") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2")))

(define-public crate-gexiv2-sys-1.1.1 (c (n "gexiv2-sys") (v "1.1.1") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0z39aqp8zj3kvkwspy821hw8kgksmigqkxigg6cil5v2k7kzgj7d") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2")))

(define-public crate-gexiv2-sys-1.1.2 (c (n "gexiv2-sys") (v "1.1.2") (d (list (d (n "bitflags") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "04vdaaikvj9m9sjc5qana3xi53mrh381d0c3cg503d5qmb26mrmk") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2")))

(define-public crate-gexiv2-sys-1.2.0 (c (n "gexiv2-sys") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "04v2zidl89p76i2qs1yksqn538wj4xixg68imxgk3sw8ysqaw403") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2") (r "1.56")))

(define-public crate-gexiv2-sys-1.3.0 (c (n "gexiv2-sys") (v "1.3.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "00an5qqbvakm928c472nrgb3vd6iq07qy2jp9dwmz1kbfxipmsap") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2") (r "1.63")))

(define-public crate-gexiv2-sys-1.4.0 (c (n "gexiv2-sys") (v "1.4.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "glib-sys") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1wfs97qwwlr2iw4gb375z50b1jf74dkl4d7ba8y8g0z3gfjggvf4") (f (quote (("xmp-packet-access" "bitflags") ("raw-tag-access" "glib-sys")))) (l "gexiv2") (r "1.63")))

