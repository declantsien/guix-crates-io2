(define-module (crates-io ge nf genfsm) #:use-module (crates-io))

(define-public crate-genfsm-0.0.1 (c (n "genfsm") (v "0.0.1") (h "0fksfxh1vyda395jdjki88hxcz17w4c6swqh3dfc2shi0j7i96a7")))

(define-public crate-genfsm-0.0.2 (c (n "genfsm") (v "0.0.2") (h "1ajsyqpx7kf5ljj7gsp44zwzsikb25x0n7c3gffb0sa78xmv5zrf")))

