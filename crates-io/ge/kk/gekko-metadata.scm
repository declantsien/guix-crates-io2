(define-module (crates-io ge kk gekko-metadata) #:use-module (crates-io))

(define-public crate-gekko-metadata-0.1.0 (c (n "gekko-metadata") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cal2677mpq8mwfz56nlaqyv7db2pk98f1bbvqsnzh1j69vax82y")))

(define-public crate-gekko-metadata-0.1.1 (c (n "gekko-metadata") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0q69i17fz07sgwrgfman3rr2qgybwabapy85iy73ksd60hmlzpp9")))

(define-public crate-gekko-metadata-0.1.2 (c (n "gekko-metadata") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0qzjyk2nk95wkdpb7a079m0wr70rj6lwsjkn07abpk8947nq84v1")))

