(define-module (crates-io ge kk gekko-generator) #:use-module (crates-io))

(define-public crate-gekko-generator-0.1.0 (c (n "gekko-generator") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "gekko-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1ascy7sb6dsxalaq4y237z8nn8cg509c54d82icysd16rdm99fz1")))

(define-public crate-gekko-generator-0.1.1 (c (n "gekko-generator") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "gekko-metadata") (r "^0.1.0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "050dg58killjz9zp97m3yg37z0fwjfqgwz9lak3xih48xr353gc0")))

(define-public crate-gekko-generator-0.1.2 (c (n "gekko-generator") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "gekko-metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0sr5mppd9hwl9nkkw65hdjrms0hvndxkdkihc72n1d8idbiimhfa")))

