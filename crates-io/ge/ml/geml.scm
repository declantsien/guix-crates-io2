(define-module (crates-io ge ml geml) #:use-module (crates-io))

(define-public crate-geml-0.1.0 (c (n "geml") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1b02pw1awyh98z3ridnkbkhpyiqllg0hl089i6mz225d9barv1sy")))

(define-public crate-geml-0.1.1 (c (n "geml") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00hkm6b6bbv100abg7004y846f9r5xmn5id5n8icl2c7ifizlfw8")))

(define-public crate-geml-0.1.2 (c (n "geml") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dfza98zd1b63n1f3c39hhzr1jf99a4bpjspnbl5ra984q7si4ir")))

(define-public crate-geml-1.0.3 (c (n "geml") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14w9cc64jxfdw7h997wk9lnwcchw1sff0n6qx2a8af2pjrfz0vig")))

(define-public crate-geml-1.0.5 (c (n "geml") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1r0cbplsf04csqhihf8c3n7idalvnb1bg676iylsargy964p310g")))

(define-public crate-geml-1.0.6 (c (n "geml") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13pak9z7iv4bwzd4qdndj32vjlw0k21m0rxhpga50lm39p5gsifn")))

(define-public crate-geml-1.0.7 (c (n "geml") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1i50m0xwh7wlckvhmzgxf3h3jjzrq6q6x37q41dvlfs56c5280yr")))

(define-public crate-geml-1.1.1 (c (n "geml") (v "1.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a8yfq63d24nffcilkh6017pizb6c0fd0dd0qwk39r38bi27qand")))

(define-public crate-geml-1.1.2 (c (n "geml") (v "1.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0d5l6hnzf0f8wqrgsh8rqwq4diq4vspxqwl8rmv317skzs57gllj")))

(define-public crate-geml-1.1.3 (c (n "geml") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a37p0j3s081vsymbi65v2a1m2f959j3l7azpkkpwkl5bgag133m")))

(define-public crate-geml-1.1.4 (c (n "geml") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kj7ncqkbpqdn5j3jn9jaxvvkdys7bpmjvl86hzr5vrsncm6kr6j")))

(define-public crate-geml-1.1.5 (c (n "geml") (v "1.1.5") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dy2sk16k0vwp5zc2kzl397hgvfajvfjqbr5gzy7mhx7x1zmby4l")))

(define-public crate-geml-1.1.6 (c (n "geml") (v "1.1.6") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0knzi7xkg923p2abfx45ipb1hzxrgva2ajqwxlrfsv7br2bcn9xz")))

(define-public crate-geml-1.1.7 (c (n "geml") (v "1.1.7") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07zq6dpfbqwx7f56z32zpsvpnnzzd717b0mcmcfgyw4s820gp3yb")))

(define-public crate-geml-1.1.8 (c (n "geml") (v "1.1.8") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w3h55hnb07ccd86saqdhdrn52nw1bf6ssw9cmmp465imqyz1bsz")))

(define-public crate-geml-1.1.9 (c (n "geml") (v "1.1.9") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16q16c50d9yrjdvljcc194dnidg6wfnk8n49lmh3b95b7vi252qx")))

(define-public crate-geml-1.1.10 (c (n "geml") (v "1.1.10") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1zzraikx96q3k7cd554kn19wjrxiyhzv20vnsz4sllw735z8lndn")))

(define-public crate-geml-1.1.11 (c (n "geml") (v "1.1.11") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0apjj59c7r3ga8436nc1dqh31b3v2p2yfrhn3j9fs8g5brf2mnwf")))

(define-public crate-geml-1.1.12 (c (n "geml") (v "1.1.12") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b77phfpw1nfjmsxs5nyc5ypj9cwnn4bn2by0gf4287r0d6y5ngw")))

(define-public crate-geml-1.1.13 (c (n "geml") (v "1.1.13") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14s6h0pzf5igz56110kzfxy2ffhvadphjdyn8wph8dcsn17ngwbf")))

(define-public crate-geml-1.1.14 (c (n "geml") (v "1.1.14") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1z95fxnvsr7c1i2j7fkaq83ky3b4d4q93y8vwd5h7wdjjl4yy1sh")))

(define-public crate-geml-1.1.15 (c (n "geml") (v "1.1.15") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1y60fn4glkydlzy98smakhpf66r21vk0zcr0s996zwk06kdhb9gd")))

(define-public crate-geml-1.1.16 (c (n "geml") (v "1.1.16") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09gfx2q246jhs8vnk7rql89w85rwvlc1d2wwnrcmz15h2w8bg3ym")))

(define-public crate-geml-1.1.17 (c (n "geml") (v "1.1.17") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "010sfv761knvybl569aqzyf638zwkiy4masn02z5g382q4d2zjcs")))

(define-public crate-geml-1.1.18 (c (n "geml") (v "1.1.18") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1c7wgam4xqd8i41z6bax13x33f4yqc04pvh5qbi2aw2nz61957qg")))

(define-public crate-geml-1.1.19 (c (n "geml") (v "1.1.19") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bd7pymyqzb41wwraa15dxs4jjw6diqqlbljw2z1bav1g2f90yla")))

(define-public crate-geml-1.1.20 (c (n "geml") (v "1.1.20") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1fdq05d6ndi3cxvij0vrpj6gir6zk0vx9nq21mcazg7xihps2a99")))

(define-public crate-geml-1.1.21 (c (n "geml") (v "1.1.21") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vb6y7gkhlkrq8mxz6sfdazdijnpc0ign6vf4sxl7950xbsmz0q5")))

(define-public crate-geml-1.1.22 (c (n "geml") (v "1.1.22") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1f0h2qxhib6vmqr87h1nh717w2993m6j9d63hz4zk2g479igrxiy")))

(define-public crate-geml-1.1.23 (c (n "geml") (v "1.1.23") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fi63v415qd28whfv0zw91phwcs7ang14040cbskndjj0rs1lvpg")))

(define-public crate-geml-1.1.24 (c (n "geml") (v "1.1.24") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10d6ns9w21z9j29cdaj46vnqh3sy4a08x47wbv5j4ixpa6xqzhmy")))

(define-public crate-geml-1.1.25 (c (n "geml") (v "1.1.25") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "081qszay0msjiwgq9bj3vvk52fvkvsyppbqk9prs2skaqawkry6w")))

(define-public crate-geml-1.1.26 (c (n "geml") (v "1.1.26") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fdfdz476d9axybl6inllzc2bmqdzp66sna279gl0iqr2fjcri61")))

