(define-module (crates-io ge ml gemla) #:use-module (crates-io))

(define-public crate-gemla-0.1.0 (c (n "gemla") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "file_linked") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0i694myz3b2pjgq3raz0gbvpcw4kc9sb280ppr86rnxjkhkmdz3h")))

(define-public crate-gemla-0.1.2 (c (n "gemla") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "file_linked") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "00bhma5fzxmlklligwjz0ikbxij5231w10xk2n6cgzwchlmlql3c")))

