(define-module (crates-io ge tf getfn) #:use-module (crates-io))

(define-public crate-getfn-0.1.0 (c (n "getfn") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "0dfmb4fx612hgrvb5gdy97k8ncd6n6w07idx9bwi12s3xakin4py")))

(define-public crate-getfn-0.1.1 (c (n "getfn") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (o #t) (d #t) (k 0)))) (h "0c07f7fnbd4fam6sdls6r93kjnddi05c8v665arpqcsw3k3yzgvz")))

(define-public crate-getfn-0.1.2 (c (n "getfn") (v "0.1.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "11wk7pbwyknw05h0bw6hjx9c9w0xwdkaw1v9447449rnz93jiwrh")))

(define-public crate-getfn-0.1.3 (c (n "getfn") (v "0.1.3") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1r4q1qdy5rfbcgz4l1brfgc2zqm0mhx709hk4ddv5n0g7ynsfwps")))

(define-public crate-getfn-0.1.4 (c (n "getfn") (v "0.1.4") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1xyxgdsi13wdvr5z0xv2xncb6p9690nnsrvyim7657lp2zdz4ssb")))

(define-public crate-getfn-0.2.0 (c (n "getfn") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1xkpixkw3qw7v0hby96mp8i3cd25jymzmjfymgfprl5f2qf39rh7")))

