(define-module (crates-io ge tf getfrompass) #:use-module (crates-io))

(define-public crate-getfrompass-0.1.0 (c (n "getfrompass") (v "0.1.0") (h "05xvlmw6j3h69ydnj869b7f0a4zdphdz56k94j46fljwfw5rjsfb")))

(define-public crate-getfrompass-0.1.1 (c (n "getfrompass") (v "0.1.1") (h "1s184gbjb36l0cllazdc7cn6ci5hpdpabs0bjrph3hy4hh2sc0q1")))

(define-public crate-getfrompass-0.1.2 (c (n "getfrompass") (v "0.1.2") (h "1zqhqjm5828dhmb36ppvlhiayfbyhzsgrsybb3zc3dvsnlng3gq9")))

(define-public crate-getfrompass-0.1.3 (c (n "getfrompass") (v "0.1.3") (h "1nn89lkhc36k74y1rlmr5n6j09ij3mxwj3j9pq873va24np6nb6r")))

(define-public crate-getfrompass-0.1.4 (c (n "getfrompass") (v "0.1.4") (h "0m6495mys8dddchc37b0a6v6qd0ds2naww1ksjqawf6h3gfm1v14")))

