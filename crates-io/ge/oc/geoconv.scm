(define-module (crates-io ge oc geoconv) #:use-module (crates-io))

(define-public crate-geoconv-0.1.0 (c (n "geoconv") (v "0.1.0") (h "1mvkggb32qy9rcjl7hx509ix2jbxda7fhj5bxad6wg6dy5v7npc8")))

(define-public crate-geoconv-0.1.1 (c (n "geoconv") (v "0.1.1") (h "1k9gnznd363ww1823kgc8nvk326ap7irmf6xx6v74xnndf00h4jj")))

(define-public crate-geoconv-0.2.0 (c (n "geoconv") (v "0.2.0") (h "1xpikwk8bwqgsd5ngrmsgxz031rh0n265hildzxf5h2sna0blhv0")))

(define-public crate-geoconv-0.2.1 (c (n "geoconv") (v "0.2.1") (h "1kfys62hm24yrvi8b9xfpvivb5b04bdb3d2pi8wkclqfn1kz2f2q")))

(define-public crate-geoconv-0.2.2 (c (n "geoconv") (v "0.2.2") (h "1nzk8vx5dqqlqag8qwhi51ylim0mzz249kfjvfr634fhqgsknszr")))

(define-public crate-geoconv-0.3.0 (c (n "geoconv") (v "0.3.0") (h "1a9h08bz0yi0kfr1q5fr3acqna8q3hwh8sjwcqi083269lib3sl0")))

(define-public crate-geoconv-0.4.0 (c (n "geoconv") (v "0.4.0") (h "147mnkyv2p3j7kqrpqsk73asp80s6j6s4v3mn5jbrwm4svksmsph")))

(define-public crate-geoconv-0.4.1 (c (n "geoconv") (v "0.4.1") (h "0agal1fhqckypvq6y4093skpz8hssnbv60a0mvmwniv9hnxfk2w1")))

