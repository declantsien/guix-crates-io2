(define-module (crates-io ge oc geocal) #:use-module (crates-io))

(define-public crate-geocal-0.1.0 (c (n "geocal") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "geodate") (r "^0.4.0") (d #t) (k 0)))) (h "0zsk6zfsdbr3lv4k2sfkvqpmf8r9p96qwmg9z16y0h4x2cm5nj9h")))

