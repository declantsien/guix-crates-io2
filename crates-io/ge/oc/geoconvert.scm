(define-module (crates-io ge oc geoconvert) #:use-module (crates-io))

(define-public crate-geoconvert-1.0.0 (c (n "geoconvert") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0rixprfwfaxcsb7nyfp80d87z0ncfs1347l3s2q0r8adsm8q9h83") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-geoconvert-1.0.1 (c (n "geoconvert") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09axk1s3pgrb7542074p7sbzndn2cmwyvxxpfwwa3g0mypciymzr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-geoconvert-1.0.2 (c (n "geoconvert") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1hh0d0ys0sxf4vdvl8j0gcjiipc3zy6ylnymwxhsh3pjmarcy5px") (s 2) (e (quote (("serde" "dep:serde"))))))

