(define-module (crates-io ge oc geocoder-abbreviations) #:use-module (crates-io))

(define-public crate-geocoder-abbreviations-0.1.0 (c (n "geocoder-abbreviations") (v "0.1.0") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lqgdrbg85xkmd04sqg1hrw7ipybhmcr8l2j7wh667zkdi8lgsjg")))

(define-public crate-geocoder-abbreviations-0.2.0 (c (n "geocoder-abbreviations") (v "0.2.0") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18yhrqpqb5xvqzwz545s9ngc789330xyf06maz90y9s51125djdn")))

(define-public crate-geocoder-abbreviations-4.1.9 (c (n "geocoder-abbreviations") (v "4.1.9") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wgggq54zyq8narzclkxqkk97nnzh9h8xg41sa4wsxzfjk9073ar")))

(define-public crate-geocoder-abbreviations-4.2.0 (c (n "geocoder-abbreviations") (v "4.2.0") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wvzffl5jpmsn21z3p1r749qcwg2cmryngdpa6j0c8jfgsnny6cw")))

(define-public crate-geocoder-abbreviations-4.3.0 (c (n "geocoder-abbreviations") (v "4.3.0") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ly3f7av1wjxmrkpjpxg8yny1a0lc3sw8pmvsvwi89zyf8f6r8a9")))

(define-public crate-geocoder-abbreviations-4.3.1 (c (n "geocoder-abbreviations") (v "4.3.1") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cnjj99kgli3gshb732a9d02qfbvk0z31f5r9ilyz39qwvmnwiih")))

(define-public crate-geocoder-abbreviations-4.6.0 (c (n "geocoder-abbreviations") (v "4.6.0") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jd7j9p5kzbspy62dx5c0cq3zf195xsk8bw852xy66c95wr6bksq")))

(define-public crate-geocoder-abbreviations-4.6.8 (c (n "geocoder-abbreviations") (v "4.6.8") (d (list (d (n "alphanumeric-sort") (r "^1.0.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hv3dzfsf55s3ihdcvcvwfch9pzrdi2apbas44h45dd0s96d2dda")))

