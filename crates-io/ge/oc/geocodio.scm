(define-module (crates-io ge oc geocodio) #:use-module (crates-io))

(define-public crate-geocodio-0.1.0 (c (n "geocodio") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0gb03g83i41apclcvqqr9z409aylvwxkxvrsb54z4awgqqvz4n18")))

(define-public crate-geocodio-0.1.1 (c (n "geocodio") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "05a34y8whyhyslnddc6whkyp7r5qi1zd4p68rhq0qi50wkjwj7y5")))

(define-public crate-geocodio-0.1.2 (c (n "geocodio") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1gwi8zmx6q716q5rfi4p8y9n7i05sj2g80w0w02p5mnymh1acinr")))

(define-public crate-geocodio-0.1.3 (c (n "geocodio") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "12yf50wvn6zaqxkm9sa57zylg8f2kwgwgbaq3n1a3k641wsa74rc")))

