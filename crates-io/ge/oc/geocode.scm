(define-module (crates-io ge oc geocode) #:use-module (crates-io))

(define-public crate-geocode-0.1.0 (c (n "geocode") (v "0.1.0") (d (list (d (n "cabot") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "0h2d3bn3hrq1j5xhimgpqv3mkj9fxs406kvm6idrfd9391qyby54")))

(define-public crate-geocode-0.1.1 (c (n "geocode") (v "0.1.1") (d (list (d (n "cabot") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1siiic2ah1znhcr78fbzzr4iidqc6ghm8pzpb3aqznzxs5qw6x5s")))

