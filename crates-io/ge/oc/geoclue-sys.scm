(define-module (crates-io ge oc geoclue-sys) #:use-module (crates-io))

(define-public crate-geoclue-sys-0.1.0 (c (n "geoclue-sys") (v "0.1.0") (d (list (d (n "gio-sys") (r "^0.14") (d #t) (k 0)) (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "129l117ajhsncaxyg31c65psqwdzrq7nlih057hfwwihv8iapg82") (f (quote (("dox")))) (l "libgeoclue-2.0")))

(define-public crate-geoclue-sys-0.1.1 (c (n "geoclue-sys") (v "0.1.1") (d (list (d (n "gio-sys") (r "^0.14") (d #t) (k 0)) (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^3") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19jd713kq1xr49yp10zxr62xdbi080mbbhr7i43w2fsxr8vd67nq") (f (quote (("dox")))) (l "libgeoclue-2.0")))

