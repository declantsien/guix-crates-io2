(define-module (crates-io ge tt gettext-macros) #:use-module (crates-io))

(define-public crate-gettext-macros-0.1.0 (c (n "gettext-macros") (v "0.1.0") (d (list (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)))) (h "03a1yczhrxs80xh4i98wdv8a8jgjrbs1zr20x1iq9czqkicyy9j5")))

(define-public crate-gettext-macros-0.2.0 (c (n "gettext-macros") (v "0.2.0") (d (list (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)))) (h "0ncvsbyzbn76sc3ks84v18inzl6i2ix8k8gp0m4j9iq88yp2g2vn")))

(define-public crate-gettext-macros-0.3.0 (c (n "gettext-macros") (v "0.3.0") (d (list (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1a4ankj6cp4489v5jvp1jk5fva6flwgyv77lahf1ss6d1q4shxz9")))

(define-public crate-gettext-macros-0.4.0 (c (n "gettext-macros") (v "0.4.0") (d (list (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)))) (h "13mb6bcdbl4yaqngr39mjyvv1aslwvdjzchz6jzdvjm6bhjv9pib")))

(define-public crate-gettext-macros-0.5.0 (c (n "gettext-macros") (v "0.5.0") (d (list (d (n "gettext") (r "^0.3") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3") (d #t) (k 0)))) (h "0jahmk66cpiy441s5lld0qhqamk6r4yafrasm7g5krdc1dhwsm5x")))

(define-public crate-gettext-macros-0.5.1 (c (n "gettext-macros") (v "0.5.1") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.3") (d #t) (k 0)))) (h "1k9rh49anjvva0z770i0fq5hrd6rgai2ya689nkbn09ss31hmxv2")))

(define-public crate-gettext-macros-0.5.2 (c (n "gettext-macros") (v "0.5.2") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "runtime-fmt") (r "^0.4") (d #t) (k 0)))) (h "0lcx9czgd1xz37rb6by4hx64vnbjk3m34apf512gcpxsdfg8xy7v")))

(define-public crate-gettext-macros-0.6.0 (c (n "gettext-macros") (v "0.6.0") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gkci2j9gkfw5niy86990dn9b63j1l5m8bafz76l1kp9sc8ib058")))

(define-public crate-gettext-macros-0.6.1 (c (n "gettext-macros") (v "0.6.1") (d (list (d (n "gettext") (r "^0.4") (d #t) (k 0)) (d (n "gettext-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cy2zx9i49lmsy74ncjva22yipq2f1pzdfdcdbm92q50rnsv77b4")))

