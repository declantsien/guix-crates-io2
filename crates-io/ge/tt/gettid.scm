(define-module (crates-io ge tt gettid) #:use-module (crates-io))

(define-public crate-gettid-0.1.0 (c (n "gettid") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q1d9k08g10g46fa27228h60mndp3vi5074g725d1visicybkl4n")))

(define-public crate-gettid-0.1.1 (c (n "gettid") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yrx2lwkcld4izhp47d0n6rv8z82zhvar4grd9bx5pwa8xgpr7ry")))

(define-public crate-gettid-0.1.2 (c (n "gettid") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03f6nij2fmjpnb4s5wn86gki3xwgdmgr8bkyxjfqmyvx4x00zcil")))

