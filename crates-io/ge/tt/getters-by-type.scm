(define-module (crates-io ge tt getters-by-type) #:use-module (crates-io))

(define-public crate-getters-by-type-0.1.0 (c (n "getters-by-type") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1w0ah005m1rhdgdnfhax1ghnm9386r9alaavbkgfbz5j0ckvm3vn")))

(define-public crate-getters-by-type-0.1.1 (c (n "getters-by-type") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1ff4576x43fmqxrrwy1vdcysvbhb0lqw8s7f4har4j23kvz6nzzs")))

(define-public crate-getters-by-type-0.1.2 (c (n "getters-by-type") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "052kviixf9id8y5233hlvmiamnbhwcj92ammljw3kl050sdcj0cg")))

(define-public crate-getters-by-type-0.2.0 (c (n "getters-by-type") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0vv4h704nxl3haja49rxbqw9s4mxdlqd6720g8wjp7kxyqwj66vn")))

(define-public crate-getters-by-type-0.2.1 (c (n "getters-by-type") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1y5321m7pf7sa3b5c5gw2hgjzbqvy3vq3cgg88ar9s9k26fxh71r")))

(define-public crate-getters-by-type-0.2.2 (c (n "getters-by-type") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1svr5zrx6jc9fkir5lqjwcdwwkbbjk3s0aznj79vs6m0rrbk5jzr")))

(define-public crate-getters-by-type-0.2.3 (c (n "getters-by-type") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0i7bn3mn4i87p3zggcpmgygjsv4pl0ppp1f74al0j762s4xwvsri")))

(define-public crate-getters-by-type-0.2.4 (c (n "getters-by-type") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1d5g46ifw3fdn2vllh5vs7nbc50dpi3zblqp0hf28asnrzfsjmz5")))

(define-public crate-getters-by-type-0.2.5 (c (n "getters-by-type") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1b6003bpwcqxzzm6plblyl5709xqivd6ws7yh2x4bh483raajid0")))

(define-public crate-getters-by-type-0.2.6 (c (n "getters-by-type") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0cpjg6xdhi63453ls27dzznxpdml3r777f6nx18qlli4bya3965v")))

