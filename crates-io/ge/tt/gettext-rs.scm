(define-module (crates-io ge tt gettext-rs) #:use-module (crates-io))

(define-public crate-gettext-rs-0.1.0 (c (n "gettext-rs") (v "0.1.0") (h "0n7ny2csry5dnh2cfkihrqxmmdwzv1n3jrfmc4g0ymydym2qn1zk")))

(define-public crate-gettext-rs-0.2.0 (c (n "gettext-rs") (v "0.2.0") (h "0yckl8krdfqs5jy95rskqdkflj84rwc62lcyr3n13k9r0wi0y03d")))

(define-public crate-gettext-rs-0.3.0 (c (n "gettext-rs") (v "0.3.0") (h "0pmyzzb1c6jffhw4q8834dcav460yk4ip699fry96pv11rfpwgc2")))

(define-public crate-gettext-rs-0.4.0 (c (n "gettext-rs") (v "0.4.0") (d (list (d (n "gettext-sys") (r "^0.19.8") (d #t) (k 0)) (d (n "locale_config") (r "^0.2") (d #t) (k 0)))) (h "0iqs52djp3xra1v8a5xzzmmzamcsrfg1y5msxslni3vmsl92931v") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4.1 (c (n "gettext-rs") (v "0.4.1") (d (list (d (n "gettext-sys") (r "^0.19.8") (d #t) (k 0)) (d (n "locale_config") (r "^0.2") (d #t) (k 0)))) (h "0j30kjy5lc2aac53a3872zl0ifd5w9mqjyn8zvainrc8w1qj0l5j") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4.2 (c (n "gettext-rs") (v "0.4.2") (d (list (d (n "gettext-sys") (r "^0.19.9") (d #t) (k 0)) (d (n "locale_config") (r "^0.2") (d #t) (k 0)))) (h "082lxgc8ygc1nd9ibc4qg3srzg1b1rlf0sqz0hdygpkxwifmw0vd") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4.3 (c (n "gettext-rs") (v "0.4.3") (d (list (d (n "gettext-sys") (r "^0.19.9") (d #t) (k 0)) (d (n "locale_config") (r "^0.2") (d #t) (k 0)))) (h "19gwh76b9906fbwlkq953ba8mjqa43hk3941hwkvnd2rxi7kcs56") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4.4 (c (n "gettext-rs") (v "0.4.4") (d (list (d (n "gettext-sys") (r "^0.19.9") (d #t) (k 0)) (d (n "locale_config") (r "^0.2") (d #t) (k 0)))) (h "0z6fcsn1g3w9mlgfj6ln6qvqf8610w3zwvk6g062h657v114lifz") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.5.0 (c (n "gettext-rs") (v "0.5.0") (d (list (d (n "gettext-sys") (r "^0.19.9") (d #t) (k 0)) (d (n "locale_config") (r "^0.3") (d #t) (k 0)))) (h "1qc9a63i54b9ad3jx951hn7xb6xf76c9f3hmi2cdy2m7rhczm58v") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.6.0 (c (n "gettext-rs") (v "0.6.0") (d (list (d (n "gettext-sys") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "locale_config") (r "^0.3") (d #t) (k 0)))) (h "040nizg9l5ap2vqgq4d2va2hi6cpykj46g8q1z9xv393vjlygx1x") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.7.0 (c (n "gettext-rs") (v "0.7.0") (d (list (d (n "gettext-sys") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "locale_config") (r "^0.3") (d #t) (k 0)))) (h "0r7kahqcjrkm83d3gzzkn83fnw2bnqj2ank5z6hsm66izalai7p4") (f (quote (("gettext-system" "gettext-sys/gettext-system"))))))

