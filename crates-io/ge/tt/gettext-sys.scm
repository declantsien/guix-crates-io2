(define-module (crates-io ge tt gettext-sys) #:use-module (crates-io))

(define-public crate-gettext-sys-0.19.8 (c (n "gettext-sys") (v "0.19.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mmkgzmzq9h8dyy98fbhq90nzgqasczm683likxhcdxpp3049ik2") (f (quote (("gettext-system")))) (l "gettext")))

(define-public crate-gettext-sys-0.19.9 (c (n "gettext-sys") (v "0.19.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0lzi6ja81vc16mhcdmn3lw35120n9ijhvsy5dh5775mpbfxc8d70") (f (quote (("gettext-system")))) (l "gettext")))

(define-public crate-gettext-sys-0.21.0 (c (n "gettext-sys") (v "0.21.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 1)))) (h "105d5zh67yc5vyzmqxdw7hx82h606ca6rzhsfjgzjczn2s012pc8") (f (quote (("gettext-system")))) (l "gettext")))

(define-public crate-gettext-sys-0.21.1 (c (n "gettext-sys") (v "0.21.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 1)))) (h "0cvnam06cldkxrg795lpww8czvrga5bvknrxv95cyla3z1ppadkb") (f (quote (("gettext-system")))) (l "gettext")))

(define-public crate-gettext-sys-0.21.2 (c (n "gettext-sys") (v "0.21.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 1)))) (h "0acrz4sd2fn2k8lg1pv7sxcn4lx13svs73k6mv4lqlg7p5mf1adg") (f (quote (("gettext-system")))) (l "gettext")))

(define-public crate-gettext-sys-0.21.3 (c (n "gettext-sys") (v "0.21.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "temp-dir") (r "^0.1.11") (d #t) (k 1)))) (h "17c3qdbirxsf9csqzp4z4jaqck2n72z4nw3nh9vhd8jn1zhf4g66") (f (quote (("gettext-system")))) (l "gettext")))

