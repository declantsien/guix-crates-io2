(define-module (crates-io ge tt getter-methods) #:use-module (crates-io))

(define-public crate-getter-methods-0.1.0 (c (n "getter-methods") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmgk8ai2d8pga2p9v4k28118basj6pcqr956m3njbwami2k7b74")))

(define-public crate-getter-methods-0.2.0 (c (n "getter-methods") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0s0v4sizbgy48kn3vbkaqpb935g5s99gh3m8hcl0f5s8dzri8xmx")))

(define-public crate-getter-methods-0.3.0 (c (n "getter-methods") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w179pw3flb5hwqlcrpn48bjb8vs76bccgq88mcd7yxn7ajmz0ys")))

(define-public crate-getter-methods-1.0.0 (c (n "getter-methods") (v "1.0.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0f9qw22690m6qljkkl869a1xw4q4s5ccpgrj9g32zasjmfx995pk")))

