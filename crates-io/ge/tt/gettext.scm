(define-module (crates-io ge tt gettext) #:use-module (crates-io))

(define-public crate-gettext-0.1.0 (c (n "gettext") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "157z06b7asqvbq3m3d10diarsajs8yil6zyx807yv3qw3m36piv9")))

(define-public crate-gettext-0.2.0 (c (n "gettext") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)))) (h "0jpqd38hvh433cih1bcp2j1ri3q02jhh0f1zxi300qyiqk9x3h9r")))

(define-public crate-gettext-0.3.0 (c (n "gettext") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)))) (h "1x1iwbx7kfgasv3mid7ycbmzx3imbjd94h4gsjqgs76mkzhbhy23")))

(define-public crate-gettext-0.4.0 (c (n "gettext") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)))) (h "0wd9kfy7nmbrqx2znw186la99as8y265lvh3pvj9fn9xfm75kfwy")))

