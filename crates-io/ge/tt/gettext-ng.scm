(define-module (crates-io ge tt gettext-ng) #:use-module (crates-io))

(define-public crate-gettext-ng-0.4.1 (c (n "gettext-ng") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)))) (h "0d6glpyvdbh3vb2ds7jzb4n92195i829b0zwcppmbcnyf7l6pj62")))

