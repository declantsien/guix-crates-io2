(define-module (crates-io ge tt getter-derive-rs) #:use-module (crates-io))

(define-public crate-getter-derive-rs-1.0.0 (c (n "getter-derive-rs") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (d #t) (k 0)))) (h "0v0sf6z495ska7dajahwhnqfkgd93cxwlx88f28146wx7l00rq9l")))

(define-public crate-getter-derive-rs-1.0.1 (c (n "getter-derive-rs") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (d #t) (k 0)))) (h "0m943hpmiha556lbv070lswnaiicjpvhkry1pgzwi8wsb85gdsal")))

(define-public crate-getter-derive-rs-1.0.2 (c (n "getter-derive-rs") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (d #t) (k 0)))) (h "016bc5dnasq765455850ld0bz3zca3ch75ccfi4dh29xr1skrfiw")))

