(define-module (crates-io ge tt getters2) #:use-module (crates-io))

(define-public crate-getters2-0.1.0 (c (n "getters2") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1ydk988qjk2291lzx1zq80w5jf48idv2jwxjgnz51mnly8048x")))

(define-public crate-getters2-0.1.2 (c (n "getters2") (v "0.1.2") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0vcy7dl0v0ba3x9zvgf0h0jhjnscydnhpqka6jh22gvbh04v7ai7")))

(define-public crate-getters2-0.1.3 (c (n "getters2") (v "0.1.3") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1rl1kqnm3528nz3k66lnbqx41z676xm3apw7xk2qalzagidylq71")))

(define-public crate-getters2-0.1.4 (c (n "getters2") (v "0.1.4") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "04png5gl9y18wz84xgvc891lpjk2wx0n9j1qd61axganbxg31rpp")))

