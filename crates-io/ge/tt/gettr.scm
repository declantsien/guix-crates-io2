(define-module (crates-io ge tt gettr) #:use-module (crates-io))

(define-public crate-gettr-0.0.1 (c (n "gettr") (v "0.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (o #t) (d #t) (k 0)))) (h "0lhx5w8hdlvd00jzj2jg5qm6kwy7jq65f6bmm3aaxd999xz3dayj") (y #t) (s 2) (e (quote (("yew" "dep:yew"))))))

(define-public crate-gettr-0.0.2 (c (n "gettr") (v "0.0.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "yew") (r "^0.21") (f (quote ("csr"))) (o #t) (d #t) (k 0)))) (h "0dyrq7xrl8h1cc5ryskfpzfrhqjf9c3f06w297lhczbnycnvgnka") (s 2) (e (quote (("yew" "dep:yew"))))))

