(define-module (crates-io ge tt getters0) #:use-module (crates-io))

(define-public crate-getters0-0.1.0 (c (n "getters0") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0q90a42xck6yqa6licavvv96ljmbv50ll3m7srxjp1pnp4w000vx")))

(define-public crate-getters0-0.1.1 (c (n "getters0") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1g0wnsykx1nqyhy50921dlsf2bpk1jc3bnf8sf6fbmpm5klxsmjf")))

