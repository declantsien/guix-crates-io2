(define-module (crates-io ge np genpet) #:use-module (crates-io))

(define-public crate-genpet-0.1.0 (c (n "genpet") (v "0.1.0") (d (list (d (n "graph6-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1si2x57yi93s6jh2gb9vk8wixbmc241pvkf4ibfw2lhfnxny54i4")))

(define-public crate-genpet-0.1.1 (c (n "genpet") (v "0.1.1") (d (list (d (n "graph6-rs") (r "^0.1.8") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1yrg1fykrd1flbyr22zd6n7jqbd5mv5hnig4qg70md45mpflgjxk")))

