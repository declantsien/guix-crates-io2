(define-module (crates-io ge np genp) #:use-module (crates-io))

(define-public crate-genp-0.1.0 (c (n "genp") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "19lsxp6w7l32wbvnldba47avvndp09pqvhk2b8irlyqj6knp0yfz")))

(define-public crate-genp-0.2.0 (c (n "genp") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1z64j6z4j56lxa16j05mpqnhz2ln11rmasqd1ycyqnbrydcdwf48")))

(define-public crate-genp-0.3.0 (c (n "genp") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1rmr5fzw2zm7c65j0rphcichhgshl8dvl5dd2ji7ji2a1fi151qv")))

(define-public crate-genp-0.3.1 (c (n "genp") (v "0.3.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0yk90gfbn3wqalm81ryklrrcyvhzx6c31dm6n3z7w4lf6ca6maj5")))

