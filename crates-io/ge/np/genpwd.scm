(define-module (crates-io ge np genpwd) #:use-module (crates-io))

(define-public crate-genpwd-0.1.0 (c (n "genpwd") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "0wnl2m2fp6pfj24mk6fhvv93pbqx0g40pmrff82v4s5j615adfni")))

(define-public crate-genpwd-0.1.1 (c (n "genpwd") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)))) (h "142dv95qxrwzsg9x512awcgag82aw0vjsgap25w3rfp61w5skllq")))

(define-public crate-genpwd-0.2.0 (c (n "genpwd") (v "0.2.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0r5aj7hmsq438dbnf6xw0br0r5hqhsbsx9q977g5xaz81yr3sm2r")))

