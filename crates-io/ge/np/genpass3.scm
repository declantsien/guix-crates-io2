(define-module (crates-io ge np genpass3) #:use-module (crates-io))

(define-public crate-genpass3-1.0.0 (c (n "genpass3") (v "1.0.0") (h "0j2j6nwq08qfw65jqs683sn7j6idndlsbxq7zkfdlbs67b9psglm")))

(define-public crate-genpass3-1.0.1 (c (n "genpass3") (v "1.0.1") (h "082n87ca7qm6q5qws7xxdm310i9y1kyjgkgmkpa1zrkw7mjhkshh")))

(define-public crate-genpass3-1.1.0 (c (n "genpass3") (v "1.1.0") (h "1bxgyh6cga04wfi3d173abpkhvcnwqpgp9494yad0vyirxxcx3hj")))

