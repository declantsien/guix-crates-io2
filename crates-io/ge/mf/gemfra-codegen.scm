(define-module (crates-io ge mf gemfra-codegen) #:use-module (crates-io))

(define-public crate-gemfra-codegen-0.1.0-dev (c (n "gemfra-codegen") (v "0.1.0-dev") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1anr5p3mlqblajdxn04xcvp1dij17sp9sy02n7qc09vhpvwa5p1v")))

(define-public crate-gemfra-codegen-0.1.0 (c (n "gemfra-codegen") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "073b1wblsd5xna3iadizgkp2d6wh3dr70gsfaimp8axc10v5ik7n")))

