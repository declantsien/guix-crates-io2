(define-module (crates-io ge td getdown) #:use-module (crates-io))

(define-public crate-getdown-0.1.0 (c (n "getdown") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "15wrqcrkkjasn4ii38qp1v394i61b84cvirmlwzrkzl7affkdzmj")))

