(define-module (crates-io l8 as l8ash) #:use-module (crates-io))

(define-public crate-l8ash-0.1.0 (c (n "l8ash") (v "0.1.0") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "ttyui") (r "^0.1.4") (d #t) (k 0)))) (h "1lgdkpfc891dfp7r9czakhp2i5a37rh9lcfh4d6kfikjg0nnjf8l")))

(define-public crate-l8ash-0.1.1 (c (n "l8ash") (v "0.1.1") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "ttyui") (r "^0.1.4") (d #t) (k 0)))) (h "0grbjjjn12wslwsiyw9ypp39pl7rndavl9qdrb0c26ld5xbz13vj")))

