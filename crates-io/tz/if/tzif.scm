(define-module (crates-io tz if tzif) #:use-module (crates-io))

(define-public crate-tzif-0.1.0 (c (n "tzif") (v "0.1.0") (d (list (d (n "combine") (r "^4.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1l560bq1kcyaav5brq626vr67jwdi8pj6c7yr32sg3z2nlfrkmj1")))

(define-public crate-tzif-0.2.0 (c (n "tzif") (v "0.2.0") (d (list (d (n "combine") (r "^4.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "17lyr1yyq5351g3dj1kia6f3qaiq2panqyp5bdcl3ghcfrgzm1kp")))

(define-public crate-tzif-0.2.1 (c (n "tzif") (v "0.2.1") (d (list (d (n "combine") (r "^4.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1wlz4kw6hci4lh7sm0hy3596808dzam5jdix03gvn60inin5q3b6")))

(define-public crate-tzif-0.2.2 (c (n "tzif") (v "0.2.2") (d (list (d (n "combine") (r "^4.6.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1v2gzbx8qdbiyba5zxm49vfgj66hi9mvihcpvi3c8lzhiigw79fa") (r "1.66")))

(define-public crate-tzif-0.2.3 (c (n "tzif") (v "0.2.3") (d (list (d (n "combine") (r "^4.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1zb62s2rnh2a7hs0m8p2ppdjcp6xj5m0kvx7xvrwc1lnj8cfpf31") (r "1.67")))

