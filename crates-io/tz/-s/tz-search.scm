(define-module (crates-io tz -s tz-search) #:use-module (crates-io))

(define-public crate-tz-search-0.1.0 (c (n "tz-search") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1dfpg0l5bli7r54f43sjsyz5nynkmbw7kgvhvd56ysj3xj06mmw0")))

(define-public crate-tz-search-0.1.1 (c (n "tz-search") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0h4rwrr3brbkpc7q4khc9dyi447i7mp0rv0sbkdwp950nwc9brlb")))

