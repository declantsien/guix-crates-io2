(define-module (crates-io tz -r tz-rs) #:use-module (crates-io))

(define-public crate-tz-rs-0.1.0 (c (n "tz-rs") (v "0.1.0") (h "17r7vgwf6c2kiw8grnx1c8iq2xkr2zdh30r3jl4lzj2n6z5dkw0c")))

(define-public crate-tz-rs-0.2.0 (c (n "tz-rs") (v "0.2.0") (h "069r392jgpm3281vjv42h89b0zhxhkqijppn2szzl48i259hnz37")))

(define-public crate-tz-rs-0.3.0 (c (n "tz-rs") (v "0.3.0") (h "0cy7rq8q38iq5layizskbh91f7jfv689byrk867pkdgdqzb3c05c")))

(define-public crate-tz-rs-0.4.0 (c (n "tz-rs") (v "0.4.0") (h "0fxg071p9n5ymy5k7i2jclr7yk76y86q1nzfgn1kjgzc87kk1q5h")))

(define-public crate-tz-rs-0.4.1 (c (n "tz-rs") (v "0.4.1") (h "05f4niwjs5z14j9is9iar53mvf8rr0cmy4pyi4yxhr26k9vqwvcc")))

(define-public crate-tz-rs-0.4.2 (c (n "tz-rs") (v "0.4.2") (h "1qgqcg9i96bpk5jv0m1rnzdkby4kq4alx69sq9gync1k491sbfgk")))

(define-public crate-tz-rs-0.4.3 (c (n "tz-rs") (v "0.4.3") (h "03hxf2pjv61slmr9gxm7yas527rkckxav095c2w2xmh655xacm49")))

(define-public crate-tz-rs-0.4.4 (c (n "tz-rs") (v "0.4.4") (h "1m1rq9n5nx4yps766yd0llfdrrvngcy8rxcflik5xm9fb0zac3cn")))

(define-public crate-tz-rs-0.4.5 (c (n "tz-rs") (v "0.4.5") (h "1lb16v081zdf5b57dd5sxhqn73mmjfla31w6yb60wyc490vd0n5a")))

(define-public crate-tz-rs-0.5.0 (c (n "tz-rs") (v "0.5.0") (h "1yj1aw2d297nkv5hp7ph3srk8bbdnc8wd4dyv942b9sjghf3fk6b")))

(define-public crate-tz-rs-0.5.1 (c (n "tz-rs") (v "0.5.1") (h "103qpc70flc6h83bfan2hg0irxcqx0b8w4xjgpbx4k14pnkr5klj")))

(define-public crate-tz-rs-0.5.2 (c (n "tz-rs") (v "0.5.2") (h "1xamqbqw5lzrgwdijjzrivh5v2g4v9rvrdbm6s1p7yqiyx3x4llh")))

(define-public crate-tz-rs-0.5.3 (c (n "tz-rs") (v "0.5.3") (h "004a120hq0p42xr6cybf5753892k7jwiwxdcqdzwxm8ffs1746ak")))

(define-public crate-tz-rs-0.6.0 (c (n "tz-rs") (v "0.6.0") (h "0h7r75jx20k1vq17k7p60cq9ynyq3jfsdlw95hr1qc6w8qg8qmhd")))

(define-public crate-tz-rs-0.6.1 (c (n "tz-rs") (v "0.6.1") (h "0smlf5w4ak21an2jb6m46gvxchlx0dli7i93jrfxihiiipald28l")))

(define-public crate-tz-rs-0.6.2 (c (n "tz-rs") (v "0.6.2") (h "1j1fvmgpfh2f2cazgzj1g679dcq94anhbj3wr2hg662h9iymm2wp")))

(define-public crate-tz-rs-0.6.3 (c (n "tz-rs") (v "0.6.3") (h "0xgwc16b2vqyjdn2y2ih81rr10d4wr4bdi0fhkijchxqcmg2avij")))

(define-public crate-tz-rs-0.6.4 (c (n "tz-rs") (v "0.6.4") (h "0lj5pbgav184i3ddl1xggfmjmj4gr98d17g89ayxgsgzpcjk8md9")))

(define-public crate-tz-rs-0.6.5 (c (n "tz-rs") (v "0.6.5") (h "007v7hz81a3z12lbal4rr3cznr66930vn67sq5vs4lw4fjy074ks")))

(define-public crate-tz-rs-0.6.6 (c (n "tz-rs") (v "0.6.6") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "073795xvmsy2vdwq2y0bvzzn4cn7bgw0z06zyk3038g0spnz3546") (f (quote (("default" "const") ("const"))))))

(define-public crate-tz-rs-0.6.7 (c (n "tz-rs") (v "0.6.7") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "0y287qznp83vhh300pfn2vlh53arnn3flrqsgkw46hj1p4l8kshd") (f (quote (("default" "const") ("const"))))))

(define-public crate-tz-rs-0.6.8 (c (n "tz-rs") (v "0.6.8") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "1fkrd9xag4zj2ayrcsmravddh6qpzw6y2qg6kdhvm5yx62lv317p") (f (quote (("default" "const") ("const"))))))

(define-public crate-tz-rs-0.6.9 (c (n "tz-rs") (v "0.6.9") (d (list (d (n "const_fn") (r "^0.4") (o #t) (d #t) (k 0)))) (h "19hwwnk140bj3jlgz76l6fyxx8sdxgnhdv0frf8s40q5vxvhaw7d") (f (quote (("default" "const") ("const" "const_fn"))))))

(define-public crate-tz-rs-0.6.10 (c (n "tz-rs") (v "0.6.10") (d (list (d (n "const_fn") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0ay0l8ha60s6yvxqm88q0bi26bfwzd58niwmcjwnqnaw4bg4qghz") (f (quote (("default" "const") ("const" "const_fn"))))))

(define-public crate-tz-rs-0.6.11 (c (n "tz-rs") (v "0.6.11") (d (list (d (n "const_fn") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0prc3027528zhd7j1yajpcfxl3pcjdx6rfqn44g60y9x7qqm9d3y") (f (quote (("default" "const") ("const" "const_fn"))))))

(define-public crate-tz-rs-0.6.12 (c (n "tz-rs") (v "0.6.12") (d (list (d (n "const_fn") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "1c7v8yi71pwil0i3bmllvhvjxlsgmmcy20zrykiy0595lbyj6gav") (f (quote (("std" "alloc") ("default" "std" "const") ("const" "const_fn") ("alloc"))))))

(define-public crate-tz-rs-0.6.13 (c (n "tz-rs") (v "0.6.13") (d (list (d (n "const_fn") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "0qi5dz93nwnz4dvhi360gkgq5m6f7h0cfzmz4sq5ncnqvwg6b9x9") (f (quote (("std" "alloc") ("default" "std" "const") ("const" "const_fn") ("alloc"))))))

(define-public crate-tz-rs-0.6.14 (c (n "tz-rs") (v "0.6.14") (d (list (d (n "const_fn") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "1d720z3p6g65awzv3924dipjnldrdsv6np0h9g7x5yj8r0aip19k") (f (quote (("std" "alloc") ("default" "std" "const") ("const" "const_fn") ("alloc"))))))

