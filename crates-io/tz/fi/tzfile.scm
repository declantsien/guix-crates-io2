(define-module (crates-io tz fi tzfile) #:use-module (crates-io))

(define-public crate-tzfile-0.1.0 (c (n "tzfile") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)))) (h "0fcpigq78gs226myqyl3riypp74v7pc8qmdqk2fmrnm3z69x73n9")))

(define-public crate-tzfile-0.1.1 (c (n "tzfile") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)))) (h "14gdsxqy8r10id83maqpifxsmdr1hka3xvqcr0b51wc0g0mpvys2")))

(define-public crate-tzfile-0.1.2 (c (n "tzfile") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)))) (h "1ak1py0zi88w4625zrxc4qxs580fz0baiawdvm3pjyf3chb3bdfc")))

(define-public crate-tzfile-0.1.3 (c (n "tzfile") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)))) (h "0bjrlhi0wy5560vwjszksyzbxidvfdr01911mp3y8dr55b22577m")))

