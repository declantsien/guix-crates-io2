(define-module (crates-io tz bu tzbuddy) #:use-module (crates-io))

(define-public crate-tzbuddy-0.1.0 (c (n "tzbuddy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1fkcfkkh5krs0jgmddlibqgqbzr787rcncisrqgrv19kr2sqgv4i")))

(define-public crate-tzbuddy-0.1.1 (c (n "tzbuddy") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1lpdpmfhynlfqyrkvdbvwg8x10ivbkrw94b64cz6n57w5kflabm2")))

(define-public crate-tzbuddy-0.1.2 (c (n "tzbuddy") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0qk6dpx0nw5r7xkq0nha4vsdx9kiaqf35wvsyacdbd46lz3prbqs")))

(define-public crate-tzbuddy-0.1.3 (c (n "tzbuddy") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "072k0kb8d0xn5as0yb47xq1sfzf3hwkhzl6gli7ihmsnwwp2f1gz")))

(define-public crate-tzbuddy-0.1.4 (c (n "tzbuddy") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1m9j1mq352f3zk4g21qnkq9r2iajhqrfai5af8z3lbdb8s0qq83z")))

(define-public crate-tzbuddy-0.1.5 (c (n "tzbuddy") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1s1mmk66nyyxbh87mm62sqnccqddr2rmkhsgxzwyzy74cahs6xga")))

(define-public crate-tzbuddy-0.1.6 (c (n "tzbuddy") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0140xyq2cfwlb609zprkn6l861332x88kyyjwigq0faf9yhrrbbi")))

(define-public crate-tzbuddy-0.1.7 (c (n "tzbuddy") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1adajfk8nv0vr2h9s2pa8crxbp0a12idx7d0ip0krg7vijqdq6fx")))

(define-public crate-tzbuddy-0.1.9 (c (n "tzbuddy") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0ks9s2q6zv7psjgr4ajjdvfv345mc9v9j56vz8v0fd2ds1p185lc")))

(define-public crate-tzbuddy-0.1.10 (c (n "tzbuddy") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "1xsjxz2s9327c7pjk2xzly6k9631719zl1mn7kfa8w0p8q9cn89v")))

(define-public crate-tzbuddy-0.2.0 (c (n "tzbuddy") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)))) (h "0w3bpwvw4m6mzpf34gm0rmrbi0qjn0yrkkw6dri1rcmfyi3rb7pd")))

(define-public crate-tzbuddy-0.3.0 (c (n "tzbuddy") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0awvwmcw9249fsl934l65n5jn32nbmgc8pkagrwci8wv5nxfrdhz")))

(define-public crate-tzbuddy-0.3.1 (c (n "tzbuddy") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yy04idy1yjl9cdfay3kw7s0nvfzk5jnnfngqqsm1bax4vgcjr04")))

