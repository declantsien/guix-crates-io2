(define-module (crates-io tz ta tztail) #:use-module (crates-io))

(define-public crate-tztail-1.0.0 (c (n "tztail") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "hourglass") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "047rcja4sdfbblqmk090znh5xxwa8wgf7caky25ws8lsb22shhz4")))

(define-public crate-tztail-1.1.0 (c (n "tztail") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "hourglass") (r "0.*") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "01ivpagg48nfdg270pqxw8aaw7rf10jrivsvjqam454swyg0i4vg")))

(define-public crate-tztail-1.1.1 (c (n "tztail") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "escargot") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0c1hfwydb9c2ar26cq9my38zlrk1x41a8wisdhyi5sbrp4nfisq0")))

(define-public crate-tztail-1.2.0 (c (n "tztail") (v "1.2.0") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "escargot") (r "^0.3.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kirpdlzllrsp44nzair99vcm07gyvli4iwnrddsp1f5wnykc7aa")))

