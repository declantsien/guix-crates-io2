(define-module (crates-io tz pa tzparse) #:use-module (crates-io))

(define-public crate-tzparse-0.3.0 (c (n "tzparse") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^0.3.0") (d #t) (k 0)))) (h "1r6c4rf4l73rfxlc3dz277lzppngvw9dzmwimka7f50ar3aix61g")))

(define-public crate-tzparse-0.3.1 (c (n "tzparse") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^0.4.0") (d #t) (k 0)))) (h "1yjpxkmnzj7fymnhim5yz88zjag27dkr8si4d98yyyk5d5nqn94z")))

(define-public crate-tzparse-0.3.2 (c (n "tzparse") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^0.4.0") (d #t) (k 0)))) (h "1n3q3ag96frgp3sk9s8jjillai6sk836vmj6ni6chc6rvs5a5dsg")))

(define-public crate-tzparse-0.3.3 (c (n "tzparse") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^0.5.1") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "129k5834fzapamx515j3szgd6h6v1xn64wv1mwihf7ws71szxyy5")))

(define-public crate-tzparse-0.3.4 (c (n "tzparse") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.0") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "0hy9jxkxi9f3mmb54rk53n4nm3rqkmcwg17l8apzxvf1ki8nanv8")))

(define-public crate-tzparse-0.3.5 (c (n "tzparse") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.1") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "08i0v971882231bh4f707dl23mygxbb7jkm8yy8cpxi8hncx0d4w")))

(define-public crate-tzparse-0.4.0 (c (n "tzparse") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.1") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "0i3609dakq2zrs3ayby3awnarfzw3ykvgp752gda67pq0yb1b60z")))

(define-public crate-tzparse-0.5.0 (c (n "tzparse") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.2") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "1z651f1zbak9qa295ls9m356ryfk5pblgf8f4h86lcpqd2admz11")))

(define-public crate-tzparse-0.5.1 (c (n "tzparse") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.3") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "1n5l7vbw61ggfkqav782fj07l14gsc5bkf0mb9vvk9xvfjcrdzp7")))

(define-public crate-tzparse-0.5.2 (c (n "tzparse") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.4") (f (quote ("with-chrono"))) (d #t) (k 0)))) (h "0wayfp9x2nprm31mmp8n2yppvh253r272l2ak3nh8fdmvy4y27qw")))

(define-public crate-tzparse-1.0.0 (c (n "tzparse") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.5") (f (quote ("with-chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w78g5lf13yh6k1d2praqqfrwv9rwcddbis76wvffjg2hrnr12nh") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-tzparse-1.0.1 (c (n "tzparse") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.5") (f (quote ("with-chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r9jr59g06qqygk2p2b0mdrwp9zvbdksrq0za3c6vmk8j5yf0spw") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.0.2 (c (n "tzparse") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.6") (f (quote ("with-chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i60r34b9csmk0k2z0iw9dc1i0zfj41zgixrqhr9zrsk8qdrpdar") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.0.3 (c (n "tzparse") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.0.6") (f (quote ("with-chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f9n3jmiqsdd95bimwcgzf4zrvjfw6j3nkf5jkkimfvmqr6wrf0i") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.1.0 (c (n "tzparse") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0kcg1rqxpy8q28q517kral59l2jj3mvk2791ymraljgca7sapa3i") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.1.1 (c (n "tzparse") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mg70fp29y9ywg9rjjdi1d8qwp8ai0x3na7zr1jh8lzz50qsq52y") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.1.2 (c (n "tzparse") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libtzfile") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "112c8bybnns3hv45791crl9gp5fk7wnq3ja6w38a7wdf4p39v4xk") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

(define-public crate-tzparse-1.1.3 (c (n "tzparse") (v "1.1.3") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "libtzfile") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (o #t) (d #t) (k 0)))) (h "14mi5al39mpn9rs8cgq7g73qda0yxrambmj2blga854vkvw6hp33") (f (quote (("json" "serde" "serde_json" "chrono/serde"))))))

