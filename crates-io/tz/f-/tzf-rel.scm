(define-module (crates-io tz f- tzf-rel) #:use-module (crates-io))

(define-public crate-tzf-rel-0.0.2022-f5 (c (n "tzf-rel") (v "0.0.2022-f5") (h "1k1al8q2ll1c0c4w8gaxnmpyrfqd3n66jz174pdwcc1qyibgzv4c")))

(define-public crate-tzf-rel-0.0.2022-g (c (n "tzf-rel") (v "0.0.2022-g") (h "19iqj7csw6sbvx8hy2vylklgfmpp5q77l0x6ps1nm5wpa90igm0r")))

(define-public crate-tzf-rel-0.0.2022-g1 (c (n "tzf-rel") (v "0.0.2022-g1") (h "0jq6af05z64m8vfmwvjyf5jdqsnfr3mxnvq891wblipj3jfxjh4p")))

(define-public crate-tzf-rel-0.0.2023-b (c (n "tzf-rel") (v "0.0.2023-b") (h "0rcwcvlz77xkmi3a9i1245h6j3zsns45ca6kyi73k71q2j4l81g3")))

(define-public crate-tzf-rel-0.0.2023-d (c (n "tzf-rel") (v "0.0.2023-d") (h "04assg3d8lf2m8a9dkvpx1l0rx7lk6398b1jdbh9whzyhvra01vg")))

(define-public crate-tzf-rel-0.0.2024-a (c (n "tzf-rel") (v "0.0.2024-a") (h "16aawyy9xhqibnkbd8rcp1kjpf7dcyi765d4ah2asnhv8kdlg9ix")))

