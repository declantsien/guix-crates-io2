(define-module (crates-io tz db tzdb_data) #:use-module (crates-io))

(define-public crate-tzdb_data-0.1.0-pre.1 (c (n "tzdb_data") (v "0.1.0-pre.1") (d (list (d (n "tz-rs") (r "^0.6.14") (f (quote ("const"))) (k 0)))) (h "0bg8s6hgr84zbclx75gq4a1zvaaznw46cz3q37x413g9f8khjxi5")))

(define-public crate-tzdb_data-0.1.0 (c (n "tzdb_data") (v "0.1.0") (d (list (d (n "tz-rs") (r "^0.6.14") (f (quote ("const"))) (k 0)))) (h "1czliwrr4xix7vgxcylv2y7msyg2d71s5c17nh8k004fpaqr2dmn")))

(define-public crate-tzdb_data-0.1.1 (c (n "tzdb_data") (v "0.1.1") (d (list (d (n "tz-rs") (r "^0.6.14") (f (quote ("const"))) (k 0)))) (h "0xibpf5p9vgp4yiq5c50sgy1vdmjm0ar8scqvv00sgqzjb95b5b2")))

(define-public crate-tzdb_data-0.1.2 (c (n "tzdb_data") (v "0.1.2") (d (list (d (n "tz-rs") (r "^0.6.14") (f (quote ("const"))) (k 0)))) (h "1mlqm5z8324hw2gnwhlgfn6as8cv5qhdahjwv70nb7f0zbgrz26i")))

