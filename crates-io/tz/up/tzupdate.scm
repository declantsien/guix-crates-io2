(define-module (crates-io tz up tzupdate) #:use-module (crates-io))

(define-public crate-tzupdate-3.0.0 (c (n "tzupdate") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("humantime"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json" "rustls" "tls"))) (k 0)))) (h "0j3wyvbnfp03grls0g7pb570hw59qfj97zy555llw1y9qaz7q59x") (r "1.64.0")))

(define-public crate-tzupdate-3.1.0 (c (n "tzupdate") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("std" "derive" "help"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("humantime"))) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("json" "rustls" "tls"))) (k 0)))) (h "0yrxgh2np055syi6nxj68wwfx52iscr0mxvkj1h1hb15c71pr14l") (r "1.64.0")))

