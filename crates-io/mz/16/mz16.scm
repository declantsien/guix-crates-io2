(define-module (crates-io mz #{16}# mz16) #:use-module (crates-io))

(define-public crate-mz16-0.1.0 (c (n "mz16") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0jcb282ji42jy9nyfqaij2ihga0zpsbdwm0mxqfy3ksy2cbblmc6")))

(define-public crate-mz16-0.1.1 (c (n "mz16") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "0lh6lwwzvwr7ach8phm30r2d2r1irmfdsaizdabh7wwpx804d8hi")))

(define-public crate-mz16-0.1.2 (c (n "mz16") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)))) (h "143q133jxrcln72xslhjw2ph86rv1n3rd8sfqxnkxx3hcn3hz22k")))

