(define-module (crates-io mz _r mz_rusoto_mobile) #:use-module (crates-io))

(define-public crate-mz_rusoto_mobile-0.46.0 (c (n "mz_rusoto_mobile") (v "0.46.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mz_rusoto_core") (r "^0.46.0") (k 0)) (d (n "mz_rusoto_mock") (r "^0.46.0") (k 2)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)))) (h "12i2k0hx5qyiyi1lhck3rcarhj20cq2bfh9ks12vsc078bz0gg26") (f (quote (("serialize_structs" "bytes/serde") ("rustls" "mz_rusoto_core/rustls") ("native-tls" "mz_rusoto_core/native-tls") ("deserialize_structs" "bytes/serde") ("default" "native-tls"))))))

