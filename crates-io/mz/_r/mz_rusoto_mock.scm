(define-module (crates-io mz _r mz_rusoto_mock) #:use-module (crates-io))

(define-public crate-mz_rusoto_mock-0.46.0 (c (n "mz_rusoto_mock") (v "0.46.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mz_rusoto_core") (r "^0.46.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m2890k91ncapadp4fsz4gd0h37hzszsfinwx5jhpxy0xqncysnj") (f (quote (("unstable") ("rustls" "mz_rusoto_core/rustls") ("nightly-testing" "mz_rusoto_core/nightly-testing") ("native-tls" "mz_rusoto_core/native-tls") ("default" "native-tls"))))))

