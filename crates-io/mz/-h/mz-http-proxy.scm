(define-module (crates-io mz -h mz-http-proxy) #:use-module (crates-io))

(define-public crate-mz-http-proxy-0.1.0 (c (n "mz-http-proxy") (v "0.1.0") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "hyper-dep") (r "^0.14.11") (o #t) (d #t) (k 0) (p "hyper")) (d (n "hyper-proxy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (o #t) (d #t) (k 0)))) (h "0f9m089il2rz1bmckkzr9317vr66iqv7rkqnnzczr2yhn7pfyk0j") (f (quote (("hyper" "hyper-dep" "hyper-proxy" "hyper-tls"))))))

