(define-module (crates-io mz #{93}# mz93uxlz) #:use-module (crates-io))

(define-public crate-Mz93uxLZ-0.1.0 (c (n "Mz93uxLZ") (v "0.1.0") (h "0s5j77ddqxb3fg87nn0x1vmn4bklxmg2xliz33wz9104gcf6slin") (y #t)))

(define-public crate-Mz93uxLZ-0.1.1 (c (n "Mz93uxLZ") (v "0.1.1") (h "0bwxsxd0c76z05z4j397j187wkh1h3lkkc8qgwr0hb9zfm8g44fy") (y #t)))

