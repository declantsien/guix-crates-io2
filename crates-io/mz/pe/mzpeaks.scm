(define-module (crates-io mz pe mzpeaks) #:use-module (crates-io))

(define-public crate-mzpeaks-0.1.0 (c (n "mzpeaks") (v "0.1.0") (h "1748cr4gn70qy0avic1nzqpfb5kg7146drrvd67dq9d6j7s7nqzh")))

(define-public crate-mzpeaks-0.2.0 (c (n "mzpeaks") (v "0.2.0") (h "1zfs4m1fd0a1zd32yb7j51paa1j0i2xl4xvdfjz4m792679infy3")))

(define-public crate-mzpeaks-0.3.0 (c (n "mzpeaks") (v "0.3.0") (h "1724288ara3qx1h1vzdjzr7bjnw8afbikr63xc5f6az6rxpjyvc1")))

(define-public crate-mzpeaks-0.4.0 (c (n "mzpeaks") (v "0.4.0") (h "12i1751j092wbm17wglj6lx1fi6h35gdrf37rgz5nwg16jvc4024")))

(define-public crate-mzpeaks-0.5.0 (c (n "mzpeaks") (v "0.5.0") (h "1xccab33c7x70l8hgfgal41wwldprnnffnjjiybrqgx7sczd4688")))

(define-public crate-mzpeaks-0.6.0 (c (n "mzpeaks") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "1v8mf9034sca1gkxq2im9kl1s8n7cq01d8iz52n57rpkvascj190") (f (quote (("serde-support" "serde" "serde_json"))))))

(define-public crate-mzpeaks-0.7.0 (c (n "mzpeaks") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "1kjfrml9qa7b6cl3hqnqa13r5yl77lrv0lgcjszipzqnaqkx79qw") (f (quote (("serde-support" "serde" "serde_json"))))))

(define-public crate-mzpeaks-0.8.0 (c (n "mzpeaks") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (o #t) (d #t) (k 0)))) (h "1w84p1nsr0svvk7ac82b57n8i3p8v72h3p67jq2y0dnm4sgqq1ar") (f (quote (("serde-support" "serde" "serde_json"))))))

(define-public crate-mzpeaks-0.9.0 (c (n "mzpeaks") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0rsyl9w2kghi3n3013ym34hnh7p7kmjrr42kjn3dk6in9p9s86fm") (f (quote (("serde-support" "serde"))))))

(define-public crate-mzpeaks-0.10.0 (c (n "mzpeaks") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "01n1hxyxxyj8zhql1k1ih1kqqpixfl0ndhnyfmxnvdjihfx943ip") (f (quote (("serde-support" "serde"))))))

(define-public crate-mzpeaks-0.11.0 (c (n "mzpeaks") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "194lc0h9llsz7q6shwg3q7f377pv14z7zny2xyrpzfj9rjgb12bq") (f (quote (("serde-support" "serde"))))))

(define-public crate-mzpeaks-0.12.0 (c (n "mzpeaks") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0k8iqi3zvlnd6ch4zx7v1kmf4rl45iylfm0lzf8g3m5sh0sfmdph") (f (quote (("serde-support" "serde"))))))

(define-public crate-mzpeaks-0.13.0 (c (n "mzpeaks") (v "0.13.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "07myr90laa6qgq32i5agqn0gq31sqnbd490lmrr7p9i1xq0cvxg7") (f (quote (("serde-support" "serde"))))))

