(define-module (crates-io xh ci xhci) #:use-module (crates-io))

(define-public crate-xhci-0.1.0 (c (n "xhci") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1l0mvq8cn9yqwkj4srh31cyqv8lra2wgbjx1dpkx1rsvbj4g26y1")))

(define-public crate-xhci-0.2.0 (c (n "xhci") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0jcfrsy1cipc57pwlxxq7bq8ypb2rrli0vlcyz8d272rsq0dch8m")))

(define-public crate-xhci-0.2.1 (c (n "xhci") (v "0.2.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1i1aj26lfdvhspb8ayzq6sc6sc655caz19glr6r40q3vbh9ym3cg")))

(define-public crate-xhci-0.2.2 (c (n "xhci") (v "0.2.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0a0r95wn822ikxiy027rd0jf54qw75zc4hvplykcxxh1n9v4qs6y")))

(define-public crate-xhci-0.2.3 (c (n "xhci") (v "0.2.3") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0zslz4l2qsmyv5rpswax7syabqz63l4kn6xwrvqrp6q6bnxibiw1")))

(define-public crate-xhci-0.2.4 (c (n "xhci") (v "0.2.4") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "041qdirdyc3qi70sszf6wfjpvq8z4hdr7rcfid0y0frivgyyrfvw")))

(define-public crate-xhci-0.2.5 (c (n "xhci") (v "0.2.5") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "165xdany5245hr785w0xgbvyldabxpdqpc75zvg7yj2an535sspc")))

(define-public crate-xhci-0.2.6 (c (n "xhci") (v "0.2.6") (d (list (d (n "accessor") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "08zj3ifhiggdr5ywsfy2qsvr6dbndqsnp14srqkxr21ilryqac98")))

(define-public crate-xhci-0.3.0 (c (n "xhci") (v "0.3.0") (d (list (d (n "accessor") (r "^0.2.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1jbj1y0vzzz39kgi645dgbm04r56lb5dlsspf75iqknz4vrhc3ar")))

(define-public crate-xhci-0.4.0 (c (n "xhci") (v "0.4.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0pr5fbmxqfjzy43x0rbvrcnlmb1y70ykhsr2r9a6ks8plyv1jc6i")))

(define-public crate-xhci-0.4.1 (c (n "xhci") (v "0.4.1") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1rrbbm2db5qcn54miqbwm22fsjg0n4fdj0zkh4v2br14322s2qy0")))

(define-public crate-xhci-0.5.0 (c (n "xhci") (v "0.5.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1s08rip2fh1li69fr2fy5xgz6vrj2p0fqn14p2faj32cbk87dld0")))

(define-public crate-xhci-0.5.1 (c (n "xhci") (v "0.5.1") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0qgyqqwjix6i1iq69r0yc986qx416dm5zhxay86wbv9l6pl4w761")))

(define-public crate-xhci-0.5.2 (c (n "xhci") (v "0.5.2") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0qwx2ixwzi6hbhhyrjwzgl693d9vira1y4m6z6d3pd9i6d5yvx1l")))

(define-public crate-xhci-0.5.3 (c (n "xhci") (v "0.5.3") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "18yvhd7ff8mmc5mp2539q6l16wdr04wkyyqkmxzvxjz8xclsqzzb")))

(define-public crate-xhci-0.5.4 (c (n "xhci") (v "0.5.4") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0f0mhj4vxbmxgif3zxj4cvx0dmz9pycfp0hgn5cwbzz5v3pwqaff")))

(define-public crate-xhci-0.5.5 (c (n "xhci") (v "0.5.5") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "08hi3j63rzswhhzl5mfashxlz4i0pn41lnicr2z467ip9gzdfkr7")))

(define-public crate-xhci-0.5.6 (c (n "xhci") (v "0.5.6") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1s49qdqffy11z9li0yimf2qwgp1zhkjnsyfk0d2g815iklqrfv37")))

(define-public crate-xhci-0.6.0 (c (n "xhci") (v "0.6.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "15vzg4k6bbsv88ri7ab9ybcsp0hl2nhz79gqanrg3srp53df2d4r")))

(define-public crate-xhci-0.7.0 (c (n "xhci") (v "0.7.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0wlxxwv5ccksrcrd9kvkxi9cwizk5szvvc4myjh3fsvjlw9wrisl")))

(define-public crate-xhci-0.8.0 (c (n "xhci") (v "0.8.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0sibb03l76fa578ijgmym2kj31av77jzixpfcm9cjmqdpj75q5hf")))

(define-public crate-xhci-0.8.1 (c (n "xhci") (v "0.8.1") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1flkapa7hfysqy0g1ddgmpplklaqds8g9jj3xl66ip1526pzd0y4")))

(define-public crate-xhci-0.8.2 (c (n "xhci") (v "0.8.2") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0nzm9d906zppkxpja0ygrsv6fw51y5qylnc20253qpl2np9zwqf5")))

(define-public crate-xhci-0.8.3 (c (n "xhci") (v "0.8.3") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1k0nxl3rcz5nif1vkcyqahxsrbyh1092pqmvbrsjn18803r2b902")))

(define-public crate-xhci-0.8.4 (c (n "xhci") (v "0.8.4") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0nln7pyyns1fr38dc4amxiqjabzw3nd12vp6l8snvrwg7dhkcvwy")))

(define-public crate-xhci-0.8.5 (c (n "xhci") (v "0.8.5") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "00akmnfgh2ps8a1bnxcyn51s61v2fcvcwj2aln88palg3dfmnssw")))

(define-public crate-xhci-0.8.6 (c (n "xhci") (v "0.8.6") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0v1fsa6pym9z6y1l1rqglic28chzb15zpaqs31n9hhrhpaa3divq")))

(define-public crate-xhci-0.8.7 (c (n "xhci") (v "0.8.7") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0jb3pjaimlzmk94i2x1qmhsswacnva6glaxjn2lb1c74qiyiizai")))

(define-public crate-xhci-0.9.0 (c (n "xhci") (v "0.9.0") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1nqdqrsnhzv1ll16hrpgcy1d8lvnid8r918jsdwq03g2v37qvkd3")))

(define-public crate-xhci-0.9.1 (c (n "xhci") (v "0.9.1") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0qwqvlaz4dhp4lp05hwp5kj9rfi4c4vq5icl5qkxb86fc9vdin5a")))

(define-public crate-xhci-0.9.2 (c (n "xhci") (v "0.9.2") (d (list (d (n "accessor") (r "^0.3.0") (d #t) (k 0)) (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "10zzhcyrnfyaqsk7v8wzwm9c6jnr0120a935vcm9y9b9754g1wq9")))

