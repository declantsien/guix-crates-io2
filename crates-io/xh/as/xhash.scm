(define-module (crates-io xh as xhash) #:use-module (crates-io))

(define-public crate-xhash-0.1.0 (c (n "xhash") (v "0.1.0") (d (list (d (n "gxhash") (r "^2.2.3") (d #t) (k 0)))) (h "0mbbaslaaiwyymbd2hfkg3c1rs3mkss1i96mp1sragwg4i8m0nyr")))

(define-public crate-xhash-0.1.1 (c (n "xhash") (v "0.1.1") (d (list (d (n "gxhash") (r "^2.3.0") (d #t) (k 0)))) (h "0rdqzxsp2mwz7jpi1rk39lhdcmd39w4z301xb2mi254i93hay1qh")))

(define-public crate-xhash-0.1.2 (c (n "xhash") (v "0.1.2") (d (list (d (n "gxhash") (r "^3.0.0") (d #t) (k 0)))) (h "1l4qbij7jq6bq59s5qmz4im6dr24ldn4pwf5iflhkf9bixk570dq")))

(define-public crate-xhash-0.1.3 (c (n "xhash") (v "0.1.3") (d (list (d (n "gxhash") (r "^3.1.0") (d #t) (k 0)))) (h "01djcm451np73f75k6l58qjisgh1yl4irbxlcrjwlx0r27imygl2")))

(define-public crate-xhash-0.1.4 (c (n "xhash") (v "0.1.4") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "082f82g9l3cacb0511xm3xzbyr9dsb9dv8sr6zkx96qpdkf2lv03")))

(define-public crate-xhash-0.1.5 (c (n "xhash") (v "0.1.5") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "1a239hcifcq08insj0ayisl2wxg567815710lyd685nbrscpz5c2")))

(define-public crate-xhash-0.1.6 (c (n "xhash") (v "0.1.6") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "1h7za031n2vmbb91kvyh5sbg6sn2v4s1lhpbxq4iry4plssgq51b") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.7 (c (n "xhash") (v "0.1.7") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "1clfnplxq07mfjh19wq2yd9j848my4nz7hpf583s6z3qib4c9cnz") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.8 (c (n "xhash") (v "0.1.8") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "17i2q36ki3xdkqgfnvys4wwx5xj2703b04v6wqsi36zlzw2c6hsm") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.9 (c (n "xhash") (v "0.1.9") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "19mkyk3vcrzipdk9qq7q4lqsxk15sb6mcsv2kkn5sr82r7arlvs8") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.10 (c (n "xhash") (v "0.1.10") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "13dnz5sq055c72x9r9jx6wjhd7148a79k0ynb2wvvgp5ypbj0gqp") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.11 (c (n "xhash") (v "0.1.11") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "0xh3im8sh7dsf3bsj85drh5zcx7jildgimr0h13mnar1dfchfbp1") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.12 (c (n "xhash") (v "0.1.12") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "0dm1s4hxkk9yfs2s8c14w7chh0smpkm6drpxijk86hg3m8if2hzj") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.13 (c (n "xhash") (v "0.1.13") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "1dlxxm939i9ghvba1hslfyds3zs6fw8yy773bp96snb90ipllidx") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.15 (c (n "xhash") (v "0.1.15") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)))) (h "0d2axpg1g8b54i45qr8v86qf8k3vgq2gdqgahd5423k8sndn00h2") (f (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1.16 (c (n "xhash") (v "0.1.16") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)))) (h "1g57i9vndzgfba54if5ds266gbwp8xya79y6qb12r25zn8h883xv") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1.17 (c (n "xhash") (v "0.1.17") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)))) (h "00rnm5b7ssdizrpprqgrx9qi6bz11hnvnpqlxb1m0200qa4lp5gb") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1.18 (c (n "xhash") (v "0.1.18") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (d #t) (k 0)))) (h "1yqrl8xdwc0sgc4i76xv2z6vgwb53sdjzgf441mxyh02vi5zg90x") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1.19 (c (n "xhash") (v "0.1.19") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (o #t) (d #t) (k 0)))) (h "0xhnmpprxws3ikyq7f543vb73b1r422vq6zrab0x8r5wnz3vcwaw") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (s 2) (e (quote (("speedy" "dep:speedy" "bin_li"))))))

(define-public crate-xhash-0.1.20 (c (n "xhash") (v "0.1.20") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (o #t) (d #t) (k 0)))) (h "0b6pkrc6gw5lyadzss9f3bhraz2n7cbvsp5s2bx7pnvxh8yhffmy") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (s 2) (e (quote (("speedy" "dep:speedy" "bin_li"))))))

(define-public crate-xhash-0.1.21 (c (n "xhash") (v "0.1.21") (d (list (d (n "gxhash") (r "^3.1.1") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 0)) (d (n "speedy") (r "^0.8.7") (o #t) (d #t) (k 0)))) (h "070i68hnbas85qha22ycs7a5xm63rh5qycha6yhs7y7gyfwcj0sy") (f (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (s 2) (e (quote (("speedy" "dep:speedy" "bin_li"))))))

