(define-module (crates-io xh sl xhsleepfor) #:use-module (crates-io))

(define-public crate-xhSleepFor-0.1.0 (c (n "xhSleepFor") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1x8x5rpz2sa6nnckyg1ligiz2a3b3fzzhd14mdhcr3sacab05b2i")))

(define-public crate-xhSleepFor-0.1.1 (c (n "xhSleepFor") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0v6fnscbvyjxbgabw4f10p39khw6v7kkc83s5k7wbwi61cmlhyf5")))

(define-public crate-xhSleepFor-0.1.2 (c (n "xhSleepFor") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0fx7kzir0vz229ihnmwav61h891r3k5kh5bnfc3pfmdfpm9zhp0w")))

