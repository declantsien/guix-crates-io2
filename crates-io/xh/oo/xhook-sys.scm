(define-module (crates-io xh oo xhook-sys) #:use-module (crates-io))

(define-public crate-xhook-sys-0.1.0 (c (n "xhook-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yg6magm8s6l12sfrnfpwayqja47v46w8raz86fa9jqiwagwg4yp")))

(define-public crate-xhook-sys-0.2.0 (c (n "xhook-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fpkgbdli3wpq6w936gkzf2ngm3c8sflcixivs95yldb1mflpypp")))

