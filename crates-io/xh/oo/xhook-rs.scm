(define-module (crates-io xh oo xhook-rs) #:use-module (crates-io))

(define-public crate-xhook-rs-0.1.0 (c (n "xhook-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xhook-sys") (r "^0.1.0") (d #t) (k 0)))) (h "06v9yfm2rqx924q1jh6364f16a4pphvsv72xz36ma3vya9miznv0")))

(define-public crate-xhook-rs-0.2.0 (c (n "xhook-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xhook-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0w9n4qc5dg7qnbqhm6q213vwzcpnd7z19q8nzyg022mn7wwyr1nj")))

