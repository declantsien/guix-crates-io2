(define-module (crates-io xh tm xhtmlchardet) #:use-module (crates-io))

(define-public crate-xhtmlchardet-0.1.0 (c (n "xhtmlchardet") (v "0.1.0") (d (list (d (n "toml") (r "*") (d #t) (k 2)))) (h "0q9y9a1n5vyx3pvsb3s0g3vmna16xmrb8qpi59wa45yfx1zv90fd")))

(define-public crate-xhtmlchardet-1.0.0 (c (n "xhtmlchardet") (v "1.0.0") (d (list (d (n "toml") (r "^0.1.23") (d #t) (k 2)))) (h "0wd7izwwhf1c637hc0zc95zmcs4xn07dlnzng7cb639gmlm3dpcb")))

(define-public crate-xhtmlchardet-1.0.1 (c (n "xhtmlchardet") (v "1.0.1") (d (list (d (n "toml") (r "^0.1.23") (d #t) (k 2)))) (h "1zzmhbr7142c5plnx06day0wvi1w1lc1grdw0zqfyqv1c8isc0j7")))

(define-public crate-xhtmlchardet-2.0.0 (c (n "xhtmlchardet") (v "2.0.0") (d (list (d (n "toml") (r "^0.2.1") (f (quote ("serde"))) (k 2)))) (h "1mqys50avnr8h5r9qi32iy25jk2ng3qid2wb1h57adnianpxbnzv")))

(define-public crate-xhtmlchardet-2.1.0 (c (n "xhtmlchardet") (v "2.1.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^0.8") (d #t) (k 2)) (d (n "toml") (r "^0.2.1") (f (quote ("serde"))) (k 2)))) (h "13dx08mxxk97s385a5ppvjf46m53h5hsmcpay1manzfrz3b0i4mk")))

(define-public crate-xhtmlchardet-2.2.0 (c (n "xhtmlchardet") (v "2.2.0") (d (list (d (n "serde") (r "^0.8") (d #t) (k 2)) (d (n "serde_derive") (r "^0.8") (d #t) (k 2)) (d (n "toml") (r "^0.2.1") (f (quote ("serde"))) (k 2)))) (h "0cz187qsrp55da7aws10fqmr79jd5blh0wqa6lkg8m499rq73i5c")))

