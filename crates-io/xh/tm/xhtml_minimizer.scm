(define-module (crates-io xh tm xhtml_minimizer) #:use-module (crates-io))

(define-public crate-xhtml_minimizer-0.1.1 (c (n "xhtml_minimizer") (v "0.1.1") (d (list (d (n "xml_tokens") (r "^0.1.4") (d #t) (k 0)))) (h "1m70yc1dgxw9ky6w0r6b58f9q98i6dwfcsvrkm81i1rj8lj75zym")))

(define-public crate-xhtml_minimizer-0.1.2 (c (n "xhtml_minimizer") (v "0.1.2") (d (list (d (n "xml_tokens") (r "^0.1.4") (d #t) (k 0)))) (h "1sgmq0smfrim6frlpipnf8kzwsds253jc1dvajmxglaa5k6kxkch")))

(define-public crate-xhtml_minimizer-0.1.3 (c (n "xhtml_minimizer") (v "0.1.3") (d (list (d (n "xml_tokens") (r "^0.1.4") (d #t) (k 0)))) (h "0mk1blxc3n4m6hgjbw5nwa5pl6064xgkzqg1yvcy6zbimdg2fmiy")))

(define-public crate-xhtml_minimizer-0.1.4 (c (n "xhtml_minimizer") (v "0.1.4") (d (list (d (n "xml_tokens") (r "^0.1.4") (d #t) (k 0)))) (h "1pnxyivpvs3fyzwd7r0yl2qv7i56d2kvs4sw1pwg2z7ipaifx3h4")))

