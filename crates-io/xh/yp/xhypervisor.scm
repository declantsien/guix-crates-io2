(define-module (crates-io xh yp xhypervisor) #:use-module (crates-io))

(define-public crate-xhypervisor-0.0.9 (c (n "xhypervisor") (v "0.0.9") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0pyh3jx0a7634c8y7lm31zwbmqqfyk0hzg30dc3w6q0xyb6b5384")))

(define-public crate-xhypervisor-0.0.10 (c (n "xhypervisor") (v "0.0.10") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "07hzvkm6hpb03yqc2shx2c63zd988sw07jssbgzrc17jr3gfg2v8")))

(define-public crate-xhypervisor-0.0.11 (c (n "xhypervisor") (v "0.0.11") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "148py4yykn7qq2b79j0vk31yjjvf0af6drbhx95sy2ifx2kxickz")))

(define-public crate-xhypervisor-0.0.12 (c (n "xhypervisor") (v "0.0.12") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "12jz6n0f5ba1c5myyagxf56jrbsl1c16nkszfv2dm584cdr74fk4")))

(define-public crate-xhypervisor-0.2.0 (c (n "xhypervisor") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l9my604dv7s449viwih48rvrcx6yq9i574y83m04n4z7dl0gpsa")))

