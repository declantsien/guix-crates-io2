(define-module (crates-io ub it ubits) #:use-module (crates-io))

(define-public crate-ubits-0.1.0 (c (n "ubits") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.2") (d #t) (k 0)))) (h "1n5sba3plnkk1nzbrhdnfy289bbvvgl83lbl1qhqmlvfs7rlkasf")))

(define-public crate-ubits-0.2.0 (c (n "ubits") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.2") (d #t) (k 0)))) (h "17kzmw9xlaa8wbfnvll1qrab2j4qgpj2kllprsc93glwinglsjmr")))

(define-public crate-ubits-0.2.1 (c (n "ubits") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.2") (d #t) (k 0)))) (h "0wzj62ilqj9y2h7kryc290dbv6b82gg9hidqsfvwgjngv1azpq51")))

(define-public crate-ubits-0.3.0 (c (n "ubits") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.2") (d #t) (k 0)))) (h "1x2sspizsm0rmqjd963p0qk728c61jnmhqn2x168wdydmdyxfcaw")))

(define-public crate-ubits-0.3.1 (c (n "ubits") (v "0.3.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.2") (d #t) (k 0)))) (h "1vasdamaydy62k0xzxz14z9cdya08s7wpz3pymfshx3xqfyfwxrf")))

