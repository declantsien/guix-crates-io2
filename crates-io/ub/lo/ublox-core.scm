(define-module (crates-io ub lo ublox-core) #:use-module (crates-io))

(define-public crate-ublox-core-0.1.0 (c (n "ublox-core") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 2)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "panic-semihosting") (r "^0.5.3") (d #t) (k 2)) (d (n "shufflebuf") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32h7") (r "^0.10.0") (f (quote ("stm32h743"))) (d #t) (k 2)))) (h "0kbvlw68nvrp8w27zhaf0dmfcagby2kci8p7zsxys4vnl8nnmg6w")))

