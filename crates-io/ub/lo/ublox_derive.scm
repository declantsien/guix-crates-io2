(define-module (crates-io ub lo ublox_derive) #:use-module (crates-io))

(define-public crate-ublox_derive-0.0.1 (c (n "ublox_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "which") (r "^3.0") (k 2)))) (h "1fgj386jcb7b8if7ljdh1swfs3v2haibg02r5xlkg63xfznh8wcx")))

(define-public crate-ublox_derive-0.0.2 (c (n "ublox_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "which") (r "^3.0") (k 2)))) (h "07fr4k2bl2b9nhivj0qcnwrfy4549y7i4bdn0pqrgvy806ids425")))

(define-public crate-ublox_derive-0.0.3 (c (n "ublox_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "which") (r "^3.0") (k 2)))) (h "00d9zr1rcm8af81df571i1vggbi05dh51anmvnf1a13i88wgpd8b")))

(define-public crate-ublox_derive-0.0.4 (c (n "ublox_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "which") (r "^3.0") (k 2)))) (h "0ylk2s6wyr1j80f1ywc17kkhqzcpinmldkc8mr4j27scr5394qb0")))

(define-public crate-ublox_derive-0.1.0 (c (n "ublox_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (k 2)))) (h "0s9y1s7gh1w7zvyy314r5acppisbhn1bnvig2svjj8vdr0ihz9ny")))

