(define-module (crates-io ub er uberbyte) #:use-module (crates-io))

(define-public crate-uberbyte-0.0.1 (c (n "uberbyte") (v "0.0.1") (h "1qxjjb275qv7mcql6gzkkl02a3d5lp2bgm70v0w030zdfgiifrrh")))

(define-public crate-uberbyte-0.0.2 (c (n "uberbyte") (v "0.0.2") (h "1bsdx4rqq3bmf6d1ykqdsh54b9xpmv45yiw0lvzg6zx0w0c35320")))

(define-public crate-uberbyte-0.5.0 (c (n "uberbyte") (v "0.5.0") (h "007pc0lkwp4rxqgbckhnmcf0ja50rkzfpwvl2c1mwfp2hca4419a")))

(define-public crate-uberbyte-0.6.0 (c (n "uberbyte") (v "0.6.0") (h "00ml67han5f125ha6pwy9q0q4fy7llcqgz2rii02zwdn57h6mhwz")))

(define-public crate-uberbyte-0.6.1 (c (n "uberbyte") (v "0.6.1") (h "1jqginw17g6qnsdi4irxy543hrcdr8klvgyj86rp8wzkhbclpqjc")))

