(define-module (crates-io ub en ubend) #:use-module (crates-io))

(define-public crate-ubend-0.1.0 (c (n "ubend") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1i2i9pb77kn9yyfs2khcjgvcycjylwi2a5d6bzbmq6l9bnqlvas1")))

(define-public crate-ubend-0.1.1 (c (n "ubend") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "15ygnhbhkgflfdghsp85rfghnfqsx1y97kjk2gk3dazig5ljwdlw")))

(define-public crate-ubend-0.1.2 (c (n "ubend") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0j63gyvzs1hcvqhqrpv78phd0pq4rbdd30a04i51l0b71mnh6n7v")))

(define-public crate-ubend-0.2.0 (c (n "ubend") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1h8zn748jv1fwgll2sxwm3m1jghbb2hkzkx6117bkiq99f8k5vkq")))

(define-public crate-ubend-0.2.1 (c (n "ubend") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "09jwlnv1irffskw9d2wa7bfmsv700pz4wckwb58l6jcg7c1i2gs5")))

(define-public crate-ubend-0.2.2 (c (n "ubend") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1as6hjb5n1ygarn4xw55yxg8vylbg2czkyfy7h96p192dqp6sfc2")))

