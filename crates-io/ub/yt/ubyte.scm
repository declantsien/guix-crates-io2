(define-module (crates-io ub yt ubyte) #:use-module (crates-io))

(define-public crate-ubyte-0.9.0 (c (n "ubyte") (v "0.9.0") (h "1kfpszjdadbrwwkw43kdl0wdgqbbjixd4rq9n8qf97gvb131pag3") (y #t)))

(define-public crate-ubyte-0.9.1 (c (n "ubyte") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "030v4z9sfnrsdg1g2m0dj5vmzpjjh44yr7mf4sbg6w0qzrpp2z06") (f (quote (("default")))) (y #t)))

(define-public crate-ubyte-0.10.0 (c (n "ubyte") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0yc4iazjzjkmwzg80bjf4zw3g05n5xph27n05q7234afqzhyqxl5") (f (quote (("default")))) (y #t)))

(define-public crate-ubyte-0.10.1 (c (n "ubyte") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1zp8x55w57dkcy20vnc50izsjcgz7mj9b0d9z3i5v188wywnnxa2") (f (quote (("default"))))))

(define-public crate-ubyte-0.10.2 (c (n "ubyte") (v "0.10.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "152fs2qpcy2xd16av6wj91z1qfznlpyxfjp1g6xjj6ilcgr2k3m5") (f (quote (("default"))))))

(define-public crate-ubyte-0.10.3 (c (n "ubyte") (v "0.10.3") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1rlg6sr14i3rd4kfhrwd7b7w7krlg6kpjxkd6vcx0si8gnp0s7y8") (f (quote (("default"))))))

(define-public crate-ubyte-0.10.4 (c (n "ubyte") (v "0.10.4") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1spj3k9sx6xvfn7am9vm1b463hsr79nyvj8asi2grqhyrvvdw87p") (f (quote (("default"))))))

