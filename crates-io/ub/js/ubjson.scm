(define-module (crates-io ub js ubjson) #:use-module (crates-io))

(define-public crate-ubjson-0.1.0 (c (n "ubjson") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r9ww9fdi65n8n4px7lhwsjlsk09akr2qbf3pf0q7dk4f9631a7d") (f (quote (("impl-serde" "serde" "serde_json") ("default"))))))

