(define-module (crates-io ub un ubuntu-distro-info) #:use-module (crates-io))

(define-public crate-ubuntu-distro-info-0.2.0 (c (n "ubuntu-distro-info") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "distro-info") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0c3s8f5r7jryhicaqkpm63d1p917pw2zb782yc5m5nhhx3l9gi7p")))

(define-public crate-ubuntu-distro-info-0.3.0 (c (n "ubuntu-distro-info") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "distro-info") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0vg76s5myfimpz6m0lh7pzkahsj17146n5d92mfbf676vfp7pqqa")))

