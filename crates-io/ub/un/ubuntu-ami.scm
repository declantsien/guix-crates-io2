(define-module (crates-io ub un ubuntu-ami) #:use-module (crates-io))

(define-public crate-ubuntu-ami-0.1.0 (c (n "ubuntu-ami") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0d1jphhmjicq65cz14bk81zsrndvga464i6apd5hapvsvikscvba")))

(define-public crate-ubuntu-ami-0.1.1 (c (n "ubuntu-ami") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1xj1xh5m6drwx6k2vljgxw9z7h3g882sf9ig20iys3635kr2yc6y")))

(define-public crate-ubuntu-ami-0.1.2 (c (n "ubuntu-ami") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0il4k3l8srm7gggmqjajkr4gz8p39rjjsv127fp674js5dqj6m9q")))

(define-public crate-ubuntu-ami-0.2.0 (c (n "ubuntu-ami") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11aic3rn5fn0bbm2lhkqxpk2c1x80py8npdn2782g7v8k0lzqsnr")))

