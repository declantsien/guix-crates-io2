(define-module (crates-io ub un ubuntu-version) #:use-module (crates-io))

(define-public crate-ubuntu-version-0.1.0 (c (n "ubuntu-version") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)))) (h "0k6yr9if9h358xv4wvmyr0qgn8fb8pm395ybx1d5a7v6qls5j6p8")))

(define-public crate-ubuntu-version-0.1.1 (c (n "ubuntu-version") (v "0.1.1") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)))) (h "0z2mwk09gkx1ddgynld89ayi2vss8ia2n6c5cbyq6gk3jgn6i7sh")))

(define-public crate-ubuntu-version-0.1.2 (c (n "ubuntu-version") (v "0.1.2") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)))) (h "0c1n462mga22zrr8b2p1gqijyx1sqx1fhc32dby4sjpqmrwj8zwh")))

(define-public crate-ubuntu-version-0.1.3 (c (n "ubuntu-version") (v "0.1.3") (d (list (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)))) (h "1kyqcq9ajh2r1mbjv1qg0fx1518gwfhi956j3n24f1kn1928r8sh")))

(define-public crate-ubuntu-version-0.1.4 (c (n "ubuntu-version") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "os-release") (r "^0.1.0") (d #t) (k 0)))) (h "0igx29xgjg3yk9w8gwkfrbik3v46qv4a2gf7ba0jii6w2kqx5jf4")))

(define-public crate-ubuntu-version-0.1.5 (c (n "ubuntu-version") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vyiisaj1i55szkklsvz47mb4f72y43jv8fnpkr9xwm4x5h9fvd0")))

(define-public crate-ubuntu-version-0.2.0 (c (n "ubuntu-version") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02m232803g2rxwxhqv5jdkjpbbccq5zi0822dq4ij9ny306qp514")))

(define-public crate-ubuntu-version-0.2.1 (c (n "ubuntu-version") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15ym15q6i682cbx58vb3r9km17rg1arxin061miadpn1fs97kj4m")))

(define-public crate-ubuntu-version-0.2.2 (c (n "ubuntu-version") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07ia2xrxvyim6hfp1pz0xgdg0hg1dhycagrv5iwva6xkbbb4fiah")))

(define-public crate-ubuntu-version-0.2.3 (c (n "ubuntu-version") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y41nnd489rd3qmdy8fy1gl1x7p702hc6yyripp0xla4bvdpb0fm")))

(define-public crate-ubuntu-version-0.2.4 (c (n "ubuntu-version") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z8lcwylhkh4f5qyph2ymhgkbcv66874vhwpmifknxry1wli39dc")))

(define-public crate-ubuntu-version-0.2.5 (c (n "ubuntu-version") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "os-release") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h5qd1v5g8z32bp8ixcczdv8mk16szi2wcvj1j0bl8hjx0klsxpy")))

