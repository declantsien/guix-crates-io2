(define-module (crates-io ub #{64}# ub64) #:use-module (crates-io))

(define-public crate-ub64-0.0.4 (c (n "ub64") (v "0.0.4") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "intbin") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "1wr2rja23daca35fz93g0nz3i7yrbgi074wfsm4r5f8w9bdmh54w") (f (quote (("default" "intbin")))) (s 2) (e (quote (("intbin" "dep:intbin"))))))

(define-public crate-ub64-0.0.5 (c (n "ub64") (v "0.0.5") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "intbin") (r "^0.0.5") (o #t) (d #t) (k 0)))) (h "198zprr8svab983gdm7sf9dq8p98r7j4nnq4mcldc7dsm27ria22") (f (quote (("default" "intbin")))) (s 2) (e (quote (("intbin" "dep:intbin"))))))

(define-public crate-ub64-0.0.10 (c (n "ub64") (v "0.0.10") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "intbin") (r "^0.0.5") (o #t) (d #t) (k 0)) (d (n "vbyte") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0jil6kgxc694fq9mk6b52y4m53kvmi3gji0gzidyp6g0hzki4wkm") (f (quote (("default")))) (s 2) (e (quote (("u64li" "dep:vbyte") ("u64" "dep:intbin"))))))

