(define-module (crates-io ub #{64}# ub64m) #:use-module (crates-io))

(define-public crate-ub64m-0.1.99 (c (n "ub64m") (v "0.1.99") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "12g2dwk7388iy7i2hfhbbiv2b3rvvcpg85raym0yk36fvb56j6g7")))

(define-public crate-ub64m-1.0.0 (c (n "ub64m") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1pwljwacnwyk9y7mq4j763n0m9dpb6mvhrm3jjpms8d98ghhh9xj")))

(define-public crate-ub64m-1.1.0 (c (n "ub64m") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "00x4lyiqmkv1rpg1048q0vcyjyg3gm4cccg8m37p18wpryhfqba1")))

