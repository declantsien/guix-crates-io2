(define-module (crates-io ub us ubus) #:use-module (crates-io))

(define-public crate-ubus-0.1.0 (c (n "ubus") (v "0.1.0") (d (list (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)))) (h "0w4k4hwhpgbgq5wzbxvxph30s8cpjamrw3wyd3pgh4nxnlc10a06") (f (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1.1 (c (n "ubus") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive" "std"))) (k 0)) (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)))) (h "1pl4xld9jkpfr93q63jd3x7irygqpbfmkah3bp21qnwipmbz02cw") (f (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1.2 (c (n "ubus") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "046bdnrna4c8ljmksa2rcjh5mcbnp292j4qn3dy3y6fsj64amj3f") (f (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1.3 (c (n "ubus") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0yyzyaciw4xcw5zsbhgxxxqak6z18s1wd47a16mp355hb0kn6y6n") (f (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1.4 (c (n "ubus") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0p2jj30c40zhlqib9712ri24pf5hdyrnsmrdcy3gc2y907s4bkzk") (f (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1.5 (c (n "ubus") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "storage_endian") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0aigi5fnfzrwcm57ai2976j161mwjx61h8wr1rlaqzgrq3v7jalv") (f (quote (("no_std") ("default"))))))

