(define-module (crates-io ub ig ubigeo-peru) #:use-module (crates-io))

(define-public crate-ubigeo-peru-0.1.0 (c (n "ubigeo-peru") (v "0.1.0") (h "0fgvdzgq9ji2qlxjfxq6rawav644p603l1fcha3i8db7z6k17q5n")))

(define-public crate-ubigeo-peru-0.1.1 (c (n "ubigeo-peru") (v "0.1.1") (h "1hwvgfazsjg4dnwvc4p5p0hfbg8k4bhfxhgsv91xc2ns60z6zyzb")))

