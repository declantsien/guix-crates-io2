(define-module (crates-io ub pf ubpf) #:use-module (crates-io))

(define-public crate-ubpf-0.1.0 (c (n "ubpf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "likely") (r "^0.1") (d #t) (k 0)) (d (n "ubpf-sys") (r "^0.1") (d #t) (k 0)))) (h "0daax0l0q39sbk1dsx028p7s0g4xzbkwmmvk2dky6sj2cx5mc6ij")))

