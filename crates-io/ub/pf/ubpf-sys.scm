(define-module (crates-io ub pf ubpf-sys) #:use-module (crates-io))

(define-public crate-ubpf-sys-0.1.0 (c (n "ubpf-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17anb9iaphgnc38y3anfdra96v6b6p0jxs1qbxp5cldax4p8n8xd")))

(define-public crate-ubpf-sys-0.1.1 (c (n "ubpf-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0arss3kqmhwapyw1dr8xk4gph2m5gszfkbys1qsmrxjky5da0gdd") (l "ubpf")))

