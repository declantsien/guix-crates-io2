(define-module (crates-io m6 #{1-}# m61-modulus) #:use-module (crates-io))

(define-public crate-m61-modulus-0.1.0 (c (n "m61-modulus") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "17c6l07661paz6dl0xhcbiz90746jcjdgrkzf9vva34ynkc1bhqj") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-m61-modulus-0.1.1 (c (n "m61-modulus") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0k54wv9v9a7j3xy66gscmfz4xqq4q1agv0sbffcfclg8sqd6pqvz") (f (quote (("std") ("nightly") ("default" "std"))))))

