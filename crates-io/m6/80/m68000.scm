(define-module (crates-io m6 #{80}# m68000) #:use-module (crates-io))

(define-public crate-m68000-0.1.0 (c (n "m68000") (v "0.1.0") (h "1892qq7585ic6l6r73jc2v7a6lybj79c9gbi4vzp1avf1jx55gdq") (f (quote (("cpu-scc68070") ("cpu-mc68000"))))))

(define-public crate-m68000-0.1.1 (c (n "m68000") (v "0.1.1") (h "1wl09kw4chif3qxrqrl6cb24ab6jw8iwj1ir2akz8v159cw7qlcm") (f (quote (("cpu-scc68070") ("cpu-mc68000"))))))

(define-public crate-m68000-0.2.0 (c (n "m68000") (v "0.2.0") (h "1zpryzckymplkxrwlyg43np1abmln30h15c13ck77qx9x1cq58md") (f (quote (("ffi") ("default"))))))

(define-public crate-m68000-0.2.1 (c (n "m68000") (v "0.2.1") (h "1v8yqz1aflzcgb3vsy0pphc278vn37rr140i4l52via4j1s0fy7s") (f (quote (("ffi") ("default"))))))

