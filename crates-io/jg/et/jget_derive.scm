(define-module (crates-io jg et jget_derive) #:use-module (crates-io))

(define-public crate-jget_derive-0.1.0 (c (n "jget_derive") (v "0.1.0") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "1n6li9fybqj07jskzfk8kza4jlpri59gbb2vmczxpzhf7nrps525")))

(define-public crate-jget_derive-0.2.0 (c (n "jget_derive") (v "0.2.0") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "16r503nsnxll7445g1hscki9xrscbz4v9lz5bj3y5p92bdhsjzr0")))

(define-public crate-jget_derive-0.3.0 (c (n "jget_derive") (v "0.3.0") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "1rdy47w9hlv4g55s58i2x9l2717cb8bbmf8mg36hxdn3f0x30bc5")))

(define-public crate-jget_derive-0.4.0 (c (n "jget_derive") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "03ki7prriym2cwb5va8vw8clycj2mah8k55qrlzbixp31j2hc0ny")))

(define-public crate-jget_derive-0.4.1 (c (n "jget_derive") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "0j7ikxwjh7mc32wrrvlvxflm3c29r7k2ws62p8q9n0vipq7sc5yz")))

