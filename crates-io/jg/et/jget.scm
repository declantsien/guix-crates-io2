(define-module (crates-io jg et jget) #:use-module (crates-io))

(define-public crate-jget-0.1.0 (c (n "jget") (v "0.1.0") (d (list (d (n "jget_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jget_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nhr550yajgrb5dpnk1563g3a8756hdhs3zqcbs73fb0znjh24w5") (f (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.2.0 (c (n "jget") (v "0.2.0") (d (list (d (n "jget_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jget_derive") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z8ikbwgxj9pzbbh8cvdgy0l3ygq860rmgh45palsy377dwwhlsc") (f (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.3.0 (c (n "jget") (v "0.3.0") (d (list (d (n "jget_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "jget_derive") (r "^0.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gflhhajfzhn0jqklzpjs0lksb9cd5wi7zpmwbwnpc11hx0n6fxr") (f (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.4.0 (c (n "jget") (v "0.4.0") (d (list (d (n "jget_derive") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "jget_derive") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vc3vic0fxhn4vkrqsbz1hbb0i20zqn68qzddn23sbkih1kj5d8z") (f (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.4.1 (c (n "jget") (v "0.4.1") (d (list (d (n "jget_derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "jget_derive") (r "^0.4.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wsmscjzy7gqfsjw5xc1w0f1xprzw87byldk45l0lkyxs7ay69p1") (f (quote (("derive" "jget_derive"))))))

