(define-module (crates-io hh sh hhsh) #:use-module (crates-io))

(define-public crate-hhsh-0.1.0 (c (n "hhsh") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)))) (h "0y6wrh6b2hxpav8rvxgiyjka8qqc1lclsa6y2427wb9y18k4sx6i")))

