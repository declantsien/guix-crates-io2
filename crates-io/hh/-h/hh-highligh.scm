(define-module (crates-io hh -h hh-highligh) #:use-module (crates-io))

(define-public crate-hh-highligh-0.1.0 (c (n "hh-highligh") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0armsfq7z0z26gq4bykr28ylp1lzccj0v9by50wg8kydf8ff65sm")))

