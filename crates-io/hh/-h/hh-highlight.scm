(define-module (crates-io hh -h hh-highlight) #:use-module (crates-io))

(define-public crate-hh-highlight-0.1.0 (c (n "hh-highlight") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0mkrb2sfpmj4km8ng7xqzn9375vvk2v5ai2r9i87mnfg2q17sj56")))

