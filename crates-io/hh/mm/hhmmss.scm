(define-module (crates-io hh mm hhmmss) #:use-module (crates-io))

(define-public crate-hhmmss-0.1.0 (c (n "hhmmss") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0qk0d9l3gv8vhlf1ml9ixa8pjxhr0ij0hqd613qixc3cj78ag8qi")))

