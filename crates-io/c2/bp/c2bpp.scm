(define-module (crates-io c2 bp c2bpp) #:use-module (crates-io))

(define-public crate-c2bpp-1.0.0 (c (n "c2bpp") (v "1.0.0") (d (list (d (n "image") (r "^0.22") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "18fqvvx2wbnvs3rp7ymwqnyk2y0mlqm3lvvl96m217bwfw2xdsn6") (y #t)))

(define-public crate-c2bpp-1.0.1 (c (n "c2bpp") (v "1.0.1") (d (list (d (n "image") (r "^0.22") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "12w27k7f4yh78jf85din277sxlpy5r80jdwlln9x9afa5f8am8rb") (y #t)))

(define-public crate-c2bpp-1.0.3 (c (n "c2bpp") (v "1.0.3") (d (list (d (n "image") (r "^0.22") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "1ya2vg2r1zkpyblyy8invjzhmxsvwy7ix03mps5p3p811ipqwgim")))

