(define-module (crates-io c2 rs c2rs) #:use-module (crates-io))

(define-public crate-c2rs-0.1.0 (c (n "c2rs") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "165fklhq4qi72mzg2dhs8mc7wsdp8b1zm1qm87s2ggsrxkk9mn2a") (y #t)))

(define-public crate-c2rs-0.1.1 (c (n "c2rs") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "1ldxdixp3l78bad18746kdqxvn45c0hmcwya4q80jjqbd441hfr0")))

(define-public crate-c2rs-0.1.2 (c (n "c2rs") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0gqcpj49fhnwyclvhc1sg7qs6rbdmqcnharam7ba9jrn3nzviqhz")))

