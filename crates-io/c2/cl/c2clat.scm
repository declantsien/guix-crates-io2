(define-module (crates-io c2 cl c2clat) #:use-module (crates-io))

(define-public crate-c2clat-1.0.0 (c (n "c2clat") (v "1.0.0") (d (list (d (n "crossbeam-utils") (r "^0.8.16") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0wsmyfacbwnjz287sagwg42zzg8rcwa0igd767lxzbb0mb7xsk6k")))

