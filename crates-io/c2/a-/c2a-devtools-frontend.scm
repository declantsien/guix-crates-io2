(define-module (crates-io c2 a- c2a-devtools-frontend) #:use-module (crates-io))

(define-public crate-c2a-devtools-frontend-0.7.0-beta.5 (c (n "c2a-devtools-frontend") (v "0.7.0-beta.5") (d (list (d (n "opslang-wasm") (r "^0.7.0-beta.5") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (f (quote ("interpolate-folder-path" "debug-embed"))) (d #t) (k 0)))) (h "0v7947hiagq1yb9xma62br8r8r9q09gd32g8kmg4a5qiy8jf4mvn")))

(define-public crate-c2a-devtools-frontend-0.7.0 (c (n "c2a-devtools-frontend") (v "0.7.0") (d (list (d (n "opslang-wasm") (r "^0.7") (d #t) (k 0)) (d (n "rust-embed") (r "^8.0.0") (f (quote ("interpolate-folder-path" "debug-embed"))) (d #t) (k 0)))) (h "1iliym9dg1dr7rs6z1gyhdm45zd0lm50gv2p1d80pvxrz84s0rc1")))

(define-public crate-c2a-devtools-frontend-1.0.0 (c (n "c2a-devtools-frontend") (v "1.0.0") (d (list (d (n "opslang-wasm") (r "^1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^8.3.0") (f (quote ("interpolate-folder-path" "debug-embed"))) (d #t) (k 0)))) (h "0afwzpbsmc2ilr5ar9ly1y53q7rxhfix2dkm03ri28w0dhva3zj9")))

