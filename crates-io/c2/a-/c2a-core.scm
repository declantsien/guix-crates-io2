(define-module (crates-io c2 a- c2a-core) #:use-module (crates-io))

(define-public crate-c2a-core-3.9.0-beta.3 (c (n "c2a-core") (v "3.9.0-beta.3") (h "0hgsnb31h2b3zim283dvmdbvfiy4dyajk5byphd5yar9xr5wl0cr")))

(define-public crate-c2a-core-3.9.0-beta.4 (c (n "c2a-core") (v "3.9.0-beta.4") (h "0ds9la8jy6fdh5q53xhpf79h0jzh6hpz9p8h35wvwp3pv6b7058i") (f (quote (("std") ("export-src" "std"))))))

(define-public crate-c2a-core-3.9.0-beta.5 (c (n "c2a-core") (v "3.9.0-beta.5") (h "0fwhrfyr9ngchjda2igsji121pvrv672m4xh2wgi35nxy9ha8ili") (f (quote (("std") ("export-src" "std"))))))

(define-public crate-c2a-core-3.9.0-beta.6 (c (n "c2a-core") (v "3.9.0-beta.6") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "1xzrwpby48dn54hrdviwqpv642bvri49wvc2hbmdxfrdlf0vpd5z") (f (quote (("std") ("export-src" "std"))))))

(define-public crate-c2a-core-3.9.0-beta.7 (c (n "c2a-core") (v "3.9.0-beta.7") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "07f37p2iihr2z7h40jiw7h58q8jmvlhwhnknmj2pxa73j4spzad1") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.9.0-beta.8 (c (n "c2a-core") (v "3.9.0-beta.8") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "1qfyjz4wxskc1h4gq8xnrcl4zxvqh7g814ncy0fcq55v79zph4sf") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.9.0-beta.9 (c (n "c2a-core") (v "3.9.0-beta.9") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "05rl5f660pzfhgsvkm8aalw8ks47lzswn2kamplnx11q5p88l27x") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.9.0 (c (n "c2a-core") (v "3.9.0") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "0midmnz3j392xvvdw3x8j1azyzailkv5gh7jvqa0pswl6kp0nr4r") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.9.1 (c (n "c2a-core") (v "3.9.1") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "0yw12gdxa4j3bn7fpblhixbf3x5mdwnrxwsw5f71pdqxsf82k0sg") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.9.2 (c (n "c2a-core") (v "3.9.2") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "1vjhw3amqpf8rbvaw4wlggwg3ng6kygs839zs0m2ks76j4gbhrc4") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-3.10.0 (c (n "c2a-core") (v "3.10.0") (d (list (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.17") (d #t) (k 1)))) (h "1jipc87v0jrf1ismy9bv3c0m3h6myflsv89r01d7j82h6zf1gc8f") (f (quote (("std") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.0 (c (n "c2a-core") (v "4.0.0-beta.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.18") (d #t) (k 1)))) (h "0mgfpxrazcz2icl6wnh8crvy8xi5wd6n760ghsxxcrbx49k3xnnq") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (y #t) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.1 (c (n "c2a-core") (v "4.0.0-beta.1") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.19") (d #t) (k 1)))) (h "1iw929l5vyhyrkc4m7q611kwjv7rvh7bxjvgml8d0rkzwkk5d5yq") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.2 (c (n "c2a-core") (v "4.0.0-beta.2") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.19") (d #t) (k 1)))) (h "0g82w356nggh9gsx1bilfppbl26zvhlc24zzmzx76cw5s4064m6b") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.3 (c (n "c2a-core") (v "4.0.0-beta.3") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "104p8f1amsbl069jlv4s939jpgajfn5gnc3811bnn2iyr53lmc03") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.4 (c (n "c2a-core") (v "4.0.0-beta.4") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "0wixy583gg66pgjgw5rwf4jchyjy4ql4spgs7y1zwn6n2qfr035g") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0-beta.5 (c (n "c2a-core") (v "4.0.0-beta.5") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "0ywrgp5km4xpn8398057619l1l31mcgpvz4820wf593qkq35r0rh") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.0 (c (n "c2a-core") (v "4.0.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "087z8973if8apzf6al06hk5s1w7rdya7d29803p69d17cb2mx7pk") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.0.1 (c (n "c2a-core") (v "4.0.1") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "1j03viw0bhxjggznajn4933zmzr15i7k1jmp0ax13imw51afnjsk") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.1.0 (c (n "c2a-core") (v "4.1.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "0clx77y00070xynkxifn9xfiza19az6jdmkdck5qgyv1cri3znwq") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime"))))))

(define-public crate-c2a-core-4.0.2 (c (n "c2a-core") (v "4.0.2") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.20") (d #t) (k 1)))) (h "0mvl4rh3p2ry770yphpwsi1rf6hmym1c3dzm5p11914nkc8q17dc") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime")))) (l "c2a-core")))

(define-public crate-c2a-core-4.2.0 (c (n "c2a-core") (v "4.2.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.21") (d #t) (k 1)))) (h "04v69iigazjrw0d1f7kvn9ngx2319x8c5w61lclhp2ac12hvidhd") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime"))))))

(define-public crate-c2a-core-4.3.0 (c (n "c2a-core") (v "4.3.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.21") (d #t) (k 1)))) (h "1kp5131hnrmmglchqb6hfjsg7wlxnadqidvak1pfs0kjy1wly6v2") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime"))))))

(define-public crate-c2a-core-4.4.0 (c (n "c2a-core") (v "4.4.0") (d (list (d (n "c2a-bind-utils") (r "^4.0.0-beta.0") (d #t) (k 1)) (d (n "clang") (r "^2.0.0") (d #t) (k 1)) (d (n "semver") (r "^1.0.22") (d #t) (k 1)))) (h "0av8sv5yffvkpgq7xaii3fcibq6lyw4gizdz4xlfx0wdhaggx12r") (f (quote (("std") ("no-c2a-link") ("export-src" "std") ("default" "clang-runtime") ("clang-runtime" "clang/runtime"))))))

