(define-module (crates-io c2 a- c2a-bind-utils) #:use-module (crates-io))

(define-public crate-c2a-bind-utils-4.0.0-beta.0 (c (n "c2a-bind-utils") (v "4.0.0-beta.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "doxygen-rs") (r "^0.4.2") (d #t) (k 0)))) (h "051jyp91kf6rpsg8qhx99spyifq9fv40j5gspshkkihvcpbgpzxz")))

