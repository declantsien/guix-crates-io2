(define-module (crates-io c2 ru c2rust-macros) #:use-module (crates-io))

(define-public crate-c2rust-macros-0.10.0 (c (n "c2rust-macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1r1viwmky1yp6z5sh57dxbcnx7jkrdzxwnwcgq1h0svcrsh1frkz")))

(define-public crate-c2rust-macros-0.11.0 (c (n "c2rust-macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0jmrwkpcar0g8ivspx2z66f8cw5f356xxl2r2i1cf59mzswy30d2")))

(define-public crate-c2rust-macros-0.14.0 (c (n "c2rust-macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0r7xwr5dg4bjplkp1dwy6azvd814y9rr7bdk09s00ri699m9da2n")))

(define-public crate-c2rust-macros-0.15.0 (c (n "c2rust-macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0hznb6c10hq9jnd2l14jkyyvsj138m7ip15zlv0jl6vdaglig6vc")))

