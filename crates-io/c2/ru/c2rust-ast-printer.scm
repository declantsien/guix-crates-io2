(define-module (crates-io c2 ru c2rust-ast-printer) #:use-module (crates-io))

(define-public crate-c2rust-ast-printer-0.13.0 (c (n "c2rust-ast-printer") (v "0.13.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04pli26d48bnpgjx5z75avx04v6f4aflawkanp7rfspcf4zvnarg")))

(define-public crate-c2rust-ast-printer-0.14.0 (c (n "c2rust-ast-printer") (v "0.14.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pvpxjfghqwvzpxn4962w3fp181b73d6j841n8r150qpnqnjmdy0")))

(define-public crate-c2rust-ast-printer-0.15.0 (c (n "c2rust-ast-printer") (v "0.15.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r8m80m48s30zaz2bcpqgb38w2j24cs6kmxm8b11dn6mq7v9i7dm")))

(define-public crate-c2rust-ast-printer-0.16.0 (c (n "c2rust-ast-printer") (v "0.16.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "proc-macro" "printing"))) (d #t) (k 0)))) (h "1sn8sd4q1cmxld517grxvmq56sc1d4z8hl6x5h741qrkg1wn6aa0")))

(define-public crate-c2rust-ast-printer-0.17.0 (c (n "c2rust-ast-printer") (v "0.17.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "proc-macro" "printing" "clone-impls"))) (d #t) (k 0)))) (h "0iwg4rw0i92yafwqj75cqg4aswck8zc6pniahh2v594s90d9ya42")))

(define-public crate-c2rust-ast-printer-0.18.0 (c (n "c2rust-ast-printer") (v "0.18.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "proc-macro" "printing" "clone-impls"))) (d #t) (k 0)))) (h "1a02bnnxn1difq917c2rv8b7654ni65lyk37hdyklv9n96inr07r")))

