(define-module (crates-io c2 ru c2rust-build-paths) #:use-module (crates-io))

(define-public crate-c2rust-build-paths-0.17.0 (c (n "c2rust-build-paths") (v "0.17.0") (d (list (d (n "print_bytes") (r "^1.1") (d #t) (k 0)))) (h "1vdsf4my435zdnp2v0rlq85n26jknyv7vk4z7p02k727knm8acbx")))

(define-public crate-c2rust-build-paths-0.18.0 (c (n "c2rust-build-paths") (v "0.18.0") (d (list (d (n "print_bytes") (r "^1.1") (d #t) (k 0)))) (h "0b2liaxbqksgfbsmr6hacdia6czlq7m0pyqx3l2rrcfcnb2ksgv0")))

