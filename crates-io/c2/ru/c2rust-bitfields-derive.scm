(define-module (crates-io c2 ru c2rust-bitfields-derive) #:use-module (crates-io))

(define-public crate-c2rust-bitfields-derive-0.1.0 (c (n "c2rust-bitfields-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0kvhvbhf89bg9i8ml4i2wv47mpwv4sdqsv8rilw6954dbi3qw2lb")))

(define-public crate-c2rust-bitfields-derive-0.2.0 (c (n "c2rust-bitfields-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfk7qq0v59jzc6w4z9l4r29aih4lgrskqdgb5xnmp019jg5adg4")))

(define-public crate-c2rust-bitfields-derive-0.2.1 (c (n "c2rust-bitfields-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hbjadr8p30lkwsjlgfxbd4ava1wasasirchv1sbi2l2gcd61l9x")))

(define-public crate-c2rust-bitfields-derive-0.17.0 (c (n "c2rust-bitfields-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "138ax1gm1jnzy7hzlmgzdy7jwv10mddybbxa844hyi80v0bwc5m5")))

(define-public crate-c2rust-bitfields-derive-0.18.0 (c (n "c2rust-bitfields-derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i95j6q1d61h1m1pk84i3ih00hsmbn8ib35xr129fz2rw81c3jyk")))

