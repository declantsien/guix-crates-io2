(define-module (crates-io c2 ru c2rust-ast-builder) #:use-module (crates-io))

(define-public crate-c2rust-ast-builder-0.10.0 (c (n "c2rust-ast-builder") (v "0.10.0") (h "12jmcrhxj11fhdgrvz2182r97a9pkqv5sj4m8rv27bjabip41rys")))

(define-public crate-c2rust-ast-builder-0.11.0 (c (n "c2rust-ast-builder") (v "0.11.0") (h "0blspvsrq02r90amlc79r4spf7zifm8pwz2syvfslyqrvfsjwi09")))

(define-public crate-c2rust-ast-builder-0.13.0 (c (n "c2rust-ast-builder") (v "0.13.0") (h "1hyhn5d4z0dvgr03wbpzvrr9ls82n8s9539wk6sh9hsi8bkckc55")))

(define-public crate-c2rust-ast-builder-0.14.0 (c (n "c2rust-ast-builder") (v "0.14.0") (h "0k7r77wdlrjajkzqk7sx7v1yyfrqwqac8lawiaa7n6sdvwm1i07n")))

(define-public crate-c2rust-ast-builder-0.15.0 (c (n "c2rust-ast-builder") (v "0.15.0") (h "1r6j3c4kzl32l7ha96skh3gjjwlyfdmy9b07i1z4lp0zk8nklh45")))

(define-public crate-c2rust-ast-builder-0.16.0 (c (n "c2rust-ast-builder") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "011pzc6krr16qflhxfy99i95b3gsg8az315vap2x1z1bgj7758la")))

(define-public crate-c2rust-ast-builder-0.17.0 (c (n "c2rust-ast-builder") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing" "clone-impls"))) (d #t) (k 0)))) (h "1zzzz1nsjfkldw3i2yb4qq3bg5yv0w8295w8ccjnvj16haidmyhf")))

(define-public crate-c2rust-ast-builder-0.18.0 (c (n "c2rust-ast-builder") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing" "clone-impls"))) (d #t) (k 0)))) (h "0w63rp66g6axkymxd16avxp3gjnphy3mg9938gsh52p4aak83nq5")))

