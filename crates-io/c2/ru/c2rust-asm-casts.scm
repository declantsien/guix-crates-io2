(define-module (crates-io c2 ru c2rust-asm-casts) #:use-module (crates-io))

(define-public crate-c2rust-asm-casts-0.1.0 (c (n "c2rust-asm-casts") (v "0.1.0") (h "10d849z72jh43jrcinsnvm5ff3126z76i91l8y3z6r7q3wdi3m3k")))

(define-public crate-c2rust-asm-casts-0.1.1 (c (n "c2rust-asm-casts") (v "0.1.1") (h "1n7sycnp1njbwksgsii6ddmfml2gl4j68pc8d2k7iq5q53p1pnk1")))

(define-public crate-c2rust-asm-casts-0.2.0 (c (n "c2rust-asm-casts") (v "0.2.0") (h "0y51pv63dqzpl322qn1qnqkx3444yn7x0prbdb1a27ifbd4km24r")))

(define-public crate-c2rust-asm-casts-0.17.0 (c (n "c2rust-asm-casts") (v "0.17.0") (h "0192vrqhqjspnip3h2n730ckp6xbgg4mdzig3v793vn4pck5cxnk")))

(define-public crate-c2rust-asm-casts-0.18.0 (c (n "c2rust-asm-casts") (v "0.18.0") (h "11ah2ws5kwqsqqxr512yfs9fq2k551idvvra9v8rlv9q6xz2jphr")))

