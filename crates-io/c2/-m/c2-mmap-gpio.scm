(define-module (crates-io c2 -m c2-mmap-gpio) #:use-module (crates-io))

(define-public crate-c2-mmap-gpio-0.1.0 (c (n "c2-mmap-gpio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w68r191c7nksbj8bvsdsrgxr80v5n7bk2m7b1cfdrq3ms3d4286")))

(define-public crate-c2-mmap-gpio-0.1.1 (c (n "c2-mmap-gpio") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jp52k1wsm5igxbsim5dwcja31p9924hbd2d6c3834zwqwn82ar1")))

(define-public crate-c2-mmap-gpio-0.1.2 (c (n "c2-mmap-gpio") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i1l9hfrjhi941h4krmfbj47icar62nxkm45gbp13pbykhszvpsq")))

(define-public crate-c2-mmap-gpio-0.2.0 (c (n "c2-mmap-gpio") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iq3pk9zjm83a0qpzvywgk1kbf4vygxdmvaaflkzfkdpx1hvng72")))

(define-public crate-c2-mmap-gpio-0.2.1 (c (n "c2-mmap-gpio") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dmxd0rsj8flp00sm50v533r0lpammcvqjx8s2020a80ima5x2qi")))

