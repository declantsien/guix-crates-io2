(define-module (crates-io c2 #{55}# c255b3) #:use-module (crates-io))

(define-public crate-c255b3-0.0.1 (c (n "c255b3") (v "0.0.1") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "0f9dr5y20pqy1k29959mxi9ay94zycpmqbwwgx27gza7prfh0c9h")))

(define-public crate-c255b3-0.0.2 (c (n "c255b3") (v "0.0.2") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "15adm8qg54k66nffnpmhykh7yi2xqhnc5pw14gjvnwzjr17ll1qv")))

(define-public crate-c255b3-0.0.3 (c (n "c255b3") (v "0.0.3") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-rc.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1d9g7bai98cwrnwl081xs3dazq7g5l39apnigrdw94qpgfh16rqw")))

