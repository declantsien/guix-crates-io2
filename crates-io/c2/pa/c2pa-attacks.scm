(define-module (crates-io c2 pa c2pa-attacks) #:use-module (crates-io))

(define-public crate-c2pa-attacks-0.0.1 (c (n "c2pa-attacks") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "c2pa") (r "^0.23.1") (f (quote ("fetch_remote_manifests" "file_io" "xmp_write"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "0x8qn7rm6vh470g4m953b9lzv7rwkn0r3yc1jzqkvjx49gm8njyy") (r "1.68.0")))

