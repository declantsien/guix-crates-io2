(define-module (crates-io c2 -s c2-sys) #:use-module (crates-io))

(define-public crate-c2-sys-0.1.0 (c (n "c2-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xyshd59jljljckbdrpib8lka7h04vm4d5hixpj9cwl117npa9ms")))

(define-public crate-c2-sys-0.2.0 (c (n "c2-sys") (v "0.2.0") (h "037hi10aq98c0jhbs766sv4hqr34zffdqyihafi1y5a3qzmrcm0d")))

