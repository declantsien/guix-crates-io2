(define-module (crates-io p9 _w p9_wire_format_derive) #:use-module (crates-io))

(define-public crate-p9_wire_format_derive-0.2.0 (c (n "p9_wire_format_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07pakzbzjh9y2rc7fp4276y368jpmbp3jscbylvmhcjn7k1vvmm4") (y #t)))

(define-public crate-p9_wire_format_derive-0.2.1 (c (n "p9_wire_format_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0lar43c1nd4mmiz1vka57vdwhm6cv5igsll01x7if773qhp5lmvv") (y #t)))

(define-public crate-p9_wire_format_derive-0.2.2 (c (n "p9_wire_format_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18qgkl999biapl3px847iayhyj3z2mm9la76149fsgmg7qd26q8s") (y #t)))

(define-public crate-p9_wire_format_derive-0.2.3 (c (n "p9_wire_format_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kiiymwsk31qcn5y9sliybrpf67grixykj5m73gwp6zcv0854276")))

