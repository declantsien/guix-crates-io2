(define-module (crates-io aj l_ ajl_logger) #:use-module (crates-io))

(define-public crate-ajl_logger-0.1.1 (c (n "ajl_logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "1a0lm39c0xgxnd91kqdi65gr7j4qr8qspnjgcj84zlmy09mpcbp8")))

(define-public crate-ajl_logger-0.1.2 (c (n "ajl_logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)))) (h "07361kap15i7kn8kzwjbj59684nsc6c5n22j58ak47hngi6v0jax")))

