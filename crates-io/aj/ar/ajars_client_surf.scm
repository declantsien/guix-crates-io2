(define-module (crates-io aj ar ajars_client_surf) #:use-module (crates-io))

(define-public crate-ajars_client_surf-0.10.0 (c (n "ajars_client_surf") (v "0.10.0") (d (list (d (n "ajars_core") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (d #t) (k 0)))) (h "1flzmxq6iv8r8dp64pblrz1mzqnh600j6f3p179g4278w3fyw9g1")))

(define-public crate-ajars_client_surf-0.11.0 (c (n "ajars_client_surf") (v "0.11.0") (d (list (d (n "ajars_core") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (d #t) (k 0)))) (h "0jgjw4x586s7w1dk1v0cfnrvnvs5vxxzqgjndh1vklyzbdc9whv1")))

