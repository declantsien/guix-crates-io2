(define-module (crates-io aj ar ajars_core) #:use-module (crates-io))

(define-public crate-ajars_core-0.2.0 (c (n "ajars_core") (v "0.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0c0pd6bq8vinj1a9069fislk700q0z17mkcqhn416xphpmd33z75")))

(define-public crate-ajars_core-0.3.0 (c (n "ajars_core") (v "0.3.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "0fn6f754sy8ai4138yf9z2x4v9iiqc1bb40a3r4lgz95dgwni58z")))

(define-public crate-ajars_core-0.4.0 (c (n "ajars_core") (v "0.4.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "1z14lclgy4j42ibn6hi906xj9an9zqmhrnsjjbnq467x6bfsg6m4")))

(define-public crate-ajars_core-0.5.0 (c (n "ajars_core") (v "0.5.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "01nwsi80a4va1yv47yfqiva44qh5siw0671prkv64hfza0nyswwx")))

(define-public crate-ajars_core-0.6.0 (c (n "ajars_core") (v "0.6.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "10nqhvrj7m4kvywrbjng6nz6awfb8vfhphk9siwvvi8j7nv7lha1")))

(define-public crate-ajars_core-0.6.1 (c (n "ajars_core") (v "0.6.1") (d (list (d (n "serde") (r "^1") (k 0)))) (h "0ls9yz8pbh8bvl4jc2pyj864gaxqsda07fb3dq3yfycnsvvyc9kv")))

(define-public crate-ajars_core-0.6.3 (c (n "ajars_core") (v "0.6.3") (d (list (d (n "serde") (r "^1") (k 0)))) (h "07lh84hgxj96q656ldmb9ipr05ca1jws2y4lkc4nchghchr6j7rp")))

(define-public crate-ajars_core-0.7.0 (c (n "ajars_core") (v "0.7.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "1l72jh1ccqj4m4hy52sswj3bf2p3sckq64scis7kzq84hi5911cx")))

(define-public crate-ajars_core-0.8.0 (c (n "ajars_core") (v "0.8.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "1h0anyr3kpzc84rp4kg5lzqylnil5lhppcpdlifi078jaa1qv6ng")))

(define-public crate-ajars_core-0.10.0 (c (n "ajars_core") (v "0.10.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "021lcpzwbd3xsc6lc9xpakxail5w2bhp0bkpgl63z602nkr21wrs")))

(define-public crate-ajars_core-0.11.0 (c (n "ajars_core") (v "0.11.0") (d (list (d (n "serde") (r "^1") (k 0)))) (h "13hlzr72gpznm4xcia1qmkkb9b71q04ffv7szaf42yh1gj9cw9v6")))

