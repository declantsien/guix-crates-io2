(define-module (crates-io aj ar ajars_server_actix_web) #:use-module (crates-io))

(define-public crate-ajars_server_actix_web-0.10.0 (c (n "ajars_server_actix_web") (v "0.10.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0") (d #t) (k 0)) (d (n "ajars_core") (r "^0.10.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "01d4iwda61gjy2wcbci6gbygrz6vdrl8iy6ja97rn0zdmg6hl5bx")))

(define-public crate-ajars_server_actix_web-0.11.0 (c (n "ajars_server_actix_web") (v "0.11.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0") (d #t) (k 0)) (d (n "ajars_core") (r "^0.11.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1ncz6kg117jgwvk1h169nfi5llg7f4c5mbcji8qk4qs911xng2xb")))

