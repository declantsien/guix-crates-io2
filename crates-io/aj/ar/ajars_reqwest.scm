(define-module (crates-io aj ar ajars_reqwest) #:use-module (crates-io))

(define-public crate-ajars_reqwest-0.2.0 (c (n "ajars_reqwest") (v "0.2.0") (d (list (d (n "ajars_core") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1z4na6kl2hiy50wi3yf6wyr1nga4ad3yxf3d2z816a311axcfpjg")))

(define-public crate-ajars_reqwest-0.3.0 (c (n "ajars_reqwest") (v "0.3.0") (d (list (d (n "ajars_core") (r "^0.3.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "10gs9v0wizpx0npn5a9q3ss58fm4lwz185v04zdiw46k7y1vpaaz")))

(define-public crate-ajars_reqwest-0.4.0 (c (n "ajars_reqwest") (v "0.4.0") (d (list (d (n "ajars_core") (r "^0.4.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0jx8sygq783fjdr2zfwjicdh4n1ykv45mnim6hy4zd60b17samar")))

(define-public crate-ajars_reqwest-0.5.0 (c (n "ajars_reqwest") (v "0.5.0") (d (list (d (n "ajars_core") (r "^0.5.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0dbdr2afawksypw3akn7nl2q51sbjgf7f6vf4w4w4lcdj2i0h5j1")))

(define-public crate-ajars_reqwest-0.6.0 (c (n "ajars_reqwest") (v "0.6.0") (d (list (d (n "ajars_core") (r "^0.6.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0ia6j9sgh9ri6d3rwz1xjljfi6b95wrgp9bm7m0h8c44fn1kbp37")))

(define-public crate-ajars_reqwest-0.6.1 (c (n "ajars_reqwest") (v "0.6.1") (d (list (d (n "ajars_core") (r "^0.6.1") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1s6777qjpc6avvjl9frihh16spg19sflki3b8g985jg0h445an61")))

(define-public crate-ajars_reqwest-0.6.3 (c (n "ajars_reqwest") (v "0.6.3") (d (list (d (n "ajars_core") (r "^0.6.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1s9hrhf45c4hnx2ap5ljpycyhz9nifpdw2a7jidr87vwi4qpck98")))

(define-public crate-ajars_reqwest-0.7.0 (c (n "ajars_reqwest") (v "0.7.0") (d (list (d (n "ajars_core") (r "^0.7.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0akhca5mv1s3aksykssmrmsfl5q9g029wwrsqjq7xz7avnizqn1b")))

(define-public crate-ajars_reqwest-0.8.0 (c (n "ajars_reqwest") (v "0.8.0") (d (list (d (n "ajars_core") (r "^0.8.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "090xgff3qdls4m0iizrjvcfz22sd0f2wckgl8dlgmn5dg0cfpfln")))

