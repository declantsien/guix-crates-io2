(define-module (crates-io aj ar ajars_actix_web) #:use-module (crates-io))

(define-public crate-ajars_actix_web-0.2.0 (c (n "ajars_actix_web") (v "0.2.0") (d (list (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0y4wi1jhvcbl5ycvss2k32r9dnyk4d888a8rwg1s00yczai5jqbi")))

(define-public crate-ajars_actix_web-0.3.0 (c (n "ajars_actix_web") (v "0.3.0") (d (list (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "0wxc5dhw84zmkvyq21kryn4y7aq29x6r9ivnnmllvjcar9lcahv5")))

(define-public crate-ajars_actix_web-0.4.0 (c (n "ajars_actix_web") (v "0.4.0") (d (list (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1g5s45qwn5rkwg9vpb4wzbix34bwvlipa754dna83q7lb69s41vr")))

(define-public crate-ajars_actix_web-0.5.0 (c (n "ajars_actix_web") (v "0.5.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.5.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1vdf6rr9h998xibsj5xrf21z1v9h8dg3fj4i8zgnr1rv0v5rppby")))

(define-public crate-ajars_actix_web-0.6.0 (c (n "ajars_actix_web") (v "0.6.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.6.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1a11w174nj91hk8qy9g5qh0hhwbkn8qgdngc3palh446vf55wfib")))

(define-public crate-ajars_actix_web-0.6.1 (c (n "ajars_actix_web") (v "0.6.1") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.6.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "088v9yp3h1kz5fh6wvryck359svh0y6p4dshhw22wjdy6vzr4va6")))

(define-public crate-ajars_actix_web-0.6.3 (c (n "ajars_actix_web") (v "0.6.3") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.8") (k 0)) (d (n "ajars_core") (r "^0.6.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1n5mqc7alq10g8cjv7lys27kq7w1gdmis3rjplw5vcs8sfhkvjl3")))

(define-public crate-ajars_actix_web-0.7.0 (c (n "ajars_actix_web") (v "0.7.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0.0-beta.12") (k 0)) (d (n "ajars_core") (r "^0.7.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1c218x3mcq8n85r1fgllp3dbaaxlhm3kfqlb33j76wn1193ijr65")))

(define-public crate-ajars_actix_web-0.8.0 (c (n "ajars_actix_web") (v "0.8.0") (d (list (d (n "actix-rt") (r "^2") (d #t) (k 2)) (d (n "actix-web") (r "^4.0") (k 0)) (d (n "ajars_core") (r "^0.8.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (k 0)))) (h "1na79caxdrajprsnm16x3ab5w9flzwhc7gcrkl9yynilcbhxzd14")))

