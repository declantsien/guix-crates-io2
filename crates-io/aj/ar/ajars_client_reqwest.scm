(define-module (crates-io aj ar ajars_client_reqwest) #:use-module (crates-io))

(define-public crate-ajars_client_reqwest-0.10.0 (c (n "ajars_client_reqwest") (v "0.10.0") (d (list (d (n "ajars_core") (r "^0.10.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1lr4rwlhnipdj28jdp6n8b9zjjh0fxyflgkp50gkiw7slzy1inbz")))

(define-public crate-ajars_client_reqwest-0.11.0 (c (n "ajars_client_reqwest") (v "0.11.0") (d (list (d (n "ajars_core") (r "^0.11.0") (d #t) (k 0)) (d (n "http") (r "^1") (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (k 0)))) (h "1i55ijqad97qlaaajaaa5djrndc0zsfslcc1ydlsfj1fs9ijniv1")))

