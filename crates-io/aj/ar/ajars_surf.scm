(define-module (crates-io aj ar ajars_surf) #:use-module (crates-io))

(define-public crate-ajars_surf-0.3.0 (c (n "ajars_surf") (v "0.3.0") (d (list (d (n "ajars_core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.2") (k 0)))) (h "0kgl8jkmnwnman5v7m0nkv2vpyxab24ka8qhgrixc11ky7qy71h2")))

(define-public crate-ajars_surf-0.4.0 (c (n "ajars_surf") (v "0.4.0") (d (list (d (n "ajars_core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.2") (k 0)))) (h "19iim2w1qav74bcic6hsx6as192i5k2ix22rcz0gnd3d5mlj2hlx")))

(define-public crate-ajars_surf-0.5.0 (c (n "ajars_surf") (v "0.5.0") (d (list (d (n "ajars_core") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.2") (k 0)))) (h "15v660v8y0jjg4phfr8vm0zyhqay3893fxz1bkyl3ixbdpjrap5g")))

(define-public crate-ajars_surf-0.6.0 (c (n "ajars_surf") (v "0.6.0") (d (list (d (n "ajars_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.2") (k 0)))) (h "0bvz4y00gyzwm8fqgcpq03s39shyq5kcgd71rzb6xc6jmjmr4a2r")))

(define-public crate-ajars_surf-0.6.1 (c (n "ajars_surf") (v "0.6.1") (d (list (d (n "ajars_core") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (k 0)))) (h "1ndika0r8cy3whhfgymkmmcls36ak9f6dvif4wizd6cx29gz95bv")))

(define-public crate-ajars_surf-0.6.3 (c (n "ajars_surf") (v "0.6.3") (d (list (d (n "ajars_core") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (k 0)))) (h "02ih72x83s0n6vm2k8ai0z427zyg4wv8df6km61dir1sj7sxr44d")))

(define-public crate-ajars_surf-0.7.0 (c (n "ajars_surf") (v "0.7.0") (d (list (d (n "ajars_core") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (k 0)))) (h "1xn8g5q85lddhvyyjvhnrcv07z0bgz8vpn35izdmchsrygis265r")))

(define-public crate-ajars_surf-0.8.0 (c (n "ajars_surf") (v "0.8.0") (d (list (d (n "ajars_core") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "surf") (r "^2.3") (k 0)))) (h "1nmz295p0vvgkjqgj0xmw8zfkj3apyhmq0wwdn1q32bgfzgq9p1y")))

