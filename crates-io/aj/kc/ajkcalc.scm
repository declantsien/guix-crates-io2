(define-module (crates-io aj kc ajkcalc) #:use-module (crates-io))

(define-public crate-ajkcalc-0.1.0 (c (n "ajkcalc") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (f (quote ("std" "internals"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "10dd837qpf7g8i4z7a02xw8pji18qv26kb6fzyv45xigccb98840")))

(define-public crate-ajkcalc-0.1.1 (c (n "ajkcalc") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (f (quote ("std" "internals"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "110x7y1534vwpa4xgccdgz05illvxvxlha38zngbcsdvmzlhyw9d")))

(define-public crate-ajkcalc-0.1.2 (c (n "ajkcalc") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "functional-closures") (r "^0.2.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (f (quote ("std" "internals"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0y1gk5kxv1cx9lmx5xqci9k1a4jfjg4gvnn14fapqfkdzvzxhfjb")))

(define-public crate-ajkcalc-0.2.0 (c (n "ajkcalc") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "functional-closures") (r "^0.2.0") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)) (d (n "rhai") (r "^1.15.1") (f (quote ("std" "internals"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termplot") (r "^0.1.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1nx8i7d56xik0l080rygk8kganf3h831gbyxrp5gr6r0lw8i4aws")))

