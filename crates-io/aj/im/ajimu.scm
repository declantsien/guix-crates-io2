(define-module (crates-io aj im ajimu) #:use-module (crates-io))

(define-public crate-ajimu-0.1.0 (c (n "ajimu") (v "0.1.0") (d (list (d (n "egg-mode") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.25") (d #t) (k 0)))) (h "1imlvn770s842lv4y0v39slklkxcqhvc0q9359lqza8f6axw71x0")))

