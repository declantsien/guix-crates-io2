(define-module (crates-io aj so ajson) #:use-module (crates-io))

(define-public crate-ajson-0.1.0 (c (n "ajson") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14hzbbjrzk4lcm6hxwpbd0lk2i4p0ysa738x9xn23n0vhnskfs1s")))

(define-public crate-ajson-0.1.1 (c (n "ajson") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "063ljxns9yfyywibp428y8kgh2njhn5vwwrmbc2m01qdbrz2l3jp")))

(define-public crate-ajson-0.2.0 (c (n "ajson") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1p7aq9l03yw4vdg4148ylwmgb9n9p3wza6c2pz7dqfji7vwss9ib")))

(define-public crate-ajson-0.2.1 (c (n "ajson") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kpx8lzlhdz45g9hf04a6p82556wb4mih3mj1wxbdd84vc7fdihr")))

(define-public crate-ajson-0.2.2 (c (n "ajson") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "064wivh8w73b00ziwlwwr4i5k0y7wq92dcpxx8h5nj5wkxvznbqn")))

(define-public crate-ajson-0.2.3 (c (n "ajson") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01h6wlsxcm51xkw23x7npmkmr9g0f5224dywdqdwl05izjhb93ry")))

(define-public crate-ajson-0.2.4 (c (n "ajson") (v "0.2.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vn61nxhi1jiachspnkj84hy2mbqynd981xxw9ccy6lg2ib66vgs")))

(define-public crate-ajson-0.3.0 (c (n "ajson") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "gjson") (r "^0.8.1") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "1d8ykwrjyldkzy7mam9clfpyjzbdh7jl31ir4ka0bnx93sdjf2ra") (f (quote (("wild" "regex") ("default"))))))

(define-public crate-ajson-0.3.1 (c (n "ajson") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "gjson") (r "^0.8.1") (d #t) (k 2)) (d (n "json") (r "^0.11.13") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 2)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "1idi5p7i274khj3gizd6jvssnbiygvmi2p0ljkqfyy7mz1n7f8r8") (f (quote (("wild" "regex") ("default"))))))

