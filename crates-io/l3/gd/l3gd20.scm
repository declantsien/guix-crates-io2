(define-module (crates-io l3 gd l3gd20) #:use-module (crates-io))

(define-public crate-l3gd20-0.1.0 (c (n "l3gd20") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "0zjcafk7pg45nmvz0bp7cdqqgd8096p4fclm09a73302lsi29bsh")))

(define-public crate-l3gd20-0.1.1 (c (n "l3gd20") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "1db4z54hwfn8v48fmz9kv8fw4gwqiy15yi9z26687ld8365hl3xv")))

(define-public crate-l3gd20-0.1.2 (c (n "l3gd20") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)))) (h "17mcdv79q4imaskglh47b6xsp1a2hzb515021shksr8in2c132iq")))

(define-public crate-l3gd20-0.2.0 (c (n "l3gd20") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "1v4nbc5x3jqx0w63fnhfl0v8bi81vs4i2qznrp2lzhgl26blmnpk")))

(define-public crate-l3gd20-0.3.0 (c (n "l3gd20") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "14gl1jyxa9s025bvb0v3a65s77n3y38py1d76rbbsbsl5g5ky4c6")))

