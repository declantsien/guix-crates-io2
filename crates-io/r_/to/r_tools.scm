(define-module (crates-io r_ to r_tools) #:use-module (crates-io))

(define-public crate-r_tools-0.1.0 (c (n "r_tools") (v "0.1.0") (h "1bxwp2p1j51wsa8fbfh4fjvdc2a97ak0k8yyh2ni2gksjv4d6wh3") (y #t)))

(define-public crate-r_tools-0.1.1 (c (n "r_tools") (v "0.1.1") (h "08ba8z2gf2qn83g6hkjca4v1b3q94gm0aci8cmq4b4hg8p56ia34") (y #t)))

(define-public crate-r_tools-0.1.2 (c (n "r_tools") (v "0.1.2") (h "0rxmcsncsbrw3swq893zv3hmm7bgkz81fjna10qyvj49g25iw5ah")))

