(define-module (crates-io r_ i1 r_i18n) #:use-module (crates-io))

(define-public crate-r_i18n-0.1.0 (c (n "r_i18n") (v "0.1.0") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "0nc637hdgfip33ijcs4sifl0g899m40w0cqrypdb01hcn9pbyyqx")))

(define-public crate-r_i18n-1.0.0 (c (n "r_i18n") (v "1.0.0") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1a1sibxadp0hizs3lrjlx11q3dk2rq798zzx5vh0bndgws7bky2f")))

(define-public crate-r_i18n-1.0.1 (c (n "r_i18n") (v "1.0.1") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1ijvpbipnckxyj71932d4ncyfrqszsz6jgb5ar2nf3c9lgs4da44")))

