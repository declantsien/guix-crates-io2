(define-module (crates-io r_ ma r_mathlib) #:use-module (crates-io))

(define-public crate-r_mathlib-0.1.0 (c (n "r_mathlib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "1i1h04pd5h02zw6p67qx12k51ll2bbymdhb5r4ihxccvk84cgmjc")))

(define-public crate-r_mathlib-0.1.1 (c (n "r_mathlib") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.54") (d #t) (k 1)))) (h "0qdxzwzk0kcibfvnx7fypr73d1c3h4q9awhfckh60ipbsrn1zmb0")))

(define-public crate-r_mathlib-0.2.0 (c (n "r_mathlib") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0mfw6zjfawdhkpzm26kgiv7bgfgiqk8bmqrihznckwrrg3lc5s52")))

