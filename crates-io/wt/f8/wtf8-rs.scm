(define-module (crates-io wt f8 wtf8-rs) #:use-module (crates-io))

(define-public crate-wtf8-rs-0.0.0 (c (n "wtf8-rs") (v "0.0.0") (h "1wa65arbim9002jnw16djxaajklq9xni65czsv9w6nc95m3y2yjq")))

(define-public crate-wtf8-rs-1.0.0 (c (n "wtf8-rs") (v "1.0.0") (h "04wy3463nhqgqrzrcwg7646788qfnwzqx243bp1zp90zsmcz74fm")))

(define-public crate-wtf8-rs-1.1.0 (c (n "wtf8-rs") (v "1.1.0") (h "0i4sbh6vn6n7qqqbsyaq2x4axh737r7xvbymjglh6qjr323y69jx")))

