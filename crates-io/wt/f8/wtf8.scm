(define-module (crates-io wt f8 wtf8) #:use-module (crates-io))

(define-public crate-wtf8-0.0.1 (c (n "wtf8") (v "0.0.1") (h "188fzbsbzvdv2cj29713an1hy54g7cg699i7d0qyq8bk35azk7f7")))

(define-public crate-wtf8-0.0.2 (c (n "wtf8") (v "0.0.2") (h "1al67xragms05sw0d3bipbkln91ayqs1bkk004v04lmjjipvp63s")))

(define-public crate-wtf8-0.0.3 (c (n "wtf8") (v "0.0.3") (h "0mkrjy25payinns0sjm9n3zcjz0jnlf37ddjis54i733hsd31ffn") (f (quote (("std") ("default" "std"))))))

(define-public crate-wtf8-0.1.0 (c (n "wtf8") (v "0.1.0") (h "01ia1dkq2zcb8mnj5h1r3vgz7g5njh4pv8fkxxv27x9q5i4yh6n0")))

