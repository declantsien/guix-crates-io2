(define-module (crates-io wt a- wta-hyper) #:use-module (crates-io))

(define-public crate-wta-hyper-0.1.0 (c (n "wta-hyper") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("server" "tcp" "http1" "http2"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.0") (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "what-the-async") (r "=0.1.0") (d #t) (k 2)) (d (n "wta-executor") (r "=0.1.0") (d #t) (k 0)) (d (n "wta-reactor") (r "=0.1.0") (d #t) (k 0)))) (h "0d64q3wfym2qy1gs1y2kaxyr9mrh3szmypynza90wpq14jqsd413")))

