(define-module (crates-io wt x- wtx-macros) #:use-module (crates-io))

(define-public crate-wtx-macros-0.1.0 (c (n "wtx-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "printing" "extra-traits" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0") (k 2)))) (h "1cxdpa04b0b3b1bj5ymlslj8pp641g6617c2zlm8432cy8y584fg") (f (quote (("default"))))))

