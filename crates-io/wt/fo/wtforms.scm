(define-module (crates-io wt fo wtforms) #:use-module (crates-io))

(define-public crate-wtforms-0.0.0 (c (n "wtforms") (v "0.0.0") (h "18v0m5mxmx12l632wb4as0ny76ibvbfms5jw2clxjn2xlhnk95fm")))

(define-public crate-wtforms-0.0.1 (c (n "wtforms") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "wtforms-derive") (r "^0.0") (d #t) (k 0)))) (h "1bap9gr1kbcb9gppjl3kd5almak8xqnahgcj2bjydkng2qi8j735")))

