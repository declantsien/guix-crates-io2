(define-module (crates-io wt fo wtforms-derive) #:use-module (crates-io))

(define-public crate-wtforms-derive-0.0.0 (c (n "wtforms-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0v4wi0l59myrp793hgi0av48hmhndkb33m59mvbqr7c8s6xfswwr")))

