(define-module (crates-io wt tr wttr) #:use-module (crates-io))

(define-public crate-wttr-0.2.0 (c (n "wttr") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "05n2mn22zyvrdbckbk57kvb8pvww5v710xg1d4ghrmxm7x4f17yg")))

