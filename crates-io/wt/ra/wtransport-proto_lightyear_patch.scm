(define-module (crates-io wt ra wtransport-proto_lightyear_patch) #:use-module (crates-io))

(define-public crate-wtransport-proto_lightyear_patch-0.1.8 (c (n "wtransport-proto_lightyear_patch") (v "0.1.8") (d (list (d (n "ls-qpack") (r "^0.1.3") (d #t) (k 0)) (d (n "octets") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "16j0b8jfy2vnvlx3k5hirdwk2zx7m9371vy1ma886qdf2jqfam85") (f (quote (("default") ("async")))) (r "1.65.0")))

