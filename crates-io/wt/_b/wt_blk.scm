(define-module (crates-io wt _b wt_blk) #:use-module (crates-io))

(define-public crate-wt_blk-0.0.1 (c (n "wt_blk") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0b0947dhhc722m9p1g6gwhva6arzkygn6a90hdq1q5m5b68v0gmj")))

(define-public crate-wt_blk-0.0.2 (c (n "wt_blk") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0s4yzf20fa9fbiqb8dm7ckqqwafxrs0w4xk0gy9i6sygzg4aa2s1")))

(define-public crate-wt_blk-0.0.3 (c (n "wt_blk") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.151") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sxirk47sd3af9s1jjjfw6ngn5clnk4s2sy81ynhnv2z8v3s7f0v")))

(define-public crate-wt_blk-0.0.4 (c (n "wt_blk") (v "0.0.4") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ndwbdl3qjsjz9p18lgyr9n8lc1yn49yzylqyl5x7srq86gh2kkg")))

(define-public crate-wt_blk-0.0.5 (c (n "wt_blk") (v "0.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rwbjn8j2scvw1y3zfv8hvnhpxhaal2bjky4czsmlsy1rvkh4d1d")))

(define-public crate-wt_blk-0.0.6 (c (n "wt_blk") (v "0.0.6") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09r8slnlirhi6kilc5dp73pizgz0rrys80rzvzil5b1c1fgxaw4c")))

(define-public crate-wt_blk-0.1.0 (c (n "wt_blk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ruzstd") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0n0lmjbsp553954a13l2q88bmw5nabfpdbqksv55w848bhlx5hlq")))

(define-public crate-wt_blk-0.1.1 (c (n "wt_blk") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ruzstd") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "04k0kg0avsmcz8z84kgwv1fyd0yx7j1gznp45kjav4cwksg3yix6")))

(define-public crate-wt_blk-0.1.2 (c (n "wt_blk") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ruzstd") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0a2zk0admj8w4zxgjmxhfr5kvi46w03d24gss76n05qw2qakzzd6")))

