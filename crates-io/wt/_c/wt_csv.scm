(define-module (crates-io wt _c wt_csv) #:use-module (crates-io))

(define-public crate-wt_csv-0.2.0 (c (n "wt_csv") (v "0.2.0") (h "031lq4k0ibv7m5bhx96y8wgvxqwil49nrmp4bcqphlfs40cqb17d") (r "1.58.1")))

(define-public crate-wt_csv-0.3.0 (c (n "wt_csv") (v "0.3.0") (h "0r9nl3ifla5wyrfjcspkcj9wz91a4dw8fkmynpsa22x09alga4bi") (r "1.58.1")))

(define-public crate-wt_csv-0.4.0 (c (n "wt_csv") (v "0.4.0") (h "0smjzpwccxvhqw39fdmjjvf2n9s660ahdrn7f1ln1lm77q40qrxs") (r "1.58.1")))

(define-public crate-wt_csv-0.5.0 (c (n "wt_csv") (v "0.5.0") (h "1kgdxmdfiw53xz0ck11rzq7lb1h2k4rfa1nlhzsv7335y4brf6si") (r "1.58.1")))

(define-public crate-wt_csv-0.6.0 (c (n "wt_csv") (v "0.6.0") (h "0ndj90anihcsm1v56g7pfyfgj8nfxk240n88wwca95an6wpdivr0") (r "1.58.1")))

(define-public crate-wt_csv-0.7.0 (c (n "wt_csv") (v "0.7.0") (h "0ac922j48hbpdc0nkipfccms09qmriwzz53yvzzkg1bra1jyfl78") (r "1.58.1")))

(define-public crate-wt_csv-0.8.0 (c (n "wt_csv") (v "0.8.0") (h "1b1sggd93y6nw4sz45211jgqnv76cg6y9ha0bbzcmwr76n4d3i00") (r "1.58.1")))

(define-public crate-wt_csv-0.9.0 (c (n "wt_csv") (v "0.9.0") (h "1nmnbq1v18l1sl7krsmmdh5ci02hkcbz9bvx8nmhmj2r5hb6qy6j") (r "1.58.1")))

(define-public crate-wt_csv-0.10.0 (c (n "wt_csv") (v "0.10.0") (h "07gwq2a02v6hn4ady4zps0g3hg4v5d9wgnav32yks9qgslgj27yw") (r "1.58.1")))

