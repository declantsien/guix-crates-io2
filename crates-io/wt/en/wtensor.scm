(define-module (crates-io wt en wtensor) #:use-module (crates-io))

(define-public crate-wtensor-0.1.0 (c (n "wtensor") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 2)))) (h "043hsi0j71sjnil9syyim2asr6bxhfg3df13sww7hg7g4nxwxlig")))

