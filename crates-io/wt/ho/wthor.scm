(define-module (crates-io wt ho wthor) #:use-module (crates-io))

(define-public crate-wthor-0.1.0 (c (n "wthor") (v "0.1.0") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09pnicp9pzvk96p9rq7xap00jibk961584gpaxhdxf3x1x5an8hz")))

(define-public crate-wthor-0.2.0 (c (n "wthor") (v "0.2.0") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17a7ywkc394c5hhsdvagwlrp2p74v34j4qnh7v5dpkn9pfxxxmad")))

(define-public crate-wthor-0.3.0 (c (n "wthor") (v "0.3.0") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7q1qi3yss0jr52wg74lrb684scinndxnnpirk41rfqldg8mh8p")))

(define-public crate-wthor-0.4.0 (c (n "wthor") (v "0.4.0") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08fvvmpiyzvckq3hsw0m9gvmr1mxwhwb8pcvhy6ma12ylz27y4nc")))

(define-public crate-wthor-0.4.1 (c (n "wthor") (v "0.4.1") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kzx1prsq08z8xzx1733d33v667j3rghf0bh1n498fcnjckfwhxd")))

(define-public crate-wthor-0.4.2 (c (n "wthor") (v "0.4.2") (d (list (d (n "magpie") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19w32vjq23ssbl3bbkvql99i0v25fq3qq28c4fzlyqdc6qjpqmgm")))

(define-public crate-wthor-0.5.0 (c (n "wthor") (v "0.5.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "magpie") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0hqy8lwzd46az6cg66xxf0cnc1v27nvchx5jndn2vdqhb2hjq2ka")))

