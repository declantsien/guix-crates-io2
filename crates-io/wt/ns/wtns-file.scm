(define-module (crates-io wt ns wtns-file) #:use-module (crates-io))

(define-public crate-wtns-file-0.1.0 (c (n "wtns-file") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "1zd1vryr3jbawcx89146905d67mnjay39z3n0bjmg5znzfqzhms0")))

(define-public crate-wtns-file-0.1.1 (c (n "wtns-file") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "1jv5qhy1dcx0assdgkjrg6a0vj4hsdhy8mmmbmv0jnjrcrn6gkv8")))

(define-public crate-wtns-file-0.1.2 (c (n "wtns-file") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "14r7sb6b1k99ccwz779vpfbm2ichwyx6zdix1qf0zx58f3m0dbwp")))

(define-public crate-wtns-file-0.1.3 (c (n "wtns-file") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "193hb56xrfxmhkzwfzavmlb4sqr7zrlrr7mqh4x4jy65396ssnzf")))

(define-public crate-wtns-file-0.1.4 (c (n "wtns-file") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "0qbxg491y48k1w2fbnwl22dbhfaj81y6g90wbnyll06cq8r8x3ff")))

(define-public crate-wtns-file-0.1.5 (c (n "wtns-file") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)))) (h "02mzv8jksym3hflyhjzsm4y7apdcif8h2fcyhyjni3r9a9j8afrx")))

