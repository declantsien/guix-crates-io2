(define-module (crates-io wt fi wtfis) #:use-module (crates-io))

(define-public crate-wtfis-0.1.0 (c (n "wtfis") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)))) (h "16cfajrxb1a3i3ll4h2rzsc4wbba0ixh58b67q0khgc6677qrz2k")))

