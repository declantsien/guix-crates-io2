(define-module (crates-io wt es wtest) #:use-module (crates-io))

(define-public crate-wtest-0.0.0 (c (n "wtest") (v "0.0.0") (d (list (d (n "paste") (r "~1.0") (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nzqkx6qbz86p6k6pzxj51m805fhrnp6jdc9annmml0531cbdpaa")))

(define-public crate-wtest-0.0.2 (c (n "wtest") (v "0.0.2") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 0)))) (h "1hpcpg5afxssg2ssvbkvz8q9kdgx8zi9qinlrs8p8if1lxfrh1xg")))

(define-public crate-wtest-0.0.3 (c (n "wtest") (v "0.0.3") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 0)))) (h "13pkfkq0vcnsikqxh5i6yjkpllyc1m0aw63621marilq099avvam")))

(define-public crate-wtest-0.1.0 (c (n "wtest") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 0)))) (h "0sn8hcg7d1kry71sbqim794ighhjip23dprjjszk4c6qnp9dq5s3")))

(define-public crate-wtest-0.1.2 (c (n "wtest") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 0)))) (h "1d3jpa67032j59x5n2cl7ph9w2cgr07m4z235dv1z6004a4mxvds") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

