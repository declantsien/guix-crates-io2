(define-module (crates-io wt -b wt-battle-report) #:use-module (crates-io))

(define-public crate-wt-battle-report-0.1.0 (c (n "wt-battle-report") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "13vjiyjxd69cwqnw9raxaxndg7kf6py1sgxb4bkxc7736y9w18js")))

(define-public crate-wt-battle-report-0.1.1 (c (n "wt-battle-report") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0rshgavqa1iswfsixzyp8lrdj3jpb33x031rszhmry5hfdb81h33")))

