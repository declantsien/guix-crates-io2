(define-module (crates-io wt in wtinylfu) #:use-module (crates-io))

(define-public crate-wtinylfu-0.1.0 (c (n "wtinylfu") (v "0.1.0") (d (list (d (n "bloomfilter") (r "^1") (d #t) (k 0)) (d (n "count-min-sketch") (r "^0.1.7") (d #t) (k 0)) (d (n "lru") (r "^0.7.6") (d #t) (k 0)))) (h "0pz44b8pd6k003iqrl442h83389v8ifmrl5g8hpl93g6lfvd17m2")))

