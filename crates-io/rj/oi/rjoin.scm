(define-module (crates-io rj oi rjoin) #:use-module (crates-io))

(define-public crate-rjoin-0.1.0 (c (n "rjoin") (v "0.1.0") (d (list (d (n "clap") (r "^2.25.1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.3") (d #t) (k 0)))) (h "1gxm91yhxg3i41vcmw3ciivw8nf5w6y78qrr0ssmdjjij9kncvz7")))

(define-public crate-rjoin-0.2.0 (c (n "rjoin") (v "0.2.0") (d (list (d (n "clap") (r "^2.25.1") (d #t) (k 0)) (d (n "csvroll") (r "^0.1.0") (d #t) (k 0)) (d (n "rollbuf") (r "^0.1.0") (d #t) (k 0)))) (h "1kvz6n08glmqxcl11ix1q7p2s3wjy6mfln0p0bbbixhymrw9zxnh")))

