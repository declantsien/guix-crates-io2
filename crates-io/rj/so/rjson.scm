(define-module (crates-io rj so rjson) #:use-module (crates-io))

(define-public crate-rjson-0.2.0 (c (n "rjson") (v "0.2.0") (h "13in9d5bglgqwigqc75mpgnlxrlsd5ygnpip49wpj0bzm1cvr18m")))

(define-public crate-rjson-0.2.1 (c (n "rjson") (v "0.2.1") (h "1iindkq84jzff3z67v531d4j88whs8gr8n6rwldrzn1n0az6nxgh")))

(define-public crate-rjson-0.3.0 (c (n "rjson") (v "0.3.0") (h "0l3nmd66h3xq23r9zqc6yx8l24j8a6sgwlszsms1zri65hxkp5vj")))

(define-public crate-rjson-0.3.1 (c (n "rjson") (v "0.3.1") (h "06d2vi3ljsn40fpmiqzlw7zqspyxnqvgdc93j7v7phy493gdn42m")))

