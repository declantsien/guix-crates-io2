(define-module (crates-io rj in rjini) #:use-module (crates-io))

(define-public crate-rjini-0.0.1 (c (n "rjini") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)))) (h "1k0cpgzlwvdh6pqk8dlv2idhjhm3jlkzddb06sqf3c8x9fd4rxcb")))

(define-public crate-rjini-0.0.2 (c (n "rjini") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "05r3czqx50jwz08r51xcrz7y8sjxrwibh2jj2q33igk6si0jvbw9")))

(define-public crate-rjini-0.0.3 (c (n "rjini") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0zcl0sirpqjqvdcd9r5f1dqwalaad09kar09dpq18lmi1bv9z1p6")))

