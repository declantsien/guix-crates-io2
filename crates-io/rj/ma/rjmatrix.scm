(define-module (crates-io rj ma rjmatrix) #:use-module (crates-io))

(define-public crate-rjmatrix-1.0.0 (c (n "rjmatrix") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "1rz17f395xly974b1hx7dh0411f1l6m0k9l3n8kz0nmniad6f53z")))

(define-public crate-rjmatrix-1.0.1 (c (n "rjmatrix") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "0ijrk6xssixsrizkxwcip442iwn7kdnjljspkr2kqyc3m6185wmv")))

(define-public crate-rjmatrix-1.0.2 (c (n "rjmatrix") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "0s798ihglbdw2pjpc6k4m7vszk6c1x78i53r04z4i0qw1rdk01nb")))

(define-public crate-rjmatrix-1.0.3 (c (n "rjmatrix") (v "1.0.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "log4rs") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (f (quote ("all-widgets"))) (d #t) (k 0)))) (h "094a85q47vz9yxz589m8cakwdmh9rim8f24g5zm49q18gamjcg39")))

