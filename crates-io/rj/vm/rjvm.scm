(define-module (crates-io rj vm rjvm) #:use-module (crates-io))

(define-public crate-rjvm-0.1.0 (c (n "rjvm") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)))) (h "0l2hh6zf6np4qh1d0hczdx6aj0fn32pig7819s5fghrmp383kz08") (f (quote (("default" "decoder") ("decoder"))))))

(define-public crate-rjvm-0.2.0 (c (n "rjvm") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)))) (h "13278d039bbijnnbnh4kmnp5mjvxfqpyxhrmrynfxcv35qv6dxk4") (f (quote (("default" "decoder") ("decoder"))))))

(define-public crate-rjvm-0.3.0 (c (n "rjvm") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "05xid62fry2r60179ajpkhcngf6q4a8x21xqia4riksq39bn8wa3") (f (quote (("default" "decoder") ("decoder"))))))

