(define-module (crates-io rj de rjdebounce) #:use-module (crates-io))

(define-public crate-rjdebounce-0.1.0 (c (n "rjdebounce") (v "0.1.0") (h "1js4cni7s6falqns284i1vbapcwzqjrg49ri6r3gknz0650bjvpm")))

(define-public crate-rjdebounce-0.1.1 (c (n "rjdebounce") (v "0.1.1") (h "0m0az8hg4gvbvvc94nbnbdh4ivacwab4nc27k30qs8mkwbgdb337")))

(define-public crate-rjdebounce-0.2.0 (c (n "rjdebounce") (v "0.2.0") (h "1ny5a2aypw5k26zpq4wv1whg5z1nn6lvhijyl8r3bid8z2c2fhab")))

(define-public crate-rjdebounce-0.2.1 (c (n "rjdebounce") (v "0.2.1") (h "1dzd0wwga59li5g6wzf5r9nyxzpyfndp3q2i0wvqk1ldh4yzyv7c")))

