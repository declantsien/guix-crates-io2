(define-module (crates-io rj w- rjw-metoffice) #:use-module (crates-io))

(define-public crate-rjw-metoffice-0.1.0 (c (n "rjw-metoffice") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "04xlfjf489mdkbwxl3h7vqncvri36ayj5hlqx7fs8s6slz87amjz")))

