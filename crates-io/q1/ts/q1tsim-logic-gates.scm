(define-module (crates-io q1 ts q1tsim-logic-gates) #:use-module (crates-io))

(define-public crate-q1tsim-logic-gates-0.1.0 (c (n "q1tsim-logic-gates") (v "0.1.0") (d (list (d (n "q1tsim") (r "^0.5") (d #t) (k 0)))) (h "1ln5zxm9rkwfxzb60kfch9cj3vrb0nr8bacddglc0yl6hgfx0ws2") (y #t)))

(define-public crate-q1tsim-logic-gates-0.1.1 (c (n "q1tsim-logic-gates") (v "0.1.1") (d (list (d (n "q1tsim") (r "^0.5") (d #t) (k 0)))) (h "0b90fzfh5h7v5dc3jnb93wfva9x10034vrpcvr0w4lgmmsaag1yk")))

(define-public crate-q1tsim-logic-gates-0.2.0 (c (n "q1tsim-logic-gates") (v "0.2.0") (d (list (d (n "q1tsim") (r "^0.5") (d #t) (k 0)))) (h "1m1pgx2076kb4sjix14p2fc56ic4bywkcqdbz8c720352c1vmybi")))

(define-public crate-q1tsim-logic-gates-0.2.1 (c (n "q1tsim-logic-gates") (v "0.2.1") (d (list (d (n "q1tsim") (r "^0.5") (d #t) (k 0)))) (h "07aj0nzkhi9yvjhwkj1k6z1bh127598d9xmxh7cj0k0sfdgh6xyi")))

