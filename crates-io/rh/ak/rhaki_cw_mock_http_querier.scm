(define-module (crates-io rh ak rhaki_cw_mock_http_querier) #:use-module (crates-io))

(define-public crate-rhaki_cw_mock_http_querier-0.1.0 (c (n "rhaki_cw_mock_http_querier") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.4") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw20") (r "^0.15.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0ky6k11gdi3kizcycj83igyh0w6axm8zhdnyh2awxb53l00sxi9d")))

