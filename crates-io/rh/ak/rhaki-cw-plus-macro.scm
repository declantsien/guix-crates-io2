(define-module (crates-io rh ak rhaki-cw-plus-macro) #:use-module (crates-io))

(define-public crate-rhaki-cw-plus-macro-0.1.0 (c (n "rhaki-cw-plus-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "150svpsy2lnqa62d31w68dbiy842r3k0zy1yq3hflvbbnbi1551x")))

(define-public crate-rhaki-cw-plus-macro-0.2.0 (c (n "rhaki-cw-plus-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "03w6g6nzwis3a2ywlnq92jx3rb6vi6pkn8qdcw14j64nlxizgrdj")))

(define-public crate-rhaki-cw-plus-macro-0.2.1 (c (n "rhaki-cw-plus-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "19zvdar1xnq267w6jr34cskhsg8c74g3j82rp4ghnwn6qb4mw22z")))

