(define-module (crates-io rh ac rhack) #:use-module (crates-io))

(define-public crate-rhack-0.1.0 (c (n "rhack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "0k30crdqzy3v4rwk3mz5ac1ai9ngfrzvhs5w1kk0a2769mxn7ssz")))

