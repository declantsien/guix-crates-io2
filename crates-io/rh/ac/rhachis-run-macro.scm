(define-module (crates-io rh ac rhachis-run-macro) #:use-module (crates-io))

(define-public crate-rhachis-run-macro-0.1.0 (c (n "rhachis-run-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0c80r3bqdhn1gp1na13rsgp0zxwqphvlq8pngksh9mza0gs4nplx")))

(define-public crate-rhachis-run-macro-0.1.1 (c (n "rhachis-run-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1v16scw4xn666miy1qha5j16naicfkybw5v31n4503ambw07a7fj")))

(define-public crate-rhachis-run-macro-0.2.0 (c (n "rhachis-run-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "06ppg8rxh4zd2b6m9718llsm5mgvp40cpx3rsm1ppgqkir2g0d5f")))

(define-public crate-rhachis-run-macro-0.3.0 (c (n "rhachis-run-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (o #t) (d #t) (k 0)))) (h "1nr043madqksalac9mjkyfq4z7ip1k0hqm2abcpmx5iziypbz6qb") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-rhachis-run-macro-0.3.1 (c (n "rhachis-run-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (o #t) (d #t) (k 0)))) (h "1p55h8bfs6gsa2blkdvqrhmh2qvi72h582bakil86l0jwdfxq0bn") (s 2) (e (quote (("tokio" "dep:tokio"))))))

