(define-module (crates-io rh ub rhub) #:use-module (crates-io))

(define-public crate-rhub-0.1.0 (c (n "rhub") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m58jb8zygd8y6kxn70kiplm3vylzvn3mp93hwh45b1kh69ldrdh")))

(define-public crate-rhub-0.2.0 (c (n "rhub") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h7x1zymk9ik9a5bimppradw6fn5krmxh2lcjbpqlnz3khdlv34r")))

