(define-module (crates-io rh uf rhuffle) #:use-module (crates-io))

(define-public crate-rhuffle-0.2.0 (c (n "rhuffle") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1sas5zpfqpyrmdpi73gdk86iz3glbc55km96hrcvbrw0ydbhpaq6")))

(define-public crate-rhuffle-0.3.0 (c (n "rhuffle") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0wqm262z80ziyi30j15rp951i720437045l2v6jwqy77gsc98mj0")))

(define-public crate-rhuffle-0.3.1 (c (n "rhuffle") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "10adcngi4c2sc7df3fvhwyfr8yv6klwxkhcnbq5yrkn4s0cx28h8")))

(define-public crate-rhuffle-0.3.2 (c (n "rhuffle") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "042vgk6kyysf84v8wfglcgldai3x1kc2gmw70m1ia7pixnb24mj8")))

(define-public crate-rhuffle-0.3.3 (c (n "rhuffle") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "12q1897amww8ncwxywi1s6ypc3r4ml326y4yfsvhmlhwcq123svf")))

