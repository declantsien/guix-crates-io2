(define-module (crates-io rh yt rhythmc_macros) #:use-module (crates-io))

(define-public crate-rhythmc_macros-0.0.2 (c (n "rhythmc_macros") (v "0.0.2") (d (list (d (n "glsl") (r "^6.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rhythmc_compiler") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0lsm196zgf2src6npci7sy75x44g20kj7a17jz666qd5pcwywrp6")))

