(define-module (crates-io rh yt rhythmc_compiler) #:use-module (crates-io))

(define-public crate-rhythmc_compiler-0.0.2 (c (n "rhythmc_compiler") (v "0.0.2") (d (list (d (n "glsl") (r "^6.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.23") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (f (quote ("parsing" "printing" "full"))) (d #t) (k 0)))) (h "1mddfglvyhady1z5zy3fir0vapngyrvq47kcn1xgrh4sjqdp85dp")))

