(define-module (crates-io rh yt rhythmc) #:use-module (crates-io))

(define-public crate-rhythmc-0.0.2 (c (n "rhythmc") (v "0.0.2") (d (list (d (n "rhythmc_compiler") (r "^0.0.2") (d #t) (k 0)) (d (n "rhythmc_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "vulkano") (r "^0.19") (d #t) (k 0)))) (h "07wh7nmmbgci8c3q8w1a35jjwskiv9lg0hpsicyjdwlgffd81yhn")))

