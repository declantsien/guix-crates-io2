(define-module (crates-io rh yt rhythm) #:use-module (crates-io))

(define-public crate-rhythm-0.0.1 (c (n "rhythm") (v "0.0.1") (h "0ng8ars3q3940an09nny6nmbvx6xx64s2269w2pwsmvqrbbxp374")))

(define-public crate-rhythm-0.0.2 (c (n "rhythm") (v "0.0.2") (h "0y1bzvl5vdrmjmjbqafybfbym7vvjam7754m9d4w0xvxn6rp59pf")))

(define-public crate-rhythm-0.1.0 (c (n "rhythm") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1gsi4hypl1i181hfp93nyrsccdms8qh3fs8ydrwg0wns3b38jbn8")))

