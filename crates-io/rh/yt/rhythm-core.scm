(define-module (crates-io rh yt rhythm-core) #:use-module (crates-io))

(define-public crate-rhythm-core-0.1.0 (c (n "rhythm-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "132aa0ak1c2ayyywiccnaizhizhyxb7cp8jpfjb3yj0y4d46wkva") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-rhythm-core-0.2.0 (c (n "rhythm-core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h0y125ydpcx6kqnc12r1ph7fh8hhmp2l62kh5d578w0zgpqaj4i") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

