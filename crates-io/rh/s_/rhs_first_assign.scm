(define-module (crates-io rh s_ rhs_first_assign) #:use-module (crates-io))

(define-public crate-rhs_first_assign-0.1.0 (c (n "rhs_first_assign") (v "0.1.0") (d (list (d (n "if_chain") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.8") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "parsing" "printing" "visit-mut"))) (d #t) (k 0)))) (h "1rr0021mfbmv8mvzyqn32awpgxq1fna5alkn14k32xd1zppmnp96")))

