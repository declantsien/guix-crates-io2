(define-module (crates-io rh od rhodonite) #:use-module (crates-io))

(define-public crate-rhodonite-0.0.1 (c (n "rhodonite") (v "0.0.1") (d (list (d (n "bytes") (r "^0.6.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.23.1") (d #t) (k 0)))) (h "1ss1zbz0qj3vr09l65lh1bwlr0pp0cgx49mwin4qcspddygjhy2y")))

