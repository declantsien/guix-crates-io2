(define-module (crates-io rh iz rhizome_proc-macro-definitions) #:use-module (crates-io))

(define-public crate-rhizome_proc-macro-definitions-0.0.1 (c (n "rhizome_proc-macro-definitions") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("derive" "parsing"))) (k 0)) (d (n "syn-mid") (r "^0.5.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "03dwf3fam1zxj8zl9lh9hir4r8j70g0r4fcfjz44nj7pxpslmir0")))

