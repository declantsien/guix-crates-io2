(define-module (crates-io rh iz rhizome) #:use-module (crates-io))

(define-public crate-rhizome-0.0.1 (c (n "rhizome") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "mapped-guard") (r "^0.0.1") (d #t) (k 0)) (d (n "rhizome_proc-macro-definitions") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1b13yshxs56j15y6bpa7b1bmldn3dfc0ssn8r3ix1nflxgpagfnj") (f (quote (("macros" "rhizome_proc-macro-definitions"))))))

