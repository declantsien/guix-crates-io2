(define-module (crates-io rh iz rhiz) #:use-module (crates-io))

(define-public crate-rhiz-0.1.0 (c (n "rhiz") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1navzgxs4z8fmwdn4f1plbw847n0q34njikca2z4lc0614kv7dsd") (f (quote (("default" "glob"))))))

(define-public crate-rhiz-0.4.0 (c (n "rhiz") (v "0.4.0") (d (list (d (n "glob") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0ask01xbvsqs56jz4b2qb8hqab2hlhjrcxnqgphbf6kxz61c51z6") (f (quote (("default" "glob"))))))

(define-public crate-rhiz-0.5.0 (c (n "rhiz") (v "0.5.0") (d (list (d (n "glob") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0qa27zcw024lnh00r7lz04z4xq5h0k3vzqgp82njgqhn6ay4vjik") (f (quote (("default" "glob"))))))

(define-public crate-rhiz-0.5.1 (c (n "rhiz") (v "0.5.1") (d (list (d (n "glob") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1lvx7zvjz7gqvh5j7p8l8j6kycxnismbzq7sqhzdhawkcxcrffln") (f (quote (("default" "glob"))))))

(define-public crate-rhiz-0.6.0 (c (n "rhiz") (v "0.6.0") (d (list (d (n "glob") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "17gz74vcc2fxqrl53m34g3v0cxpdigqcg9lbzy2kln237g5d0mfm") (f (quote (("default" "glob"))))))

