(define-module (crates-io rh iz rhizo-types) #:use-module (crates-io))

(define-public crate-rhizo-types-0.0.1 (c (n "rhizo-types") (v "0.0.1") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "132xd99i4m4xf2qkbxvnwszdp3lgi8gi2bnrryvw6ccvq7cylx54")))

(define-public crate-rhizo-types-0.0.11 (c (n "rhizo-types") (v "0.0.11") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m9fz8zllhha1wzxzxrq00fcblf0qg8105sjj4kkz3dl1qvzvawm")))

(define-public crate-rhizo-types-0.1.0 (c (n "rhizo-types") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jmgs21s7qfq8wwi3k6kn0rnmrxl6z84n9vmadsl35498r34zlhg")))

(define-public crate-rhizo-types-0.1.1 (c (n "rhizo-types") (v "0.1.1") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "07g3v487dab2finb38ylgx9v7cpgy6yff71cp5lmvjc112zifngx")))

(define-public crate-rhizo-types-0.1.3 (c (n "rhizo-types") (v "0.1.3") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1psixq5vkwi8v9f8dw1l373qh15qyr0rgpgkdb3izvk0g4h02fx5")))

(define-public crate-rhizo-types-0.1.4 (c (n "rhizo-types") (v "0.1.4") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sd3i69z17vsr5srns99yzc1vabxyj631jf5r9mqri90jjkaqrm4")))

(define-public crate-rhizo-types-0.1.5 (c (n "rhizo-types") (v "0.1.5") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pqc48f7q1zv1s94amsiajj4gjarvjrydniy8gyhk1k4azbw1v31")))

(define-public crate-rhizo-types-0.1.6 (c (n "rhizo-types") (v "0.1.6") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1apm2wv7ql9c2b3gl4yfz9m3zmd57qzhx64hd6db9dg21v8gnv5h")))

(define-public crate-rhizo-types-0.1.7 (c (n "rhizo-types") (v "0.1.7") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zx2w5gribqpc8b56db5ph2pj8v0wizp6y7dxcbplrbgj5gy4pxj")))

(define-public crate-rhizo-types-0.1.8 (c (n "rhizo-types") (v "0.1.8") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yvfg3mj1jlx2z8n62khz53hw4zp8w62jgj26apqp1i9968rp18a")))

(define-public crate-rhizo-types-0.1.9 (c (n "rhizo-types") (v "0.1.9") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "088970wq02wrcc014fy0y05b1ljycdi3221lmv30i5z99g88jz27")))

(define-public crate-rhizo-types-0.1.10 (c (n "rhizo-types") (v "0.1.10") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fn07543m44crlf57wn4jz51rcfqzfzm0skdx6maxrqb6vxzjs1")))

(define-public crate-rhizo-types-0.1.11 (c (n "rhizo-types") (v "0.1.11") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "03rmcqhdqywmxjzvbgpwwyny47gcphf7b11xpnq6xvckf105pg6r")))

(define-public crate-rhizo-types-0.1.12 (c (n "rhizo-types") (v "0.1.12") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1isfcinsywndrlbsgwb12cnkgqj55d6id70y0n5sdykmy5i97zsh")))

