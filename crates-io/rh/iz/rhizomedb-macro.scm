(define-module (crates-io rh iz rhizomedb-macro) #:use-module (crates-io))

(define-public crate-rhizomedb-macro-0.1.0 (c (n "rhizomedb-macro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00nb5i8mxvgmprkrd585ky8qvprqb9hh1sdf6bz7xhgrk4kqcr2x")))

