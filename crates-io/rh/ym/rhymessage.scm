(define-module (crates-io rh ym rhymessage) #:use-module (crates-io))

(define-public crate-rhymessage-1.0.0 (c (n "rhymessage") (v "1.0.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "187wzwg84fn7xj8jbxg4h17y4yma7jq79pwxvimwj25cdr8mqnfl")))

(define-public crate-rhymessage-1.0.1 (c (n "rhymessage") (v "1.0.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11n1szrj0p409zxbwjwp19ld21sbpr3rjk5pgabl16wfvphbxrrn")))

(define-public crate-rhymessage-1.0.2 (c (n "rhymessage") (v "1.0.2") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bw4i7lg3pvy7byv8wqrp02c2sdlsajd8mb1idfjvpn4d3x5w4pk")))

(define-public crate-rhymessage-1.1.0 (c (n "rhymessage") (v "1.1.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lqj4jnsrisaidwli788ww450rlf0im0nh5ycq4qn1z97vl6zm98")))

(define-public crate-rhymessage-1.2.0 (c (n "rhymessage") (v "1.2.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g10b0gpfmpy69d4jb3mx5lb9pkk3l3ybr4igsxcqrd3kyr0dn2v")))

(define-public crate-rhymessage-1.3.0 (c (n "rhymessage") (v "1.3.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ysi1lrcyb9h62hry2pxqswcpl1a4h3lkclkl7rnx1l7gmlq3cfp")))

(define-public crate-rhymessage-1.3.1 (c (n "rhymessage") (v "1.3.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y9dlx9pcwin910spmfzmkblipb2dknddabasqjlaahiyjwjk7cy")))

