(define-module (crates-io rh ym rhymuweb) #:use-module (crates-io))

(define-public crate-rhymuweb-1.0.0 (c (n "rhymuweb") (v "1.0.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rhymessage") (r "^1.3") (d #t) (k 0)) (d (n "rhymuri") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ypjgyq4iam6qh0phhb1b9da6q65wbjl82s4i7wncdfkg1zvvpmr")))

(define-public crate-rhymuweb-1.1.0 (c (n "rhymuweb") (v "1.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rhymessage") (r "^1.3") (d #t) (k 0)) (d (n "rhymuri") (r "^1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1frr48vwf2qc1ar4iz4g0lp9hlik52xlkb2cpgs82030pihcy66f")))

(define-public crate-rhymuweb-1.2.0 (c (n "rhymuweb") (v "1.2.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rhymessage") (r "^1.3") (d #t) (k 0)) (d (n "rhymuri") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ja4jz19g70phqz7a4j71575byphz11r75mvn7lx64n0x634jj22")))

(define-public crate-rhymuweb-1.2.1 (c (n "rhymuweb") (v "1.2.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rhymessage") (r "^1.3") (d #t) (k 0)) (d (n "rhymuri") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ald9mvbfl2k1rgk40zc8kqff65f5fv491pbd6l208q70akry7ik")))

