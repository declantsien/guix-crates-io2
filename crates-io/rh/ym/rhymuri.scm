(define-module (crates-io rh ym rhymuri) #:use-module (crates-io))

(define-public crate-rhymuri-1.0.0 (c (n "rhymuri") (v "1.0.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wwk6l9s0wgvh5rv6dfxkxgl7m8dvkjch3w0mci5j7ggqbaiw31a")))

(define-public crate-rhymuri-1.0.1 (c (n "rhymuri") (v "1.0.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1igmdf40sqj4lg5igzw8lx42374v3n7g8k59frmkzz6qs4k7lk0d")))

(define-public crate-rhymuri-1.0.2 (c (n "rhymuri") (v "1.0.2") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19vj45ikkm5l04fk66wap8w0wdss5zxzmx73m47gw7lq36lxj1br")))

(define-public crate-rhymuri-1.1.0 (c (n "rhymuri") (v "1.1.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "082fq2xr6zbhf4s3q29yxs0qn45xsrmj3kpzjfrjmmp8whfzynxd")))

(define-public crate-rhymuri-1.1.1 (c (n "rhymuri") (v "1.1.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "120s5vyy2ra6l5k3lw9dbg1jy66lpgwpi7ziw4889g85ff5iq54s")))

(define-public crate-rhymuri-1.2.0 (c (n "rhymuri") (v "1.2.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1i9xf6gd0ddxh60p78ac1gnxin62zknwfkcfd639y0m3dl3m2i7s")))

(define-public crate-rhymuri-1.3.0 (c (n "rhymuri") (v "1.3.0") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l6rjxhshr8qkr4cdf199rjzj9r9pa9z2jsraq9knd1vxw9hfy40")))

(define-public crate-rhymuri-1.3.1 (c (n "rhymuri") (v "1.3.1") (d (list (d (n "named_tuple") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r2vr6yd0y3f1x593a60c8kz39fdnbnwxakc5z223h6k01zbnd1i")))

