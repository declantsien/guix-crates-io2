(define-module (crates-io rh ym rhymuproc) #:use-module (crates-io))

(define-public crate-rhymuproc-1.0.0 (c (n "rhymuproc") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1gfs6glj16bsshzzb8sz13ap7vahaxxighfvlwb8x5gd6lhnff40")))

(define-public crate-rhymuproc-1.1.0 (c (n "rhymuproc") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0fhjzmbayjgmaaxxd00ahvc7vvimh8qii3kacvfdyipa2fjvlmlx")))

(define-public crate-rhymuproc-1.1.1 (c (n "rhymuproc") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0p7z53v6zr3dab7wcmqg9g2wy6qibacf2h2dyglwfpvds742mjdh")))

(define-public crate-rhymuproc-1.1.2 (c (n "rhymuproc") (v "1.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0f5l7l8mjyfk7drjsfdxcwvxwkcnyk07zff4hxwdzchj6a18y5m4")))

