(define-module (crates-io rh ym rhyme_dictionary) #:use-module (crates-io))

(define-public crate-rhyme_dictionary-0.0.1 (c (n "rhyme_dictionary") (v "0.0.1") (h "0z7k32y558l5ap3nw1l8jqxanp00by701kr09hlwjzw43xdf7843") (y #t)))

(define-public crate-rhyme_dictionary-0.0.2 (c (n "rhyme_dictionary") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0a3kjxr4vjafk9qsncrinwhjbc0r3ad13swk8wymzq1cp1rhnpn5") (y #t)))

(define-public crate-rhyme_dictionary-0.0.3 (c (n "rhyme_dictionary") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1zbfcsgxcr1faxmlvgv5c024lld6s858fbsfys17l95jpmmf9vi5") (y #t)))

