(define-module (crates-io rh ym rhyme) #:use-module (crates-io))

(define-public crate-rhyme-0.1.0 (c (n "rhyme") (v "0.1.0") (d (list (d (n "cmudict") (r "^0.3.2") (d #t) (k 0)))) (h "0pq245a8xv7q316ggvdbh8mafc9530d3bwp41rdzdrrq1h9mqnff")))

(define-public crate-rhyme-0.1.1 (c (n "rhyme") (v "0.1.1") (d (list (d (n "cmudict") (r "^0.3.2") (d #t) (k 0)))) (h "0b6x1azwnd477c2pn8xvhdhzcn9w83qlcasn8ayd9b961665f586")))

