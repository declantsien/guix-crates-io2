(define-module (crates-io rh um rhumblinelib) #:use-module (crates-io))

(define-public crate-rhumblinelib-0.1.0 (c (n "rhumblinelib") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wyb6649b5320pns0xjfwcqblg7qmh009c3077wibr0h7m9a6ydx")))

(define-public crate-rhumblinelib-0.1.1 (c (n "rhumblinelib") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0mzlx0jcb95xilh76w23ax4wynh119wq4fqf4cc46ac2smz24h5s")))

(define-public crate-rhumblinelib-0.1.2 (c (n "rhumblinelib") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yz198mnq6jxd5618fwvd6yyg5380r53067d5583axv5r8rxp44y")))

(define-public crate-rhumblinelib-0.1.3 (c (n "rhumblinelib") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lzb9nqs9n1wcasbnj2bxd2k7rmwqyn7vlwy4wfv02vrvwrw0a7y")))

(define-public crate-rhumblinelib-0.1.4 (c (n "rhumblinelib") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1cd51pfwdbaz1x1s9vznf1cgnw6xpyswb4l98vgfvsxdr4h5qc87")))

(define-public crate-rhumblinelib-0.1.5 (c (n "rhumblinelib") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jgpzkqxlhapary6cphnlqy8sbcm7ffg5pqas8rx8r4r5df7ahkd")))

