(define-module (crates-io rh in rhinopuffin) #:use-module (crates-io))

(define-public crate-rhinopuffin-0.2.1 (c (n "rhinopuffin") (v "0.2.1") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0ip68dify0g7lwsvb5lr95dq78p06f68ydvbxnwdp0pa99bwklf5")))

