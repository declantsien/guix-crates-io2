(define-module (crates-io rh yo rhyoea-common) #:use-module (crates-io))

(define-public crate-rhyoea-common-0.1.1 (c (n "rhyoea-common") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0k1rn6l7b0gpyh4973d89pvi7v3rz7lzn6h7x8p56bmxndyrr0sh") (y #t)))

(define-public crate-rhyoea-common-0.1.2 (c (n "rhyoea-common") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0a7pividcwl7zqhk1ys1ds89c8zrzwfq348aljx4a3lhib4pdzja")))

(define-public crate-rhyoea-common-0.1.3 (c (n "rhyoea-common") (v "0.1.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0g6xlvhgx5qq514ggkaa1jgxyhgsp7wsih271wk279y5j06if6fx")))

(define-public crate-rhyoea-common-0.1.4 (c (n "rhyoea-common") (v "0.1.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0k3yjnzfcsk6w7anynypkrgw9zs8yqi7yw8b9w7vpcx1wg8ib2mz")))

(define-public crate-rhyoea-common-0.1.5 (c (n "rhyoea-common") (v "0.1.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0nydy1cxrgrahhlwnx9lh6md9kcb1vc29vs1z4aj313saynfavyx")))

(define-public crate-rhyoea-common-0.1.6 (c (n "rhyoea-common") (v "0.1.6") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0ff6l5z78nrn456kcc3sx4jdz2g4dr09szkbfxpcra8nvw6ycajb")))

(define-public crate-rhyoea-common-0.1.7 (c (n "rhyoea-common") (v "0.1.7") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "1rgxf0gvvl5h429vzidbrpkz6dwi5cw7bfn8nx5a2rf17fchpzlb")))

(define-public crate-rhyoea-common-0.1.8 (c (n "rhyoea-common") (v "0.1.8") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "1jyp53gzpg6dgq5x4ggwj4l3i5ringg7zim15h9rhizq6lap9n4d")))

(define-public crate-rhyoea-common-0.1.9 (c (n "rhyoea-common") (v "0.1.9") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "1ksl9wzyljjhsg47ja9s74gf3268i2gnwvqvid1g3bpbs1zsbfga")))

(define-public crate-rhyoea-common-0.1.10 (c (n "rhyoea-common") (v "0.1.10") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0mlb0164vsmyw4ld6br8r531l7p471v4pijvl241jb1jf9fk3vcw")))

(define-public crate-rhyoea-common-0.1.11 (c (n "rhyoea-common") (v "0.1.11") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0zwg0jxjmyhx2l1dhkwdq2aspgg050i0g5pwx2q7mjw1f1d5gva7")))

(define-public crate-rhyoea-common-0.1.12 (c (n "rhyoea-common") (v "0.1.12") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0frvbl95gzfa4zp6fad4whzwjj282djbhakx7hcj88ch2cnpb7bv")))

(define-public crate-rhyoea-common-0.1.13 (c (n "rhyoea-common") (v "0.1.13") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "17827qfkwp3mrip3y2x2k302ljdqbzjz86nfy2fp8gpgqzzgnf8g")))

(define-public crate-rhyoea-common-0.1.14 (c (n "rhyoea-common") (v "0.1.14") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0mlxbh4ij93fcjdk9zds29is14xbiwbngm221gn07zndrkla1yd3")))

(define-public crate-rhyoea-common-0.1.15 (c (n "rhyoea-common") (v "0.1.15") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "1fg49qkppdlbqd9bkl3mqlg87bm0w2dbgxlwpjgsa7knn9l5kj9p")))

(define-public crate-rhyoea-common-0.1.16 (c (n "rhyoea-common") (v "0.1.16") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "1livrmxwdvfm2vgw0bw6z13cbmycpyrdfkcdx65jps6hqpdmhlcb")))

(define-public crate-rhyoea-common-0.1.17 (c (n "rhyoea-common") (v "0.1.17") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0vyx9d58mjshybkgj6nfdgi7r75maq1pjd7a5zyh7lsq64z6hpsv")))

(define-public crate-rhyoea-common-0.1.18 (c (n "rhyoea-common") (v "0.1.18") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "15gy8r27jr4qvhs2rxr61hkmvmpaf3nl81hc2dwbd42c70dajaic")))

(define-public crate-rhyoea-common-0.1.19 (c (n "rhyoea-common") (v "0.1.19") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "0grglz4hwgwl3cvfkdsnfjijnz2jjyyk8185sc80di7l597013k1")))

(define-public crate-rhyoea-common-0.1.20 (c (n "rhyoea-common") (v "0.1.20") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)))) (h "05xyysackyyg6n04pn7wl801c9h9496n6j2n3ic7a4w5v3nb6isl")))

