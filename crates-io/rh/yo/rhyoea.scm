(define-module (crates-io rh yo rhyoea) #:use-module (crates-io))

(define-public crate-rhyoea-0.1.0 (c (n "rhyoea") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (d #t) (k 2)) (d (n "colored") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "18i7z9gxd9x0rrv2b46n1kxa1i5a48fwl6b5ygmnld6bi46xi78y") (f (quote (("no-color" "colored/no-color"))))))

