(define-module (crates-io rh us rhusics-transform) #:use-module (crates-io))

(define-public crate-rhusics-transform-0.1.0 (c (n "rhusics-transform") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.15") (d #t) (k 0)))) (h "07ggw6i4h56rzyx0g8q728n5ipkdfl48vhr39xfypprs539p5r3n")))

(define-public crate-rhusics-transform-0.2.0 (c (n "rhusics-transform") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.15") (d #t) (k 0)))) (h "0f0vh1cyfm49aha4x9nph8xfcsh1xvn53b0vahn6dlyaq1nm1mip")))

(define-public crate-rhusics-transform-0.3.0-alpha1 (c (n "rhusics-transform") (v "0.3.0-alpha1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.16") (d #t) (k 0)))) (h "03ck95spdqyin5zr8bjn7gdlcq9wi9k2zblkg6s0j5hl6n664ilb")))

(define-public crate-rhusics-transform-0.3.0 (c (n "rhusics-transform") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.17") (d #t) (k 0)))) (h "0x4p6zis1fqwl535vcki73vl3a2kk8xxi35plwjmgsl4ygx47nz4")))

(define-public crate-rhusics-transform-0.4.0 (c (n "rhusics-transform") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.18") (d #t) (k 0)))) (h "0blpcgjhm657k6xpmbdngvndq6787k1yjw3bah4ncqf736vajxrx")))

