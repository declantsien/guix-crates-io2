(define-module (crates-io rh us rhusics) #:use-module (crates-io))

(define-public crate-rhusics-0.1.0 (c (n "rhusics") (v "0.1.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "collision") (r "^0.12") (d #t) (k 0)) (d (n "shrev") (r "^0.4") (d #t) (k 0)) (d (n "specs") (r "^0.9") (d #t) (k 0)))) (h "146zp3nqa9yp9mg2378s97584kkm8bimkqvaczr8g29b7s53pd9i") (f (quote (("double"))))))

(define-public crate-rhusics-0.2.0 (c (n "rhusics") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "collision") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "shrev") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.10") (o #t) (d #t) (k 0)))) (h "08kdn4w05vlw9ba6hqhlfkvizyyi0zbmmr5nkavv04i845lq265j") (f (quote (("eders" "serde" "cgmath/serde" "collision/eders" "specs/serde") ("ecs" "specs" "shrev"))))))

