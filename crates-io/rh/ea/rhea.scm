(define-module (crates-io rh ea rhea) #:use-module (crates-io))

(define-public crate-rhea-0.1.0 (c (n "rhea") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11flprr0z8jfv93rfhvxm5q9hy90xbdagxxv2x015vqqjs43lnnn")))

(define-public crate-rhea-0.1.1 (c (n "rhea") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgj96vdbzwhkdd6zhbai694cswbjfwbwca50llxacwnx7d3nxdq")))

