(define-module (crates-io rh tm rhtml) #:use-module (crates-io))

(define-public crate-rhtml-0.1.0 (c (n "rhtml") (v "0.1.0") (h "0hdj2if53xj8vfgsik2x9bgck7cndrbzbmsb2j292q1myb2d15jj")))

(define-public crate-rhtml-0.1.1 (c (n "rhtml") (v "0.1.1") (h "0p9claddf6a111s1whnghdxmd9picqksd49kblb94hcwkgr37qgj")))

(define-public crate-rhtml-0.1.2 (c (n "rhtml") (v "0.1.2") (h "12d3ci2n989h03714vk4z75aqzz4chvmq3brpbg7y1zhnk8d0cqk")))

(define-public crate-rhtml-0.1.3 (c (n "rhtml") (v "0.1.3") (h "1d6cf90k6kyd7rr4rlv3cp1m1q1hw6mfx556rvzf61c1q7pzhd1m")))

(define-public crate-rhtml-0.1.4 (c (n "rhtml") (v "0.1.4") (h "1x18k8xj1qqx1y3swwkc513aa767q5yz2mkagl8n1gc34m14fg5p")))

(define-public crate-rhtml-0.1.5 (c (n "rhtml") (v "0.1.5") (h "12p6kwhvjm1qhvj7lmh1zzp30nlv088dig47fpcgz9kgyfg378yg")))

