(define-module (crates-io rh dx rhdxmr-publish-test3) #:use-module (crates-io))

(define-public crate-rhdxmr-publish-test3-0.1.0 (c (n "rhdxmr-publish-test3") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "zero") (r "^0.1") (d #t) (k 0)))) (h "0qkav3d5zvfnc5wdnnbzqc6gk4p0w2lp45jvswk78a3jivxkqwrg") (l "bpf")))

