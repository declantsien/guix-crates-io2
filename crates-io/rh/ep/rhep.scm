(define-module (crates-io rh ep rhep) #:use-module (crates-io))

(define-public crate-rhep-0.1.0 (c (n "rhep") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)))) (h "1q8mcx0ww3sp4x44m2ncsak1dmrkpzgn9m9il8c21kljrqaw7xbi")))

