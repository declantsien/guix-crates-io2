(define-module (crates-io rh ex rhexdump) #:use-module (crates-io))

(define-public crate-rhexdump-0.1.0 (c (n "rhexdump") (v "0.1.0") (h "0467kqh1hya60x97880bxwqrirm9ahyl021vxqpfvr59q8vakkjm") (y #t)))

(define-public crate-rhexdump-0.1.1 (c (n "rhexdump") (v "0.1.1") (h "0iwqawqszy3apj4ci1z0l068i2vwk4x33h6i4jgy6da9axjazsf5")))

(define-public crate-rhexdump-0.2.0 (c (n "rhexdump") (v "0.2.0") (h "1isihflwds9s1wxf0s46adxf319vbl8kkwzyfwviw8nxlghwp4ls")))

