(define-module (crates-io rh ai rhai-url) #:use-module (crates-io))

(define-public crate-rhai-url-0.0.1 (c (n "rhai-url") (v "0.0.1") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "url") (r ">=2.0") (d #t) (k 0)))) (h "171h0gdplv59k7g3qcz7q6b6kk1bmjl5id6nzzl63bmydx43hg07")))

(define-public crate-rhai-url-0.0.2 (c (n "rhai-url") (v "0.0.2") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "url") (r ">=2.0") (d #t) (k 0)) (d (n "url") (r ">=2.0") (d #t) (k 1)))) (h "0ni29ivvri1pbgivc4slqk99sxja4cr42qq7vl6zp0l5hvkqnhkl") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default"))))))

(define-public crate-rhai-url-0.0.3 (c (n "rhai-url") (v "0.0.3") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "url") (r ">=2.0") (d #t) (k 0)) (d (n "url") (r ">=2.0") (d #t) (k 1)))) (h "00ddgfxlc66sch5n2sfv6j092ln68m06yzjgflzj6cfrrjbgg336") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default"))))))

(define-public crate-rhai-url-0.0.4 (c (n "rhai-url") (v "0.0.4") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "url") (r ">=2.0") (d #t) (k 0)) (d (n "url") (r ">=2.0") (d #t) (k 1)))) (h "1scc89wlkf39qwhahbcrmg06br12scpizi6r8kci46lwlk7nlqdc") (f (quote (("metadata" "rhai/metadata") ("default" "array") ("array"))))))

(define-public crate-rhai-url-0.0.5 (c (n "rhai-url") (v "0.0.5") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "url") (r ">=2.0") (d #t) (k 0)) (d (n "url") (r ">=2.0") (d #t) (k 1)))) (h "0r3r8iwaq6fayw781vw88xw8nhwnnqfnjivyl230hhamcg04ipp2") (f (quote (("metadata" "rhai/metadata") ("default" "array") ("array"))))))

