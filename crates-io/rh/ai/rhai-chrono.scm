(define-module (crates-io rh ai rhai-chrono) #:use-module (crates-io))

(define-public crate-rhai-chrono-0.1.0 (c (n "rhai-chrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (f (quote ("serde" "unstable-locales"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9") (d #t) (k 0)) (d (n "rhai") (r "^1.15") (d #t) (k 0)))) (h "1rpbx2crfj8wfq8hsrzzjwgk9g03hgrkhkn3daqlwr37xmsmbzg9") (f (quote (("sync" "rhai/sync") ("default"))))))

