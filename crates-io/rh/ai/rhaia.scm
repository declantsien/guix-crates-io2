(define-module (crates-io rh ai rhaia) #:use-module (crates-io))

(define-public crate-rhaia-0.1.0 (c (n "rhaia") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "fspp") (r "^1.1.0") (d #t) (k 0)) (d (n "piglog") (r "^1.3.1") (d #t) (k 0)) (d (n "rhai") (r "^1.16.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1wv62a9jw4vcs85i9rz154rbfxfiw5vcrpq211flm3aa75f2sacn")))

(define-public crate-rhaia-0.1.1 (c (n "rhaia") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "fspp") (r "^1.1.0") (d #t) (k 0)) (d (n "piglog") (r "^1.3.1") (d #t) (k 0)) (d (n "rhai") (r "^1.16.3") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1xmrm0zw27d7k88rd2ib2xjym919cab0za3kcg8kl3ja4xm69ga9")))

