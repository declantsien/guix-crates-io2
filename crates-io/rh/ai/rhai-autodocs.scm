(define-module (crates-io rh ai rhai-autodocs) #:use-module (crates-io))

(define-public crate-rhai-autodocs-0.1.0 (c (n "rhai-autodocs") (v "0.1.0") (d (list (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1d7iss620wj1441gyf77dpxas37irijf6vv0rqd1ryfzdwjzlb1h")))

(define-public crate-rhai-autodocs-0.1.1 (c (n "rhai-autodocs") (v "0.1.1") (d (list (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1w3m0x6psli0a8g64spqrldng0yxxi14li41ll9ar4ad6wm8b8mq")))

(define-public crate-rhai-autodocs-0.1.2 (c (n "rhai-autodocs") (v "0.1.2") (d (list (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0zc77j1d5gp4bkfx49hqrzypkych5jbqis0plpcwmrw72vnz94fs")))

(define-public crate-rhai-autodocs-0.1.3 (c (n "rhai-autodocs") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1850dz5ad0srkig8zzgbj79zjkzs95wka85j4nvzl8vqm1j5rich")))

(define-public crate-rhai-autodocs-0.1.4 (c (n "rhai-autodocs") (v "0.1.4") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0alip4iwdcq732kpvil424gi1i2v4h8gif315zvyfy7bz7nhdz5i")))

(define-public crate-rhai-autodocs-0.1.5 (c (n "rhai-autodocs") (v "0.1.5") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1d9bspyzib05l1lh6247dnxb3b33yskgjyw2xa6fkm9d31zsy2sa")))

(define-public crate-rhai-autodocs-0.1.6 (c (n "rhai-autodocs") (v "0.1.6") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.11.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "006pq9rgzx28230qk53sj3a0pmds2bgqdrn44x56hc8164c2a548")))

(define-public crate-rhai-autodocs-0.1.7 (c (n "rhai-autodocs") (v "0.1.7") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.14.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "083gg18hjqdknz00xxa100740jjrghjfzbx9nlwshgcy6lv2fkvp")))

(define-public crate-rhai-autodocs-0.2.0 (c (n "rhai-autodocs") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.15.1") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0bv157kylqcqqddj1dm82ggn9fs3c3g72kqxxvvarmhyqcxsfizk")))

(define-public crate-rhai-autodocs-0.2.1 (c (n "rhai-autodocs") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rhai") (r "^1.15.1") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1fniz56p2a7ynidahyqhdbmn9vlgc8g4qkkkrx8hrpzdfxdkzzpy")))

(define-public crate-rhai-autodocs-0.2.2 (c (n "rhai-autodocs") (v "0.2.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.16.1") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0ivsrdr9y1kd4b2zqyf5h8n8q5q8as5h1vpj0lcakhj8lrvpw1j8")))

(define-public crate-rhai-autodocs-0.2.3 (c (n "rhai-autodocs") (v "0.2.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.16.2") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "16pz0bsjas1ql91mhk92lkl2w4jnn0ln90sdj24jnzzqmc6vfmq8")))

(define-public crate-rhai-autodocs-0.3.0 (c (n "rhai-autodocs") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.16.2") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "01b3d5lsb5zdig5ngmqf3mv7lvk8ncaiddw04w05girhlb8vb3dd")))

(define-public crate-rhai-autodocs-0.4.0 (c (n "rhai-autodocs") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.16.2") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1akm8vrcgid4qmvykszavyxph93wibljj3anrgybgdq0hmd7hc2m")))

(define-public crate-rhai-autodocs-0.4.1 (c (n "rhai-autodocs") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.16.3") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "09jai0i3a37laf5yg1f80kdzhjbarp8iw5y11h1bpmmr59iv69ik")))

(define-public crate-rhai-autodocs-0.4.2 (c (n "rhai-autodocs") (v "0.4.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.17.0") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0d09rl4bhx20918l80gvk7zhay01f7wqsdkn44nqq8x6v3z7fmav")))

(define-public crate-rhai-autodocs-0.4.3 (c (n "rhai-autodocs") (v "0.4.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.17.1") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "01021s13w19nqixws6hzxlssm52qwhc7d16dglf8dvk5bvl6x78v")))

(define-public crate-rhai-autodocs-0.5.0 (c (n "rhai-autodocs") (v "0.5.0") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "14chjpjdk2w2l5blq1fmx8wjmbbbiv7flvxkivgk670ri4jrirhy")))

(define-public crate-rhai-autodocs-0.5.1 (c (n "rhai-autodocs") (v "0.5.1") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1x6xg6idwnmrc19ky9dlyk7hb573yqflgpmidvwa3s3l3qk3q6q2")))

(define-public crate-rhai-autodocs-0.5.2 (c (n "rhai-autodocs") (v "0.5.2") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "06hfsskrnrf8bwgg7l25slrpyk2b2l1qhicqyfnhfz49pbm85xv3")))

(define-public crate-rhai-autodocs-0.5.3 (c (n "rhai-autodocs") (v "0.5.3") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0ymycqiwwpvqv45ays7m36xvs3595w2vrsyhr1jy8vmja98fkw56")))

(define-public crate-rhai-autodocs-0.5.4 (c (n "rhai-autodocs") (v "0.5.4") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1bxm2dlrrmhwl8ppjln8gj8j1xyl47da8msy9khhw67nh6kj51fk")))

(define-public crate-rhai-autodocs-0.6.0 (c (n "rhai-autodocs") (v "0.6.0") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1h74dbmryv0hnrcpkqd0kzydj3xsg9v0bczgxgccxs53cqg2zl3q")))

(define-public crate-rhai-autodocs-0.6.1 (c (n "rhai-autodocs") (v "0.6.1") (d (list (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rhai") (r "^1.18") (f (quote ("metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0dg3vz6fkj8a03dyzq0jhr1j31mw0hyfg7knddah7a6lf32s4l8d")))

