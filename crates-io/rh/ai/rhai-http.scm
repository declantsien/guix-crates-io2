(define-module (crates-io rh ai rhai-http) #:use-module (crates-io))

(define-public crate-rhai-http-0.1.0 (c (n "rhai-http") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "rhai") (r "^1.18") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mm2mnj14p2nw31kp4l9d0jr886k2imf238gx2z7azl3g1piwb36")))

(define-public crate-rhai-http-0.1.1 (c (n "rhai-http") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "rhai") (r "^1.18") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1qix1a1923nbq5q0a88ld1xfhcl3hp33vcdmzhpjpspsd07p2vhi")))

