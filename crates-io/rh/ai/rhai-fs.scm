(define-module (crates-io rh ai rhai-fs) #:use-module (crates-io))

(define-public crate-rhai-fs-0.0.1 (c (n "rhai-fs") (v "0.0.1") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jyfq82x1v1wgg5jpdj9rxlv9ymw5l7l4gyssypgikf6f7p4ijhr") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default")))) (y #t)))

(define-public crate-rhai-fs-0.1.0 (c (n "rhai-fs") (v "0.1.0") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1qd08pwsza2wzhmvppkszvs94q0dz2rf1a0fk5nxm9jhkab2pj1p") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default"))))))

(define-public crate-rhai-fs-0.1.1 (c (n "rhai-fs") (v "0.1.1") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1b9npa8762qr5ybai8dkv9m4bsc36lgkgh36hvywmh1mx3yz0fl6") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default"))))))

(define-public crate-rhai-fs-0.1.2 (c (n "rhai-fs") (v "0.1.2") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.11") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "19h79gnzbb1mycq3127fn1j1psshxws2lm86ghfbf9b0j8nv9nr5") (f (quote (("no_index") ("metadata" "rhai/metadata") ("default"))))))

(define-public crate-rhai-fs-0.1.3 (c (n "rhai-fs") (v "0.1.3") (d (list (d (n "rhai") (r ">=1.9") (d #t) (k 0)) (d (n "rhai") (r ">=1.11") (d #t) (k 1)) (d (n "serde") (r "^1.0.140") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05pzqv8y3d00pczpr0r86ng5dg9x1d7kwfs74hwh753igmqzgwws") (f (quote (("sync" "rhai/sync") ("no_index") ("metadata" "rhai/metadata") ("default"))))))

