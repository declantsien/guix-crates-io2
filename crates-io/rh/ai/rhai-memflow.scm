(define-module (crates-io rh ai rhai-memflow) #:use-module (crates-io))

(define-public crate-rhai-memflow-0.1.0 (c (n "rhai-memflow") (v "0.1.0") (d (list (d (n "cglue") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins" "dummy_mem"))) (d #t) (k 0)) (d (n "rhai") (r "^1.9") (f (quote ("internals"))) (d #t) (k 0)))) (h "0bfh35hfc6z6nq18n0n4n96y1rfwkbxp9zbspbqq7l3bz0zcfvrp")))

(define-public crate-rhai-memflow-0.1.1 (c (n "rhai-memflow") (v "0.1.1") (d (list (d (n "cglue") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins" "dummy_mem"))) (d #t) (k 0)) (d (n "rhai") (r "^1.9") (f (quote ("internals"))) (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "1fc404ka271g98h587vwlb553mrdc4fx4gspv3acp6rkygrmgs6z")))

(define-public crate-rhai-memflow-0.1.2 (c (n "rhai-memflow") (v "0.1.2") (d (list (d (n "cglue") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2") (d #t) (k 2)) (d (n "memflow") (r "^0.2.0-beta") (f (quote ("plugins" "dummy_mem"))) (d #t) (k 0)) (d (n "rhai") (r "^1.9") (f (quote ("internals"))) (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "0yd997cngdsxqaagxa6galbz2z0nxkgibkc7nhy0zfk023frysyc")))

