(define-module (crates-io rh ai rhai-dylib) #:use-module (crates-io))

(define-public crate-rhai-dylib-0.1.0 (c (n "rhai-dylib") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "rhai") (r "^1.10.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "12kmh3dia5gl20igq0pl03rd7l2jl7nwb8x7pwcc0vh2jk65xvnk") (r "1.64.0")))

(define-public crate-rhai-dylib-0.1.1 (c (n "rhai-dylib") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.10.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "025g4109n7s0m725ymcc47kkva732474dfjq8x5ciqfcd5ym19ms") (f (quote (("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.64.0")))

(define-public crate-rhai-dylib-0.1.2 (c (n "rhai-dylib") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.10.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "06rlq0hypy2f9sw5i63abnhjkrd4sjj238njgkcvx9ys296vc9xg") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.64.0")))

(define-public crate-rhai-dylib-0.1.3 (c (n "rhai-dylib") (v "0.1.3") (d (list (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.11.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "0i7q1b5ib5pgfbcj6qgv3kf4hy7lx9js7i4lx1b55dlqysq2v1lq") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.64.0")))

(define-public crate-rhai-dylib-0.1.4 (c (n "rhai-dylib") (v "0.1.4") (d (list (d (n "libloading") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.11.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "07bkq9lc085vncmvxyq2jzmck05qshlkqd9qdglb5h4w1v24x490") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.61.0")))

(define-public crate-rhai-dylib-0.1.5 (c (n "rhai-dylib") (v "0.1.5") (d (list (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "1yh180a305l3lrdcd9f8vfi938h3y6rjzrnl6lm2jlqjidyq28z4") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.61.0")))

(define-public crate-rhai-dylib-0.1.6 (c (n "rhai-dylib") (v "0.1.6") (d (list (d (n "libloading") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.12.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "0ks13w3hr13jrj1rxfk8zqw7lgi3lw29jzr1w6j3ljdkjn9fc8jx") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading")))) (r "1.61.0")))

(define-public crate-rhai-dylib-0.1.7 (c (n "rhai-dylib") (v "0.1.7") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rhai") (r "=1.13.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "1rdmp3w7srw2hkhy3b8xfklx4nhqcr6dz6s321g20y8x0fkhqg12") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.1.8 (c (n "rhai-dylib") (v "0.1.8") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rhai") (r "=1.14.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "06cja229qapad709j6cf552zbmc94z574issx03nd27l5470lj8h") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.1.9 (c (n "rhai-dylib") (v "0.1.9") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rhai") (r "=1.15.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "0xk18y7zdqg150sjamwd01ciw27hv0xd4pl5dhkd84w268kg21px") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.1.10 (c (n "rhai-dylib") (v "0.1.10") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rhai") (r "=1.16.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "0584l7mcrcxs6dzm0aqbzvi228z1alcszivgxhjm33128rf8ln4h") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.1.11 (c (n "rhai-dylib") (v "0.1.11") (d (list (d (n "libloading") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.16.2") (f (quote ("internals"))) (d #t) (k 0)))) (h "1nrs27fvd8k8abdnc3l27c0hx3a4q61m1ga9mk41ydcar1x623pz") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.1.12 (c (n "rhai-dylib") (v "0.1.12") (d (list (d (n "libloading") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.16.3") (f (quote ("internals"))) (d #t) (k 0)))) (h "1i8kpz984ga8n1j3123f7dgz8ih1hp0v7xwa87wsqk7f9ahbd5a8") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.2.0 (c (n "rhai-dylib") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.17.0") (f (quote ("internals"))) (d #t) (k 0)))) (h "1xhjcwj5n2anyzrswfr8mp045ymn676n2gcwwm4bg7dbfgq3yjdj") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.2.1 (c (n "rhai-dylib") (v "0.2.1") (d (list (d (n "libloading") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.17.1") (f (quote ("internals"))) (d #t) (k 0)))) (h "0h9b6p1gy40flnpll0sypcs8aw81v51maxhwyyfxvzk0h74kz3l0") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

(define-public crate-rhai-dylib-0.3.0 (c (n "rhai-dylib") (v "0.3.0") (d (list (d (n "libloading") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.18") (f (quote ("internals"))) (d #t) (k 0)))) (h "1jxbi81hcrifpjq76hjzjx8k8xnnb8whcl5rmam6gsrn4bkkbk5b") (f (quote (("sync" "rhai/sync") ("default" "libloading")))) (s 2) (e (quote (("libloading" "dep:libloading"))))))

