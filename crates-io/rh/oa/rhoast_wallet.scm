(define-module (crates-io rh oa rhoast_wallet) #:use-module (crates-io))

(define-public crate-rhoast_wallet-0.1.0 (c (n "rhoast_wallet") (v "0.1.0") (d (list (d (n "rhoast_client") (r "^0.1.0") (d #t) (k 0)) (d (n "rhoast_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yd0vzv7b0pw7zpwn6r62vgm14ihmr7dgbakc62y0s7xi7g2smyf")))

