(define-module (crates-io rh oa rhoast_client_v03) #:use-module (crates-io))

(define-public crate-rhoast_client_v03-0.1.0 (c (n "rhoast_client_v03") (v "0.1.0") (d (list (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "13bnspx07f8m33sqas1h3pr6n8v81izvg9pxdfy29a79z31c607c")))

