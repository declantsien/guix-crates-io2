(define-module (crates-io rh oo rhook) #:use-module (crates-io))

(define-public crate-rhook-0.1.0 (c (n "rhook") (v "0.1.0") (h "1j1lr7h710zijqjar9ddq3p9q3v4b0xd92z08mlfxhzqgqhf8ml5")))

(define-public crate-rhook-0.2.0 (c (n "rhook") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "0pclwiilx4fx2gzn6gw5rrg9arfcji5bjv4v2gqvwsiygzj6jfb4")))

(define-public crate-rhook-0.2.1 (c (n "rhook") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "157cghzh3pqfk0i6167zlxzfx334ghhzxi4i0vvrv6csmfvfdcpc")))

(define-public crate-rhook-0.2.2 (c (n "rhook") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "15p6bwh1jgimxbf6px2rli77bnz2xpzaiv820j8vv7kakynq1vz3")))

(define-public crate-rhook-0.3.0 (c (n "rhook") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "13aw88aib3wvqj8n26hr785l26l2s5flig8awvv10zgipzp6gd1s")))

(define-public crate-rhook-0.4.0 (c (n "rhook") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "15r07m30nyrggwc34nqpg31s2k8y9lyka86nm84yg0fcd43r7byk")))

(define-public crate-rhook-0.5.0 (c (n "rhook") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "01s76qd92252sfhig2w2nbhrrwcp21s41wwhymhy2dw21nmzzgxx")))

(define-public crate-rhook-0.8.0 (c (n "rhook") (v "0.8.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)))) (h "171k4n0lld5ny3l2ihb74pfdmv1r5bmv58md10323a83zriwd207")))

