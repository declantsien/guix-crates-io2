(define-module (crates-io a2 vc a2vconverter) #:use-module (crates-io))

(define-public crate-A2VConverter-0.1.0 (c (n "A2VConverter") (v "0.1.0") (h "0ppm3lyspjyhpp64vgbcxz090f560ld5q5in5i5x0isq8mhs4jvr")))

(define-public crate-A2VConverter-0.1.1 (c (n "A2VConverter") (v "0.1.1") (h "1xmapi67h556n0n2y0gb6rg0qmfks6bh7m4ddiw314y1abi1h92c")))

