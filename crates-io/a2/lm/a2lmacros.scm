(define-module (crates-io a2 lm a2lmacros) #:use-module (crates-io))

(define-public crate-a2lmacros-0.8.0 (c (n "a2lmacros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1jc6hdsp6shz47hlawr9ag65hqrvgbyq6awi1gdw3x004l4gvsj7")))

(define-public crate-a2lmacros-0.8.1 (c (n "a2lmacros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1w2f354ld6q8npb8gxp8786l8d971qdglcpx1q5p1zmr28n0wl3s")))

(define-public crate-a2lmacros-0.8.2 (c (n "a2lmacros") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1iaqkdcfxfkl9bx0sy1a1dp9by0m82jc96pjfrkkq7n84h58gwhv")))

(define-public crate-a2lmacros-1.0.0 (c (n "a2lmacros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "04gq30m4wj704w17yrjj6m95bgf9f2cciziz1d45arf4lws00vs9")))

(define-public crate-a2lmacros-1.0.1 (c (n "a2lmacros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "11qxnbb06v9z440ahjpwp7asn32vrr8ijnm8zm86dshn16iwzbg4")))

(define-public crate-a2lmacros-1.1.0 (c (n "a2lmacros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0cdw38cr79qnxnvl2mhjh4x2h4qrpz9mbv22wski2fcs101q8dcq")))

(define-public crate-a2lmacros-1.1.1 (c (n "a2lmacros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1yr5m0rxhfz0q6gjplchxnk24x8dclgnrqcgvcznd4jfmn795fqv")))

(define-public crate-a2lmacros-1.1.2 (c (n "a2lmacros") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "16dr58a6wyg0113dmx6rwjlsd5z0w77n958gvpgnc8b927ybl6wl")))

(define-public crate-a2lmacros-1.2.0 (c (n "a2lmacros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1isw49wq1zlylw43m6jgphy44219vcyxs6v155bkyjwx9zf9wax3")))

(define-public crate-a2lmacros-1.3.0 (c (n "a2lmacros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1fh5nyah1g9i6rxqchj3a59araj1s3m7infl79yfm2v5pkbydzqa")))

(define-public crate-a2lmacros-2.0.0 (c (n "a2lmacros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1nxma28n75s75cga4rs0qxnjl8kzxz6izmcs9g1c6l14gc6fvvfd")))

