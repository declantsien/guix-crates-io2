(define-module (crates-io i8 #{08}# i8080) #:use-module (crates-io))

(define-public crate-i8080-0.1.0 (c (n "i8080") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "ears") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.15") (k 0)) (d (n "piston_window") (r "^0.70") (d #t) (k 0)))) (h "0mvqq5yhkrk3i49yrz41bkjhj5i7binhcf8g6qk7mq3hmxxqn1z3") (f (quote (("cpudiag"))))))

