(define-module (crates-io tv is tvis) #:use-module (crates-io))

(define-public crate-tvis-0.1.0 (c (n "tvis") (v "0.1.0") (h "0izvg3548vp5rji44wz4lvfib7h2v6ah8sqvjg92gbcgahfqsyrl")))

(define-public crate-tvis-0.1.1 (c (n "tvis") (v "0.1.1") (h "05dsppqpp3y4by1qm89az4v56hw8g6kh7larswsqrmja23k5786d")))

(define-public crate-tvis-0.2.0 (c (n "tvis") (v "0.2.0") (h "1bgnizk6gri0vyss4mchj5ww877q6qbw3fa3r1vanr3ywika76vn")))

(define-public crate-tvis-0.3.0 (c (n "tvis") (v "0.3.0") (h "1cz4a7kwj0zs0qylijk2a97kz1sdbdkv42nivmvwj3cz7hz8wqcg")))

(define-public crate-tvis-0.3.1 (c (n "tvis") (v "0.3.1") (h "0bx7rx5xhcal063x56qiffyvy96zkncn0g6nq68f3mxdnn9gmxsj")))

(define-public crate-tvis-0.5.0 (c (n "tvis") (v "0.5.0") (h "0b36lbhxgxh245kg2s2xsb77zvkmvraqnjp52lgg6d3jrzab40cy")))

(define-public crate-tvis-0.5.1 (c (n "tvis") (v "0.5.1") (h "0asdinz56l01qmclbx8p5h87clcrsgzxi8y09ssi43qwwnyzfwm0")))

(define-public crate-tvis-0.6.0 (c (n "tvis") (v "0.6.0") (h "13v0n9ibn7acl1zhlxvnmx0q2g8hrd5nf5d0ka33bhx3p93qkkda")))

(define-public crate-tvis-0.7.0 (c (n "tvis") (v "0.7.0") (h "1sg5nqn86611jda7zq8g7rpkqhzv1x7nvalhbqzsl9raficffrzd")))

(define-public crate-tvis-0.10.0 (c (n "tvis") (v "0.10.0") (h "09g9nd3ykl7qw979p8bf301ingpprfyah1p7wdkdygl1zpx2ydmk")))

(define-public crate-tvis-0.10.1 (c (n "tvis") (v "0.10.1") (d (list (d (n "tinf") (r "^0.10.0") (d #t) (k 0)))) (h "1h6fi2n2d3i10zd1wm7nlhwz6wfmksa5kmi00xamawchg869y873")))

(define-public crate-tvis-0.11.0 (c (n "tvis") (v "0.11.0") (d (list (d (n "tinf") (r "^0") (d #t) (k 0)))) (h "1d3p217mwrsc30jxj96g14rgkh80zal80icgw8y6mn7jqbi4wja7")))

(define-public crate-tvis-0.12.0 (c (n "tvis") (v "0.12.0") (d (list (d (n "tinf") (r "^0") (d #t) (k 0)))) (h "0ijppm3plfr7yfhfxb6i392cjyfg6ix7ci5326xq6plalhlm9j89")))

(define-public crate-tvis-0.14.1 (c (n "tvis") (v "0.14.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "15ixqqyb5710cf465dkvnzfy5wjh7szy1sp8dn6dmrd8s7y3hj28")))

(define-public crate-tvis-0.15.0 (c (n "tvis") (v "0.15.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0ynz6ms8v1dd5qahn2j4aiilxkk4xnc03f3k3xq66m5ck4a6pvl9")))

(define-public crate-tvis-0.15.2 (c (n "tvis") (v "0.15.2") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0aw7mf5ir8m85jakpl5zly5b1v9qv1kcam0arsxqy5307yky6p2w")))

(define-public crate-tvis-0.15.3 (c (n "tvis") (v "0.15.3") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "dlx") (r "^0.1.0") (d #t) (k 2)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 2)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "tinf") (r "^0") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tvis_util") (r "^0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0amcjql6iyqkvzhlj26ryhxghagzv00k66gnsslldmil6pzg8h3y")))

