(define-module (crates-io tv is tvis_util) #:use-module (crates-io))

(define-public crate-tvis_util-0.1.0 (c (n "tvis_util") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0g9506b9jkf4yxkp9px274xmaydbgxjij54cf6f1mcgxnk0rz9z0")))

(define-public crate-tvis_util-0.2.0 (c (n "tvis_util") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "05d8pg3591d5rvvv36w8rm7c23wgai1n1wk4g4cvk8g41flz3g1k")))

(define-public crate-tvis_util-0.3.0 (c (n "tvis_util") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "12g09v21dj3iad7kmldzzhnv7lljaz3n4258fjxp6f2ainnpz5h3")))

(define-public crate-tvis_util-0.3.1 (c (n "tvis_util") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0yvyh45a972dx2wmx5ykmhv3y5lvvz56gfacw1s8clagr6vpcj4q")))

(define-public crate-tvis_util-0.3.2 (c (n "tvis_util") (v "0.3.2") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0ab2w0vb8hyqw279a0qpbv7yfx8xzg6hnfnv0hxkwah5zq34vp9b")))

(define-public crate-tvis_util-0.4.0 (c (n "tvis_util") (v "0.4.0") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "19k5jss5zi4jkfwvcrl1dd1rsmwk0vdfbkczvahr9vn0sbq386jy")))

(define-public crate-tvis_util-0.5.0 (c (n "tvis_util") (v "0.5.0") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0zdk4vggpjr3syrhqcmlzc7x2mhr7b6h52kx7j64b3x72169098h")))

(define-public crate-tvis_util-0.5.2 (c (n "tvis_util") (v "0.5.2") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.32") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "0kfsfg4f7mm40my4xb2rmx7yh1cs0wd8s3kiyx48nvrf0rdmvlkj")))

