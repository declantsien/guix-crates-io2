(define-module (crates-io tv _l tv_lib) #:use-module (crates-io))

(define-public crate-tv_lib-0.1.1 (c (n "tv_lib") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "calm_io") (r "^0.1.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)))) (h "15rpy5sczrx5iz7nssnpnaz481czx7z81vla3vvq9rl0f66bhx6k") (y #t)))

(define-public crate-tv_lib-0.1.2 (c (n "tv_lib") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "calm_io") (r "^0.1.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.23.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "owo-colors") (r "^3.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unicode-truncate") (r "^0.2.0") (d #t) (k 0)))) (h "19vwfjwjm4rf4jrjqhml8kw79iy2bnkvlfmh74g5j1l5rhx8d29z")))

