(define-module (crates-io tv -o tv-organizer) #:use-module (crates-io))

(define-public crate-tv-organizer-0.1.1 (c (n "tv-organizer") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tvdb") (r "^0.6") (d #t) (k 0)))) (h "0iqzp01brmvfkdrzcp68h8f385n6gjdjpb18b4l5p9577mbigpfg")))

