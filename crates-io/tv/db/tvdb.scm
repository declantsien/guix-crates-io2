(define-module (crates-io tv db tvdb) #:use-module (crates-io))

(define-public crate-tvdb-0.1.0 (c (n "tvdb") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "0px3zlwfjzdh23lxd88ikv5nhngm03b20j1vhqlbvl0vxj9wmm5l")))

(define-public crate-tvdb-0.2.0 (c (n "tvdb") (v "0.2.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)) (d (n "xmltree") (r "^0.2") (d #t) (k 0)))) (h "01rdxdd6c9z2hy9clsddq5jaz7dkwkd52lwiscbiy6fvxdysld8v")))

(define-public crate-tvdb-0.3.1 (c (n "tvdb") (v "0.3.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.3") (d #t) (k 0)))) (h "0pnyim4zk7al9bhflrihhpggk32rpg7w4dyc1lpnj1sxn4p4za7w")))

(define-public crate-tvdb-0.4.0 (c (n "tvdb") (v "0.4.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "url") (r "^1.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.3") (d #t) (k 0)))) (h "1bmkf64w69igwgv074jhhxyi656dyfw2y21bwa3vhwhdh93dssxn")))

(define-public crate-tvdb-0.5.0 (c (n "tvdb") (v "0.5.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "08db7841661sw00c3mvsxdlp5xwj5j5jlxc3g57hfhc7z0bzz9i2")))

(define-public crate-tvdb-0.5.1 (c (n "tvdb") (v "0.5.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "0mvxa78bnhgi47zww64bhf9abalqf9c6vzfg3d0kdlnra58w8jsf")))

(define-public crate-tvdb-0.6.0 (c (n "tvdb") (v "0.6.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "05p3d71jx5vk1kbfnx2adpf5rjlpsg21b7jg7qabzz3hynycjpvj")))

