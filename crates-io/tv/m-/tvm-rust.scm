(define-module (crates-io tv m- tvm-rust) #:use-module (crates-io))

(define-public crate-tvm-rust-0.0.1 (c (n "tvm-rust") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0.206") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tvm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1s5l6alsqn4fplr3sxq5xib4237hmmyr1jm9mxzfh33jilpkmsgv") (y #t)))

(define-public crate-tvm-rust-0.0.2 (c (n "tvm-rust") (v "0.0.2") (d (list (d (n "clippy") (r "^0.0.206") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tvm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1zjrdw1dcxqd4wkw0r6sdv88qrklk60brx4w2qyrjw2a5wzlayjm") (y #t)))

