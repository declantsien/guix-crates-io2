(define-module (crates-io tv m- tvm-macros) #:use-module (crates-io))

(define-public crate-tvm-macros-0.1.1-alpha (c (n "tvm-macros") (v "0.1.1-alpha") (d (list (d (n "goblin") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0z72ljpjv19c2qfmhrqsvlq6g9wq3x2g9wq581p27xq6dbzvasry")))

