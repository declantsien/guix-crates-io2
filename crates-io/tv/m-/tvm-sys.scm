(define-module (crates-io tv m- tvm-sys) #:use-module (crates-io))

(define-public crate-tvm-sys-0.1.0 (c (n "tvm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ddx9xgra0bvypd16n1ls06ivdn7sx10s015mczspgmfvjbxc5cw") (f (quote (("nightly"))))))

(define-public crate-tvm-sys-0.1.1-alpha (c (n "tvm-sys") (v "0.1.1-alpha") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "bindgen") (r "^0.57") (k 1)) (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tvm-build") (r "^0.1") (d #t) (k 1)))) (h "0ffxjs7wsr48csbq58yx655qrw7hknhv1gnh4xqq7xmags7bj53c") (f (quote (("static-linking") ("dynamic-linking") ("default" "dynamic-linking"))))))

