(define-module (crates-io tv he tvheadproxy) #:use-module (crates-io))

(define-public crate-tvheadproxy-0.1.0 (c (n "tvheadproxy") (v "0.1.0") (d (list (d (n "handlebars") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "= 0.9.17") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)) (d (n "warp") (r "^0.1") (d #t) (k 0)))) (h "19gzmk37gd7zcn1wfp620w0ry3sawkbnbd8cw4bkjk293z9mjp0v")))

