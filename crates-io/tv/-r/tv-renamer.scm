(define-module (crates-io tv -r tv-renamer) #:use-module (crates-io))

(define-public crate-tv-renamer-0.2.0 (c (n "tv-renamer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_16"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "0g262pd8p385ydw2s5fn8d9f01m60rf9z82kq5yi8h8vcyxw5zlr") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.1 (c (n "tv-renamer") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "1kxvyzya5b3x0rdj56824wcf6bcdw38zvwlhdkz1rmy0b8bx65fy") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.2 (c (n "tv-renamer") (v "0.2.2") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "19hgpjgxjzqq7isnncz5w3wpi8ilsyf28fxc058if85j36s9kpca") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.3 (c (n "tv-renamer") (v "0.2.3") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "1pq4yq1n9x9aphfrfdyd7qcwhg59iv3v472737kw9d0y0gj7nyx5") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.4 (c (n "tv-renamer") (v "0.2.4") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "1p3cpawz1f5s5g9hp1fzl6zbfa8yzn34lx9m2gwf63ygajz7d6j8") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.5 (c (n "tv-renamer") (v "0.2.5") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "12w9vvym9d02yfq374hsnvlmvwx5w9k6g75qjzl2jb9bspwvbjij") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.6 (c (n "tv-renamer") (v "0.2.6") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.0.7") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.2.0") (d #t) (k 0)))) (h "1hmb1xgv75dkhrmrgiskajkfg5sb9sfkb3ckn464n5zr4igb1yl3") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.2.7 (c (n "tv-renamer") (v "0.2.7") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "gdk") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_14"))) (o #t) (d #t) (k 0)) (d (n "tvdb") (r "^0.4.0") (d #t) (k 0)))) (h "0qv6np11fihcr44wir2i58f9jc9n902a23fc134anvp61wf3w69n") (f (quote (("enable_gtk" "gtk" "gdk"))))))

(define-public crate-tv-renamer-0.3.0 (c (n "tv-renamer") (v "0.3.0") (d (list (d (n "gdk") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "tvdb") (r "^0.4.0") (d #t) (k 0)))) (h "1byb0iscgir9klv9sm5zbg97r9w9nq6ih3m1kvyshzn6xj2gn099")))

(define-public crate-tv-renamer-0.3.2 (c (n "tv-renamer") (v "0.3.2") (d (list (d (n "gdk") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "tvdb") (r "^0.4.0") (d #t) (k 0)))) (h "13pb8w5smv4y9fbnfx6sbv9xycnb81c61xr3zf1wp4n4mcyqsrqp")))

(define-public crate-tv-renamer-0.3.3 (c (n "tv-renamer") (v "0.3.3") (d (list (d (n "gdk") (r "^0.5") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "tvdb") (r "^0.4") (d #t) (k 0)))) (h "02ws5l3fbr3sij5vz6dnvdacvzdsmnmxfbgkbnkyjkmrxwcv0xc9")))

