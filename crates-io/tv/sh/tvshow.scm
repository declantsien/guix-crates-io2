(define-module (crates-io tv sh tvshow) #:use-module (crates-io))

(define-public crate-tvshow-0.1.0 (c (n "tvshow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w8paywv1qf439q87b6cg61c3xy7af41ngc0cpyhm75wif29vbyw")))

(define-public crate-tvshow-0.1.1 (c (n "tvshow") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00f6q7sbq9wiy6h9a58zli8nz7lmmq7wl936acahicmzbq5lbf6i")))

(define-public crate-tvshow-0.1.2 (c (n "tvshow") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wn2b922djdb4lgavak8ayfbg0rp4yqkh5m06mxlvx01j2j4j08r")))

