(define-module (crates-io qn #{90}# qn908x-rs) #:use-module (crates-io))

(define-public crate-qn908x-rs-0.0.1 (c (n "qn908x-rs") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1p3pkbawzf22ismzdaiwwv1rd8qch782fxsll8d96g6kqckgb37s") (f (quote (("rt" "cortex-m-rt"))))))

