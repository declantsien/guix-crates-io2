(define-module (crates-io qn i- qni-core-rs) #:use-module (crates-io))

(define-public crate-qni-core-rs-0.2.0 (c (n "qni-core-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "0nm90vbrg3da94p4gl7zkzrizyamfl464p3k3ia34rs239rv8b2p")))

(define-public crate-qni-core-rs-0.2.1 (c (n "qni-core-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "1pas4f2qghmbg92xnyhdxb0x10jl2yswfrph97zm5jld62549l2k")))

(define-public crate-qni-core-rs-0.2.2 (c (n "qni-core-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "1s6593913067gv6yrrwmlvqrsb0qmbkzhq3rqiwrlcsbp56n7lzn")))

(define-public crate-qni-core-rs-0.2.3 (c (n "qni-core-rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "multiqueue") (r "^0.3.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "0yxx259lawykb84lymbl798h5h4rza7835yvyr5kqwb0m5ixfbyv")))

(define-public crate-qni-core-rs-0.2.4 (c (n "qni-core-rs") (v "0.2.4") (d (list (d (n "bus") (r "^2.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "0al6dqjrcwvflpi8z6agqlvr1yg65hsp83xwpmzj01gv35jymq8p")))

(define-public crate-qni-core-rs-0.2.5 (c (n "qni-core-rs") (v "0.2.5") (d (list (d (n "atomic-option") (r "^0.1.2") (d #t) (k 0)) (d (n "bus") (r "^2.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "1ss95ndbas9skh2gwa3c7vjb3rc0rklbdgkrdz0fymf2w4s51719")))

(define-public crate-qni-core-rs-0.2.6 (c (n "qni-core-rs") (v "0.2.6") (d (list (d (n "atomic-option") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "0c6b1r7if1isi3zfpbnl0362lkzqj68l62bw7dha4kghivf3f29n")))

(define-public crate-qni-core-rs-0.3.0 (c (n "qni-core-rs") (v "0.3.0") (d (list (d (n "atomic-option") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "1si7mlvzk9ljxb3rfw01vvd8arh1slx75xx2lr54bbhcdzqdzp7j")))

(define-public crate-qni-core-rs-0.3.1 (c (n "qni-core-rs") (v "0.3.1") (d (list (d (n "atomic-option") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "protobuf") (r "^2.2.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.2.0") (d #t) (k 1)))) (h "1sq17d652irrihkwvip384amxmvjm3apf342p76wb0r4hlnpvdl4")))

(define-public crate-qni-core-rs-0.3.2 (c (n "qni-core-rs") (v "0.3.2") (d (list (d (n "atomic-option") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "protobuf") (r "^2.8") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.8") (d #t) (k 1)))) (h "0ms0awmxm9a1zx0s5zl3bl5f34n9y81iqldc28pk86a8flvwzh7c")))

