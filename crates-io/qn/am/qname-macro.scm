(define-module (crates-io qn am qname-macro) #:use-module (crates-io))

(define-public crate-qname-macro-0.1.0 (c (n "qname-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "qname-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g1k73fvhszp5qswbkcj8lqb4d96rihs7i23qkx1kzfwlbd3yrqy")))

