(define-module (crates-io qn am qname-impl) #:use-module (crates-io))

(define-public crate-qname-impl-0.1.0 (c (n "qname-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0143g3nc6y8zi7ic664swrwzwj76krdx8h6ybh3wpl0y9lrx1362")))

