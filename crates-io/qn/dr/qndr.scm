(define-module (crates-io qn dr qndr) #:use-module (crates-io))

(define-public crate-qndr-0.1.0 (c (n "qndr") (v "0.1.0") (h "0g2ifi4l602x4lxqxilgvipqa455y13vr0haz1mnn7jv91zv730j") (y #t)))

(define-public crate-qndr-0.1.1 (c (n "qndr") (v "0.1.1") (h "0i9n75pc320vnias7rxp1af2yr2basrb3f5aq6d6hhrgvna97qz8") (y #t)))

(define-public crate-qndr-0.1.2 (c (n "qndr") (v "0.1.2") (h "1b9spnj6n8lpssdsxf0b5z9nvyijxnpi2whj8cang25wk52wazjw") (y #t)))

(define-public crate-qndr-0.1.3 (c (n "qndr") (v "0.1.3") (h "12ddlipyflba8pjrq8ysiy20q8fc6cw6vifnrmkwvhrwcny321fa") (y #t)))

(define-public crate-qndr-0.1.4 (c (n "qndr") (v "0.1.4") (h "00sys93p5m07nxizx3v3is6h8jrnwrq89q5aav2n677nid2i5l00") (y #t)))

(define-public crate-qndr-0.1.5 (c (n "qndr") (v "0.1.5") (h "1300w9xxf3iyxdmihhxhkpzxrv1wki080zzbxlhrynkd7ik5bfiq") (y #t)))

(define-public crate-qndr-0.1.6 (c (n "qndr") (v "0.1.6") (h "1yjz735g5ssa063xq2z45zp2wf1z35zm66w478ksqkbasanmyrml") (y #t)))

(define-public crate-qndr-0.1.7 (c (n "qndr") (v "0.1.7") (h "0z9y4ms0vm6fgnw4qpx6m9c26c7kwhxs8jsbhvrlx0lw2chl3ybp")))

