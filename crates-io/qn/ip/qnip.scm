(define-module (crates-io qn ip qnip) #:use-module (crates-io))

(define-public crate-qnip-0.1.0 (c (n "qnip") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "05z0yqc6h5k1p1rcw8dp9ylmh0dfpi9qzc9hx6gx3ngxffxg9ky2") (r "1.60.0")))

