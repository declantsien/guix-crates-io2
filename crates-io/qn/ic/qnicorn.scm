(define-module (crates-io qn ic qnicorn) #:use-module (crates-io))

(define-public crate-qnicorn-1.0.0 (c (n "qnicorn") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "build-helper") (r "^0.1") (d #t) (k 1)) (d (n "bytes") (r "^1.1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.22") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "tar") (r "^0.4.37") (d #t) (k 1)))) (h "05r2hg3hds2vqp3q6a72k2ialn1im39fl683cg5m163admw8sf8j") (l "qnicorn")))

