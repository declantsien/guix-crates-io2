(define-module (crates-io di rx dirx) #:use-module (crates-io))

(define-public crate-dirx-0.1.0 (c (n "dirx") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0") (d #t) (k 2)) (d (n "nix") (r "^0.26") (f (quote ("dir" "fs"))) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.6") (d #t) (k 2)))) (h "01lwhcdkj0cmbyvpxgv6qiavb8sx6pkprgs8biqcg9y71s82gcqv")))

