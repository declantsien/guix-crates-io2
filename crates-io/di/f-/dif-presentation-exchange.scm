(define-module (crates-io di f- dif-presentation-exchange) #:use-module (crates-io))

(define-public crate-dif-presentation-exchange-0.1.0 (c (n "dif-presentation-exchange") (v "0.1.0") (h "1yxxrix2xg1127xgslskzagrxpfr7q33sh9aiq7z1kiijdaz3rml")))

(define-public crate-dif-presentation-exchange-0.2.0 (c (n "dif-presentation-exchange") (v "0.2.0") (d (list (d (n "getset") (r "^0.1") (d #t) (k 0)) (d (n "jsonpath_lib") (r "^0.3") (d #t) (k 0)) (d (n "jsonschema") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.0") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yfk4g3931v3ksmxaks9kkjjkbl8lkgfn2vnkz7cvigp4lp9pgzd")))

