(define-module (crates-io di ct dictionary-1024) #:use-module (crates-io))

(define-public crate-dictionary-1024-0.1.0 (c (n "dictionary-1024") (v "0.1.0") (h "02b7cb692ilm9ma4vfvjrhd5f0sqq9qp3vrzyq7rk537a2f0plvb")))

(define-public crate-dictionary-1024-0.2.0 (c (n "dictionary-1024") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "16jc296mjaw4awz6phyp1djaw7y8fbq1kh83yiq39pjmjbdgk40r")))

(define-public crate-dictionary-1024-0.2.1 (c (n "dictionary-1024") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "1nmd4mwv4v2gdjml8vgd9mf747dpxzg1x48p5bfl3svwxsad31vg")))

(define-public crate-dictionary-1024-0.3.1 (c (n "dictionary-1024") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "13l0hv54isdvwqq2209vqwkbcy81xcfw391jxip0jlv2hgizimpj")))

