(define-module (crates-io di ct dictionary2) #:use-module (crates-io))

(define-public crate-dictionary2-1.0.0 (c (n "dictionary2") (v "1.0.0") (h "1fg345rijp7p8wa447afi975lnfwcndwjdadcr95i7r7h7d16b88")))

(define-public crate-dictionary2-1.0.1 (c (n "dictionary2") (v "1.0.1") (h "03j0cqvl91v51y3ai4qjfiz94i79plmkc7fwcji54x9dmnibb5sf")))

