(define-module (crates-io di ct dictionnaire) #:use-module (crates-io))

(define-public crate-dictionnaire-0.1.0 (c (n "dictionnaire") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09g2jmgwvw6sj24icwnfczizzyn5dkw2wgg56brqjfnk36jsrpyc")))

(define-public crate-dictionnaire-0.1.1 (c (n "dictionnaire") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19flz0w2123h8jp63h63wa5wcq836nbslgg19i84kawd78cv7a5f")))

(define-public crate-dictionnaire-0.1.2 (c (n "dictionnaire") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03hgxyrbrvyr3fv9hdb6w4fwqsp8lb288jsr0giigv9g7cg0gddq")))

