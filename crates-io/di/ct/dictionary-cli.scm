(define-module (crates-io di ct dictionary-cli) #:use-module (crates-io))

(define-public crate-dictionary-cli-0.1.0 (c (n "dictionary-cli") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "savefile") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12711zz3mcqqfm80iqmjj805xag4zcp7x39nq999pqqha9k6cnn5")))

