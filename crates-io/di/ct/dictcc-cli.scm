(define-module (crates-io di ct dictcc-cli) #:use-module (crates-io))

(define-public crate-dictcc-cli-0.1.0 (c (n "dictcc-cli") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 0)) (d (n "sqlite") (r "^0.23.9") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2.9") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12.0") (d #t) (k 0)))) (h "1qgfk25gb0ck6dsyg5xcjp667akx7wcvd1286kp29kxn19jbd6qf")))

(define-public crate-dictcc-cli-0.2.0 (c (n "dictcc-cli") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 0)) (d (n "sqlite") (r "^0.23.9") (d #t) (k 0)) (d (n "sqlite3-src") (r "^0.2.9") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12.0") (d #t) (k 0)))) (h "03x8gq8bmk4gbjp95ghyv35zyhynsn5p03jkhham1j8a7jqb747l")))

