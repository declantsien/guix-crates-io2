(define-module (crates-io di ct dict_derive) #:use-module (crates-io))

(define-public crate-dict_derive-0.1.0 (c (n "dict_derive") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.7.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wrnhf8q1rqhg3j5pqw68cj4m80y8yhpkpllqnr728k359ilpdvz")))

(define-public crate-dict_derive-0.1.1 (c (n "dict_derive") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.7.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0k066jcikvyynkqjn4zg4igkyr1na2b4lqd5g4914n8kwjcsa320")))

(define-public crate-dict_derive-0.1.2 (c (n "dict_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ss7aqwn91nfvxj8bzcxmbml7vvsd9jhbpignwgngy9898ix772r")))

(define-public crate-dict_derive-0.2.0 (c (n "dict_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0q1c7bcnrx14wjy8ayra4cvqhrz6cx8hkkwbbsymg0x4d9kyazlx")))

(define-public crate-dict_derive-0.3.0 (c (n "dict_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "00a4gxsbvm7mwplaynrhym6v96pic6pl1gk4gzjs9yfzn1kzf1xv")))

(define-public crate-dict_derive-0.3.1 (c (n "dict_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.12") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05dq6vvh90x6xvlfbdw9yssqmn1n7xk4scj0wc5vjfm1n28dmb8b")))

(define-public crate-dict_derive-0.4.0 (c (n "dict_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.14.1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kw0rmdf57yn4wxaajnyvzz4c6nki38h7nqfif2h1gxj6dmz81v2")))

(define-public crate-dict_derive-0.5.0 (c (n "dict_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1mnm557yydnv5q7pnbj5xqbdhyjkqi3f9kkq9rl2yfs9472cfsxl")))

