(define-module (crates-io di ct dictionary) #:use-module (crates-io))

(define-public crate-dictionary-0.1.0 (c (n "dictionary") (v "0.1.0") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "1h9mf60983jyc3qyqpnls5rh6s5il9xy7xqb1vxzjas0dydssbn4")))

