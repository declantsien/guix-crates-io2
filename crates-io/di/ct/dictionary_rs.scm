(define-module (crates-io di ct dictionary_rs) #:use-module (crates-io))

(define-public crate-dictionary_rs-0.2.0 (c (n "dictionary_rs") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)))) (h "1j8kghxyqfl3hx6ilhx52fdgmvwa7qyqzfakr0k5g1ydblphiqlj")))

(define-public crate-dictionary_rs-0.2.1 (c (n "dictionary_rs") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)))) (h "0f20m2vhhb8jw0hg0dsyc7pzcvab7dynpsri8lx33w4y78xhn0jj")))

