(define-module (crates-io di me dimensioned) #:use-module (crates-io))

(define-public crate-dimensioned-0.0.2 (c (n "dimensioned") (v "0.0.2") (h "1m9j5mnnyr9nj42kbhyy83x8dcnfhfyxv6p2f2h574kjj44n2jnz")))

(define-public crate-dimensioned-0.1.0 (c (n "dimensioned") (v "0.1.0") (h "0ydvhvsnkwczqjsz8icfb4w9g9baydf20bhhkl32i3k0p21mwfl3")))

(define-public crate-dimensioned-0.1.1 (c (n "dimensioned") (v "0.1.1") (h "0xmaxskfxf36d9qaag4zjz54v5k7hivffr5z8zhp2nx1dgfkzfcl")))

(define-public crate-dimensioned-0.1.2 (c (n "dimensioned") (v "0.1.2") (h "0ywk064w6843zpsmmmqlbvm94x6nv84cc13pq48iv8h6irqixrq0")))

(define-public crate-dimensioned-0.1.3 (c (n "dimensioned") (v "0.1.3") (h "0s16yz128mwbfawv2245qx2xmzafixgxadsy85s4vizfy08c8hna")))

(define-public crate-dimensioned-0.1.4 (c (n "dimensioned") (v "0.1.4") (h "0j8slqp4qr3rxv5fsbfy4rbgk6vhggvrmlmmnrqk2l2pb0zp4a52")))

(define-public crate-dimensioned-0.1.5 (c (n "dimensioned") (v "0.1.5") (h "1hjvs4nq6m2qg54lxd1wicv9xc1sf291m9vbq66110aqrpd7wz1w")))

(define-public crate-dimensioned-0.1.6 (c (n "dimensioned") (v "0.1.6") (h "0s05pw22kc8w9bw2ajjlcsxswwmd308x3bcrckzc9cc4whyg5r0z")))

(define-public crate-dimensioned-0.1.7 (c (n "dimensioned") (v "0.1.7") (h "0jp7r1rh99pqyihy91z5f79xadfsmidfybwm0rc3ciwc4rd8k8iv")))

(define-public crate-dimensioned-0.1.8 (c (n "dimensioned") (v "0.1.8") (h "1ax0rp1akwjiasfjyvdky6dhgpnvdcj4hhnpd9v8yixw4mp41zbk")))

(define-public crate-dimensioned-0.1.9 (c (n "dimensioned") (v "0.1.9") (h "1h8vjdgkcrd4az8y18z6by0asn7g0wny1rnfvfss6k7f97hx1nkk")))

(define-public crate-dimensioned-0.1.10 (c (n "dimensioned") (v "0.1.10") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "01yb3xjfg3nkq4r3nslwsyjhj2gim03l5k21ba8gjfh63zzl55g4")))

(define-public crate-dimensioned-0.1.11 (c (n "dimensioned") (v "0.1.11") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1fdnbcz737jcdg8crs6sm5qm8m14wsxgigs0q92dd23lp9b2gb97")))

(define-public crate-dimensioned-0.2.0 (c (n "dimensioned") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0807hg3bl9pw16icr9q4dhh8dv40vya2pf8i44vbv8vj6fh8mavr")))

(define-public crate-dimensioned-0.2.1 (c (n "dimensioned") (v "0.2.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1wpx7ix74ippzmv2x6ywbj581whl644i3ngr1s8m2mrddsaisbhs")))

(define-public crate-dimensioned-0.2.2 (c (n "dimensioned") (v "0.2.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1rc717bzlqv4n5njwfay1xlapyjnj362wmfxkw765lbsh8ff0ay1")))

(define-public crate-dimensioned-0.2.3 (c (n "dimensioned") (v "0.2.3") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "06swmmkgffbcvkjwnsp0n9m06jzdwdvgqz2pidfmm46sikvrc9wz")))

(define-public crate-dimensioned-0.2.4 (c (n "dimensioned") (v "0.2.4") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0s6qjvq1ql33shpv91w11si1apkbw1dfg5dy6gs7rcrc88z0x8hl")))

(define-public crate-dimensioned-0.3.0 (c (n "dimensioned") (v "0.3.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0banjyjkfbkcmqr8b50hk5719pzxzqp6b3wmdlphblpz1nsvy5ra")))

(define-public crate-dimensioned-0.3.1 (c (n "dimensioned") (v "0.3.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1q2bkkb1m4v1sy7v1xqp1xyh135hmz49ax6pw2gdjjq80h6yf2yd")))

(define-public crate-dimensioned-0.3.2 (c (n "dimensioned") (v "0.3.2") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1i64rqkmlwm735838nkavyiqaf7z8cgrk77i6xf8k2670assmlcb")))

(define-public crate-dimensioned-0.4.0 (c (n "dimensioned") (v "0.4.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "peano") (r "*") (d #t) (k 0)))) (h "1jyhxr4nmb6jm6dhixjcl7m6lj82dbbkx8kqpxbr44666q0rargm")))

(define-public crate-dimensioned-0.5.0 (c (n "dimensioned") (v "0.5.0") (d (list (d (n "num") (r "^0.1.28") (d #t) (k 0)) (d (n "typenum") (r "^1.1.0") (d #t) (k 0)))) (h "1nwh06d84vmww0w7ni3jl6rqab5biiq1149jbpb1kdq0ngz94xlm")))

(define-public crate-dimensioned-0.6.0 (c (n "dimensioned") (v "0.6.0") (d (list (d (n "approx") (r "^0.1.1") (f (quote ("no_std"))) (o #t) (d #t) (k 0)) (d (n "clippy") (r "^0.0.117") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.5.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.6.0") (d #t) (k 0)))) (h "0v5w56934fcnlni511qrr13wmrv8xriqmkrxzvyqxckpf2b5sn5h") (f (quote (("test" "clippy" "quickcheck" "quickcheck_macros" "approx" "oibit" "spec") ("spec") ("oibit") ("default"))))))

(define-public crate-dimensioned-0.7.0 (c (n "dimensioned") (v "0.7.0") (d (list (d (n "approx") (r "^0.1.1") (o #t) (k 0)) (d (n "clapme") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.11.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (k 0)) (d (n "quickcheck") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.6.0") (d #t) (k 0)))) (h "09ky8s3higkf677lmyqg30hmj66gpg7hx907s6hfvbk2a9av05r5") (f (quote (("test" "approx" "ci" "clapme" "quickcheck" "serde" "serde_test") ("std") ("spec") ("oibit") ("default" "std") ("ci"))))))

(define-public crate-dimensioned-0.8.0 (c (n "dimensioned") (v "0.8.0") (d (list (d (n "approx") (r "^0.5.1") (o #t) (k 0)) (d (n "auto-args") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "clapme") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.6.0") (d #t) (k 0)))) (h "15s3j4ry943xqlac63bp81sgdk9s3yilysabzww35j9ibmnaic50") (f (quote (("test" "approx" "clapme" "serde" "serde_test" "rand") ("std") ("spec") ("oibit") ("nightly") ("default" "std"))))))

