(define-module (crates-io di me dimensional_quantity) #:use-module (crates-io))

(define-public crate-dimensional_quantity-0.0.1-alpha.1 (c (n "dimensional_quantity") (v "0.0.1-alpha.1") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "1gdi9njjwp8idb2hqv62yhj59rhwbdcigchikzrrwlfqv38dvdmr")))

(define-public crate-dimensional_quantity-0.0.1-alpha.2 (c (n "dimensional_quantity") (v "0.0.1-alpha.2") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "1gyibj18aqlwd5rwb8g68gz7fkkw6z95q3vf6whbbf46wzgw4vaj")))

(define-public crate-dimensional_quantity-0.0.1-alpha.3 (c (n "dimensional_quantity") (v "0.0.1-alpha.3") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "1wcqayvaw9a0sdns8qpxzscdx53bvzlmhmndwavb4wrs8p9b6q0i")))

(define-public crate-dimensional_quantity-0.0.1-alpha.4 (c (n "dimensional_quantity") (v "0.0.1-alpha.4") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "08wgz0xsq9rkjr25lbgyla10iima1d6b4nc3fabjs9sm3ms50w16")))

(define-public crate-dimensional_quantity-0.0.1-alpha.5 (c (n "dimensional_quantity") (v "0.0.1-alpha.5") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "0bmanpkickclcclxdahr4pgp5k1g5sc74jk00cxqfc10fbj45cc3")))

(define-public crate-dimensional_quantity-0.0.1-alpha.6 (c (n "dimensional_quantity") (v "0.0.1-alpha.6") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "0cyzdqm97kqpq3q28yqdn3qq0h05naihgfni52nlwkhg4y3x9hp5")))

(define-public crate-dimensional_quantity-0.0.1 (c (n "dimensional_quantity") (v "0.0.1") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "1qaizziqlf59vf50r7nlpwkicjs8hqfppxcyman0gxjnisgvk94m")))

(define-public crate-dimensional_quantity-0.0.2 (c (n "dimensional_quantity") (v "0.0.2") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.5.*") (d #t) (k 1)))) (h "0sj1473riz06kpz5wyg7m6mls1x1h573gy75h2066vz7kk21416j")))

(define-public crate-dimensional_quantity-0.0.3 (c (n "dimensional_quantity") (v "0.0.3") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.7.*") (d #t) (k 1)))) (h "1r6g39g4ac8n1nl2w1kwdvllp7vrsjcmard3ya83i9bhxghf828y") (s 2) (e (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.0.4 (c (n "dimensional_quantity") (v "0.0.4") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "0.2.*") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "0.7.*") (d #t) (k 1)))) (h "0srv3n5j1daqjykyh776jqa4dhdqypkkh6vnw3g21p77b47bsnr2") (s 2) (e (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.0.5 (c (n "dimensional_quantity") (v "0.0.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "0hvla6lzkay5q123pnbs15bnlca4ryfs6f0izv7dj6239b1899wd") (s 2) (e (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1.0 (c (n "dimensional_quantity") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "1zh7lk4f7134mhfjdmf12hp3jyfmqxcign6cp6la7ys9lp699lmy") (s 2) (e (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1.1 (c (n "dimensional_quantity") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "1xdqx6ns4mnwwkbz61qwq91fniiafdgiiy1mrb14b0jgnqpkl3bz") (s 2) (e (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1.2 (c (n "dimensional_quantity") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 1)) (d (n "rust_decimal") (r "^1.35") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "02xg9k2i7zil9xf95533q6r2ifsi4gwx15cwpifqnsvvlxb50d5k") (s 2) (e (quote (("use_serde" "dep:serde") ("decimal" "dep:rust_decimal"))))))

(define-public crate-dimensional_quantity-0.1.3 (c (n "dimensional_quantity") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 1)) (d (n "rust_decimal") (r "^1.35") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.8") (d #t) (k 1)))) (h "13m337db8nagxc41wh58w6sqlmmrbar7f2pwhp0jfhzqx93znnab") (s 2) (e (quote (("use_serde" "dep:serde") ("decimal" "dep:rust_decimal"))))))

