(define-module (crates-io di me dimensions) #:use-module (crates-io))

(define-public crate-dimensions-0.1.0 (c (n "dimensions") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "0iywwq0ifsbymy9m1b8il2km1bwak8pmgb6lq0ab0m233l29jv88")))

