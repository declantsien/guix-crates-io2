(define-module (crates-io di da didaskalia) #:use-module (crates-io))

(define-public crate-didaskalia-0.0.0 (c (n "didaskalia") (v "0.0.0") (h "112bs7ifiaf3gv6q10jki5bkaslz0q56nx9k3zr1gbazrb55kzks")))

(define-public crate-didaskalia-0.1.0 (c (n "didaskalia") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "1raskxjhdrl87zincp4bvi0k0gac5li705mlkgmb4bfp2db0hg54")))

(define-public crate-didaskalia-0.1.1 (c (n "didaskalia") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "1510vpvk9ipzh5dm55bp8ggk2hrf2b0ik7zzhy89c3fmsb1jp4sc")))

(define-public crate-didaskalia-0.1.2 (c (n "didaskalia") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 2)))) (h "0lj5hzlfb7ni0ij0zdf7wj7iij49x9prbq8w5x2yxh6iz9v3w412")))

