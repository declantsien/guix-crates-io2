(define-module (crates-io di cg dicgen) #:use-module (crates-io))

(define-public crate-dicgen-0.1.0 (c (n "dicgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "1ylf5mb5k3dckns5z1wy2bghqxi7inwcpjjc5f2j6i2gi8mwyvb6")))

(define-public crate-dicgen-0.2.0 (c (n "dicgen") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "0r2ma4xcy296a3c4dhw5czn2dyrq4nfhp6f69d87krvimavk6glx")))

(define-public crate-dicgen-0.2.1 (c (n "dicgen") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)))) (h "1ryv5mrj34ddn3gfa5ngia105sqb571cjkr983886k8v0m8nzrpc")))

(define-public crate-dicgen-0.3.0 (c (n "dicgen") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0wiycmxcg561z8xgjp4y5hd0al5ij2078xbkwrpng5p1wqgg48hf")))

