(define-module (crates-io di vs divsufsort) #:use-module (crates-io))

(define-public crate-divsufsort-1.0.0 (c (n "divsufsort") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "sacabase") (r "^1.0.0") (d #t) (k 0)))) (h "0ji57b6r8qam9hk1val3i7n2j6rryjjd1x7hf43ai4wmzgf3qsr5") (f (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-1.0.1 (c (n "divsufsort") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "sacabase") (r "^1.0.0") (d #t) (k 0)))) (h "13z4zmb8lzvqwvlzz66znbbjgqbni6f9lvv20h93gx4pskzklhx8") (f (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-1.0.2 (c (n "divsufsort") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "sacabase") (r "^1.0.0") (d #t) (k 0)))) (h "10b5jrx2y5m4pk8y0k0nmiix4m978da334rqm71ck25xjgl28rlp") (f (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-2.0.0 (c (n "divsufsort") (v "2.0.0") (d (list (d (n "once_cell") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "sacabase") (r "^2.0.0") (d #t) (k 0)))) (h "08783gfacg6jnwxwjfxyyy9w2vglwb6563k9pkwlw2zy8mnf6msf") (f (quote (("crosscheck" "once_cell"))))))

