(define-module (crates-io di ag diag) #:use-module (crates-io))

(define-public crate-diag-0.1.0 (c (n "diag") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0asrvqacc012jjqqhcpdhys44hmxdz8d0vc1xhjran12yjxhfwjz") (y #t)))

(define-public crate-diag-0.2.0 (c (n "diag") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1k3nbkxinr5fsxr4qqfi2z08c660cgg6s3q98p2k4fhixfcbdb00") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.3.0 (c (n "diag") (v "0.3.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dc9jf2z7x953xk87x67a56bi03dgm63sfyxi0w5kag5gpms52ab") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.4.0 (c (n "diag") (v "0.4.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pgi0ar09l3iv3ihp7g43sc93wp3js2pddsm374xb8n7xl1g646x") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.5.0 (c (n "diag") (v "0.5.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jbm5mijl7lgm36hfsskhawyl2dxkyr49scyaj9wnnzisvwlj14b") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.6.0 (c (n "diag") (v "0.6.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "0cwyj0m27qdphxk3jylj4zfh2kikanrdqpq3mja4bxbdvc86m90i") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.6.1 (c (n "diag") (v "0.6.1") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "101yffq3jlrkp45mja91mjh283xq1hg30vqc5nmh75ad5bq0h0hj") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.7.0 (c (n "diag") (v "0.7.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)))) (h "1pq3kr5fjh1gf1a6qdir6ppmazqg2nan5g5pv0vmqkjzf3dd46mw") (f (quote (("qc" "quickcheck") ("default" "qc")))) (y #t)))

(define-public crate-diag-0.8.0 (c (n "diag") (v "0.8.0") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0j84xmhfpcamh6di761k9wjfwhrsb0gkvs6lh81lpfb303d7qr0k") (y #t)))

(define-public crate-diag-0.8.1 (c (n "diag") (v "0.8.1") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "02zjk244w8n6gmxsm89fbavhx4d196cyb2hzqwxppj34ywf1q8pi") (y #t)))

(define-public crate-diag-0.8.2 (c (n "diag") (v "0.8.2") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1qf1qapfyvq8x9g699cij7airjpp9ad54hh0l7fvvqhz0fw39lwg") (y #t)))

(define-public crate-diag-0.8.3 (c (n "diag") (v "0.8.3") (d (list (d (n "fixedbitset") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "18mvlcw0cghnv3bca3kxxzk8b2vbzzmd2idsj6pg5y3vg2c4asxj") (y #t)))

