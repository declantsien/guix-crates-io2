(define-module (crates-io di ag diagnostic_msgs) #:use-module (crates-io))

(define-public crate-diagnostic_msgs-4.2.3 (c (n "diagnostic_msgs") (v "4.2.3") (d (list (d (n "builtin_interfaces") (r "^4.2.3") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "std_msgs") (r "^1.2.1") (d #t) (k 0)))) (h "0wx5bvisphy69jwmb199m256cwpwk1yp7g1x9050yv5nhw9mj689") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "std_msgs/serde" "builtin_interfaces/serde"))))))

