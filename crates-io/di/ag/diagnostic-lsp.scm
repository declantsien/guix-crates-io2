(define-module (crates-io di ag diagnostic-lsp) #:use-module (crates-io))

(define-public crate-diagnostic-lsp-0.0.0 (c (n "diagnostic-lsp") (v "0.0.0") (d (list (d (n "diagnostic") (r "^0.1.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0ly23ixybby15lnk8nmmpayc8wzzkav6z8xklhkf2pgairpc2rgq")))

(define-public crate-diagnostic-lsp-0.1.0 (c (n "diagnostic-lsp") (v "0.1.0") (d (list (d (n "diagnostic") (r "^0.1.2") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0pphyjq44craamdxqpbvkmnajy6y2kvc9kf4zq6s2yyqml9jyr7c")))

(define-public crate-diagnostic-lsp-0.2.0 (c (n "diagnostic-lsp") (v "0.2.0") (d (list (d (n "diagnostic") (r "^0.3.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0kmqbv65lz427v2gd9ssnfg714wdygri0bz868ia7393vj5adc8y")))

