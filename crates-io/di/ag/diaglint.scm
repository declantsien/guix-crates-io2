(define-module (crates-io di ag diaglint) #:use-module (crates-io))

(define-public crate-diaglint-0.1.0 (c (n "diaglint") (v "0.1.0") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0702g9shd2zcvvhsdl3d60rak63x5p2nqpfq1kpbbzghv6ngp3pv")))

(define-public crate-diaglint-0.1.1 (c (n "diaglint") (v "0.1.1") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ghl71k439zrifk7ml88jhmbc4yf7qyhdpkp6vrf82qc25xm0r4m")))

(define-public crate-diaglint-0.1.2 (c (n "diaglint") (v "0.1.2") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hl9yf1s50fj3dzba52bkmkv5vaw288d3626rshvqaw29hy2hd4y")))

(define-public crate-diaglint-0.1.3 (c (n "diaglint") (v "0.1.3") (d (list (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0njvi89jb1q81bd93m7a1m9m3ml5w1vhhr9f3g02zvypiqj484bc")))

(define-public crate-diaglint-0.1.4 (c (n "diaglint") (v "0.1.4") (d (list (d (n "annotate-snippets") (r "^0.9") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i8zx0xd801wr4lw0i5g1hhaif3i1a52hny8bls0q61ix3hx52sk")))

(define-public crate-diaglint-0.1.5 (c (n "diaglint") (v "0.1.5") (d (list (d (n "annotate-snippets") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ga6b2lq4k9ba3wsjd9ng34flcvgm0vfg08ar6w8pibcam29nn3d")))

(define-public crate-diaglint-0.1.6 (c (n "diaglint") (v "0.1.6") (d (list (d (n "annotate-snippets") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16gz0nrr4i5zwrrgpyc41nh18bn6hjwsfpbp711fkqin5sj1f8c2")))

(define-public crate-diaglint-0.1.7 (c (n "diaglint") (v "0.1.7") (d (list (d (n "annotate-snippets") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1daklikl165k8zg1qshf5shrgyfi3w12jiqsr2imdhk3355kcs9m")))

(define-public crate-diaglint-0.1.8 (c (n "diaglint") (v "0.1.8") (d (list (d (n "annotate-snippets") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y1pic0c3jv4785lwrj4bjjj6a3yw98c077w7n7m1zyavb6y2c1p")))

