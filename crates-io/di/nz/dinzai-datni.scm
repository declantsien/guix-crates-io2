(define-module (crates-io di nz dinzai-datni) #:use-module (crates-io))

(define-public crate-dinzai-datni-0.1.0 (c (n "dinzai-datni") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "03qyv2bkbkp0nzwfmbsx0wpbz6fbs4x9sqdqmm99k97v3qx3fshz") (f (quote (("nightly") ("default"))))))

(define-public crate-dinzai-datni-0.1.1 (c (n "dinzai-datni") (v "0.1.1") (d (list (d (n "hyper") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.2") (d #t) (k 0)))) (h "04gk5hj8cbisffiyc7ibxa0sb96hpzhcl0qz52jk4k5j2s862x9j") (f (quote (("nightly") ("default"))))))

