(define-module (crates-io di on dionysos-consumer-derive) #:use-module (crates-io))

(define-public crate-dionysos-consumer-derive-0.1.0 (c (n "dionysos-consumer-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0myhpr26y0adax6y8d79l9nkxw08js5846nb1c0lj4diyz5dw350") (y #t)))

(define-public crate-dionysos-consumer-derive-0.1.2 (c (n "dionysos-consumer-derive") (v "0.1.2") (d (list (d (n "dionysos-synhelper") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dd4dzdgjbpilhs8g6vd1fpbakyi0cgn9cizji1dl4j0y9p1rsma") (y #t)))

