(define-module (crates-io di on dionysos-provider-derive) #:use-module (crates-io))

(define-public crate-dionysos-provider-derive-0.1.0 (c (n "dionysos-provider-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03grl4j11ixk9c6gsdkbw38y4n3aqsm2ypf970vx5ij5n8hxvqjk") (y #t)))

(define-public crate-dionysos-provider-derive-0.1.2 (c (n "dionysos-provider-derive") (v "0.1.2") (d (list (d (n "dionysos-synhelper") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c6bfr6qazs634qrqmdjw8ilbw46yhql418rccx3yryf6rx5m9nn") (y #t)))

