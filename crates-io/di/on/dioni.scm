(define-module (crates-io di on dioni) #:use-module (crates-io))

(define-public crate-dioni-1.0.0 (c (n "dioni") (v "1.0.0") (d (list (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rspotify") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "06w391h7py8xg2iwav8shplij0xxnj592ppv2d3n6kn4md17mmhj")))

(define-public crate-dioni-1.0.1 (c (n "dioni") (v "1.0.1") (d (list (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rspotify") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1dw1nlwrw91hcvjvk2f2j349wama8g2f67330cg9wlqjfb47ap8z")))

(define-public crate-dioni-1.1.0 (c (n "dioni") (v "1.1.0") (d (list (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rspotify") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.5") (d #t) (k 0)))) (h "1jx9h72aq28d27bm0kv1wnpn1rqfh65khchssbf136n4378v5fwp")))

