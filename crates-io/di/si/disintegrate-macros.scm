(define-module (crates-io di si disintegrate-macros) #:use-module (crates-io))

(define-public crate-disintegrate-macros-0.1.0 (c (n "disintegrate-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0jjww92q5qqn8yb0gj1q9i2s7bckfldixp5k9k6mpil1kzyjayhz") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.2.0 (c (n "disintegrate-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1lkwq2j08g1fzfdcp77ka7b002373ncgx7nw44fhab34349izk1p") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.3.0 (c (n "disintegrate-macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0mp1lxk0dkj69d7cgpj29fb0qz1yhnw968s4qvlg0nqgi1sza6hw") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.4.0 (c (n "disintegrate-macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1n6ywp06x6wxpsyj6gyhljj8ngfami9gk984yw5pckh5jigniraf") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.4.1 (c (n "disintegrate-macros") (v "0.4.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1amr0mpsx3s4irnqzf94g3im4b0fp7gixgpff6i4z7fkyjc16m6c") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.6.0 (c (n "disintegrate-macros") (v "0.6.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0dp1lfmmj6iivz61h3bp03w9wfvw8n2z3mnw2vp2s6f2gk0gc0ba") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.7.0 (c (n "disintegrate-macros") (v "0.7.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "01qv6phsyd3pzwjxxzwmyrz91g2radi1gq832y4552wii8aljzs5") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.7.1 (c (n "disintegrate-macros") (v "0.7.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "171lnvpfrs1fh3bwyzh8asxw8prgkhj9pjbvmy7121zip2san5xz") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.7.2 (c (n "disintegrate-macros") (v "0.7.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "04pfsjhmldwyccxhgxja7pl6n7bfnfbbsz5a2xgs0p7rdap18npy") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.7.3 (c (n "disintegrate-macros") (v "0.7.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "00lzzgj5dpbzi12lvdb8hr1rahqj45bd5g5hfw0wwsrn0kam2qs4") (f (quote (("never"))))))

(define-public crate-disintegrate-macros-0.8.0 (c (n "disintegrate-macros") (v "0.8.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "04j09djfwg6vx2mg64naw3a6d4v0378gsz1c644731nfmjhj5l4x") (f (quote (("never"))))))

