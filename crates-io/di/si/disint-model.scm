(define-module (crates-io di si disint-model) #:use-module (crates-io))

(define-public crate-disint-model-0.1.0 (c (n "disint-model") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)))) (h "076khwz7gmdinsscnc286nkg0lwlbcs79yc5z2gfp15317sdrxfl") (f (quote (("incomplete") ("default"))))))

(define-public crate-disint-model-0.2.0 (c (n "disint-model") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.122") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qijqpisjkmabbr3sn42dxms96ipbhkpck4h514d1l757h74wdlh") (f (quote (("incomplete") ("default"))))))

