(define-module (crates-io di si disint-tide) #:use-module (crates-io))

(define-public crate-disint-tide-0.1.0 (c (n "disint-tide") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "disint-security") (r "^0.1.0") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "1kpk05v9q678p7f4law558dql0py1mym988k64ldcw6lr21lh7pg")))

