(define-module (crates-io di gu digu) #:use-module (crates-io))

(define-public crate-digu-0.0.1 (c (n "digu") (v "0.0.1") (h "0p0c55j8yy32zx2152jwmm7k0hh0fv4rdhj8s9ywic3v4d55cjb0")))

(define-public crate-digu-0.0.2 (c (n "digu") (v "0.0.2") (h "19g29yn7bscj0n2fmyhjdbab5qrfrw16wxym99z3gzhh219sx9gc")))

(define-public crate-digu-0.0.3 (c (n "digu") (v "0.0.3") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "13rndyxcp5y4kwwn97bd2wimay1wpmjrbp60yqvmjif9i1q3mlf2")))

(define-public crate-digu-0.0.4 (c (n "digu") (v "0.0.4") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dsysxkzqk2g3na6lnmd0dwjkjr7psizkwfacnvf984mmv8wwd19")))

(define-public crate-digu-0.0.5 (c (n "digu") (v "0.0.5") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qlgp1d5c4n7rqdl6d96mqlcv7shzhsxq9x4ly12d0c3scp4j3c8")))

(define-public crate-digu-0.0.6 (c (n "digu") (v "0.0.6") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wy0qfjvm7la6ybgrd2ixslrk8vxsy8aind0bgqbbw6m8jpdzgi9")))

(define-public crate-digu-0.0.8 (c (n "digu") (v "0.0.8") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "dropshot") (r "^0.6.0") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lgaz0jnlc94j38n25j5m39364zi88g3sjydxrkfihhdj7p3yfyd")))

