(define-module (crates-io di al dialectic-tokio-mpsc) #:use-module (crates-io))

(define-public crate-dialectic-tokio-mpsc-0.1.0 (c (n "dialectic-tokio-mpsc") (v "0.1.0") (d (list (d (n "dialectic") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)))) (h "1r13zjyllxgsk9d2cvlri1ixr3vvalhwgpgrx1w3w8vd91l49h43")))

