(define-module (crates-io di al dialectic-macro) #:use-module (crates-io))

(define-public crate-dialectic-macro-0.1.0 (c (n "dialectic-macro") (v "0.1.0") (d (list (d (n "dialectic") (r "^0.4") (d #t) (k 2)) (d (n "dialectic-compiler") (r "^0.1") (d #t) (k 0)) (d (n "dialectic-tokio-mpsc") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2") (d #t) (k 2)))) (h "17mbh3i85fcrjvnk26951l6xfk05hwq4iq56d2rs5845qm8w9bjc")))

