(define-module (crates-io di al dialectic-tokio-serde-bincode) #:use-module (crates-io))

(define-public crate-dialectic-tokio-serde-bincode-0.1.0 (c (n "dialectic-tokio-serde-bincode") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dialectic") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dialectic-tokio-serde") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1x1212jcps0hz0nd7p5prp5by5rr7j4mv83vfa6kr27axjx7hgd1")))

