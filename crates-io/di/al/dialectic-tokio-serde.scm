(define-module (crates-io di al dialectic-tokio-serde) #:use-module (crates-io))

(define-public crate-dialectic-tokio-serde-0.1.0 (c (n "dialectic-tokio-serde") (v "0.1.0") (d (list (d (n "dialectic") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0wifc84r5dgh9n1h1safqf4ib6j4v166712hh8lvg6zpsq1kcbwd")))

