(define-module (crates-io di al dialectic-compiler) #:use-module (crates-io))

(define-public crate-dialectic-compiler-0.1.0 (c (n "dialectic-compiler") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thunderdome") (r "^0.4") (d #t) (k 0)))) (h "15f3y7hdjdfw3208gc2y64cm30xjrqz7z8wmx40kag1c6majhdfn")))

