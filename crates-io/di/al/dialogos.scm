(define-module (crates-io di al dialogos) #:use-module (crates-io))

(define-public crate-dialogos-0.1.0 (c (n "dialogos") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "02gqjh8hcsvl29iydwiq05wpcmznx7fi0pafh0llaz5122l3a694")))

(define-public crate-dialogos-0.1.1 (c (n "dialogos") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0wwgkb9q5y9rx1ba919x0whr8ysp3krkq4m3y3inpy36b5y784nd")))

