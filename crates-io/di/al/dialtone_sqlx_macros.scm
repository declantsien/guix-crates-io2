(define-module (crates-io di al dialtone_sqlx_macros) #:use-module (crates-io))

(define-public crate-dialtone_sqlx_macros-0.1.0 (c (n "dialtone_sqlx_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1aqria8z5g4cp71l9xp722y06nhclmlmliw2w4ijklqqd73nsvsk")))

