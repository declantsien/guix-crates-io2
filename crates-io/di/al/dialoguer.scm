(define-module (crates-io di al dialoguer) #:use-module (crates-io))

(define-public crate-dialoguer-0.1.0 (c (n "dialoguer") (v "0.1.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "10x6b8phf7905nvb44nc4rf3v24r5abd1gql34iqs9cw184cirmc")))

(define-public crate-dialoguer-0.2.0 (c (n "dialoguer") (v "0.2.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.11") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "0in713r726r4ppmv18zr91glg4vhab0jbvfp9a9xk12qdx400pgv")))

(define-public crate-dialoguer-0.3.0 (c (n "dialoguer") (v "0.3.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 0)))) (h "1a9gqvqp83gg4jbm286q5ab3l44zyyzlsdaiqmw8x4k80fdc5l8s")))

(define-public crate-dialoguer-0.4.0 (c (n "dialoguer") (v "0.4.0") (d (list (d (n "console") (r ">= 0.3.0, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "135sazn5bg202piicrn1v60lffyc23vlzajp53az16miwz26cvqi")))

(define-public crate-dialoguer-0.5.0 (c (n "dialoguer") (v "0.5.0") (d (list (d (n "console") (r ">= 0.9.1, < 1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1dh3z5brmiqwc970riwd4n2ab15d6g5gcifiadrcjky0s8jnwqcl")))

(define-public crate-dialoguer-0.5.1 (c (n "dialoguer") (v "0.5.1") (d (list (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0ymzwfiafsqhdh165acd2dckk1mq3w9lp1ldimdrakrwrq7ypdfq")))

(define-public crate-dialoguer-0.6.0 (c (n "dialoguer") (v "0.6.0") (d (list (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1171b0vmmd25crqrhnrz95fwf9x0845zqjqrlspzs82a861mx4lm")))

(define-public crate-dialoguer-0.6.1 (c (n "dialoguer") (v "0.6.1") (d (list (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0adzm8k2yk3srf98622hvg0b8hr75gd661k0jl9a90c8dl1x9vlw")))

(define-public crate-dialoguer-0.6.2 (c (n "dialoguer") (v "0.6.2") (d (list (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0f31ahy6myg2vz9xrdmp0vx0m7x427a1wxpgrgwhxd0rgfpqdapl")))

(define-public crate-dialoguer-0.7.0 (c (n "dialoguer") (v "0.7.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zeroize") (r "^0.9.3") (d #t) (k 0)))) (h "1jdv2p8y3cv48v9f547wqs56zphbxc63q1vjm34lfmxw2gqbmg7l")))

(define-public crate-dialoguer-0.7.1 (c (n "dialoguer") (v "0.7.1") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zeroize") (r "^0.9.3") (f (quote ("std"))) (k 0)))) (h "09lwjynb808jrvs8pf70kca74979sxjrsza98ybhzj9xjjr0gy3h")))

(define-public crate-dialoguer-0.8.0 (c (n "dialoguer") (v "0.8.0") (d (list (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (d #t) (k 0)))) (h "037s4cn20rflrkxnxhj280s5cklns7kl2jmvz8cji4k5if7hbpf9")))

(define-public crate-dialoguer-0.9.0 (c (n "dialoguer") (v "0.9.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1sxy4nd9kd9wslxnjdjyxgmsg5fil3dnzy63z8f07in09vd9lmv1") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password"))))))

(define-public crate-dialoguer-0.10.0 (c (n "dialoguer") (v "0.10.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1nmrfdi6khlxjm9x982694agnjbymjam7q8svzhrg7ndmd7np79l") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

(define-public crate-dialoguer-0.10.1 (c (n "dialoguer") (v "0.10.1") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0if5vnxczblxjp6w92nmlx4l5f2wsmi8s33im0rdrb80wi4axj6q") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

(define-public crate-dialoguer-0.10.2 (c (n "dialoguer") (v "0.10.2") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1ccf0xnhlcfxjb68688cb538x4xhslpx9if0q3ymfs7gxhvpwbm9") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

(define-public crate-dialoguer-0.10.3 (c (n "dialoguer") (v "0.10.3") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1wlisvi5n6iw7aw419jap3y22245z93inqc1sngqsh0b7dppjg5g") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

(define-public crate-dialoguer-0.10.4 (c (n "dialoguer") (v "0.10.4") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "11rgzrhi677w9gf1r3ip2x361svdkjkr2m5dsfca9fcljacg5ijr") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion"))))))

(define-public crate-dialoguer-0.11.0 (c (n "dialoguer") (v "0.11.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "1pl0744wwr97kp8qnaybzgrfwk66qakzq0i1qrxl03vpbn0cx2v5") (f (quote (("password" "zeroize") ("history") ("fuzzy-select" "fuzzy-matcher") ("editor" "tempfile") ("default" "editor" "password") ("completion")))) (r "1.63.0")))

