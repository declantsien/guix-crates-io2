(define-module (crates-io di al dialtone) #:use-module (crates-io))

(define-public crate-dialtone-0.1.0 (c (n "dialtone") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dialtone_ccli_util") (r "^0.1.0") (d #t) (k 0)) (d (n "dialtone_common") (r "^0.1.0") (d #t) (k 0)) (d (n "dialtone_reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "requestty") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (f (quote ("paris"))) (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1659wpxsdcz923d8j4wj6s1i54nj94w7h8zh89gimk8qhvw0h1nr")))

