(define-module (crates-io di al dialect) #:use-module (crates-io))

(define-public crate-dialect-0.1.0 (c (n "dialect") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1pqpdn086i4lrvzigpaw3c9qrr6jblhfx45y9yv96b0qwpmxja0z")))

(define-public crate-dialect-0.1.1 (c (n "dialect") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "192anpj243ancfppx7jalka0idp2n9pndijp4gagz0x3rcgv64nl")))

(define-public crate-dialect-0.1.2 (c (n "dialect") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "0qg89n8v5vings3siajcg32ydz0am499wq6njgy3kr48jz2rg7rc")))

(define-public crate-dialect-0.2.0 (c (n "dialect") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)))) (h "1sfhx1kgbgmzxwrj2z9fyk6b1gwcnham1cgcidn3vs7xh6lcsi85")))

(define-public crate-dialect-0.3.0 (c (n "dialect") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0s13wvwa4v70mv22jl6j35xjvc51hlaw417qsx93rdm17mjp5gxi")))

(define-public crate-dialect-0.4.0 (c (n "dialect") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "0wmgbswqqdpvbyvs0f85qprf03mlq11vgmcazql802y94fg7dr07")))

(define-public crate-dialect-0.4.1 (c (n "dialect") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.18") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "12fgsik9fhiv6w900f9wp5nr9x6j9djg21rxc8pl1ly0x7ly7mg7")))

