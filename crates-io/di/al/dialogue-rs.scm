(define-module (crates-io di al dialogue-rs) #:use-module (crates-io))

(define-public crate-dialogue-rs-0.1.0 (c (n "dialogue-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "03d3p6w7dz4n7rk2rp91fk6hm9rlsn8ff86drj93qqv01k4ddiqw")))

