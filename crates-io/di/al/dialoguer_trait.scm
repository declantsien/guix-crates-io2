(define-module (crates-io di al dialoguer_trait) #:use-module (crates-io))

(define-public crate-dialoguer_trait-0.1.0 (c (n "dialoguer_trait") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 2)) (d (n "dialoguer-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)))) (h "0wjjcqm0v0gwc0pc8vm9byw4cvfsdcwl029c46lwn8yb2wm5053h") (y #t)))

(define-public crate-dialoguer_trait-0.1.1 (c (n "dialoguer_trait") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "dialoguer_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "0hahlssg1kx2gnsp0f9sfc2zil84h5ijvsljxk183nk7nqm3syng")))

(define-public crate-dialoguer_trait-0.1.2 (c (n "dialoguer_trait") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "dialoguer_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1z0x4a6ra74an3p8fhsmz8y4cv02j1bv7g3isadzc63nwjz5h442")))

(define-public crate-dialoguer_trait-0.2.0 (c (n "dialoguer_trait") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "dialoguer_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 2)))) (h "1r5r1nm9syg0iq3z3rgfffrcbqi4va3g4kh1dhgir2dc8f6vxifz")))

