(define-module (crates-io di al dialogue-macro) #:use-module (crates-io))

(define-public crate-dialogue-macro-0.1.0 (c (n "dialogue-macro") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hr93i429j61zxq8x0ryfs8sh37fcvv328rwbq6b4z0463s1hgdr")))

(define-public crate-dialogue-macro-0.1.1 (c (n "dialogue-macro") (v "0.1.1") (d (list (d (n "dialogue-core") (r "^0.1.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "06p9bwp6x88xkzrrhk0zf1baqjn1g6lpiacmcm9h9m0ckp043hvg")))

(define-public crate-dialogue-macro-0.1.2 (c (n "dialogue-macro") (v "0.1.2") (d (list (d (n "dialogue-core") (r "^0.1.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1fzynz1hg91kjsizcry2g6yw0bx3capgbfn6nbmnii2gzgxd415w")))

(define-public crate-dialogue-macro-0.1.3 (c (n "dialogue-macro") (v "0.1.3") (d (list (d (n "dialogue-core") (r "^0.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "175clfj4f5ima1my5n92fr76irk94qrwxn9pp7psgzxs1qrvr100")))

(define-public crate-dialogue-macro-0.2.0 (c (n "dialogue-macro") (v "0.2.0") (d (list (d (n "dialogue-core") (r "^0.2.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0093iv84q993di1jc5ppbl4cb4n4r3cgcnynjx51b3x3s2g2326m")))

(define-public crate-dialogue-macro-0.2.1 (c (n "dialogue-macro") (v "0.2.1") (d (list (d (n "dialogue-core") (r "^0.2.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "00y7km8kq82a51m4yrk9qg50h41jmwvyxkzia8sr01j0sjyd4xbw")))

(define-public crate-dialogue-macro-0.2.2 (c (n "dialogue-macro") (v "0.2.2") (d (list (d (n "dialogue-core") (r "^0.2.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hpm6jpi3ix8s09j1ylvg0627k0m998nwh1xqmfr59284m296xcy")))

(define-public crate-dialogue-macro-0.2.3 (c (n "dialogue-macro") (v "0.2.3") (d (list (d (n "dialogue-core") (r "^0.2.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bmpid4iwfnlan8rkq0i8hj0dp0g5g45pzlvygsgc6xnfg3ms4hb")))

(define-public crate-dialogue-macro-0.2.4 (c (n "dialogue-macro") (v "0.2.4") (d (list (d (n "dialogue-core") (r "^0.2.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ddi5vgbjzhjwv9gswbc8h8y5zqfa92js8v5g1x7gna5lminsx36")))

(define-public crate-dialogue-macro-0.2.5 (c (n "dialogue-macro") (v "0.2.5") (d (list (d (n "dialogue-core") (r "^0.2.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0sj1klja8lgrjmahyhq45z6rp675arbm0w43iscshp5ng7zm90d8")))

(define-public crate-dialogue-macro-0.3.0 (c (n "dialogue-macro") (v "0.3.0") (d (list (d (n "dialogue-core") (r "^0.3.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "02z73x9wl47bmm36wdr0aam5w5k25kanhsmzzydhw9zc2vlfdccc")))

(define-public crate-dialogue-macro-0.3.1 (c (n "dialogue-macro") (v "0.3.1") (d (list (d (n "dialogue-core") (r "^0.3.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "10wz9vfvfmbcbkrqs12hnal5gfrdk61hmkrwm5pnm251298fccn6")))

(define-public crate-dialogue-macro-0.3.2 (c (n "dialogue-macro") (v "0.3.2") (d (list (d (n "dialogue-core") (r "^0.3.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mjripri6z0y4jbaq405gbx6mzz4cy7q0x98fd0lxqghxbaacgxh")))

