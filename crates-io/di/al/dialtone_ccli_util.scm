(define-module (crates-io di al dialtone_ccli_util) #:use-module (crates-io))

(define-public crate-dialtone_ccli_util-0.1.0 (c (n "dialtone_ccli_util") (v "0.1.0") (d (list (d (n "dialtone_common") (r "^0.1.0") (d #t) (k 0)) (d (n "dialtone_reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0l94vwxx8r878dm7m1rlq42mwvzdcc2c9rd2c5yk4zmfph11wz8f")))

