(define-module (crates-io di al dialtone_common) #:use-module (crates-io))

(define-public crate-dialtone_common-0.1.0 (c (n "dialtone_common") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.21") (d #t) (k 0)) (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_variant") (r "^0.1.1") (d #t) (k 0)))) (h "1i6ih773x1y4mjkbqzn4l2a68ljnz02bcyqhjycgx26w3s6nk45j")))

