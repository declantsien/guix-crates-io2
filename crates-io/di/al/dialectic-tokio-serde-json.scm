(define-module (crates-io di al dialectic-tokio-serde-json) #:use-module (crates-io))

(define-public crate-dialectic-tokio-serde-json-0.1.0 (c (n "dialectic-tokio-serde-json") (v "0.1.0") (d (list (d (n "dialectic") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dialectic-tokio-serde") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0zs4q394s9nzf03d4jl9fnbj06ahjdmmrlzysrxq5zqzc3lxp1xv")))

