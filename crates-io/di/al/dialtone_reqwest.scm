(define-module (crates-io di al dialtone_reqwest) #:use-module (crates-io))

(define-public crate-dialtone_reqwest-0.1.0 (c (n "dialtone_reqwest") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "dialtone_common") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1s0g12i58x6nm90pfzrgpgyf5c8d7v28lm12cnncsb9qgcqq4c3f")))

