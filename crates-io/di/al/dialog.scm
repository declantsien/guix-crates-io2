(define-module (crates-io di al dialog) #:use-module (crates-io))

(define-public crate-dialog-0.1.0 (c (n "dialog") (v "0.1.0") (h "1v5p2pkgxyzakz6i06jrc8xxv3qn7gq4hx1shxf0p15f2ys990c0")))

(define-public crate-dialog-0.1.1 (c (n "dialog") (v "0.1.1") (h "0rgqylcbi5w38c4a7ixg9jjbm4icx6fn43503i2wk31cvalvdlqz")))

(define-public crate-dialog-0.2.0 (c (n "dialog") (v "0.2.0") (d (list (d (n "rpassword") (r "^2") (d #t) (k 0)))) (h "1xljq5br23c3ffpvbhgh9x8hcsfhn7b7yvgcscazdv7nv7v8araz")))

(define-public crate-dialog-0.2.1 (c (n "dialog") (v "0.2.1") (d (list (d (n "rpassword") (r "^2") (d #t) (k 0)))) (h "0x52cf6bfrc2539ymgqj45ywhgxl9z05nci587876inw0gc6hrr2")))

(define-public crate-dialog-0.3.0 (c (n "dialog") (v "0.3.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "rpassword") (r "^2") (d #t) (k 0)))) (h "1w4vsab7xjgi8b6kci8ha9l2q651210pm995ayc4rla7sqvansvk")))

