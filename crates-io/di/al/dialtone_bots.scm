(define-module (crates-io di al dialtone_bots) #:use-module (crates-io))

(define-public crate-dialtone_bots-0.1.0 (c (n "dialtone_bots") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dialtone_ccli_util") (r "^0.1.0") (d #t) (k 0)) (d (n "dialtone_common") (r "^0.1.0") (d #t) (k 0)) (d (n "dialtone_reqwest") (r "^0.1.0") (d #t) (k 0)) (d (n "requestty") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (f (quote ("paris"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1gd4jymlq269cvzrkh1ldq7x4k83ya9skkc6kanh2qbdjcj5kl1f")))

