(define-module (crates-io di al dialogue-core) #:use-module (crates-io))

(define-public crate-dialogue-core-0.1.0 (c (n "dialogue-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kqkwkm2d4n4rmvas86fhzarnvs0gax3gl97xyjdbglbpl9h8ncx")))

(define-public crate-dialogue-core-0.1.2 (c (n "dialogue-core") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q30g2mgzzywdxszyqic2qsnkp5anmy54rr6z8d0cn7l5yqg5165")))

(define-public crate-dialogue-core-0.1.3 (c (n "dialogue-core") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vxdglnmn7sakgrzg4c2c97jrjih20wqmykhy90g54rf404jfrd8")))

(define-public crate-dialogue-core-0.2.0 (c (n "dialogue-core") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0h5wwn58cnb92qvkmxjx8y5n9fk1n2ri1qy815pxpgqf39z8mx1d")))

(define-public crate-dialogue-core-0.2.1 (c (n "dialogue-core") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rpcdsmlb368j5v3gd9d059qigflg9x3kzm742rk9hlhyb12x92k")))

(define-public crate-dialogue-core-0.2.2 (c (n "dialogue-core") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0p2b2bki27g31pxnbq98fab4fgxf4vcxciv3m4k1wz3k1p17b3li")))

(define-public crate-dialogue-core-0.2.3 (c (n "dialogue-core") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0iflziqfpgabh0d8b82jdbw8lcl0k9c11nkdk6189hfxixl8ij27")))

(define-public crate-dialogue-core-0.2.4 (c (n "dialogue-core") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fi12x18yyh57b9jnsssjgsi64x7017n7wilpcqzp0flasw19s42")))

(define-public crate-dialogue-core-0.2.5 (c (n "dialogue-core") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08wchnm771mz6l5qrayd1yagw591rxvl6q03ibram5ngwy06hhij")))

(define-public crate-dialogue-core-0.3.0 (c (n "dialogue-core") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18w6yrmf9hanj4pvqk9sd83h693jp2c13sk0947q2h0q59zyv0r3")))

(define-public crate-dialogue-core-0.3.1 (c (n "dialogue-core") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hckvhr5pa52cmb016ra28sda8vsp349jmrnz4l2636l59vlns0v")))

(define-public crate-dialogue-core-0.3.2 (c (n "dialogue-core") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "16wbmw5fhzc0lmrx6y8f810nq3xhxgzpdn0idccdym27g83lrwcy")))

