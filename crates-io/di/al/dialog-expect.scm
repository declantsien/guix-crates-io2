(define-module (crates-io di al dialog-expect) #:use-module (crates-io))

(define-public crate-dialog-expect-1.0.0 (c (n "dialog-expect") (v "1.0.0") (d (list (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "1p4hy9mqan4afm5rrjxnc3k8dcqnfcxxmxzw4px2f6gx08amp31a")))

(define-public crate-dialog-expect-1.0.1 (c (n "dialog-expect") (v "1.0.1") (d (list (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "1mp5dxr03igi7z72d1kjnkmp2yr6a7xivvkg3c1s6bygh80xsdiy")))

(define-public crate-dialog-expect-1.0.2 (c (n "dialog-expect") (v "1.0.2") (d (list (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "03sp4x3a9qqdqfwjihw4g83qhpg3q671vabsh396604wi2ipg5n0")))

(define-public crate-dialog-expect-1.0.3 (c (n "dialog-expect") (v "1.0.3") (d (list (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "1jj9k9kdxnli5665ibbxbijp1b6wnsh6miw6pvysix8vqazaniah")))

(define-public crate-dialog-expect-1.0.4 (c (n "dialog-expect") (v "1.0.4") (d (list (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)))) (h "0g8cv7i61rarg12bks2p28mcsijgsk2mws1fc98spc6pxpzp76b6")))

