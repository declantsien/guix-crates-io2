(define-module (crates-io di e_ die_parser) #:use-module (crates-io))

(define-public crate-die_parser-1.0.0 (c (n "die_parser") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1axdm66if9w9f4rh8sg7h3jkqkvx0q4bicwhp7aq2f2y5l2a8iqj")))

(define-public crate-die_parser-1.0.1 (c (n "die_parser") (v "1.0.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "02h4r9px20dxln7j8i8jizn9j98gndg4m0y896qv3yijc5ypvksq")))

