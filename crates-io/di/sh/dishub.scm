(define-module (crates-io di sh dishub) #:use-module (crates-io))

(define-public crate-dishub-0.1.0 (c (n "dishub") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.18") (d #t) (k 0)) (d (n "discord") (r "^0.7") (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1qhbskhjm46xb7dj5va0gaw9lhaasgd2ymjkdnqnn319y20z26kl")))

(define-public crate-dishub-0.1.1 (c (n "dishub") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.18") (d #t) (k 0)) (d (n "discord") (r "^0.7") (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "15aw2cj3clb34mqinshvkmnhcxkzl5n8kc3zydf6kj06gsdn5kyy")))

