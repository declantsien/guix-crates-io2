(define-module (crates-io di su disuse) #:use-module (crates-io))

(define-public crate-disuse-0.0.1 (c (n "disuse") (v "0.0.1") (h "17rmlk5j5pq374vzh5mvg8qspv3my7qkvsbm6929ir63xpdw5j3a")))

(define-public crate-disuse-0.0.2 (c (n "disuse") (v "0.0.2") (h "0pjjc1b86iz5l6vlzii6f0lzg688mqkzff6kw057apbcfxp9rwid")))

