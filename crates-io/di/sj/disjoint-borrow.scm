(define-module (crates-io di sj disjoint-borrow) #:use-module (crates-io))

(define-public crate-disjoint-borrow-0.1.0 (c (n "disjoint-borrow") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.22") (f (quote ("stable"))) (d #t) (k 2)))) (h "0lnq51hr4s31dxxki5bdl1mh2mv1livdr4ji87xnqdkln62p2z9d")))

(define-public crate-disjoint-borrow-0.1.1 (c (n "disjoint-borrow") (v "0.1.1") (d (list (d (n "trybuild") (r "^1.0.11") (d #t) (k 2)))) (h "0iqdilc8v8azvb8hpbkp4a3c3cdnhhyzwjiy7ighfxgy65bkgq49")))

