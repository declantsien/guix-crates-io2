(define-module (crates-io di sj disjoint-set) #:use-module (crates-io))

(define-public crate-disjoint-set-0.0.1 (c (n "disjoint-set") (v "0.0.1") (h "02g1nfdj809rxzr0l59ndq16n7bx9chz2nlrf5m7hdjq9x2sfrhy")))

(define-public crate-disjoint-set-0.0.2 (c (n "disjoint-set") (v "0.0.2") (h "11bmpd96rc166wrk08i6kdsd58klq036rm6ni37dvkgxcajg20ni")))

