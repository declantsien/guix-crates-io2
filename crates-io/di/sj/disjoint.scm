(define-module (crates-io di sj disjoint) #:use-module (crates-io))

(define-public crate-disjoint-0.1.0 (c (n "disjoint") (v "0.1.0") (h "144k5gib2lzy59mvqiw3k6j1vd5wyyk634w3aqlgcna4g35jbksk") (r "1.56")))

(define-public crate-disjoint-0.2.0 (c (n "disjoint") (v "0.2.0") (h "0m951g8zdfwklf2sjdjmpvyrj1qa7rsihvhng18bncvnp268g630") (r "1.56")))

(define-public crate-disjoint-0.3.0 (c (n "disjoint") (v "0.3.0") (h "0l1vbi1wa95dl16hli5dhlh0w6d83ss51cxyd9snygk528x9z70j") (r "1.56")))

(define-public crate-disjoint-0.4.0 (c (n "disjoint") (v "0.4.0") (h "12hjcaclbs3rg4xdnh4hz9n0590110zzjkygx5qrnwvl1f7c4naw")))

(define-public crate-disjoint-0.5.0 (c (n "disjoint") (v "0.5.0") (h "147jqkwq8hqssp0f8fxsjdbchca0s6z7zf45ccwh76pnxq02fyvh")))

(define-public crate-disjoint-0.6.0 (c (n "disjoint") (v "0.6.0") (h "06ihbnlv4q5n7za2i1zdma5rpb0a64x20jxhq8s7bbhn2im9fdqd")))

(define-public crate-disjoint-0.7.0 (c (n "disjoint") (v "0.7.0") (h "168w9bz96h2nmvk759hl30s0zxa6fpqg2xbr25pph9b5324zfvqp")))

