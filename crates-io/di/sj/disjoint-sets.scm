(define-module (crates-io di sj disjoint-sets) #:use-module (crates-io))

(define-public crate-disjoint-sets-0.1.0 (c (n "disjoint-sets") (v "0.1.0") (h "1m27as3ljy6hdpilmm2i3q1gqdxdvqskpmr6kcniraw7z3fbf8mw")))

(define-public crate-disjoint-sets-0.1.1 (c (n "disjoint-sets") (v "0.1.1") (h "1g8pc4ignjq8dnhf2aqc9dda5pqw6ydxq2h105laq82scb508fx5")))

(define-public crate-disjoint-sets-0.2.0 (c (n "disjoint-sets") (v "0.2.0") (h "00cg0kzzqsl2lp9fzam3aa037iazfcdpgr02mchcnh340gmcsvkk")))

(define-public crate-disjoint-sets-0.2.1 (c (n "disjoint-sets") (v "0.2.1") (h "03ldy3pjfky44mk2q6xl0g0s8k14q3q72i1xsccdgif422vlk2dh")))

(define-public crate-disjoint-sets-0.2.2 (c (n "disjoint-sets") (v "0.2.2") (h "0v4dm40zps3zvk87gfaryj7gdsab0xx4gswc35vhbyylhl5f9k2m")))

(define-public crate-disjoint-sets-0.2.3 (c (n "disjoint-sets") (v "0.2.3") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1ac5lhklxhck93ng58yw634p8qjinn1ib33cxhcdqbb9pgc6pcv5")))

(define-public crate-disjoint-sets-0.3.0 (c (n "disjoint-sets") (v "0.3.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "01p0mv8yic2djvgs3gawpq7n914lkzsnp6ma327fwk5kl501dyjg")))

(define-public crate-disjoint-sets-0.3.1 (c (n "disjoint-sets") (v "0.3.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "0fnja7qvzp3n75v7ag3q74dmgy1bphx048symgx8lr86ssyzgcym")))

(define-public crate-disjoint-sets-0.3.2 (c (n "disjoint-sets") (v "0.3.2") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1r6mwq6zk0rrxs8jd6ygcb9a3hh62k2xl63mqsk2p61rm6fqfxsv")))

(define-public crate-disjoint-sets-0.3.3 (c (n "disjoint-sets") (v "0.3.3") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "1s8691qpgih9cb22x8wg7svi6miqqhk7c2ynzn44xlcbk9fy05h4")))

(define-public crate-disjoint-sets-0.3.4 (c (n "disjoint-sets") (v "0.3.4") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)))) (h "0r81zbsdl6mbzszcnd6cpnfdm10cr7vcv5akvz1x73yxqw85hndc")))

(define-public crate-disjoint-sets-0.3.5 (c (n "disjoint-sets") (v "0.3.5") (h "0y95scrr1002zmqkkc5zdikajjj2in6syqjd3z6d8hhaq96qkha0")))

(define-public crate-disjoint-sets-0.4.0 (c (n "disjoint-sets") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02nclwzliyyp6n33qzj0w1x1bm87m218pgzz5rgqxvk2097v359a")))

(define-public crate-disjoint-sets-0.4.1 (c (n "disjoint-sets") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1q09aa3ljhl2jqv59skf6zqkchvp57r5jy4js8dwszkb0mnfh5nm")))

(define-public crate-disjoint-sets-0.4.2 (c (n "disjoint-sets") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d1x5k2qhszrf6323w3f2ynzr9dl9dnn2p2kl9y00ix2sskvrjsc")))

