(define-module (crates-io di sj disjoint_impls) #:use-module (crates-io))

(define-public crate-disjoint_impls-0.1.0 (c (n "disjoint_impls") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1yy7inx0db5gb9h970kzfm3jjc0y72q9qkqli6788x0qnz9xmxax")))

(define-public crate-disjoint_impls-0.2.0 (c (n "disjoint_impls") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1wds7vi4dhvqgjj9c27d2xjxyj36hp9ljnp5n63hfkp8vn11lax4")))

(define-public crate-disjoint_impls-0.3.0 (c (n "disjoint_impls") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1hb17jpwf84yabk6fdbp15d9y6b11c2sd9mbhmw9bx3p1dfs57gl")))

(define-public crate-disjoint_impls-0.4.0 (c (n "disjoint_impls") (v "0.4.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0v88rbkf09krks34fi2gfvcrg4b2abwygakh8q25rcnkywcma70i")))

(define-public crate-disjoint_impls-0.5.0 (c (n "disjoint_impls") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0iil8dl9nbkdf1j1dsl2p22k55zsyk4z1s6hxirn7228v5rqrvc6")))

(define-public crate-disjoint_impls-0.6.0 (c (n "disjoint_impls") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0pjsp96q86bap1l2nk54iqpapdv3y25xz3b1j22q49zgcxisn1il")))

(define-public crate-disjoint_impls-0.7.0 (c (n "disjoint_impls") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1ag6jlisdlm14042f5mhsbr1li7bh7qpc4n0b08cmji7df89fqim")))

(define-public crate-disjoint_impls-0.7.1 (c (n "disjoint_impls") (v "0.7.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "055m1fyj4yfrr8fmzqhlc8qbswq885sx92n8y76b360f8iba1vh0")))

