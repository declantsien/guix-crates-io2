(define-module (crates-io di r_ dir_update) #:use-module (crates-io))

(define-public crate-dir_update-0.1.0 (c (n "dir_update") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0a5hjcf1ndp08lyvkfz9bngjhc51zwy0r0s272grwr2ls1yckp64")))

(define-public crate-dir_update-0.2.0 (c (n "dir_update") (v "0.2.0") (d (list (d (n "structopt") (r "^0.2") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "17fwp22sbzwg0jgxzr64w7lbj7msvy3z79vay5p0g362wd8ibcan")))

