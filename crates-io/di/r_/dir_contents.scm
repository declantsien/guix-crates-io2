(define-module (crates-io di r_ dir_contents) #:use-module (crates-io))

(define-public crate-dir_contents-0.1.0 (c (n "dir_contents") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qzycs8cci6936rswmvl378fs6viisaxgvdwf0jv70z2qikn5rvh")))

(define-public crate-dir_contents-0.1.1 (c (n "dir_contents") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0df7w8xpm874g420aw0m4g1wf8zhcd396bywn1vfa8qflmfci09g")))

