(define-module (crates-io di r_ dir_indexer) #:use-module (crates-io))

(define-public crate-dir_indexer-0.0.1 (c (n "dir_indexer") (v "0.0.1") (h "12lff431qklrdbpzmnq7r7dh84236yjngn0bpvvb5a6cllrw2yh1") (r "1.56.0")))

(define-public crate-dir_indexer-0.0.2 (c (n "dir_indexer") (v "0.0.2") (h "02may0xxhdb2g0nfsw02n2w87nrkakwa4jind5x3in5qgj142din") (r "1.56.0")))

