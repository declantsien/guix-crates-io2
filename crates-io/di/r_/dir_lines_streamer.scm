(define-module (crates-io di r_ dir_lines_streamer) #:use-module (crates-io))

(define-public crate-dir_lines_streamer-0.1.0 (c (n "dir_lines_streamer") (v "0.1.0") (d (list (d (n "alphanumeric-sort") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15l12rfwlzw2pkxydvjs4dzr9yjga7plnc487ahn0231m6ng57vb")))

(define-public crate-dir_lines_streamer-0.2.0 (c (n "dir_lines_streamer") (v "0.2.0") (d (list (d (n "alphanumeric-sort") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06qf3k3jplm1vn8qpgns6qrjmdw4kc023682sg82yqjqjzgf9w07") (f (quote (("gz" "flate2") ("default" "gz"))))))

