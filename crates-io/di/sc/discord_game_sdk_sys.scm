(define-module (crates-io di sc discord_game_sdk_sys) #:use-module (crates-io))

(define-public crate-discord_game_sdk_sys-0.1.0 (c (n "discord_game_sdk_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)))) (h "0rfb8yhlpq8hxh9icpwafrd7gdzkf6i5jzd6qgzpvwni3s8ghjyx") (f (quote (("no_linking"))))))

(define-public crate-discord_game_sdk_sys-0.2.0 (c (n "discord_game_sdk_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1zn06b2sz176439hivv71w6bih887ziiwpf2q6kl97v8jaj1x35k") (f (quote (("mock") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.3.0 (c (n "discord_game_sdk_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.50.0") (d #t) (k 1)))) (h "1kh3wg1d56f7ng0igmvzvzqcybfd9hma4jbfbwn8q6dx8h18i8hb") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.4.0 (c (n "discord_game_sdk_sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "042ia320dspr5plgpjwgfj3xyp1prfaxa5895z4dmpwpw1sdb3c0") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.4.1 (c (n "discord_game_sdk_sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1qgdsc0kb624hcdz7rsmq3z87yl05znl1cfdwcqpgvhnpz6fvaqb") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.4.2 (c (n "discord_game_sdk_sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "01dh1zzvwkjyqfxrqsl9fq91appanw1i4vfiif4aj1kp14v7j15a") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.5.0-alpha.1 (c (n "discord_game_sdk_sys") (v "0.5.0-alpha.1") (d (list (d (n "bindgen") (r "^0.52.0") (k 1)))) (h "1gzywswzm80nafjjnfwpj6pdncy5hbyknh1b6x3c7gk4xigqfc3j") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-0.5.0-alpha.2 (c (n "discord_game_sdk_sys") (v "0.5.0-alpha.2") (d (list (d (n "bindgen") (r "^0.52.0") (f (quote ("runtime"))) (k 1)))) (h "0pkpd2db2dzrrm94ajiwsnbsyzs3i4hspb8mbh9l2haimqvcbqvh") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-1.0.0-rc.1 (c (n "discord_game_sdk_sys") (v "1.0.0-rc.1") (d (list (d (n "bindgen") (r "^0.52.0") (f (quote ("runtime"))) (k 1)))) (h "0bsxvk0jn0mc03hywgr9xljvwxbrmyf8dsvdr6fp5gmhwyy7w1jp") (f (quote (("link") ("doc"))))))

(define-public crate-discord_game_sdk_sys-1.0.0 (c (n "discord_game_sdk_sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("runtime"))) (k 1)))) (h "0kv9g4i4rmdn57pmwazpp2fxjpsp5niqs9yw1l2ijsw8c6ism67k") (f (quote (("private-docs-rs") ("link"))))))

(define-public crate-discord_game_sdk_sys-1.0.1 (c (n "discord_game_sdk_sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("runtime"))) (k 1)))) (h "0vclmyvracf59ysbprnlm6jfmpdmbryi00bn0p1r8khx8kpylhh4") (f (quote (("private-docs-rs") ("link"))))))

