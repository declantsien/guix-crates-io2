(define-module (crates-io di sc discord-md) #:use-module (crates-io))

(define-public crate-discord-md-1.0.0-rc.1 (c (n "discord-md") (v "1.0.0-rc.1") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "18kv72cmy7ssswjmbc0bn4w9c3vh1x7k9mlxb75xgdmk56fk0l3c")))

(define-public crate-discord-md-1.0.0 (c (n "discord-md") (v "1.0.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)))) (h "0gcnmx6a5nns4mkbkjgx2qpdcwqlv7bhh5g8ys8bmn0awz21kcfy")))

(define-public crate-discord-md-2.0.0-rc.1 (c (n "discord-md") (v "2.0.0-rc.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "11i50fwrs9xzdrj9ix0304nfy0q995lpvbibcpdga3lmgxpikb7f")))

(define-public crate-discord-md-2.0.0-rc.2 (c (n "discord-md") (v "2.0.0-rc.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1n59rkpm95vbz2rrbwfz1f1z0ivmbzm8i9p63vzfv3fqcycxqqx5")))

(define-public crate-discord-md-2.0.0-rc.3 (c (n "discord-md") (v "2.0.0-rc.3") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "11bdnmbiqw7l34x53dl0arh0bg0f9gdmp75sm6l9ym7h9cyqzi1l")))

(define-public crate-discord-md-2.0.0 (c (n "discord-md") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1c97jcrjwx2rkryq9ppwljqk7rkrwbl10hznda7v71rsih7xfaxm")))

(define-public crate-discord-md-2.0.1 (c (n "discord-md") (v "2.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "094qz5cwzzbdrcl4z931kkjb0kzk9q5c9agv1ss9r3kmn7nnm6zp")))

(define-public crate-discord-md-3.0.0-rc.1 (c (n "discord-md") (v "3.0.0-rc.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0f4irh3nnlb0zhd6cxmcipdm1ak9m7laic600j90l180jag1f9y9")))

(define-public crate-discord-md-3.0.0 (c (n "discord-md") (v "3.0.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0szr7vgrg74m068qlkqdm1vwwyb7rq1z0yk7szz9yg9irxklhwxz")))

