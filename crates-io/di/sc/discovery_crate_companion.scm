(define-module (crates-io di sc discovery_crate_companion) #:use-module (crates-io))

(define-public crate-discovery_crate_companion-1.3.0 (c (n "discovery_crate_companion") (v "1.3.0") (h "1dix1n01in1g1khl7gvjycnv718cyizqid176m9ys88ivpwzks36")))

(define-public crate-discovery_crate_companion-1.4.0 (c (n "discovery_crate_companion") (v "1.4.0") (d (list (d (n "discovery_crate") (r ">1.0") (d #t) (k 0)))) (h "02bkmihn312f2aaiy1ablamg3172yyxnk69ihyw8qx003l10f9gq")))

(define-public crate-discovery_crate_companion-1.9.0 (c (n "discovery_crate_companion") (v "1.9.0") (d (list (d (n "discovery_crate") (r ">1.1") (d #t) (k 0)))) (h "0zrafi2h5iw5xrc93nqxnnbjws2nzjsq9nzn4rz8zl51bp61ldma")))

