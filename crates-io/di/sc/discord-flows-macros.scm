(define-module (crates-io di sc discord-flows-macros) #:use-module (crates-io))

(define-public crate-discord-flows-macros-0.1.0 (c (n "discord-flows-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serenity") (r "^0.11.5") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00ngcyg46rrhxp7hc1yfyymplgaza2lxw152m6vqhy89hiaw1fwy")))

(define-public crate-discord-flows-macros-0.1.1 (c (n "discord-flows-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serenity") (r "^0.11.5") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hmcbk4k56jlf9hnclw5wqc2772ihwdwmqli0y9xxbij7sklz3ki")))

(define-public crate-discord-flows-macros-0.1.2 (c (n "discord-flows-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serenity") (r "^0.11.5") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mgpnxcahi3b61hkn4pprnlxdi17nfc6xz3sgfcckpngq3vm6g2b")))

