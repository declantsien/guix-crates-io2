(define-module (crates-io di sc discord-toggle-rpc) #:use-module (crates-io))

(define-public crate-discord-toggle-rpc-0.1.0 (c (n "discord-toggle-rpc") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "16gw5pcfdjmka56bfxbmj5sdl5k1nxw3z7c3hv9a05sx40p3b008")))

