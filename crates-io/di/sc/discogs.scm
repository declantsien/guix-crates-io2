(define-module (crates-io di sc discogs) #:use-module (crates-io))

(define-public crate-discogs-0.1.0 (c (n "discogs") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.13") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0n7q092fdd2g9x662pk10x3yw51zzwygyh8xsfnnmqkrq5caxfap")))

(define-public crate-discogs-0.2.0 (c (n "discogs") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "mockito") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.6") (d #t) (k 0)))) (h "0gk7aqnvla1w79xqrk5lvyjv8i1py0sdsgrn4nk50spry561a9ms")))

(define-public crate-discogs-0.2.1 (c (n "discogs") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "mockito") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.6") (d #t) (k 0)))) (h "10xz9bcn17p1mwyzw1fnbm3gwi0h77zpvjbfkiw0m48bk1cn30lg") (f (quote (("nightly") ("default"))))))

(define-public crate-discogs-0.3.1 (c (n "discogs") (v "0.3.1") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "mockito") (r "^0.2.4") (d #t) (k 2)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.6") (d #t) (k 0)))) (h "13yh15cc55d0v5sz7s5x4qggfbcf171410gpm609cvg2brqmbcm2") (f (quote (("nightly") ("default"))))))

