(define-module (crates-io di sc discord-ipc-rp) #:use-module (crates-io))

(define-public crate-discord-ipc-rp-0.1.0 (c (n "discord-ipc-rp") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "18fnvjgpg4s9561xl9vcjxdqwv2hcln5h7xa8vcsh95ji43kzsjh")))

(define-public crate-discord-ipc-rp-0.1.1 (c (n "discord-ipc-rp") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "07g85z599qrx2smsijvxxfcz33bdj9si59lsvzxy10f76wqdk3dw")))

