(define-module (crates-io di sc discord_typed_interactions) #:use-module (crates-io))

(define-public crate-discord_typed_interactions-0.1.0 (c (n "discord_typed_interactions") (v "0.1.0") (d (list (d (n "discord_typed_interactions_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "discord_typed_interactions_proc_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "09zzvp8684j99gn3zjadik0b0hkgr6rdywbzrcr21n545fwps1il") (f (quote (("macro" "discord_typed_interactions_proc_macro") ("builder"))))))

