(define-module (crates-io di sc discord-bots-org) #:use-module (crates-io))

(define-public crate-discord-bots-org-0.1.0 (c (n "discord-bots-org") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.12") (f (quote ("compat"))) (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0s8qxq4r2ia7mqm54mfk7c3w6ykx5wj3ly7wbjwy20xsm2nrv43p") (f (quote (("reqwest-sync-support" "reqwest") ("reqwest-async-support" "futures-preview" "reqwest") ("default" "reqwest-sync-support"))))))

