(define-module (crates-io di sc discord-rich-presence) #:use-module (crates-io))

(define-public crate-discord-rich-presence-0.1.1 (c (n "discord-rich-presence") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "128kvwrsj2syv1vmkvnmvxkpmmv6lri0779fb2j86dggf8p1drnp")))

(define-public crate-discord-rich-presence-0.1.2 (c (n "discord-rich-presence") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0g8arsjxsvnrr6nlnzwcr7qlz1xnxd3zxm48wvbg8lkl24h8r466")))

(define-public crate-discord-rich-presence-0.1.3 (c (n "discord-rich-presence") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1nhxyzc00m5q7sw4p23ij3x4ccq79sf2xghlj5kg6z11nxj1irnl")))

(define-public crate-discord-rich-presence-0.1.4 (c (n "discord-rich-presence") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1f9i6wh4mp650ckba5cnikcn4q578r9z7jm9xddf9f9fzpqy0fwq")))

(define-public crate-discord-rich-presence-0.1.5 (c (n "discord-rich-presence") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "02hrqyv2zhibam48ga2dqc0g7j627p5kqhawbhjkm9d2ys1919ll")))

(define-public crate-discord-rich-presence-0.1.6 (c (n "discord-rich-presence") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1jfnl8mpvcnby4f95yagr8qi3yapczkkhxdpimxgn7mr80pz7xcy")))

(define-public crate-discord-rich-presence-0.1.7 (c (n "discord-rich-presence") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1dgwnk7lsry0qk80g1p20faql0445x0n2nz4sabwgqd3x21pb7q5")))

(define-public crate-discord-rich-presence-0.1.8 (c (n "discord-rich-presence") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0vvqxrhpmrdmp85gzm61vc67qkp2212v9n8z1vzwll3ajhciljyy")))

(define-public crate-discord-rich-presence-0.2.0 (c (n "discord-rich-presence") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "windows-named-pipe") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1vxar89x51pqs0lfg167yq8awpcqyxcdbhw9b1wmg818lxz9kr25")))

(define-public crate-discord-rich-presence-0.2.1 (c (n "discord-rich-presence") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jp5v801f7dzp8j3k53hwilw381kld7wibqd81ank0ki55yh1q6n")))

(define-public crate-discord-rich-presence-0.2.2 (c (n "discord-rich-presence") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "08dhp8av8l7mwwaarifxvfgccc057jcw8p37ndrnp6lprsfhax16")))

(define-public crate-discord-rich-presence-0.2.3 (c (n "discord-rich-presence") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ra7i586raw2nbb16jwcfb3xq84w9lx0g6c4b1hi9vl5zgplpz27")))

(define-public crate-discord-rich-presence-0.2.4 (c (n "discord-rich-presence") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "111zdci74s2nprd5r8i0kv198n47z42i8szlvy7bzzlp7qga8cpn")))

