(define-module (crates-io di sc discord_rust_framework) #:use-module (crates-io))

(define-public crate-discord_rust_framework-0.0.1 (c (n "discord_rust_framework") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "websocket") (r "^0.20.2") (d #t) (k 0)))) (h "1n411828ci7pmjmqyy6pqyk61svw4jdw4zja8g6m2xwh6p9sqxak")))

