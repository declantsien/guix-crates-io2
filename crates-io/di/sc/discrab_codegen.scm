(define-module (crates-io di sc discrab_codegen) #:use-module (crates-io))

(define-public crate-discrab_codegen-0.1.0 (c (n "discrab_codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "1qi1c8gy3hl5p5g2ksy9xqz0qbsrgp7l5f882y58yndlxwb7bp2k")))

(define-public crate-discrab_codegen-0.5.0 (c (n "discrab_codegen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full"))) (d #t) (k 0)))) (h "0p7agzyjdff7g14bhg5lgzvybxrsjqcnv8hjyazfd437wc3v3ajn")))

