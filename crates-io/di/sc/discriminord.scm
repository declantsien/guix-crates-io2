(define-module (crates-io di sc discriminord) #:use-module (crates-io))

(define-public crate-discriminord-0.1.0 (c (n "discriminord") (v "0.1.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1wg1yrjj7k2snz9wsc2hb96c7p55fs0iic606vf4r9ankdgv7vb6")))

(define-public crate-discriminord-0.2.0 (c (n "discriminord") (v "0.2.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0n081r6czyrwhw7f88939c447rwpwvg8xrv7b56265ilparcxl44")))

(define-public crate-discriminord-0.3.0 (c (n "discriminord") (v "0.3.0") (d (list (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0mxc43kr72kyvw901ia9jimcc5szbmsklxx9bkys85qc0j6pncv4")))

