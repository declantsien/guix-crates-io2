(define-module (crates-io di sc discloud-rs) #:use-module (crates-io))

(define-public crate-discloud-rs-0.1.0-alpha (c (n "discloud-rs") (v "0.1.0-alpha") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1i53y1kajjk5qgw1w0jsha6s8p33729kx7p4nj09fxc6igvf95jh")))

