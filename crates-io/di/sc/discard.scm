(define-module (crates-io di sc discard) #:use-module (crates-io))

(define-public crate-discard-1.0.0 (c (n "discard") (v "1.0.0") (h "17i2nhhkvi720pg0f9pqg2fsvs66rd6axxnp2xnfb7sw40z3qqcb")))

(define-public crate-discard-1.0.1 (c (n "discard") (v "1.0.1") (h "0pd2vvl9pqcgasrxv9xlc1y5bs1rf1gvbdc4pvamwqp60dg6s96x")))

(define-public crate-discard-1.0.2 (c (n "discard") (v "1.0.2") (h "0zi09kjk85vlazg948ai2sva85kpw5a50ldmg84xyc3bgwqllm5p")))

(define-public crate-discard-1.0.3 (c (n "discard") (v "1.0.3") (h "1gk5ckkyphmdxg8jflccqw09yr9dypy7zjp2p1y6bid35m81g4cs")))

(define-public crate-discard-1.0.4 (c (n "discard") (v "1.0.4") (h "1h67ni5bxvg95s91wgicily4ix7lcw7cq0a5gy9njrybaibhyb91")))

