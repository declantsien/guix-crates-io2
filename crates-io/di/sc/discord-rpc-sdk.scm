(define-module (crates-io di sc discord-rpc-sdk) #:use-module (crates-io))

(define-public crate-discord-rpc-sdk-0.1.0 (c (n "discord-rpc-sdk") (v "0.1.0") (d (list (d (n "discord-rpc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "17k25hqjbpdljxark5f4f077bssdxsj0v6bdqs9y7jhbsrf2mmlb")))

(define-public crate-discord-rpc-sdk-0.1.1 (c (n "discord-rpc-sdk") (v "0.1.1") (d (list (d (n "discord-rpc-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)))) (h "1kr89n2qgrm6aarq4jfv8jppk7z9zfgx6yvhcy2f1j3v7yyk519w")))

