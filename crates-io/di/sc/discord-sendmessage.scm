(define-module (crates-io di sc discord-sendmessage) #:use-module (crates-io))

(define-public crate-discord-sendmessage-0.1.0 (c (n "discord-sendmessage") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "discord-message") (r "^0.1.0") (d #t) (k 0)) (d (n "discord-webhook-client") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "19294zljnvp6pgbj491pdcl3kpxa7i6wm75zv4jj1xhn1v7vhn9x")))

