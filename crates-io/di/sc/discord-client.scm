(define-module (crates-io di sc discord-client) #:use-module (crates-io))

(define-public crate-discord-client-0.1.0 (c (n "discord-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0skf5cnp38yq25g3csk4rwjw43pi3hwrqzhmky09fm8ap0g33glj")))

(define-public crate-discord-client-0.1.1 (c (n "discord-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0lbffrqqkwh4ddh449mqg7h2nxi68lxp90gsaja11zzbqqiyzfq9")))

