(define-module (crates-io di sc discrete-logarithm) #:use-module (crates-io))

(define-public crate-discrete-logarithm-0.0.0 (c (n "discrete-logarithm") (v "0.0.0") (d (list (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)))) (h "0pyv9ayz91rs1cn0jr2ziz7120svazvc816k2fn59gd1xjpmgp33")))

(define-public crate-discrete-logarithm-0.1.0 (c (n "discrete-logarithm") (v "0.1.0") (d (list (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p68l8v00mywzr5fp1d10icid5qlr1ahkks06j7d26ggyfjcxscd")))

