(define-module (crates-io di sc disco-quick) #:use-module (crates-io))

(define-public crate-disco-quick-0.1.0 (c (n "disco-quick") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0an86x5m62823daaak2xyvn5b2haryydcb3rhqp5bqazkaq0xwbl") (f (quote (("default"))))))

(define-public crate-disco-quick-0.1.1 (c (n "disco-quick") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mnq1i03yv9vcij5r2vwxq6g1qsbia4aklchl29d2fizhh6paa8b") (f (quote (("default"))))))

