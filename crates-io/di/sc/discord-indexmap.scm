(define-module (crates-io di sc discord-indexmap) #:use-module (crates-io))

(define-public crate-discord-indexmap-1.4.0 (c (n "discord-indexmap") (v "1.4.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "fnv") (r "^1.0") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.99") (d #t) (k 2)))) (h "1183wlbj3dz7xc1hy6d8nvymjh9dxj0pidxn9giph0vwd89qikpm") (f (quote (("test_low_transition_point") ("test_debug") ("serde-1" "serde"))))))

