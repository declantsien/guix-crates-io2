(define-module (crates-io di sc discosearch-rs) #:use-module (crates-io))

(define-public crate-discosearch-rs-0.1.0 (c (n "discosearch-rs") (v "0.1.0") (h "092srcj0lxph7lxg859gg9rwf35j78lhpr1z3ai6mhc7lmfi5f0v")))

(define-public crate-discosearch-rs-0.1.1 (c (n "discosearch-rs") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0fbblqdsnzlgczrm7qql49px4xllrx6h0s46yb48qjjanwyligx7")))

(define-public crate-discosearch-rs-0.1.2 (c (n "discosearch-rs") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0910yp6m8lxl54a93q7rxczkalwkgsq98wby6biw9vrha1yqs347")))

