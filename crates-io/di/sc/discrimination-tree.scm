(define-module (crates-io di sc discrimination-tree) #:use-module (crates-io))

(define-public crate-discrimination-tree-0.1.0 (c (n "discrimination-tree") (v "0.1.0") (d (list (d (n "id-arena") (r "^2.2") (d #t) (k 0)))) (h "1ys3q9hfbihzhcdccc7ifm2wnqhd65dix686scc6wchl6s0iqyfr")))

(define-public crate-discrimination-tree-0.1.1 (c (n "discrimination-tree") (v "0.1.1") (h "1vi0bi80y2ncg2v9s7mzlz42236yx0f8iabj6ylvfamzdlfkf8d1")))

(define-public crate-discrimination-tree-0.2.0 (c (n "discrimination-tree") (v "0.2.0") (h "146pmpd7fng9sm5j6rgz9v4zmibkdlncac9a583ni586sqjd3xg3")))

(define-public crate-discrimination-tree-0.2.1 (c (n "discrimination-tree") (v "0.2.1") (h "035qrzb3c1jrp4s8z2cmcij3lw6794qgimvlp23x6119kdn59mx9")))

(define-public crate-discrimination-tree-0.3.0 (c (n "discrimination-tree") (v "0.3.0") (h "1lp61mkskbwy186x89wmkwsz9sa8x018v3pyqxfcyr8sxhqa9kf1")))

