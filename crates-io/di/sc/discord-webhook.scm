(define-module (crates-io di sc discord-webhook) #:use-module (crates-io))

(define-public crate-discord-webhook-3.0.0 (c (n "discord-webhook") (v "3.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1prz0xgbwm9jm58av5gn540pkzr4a8sxrg5kmg1afvc1v1l5hnyn") (f (quote (("models") ("full" "client" "models") ("default" "client") ("client" "reqwest")))) (y #t)))

(define-public crate-discord-webhook-0.1.0 (c (n "discord-webhook") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "16abyl27bvpw7sq8yzcf3w6rbhl1vp001c8wwh3mwc5pb7dm6i6v") (f (quote (("models") ("full" "client" "models") ("default" "client") ("client" "reqwest"))))))

