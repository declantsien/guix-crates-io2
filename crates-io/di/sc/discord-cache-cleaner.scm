(define-module (crates-io di sc discord-cache-cleaner) #:use-module (crates-io))

(define-public crate-discord-cache-cleaner-0.1.0 (c (n "discord-cache-cleaner") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0m3w6ycnf0vqrp93snf2wph4gi21va3cd2s2k46s5rnis8i2pf6z")))

