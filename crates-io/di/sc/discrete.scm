(define-module (crates-io di sc discrete) #:use-module (crates-io))

(define-public crate-discrete-0.0.0 (c (n "discrete") (v "0.0.0") (h "1vglzd80i6yimns23bw6yir362gcs56cp032brras9kf76lpyw24")))

(define-public crate-discrete-0.0.1 (c (n "discrete") (v "0.0.1") (h "1l137krxhzvip0rn0wsypsaf7qz8q5szdr082zycmvzxz6apmsac")))

(define-public crate-discrete-0.0.5 (c (n "discrete") (v "0.0.5") (h "0jriwlfkgg73wn2dza8c5f86wwfgnxqmyp5w09rkpr2g72nvn68y")))

(define-public crate-discrete-0.0.6 (c (n "discrete") (v "0.0.6") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0724rpvx3xpjgr15w1yjjy66ga8nhr8dhpxwp5vh2msskw3zxskk")))

(define-public crate-discrete-0.0.7 (c (n "discrete") (v "0.0.7") (h "063yklgnjvhb65ib6ijgsb8q8qg8lk6fza9j50l4g8qh5palkbml")))

(define-public crate-discrete-0.1.0 (c (n "discrete") (v "0.1.0") (h "1hwaqlvry1z12jzh07rmzkfnv894yd1ksgrhh168frq0z6c0x6z1")))

(define-public crate-discrete-0.2.0 (c (n "discrete") (v "0.2.0") (h "0dwj8kd94zll6jqvilx3q73i31434m21r26d505qyq26q057lf89")))

(define-public crate-discrete-0.2.1 (c (n "discrete") (v "0.2.1") (h "0nkzfzry7iwlnymkwxz5sipy0whxmd03cslpwznwkpnx7fsdx3hw")))

(define-public crate-discrete-0.3.0 (c (n "discrete") (v "0.3.0") (h "12ibppmvz4cgn3l846ybdfz7dpddwdqrjydrfqw14bidxqid2zyg")))

(define-public crate-discrete-0.4.0 (c (n "discrete") (v "0.4.0") (h "1djw6wn0avznn08z5kwq0fcaqr36z3gq5kfi54gvfx0q06v65ja9")))

(define-public crate-discrete-0.5.0 (c (n "discrete") (v "0.5.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1vpkc8id82p7q7yb28c8gh4a07x4g1mkk66n26ixbc6fhkhgxbx2")))

