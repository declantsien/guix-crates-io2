(define-module (crates-io di sc discombobulate) #:use-module (crates-io))

(define-public crate-discombobulate-0.1.0 (c (n "discombobulate") (v "0.1.0") (h "1ksh30n6qw93i9c0l3w8znv3chglf3986fd8x7i68siyaywjyh8i")))

(define-public crate-discombobulate-0.1.2 (c (n "discombobulate") (v "0.1.2") (h "0lfmnv2jax3zdyy4d39184frll5d135z70hzjizf08bvif0vbcqx")))

(define-public crate-discombobulate-0.1.3 (c (n "discombobulate") (v "0.1.3") (h "1cv4qda5199if4s7h11lg229zh3443cii8fdxaz8b6h2psbf9gaz")))

