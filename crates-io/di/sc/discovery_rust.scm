(define-module (crates-io di sc discovery_rust) #:use-module (crates-io))

(define-public crate-discovery_rust-0.1.0 (c (n "discovery_rust") (v "0.1.0") (d (list (d (n "consul_rust") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1h8imqwfqraazh0g9hv01ndq4zkz1a80vnyzx44w952dix65nkpk") (y #t)))

(define-public crate-discovery_rust-0.1.1 (c (n "discovery_rust") (v "0.1.1") (d (list (d (n "consul_rust") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "069n12bar9mji3kylh9y5abi1wbbv8b4316g59r36ql01vn7x2mn") (y #t)))

