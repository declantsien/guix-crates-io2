(define-module (crates-io di sc discord-ipc) #:use-module (crates-io))

(define-public crate-discord-ipc-0.0.1-alpha.1 (c (n "discord-ipc") (v "0.0.1-alpha.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "188jksv7d8x1y1big4m112kss83nkzkn8hxwlkaf412wys3avhi2") (y #t)))

