(define-module (crates-io di sc discotech_zookeeper) #:use-module (crates-io))

(define-public crate-discotech_zookeeper-0.1.0 (c (n "discotech_zookeeper") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "schedule_recv") (r "^0.0.1") (d #t) (k 0)))) (h "1b4aljp6anjzh3svrlvzvx5p7d9vld4n409nzg0j4q3bhivdaczq") (f (quote (("unstable"))))))

