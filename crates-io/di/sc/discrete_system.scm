(define-module (crates-io di sc discrete_system) #:use-module (crates-io))

(define-public crate-discrete_system-0.1.0 (c (n "discrete_system") (v "0.1.0") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "1r0fa20lc8rjz6lvxq02299wziadngr9g306m6g7lid78hqbfv52")))

(define-public crate-discrete_system-0.1.1 (c (n "discrete_system") (v "0.1.1") (d (list (d (n "moddef") (r "^0.1.0") (d #t) (k 0)))) (h "10xi01yzf2215ql89b9yv3ahkixf33qzvhc203kz8hrq66c2g15i")))

