(define-module (crates-io di sc discord-finder) #:use-module (crates-io))

(define-public crate-discord-finder-0.1.0 (c (n "discord-finder") (v "0.1.0") (d (list (d (n "minreq") (r "^2.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "1q8g5r9bsd5kc0918fqjch9bknlwdvdcl2chra1pdp8k6xsib9yl") (y #t)))

(define-public crate-discord-finder-0.1.1 (c (n "discord-finder") (v "0.1.1") (d (list (d (n "minreq") (r "^2.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "030cagqfg9wwc1d2s69b9zf0dc0rfmsfsjidxzswwqywvk72s06j") (y #t)))

(define-public crate-discord-finder-0.1.2 (c (n "discord-finder") (v "0.1.2") (d (list (d (n "minreq") (r "^2.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "04ccpxpjn4lfjqvmxppq2k8z1ypk0l3sq754hisk2py14m2cxxy5") (y #t)))

(define-public crate-discord-finder-0.1.3 (c (n "discord-finder") (v "0.1.3") (d (list (d (n "minreq") (r "^2.1") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "string-tools") (r "^0.1.0") (d #t) (k 0)))) (h "1vkk9vjfjy05f2hcpczkbjb6h8rp12p6s91xlj567pbhmdgj4rnd") (y #t)))

(define-public crate-discord-finder-0.0.0-do-not-use (c (n "discord-finder") (v "0.0.0-do-not-use") (h "1a5zrccrb0ydlfzgs2fk3hyl2xcjv86xpaa45xlpfppsipqqxhl5")))

