(define-module (crates-io di sc discord_game_sdk_mock) #:use-module (crates-io))

(define-public crate-discord_game_sdk_mock-0.1.0 (c (n "discord_game_sdk_mock") (v "0.1.0") (d (list (d (n "discord_game_sdk_sys") (r "^0.1.0") (f (quote ("no_linking"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "1my4mh3b9whn9f7pyjh99pyx6h5ql1yxi418cwpk2szkxbcs1hbk") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.0 (c (n "discord_game_sdk_mock") (v "0.2.0") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "1pgdhi58rxn57a513fzhbxr8f8mlzc7npz0pkz2a3x731n4v7lsw") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.1 (c (n "discord_game_sdk_mock") (v "0.2.1") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "0qx64dsnjf05chq6nca2fsly15pv1xw2b2g4zq9ngmshm3ym7zyy") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.2 (c (n "discord_game_sdk_mock") (v "0.2.2") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "15l9gpyy97n7hsa6zy45kq6qh1pskv3arks22b0gd4dzvs23f498") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.3 (c (n "discord_game_sdk_mock") (v "0.2.3") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "0ay4brnh2m6d72g1zhpr3x6imhvpx17jhs9zi39pb3sy3pqhfwdx") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.4 (c (n "discord_game_sdk_mock") (v "0.2.4") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "14bjcyfm8y46bhkhh19ks2hh250g2cjria93c2ijzrngijiybs52") (y #t)))

(define-public crate-discord_game_sdk_mock-0.2.5 (c (n "discord_game_sdk_mock") (v "0.2.5") (d (list (d (n "discord_game_sdk_sys") (r "^0.2.0") (f (quote ("mock"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "memoffset") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0.0") (d #t) (k 0)))) (h "1m314fm905hhw1ic211i863yb2amcrn4bggxccig36n4qfz3rwn4") (y #t)))

