(define-module (crates-io di sc disco-cli) #:use-module (crates-io))

(define-public crate-disco-cli-0.1.0 (c (n "disco-cli") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "discorec") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "18c97wcs7mqx3lfdmzxgb1z6qnj80mw93l6cj8yqm43vklgwwkz4")))

