(define-module (crates-io di sc discord-base) #:use-module (crates-io))

(define-public crate-discord-base-0.1.0 (c (n "discord-base") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "once_cell") (r "^1.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serenity") (r "^0.10") (f (quote ("cache" "collector" "model" "standard_framework" "rustls_backend"))) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite"))) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (k 0)))) (h "0m58w8l7zg40pqirb5748m3ng06fxpsrkzxcgajs287xhsw2a7mr") (y #t)))

