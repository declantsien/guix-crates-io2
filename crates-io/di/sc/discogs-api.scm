(define-module (crates-io di sc discogs-api) #:use-module (crates-io))

(define-public crate-discogs-api-0.0.1 (c (n "discogs-api") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cqswd4rjx199aibcis6iv3956xyh5p4g2ldpyd9w4il4wgmsczz")))

(define-public crate-discogs-api-0.0.3 (c (n "discogs-api") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "174q6yvjfsnvysligiz7r3npx7kwnx06cdszy8s4wmi6rcbl2bmj")))

(define-public crate-discogs-api-0.0.4 (c (n "discogs-api") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bp3nvgh5b9dzlbg9mz14ly1n1r1a16902cg1m8a9q6jvwlyb3yk")))

