(define-module (crates-io di sc discovery-connect) #:use-module (crates-io))

(define-public crate-discovery-connect-1.0.0 (c (n "discovery-connect") (v "1.0.0") (d (list (d (n "base64-url") (r "^2.0.0") (d #t) (k 0)) (d (n "graphql_client") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("brotli" "default-tls" "json" "stream"))) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1wbwb1mc0y5x43500ahxg64ws70a8n4wrrz678527inh809vkd4b")))

