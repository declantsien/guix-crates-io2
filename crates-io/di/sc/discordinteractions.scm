(define-module (crates-io di sc discordinteractions) #:use-module (crates-io))

(define-public crate-discordinteractions-1.0.0 (c (n "discordinteractions") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sn91ivf5v7icma3nwzi1rhlx4yxg4qrk0xknb3x28awhbcnrfc0") (f (quote (("serde_support" "serde" "serde_json"))))))

(define-public crate-discordinteractions-1.0.1 (c (n "discordinteractions") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "19mx43r288slj7frlwy3wzkmn6awk28ih49cv1161dmvhkp709ja") (f (quote (("serde_support" "serde" "serde_json"))))))

