(define-module (crates-io di sc discard-while) #:use-module (crates-io))

(define-public crate-discard-while-0.1.0 (c (n "discard-while") (v "0.1.0") (h "0cby6m6982xc8i8cicg1wcql5z9gm4r1nixcms0q98xi42jca77h")))

(define-public crate-discard-while-0.1.1 (c (n "discard-while") (v "0.1.1") (h "0rczccy9k36xlk49i26xmjp986j2746i2d4akiiampy2i87p21cx")))

(define-public crate-discard-while-0.1.2 (c (n "discard-while") (v "0.1.2") (h "080gpbx5mgcfz13wy64gvp3rzibb4bvg5nhcxinkn6zxypxafhdq")))

(define-public crate-discard-while-0.1.3 (c (n "discard-while") (v "0.1.3") (h "1p5h827visx4219bc40bc2ln5mm31bbaa128y34rv96l4vi28grd")))

(define-public crate-discard-while-0.1.4 (c (n "discard-while") (v "0.1.4") (h "195xf2l3daj7c9kwfv2jvpmy89z9pr8py6nq1l990771hy48pmyz")))

(define-public crate-discard-while-0.1.5 (c (n "discard-while") (v "0.1.5") (h "02rf6j0n8r8sxglrklv7njd4jhlny9qnr9k2nw3mjdvjkxf90ynf")))

(define-public crate-discard-while-0.1.6 (c (n "discard-while") (v "0.1.6") (h "0wqzkfcgx86vmjlaziygv74lik5apkqcixa0awn6a0hb4dlwz1z1")))

