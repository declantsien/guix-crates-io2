(define-module (crates-io di sc discord-termview) #:use-module (crates-io))

(define-public crate-discord-termview-0.1.0 (c (n "discord-termview") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "serenity") (r "^0.10") (f (quote ("client" "gateway" "rustls_backend" "model" "cache"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread" "process"))) (d #t) (k 0)))) (h "1pcvg320a5di9q3r0hn6f927iflw1b7bc0kvqfhx7nwjjvd2yrg7")))

