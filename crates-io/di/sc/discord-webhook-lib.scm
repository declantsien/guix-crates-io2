(define-module (crates-io di sc discord-webhook-lib) #:use-module (crates-io))

(define-public crate-discord-webhook-lib-0.1.3 (c (n "discord-webhook-lib") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "serde_with") (r "^3.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vpfxprna801ciwzv0b3sah9m953qldjcfj727jly0vzqyz28jgh") (y #t)))

(define-public crate-discord-webhook-lib-0.1.4 (c (n "discord-webhook-lib") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "serde_with") (r "^3.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0whnpal49zch6wwrby9sg1xjgxa7yf7vqc4ns7232m9ijb3hiphz") (y #t)))

(define-public crate-discord-webhook-lib-0.1.5 (c (n "discord-webhook-lib") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pf8wa45ri33pn7bjz5s1jbrhinlaq0fi1mm8nm33qgijypyzs04") (y #t)))

(define-public crate-discord-webhook-lib-0.1.7 (c (n "discord-webhook-lib") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1989crh4mfhlx9x9jdcnajhpnc5wsfrmfabi56fs614mik9x9z78")))

