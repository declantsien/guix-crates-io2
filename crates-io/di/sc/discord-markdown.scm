(define-module (crates-io di sc discord-markdown) #:use-module (crates-io))

(define-public crate-discord-markdown-0.1.0 (c (n "discord-markdown") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (f (quote ("regexp"))) (d #t) (k 0)))) (h "0398jjgkrlz4h56nx31r2v06bi8zrafrrv4cd5qzxhwd8qlwjxp7")))

(define-public crate-discord-markdown-0.1.1 (c (n "discord-markdown") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (f (quote ("regexp"))) (d #t) (k 0)))) (h "0yh322fz3hrvrz2516s36fyx828vm3nblc6yqwsxv3wimn2jnwkq")))

(define-public crate-discord-markdown-0.1.2 (c (n "discord-markdown") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (f (quote ("regexp"))) (d #t) (k 0)))) (h "0lfl22x7zzkz9b6rmy9z5k0fyz5mks2y2c6qqmhr1ajcf156khs8")))

