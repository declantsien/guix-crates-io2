(define-module (crates-io di sc discord-webhook-client) #:use-module (crates-io))

(define-public crate-discord-webhook-client-0.1.0 (c (n "discord-webhook-client") (v "0.1.0") (d (list (d (n "discord-message") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "04kv9fb74bqzyyj5srpbwydc60dvykq7kynfvpnr29ch4zh4w0va")))

