(define-module (crates-io di sc discrim) #:use-module (crates-io))

(define-public crate-discrim-0.0.1 (c (n "discrim") (v "0.0.1") (d (list (d (n "discrim-codegen") (r "^0.0.1") (d #t) (k 0)))) (h "0n6sa7hxnxglpk9lx5505lq464lh0bhaidxzmb6p0826vnsvnvmq")))

(define-public crate-discrim-0.0.2 (c (n "discrim") (v "0.0.2") (d (list (d (n "discrim-codegen") (r "^0.0.2") (d #t) (k 0)))) (h "0xqamlsk7yfhn8ll8z1rd3ax5p8hhwhffw45vsxlg264yx50znx0")))

(define-public crate-discrim-0.1.0 (c (n "discrim") (v "0.1.0") (d (list (d (n "discrim-codegen") (r "^0.1.0") (d #t) (k 0)))) (h "0x3gvya0lhb1z8mmi9fc6bsc5lv6d5dxdvy6f6xgxlqc6m75lb1p")))

(define-public crate-discrim-0.1.1 (c (n "discrim") (v "0.1.1") (d (list (d (n "discrim-codegen") (r "^0.1.1") (d #t) (k 0)))) (h "1hhpip60rfy2iqzcphqc3xc0iq0x8zifnjyz2vncknlc27wlbj61")))

(define-public crate-discrim-0.1.2 (c (n "discrim") (v "0.1.2") (d (list (d (n "discrim-codegen") (r "^0.1.1") (d #t) (k 0)))) (h "0vihkkpx3pj5mgxsfbix3m09sr9s09vc2pcwsjy8aqkqj8f0745w")))

