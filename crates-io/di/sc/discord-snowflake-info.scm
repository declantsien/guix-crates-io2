(define-module (crates-io di sc discord-snowflake-info) #:use-module (crates-io))

(define-public crate-discord-snowflake-info-1.0.0 (c (n "discord-snowflake-info") (v "1.0.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)))) (h "1p3ccrakrk9q1c6wjhwlv32whr2mzkgm5ndf4id0wg8cdfs72a2r")))

(define-public crate-discord-snowflake-info-1.1.0 (c (n "discord-snowflake-info") (v "1.1.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)))) (h "038sqmqxl2i9mm9d9ykw09fqxijhy7zg1gilha2ikp82dig2yivm")))

(define-public crate-discord-snowflake-info-1.2.0 (c (n "discord-snowflake-info") (v "1.2.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)))) (h "0dii146ffj0pggl3ax978zv4iwvnd65170296fy5n85y8162m3dj") (f (quote (("result") ("default"))))))

(define-public crate-discord-snowflake-info-1.2.1 (c (n "discord-snowflake-info") (v "1.2.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)))) (h "1yn4ybqmcjdq1k449ivhdsw7ky36jvlmrcwcy3jxb9cph5x967y3") (f (quote (("result") ("default"))))))

