(define-module (crates-io di sc discord_typed_interactions_lib) #:use-module (crates-io))

(define-public crate-discord_typed_interactions_lib-0.1.0 (c (n "discord_typed_interactions_lib") (v "0.1.0") (d (list (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09jk3mrkj8r8bqs9s8fjs6yf7aigk8iyzwlxgjbir0dgiqmj564j")))

