(define-module (crates-io di sc discordrpc) #:use-module (crates-io))

(define-public crate-discordrpc-0.1.0 (c (n "discordrpc") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive" "cargo" "std"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.0.6") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.1.8") (d #t) (k 0)))) (h "0fqj8siffvb21xqgamqma2gqg3dybanbccfm03vgxij758agxivb")))

(define-public crate-discordrpc-0.2.0 (c (n "discordrpc") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "cargo" "std"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.0") (d #t) (k 0)))) (h "1gnq5rvvglmh08dplajwqx980h51pn3nk3m069lcal22126sn62q")))

(define-public crate-discordrpc-0.2.2 (c (n "discordrpc") (v "0.2.2") (d (list (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.4") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.1.6") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.0") (d #t) (k 0)))) (h "1r9gdk6qhfrqidpccy2f6vmq5hvjb58bfd0vdpv5z296r2wrkshc")))

