(define-module (crates-io di sc disclose) #:use-module (crates-io))

(define-public crate-disclose-0.9.1 (c (n "disclose") (v "0.9.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r ">=1, <2") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "1gpavq5k4ay74pyhidzdg1q0wbbp0mncm3z117by0br364ba9b4k")))

