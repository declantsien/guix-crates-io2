(define-module (crates-io di sc discord-rs) #:use-module (crates-io))

(define-public crate-discord-rs-0.1.1 (c (n "discord-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14v0a97997v1lkg70gvd90qfzh5r2dljp5fglw7mfrdjkq87pb6k") (r "1.70.0")))

