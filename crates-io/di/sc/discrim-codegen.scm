(define-module (crates-io di sc discrim-codegen) #:use-module (crates-io))

(define-public crate-discrim-codegen-0.0.1 (c (n "discrim-codegen") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("derive" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1kv4x46157w1ajkanw6w7qxji92bpnigc9riywmm7x5y4apyp212")))

(define-public crate-discrim-codegen-0.0.2 (c (n "discrim-codegen") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("derive" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0flwwwr8bpbqqas8as858q06sll38r7s1nnsgk5lvgvf4xn208xa")))

(define-public crate-discrim-codegen-0.1.0 (c (n "discrim-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "15p69ql1v3bi4y98gwkdl5jj103s51ncx3i1jazwnmqgzpdyqdzf")))

(define-public crate-discrim-codegen-0.1.1 (c (n "discrim-codegen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "1vdkwxs4swyp0yka8cj1w6psgy9pqqi7zlpxx1nwj5sni285hh4y")))

(define-public crate-discrim-codegen-0.1.2 (c (n "discrim-codegen") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.61") (d #t) (k 2)))) (h "0bvhjyslbzxg0q1imgmzz6av1ip3793smqkkdd4aiaszikc5rbhm")))

