(define-module (crates-io di sc discotech) #:use-module (crates-io))

(define-public crate-discotech-0.0.1 (c (n "discotech") (v "0.0.1") (d (list (d (n "discotech_zookeeper") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0lfymsblz1gqn4lkq70ka93vpgb6z77j1v1578fkszyjkvsq7fx9")))

(define-public crate-discotech-0.0.2 (c (n "discotech") (v "0.0.2") (d (list (d (n "discotech_zookeeper") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02szsm34lhi60bdq69nlq6jgqky4va135253p18dgqi9ramzapqf")))

(define-public crate-discotech-0.0.3 (c (n "discotech") (v "0.0.3") (d (list (d (n "discotech_zookeeper") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dciw0j9v104nvmmcv195l9cw5vbsvp88hvmh7xylaf8ddnr74n9")))

