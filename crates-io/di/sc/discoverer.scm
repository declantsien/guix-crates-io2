(define-module (crates-io di sc discoverer) #:use-module (crates-io))

(define-public crate-discoverer-0.0.0 (c (n "discoverer") (v "0.0.0") (h "1kw7s102516flkyyy8kz6a16k9jwr3jmq0kdwq40b1c9034rp72k")))

(define-public crate-discoverer-0.1.0 (c (n "discoverer") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0zf9ldcnq84hdlib3kggxf4axhwfg2zkznihnj71avhgyq09yjzx")))

(define-public crate-discoverer-0.2.0 (c (n "discoverer") (v "0.2.0") (d (list (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "19v70bx642gc99py0va162psbfmjsxhgrn5rm0cdbn47i8zxxv09")))

