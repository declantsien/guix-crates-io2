(define-module (crates-io di sc discorec) #:use-module (crates-io))

(define-public crate-discorec-0.1.0 (c (n "discorec") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sprs") (r "^0.11.0") (d #t) (k 0)))) (h "1c69pgx2ipsjwrz8hvyc78cf26xprgchvzlw7py9fz4dbckiymg3")))

(define-public crate-discorec-0.1.1 (c (n "discorec") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (k 0)))) (h "0bbq0cnfg8q9n0177r6592kzm85hdjjrsx89wadbqhj0zrhwfnd4")))

(define-public crate-discorec-0.1.2 (c (n "discorec") (v "0.1.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (k 0)))) (h "1nbwkfk0vgn1hxz92wfpanfc6w4ygvfyq9blri8w3jy15vqbamm6")))

(define-public crate-discorec-0.2.0 (c (n "discorec") (v "0.2.0") (h "1cwyhmchsxj768rv14kmcg2z0gz1xfg89gnirq48j9b2h0vl124d") (r "1.56.0")))

