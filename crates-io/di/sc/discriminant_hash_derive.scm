(define-module (crates-io di sc discriminant_hash_derive) #:use-module (crates-io))

(define-public crate-discriminant_hash_derive-0.1.0 (c (n "discriminant_hash_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "16v3191fmz31qv24q5yq1nl50rcpxffpj084zyf5l4pnwyzzmx4s")))

