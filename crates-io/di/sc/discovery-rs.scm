(define-module (crates-io di sc discovery-rs) #:use-module (crates-io))

(define-public crate-discovery-rs-0.1.0 (c (n "discovery-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (d #t) (k 0)) (d (n "clap_derive") (r "^4.5.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (f (quote ("default" "select"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "mdns-sd") (r "^0.11.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)))) (h "0bc4n598m7ry0gh0xf3hxgvpn8h4gkavgpl0kzj0frir9snx6q1w") (r "1.74.0")))

