(define-module (crates-io di sc disco) #:use-module (crates-io))

(define-public crate-disco-0.1.0 (c (n "disco") (v "0.1.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "strobe-rs") (r "^0.5.2") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.6.0") (d #t) (k 0)))) (h "1bpprkqp6lg2lrx30kq4mvd5fjrghg304vsbchf9viibn69br1w8")))

