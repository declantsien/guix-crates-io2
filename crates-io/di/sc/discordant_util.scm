(define-module (crates-io di sc discordant_util) #:use-module (crates-io))

(define-public crate-discordant_util-0.1.0 (c (n "discordant_util") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "discordant_types") (r "^0.1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0pxp0gm8km4vsnwc8h7shg6mp4nx1455l178z940vyj4nppdbw4y")))

(define-public crate-discordant_util-0.2.0 (c (n "discordant_util") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "discordant_types") (r "^0.2.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1vvqinaapfrfwc47bxhl4xi35jcrl7v8ha49j2wn2y8d3hbsssmw")))

