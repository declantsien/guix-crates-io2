(define-module (crates-io di sc discordant_types) #:use-module (crates-io))

(define-public crate-discordant_types-0.1.0 (c (n "discordant_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "0jz9rh6sxsxiqzp8a3853isky49j0mikhd42mdciz0mkc6al1hw1")))

(define-public crate-discordant_types-0.2.0 (c (n "discordant_types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "1mjpwvsrqn99cxaxcakngqiaaqdaiy8ayh9n1yr0i75hl04anzbd")))

