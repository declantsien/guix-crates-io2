(define-module (crates-io di sc discogs2csv) #:use-module (crates-io))

(define-public crate-discogs2csv-0.1.0 (c (n "discogs2csv") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "main_error") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.1") (d #t) (k 0)) (d (n "smallstr") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1174ia8z187y0ks13q132mkfahin9pnzmy1kksd2wcn870p6r23z")))

