(define-module (crates-io di sc disco-rpc) #:use-module (crates-io))

(define-public crate-disco-rpc-0.1.0 (c (n "disco-rpc") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)) (d (n "mlua") (r "^0.8.9") (f (quote ("lua54" "send"))) (d #t) (k 0)))) (h "1zzcnarbx8kdxw602frsa5yi066bz9f7897f2d9051gnd6wv896k") (f (quote (("unsafe"))))))

(define-public crate-disco-rpc-0.2.0 (c (n "disco-rpc") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive" "env" "string"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mlua") (r "^0.8.9") (f (quote ("lua54" "send"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "1cfyvynm58gkpsm3yp38zjl6bjnf8w567lj8951hcg4sbj0bykg1") (f (quote (("unsafe"))))))

(define-public crate-disco-rpc-0.2.1 (c (n "disco-rpc") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive" "env" "string"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "discord-rich-presence") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "mlua") (r "^0.8.9") (f (quote ("lua54" "send" "vendored"))) (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)))) (h "0azx77hbvlvg0mijmb2pixvqa3rqr7bpydkpp03n90ndslxswy8s") (f (quote (("unsafe"))))))

