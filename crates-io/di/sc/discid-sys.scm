(define-module (crates-io di sc discid-sys) #:use-module (crates-io))

(define-public crate-discid-sys-0.1.0 (c (n "discid-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1fbyj8czz3hs0h9ccvxmnxyfga0fc09na0y8lsnzw006nclvg0g4")))

(define-public crate-discid-sys-0.1.1 (c (n "discid-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "09lcwgjvhhirii01fik4gdl4rbkq3bsfr2zj65wad292y7d5wg8f")))

(define-public crate-discid-sys-0.2.0 (c (n "discid-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1sfbhx9xkczvsnz9nnd9vv720a6qlk63620dnli6v4rb2bd2kv3h")))

(define-public crate-discid-sys-0.2.1 (c (n "discid-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "0rv5d36ilbq4vjgslw9i8j67f0hira6njj2smb70dvbmw6g7b7ii")))

(define-public crate-discid-sys-0.3.0 (c (n "discid-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1hb7ig409c2fip9l4ih8hb50f98r6c4qfmi9wc440j36aj64fq6k")))

(define-public crate-discid-sys-0.4.0 (c (n "discid-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "09nql6fpfkq872mvj5c1ggv97ir8kqy9r3709g2ma8zvs6fb4vrz")))

(define-public crate-discid-sys-0.4.1 (c (n "discid-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0wwnf306qrb9pwvmzjk9whhhcd1ppgjpyvc6vfzvk1b58c23s9ai")))

(define-public crate-discid-sys-0.5.0 (c (n "discid-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0m9fh268xcpqpp1hk74xby2jlf062i04z813z3li0qin4mkfw5vx")))

