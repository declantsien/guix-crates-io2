(define-module (crates-io di sc discoid) #:use-module (crates-io))

(define-public crate-discoid-0.0.1 (c (n "discoid") (v "0.0.1") (h "1akahin95rqg4fhcxrrsxi1yrib0l4f3rz2ddcvlp7xnrkd4q8gj")))

(define-public crate-discoid-0.0.2 (c (n "discoid") (v "0.0.2") (h "099gc1hgp9a5jcrwkl6m2pp5p4lblbviyj13jnz8fzv0sgiq1b0z")))

(define-public crate-discoid-0.0.3 (c (n "discoid") (v "0.0.3") (h "0yqsj6kffdrlpnjwgqr30vinr7kpppb78jyrgb8nmnhrn04c72im")))

