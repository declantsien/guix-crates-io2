(define-module (crates-io di sc disc-bot) #:use-module (crates-io))

(define-public crate-disc-bot-0.1.0 (c (n "disc-bot") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1mjsazyrmdyqsaqq87w8xsp9zwrafj58j7c4ak2yq3l5jmxg8xvp")))

