(define-module (crates-io di sc discord-webhooks) #:use-module (crates-io))

(define-public crate-discord-webhooks-0.1.0 (c (n "discord-webhooks") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rzcqy4z1pbvr4pjgdw1kifnkrn0f41c99gc91b219apgkr3lbc4")))

