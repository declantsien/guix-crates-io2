(define-module (crates-io di sc discord-rpc-sys) #:use-module (crates-io))

(define-public crate-discord-rpc-sys-0.1.0 (c (n "discord-rpc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "10g12amjim4yiwki4h7y0wm4dp2i2zr5mdiz5h9hyap55bdk3dgf")))

