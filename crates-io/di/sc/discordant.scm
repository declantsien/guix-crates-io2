(define-module (crates-io di sc discordant) #:use-module (crates-io))

(define-public crate-discordant-0.1.0 (c (n "discordant") (v "0.1.0") (d (list (d (n "discordant_types") (r "^0.1.0") (d #t) (k 0)) (d (n "discordant_util") (r "^0.1.0") (d #t) (k 0)))) (h "0r999c29hm9rl1hg5f5xcgwglrnafp34kghpracri879ndrygcgy")))

(define-public crate-discordant-0.1.1 (c (n "discordant") (v "0.1.1") (d (list (d (n "discordant_types") (r "^0.1.0") (d #t) (k 0)) (d (n "discordant_util") (r "^0.1.0") (d #t) (k 0)))) (h "0pislgdr7krdqgads6ay66s497vrwlrqshvf4syl5r4v0z2ab1w5")))

(define-public crate-discordant-0.2.0 (c (n "discordant") (v "0.2.0") (d (list (d (n "discordant_types") (r "^0.2.0") (d #t) (k 0)) (d (n "discordant_util") (r "^0.2.0") (d #t) (k 0)))) (h "0acpnm0dj2qzx9wi3bzira45wi1r4sgb6dfqk4q18k9l3hq8vgj4")))

