(define-module (crates-io di sc discortp) #:use-module (crates-io))

(define-public crate-discortp-0.1.0 (c (n "discortp") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.26") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.26") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "0bdhnv0pv1flyyjvr20n5sr1fid4xqsvfxgz1kd7dvcpvm3z907q") (f (quote (("discord") ("default"))))))

(define-public crate-discortp-0.1.1 (c (n "discortp") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.26") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.26") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "04hcwm2xn2sy32k9wwhbnkhj5m5wly64bg8554sspck3slq386b5") (f (quote (("discord") ("default"))))))

(define-public crate-discortp-0.2.0 (c (n "discortp") (v "0.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.26") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.26") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "062wxm2s88k3l2cm7b8lxi1icwh1rqzmdjxbzj6z7gi7ymdpqbls") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp"))))))

(define-public crate-discortp-0.2.1 (c (n "discortp") (v "0.2.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.26") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.26") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "1f0dswzyfm2ldpw1bijaqdqihc3m5kdaa009lmgqp00730py3lz9") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp"))))))

(define-public crate-discortp-0.2.2 (c (n "discortp") (v "0.2.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.26") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.26") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "1s72r0rw7nnygl9k684wvwan5wj5ysimsxi98lba4dn3i0j4h3cw") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp"))))))

(define-public crate-discortp-0.3.0 (c (n "discortp") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "pnet_macros") (r "^0.27") (d #t) (k 1)) (d (n "pnet_macros_support") (r "^0.27") (d #t) (k 0)) (d (n "syntex") (r "^0.42") (d #t) (k 1)))) (h "1v0b4gqd9ysqqc8qa30jf9ja6m134xdaafh6wpvhll1l83z159xq") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp"))))))

(define-public crate-discortp-0.4.0 (c (n "discortp") (v "0.4.0") (d (list (d (n "pnet_macros") (r "^0.28") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.28") (d #t) (k 0)))) (h "0wk9s8jm32c0b1sysh8sgxj2z5dmig0kmmihxasj10m48rv02rpv") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp"))))))

(define-public crate-discortp-0.5.0 (c (n "discortp") (v "0.5.0") (d (list (d (n "pnet_macros") (r "^0.31") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.31") (d #t) (k 0)))) (h "0sw9naanbxv2fp8g86lxaq35nmw3dgy8rmc85kgawx4iq0wr8jsj") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp")))) (r "1.56.1")))

(define-public crate-discortp-0.6.0 (c (n "discortp") (v "0.6.0") (d (list (d (n "pnet_macros") (r "^0.34") (d #t) (k 0)) (d (n "pnet_macros_support") (r "^0.34") (d #t) (k 0)))) (h "0p9zgprysjvv2a1dzbvzc7128156zwiq39izc5a1fdjxl4438yaw") (f (quote (("rtp") ("rtcp") ("pnet") ("discord-full" "default" "demux" "discord") ("discord") ("demux") ("default" "pnet" "rtp" "rtcp")))) (r "1.65.0")))

