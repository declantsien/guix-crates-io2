(define-module (crates-io di sc disc_riider) #:use-module (crates-io))

(define-public crate-disc_riider-0.1.0 (c (n "disc_riider") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "binrw") (r "^0.10.0") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "sha1") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1jjb3kbxwa58c861kp9zq22ci7vvdjsqzdgliy53mdkhhyhfsjll")))

(define-public crate-disc_riider-0.2.0 (c (n "disc_riider") (v "0.2.0") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "17vhy9b32kxlf1aij5fcfvwj6sx6f093lw8vjsfrslp3qq72d0kj")))

(define-public crate-disc_riider-0.2.1 (c (n "disc_riider") (v "0.2.1") (d (list (d (n "aes") (r "^0.8.2") (d #t) (k 0)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1cf298b5sihm8wj344s2f27c523q8zkzfbj6z61nyjmm9cvrfj55")))

