(define-module (crates-io di sc discord-lambda) #:use-module (crates-io))

(define-public crate-discord-lambda-0.1.0 (c (n "discord-lambda") (v "0.1.0") (d (list (d (n "discord-message") (r "^0.1.0") (d #t) (k 0)) (d (n "discord-webhook-client") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "vercel_lambda") (r "^0.2") (d #t) (k 0)))) (h "1ml4xyax5lbp8hl8hkas64c5w9xpc66fi7aq8hqliv22c0048gsc")))

