(define-module (crates-io di sc discovery_crate) #:use-module (crates-io))

(define-public crate-discovery_crate-0.1.0 (c (n "discovery_crate") (v "0.1.0") (d (list (d (n "anyhow") (r ">0.5") (d #t) (k 0)) (d (n "rand") (r "=0.3.20") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "1z0s57n2w3qvwknk26bp17l3dydsdinnrwdr3i5r6crwvb36fdvp")))

(define-public crate-discovery_crate-0.2.0 (c (n "discovery_crate") (v "0.2.0") (d (list (d (n "anyhow") (r ">0.5") (d #t) (k 0)) (d (n "discovery_crate_companion") (r ">1.0") (d #t) (k 0)) (d (n "rand") (r "=0.3.20") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "007rn8d23cjdnhja0z93w0i33fjg7qryh1r5g6rmw181sag5y0rm")))

(define-public crate-discovery_crate-1.2.0 (c (n "discovery_crate") (v "1.2.0") (d (list (d (n "anyhow") (r ">0.5") (d #t) (k 0)) (d (n "rand") (r "=0.3.20") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "042iwp66br292rc63svxfcq0gi6z3hjyxba3b0ib122085ad0d3f")))

