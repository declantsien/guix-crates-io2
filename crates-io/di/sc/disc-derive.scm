(define-module (crates-io di sc disc-derive) #:use-module (crates-io))

(define-public crate-disc-derive-0.1.0 (c (n "disc-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)))) (h "15ry8jb6dhi1c38wvd5wzgxkaiqz3zaryy1v12qmf5fsm1cz0ark")))

