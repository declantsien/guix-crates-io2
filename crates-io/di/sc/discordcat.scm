(define-module (crates-io di sc discordcat) #:use-module (crates-io))

(define-public crate-discordcat-0.2.0 (c (n "discordcat") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "052jdmv1y74k8f6vnvry8sgmsgjs0adx0b37gyyc20z2pa555xhx")))

