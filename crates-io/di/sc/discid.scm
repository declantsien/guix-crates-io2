(define-module (crates-io di sc discid) #:use-module (crates-io))

(define-public crate-discid-0.1.0 (c (n "discid") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1mg1fqpbrn56qmmzinwz83ddsh65pa6ahwbmixjk26nwhml6p1z6")))

(define-public crate-discid-0.2.0 (c (n "discid") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0akkgbbw0h4jdfbb0dlg8bwjwn93ir9pph82fpys8iqmsaca1w3q")))

(define-public crate-discid-0.3.0 (c (n "discid") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0m3knzf4c0fyydx0r9x0rnqggy4bcks5mf2883kzy1zp1hq1c74m")))

(define-public crate-discid-0.4.0 (c (n "discid") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.2.0") (d #t) (k 0)))) (h "12nh0h1y1nd57i5v7h1akraq9z3k5lhgsdl88gl66bmqza832kn9")))

(define-public crate-discid-0.4.1 (c (n "discid") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.2.0") (d #t) (k 0)))) (h "00cplhjj5dlpj4rmb7qsai8wpc5plk0x3ii1d7dfiayravlm8z1f")))

(define-public crate-discid-0.4.2 (c (n "discid") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "0.*") (d #t) (k 0)))) (h "045wyi2j50zgx4bv6fyill9nbi8dy0i03r5vg4shjj295zaikij6")))

(define-public crate-discid-0.4.3 (c (n "discid") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.3.0") (d #t) (k 0)))) (h "152w02mcmcg3a3xsps5rq6cjdvhprmhl8bhy3qypwcnfbjk032rh")))

(define-public crate-discid-0.4.4 (c (n "discid") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "discid-sys") (r "^0.4.0") (d #t) (k 0)))) (h "02r2g83jdx62900b71z8p24ilx1zzwwvl09yja58znr1bycjrq77")))

(define-public crate-discid-0.4.5 (c (n "discid") (v "0.4.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "discid-sys") (r "^0.4") (d #t) (k 0)))) (h "0qhhbs0aj779j8sjwnd5fsp4xcv7xw0adizryl8ign2mb4jq9khx")))

(define-public crate-discid-0.5.0 (c (n "discid") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "discid-sys") (r "^0.5") (d #t) (k 0)))) (h "1c2lg8f5w5sziy01qqh3w99niks7rnif58bgypkfpma9zfqnmwq5")))

