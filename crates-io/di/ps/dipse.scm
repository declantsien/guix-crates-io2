(define-module (crates-io di ps dipse) #:use-module (crates-io))

(define-public crate-dipse-0.1.0 (c (n "dipse") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0q5dp2k6dfzs9x5pd7yj7sf3xdh7prhm193mm56hd05y8ad0asvy")))

(define-public crate-dipse-0.2.0 (c (n "dipse") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "19i7qqxdnaxvr7x4vvzjnsiaa1ismbvr5plxinpjpjc4qk25ig1l")))

(define-public crate-dipse-0.3.0 (c (n "dipse") (v "0.3.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "059kdl8shl21yx2mlkyj9c31mx8qaqjq1n20i5b6jjnhsn9g1wis")))

(define-public crate-dipse-0.4.0 (c (n "dipse") (v "0.4.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0sv4pw7kc56m4097cn9m0n2ajqmzchg0mrbb5wc748qm3yskjc1i")))

(define-public crate-dipse-0.4.1 (c (n "dipse") (v "0.4.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0b1wpc0bdx1bknpfbz3n5f8f35v3czx2sbrn3vawm5vsnbwpb1yr")))

(define-public crate-dipse-0.5.0 (c (n "dipse") (v "0.5.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1m3vch7i5v9ravgznysf20wwgbgzdma3fa0vzznxgz3k89nhaxlm")))

(define-public crate-dipse-0.5.1 (c (n "dipse") (v "0.5.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1fildxhcdrahzaqxjhrl0p0mvks432g15jnli70ar176rajdawrv")))

(define-public crate-dipse-0.6.0 (c (n "dipse") (v "0.6.0") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "11mz92y47hfkly6yp20ckp7fy25qr3bbwnckrlksqmfb8lwpb992")))

