(define-module (crates-io di gi digit-sequence) #:use-module (crates-io))

(define-public crate-digit-sequence-0.1.0 (c (n "digit-sequence") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "0mhhdaqzwm1m3dvqlyx3iy1sl7gvfyw5mm42j7426nmbzm8f17yn")))

(define-public crate-digit-sequence-0.2.0 (c (n "digit-sequence") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "00hqs20h28c4p21rl9a2jxzzw2p3hddxv29zbjpyc9hkfwzvzq9m")))

(define-public crate-digit-sequence-0.2.1 (c (n "digit-sequence") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "1ll8gyv7bp7k4d2zmmgjpziakjga3722hwiyp7ajp3j0agsxh9vw")))

(define-public crate-digit-sequence-0.3.0 (c (n "digit-sequence") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "0vh758ajvarl8glfwlqwaxv40wskbwrvj2fw00ghxsnc4slc6brn")))

(define-public crate-digit-sequence-0.3.1 (c (n "digit-sequence") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "1g8m6szlv1c86dlja62f7nyilcnw4vx47817askbln6y24xmn7gg")))

(define-public crate-digit-sequence-0.3.2 (c (n "digit-sequence") (v "0.3.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "1la3x1jdbaxbqn9nlbqwwq7asx553bh7g5ca18bf0p420jj9zn17")))

(define-public crate-digit-sequence-0.3.3 (c (n "digit-sequence") (v "0.3.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "1g1n79xr7xsjk12kbhl4zarxg7ag3m52yhl3hw13m10iwdy7bs48")))

(define-public crate-digit-sequence-0.3.4 (c (n "digit-sequence") (v "0.3.4") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "speculate2") (r "^0.2") (d #t) (k 2)))) (h "0gzv74n4jr6s4k51r5f7a1sci4bi30nz3sifncpk7sxy529wvxfj")))

