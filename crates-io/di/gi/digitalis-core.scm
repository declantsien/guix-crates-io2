(define-module (crates-io di gi digitalis-core) #:use-module (crates-io))

(define-public crate-digitalis-core-0.0.0 (c (n "digitalis-core") (v "0.0.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.19") (k 0)))) (h "1chj34p12s4mr7j5kdjdmf8fcwmch68rs6j9g3hci4lwpy70misr") (r "1.60.0")))

