(define-module (crates-io di gi digibyte-rpc-json) #:use-module (crates-io))

(define-public crate-digibyte-rpc-json-0.13.0 (c (n "digibyte-rpc-json") (v "0.13.0") (d (list (d (n "digibyte") (r "^0.27") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16yhb9jv7472x3ipk7lah4s5fb14ji4wn0yrf2rb2g1wx3xif546")))

(define-public crate-digibyte-rpc-json-0.13.1 (c (n "digibyte-rpc-json") (v "0.13.1") (d (list (d (n "digibyte") (r "^0.27.1") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0baklj232m3fmq48y86rdvkzk7vjkaxkpb21j9qfzrrnyxv7r212")))

(define-public crate-digibyte-rpc-json-0.13.2 (c (n "digibyte-rpc-json") (v "0.13.2") (d (list (d (n "digibyte") (r "^0.27.1") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bn25zqzvgw0kydsjg53zvvbcr5v0zdf8aplz736m4rkssqdvqdv")))

(define-public crate-digibyte-rpc-json-0.13.3 (c (n "digibyte-rpc-json") (v "0.13.3") (d (list (d (n "digibyte") (r "^0.27.2") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kxf8sx8l7h3b3xgw1zdf2y0iv6zzy9k93b3x7fxd0kzc8gqghdj")))

