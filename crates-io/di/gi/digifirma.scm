(define-module (crates-io di gi digifirma) #:use-module (crates-io))

(define-public crate-digifirma-0.1.0 (c (n "digifirma") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "codice_fiscale") (r "^0.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 1)) (d (n "httpstatus") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "09rz9nh9r6y7fic87958dq35fh7ag0h1jwk109cpnfb6igncgly1")))

(define-public crate-digifirma-0.1.1 (c (n "digifirma") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "codice_fiscale") (r "^0.4") (d #t) (k 0)) (d (n "httpstatus") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1qbbvvvgxcqjxcyxkzniqwv1fvhqx7hr62v8372wrzr9rnwz1l96")))

