(define-module (crates-io di gi digit-invariant) #:use-module (crates-io))

(define-public crate-digit-invariant-0.1.0 (c (n "digit-invariant") (v "0.1.0") (d (list (d (n "narcissistic") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "118lal4k4mqjrr7x67cjs83r53vdf3hg6y09qviq7ylqnr7qlv5k")))

