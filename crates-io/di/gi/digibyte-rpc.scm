(define-module (crates-io di gi digibyte-rpc) #:use-module (crates-io))

(define-public crate-digibyte-rpc-0.13.0 (c (n "digibyte-rpc") (v "0.13.0") (d (list (d (n "digibyte-rpc-json") (r "^0.13.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wjnbrk1k96n4ky3h4iqavd4xd0fsss24qm268vhw1i8di05r76a")))

(define-public crate-digibyte-rpc-0.13.1 (c (n "digibyte-rpc") (v "0.13.1") (d (list (d (n "digibyte-rpc-json") (r "^0.13.0") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y3sgkvv1n6shfcwlr0kvmdwbsb2g8y38va4m572myskdc2zynx8")))

(define-public crate-digibyte-rpc-0.13.2 (c (n "digibyte-rpc") (v "0.13.2") (d (list (d (n "digibyte-rpc-json") (r "^0.13.2") (d #t) (k 0)) (d (n "jsonrpc") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rnxhk0ckxqcq8rmd7f4mmf2pwmjz93w2im5qjv3x4pix5l0c84y")))

