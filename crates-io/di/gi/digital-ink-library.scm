(define-module (crates-io di gi digital-ink-library) #:use-module (crates-io))

(define-public crate-digital-ink-library-1.4.0 (c (n "digital-ink-library") (v "1.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1gp29nv98wi9d3kw55w2ljnh7882sbk727z408hx8rgpf0rmvpcp")))

