(define-module (crates-io di gi digitalis-protobuf) #:use-module (crates-io))

(define-public crate-digitalis-protobuf-0.0.0 (c (n "digitalis-protobuf") (v "0.0.0") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "11klv81y48sf0pad93cffp6myqrcrp9ndw2jb7c3rb12irmd9gcs") (r "1.60.0")))

