(define-module (crates-io di gi digitize) #:use-module (crates-io))

(define-public crate-digitize-0.1.0 (c (n "digitize") (v "0.1.0") (h "09947s9w2s385k3aqdvq497pr1kayhc2pi3q5v6c3abk2c2m4ysh")))

(define-public crate-digitize-0.1.1 (c (n "digitize") (v "0.1.1") (h "1mihj690w888v11grpqqrhw72m5nclhqys3v6vxlgxc657whz07f")))

