(define-module (crates-io di gi digital-filter) #:use-module (crates-io))

(define-public crate-digital-filter-0.1.1 (c (n "digital-filter") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5.1") (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0r86jffc2zgaa5a9232f3z4b99qwbm5zj9125bwywrpkdsz39hdv")))

