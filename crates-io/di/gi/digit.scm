(define-module (crates-io di gi digit) #:use-module (crates-io))

(define-public crate-digit-0.1.0 (c (n "digit") (v "0.1.0") (h "1xaf57x141lg9zfybsnl89z8r5232gpxzr84mr8svdzkailp7nyx") (y #t)))

(define-public crate-digit-0.2.0 (c (n "digit") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "predicates") (r "^3") (d #t) (k 2)))) (h "07h6x2xmfav4d1diqgm4sf13x91rqvj3pnjl1xqn9cz8nl9rz944")))

