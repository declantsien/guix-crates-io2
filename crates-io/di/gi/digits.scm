(define-module (crates-io di gi digits) #:use-module (crates-io))

(define-public crate-digits-0.0.1 (c (n "digits") (v "0.0.1") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1zd4rcc60wvhpx5kfwqfwsgz3vxj2w9cf0gk7yhrqbnldkqkygsy")))

(define-public crate-digits-0.0.2 (c (n "digits") (v "0.0.2") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "15532zq2l4kb8l4dw6v8g4553jcfm0dh3sh7yq5c4v5n5jljdssv")))

(define-public crate-digits-0.0.3 (c (n "digits") (v "0.0.3") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "01d26dbdvid2qxqbw04532gkc7mkn1hh1nc1sb322da3vw6df551")))

(define-public crate-digits-0.0.4 (c (n "digits") (v "0.0.4") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1ammjp5i34n32q5yrhhph3g2h26w5myijn7i9vqa0lyn8wi31mvx")))

(define-public crate-digits-0.0.5 (c (n "digits") (v "0.0.5") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1wq0qfkbmvsy3ln2zj0qxzfj11hvdvid41gcynxfwskpzksjml6q")))

(define-public crate-digits-0.1.0 (c (n "digits") (v "0.1.0") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "128spjmgym53znw1134qc49p42z3x2rma1d1lkz1d95pj66kcyi9")))

(define-public crate-digits-0.1.1 (c (n "digits") (v "0.1.1") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1j6rzg691jik73zcv9q84i1rrf8cndmkpzbm2icxmz2cswz8rxir")))

(define-public crate-digits-0.2.0 (c (n "digits") (v "0.2.0") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1lw644bfq0gagbdkaqmq1smwsgp7fi3ciahjmm578lagjdn1ii52")))

(define-public crate-digits-0.2.1 (c (n "digits") (v "0.2.1") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "0pbj9l8381s7fkcw5axs351d6dmqgwf5ksn6c48hk7l3w5h5fhah")))

(define-public crate-digits-0.2.2 (c (n "digits") (v "0.2.2") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "00m2ngfqz47pji4l4qz9xiyjj9iq408x445i7wmwxr59vrz02mz1")))

(define-public crate-digits-0.2.3 (c (n "digits") (v "0.2.3") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1bh43m8jygwxmb8777aawankdp5y8gb5yp1h4k37vfxv70ndifv6")))

(define-public crate-digits-0.2.4 (c (n "digits") (v "0.2.4") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "16fhl3dcv69j69x291vg2p6d1z723pyjyxiqz05k2i9v0mhlzdxx")))

(define-public crate-digits-0.3.0 (c (n "digits") (v "0.3.0") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "12nil0400jwrgvjrbf9ndy0fsnfmhdhc8hhdpx1mj4sd5rgk06bh")))

(define-public crate-digits-0.3.1 (c (n "digits") (v "0.3.1") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "1xsmgbq965p34w49b2vjqj5nxv4f4k08sxrddbv8hwpqpapinnrv")))

(define-public crate-digits-0.3.2 (c (n "digits") (v "0.3.2") (d (list (d (n "base_custom") (r "^0.1") (d #t) (k 0)))) (h "0dvpxaz92ji0i4sxfp7vfxc292xx82lg7r6w345x0c9p9ka2z28p")))

(define-public crate-digits-0.3.3 (c (n "digits") (v "0.3.3") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "0xrkqvbavng46jc5b1j36wgxlfy6z0nscpj5d70pgn7l99fg7lqq")))

(define-public crate-digits-0.3.4 (c (n "digits") (v "0.3.4") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "113ikblcj4vwkxlwimc2vs6bwp1f13yw0klrhglvyp7713qllrpg")))

(define-public crate-digits-0.3.5 (c (n "digits") (v "0.3.5") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "145ppk49j67ikl2ab6vsxfhy0265cdxhwj8s46x48fkkqkyhnwjm")))

(define-public crate-digits-0.3.6 (c (n "digits") (v "0.3.6") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "0zimz3h8i715mbkdkzs84xrhz9aldimy721ixhbgsfz1lam9alki")))

(define-public crate-digits-0.3.7 (c (n "digits") (v "0.3.7") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "0f1i1f7f1j3r5kp25fqp8ygspjmqvavwvcfc04rp7r8mbv7j05qh")))

(define-public crate-digits-0.3.8 (c (n "digits") (v "0.3.8") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "17zpymn21277p9bv0kd28hhkcb29mhkr4zzm65f7pdjpgj97s7bv")))

(define-public crate-digits-0.3.9 (c (n "digits") (v "0.3.9") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.11") (d #t) (k 0)))) (h "1l77fzs56qzzqwrjmprg3wl2gnbm8m7bafskmav0q0yycgx92k2n")))

(define-public crate-digits-1.0.0 (c (n "digits") (v "1.0.0") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.12") (d #t) (k 0)))) (h "0rwfcp8664k86z4qp283mkj6ar92g5y7dc87cgdrb7lakydi845w")))

(define-public crate-digits-1.1.0 (c (n "digits") (v "1.1.0") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)))) (h "181q5bwv7scp9j796ja2rr61d0ykrzg2lmw1cbp2w0vj94ndbmwa") (f (quote (("default"))))))

(define-public crate-digits-1.1.1 (c (n "digits") (v "1.1.1") (d (list (d (n "array_tool") (r "^0.4.1") (d #t) (k 0)) (d (n "base_custom") (r "^0.1.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.175") (o #t) (d #t) (k 0)))) (h "0s3gcmqqmaxzp8b04kww2jfzjn63h09kwb21ggglz80qrr85iwvb") (f (quote (("default"))))))

