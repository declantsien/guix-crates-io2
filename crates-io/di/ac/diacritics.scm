(define-module (crates-io di ac diacritics) #:use-module (crates-io))

(define-public crate-diacritics-0.1.0 (c (n "diacritics") (v "0.1.0") (h "15dl07sqih5b7497k2kabv4rvwv01vyz8rcjsl1l8iay9ydpz1gb")))

(define-public crate-diacritics-0.1.1 (c (n "diacritics") (v "0.1.1") (h "0x0bh4msmxjbnxgchpqz45h0f9v719kyyj4x8cdcls6djchrpyf4")))

(define-public crate-diacritics-0.1.2 (c (n "diacritics") (v "0.1.2") (h "007jiy163jhqanmxdypr8bdb6wxh8ms55a7bv52x5ndj7bfkgnns")))

(define-public crate-diacritics-0.1.3 (c (n "diacritics") (v "0.1.3") (h "0ld1l8fn6xk98i8lh7slrpfcxix94xxy323d5f6majh09xvhf23j")))

(define-public crate-diacritics-0.2.0 (c (n "diacritics") (v "0.2.0") (h "1qn7i56fymxydy07m7xkj300x5r377p1vrfm1kql9kiibd749ipy")))

(define-public crate-diacritics-0.2.1 (c (n "diacritics") (v "0.2.1") (h "1qxq6py4ppszw9p4kg7s1pp5f8apfsvxd6rlfbpbaxzvmh204diy")))

(define-public crate-diacritics-0.2.2 (c (n "diacritics") (v "0.2.2") (h "19nxjscpcnz1bbnbzhgxqciq2ds635zfxz1n134rfza33rlq56p3")))

