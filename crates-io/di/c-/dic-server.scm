(define-module (crates-io di c- dic-server) #:use-module (crates-io))

(define-public crate-dic-server-0.1.0 (c (n "dic-server") (v "0.1.0") (d (list (d (n "dic") (r "^0.1.0") (d #t) (k 0)))) (h "1p27mk2bhyzppyrdpq2pwdxq6y87967wymzzl7wxnabgr03bfqdk") (y #t)))

(define-public crate-dic-server-0.1.1 (c (n "dic-server") (v "0.1.1") (d (list (d (n "dic") (r "^0.1") (d #t) (k 0)))) (h "11g1cgj7c7nyzdy28jv58h21b7wxm3cfhryabp0lllbkyd3d9gdr") (y #t)))

