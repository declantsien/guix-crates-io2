(define-module (crates-io di st distmat) #:use-module (crates-io))

(define-public crate-distmat-0.2.0 (c (n "distmat") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dd75s21q9d1lzzd0b3lbnphg2wzqlhqfcqwvzawncfv0q153y6z")))

(define-public crate-distmat-0.2.1 (c (n "distmat") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17i9cv8adzpnik9agbncx5f82cxyc49r8qf00wp128f7rz9rpg3z")))

(define-public crate-distmat-0.3.0 (c (n "distmat") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ajkwfmgmlmk8f8iiarnx1v478bz2mr3r0a8bdnmqg80aa9j8h51")))

