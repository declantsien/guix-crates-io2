(define-module (crates-io di st distill-serde-importable-derive) #:use-module (crates-io))

(define-public crate-distill-serde-importable-derive-0.0.1 (c (n "distill-serde-importable-derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y59hrn2y4j8w7r59zzvv87hf47w8nsc94pdr2b0737176v4mdnl") (y #t)))

(define-public crate-distill-serde-importable-derive-0.0.2 (c (n "distill-serde-importable-derive") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("derive"))) (d #t) (k 0)))) (h "1swcasamygjv1a0p3x7635vv2v7fsvvzw3yy6m6r1afd41s9z6qa")))

(define-public crate-distill-serde-importable-derive-0.0.3 (c (n "distill-serde-importable-derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b36bl1jhikjnnqf453wh3r2ncpk5k7sz90gwfwzfjpl8ibkgj6j")))

