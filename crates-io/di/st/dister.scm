(define-module (crates-io di st dister) #:use-module (crates-io))

(define-public crate-dister-0.1.0 (c (n "dister") (v "0.1.0") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1fih22jcfnfxwwd6r5lddpvy6wz2h34s5xylsj3ny9diqjs3k6r4")))

(define-public crate-dister-0.1.1 (c (n "dister") (v "0.1.1") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nq4q82xn5055z82gj0gsyx7z2vx1ffadz8hyivwgfcg1s0dhl4q")))

(define-public crate-dister-0.1.2 (c (n "dister") (v "0.1.2") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0g2fjg8yiyw90cc77z1il85z7vlqlx6sh4v6sh4mw32zw47md5yw")))

(define-public crate-dister-0.1.3 (c (n "dister") (v "0.1.3") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0929zwd7vlr87g90wi0as8fi790wvphkz1w2vqi69cha9vp4ka3g")))

(define-public crate-dister-0.1.4 (c (n "dister") (v "0.1.4") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1j91b2ijyhn8k53jsa0x4rbr0lwxvf43jiavp8az5b3f85y9vskf")))

(define-public crate-dister-0.1.5 (c (n "dister") (v "0.1.5") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "156qfvsa3ayg74hny3ndbsb6r9qb4zinqh8bm6l40d26brx6nw3y")))

(define-public crate-dister-0.1.6 (c (n "dister") (v "0.1.6") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "09sk9hl7gizkw5ki338305b7dn1l1k6dh94n7bpd7d9dzjm419s6")))

(define-public crate-dister-0.1.7 (c (n "dister") (v "0.1.7") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15vcbficds3p9jw72j8x95dfibhqwp9w742vrycl70nk0575nzsh")))

(define-public crate-dister-0.1.8 (c (n "dister") (v "0.1.8") (d (list (d (n "livid-cli") (r "^0.1.6") (d #t) (k 0)))) (h "090ai8nr93d431ivibwc4ng1lrxlicj3k6p9bpx7m0229ypgb2m8")))

(define-public crate-dister-0.1.9 (c (n "dister") (v "0.1.9") (d (list (d (n "livid-cli") (r "^0.1.6") (d #t) (k 0)))) (h "0bnn08rd76iv573b0aby0684iwv1gfjqz0wav7zq2zk1y39ci0rc")))

