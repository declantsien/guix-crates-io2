(define-module (crates-io di st distributed_control) #:use-module (crates-io))

(define-public crate-distributed_control-0.1.0 (c (n "distributed_control") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "16s35kgqyhlgdc3cvjadjib2i2k96a6cdhi31kmvfyxjqd1qsqaj")))

(define-public crate-distributed_control-0.1.1 (c (n "distributed_control") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0j9sw3wp0j4kbgyfj5n3ca5bafqkh3n36wgchp2b48xv2fwfllpi")))

(define-public crate-distributed_control-0.2.0 (c (n "distributed_control") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0jdl6697kir4kf1nj4qkwrj9agw2hswnz0gbnkahj7vsij6vki5n")))

(define-public crate-distributed_control-0.3.0 (c (n "distributed_control") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-system"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0c5k5qp9b9ab7hadp3k9vdw1xrvln6p0s6v6np2hfdii493mfajr") (f (quote (("openblas-system" "ndarray-linalg/openblas-system"))))))

(define-public crate-distributed_control-0.3.1 (c (n "distributed_control") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-system"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0g8xda32v1vhsrfrbrk3w1s8ywqbli2mckiy6x5vfpgl99lx63p9") (f (quote (("openblas-system" "ndarray-linalg/openblas-system"))))))

(define-public crate-distributed_control-0.3.2 (c (n "distributed_control") (v "0.3.2") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-system"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1mmnq4wxxjkgm6zrly1kg44rigbmi9jvlchcp7li64v2s4wwi2h9") (f (quote (("openblas-system" "ndarray-linalg/openblas-system"))))))

(define-public crate-distributed_control-0.3.3 (c (n "distributed_control") (v "0.3.3") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-system"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0cc2xcj41ia4ms4sca5abn9kmyimzxrabam5psabhpiqdzk0la9j") (f (quote (("openblas-system" "ndarray-linalg/openblas-system"))))))

(define-public crate-distributed_control-0.4.0 (c (n "distributed_control") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "1c1qmfqfnhl8cm9sp6cpzpnfcgz4wqmpq7kdyaiqvy2wrlhm1027")))

(define-public crate-distributed_control-0.5.0 (c (n "distributed_control") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0cw7p72n88daaskff3039rs63dgbqip1hbk5r0sx3dpa3mzfq83k")))

(define-public crate-distributed_control-0.5.1 (c (n "distributed_control") (v "0.5.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("intel-mkl"))) (d #t) (k 2)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)))) (h "0w5ghp06qvqp0r12d18d5v7plpnjg7jqbljrmr6gx4l1q8bsx9yr")))

