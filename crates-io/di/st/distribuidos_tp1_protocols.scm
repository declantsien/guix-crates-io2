(define-module (crates-io di st distribuidos_tp1_protocols) #:use-module (crates-io))

(define-public crate-distribuidos_tp1_protocols-0.1.0 (c (n "distribuidos_tp1_protocols") (v "0.1.0") (h "1fzknyyds8fl9qkqmfc5nqadcbmy4qgbq4k6hnk5ff4qj4cwbnk7")))

(define-public crate-distribuidos_tp1_protocols-1.0.0 (c (n "distribuidos_tp1_protocols") (v "1.0.0") (d (list (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "distribuidos_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1jc6p4c85hvd0aa6730v2xnwhnwbvbpgdk0icym9irz9pp0vj9kx")))

(define-public crate-distribuidos_tp1_protocols-1.0.1 (c (n "distribuidos_tp1_protocols") (v "1.0.1") (d (list (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "distribuidos_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1879zj4fli70hxpc14aws4b10ikjwv2iykyg4c52vn4yxirs0rl5")))

(define-public crate-distribuidos_tp1_protocols-1.0.2 (c (n "distribuidos_tp1_protocols") (v "1.0.2") (d (list (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "distribuidos_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1phq6m75lrm5rnqgxgzlsnrhc5q0b6vcpvav6wb4smj1accnc6xc")))

(define-public crate-distribuidos_tp1_protocols-1.1.0 (c (n "distribuidos_tp1_protocols") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "0p8kw6gviw94y1dhy6hpfahh6l24845790gzwi7pw1bmc56wivm0")))

(define-public crate-distribuidos_tp1_protocols-1.1.1 (c (n "distribuidos_tp1_protocols") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "0sgn0xab19hrmfbg3jy2bvm4050g5c3zpysy7bii8adfswgis4c7")))

(define-public crate-distribuidos_tp1_protocols-1.1.2 (c (n "distribuidos_tp1_protocols") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "0kqcl8646j12ibkvr7mvij8w74llh9c7sssp8g9719nb77vgbpp6")))

(define-public crate-distribuidos_tp1_protocols-1.1.3 (c (n "distribuidos_tp1_protocols") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "1gjaq902xkykiyyg0yn6f43jshmhh5bqw8yx98z81arhn8n9f7y6")))

(define-public crate-distribuidos_tp1_protocols-1.2.0 (c (n "distribuidos_tp1_protocols") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "1d5zihzzfic4d57r3vk53fcf094cmlsqci3md5vxjfp19naxckz5")))

(define-public crate-distribuidos_tp1_protocols-1.2.1 (c (n "distribuidos_tp1_protocols") (v "1.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "1wxhs8g9y404g00qlkr41sk4x4nr872m9k36fdrp269x4qgd2liv")))

(define-public crate-distribuidos_tp1_protocols-1.2.2 (c (n "distribuidos_tp1_protocols") (v "1.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "1kr64f7r3wq9k89gmfiq7w0zrga43kwq4zkfwvyvycjrn5kymcq7")))

(define-public crate-distribuidos_tp1_protocols-1.2.3 (c (n "distribuidos_tp1_protocols") (v "1.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "0k7dy68k914p3yld85anm015xaw3hi9a9sq6y4i5jcvwkjcwgnjj")))

(define-public crate-distribuidos_tp1_protocols-1.2.4 (c (n "distribuidos_tp1_protocols") (v "1.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)))) (h "18zhw9v3sxvf4vy2qfs3xn7jc7schd7jvs69q0vxi6v9aj1c74hb")))

(define-public crate-distribuidos_tp1_protocols-1.2.5 (c (n "distribuidos_tp1_protocols") (v "1.2.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jf8qxnailmq6dm8g9y8awxsh7y3snf0ksmicl4lvg48fv21gp1y")))

(define-public crate-distribuidos_tp1_protocols-1.2.6 (c (n "distribuidos_tp1_protocols") (v "1.2.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "120i1lr35icnvxgzzniczydzrlqcjscw4vk0xr6gqxgn60gk73qq")))

