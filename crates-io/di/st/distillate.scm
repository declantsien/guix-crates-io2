(define-module (crates-io di st distillate) #:use-module (crates-io))

(define-public crate-distillate-0.1.0 (c (n "distillate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (f (quote ("auto-color"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wc1p8jcnw8ipilz6xpv3133k5vqlgzlsd9wifkx04jq8agjsy79")))

