(define-module (crates-io di st distinct) #:use-module (crates-io))

(define-public crate-distinct-0.1.0 (c (n "distinct") (v "0.1.0") (h "0lhjchgnfbxwvjcn4135zsxyvzqvb7kxbqwdazgljknfvskm0iz9")))

(define-public crate-distinct-0.1.1 (c (n "distinct") (v "0.1.1") (h "0v1bxp47ylj9jpxpp7pixh1cb3ivmqhbjdnwi5wa6hqwipj1h55s")))

