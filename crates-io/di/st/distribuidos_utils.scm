(define-module (crates-io di st distribuidos_utils) #:use-module (crates-io))

(define-public crate-distribuidos_utils-0.1.0 (c (n "distribuidos_utils") (v "0.1.0") (h "093fy1hrci0kzwf723m2sm2ra66h3zjkj4wzcxxv6m5sqvg6nhlp")))

(define-public crate-distribuidos_utils-0.1.1 (c (n "distribuidos_utils") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kyg72aidgjcrf3w87l02xbgwlryp4lnr3c5kq21abmpxcqak18i")))

(define-public crate-distribuidos_utils-0.1.2 (c (n "distribuidos_utils") (v "0.1.2") (h "0vhvczwyzi7an9p8508cybkc1l3qh2zzwy5bzkpzwfiwfl98f5za")))

