(define-module (crates-io di st dist_tx) #:use-module (crates-io))

(define-public crate-dist_tx-0.1.0 (c (n "dist_tx") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0abkbn813813x3hxh1ww0njgam4bxq1is7c2pjmk6zdzppvdi9q3")))

(define-public crate-dist_tx-0.2.0 (c (n "dist_tx") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rd10ibcyp5ykjj94qanh6simnmrlhf45lvx2wf56xrdvr776g0b")))

(define-public crate-dist_tx-0.2.1 (c (n "dist_tx") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wp2irwqgxg5lpanlbw0jwyswwg2l6dhcqadigpccdmb53z4yjkg")))

(define-public crate-dist_tx-0.2.2 (c (n "dist_tx") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xnc8562fyhkjgzqn0bb66ql0s6f8nglikvm941vr71v1lhfk96s")))

(define-public crate-dist_tx-0.2.3 (c (n "dist_tx") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "086y60bm9l0acq73g00fnx4rjqhxfwj8zpvwj6kajiinn1p7vcyc")))

(define-public crate-dist_tx-0.2.4 (c (n "dist_tx") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00bw9vz1ssz493iv36m86smh94mlai1idxi432qzvqn70a82nfa3")))

(define-public crate-dist_tx-0.3.0-alpha-1 (c (n "dist_tx") (v "0.3.0-alpha-1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16m74z3qwykpkxi97rclv3wx285vx04y09lxxg1gjw5pf1kba9p0") (f (quote (("sync") ("default") ("async" "async-trait"))))))

(define-public crate-dist_tx-0.3.0 (c (n "dist_tx") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a3gxnqr2zsmhzfpbmjbw2nlqr7rmv33sjf4fvz9sl18hnywlw1r") (f (quote (("sync") ("default") ("async" "async-trait"))))))

(define-public crate-dist_tx-0.4.0 (c (n "dist_tx") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ww9vw84m8knxpfjm2vpyqnszzs51aikz1j3di8hw9hwfildz08h") (f (quote (("sync") ("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

(define-public crate-dist_tx-0.5.0 (c (n "dist_tx") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15l79afxjvfar6k1vx9j3zsclwrk7ll8718p4xw287yv2napg0aa") (f (quote (("sync") ("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

