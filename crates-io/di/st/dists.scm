(define-module (crates-io di st dists) #:use-module (crates-io))

(define-public crate-dists-0.1.0 (c (n "dists") (v "0.1.0") (h "0vgbq698l5m45k99aaxq8si2jmpd5viinpswnahrzv9nfi7zw4nk")))

(define-public crate-dists-0.1.2 (c (n "dists") (v "0.1.2") (h "1yml9mkrfzwslqzfzxnpyqzcaxmxv7jzgz5ddv7iaq8gb9207gbp")))

(define-public crate-dists-0.1.3 (c (n "dists") (v "0.1.3") (h "1zaling495jazspbxgpcmrbwfkkggijin6j0vvcyzvmp96iji9ar")))

(define-public crate-dists-0.1.5 (c (n "dists") (v "0.1.5") (h "0qmkdfa5cgzqib1flysxjc0wp18vsc0zawr52y7imqy39lhydlmz")))

(define-public crate-dists-0.1.6 (c (n "dists") (v "0.1.6") (d (list (d (n "axgeom") (r "1.2.*") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1alkws61qm5dvrnbsi4gcaas2dz2wgb93ya25nfq5bgliikri7hf")))

(define-public crate-dists-0.2.0 (c (n "dists") (v "0.2.0") (d (list (d (n "axgeom") (r "1.2.*") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1yacxv3zf7jgh00zy7ssbg553qjf2hqm5p2mlvy6pbbgms4r6pia")))

(define-public crate-dists-0.3.0 (c (n "dists") (v "0.3.0") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "14k3q7cj4k8myniyzh0fcabnya034wll5caf88hbhffvqgqw7w1b")))

(define-public crate-dists-0.3.1 (c (n "dists") (v "0.3.1") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0y5y4f7h91z0smgiz7d457da0ip3216bn11gabgdf54sha67clfm")))

(define-public crate-dists-0.4.0 (c (n "dists") (v "0.4.0") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0hnvpmvbqdxkvlczqdc15daiylcnfflly9s9zd0x2vkljg1nrlpy")))

(define-public crate-dists-0.4.1 (c (n "dists") (v "0.4.1") (d (list (d (n "axgeom") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1234l3afhc32mghzrsl4wq1h848vxvyzsg27q2nb7g9gpqxd5sdr")))

(define-public crate-dists-0.4.2 (c (n "dists") (v "0.4.2") (d (list (d (n "axgeom") (r "^1.9") (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "00r1rygjag4y6r5xkx85di66yr026h5kw7c73msk5a9v7mqlga1q")))

