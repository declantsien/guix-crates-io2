(define-module (crates-io di st distrustc) #:use-module (crates-io))

(define-public crate-distrustc-0.1.0 (c (n "distrustc") (v "0.1.0") (d (list (d (n "distrustc-client") (r "^0.1.0") (d #t) (k 0)) (d (n "distrustc-server") (r "^0.1.0") (d #t) (k 0)))) (h "0lr5xsm0q3hdk9b2wdiqm3da3jglxvq0dny4jg72ffjkn9zfav6g")))

