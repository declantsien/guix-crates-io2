(define-module (crates-io di st distribuidos_sync) #:use-module (crates-io))

(define-public crate-distribuidos_sync-0.1.0 (c (n "distribuidos_sync") (v "0.1.0") (h "06r7z51aip6yjsdijj4g74hchnsxiazkimy78fcx36j38mw3xxwa")))

(define-public crate-distribuidos_sync-1.0.0 (c (n "distribuidos_sync") (v "1.0.0") (h "09z4f2hz7hk5q5j379799m9z1087gyvxmac9855qjcsjdbbqq3a6")))

(define-public crate-distribuidos_sync-1.0.1 (c (n "distribuidos_sync") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bq3452vg6ms4pwlky6rxlr5pnjhbs4aric9lgsnaayh98nm3x09")))

(define-public crate-distribuidos_sync-1.1.0 (c (n "distribuidos_sync") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vnc5ld67c5z8l1wkd2cx0mz89j47fk80209yl9za7sk8x9i3sfz")))

(define-public crate-distribuidos_sync-1.1.1 (c (n "distribuidos_sync") (v "1.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05fxgwqprwhdqf2329g6x84p9zymnj2mw3ixz51pivsp2bq2lk24")))

(define-public crate-distribuidos_sync-1.1.2 (c (n "distribuidos_sync") (v "1.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "135ihzsb9i1k14fkggyimx7p4n0i2fnrrn5kwha134jpn5ak0rdv")))

(define-public crate-distribuidos_sync-1.1.3 (c (n "distribuidos_sync") (v "1.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z6ffz7bwcf6mga8d1q8sf004jvhlplhvd18cl54rdm73hbk673a")))

(define-public crate-distribuidos_sync-1.1.4 (c (n "distribuidos_sync") (v "1.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zd750gkj61561xz8m188khwf5qvdclgy258mr5gn71k1k8xqmzn")))

(define-public crate-distribuidos_sync-1.1.5 (c (n "distribuidos_sync") (v "1.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05lwvv5wfh5jdf7mszr2rrc8bijqdmy73wn08fb22b3kl28x1in6")))

(define-public crate-distribuidos_sync-1.2.0 (c (n "distribuidos_sync") (v "1.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "170ynrig9zr2kr86d81y3727f7kxlamkpmnx12x6ms5qhz7b13xk")))

