(define-module (crates-io di st disturbance) #:use-module (crates-io))

(define-public crate-disturbance-0.1.0 (c (n "disturbance") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.5.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0jn760w24833gzllxhahd5w6zv71563lz9wq6hy28cncpxz4y8lr")))

(define-public crate-disturbance-0.2.0 (c (n "disturbance") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.5.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0l0dky7a4mrgbw2lh958ibbmb7g1zsv54ypgz6fi5wsp2kgfca93")))

