(define-module (crates-io di st distance_difference_triangulation) #:use-module (crates-io))

(define-public crate-distance_difference_triangulation-0.3.0 (c (n "distance_difference_triangulation") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1nw8rh43qa6pp0xdggqkbzfw41ql6f9r63qvfap53cljjwzvyywb") (f (quote (("std") ("default" "std"))))))

(define-public crate-distance_difference_triangulation-0.3.1 (c (n "distance_difference_triangulation") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1kc1yfz8cyd4h89f8pb8rc43nm3l41ks7lawrb63azvffhgbas32") (f (quote (("std") ("default" "std"))))))

(define-public crate-distance_difference_triangulation-0.3.2 (c (n "distance_difference_triangulation") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1kyhbi458rgxainqr82n58nrwhbmw0laanbaa064r6sd8q3w1mvr") (f (quote (("std") ("default" "std"))))))

