(define-module (crates-io di st distro-info) #:use-module (crates-io))

(define-public crate-distro-info-0.1.0 (c (n "distro-info") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1p3zfhpsdd5480bc1pfkjhhaqzaa98p3b6g53yg044izzavxzvvl")))

(define-public crate-distro-info-0.1.1 (c (n "distro-info") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1pz69jy6v8937y03hg8q0kckfdjn9kblpnvx3nbd1hq3xm0ymb2v")))

(define-public crate-distro-info-0.1.2 (c (n "distro-info") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0i14kb44wflmksaav5ds5nxdsnxgnr2azq0jdgilqwv2h30352bc")))

(define-public crate-distro-info-0.1.3 (c (n "distro-info") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1hr8l441h82x28adh7jjmincga2xs3ldn09fv7jlgn17n6h9q5gn")))

(define-public crate-distro-info-0.2.0 (c (n "distro-info") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "19bfdgz2i485q5nm0f97fp1a8rn08cgwbqzm9ps092w26kyfd1n6")))

(define-public crate-distro-info-0.3.0 (c (n "distro-info") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "17qzq17q4ar584zq255hwsiqani1dcwhbajbalv71yp486jssbw1")))

(define-public crate-distro-info-0.4.0 (c (n "distro-info") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0wfaw2xqnbx6a26485vq6l5bwwrfflq95dn07r2hx6gd5izj64pg")))

