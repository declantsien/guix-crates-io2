(define-module (crates-io di st distance) #:use-module (crates-io))

(define-public crate-distance-0.1.0 (c (n "distance") (v "0.1.0") (h "044m0jda9n2zp7ap14hfxf8gnxrlm1dl7iqcrmzcn13z65wvr7mb")))

(define-public crate-distance-0.2.0 (c (n "distance") (v "0.2.0") (h "1jdlr5wcyidabxgn7wghl0n2syzk6flgdc762kpb9qlhqysdd4jd")))

(define-public crate-distance-0.3.0 (c (n "distance") (v "0.3.0") (h "1zalvw1fyxdgnfq1qvm60fpyyab0ahdwcsdyyd7ka12vaiw317wg")))

(define-public crate-distance-0.4.0 (c (n "distance") (v "0.4.0") (h "0h4jzrnwdpdaa6lxainpfqqk1r7msa3s78ql647pv7c4rxj8d7bd")))

