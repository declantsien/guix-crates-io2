(define-module (crates-io di st distill-downstream-lmdb-sys) #:use-module (crates-io))

(define-public crate-distill-downstream-lmdb-sys-0.8.0-windows-fix (c (n "distill-downstream-lmdb-sys") (v "0.8.0-windows-fix") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "10vcysl9dkgd8nm26mf2wxjvlzjfk6mhzymvfwzlw26x9pp1cs8m")))

