(define-module (crates-io di st dist_lock_codegen) #:use-module (crates-io))

(define-public crate-dist_lock_codegen-0.0.1 (c (n "dist_lock_codegen") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "015svywk5mrzygvzywwih7455z7v667awir5zzm4dx887i28hcyn") (f (quote (("zookeeper") ("redis") ("diesel") ("default" "redis") ("async"))))))

