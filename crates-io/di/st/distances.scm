(define-module (crates-io di st distances) #:use-module (crates-io))

(define-public crate-distances-0.1.0 (c (n "distances") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.2.7") (d #t) (k 0)))) (h "1bm99zpgxr3c6jzlsf6anbhq3h1rmxa3l894m3rdsl1h8sxvfs78")))

(define-public crate-distances-0.1.1 (c (n "distances") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "time") (r "^0.2.7") (d #t) (k 0)))) (h "1q1dzisd16y18j2j5cd8vrdf7m449wpc25wc2k1zcxy8barfikw1")))

(define-public crate-distances-1.0.0 (c (n "distances") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0dwwjh8kxhg9m2hn3kwwyq7dfg0lcc3xvsxgkcxvv2ykr0w8jfv9")))

(define-public crate-distances-1.0.1 (c (n "distances") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0yw27z0377gwvwxvggv6f1wkrc5jnvf68s8ys43cr8aixizfh7qa")))

(define-public crate-distances-1.1.0 (c (n "distances") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)))) (h "0sv724xpi3dxx89ssjxxy163s95mr1pfs07d1m6c0hsklb0ypmr5")))

(define-public crate-distances-1.1.1 (c (n "distances") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1fv0f096syvn49fsqz6xx0fypc749x2cdvxc51pq4km8x4cjh2y2")))

(define-public crate-distances-1.1.2 (c (n "distances") (v "1.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "0f4r37i8z8h6kiz8hxiz4hjpqkwck0jp5blijbc1fz48mnqk12pa")))

(define-public crate-distances-1.1.3 (c (n "distances") (v "1.1.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "1bniib2pq6x80yl6yps5m6sp2rlz1aq006c3mg6111x34gqirqql")))

(define-public crate-distances-1.1.4 (c (n "distances") (v "1.1.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "093xz69zprvvmy3s0lxpb8za3dqi1cflsvckhy2vq5m6j5srha8r")))

(define-public crate-distances-1.2.0 (c (n "distances") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "0s5qqnmspl5hyp66h7msznrdknr3cyi4kpcahw2ff0hb3mhkkdbc")))

(define-public crate-distances-1.3.0 (c (n "distances") (v "1.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "17q6098m9zdv15m7p2rvhjw6x7jclk4b6c7h3z5awxwvgc1ja3s6")))

(define-public crate-distances-1.4.0 (c (n "distances") (v "1.4.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "0zdv1vkx0n8l2sg7g7bwz7ss1zypp4ibgy8rb33i2z6fc338ix81")))

(define-public crate-distances-1.4.1 (c (n "distances") (v "1.4.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)))) (h "00a2dgsza7x8rssmsy144s3fpn81hrviyy3vhnh4h0kys1v3mw9y")))

(define-public crate-distances-1.5.0 (c (n "distances") (v "1.5.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "1m4510fahqa4ynlrc1mgjd5w8rh20xjshq04lhbhlqs6x3rk3fx2")))

(define-public crate-distances-1.5.1 (c (n "distances") (v "1.5.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "symagen") (r "^0.1.1") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "0l5656kin8b3kp5nsky1crr5fl5kh6l1s422acm7mzg7p7bcfxbl")))

(define-public crate-distances-1.6.0 (c (n "distances") (v "1.6.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symagen") (r "^0.1.3") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "0lnla5k8zqf4i239gah6db5zblyzysvy5r87kh6amq7fa653pg9n")))

(define-public crate-distances-1.6.1 (c (n "distances") (v "1.6.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symagen") (r "^0.2.0") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "0zwkdvyy8s7iklfbpipy61kg5ya7zrp95wxszz7qfcxnjdyz5nqz")))

(define-public crate-distances-1.6.2 (c (n "distances") (v "1.6.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "symagen") (r "^0.2.1") (d #t) (k 2)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "0kb0wnzbim337qxdnwraf23rghw0a71fwr8h4r42kqh5c3z6sciy")))

(define-public crate-distances-1.6.3 (c (n "distances") (v "1.6.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "test-case") (r "^3.2.1") (d #t) (k 2)))) (h "02knp23qrjbfp8cqd0xpj5q64d3lczvq75pva8l18qlh81mn69zs")))

