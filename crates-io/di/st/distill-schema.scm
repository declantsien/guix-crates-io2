(define-module (crates-io di st distill-schema) #:use-module (crates-io))

(define-public crate-distill-schema-0.0.1 (c (n "distill-schema") (v "0.0.1") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "distill-core") (r "^0.0.1") (d #t) (k 0)))) (h "1k7h6qvba9igjibnhn5rmgrwvhn79mq9sy1hypdmjcw8pxf02v2i") (y #t)))

(define-public crate-distill-schema-0.0.2 (c (n "distill-schema") (v "0.0.2") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "distill-core") (r "=0.0.2") (d #t) (k 0)))) (h "15d3y4lj8iswvhggp9dm1ak21m5rf3f312ibbkbyjr9b0szk7axz")))

(define-public crate-distill-schema-0.0.3 (c (n "distill-schema") (v "0.0.3") (d (list (d (n "capnp") (r "^0.14.0") (d #t) (k 0)) (d (n "distill-core") (r "^0.0.3") (d #t) (k 0)))) (h "0q86i38qshx282jp1mdwxjh6jwgz2fxp5hlh33kr7bjb6j884m26")))

