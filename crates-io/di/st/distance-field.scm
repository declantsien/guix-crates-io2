(define-module (crates-io di st distance-field) #:use-module (crates-io))

(define-public crate-distance-field-0.1.0 (c (n "distance-field") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)))) (h "01dwvbcbcwf9kz49ba3m024xxqz885prxdpmcr3ljg0mnwp3lppa")))

(define-public crate-distance-field-0.1.1 (c (n "distance-field") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)))) (h "0ds4i8lxg0mn623ykd1kmb1d0daypmyf9b3arp3xw6c8wvirp5m6")))

(define-public crate-distance-field-0.1.2 (c (n "distance-field") (v "0.1.2") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "1axdf451mj2chl2nkilvq3kyzdv3hvfixw3sjmbzqnmmr6wv1bfb")))

(define-public crate-distance-field-0.1.3 (c (n "distance-field") (v "0.1.3") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "0afqjyhv3qdrvdkan9ya4kqj6jsk31l2wrdjywvgqxg9in2qfv94")))

(define-public crate-distance-field-0.1.4 (c (n "distance-field") (v "0.1.4") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "1v4im1pxy9n0grzc2i2dpwhgcs9jxkm82a6h4n0vqy3l8csbr0yx")))

(define-public crate-distance-field-0.1.5 (c (n "distance-field") (v "0.1.5") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "0281g41jbj7s9q1zi6dkzp8w1hgpj08rhby73748w12g0ks3h0zi")))

(define-public crate-distance-field-0.1.6 (c (n "distance-field") (v "0.1.6") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "0apfyb64j12anw96838zm3szman0v3azyzwynjrsbq1df60dhyh6")))

(define-public crate-distance-field-0.1.7 (c (n "distance-field") (v "0.1.7") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 2)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "spiral") (r "^0.1") (d #t) (k 0)))) (h "0230h3r1kjbhvmszmdbnd7jnzf722r02cnpjb24m7p9xdd6hvmzp")))

(define-public crate-distance-field-0.1.8 (c (n "distance-field") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3.6") (k 2)) (d (n "docopt") (r "^1.1.1") (d #t) (k 2)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "pprof") (r "^0.10.1") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "spiral") (r "^0.2.0") (d #t) (k 0)))) (h "01cvjssyhifsimivkyy658ar85xkvwnlzq915pwabq4359frkrmk")))

(define-public crate-distance-field-0.2.0 (c (n "distance-field") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (k 2)) (d (n "docopt") (r "^1.1.1") (d #t) (k 2)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "pprof") (r "^0.10.1") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "spiral") (r "^0.2.0") (d #t) (k 0)))) (h "1wmsi91x0pb4p3glhg9w9jwh2d420z26paa29lmwz2bzwzkwkx9s")))

(define-public crate-distance-field-0.2.1 (c (n "distance-field") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "docopt") (r "^1.1.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "pprof") (r "^0.13.0") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "spiral") (r "^0.2.0") (d #t) (k 0)))) (h "1r36xyldl7x7db8haii5vaqhh4m11ia134slw3spvndipv9yv3hw")))

