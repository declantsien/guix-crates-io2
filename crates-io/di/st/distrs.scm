(define-module (crates-io di st distrs) #:use-module (crates-io))

(define-public crate-distrs-0.1.0 (c (n "distrs") (v "0.1.0") (h "1v6zy45w94s3jc3q7mswlfyb9rjzq9l9ilvrfmwgqd9ivqwr73wm")))

(define-public crate-distrs-0.1.1 (c (n "distrs") (v "0.1.1") (h "06wq69i9zpklv1r4rcy8bnk2rsv3x6n9hgln2i0ymswi6nyzgjhy")))

(define-public crate-distrs-0.1.2 (c (n "distrs") (v "0.1.2") (h "1c0wqjlx52f5wdssc4mss369iiivwvsa1ln764xlnkkhfi0h7584")))

(define-public crate-distrs-0.1.3 (c (n "distrs") (v "0.1.3") (h "0fhr26pdaqq24mm7xhsxcfv91x6l9y2ps2mwggckchw8r7z52c8v")))

(define-public crate-distrs-0.2.0 (c (n "distrs") (v "0.2.0") (h "1rcdsflbbps6kzrdg2yk0b5x9h7j67fyz387faajsxj5g6rc6y4d")))

(define-public crate-distrs-0.2.1 (c (n "distrs") (v "0.2.1") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "04jkj2bbrm8sbzn2c7mk10j5sf25280n4vbwkmxl9b3yvzwlsjzv") (f (quote (("no_std" "libm"))))))

