(define-module (crates-io di st distribuidos_tp1_gateway) #:use-module (crates-io))

(define-public crate-distribuidos_tp1_gateway-0.1.0 (c (n "distribuidos_tp1_gateway") (v "0.1.0") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.0") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jmllbdpi9qgwng9jfxhdrgabad3y6ldsyk7g3gfpzz4hb5vlzvk")))

(define-public crate-distribuidos_tp1_gateway-0.1.1 (c (n "distribuidos_tp1_gateway") (v "0.1.1") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.1") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03xy9mj8brn913psjs9nagyhdx7xzlk7id696b7lql6s8gqsz0im")))

(define-public crate-distribuidos_tp1_gateway-0.1.2 (c (n "distribuidos_tp1_gateway") (v "0.1.2") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.1") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dlds97mwqdp8kqvqfqw509v13pi3gn0sz8w8s71917f5sckmi8l")))

(define-public crate-distribuidos_tp1_gateway-0.1.3 (c (n "distribuidos_tp1_gateway") (v "0.1.3") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.1") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zfr0macr1112h3nf36bac9j3m1msz0jdn8lcwdwf3m0hwgxj78r")))

(define-public crate-distribuidos_tp1_gateway-0.1.4 (c (n "distribuidos_tp1_gateway") (v "0.1.4") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.3") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16y2bap6samama24lizlappvqss9r4q0s8rfzmbgn0hib61pyinn")))

(define-public crate-distribuidos_tp1_gateway-0.1.5 (c (n "distribuidos_tp1_gateway") (v "0.1.5") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.4") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w7jbs7q97267v9ap1plsphfsi2kcydhp4ajmj87n33syx7qazd1")))

(define-public crate-distribuidos_tp1_gateway-0.1.6 (c (n "distribuidos_tp1_gateway") (v "0.1.6") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.5") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r7671yx3l5dlcykmrphvim5bsizka9b8pr7q38ma4vrgzm7iqmc")))

(define-public crate-distribuidos_tp1_gateway-0.1.7 (c (n "distribuidos_tp1_gateway") (v "0.1.7") (d (list (d (n "distribuidos_tp1_protocols") (r "^1.2.6") (d #t) (k 0)) (d (n "distribuidos_types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hifnf9n8mjj4b92d4n08n64crldsan1lhrqybcil12ndcb3bwx9")))

