(define-module (crates-io di st distribuidos_tp1_utils) #:use-module (crates-io))

(define-public crate-distribuidos_tp1_utils-0.1.0 (c (n "distribuidos_tp1_utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "distribuidos_tp1_protocols") (r "^1.1.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19bnfw4ip0m4dw8xs0bxmrcjypw14va0990y928qxmac7hs52mla")))

(define-public crate-distribuidos_tp1_utils-0.1.1 (c (n "distribuidos_tp1_utils") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0hvnlymmvp38s0vbzilf18xb6qaf06dgfrkxcscyzmdgags941cx")))

(define-public crate-distribuidos_tp1_utils-1.0.0 (c (n "distribuidos_tp1_utils") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1r3nk2xwpj1ijm65kmnph1bcfqr84g52jmqkyhnr13pwbkw6pbxy")))

(define-public crate-distribuidos_tp1_utils-1.0.1 (c (n "distribuidos_tp1_utils") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "05jqgq6rwnshxsh08df3gsiwip2jalh3ywd9w20k9ajvphkpsy5z")))

(define-public crate-distribuidos_tp1_utils-1.0.2 (c (n "distribuidos_tp1_utils") (v "1.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01svxkrc19xikv71bljc2i5rajwrpypk4i6y29a0y42fpv0dy45h")))

(define-public crate-distribuidos_tp1_utils-1.0.3 (c (n "distribuidos_tp1_utils") (v "1.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ahk79xygcb0kb1wh7xb35a0ws56x3j7y1fl8509fhq2i9j7184j")))

