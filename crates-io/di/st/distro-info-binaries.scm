(define-module (crates-io di st distro-info-binaries) #:use-module (crates-io))

(define-public crate-distro-info-binaries-0.4.0 (c (n "distro-info-binaries") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "distro-info") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "04plk2imchdgq493xlpl7g5d42hp8pmyxpb4svi6d0x9nn27inpj")))

