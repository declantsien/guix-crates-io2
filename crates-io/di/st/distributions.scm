(define-module (crates-io di st distributions) #:use-module (crates-io))

(define-public crate-distributions-0.0.1 (c (n "distributions") (v "0.0.1") (h "199x01v8vkc3yd8hcbwwgax921j5zjs799b73gq10g0d3wqxqbbq")))

(define-public crate-distributions-0.0.2 (c (n "distributions") (v "0.0.2") (d (list (d (n "GSL") (r "~0.4.11") (d #t) (k 0)))) (h "01k1rah8hf3nirg7cwrb4vc2sjvll1zq2avprlvvpll9vf9c2q9v")))

