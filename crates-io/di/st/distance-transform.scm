(define-module (crates-io di st distance-transform) #:use-module (crates-io))

(define-public crate-distance-transform-0.1.0 (c (n "distance-transform") (v "0.1.0") (d (list (d (n "image") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1b5jvrl9phbb01jp10793d7dhrs3vlj4psbl8ciyw8a9xgvgdnqs") (f (quote (("image-utils" "image") ("default"))))))

(define-public crate-distance-transform-0.1.1 (c (n "distance-transform") (v "0.1.1") (d (list (d (n "image") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "0f7mpy2ip29jzidp7kixacn51k43l7mf186r0gapl81q0pdnfp6m") (f (quote (("image-utils" "image") ("default"))))))

(define-public crate-distance-transform-0.1.2 (c (n "distance-transform") (v "0.1.2") (d (list (d (n "image") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1w3mw8wpn2d1zmjdz5i3q6z7hykysxxdxkggs5syn7p9i7dy6z8r") (f (quote (("image-utils" "image") ("default"))))))

