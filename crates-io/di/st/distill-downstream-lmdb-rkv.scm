(define-module (crates-io di st distill-downstream-lmdb-rkv) #:use-module (crates-io))

(define-public crate-distill-downstream-lmdb-rkv-0.11.0-windows-fix (c (n "distill-downstream-lmdb-rkv") (v "0.11.0-windows-fix") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 2)) (d (n "distill-downstream-lmdb-sys") (r "^0.8.0-windows-fix") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1j8aqdkh26vzq3ldk2zms9r0k4azrmcrzpymfcil8gxpfcfd48gm")))

