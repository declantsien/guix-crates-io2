(define-module (crates-io di st distroprotos) #:use-module (crates-io))

(define-public crate-distroprotos-0.2.0 (c (n "distroprotos") (v "0.2.0") (d (list (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (f (quote ("transport" "tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "=0.7.1") (d #t) (k 1)) (d (n "vergen") (r "^1") (d #t) (k 1)))) (h "0aah6nc627422j1nd6igh4nzy496jdfcd8g34kzfwwndpf1aflli") (y #t)))

