(define-module (crates-io di rk dirk) #:use-module (crates-io))

(define-public crate-dirk-0.1.1 (c (n "dirk") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.42.0") (d #t) (k 0)) (d (n "rusoto_ssm") (r "^0.42.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "078r767ypmszgjyyxsxf8plrq1brqz8bab9hbv4hl2fqj9i2a634")))

