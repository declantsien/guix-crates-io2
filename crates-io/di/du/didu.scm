(define-module (crates-io di du didu) #:use-module (crates-io))

(define-public crate-didu-2.2.1 (c (n "didu") (v "2.2.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "093wly9rf9arcdvbij4caslgb514a45p7x9bl44yldbcydj97p4v")))

(define-public crate-didu-2.2.2 (c (n "didu") (v "2.2.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "1g75dj4bky5ryjmsfh7l99rc3nx5rjkg0srlywih43b5b95rsyn5")))

(define-public crate-didu-2.2.3 (c (n "didu") (v "2.2.3") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "1ih0wwp2228lrxh5nvmy2s6hgni3s20619jzbm8w3ywd3hrd7s56")))

(define-public crate-didu-2.3.1 (c (n "didu") (v "2.3.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "0gjjbgahlssbgd42dpl5fw1n1wc59b5lamhpncp0wrzd5a6r59gy")))

(define-public crate-didu-2.4.0 (c (n "didu") (v "2.4.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "0rz7fsdddfsrmm98x99nwwjx2avk6121q785vw9d2q3qxql7prch")))

(define-public crate-didu-2.5.0 (c (n "didu") (v "2.5.0") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "19gcjvdmmih3zqv9pfhfczzgfqqr5dacw6dbh0jgrfpgva59nhc7")))

(define-public crate-didu-2.5.1 (c (n "didu") (v "2.5.1") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "05gbqpqwb5nfc6fvl8kmx9ig790sdlifsq6pzzgdh1pjyqfmhf0y")))

(define-public crate-didu-2.5.2 (c (n "didu") (v "2.5.2") (d (list (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "recolored") (r "^1.9.3") (d #t) (k 0)) (d (n "unit-conversions") (r "^0.1.11-alpha.5") (d #t) (k 0)))) (h "1xmya1fa9bxhjkf1zfahqddm05bdhywh7gs8800h4k1lqs3720n6")))

