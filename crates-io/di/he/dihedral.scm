(define-module (crates-io di he dihedral) #:use-module (crates-io))

(define-public crate-dihedral-0.0.1 (c (n "dihedral") (v "0.0.1") (h "1z6vnx04984vyp8lpixfn723n4cnabs715lkpvawrwc4whfid448")))

(define-public crate-dihedral-0.0.2 (c (n "dihedral") (v "0.0.2") (h "1vynh28rc1v8i1ws05diw01vl8s7cnqc0zbh0h90zfhmphqjsr34") (f (quote (("f32"))))))

(define-public crate-dihedral-0.0.3 (c (n "dihedral") (v "0.0.3") (h "0gipbns58kb663zqb7bm7s6gc3bzj6hx7cz7w3794a9h3bzwqm01") (f (quote (("f32"))))))

(define-public crate-dihedral-0.0.4 (c (n "dihedral") (v "0.0.4") (h "1ynh1v6rwqsilnlrcpq5w9na57nmf7vi0adgmbnc9mpa1ygxisrg") (f (quote (("f32"))))))

