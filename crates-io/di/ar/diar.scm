(define-module (crates-io di ar diar) #:use-module (crates-io))

(define-public crate-diar-1.0.0 (c (n "diar") (v "1.0.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "1gpx6av7slxmjvlcv5sc7bpvqpmq7pygad3rlg2nrs4fx8mpgxcr")))

(define-public crate-diar-1.0.1 (c (n "diar") (v "1.0.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "1ix3svrg8pwqhh3nyhcxigs754b92hnnyvkywkpycmarkd6rv9qd")))

(define-public crate-diar-1.0.2 (c (n "diar") (v "1.0.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "08p5xhjzyyrdswgnpx6cac3bzqf76fwp8pb5j3cw39jx06k1ga2h")))

(define-public crate-diar-1.0.3 (c (n "diar") (v "1.0.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "16r5xi4qz6cmwa3mjr0sqnxapg83r39ava5bgcawxqwz1mdsa41f")))

(define-public crate-diar-2.0.0 (c (n "diar") (v "2.0.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "1jckxmxc6jj3iz8k2qrb5zpzzkgkabffn1b52jzb2vcg2l533fmx")))

(define-public crate-diar-2.1.1 (c (n "diar") (v "2.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "17gd1mrs1zz24fb1ff95j1zvhss1zk95fi2ixilcza2f3yqhgmza")))

(define-public crate-diar-2.2.0 (c (n "diar") (v "2.2.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "0dmgh4s1bby0bphv6ayl9jgcmmbl6ag2ipi8skk2dbhdfa6acsyk")))

(define-public crate-diar-2.3.0 (c (n "diar") (v "2.3.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "sled") (r "^0.27") (d #t) (k 0)))) (h "1rzi3p33ny1zkvxmgvk1rj63j5cblsh3sms9gfnb89scjn22q5xf")))

