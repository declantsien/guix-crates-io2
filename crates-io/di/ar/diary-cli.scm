(define-module (crates-io di ar diary-cli) #:use-module (crates-io))

(define-public crate-diary-cli-1.3.4 (c (n "diary-cli") (v "1.3.4") (d (list (d (n "chrono") (r "^0.4.30") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy-db") (r "^1.5.3") (d #t) (k 0)) (d (n "soulog") (r "^1.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "00gvx8q130hvgm8lpg9fn9a8p3fbl0qklgpnc0jm4f06d715f95i")))

