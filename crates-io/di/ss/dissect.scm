(define-module (crates-io di ss dissect) #:use-module (crates-io))

(define-public crate-dissect-0.1.0 (c (n "dissect") (v "0.1.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.2") (f (quote ("known-key"))) (d #t) (k 0)))) (h "0nmqwy0v6xa5p0kqlv1gxk2dsg6sgh1j1s3ffjnalhd68p4jdjrw")))

(define-public crate-dissect-0.1.1 (c (n "dissect") (v "0.1.1") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1pids9cv0w8p072yacg192h29n9j3723saagvill4jdndhwsk3yj")))

(define-public crate-dissect-0.1.2 (c (n "dissect") (v "0.1.2") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.3") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1rkk39jw8c712y6kmjal0sv0bs2gvgqb3lha29qj7bl5k360qr0h")))

(define-public crate-dissect-0.2.0 (c (n "dissect") (v "0.2.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.4") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1d1k5xnf6cn1clyl166p7bav43xn0ij61vmwa0ybmbpm9pyc4gmr")))

(define-public crate-dissect-0.2.1 (c (n "dissect") (v "0.2.1") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.4") (f (quote ("known-key"))) (d #t) (k 0)))) (h "18bz7hyn6falwsjgjcn9xywwz9bzg9yjcrg4cn9a0a456ra78465")))

(define-public crate-dissect-0.2.2 (c (n "dissect") (v "0.2.2") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.5") (f (quote ("known-key"))) (d #t) (k 0)))) (h "0nlwdm09fak6593xxyyf5jfwv13x5gh9fvgwjq4vfs7v97s5x1a5") (y #t)))

(define-public crate-dissect-0.3.0 (c (n "dissect") (v "0.3.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.5") (f (quote ("known-key"))) (d #t) (k 0)))) (h "1apdpk25291593wyp1r4zyv7058i5sn9r4xj7zs772iixik4yxrn")))

(define-public crate-dissect-0.4.0 (c (n "dissect") (v "0.4.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.6") (f (quote ("known-key"))) (d #t) (k 0)))) (h "09dns9rnc75m2ppfrlma7867hbnjl5j7gib3dynwfqi4llpxn1l2")))

(define-public crate-dissect-0.5.0 (c (n "dissect") (v "0.5.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.7") (f (quote ("known-key"))) (d #t) (k 0)))) (h "09aybfvwk0yskg2l57ziy5mnvlb6y0ajnax9777mzkhg54w6pnw7")))

(define-public crate-dissect-0.6.0 (c (n "dissect") (v "0.6.0") (d (list (d (n "halfbrown") (r "^0.1") (d #t) (k 0)) (d (n "simd-json") (r "^0.8") (f (quote ("known-key"))) (d #t) (k 0)))) (h "10cmvsszrm59vpbbj1kimvmvxv1vginhprddmsh214qlc7x6d3kd")))

(define-public crate-dissect-0.7.0 (c (n "dissect") (v "0.7.0") (d (list (d (n "simd-json") (r "^0.10.1") (d #t) (k 0)))) (h "1psb56pwis7a4zhx5d377c4l7vsab88fydvn1rd6p2riynlmb955") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

(define-public crate-dissect-0.7.1 (c (n "dissect") (v "0.7.1") (d (list (d (n "simd-json") (r "^0.11") (d #t) (k 0)))) (h "005crf3p3bw50msrlr61rxm89k97az6w57g92n2ifrk7rvpxh48n") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

(define-public crate-dissect-0.7.2 (c (n "dissect") (v "0.7.2") (d (list (d (n "simd-json") (r "^0.12") (d #t) (k 0)))) (h "1rnpnrb2040in5pd3viar2sfrqnc4hlmlw1vr36hzq2nnajkfxlj") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

(define-public crate-dissect-0.7.3 (c (n "dissect") (v "0.7.3") (d (list (d (n "simd-json") (r "^0.13") (d #t) (k 0)))) (h "0dpxv3wxwhdk4fjjiwq1z77igcmjq19qpahj70x6z703s6v13fz8") (f (quote (("known-key" "simd-json/known-key") ("default" "known-key") ("arraybackend" "simd-json/arraybackend"))))))

