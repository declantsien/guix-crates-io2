(define-module (crates-io di ss dissolve) #:use-module (crates-io))

(define-public crate-dissolve-0.1.0 (c (n "dissolve") (v "0.1.0") (d (list (d (n "html5ever") (r "~0.5.4") (d #t) (k 0)) (d (n "tendril") (r "~0.2.2") (d #t) (k 0)))) (h "0gn3hyl6ir59izlshwvbxr1cjd2iqkl50p6r1g18kjh44pk3xwyn")))

(define-public crate-dissolve-0.1.1 (c (n "dissolve") (v "0.1.1") (d (list (d (n "html5ever") (r "~0.5.4") (d #t) (k 0)) (d (n "tendril") (r "~0.2.2") (d #t) (k 0)))) (h "08adgf613k7pls3hsmaadrj50p4wgfgsm25zfiy4rd4xccnznzb4")))

(define-public crate-dissolve-0.1.2 (c (n "dissolve") (v "0.1.2") (d (list (d (n "html5ever") (r "~0.5.4") (d #t) (k 0)) (d (n "tendril") (r "~0.2.2") (d #t) (k 0)))) (h "13309mayvbdavkrjdb656jzggp863wdm4c6gp38nzwc165zfvj9h")))

(define-public crate-dissolve-0.1.3 (c (n "dissolve") (v "0.1.3") (d (list (d (n "html5ever") (r "~0.5.4") (d #t) (k 0)) (d (n "tendril") (r "~0.2.2") (d #t) (k 0)))) (h "06p2gibbgj126bdgmw966w1cw1777cvr9l3kdacsmsi1l3y760rs")))

(define-public crate-dissolve-0.2.0 (c (n "dissolve") (v "0.2.0") (d (list (d (n "html5ever") (r "~0.21.0") (d #t) (k 0)) (d (n "tendril") (r "~0.4.0") (d #t) (k 0)))) (h "1vhs8pawwypkinmnh6hr8i31hc8z77fqcd78ncv2ck5xl75ad4xn")))

(define-public crate-dissolve-0.2.1 (c (n "dissolve") (v "0.2.1") (d (list (d (n "html5ever") (r "~0.21.0") (d #t) (k 0)) (d (n "tendril") (r "~0.4.0") (d #t) (k 0)))) (h "1069vll6cnj1q0bnkr8j7dxgxm59dk143fqywqlj83z9lqvqq62l")))

(define-public crate-dissolve-0.2.2 (c (n "dissolve") (v "0.2.2") (d (list (d (n "html5ever") (r "~0.21.0") (d #t) (k 0)) (d (n "tendril") (r "~0.4.0") (d #t) (k 0)))) (h "140sja7rm2w21xwv9chwlz7jdmljnwqw6kwf5h495n8n8yz451c9")))

