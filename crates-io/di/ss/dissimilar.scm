(define-module (crates-io di ss dissimilar) #:use-module (crates-io))

(define-public crate-dissimilar-1.0.0 (c (n "dissimilar") (v "1.0.0") (h "1hln4lw7s83kqzaz82aljx23sa03jpxijlg4q2n6d8b3r2a27k2g")))

(define-public crate-dissimilar-1.0.1 (c (n "dissimilar") (v "1.0.1") (h "154ba92ifmh3js1k0hbmxz7pv95n8wmahlyvhdbnxggbs8f1dpir")))

(define-public crate-dissimilar-1.0.2 (c (n "dissimilar") (v "1.0.2") (h "1fwa892hcf7y36b8w41p14b3ld3df389y9jpglkbz55vp7s2jjzw")))

(define-public crate-dissimilar-1.0.3 (c (n "dissimilar") (v "1.0.3") (h "1pbr27j3f1c3wmb1rcif2naj0ip985x8kgylv3m9v5j05xjr7b9i")))

(define-public crate-dissimilar-1.0.4 (c (n "dissimilar") (v "1.0.4") (h "19flq7cv0y3zdbl4dhjg11q9gxmn8wxdv7383s74pn416livk5wc") (r "1.31")))

(define-public crate-dissimilar-1.0.5 (c (n "dissimilar") (v "1.0.5") (h "1fzvwgi9b72ywixsqdjmq4ipr5vl5s6j7rjh4nmvhrnj9dz0qpxx") (r "1.31")))

(define-public crate-dissimilar-1.0.6 (c (n "dissimilar") (v "1.0.6") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0phdkg9d9c1vmn5v74lq7rmaba2m52fkwcryd3cbw46pww5cc3i1") (r "1.36")))

(define-public crate-dissimilar-1.0.7 (c (n "dissimilar") (v "1.0.7") (d (list (d (n "once_cell") (r "^1") (d #t) (k 2)))) (h "0cn6i035s4hsvv078lg3alsfwjy0k2y7zy5hnsr1cvpf1v4bvqw6") (r "1.36")))

(define-public crate-dissimilar-1.0.8 (c (n "dissimilar") (v "1.0.8") (h "0n1hvsxiw0nrg6vyf0z2mvdnhf505jdpdd0934358qyrqv05j3a4") (r "1.36")))

(define-public crate-dissimilar-1.0.9 (c (n "dissimilar") (v "1.0.9") (h "0bcn4s99ghigd3yadpd7i3gljv5z2hkr07ijvvxvsxmz3yfygy2r") (r "1.36")))

