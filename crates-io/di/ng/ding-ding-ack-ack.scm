(define-module (crates-io di ng ding-ding-ack-ack) #:use-module (crates-io))

(define-public crate-ding-ding-ack-ack-0.1.0 (c (n "ding-ding-ack-ack") (v "0.1.0") (h "0wp447q5dcyqs7ny5wrjvizmyp7r1zfc7i3a0j4f1jharbkl4cxn") (y #t)))

(define-public crate-ding-ding-ack-ack-0.1.1 (c (n "ding-ding-ack-ack") (v "0.1.1") (h "0h95m9z3ps3zibiqrrficyd0sh3hbw9hdksja8qinkddc15c8shw") (y #t)))

(define-public crate-ding-ding-ack-ack-0.2.0 (c (n "ding-ding-ack-ack") (v "0.2.0") (h "1zn85nwhcgi4wc3x9870qml1qn512lcpbz5q6nf5blhhpdp73vsh") (y #t)))

(define-public crate-ding-ding-ack-ack-0.2.1 (c (n "ding-ding-ack-ack") (v "0.2.1") (h "1vm4m19720bqcafq3rscg97j870yb4wzyjbddvrmicshj4nz43kw") (y #t)))

