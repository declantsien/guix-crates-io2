(define-module (crates-io di ng dinghy-build) #:use-module (crates-io))

(define-public crate-dinghy-build-0.3.0 (c (n "dinghy-build") (v "0.3.0") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nq9x6zy3xkka3ns9qaby51xhsplfnkflfnsh584qfd764z399wz") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.3.1 (c (n "dinghy-build") (v "0.3.1") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08k4plars68fadl9m9nhnvn20gk97qmhn7rsd8mh8k63nryil18a") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.3.2 (c (n "dinghy-build") (v "0.3.2") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01f13r4bacs6mrs2jnmnwvw682b8yk9m7q41l0dmf2ir9wa793p4") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.3.3 (c (n "dinghy-build") (v "0.3.3") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qg6ahanpqwfawsai90a2xdm5rv5plibrm83dzzy9013k8zqv4n2") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.3.4 (c (n "dinghy-build") (v "0.3.4") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a9zwnj16ikcyxrzg7231layl7mnrp2mp15hhiviq38lf49f5dqi") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.3.5 (c (n "dinghy-build") (v "0.3.5") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lc184rih1bzw8ijhxwrkmp7g8snkl7blmw7awxvylp55cp0jmvm") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.0 (c (n "dinghy-build") (v "0.4.0") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bmaiija518qm9fxqwm90h0hka6b183gvpff0s423q9vxdnv604d") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.1 (c (n "dinghy-build") (v "0.4.1") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10c2a42l33lhfshh50ip3zxjnkc5rd8rh911wa34r6zszxdjaxdx") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.3 (c (n "dinghy-build") (v "0.4.3") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02a9csl9n0mbfnqkq5k55pc1z8vl6cy0g2yrvxq9mc30zy75dsrz") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.4 (c (n "dinghy-build") (v "0.4.4") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1q4414ldq2jczgxq84wv59dz7yb49wyi23dzd6dmmg6lb62hljyb") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.5 (c (n "dinghy-build") (v "0.4.5") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lvc7b90kbfwxdqwdcy2p36cw1b5g0d4cl29rdgr3dbibxpfpwkh") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.6 (c (n "dinghy-build") (v "0.4.6") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06c8mkkxlr853w90b5c9wb994kj4divhmzwp4jzd2m0wd8lm4i22") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.7 (c (n "dinghy-build") (v "0.4.7") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v7xwng1q66v5ckzwzdf845n6h1pyxc8sil49p6wcvjxj66v8k55") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.8 (c (n "dinghy-build") (v "0.4.8") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hc9xc94yp0lf7i6140d4rwladdd0a5hgszpg2pqagcnrgv6fxqh") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.10 (c (n "dinghy-build") (v "0.4.10") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b2f8ihz0mgsa34dhn7xysq013p4rr6zbpycw38d5sv4vjacrjmn") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.11 (c (n "dinghy-build") (v "0.4.11") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05p8mmn72mn1psrdg9l2wbk8gwndahlqdn0jf8kpms5vnzj7lbgx") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.12 (c (n "dinghy-build") (v "0.4.12") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p17kg2pqwkld0s78h7j6w4v45609i4gqr10wvgxm7ap9c2j6q6r") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.13 (c (n "dinghy-build") (v "0.4.13") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "113p6drxws2h6brg9zszh85hnpacikly582rapyws5gnkzvjfz0l") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.14 (c (n "dinghy-build") (v "0.4.14") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mjhrwqnfli30hib4313z4b6s97810zljwyzcyg4s2arlyxqqh62") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.15 (c (n "dinghy-build") (v "0.4.15") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ylglsf9gz5wppwr4zj7gi2qq1vcj0ssgzlwnmdrbhbnx2h8f46h") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.16 (c (n "dinghy-build") (v "0.4.16") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01n169jp87xqi85d6axvn71vsgpc4mvpgymwvcbpzml4xyd44hwd") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.17 (c (n "dinghy-build") (v "0.4.17") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fdasr0cdxk9649jd91f0p9a2w9xi9drca5aga4y5ygyvl3xc606") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.18 (c (n "dinghy-build") (v "0.4.18") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00j9vgs7lv8xk40ri53ymv86f7hcv8i4qilx8101zdq1w89pqfis") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.19 (c (n "dinghy-build") (v "0.4.19") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fvfh8p157dagllhm4nyf3jfcr50xg24dychbifiv52vk4lj2l59") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.20 (c (n "dinghy-build") (v "0.4.20") (d (list (d (n "bindgen") (r "^0") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04cwsq5z9k4al023s2dj9fjxcv9y0q2pdyajrmrhpy8s32sp2z5x") (f (quote (("with-bindgen" "bindgen"))))))

(define-public crate-dinghy-build-0.4.21 (c (n "dinghy-build") (v "0.4.21") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c8snfb60ys48bikbi6l3jj6lxbg2d52gycym5r8ssz7yajlig9n")))

(define-public crate-dinghy-build-0.4.22 (c (n "dinghy-build") (v "0.4.22") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01lrx2p7gmxaqblqifin9w7i7q3p6lkisa3y06wyfsvk0brvkr10")))

(define-public crate-dinghy-build-0.4.23 (c (n "dinghy-build") (v "0.4.23") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g6khsqknaqdbjl7p3q98wax0dzyycmgsji80f6kws3nj5s6ls49")))

(define-public crate-dinghy-build-0.4.24 (c (n "dinghy-build") (v "0.4.24") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "080p40r4yiq5vk2zj1p4g2v7l7ighpdc4zad42xg1l3f530d28s4")))

(define-public crate-dinghy-build-0.4.25 (c (n "dinghy-build") (v "0.4.25") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g30bqh1cg1slc8w958p6qzsa1vdi17ymfnn7yl1nx8c6d0gj01s")))

(define-public crate-dinghy-build-0.4.26 (c (n "dinghy-build") (v "0.4.26") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fafph47hr56jizr5vsiamsqaf4rrd79gd23zk04969lv0b3hzvl")))

(define-public crate-dinghy-build-0.4.27 (c (n "dinghy-build") (v "0.4.27") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1np90a3bh8lqrk49j2qxyl66rah0sbnqqywblhh9f052g1idpx0j")))

(define-public crate-dinghy-build-0.4.28 (c (n "dinghy-build") (v "0.4.28") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lyalhbwswgap0jkp5sbv6w35m5r6hhisd6214fx3x0nkvr48s1j")))

(define-public crate-dinghy-build-0.4.29 (c (n "dinghy-build") (v "0.4.29") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ld6xgwnxmbxw9xfzq2c9wb45g4vn273n3krqwrlkmrxsm8v6l03")))

(define-public crate-dinghy-build-0.4.30 (c (n "dinghy-build") (v "0.4.30") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fgyw4nrmdxfrp81kir05p3d0577j8rq9nz8w89gyix5ka0y9794")))

(define-public crate-dinghy-build-0.4.31 (c (n "dinghy-build") (v "0.4.31") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "162z1pjx581d9hs6plrzc3mbn52mvkxdf7an6scyf1yhbkf8x0as")))

(define-public crate-dinghy-build-0.4.32 (c (n "dinghy-build") (v "0.4.32") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dp1a8lvjy66dql6nnv0yf3gn9ayy9ap915r0965yn1kr37dra49")))

(define-public crate-dinghy-build-0.4.33 (c (n "dinghy-build") (v "0.4.33") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "166v9509pbx85s4a29qjwyrivb5pgl936f0d8dgrq8ylp17nqvz7")))

(define-public crate-dinghy-build-0.4.34 (c (n "dinghy-build") (v "0.4.34") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g524cmgi2njhq4z981izq1sifwqbzhsnbhr47jha9cyv5bcamxb")))

(define-public crate-dinghy-build-0.4.35 (c (n "dinghy-build") (v "0.4.35") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12ygr7zkxj2hy7cjgxab5kln8rvnlf0vh114lnhxmffg6agjn88w")))

(define-public crate-dinghy-build-0.4.37 (c (n "dinghy-build") (v "0.4.37") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18qzlk0xzpip7nxrl4sbysm42wcsbz4af4zjbrdv0qi34x17r80s")))

(define-public crate-dinghy-build-0.4.38 (c (n "dinghy-build") (v "0.4.38") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1aa71ymvlxx2b56ssyw1x46jr53lqg7xq01n641nwi2s7ladv8g2")))

(define-public crate-dinghy-build-0.4.39 (c (n "dinghy-build") (v "0.4.39") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0k4da3hcz9f6a7z5h4cg1gg0mhb1q34n13m6nqsawrxm5a09phx3")))

(define-public crate-dinghy-build-0.4.40 (c (n "dinghy-build") (v "0.4.40") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p3kpnx9yhnq1kjdgz6wbz5cz4dvhlv91g670fdnyrjqjgixcfjq")))

(define-public crate-dinghy-build-0.4.41 (c (n "dinghy-build") (v "0.4.41") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bid91fk6fxz2lbqvzd7fzzynk508vcnkwaiyh1972wp43y2ngpq")))

(define-public crate-dinghy-build-0.4.42 (c (n "dinghy-build") (v "0.4.42") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s5fw9pvnx018yyi98gl8i83d23chiyy0qjbjw7hq90bm170kjkl")))

(define-public crate-dinghy-build-0.4.43 (c (n "dinghy-build") (v "0.4.43") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dzgbny4wq81v1aymq33jd14alyrsvb3kdp08wzcc47lq4zf530k")))

(define-public crate-dinghy-build-0.4.44 (c (n "dinghy-build") (v "0.4.44") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rj3sk1crafw5rk3gyslvc4wjixab4i7zs3xvh265ni58570j2v8")))

(define-public crate-dinghy-build-0.4.45 (c (n "dinghy-build") (v "0.4.45") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19jcf62p2g2mmz28jx4s6ppfis13vgjwh46mp0cqkbia5ni3m1gc")))

(define-public crate-dinghy-build-0.4.46 (c (n "dinghy-build") (v "0.4.46") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dl1sr1k4xlmks9n8j09ci2zf5j8hr40a2jpgxn79yb5ssm5gdfj")))

(define-public crate-dinghy-build-0.4.47 (c (n "dinghy-build") (v "0.4.47") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x3fh4bzx3pxiv0l5qylhvz523z99gzvja1fx5pb92y5grr9mnw1")))

(define-public crate-dinghy-build-0.4.48 (c (n "dinghy-build") (v "0.4.48") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v1yagiggsa9m8w5gxjq8v7wkrjcldls51azaqxszgbq396mzaxn")))

(define-public crate-dinghy-build-0.4.50 (c (n "dinghy-build") (v "0.4.50") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lrlli52w76ldxqmmm7dri3gm7zd80kvfzmawgi54c07a1bnm1dy")))

(define-public crate-dinghy-build-0.4.51 (c (n "dinghy-build") (v "0.4.51") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q5sbd6ckzr1wn0ia2w71nvv86932gzly6aniayz4k793rrnbh8m")))

(define-public crate-dinghy-build-0.4.52 (c (n "dinghy-build") (v "0.4.52") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q4f1lv540zqys4yqs7766n41k7h3qngvm8wjp5kaa96pbd6v02i")))

(define-public crate-dinghy-build-0.4.53 (c (n "dinghy-build") (v "0.4.53") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jr8gdx9hcrnmfnahqlzb66pha9l41117an5c3k4y1zlf841xlj6")))

(define-public crate-dinghy-build-0.4.54 (c (n "dinghy-build") (v "0.4.54") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sp0k85pdhlcjq643xa53f5gxpvnx368i9gkrqsfw0dl1q2iccai")))

(define-public crate-dinghy-build-0.4.55 (c (n "dinghy-build") (v "0.4.55") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ylcs5rmam2bisgk5id3lsra5x8kz38ys1fj60240rpiaacjz77v")))

(define-public crate-dinghy-build-0.4.56 (c (n "dinghy-build") (v "0.4.56") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pf42xavqs56q0arlhlp869fxag65mqp9fvj78ax36yxf1yimdx0")))

(define-public crate-dinghy-build-0.4.57 (c (n "dinghy-build") (v "0.4.57") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zxgg4g1jvkd2jxd963hx524fazbq8qarsk1p7nj1p2wqq6lyvmp")))

(define-public crate-dinghy-build-0.4.58 (c (n "dinghy-build") (v "0.4.58") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vn1mw2p4z56fwpsxlzrgp1f1mx0dgmwidvjj2368azp8871im3n")))

(define-public crate-dinghy-build-0.4.59 (c (n "dinghy-build") (v "0.4.59") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f0527maiq7lyvwnsg7g9gx5sj5lpkfjp4zyp7jm6rfbpjpx6pq3")))

(define-public crate-dinghy-build-0.4.60 (c (n "dinghy-build") (v "0.4.60") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "059s10a5ayakgxkdp135c2ygsxw8i7zsa6igz3cr4wmy5qjqxn8d")))

(define-public crate-dinghy-build-0.4.61 (c (n "dinghy-build") (v "0.4.61") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01bqwpixn0gvf995y8ga49krx39wn9r528nwwakxk2zf95plc2ma")))

(define-public crate-dinghy-build-0.4.62 (c (n "dinghy-build") (v "0.4.62") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cfszxnc07s85j7szafl61zwk97j1jpw593dh3529x363pyw3ddc")))

(define-public crate-dinghy-build-0.4.63 (c (n "dinghy-build") (v "0.4.63") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vbwi9nf5zaq32y1crp7w9jjh3wykvkplfn92y133smhfrimfxi4")))

(define-public crate-dinghy-build-0.4.65 (c (n "dinghy-build") (v "0.4.65") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v790q378xbm9md5jrr79c9p5y7iipsllgysbrqy659nj3bp0v50")))

(define-public crate-dinghy-build-0.4.66 (c (n "dinghy-build") (v "0.4.66") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j0060hl74pjqvp3fb77a8z33cjaz0d4yg385aikkgqlah5miwjv")))

(define-public crate-dinghy-build-0.4.67 (c (n "dinghy-build") (v "0.4.67") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07yahxwzi9r9v0mvn7a84afhj9q7q99qfwl7967x4wxklv7nwxsw")))

(define-public crate-dinghy-build-0.4.68 (c (n "dinghy-build") (v "0.4.68") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11vzdlpm4diw9lk2kd0mblgvxv1bc2g6fg6pagk09vbf6z13y393")))

(define-public crate-dinghy-build-0.4.69 (c (n "dinghy-build") (v "0.4.69") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1dddfjbqkrn1pxbn5dxdklylpvpfy5dkh908ajcgsh1m973jgi7r")))

(define-public crate-dinghy-build-0.4.70 (c (n "dinghy-build") (v "0.4.70") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16cy6n6li9kynnv6s09kylsr01cd7z939qxvdcyyz0ivycj70kzy")))

(define-public crate-dinghy-build-0.4.71 (c (n "dinghy-build") (v "0.4.71") (d (list (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mm1si558xwf0gs3yzk9f3wrv0r6c4pkd5vmmd8a4kr0gv1ryxjm")))

(define-public crate-dinghy-build-0.5.0 (c (n "dinghy-build") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hgd09ncm4bf72sdb77rb25smf0xyh145g99axgnfa9yw201h8rd")))

(define-public crate-dinghy-build-0.5.1 (c (n "dinghy-build") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hq7cy4baf8aa9f2j2asr84gj45d7xxapv747xfcymjb5gc26w92")))

(define-public crate-dinghy-build-0.6.0 (c (n "dinghy-build") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0v0v5bysdw51mi8baqjzahzdbqnnj87w3gwlp8lj7m4343hj8h2q")))

(define-public crate-dinghy-build-0.6.1 (c (n "dinghy-build") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fgj3yqjm5xy3dv4svhgay5q6szp7855vwgfyp9nc8wyj8rs3g76")))

(define-public crate-dinghy-build-0.6.2 (c (n "dinghy-build") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1gabspb6m8y7iz4f8hw49ibcagfyanfwn6znhbabpry373g74ifi")))

(define-public crate-dinghy-build-0.6.3 (c (n "dinghy-build") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08mbwpsdi1pcd7z6a1rz2f0wh76s4b2c481mpdwmig4lpw242knm")))

(define-public crate-dinghy-build-0.6.4 (c (n "dinghy-build") (v "0.6.4") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1l1fkgv6pxsn6bsh90mhr6p2kk1k9ycjnydii6bx2m3q35qpd0x4")))

(define-public crate-dinghy-build-0.6.5 (c (n "dinghy-build") (v "0.6.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05cdzfjwk42j9b1gczmjr4isml0lklicw62jvxxp5mjk6bm4izvs")))

(define-public crate-dinghy-build-0.6.6 (c (n "dinghy-build") (v "0.6.6") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mswsh87m1mrrrw8rbx6mb7dg9dh4jk5mjgnrrb98fbyi46w9s82")))

(define-public crate-dinghy-build-0.6.7 (c (n "dinghy-build") (v "0.6.7") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z4w15qvp4ppinzr5wmfsk0mn358sk9b017m849nsgw994my6334")))

(define-public crate-dinghy-build-0.6.8 (c (n "dinghy-build") (v "0.6.8") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "103y0q13n02s016wp8c7rhqm6c4vqd2n9drsgc7bh0xqcvka9awf")))

(define-public crate-dinghy-build-0.7.0 (c (n "dinghy-build") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "121kydq2vpwsp0wg52li7gglpkfw9aqs3gcq8plgvsn8pl5az31v")))

(define-public crate-dinghy-build-0.7.1 (c (n "dinghy-build") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h74f13lcm37vp3jirs6yzi3v2r8m60l3fz0klvin6adkg9422l4")))

