(define-module (crates-io di ng dinglebit-log) #:use-module (crates-io))

(define-public crate-dinglebit-log-0.1.0 (c (n "dinglebit-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "00v79qjz54104g78z797dwgzbcqxgskc8g54nn10mvpzhqs2xdqn")))

(define-public crate-dinglebit-log-1.0.0 (c (n "dinglebit-log") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1j19vvcijm6fql7kgcrza828szmddh79l9kdsdzh7mirkdfcxqai")))

(define-public crate-dinglebit-log-1.1.0 (c (n "dinglebit-log") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "dinglebit-terminal") (r "^0.1.3") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1fqqfjqf1pkrcsiw9nkac8ynb6lz9h2vlhyvimjmhznp1f7m47js")))

