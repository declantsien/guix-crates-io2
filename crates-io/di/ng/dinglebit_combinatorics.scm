(define-module (crates-io di ng dinglebit_combinatorics) #:use-module (crates-io))

(define-public crate-dinglebit_combinatorics-1.0.0 (c (n "dinglebit_combinatorics") (v "1.0.0") (h "1h57wjd838i2vcn3j3nzirspdmlxv6lhd12r4s1lizxq3fzfywqf")))

(define-public crate-dinglebit_combinatorics-1.1.0 (c (n "dinglebit_combinatorics") (v "1.1.0") (h "13xmbqj17n14lqabpdxzbhqc85845zqck2b5z0k9yk08y51m0azi")))

