(define-module (crates-io di ng dinglebit-terminal) #:use-module (crates-io))

(define-public crate-dinglebit-terminal-0.1.2 (c (n "dinglebit-terminal") (v "0.1.2") (h "0d0fdkg20lc0hzr0bkjq1hijj4llc1ym9pqnd5iw9cxsspv5605x")))

(define-public crate-dinglebit-terminal-0.1.3 (c (n "dinglebit-terminal") (v "0.1.3") (h "0vkb1f0hxq93a45a2363cl140w3yc59h9vgq5b7a4lpbkwfi5ga4")))

(define-public crate-dinglebit-terminal-1.0.0 (c (n "dinglebit-terminal") (v "1.0.0") (h "0w7qjywwj3mkcxjipy3f6qvbhbgjppyqr7jrjirc5n0m0j8p215b")))

(define-public crate-dinglebit-terminal-1.1.0 (c (n "dinglebit-terminal") (v "1.1.0") (h "0z1y2786acr13057d9c9ljii1iwc86agw3v4ha2hqx6s0f1dymqi")))

