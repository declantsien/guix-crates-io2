(define-module (crates-io di ng dingus) #:use-module (crates-io))

(define-public crate-dingus-0.3.0 (c (n "dingus") (v "0.3.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "02jwj5kdp2d9wklhip0jj7mgspnwam05xxr4cpw74klcvlpqdkl8")))

(define-public crate-dingus-0.3.1 (c (n "dingus") (v "0.3.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "04vm61rbwdzn1qn89bzld01p1g4cd8gz2a3jc3k16z0wkpkagzjw")))

(define-public crate-dingus-0.3.2 (c (n "dingus") (v "0.3.2") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "02q60yk9vczr12cp9bnq71n5bi605n764kkr5r4vaixf1m7hjk76")))

(define-public crate-dingus-0.3.3 (c (n "dingus") (v "0.3.3") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "09acvpk1vbyrz2rih0ij7kvzvajy56d2lp2vn3jhbf0pg4cfypq5")))

(define-public crate-dingus-0.3.4 (c (n "dingus") (v "0.3.4") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1ccm202yy18bgvywdp8f6k9nri3xsrjy5kh7972k4fmxvdr1705g")))

(define-public crate-dingus-0.3.5 (c (n "dingus") (v "0.3.5") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "04a6yi27s5p789jg0kkxzwcmsc8kpfck53ady6pl4j0499wwqqba")))

(define-public crate-dingus-0.3.6 (c (n "dingus") (v "0.3.6") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "01cylnqnv50hcaxfrinh0kxbcil202gdy7xxz1lbyf831bm8x7z1")))

(define-public crate-dingus-0.3.7 (c (n "dingus") (v "0.3.7") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1s6lcm47c41ckdk3rlila62l8nyy8j44i96mwi37a1448awqlsfs")))

(define-public crate-dingus-0.3.8 (c (n "dingus") (v "0.3.8") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1ip680dvsp4jwl3zyvlrgqagvw9ssgm44dap29pwcqcsanskq4xs")))

(define-public crate-dingus-0.4.0 (c (n "dingus") (v "0.4.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0wm2xavmqv5188wr1v1ix3x6xjrd9iwki1zxvqkqd2l0gi9yxzs0")))

(define-public crate-dingus-0.4.1 (c (n "dingus") (v "0.4.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1rh2qpd15ym9ykn45xrgrna9kcf0zw2n4pj5cx1vm25a93kilvxf")))

(define-public crate-dingus-0.4.2 (c (n "dingus") (v "0.4.2") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1yfclgyzksmfjsiiryb8v7qg0rxg4pli4rfpvqavn6cjc2ajx6yb")))

(define-public crate-dingus-0.5.0 (c (n "dingus") (v "0.5.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "10jmmzpapnsz25p80ms1wlc97j98bybwq2i351qmzg29sn2w59ww") (y #t)))

(define-public crate-dingus-0.5.1 (c (n "dingus") (v "0.5.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0ypwyp1mwcdm19crkmb4gycl0xwcmdhww5da3vz60i8123m5xqyy")))

(define-public crate-dingus-0.5.2 (c (n "dingus") (v "0.5.2") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0s1m98q54z2wplpgf5sgk356k0kny1czav76lf3gmzmzn0biys3j")))

(define-public crate-dingus-0.5.3 (c (n "dingus") (v "0.5.3") (d (list (d (n "clap") (r "^2.32.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.6") (d #t) (k 0)))) (h "0j1z2dc1rci7ashr91j2xdfv2jr83v0m090kkz52fkg1w92l1l0a")))

(define-public crate-dingus-0.5.4 (c (n "dingus") (v "0.5.4") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.6") (d #t) (k 0)))) (h "1yqmnqb4dzvjxhfn2h9adlkqhpz9gd6a9dlkmmciy606i7ygviwg")))

(define-public crate-dingus-0.5.5 (c (n "dingus") (v "0.5.5") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.6") (d #t) (k 0)))) (h "11kf1g3fjfabrx74sc28jf9wbzghxfvwgn43rp96gghx7dgik1rr")))

(define-public crate-dingus-0.5.6 (c (n "dingus") (v "0.5.6") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.6") (d #t) (k 0)))) (h "0mnyy9msr1z8w2qp61qpmm7k8mhniq8v11v9yc01p4g0z4sqlxh9")))

(define-public crate-dingus-0.5.7 (c (n "dingus") (v "0.5.7") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "1dl2l6n0i647wd88zc6j8x6d6jqnx0g9f9ca0mq6776ifamm9gx1")))

