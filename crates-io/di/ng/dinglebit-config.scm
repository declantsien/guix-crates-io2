(define-module (crates-io di ng dinglebit-config) #:use-module (crates-io))

(define-public crate-dinglebit-config-0.1.0 (c (n "dinglebit-config") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0cc3qsgd4ax6xqlgiwk9q4j9mp1l4map6crj5g7901122kgwsfi1")))

(define-public crate-dinglebit-config-0.2.0 (c (n "dinglebit-config") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zbd7xjn4h5b1hic1xcmzmxjd5xchz36ll253yc4dciwp32p8rm4")))

(define-public crate-dinglebit-config-0.2.1 (c (n "dinglebit-config") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "007yx4qa6qwhd6c826viv76075gcv9wzh5yc63wavwhf913ns76x")))

(define-public crate-dinglebit-config-0.2.3 (c (n "dinglebit-config") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0wfks6qy8n5spnll7gnq0hf46mmrdv5i47fzc169aabxg9dqg3s3")))

(define-public crate-dinglebit-config-1.0.0 (c (n "dinglebit-config") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1k73cmwnzrdblzxznd97mdnmxjrfjrmjkgmw6y05zqq6q4q9xy4k")))

(define-public crate-dinglebit-config-1.1.0 (c (n "dinglebit-config") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fgqr186bb795qcb5k8kxcn9jw4mv7ybv4w4ijl0nnhx199bnm92")))

