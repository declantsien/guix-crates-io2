(define-module (crates-io di ng dingusim) #:use-module (crates-io))

(define-public crate-dingusim-0.1.0 (c (n "dingusim") (v "0.1.0") (d (list (d (n "common_macros") (r "^0.1") (d #t) (k 0)) (d (n "pixels") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1n8366786bqpnjjjw4xs1jiz60jsiqg0mqw05lymwialbvra9hka")))

