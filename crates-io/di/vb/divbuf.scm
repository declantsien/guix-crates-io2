(define-module (crates-io di vb divbuf) #:use-module (crates-io))

(define-public crate-divbuf-0.1.0 (c (n "divbuf") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "1gr00dvgm4947h4ja1spxsc4ky0d8p7dilpksyp6bhkil1c2lxny")))

(define-public crate-divbuf-0.2.0 (c (n "divbuf") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "0fbvz1s3hv95ngnix7x20dn1rlzijg8hjpadydc5kfq4zxk2bi7q")))

(define-public crate-divbuf-0.3.0 (c (n "divbuf") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "version_check") (r "^0.1.4") (d #t) (k 2)))) (h "1gf9f6kp85r45s6nbby6cisslpxc5j9fddqsbc99jw9lpijzsbs8")))

(define-public crate-divbuf-0.3.1 (c (n "divbuf") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "version_check") (r "^0.1.4") (d #t) (k 2)))) (h "092nj17y839y34ikinlk65p7hv61iqhw9038i4rnm2p7vfg760pv")))

