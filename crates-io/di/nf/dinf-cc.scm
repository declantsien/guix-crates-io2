(define-module (crates-io di nf dinf-cc) #:use-module (crates-io))

(define-public crate-dinf-cc-0.1.0 (c (n "dinf-cc") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1xf6068p9612n9q8v2q6y6n6qvx5g3ixdra12i7mp0f1f1kllrp1") (y #t)))

(define-public crate-dinf-cc-0.1.1 (c (n "dinf-cc") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1iprh0n9y16s0ibp5qvypwciaix5jrcnwmx2lr6v02w1xq8pi75m")))

