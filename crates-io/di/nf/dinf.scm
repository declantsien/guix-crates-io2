(define-module (crates-io di nf dinf) #:use-module (crates-io))

(define-public crate-dinf-0.1.0 (c (n "dinf") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03d9zmxhyzz16nrj2awr3p309sy5fh5g598dnvfrmw5hhjc5ql47")))

(define-public crate-dinf-0.1.1 (c (n "dinf") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ch27k4xd25zww69pixllqmjq2fx2j5p23rrj7mxy816z4v7bpfv")))

(define-public crate-dinf-0.1.2 (c (n "dinf") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "derive_more") (r "^1.0.0-beta.6") (f (quote ("from" "display"))) (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1b30cpxn7m4kng6mjkh9qgadc61nax9aa5r20khdp2mkzg6417y4")))

