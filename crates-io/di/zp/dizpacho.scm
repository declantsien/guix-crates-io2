(define-module (crates-io di zp dizpacho) #:use-module (crates-io))

(define-public crate-dizpacho-0.1.0 (c (n "dizpacho") (v "0.1.0") (d (list (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dr57c17h6mrfy8y3k239mf199cr642p1cr6mid3vxhf9kh78qp5")))

