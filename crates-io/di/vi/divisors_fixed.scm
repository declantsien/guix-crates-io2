(define-module (crates-io di vi divisors_fixed) #:use-module (crates-io))

(define-public crate-divisors_fixed-0.3.0 (c (n "divisors_fixed") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "05n0273bfrjv2ppc1c0ma5vrfimrww3bsx23lr33q0946xm9nm9w")))

(define-public crate-divisors_fixed-0.4.0 (c (n "divisors_fixed") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "02779bn8lzpaylb01yjl8r0db7jplp3vx8wjy28bsrnf01cv8v4z")))

