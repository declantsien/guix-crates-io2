(define-module (crates-io di vi divide) #:use-module (crates-io))

(define-public crate-divide-0.1.0 (c (n "divide") (v "0.1.0") (h "09m1fz7493p8pxmgznd1xiv05fpyrd8a9arqyvklprk980kqslx7")))

(define-public crate-divide-0.1.1 (c (n "divide") (v "0.1.1") (h "0ykr3r0ryaly1vbfmb7d8jnj0dbbn7fzrggxi9g13hrkf0j6707f")))

