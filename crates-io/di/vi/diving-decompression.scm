(define-module (crates-io di vi diving-decompression) #:use-module (crates-io))

(define-public crate-diving-decompression-0.1.0 (c (n "diving-decompression") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0shsa03604h3v1ph68m22128axav8595myqmk38yqg8wjk8ai8k0")))

(define-public crate-diving-decompression-0.1.1 (c (n "diving-decompression") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0793c9sy30zz3m0b3p4p160ws8ngyjvir9v0vg8z453nynz6s3n0")))

(define-public crate-diving-decompression-0.1.2 (c (n "diving-decompression") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qr8c138ylmkcmc8sam0fxhnn69c8p7wnn8m7ix1sb0hk5pqaq30")))

(define-public crate-diving-decompression-0.1.3 (c (n "diving-decompression") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18sab72m70g71al47cy7a8fmjywxs1jgzjgjy13g7ih66p5pg3jf")))

(define-public crate-diving-decompression-0.1.4 (c (n "diving-decompression") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h8mdi2agkwwa47kqqni365gx30dsjad2328rdf8m2zc53jns033")))

(define-public crate-diving-decompression-0.1.6 (c (n "diving-decompression") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09m95lm505h1mxz570p8fi79l9dlc8ks7xaasrhf6l98p9v45cyj")))

(define-public crate-diving-decompression-0.1.7 (c (n "diving-decompression") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12p758jdkygfx90m73spv0y79xaf4w4zyzr8s98jyr5l7gd36lgx")))

(define-public crate-diving-decompression-0.1.8 (c (n "diving-decompression") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dqkimvvmzjnxs5lrcghjadpy01fs5jpp3gh8m0p5l81xdw57b2g")))

(define-public crate-diving-decompression-0.1.9 (c (n "diving-decompression") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kglyrj69crq9ymr90hyjp5fk247fcacs8iaswgrlipvzwcdljh7")))

