(define-module (crates-io di vi diviner) #:use-module (crates-io))

(define-public crate-diviner-0.1.0 (c (n "diviner") (v "0.1.0") (h "0x8nlh6l2pg902svbiz3cm0jckgb3l84lz8zjnd1a5s36h91ci2v")))

(define-public crate-diviner-0.2.0 (c (n "diviner") (v "0.2.0") (d (list (d (n "async-task") (r "^1.3.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)))) (h "0d68gdibj6w29jznm6xh98kfhi9c3prjxd32v7nxkasxjgkxin1k")))

