(define-module (crates-io di vi divisors) #:use-module (crates-io))

(define-public crate-divisors-0.1.0 (c (n "divisors") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1y0rrxn5366n6c9ihlfnla9056hcsf0ljs61w7yhha68g2khcrlm")))

(define-public crate-divisors-0.1.1 (c (n "divisors") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1caa4himc0haf53cj8bshyqmhsjjp9rxm1avprxy8qc9q1r2qhza")))

(define-public crate-divisors-0.1.2 (c (n "divisors") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1pdx76ikyfbah1fwl22579clbrbql3y0wnjrxs84ylvw098j2w2y")))

(define-public crate-divisors-0.1.3 (c (n "divisors") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1mf2j695pxixs8xm3hnxzj9x4mmimh19h1gkh118xkilcccar776")))

(define-public crate-divisors-0.1.4 (c (n "divisors") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1hlv7c45wp2h50a7vfbvhqb6zzn0pc9683bkmj960b1d6qkfj5zf")))

(define-public crate-divisors-0.1.5 (c (n "divisors") (v "0.1.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1nrq1zcgqjl3nv2iji65n2hxk2a18d590gsvc4737l7haykjkhgs")))

(define-public crate-divisors-0.2.0 (c (n "divisors") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0ig7jwv3gs2xv133lh6bfp480jjb0ni3j6zm1b3v8mxwc6n64s2k")))

(define-public crate-divisors-0.2.1 (c (n "divisors") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "0vhkp7fyyy5rxjbw335di2gz69fc1nki6zg0y0i1sysnjhxldcdv")))

