(define-module (crates-io di vi divider) #:use-module (crates-io))

(define-public crate-divider-0.1.0 (c (n "divider") (v "0.1.0") (h "13by88q3nhr159d4yg9rhqrliqkp26g744520fyipbljflhqq564")))

(define-public crate-divider-0.1.1 (c (n "divider") (v "0.1.1") (h "0bl7rcgczi4kph5wg2vjfdvlqk4fz0qp7hb3f22vj3plindxb5s5")))

