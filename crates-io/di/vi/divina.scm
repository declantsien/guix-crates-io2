(define-module (crates-io di vi divina) #:use-module (crates-io))

(define-public crate-divina-0.1.0 (c (n "divina") (v "0.1.0") (d (list (d (n "divina_compile") (r "^0.1.0") (d #t) (k 0)) (d (n "divina_config") (r "^0.1.0") (d #t) (k 0)) (d (n "divina_git") (r "^0.1.0") (d #t) (k 0)) (d (n "divina_util") (r "^0.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.3") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "mimalloc") (r "^0.1.26") (t "cfg(windows)") (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^0.3.7") (f (quote ("full"))) (d #t) (k 0)))) (h "1d9b2kny0pw6zs040waqchldm3w0bq89l666lxydkc9fy809l3sx")))

