(define-module (crates-io di sa disambiseq) #:use-module (crates-io))

(define-public crate-disambiseq-0.1.0 (c (n "disambiseq") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0mr6y5kv9g66zwy1pgxkvvcwygdpjngyninj82cbcn5nwngc588j")))

(define-public crate-disambiseq-0.1.1 (c (n "disambiseq") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0gxyyb5g1iaqxmcy09j4sm14g3x0zwdd8lny5ndggh2pka6qp161")))

(define-public crate-disambiseq-0.1.2 (c (n "disambiseq") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0c01ms7qxislbi4pki6nxppmj9988hbrlxnv0wasagb5g7lr6dis")))

(define-public crate-disambiseq-0.1.3 (c (n "disambiseq") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "03sfv1vcnrahd736214hfw6c6dcw5gydyr1wp9af9sakrl9dk1rc")))

(define-public crate-disambiseq-0.1.4 (c (n "disambiseq") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "1fxfjxlc5wc3371bgwj19f8qrd04r7chxy5g241brpivc4lsl6bg")))

(define-public crate-disambiseq-0.1.5 (c (n "disambiseq") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "1cwflmy291jlb4i9wxm4laijyid22xbgwsb4fm6mhprs1844xrl9")))

(define-public crate-disambiseq-0.1.6 (c (n "disambiseq") (v "0.1.6") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0fnw0ij5hj37rd18ffnmwdw46rirvz4d4xlz96x6xrb6ah8z03ck")))

(define-public crate-disambiseq-0.1.7 (c (n "disambiseq") (v "0.1.7") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0lw7nvpkmxf8dad51nvq5c4vgi1fdjp2zhk3i00ylcg3d7f2ij68")))

(define-public crate-disambiseq-0.1.8 (c (n "disambiseq") (v "0.1.8") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "1ghhng7dmvs3a0wmj1zacm27a6k7724f66wpb15dwr63yi7rsqh7")))

(define-public crate-disambiseq-0.1.9 (c (n "disambiseq") (v "0.1.9") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "0qvq36l3r71mzs0rmqxv2ibhf472s58g8mar4zlxxlhnpp0wx8gw")))

(define-public crate-disambiseq-0.1.10 (c (n "disambiseq") (v "0.1.10") (d (list (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)))) (h "11kzpq9k2y4y97vn0idjd12w25yzxxxhahsvf32a08z4120mz6xd")))

