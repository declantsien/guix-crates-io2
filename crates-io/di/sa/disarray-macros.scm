(define-module (crates-io di sa disarray-macros) #:use-module (crates-io))

(define-public crate-disarray-macros-0.1.4 (c (n "disarray-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0qbhs8w5vw73jp5jq10v1him7qzq95vi086bcsds8qq25fjdd244")))

(define-public crate-disarray-macros-0.1.5 (c (n "disarray-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "167vkladv6hjv2hw6hms5ji0x01y0s4fn4vqcba5jng7wjd42cvx")))

(define-public crate-disarray-macros-0.1.6 (c (n "disarray-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1hhr0syc6ski7jlvgazb35sfcl29fhifaby7znyglj3y3wl150ln")))

(define-public crate-disarray-macros-0.1.7 (c (n "disarray-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "01igac4pz9vwmqz0nq9m9665fk0nzi2xmb5jxw0vssrhbr5msw5x")))

(define-public crate-disarray-macros-0.1.8 (c (n "disarray-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0a101zraqsrm441xb87zn0psyy05l5ljnmj4vy4178cmnhinf0hs")))

(define-public crate-disarray-macros-0.1.9 (c (n "disarray-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0vsn44vaa0rk0mxmfiv8p77vv0zd36qfvczhs3c18lgb21p5llz2")))

(define-public crate-disarray-macros-0.1.10 (c (n "disarray-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "08j09q8dck7yy27xgpykxx2cl8v1cs70g2d98p09k8aikhiprq45")))

(define-public crate-disarray-macros-0.1.12 (c (n "disarray-macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0v0j7hfmszcihq4ckjcbk5v38rqay4pwq8svj0pch5ar549s6ww6")))

(define-public crate-disarray-macros-0.1.13 (c (n "disarray-macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "08fzjav2cly6iilffw1y49w2m0na1lrwzrv93d7aw87fdi8kq0hs")))

