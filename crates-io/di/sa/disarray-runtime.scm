(define-module (crates-io di sa disarray-runtime) #:use-module (crates-io))

(define-public crate-disarray-runtime-0.1.20 (c (n "disarray-runtime") (v "0.1.20") (d (list (d (n "algae") (r "^0.1.13") (f (quote ("merkle"))) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 0)) (d (n "scsys") (r "^0.1.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasmer") (r "^2.3.0") (d #t) (k 0)))) (h "1cjc1v8di031ypp333zvady4c83q4x4gs589s7268ap508y1d45w")))

