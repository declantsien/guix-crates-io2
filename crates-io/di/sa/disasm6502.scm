(define-module (crates-io di sa disasm6502) #:use-module (crates-io))

(define-public crate-disasm6502-0.1.0 (c (n "disasm6502") (v "0.1.0") (h "0dkzb4pghlg30zwvxz1qdn76d90s4pdz8a0gb23l373xx7yq9443")))

(define-public crate-disasm6502-0.2.0 (c (n "disasm6502") (v "0.2.0") (h "0d1z2xzr5lg6479w7y5a2ribqa6rqrkmh0b5hi96b4bgh8ws574c")))

(define-public crate-disasm6502-0.2.1 (c (n "disasm6502") (v "0.2.1") (h "1rza7vlsfd552fp9mnr0big42cpq5wxghpz300bl3r24vwkpvjgj")))

(define-public crate-disasm6502-0.2.2 (c (n "disasm6502") (v "0.2.2") (h "17c8par5rw8p1jzm94ylkdz50il3i169q1r98wq7apv15x585an8")))

