(define-module (crates-io di sa disarm64_defn) #:use-module (crates-io))

(define-public crate-disarm64_defn-0.1.0 (c (n "disarm64_defn") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "14xajamhi1p62skhzd72cffipckyk7ykllk5a6cva59zqldbmxw2")))

(define-public crate-disarm64_defn-0.1.1 (c (n "disarm64_defn") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1w19mmkgdwa9xhw6rajzdy9yws3r1bjm0h6n7aaswiijm8ydjn5s")))

(define-public crate-disarm64_defn-0.1.2 (c (n "disarm64_defn") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "187bsy1bb56h2y7idrfkdnswvvqzir74plmnwhv0czml42217wcj")))

(define-public crate-disarm64_defn-0.1.3 (c (n "disarm64_defn") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1bdm2acympw1y056qapm5wknic9kqhphxv4w8jlxi1wbz7wk17zr")))

(define-public crate-disarm64_defn-0.1.4 (c (n "disarm64_defn") (v "0.1.4") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0a1i1lpd7agkhf2xlkysnbfy2nv5yhw6r2z3m4h67dpfjwx4z51w") (f (quote (("std") ("default" "std"))))))

(define-public crate-disarm64_defn-0.1.5 (c (n "disarm64_defn") (v "0.1.5") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "14p3ci8lf1grjz12d6blm88mxy2v2cgxln7qzq36s13lnszqszym") (f (quote (("std") ("default" "std"))))))

(define-public crate-disarm64_defn-0.1.7 (c (n "disarm64_defn") (v "0.1.7") (d (list (d (n "bitflags") (r "^2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1frbbs5v5a7v3d3bakp8gfp99d0wdr437wfvpf1bp74x87mr2q2j") (f (quote (("std") ("default" "std"))))))

