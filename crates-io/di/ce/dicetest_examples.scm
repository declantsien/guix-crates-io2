(define-module (crates-io di ce dicetest_examples) #:use-module (crates-io))

(define-public crate-dicetest_examples-0.1.0 (c (n "dicetest_examples") (v "0.1.0") (d (list (d (n "dicetest") (r "^0.1.0") (d #t) (k 0)))) (h "1ni5g2178pka8r5kxh5y70r6280grk4yry1h2knkjb5rlc7r885i") (y #t)))

(define-public crate-dicetest_examples-0.2.0 (c (n "dicetest_examples") (v "0.2.0") (h "0l3qaqacjna9wrpql3fzrb462jx840wsilwsp9s5rgzcqah3aaj9")))

