(define-module (crates-io di ce diceware_wordlists) #:use-module (crates-io))

(define-public crate-diceware_wordlists-0.5.4 (c (n "diceware_wordlists") (v "0.5.4") (h "1ym21vd9iwbsvqkdqiv5r212in9yz69jbygnfw99i54qgk4qp46h")))

(define-public crate-diceware_wordlists-0.5.5 (c (n "diceware_wordlists") (v "0.5.5") (h "1i12ihv0xihjgprmbh5dh8pzxc157pvv6wi65dklh0z50mxrzrjz")))

(define-public crate-diceware_wordlists-0.5.6 (c (n "diceware_wordlists") (v "0.5.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0ssl1shsr4v037zsmz7mc4k5gzcrz94hbycfdk4lv4xxlr0fiw5v") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1.0.0 (c (n "diceware_wordlists") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1kh52ddpn47zsxnbxj05lpggd7i6x19h2qwvacp8lr6rc3bpi0gy") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1.1.0 (c (n "diceware_wordlists") (v "1.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1ndkx2ymz4xsk2shkpi3ajyhs41j7b4vj6wr75sgpjixdi5108vr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1.1.1 (c (n "diceware_wordlists") (v "1.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0vxmmqfcv7kzah8b299m8gvvy1ndb18a9qc52hng0dlpqpx2p3qb") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1.2.1 (c (n "diceware_wordlists") (v "1.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1sx1lbw5z0bvm9d1z0mkpz5d0k7b6f2wwga2il78jfnk7j2f6sw2") (s 2) (e (quote (("serde" "dep:serde"))))))

