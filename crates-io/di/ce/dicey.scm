(define-module (crates-io di ce dicey) #:use-module (crates-io))

(define-public crate-dicey-1.0.0 (c (n "dicey") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1ncbkn76nazbvwar5yyqgvqf965l7dmyrna8alsghb2ga0xkvd1a")))

(define-public crate-dicey-1.0.1 (c (n "dicey") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1qf7sb8gc2d4gd83gmw65g5r5j00j0hp5p8533g5gbhmj5wd4mgx")))

(define-public crate-dicey-1.1.0 (c (n "dicey") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0f3yz1qqql7gk5sl1w6q5dlfyqzj9cc84y8rvyn03i7hqzlf3xbb")))

