(define-module (crates-io di ce dicetest) #:use-module (crates-io))

(define-public crate-dicetest-0.1.0 (c (n "dicetest") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1wf9pv4imaiad1ii4qn56dmqpwmqw56jsw7p16pwchxanq9kcb41") (f (quote (("stats") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.2.0 (c (n "dicetest") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0) (p "rand")) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0alx9rviijm7b18qmjagxhgsy0h1jgmcxvpmw85avv551chq39i2") (f (quote (("stats") ("rand_full" "rand_core" "rand") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.2.1 (c (n "dicetest") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02hqhqdqmphcyxr66cbfa7h8jinz5snhj28xcaiw00v8256hr2w3") (f (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.3.0 (c (n "dicetest") (v "0.3.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0n7w08w4p21a5zqfg10za9mv6pmmk4sl9k7qvgrvsx8bs5wba37x") (f (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.3.1 (c (n "dicetest") (v "0.3.1") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1zdg1l4h5brdpbbbq3mnl4gxabrjzh26iajvmcyllk7jqvn1cdy9") (f (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

