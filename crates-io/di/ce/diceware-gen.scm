(define-module (crates-io di ce diceware-gen) #:use-module (crates-io))

(define-public crate-diceware-gen-0.1.0 (c (n "diceware-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1srgl4s26qmxijaad5xpc66klrn0ad21v5ms3aiahnqkjqv6xgdm")))

(define-public crate-diceware-gen-0.2.0 (c (n "diceware-gen") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1i46ff1wp2fsp5sjklg0xidbs1jg4a0bwh2wjibsj089hxzykh0v")))

(define-public crate-diceware-gen-0.2.1 (c (n "diceware-gen") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "055qzpxv1hz030gyik48pmrf1x4svq0jdr8cl6m8ypwz12g7w0sz")))

(define-public crate-diceware-gen-0.2.2 (c (n "diceware-gen") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00xbsmq4wjgzl5mf04p6bqy25pxh46kb7527jzxn0m37z970727f")))

