(define-module (crates-io di ce dicenotation) #:use-module (crates-io))

(define-public crate-dicenotation-0.1.0 (c (n "dicenotation") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-iter") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0dj77xcjyalbbj2pq0qkng11g7721q3h51rip9m5g1piz291krfq")))

(define-public crate-dicenotation-0.2.0 (c (n "dicenotation") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-iter") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0z34x1jjripjil58pjm6dwj1ijylxi30dixlp9pjxfcazwm4fijj")))

