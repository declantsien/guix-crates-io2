(define-module (crates-io di ce dice-roll) #:use-module (crates-io))

(define-public crate-dice-roll-0.1.0 (c (n "dice-roll") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1niz1h9a770917kz2bqd931yhy7nc87mrnapa86frm6dwjahwvyl") (f (quote (("roll" "rand") ("parser" "nom") ("logging" "log" "rand/log"))))))

(define-public crate-dice-roll-0.1.1 (c (n "dice-roll") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ml7971nlp4jl0h3rv4wb7klwj87zsnnbfiv65vbx4zfgzs8hvbh") (f (quote (("roll" "rand") ("parser" "nom") ("logging" "log" "rand/log") ("default" "parser" "roll"))))))

