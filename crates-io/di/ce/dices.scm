(define-module (crates-io di ce dices) #:use-module (crates-io))

(define-public crate-dices-0.1.0 (c (n "dices") (v "0.1.0") (d (list (d (n "fraction") (r "^0.11.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03pjfwgwsa2fk975d5m8qgciz5mc9l1z8211pqwp7lzwp2mwd3l1")))

(define-public crate-dices-0.2.0 (c (n "dices") (v "0.2.0") (d (list (d (n "fraction") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "19bcbwp76cfv135040gg6llwk5vxv04sjjycr5bx5p6bp3rmp8kn")))

(define-public crate-dices-0.3.0 (c (n "dices") (v "0.3.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fraction") (r "^0.11.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("Window" "Performance"))) (o #t) (d #t) (k 0)))) (h "1i1xadmb0s4mhsx5wmxmzgrcga4i8scdzgmawzmyly6gm2xjs7bp") (f (quote (("default" "nowasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:serde-wasm-bindgen" "dep:serde" "dep:web-sys") ("nowasm" "dep:rand") ("console_error_panic_hook" "dep:console_error_panic_hook"))))))

