(define-module (crates-io di ce diceprop) #:use-module (crates-io))

(define-public crate-diceprop-0.1.0 (c (n "diceprop") (v "0.1.0") (d (list (d (n "dicetest") (r "^0.2.1") (d #t) (k 0)))) (h "12hcxbcr6x8fq0wdj01im1kl7ang69z0hs9n56nmrb62517pmp4q")))

(define-public crate-diceprop-0.2.0 (c (n "diceprop") (v "0.2.0") (d (list (d (n "array-init") (r "^2.0") (d #t) (k 0)) (d (n "dicetest") (r "^0.3") (d #t) (k 0)))) (h "0ajncv8j5d1y0gh945vp9rgchlckjavhfbv51ybzi4f3qf0d0hd4")))

