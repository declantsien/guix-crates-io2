(define-module (crates-io di ce dicers) #:use-module (crates-io))

(define-public crate-dicers-0.2.0 (c (n "dicers") (v "0.2.0") (d (list (d (n "lambda_runtime") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "0kzclb45djajf5ah9wjdvg4fxb6c3408s1nk7cg23b5wpkqixjs3") (f (quote (("lambda" "lambda_runtime" "serde" "serde_derive") ("default"))))))

(define-public crate-dicers-0.2.1 (c (n "dicers") (v "0.2.1") (d (list (d (n "lambda_runtime") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "081fqg6ff6y4lny7vc1c0mig5qw7fh0k7n17kw7bilgfp7457w4v") (f (quote (("lambda" "lambda_runtime" "serde" "serde_derive") ("default"))))))

