(define-module (crates-io di ce diceware) #:use-module (crates-io))

(define-public crate-diceware-0.1.0 (c (n "diceware") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gsk0x9wn2l58f2blimr693k9v556iid6w0kqlbzb4ah7f82mbj0")))

(define-public crate-diceware-0.2.0 (c (n "diceware") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0jj7xc3f0q1mk4pw41ryscyk6qgxpyj3aar0gaw39nbf4bwvnz1w")))

(define-public crate-diceware-0.3.0 (c (n "diceware") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0n4llnzl8c3ryhjy6ghiyyip696mfbrp9g6g9489dpf2im3lpbsn")))

(define-public crate-diceware-0.3.1 (c (n "diceware") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0hdw1m82n5rkz3hv9g9kzfb7g445sx0bdfmqa58x9wp8c5698d3a")))

(define-public crate-diceware-0.3.2 (c (n "diceware") (v "0.3.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04sfmhvlnmw893782lmp8fdvllkkfqj603mi3s1bxnbzdmgx74vz")))

(define-public crate-diceware-0.3.3 (c (n "diceware") (v "0.3.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jhj0x21kk0gs02vjwkd0dryx9qgxvva6843r2w97v3jzsv2iayf")))

(define-public crate-diceware-0.3.4 (c (n "diceware") (v "0.3.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0jdka3vxd4wgb8cv8mv6zhzbwwdjsmvgzpylp0w1bi4sh7k8hhb9")))

(define-public crate-diceware-0.3.5 (c (n "diceware") (v "0.3.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0n1y7jhqr9hbssyzzpb6sjxpiq4qw0ixm40575ngl90ryp4yabcn")))

(define-public crate-diceware-0.3.6 (c (n "diceware") (v "0.3.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yl4jd7qyrlaj53953dw67gqixj65654nl4wmzfi00i44n7zn23y")))

(define-public crate-diceware-0.3.7 (c (n "diceware") (v "0.3.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mqv00rx2iw1yl93srpxjvl01xzwnl7adk354dkvpx0zab16d6x5")))

(define-public crate-diceware-0.3.8 (c (n "diceware") (v "0.3.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "025i9ljdsh04isna6k8diz3jsfr8hvic3zx06r4hvbvc20vl8hvf")))

(define-public crate-diceware-0.3.9 (c (n "diceware") (v "0.3.9") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09br2kl4kvi28n3wij0jr5z4q0san0859557swdphqlf7nawbfv7")))

(define-public crate-diceware-0.4.0 (c (n "diceware") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04pi4yj0pg3l9qwm0gyij6jsywgr25fhjj4vi0gazx0jkzs8wb57")))

(define-public crate-diceware-0.5.0 (c (n "diceware") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0m08wy6zbqln8nvbd62hjkvgxz78bk64bsaanfy4nsl3c6iv7f6w")))

(define-public crate-diceware-0.5.1 (c (n "diceware") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0gqf0h5cnsfnm0hjhgn2pffvdwifpzqal6hk7yyfi4w9h7lli03s")))

(define-public crate-diceware-0.5.2 (c (n "diceware") (v "0.5.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1m77ka2rlf9m84bh21hqnsbikixl7nwl8y7j6faj0jkbf234adzw")))

(define-public crate-diceware-0.5.3 (c (n "diceware") (v "0.5.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12z1678x81n33ifbpx4kwl04s8wkwjnydpp54h3l01nyarrv90qw")))

(define-public crate-diceware-0.5.4 (c (n "diceware") (v "0.5.4") (d (list (d (n "diceware_wordlists") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "081jcdswc6xn242j1jqbnnq26rz4z4rxq46zmfg0l4z609z51asc")))

(define-public crate-diceware-0.5.5 (c (n "diceware") (v "0.5.5") (d (list (d (n "diceware_wordlists") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14yqr1vlv92y2nf9i6mfa2qddwf8cwxzjd6xzlnkrivapdpz37pz")))

(define-public crate-diceware-0.5.6 (c (n "diceware") (v "0.5.6") (d (list (d (n "diceware_wordlists") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0v3w7v2hfg330rgp6xs1ks6qyx2g7kzna89f49jb9j1r5snxx19a")))

(define-public crate-diceware-0.5.7 (c (n "diceware") (v "0.5.7") (d (list (d (n "diceware_wordlists") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1db424w6f64q07r6vn773bbhjn38bhs6g52myb1q25ym7ascwcak")))

(define-public crate-diceware-0.5.8 (c (n "diceware") (v "0.5.8") (d (list (d (n "diceware_wordlists") (r "^1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)))) (h "082l4a444qq9scmnc25kqp11v02gv3yms1q8ysrhsirb04fvjy6y")))

