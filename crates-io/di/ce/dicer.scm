(define-module (crates-io di ce dicer) #:use-module (crates-io))

(define-public crate-dicer-0.1.0 (c (n "dicer") (v "0.1.0") (h "1lypmab2njnaxgcgynkalf6ivbwc41mqs28dsfh55lbkfliqz0ss")))

(define-public crate-dicer-0.2.0 (c (n "dicer") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1s6hj5zm3a418064f91wb854fp6xnn1rp532x4ws0196nragvi8m")))

(define-public crate-dicer-0.2.1 (c (n "dicer") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1hji96sv6mqd056jlhf74cjrgn0j34p537c3w6kqlasxkskldyzr")))

(define-public crate-dicer-0.2.2 (c (n "dicer") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1afla4hg8fj27d0anyi3vqchkrlmg87k939l3aq1w3zg8an1wka8")))

(define-public crate-dicer-1.0.0 (c (n "dicer") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1qv3rc6cvajlxvmhmd6dr2yj8i7z50sfli7gcmjvjl4nwsk03kci")))

(define-public crate-dicer-1.0.1 (c (n "dicer") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0kksw7xwxn21pwvc58gy065ymhw084yrdf7wzg1v61i8xzibwmxz")))

(define-public crate-dicer-1.0.2 (c (n "dicer") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1dz48lnv99ddg094grnsd71w5cq1jpgjmlpwa1rfxkhc08xz0j8l")))

