(define-module (crates-io di ce dice-roller) #:use-module (crates-io))

(define-public crate-dice-roller-0.1.0 (c (n "dice-roller") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "101qa73cgyvqvy2hi0rb746w9ghx9ixq6f9ng40sp4hjmmgmp5dy")))

(define-public crate-dice-roller-0.1.1 (c (n "dice-roller") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "0d6yprf482g3bh6fmymi9kwska7lbyg7pp060fsw05cakznrpaf8")))

(define-public crate-dice-roller-0.1.3 (c (n "dice-roller") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "00bbg7ph6akznidg58jnmq8ciywm1c9xk3xky07rj7k0p3r7vips")))

