(define-module (crates-io di ce dice-command-parser) #:use-module (crates-io))

(define-public crate-dice-command-parser-0.3.1 (c (n "dice-command-parser") (v "0.3.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qas3wvs5i5mppak8xk6zv94jjzcj4msv8m281fcayi4x5ak3pdq")))

(define-public crate-dice-command-parser-0.4.0 (c (n "dice-command-parser") (v "0.4.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sj65dfyqdmvirs7d6q6nqc492dhjbfwb88957a3hhpqlh231i9y")))

(define-public crate-dice-command-parser-0.5.0 (c (n "dice-command-parser") (v "0.5.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qf1vzf9f4vk2hx2gcqay7adgwx1aqjmzfk3s9kwrjlzl2bqlfbj")))

(define-public crate-dice-command-parser-0.5.1 (c (n "dice-command-parser") (v "0.5.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jdviswa0alwchk5c0by43k8g7bz6rs0vd1l1fcj3v724nl2hafy")))

(define-public crate-dice-command-parser-0.6.0 (c (n "dice-command-parser") (v "0.6.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08gvj57g757l3x4pp7c5mrlnv5933prm0qmlcrbyrwyi9vy2m6h6")))

(define-public crate-dice-command-parser-0.7.0 (c (n "dice-command-parser") (v "0.7.0") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04yspg5i51sc02r9q41cs5fd7ph8smimp411xykkj1i736r2h95p")))

(define-public crate-dice-command-parser-0.7.1 (c (n "dice-command-parser") (v "0.7.1") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vb9ywj57igm2cr41sd9wm6wjgnbsqrdr5jc9bl14x7z6b338lgy")))

(define-public crate-dice-command-parser-0.7.2 (c (n "dice-command-parser") (v "0.7.2") (d (list (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02dwcwhj1yhbzrrilk6jhcsgh6qz3nm90ali5c20biryd7q5csc4")))

(define-public crate-dice-command-parser-0.7.3 (c (n "dice-command-parser") (v "0.7.3") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0a2rgxvwx9q2xr66jcf9nnkhw9hps38qd9wcgw0w415wnj2dcw8h")))

(define-public crate-dice-command-parser-0.8.0 (c (n "dice-command-parser") (v "0.8.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "11cyi3l5km78g3f0dv7nd58rwixd2wix0ngaj9iixhys9cwnnl1z")))

