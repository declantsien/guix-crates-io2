(define-module (crates-io di ce diceware-rs) #:use-module (crates-io))

(define-public crate-diceware-rs-0.1.0 (c (n "diceware-rs") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0wh2xxfa73vhxdsi45mgm18ivbhwqg8vqsmm0rwkd7nm3fbwrnfj")))

(define-public crate-diceware-rs-0.1.1 (c (n "diceware-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "1354xwf6n35qbfx8slqdf8ipply61kb4ni9nqp7bhkr3b2v1rswl")))

