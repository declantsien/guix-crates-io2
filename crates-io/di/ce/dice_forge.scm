(define-module (crates-io di ce dice_forge) #:use-module (crates-io))

(define-public crate-dice_forge-0.1.0 (c (n "dice_forge") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "006vvhm9w89lpbxarhiv31aa9gbaknmfpsd5jj72gpj1312yiq4c")))

(define-public crate-dice_forge-0.1.1 (c (n "dice_forge") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0892263yic37j9fs28ibdsx2xw550jg0zmhybsba55ad4785w5xx")))

(define-public crate-dice_forge-0.2.2 (c (n "dice_forge") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1568lz7g908dfichjmpv1dl1ps29xzphv0basm1czvmnqqqnpf2m")))

(define-public crate-dice_forge-0.3.0 (c (n "dice_forge") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b1v79zynwsl416wrjf1savsaj3y3nzgj1rjhbjnzms1ach3knz5")))

