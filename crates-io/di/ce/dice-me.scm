(define-module (crates-io di ce dice-me) #:use-module (crates-io))

(define-public crate-dice-me-0.1.0 (c (n "dice-me") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "07mfgapzsfd6k1dzpls06brdhjwfbj0w8wj70jh2wgjhmrmz1fjs")))

(define-public crate-dice-me-0.1.1 (c (n "dice-me") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "00y7axbv5gzh7b50nmb7wqpw2h25pwyl7vhcrf4q49c6nxx77ikd")))

