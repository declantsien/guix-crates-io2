(define-module (crates-io di sq disqrust) #:use-module (crates-io))

(define-public crate-disqrust-0.1.0 (c (n "disqrust") (v "0.1.0") (d (list (d (n "disque") (r "^0.2.1") (d #t) (k 0)) (d (n "redis") (r "^0.5") (d #t) (k 0)))) (h "0z70nbnr2i3f4x4sb8kqk89s850d78igj10dv7vrsm9b3pzx0gv7") (f (quote (("nightly"))))))

