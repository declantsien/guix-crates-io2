(define-module (crates-io di sq disque-cli) #:use-module (crates-io))

(define-public crate-disque-cli-0.1.0 (c (n "disque-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.2.6") (d #t) (k 0)))) (h "1d9rfdjqibmalphxcgfwhcn759fa1c4079nsh6zmh6d90hzk8jrq")))

(define-public crate-disque-cli-0.1.1 (c (n "disque-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.1.1") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.2") (d #t) (k 0)))) (h "0ldccmlwpkpyncza88ww7y2ws4p4mcqw7zl9fic05i28r0mcvq7k")))

(define-public crate-disque-cli-0.2.0 (c (n "disque-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.1.2") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.2") (d #t) (k 0)))) (h "0m733l0fnpmjdj8zx70mkxmdyyk8vqbai68qgi1prz5qfzs8i0fx")))

(define-public crate-disque-cli-0.2.1 (c (n "disque-cli") (v "0.2.1") (d (list (d (n "clap") (r "^2.5.2") (f (quote ("yaml"))) (k 0)) (d (n "resp") (r "^0.3.6") (d #t) (k 0)))) (h "1dyws3zxk9ix5lp6jaxvar25qcs818z54i74sif7464g52695axn")))

