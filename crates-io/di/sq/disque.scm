(define-module (crates-io di sq disque) #:use-module (crates-io))

(define-public crate-disque-0.1.0 (c (n "disque") (v "0.1.0") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)))) (h "1fcyzq6yww34n13isqr5vfm3yv7x11pxvrx17lji52b4pa179i2n")))

(define-public crate-disque-0.2.0 (c (n "disque") (v "0.2.0") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)))) (h "0kncwsph6pvcyl54c8s0fq10kl0bf2gq4gj7f7601i66w4hybvzh")))

(define-public crate-disque-0.2.1 (c (n "disque") (v "0.2.1") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)))) (h "1ybbz6yws722walg59h73dy3j5k2igg95z3kskiajhdpn6najgcp")))

(define-public crate-disque-0.2.2 (c (n "disque") (v "0.2.2") (d (list (d (n "redis") (r "^0.5.0") (d #t) (k 0)))) (h "0nhfkk1wzkrz6v3hiyw5hvh7mi1vbk8p1845n0ls0a1wgyhhh19v")))

(define-public crate-disque-0.2.3 (c (n "disque") (v "0.2.3") (d (list (d (n "redis") (r "^0.5.4") (d #t) (k 0)))) (h "13ynplczhj59wlln03j8zjd8vdq2q2w1ycl2vgmf791pphy1z7x5")))

