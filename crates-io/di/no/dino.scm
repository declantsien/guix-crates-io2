(define-module (crates-io di no dino) #:use-module (crates-io))

(define-public crate-dino-0.1.0 (c (n "dino") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kva23jy82fvhz52i56rjhn9lrxdvsq8hg43ig2d77bl35j5ghyp")))

(define-public crate-dino-0.1.1 (c (n "dino") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c5s85rq9m89mnn0ivvxinyc681k632ysafg1avm793avx1bccvg")))

(define-public crate-dino-0.1.2 (c (n "dino") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1miy84hwd5l59366rn16axmxhwhjahy7gw6bv6bgvzlg9b5zpidj")))

