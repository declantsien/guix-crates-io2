(define-module (crates-io di no dinotree_sample) #:use-module (crates-io))

(define-public crate-dinotree_sample-0.1.0 (c (n "dinotree_sample") (v "0.1.0") (d (list (d (n "axgeom") (r "^1.0.5") (d #t) (k 0)) (d (n "dists") (r "^0.1.5") (d #t) (k 0)) (d (n "new-ordered-float") (r "^1.0.1") (d #t) (k 0)))) (h "1232pw51mb7kmrs67xx52c5y7snmdxzc4ckvh1msm4i4l5sr4yac") (y #t)))

