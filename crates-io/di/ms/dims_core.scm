(define-module (crates-io di ms dims_core) #:use-module (crates-io))

(define-public crate-dims_core-0.2.1 (c (n "dims_core") (v "0.2.1") (h "1npsqr0f6yzi4xkhx4q0y3g5q8alpd4s8qvf9nmn3pqcj59qni1g") (f (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.3.0 (c (n "dims_core") (v "0.3.0") (h "1hjfzwr8qzlrnn27aigj6nhhf6f8cxlzbmhv48x7fw9xzln64s6q") (f (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.3.1 (c (n "dims_core") (v "0.3.1") (h "0l24gsjncsxnx8gv2qqf3nqshf192bpd0wmqwx0ag3akwcxlv7yk") (f (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.4.0 (c (n "dims_core") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "opimps") (r "^0.1.4") (d #t) (k 0)))) (h "0g05g2dzhn9sy4kjgp0fgg28v6sba88fx32n44102mxwrbbjw5rq") (f (quote (("str") ("std" "num-traits/std") ("f64") ("default" "str" "std"))))))

(define-public crate-dims_core-0.4.1 (c (n "dims_core") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0asx90326yvllx8g8psv1pkhin1zdmr70478hpgh03dh3g4m7hrn") (f (quote (("str") ("std" "num-traits/std") ("f64") ("default" "str" "std"))))))

