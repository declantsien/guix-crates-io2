(define-module (crates-io di ms dims_macro) #:use-module (crates-io))

(define-public crate-dims_macro-0.2.1 (c (n "dims_macro") (v "0.2.1") (d (list (d (n "dims_core") (r "^0.2.1") (k 2)) (d (n "dims_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0rw5bki60026hzs87yg3bh033knlav20iak2l0pybl2cqxx8lr3d") (f (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.3.0 (c (n "dims_macro") (v "0.3.0") (d (list (d (n "dims_core") (r "^0.3.0") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0rc32rrgfxgmfmv1ihhb0kbg4jn4g0ml9c9ikb0xaf9z6pri00n5") (f (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.3.1 (c (n "dims_macro") (v "0.3.1") (d (list (d (n "dims_core") (r "^0.3.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "01imvkdiw4lfi0h062f5x8ldmshmy4bfd29vdsh6m1bm1zbihywd") (f (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.4.0 (c (n "dims_macro") (v "0.4.0") (d (list (d (n "dims_core") (r "^0.4.0") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0mz6vzdr629i5kdmzgnk2wasjls2bij4ghnrba3216bqh448xm4y") (f (quote (("str" "dims_core/str") ("std" "dims_core/std") ("f64" "dims_core/f64") ("default" "str" "std"))))))

(define-public crate-dims_macro-0.4.1 (c (n "dims_macro") (v "0.4.1") (d (list (d (n "dims_core") (r "^0.4.1") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "11k4ng4fxfb7b2658sab8bssz2a9cbvacnilx10zafi1byiskh3l") (f (quote (("str" "dims_core/str") ("std" "dims_core/std") ("f64" "dims_core/f64") ("default" "str" "std"))))))

