(define-module (crates-io di ms dims) #:use-module (crates-io))

(define-public crate-dims-0.2.1 (c (n "dims") (v "0.2.1") (d (list (d (n "dims_core") (r "^0.2.1") (k 0)) (d (n "dims_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "dims_macro") (r "^0.2.1") (k 0)))) (h "1lpwcyzp21yj6959rncvq1z5skxcd1pd7vxpfx94g8salkzfa1qq") (f (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str"))))))

(define-public crate-dims-0.3.0 (c (n "dims") (v "0.3.0") (d (list (d (n "dims_core") (r "^0.3.0") (k 0)) (d (n "dims_macro") (r "^0.3.0") (k 0)))) (h "09b9ylsf8rzfv94yn71l1dv8qch6xdgg1jlxyj9cgjqbi67an16h") (f (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str") ("debug_us"))))))

(define-public crate-dims-0.3.1 (c (n "dims") (v "0.3.1") (d (list (d (n "dims_core") (r "^0.3.1") (k 0)) (d (n "dims_macro") (r "^0.3.1") (k 0)))) (h "1gfrwy8r2a1p1zcwjws1rfmmy1rdfy0iaabrgcvymzcms6zw53k3") (f (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str") ("debug_us"))))))

(define-public crate-dims-0.4.0 (c (n "dims") (v "0.4.0") (d (list (d (n "dims_core") (r "^0.4.0") (k 0)) (d (n "dims_macro") (r "^0.4.0") (k 0)))) (h "0cshjh1gd9g423maynhms84l1hlsf0lnqdd3n6bd4gynnx5h0d17") (f (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("std" "dims_core/std") ("si") ("f64") ("default" "us" "si" "str" "std") ("debug_us"))))))

(define-public crate-dims-0.4.1 (c (n "dims") (v "0.4.1") (d (list (d (n "dims_core") (r "^0.4.1") (k 0)) (d (n "dims_macro") (r "^0.4.1") (k 0)))) (h "0pw5aigkic7q135q1n2akhisizis5jysnjl7hbn08zshswyvck68") (f (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("std" "dims_core/std") ("si") ("f64") ("default" "us" "si" "str" "std") ("debug_us"))))))

