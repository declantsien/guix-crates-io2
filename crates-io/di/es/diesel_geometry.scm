(define-module (crates-io di es diesel_geometry) #:use-module (crates-io))

(define-public crate-diesel_geometry-1.2.0 (c (n "diesel_geometry") (v "1.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "diesel") (r "^1.2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r ">= 0.8, < 0.11") (d #t) (k 2)))) (h "04w23xdllhsf0867yf1mn3d0g8x9jbnszv3l99k73x767j0zdcqq") (f (quote (("postgres") ("default" "postgres"))))))

(define-public crate-diesel_geometry-1.2.1 (c (n "diesel_geometry") (v "1.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "diesel") (r "^1.2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r ">= 0.8, < 0.11") (d #t) (k 2)))) (h "1wmsi5rhf6rcpzp6iz7r0q5hr7c6wvfz18q1l72kq0askp6aif48") (f (quote (("postgres") ("default" "postgres"))))))

(define-public crate-diesel_geometry-1.2.2 (c (n "diesel_geometry") (v "1.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "diesel") (r "^1.2") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r ">= 0.8, < 0.11") (d #t) (k 2)))) (h "09kwylgq5drl01kz50w4ndjcl4sm3h45500rina3a9bj8hy0awaq") (f (quote (("postgres") ("default" "postgres"))))))

(define-public crate-diesel_geometry-1.3.0 (c (n "diesel_geometry") (v "1.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "diesel") (r ">= 1.2, < 1.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r ">= 0.8, < 0.12") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kk2dgjh1xwlwp20fyppg5n2p6k607kbqydia7pj5sijpx3sp42q") (f (quote (("postgres") ("default" "postgres"))))))

(define-public crate-diesel_geometry-1.4.0 (c (n "diesel_geometry") (v "1.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 2)) (d (n "diesel") (r ">= 1.2, < 1.5") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r ">= 0.8, < 0.14") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05hb66b2b7v3x7zrksv9brf9s48fwkcarwbm70v0zk1ks96d7gww") (f (quote (("postgres") ("default" "postgres"))))))

