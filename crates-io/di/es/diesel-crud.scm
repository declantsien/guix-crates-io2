(define-module (crates-io di es diesel-crud) #:use-module (crates-io))

(define-public crate-diesel-crud-0.1.0 (c (n "diesel-crud") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "0vx7m981gsq3xw7z3yfq8v5rz38zyzqh4776fsvgil65hdx7bhnk")))

(define-public crate-diesel-crud-0.1.1 (c (n "diesel-crud") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "07yvj7ywrik356bqlvjkvysdw6xs9gm12cq60i000fqd0wzjxw6f")))

(define-public crate-diesel-crud-0.1.2 (c (n "diesel-crud") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "13szpkk9b8g1w5c4w3vcz6dp9ipzis2ixn4pnjzljqll0db7s599")))

(define-public crate-diesel-crud-0.1.3 (c (n "diesel-crud") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "12m7q31jzgdi91p8cihnlqzxz7ypgsvca4cbfyx0rlkf5y1gccmw")))

(define-public crate-diesel-crud-0.2.0 (c (n "diesel-crud") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.3") (f (quote ("r2d2"))) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.8") (d #t) (k 0)))) (h "02w06vjfk1727q6hxrf93i2vsvmwc0x9gwlqa23zzvp0g9ymd2wm")))

