(define-module (crates-io di es diesel-point) #:use-module (crates-io))

(define-public crate-diesel-point-0.1.0 (c (n "diesel-point") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ql2wkzv697w67p7dsp37idv838s0bx17b91qy50qkwvhyg30rc9")))

(define-public crate-diesel-point-0.1.1 (c (n "diesel-point") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14pcsvnklswv02z2pkiwz76vc8a2cx2x6x2pwnm37iv8qhlvy4p6")))

