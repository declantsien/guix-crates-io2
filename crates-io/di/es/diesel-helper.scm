(define-module (crates-io di es diesel-helper) #:use-module (crates-io))

(define-public crate-diesel-helper-0.0.1 (c (n "diesel-helper") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("r2d2" "chrono" "postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "077zcvfzixb568ywgwvn5wwwzm3s313ms2h66bcn4azyn6flgc4a")))

