(define-module (crates-io di es diesel-derive-newtype) #:use-module (crates-io))

(define-public crate-diesel-derive-newtype-0.1.0 (c (n "diesel-derive-newtype") (v "0.1.0") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18mbrs5dj6i09552j0c5lxfqh4nrnicy4f3qyqi3cq19sj81f425")))

(define-public crate-diesel-derive-newtype-0.1.1 (c (n "diesel-derive-newtype") (v "0.1.1") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1pyp6ag0y5yc332yj7pnp12c80pl2kyjsh1w99vpmds8ajjnnlqr")))

(define-public crate-diesel-derive-newtype-0.1.2 (c (n "diesel-derive-newtype") (v "0.1.2") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "17vh06vpw32y3gbk6q41aszxz4sdjx7j7m4p1fm2gksxyvkfhi78")))

(define-public crate-diesel-derive-newtype-2.0.0-rc.0 (c (n "diesel-derive-newtype") (v "2.0.0-rc.0") (d (list (d (n "diesel") (r "^2.0.0-rc.0") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0jn3fnsm671mkjqgcwr66xk0218afj3i3knjxi79y11wnra7cmyx")))

(define-public crate-diesel-derive-newtype-1.0.0 (c (n "diesel-derive-newtype") (v "1.0.0") (d (list (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1xbykymcrp114hl79wvmsdiqym9mrcrnwjbsyj8pzd6ahksrcpkg")))

(define-public crate-diesel-derive-newtype-1.0.1 (c (n "diesel-derive-newtype") (v "1.0.1") (d (list (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0h8nlqyl8qzdxmbn6jfpsx8zgjbh4sy810j95h8zbxzqf0j5d68j")))

(define-public crate-diesel-derive-newtype-1.0.2 (c (n "diesel-derive-newtype") (v "1.0.2") (d (list (d (n "diesel") (r "^1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "0vxdj2cmk49qay0wvdqr26zg4apwb406dszc27jd4cgfr85kgw4y") (r "1.56")))

(define-public crate-diesel-derive-newtype-2.0.0 (c (n "diesel-derive-newtype") (v "2.0.0") (d (list (d (n "diesel") (r "^2.0.0-rc.0") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "1c8bjnhdrnyszv8i3x1v43bxz54qwhbf5srrp31cp9lfhdg5fqgb") (r "1.56")))

(define-public crate-diesel-derive-newtype-2.0.1 (c (n "diesel-derive-newtype") (v "2.0.1") (d (list (d (n "diesel") (r "~2.0.0") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "1nrsl25zvyrcd7yw90qr19fp93jljmc4nfyza4zbv4w73h605pql") (r "1.56")))

(define-public crate-diesel-derive-newtype-2.1.0 (c (n "diesel-derive-newtype") (v "2.1.0") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("sqlite"))) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "1dzvxjrbnmps39ai7wnnf9i3l70g2a6pzncvw85gcbdislvp89n7") (r "1.65")))

(define-public crate-diesel-derive-newtype-2.2.0 (c (n "diesel-derive-newtype") (v "2.2.0") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("sqlite"))) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "0ik8c5gzsq9dpwva7zinw1n4hq70408y3yx0q6dkb9gpir38yn7b") (y #t) (r "1.65")))

(define-public crate-diesel-derive-newtype-2.1.1 (c (n "diesel-derive-newtype") (v "2.1.1") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("sqlite"))) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "0kkylhncdmnlcw8cpqybnk2s7jvq0vdwsl1z9kmyi3ccc9llvvgl") (r "1.65")))

(define-public crate-diesel-derive-newtype-2.1.2 (c (n "diesel-derive-newtype") (v "2.1.2") (d (list (d (n "diesel") (r "^2.1.0") (f (quote ("sqlite"))) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "05h7zqa35xzczj94ljn9i9br4alac27qkqp0dir37zl4qn4gdbfm") (r "1.65")))

