(define-module (crates-io di es diesel_full_text_search) #:use-module (crates-io))

(define-public crate-diesel_full_text_search-0.5.1 (c (n "diesel_full_text_search") (v "0.5.1") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.7.0") (d #t) (k 0)))) (h "04fm0fa96g1hxvx9l35rf9679ija7kwn6yg9srswi3dpbvxqrsph")))

(define-public crate-diesel_full_text_search-0.5.2 (c (n "diesel_full_text_search") (v "0.5.2") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.8.0") (d #t) (k 0)))) (h "1g2krvm22sk7nld25r1h2f2z05im3sicihm4imyybaxrfn1css0a")))

(define-public crate-diesel_full_text_search-0.9.0 (c (n "diesel_full_text_search") (v "0.9.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.10.0") (d #t) (k 0)))) (h "0wm5kh0r2rbr5r1hac89ir4iindqvwfsvbyawi6scgnpmmyfhhak")))

(define-public crate-diesel_full_text_search-0.10.0 (c (n "diesel_full_text_search") (v "0.10.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.11.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1ak04y9l1y44f7l7k8z7zkps2pkj84cixrgdcicw83gbg0yx0l79")))

(define-public crate-diesel_full_text_search-0.11.0 (c (n "diesel_full_text_search") (v "0.11.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.12.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "18a07cj2b9kpf52cvijazbxvwd8n5rc9mmdf0c4xlx14wwpgx9pl")))

(define-public crate-diesel_full_text_search-0.11.1 (c (n "diesel_full_text_search") (v "0.11.1") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.12.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1c84am027njq62wfwl79nc15b60hrrhdyjj5h3ppza61ygcakmkv")))

(define-public crate-diesel_full_text_search-0.12.0 (c (n "diesel_full_text_search") (v "0.12.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.13.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "14bm7i28rxpv1wnjvp7rn78iviq77g5fir9anqjl066vn5dl8hm3")))

(define-public crate-diesel_full_text_search-0.13.0 (c (n "diesel_full_text_search") (v "0.13.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.14.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1gj4wq39p1frnqsb72h51l3szm2n6dcwjqkxm0dgqjy5b2wp0zyr")))

(define-public crate-diesel_full_text_search-0.14.0 (c (n "diesel_full_text_search") (v "0.14.0") (d (list (d (n "diesel") (r ">= 0.5.0, < 0.15.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "0qbrai8hcygavj7hk1lx8b2sk1a6kb6l5xzw1jvf642rxj66k8xx") (y #t)))

(define-public crate-diesel_full_text_search-0.14.1 (c (n "diesel_full_text_search") (v "0.14.1") (d (list (d (n "diesel") (r "^0.14.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1lgfgy1ibbyxlp8vk4pikjdiga5qmv2s5cfr61s8fnjb6pxqr1dc")))

(define-public crate-diesel_full_text_search-0.15.0 (c (n "diesel_full_text_search") (v "0.15.0") (d (list (d (n "diesel") (r "^0.15.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1hm75vkmh4rv23pnfw58v65fx1s54nq1lnd3a824bdlhp862hbb6")))

(define-public crate-diesel_full_text_search-0.16.0 (c (n "diesel_full_text_search") (v "0.16.0") (d (list (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1dn40xbhhz8vcmykjkkkgywb94p2bj8plvq63r448lxwfrvb915b")))

(define-public crate-diesel_full_text_search-0.99.0 (c (n "diesel_full_text_search") (v "0.99.0") (d (list (d (n "diesel") (r ">= 0.16.0, < 1.0.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1bq2gwrz3mgm22dd0283l9dhj66xp56vsyhicnjvciwagfkrjr7f")))

(define-public crate-diesel_full_text_search-1.0.0-beta1 (c (n "diesel_full_text_search") (v "1.0.0-beta1") (d (list (d (n "diesel") (r ">= 0.16.0, <= 1.0.0-beta1") (f (quote ("postgres"))) (d #t) (k 0)))) (h "0jvbgkk602smfah7pxiqlnbgy0i2mw2vakxzxqgk4i2382ffllqr")))

(define-public crate-diesel_full_text_search-1.0.0-rc1 (c (n "diesel_full_text_search") (v "1.0.0-rc1") (d (list (d (n "diesel") (r ">= 0.16.0, <= 1.0.0-rc1") (f (quote ("postgres"))) (d #t) (k 0)))) (h "00h4qr3jnfvn9ya7nffbipkj2pk6lbsd7xp3mcxgdn9g3q96n5z1")))

(define-public crate-diesel_full_text_search-1.0.0 (c (n "diesel_full_text_search") (v "1.0.0") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1bmlhmjmgfl6ygic927nxpxmsyzrrnk7z97lf93cvpcrcjr7dfy6")))

(define-public crate-diesel_full_text_search-1.0.1 (c (n "diesel_full_text_search") (v "1.0.1") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "10spn097k63bppff05gs27azkl9qvjfvgawzij5wa210kn6idlvs")))

(define-public crate-diesel_full_text_search-2.0.0 (c (n "diesel_full_text_search") (v "2.0.0") (d (list (d (n "diesel") (r "^2.0.0") (f (quote ("postgres"))) (k 0)))) (h "0wz6c0vy1asxdwj52b0wp8y6ri073mc33iqzpcdxv0cazx73kv7j")))

(define-public crate-diesel_full_text_search-2.1.0 (c (n "diesel_full_text_search") (v "2.1.0") (d (list (d (n "diesel") (r "~2.1.0") (f (quote ("postgres_backend"))) (k 0)))) (h "1pazaf5i1yljgmnj6gidlb3ggvlm3x65l7ljgpfjjyap8j570x9r") (f (quote (("with-diesel-postgres" "diesel/postgres") ("default" "with-diesel-postgres"))))))

(define-public crate-diesel_full_text_search-2.1.1 (c (n "diesel_full_text_search") (v "2.1.1") (d (list (d (n "diesel") (r "~2.1.0") (f (quote ("postgres_backend"))) (k 0)))) (h "02xkya8flhbshl17wny7k7gwc33klr32dspy0bcab9j1wibs0i4b") (f (quote (("with-diesel-postgres" "diesel/postgres") ("default" "with-diesel-postgres"))))))

