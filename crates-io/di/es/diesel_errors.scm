(define-module (crates-io di es diesel_errors) #:use-module (crates-io))

(define-public crate-diesel_errors-0.1.0 (c (n "diesel_errors") (v "0.1.0") (d (list (d (n "diesel") (r "~1.2.0") (f (quote ("postgres" "r2d2" "uuid" "chrono"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3.12") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.12") (d #t) (k 0)))) (h "17b25jvdpkrfrdw5f0cyvy9rnyl8hfjr0cgw6h4zx6zs1nc6w1mp")))

(define-public crate-diesel_errors-0.1.1 (c (n "diesel_errors") (v "0.1.1") (d (list (d (n "diesel") (r "^1.3.2") (f (quote ("postgres" "r2d2" "uuid" "chrono"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.13") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.13") (d #t) (k 0)))) (h "1aknm2qbz9z0ljn67lan4mrlawv0ah9f07dp5hmb4ri03xlbczqi")))

(define-public crate-diesel_errors-0.1.2 (c (n "diesel_errors") (v "0.1.2") (d (list (d (n "diesel") (r "^1.3.2") (f (quote ("postgres" "r2d2" "uuid" "chrono"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.13") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.13") (d #t) (k 0)))) (h "0nq49a45ylh02iibkq2v0fqi63f41hj457hw8zv46765m3rnn0s7")))

(define-public crate-diesel_errors-0.1.3 (c (n "diesel_errors") (v "0.1.3") (d (list (d (n "diesel") (r "^1.3.2") (f (quote ("postgres" "r2d2" "uuid" "chrono"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "rocket") (r "^0.3.13") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "0k10x9aawj3vgs1jszizmd4kw487wnym97ly28b7lxw4nk5qhkj3")))

(define-public crate-diesel_errors-0.1.4 (c (n "diesel_errors") (v "0.1.4") (d (list (d (n "diesel") (r "^1.3.2") (f (quote ("postgres" "r2d2" "uuid" "chrono"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "rocket") (r "^0.3.15") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "0wihx2pqw92si2633pxbpyg0pfrhcqwi7kjgw88hz885g42gl5yp") (y #t)))

