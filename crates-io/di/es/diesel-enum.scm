(define-module (crates-io di es diesel-enum) #:use-module (crates-io))

(define-public crate-diesel-enum-0.0.0 (c (n "diesel-enum") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "19qnvkh4mvbknkf5yffh5i9k7776kcsv90fiz2s0bc9545a7p1fm")))

(define-public crate-diesel-enum-0.0.1 (c (n "diesel-enum") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10xgpi2sawswv7hnmrcbx8jv7c17j77i1w330icp8dq3d298dqq7")))

(define-public crate-diesel-enum-0.0.2 (c (n "diesel-enum") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n3dd0ka5zshng46dm8gyvn16jy0v49mwcpg8cw6vc6rk119fnis")))

(define-public crate-diesel-enum-0.0.3 (c (n "diesel-enum") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14ms7amy26v1qnr6v9p428rz8803x6hl9dal5n5ls925y1shk5my")))

(define-public crate-diesel-enum-0.0.4 (c (n "diesel-enum") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1azkr5dxqxp28r843ha58ndj0l7yj2hyy6lqbm92b46pnnp3gykl")))

(define-public crate-diesel-enum-0.0.5 (c (n "diesel-enum") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k0n6brfl6h9krgk67955dxnxg69zyv43g22mzhp4ch234i8pwv1")))

(define-public crate-diesel-enum-0.0.6 (c (n "diesel-enum") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "151xli3xhm0smmy66gbs9j0bl7dhgshwcb9pnaqqd1j7lwyq3cbs")))

(define-public crate-diesel-enum-0.1.0 (c (n "diesel-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01hv39x7yr3dyp5vm9b83g2g3ydhwq4lggxwa52f3f3rv4c309rx")))

(define-public crate-diesel-enum-0.2.0 (c (n "diesel-enum") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mjzkmihm0a317pl3j7r963815vi9hrppqasma3i34y2rcgl0gdw")))

(define-public crate-diesel-enum-0.2.1 (c (n "diesel-enum") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05dl8w9ynsbngvpkrfwavxpw0avw2snvczbx7zahy4npjc24gf64")))

