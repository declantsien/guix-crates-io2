(define-module (crates-io di es diesel-mysql-spatial) #:use-module (crates-io))

(define-public crate-diesel-mysql-spatial-0.1.0 (c (n "diesel-mysql-spatial") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("mysql"))) (k 0)) (d (n "geo-types") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wkb") (r "^0.7") (d #t) (k 0)))) (h "1zg9m37w9qvra4m3qlwlf3s7sn580g6b39836bnwgpgbpih0z754") (s 2) (e (quote (("serde" "dep:serde" "geo-types/serde"))))))

(define-public crate-diesel-mysql-spatial-0.1.1 (c (n "diesel-mysql-spatial") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("mysql"))) (k 0)) (d (n "geo-types") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wkb") (r "^0.7") (d #t) (k 0)))) (h "19sg9qfrl8q9n38y3zw56if2hn2dpsn1dff4dbvp04xp5lfayfmf") (s 2) (e (quote (("serde" "dep:serde" "geo-types/serde"))))))

(define-public crate-diesel-mysql-spatial-0.1.2 (c (n "diesel-mysql-spatial") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("mysql"))) (k 0)) (d (n "geo-types") (r "^0.7") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wkb") (r "^0.7") (d #t) (k 0)))) (h "1lyk708zasmkbb98h068kk990f6s6whppdcaxqz5gfyhmg6qas97") (s 2) (e (quote (("serde" "dep:serde" "geo-types/serde"))))))

