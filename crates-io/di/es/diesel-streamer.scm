(define-module (crates-io di es diesel-streamer) #:use-module (crates-io))

(define-public crate-diesel-streamer-0.1.0 (c (n "diesel-streamer") (v "0.1.0") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)))) (h "0h8lcki04hkc61y11ndw6kwmhqcawas7545gnhvqdc07jqc91jd0") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56")))

(define-public crate-diesel-streamer-0.1.1 (c (n "diesel-streamer") (v "0.1.1") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)))) (h "1yc7kfddd611hlzalikp6r3q8m30dyq8pf3ykb19jiw1w4x3l68c") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56")))

(define-public crate-diesel-streamer-0.1.2 (c (n "diesel-streamer") (v "0.1.2") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)))) (h "1mhmmn6nqprh69frf7b1q0vksicdz4rp4mjbmi1l6r4bx9lfzyi2") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56")))

(define-public crate-diesel-streamer-0.1.3 (c (n "diesel-streamer") (v "0.1.3") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)))) (h "0745m35fpz9x4l3yw2bfh9b191j8gbwyvxfg5kn5vbkmjm36w1s3") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.4 (c (n "diesel-streamer") (v "0.1.4") (h "0fvyjkwr4ri8hr63ya02qnqc279hfmnv8dw9nvgkvqhw3x6fsvnb") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.5 (c (n "diesel-streamer") (v "0.1.5") (h "0pbrhh3yf85p9znhhfg8d76nknlabx1whx4q81sg2bh25lh10vqp") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.6 (c (n "diesel-streamer") (v "0.1.6") (h "06gf7hdi86xwmk6i455v055288yxc3xvimjmdanvw6vmprqfh1xw") (f (quote (("sync") ("default" "sync") ("async")))) (y #t) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.7 (c (n "diesel-streamer") (v "0.1.7") (h "0x8ipsnc12m7px275g02m3qi0jq1py12by32k7hdac7h0ga3jk96") (f (quote (("sync") ("async")))) (y #t) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.8 (c (n "diesel-streamer") (v "0.1.8") (h "0inzqkwl1bz0xl9x1rx1m5ap3813hhkj06ql66lcm71dbj69ab8n") (f (quote (("sync") ("async")))) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.9 (c (n "diesel-streamer") (v "0.1.9") (h "0bydxibsqmszql9jfjvjlrs58iw9bxgawkrxfzgknwbhh989bywr") (f (quote (("sync") ("async")))) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.10 (c (n "diesel-streamer") (v "0.1.10") (h "1mg2i6hn75z4mrkrm00w8ddgvj67mwhwgf557fgfzfi8ak4a6hs7") (f (quote (("sync") ("async")))) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.11 (c (n "diesel-streamer") (v "0.1.11") (h "0gb79fbaqb4ckkvkv39hgc0w551cmb4nv7h6fb8yd40ld5i5bkyr") (f (quote (("sync") ("async")))) (r "1.56.1")))

(define-public crate-diesel-streamer-0.1.12 (c (n "diesel-streamer") (v "0.1.12") (h "16fciqf24xnvl7gs2vh6j99mgb4fhdrndia9v9cvxck62sqscrip") (f (quote (("sync") ("async")))) (r "1.56.1")))

