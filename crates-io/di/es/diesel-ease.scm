(define-module (crates-io di es diesel-ease) #:use-module (crates-io))

(define-public crate-diesel-ease-0.1.0 (c (n "diesel-ease") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1knrf7smrb2n2j0246i4wvicr2x07d7y1cnmkp1518jsjwzfc961")))

