(define-module (crates-io di es diesel-crud-codegen) #:use-module (crates-io))

(define-public crate-diesel-crud-codegen-0.0.1 (c (n "diesel-crud-codegen") (v "0.0.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "14mw7qw943r5cixn0p0nhgw9rlcmnlc3763wp61rs0bwjiph7dnz")))

(define-public crate-diesel-crud-codegen-0.0.2 (c (n "diesel-crud-codegen") (v "0.0.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03sdwsm11zvfy86pyvwj6q5k9lqndziqgplv1d6mms1apps8kdgw")))

