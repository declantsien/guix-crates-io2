(define-module (crates-io di es diesel_mate_derive) #:use-module (crates-io))

(define-public crate-diesel_mate_derive-0.1.0 (c (n "diesel_mate_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1m6afpld5mhybx3y8yy08wvm8bian89mf59sqd5hdh39q62rhhh7")))

