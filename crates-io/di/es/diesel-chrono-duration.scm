(define-module (crates-io di es diesel-chrono-duration) #:use-module (crates-io))

(define-public crate-diesel-chrono-duration-0.1.0 (c (n "diesel-chrono-duration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1") (d #t) (k 0)))) (h "1bp8j94i90nb8mv9k3v2yryxfvqxk1xbip93xw402ljh3w6gw3mm")))

(define-public crate-diesel-chrono-duration-1.0.0 (c (n "diesel-chrono-duration") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1") (d #t) (k 0)))) (h "13yrr31vydy51b7pvyvcvksdvspi225a8dwbgiw4cv126ql7kcah")))

(define-public crate-diesel-chrono-duration-1.1.0 (c (n "diesel-chrono-duration") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^2") (d #t) (k 0)))) (h "1rw1irrxfhnp1rxq8z8lddj5l1v2d77hhprlsp7wvafmwpkqx0m3")))

