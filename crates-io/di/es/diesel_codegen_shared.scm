(define-module (crates-io di es diesel_codegen_shared) #:use-module (crates-io))

(define-public crate-diesel_codegen_shared-0.8.0 (c (n "diesel_codegen_shared") (v "0.8.0") (d (list (d (n "diesel") (r "^0.8.0") (k 0)) (d (n "dotenv") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1bvck0il90liqgrl8krm4fizykr0jc2250mz6kfb3sgwwjacbm09") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("default" "dotenv"))))))

(define-public crate-diesel_codegen_shared-0.9.0 (c (n "diesel_codegen_shared") (v "0.9.0") (d (list (d (n "diesel") (r "^0.9.0") (k 0)) (d (n "dotenv") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1j31w72c6xa83q0ydhi37gwdhzc5g90zbgl6d0accqm4gnq83xi8") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("default" "dotenv"))))))

(define-public crate-diesel_codegen_shared-0.10.0 (c (n "diesel_codegen_shared") (v "0.10.0") (d (list (d (n "diesel") (r "^0.10.0") (k 0)) (d (n "dotenv") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1gc4q2i4jk77h4zcvvaqhim9s495si1xlf004lgilc960859xazr") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("default" "dotenv"))))))

