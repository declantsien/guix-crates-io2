(define-module (crates-io di es diesel_linker) #:use-module (crates-io))

(define-public crate-diesel_linker-0.1.0 (c (n "diesel_linker") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("postgres" "mysql" "sqlite"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wa4c1rkxq53dqn2h0a7vzx60g3wnysspshq8km10mv314qggpm8")))

(define-public crate-diesel_linker-1.0.0 (c (n "diesel_linker") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("postgres" "mysql" "sqlite"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0an3m75cirfdxxxvi9z1zywx0s2h71f4s002sb4473226gbsz961")))

(define-public crate-diesel_linker-1.1.0 (c (n "diesel_linker") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.18") (d #t) (k 0)) (d (n "diesel") (r "^2.1.4") (f (quote ("postgres" "mysql" "sqlite"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mdl0c52as1xx67g7ks5r4h99g147m3szi4742kqdfc9mw658d34")))

