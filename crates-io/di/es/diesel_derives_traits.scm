(define-module (crates-io di es diesel_derives_traits) #:use-module (crates-io))

(define-public crate-diesel_derives_traits-0.1.0 (c (n "diesel_derives_traits") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.150") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.0.0-beta1") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1z1lhrzx7i6qv7fr4fzzs9avjnc6v32ifyi2cqm1sb0a5l30dw6y")))

(define-public crate-diesel_derives_traits-0.1.1 (c (n "diesel_derives_traits") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.150") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "0bks9j6cm85ls845bx9rv9p3ghh32vpgya8381ndsjfgc6z9s8lk")))

(define-public crate-diesel_derives_traits-0.1.2 (c (n "diesel_derives_traits") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.179") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (d #t) (k 0)))) (h "08cfswgxf628rj42n83j2yxlqav95mpf20l7q055xc1h091g1q0n")))

(define-public crate-diesel_derives_traits-0.2.0 (c (n "diesel_derives_traits") (v "0.2.0") (d (list (d (n "diesel") (r "^1.4.6") (d #t) (k 0)))) (h "16gbp2lizcgrsjfzcg0ns8l95mjmxij208l0qyfn10xmdhn85l32")))

