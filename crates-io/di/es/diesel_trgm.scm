(define-module (crates-io di es diesel_trgm) #:use-module (crates-io))

(define-public crate-diesel_trgm-0.1.0 (c (n "diesel_trgm") (v "0.1.0") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)))) (h "0j4v8gfd6by5bb5dlpisa6a4agvnb4yjm7ckki860af1kns9apwz")))

(define-public crate-diesel_trgm-0.1.1 (c (n "diesel_trgm") (v "0.1.1") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)))) (h "0m5x6apqdrraqfvy7swvl9bi99hk2jclv26arr5z1b3kn5jaswlp")))

