(define-module (crates-io di es diesel_cli_ext) #:use-module (crates-io))

(define-public crate-diesel_cli_ext-0.1.0 (c (n "diesel_cli_ext") (v "0.1.0") (h "0xr9kkhn6wm97ppfjvmvx7fzxr6wc76q4zy7567lmiizsv9yyxlf")))

(define-public crate-diesel_cli_ext-0.1.1 (c (n "diesel_cli_ext") (v "0.1.1") (h "1a2x11xdrsalpgjqskh0yd6s3s911n3h0r5q6amd7dg2mkvbbnr3")))

(define-public crate-diesel_cli_ext-0.1.2 (c (n "diesel_cli_ext") (v "0.1.2") (h "1my5qk531fjivq8f2jnhi445j1n5pavy3n0a4bz1hvh3ad8zdygc")))

(define-public crate-diesel_cli_ext-0.1.3 (c (n "diesel_cli_ext") (v "0.1.3") (h "02nfl4144b4js7852053grdms5dwyhr2jabg4a41wysckisbppyp")))

(define-public crate-diesel_cli_ext-0.1.4 (c (n "diesel_cli_ext") (v "0.1.4") (h "0j31qa7rm92m6z6rvcnk2b0963qpxm2s46znvjw3h00z7bwznh58")))

(define-public crate-diesel_cli_ext-0.1.5 (c (n "diesel_cli_ext") (v "0.1.5") (h "0fsswkamprnh432kj46gwbp51gjzkh1dm8kkqg0kbgjv3fk7bhbb")))

(define-public crate-diesel_cli_ext-0.1.6 (c (n "diesel_cli_ext") (v "0.1.6") (h "17kjfahl7nphdg1xm7mjs6007sbcgq8zlnl5l73ax51hxkv477yy")))

(define-public crate-diesel_cli_ext-0.1.7 (c (n "diesel_cli_ext") (v "0.1.7") (h "17h0awr0bghy33splxjdw9g0f91k9rsjnbgzg6m8ghpa7ln2ps9i")))

(define-public crate-diesel_cli_ext-0.1.8 (c (n "diesel_cli_ext") (v "0.1.8") (h "0msfv2gdq9myr5im0z4n63b8h0amz5ncmq14kdizd7zdhf0j3x5d")))

(define-public crate-diesel_cli_ext-0.1.9 (c (n "diesel_cli_ext") (v "0.1.9") (h "1y2s505mll1cmn8ij7fmvn8zijcf64h1bxikmrwgrc5vhsjnix5w")))

(define-public crate-diesel_cli_ext-0.1.10 (c (n "diesel_cli_ext") (v "0.1.10") (h "1k2qjavky2sj5gbm0xzng6jpyza2fqj6hczd94snk8dp53scyyby")))

(define-public crate-diesel_cli_ext-0.1.11 (c (n "diesel_cli_ext") (v "0.1.11") (h "0a3ac397yrzgxh2ipybgl91599w3zi81x02nf2bq70r2f3inxqvd")))

(define-public crate-diesel_cli_ext-0.1.12 (c (n "diesel_cli_ext") (v "0.1.12") (h "0qc1nxq3knpfxp6adrdk4hr4adwn2fbyyfq28rpakgjxdpp0dplc")))

(define-public crate-diesel_cli_ext-0.1.13 (c (n "diesel_cli_ext") (v "0.1.13") (h "0v2pqfi4qh5331p3xmvvykil9qrc3zid5axdf3n6bsypk78zknrn")))

(define-public crate-diesel_cli_ext-0.1.14 (c (n "diesel_cli_ext") (v "0.1.14") (h "09yirkbx2rgc849q4iml68j0cf9dh8qbd40qkhayqpikwxs67q2b")))

(define-public crate-diesel_cli_ext-0.1.15 (c (n "diesel_cli_ext") (v "0.1.15") (h "1il41bac7pcjghz9pryvmdfz0mb5h5h92ycwc7vjvpg8q7w4lw42")))

(define-public crate-diesel_cli_ext-0.1.16 (c (n "diesel_cli_ext") (v "0.1.16") (h "02nsgcvdjkcdm1wg3bi3lx94ylcid06sqnmgsshpgk64f2ifdrpa")))

(define-public crate-diesel_cli_ext-0.1.17 (c (n "diesel_cli_ext") (v "0.1.17") (h "0kynsq63n64s7s3q104kr6vv93r5k2jpq2pgp98xsjbs9xjpbqf3")))

(define-public crate-diesel_cli_ext-0.2.0 (c (n "diesel_cli_ext") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0qk5l527bl2d2pjyyqrf7905cvnrgbq2v4qajq1yy4fsk3nlixqh")))

(define-public crate-diesel_cli_ext-0.2.1 (c (n "diesel_cli_ext") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0w9qmb09qnin2h8c94zj3g9sj3b5pspf79jc1wix3nzb3s9zjrmn")))

(define-public crate-diesel_cli_ext-0.2.2 (c (n "diesel_cli_ext") (v "0.2.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0vl4359j9hykqglcfg6hh7g8srd52sax2p8m3xmmi2jx8pawk4ni")))

(define-public crate-diesel_cli_ext-0.2.3 (c (n "diesel_cli_ext") (v "0.2.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0b4c8bi2z3i0j8wy5xwhkdr2rnfmrhv54aklpl7p7ifbx1rr5chv")))

(define-public crate-diesel_cli_ext-0.2.4 (c (n "diesel_cli_ext") (v "0.2.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "16mak9fcp0lkas012cfijg0cfpm75i3qapzs1rkihch7rj3zczxx")))

(define-public crate-diesel_cli_ext-0.2.5 (c (n "diesel_cli_ext") (v "0.2.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1i2z2n0g6s43yjwx2b84mdyz44wwsl8qlyqjzj29dbq3n7spg2k9")))

(define-public crate-diesel_cli_ext-0.2.6 (c (n "diesel_cli_ext") (v "0.2.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0r3aqji5hs9w49hkymc32kp48f5xqslg9874is4qb0c9a4cb066p")))

(define-public crate-diesel_cli_ext-0.2.8 (c (n "diesel_cli_ext") (v "0.2.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0yc1f5dpaj3r5qxbp5wq330s108wacrpphg18k6szagmziiw6x42")))

(define-public crate-diesel_cli_ext-0.2.9 (c (n "diesel_cli_ext") (v "0.2.9") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "03d1yvfq2v0abv8gl5ly8m6qivxcxnq1sf1453szrz9fhnbj6rhv")))

(define-public crate-diesel_cli_ext-0.3.0 (c (n "diesel_cli_ext") (v "0.3.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1zf7myik20i0gw765ns7qn2xs6sw7qp3d4ffv6dpi72dkpvhv4vi")))

(define-public crate-diesel_cli_ext-0.3.1 (c (n "diesel_cli_ext") (v "0.3.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0ig7hv7f3lbwrm8c7403pkfmhp4f1yi0mmnr9q7hggqf3054zslg")))

(define-public crate-diesel_cli_ext-0.3.2 (c (n "diesel_cli_ext") (v "0.3.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "03fvyn6lccy36nxxjw12idfjk8grgdpfrh25rbhy8w5103i09h5z")))

(define-public crate-diesel_cli_ext-0.3.3 (c (n "diesel_cli_ext") (v "0.3.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "0r0hkgydslswxs8xri4r8imgwvdxy4xs9s3x3gmh8a7vqzldcp6x")))

(define-public crate-diesel_cli_ext-0.3.4 (c (n "diesel_cli_ext") (v "0.3.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1rghd5zw3nfk5gd42ccjd7zy3ws4n406s24j024ikrkpbfaq0nbv")))

(define-public crate-diesel_cli_ext-0.3.5 (c (n "diesel_cli_ext") (v "0.3.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0syl1hjszb8glysnhr23i7mfn4p4n4ad86vi4fcr7596v76ynnhp")))

(define-public crate-diesel_cli_ext-0.3.6 (c (n "diesel_cli_ext") (v "0.3.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "14kk1nb1wnq6kvdms6x6absl75rq0zlyx92s2aw7j0j2qidifqyl")))

(define-public crate-diesel_cli_ext-0.3.7 (c (n "diesel_cli_ext") (v "0.3.7") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1z9gy9ycz26r4zvkichxgh40j07mwys9rzcy59lzqv86cd77pqjg")))

(define-public crate-diesel_cli_ext-0.3.8 (c (n "diesel_cli_ext") (v "0.3.8") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0v2a947085xniajn1y29gsj9q6nzlx4ydc67icnwy22xqkn8n2mm")))

(define-public crate-diesel_cli_ext-0.3.9 (c (n "diesel_cli_ext") (v "0.3.9") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0zvklhkrlrfrr0bp0pdmlyqm4nvdjq3zks4b5gq23rywh6n5c29i")))

(define-public crate-diesel_cli_ext-0.3.10 (c (n "diesel_cli_ext") (v "0.3.10") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1h25b5apfmr72kn2brg4prvpqpvck8afp7c3m1na56l1ks885bal")))

(define-public crate-diesel_cli_ext-0.3.11 (c (n "diesel_cli_ext") (v "0.3.11") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "04z9ic158kifmxx40nrpp5cw59kysbpgzcmkda3w5fskfqda4x0f")))

(define-public crate-diesel_cli_ext-0.3.13 (c (n "diesel_cli_ext") (v "0.3.13") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "0bz2bpv6xlb14dcsqjzbd35fsswhlcx833sijcp90fa6y612kqc9")))

(define-public crate-diesel_cli_ext-0.3.14 (c (n "diesel_cli_ext") (v "0.3.14") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1k8l2lxq6yh43qjalgr8qm7cd9my89zav1r94khmapivkv65g0fk")))

