(define-module (crates-io di es diesel_pg_tester) #:use-module (crates-io))

(define-public crate-diesel_pg_tester-0.5.0 (c (n "diesel_pg_tester") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "thread-id") (r "^3") (d #t) (k 0)))) (h "1i4qz4pq1pj3pjpb6axcwrzxjwwkw1ksmvdsdwsq9ssx9r8fqgbv")))

(define-public crate-diesel_pg_tester-0.5.1 (c (n "diesel_pg_tester") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "thread-id") (r "^3") (d #t) (k 0)))) (h "02jkni0scqk2yfd1b6nh0h3ylwkipcaac5frbwc1d7pvnvk8vk22")))

