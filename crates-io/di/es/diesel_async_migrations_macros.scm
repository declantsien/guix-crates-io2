(define-module (crates-io di es diesel_async_migrations_macros) #:use-module (crates-io))

(define-public crate-diesel_async_migrations_macros-0.12.0 (c (n "diesel_async_migrations_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0ybnqj8ndbii82xy4vfxssgrzbrrz0vkrc014lb1ib76647j3ph5")))

