(define-module (crates-io di es diesele) #:use-module (crates-io))

(define-public crate-diesele-0.1.0 (c (n "diesele") (v "0.1.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("postgres_backend"))) (k 0)) (d (n "diesel-async") (r "^0.4") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "scoped-futures") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sqlformat") (r "^0.2") (d #t) (k 0)))) (h "0l5j8p4sw77adk4al0gncnm0ssqgj3f35p3qgcl917d959r95i6n")))

