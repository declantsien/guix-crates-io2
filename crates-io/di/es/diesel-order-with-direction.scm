(define-module (crates-io di es diesel-order-with-direction) #:use-module (crates-io))

(define-public crate-diesel-order-with-direction-0.1.0 (c (n "diesel-order-with-direction") (v "0.1.0") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("mysql"))) (d #t) (k 2)))) (h "1jl1ks7yfv4i8gkyfxxgalvai3cw5wdgb29zk441hahd43p9q4l5")))

(define-public crate-diesel-order-with-direction-0.2.0 (c (n "diesel-order-with-direction") (v "0.2.0") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)) (d (n "diesel") (r "^2") (f (quote ("mysql"))) (d #t) (k 2)))) (h "1v4fvjzmqvd7cljmjsaqvdm89mc7kmph1nki6aalrrf27dj2vwis")))

(define-public crate-diesel-order-with-direction-0.2.1 (c (n "diesel-order-with-direction") (v "0.2.1") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)) (d (n "diesel") (r "^2") (f (quote ("mysql"))) (d #t) (k 2)))) (h "08ipi3d8j42z7sb6n3kzd2hsqmfqdl4gszxhk509cc4bgc5rh47m")))

(define-public crate-diesel-order-with-direction-0.1.1 (c (n "diesel-order-with-direction") (v "0.1.1") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("mysql"))) (d #t) (k 2)))) (h "0s10hr39wx5fcqpdv0b9r6wzkvmaprx82aq1bfa6majjbw5rgh01")))

(define-public crate-diesel-order-with-direction-0.2.2 (c (n "diesel-order-with-direction") (v "0.2.2") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)) (d (n "diesel") (r "^2") (f (quote ("mysql"))) (d #t) (k 2)))) (h "0ar18bld3v486pnnncxa6l60nmdj6mb61931nvqybn1nvf3xqd6r")))

(define-public crate-diesel-order-with-direction-0.1.2 (c (n "diesel-order-with-direction") (v "0.1.2") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1") (f (quote ("mysql"))) (d #t) (k 2)))) (h "10lj93c8f0wn1sjsz2bxdvyh1qj76jgl9mkx1i4bfink062x61c7")))

