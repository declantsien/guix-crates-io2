(define-module (crates-io di es diesel-geography) #:use-module (crates-io))

(define-public crate-diesel-geography-0.1.0 (c (n "diesel-geography") (v "0.1.0") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "postgis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jfdq36pz694sb9ymrlv9w7h9136mmygznfhsnpd1sfmdikskhwv")))

(define-public crate-diesel-geography-0.2.0 (c (n "diesel-geography") (v "0.2.0") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "postgis") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n6cvrzmrkwbkgsvczzf8xyi34v5fnrirskzf3z903yrzz6qibvn")))

