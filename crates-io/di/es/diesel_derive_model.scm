(define-module (crates-io di es diesel_derive_model) #:use-module (crates-io))

(define-public crate-diesel_derive_model-0.1.0 (c (n "diesel_derive_model") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.21") (d #t) (k 0)))) (h "13j4irhy0vfk1a1hb2ay8iawq5gf615j51imz0lklcn8r77lwkyl")))

