(define-module (crates-io di es diesel_table_macro_syntax) #:use-module (crates-io))

(define-public crate-diesel_table_macro_syntax-0.1.0 (c (n "diesel_table_macro_syntax") (v "0.1.0") (d (list (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i9115qgsnargr6a707lqcjc45wqzq351a2gbvnnyw2kqkpmfmgw") (r "1.65.0")))

