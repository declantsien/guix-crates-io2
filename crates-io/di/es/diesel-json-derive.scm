(define-module (crates-io di es diesel-json-derive) #:use-module (crates-io))

(define-public crate-diesel-json-derive-0.1.0 (c (n "diesel-json-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "0w8hwnzsdrs7j4cbh2fn16jvy654dl9wgga5hmgzw2csdv33xfx4")))

(define-public crate-diesel-json-derive-0.1.1 (c (n "diesel-json-derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro" "derive"))) (k 0)))) (h "1c4n4k9zb842s08cmayaknrmxx2bbaxg20gjqlzcvk5rp9yicxyh")))

