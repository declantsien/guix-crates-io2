(define-module (crates-io di es diesel_transaction_handles) #:use-module (crates-io))

(define-public crate-diesel_transaction_handles-0.1.0 (c (n "diesel_transaction_handles") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0vwdwk8925rxys779gw6xl50pmj90sx21a56q7yfzvczlhmpzwfy") (f (quote (("rollback_hooks") ("panic_errors_on_drop") ("log_errors_on_drop") ("default" "rollback_hooks"))))))

(define-public crate-diesel_transaction_handles-0.1.1 (c (n "diesel_transaction_handles") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0hhk5dx1cr7a7l81fwbkxkjz318cizdfaxxyb9kgi4jvglbbbgdx") (f (quote (("rollback_hooks") ("panic_errors_on_drop") ("log_errors_on_drop") ("default" "rollback_hooks"))))))

