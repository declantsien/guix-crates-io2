(define-module (crates-io di es diesel_cockroach) #:use-module (crates-io))

(define-public crate-diesel_cockroach-0.1.0 (c (n "diesel_cockroach") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1khpc1fl981c8nrh8b64m7xkklvkksry2li80dhw4lp9qi3chs5h")))

(define-public crate-diesel_cockroach-0.1.1 (c (n "diesel_cockroach") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres"))) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0b5aypzpi8q64q7jrsacrwiyy21xnp6isc7i4h8h1hrgic5lnvlr")))

