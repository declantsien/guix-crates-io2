(define-module (crates-io di es diesel_async_migrations) #:use-module (crates-io))

(define-public crate-diesel_async_migrations-0.12.0 (c (n "diesel_async_migrations") (v "0.12.0") (d (list (d (n "diesel") (r "^2") (d #t) (k 0)) (d (n "diesel-async") (r "^0.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel_async_migrations_macros") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "scoped-futures") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0lsmna7yp6qwrsyxr2p07xn07w9n5gyp0n86sfa3p5qp7awdc057")))

