(define-module (crates-io di es diesel-citext) #:use-module (crates-io))

(define-public crate-diesel-citext-0.1.0 (c (n "diesel-citext") (v "0.1.0") (d (list (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "037r0y4xwbyw75i3g74501rs467ybz9fi7f1p18nd1yvgrisqj8p")))

(define-public crate-diesel-citext-0.2.0 (c (n "diesel-citext") (v "0.2.0") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "010s4x3dr8sfkqwmzl1934s4f2ipygaqg5bpmsmx4wjk72mr1cxv") (f (quote (("with-actix-web" "actix-web"))))))

(define-public crate-diesel-citext-0.3.0 (c (n "diesel-citext") (v "0.3.0") (d (list (d (n "actix-web") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pjckfwz11caca26mbnkfixnl4k1rnlgcn8mshcfmkplrjpbdvf3") (f (quote (("with-actix-web" "actix-web"))))))

