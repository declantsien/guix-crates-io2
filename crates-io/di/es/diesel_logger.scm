(define-module (crates-io di es diesel_logger) #:use-module (crates-io))

(define-public crate-diesel_logger-0.1.0 (c (n "diesel_logger") (v "0.1.0") (d (list (d (n "clippy") (r "= 0.0.179") (o #t) (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "19xyij39mgk42gz1wh8n0wab2ibafxq8pga4cayg9zzhs28sfwqx")))

(define-public crate-diesel_logger-0.1.1 (c (n "diesel_logger") (v "0.1.1") (d (list (d (n "diesel") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1drxds26v11inhnzpk05a8s0qr1jg4asfx0mllmbz1j5s5d974qp")))

(define-public crate-diesel_logger-0.2.0 (c (n "diesel_logger") (v "0.2.0") (d (list (d (n "diesel") (r "^2.0") (f (quote ("r2d2" "i-implement-a-third-party-backend-and-opt-into-breaking-changes"))) (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1yyzbrxlmvsyp5yyb102brk5kizd1nrjl2wiaj4yssaah3sb28hs")))

(define-public crate-diesel_logger-0.3.0 (c (n "diesel_logger") (v "0.3.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("r2d2" "i-implement-a-third-party-backend-and-opt-into-breaking-changes"))) (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)))) (h "0bsz6x2y68knd4rjf8w59kfx7lkgswv5zcqzn74rs4hpfm80n093")))

