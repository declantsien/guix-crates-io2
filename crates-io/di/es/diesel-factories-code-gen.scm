(define-module (crates-io di es diesel-factories-code-gen) #:use-module (crates-io))

(define-public crate-diesel-factories-code-gen-0.0.1 (c (n "diesel-factories-code-gen") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1m3f591gj3h7hyrjv3z1384z4k58y47b62l1l1cdb6rmmk08xigk")))

(define-public crate-diesel-factories-code-gen-0.1.0 (c (n "diesel-factories-code-gen") (v "0.1.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00lkdhzhmwlgnsgmr4idq5xlcz7sapv2chvl394vl7d6czp8zliy")))

(define-public crate-diesel-factories-code-gen-0.1.1 (c (n "diesel-factories-code-gen") (v "0.1.1") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08883r7j52kadnmn8fqglb3958a6hzgirkh9h6m6x63qs6r7fhk1")))

(define-public crate-diesel-factories-code-gen-0.1.2 (c (n "diesel-factories-code-gen") (v "0.1.2") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "19v74w6ccxqhk1aaax7hpvs53202lc8b7kih0cf6g8kmg7lr7h71")))

(define-public crate-diesel-factories-code-gen-1.0.0 (c (n "diesel-factories-code-gen") (v "1.0.0") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1byi4k68vq987dmannpa9dc15p5byprzmdhkzx7zhaggm54sqagr")))

(define-public crate-diesel-factories-code-gen-1.0.1 (c (n "diesel-factories-code-gen") (v "1.0.1") (d (list (d (n "darling") (r "^0.9") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lv3y59r1wb0r75mnqiy5wzn8j6a9q35aqb2zsaigmgp7hl165gp")))

(define-public crate-diesel-factories-code-gen-2.0.0 (c (n "diesel-factories-code-gen") (v "2.0.0") (d (list (d (n "bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vr05kz9v75r4vmkasm3gqbimsl3vn5bjb5189gy212qf9fmh714")))

