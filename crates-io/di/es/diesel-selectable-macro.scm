(define-module (crates-io di es diesel-selectable-macro) #:use-module (crates-io))

(define-public crate-diesel-selectable-macro-0.1.0 (c (n "diesel-selectable-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0460vwbfxpprgn07qg26q79r3c7gvdlgra3lydl1x2kl65jjfsjn")))

(define-public crate-diesel-selectable-macro-0.2.0 (c (n "diesel-selectable-macro") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cv06b5klpr52jhj8fkmzl5p5w2v00bs03ca3bj7052ah6n2mja9") (y #t)))

(define-public crate-diesel-selectable-macro-0.3.0 (c (n "diesel-selectable-macro") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ccbwkc2s49mkqc1xv1s43k69lr4r5y8r043yka3p3wsn01qvr4k")))

(define-public crate-diesel-selectable-macro-1.0.0 (c (n "diesel-selectable-macro") (v "1.0.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggkdqx0z21vcdib2q8wzd97zy98awrgmp07y2lj97xncqvyfyrw")))

