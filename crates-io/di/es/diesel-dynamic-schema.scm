(define-module (crates-io di es diesel-dynamic-schema) #:use-module (crates-io))

(define-public crate-diesel-dynamic-schema-0.2.0-rc.0 (c (n "diesel-dynamic-schema") (v "0.2.0-rc.0") (d (list (d (n "diesel") (r "~2.0.0-rc.0") (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 2)))) (h "125ig83whhkjyqf15cr4gwhvq2ppv6bp0qkxmqyjn2vgf3pr7wc7") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres_backend") ("mysql" "diesel/mysql_backend") ("default"))))))

(define-public crate-diesel-dynamic-schema-0.2.0-rc.1 (c (n "diesel-dynamic-schema") (v "0.2.0-rc.1") (d (list (d (n "diesel") (r "~2.0.0-rc.1") (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 2)))) (h "070dyjrx718a0d9lq38686dz86bci2aadx81jah63rabk18d0imc") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres_backend") ("mysql" "diesel/mysql_backend") ("default")))) (r "1.56.0")))

(define-public crate-diesel-dynamic-schema-0.2.0 (c (n "diesel-dynamic-schema") (v "0.2.0") (d (list (d (n "diesel") (r "~2.0.0") (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 2)))) (h "0wxmf8zk5lh4a32bz1h12qlb23ibz5dwg7p1f38ali7g8fgirhc8") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres_backend") ("mysql" "diesel/mysql_backend") ("default")))) (r "1.56.0")))

(define-public crate-diesel-dynamic-schema-0.2.1 (c (n "diesel-dynamic-schema") (v "0.2.1") (d (list (d (n "diesel") (r "~2.1.0") (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 2)))) (h "0ab08q4g5zl43bf8xl0jgd6332d5s9gsnr51m326g11rys94pav4") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres_backend") ("mysql" "diesel/mysql_backend") ("default")))) (r "1.65.0")))

