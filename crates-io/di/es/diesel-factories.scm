(define-module (crates-io di es diesel-factories) #:use-module (crates-io))

(define-public crate-diesel-factories-0.0.1 (c (n "diesel-factories") (v "0.0.1") (d (list (d (n "diesel") (r "^1.3.3") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^0.0.1") (d #t) (k 0)))) (h "128x0692vyi06gbx03wwbdjc33jzpm5q8x9mhisylwad5j256zzr")))

(define-public crate-diesel-factories-0.1.0 (c (n "diesel-factories") (v "0.1.0") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "0faivlnmx66kiy7ay41rkp8bvzbqzy4ibnzcllhpa6hqcppz0nh6")))

(define-public crate-diesel-factories-0.1.1 (c (n "diesel-factories") (v "0.1.1") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.3") (d #t) (k 2)))) (h "1kvrkqd9052n9n0rpi2s7gsaxv9sqmyz7mybii0zjr61pvp2l25s")))

(define-public crate-diesel-factories-0.1.2 (c (n "diesel-factories") (v "0.1.2") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.3") (d #t) (k 2)))) (h "1cm8nryjn00221gzsiz3wlbxxkxanw4lhwzqcd9asgygyjkafzzq")))

(define-public crate-diesel-factories-1.0.0 (c (n "diesel-factories") (v "1.0.0") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.3") (d #t) (k 2)))) (h "1334a64cb0xkc7z0l4jxln95mhi5zp8cy5c3jkxc92zjdpm55zl4")))

(define-public crate-diesel-factories-1.0.1 (c (n "diesel-factories") (v "1.0.1") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.3") (d #t) (k 2)))) (h "0rj8b8cn395kj6xsnn0b0xpi0d07ydw785fygk2zvrm6yhlya6sg")))

(define-public crate-diesel-factories-2.0.0 (c (n "diesel-factories") (v "2.0.0") (d (list (d (n "diesel") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.3") (f (quote ("postgres" "network-address"))) (d #t) (k 2)) (d (n "diesel-factories-code-gen") (r "^2.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.3") (d #t) (k 2)))) (h "0z5mrd5l8n40x9qv5aywskfxbpmm2zi4jq4p8imahh3frdfhc70x")))

