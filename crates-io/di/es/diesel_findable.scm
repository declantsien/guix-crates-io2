(define-module (crates-io di es diesel_findable) #:use-module (crates-io))

(define-public crate-diesel_findable-0.1.0 (c (n "diesel_findable") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1c8hiryaazp89g3805g1r3k8cfgscb9fir3xsdqs8va2zvml89gb")))

(define-public crate-diesel_findable-0.1.1 (c (n "diesel_findable") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "0l7mpmbnfr2dc6k646scnb8wh0amy9i6dhkcc8kpjn795ndlq2lx")))

(define-public crate-diesel_findable-0.1.2 (c (n "diesel_findable") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1idd9ql92n90kb6dskzvs9091ah30n3farmcpx4m8aq1y9gc8agw")))

(define-public crate-diesel_findable-0.1.3 (c (n "diesel_findable") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1k8cwzgybl2ai6c5i216gwp12b12f78iaggmjj6h12p17fdri3jq")))

(define-public crate-diesel_findable-0.1.4 (c (n "diesel_findable") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "0gd6z6i8yymxc0pzazq770f65vmr8251bbf2npl1gwfrx4j9nx4d")))

(define-public crate-diesel_findable-0.1.5 (c (n "diesel_findable") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1sghqdlr2av7b5x2751r3zfrf0smhww8asjl76frm5wa5xfnpajw")))

(define-public crate-diesel_findable-0.1.6 (c (n "diesel_findable") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1djgss364ll83q8n3lz1wcfd611sslvx7cpfs065l4x3d8h7x9dg")))

(define-public crate-diesel_findable-0.1.7 (c (n "diesel_findable") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (d #t) (k 0)))) (h "1z4qq50528h6k0qkv620dlrzrpfjnsf8iby3wraf5cc5b5z9b1dr")))

