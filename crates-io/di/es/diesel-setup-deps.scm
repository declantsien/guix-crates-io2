(define-module (crates-io di es diesel-setup-deps) #:use-module (crates-io))

(define-public crate-diesel-setup-deps-0.1.0 (c (n "diesel-setup-deps") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16x96xprp65mfzks2zza7q4nny1111wbqkmbjxcs8fl8v9hmkc84")))

