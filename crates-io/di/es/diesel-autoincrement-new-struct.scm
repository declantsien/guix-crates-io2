(define-module (crates-io di es diesel-autoincrement-new-struct) #:use-module (crates-io))

(define-public crate-diesel-autoincrement-new-struct-0.1.0 (c (n "diesel-autoincrement-new-struct") (v "0.1.0") (d (list (d (n "diesel") (r "^2.0.0-rc.1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0ghngzpmgzrkjpwja7zlmyircs47h2dg206i0y9hf29n3m2bzmzz")))

(define-public crate-diesel-autoincrement-new-struct-0.1.1 (c (n "diesel-autoincrement-new-struct") (v "0.1.1") (d (list (d (n "diesel") (r "^2.0.0-rc.1") (f (quote ("sqlite"))) (d #t) (k 2)) (d (n "macro_rules_attribute") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1jyvwr59a3wv7vbmw6177nj33lrfxnr4fd4wh2pislj2gdlq2xfq")))

