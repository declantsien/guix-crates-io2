(define-module (crates-io di es diesel_json) #:use-module (crates-io))

(define-public crate-diesel_json-0.1.0 (c (n "diesel_json") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4") (f (quote ("postgres" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r ">=0.8.0, <2.0") (d #t) (k 0)))) (h "0ycx716g9x65ziv8h8ks79r7h3a44b8219xlrp5hkwjhga2sjccj")))

(define-public crate-diesel_json-0.1.1 (c (n "diesel_json") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4") (f (quote ("postgres" "serde_json"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r ">=0.8.0, <2.0") (d #t) (k 0)))) (h "1jcpnbl9kj0yfnwlq3syiy9vl893vk175f7xggxhhdbd7gvg04i8")))

(define-public crate-diesel_json-0.2.0 (c (n "diesel_json") (v "0.2.0") (d (list (d (n "diesel") (r "^2.0") (f (quote ("postgres" "serde_json"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r ">=0.8.0, <2.0") (d #t) (k 0)))) (h "0zc6k2f26q604ykdbkdyf06c7f6pxsrlg6ipj3320ck5m83crkph")))

(define-public crate-diesel_json-0.2.1 (c (n "diesel_json") (v "0.2.1") (d (list (d (n "diesel") (r "^2.0") (f (quote ("postgres" "serde_json"))) (k 0)) (d (n "diesel_migrations") (r "^2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r ">=0.8.0, <2.0") (d #t) (k 0)))) (h "120qjmpj6pb77ky9rvplhnsklkk88sw2sb9yn3xmvd99szhhvi8g")))

