(define-module (crates-io di es diesel-derive-enum) #:use-module (crates-io))

(define-public crate-diesel-derive-enum-0.1.0 (c (n "diesel-derive-enum") (v "0.1.0") (d (list (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "10ligrl4ik3l1x5s1djzj348xbibvd43xgpi2jwgvzg415mf3xmv")))

(define-public crate-diesel-derive-enum-0.2.0 (c (n "diesel-derive-enum") (v "0.2.0") (d (list (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "041fkn4ixwc89k9fsnvixsx5nyr64hs4aypy2amrdsggzwrixy32")))

(define-public crate-diesel-derive-enum-0.2.1 (c (n "diesel-derive-enum") (v "0.2.1") (d (list (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0nxcj31fk09mg9rk0190z5mw8iyzrs9crbwyj0qprij80wqdm96w")))

(define-public crate-diesel-derive-enum-0.2.2 (c (n "diesel-derive-enum") (v "0.2.2") (d (list (d (n "diesel") (r "^1") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0xbkjmy5wpcy1jijk6ihdwdhvgzk4lza3whx0z4qasfqnjhd6hd5")))

(define-public crate-diesel-derive-enum-0.3.0 (c (n "diesel-derive-enum") (v "0.3.0") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0ky0ig2z0wf0pzmiqqay0irv0rnqc3dfvjxq79q4pbf7mwxmwgmp")))

(define-public crate-diesel-derive-enum-0.4.0 (c (n "diesel-derive-enum") (v "0.4.0") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres" "sqlite"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1x3j8vb7knn2f5wfralppbxbdcrna6c5f2ayxlqb0nq906s9anlw") (f (quote (("sqlite") ("postgres"))))))

(define-public crate-diesel-derive-enum-0.4.1 (c (n "diesel-derive-enum") (v "0.4.1") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres" "sqlite"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0w08qscsz4qlib1zmqkmarkca0r3h82smi1inhcnzjx2j88kzsnl") (f (quote (("sqlite") ("postgres")))) (y #t)))

(define-public crate-diesel-derive-enum-0.4.2 (c (n "diesel-derive-enum") (v "0.4.2") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres" "sqlite"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "16vm18j0lmqhm02sp4aq4yb2y1d3wsvhygw9p36jdhwjnj1h6jaa") (f (quote (("sqlite") ("postgres"))))))

(define-public crate-diesel-derive-enum-0.4.3 (c (n "diesel-derive-enum") (v "0.4.3") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres" "sqlite" "mysql"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1d74a88f3qwzbw7bimj8vnv1cylw138dr1fwj979gnr2z23p6r60") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-0.4.4 (c (n "diesel-derive-enum") (v "0.4.4") (d (list (d (n "diesel") (r "^1.1") (f (quote ("postgres" "sqlite" "mysql"))) (d #t) (k 2)) (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1d9g1ya1f9xr6g2h8dvbn9di3i3grv1jvbffs72fvpjf6khzbfmd") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-1.0.0 (c (n "diesel-derive-enum") (v "1.0.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vrf60xppisdykrfphv8h3n17q5fwzskywza31bmwhkjmdzwpdf2") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-1.1.0 (c (n "diesel-derive-enum") (v "1.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yqd78cl6i6jfl41g7104bk9w5qpnbc402xbq2lqsbgad3172gkh") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-1.1.1 (c (n "diesel-derive-enum") (v "1.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1g23y9ddv9b00q8sbykkndnxvkxkjgya701n4ipn93ijprq6p03h") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-1.1.2 (c (n "diesel-derive-enum") (v "1.1.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vdv2x5xgdka92dg9w3i9f4sy25a2bg0dw4qcbqjlkh13f9112bc") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-2.0.0-rc.0 (c (n "diesel-derive-enum") (v "2.0.0-rc.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jh6g6lz6mv5w47qm1zy0ciix614w3nms46rib2yp17ibfgzqa2z") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-2.0.0 (c (n "diesel-derive-enum") (v "2.0.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "087lhgmqj2fdky0hic29rmawhb1px5gqrx568xv1dd7slc8qa1fs") (f (quote (("sqlite") ("postgres") ("mysql")))) (y #t)))

(define-public crate-diesel-derive-enum-2.0.1 (c (n "diesel-derive-enum") (v "2.0.1") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1hnnl6fw8agal71z3q46gfkrqb5f9skxiqavzmdx0cs3jlxw043b") (f (quote (("sqlite") ("postgres") ("mysql"))))))

(define-public crate-diesel-derive-enum-2.1.0 (c (n "diesel-derive-enum") (v "2.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1m34vd3f3ifdg1kja6rd7ad24alc6lzlh7dd3ms69vwm50d17ic1") (f (quote (("sqlite") ("postgres") ("mysql"))))))

