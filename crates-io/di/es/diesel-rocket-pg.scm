(define-module (crates-io di es diesel-rocket-pg) #:use-module (crates-io))

(define-public crate-diesel-rocket-pg-0.1.0 (c (n "diesel-rocket-pg") (v "0.1.0") (d (list (d (n "diesel") (r "^2.0.3") (f (quote ("r2d2" "postgres"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "04hyqgyrr7c2abl2zmlz458gx6xcrx1wma30nsrngfdsd1l3dlc0")))

