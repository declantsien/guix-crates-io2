(define-module (crates-io di es diesel_ltree) #:use-module (crates-io))

(define-public crate-diesel_ltree-0.1.0 (c (n "diesel_ltree") (v "0.1.0") (d (list (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 0)))) (h "1vp9i4ylg1p4q8nifh2dribn4fvi0dqf55sjs24ayb8qv92rqh4l")))

(define-public crate-diesel_ltree-0.1.1 (c (n "diesel_ltree") (v "0.1.1") (d (list (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_codegen") (r "^0.16.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "0dj6ap8apfj354976hi0bi3mq79pvg1r5m2f1v9j6v4gjxmxnb3w")))

(define-public crate-diesel_ltree-0.1.2 (c (n "diesel_ltree") (v "0.1.2") (d (list (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_codegen") (r "^0.16.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "05zd8zfr5cq85q80ba7nrn7lfflb4sm1zpg2isf669aay5pqzavh")))

(define-public crate-diesel_ltree-0.1.3 (c (n "diesel_ltree") (v "0.1.3") (d (list (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^0.16.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_codegen") (r "^0.16.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "1smgrr78j50l1ahrikm81lwzn31sbvsc0n87q8mj514wkyr95gm5")))

(define-public crate-diesel_ltree-0.2.0 (c (n "diesel_ltree") (v "0.2.0") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_migrations") (r "^1.0") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "0237gx3yiw7qnqd67i727m7z8gnn7yn99psnzm1fnrksw2wr0lhj")))

(define-public crate-diesel_ltree-0.2.1 (c (n "diesel_ltree") (v "0.2.1") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_migrations") (r "^1.0") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "18b1c3n33qggp2fcjgyr3r9wzgxyd2yjngq974sxxdf2bs5m1k9q")))

(define-public crate-diesel_ltree-0.2.2 (c (n "diesel_ltree") (v "0.2.2") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_migrations") (r "^1.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "0bz1rw7c5nsn9yyk35ryvqvfhpmd6ccqxmgxrla37d1hkz1cy7gc")))

(define-public crate-diesel_ltree-0.2.3 (c (n "diesel_ltree") (v "0.2.3") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "diesel") (r "^1.0") (f (quote ("postgres"))) (d #t) (k 1)) (d (n "diesel_migrations") (r "^1.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.10") (d #t) (k 1)) (d (n "dotenv") (r "^0.10") (d #t) (k 2)))) (h "1fyz3rxy37ca7q0y0nh65s91ab1bcmgpmy7pz8g5p2alifzwqxix")))

(define-public crate-diesel_ltree-0.2.4 (c (n "diesel_ltree") (v "0.2.4") (d (list (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "0ikcqwdg0xx0as2nnazlqrxdld3mzhdb5dykir3bmh4yrswbbxqn")))

(define-public crate-diesel_ltree-0.2.5 (c (n "diesel_ltree") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "07kwcy8239bzh98gw75vw64qwwxbbp8ipjv3mny3sz8dsgbw8zxj")))

(define-public crate-diesel_ltree-0.2.6 (c (n "diesel_ltree") (v "0.2.6") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "1wsmhiraghznlzkp5zq34cmq6wsgm7swxcd2bzhkvd8dqdmi96v8")))

(define-public crate-diesel_ltree-0.2.7 (c (n "diesel_ltree") (v "0.2.7") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("postgres"))) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "1c3q4r63vxgxl11a4h6bga5wrg90ggrykkiwcymxi8j8x6rb582m")))

(define-public crate-diesel_ltree-0.3.0 (c (n "diesel_ltree") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "diesel") (r "^2.0") (f (quote ("postgres"))) (k 0)) (d (n "diesel_migrations") (r "^2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "0pfmvff6cm2ns2yd2sdzi0kjd9s1p415h40zlxl7pj6nfawg1ncj")))

(define-public crate-diesel_ltree-0.3.1 (c (n "diesel_ltree") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "diesel") (r "^2.0") (f (quote ("postgres_backend"))) (k 0)) (d (n "diesel") (r "^2.0") (f (quote ("postgres"))) (k 2)) (d (n "diesel_migrations") (r "^2.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15") (d #t) (k 2)))) (h "1wh6brz1qwvb9mszwqw886kmpny484r2drfpgg73vac7lbzq8n4z")))

