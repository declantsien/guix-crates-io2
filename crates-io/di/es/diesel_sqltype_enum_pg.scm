(define-module (crates-io di es diesel_sqltype_enum_pg) #:use-module (crates-io))

(define-public crate-diesel_sqltype_enum_pg-0.1.0 (c (n "diesel_sqltype_enum_pg") (v "0.1.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8s3cavrkibw5yajgsh3qv32njy4wwwgq0xgm7lbcphnfjq4cmd")))

(define-public crate-diesel_sqltype_enum_pg-0.1.1 (c (n "diesel_sqltype_enum_pg") (v "0.1.1") (d (list (d (n "diesel") (r "^2.1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "020q96rrh97mvpyyp09wb3yad8k1djq0f7qbh6d6sllvqk8x69c3")))

(define-public crate-diesel_sqltype_enum_pg-0.2.0 (c (n "diesel_sqltype_enum_pg") (v "0.2.0") (d (list (d (n "diesel") (r "^2.1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zhis1a61l9h0s4y22ddrwlazh5lzv0wafd3a14ijm514pkv4lig")))

