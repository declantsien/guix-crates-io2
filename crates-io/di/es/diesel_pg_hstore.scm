(define-module (crates-io di es diesel_pg_hstore) #:use-module (crates-io))

(define-public crate-diesel_pg_hstore-0.1.0 (c (n "diesel_pg_hstore") (v "0.1.0") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 0)) (d (n "diesel") (r "~1.0.0-beta1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "~0.10") (d #t) (k 2)) (d (n "fallible-iterator") (r "~0.1") (d #t) (k 0)))) (h "0bmd8x1jrdkwj5xs2hvzm0j9if8frgs60biz86cqhl8ggnrz1nx7")))

(define-public crate-diesel_pg_hstore-0.2.0 (c (n "diesel_pg_hstore") (v "0.2.0") (d (list (d (n "byteorder") (r "~1.2") (d #t) (k 0)) (d (n "diesel") (r "~1.0.0-beta1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "~0.10") (d #t) (k 2)) (d (n "fallible-iterator") (r "~0.1") (d #t) (k 0)))) (h "1vk7xpix73smvg21c6griyffk83dkh3fl1430wajvdh83hmbisnv")))

