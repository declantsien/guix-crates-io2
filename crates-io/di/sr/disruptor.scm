(define-module (crates-io di sr disruptor) #:use-module (crates-io))

(define-public crate-disruptor-0.0.1 (c (n "disruptor") (v "0.0.1") (h "1cmm5cxa589j7vbak2fa8vndkkcv8bp7181z7wxksd3jl0brxlgk")))

(define-public crate-disruptor-0.1.0 (c (n "disruptor") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0dr8f46fq9xwjjzwf6nwnf4m1a9zdag1skajlqcyngp98bmdnfqr")))

(define-public crate-disruptor-0.2.0 (c (n "disruptor") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1rsbjf0l5fmxa2cky9x0h1pb9v8i7gvqb0034i60v7b5l57qmcw5")))

(define-public crate-disruptor-0.2.1 (c (n "disruptor") (v "0.2.1") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1y5794s1bjskj5ngm1il69zryh69rsn1l39n938p0f4ahkb3j2qh")))

(define-public crate-disruptor-0.3.0 (c (n "disruptor") (v "0.3.0") (d (list (d (n "core_affinity") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0nihg2imaac3i7ja0hhb0kx29vj4amp8w1whv04jfsidx18jznkw")))

(define-public crate-disruptor-0.4.0 (c (n "disruptor") (v "0.4.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "17r9ahmkgv3rkgghrckwz7q2pdywqf9a3gnd8fjhnvj5a8vslnhf")))

(define-public crate-disruptor-0.5.0 (c (n "disruptor") (v "0.5.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0a57ycll8l463x27kz2r8m168cb2i4bbbi17yd2nrqhg32q6chj1")))

(define-public crate-disruptor-0.6.0 (c (n "disruptor") (v "0.6.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "05hszjmdgkxs8yxbw3k5k115wnr0i5fz37dja2g23fjfzw3hxbv8")))

(define-public crate-disruptor-0.6.1 (c (n "disruptor") (v "0.6.1") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "058xah4xi5hbwmpi573ra55pihv2vl4pxwiws14n08lqcw2gpywn")))

(define-public crate-disruptor-0.7.0 (c (n "disruptor") (v "0.7.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0xpq9a3lpwysd6zh6abidkb22byz9w7l05i4dmxw1vwwygbzzlim")))

(define-public crate-disruptor-0.7.1 (c (n "disruptor") (v "0.7.1") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1m28msgicmrr75vak86z2f47s1y2lfqlwhl6p9razlyv3fnr42b4")))

(define-public crate-disruptor-0.7.2 (c (n "disruptor") (v "0.7.2") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "053jy3a0m937nn1j0gxagyj7kmfp07f5qgi5z76x8rzsjnhnkhvd")))

(define-public crate-disruptor-1.0.0 (c (n "disruptor") (v "1.0.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1kbdgmzaiq08a0fyzbm1p7hamvvir5qskwjg7b2v4kyfncch6v52")))

(define-public crate-disruptor-1.0.1 (c (n "disruptor") (v "1.0.1") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0773883y1c8sqsnm8fhbb56j431rfzcs4w70llvyfv7a0dnx6gjk")))

(define-public crate-disruptor-1.0.2 (c (n "disruptor") (v "1.0.2") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "0p61qqi2risnnz69cqw7mbkz6k9jnzk5q4dh185sq0yrb26cyc4v")))

(define-public crate-disruptor-1.1.0 (c (n "disruptor") (v "1.1.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1bam56fx2cy8iw790a3w3142k4b65wh1s6khy8crsmdp9kq90zng")))

(define-public crate-disruptor-1.2.0 (c (n "disruptor") (v "1.2.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "04plz79qhdxp28fznkn4kn1zbxh7cj58pl3gp2zs8x01l23gb7l0")))

(define-public crate-disruptor-2.0.0 (c (n "disruptor") (v "2.0.0") (d (list (d (n "core_affinity") (r "^0.8.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1x61qz3rwp9p952fpmmv9sppifx987hw57gdqf62xy5ayrfl1q9i")))

