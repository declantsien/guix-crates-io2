(define-module (crates-io di sr disruption_gateway) #:use-module (crates-io))

(define-public crate-disruption_gateway-0.1.0 (c (n "disruption_gateway") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.9.0") (d #t) (k 0)) (d (n "disruption_types") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.20.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1hnnk71asp8rzhfqvf35zdmh928bbh3qb638kq83zl3pw7vayqar")))

