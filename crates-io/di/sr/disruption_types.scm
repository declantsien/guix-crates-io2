(define-module (crates-io di sr disruption_types) #:use-module (crates-io))

(define-public crate-disruption_types-0.1.0 (c (n "disruption_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.16") (d #t) (k 0)))) (h "1l4jrgjngki0sm5pav2hdbd5h10p7g7bswjqcj6nhj2c1mg7srfq")))

