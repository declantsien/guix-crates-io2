(define-module (crates-io di e- die-exit-2) #:use-module (crates-io))

(define-public crate-die-exit-2-0.4.0 (c (n "die-exit-2") (v "0.4.0") (h "0z4b8bn0224ww5zf8px2041g85f8nfg9zz1fbsbg8kkr9kbc76iy") (f (quote (("test"))))))

(define-public crate-die-exit-2-0.4.1 (c (n "die-exit-2") (v "0.4.1") (h "1crbiy5rn3l3nvpymavjpshdv40y1raka7i94nd61crw6x8f5xr4") (f (quote (("test"))))))

