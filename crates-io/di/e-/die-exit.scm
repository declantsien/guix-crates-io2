(define-module (crates-io di e- die-exit) #:use-module (crates-io))

(define-public crate-die-exit-0.3.0 (c (n "die-exit") (v "0.3.0") (h "090f2jvzw40k16b3pkijqqiwdr1bsp7fjj0x079hj8llclymlvjb") (f (quote (("test")))) (y #t)))

(define-public crate-die-exit-0.3.1 (c (n "die-exit") (v "0.3.1") (h "1rv4xawrr84rf42lm97vj7c2cnaqnbnf8r7b183clzam7g0a9i3a") (f (quote (("test")))) (y #t)))

(define-public crate-die-exit-0.3.2 (c (n "die-exit") (v "0.3.2") (h "041m812cw8c1yvqgvxgmyxgl64k2vrgm78a2gjz095wvdqrni5g8") (f (quote (("test")))) (y #t)))

(define-public crate-die-exit-0.3.3 (c (n "die-exit") (v "0.3.3") (h "12spip9d318rmf3v9w5p6jy1d81lqiy45bificc2zvi1wyy358vk") (f (quote (("test"))))))

(define-public crate-die-exit-0.4.0 (c (n "die-exit") (v "0.4.0") (h "0jcz97kdzsshg451v30zdwb8kl157g8yn2n9y4z7nrgwa50xxirq") (f (quote (("test"))))))

(define-public crate-die-exit-0.5.0 (c (n "die-exit") (v "0.5.0") (h "19ysgx4ca57qmy9099fnhr9q67gpqywbrpdy3ib8g6v7jidqki7f") (f (quote (("test") ("red"))))))

