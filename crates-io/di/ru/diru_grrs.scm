(define-module (crates-io di ru diru_grrs) #:use-module (crates-io))

(define-public crate-diru_grrs-0.1.0 (c (n "diru_grrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1hc6n3gzcgqvmqb75wdwnh8ci56wx5h35kaf0ll39i9la4fmg0jm")))

(define-public crate-diru_grrs-0.1.1 (c (n "diru_grrs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "138ngnkbmj8a45ik7snxsk0apwvpfs1nhf91gaqg9yy25f6xg1mb")))

