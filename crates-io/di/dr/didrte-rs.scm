(define-module (crates-io di dr didrte-rs) #:use-module (crates-io))

(define-public crate-didrte-rs-0.1.0 (c (n "didrte-rs") (v "0.1.0") (h "0fy6q324g57m0cncgnws20b12mwrvr9pf1p2kcddysf1glx916c9") (y #t)))

(define-public crate-didrte-rs-0.1.1 (c (n "didrte-rs") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1sslamwcz54r2jrxzqknn9xvi3lyvk4qdfc24fnlqyp1vmw23p34") (y #t)))

