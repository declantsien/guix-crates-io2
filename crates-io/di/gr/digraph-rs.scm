(define-module (crates-io di gr digraph-rs) #:use-module (crates-io))

(define-public crate-digraph-rs-0.1.0 (c (n "digraph-rs") (v "0.1.0") (d (list (d (n "graphviz-rust") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cdrfmzvj8nqj5ih53q85b7ld0kjkxjfdbcwzvv1hi7wsj0xhp7c")))

