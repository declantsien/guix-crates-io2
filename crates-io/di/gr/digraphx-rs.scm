(define-module (crates-io di gr digraphx-rs) #:use-module (crates-io))

(define-public crate-digraphx-rs-0.1.0 (c (n "digraphx-rs") (v "0.1.0") (d (list (d (n "num") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.109") (d #t) (k 0)))) (h "0hppf4ad5ls82x8426bh5080lf4j46js3pjkpz2qrwk3sjrd455d")))

