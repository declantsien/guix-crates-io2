(define-module (crates-io di d_ did_url) #:use-module (crates-io))

(define-public crate-did_url-0.1.0 (c (n "did_url") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.0") (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "01bwfvzmfqf6wxc2grk8d6vrc0li9hzfzd0acnskngj79qrzdmbh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

