(define-module (crates-io di d_ did_url_parser) #:use-module (crates-io))

(define-public crate-did_url_parser-0.2.0 (c (n "did_url_parser") (v "0.2.0") (d (list (d (n "form_urlencoded") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "0fg4xf3ypif81lg4ms94ngi4sssiv94nkjhyqlcrm0ig6i8s15c0") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

