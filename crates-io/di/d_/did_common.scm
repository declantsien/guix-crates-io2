(define-module (crates-io di d_ did_common) #:use-module (crates-io))

(define-public crate-did_common-0.1.0 (c (n "did_common") (v "0.1.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "nom") (r "^5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1apyd0s4a22n5gjlfzyp774ksicjhjsrscghsds6wx7g1billsvm")))

(define-public crate-did_common-0.2.0 (c (n "did_common") (v "0.2.0") (d (list (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "nom") (r "^5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15khbgzif5c6h2xg3q4xgaj8c8x29zml96ghi3r4qmzwx54vd21v")))

(define-public crate-did_common-0.3.0 (c (n "did_common") (v "0.3.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "nom") (r "^5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "regex") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0vppysr07215711nb05zjzsawvwpa1w7zmqh8ivyxkpg5x2jv329") (f (quote (("std") ("default" "std"))))))

