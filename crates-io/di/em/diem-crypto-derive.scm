(define-module (crates-io di em diem-crypto-derive) #:use-module (crates-io))

(define-public crate-diem-crypto-derive-0.0.0 (c (n "diem-crypto-derive") (v "0.0.0") (h "0ljniljp3pyybxa181pwbw6vh2nqr881x6q6i8rrplr8bk1357rc")))

(define-public crate-diem-crypto-derive-0.0.1 (c (n "diem-crypto-derive") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rfm01fnqnj5iklv1xhhw68viv6lx6whk026pn73zhxn4hispkdl")))

(define-public crate-diem-crypto-derive-0.0.2 (c (n "diem-crypto-derive") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1401590pffpiiwqzp3jf0kfhddxsfkwnmigppq3yv8rppgngk39z")))

(define-public crate-diem-crypto-derive-0.0.3 (c (n "diem-crypto-derive") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l82vxksbyyajdlnqxp849yz01apwf7qgqgvg6anmyyrqxj1ji84")))

