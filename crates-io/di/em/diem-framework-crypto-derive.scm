(define-module (crates-io di em diem-framework-crypto-derive) #:use-module (crates-io))

(define-public crate-diem-framework-crypto-derive-0.1.2 (c (n "diem-framework-crypto-derive") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z0ah47h5yxy8z0apwzid4a87di047sqwb0h2ziy01f6h1mf2f9m") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.1.4 (c (n "diem-framework-crypto-derive") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n2ynhsywpylswjny5c46kkawi07q27fn4z5wrlx43qdkj6yzbdp") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.1.5 (c (n "diem-framework-crypto-derive") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1an1k5nrx9lzzl6jzmgbjk3rl56qrx6hikcx730k52ild8yak11r") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.1.6 (c (n "diem-framework-crypto-derive") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ggn8j2n48wkkv1ck47qzm6j25m706763fkfwhn7g73vlblirxb5") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.2.0 (c (n "diem-framework-crypto-derive") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kzhi4kbzj2fd09bb6q65vr0i71s5fj19hp655bhqirl6hgck6wz") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.2.1 (c (n "diem-framework-crypto-derive") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z7915azykdsl83zmzw5mf6h08l8p8mbsrp6r88z8zlv9ky2b41b") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.3.0 (c (n "diem-framework-crypto-derive") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bpxxbb3bdvxj3j19gm4l09vg7fy46bm5v5aaw826aj5k8h5mxi8") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.3.1 (c (n "diem-framework-crypto-derive") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cr93ja6bp35azb4gh6x8sd6z68a9sgklgvwsmsag28lsrk573wb") (y #t)))

(define-public crate-diem-framework-crypto-derive-0.3.2 (c (n "diem-framework-crypto-derive") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m3iks7gkjdd1rp65gqy0k06nnzamygj89s2j4vgj5396xc66cdp") (y #t)))

