(define-module (crates-io di em diem-sdk) #:use-module (crates-io))

(define-public crate-diem-sdk-0.0.0 (c (n "diem-sdk") (v "0.0.0") (h "130lxjjf9bylnwv0jmy15kd73ssnibxvs2wqrg1bpvgmns8v8xli")))

(define-public crate-diem-sdk-0.0.1 (c (n "diem-sdk") (v "0.0.1") (d (list (d (n "diem-client") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "diem-crypto") (r "^0.0.1") (d #t) (k 0)) (d (n "diem-transaction-builder") (r "^0.0.1") (d #t) (k 0)) (d (n "diem-types") (r "^0.0.1") (d #t) (k 0)) (d (n "move-core-types") (r "^0.0.1") (d #t) (k 0)))) (h "1pja89rihpj7z786idzd3wnigdhs6h4hkjc87r8jk8j0yaisraz6") (f (quote (("default" "client") ("client" "diem-client"))))))

(define-public crate-diem-sdk-0.0.2 (c (n "diem-sdk") (v "0.0.2") (d (list (d (n "bcs") (r "^0.1") (d #t) (k 0)) (d (n "diem-client") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "diem-crypto") (r "^0.0.2") (d #t) (k 0)) (d (n "diem-transaction-builder") (r "^0.0.2") (d #t) (k 0)) (d (n "diem-types") (r "^0.0.2") (d #t) (k 0)) (d (n "move-core-types") (r "^0.0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pn5w4rzi4isg4rfywlf26g36mrn6d1j37z08zgax4wj6f98zayg") (f (quote (("default" "client") ("client" "diem-client"))))))

(define-public crate-diem-sdk-0.0.3 (c (n "diem-sdk") (v "0.0.3") (d (list (d (n "bcs") (r "^0.1") (d #t) (k 0)) (d (n "diem-client") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "diem-crypto") (r "^0.0.3") (d #t) (k 0)) (d (n "diem-transaction-builder") (r "^0.0.3") (d #t) (k 0)) (d (n "diem-types") (r "^0.0.3") (d #t) (k 0)) (d (n "move-core-types") (r "^0.0.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gh1fpn4869rnxyddi7sh1wlhqm7cpl6axwjcxqkg6lvk7x24vwl") (f (quote (("default" "client") ("client" "diem-client"))))))

