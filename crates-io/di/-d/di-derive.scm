(define-module (crates-io di -d di-derive) #:use-module (crates-io))

(define-public crate-di-derive-0.1.0 (c (n "di-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.20") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l9v4jll1jdgl0jrch6b5gb4jck22brcydjha4990xi9qknnp6y1") (y #t)))

