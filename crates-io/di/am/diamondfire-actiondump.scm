(define-module (crates-io di am diamondfire-actiondump) #:use-module (crates-io))

(define-public crate-diamondfire-actiondump-0.1.0 (c (n "diamondfire-actiondump") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lmafz9cdyrl5rlyb8qw4rl0qlidb5vi1r9z34js6hb6lm6szfpc")))

(define-public crate-diamondfire-actiondump-0.2.0 (c (n "diamondfire-actiondump") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07gbb350c1aj21d4rp5sxdvy7cjfmq43djckh2nx7dyajv4sb5bz") (f (quote (("registry") ("default" "registry"))))))

(define-public crate-diamondfire-actiondump-0.2.1 (c (n "diamondfire-actiondump") (v "0.2.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jzrjp1dilssqfnnf1vlp1s8l0shrpv4qqsa07bsz7j2kfs7ib7d") (f (quote (("registry") ("default" "registry"))))))

