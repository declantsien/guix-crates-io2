(define-module (crates-io di la dilate) #:use-module (crates-io))

(define-public crate-dilate-0.3.0 (c (n "dilate") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 2)))) (h "0gzp6rb4d3jrzcd5hfbrzybs5p3m72xw2fnnkflm2kw73m9gklbp")))

(define-public crate-dilate-0.4.0 (c (n "dilate") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "054sccvqbpc1y9vby42dp4m4kjivk5n8c93mbkm9w8lykls3npqs")))

(define-public crate-dilate-0.5.0 (c (n "dilate") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0h8sm2sl20h0rqyr7p82jz3ynmfcw49d15pcvnk2xd6sqpx2bisp")))

(define-public crate-dilate-0.6.0 (c (n "dilate") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "12jli12gqwcnhn96hv29nyxdj1cfp5zgdqbz0kn96cb7wdgklhxw") (f (quote (("std"))))))

(define-public crate-dilate-0.6.1 (c (n "dilate") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "18jsijk4p0pqc256jf5yxjwx30xn2d25j7f7djdjj3nsss3j0201") (f (quote (("std"))))))

(define-public crate-dilate-0.6.2 (c (n "dilate") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "1dcs3aihr8ddfp22vvw7nly6b1g7nr74a40h3zsg74l90lhibb9q") (f (quote (("std"))))))

(define-public crate-dilate-0.6.3 (c (n "dilate") (v "0.6.3") (d (list (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "1w1lgz4dvjhgf4g4xymfzm8pprj289j7yqw6galljliiawbmnvkj") (f (quote (("std")))) (r "1.70")))

