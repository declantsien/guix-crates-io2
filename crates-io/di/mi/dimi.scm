(define-module (crates-io di mi dimi) #:use-module (crates-io))

(define-public crate-dimi-0.1.0 (c (n "dimi") (v "0.1.0") (d (list (d (n "flume") (r "^0.10") (f (quote ("async"))) (k 0)) (d (n "lookit") (r "^0.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "pasts") (r "^0.8") (d #t) (k 2)) (d (n "smelling_salts") (r "^0.5") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1qf54cdif1pp0qn0frgc7rnd6ffgb2l73n1f19zrpdsr2qlyd382")))

