(define-module (crates-io di va divans) #:use-module (crates-io))

(define-public crate-divans-0.0.1 (c (n "divans") (v "0.0.1") (d (list (d (n "alloc-no-stdlib") (r "~1.3") (d #t) (k 0)) (d (n "brotli") (r "~2.5") (d #t) (k 0)) (d (n "vergen") (r "^0.1") (d #t) (k 1)))) (h "1szcpmwrdy9fglainqcagg84r7wbm0ijlzk5mxcag75hpcca78ik") (f (quote (("uncached_frequentist") ("threadlog") ("simd") ("safe") ("portable-simd") ("no-stdlib-rust-binding") ("no-stdlib" "alloc-no-stdlib/no-stdlib" "brotli/no-stdlib") ("no-inline") ("findspeed") ("external-literal-probability" "brotli/external-literal-probability") ("debug_entropy") ("blend") ("billing") ("benchmark" "brotli/benchmark") ("avx2") ("avoid-divide"))))))

