(define-module (crates-io di va divan-macros) #:use-module (crates-io))

(define-public crate-divan-macros-0.0.0 (c (n "divan-macros") (v "0.0.0") (d (list (d (n "divan") (r "^0.0.0") (d #t) (k 2)))) (h "0jwki2jr67pc8dvg4g9v5j0qi3m59v1yb5iw5p7d4z076s2r6chg")))

(define-public crate-divan-macros-0.1.0 (c (n "divan-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0v1jl44v7n4xn69y1c3311b7r55d872d5x6mj1yxk5q839k2zqdn")))

(define-public crate-divan-macros-0.1.1 (c (n "divan-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1k7k8rfc7b1vnr83fifpgdypn6srzdgh9jlipibcmjyk4mjnah9w")))

(define-public crate-divan-macros-0.1.2 (c (n "divan-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1d4x05hjc5k0gl45r0y7x8a5lx7y8zy1924l392mq2qyama66p8w")))

(define-public crate-divan-macros-0.1.3 (c (n "divan-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1x82r6q64qslan29p0pzf8anwmkvrkzm8ghi8n6ixbwg51d70sh1")))

(define-public crate-divan-macros-0.1.4 (c (n "divan-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0mnap6k640kmkmzw7m64zy70y0152qkf7hrjr8m6qw7mvvkcc0ny")))

(define-public crate-divan-macros-0.1.5 (c (n "divan-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "074bwlnb9j7k81na9cbiamibf09wd61q0xhwgapk42xab15vhpw7")))

(define-public crate-divan-macros-0.1.6 (c (n "divan-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1qnr1mn3s43si33j88lx0258bhgx2y035ajvrx0f582mm448wb8c")))

(define-public crate-divan-macros-0.1.7 (c (n "divan-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "13hgvzcpir972nvrxb41sxpj3qwp4rfxksxay541iylk1dn9qrnl")))

(define-public crate-divan-macros-0.1.8 (c (n "divan-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1h6xg0snsi08jma5990kbv9zd8r6glwiwxhk0bnch6g1cf9gc3ji")))

(define-public crate-divan-macros-0.1.9 (c (n "divan-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1vfhgwl6bgkm1igpv5vvmjq8vpvqqmabbr15anzzd8ymz6g4npp5")))

(define-public crate-divan-macros-0.1.10 (c (n "divan-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0kcikj7v883sk1yfkiakk7w7y93s11wssbg0g6fkvm43l2j06qm0")))

(define-public crate-divan-macros-0.1.11 (c (n "divan-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1i1y9f7r8yxfvshd2ypx9vzk9j845kl1lwsjapl02fjnndpgd4jh")))

(define-public crate-divan-macros-0.1.12 (c (n "divan-macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "12xas7j49fswdj13xx9rh11ra198x57hwafny7nb1aql6njyd316")))

(define-public crate-divan-macros-0.1.13 (c (n "divan-macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0ardj9pqajw7smwlf8nzl3apsvqggqmwwa4g1f3dc7afa0xig8pz")))

(define-public crate-divan-macros-0.1.14 (c (n "divan-macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full" "clone-impls" "parsing" "printing" "proc-macro"))) (k 0)))) (h "18klwl81akqmd0rqgz6l8hmc649hvbcdfc01ix6lh3dy96phnm17")))

