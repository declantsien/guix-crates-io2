(define-module (crates-io di va diva) #:use-module (crates-io))

(define-public crate-diva-0.0.0 (c (n "diva") (v "0.0.0") (h "1x82p4l1ymap6sxl6r9l3w6nkiivsakpra00b89s01svmxxpr0fs")))

(define-public crate-diva-0.1.0 (c (n "diva") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01hbl8364r3inzf02n1rkfkvsxikfw209k973c454npjfyfv2qj9")))

