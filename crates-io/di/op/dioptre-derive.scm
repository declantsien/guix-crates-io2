(define-module (crates-io di op dioptre-derive) #:use-module (crates-io))

(define-public crate-dioptre-derive-0.1.0 (c (n "dioptre-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x755kkhsip7g1gww5gippw7piwzbhvi5w12rkdh0p6wdw9k73pa")))

