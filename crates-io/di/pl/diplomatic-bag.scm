(define-module (crates-io di pl diplomatic-bag) #:use-module (crates-io))

(define-public crate-diplomatic-bag-0.1.0 (c (n "diplomatic-bag") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0aslar926hzaxbsh9c3y17aiq3k28fq9xc0hihiaph3rgqncyax0")))

(define-public crate-diplomatic-bag-0.1.1 (c (n "diplomatic-bag") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "197gbih1ibh1wviaf5hi8g8xkabn5ls6p4vqdcv9h8xpjypdma01")))

(define-public crate-diplomatic-bag-0.2.0 (c (n "diplomatic-bag") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ayin22fp3qd4ij874rg67d5zp0krx3z8ig54ybdvxhnav7ssj34")))

(define-public crate-diplomatic-bag-0.3.0 (c (n "diplomatic-bag") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1yqv40pa2dnna0v74kylwdb0j69sg5s7pd9qraakf1d5gaj7gsc1")))

(define-public crate-diplomatic-bag-0.3.1 (c (n "diplomatic-bag") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "static_assertions") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "01bgv9dr9wwdpfcr2zbm2f3q8z4nfgi7ajnclslihcdgipcnas1z")))

