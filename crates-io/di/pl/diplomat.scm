(define-module (crates-io di pl diplomat) #:use-module (crates-io))

(define-public crate-diplomat-0.1.0 (c (n "diplomat") (v "0.1.0") (h "0dr83ml5ff9zn4i1466gk2p6y0ws42a71j7i8ms2075sxd0vhcs3")))

(define-public crate-diplomat-0.2.0 (c (n "diplomat") (v "0.2.0") (d (list (d (n "diplomat_core") (r "^0.2.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hr9zkpylbl9vsxvyyhavhgjl5m031bwl171lkssgwrxrqjxnc73")))

(define-public crate-diplomat-0.3.0 (c (n "diplomat") (v "0.3.0") (d (list (d (n "diplomat_core") (r "^0.3.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1wvnq4p3wsp88ik571j54vf1sssjadykw94cv6rv0wl38bixf9xq")))

(define-public crate-diplomat-0.4.0 (c (n "diplomat") (v "0.4.0") (d (list (d (n "diplomat_core") (r "^0.4.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "05fr60zrv7prk7fanqr94pn96s8dbqpbmz3imy76zli3ww1qdlms")))

(define-public crate-diplomat-0.4.1 (c (n "diplomat") (v "0.4.1") (d (list (d (n "diplomat_core") (r "^0.4.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1nz0bij508sp68pqai6iv846lsj8kc618j0if9057kykywpks2fx")))

(define-public crate-diplomat-0.4.2 (c (n "diplomat") (v "0.4.2") (d (list (d (n "diplomat_core") (r "^0.4.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1zciz8vg3hy8p633s5ra24nqyhm0z205z6nq4pk6gjkqnnn9kd02")))

(define-public crate-diplomat-0.5.0 (c (n "diplomat") (v "0.5.0") (d (list (d (n "diplomat_core") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hh7ypil280c4ldi9iwpkf3730pjdza2sq3z7qggqfv2lc4rlp3i")))

(define-public crate-diplomat-0.5.1 (c (n "diplomat") (v "0.5.1") (d (list (d (n "diplomat_core") (r "^0.5.1") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "14bljmx8720qmk4lq7zxr2pd1fl5kj0zwjjr3igin574d3iyms9k")))

(define-public crate-diplomat-0.5.2 (c (n "diplomat") (v "0.5.2") (d (list (d (n "diplomat_core") (r "^0.5.2") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0d6ph4msgi9a8gj7ybnb1a50xgqp20133c989dw4hdnbfj27q6hk")))

(define-public crate-diplomat-0.6.0 (c (n "diplomat") (v "0.6.0") (d (list (d (n "diplomat_core") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "171f3vz4jdpq36nnzjsqjy7l98bmddzp1205s6pmq39kkikdaay9")))

(define-public crate-diplomat-0.7.0 (c (n "diplomat") (v "0.7.0") (d (list (d (n "diplomat_core") (r "^0.7.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1wn5p2klkgpn4n13qzg7c8wz3j2sk9x6ywlq7in8diy3xfrp45m3")))

(define-public crate-diplomat-0.8.0 (c (n "diplomat") (v "0.8.0") (d (list (d (n "diplomat_core") (r "^0.8.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1hpgsfjxzyv4n7k7xpsvr7c8v24gjif99yd7zkdr3i5ss90ccdri")))

