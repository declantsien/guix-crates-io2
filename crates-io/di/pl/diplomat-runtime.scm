(define-module (crates-io di pl diplomat-runtime) #:use-module (crates-io))

(define-public crate-diplomat-runtime-0.2.0 (c (n "diplomat-runtime") (v "0.2.0") (h "0jgnyd64nppan0l5vh5xb5sibdvw2k078ww63qsfsisy12gnmz6x")))

(define-public crate-diplomat-runtime-0.3.0 (c (n "diplomat-runtime") (v "0.3.0") (h "1x3h05zi2zizcyd4x7rqsrl1mlavxk7dgw7ibzcj5gj9ndaahnz5")))

(define-public crate-diplomat-runtime-0.4.0 (c (n "diplomat-runtime") (v "0.4.0") (h "0qf8da9np1bk3n9ja3bjbp38523fb3rqqwmyvw5a4nmwzd67c9dc")))

(define-public crate-diplomat-runtime-0.4.1 (c (n "diplomat-runtime") (v "0.4.1") (h "13pw20l01gbjh3flyyxrg6dpv68angkhyvrb7hrqahkvzdywpx09")))

(define-public crate-diplomat-runtime-0.4.2 (c (n "diplomat-runtime") (v "0.4.2") (h "1mslp3dpcdddq7gk80vm0lvh5fkkh072cp5l6ryllnshzf6waxay")))

(define-public crate-diplomat-runtime-0.5.0 (c (n "diplomat-runtime") (v "0.5.0") (h "0pmdm0s0vq8zg1zhmfrqlnl37zn2j9cs6sfy2i0laz23q5jn2hvj")))

(define-public crate-diplomat-runtime-0.5.1 (c (n "diplomat-runtime") (v "0.5.1") (h "1kiw3r05ni93zq4iv1ndnb6dipdmbccg79108aib30l6x701qh4f")))

(define-public crate-diplomat-runtime-0.5.2 (c (n "diplomat-runtime") (v "0.5.2") (h "0npawyzaswfc0lnx03zlc8yrg8z7n6kg3rj06x35xxllsyi8vrvv")))

(define-public crate-diplomat-runtime-0.6.0 (c (n "diplomat-runtime") (v "0.6.0") (h "0ymykkh8r94291g54nfj6mg4w7ibg24h5fgxahjnpskzirni3h6h")))

(define-public crate-diplomat-runtime-0.7.0 (c (n "diplomat-runtime") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0g6g6jb3kc3axkyiz7wmcbbx1kk49ka0qjg54q758ilsahyz5c7p")))

(define-public crate-diplomat-runtime-0.8.0 (c (n "diplomat-runtime") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1agrrpl2y6zbzii8w68wqc2z052myhnyzci4vhx9kg55ypbvw92i")))

