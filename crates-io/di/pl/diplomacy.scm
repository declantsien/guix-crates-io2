(define-module (crates-io di pl diplomacy) #:use-module (crates-io))

(define-public crate-diplomacy-0.1.0 (c (n "diplomacy") (v "0.1.0") (d (list (d (n "from_variants") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c5hd0kgk4cndrs721s0fxxxr4iacwcp0nb65n3z6cqldjxligkl") (f (quote (("dependency-graph"))))))

(define-public crate-diplomacy-0.1.1 (c (n "diplomacy") (v "0.1.1") (d (list (d (n "from_variants") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0masv4p37mgzss374xbz69qyk8yy4jhghhgfjh5wm4i9xil0bvf2") (f (quote (("dependency-graph"))))))

(define-public crate-diplomacy-0.1.2 (c (n "diplomacy") (v "0.1.2") (d (list (d (n "from_variants") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a8y0mvqf9qdnqqp4d08p52c0pb8fyy3rsxkkdn7dlmgvgp5q25r") (f (quote (("dependency-graph"))))))

(define-public crate-diplomacy-0.1.3 (c (n "diplomacy") (v "0.1.3") (d (list (d (n "from_variants") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.112") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nlc145lya82ddq9brjypmzhhpzb15n1n927n3k7xfgi1676n19f") (f (quote (("dependency-graph"))))))

