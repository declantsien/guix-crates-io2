(define-module (crates-io di rm dirmod) #:use-module (crates-io))

(define-public crate-dirmod-0.1.0 (c (n "dirmod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15y9i3cghnyyb49ca5sziqzss990jy9q2x8d4ql5irc53b0v3ink")))

(define-public crate-dirmod-0.1.1 (c (n "dirmod") (v "0.1.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qgq494f0df61jilkma9zpdzyqfqwp7jlv8p05adgxm8nyhrwk8f")))

(define-public crate-dirmod-0.1.2 (c (n "dirmod") (v "0.1.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p1cc4czd6bgw5p6c90fk118ny87qssvh6al0cmcjfg328zg4nj1")))

(define-public crate-dirmod-0.1.3 (c (n "dirmod") (v "0.1.3") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11y37d0521zw4pj8pab09ywcmjz1y20aa18h9lj9ik8z5dy4chfp")))

(define-public crate-dirmod-0.1.4 (c (n "dirmod") (v "0.1.4") (d (list (d (n "dirmod-codegen") (r "^0.1.4") (d #t) (k 0)))) (h "0m2mcp12wafp0rcrpdjgmsqbqbm3rlh1hpl67985yc8110l86j95")))

(define-public crate-dirmod-0.1.5 (c (n "dirmod") (v "0.1.5") (d (list (d (n "dirmod-codegen") (r "^0.1.4") (d #t) (k 0)))) (h "0mvfmd7j0f98njlqy3xm8np4xvvv5q3wk7h8nkph0a2p06qa8i1x")))

(define-public crate-dirmod-0.2.0-alpha.1 (c (n "dirmod") (v "0.2.0-alpha.1") (d (list (d (n "dirmod-codegen") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "13vy441llxsx5sydibqjrswpd90hk4p3xppicda8p3dr3i0qsway")))

