(define-module (crates-io di rm dirmod-codegen) #:use-module (crates-io))

(define-public crate-dirmod-codegen-0.1.4 (c (n "dirmod-codegen") (v "0.1.4") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s9gr6vyc6rx0rg6gggdwg5lzflxmbahsp2dy16gzkldxrw9w65n")))

(define-public crate-dirmod-codegen-0.2.0-alpha.1 (c (n "dirmod-codegen") (v "0.2.0-alpha.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "156c8nk90xcvhp9vsldqawjg2wjx6x1ckbi0wq8fv8zax5m3kzmi")))

