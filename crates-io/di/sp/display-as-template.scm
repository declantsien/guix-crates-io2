(define-module (crates-io di sp display-as-template) #:use-module (crates-io))

(define-public crate-display-as-template-0.1.0 (c (n "display-as-template") (v "0.1.0") (d (list (d (n "combine") (r "^3.6.2") (d #t) (k 0)) (d (n "display-as") (r "^0.1.3") (d #t) (k 0)) (d (n "display-as-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "047x21m4vyqayh03z34xwxd9g08ssbycmn6kn8p88izlgqnxl1s5") (y #t)))

(define-public crate-display-as-template-0.1.1 (c (n "display-as-template") (v "0.1.1") (d (list (d (n "combine") (r "^3.6.2") (d #t) (k 0)) (d (n "display-as") (r "^0.1.4") (d #t) (k 0)) (d (n "display-as-proc-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1zmkvfikz9wcwagmlrgcv22rwgzjbhnj8gsq94h4vvpxsrlkxm3s") (y #t)))

