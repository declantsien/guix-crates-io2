(define-module (crates-io di sp display-error-chain) #:use-module (crates-io))

(define-public crate-display-error-chain-0.1.0 (c (n "display-error-chain") (v "0.1.0") (h "1nhwzwjy8zynvzrf8b3k2rcpa2qzwqcg683rk7jnjkkiy12lm979")))

(define-public crate-display-error-chain-0.1.1 (c (n "display-error-chain") (v "0.1.1") (h "0j5q2wqjchr7vr5842x6bl6yl0nisakgmw2fm2wyw9f1n938c6ky")))

(define-public crate-display-error-chain-0.2.0 (c (n "display-error-chain") (v "0.2.0") (h "1ffvbx1ix5ra5p7v76vspmpw7q2vn2l4aighylsaphbqapkzjypp")))

