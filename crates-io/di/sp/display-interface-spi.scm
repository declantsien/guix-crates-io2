(define-module (crates-io di sp display-interface-spi) #:use-module (crates-io))

(define-public crate-display-interface-spi-0.1.0 (c (n "display-interface-spi") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1k5ajkymhyq95ja2wydn4dj3pi3jwyajf3hm5drx9191w9qny97f")))

(define-public crate-display-interface-spi-0.2.0 (c (n "display-interface-spi") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1m5cr8bazy6v087bmhfc9ajwg023gicczzv4rq9j6pr54acxxlk3")))

(define-public crate-display-interface-spi-0.3.0 (c (n "display-interface-spi") (v "0.3.0") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1zidn7nm029sgjhnk144lxc1nvj97qiz47j8lii39z36m27dn0fl")))

(define-public crate-display-interface-spi-0.3.1 (c (n "display-interface-spi") (v "0.3.1") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1s1k3lqap8kq3cgbarjvpvsbps0fbmyk8a7zpc3a14pxgc7q9r43")))

(define-public crate-display-interface-spi-0.4.0 (c (n "display-interface-spi") (v "0.4.0") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1c1q3g1ppk5km7vrnn8ndjj4p0x166d01zmlrg5ndcc22qx8rl4l")))

(define-public crate-display-interface-spi-0.4.1 (c (n "display-interface-spi") (v "0.4.1") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "19g183w37b0k5bddrsig3syy9jswb0fza28zpmpi8qj80nnpi4s8")))

(define-public crate-display-interface-spi-0.5.0 (c (n "display-interface-spi") (v "0.5.0") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (k 0)) (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "0ykbvi4kf2kcs5wih05h3fr9lhbz04ycr3q3l9frbca8031rwszq") (r "1.75")))

