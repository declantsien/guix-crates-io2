(define-module (crates-io di sp dispatchers) #:use-module (crates-io))

(define-public crate-dispatchers-0.1.0 (c (n "dispatchers") (v "0.1.0") (h "075znf0c2d0z5gap83n7ws8pki78xjcbikfs90jdnq8vmwmhkvqr")))

(define-public crate-dispatchers-0.2.0 (c (n "dispatchers") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "07q6vbk6050a8jrbwssbixbryja1d5zk5jxgyfzyxyg516gpb8yg") (f (quote (("shared" "async-trait" "tokio"))))))

(define-public crate-dispatchers-0.2.1 (c (n "dispatchers") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.64") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "00gkflh1p33bgmrljzla19vbawgkcrjlzb0qyxr4dfhdcqpjja12") (f (quote (("shared" "async-trait" "tokio"))))))

(define-public crate-dispatchers-0.2.2 (c (n "dispatchers") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1nkh9xmm45al836llvj6hk5brrkbvdyq8c913xq8gdqqrpzgcs1n") (f (quote (("shared" "tokio"))))))

(define-public crate-dispatchers-0.2.3 (c (n "dispatchers") (v "0.2.3") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1mjqg8d7ryhb2p0ba8avcgn7c2flwvg5xbbh5ac99aqd7v1dg7c6") (f (quote (("shared" "tokio"))))))

(define-public crate-dispatchers-0.2.4 (c (n "dispatchers") (v "0.2.4") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10p07ni6vvd1pkxpcq9r07b3jbb1n164gkdyf63faqsgzcavqwxf") (f (quote (("shared" "tokio"))))))

(define-public crate-dispatchers-0.2.5 (c (n "dispatchers") (v "0.2.5") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1l4b2hb89g3f53d6lgajs6hnfwh1p9iz01rbhayy843dp486wrwf") (f (quote (("shared" "tokio"))))))

(define-public crate-dispatchers-0.3.0 (c (n "dispatchers") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1c58wg9g5m2scfz5rjg1050i8zb9l0v6y2lv154mpgr7kwrmr1ia") (f (quote (("shared" "tokio"))))))

