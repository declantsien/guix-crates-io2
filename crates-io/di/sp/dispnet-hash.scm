(define-module (crates-io di sp dispnet-hash) #:use-module (crates-io))

(define-public crate-dispnet-hash-0.1.0 (c (n "dispnet-hash") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15daji1hvmrlyp6ymvbfna637aq4xznmx0ajkbqssjmklpnl9743")))

(define-public crate-dispnet-hash-0.2.0 (c (n "dispnet-hash") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)))) (h "1bplf5v3dvqkjj8n1fi2r4g5cik7494dhg7i42n2smarav2mw4xm")))

(define-public crate-dispnet-hash-0.3.0 (c (n "dispnet-hash") (v "0.3.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)))) (h "0phwx4bwz97dczi4b0q52h5n2hj0mf1p5y42zq95cpb3az33fa3m")))

(define-public crate-dispnet-hash-0.4.0 (c (n "dispnet-hash") (v "0.4.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)))) (h "01h8jjybhn09gn1m8lfjrhc59c1z98rsf12dfnfgnlhazap50z81")))

