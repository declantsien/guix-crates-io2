(define-module (crates-io di sp display_adapter) #:use-module (crates-io))

(define-public crate-display_adapter-0.1.0 (c (n "display_adapter") (v "0.1.0") (d (list (d (n "display_adapter_attr") (r "^0.1.1") (d #t) (k 0)))) (h "13ixrwp7pqfnnwvqai56gg36qi53j2798lvg3qkjy5q8p9q2x0s1")))

(define-public crate-display_adapter-0.1.1 (c (n "display_adapter") (v "0.1.1") (d (list (d (n "display_adapter_attr") (r "^0.1.1") (d #t) (k 0)))) (h "1z7w0g318sfnvgv5d8fmng76a1kvrsw2k5qwhizqc6jhwhnbm8jx")))

