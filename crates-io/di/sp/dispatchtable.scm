(define-module (crates-io di sp dispatchtable) #:use-module (crates-io))

(define-public crate-dispatchtable-0.1.0 (c (n "dispatchtable") (v "0.1.0") (h "10ckysfic4ci3rq5nlpdgf3gwfimv8vmf0fb4cq227575a6p5is1")))

(define-public crate-dispatchtable-0.1.1 (c (n "dispatchtable") (v "0.1.1") (h "1z7s54hlqlsipmc32p179ca73c79avzv8r0lsaadcjw1gj92r4pc")))

(define-public crate-dispatchtable-0.1.2 (c (n "dispatchtable") (v "0.1.2") (h "04yadsrjclkjkwljgc17zl12rh34yaq1swzg2314q60kvvzc87wl")))

(define-public crate-dispatchtable-0.1.3 (c (n "dispatchtable") (v "0.1.3") (h "0n9nhppw0vjgynasfyn5v0xbjvwyf0fdkdqkk527h9kg8xcdb6g3")))

(define-public crate-dispatchtable-0.1.4 (c (n "dispatchtable") (v "0.1.4") (h "0nish1bg49ff3m2a9wymmv70zj22wl1n4bnixslpdmcw7paasafa")))

(define-public crate-dispatchtable-0.2.0 (c (n "dispatchtable") (v "0.2.0") (h "094z8qamiz2sp7d7jj41ajm5ki7pa7744khgb7ljm9xd70p4hz8f")))

