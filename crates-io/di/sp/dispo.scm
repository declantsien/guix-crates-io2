(define-module (crates-io di sp dispo) #:use-module (crates-io))

(define-public crate-dispo-0.1.0 (c (n "dispo") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "email_address") (r "^0.2.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 1)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 1)))) (h "0f5svvzdna36qkb6a7mk9kkziqzgddmxi2kqxqxqvbd81q0w0zay")))

(define-public crate-dispo-0.1.1 (c (n "dispo") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "email_address") (r "^0.2.4") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 1)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 1)))) (h "1vl0nijjjjwhwlgx41rb64b119gj404qpq2j8pma0jc2h4sv7qqi")))

