(define-module (crates-io di sp display_json) #:use-module (crates-io))

(define-public crate-display_json-0.1.0 (c (n "display_json") (v "0.1.0") (d (list (d (n "display_json_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0wq5ln178fblilcs9ssimfijmrznwxifanw109bn572dq44p3dqb")))

(define-public crate-display_json-0.1.1 (c (n "display_json") (v "0.1.1") (d (list (d (n "display_json_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "16mpa064sm3hw3w6x2wp75a1yl76267115g07wpxzbjxka7d94y3")))

(define-public crate-display_json-0.1.2 (c (n "display_json") (v "0.1.2") (d (list (d (n "display_json_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1zs8jfs1xlmdqj6fzqjr21ih4g9g5a59j9wrmjs8c9kqk9wv63a9")))

(define-public crate-display_json-0.1.3 (c (n "display_json") (v "0.1.3") (d (list (d (n "display_json_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1wdxilxlwmc5hkvqjymn1gfjmnycpz7345wsag1xbh6yac9dxfb5")))

(define-public crate-display_json-0.2.0 (c (n "display_json") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "02pnqy8bb7ak3w24vdv6ca5wnvamklmck1rgnkmgnfzkbv8mwlc4")))

(define-public crate-display_json-0.2.1 (c (n "display_json") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "073snpcq6l15qkhib7w73bvw6gfvc2lnk6lj2pzabwh4a6z1k1kf")))

