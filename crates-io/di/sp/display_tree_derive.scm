(define-module (crates-io di sp display_tree_derive) #:use-module (crates-io))

(define-public crate-display_tree_derive-1.0.0 (c (n "display_tree_derive") (v "1.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "011i96lvznxc8m7zwpz47j3dnf5v48pdy8zm2njxk7zydqx6jd3w")))

(define-public crate-display_tree_derive-1.0.1 (c (n "display_tree_derive") (v "1.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "1zgkard7i9zw6897vdxc9ms88i6xs03n04636r4w1np5vvcjgrd9")))

(define-public crate-display_tree_derive-1.0.2 (c (n "display_tree_derive") (v "1.0.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "014xwjngv4l6cywhi8fjsln56221ds3wj7spraj315ph0hlnzxly")))

(define-public crate-display_tree_derive-1.0.3 (c (n "display_tree_derive") (v "1.0.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "1wjac70bg9wqgk0dq4m5vcvabbi5hn6gcnq8vvjz0cyrxp873m5l")))

