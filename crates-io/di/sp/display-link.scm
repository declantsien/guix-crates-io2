(define-module (crates-io di sp display-link) #:use-module (crates-io))

(define-public crate-display-link-0.1.0 (c (n "display-link") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "04q5drq6s2hv445aak3ma9aphdy9s2ckrr4pnycbini91b8ms1zf")))

(define-public crate-display-link-0.1.1 (c (n "display-link") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "02h1c20jhw36fjwiyj7k3fi16sxh6rswzxn0d4jcisc3d8yn6smx")))

(define-public crate-display-link-0.1.2 (c (n "display-link") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "002f0b19j6g3vfyzcfl1pilvjzaljh3ivzs0f1yxy70b3qq334na")))

(define-public crate-display-link-0.1.3 (c (n "display-link") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "0ywmyvzl27rhvqfrhb74b6mnl4750iqh599f19hq6l27z0vp8idh")))

(define-public crate-display-link-0.1.4 (c (n "display-link") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.5") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "1gdzrhqwrp7q7g6sidw7fb9i5blnw2zv0wqqnig52whlfbmh2nmd")))

(define-public crate-display-link-0.1.5 (c (n "display-link") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "0zjh1kksljbcj8kv691yxyrr5sy17iaallcypb9r13qwmgyp788f")))

(define-public crate-display-link-0.1.6 (c (n "display-link") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)))) (h "1n7b3khsal0zxw46h7rblgsma8xjf8dinkmlwfqa6h6csfikmnld")))

(define-public crate-display-link-0.1.7 (c (n "display-link") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "winit") (r "^0.20.0-alpha2") (o #t) (d #t) (k 0)))) (h "16y0nbpyji9gk0jyzhdvzhq5l084pri524jgx229705p79si05fi") (f (quote (("default"))))))

(define-public crate-display-link-0.2.0 (c (n "display-link") (v "0.2.0") (d (list (d (n "foreign-types") (r "^0.3.2") (d #t) (k 0)) (d (n "objc") (r "^0.2.6") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(any(target_os = \"ios\"))") (k 0)) (d (n "time-point") (r "^0.1.1") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha3") (o #t) (d #t) (k 0)))) (h "148l6fgnmwbvfq0qj1r1jpgb8h5x69cnyi93g3mq573g70rfcg9h") (f (quote (("default"))))))

