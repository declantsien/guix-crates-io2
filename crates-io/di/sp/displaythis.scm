(define-module (crates-io di sp displaythis) #:use-module (crates-io))

(define-public crate-displaythis-1.0.22 (c (n "displaythis") (v "1.0.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "displaythis-impl") (r "=1.0.22") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b7sdb5wc1n0sb1y4jpx9cpqh4clcc31242sxrkf5msxcriyclrd")))

(define-public crate-displaythis-1.0.23 (c (n "displaythis") (v "1.0.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "displaythis-impl") (r "=1.0.23") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0586ig29kdkf4hn2xplfkb46h758kdd9r1x3yglcgzxlwyiz17y7")))

