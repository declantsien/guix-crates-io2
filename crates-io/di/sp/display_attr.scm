(define-module (crates-io di sp display_attr) #:use-module (crates-io))

(define-public crate-display_attr-0.1.0 (c (n "display_attr") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ihhnxnc8vwcy9zzr5q9l7d8kzqdmljl8znl7lb8f7xjvz82z4lx")))

(define-public crate-display_attr-0.1.1 (c (n "display_attr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12hyjfcdvqz4hajh8790lvf2mi4spv16plrcxylx9gmlf5wr3axg")))

