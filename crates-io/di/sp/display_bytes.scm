(define-module (crates-io di sp display_bytes) #:use-module (crates-io))

(define-public crate-display_bytes-0.1.0 (c (n "display_bytes") (v "0.1.0") (h "1whyzwsx4xayd6hckc75vjxqx5rbrmazw70vnrkw3wkcjblcvz2h")))

(define-public crate-display_bytes-0.2.0 (c (n "display_bytes") (v "0.2.0") (h "0iprsh8ising348h9345mb4f9p7m4rzd3w507j50bar8dmficg50")))

(define-public crate-display_bytes-0.2.1 (c (n "display_bytes") (v "0.2.1") (h "00wq6a1jsacqii4fpaidm507gsahy7kjgb2w6k5a5j9bk4vkqczs")))

