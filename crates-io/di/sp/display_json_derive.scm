(define-module (crates-io di sp display_json_derive) #:use-module (crates-io))

(define-public crate-display_json_derive-0.1.0 (c (n "display_json_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0p5m2z81ixsll88vj9xr9j3lywlia95vh1mba74bri4570q1m7nh")))

(define-public crate-display_json_derive-0.1.1 (c (n "display_json_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "15xz4kbpqg45j4kpwym1j25c0ykayr23njn6y5llyd7v10s92hdc")))

(define-public crate-display_json_derive-0.1.2 (c (n "display_json_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits"))) (d #t) (k 0)))) (h "195cc7lhvhz47vl8rb1jb475nkrgciv3qzb4cvrmpqcvza2p0acb")))

(define-public crate-display_json_derive-0.1.3 (c (n "display_json_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("derive" "printing" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0zdqiqvi75mi4mivimjd8jri5w2ccm3vqy198iw43mmc8h4qqa3l")))

