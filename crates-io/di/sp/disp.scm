(define-module (crates-io di sp disp) #:use-module (crates-io))

(define-public crate-disp-0.1.0 (c (n "disp") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gavmj275si3yc7dh5cvci9w5nr989m3myw89gy9j3zpm2jambkl")))

(define-public crate-disp-0.1.1 (c (n "disp") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1p8hxzxn02jag90q9agqzvp1ypl970yyvyc82s5s0s568gazk1fs")))

