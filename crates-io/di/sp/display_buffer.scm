(define-module (crates-io di sp display_buffer) #:use-module (crates-io))

(define-public crate-display_buffer-0.1.0-rc.1 (c (n "display_buffer") (v "0.1.0-rc.1") (h "1q2c7bnb726m6aavhaziznmf7z1dq2aibnxaiksy5qskz181pzjg") (r "1.64")))

(define-public crate-display_buffer-0.1.0 (c (n "display_buffer") (v "0.1.0") (h "1ndjmrdjaac3aihylzrsy1mhr1nfs94180613nb78kmrfqhc1cpg") (r "1.64")))

