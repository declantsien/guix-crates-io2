(define-module (crates-io di sp displaydoc-lite-proc_macros) #:use-module (crates-io))

(define-public crate-displaydoc-lite-proc_macros-0.1.1 (c (n "displaydoc-lite-proc_macros") (v "0.1.1") (h "1pfiq9rkaq2kibbwk10y5fv5m009pcg6wqqcw9lm1lxjxw5im55v") (y #t)))

(define-public crate-displaydoc-lite-proc_macros-0.1.2 (c (n "displaydoc-lite-proc_macros") (v "0.1.2") (h "0nsaj7n5pz8wa88xgcshhskwmm62j99azk97hd1w78xdrifp6291")))

(define-public crate-displaydoc-lite-proc_macros-0.1.3 (c (n "displaydoc-lite-proc_macros") (v "0.1.3") (h "1fpd16m1kaqawp3n3kag5s4cl8w11lilja37nq81kdzp3xn56kdi")))

