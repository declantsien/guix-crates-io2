(define-module (crates-io di sp displaydoc-watt) #:use-module (crates-io))

(define-public crate-displaydoc-watt-0.1.0 (c (n "displaydoc-watt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.0") (d #t) (k 2)) (d (n "static_assertions") (r "^0.3.4") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)) (d (n "watt") (r "^0.3") (d #t) (k 0)))) (h "0pb2dzg0sic8gdh90c5fg24ign2avhipibxm312wmj3m3n5qsp7c") (f (quote (("std") ("default" "std"))))))

