(define-module (crates-io di sp display_macro) #:use-module (crates-io))

(define-public crate-display_macro-0.1.0 (c (n "display_macro") (v "0.1.0") (h "11vhrvir197rr7mdvdkcf1qamy5diha2r08gl5g45pcjpawaa8za") (y #t)))

(define-public crate-display_macro-0.1.1 (c (n "display_macro") (v "0.1.1") (h "0ak6lwh8zx2942fzkw7isjrlvi8jfm2j3y7pd3x2lnv79f2qiakq")))

