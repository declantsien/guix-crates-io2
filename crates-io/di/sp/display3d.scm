(define-module (crates-io di sp display3d) #:use-module (crates-io))

(define-public crate-display3d-0.1.4 (c (n "display3d") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gemini-engine") (r "^0.10.3") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.0") (d #t) (k 0)))) (h "088029ynlav0ssgijszcyrfyfaj0hinrwjxa8ggky164hq6mlr6z") (y #t)))

(define-public crate-display3d-0.1.5 (c (n "display3d") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gemini-engine") (r "^0.10.3") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.0") (d #t) (k 0)))) (h "0ba33a4byjpzx2r83ybhika0l8pbzs9hi21aszjh2qabfcwy8xaf") (y #t)))

(define-public crate-display3d-0.1.6 (c (n "display3d") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gemini-engine") (r "^0.10.3") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.0") (d #t) (k 0)))) (h "1mpmis2hid9zd69j4bgrlfs8fv040633a3ysyrsgm6cd5pfb4wd7")))

(define-public crate-display3d-0.1.7 (c (n "display3d") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.10.3") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.0") (d #t) (k 0)))) (h "1l4qpy5r12d66az9ipa957qm7348wayd3y5n2yhb4hg3v8l1ckf7")))

(define-public crate-display3d-0.1.8 (c (n "display3d") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.11.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.0") (d #t) (k 0)))) (h "1c5k85q2qy191wdh4qm91rc34cg886pibz8hhgzyk83p3i8zr3v8")))

(define-public crate-display3d-0.1.9 (c (n "display3d") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.11.2") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0") (d #t) (k 0)))) (h "1q8nyg07ryn9fwxp38373y009g0lpdcibflb423wx565m113p6cv")))

(define-public crate-display3d-0.1.10 (c (n "display3d") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.13.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.1") (d #t) (k 0)))) (h "0l8hcqapikz27k7sbaddacvipj2hih2xj60nn48kxh4sjkl8rcx4")))

(define-public crate-display3d-0.1.11 (c (n "display3d") (v "0.1.11") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.13.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.1") (d #t) (k 0)))) (h "064sjw2fgksql0c1b2hbrshbh0fglilcd3lf0aza9hrvhz35xp8p")))

(define-public crate-display3d-0.1.12 (c (n "display3d") (v "0.1.12") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.14.0") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.1") (d #t) (k 0)))) (h "02djqbf3p4fyld4d7z8l0n1yjzm4rgsi0nfqp0ihbh3d8sxgzrv0")))

(define-public crate-display3d-0.1.13 (c (n "display3d") (v "0.1.13") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.14.1") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.1") (d #t) (k 0)))) (h "195wwx4vra8aj4dzgbym42h6l92ck77hzx68c20bp3xs1fg6m5ga")))

(define-public crate-display3d-0.1.14 (c (n "display3d") (v "0.1.14") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "gemini-engine") (r "^0.14.2") (d #t) (k 0)) (d (n "stl_io") (r "^0.7.0") (d #t) (k 0)) (d (n "tobj") (r "^4.0.1") (d #t) (k 0)))) (h "0i5j3ga3v8smkrimqc8zdz49909c3slb1cb2cva44f8hnbs6sj54")))

