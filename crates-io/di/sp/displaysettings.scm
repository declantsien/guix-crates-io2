(define-module (crates-io di sp displaysettings) #:use-module (crates-io))

(define-public crate-DisplaySettings-0.1.0 (c (n "DisplaySettings") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wingdi" "winnt"))) (d #t) (k 0)))) (h "1czjvyvxfdjgplvkpk78l6rm14zhs7pnraz26kr2sxwfj1fzrdcs")))

(define-public crate-DisplaySettings-0.1.1 (c (n "DisplaySettings") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wingdi" "winnt"))) (d #t) (k 0)))) (h "1h73kqsc860y2lnvmc1mzz1fjn73g3zcdbgaj4klqb9cl2lgq7f9")))

(define-public crate-DisplaySettings-0.1.2 (c (n "DisplaySettings") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wingdi" "winnt"))) (d #t) (k 0)))) (h "09il6rnbfk5gsl4m4ycqbal8mw87v7m7h93lwj8pjpm44pdj4vfh")))

