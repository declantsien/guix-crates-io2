(define-module (crates-io di sp displaydoc-lite) #:use-module (crates-io))

(define-public crate-displaydoc-lite-0.1.0 (c (n "displaydoc-lite") (v "0.1.0") (d (list (d (n "defile") (r "^0.1") (d #t) (k 0)))) (h "0cx19141xshxi5bhczp2sna3ycfhbj7jw1mi0pg0k1b0cz9anzny")))

(define-public crate-displaydoc-lite-0.1.1 (c (n "displaydoc-lite") (v "0.1.1") (d (list (d (n "defile") (r "^0.1") (d #t) (k 0)) (d (n "proc-macros") (r "^0.1.1") (d #t) (k 0) (p "displaydoc-lite-proc_macros")))) (h "1xgikksvyh2cnf0iw592301mdphdiqvnsarcz3f29b2df0gzblin") (y #t)))

(define-public crate-displaydoc-lite-0.1.2 (c (n "displaydoc-lite") (v "0.1.2") (d (list (d (n "defile") (r "^0.1") (d #t) (k 0)) (d (n "displaydoc-lite-proc_macros") (r "^0.1") (d #t) (k 0)))) (h "0nxfgj7f39mq87g8dl00l4pjirzczipqxzzr7pwbvb17f5mx68nh")))

(define-public crate-displaydoc-lite-0.1.3 (c (n "displaydoc-lite") (v "0.1.3") (d (list (d (n "displaydoc-lite-proc_macros") (r "^0.1") (d #t) (k 0)))) (h "12hl548xdv38m2lzy16n2ijhqzp74aznyvlnprrv93f32ny8b7my")))

