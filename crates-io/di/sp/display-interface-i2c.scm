(define-module (crates-io di sp display-interface-i2c) #:use-module (crates-io))

(define-public crate-display-interface-i2c-0.1.0 (c (n "display-interface-i2c") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0snk6kcfdm66a5fp4y09jdngw1r5f158hgxx1gb6xs2qsraswaqy")))

(define-public crate-display-interface-i2c-0.2.0 (c (n "display-interface-i2c") (v "0.2.0") (d (list (d (n "display-interface") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1nx9n5695i48gs5j0irsfp7li7vm6gm9n916l9zc2mk2mlch6036")))

(define-public crate-display-interface-i2c-0.3.0 (c (n "display-interface-i2c") (v "0.3.0") (d (list (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0jq821mjglpsb4zanvy6mr8w1bwbqlyv216m6dqfz0v26b1bky47")))

(define-public crate-display-interface-i2c-0.3.1 (c (n "display-interface-i2c") (v "0.3.1") (d (list (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "14jkncfs9z16b6pfw8ccldm1pdxvs2vpd91xm2gvhkj95x6fissl")))

(define-public crate-display-interface-i2c-0.4.0 (c (n "display-interface-i2c") (v "0.4.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1gsfgf2dswgsxdd825ddv1fjgn5ahz3yxwfpf3rnwlz5ai7cv5a8")))

(define-public crate-display-interface-i2c-0.5.0 (c (n "display-interface-i2c") (v "0.5.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)))) (h "0b96rnhh03bbgsmw1nkjmpiw6pdc0ya6jn3fs3nadddvbfl4z5hd") (r "1.75")))

