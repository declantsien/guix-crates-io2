(define-module (crates-io di sp display-99bugs-driver) #:use-module (crates-io))

(define-public crate-display-99bugs-driver-0.1.0 (c (n "display-99bugs-driver") (v "0.1.0") (d (list (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "1d3s2bl8gnrr4plxxdhzcz5ighxg35wjrb37ibvl1apjr8k0a84w")))

(define-public crate-display-99bugs-driver-0.1.0-a (c (n "display-99bugs-driver") (v "0.1.0-a") (d (list (d (n "spidev") (r "^0.3.0") (d #t) (k 0)))) (h "036mxn415i2kcgxdfb8yc45w8w143mxnm25jlvpqxrk6qfxlxq7s")))

