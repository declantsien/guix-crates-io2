(define-module (crates-io di sp dispatch) #:use-module (crates-io))

(define-public crate-dispatch-0.0.1 (c (n "dispatch") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "15jmvqffxc4k1nrpw1y25ffysiw4xl7lllr0yy02w7j3962j0drk")))

(define-public crate-dispatch-0.0.2 (c (n "dispatch") (v "0.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ni7a0jxhjim54l8vrknmhyxx3r3jrwrdqjy988ygnwak4mvz0zl")))

(define-public crate-dispatch-0.0.3 (c (n "dispatch") (v "0.0.3") (d (list (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)))) (h "0qzj8b0a3yg6s96cqffg9qh4sbkzcin02ddj85bqmp5ssxx1pzzd")))

(define-public crate-dispatch-0.1.0 (c (n "dispatch") (v "0.1.0") (h "1ixjb7pmgxkw05a5jwglz67z57ps60d15p3vy0nzvyzfmvhyssz4")))

(define-public crate-dispatch-0.1.1 (c (n "dispatch") (v "0.1.1") (h "0ikrf1j03jb3ryvphr63xifykwd7w6kahhk18phz4k4qg3dgswq1")))

(define-public crate-dispatch-0.1.2 (c (n "dispatch") (v "0.1.2") (h "16b6rpilf65bkp13qacxn3s30p5pwhvij80hf1mda51i191wj7d8")))

(define-public crate-dispatch-0.1.3 (c (n "dispatch") (v "0.1.3") (h "06y072zprccz8j93z37niyjyvklj748nbwl70zm9l286si8p9lwp")))

(define-public crate-dispatch-0.1.4 (c (n "dispatch") (v "0.1.4") (h "019nzy993hxaiazcdnayx3csv2iki34i535asw11ki96hakkrs84")))

(define-public crate-dispatch-0.2.0 (c (n "dispatch") (v "0.2.0") (h "0fwjr9b7582ic5689zxj8lf7zl94iklhlns3yivrnv8c9fxr635x")))

