(define-module (crates-io di sp display_derive) #:use-module (crates-io))

(define-public crate-display_derive-0.0.0 (c (n "display_derive") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.6.0") (d #t) (k 0)))) (h "1x1yjzmscfdb1fkzns7yyvdvdgyhp9xszab5rygn6m98dp6mvfjb") (f (quote (("std") ("default" "std"))))))

