(define-module (crates-io di sp display-interface-parallel-gpio) #:use-module (crates-io))

(define-public crate-display-interface-parallel-gpio-0.1.0 (c (n "display-interface-parallel-gpio") (v "0.1.0") (d (list (d (n "display-interface") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "00acp055ysjsnvigi827jg5zlvk61npy8hijpl3kmwwgzda7xqj3")))

(define-public crate-display-interface-parallel-gpio-0.1.1 (c (n "display-interface-parallel-gpio") (v "0.1.1") (d (list (d (n "display-interface") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "167mclqvyg3wp3w9zj0bvrz6jl1lba7al450qpqdq9jxfmz82s8p")))

(define-public crate-display-interface-parallel-gpio-0.2.0 (c (n "display-interface-parallel-gpio") (v "0.2.0") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0h8i2j1pln5ig430bnp8w48p8s8fz2zr2vcb6vf6d361rd628c3f")))

(define-public crate-display-interface-parallel-gpio-0.2.1 (c (n "display-interface-parallel-gpio") (v "0.2.1") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1j5ijf6h88ww9klqbjxvw9951bagjfwknn5yfz5cvk9df32zpg5v")))

(define-public crate-display-interface-parallel-gpio-0.4.0 (c (n "display-interface-parallel-gpio") (v "0.4.0") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "035kk8v3xzx54add4bwlvd6x07j4385bf7k6jp52yribsbg3hh9a")))

(define-public crate-display-interface-parallel-gpio-0.4.1 (c (n "display-interface-parallel-gpio") (v "0.4.1") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1nl65a5r3r27k7gkqnsac4zvb5lj0kvpzjz8v5dl3zgr2x2s2vq0")))

(define-public crate-display-interface-parallel-gpio-0.5.0 (c (n "display-interface-parallel-gpio") (v "0.5.0") (d (list (d (n "byte-slice-cast") (r "^0.3.5") (k 0)) (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "14hajb49d6jq72lnpa4r8dgr56ardsyy7akr263la9kjlibr4lif")))

(define-public crate-display-interface-parallel-gpio-0.6.0 (c (n "display-interface-parallel-gpio") (v "0.6.0") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "005j6w6lczqakq275rsz3s1jf2mpz7bs50y1vrsj47gf1a1v40jk")))

(define-public crate-display-interface-parallel-gpio-0.7.0 (c (n "display-interface-parallel-gpio") (v "0.7.0") (d (list (d (n "display-interface") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1sk3wpbb5jsc9diw4r243gwldfrvxhbrgqa1svx8lbjq7dai5f7v") (r "1.75")))

