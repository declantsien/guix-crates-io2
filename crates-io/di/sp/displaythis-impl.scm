(define-module (crates-io di sp displaythis-impl) #:use-module (crates-io))

(define-public crate-displaythis-impl-1.0.22 (c (n "displaythis-impl") (v "1.0.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "06iznyfj77wwpcw8125jixz4qyjvq7gwklxrkq0nd3cv1rp0cmmr")))

(define-public crate-displaythis-impl-1.0.23 (c (n "displaythis-impl") (v "1.0.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0hm8a2ljnj7xdg1x9wxzgsq0ppb8mlyibmr9d14h8mryp3h48jvz")))

