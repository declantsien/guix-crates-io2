(define-module (crates-io di sp dispose) #:use-module (crates-io))

(define-public crate-dispose-0.1.0 (c (n "dispose") (v "0.1.0") (h "10blsvxhgf9dqin8qwgms4a2lw3krmszdd57kn2qyh8mi0g73gmd")))

(define-public crate-dispose-0.2.0 (c (n "dispose") (v "0.2.0") (d (list (d (n "dispose-derive") (r "^0.1.0") (d #t) (k 0)))) (h "172qvz8s4z3pw00xl2r7d5zn9749as9sbw9h16q8i09zsfidynrx")))

(define-public crate-dispose-0.2.1 (c (n "dispose") (v "0.2.1") (d (list (d (n "dispose-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0fhr4xizijlgyh1h2667mz79l1n7xyp8wr8naz7nzaby86q6hmsz")))

(define-public crate-dispose-0.3.0 (c (n "dispose") (v "0.3.0") (d (list (d (n "dispose-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1gsjf090cx8c55cznkib2jwba30r9m56fx6p64grb78qcgdchzsx")))

(define-public crate-dispose-0.3.1 (c (n "dispose") (v "0.3.1") (d (list (d (n "dispose-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1738d84npbsa12pcdpkqy5b0mhfh95lbg73jwh09md0q5577ghv8")))

(define-public crate-dispose-0.4.0 (c (n "dispose") (v "0.4.0") (d (list (d (n "dispose-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1l80hz8ppgk74d09cka7kia5kvbq0q64kcyqmd3d4msn5i1j4g57")))

(define-public crate-dispose-0.5.0 (c (n "dispose") (v "0.5.0") (d (list (d (n "dispose-derive") (r "^0.4.0") (d #t) (k 0)))) (h "0lp4jk8sf2clclkmr5md02h94cavwhd6miv2k5227q2k7ilgf9gc")))

