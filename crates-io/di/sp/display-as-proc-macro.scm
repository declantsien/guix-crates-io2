(define-module (crates-io di sp display-as-proc-macro) #:use-module (crates-io))

(define-public crate-display-as-proc-macro-0.1.0 (c (n "display-as-proc-macro") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0xfpw4m6a4hfy3xybv303rjfwy2a5qyn11nfz52qhdah56zb873s")))

(define-public crate-display-as-proc-macro-0.1.1 (c (n "display-as-proc-macro") (v "0.1.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "02kfgxi9ggsk8jj9is1kgikdqvay290kziqbwpjc0yfw37idld4a")))

(define-public crate-display-as-proc-macro-0.2.0 (c (n "display-as-proc-macro") (v "0.2.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "05xkxk1an57mn347dq4gcvi9bmahkis3sncy6dys4zx9f22n4v61")))

(define-public crate-display-as-proc-macro-0.2.1 (c (n "display-as-proc-macro") (v "0.2.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1xz59703dsj1z5g29qhvargf3ca1ghq3z2906ws8wmv8xs2jzhk4")))

(define-public crate-display-as-proc-macro-0.3.0 (c (n "display-as-proc-macro") (v "0.3.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "13021yr6x6wvg09vqg6wm53ifdsy1d7gdw9bxyca86vy72zn59q6")))

(define-public crate-display-as-proc-macro-0.3.1 (c (n "display-as-proc-macro") (v "0.3.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1gagma63k4kh0zg796hy51svp5f1scppil4m6gm9pqg1gpxqxylf")))

(define-public crate-display-as-proc-macro-0.4.0 (c (n "display-as-proc-macro") (v "0.4.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1c5ch6j3jfa9bdsw7jix9hgf4q065cckyl6hg8b3chlqgfzr90i0")))

(define-public crate-display-as-proc-macro-0.4.1 (c (n "display-as-proc-macro") (v "0.4.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0wk0yz5y0bskvzgncsbmqy0bzg3y9dqrwqyxvz4mqaydp12fpxiz")))

(define-public crate-display-as-proc-macro-0.4.2 (c (n "display-as-proc-macro") (v "0.4.2") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "04dq0575y1z3v83r249gqispcj0hf58w81s5n5b59wk3kwj3ssxd")))

(define-public crate-display-as-proc-macro-0.4.3 (c (n "display-as-proc-macro") (v "0.4.3") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "04fcs3idg0zq211055fzvfgbj6pir4flhwhlnwsv6v4rib6gy1yz")))

(define-public crate-display-as-proc-macro-0.6.0 (c (n "display-as-proc-macro") (v "0.6.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "08cb8cfnprs3jzl3i1sf0xlb8rpp8wgylkcqd05hvaw3v65a6jj3")))

(define-public crate-display-as-proc-macro-0.6.1 (c (n "display-as-proc-macro") (v "0.6.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1fg97gak5x2spnshjnfvbvzzl6cjxfsav1npyxblhifbbx18hvan")))

(define-public crate-display-as-proc-macro-0.6.2 (c (n "display-as-proc-macro") (v "0.6.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0pn7zf6w54346qxwwfisscn869vp1rrpsk0wd42869x6p4mgglqx")))

(define-public crate-display-as-proc-macro-0.6.3 (c (n "display-as-proc-macro") (v "0.6.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1yv088c7z1h5f5n57lyvs8dcp6jypxljgmkgzcsa37scf64ahxi7")))

