(define-module (crates-io di sp display_with_options) #:use-module (crates-io))

(define-public crate-display_with_options-0.1.0 (c (n "display_with_options") (v "0.1.0") (h "08j60ixxm5vqlfkyzjyydm6pb7fdzsbjz85f1k92l1i5adi3mfw3")))

(define-public crate-display_with_options-0.1.1 (c (n "display_with_options") (v "0.1.1") (h "0rr1gspckv8jjwcmnb7cxara1x6kx3ab0rg0b3n8h7c7l6s9n47h")))

(define-public crate-display_with_options-0.1.2 (c (n "display_with_options") (v "0.1.2") (h "1cmqggv62r0287hw1z464n5d8zydrcvl6045cmqj06ir6jh80851")))

(define-public crate-display_with_options-0.1.3 (c (n "display_with_options") (v "0.1.3") (h "1vajxsihs417c6lwgxylvjy04pnma2lvsivd1qd1qg1avqxkm9kl")))

