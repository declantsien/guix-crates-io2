(define-module (crates-io di sp display_adapter_attr) #:use-module (crates-io))

(define-public crate-display_adapter_attr-0.1.0 (c (n "display_adapter_attr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02fh5j58ma9p6swwfx4ppwjh2r3fq54zdiajhk1nns6qbvh7a2lh") (y #t)))

(define-public crate-display_adapter_attr-0.1.1 (c (n "display_adapter_attr") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xyz7f74y3xj9mi8zm85kpacaa523snhsa5msrcqjbxrwwv08za6")))

