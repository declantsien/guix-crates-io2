(define-module (crates-io di sp display_container) #:use-module (crates-io))

(define-public crate-display_container-0.9.0 (c (n "display_container") (v "0.9.0") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "indenter") (r "^0.3.3") (d #t) (k 0)))) (h "1yk6jiijfl8gibyrb79nga5jh46pl40flgc2cn7frvbbr5shl48a")))

