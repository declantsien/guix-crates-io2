(define-module (crates-io di sp display_tree) #:use-module (crates-io))

(define-public crate-display_tree-1.0.0 (c (n "display_tree") (v "1.0.0") (d (list (d (n "display_tree_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1w50zr28kvadkrx90kvfannah0qyqgldjrx0jv5hrraxii4fhhlw") (y #t)))

(define-public crate-display_tree-1.0.1 (c (n "display_tree") (v "1.0.1") (d (list (d (n "display_tree_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0xdddz4fzbnwpbh7w7jjd92kr7jh8apc163b01xx0p59wxs305cl") (y #t)))

(define-public crate-display_tree-1.0.2 (c (n "display_tree") (v "1.0.2") (d (list (d (n "display_tree_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1cng5j3s44qday8s5b80wdkrnsskmagrbksgahw5pvk1abx7516s") (y #t)))

(define-public crate-display_tree-1.0.3 (c (n "display_tree") (v "1.0.3") (d (list (d (n "display_tree_derive") (r "^1.0.1") (d #t) (k 0)))) (h "05j7g936af2ni31k2h3a0xsqs8yv0r4kpccg4j19y7lpgkylgbwj")))

(define-public crate-display_tree-1.1.0 (c (n "display_tree") (v "1.1.0") (d (list (d (n "display_tree_derive") (r "^1.0.1") (d #t) (k 0)))) (h "0a0fk2nn94d3avdl5k0qxanpw2f2pgy07ywd065pksmqdpm1d4bl")))

(define-public crate-display_tree-1.1.1 (c (n "display_tree") (v "1.1.1") (d (list (d (n "display_tree_derive") (r "^1.0.2") (d #t) (k 0)))) (h "0jdk7r5c61ql9nx9x3zr6smfirlbi8nzwsdwn8zx5i2z6y6rcjr2")))

(define-public crate-display_tree-1.1.2 (c (n "display_tree") (v "1.1.2") (d (list (d (n "display_tree_derive") (r "^1.0.3") (d #t) (k 0)))) (h "0fkqgx9a5n3wcv2pfigw4gw155zh65qlxrz34kcghjb0gahiss7f")))

