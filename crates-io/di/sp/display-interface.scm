(define-module (crates-io di sp display-interface) #:use-module (crates-io))

(define-public crate-display-interface-0.1.0 (c (n "display-interface") (v "0.1.0") (h "0mmqsz2k0ddzwym39daznpp9wasl1hazv29dd4am92kiha8a2nsg")))

(define-public crate-display-interface-0.1.1 (c (n "display-interface") (v "0.1.1") (h "1ysclbwkfb5ncdlq65dsybxyjjinkd5w9k8gwklc7jvb1d32dm58")))

(define-public crate-display-interface-0.2.0 (c (n "display-interface") (v "0.2.0") (h "00ky9l8vm6wjq9rv9hvg20fjhz0fq74v7q7mb0yn131fskc51lk1")))

(define-public crate-display-interface-0.2.1 (c (n "display-interface") (v "0.2.1") (h "13zjfbs7xg091hh86ly74c4ny9kzzvl52l9ry2zifaxrvz9aip87")))

(define-public crate-display-interface-0.3.0 (c (n "display-interface") (v "0.3.0") (h "176jqxl3hmzid8cd0wykycq003rylazzaf4izviy057ysczi54m2")))

(define-public crate-display-interface-0.4.0 (c (n "display-interface") (v "0.4.0") (h "1kw3ymads65m7scgfl047gd766ivcpf0723y4jp9bkjwvf9ah4ns")))

(define-public crate-display-interface-0.4.1 (c (n "display-interface") (v "0.4.1") (h "04j1zg2m5n40nk0zf9riadw0if3x2y4s0jl826qh4yvdj90c05vm")))

(define-public crate-display-interface-0.5.0 (c (n "display-interface") (v "0.5.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1803dh7csbxpmv2vq3v57lxvkvf5baxxwqj1h3vyd4rpxyqsm8kv") (f (quote (("default")))) (s 2) (e (quote (("defmt-03" "dep:defmt")))) (r "1.75")))

