(define-module (crates-io di sp displayz) #:use-module (crates-io))

(define-public crate-displayz-0.1.0 (c (n "displayz") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winsafe") (r "^0.0.10") (f (quote ("user"))) (d #t) (k 0)))) (h "098sf27b9h7mlj6lr6daf29hi3vl5smbcbcjfv7flbxajavi3d0j")))

