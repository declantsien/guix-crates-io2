(define-module (crates-io di a- dia-go) #:use-module (crates-io))

(define-public crate-dia-go-0.0.2 (c (n "dia-go") (v "0.0.2") (h "1l8q6rji9n7z144kc4lwxnx4x68xwzn1ifcsxbwlzppyjri2kxqz") (y #t)))

(define-public crate-dia-go-0.0.3 (c (n "dia-go") (v "0.0.3") (h "0vd50i8kbld8yp16yg8n7h0a4yyrpcaqd8whfcz27m5ffyx2glm9") (y #t)))

(define-public crate-dia-go-0.0.4 (c (n "dia-go") (v "0.0.4") (h "0lsbm3ix67r0sc1zirwdf8ngxb0lcb0vz7cllxw4j0b58nix0yqm") (y #t)))

(define-public crate-dia-go-0.0.5 (c (n "dia-go") (v "0.0.5") (h "0zpaq688pi7wq237dbz8jy4ri25jq5xq0xliam31ca2krwq0qwji") (y #t)))

(define-public crate-dia-go-0.1.0 (c (n "dia-go") (v "0.1.0") (h "0naxm3k23b50z0a8b6xnp8f5d37gjmzy6illgddgz2f07lxvyx9b") (y #t)))

(define-public crate-dia-go-0.2.0 (c (n "dia-go") (v "0.2.0") (h "16rj3zv2m5ad5r4f70s12h2yrmmki9yrvkivslnsfxnvn9dab5nn") (y #t)))

(define-public crate-dia-go-0.2.1 (c (n "dia-go") (v "0.2.1") (h "09mib6imcdqgihm4z3y4cjnagp9fbawnznvf4c74q89cp6bs75ik") (y #t)))

(define-public crate-dia-go-0.3.1+deprecated (c (n "dia-go") (v "0.3.1+deprecated") (h "0p5j6y1b16smrlqcg1d8ns0hwh7wm5ddikkkgvr5wnsqdsld1q8m") (y #t)))

