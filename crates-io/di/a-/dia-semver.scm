(define-module (crates-io di a- dia-semver) #:use-module (crates-io))

(define-public crate-dia-semver-0.0.2 (c (n "dia-semver") (v "0.0.2") (h "0byvp9894d525gzkz9ilg1z9vr5yzj5dg1i3ncjg26pbyhs54z8x")))

(define-public crate-dia-semver-0.0.4 (c (n "dia-semver") (v "0.0.4") (h "16hpsd53pfw9wxhhhdla35wrly3pi04pxnznn7g3k97nj1c5sv1p")))

(define-public crate-dia-semver-0.1.0 (c (n "dia-semver") (v "0.1.0") (h "1q7qrjr4szfvw92z36n5dwbj6rh234yv8vjikccwrwl4g8z8g1gc")))

(define-public crate-dia-semver-0.2.0 (c (n "dia-semver") (v "0.2.0") (h "1xlnvc50ip5ahnxjiblc9ncphc4s8wbiv5bmr57nqm4v7vy3sgp7")))

(define-public crate-dia-semver-0.3.0 (c (n "dia-semver") (v "0.3.0") (h "1y7iln1m08skrzwc5lbsbjqxkc9alfa1r80xrx8w72798896cc8y")))

(define-public crate-dia-semver-0.4.0 (c (n "dia-semver") (v "0.4.0") (h "0dibhl9ay0p088ba8fyay91dhy56kc105lx37x7q0mnvr8fqg59l")))

(define-public crate-dia-semver-0.5.0 (c (n "dia-semver") (v "0.5.0") (h "11v211d405qhm6hg3fdrhcds8bsj19s759j579nv569sn9zrs7i6")))

(define-public crate-dia-semver-0.6.0 (c (n "dia-semver") (v "0.6.0") (h "0nm52m91bbxmcbrah0a7ibmy3lki16zfhaxflrdrfqhychc5g7zi")))

(define-public crate-dia-semver-0.7.0 (c (n "dia-semver") (v "0.7.0") (h "1s92256vdi9rlchkmzcp6bdci31fzwgwwk3pca5jdkqy2p0ww8fm")))

(define-public crate-dia-semver-0.7.1 (c (n "dia-semver") (v "0.7.1") (h "0p2k70pzv5q6004znr98ypmn4zj0m5ffhrsk0sx5m2kddl7ig5lb")))

(define-public crate-dia-semver-0.7.2 (c (n "dia-semver") (v "0.7.2") (h "0lz3wxxqp7h8dh34l4955h2pl97lgz5vvpri3warhgg534ij3w13")))

(define-public crate-dia-semver-1.0.0 (c (n "dia-semver") (v "1.0.0") (h "0hjgnjc6napa8vwj6s7j2byw8siy3ib1gral9dz23h5xjp90swh8")))

(define-public crate-dia-semver-2.0.0 (c (n "dia-semver") (v "2.0.0") (h "1014spg2c8bi3znhqmc6jyq73fqa2w82jpizd4l4xq62vvl9ihq5")))

(define-public crate-dia-semver-3.0.0 (c (n "dia-semver") (v "3.0.0") (h "1fn87zkli64mpx83ima51jrggjsv77p07jd1nds8yzkhbkpsgdh6")))

(define-public crate-dia-semver-4.0.0 (c (n "dia-semver") (v "4.0.0") (h "1v0mbaalrl0pcdlch7g1v48ln7569nyfwbj0k5hrwijhspm30l06")))

(define-public crate-dia-semver-4.1.0 (c (n "dia-semver") (v "4.1.0") (h "1r4d353b6hbk9r0dc9vnyzwr48wnrf8wa5h4lyzxm69rh2s02crg")))

(define-public crate-dia-semver-4.2.0 (c (n "dia-semver") (v "4.2.0") (h "1rw27c5qdk0i9pvggqzz8nwfq4mq3qqj8jvy70rf3jmj1a5x2fai")))

(define-public crate-dia-semver-4.3.0 (c (n "dia-semver") (v "4.3.0") (h "1hpcikfgbgdrx4g166sm6qjsr3p5v0jzhpp7c2wy5s2bfln62h5r")))

(define-public crate-dia-semver-5.0.0 (c (n "dia-semver") (v "5.0.0") (h "1mn96b5zv9lzv80dxm2xapns83kyszyzk7wgca4qxvazwqfwbj9y")))

(define-public crate-dia-semver-5.1.0 (c (n "dia-semver") (v "5.1.0") (h "1k7ch2gr6y8mk8jh6w2fjy0lg16js4x13215kffnn2sywr1vpd26")))

(define-public crate-dia-semver-6.0.0 (c (n "dia-semver") (v "6.0.0") (h "023riiqar49hnf3ck0vb66y3l2rivlpcsm53nq25bp5pfqxxcix5")))

(define-public crate-dia-semver-6.1.0 (c (n "dia-semver") (v "6.1.0") (h "16xmczy4x8s0v288f7i5k3n0wknff2y1kdqnwry1575w5w178rnz")))

(define-public crate-dia-semver-6.1.1 (c (n "dia-semver") (v "6.1.1") (h "1hihxbkpqnp5znahnwm203via422wi74fnnyds5zjz1009r5db3y")))

(define-public crate-dia-semver-7.0.0 (c (n "dia-semver") (v "7.0.0") (h "1xw2gqpws3bj8gbjkaggybfw5lsb126mfvn7dpcm3iqpy72zdny1") (f (quote (("std"))))))

(define-public crate-dia-semver-7.0.1 (c (n "dia-semver") (v "7.0.1") (h "0ii4n1ri92f2j6wjkcywhgqq6jm1cyfhymmi78rss8dnl69jxga7") (f (quote (("std"))))))

(define-public crate-dia-semver-8.0.0 (c (n "dia-semver") (v "8.0.0") (h "1vzb31z0zgzksr6igxp8vipqlz4qicgbzv7fdmmxyk4jvwcxfaf6") (f (quote (("std"))))))

(define-public crate-dia-semver-8.0.1 (c (n "dia-semver") (v "8.0.1") (h "1pqngg7114rv190ydsx4w4j1qkcrzl9kwrd01f1g9qr75h8387mw") (f (quote (("std"))))))

(define-public crate-dia-semver-8.1.0 (c (n "dia-semver") (v "8.1.0") (h "0dhwhkgpkzif3wlr8drixx8kf4akcwzdxgchzscr4n2f70jim0mr") (f (quote (("std"))))))

(define-public crate-dia-semver-8.1.1 (c (n "dia-semver") (v "8.1.1") (h "0s0g5mlfgrc81jzlhmbv8y4s5nm402f9ii8nk4fpi7hfabwhwp0x") (f (quote (("std"))))))

(define-public crate-dia-semver-8.2.0 (c (n "dia-semver") (v "8.2.0") (h "1mkg9z6v2vawa5l9m6mkywmhzqp3719q18bpcbpp6bw0czrzrj4m") (f (quote (("std"))))))

(define-public crate-dia-semver-8.3.0 (c (n "dia-semver") (v "8.3.0") (h "15lw2qalak5z2292jxj548z5f7gh1kcr001wdp6mcvwmv6sm2xbp") (f (quote (("std"))))))

(define-public crate-dia-semver-8.3.1 (c (n "dia-semver") (v "8.3.1") (h "1z3s4r88jmw0j8rwayzm76jz29q51d320qjxd7i7f8a6wf86xsvj") (f (quote (("std"))))))

(define-public crate-dia-semver-8.3.2 (c (n "dia-semver") (v "8.3.2") (h "0v8i4k3ln6m9i04070kbsb904wvcmryjppgmcaqdh4bszmqlnmqv") (f (quote (("std"))))))

(define-public crate-dia-semver-8.3.3 (c (n "dia-semver") (v "8.3.3") (h "16vmp1ghxph659pnf5ichngrzgdcqgxjc72n1lwndplp3nm2agrz") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.0 (c (n "dia-semver") (v "9.0.0") (h "09p7vs7bmbfzpzw3xdl57dxd2q2fynibgcsc5mg3z546mv3x0m2y") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.1 (c (n "dia-semver") (v "9.0.1") (h "0ycpl1a5x6lf0wz3wa2kjfghfyzyha3mfjr186m9lhcsgsia3gfg") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.2 (c (n "dia-semver") (v "9.0.2") (h "01d94w6173wbx835ni0i41wh7cbapmxi2hwbfac2hvxcc956dm0m") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.3 (c (n "dia-semver") (v "9.0.3") (h "0icvqzp3v114b4j0f297m2shnm5nqzvkgb1dh42jz0ral4jbyxp0") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.4 (c (n "dia-semver") (v "9.0.4") (h "165zc4rbrhzq55m5338h75ddfzqg8ls73gmsqd3y6cj9qqss0znm") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.5 (c (n "dia-semver") (v "9.0.5") (h "0a41mdx49cqqy4wyazwy6ld4cg8ma95qcpixn6662jb57h2gjmn9") (f (quote (("std"))))))

(define-public crate-dia-semver-9.0.6 (c (n "dia-semver") (v "9.0.6") (h "0y9v8cwiav7s27fcgyv0mmdvyhi41faqnrwwyi9aq1jh2mqi8wr3") (f (quote (("std"))))))

(define-public crate-dia-semver-10.0.0 (c (n "dia-semver") (v "10.0.0") (h "1h2070qsnzanidx21czx8zbh22a9sdj7hxf52dafpvvv8jkkk1ag") (f (quote (("std"))))))

(define-public crate-dia-semver-10.0.1 (c (n "dia-semver") (v "10.0.1") (h "135mh2akbdpcphydcnvwqfyj4nddhs9rrd1ykp06xqy9f9plms8d") (f (quote (("std"))))))

(define-public crate-dia-semver-11.0.0 (c (n "dia-semver") (v "11.0.0") (h "08wn0vw0rqxh3hpi625gnvpxww54sg78y8q61zci626dq2fnkg8j") (f (quote (("std"))))))

(define-public crate-dia-semver-11.0.1 (c (n "dia-semver") (v "11.0.1") (h "1ja4a88gj8l5gnmfmjcff0qzl1s99c5x32nb2dpaqbj0f8j4mzzz") (f (quote (("std"))))))

