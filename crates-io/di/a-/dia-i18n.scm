(define-module (crates-io di a- dia-i18n) #:use-module (crates-io))

(define-public crate-dia-i18n-0.0.1 (c (n "dia-i18n") (v "0.0.1") (h "0az8ah72q98g1y5dk1wgsk02ld48hp8bi92hrw3i9kgzf0y7nncd")))

(define-public crate-dia-i18n-0.1.0 (c (n "dia-i18n") (v "0.1.0") (h "1ixfd3pakl2lxmvli725v07hwk4c6f0ipd3r64i9yxxgicsys2l0")))

(define-public crate-dia-i18n-0.2.0 (c (n "dia-i18n") (v "0.2.0") (h "132v3brj3rpc2zw7p650m9glf53hi2vn71ys65f1pfidgygrhw8q")))

(define-public crate-dia-i18n-0.3.0 (c (n "dia-i18n") (v "0.3.0") (h "05i4q0h5h6mbv8n66zxaa8fd5mr0dlq8dcy34069sdzkac31d508")))

(define-public crate-dia-i18n-0.4.0 (c (n "dia-i18n") (v "0.4.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1x5kk39pi176dv2i5nhk2p327p1c69ivl6gsfgckbsqp60ksz3li") (f (quote (("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.4.1 (c (n "dia-i18n") (v "0.4.1") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1hs8nl358938x6y0n903mb4fvwyc08cm6b9q1pcyr29bcpqhk37g") (f (quote (("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.5.0 (c (n "dia-i18n") (v "0.5.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.18") (o #t) (d #t) (k 0)))) (h "1i175ahhqbbkh01az8nhcy5jzck1jkhmz062bwkyhiyjrc6ilysz") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.6.0 (c (n "dia-i18n") (v "0.6.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.18") (o #t) (d #t) (k 0)))) (h "0m9d7ghsngj7rgcl21vr0xwnl3nm15dy7msnh00yx0aykn7flg68") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.7.0 (c (n "dia-i18n") (v "0.7.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.20") (o #t) (d #t) (k 0)))) (h "0hssrs1rck1kp98qyzfya31lx6iqjrqq123l5qf399hca0b8c5bj") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.8.0 (c (n "dia-i18n") (v "0.8.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.23") (o #t) (d #t) (k 0)))) (h "0ps9dgxw4xmhhzsw10c4x1rbmzc3y31ddd1gazc1yr6cldfhyf72") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.9.0 (c (n "dia-i18n") (v "0.9.0") (d (list (d (n "dia-args") (r "^0.52") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r "^0.24") (o #t) (d #t) (k 0)))) (h "0la25l92jf7hhcmbm315c8pxjzic1dcgl8kni561n71snbpir37j") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

(define-public crate-dia-i18n-0.10.0 (c (n "dia-i18n") (v "0.10.0") (d (list (d (n "dia-args") (r ">=0.59, <0.60") (o #t) (d #t) (k 0)) (d (n "sub-strs") (r ">=0.27, <0.28") (o #t) (d #t) (k 0)))) (h "1bhi85zxrrn735v13ipvgnh00g7kl0vm220p7picbzx80fwsnar9") (f (quote (("std") ("bin" "dia-args" "sub-strs"))))))

