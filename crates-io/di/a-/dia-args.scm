(define-module (crates-io di a- dia-args) #:use-module (crates-io))

(define-public crate-dia-args-0.0.4 (c (n "dia-args") (v "0.0.4") (h "1697gndj0hk558qkw0gp665w3q4bmhwlvkqfxfgnkfmbp22vyv74")))

(define-public crate-dia-args-0.1.0 (c (n "dia-args") (v "0.1.0") (h "0hkky9fxyjz1nli94inia1qiy156aw9bvibz0l02ms56xb9l5bn6")))

(define-public crate-dia-args-0.2.0 (c (n "dia-args") (v "0.2.0") (h "1jdqnm1wbsnqfsvj2nqq2l1d311a87r74cpz9fm3cxwv0ggjqyjv")))

(define-public crate-dia-args-0.2.1 (c (n "dia-args") (v "0.2.1") (h "17jjizy19kda2vkc8imy7qi9ra1hh220ddmsfzi734450dhxqjci")))

(define-public crate-dia-args-0.3.0 (c (n "dia-args") (v "0.3.0") (h "1n9ab5rg0liiihpjvpy2fypyd165g3vqif6nd8ji9dbvhzqsrisb")))

(define-public crate-dia-args-0.4.0 (c (n "dia-args") (v "0.4.0") (h "14mga6b4pxbd0r4j7h6wwmyyid7qa7vxx37vkshv916xm70m8yn8")))

(define-public crate-dia-args-0.5.0 (c (n "dia-args") (v "0.5.0") (h "1wrp4yxdh2gpz5m1in3dqb0pw2c4sxyyh6785wbp1bahpfmifcm7")))

(define-public crate-dia-args-0.6.0 (c (n "dia-args") (v "0.6.0") (h "136rm1vysc4aqswv4cadznjckn6nvnxr0yvirjdjxblma5vjbnq9")))

(define-public crate-dia-args-0.7.0 (c (n "dia-args") (v "0.7.0") (h "0344hc60iapqnxw7yfp6jw2s4g4xn5p509mqhb0c94n9j4sjbbzq")))

(define-public crate-dia-args-0.8.0 (c (n "dia-args") (v "0.8.0") (h "1nm4i4fxiwj0lmzmj7mjzw8kinw4rvpqk3smb8qfgd8jr1bylqjr")))

(define-public crate-dia-args-0.9.0 (c (n "dia-args") (v "0.9.0") (h "0wqizy2mj6kr3qy2xnhm8s617f1p6z42r0kwdbhkvlh8cmrw7zlh")))

(define-public crate-dia-args-0.10.0 (c (n "dia-args") (v "0.10.0") (h "0ilya994m8jgby0v18c5yq832mwg6qrp37z23nxad1bp4gncbsc3")))

(define-public crate-dia-args-0.11.0 (c (n "dia-args") (v "0.11.0") (h "1hckp93n13diwrd5y7nnivlr3sc0i8d395qpwqgz4fvkxdyiwbga")))

(define-public crate-dia-args-0.12.0 (c (n "dia-args") (v "0.12.0") (h "0v4v1yvan70kvbc2xh40bnk7hkb5my37c4incilaq4m7ivafs512")))

(define-public crate-dia-args-0.13.0 (c (n "dia-args") (v "0.13.0") (h "0wcgx1mlx88r24g3pi2s1pdns9b1l0kpk192brmnl67swga4km6b")))

(define-public crate-dia-args-0.14.0 (c (n "dia-args") (v "0.14.0") (h "1ywj7r8wiz52isl22p9jjb2jprcg29fiv8wqb2lkdjvbkxknvbhs")))

(define-public crate-dia-args-0.15.0 (c (n "dia-args") (v "0.15.0") (h "01qxiqnnl9q2ipdknyrix1pmbdig4ccrkkd50k2dfj08dh5j9mvx")))

(define-public crate-dia-args-0.16.0 (c (n "dia-args") (v "0.16.0") (h "07v1rc1hscb7q36g502gx6byz6vgxxv48yzfypcvpzmxl59mbdl9")))

(define-public crate-dia-args-0.17.0 (c (n "dia-args") (v "0.17.0") (h "11w2l6d1sg8a6c4p11l51gwqs62mybrjq7cilzmk25js9bmpppam")))

(define-public crate-dia-args-0.17.1 (c (n "dia-args") (v "0.17.1") (h "0d8zz7zlgnl2hiq8g1f33c9qpvqamc0kazcw4f4rjnxsqfqzsdy0")))

(define-public crate-dia-args-0.18.0 (c (n "dia-args") (v "0.18.0") (h "1dc95bd4pcj11s0aliq4cc7dnkpmlzdr6hagp0d8a3ngkka03jh0")))

(define-public crate-dia-args-0.19.0 (c (n "dia-args") (v "0.19.0") (h "0c3n0y0wjr61scjlwwfgx1q1z448sxb2qswjd2lrhginny0zcfc2")))

(define-public crate-dia-args-0.19.1 (c (n "dia-args") (v "0.19.1") (h "1np8bpnw2zjdqxq7i70m1s3z5lmva0g9sn3a9jpbq1nj0ga06678")))

(define-public crate-dia-args-0.20.0 (c (n "dia-args") (v "0.20.0") (h "17g2v5nq206yyf37fkcpp5agrz2xl8d02w55whj19pdfs8k80qgv")))

(define-public crate-dia-args-0.21.0 (c (n "dia-args") (v "0.21.0") (h "1gifzld2rbyaif9nks7zs3ddwfgkmckac0lllm190djgghq3di9n")))

(define-public crate-dia-args-0.22.0 (c (n "dia-args") (v "0.22.0") (h "1wdwklp30wj1l4fh7k51b5pha1fyfilvcp72c39db9yzymy8nwb7")))

(define-public crate-dia-args-0.23.0 (c (n "dia-args") (v "0.23.0") (h "1w8yrwa94g9869xqlyql4an80xjzjmdk1g7rpbb3jd2vxn6p9133")))

(define-public crate-dia-args-0.24.0 (c (n "dia-args") (v "0.24.0") (h "00x5d7klkq0wfknhij964x14qcqm6rh3441l3lqzgnlh0hv8c1yv")))

(define-public crate-dia-args-0.25.0 (c (n "dia-args") (v "0.25.0") (h "0g828yg6d4hn4mr2g5285fx0i0s4a4zkwphjlv92lif9zn9iacbl")))

(define-public crate-dia-args-0.26.0 (c (n "dia-args") (v "0.26.0") (h "1swnixilq2lyjlijvk6m8xhhcbizip8b24x8mzz25vxqsam32jhl")))

(define-public crate-dia-args-0.27.0 (c (n "dia-args") (v "0.27.0") (h "021pfkcpknjc8l00l7i517b0jz3z00kfvrn0xv2kawxbk5q0dzk9")))

(define-public crate-dia-args-0.28.0 (c (n "dia-args") (v "0.28.0") (h "1yif7l0kis55w7bhpdlqab35bfwrpr42i7128iaxk7dk7z7rjy0c")))

(define-public crate-dia-args-0.28.1 (c (n "dia-args") (v "0.28.1") (h "1gwbvajz492bdsc3bg8nbl8nl8gyzmzhjkgw83h3b3cdvhf92p19")))

(define-public crate-dia-args-0.29.0 (c (n "dia-args") (v "0.29.0") (h "111nx455vdbcwbqgx2gsgh80r1z96g33vkkm45w86q410i1xivfv")))

(define-public crate-dia-args-0.30.0 (c (n "dia-args") (v "0.30.0") (d (list (d (n "dia-time") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)))) (h "0ps6qsv6142h9zksvi0zizyyglsvpg7kn2jbalwhgg9yphwlp4r0") (f (quote (("bin" "dia-time" "libc" "tiny-keccak"))))))

(define-public crate-dia-args-0.31.0 (c (n "dia-args") (v "0.31.0") (d (list (d (n "dia-files") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)))) (h "1c68q3nzb0jxzfvrhlkn4y4bnxawgh4ll6sn2j7k9zhin1m8m2nm") (f (quote (("bin" "dia-files" "dia-time" "libc" "tiny-keccak"))))))

(define-public crate-dia-args-0.32.0 (c (n "dia-args") (v "0.32.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)))) (h "0g654fbpmvhjyw7r6hbfawj31h207961r0vx9lzsc1f7r9ydvnmb") (f (quote (("bin" "dia-files" "dia-time" "libc" "tiny-keccak"))))))

(define-public crate-dia-args-0.33.0 (c (n "dia-args") (v "0.33.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)))) (h "17p7d1kx61kzcyah10rs4dsydkqdb5g70larb33zgcn7spf5wfjm") (f (quote (("bin" "dia-files" "dia-time" "libc" "tiny-keccak"))))))

(define-public crate-dia-args-0.34.0 (c (n "dia-args") (v "0.34.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^4") (o #t) (d #t) (k 0)))) (h "0c6h94vrqrjhbk7kgbp7is8cwrdx1i0i3l2r8gmz4sdsk5dizms7") (f (quote (("bin" "dia-files" "dia-time" "libc" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.35.0 (c (n "dia-args") (v "0.35.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^4") (o #t) (d #t) (k 0)))) (h "1wjfxn5zqcgimqd888gkn0d7s73c5j237jlahxd6sqk07rj7zawc") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.36.0 (c (n "dia-args") (v "0.36.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^4") (o #t) (d #t) (k 0)))) (h "07p74dzxk8njsglhghgj37byivaxgpvd0pnkqg5rxnklhd4vzc1v") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.36.1 (c (n "dia-args") (v "0.36.1") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^4") (o #t) (d #t) (k 0)))) (h "04567lqjbh1mxz66ylmwf88lxdbd8pc0ckqa4kb82ab2d6m3f5l6") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.37.0 (c (n "dia-args") (v "0.37.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "07ksx5issqih8n7b43pi23wgb34v08dz146hpczldh8fqw4v0yab") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.38.0 (c (n "dia-args") (v "0.38.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "1qyimwkczx9dadqml3v4l78fz05fab0vsy165vaq2ba85jkyqail") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.39.0 (c (n "dia-args") (v "0.39.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "1v17wjn8z4awwb3jbgllz6z6lljmiwxr6pgghz1l4klfag6vrkfj") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.40.0 (c (n "dia-args") (v "0.40.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "19l4mlrahhihvfa4dm59f6jwj0py6qsdn2zsmi5cj5xzhy562qh0") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.41.0 (c (n "dia-args") (v "0.41.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "1na68nrj5mlqn3hf4xwkxvisag1iy2cwzn6nmaxi2r2klimdhphf") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.42.0 (c (n "dia-args") (v "0.42.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "1jpjwyxhbjimawcqhhp673xwmabw1n0bsw2wg2bhfsilwrhr6m3d") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.43.0 (c (n "dia-args") (v "0.43.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "0lxv95g6aazs4j0990q03vxxn1dnl5j8fzi2hb2cwasg3qrccb5w") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.44.0 (c (n "dia-args") (v "0.44.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "08s6fp3jr32dyk2lvv6wacmh880cgnbcnhcmw16qs2m7agady55i") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.45.0 (c (n "dia-args") (v "0.45.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "0c3g3rqyhvfy45xf97cp8hq1jg7gq8hrh0y1dsrccvxb54k9xfhz") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.46.0 (c (n "dia-args") (v "0.46.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.1") (o #t) (d #t) (k 0)))) (h "0163v8syf8vf9dvni6sfbbglmfvb7y9xj9bz8f778dldqlzk7022") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.47.0 (c (n "dia-args") (v "0.47.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "10nwhn44chkdsg80yzr4ra11iipdhc7ni3xfiqbnpcpx4lzqisbl") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.48.0 (c (n "dia-args") (v "0.48.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0y7hyls75vkkv7n89g9ab98rixhv1kkhbbzppk50ka1mqcbs0sx6") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.49.0 (c (n "dia-args") (v "0.49.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0i5d2zmpjqi6ddc931j5frnszp9shm1lcqx46p3y5gz8iyk82hd9") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.49.1 (c (n "dia-args") (v "0.49.1") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0s0141ah9i8cz22zh1kxmx8y46p1qwsw5iz6fxqvlz9napmacn88") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.50.0 (c (n "dia-args") (v "0.50.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0jvfshiyivs8jkcxwg7la2p8s1kr1r8q040yqbc83vccijl54ga2") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.0 (c (n "dia-args") (v "0.51.0") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0c2x892admz6c39xiwrfc82knv8d1dl4pi7mpxvs347vjaf3bhji") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.1 (c (n "dia-args") (v "0.51.1") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "1srgxgasqijr48kp87kliprifghh25dx7z9snlgsgagdc96svaav") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.2 (c (n "dia-args") (v "0.51.2") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "01dakxymsavq5dwpqi7c5rj78gzj76d13a3v6h11wyny4dy9z24w") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.3 (c (n "dia-args") (v "0.51.3") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "0b6mycgxpfgd2rdhn30k38hlflnfn9a03d3xq9wyn8a7gfb8k1s3") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.4 (c (n "dia-args") (v "0.51.4") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "1hniiwqms54qhvyb9v9vym51q50n2id998lnlg1d7aay5z1az5lr") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.5 (c (n "dia-args") (v "0.51.5") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "1yy5y0qnmsx23g8l6g24r46l1ra0ns00r37c3bsaw6fn4d1fbzz5") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.51.6 (c (n "dia-args") (v "0.51.6") (d (list (d (n "dia-files") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^7") (o #t) (d #t) (k 0)))) (h "04l4dyinvfsi5vp06ss07vkk5zvgigb9hagabpkgjnk1ni7y4ar1") (f (quote (("bin" "dia-files" "time" "tiny-keccak" "zeros"))))))

(define-public crate-dia-args-0.52.0 (c (n "dia-args") (v "0.52.0") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^5") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "0i2p8ix11y90irs7br6a3bbg4bcd68vypjms80zp6s6jszm3sagm") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.52.1 (c (n "dia-args") (v "0.52.1") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^5") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "17vf36v5035zrw7zzammp8hbhizldkzxn0a3gmgzw7pv1jcgcdyq") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.52.2 (c (n "dia-args") (v "0.52.2") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^5") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "15dl7k1v5s91rr89zvl17jhl1vxz696j23c98zsvc7hvp0fqm4n4") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.52.3 (c (n "dia-args") (v "0.52.3") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^5") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "1s5ihmhl67d1fjhkhnz07m8s3cwc2wazrmg0rix3916rnlxkq36b") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.53.0 (c (n "dia-args") (v "0.53.0") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^6") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "0rsyq6gqqz7xlvw7dkk8q1xnskkpmwha0si9jndls5ks65vy1ps4") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.53.1 (c (n "dia-args") (v "0.53.1") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^6") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "16fppq87jim7k2q0ig5w4wa5qfzpq46nggl8mvmxzlzs9l6fw3rv") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.54.0 (c (n "dia-args") (v "0.54.0") (d (list (d (n "dia-files") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^6") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10") (o #t) (d #t) (k 0)))) (h "140q9mhj034dxxw8irxpcfy6wcjz4dyr9564dz71cyz4yk40hcvl") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.54.1 (c (n "dia-args") (v "0.54.1") (d (list (d (n "dia-files") (r ">=0.12.1, <0.13") (o #t) (d #t) (k 0)) (d (n "dia-time") (r "^6") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.1, <12") (o #t) (d #t) (k 0)))) (h "0rm44mlak36zk1i79j3z29cphq46gyxh8aq8qn3bn14ibqndwqmw") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.55.0 (c (n "dia-args") (v "0.55.0") (d (list (d (n "dia-files") (r ">=0.13.1, <0.14") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=6.2.1, <7") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.1, <12") (o #t) (d #t) (k 0)))) (h "093v3zzyswl2yrrhgyfmirz1i6d2lbc81haxnmgzi6j9j1lzbvjc") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.55.1 (c (n "dia-args") (v "0.55.1") (d (list (d (n "dia-files") (r ">=0.15, <0.16") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=6.2.1, <7") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.2, <12") (o #t) (d #t) (k 0)))) (h "0ivjvrbib6zsa42zzfwqnrm9691wzz8kj0l0flj4dx27sl7aiz0q") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.56.0 (c (n "dia-args") (v "0.56.0") (d (list (d (n "dia-files") (r ">=0.15, <0.16") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=6.2.1, <7") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.2, <12") (o #t) (d #t) (k 0)))) (h "0bc1w271i6xky3py4scfm28d94jcpsfq2iyf4g801c1qc2cckxzr") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.57.0 (c (n "dia-args") (v "0.57.0") (d (list (d (n "dia-files") (r ">=0.15, <0.16") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=6.2.1, <7") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.2, <12") (o #t) (d #t) (k 0)))) (h "0f8a5d6h97bcsss0177rmr2x6b82hlv4k63b84zafrcn4k7zf8jg") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.58.0 (c (n "dia-args") (v "0.58.0") (d (list (d (n "dia-files") (r ">=0.15, <0.16") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=6.2.1, <7") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.2, <12") (o #t) (d #t) (k 0)))) (h "01cdr46bhd4zfym4rg0s2iyrmxfcsbvan0sqqd342jv4rpggigy7") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.0 (c (n "dia-args") (v "0.59.0") (d (list (d (n "dia-files") (r ">=0.16, <0.17") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=12, <13") (o #t) (d #t) (k 0)))) (h "13j3vanf2sa5k7sp11ph7dds1cyzs2y2i10sfj52gggyrzrda6hj") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.1 (c (n "dia-args") (v "0.59.1") (d (list (d (n "dia-files") (r ">=0.16, <0.17") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=12, <13") (o #t) (d #t) (k 0)))) (h "1prvxwswhhzlvh7m3kyqqllayb09j5qrq2sqqrcj5v4n5i7iad77") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.2 (c (n "dia-args") (v "0.59.2") (d (list (d (n "dia-files") (r ">=0.17.2, <0.18") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=12.2.1, <13") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1mkb9518s3r2yhwhaikbzifp9si66nqfc13gnm0rv7wb21g367yi") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.3 (c (n "dia-args") (v "0.59.3") (d (list (d (n "dia-files") (r ">=0.17.2, <0.18") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=12.2.1, <13") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "13dicw2dl6z4g7gl1w8zhmyq2lyc3rding9dlb4cqh61yr3653cf") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.4 (c (n "dia-args") (v "0.59.4") (d (list (d (n "dia-files") (r ">=0.17.2, <0.18") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=15, <16") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "022ayin8vsl2219wp785px25hk9g84hcfw8zj3jzpiyvdqsjxblr") (f (quote (("bin" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.5 (c (n "dia-args") (v "0.59.5") (d (list (d (n "debt64") (r ">=9, <10") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "dia-files") (r ">=0.17.6, <0.18") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0jd4pynvsjjk3m448nys3dhkp8j1jrm7adcg0j2ihxsww6brm2pn") (f (quote (("bin" "debt64" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.59.6 (c (n "dia-args") (v "0.59.6") (d (list (d (n "debt64") (r ">=9, <10") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "dia-files") (r ">=0.17.6, <0.18") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0li75kwvf6c76vr6w6iv8gmxwwbfkhx8nqhkfvf7ri3qn9lgi1ik") (f (quote (("bin" "debt64" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.60.0 (c (n "dia-args") (v "0.60.0") (d (list (d (n "debt64") (r ">=9, <10") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "dia-files") (r ">=0.19, <0.20") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1b83swx84f45ym6jfsgs2mlw092yvcr411zhx3k1dgf599a3xr5q") (f (quote (("bin" "debt64" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.60.1 (c (n "dia-args") (v "0.60.1") (d (list (d (n "debt64") (r ">=9, <10") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "dia-files") (r ">=0.19, <0.20") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0ychkicyvmrb3acfqrjm9z6rigw8xyw0wpqzsi5x7kk56hl80y98") (f (quote (("bin" "debt64" "dia-files" "dia-time" "zeros"))))))

(define-public crate-dia-args-0.60.2 (c (n "dia-args") (v "0.60.2") (d (list (d (n "debt64") (r ">=9, <10") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "dia-files") (r ">=0.19, <0.20") (o #t) (d #t) (k 0)) (d (n "dia-time") (r ">=7, <8") (f (quote ("lib-c" "std"))) (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=16, <17") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0c1z2jdjcv283wv918kl1b1i1izsv4748lbmgabn0dvpxb6nd4qy") (f (quote (("bin" "debt64" "dia-files" "dia-time" "zeros"))))))

