(define-module (crates-io di a- dia-ip-range) #:use-module (crates-io))

(define-public crate-dia-ip-range-0.1.0 (c (n "dia-ip-range") (v "0.1.0") (d (list (d (n "perm") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1zgxfjyyvr8z3j05d85q7bk0fwbv73xwwc1y699fvy5zmkx568v9") (f (quote (("iter" "perm"))))))

(define-public crate-dia-ip-range-0.1.1 (c (n "dia-ip-range") (v "0.1.1") (d (list (d (n "perm") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1zad11dqiqvfx4vw56f9g34xzci8wb9ig9bc2566as7px01hpnpm") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.2.0 (c (n "dia-ip-range") (v "0.2.0") (d (list (d (n "perm") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1k3mpj2g9dbqlla4h18xg26d43bs2q09ycsiynqkw10agsy2b7pb") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.3.0 (c (n "dia-ip-range") (v "0.3.0") (d (list (d (n "perm") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1hafvy30y2ldw5jjd32pazjz51bbyphb3ygi7bf4vf59qb913v63") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.4.0 (c (n "dia-ip-range") (v "0.4.0") (d (list (d (n "perm") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0f4gii8sg8bix17fyy54f217qma7cl3917flm4jvn98gg6qvsqdf") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.5.0 (c (n "dia-ip-range") (v "0.5.0") (d (list (d (n "perm") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0r3fmww0sq88xiil23hbnra27qiykn4yqi2kg5ziifdhnyzw564i") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.5.1 (c (n "dia-ip-range") (v "0.5.1") (d (list (d (n "perm") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1crs45agsw1ww0dkls5vd1n2pxz66yqvips8l1is59qrv6s6k6p8") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.6.0 (c (n "dia-ip-range") (v "0.6.0") (d (list (d (n "perm") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0knsh1m0359lxdn2fycc0kl2dw9c3yifrjhw2l8ychykhmk6pgc6") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.7.0 (c (n "dia-ip-range") (v "0.7.0") (d (list (d (n "perm") (r ">=0.8, <0.9") (o #t) (d #t) (k 0)))) (h "14aczgw28qn6hrjzd3pj7y8vmvn1gyfwf5lr3dfdlrknm1w4lfnc") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.7.1 (c (n "dia-ip-range") (v "0.7.1") (d (list (d (n "perm") (r ">=0.8, <0.9") (o #t) (d #t) (k 0)))) (h "0rj60h5wwsn0ik0jg9x77jqrfgwhp3d6q8bzd546qd3rjpafxiib") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.8.0 (c (n "dia-ip-range") (v "0.8.0") (d (list (d (n "perm") (r ">=0.9, <0.10") (o #t) (d #t) (k 0)))) (h "14sabpdmpnlwgfz0hxbri81ff8zhfw565w486bj3w4mn7knfy2xm") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.9.0 (c (n "dia-ip-range") (v "0.9.0") (d (list (d (n "perm") (r ">=0.9.1, <0.10") (o #t) (d #t) (k 0)))) (h "0zhh4mfnskpiqcdcjmw7bwiskwnarywbx3ra88a60gkvp1k9n5b9") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.9.1 (c (n "dia-ip-range") (v "0.9.1") (d (list (d (n "perm") (r ">=0.9.3, <0.10") (o #t) (d #t) (k 0)))) (h "19p8yv5a8329nvckw21ppryhd4hcmyii65k85ams7cw0hz9vhi37") (f (quote (("std") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.9.2 (c (n "dia-ip-range") (v "0.9.2") (d (list (d (n "perm") (r ">=0.9.3, <0.10") (o #t) (d #t) (k 0)))) (h "02h6bpg90p0r06sp664i7gzn4zzcy1s92m70gsxj2m01igk8x5i5") (f (quote (("std") ("pub-perm") ("iter" "perm"))))))

(define-public crate-dia-ip-range-0.10.0 (c (n "dia-ip-range") (v "0.10.0") (d (list (d (n "perm") (r ">=0.9.3, <0.10") (o #t) (d #t) (k 0)))) (h "10cl15dwggn9z7681f5771r2m1pm3fhz02hp7r8cab6ffis4zhma") (f (quote (("std") ("iter" "perm"))))))

