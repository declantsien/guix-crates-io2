(define-module (crates-io di a- dia-range) #:use-module (crates-io))

(define-public crate-dia-range-0.0.2 (c (n "dia-range") (v "0.0.2") (h "12hnbdjy425g32hxag4z5fd1c2s2zd2nhnic1xzljdlnph120niv") (y #t)))

(define-public crate-dia-range-0.1.0 (c (n "dia-range") (v "0.1.0") (h "1gr6vfp5s9rx5vv90sdpav2b3vrjh07z2q2xq0x84l0xsi1zaxvr") (y #t)))

(define-public crate-dia-range-0.2.0 (c (n "dia-range") (v "0.2.0") (h "1lf6k5s0ylykj6i3j53kzsz23g5ypmyj5jz7f0sxw7k7vwk3gz7l") (y #t)))

(define-public crate-dia-range-0.3.0 (c (n "dia-range") (v "0.3.0") (h "1jpkbp8df8fza5sxhqcgnnrncxi9ifsrr3xmrlfqkw5swgcll374") (y #t)))

(define-public crate-dia-range-0.4.0 (c (n "dia-range") (v "0.4.0") (h "1y7r541nrdp4i1xyhd6crsq245329nzq35yyncyylp4268qm6iiq") (y #t)))

(define-public crate-dia-range-0.5.0 (c (n "dia-range") (v "0.5.0") (h "0kw08cmznkm3vljlk8y5mcz5dk7h08m30p4cxqkqgpmfds0jy2zg") (y #t)))

(define-public crate-dia-range-0.6.0 (c (n "dia-range") (v "0.6.0") (h "0bmwvhd6gi3x8zmhyqx1sg5b9pgdfqkspl534z70klm75aqkikzd") (y #t)))

(define-public crate-dia-range-0.7.0 (c (n "dia-range") (v "0.7.0") (h "0y3iz6h6wkddcg2in45mq9lw6pwb3dh1klxj9xixk3rips8y3s46") (y #t)))

(define-public crate-dia-range-0.8.0 (c (n "dia-range") (v "0.8.0") (h "0vahzr69lzd1s6jmirc548dafqwwvmzvl1w0jasbh8lk0r4wg9cr") (y #t)))

(define-public crate-dia-range-0.9.0 (c (n "dia-range") (v "0.9.0") (h "0v0xdgxy7y2m79zpcdmh308046zav0swnadaivl5xh2ysdfhmxcw") (y #t)))

(define-public crate-dia-range-0.10.0 (c (n "dia-range") (v "0.10.0") (h "0w1rdrsya18y2q20lk1kszhwc4cyhs9h0bfna1pjl0f8rxzgz3vc") (f (quote (("std")))) (y #t)))

