(define-module (crates-io di a- dia-files) #:use-module (crates-io))

(define-public crate-dia-files-0.0.2 (c (n "dia-files") (v "0.0.2") (h "0bv2h8m9cxlpi3dw1fja0h6gp6bip8zklb9a9wkp2z5fi2hn2g2p")))

(define-public crate-dia-files-0.1.0 (c (n "dia-files") (v "0.1.0") (h "1c32pwzzbn1ccm11hvgcjxs7ckd3yv77gjb4z4l7gzpfh9yk2k0f")))

(define-public crate-dia-files-0.2.0 (c (n "dia-files") (v "0.2.0") (h "0w14ncjxmm2jdppa2d7xvnaahbahrzygg4i5y2fnh7ajpljn9dsc")))

(define-public crate-dia-files-0.3.0 (c (n "dia-files") (v "0.3.0") (h "18y7r876s2fwdc1wphkcc30l2p4papfdrmmzsldcfqdqg397vrw2")))

(define-public crate-dia-files-0.4.0 (c (n "dia-files") (v "0.4.0") (h "17fhr5xwj02d17p89n38ni1hyjw8gv6axh74gnps6xy1szinqbb4")))

(define-public crate-dia-files-0.5.0 (c (n "dia-files") (v "0.5.0") (h "0p20acl2x0sfvlr9fah5k701b8qxpjyjb9ixmn3phd9mq8igdvn0")))

(define-public crate-dia-files-0.6.0 (c (n "dia-files") (v "0.6.0") (h "1kkb5w4a8krjbr7lvmixhlf0yr5443bv3j29gb0p3pkypa81chfy")))

(define-public crate-dia-files-0.6.1 (c (n "dia-files") (v "0.6.1") (h "0q6v7zqd9zys015zni29n3cyyvqdqvxim1ba6mfybqfr0npfkz4j")))

(define-public crate-dia-files-0.7.0 (c (n "dia-files") (v "0.7.0") (h "05qzmgfq06d3pix3g73lz66l9vsladxw80794bm8m0v364xbpkdm")))

(define-public crate-dia-files-0.8.0 (c (n "dia-files") (v "0.8.0") (h "1fvkzw0hjxr7a3q8aw6nby2sii5mbfjipkh1g8nhd85cvh4iivqd")))

(define-public crate-dia-files-0.9.0 (c (n "dia-files") (v "0.9.0") (h "1wrrfwgfar3wb1qvbfmb681vraw0a6jb618jh3xyr83fn4m0df1r")))

(define-public crate-dia-files-0.10.0 (c (n "dia-files") (v "0.10.0") (h "06diqj193pap3qfd7yy59m8rx3g04z1nyz86an53r0246q2qhdkg")))

(define-public crate-dia-files-0.10.1 (c (n "dia-files") (v "0.10.1") (h "1n4m98yzcbiqn6n7bgahgzchcg3hig1a1b73rg9h9q786r6apxd1")))

(define-public crate-dia-files-0.11.0 (c (n "dia-files") (v "0.11.0") (h "1xj3qs10270h16jr1yimckag0043sa928kwbas5sfc2k6i57wb4i")))

(define-public crate-dia-files-0.11.1 (c (n "dia-files") (v "0.11.1") (h "1ypl3ixr02f2xmnpjciwgg8rkc4kvvx4as4wnm4g54i5c8lis3na")))

(define-public crate-dia-files-0.12.0 (c (n "dia-files") (v "0.12.0") (h "0plvvn937mcv926kf41k656czyflyvzn65cjnalbkby6gab4dyq2")))

(define-public crate-dia-files-0.12.1 (c (n "dia-files") (v "0.12.1") (h "1qk1bdmrnb6wm06v3lvx3fxy4dvjddqjldsg9ka0pf4wp9bpx62q")))

(define-public crate-dia-files-0.13.0 (c (n "dia-files") (v "0.13.0") (h "04fs8457933vkhl1ivp5srqmwaibwkwf3334293mw9sy5y1lbpqz")))

(define-public crate-dia-files-0.13.1 (c (n "dia-files") (v "0.13.1") (h "00zfgwlrkgs388b9sdsfghv0fn2r6d4ifvdsv8pjdm0rw2a8zp2y")))

(define-public crate-dia-files-0.14.0+master (c (n "dia-files") (v "0.14.0+master") (h "1ip4szm5y6bmik9v4r37xyzygyyvx8qbzgd9krrdjxnr7dyp7251")))

(define-public crate-dia-files-0.15.0 (c (n "dia-files") (v "0.15.0") (h "19srh2j5y4hq4lzpghy69i25fkn76zvk8jhk4gk2qi2460rrp8vn")))

(define-public crate-dia-files-0.15.1 (c (n "dia-files") (v "0.15.1") (h "1p678r9jzdipcpxwqfw6axhm2hzz4sgf0b2qyj8bg3mq79yd1dy8")))

(define-public crate-dia-files-0.16.0 (c (n "dia-files") (v "0.16.0") (h "1szihq89251nk3p316xl7rfzpq7inc6brr31c77dv5v14kaicw4i")))

(define-public crate-dia-files-0.16.1 (c (n "dia-files") (v "0.16.1") (h "1bxnjnkzws97156syf7jkc0zabhj6xh0b41ydyxjnpkpwgwwcpza")))

(define-public crate-dia-files-0.16.2 (c (n "dia-files") (v "0.16.2") (h "1y6pzsfr3qy2vghjpq1jbrwjih9g98v4vi8rvhxzc48vw2ycgs67")))

(define-public crate-dia-files-0.17.0 (c (n "dia-files") (v "0.17.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "18wi38c7mr3vf263yrvsq0cwg8nax7lijrxz5ssg4cz4ih12izmh") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.1 (c (n "dia-files") (v "0.17.1") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0vkgczgqnq6glrhcr3a7vycf28xj0pag0cabf5w26qwfxz0828qi") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.2 (c (n "dia-files") (v "0.17.2") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "054y3nrj1n5bk2gv13f3r5b6fjxhhb7z47jszzzj9lc62g5a2z4h") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.3 (c (n "dia-files") (v "0.17.3") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "1bw6r8dxwwaazzav1lq86qil0q39gn7ih4dszvpgd9kwkfbrq45s") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.4 (c (n "dia-files") (v "0.17.4") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "172qaqbg0jpf8jbhqcwkd4db7rr8mmilqw4cg90kg3jazqgxsd08") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.5 (c (n "dia-files") (v "0.17.5") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "05lw8aixal66n8kvlh94cj63bmxl6zdbgp63gn838ksbsybs3f7z") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.17.6 (c (n "dia-files") (v "0.17.6") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0qggvmmskygdydahpzgy6z7s7hj76cy84vdfpm0ssyqi6c47birh") (f (quote (("lib-c" "libc"))))))

(define-public crate-dia-files-0.18.0 (c (n "dia-files") (v "0.18.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "01r7j988z3s1bk9wrs480mfxnaqc0mpmn10n25gay7gh6rdlcnik") (f (quote (("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.18.1 (c (n "dia-files") (v "0.18.1") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0jkib6b9ngdh2xvnmjqsi4jnaazrxmqb42n3lddms9qi9vn3dgw8") (f (quote (("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.19.0 (c (n "dia-files") (v "0.19.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0f1mskhz99fb9c28dmgxvl66na304x4w7dd5ih0q88vp9kyyrlmy") (f (quote (("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.19.1 (c (n "dia-files") (v "0.19.1") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "1a10b1gg84p79rwj5lyq31mslv122vrjy0l9323f95qlhwk44dmx") (f (quote (("pub-libc") ("pub-async-std") ("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.20.0 (c (n "dia-files") (v "0.20.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "1bvm6xwzjwddjkx4m400afj0wdf6ka6pns6dkpvjiyynk424qi7h") (f (quote (("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.20.1 (c (n "dia-files") (v "0.20.1") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0w6d3wyqmnbkd3b9vqvxvwfzx8vc9shky3prwgyicn6102l7rxv8") (f (quote (("lib-c" "libc") ("async_std" "async-std"))))))

(define-public crate-dia-files-0.21.0 (c (n "dia-files") (v "0.21.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "0nwiypr9n3rh6lksa84fmsifpdbcvqfs1ynifh4lh59k8v5b6cny") (s 2) (e (quote (("libc" "dep:libc") ("async-std" "dep:async-std"))))))

(define-public crate-dia-files-0.22.0 (c (n "dia-files") (v "0.22.0") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "02z73gs0zr9qbksivi60xsvlkmblvv28s5gxfk35r7n1b3azr9v4") (s 2) (e (quote (("libc" "dep:libc") ("async-std" "dep:async-std"))))))

(define-public crate-dia-files-0.22.1 (c (n "dia-files") (v "0.22.1") (d (list (d (n "async-std") (r ">=1, <2") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)))) (h "1lp9dfriagcjr6p6v6198pwzxik3mjbhdpiqly36i62lfmxxf069") (s 2) (e (quote (("libc" "dep:libc") ("async-std" "dep:async-std"))))))

