(define-module (crates-io di a- dia-hammer) #:use-module (crates-io))

(define-public crate-dia-hammer-0.2.0 (c (n "dia-hammer") (v "0.2.0") (d (list (d (n "dia-args") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1sgprnyl8vigan1nkzgxnnq04y1xka5k1fc7sr31lv8aw952w0na") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-0.3.0 (c (n "dia-hammer") (v "0.3.0") (d (list (d (n "dia-args") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^0.3") (o #t) (d #t) (k 0)))) (h "06chi6byh21gyha3nng6y48swzqzdx69g4j3xzdkm0rp7hcbwnw0") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-0.4.0 (c (n "dia-hammer") (v "0.4.0") (d (list (d (n "dia-args") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0pxcfirjc3rg0vnwv2ljwhv2ira7s6zmhc4j3g7iwham2i8jby49") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-0.4.1 (c (n "dia-hammer") (v "0.4.1") (d (list (d (n "dia-args") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^0.3") (o #t) (d #t) (k 0)))) (h "08nzsr06d5ccnszy0k4vdpbvvd5zb1zk62zas6cwais3fnhzpdcf") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-0.5.0 (c (n "dia-hammer") (v "0.5.0") (d (list (d (n "dia-args") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0gvp5jxn004sjnpwgzr75996g2vph6m4blyaxd8pk77s2rmyi6lg") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.0.0 (c (n "dia-hammer") (v "1.0.0") (d (list (d (n "dia-args") (r "^0.28.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^1") (o #t) (d #t) (k 0)))) (h "1fjc3xqkisbbahkjc46fcs8jg126wgvls874asx8wn0vb5433gza") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.0.1 (c (n "dia-hammer") (v "1.0.1") (d (list (d (n "dia-args") (r "^0.28.1") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^1") (o #t) (d #t) (k 0)))) (h "15zmsh249xgg0wr2w7wjar2fxjjyl1650jkmd6sgk8d1k985gjnk") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.1.0 (c (n "dia-hammer") (v "1.1.0") (d (list (d (n "dia-args") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^2.1") (o #t) (d #t) (k 0)))) (h "0w605c1mbpfavj4vif4qjffanmicihlhzhcfcvpydczh6bidp2wl") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.2.0 (c (n "dia-hammer") (v "1.2.0") (d (list (d (n "dia-args") (r "^0.46") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "1l4ddk10v6hx7kalk1w8rg36cc5bigwdznv60dprwrysagq6xlm8") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.3.0 (c (n "dia-hammer") (v "1.3.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "12y0gj111k1fajr2wdx4yapmzdzlaj7my9d29s0an2hgs2wlwpk2") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-1.4.0 (c (n "dia-hammer") (v "1.4.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^5.2") (o #t) (d #t) (k 0)))) (h "077sxfzylf7qbdmz3zlh8vzzx3wy2h93rl489jylavjwbilg8p9l") (f (quote (("bin" "dia-args" "tiny-keccak" "zeros"))))))

(define-public crate-dia-hammer-2.0.0 (c (n "dia-hammer") (v "2.0.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^9.1") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1wd3fkq3yh10na4479ii7jn3j4z6apik3a5pflx70n54vw4fw6cf") (f (quote (("bin" "dia-args" "zeros"))))))

(define-public crate-dia-hammer-2.0.1 (c (n "dia-hammer") (v "2.0.1") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10.0.2") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "1d5nyn2il7khqz1rh0nrmhij3ranyhpv7dqrnbyq5si07x1pigy6") (f (quote (("bin" "dia-args" "zeros"))))))

(define-public crate-dia-hammer-2.1.0 (c (n "dia-hammer") (v "2.1.0") (d (list (d (n "dia-args") (r "^0.52") (o #t) (d #t) (k 0)) (d (n "zeros") (r "^10.0.2") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "18nc2bvmm5vip8cl4857ixrm1n9pbfj4ahw8szxpl8ndfjiigjk7") (f (quote (("bin" "dia-args" "zeros"))))))

(define-public crate-dia-hammer-2.1.1 (c (n "dia-hammer") (v "2.1.1") (d (list (d (n "dia-args") (r ">=0.55, <0.56") (o #t) (d #t) (k 0)) (d (n "zeros") (r ">=11.1, <12") (f (quote ("std"))) (o #t) (d #t) (k 0)))) (h "0sjsvy9yala5rvjsqaqmzn3j2pyxnhaircz636w6q2ygp51x38nr") (f (quote (("bin" "dia-args" "zeros"))))))

