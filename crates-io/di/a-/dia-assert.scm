(define-module (crates-io di a- dia-assert) #:use-module (crates-io))

(define-public crate-dia-assert-0.0.3 (c (n "dia-assert") (v "0.0.3") (h "06288n84p0gxasm51adaivi06m3a2h816q529pv1v4k4354kqwsp")))

(define-public crate-dia-assert-0.0.4 (c (n "dia-assert") (v "0.0.4") (h "1411andrbq6qvnfs4zqwhx7zdy8awccx00va0caqcxn1n6q951c4")))

(define-public crate-dia-assert-0.0.5 (c (n "dia-assert") (v "0.0.5") (h "09f82ysnidkx77kx12x0jpp4pyzzvdw4dbn73w2rx0q6gky3ihjn")))

(define-public crate-dia-assert-0.0.6 (c (n "dia-assert") (v "0.0.6") (h "0ilbzff4xhlyk8q71ks5346bchr6q2cqddk8bjc8g6qvsmsg781l")))

(define-public crate-dia-assert-0.0.7 (c (n "dia-assert") (v "0.0.7") (h "10rs8mz68d8fckb6cdxdc46cqcii06j381y2s3c5873l2s691s0r")))

(define-public crate-dia-assert-1.0.0 (c (n "dia-assert") (v "1.0.0") (h "0rk5wbvadbhxlq17bdy12v83d4rlji86f8qkkm925i2vrriyha1m")))

(define-public crate-dia-assert-1.1.0 (c (n "dia-assert") (v "1.1.0") (h "1gfxf4mcw0102ia8qpmxi3f3isn2n1wvqkq2wpfyahr0w7yqnd23")))

(define-public crate-dia-assert-1.2.0 (c (n "dia-assert") (v "1.2.0") (h "1ch21bnqrw0s1lz3kd73d4aa50a25qlrx8vjymb46hgsx9glknim")))

(define-public crate-dia-assert-1.2.1 (c (n "dia-assert") (v "1.2.1") (h "1mh2bxxrm2jly3ckqh8kpia3m61ivvh8wscc58f99zahn1npwq8m")))

(define-public crate-dia-assert-1.3.0 (c (n "dia-assert") (v "1.3.0") (h "1lpq6abwvsi4zq1rrg1ihqsxkljfaihna9f73x55mna31i49yayg")))

(define-public crate-dia-assert-1.4.0 (c (n "dia-assert") (v "1.4.0") (h "1bj2qckp9dj1fhxal6ajky3kgdrfpxy04f2s78ild3laldcpg08x")))

(define-public crate-dia-assert-2.0.0 (c (n "dia-assert") (v "2.0.0") (h "1w2qjz4hb814lgblvah68yp0kqm3dy618fh81ynckxrqv44g6r40")))

(define-public crate-dia-assert-3.0.0 (c (n "dia-assert") (v "3.0.0") (h "0b656ada2kvwjaf48wdblq62wc2cqzwp65zwqjii5hdyfl8919k2")))

(define-public crate-dia-assert-4.0.0 (c (n "dia-assert") (v "4.0.0") (h "0x01zn789glg4w234j37j6s8bsvj6hy6mxi8qlvl11yjycw81aln")))

