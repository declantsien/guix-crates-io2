(define-module (crates-io di a- dia-time) #:use-module (crates-io))

(define-public crate-dia-time-0.0.2 (c (n "dia-time") (v "0.0.2") (h "09ci174j5in01bg3mgz4bfy5i59bampxamnny2f93i1hxswvfn15")))

(define-public crate-dia-time-0.1.0 (c (n "dia-time") (v "0.1.0") (h "1bqmcpidqvr70phfn8cxzx9l7vk0yqjhwbzck3g1h7nhy42fd566")))

(define-public crate-dia-time-1.0.0 (c (n "dia-time") (v "1.0.0") (h "0jqm497rlh74xc5jl6x4jbl8hh51rpbvk6g313zk33swqf4ybbam")))

(define-public crate-dia-time-1.1.0 (c (n "dia-time") (v "1.1.0") (h "0cq2jmqmbglv4868yj907kbl8cl36mgg0wgl1crvvnqzwb8ypv9r")))

(define-public crate-dia-time-1.2.0 (c (n "dia-time") (v "1.2.0") (h "0rvkfmmj2v45wjxby6v548ph7801jifvfqyf0aqsajmwah89ajsh")))

(define-public crate-dia-time-1.3.0 (c (n "dia-time") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0p7230bw6d7rqj6r3apxxi8171aq0zq4rmrxy9phannkbzmdixli")))

(define-public crate-dia-time-1.4.0 (c (n "dia-time") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1jccmzapcvq78w3ndbdbcc5abn8r3kga3adz3n1qm0bjrlqqwc1d")))

(define-public crate-dia-time-2.0.0 (c (n "dia-time") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "10k1kg24s25fprfa39saizbhkkqcz4qjcfy1djwvlmlf3p11zg6a")))

(define-public crate-dia-time-3.0.0 (c (n "dia-time") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1f60cvd49fk6mq7bai1cj2s5ql6qwp8ga41nvlbpdwv6ggj98n0d")))

(define-public crate-dia-time-4.0.0 (c (n "dia-time") (v "4.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0jrfwr2gdd3gnqj0xaj827j620h4q8nfjya13ah9xjg2r0zfr96q")))

(define-public crate-dia-time-4.1.0 (c (n "dia-time") (v "4.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1xir9gv4dc3m93vqwh10kmg52m5lskc5ql03kw15jx9sf5ys7awd") (f (quote (("unix" "libc"))))))

(define-public crate-dia-time-4.1.1 (c (n "dia-time") (v "4.1.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "00ng5vr3r8zspfh04x99i428w3m6bh8cp98bd4wl9ibra9l4vba4") (f (quote (("unix" "libc"))))))

(define-public crate-dia-time-4.2.0 (c (n "dia-time") (v "4.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0d840nq0v3dk4q659fwg3941sbpiqwxxpva114ry0dzfsi5dk070") (f (quote (("unix" "libc") ("std"))))))

(define-public crate-dia-time-4.2.1 (c (n "dia-time") (v "4.2.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1g866b3d4djpvwcrcfg19z7q573vm0hi51c8mzah78mc692b3782") (f (quote (("unix" "libc") ("std"))))))

(define-public crate-dia-time-4.2.2 (c (n "dia-time") (v "4.2.2") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0xlc09l1hv1r6zvmla77raqg7mcmjcvy85vvqjm1xcp20akg2r42") (f (quote (("unix" "libc") ("std"))))))

(define-public crate-dia-time-4.3.0 (c (n "dia-time") (v "4.3.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12idmb8dlbawymirb1lr4lsrmznj26rcixssa1pmdf5lh2dk23ai") (f (quote (("unix" "libc") ("std"))))))

(define-public crate-dia-time-5.0.0 (c (n "dia-time") (v "5.0.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0z7p0c9sabp393rqims2lbkbf5kgy1y0cf74zrj3sj2qhd6dk3sg") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-5.0.1 (c (n "dia-time") (v "5.0.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "134ygw2l9a5wn8ghjnyi5c0g5lnbhcjdwpa0jaj7f8xlvn1x0f3b") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-5.1.0 (c (n "dia-time") (v "5.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "09p9vb36975kwryp2ws990qg6c2i14hf9pfbzb83dbcwlvay321y") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-5.2.0 (c (n "dia-time") (v "5.2.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0wvi10xy35vd0xjw397wcml9ni5w0irb4nfjkvhw3i492xzdw3v1") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-5.3.0 (c (n "dia-time") (v "5.3.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "15myg641f65qi5z2l0pbh9h17wc0iibhd8gqz29bjzqjdyw8cg25") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-5.3.1 (c (n "dia-time") (v "5.3.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0lp0x4likwxzlshffb3gnyvsp16qcmyvz9nhivy307911snkc118") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-6.0.0 (c (n "dia-time") (v "6.0.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0ycgvjx1nqc89rrzcf2y26bgigskrmccx9iy7p9cx0im7pff6q0x") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-6.1.0 (c (n "dia-time") (v "6.1.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "02wv34l4bjfa3fnhrl2pfwi9v7nrsa3jjj4hd9rpdrw1k5fvx05i") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-6.2.0 (c (n "dia-time") (v "6.2.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0dc96ph1qy352gpzsryw08q75k9rqwrydw90rxmycyjz925f5lwz") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-6.2.1 (c (n "dia-time") (v "6.2.1") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0qaa63fhkzk1a3bs7d98d98jhp8gvbgibajgg1cdhga44qxsa5sg") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-6.2.2 (c (n "dia-time") (v "6.2.2") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "1m8n4d18m00v4640xg0kx7jfdypi536nkpq1bf4cmcpv3261q897") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.0 (c (n "dia-time") (v "7.0.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "04k62i965axgpq84w7rzbr0dm1q4dilypl9jaz3xl35wvqblm4vm") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.1 (c (n "dia-time") (v "7.0.1") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "02m357kx8x90kby4dgx1k0ly8bhhmyq8yyaqbqzb9lv7v8siz3xh") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.2 (c (n "dia-time") (v "7.0.2") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "13ny584a5znhsqdghzq7cdn18f7856c1lymfaq2psr8cybbda0f7") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.3 (c (n "dia-time") (v "7.0.3") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0g0brpxrb22f4l3jjhmycgd3hxqmixpd7y4n4crr41q9184ghiil") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.4 (c (n "dia-time") (v "7.0.4") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0l0zn0z1g50nflx3wxyr6qc4ykk900gkvszv963dkfaqr87awzib") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.5 (c (n "dia-time") (v "7.0.5") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "07ymxjhr7d5dl05l2wz4h42jhpx5m19rqjcwjqis0h0yij44h0z2") (f (quote (("std") ("pub-libc") ("lib-c" "libc"))))))

(define-public crate-dia-time-7.0.6 (c (n "dia-time") (v "7.0.6") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "1slnnwlzz8c1avarbfsk6gk1ybv3bp0x5vgaark1rkmmpsf839d0") (f (quote (("std") ("pub-libc") ("lib-c" "libc"))))))

(define-public crate-dia-time-8.0.0 (c (n "dia-time") (v "8.0.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0x9hskg92a1r5hf4c8ih9y3d66xlgwhavsplbh2pdm2c11czbhgb") (f (quote (("std") ("lib-c" "libc"))))))

(define-public crate-dia-time-9.0.0 (c (n "dia-time") (v "9.0.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0p5xp01nqgkq4c37v7fiq969amaw4hw5xk2dwli7avjl9vf08pxh") (f (quote (("std")))) (s 2) (e (quote (("libc" "dep:libc"))))))

