(define-module (crates-io di nv dinvoke_rs) #:use-module (crates-io))

(define-public crate-dinvoke_rs-0.1.0 (c (n "dinvoke_rs") (v "0.1.0") (d (list (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "1g5iy124mg5f60zdab4zkj2dn4gf4xpny9wqr3bksbpcv9mdjhwr")))

(define-public crate-dinvoke_rs-0.1.2 (c (n "dinvoke_rs") (v "0.1.2") (d (list (d (n "dinvoke") (r "^0.1.1") (d #t) (k 0)) (d (n "dinvoke_data") (r "^0.1.0") (d #t) (k 0)) (d (n "dinvoke_overload") (r "^0.1.0") (d #t) (k 0)) (d (n "dmanager") (r "^0.1.0") (d #t) (k 0)) (d (n "manualmap") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0fn3rz97vfkslr8y8f180kag5hkx2gmz11gyhf1031sn7x3iczzs")))

(define-public crate-dinvoke_rs-0.1.3 (c (n "dinvoke_rs") (v "0.1.3") (d (list (d (n "dinvoke") (r "^0.1.2") (d #t) (k 0)) (d (n "dinvoke_data") (r "^0.1.1") (d #t) (k 0)) (d (n "dinvoke_overload") (r "^0.1.2") (d #t) (k 0)) (d (n "dmanager") (r "^0.1.1") (d #t) (k 0)) (d (n "manualmap") (r "^0.1.1") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "138hqpays0zp1dp3h1vhmyr7sdq8daklnq4v2z2si1zx1whnx1cb")))

(define-public crate-dinvoke_rs-0.1.4 (c (n "dinvoke_rs") (v "0.1.4") (d (list (d (n "dinvoke") (r "^0.1.3") (d #t) (k 0)) (d (n "dinvoke_data") (r "^0.1.1") (d #t) (k 0)) (d (n "dinvoke_overload") (r "^0.1.3") (d #t) (k 0)) (d (n "dmanager") (r "^0.1.2") (d #t) (k 0)) (d (n "manualmap") (r "^0.1.2") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0rf2fmkjk8h7dxb15s31pkwsyz732gcp3b120bwkyjjm1wzz58a4")))

(define-public crate-dinvoke_rs-0.1.5 (c (n "dinvoke_rs") (v "0.1.5") (d (list (d (n "dinvoke") (r "^0.1.4") (d #t) (k 0)) (d (n "dinvoke_data") (r "^0.1.2") (d #t) (k 0)) (d (n "dinvoke_overload") (r "^0.1.4") (d #t) (k 0)) (d (n "dmanager") (r "^0.1.3") (d #t) (k 0)) (d (n "manualmap") (r "^0.1.3") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "1xs26i3vssh08nfs5gybvpcxnyb7ibgr7bz14a32mr95vs1hzna2")))

