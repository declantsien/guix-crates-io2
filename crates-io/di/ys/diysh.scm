(define-module (crates-io di ys diysh) #:use-module (crates-io))

(define-public crate-diysh-1.1.0 (c (n "diysh") (v "1.1.0") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "12b22sdlxf05lw1i9qvq2p075037hbnadbq1fj2ad01jjpz2fms2")))

(define-public crate-diysh-1.1.1 (c (n "diysh") (v "1.1.1") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0zafx3h8p84jzwk8hd28w86nmiq782wr1j55scy24rm2rdrnq9k0")))

(define-public crate-diysh-2.0.0 (c (n "diysh") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "1faian465zfiazzs8sg6yk4blq8vpam283phpgjjsd2p1m2k26ii")))

(define-public crate-diysh-2.0.1 (c (n "diysh") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0fjfqy1d22kcgv6g7klwqqc7sz6q0vdk6f3bd5b5lmx71kqybqg1")))

(define-public crate-diysh-2.0.2 (c (n "diysh") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "0haynx069an55j5a3478ynvfgn5y4lwzm6hvy6frvjn7k0dmhpjf")))

(define-public crate-diysh-2.1.3 (c (n "diysh") (v "2.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)))) (h "198gm82iavxgykpjrmgcyjawz583pchcl4lq520s7rip5bcb2q2w")))

