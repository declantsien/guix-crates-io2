(define-module (crates-io di ol diol-cbind) #:use-module (crates-io))

(define-public crate-diol-cbind-0.8.0 (c (n "diol-cbind") (v "0.8.0") (d (list (d (n "diol") (r "^0.8.0") (k 0)) (d (n "equator") (r "^0.2.0") (d #t) (k 0)))) (h "0hiqpi8rb8jhglnpy5c9wpyxr8v43477wyn37ihnk2vzcng41lbn")))

(define-public crate-diol-cbind-0.8.2 (c (n "diol-cbind") (v "0.8.2") (d (list (d (n "diol") (r "^0.8.2") (k 0)) (d (n "equator") (r "^0.2.0") (d #t) (k 0)))) (h "1adqnwa4b67xg9zs71ls3d9qhjbfsqd574xwxsbf5wbazn38sp7h")))

