(define-module (crates-io di ab diablo) #:use-module (crates-io))

(define-public crate-diablo-0.1.0 (c (n "diablo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)))) (h "1g0njzb7jnpvl6bk17gv9f4n9yxqygdqq5z4bvfg7rwamzyhgplh")))

