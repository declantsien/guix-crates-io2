(define-module (crates-io di s- dis-rust) #:use-module (crates-io))

(define-public crate-dis-rust-0.1.0 (c (n "dis-rust") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cazi2hqpcbazah0l8909x5j7bq6sz15jiv035lp4iasyn5bbq64")))

(define-public crate-dis-rust-0.1.1 (c (n "dis-rust") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14m664lvgylmx38h482lfjm9jf5hn5qhi19h9haa8wljrf45yqhd")))

(define-public crate-dis-rust-0.1.2 (c (n "dis-rust") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gz89l5zf15dm7562dm597gazkan74714h6why0aqbp9c3f61gwa")))

(define-public crate-dis-rust-0.1.3 (c (n "dis-rust") (v "0.1.3") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fxxzxgz0b4ai8w26wzcrlz64slvwnz9lgfj249cjw60pfvbh3kv")))

(define-public crate-dis-rust-0.2.0 (c (n "dis-rust") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "073xf9z1dc7mhqbcjhnhwpqlj847bplp2raky66qiql2vr3vjba5")))

(define-public crate-dis-rust-0.2.1 (c (n "dis-rust") (v "0.2.1") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0517z1x4a5q8i9r07m5v3zj35cypmyg3cl7b21shbb226df8f94c")))

(define-public crate-dis-rust-0.2.2 (c (n "dis-rust") (v "0.2.2") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "02xnii6i9bkm9l9qcwb2yyji9ql2kvjpzr77yhwy96y8hsyr2p71")))

(define-public crate-dis-rust-0.2.3 (c (n "dis-rust") (v "0.2.3") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "17pg5gpfw90gw0p4gijdd395hiv6gfv1fblvjgji3v5v6ffhhdrx")))

(define-public crate-dis-rust-0.2.4 (c (n "dis-rust") (v "0.2.4") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0diafw16zaz7l7ryjwgdrrf0h5lwq0pxblnkhv0cpllxf1zr7mg5")))

(define-public crate-dis-rust-0.2.5 (c (n "dis-rust") (v "0.2.5") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "1672gxrnwn84y29h7cp4v2yph1d4x6rjxrjjwnj254cl7xnc2xbd")))

(define-public crate-dis-rust-0.2.6 (c (n "dis-rust") (v "0.2.6") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jp8a5zrrdyxgzybs56jki1qljhyl8jy6vpp6qgk4c34zrv52ghw")))

(define-public crate-dis-rust-0.2.7 (c (n "dis-rust") (v "0.2.7") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p57cxpa94j31r26n9mhy3pb0lb7bfnswqzmxrklap6vnrlqhgps")))

(define-public crate-dis-rust-0.2.8 (c (n "dis-rust") (v "0.2.8") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "012011mb38bc5ggmp8sxf3am6f52gx7vj8lr3wws4w28rhri0l8j")))

(define-public crate-dis-rust-0.2.9 (c (n "dis-rust") (v "0.2.9") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p2s10q98lkk42xhgd1116rshrqvpbmhysh16mqwxb6n75ivqqad")))

(define-public crate-dis-rust-0.2.10 (c (n "dis-rust") (v "0.2.10") (d (list (d (n "bytes") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f28h3xb0nza5l3k5f9v11m1vajpdvhzffxisjsrwzagindw1id8")))

