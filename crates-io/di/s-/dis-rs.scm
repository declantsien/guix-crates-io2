(define-module (crates-io di s- dis-rs) #:use-module (crates-io))

(define-public crate-dis-rs-0.2.0 (c (n "dis-rs") (v "0.2.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 1)))) (h "03n25pjdy9158w9czsqwpgp283iz7jbdy8iyv8rvxzhdkwdglgqg") (f (quote (("default"))))))

(define-public crate-dis-rs-0.2.1 (c (n "dis-rs") (v "0.2.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 1)))) (h "0dg13w4yw69gpri39l7305xzm09m1l15l8m741g6463pk1954xrb") (f (quote (("default"))))))

(define-public crate-dis-rs-0.3.0 (c (n "dis-rs") (v "0.3.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 1)))) (h "1s0wp8qdfa3ajjax272dyd58ljvajins6rirlnh8sazqly9ns644") (f (quote (("default"))))))

(define-public crate-dis-rs-0.3.1 (c (n "dis-rs") (v "0.3.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 1)))) (h "0pkn2by3sv5wgrw32jx5ghm4d5wfvs0502ynzanag11rw5z258mg") (f (quote (("default"))))))

(define-public crate-dis-rs-0.3.2 (c (n "dis-rs") (v "0.3.2") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.21") (d #t) (k 1)))) (h "0hwdyx5pb6m9nxhr0xpzg0xjvc2lvl8vzw2r19g71im9c6i1nc88") (f (quote (("default"))))))

(define-public crate-dis-rs-0.4.0 (c (n "dis-rs") (v "0.4.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "0043xk3085zaplb76qyyf50pw717lnxz659477glkmz9ycygl1db") (f (quote (("default"))))))

(define-public crate-dis-rs-0.5.0 (c (n "dis-rs") (v "0.5.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "1gjhwv1ql8kfl5x06aabs9ipzmy2dcnkbmgkpaq9s5wwhpvb234n") (f (quote (("default"))))))

(define-public crate-dis-rs-0.5.1 (c (n "dis-rs") (v "0.5.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "13yl2iaq0znx73l9y7szpirixnr3v135gjjnb9wd4k7n05sncwwm") (f (quote (("default"))))))

(define-public crate-dis-rs-0.5.2 (c (n "dis-rs") (v "0.5.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "0yf7w3jhbcy6fb3g6ibl6cyhgdn3sbsn7l553r6xhbxvnll54gg8") (f (quote (("default"))))))

(define-public crate-dis-rs-0.6.0-beta (c (n "dis-rs") (v "0.6.0-beta") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "0sk8saqarsjybhzd7i6bmyjhg3jd1kys8f09m6dmqrnsryjglrwy") (f (quote (("default"))))))

(define-public crate-dis-rs-0.6.0-beta-2 (c (n "dis-rs") (v "0.6.0-beta-2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "1mafbxgf11s8xdfz2xl5pm2rf044g2s3rqddg8ym9r9yl2lnf8ir") (f (quote (("default"))))))

(define-public crate-dis-rs-0.6.0 (c (n "dis-rs") (v "0.6.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "1qr8g113jjwqik38wabh8la9wqb0166ns0y039i007045j5wyv9s") (f (quote (("default"))))))

(define-public crate-dis-rs-0.7.0 (c (n "dis-rs") (v "0.7.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "1zm326bk7psyn77xcykja289ya9pylipswr5ipzk0w5pn1nj9819") (f (quote (("default"))))))

(define-public crate-dis-rs-0.8.0 (c (n "dis-rs") (v "0.8.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 1)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.26") (d #t) (k 1)))) (h "08vz7fv2yz2lh2251m6n99szyh7jbwvqrdk7p3w7yyhaj4aa3nhm") (f (quote (("default"))))))

