(define-module (crates-io di rc dircpy-stable) #:use-module (crates-io))

(define-public crate-dircpy-stable-0.3.5 (c (n "dircpy-stable") (v "0.3.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0jajyq8p9nb6lfgdrinnqpij2l0pq8zj3zd2vkhd56hzyx8gawkv")))

