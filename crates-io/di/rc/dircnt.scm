(define-module (crates-io di rc dircnt) #:use-module (crates-io))

(define-public crate-dircnt-1.0.0 (c (n "dircnt") (v "1.0.0") (h "16h9gwpcm5l6v24ak7639nzlqrcmzj561ybnhbndr8mprq71yy6h")))

(define-public crate-dircnt-1.0.1 (c (n "dircnt") (v "1.0.1") (h "079qqzrn2zvil04gvg4pdjda57wj86xljhym2sznkl7yq2dmhcn0")))

(define-public crate-dircnt-1.0.2 (c (n "dircnt") (v "1.0.2") (h "0ngzmjpmdkk310x1gvc9j8ld8dngm7gq7biv9jmqn3qlggfd89d3")))

