(define-module (crates-io di rc dircmp) #:use-module (crates-io))

(define-public crate-dircmp-0.1.0 (c (n "dircmp") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0bs54qsy0lhgwpmya6a8fzgrpqzpig1wrf0qvwh3b1n98ycapvyb")))

(define-public crate-dircmp-0.2.0 (c (n "dircmp") (v "0.2.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03qgd04fgfzhivv3yygifaivgpdc2bs7krkhf1jq0y9rpaipzjn3")))

