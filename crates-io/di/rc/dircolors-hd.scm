(define-module (crates-io di rc dircolors-hd) #:use-module (crates-io))

(define-public crate-dircolors-hd-0.2.0 (c (n "dircolors-hd") (v "0.2.0") (d (list (d (n "ansi_colours") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0flb4kyynfps6dhpsd37iqrqypv68xrwcjlg7v5mbwd8bmwsqjh4")))

