(define-module (crates-io di rc dircpy) #:use-module (crates-io))

(define-public crate-dircpy-0.1.0 (c (n "dircpy") (v "0.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "043gm9l1vwaskxpj9jvnkl0hl8zh50vn6xsa4s6a838p2aj8ni66")))

(define-public crate-dircpy-0.2.0 (c (n "dircpy") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "11xaw9hh9imshg79xgjlbw1x3360ns0mcaqc8hi8nm4bkk2wl1yq")))

(define-public crate-dircpy-0.2.1 (c (n "dircpy") (v "0.2.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "138x3ijj78jgslfy7shfi4jy5fzj5r4m8gr7ydhdgzv4bqm7gzf9")))

(define-public crate-dircpy-0.3.0 (c (n "dircpy") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "13s0qlvrr2d743mnsj6ch6m8v0fkdzfh38cbz4cqjbvyq5ynrn38")))

(define-public crate-dircpy-0.3.1 (c (n "dircpy") (v "0.3.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1xjar27g7bvkg50bxq7p39zmk676wxab01s07xyvhzlbivcldfdg")))

(define-public crate-dircpy-0.3.2 (c (n "dircpy") (v "0.3.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0m7lk8ipmpwrjqsrldr8pz3dg6q88yrzhqnwkpwqmnrk3ppyz5ns")))

(define-public crate-dircpy-0.3.3 (c (n "dircpy") (v "0.3.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0cxinrj1d64hcgvvhndvk72gs660hq4nh74g6hblvp4asw32fim7")))

(define-public crate-dircpy-0.3.4 (c (n "dircpy") (v "0.3.4") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ggbhw6ji8lp0y9a0pnvv9z4wm8166alrx0yap0p8rn7qs49fizl")))

(define-public crate-dircpy-0.3.5 (c (n "dircpy") (v "0.3.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1pnqxbf058pfjl9ryvlj49ffbjrbiqi1g6x905sj1dsigdkfrfb4")))

(define-public crate-dircpy-0.3.6 (c (n "dircpy") (v "0.3.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1aam18anmk79wac09g4cvf6f9s2184397pqp0cp7rnqnbj1giym7")))

(define-public crate-dircpy-0.3.7 (c (n "dircpy") (v "0.3.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "12vsp83mqxxrdscxcs014wzq7br1rrs4w9ha5pcnhmglgafxfj7z")))

(define-public crate-dircpy-0.3.8 (c (n "dircpy") (v "0.3.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "18d8s7289n4afq4kbs9x3pc7qp7jwz01ra421xya7dsixqf0qs4r")))

(define-public crate-dircpy-0.3.9 (c (n "dircpy") (v "0.3.9") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0859gn0jlb9f37yhr5bax2ghz029l0ishjgq6j66kh5s6iindckh")))

(define-public crate-dircpy-0.3.10 (c (n "dircpy") (v "0.3.10") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1lvc1vgdlj512x6w65ll5lipcw6ncazj825nnvr3yyki5056i223") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.11 (c (n "dircpy") (v "0.3.11") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "18a3qlj68fh4gbvfrmjhk9j3a34ix1dnqrrgsim4z0088m1klym8") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.12 (c (n "dircpy") (v "0.3.12") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "07s4vyjq98q8gjzwgsdisc6882pysvjplg7l5ffiimqymwaqyvni") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.13 (c (n "dircpy") (v "0.3.13") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1afzabg6dwscpn6fa44sr1p2fh23890b2ppzl0h5433wniln5zvk") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.14 (c (n "dircpy") (v "0.3.14") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "jwalk") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0zjsdrhh6dwjxffp5j4s57cxfy1j95b4rwjgwxq0rhhdklmn5dhh") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.15 (c (n "dircpy") (v "0.3.15") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "07lc9bbskw6kn0ji10id85gdsbvgzx5svxmyzfflrnm6ik9ghrl4") (f (quote (("default" "jwalk"))))))

(define-public crate-dircpy-0.3.16 (c (n "dircpy") (v "0.3.16") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "jwalk") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "unzip") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.4") (d #t) (k 0)))) (h "1jkj888b22ln6qcvjlrl6a2za1y5j1c8f021qjzq0jf3a6vrs999") (f (quote (("default" "jwalk"))))))

