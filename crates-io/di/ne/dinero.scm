(define-module (crates-io di ne dinero) #:use-module (crates-io))

(define-public crate-dinero-0.0.1 (c (n "dinero") (v "0.0.1") (d (list (d (n "insta") (r "^1.17.1") (f (quote ("backtrace" "redactions"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0lfpfjyzk6s4jkss7xr76q6hxy10zps1mgvp5yl5xg2c1x9818k8") (y #t)))

(define-public crate-dinero-0.0.2 (c (n "dinero") (v "0.0.2") (d (list (d (n "insta") (r "^1.17.1") (f (quote ("backtrace" "redactions"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1hlpcjah2adc5kywnkhga4nmkghf2c2mnzmxy8dmsa36wk66vqvh") (y #t)))

(define-public crate-dinero-0.0.3 (c (n "dinero") (v "0.0.3") (d (list (d (n "insta") (r "^1.17.1") (f (quote ("backtrace" "redactions"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "08ks4nnshzvkg0ddljy321i5l1zlmkxb7l2j0cd087d11rbbyxj3") (y #t)))

(define-public crate-dinero-0.0.4 (c (n "dinero") (v "0.0.4") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "01rk6x6cx9hy3p36q8vdivp9l70pr2hxjb5n0m16a8bi73vwl3q3") (y #t)))

(define-public crate-dinero-0.0.5 (c (n "dinero") (v "0.0.5") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1cqn03dmrlbkwsxn0h0dbx9rm3q66b5abpl8r5wppdf1v781vlc8") (y #t)))

(define-public crate-dinero-0.0.6 (c (n "dinero") (v "0.0.6") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0i2yhfbapm0x338lml243q3bm968dpj5kdvj32mhw168nwaxc2qs") (y #t)))

(define-public crate-dinero-0.0.7 (c (n "dinero") (v "0.0.7") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1c11hwyfsh2a34wydf44bl3s921j9fqpk1gdpbwzs0ykhfar2n7z")))

(define-public crate-dinero-0.0.8 (c (n "dinero") (v "0.0.8") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0x51qawqdy9jgf3zi4g0jq7qm4zd394fbs5mg3rc4bfhfkv9sdvc")))

(define-public crate-dinero-0.0.9 (c (n "dinero") (v "0.0.9") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0kmhp1a981am5yg6h9y42qbnw27hmpskk5zrdq1r8x2sdnr2xhz0")))

(define-public crate-dinero-0.0.10 (c (n "dinero") (v "0.0.10") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1h9853hlzdn5w2h42khazfqd5j8argykxd6fg41ifr53ny2zq36h")))

(define-public crate-dinero-0.0.11 (c (n "dinero") (v "0.0.11") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0b9pswjdivsiccipyfwhvzcp7cp07nxlfyfz4kqh52zrfa20bm2z")))

