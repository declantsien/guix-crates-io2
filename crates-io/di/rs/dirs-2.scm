(define-module (crates-io di rs dirs-2) #:use-module (crates-io))

(define-public crate-dirs-2-1.1.0 (c (n "dirs-2") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.2.0") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0p3nfr709pvi2d5f3dv6yl6ia0wvx1d0mm9mi727w4vkbjvf5dsh")))

(define-public crate-dirs-2-3.0.1 (c (n "dirs-2") (v "3.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.2.0") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1x27wg2nvs04x62crx394sn0i2k220wv1g70fzp1brxp89wyma3p")))

