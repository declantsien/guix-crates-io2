(define-module (crates-io di rs dirsum) #:use-module (crates-io))

(define-public crate-dirsum-0.1.0 (c (n "dirsum") (v "0.1.0") (d (list (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "19kd1qrkawvzhr6pd4sr64g2kp8cwy44c1qcibx5gnrnm35mn244")))

(define-public crate-dirsum-0.1.1 (c (n "dirsum") (v "0.1.1") (d (list (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "083mrqys4fg8dp3fxwz7ah4rz6rk9nh7927xlpd51v6v4iyka35b")))

(define-public crate-dirsum-0.1.2 (c (n "dirsum") (v "0.1.2") (d (list (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "10xg739bhyns73pfyks674cjcbs26938h31sck9g8lr8h05dwhqx")))

