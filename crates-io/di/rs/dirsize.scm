(define-module (crates-io di rs dirsize) #:use-module (crates-io))

(define-public crate-dirsize-0.1.0 (c (n "dirsize") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kc24x3a2dsky78sjqgyj9rcwirnbvzz16sqylz7n143wdrdmpy6")))

(define-public crate-dirsize-0.1.1 (c (n "dirsize") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1k5r2f4aqw38jk5ab7gdl1qr4jhqpibph7fvavdrdjxq5r81lknr")))

(define-public crate-dirsize-0.2.0 (c (n "dirsize") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1vpaykm0hfpm5kcybkrnc4vk3xvplcr320bmr27wfnfynax796hj")))

(define-public crate-dirsize-0.3.0 (c (n "dirsize") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0bw0lzq4l20vs21amij6vpy3vnypi1wjdvvh1s6irqs77mrlvg51")))

(define-public crate-dirsize-0.4.0 (c (n "dirsize") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)))) (h "0r4rdfpcfgh632i1zlcv6a6ggv4dx4aki6ysp19i5mxlx9g8vd8f")))

(define-public crate-dirsize-0.5.0 (c (n "dirsize") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "opener") (r "^0.5.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0hfaqbnqn5g3r8fklqvka0dywy7v8dfh36ibryns33ccmhg4rckn")))

