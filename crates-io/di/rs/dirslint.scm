(define-module (crates-io di rs dirslint) #:use-module (crates-io))

(define-public crate-dirslint-0.0.2 (c (n "dirslint") (v "0.0.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0rvnyl3sgbmvdgfarmrx8d4402qazfi8lh7ggmgmx5asbwxx7ab1")))

