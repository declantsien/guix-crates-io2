(define-module (crates-io di rs dirs-cli) #:use-module (crates-io))

(define-public crate-dirs-cli-0.0.1 (c (n "dirs-cli") (v "0.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0vbhaa9jl5xvgmhnfy9xqzgjqpmj97y5grd28n2gkkcaa45hw02s")))

(define-public crate-dirs-cli-0.0.2 (c (n "dirs-cli") (v "0.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "win32console") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)))) (h "1blf0xa9mvkwxb2wdrw6c26j6qfsdw7hs6d4wm44ps6cnxmm3wsq")))

(define-public crate-dirs-cli-0.1.0 (c (n "dirs-cli") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Com" "Win32_System_Console" "Win32_System_IO" "Win32_Storage_FileSystem"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0n8gwm4dcfmpnnv6k6arr3w15dxnh1ancxyvgjf7qgk9k3iglgv0")))

