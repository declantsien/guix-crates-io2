(define-module (crates-io di rs dirs-next) #:use-module (crates-io))

(define-public crate-dirs-next-0.0.0 (c (n "dirs-next") (v "0.0.0") (h "0wwv34xk9ym672780jcyhic5124a3krah4qaw5f0r7ig8kmglga9") (y #t)))

(define-public crate-dirs-next-1.0.0 (c (n "dirs-next") (v "1.0.0") (d (list (d (n "cfg-if") (r "= 0.1.9") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1zvg5qg39pxgvv629c455ppna5vc33vs4m3b16iyy7d2b1ln7zxq")))

(define-public crate-dirs-next-1.0.1 (c (n "dirs-next") (v "1.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1a0i7ivhkgxw5fg8qalg17xdbzww5sz6njdxjmi113cy3ljgkg0w")))

(define-public crate-dirs-next-1.0.2 (c (n "dirs-next") (v "1.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1dl2dqzsrcb7qigfiwpdpakhdkpz0629pvylbj2ylyrkh1dfcdng")))

(define-public crate-dirs-next-2.0.0 (c (n "dirs-next") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1q9kr151h9681wwp6is18750ssghz6j9j7qm7qi1ngcwy7mzi35r")))

