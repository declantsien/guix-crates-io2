(define-module (crates-io di rs dirsearch) #:use-module (crates-io))

(define-public crate-dirsearch-0.1.0 (c (n "dirsearch") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "file-size") (r "^1.0.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "19s9cjvd0pm4w8mdi6g1sa89lkawfps6fvfiwfyv2azrli7yn4xb")))

