(define-module (crates-io di rs dirs-sys-next) #:use-module (crates-io))

(define-public crate-dirs-sys-next-0.0.0 (c (n "dirs-sys-next") (v "0.0.0") (h "1j1wra5siw705xasmdlgp74xfvxxcdzskdp0jl5yfxhsx14390fs") (y #t)))

(define-public crate-dirs-sys-next-0.1.0 (c (n "dirs-sys-next") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.3.0") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16iw13jarlagihbiq3n5sd8wfl7vpra089i3h8a2cfcmm2wgfq4w")))

(define-public crate-dirs-sys-next-0.1.1 (c (n "dirs-sys-next") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.3.0") (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zgy7is3h2dyf1l4sa7k065w2kvx0l12l40my4rswm2mc1gkdplr")))

(define-public crate-dirs-sys-next-0.1.2 (c (n "dirs-sys-next") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.4.0") (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kavhavdxv4phzj4l0psvh55hszwnr0rcz8sxbvx20pyqi2a3gaf")))

