(define-module (crates-io di rs dirsh) #:use-module (crates-io))

(define-public crate-dirsh-0.1.0 (c (n "dirsh") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "1iksafq4ny3c0f2q89yyglkbdp9q42k7nf1i4vk815688qcbww0p")))

(define-public crate-dirsh-0.2.0 (c (n "dirsh") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (d #t) (k 0)))) (h "0pv18csdvn799c2pqs8khqn95l6hrhxyppxc2kky1p94qhvmnfvf")))

