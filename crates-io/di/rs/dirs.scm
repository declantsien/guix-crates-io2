(define-module (crates-io di rs dirs) #:use-module (crates-io))

(define-public crate-dirs-0.1.0 (c (n "dirs") (v "0.1.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "0mkhw7cmsrv2gkpsl5h6wypja7rckwl2qf5nlws3gknflgljba47")))

(define-public crate-dirs-0.2.0 (c (n "dirs") (v "0.2.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "0fla4r2a0cxzybwsah0sn86ns2cv26pg4835wnq28vda5sgvixyh")))

(define-public crate-dirs-0.3.0 (c (n "dirs") (v "0.3.0") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "068j49057qbsqlkj41xvib6fvilz8vq1ignqwsby6kw1ny2mf18i")))

(define-public crate-dirs-0.3.1 (c (n "dirs") (v "0.3.1") (d (list (d (n "ole32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "xdg") (r "^2.0.0") (d #t) (k 0)))) (h "1kkid140x0p2kcw4r9rd1xq5qzsk9mh9yabivwx5p9jhj3ig2vi9")))

(define-public crate-dirs-1.0.0 (c (n "dirs") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19ncg94zfg39dvqbpk2j8lv858l780b1ynvilbxhm7qv9q2jfhc3")))

(define-public crate-dirs-1.0.1 (c (n "dirs") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z7mjgwfwd8wp4w3biphy63c16w8431nyn5c9fql8znsihkfdr4r")))

(define-public crate-dirs-1.0.2 (c (n "dirs") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0i2lzf7g44crvczqnvr630x1adyg0cwx8sdvs03p3bwpp7c6v9rp")))

(define-public crate-dirs-1.0.3 (c (n "dirs") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02vigc566z5i6n9wr2x8sch39qp4arn89xhhrh18fhpm3jfc0ygn")))

(define-public crate-dirs-1.0.4 (c (n "dirs") (v "1.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.2.0") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0akxx6pmlb9glfd8fzwdjh7qwrz0505mmn23ns9804gnj7l2v5w8")))

(define-public crate-dirs-1.0.5 (c (n "dirs") (v "1.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_users") (r "^0.3.0") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "009rrhzj9pxyncmm2vhlj70npg0cgggv2hjbbkiwdl9vccq8kmrz")))

(define-public crate-dirs-2.0.0 (c (n "dirs") (v "2.0.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1nqdv36pnsns6a87jnkq9wzl1afm7q2bgwbhgm8i8fkr5jnwczp0")))

(define-public crate-dirs-2.0.1 (c (n "dirs") (v "2.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.3") (d #t) (k 0)))) (h "18aggvhdg6jcgf1gz8z51rzcx4mgfgjpyb1akqrr7lq2p6lgakhw")))

(define-public crate-dirs-2.0.2 (c (n "dirs") (v "2.0.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.4") (d #t) (k 0)))) (h "1qymhyq7w7wlf1dirq6gsnabdyzg6yi2yyxkx6c4ldlkbjdaibhk")))

(define-public crate-dirs-3.0.0 (c (n "dirs") (v "3.0.0") (d (list (d (n "cfg-if") (r "=0.1.9") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.5") (d #t) (k 0)))) (h "0jc5ykjplvgkzb9994d9chmhn7aff5zwisq69qw555cg1mhw7p9g")))

(define-public crate-dirs-3.0.1 (c (n "dirs") (v "3.0.1") (d (list (d (n "dirs-sys") (r "^0.3.5") (d #t) (k 0)))) (h "1zxrb3anxsh80mnp2il7awccv0s5gvy7djn6gis18nbm0bnraa8l")))

(define-public crate-dirs-3.0.2 (c (n "dirs") (v "3.0.2") (d (list (d (n "dirs-sys") (r "^0.3.6") (d #t) (k 0)))) (h "028kqy0vrbfgrk1yc1flq2fqh8snyg17qlygawm0r79w211s1fih")))

(define-public crate-dirs-4.0.0 (c (n "dirs") (v "4.0.0") (d (list (d (n "dirs-sys") (r "^0.3.6") (d #t) (k 0)))) (h "0n8020zl4f0frfnzvgb9agvk4a14i1kjz4daqnxkgslndwmaffna")))

(define-public crate-dirs-5.0.0 (c (n "dirs") (v "5.0.0") (d (list (d (n "dirs-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1kbfyhyw7ycf2g26rp1fi5x84dbcilyfpqy2iajy6lrkrnd05kny")))

(define-public crate-dirs-5.0.1 (c (n "dirs") (v "5.0.1") (d (list (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0992xk5vx75b2x91nw9ssb51mpl8x73j9rxmpi96cryn0ffmmi24")))

