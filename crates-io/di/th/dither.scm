(define-module (crates-io di th dither) #:use-module (crates-io))

(define-public crate-dither-0.2.0 (c (n "dither") (v "0.2.0") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1jakg909shgv0c21mnaizhfxbfxanpzvswlfghnn0lskppkggam8")))

(define-public crate-dither-0.2.1 (c (n "dither") (v "0.2.1") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0wncq3rnv7ffbff9rc1jdnrgf7i5djixsk048ay41mmsh7s5g287")))

(define-public crate-dither-0.3.0 (c (n "dither") (v "0.3.0") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0hxf195rnsfa6czgbp96pv98w4rl8wryar6lmslc9f906b2mgck0")))

(define-public crate-dither-0.3.1 (c (n "dither") (v "0.3.1") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "035w1mcm3rw8hj2xpmcyj916m0bxc1zch7ilqxdgp2mnxcfbs4s6")))

(define-public crate-dither-0.3.2 (c (n "dither") (v "0.3.2") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0fq8v9629sky8jjlqy0nr6bff6pyyr8j7ab9g1b2gggf0rqnrbfq")))

(define-public crate-dither-0.3.3 (c (n "dither") (v "0.3.3") (d (list (d (n "png") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "06fz1r4qvn7p4l5g0y3g5gx90h4gpawc56b39k38bzcvf5q8a07m")))

(define-public crate-dither-1.0.0 (c (n "dither") (v "1.0.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1fjq15rv9yb42ar5v1d7k86b2khdrcys9x2bpyhk7wmj19w6l0yk")))

(define-public crate-dither-1.0.1 (c (n "dither") (v "1.0.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1cmylpl3nvlhr4jvbhqgbr48xh0wg8miam2mv1zs1d3zzs2zk316")))

(define-public crate-dither-1.0.2 (c (n "dither") (v "1.0.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "15qzl9xqypliaxhgnxw6f0gla9s9pfbdrxwy6p2k8ifham0rbnln")))

(define-public crate-dither-1.0.3 (c (n "dither") (v "1.0.3") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1hyb9l4sqajs5m3jsz3i97b1fc1zd3f4802bs24kgs41dmncyq5a")))

(define-public crate-dither-1.0.4 (c (n "dither") (v "1.0.4") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1r1nnviqyphcnl0xrn7kl11rq82qs5vmp4cbjkd6b73rd1748c48")))

(define-public crate-dither-1.0.5 (c (n "dither") (v "1.0.5") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1b2vd8shr4hpg22hgd9dv8adrann1pg3i8v8dnym7ghfjgk1xpb8")))

(define-public crate-dither-1.1.0 (c (n "dither") (v "1.1.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "11lwwj0z73pq4wb88d4ff4vgb2fy1minzc7hadddpfibj7qmibhf") (y #t)))

(define-public crate-dither-1.1.1 (c (n "dither") (v "1.1.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0dsg74qgnc07insyph51xq1nmdms56pdwspklvh08bv32qi5xb5m") (y #t)))

(define-public crate-dither-1.2.0 (c (n "dither") (v "1.2.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1a8hdn7dprqlch60hy89wf309j03cb21w5y6ldxh2yxf738ia0dr")))

(define-public crate-dither-1.2.1 (c (n "dither") (v "1.2.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0bd7sv5f79m5v48zasrxmskbw4p9p21pp1k0w719gc34r94fa9xi")))

(define-public crate-dither-1.3.0 (c (n "dither") (v "1.3.0") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "03zshgsirza6n19z0wx0v94b0h45bn0vgasz8dgg49qrnpn6cvns")))

(define-public crate-dither-1.3.1 (c (n "dither") (v "1.3.1") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1m9a92gp2gxiwi11mq9xskjzngx76cgpnhvpby8nw3q1mbgvwh5d")))

(define-public crate-dither-1.3.2 (c (n "dither") (v "1.3.2") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0k9pii7c95gq1cazqmfir1ks4m4iq42q9rpgfw3s13sn1brkyvq9")))

(define-public crate-dither-1.3.3 (c (n "dither") (v "1.3.3") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0blxqwgiizvs0w4x10yvvbqih4wq78svyqpsjxqb69zabg62ka9w")))

(define-public crate-dither-1.3.4 (c (n "dither") (v "1.3.4") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0fyjl167gk1s12fa0yzmgx95xg5pxdakgdl8r81dcrcdsbmmmhyc")))

(define-public crate-dither-1.3.5 (c (n "dither") (v "1.3.5") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0rgmir8criy9lf3x02947zj4h5hqvbaxxc6m85y6c46b5w7cwhxc") (y #t)))

(define-public crate-dither-1.3.6 (c (n "dither") (v "1.3.6") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "08hsjncja32fcdc5xsnz8qm4h1jxlndgnkfcwblh5rxbakjasc1v")))

(define-public crate-dither-1.3.7 (c (n "dither") (v "1.3.7") (d (list (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "03bjl76qkksvjxqhl0dc9n2z4ljndlcps5r1fwckffq74z8jzm7d")))

(define-public crate-dither-1.3.8 (c (n "dither") (v "1.3.8") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "0828z6k49hgjr937xw4qcwch510cndnb866wasxl7b9gwhskg91h") (y #t)))

(define-public crate-dither-1.3.9 (c (n "dither") (v "1.3.9") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "0zz45g216nk5r3wqm8xaxy8d8j28hk5aqjdk22ymsxygsjx32mry") (y #t)))

(define-public crate-dither-1.3.10 (c (n "dither") (v "1.3.10") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1qnm0n71r1wrxbld2pln15h4z42h2kk4zllqqhyci8h60vh5sszg")))

