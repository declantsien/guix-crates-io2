(define-module (crates-io di th dithor) #:use-module (crates-io))

(define-public crate-dithor-0.1.0 (c (n "dithor") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "03xpg59pkpaflkf66zyh8490hlm4g3cxb8vc2qaxwbgv5bdzdr1l")))

(define-public crate-dithor-0.2.0 (c (n "dithor") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1gpqk84190mgsviz70vj5p6jp6nx4dldyrnx3l7k6nwbx2s4w76s")))

(define-public crate-dithor-0.2.1 (c (n "dithor") (v "0.2.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "1ry3a4s8biapqvyh8g9bl5v5r612pq6rs7xw62pm0fsaj89gj4cn")))

(define-public crate-dithor-0.3.0 (c (n "dithor") (v "0.3.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0whb8b90shmckqzy1n5f79fv51z7fvy4vwb9fd6i94ny0sr6n3nv")))

(define-public crate-dithor-0.3.1 (c (n "dithor") (v "0.3.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "1wilp0ikr6x548ia1i4i1vkx8zhg1ri1nqarwrd7f3bvakli0phl")))

