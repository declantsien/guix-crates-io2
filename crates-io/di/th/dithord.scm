(define-module (crates-io di th dithord) #:use-module (crates-io))

(define-public crate-dithord-0.1.0 (c (n "dithord") (v "0.1.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0mv5isan8d9gc6pd88da55933pxra51sx4p3pyrzdgi6kfxplc19")))

(define-public crate-dithord-0.1.1 (c (n "dithord") (v "0.1.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0fjkly4jwnsb8pv1yr01x9jl9b94l4mq8lm5hmi7dwb9nz64pyi8")))

(define-public crate-dithord-0.1.2 (c (n "dithord") (v "0.1.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1c7vzai21dbfall8ds9xdxzzz0nq60wmjwjnzg54sngmpwnasdkq")))

(define-public crate-dithord-0.2.0 (c (n "dithord") (v "0.2.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1dskdlb9ch3w75y7i4d45a5m5zcj1sz47i8fsw9w74w0xn5cmjbw")))

(define-public crate-dithord-0.3.0 (c (n "dithord") (v "0.3.0") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1mg5k2m3s9njhzf3n0ymyl52ckc5gahkw7bz9v0bqzdxx3vhhaj6")))

(define-public crate-dithord-0.4.0 (c (n "dithord") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1g2abvy5v5sxrk71wdwpk4lv8ipz03vq2w63imhd2clgl3596iw1") (f (quote (("default") ("cli" "clap"))))))

(define-public crate-dithord-0.4.1 (c (n "dithord") (v "0.4.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "171iv996s5lyz82fhgm67mvb45ghg412gkfk7cxqa06nm24gs3f9") (f (quote (("default") ("cli" "clap"))))))

