(define-module (crates-io di ve dive-deco) #:use-module (crates-io))

(define-public crate-dive-deco-0.1.0 (c (n "dive-deco") (v "0.1.0") (h "094fbd7h4z6j6jrb24iq6lgar4dzg1paxpc7i1rfh31d367zhsd8")))

(define-public crate-dive-deco-0.1.1 (c (n "dive-deco") (v "0.1.1") (h "12vrmly04w72wv337s5hrphrwwrisig7hcyr15lvpsib0vya2qr4")))

(define-public crate-dive-deco-0.2.0 (c (n "dive-deco") (v "0.2.0") (h "0ry6j4nqpgia02c3np1cgqh6sccvicc9q697qhdpagw87vda1950")))

(define-public crate-dive-deco-0.3.0 (c (n "dive-deco") (v "0.3.0") (h "1dyfv02w6ig4dj0dz8lxcw9ynyfg02rb15hifvfxk6ja1lybw3b1")))

(define-public crate-dive-deco-0.4.0 (c (n "dive-deco") (v "0.4.0") (h "09zggc3iw6p6a1nd9hf140lcblqn3v3r602njlyy582jr0srjixp")))

(define-public crate-dive-deco-0.5.0 (c (n "dive-deco") (v "0.5.0") (h "1bf51c36pn48s96rs69zh7x61n8f76rvdf1y31hn3xw7qkm593fg")))

(define-public crate-dive-deco-0.5.1 (c (n "dive-deco") (v "0.5.1") (h "16297pwn9ipy8ad76hihgng57rr6ihs2i17rvs6jsnwjkvn9qhjs")))

(define-public crate-dive-deco-0.6.1 (c (n "dive-deco") (v "0.6.1") (h "0c282b8y3a8fppwcmxz67479ghcjnr636s1nri1r4s12257h0qq4")))

(define-public crate-dive-deco-1.0.0 (c (n "dive-deco") (v "1.0.0") (h "115k6wz719rm9fqjzi4lqcic1w24nv8394i23i420m6bq767awv2")))

(define-public crate-dive-deco-1.1.0 (c (n "dive-deco") (v "1.1.0") (h "1k5k1wv9m4x2cxm7cfyqk8brb8ch6g37nwck7vzzf62kad9kcikm")))

(define-public crate-dive-deco-1.2.0 (c (n "dive-deco") (v "1.2.0") (h "1gc6dy883vcy103kn929cn6c629cccmcdi8cap3r1fqhl8ja8b9x")))

