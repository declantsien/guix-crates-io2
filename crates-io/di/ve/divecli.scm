(define-module (crates-io di ve divecli) #:use-module (crates-io))

(define-public crate-divecli-0.1.0 (c (n "divecli") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05smcgcwn6hk23gss7p79nwpmv1nd7vc2qh4qbmy49fmz55a7895")))

(define-public crate-divecli-0.1.1 (c (n "divecli") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0syd4rx4gak107s8lpbar73vwvpqhhr3qz3xpqi8hc8cyd8rfv36")))

