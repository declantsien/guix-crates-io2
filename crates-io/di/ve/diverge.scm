(define-module (crates-io di ve diverge) #:use-module (crates-io))

(define-public crate-diverge-0.0.1 (c (n "diverge") (v "0.0.1") (h "0gqbv1a48fxc15143c57ab8zbzlwimvl3jpna6b42b8fyi5gr225")))

(define-public crate-diverge-0.0.2 (c (n "diverge") (v "0.0.2") (h "1s31aclcy44jpyq25qjmh09p0rrk7r569syp45v4si217cn1j7v8")))

(define-public crate-diverge-0.0.3 (c (n "diverge") (v "0.0.3") (h "0h56lvyys54a39f3jhcmw55zz54n2wmgihsxava5d0zxib445kp8")))

(define-public crate-diverge-0.0.4 (c (n "diverge") (v "0.0.4") (h "1jwg12qy6jwq1664ga5ci537h7wig42hl638y4vzn3ypliwjax5g")))

