(define-module (crates-io di ve diverter) #:use-module (crates-io))

(define-public crate-diverter-0.1.0 (c (n "diverter") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0wqi18ypyldbbnv56r7my963acmdkl1hq23vpyv9cmabn66yxp61")))

(define-public crate-diverter-1.1.0 (c (n "diverter") (v "1.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winreg"))) (d #t) (k 0)))) (h "144lxh0ivzrywpmg6k07v1j0qlqxf4vli010n67rm4sz7pc2x95b")))

(define-public crate-diverter-1.2.0 (c (n "diverter") (v "1.2.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "0g4af21p5vjqzdbkm9lbbjb6vk00ixfvc5rmd46mc7i20s0zwk7l")))

(define-public crate-diverter-1.3.0 (c (n "diverter") (v "1.3.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "1ha08cgn1p473scca4ja6y44c8z6diqff8vw0mdgzgra93mk7vc3")))

(define-public crate-diverter-1.3.1 (c (n "diverter") (v "1.3.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "0n9lsgzmn4sqh1z21n6fj1l5kyp13iks3jgd2yylw8r7fky3qd09")))

(define-public crate-diverter-2.0.0 (c (n "diverter") (v "2.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)))) (h "1r3can9fbgv0kjafaxpf3xyiggrcx5r77yavdbh0g7hh05g5vl8l")))

