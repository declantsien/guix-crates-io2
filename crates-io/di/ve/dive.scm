(define-module (crates-io di ve dive) #:use-module (crates-io))

(define-public crate-dive-0.0.0 (c (n "dive") (v "0.0.0") (d (list (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "1174695s554ncyzrrcghgg2sxn2mmkl45mgri2f8n08q51d4hpj0")))

(define-public crate-dive-0.0.1 (c (n "dive") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stronghold") (r "^0.2") (d #t) (k 0)) (d (n "wavy") (r "^0.1") (d #t) (k 0)) (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "07069bkkxxfcli3civyybqa8hmng219bm6yi8qyq5bnj6yvywrba")))

