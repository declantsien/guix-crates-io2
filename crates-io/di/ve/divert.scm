(define-module (crates-io di ve divert) #:use-module (crates-io))

(define-public crate-divert-0.1.0 (c (n "divert") (v "0.1.0") (h "1w4cn8lflw70ds4jpqgvr0r2mp9zbqihgapfz6inwb7mkb2y3vjz")))

(define-public crate-divert-0.2.0 (c (n "divert") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0v6hxsrflwwgyza8q7lgd6y3pmglpx2xk4q1zy23lb48cl7m5h34")))

(define-public crate-divert-0.3.0 (c (n "divert") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1r22yviz7n3q4g1s9nrwb7gysdnsn24gx5mi31fv6npkd2bxnl7l")))

(define-public crate-divert-0.4.0 (c (n "divert") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "08vdf94lw95ir28b6s9qpq9h4pknh958v821afxcb0zqpmcqqzx7")))

(define-public crate-divert-0.5.0 (c (n "divert") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "recastnavigation-sys") (r "^1.0.1") (f (quote ("detour_large_nav_meshes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0ljwmw07v5j2vbw78wkxdfzjcwh6v4fwzb5lq97cfi0pfiyslw69")))

(define-public crate-divert-0.6.0 (c (n "divert") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "recastnavigation-sys") (r "^1.0.3") (f (quote ("detour_large_nav_meshes"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0ic4q39izv29psygjklag2avayim9km1xycdg1qhkj4fabb9d7fg")))

