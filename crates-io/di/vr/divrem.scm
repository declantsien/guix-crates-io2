(define-module (crates-io di vr divrem) #:use-module (crates-io))

(define-public crate-divrem-0.1.0 (c (n "divrem") (v "0.1.0") (h "1ff0vqmr0rmwmxr3is6vn09ad6ldzyxkc9g8zsyr365rvha8k7xw")))

(define-public crate-divrem-1.0.0 (c (n "divrem") (v "1.0.0") (h "10nx8ipssl505knk1g42hb0w1r367nq2j2aysv0i4ppgiwgfbpb9")))

