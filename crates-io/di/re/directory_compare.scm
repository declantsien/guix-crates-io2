(define-module (crates-io di re directory_compare) #:use-module (crates-io))

(define-public crate-directory_compare-0.1.0 (c (n "directory_compare") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "02in7msn0q2v0pjicasc0szcbsqsmbfihk4sdk31nv1wbf20hsgm")))

(define-public crate-directory_compare-0.1.1 (c (n "directory_compare") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1j3m1r31621srkgz1p6c2bjhxyrrgkl7hiags38pvc5r8xp13b73")))

(define-public crate-directory_compare-0.1.2 (c (n "directory_compare") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "12z8cbv3djc6bmgy1fkaw54yvvyjirbn4xqvgb9jp1abzb6z87k7")))

(define-public crate-directory_compare-0.1.3 (c (n "directory_compare") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "06670amfzjl0p8d9xm0gqhcs5dcv9fr3xiqgvl8gb8iq688ya13h")))

(define-public crate-directory_compare-0.1.4 (c (n "directory_compare") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1gzwcjf591qikpz1qxix5x9ii84f7xxkky3wv56hnkcm3pvz6z50")))

