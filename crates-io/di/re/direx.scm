(define-module (crates-io di re direx) #:use-module (crates-io))

(define-public crate-direx-0.1.0 (c (n "direx") (v "0.1.0") (h "1k8zvz3gl918msq1cmm8lym1zrrlhcw9qxb8a3m0fmd0w8d0m92i")))

(define-public crate-direx-0.2.0 (c (n "direx") (v "0.2.0") (h "1n8bnjriziim07z85pjypckf9w7p05qrprv1jp2w64xqlbaga9zp")))

(define-public crate-direx-0.2.1 (c (n "direx") (v "0.2.1") (h "0prykyaba3zs3qvnjc80rcqi8g7bl44df8hqg1x8ka1ilpk5l9dr")))

