(define-module (crates-io di re direntryfilter) #:use-module (crates-io))

(define-public crate-direntryfilter-0.1.0 (c (n "direntryfilter") (v "0.1.0") (h "1ahqllknjjjimp7fzxagy77pqrb1zbgn7g4ajv99fffss3fvmjd5")))

(define-public crate-direntryfilter-0.2.0 (c (n "direntryfilter") (v "0.2.0") (h "10qdfw0dxlvcw274jqfnganxvf13pgqxgjdyrcl2c7yrjh7150y6")))

(define-public crate-direntryfilter-0.4.0 (c (n "direntryfilter") (v "0.4.0") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m9dx1h2qkpdvdvhg88bzsd2hggnhhv2vrl6d2rf4wpyvlggay55") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-direntryfilter-0.4.1 (c (n "direntryfilter") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jql918jv6j9wflzmlaycl55pxpyggsqrqa355zpgm4f6k8kh1kc") (s 2) (e (quote (("serde" "dep:serde"))))))

