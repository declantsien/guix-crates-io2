(define-module (crates-io di re direct-executor) #:use-module (crates-io))

(define-public crate-direct-executor-0.1.0 (c (n "direct-executor") (v "0.1.0") (d (list (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0racgq353n73xq9qwgk9zv2zrg430mh06x7a7h8z5nlsc3pivarv")))

(define-public crate-direct-executor-0.2.0 (c (n "direct-executor") (v "0.2.0") (d (list (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0w1lj3jlbxqgn40ffqmsz386j1zd2nzjv0ycdqsvj9zsns625a9m")))

(define-public crate-direct-executor-0.3.0 (c (n "direct-executor") (v "0.3.0") (d (list (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "13dzzgw5kgdxaygny7094z8l1pzlz3h64i5d8wg511w5ppca3wly")))

