(define-module (crates-io di re directip) #:use-module (crates-io))

(define-public crate-directip-0.1.0 (c (n "directip") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vrgq4w6bx8izwazjba3rmip0zmn6bx0750rbr7nnsa6lymg2sm1")))

(define-public crate-directip-0.1.1 (c (n "directip") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04jp8yh8ckjlhlskzq22b8azs6zc2s1iijr5wlcndbqi76pb0bv8")))

(define-public crate-directip-0.1.3 (c (n "directip") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jkn4mny24c8inaf0kk4vd7rs2m43lz83zk8ls30fzwkih2qmh53")))

(define-public crate-directip-0.1.4 (c (n "directip") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1afp39cn0l3a5xlcpggdv7wp5479l4hxrm5w83agkd8x1dk3jfqq")))

(define-public crate-directip-0.2.0 (c (n "directip") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gvvjkj3zd6bw0ffjpf2azpbpyxyhygbmxdmkzznlf4l6mwj9hzf")))

(define-public crate-directip-0.2.1 (c (n "directip") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zh4v4y6smjjya7ys34bhlq5kmy07mawbr7wpd4pbhjywrpj4pz6")))

(define-public crate-directip-0.2.2 (c (n "directip") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b3vdjw18xdq26f9qwjs83hvv8nnzv50aghj9jkzzbj9pyrlykld")))

(define-public crate-directip-0.2.3 (c (n "directip") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1c46p5sjh4mnvqllwfhbwc78il9056lgbrzirrqy1z6zbnpfy078")))

(define-public crate-directip-0.2.4 (c (n "directip") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mp75248y9yrvgggficm91hs2a0fqyjgpna4aw9k32j28kpgpwb0")))

(define-public crate-directip-0.2.6 (c (n "directip") (v "0.2.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("serde_derive"))) (o #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02bd7fhr775gi8hjr38cmfdibgvv169sqkml0iif494g9cmdzrls") (s 2) (e (quote (("serde" "dep:serde" "dep:serde_bytes" "chrono/serde")))) (r "1.60.0")))

