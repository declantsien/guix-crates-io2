(define-module (crates-io di re directcpp) #:use-module (crates-io))

(define-public crate-directcpp-0.1.0 (c (n "directcpp") (v "0.1.0") (d (list (d (n "directcpp-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1fqx5wgwdxlxcrylxdsvc7697g4qsnmds8lx7r5gm8j3glkyv7ns") (r "1.76")))

(define-public crate-directcpp-0.1.1 (c (n "directcpp") (v "0.1.1") (d (list (d (n "directcpp-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0576rzfq8a7032b644i4qimjncmd9y4amyz1lga93g7xmg8nx4yr") (r "1.76")))

(define-public crate-directcpp-0.1.2 (c (n "directcpp") (v "0.1.2") (d (list (d (n "directcpp-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1zncjyhrin7v6isdps18pampc7jmqncxr8yhn2y8bnh59frakvx8") (r "1.76")))

(define-public crate-directcpp-0.1.3 (c (n "directcpp") (v "0.1.3") (d (list (d (n "directcpp-macro") (r "^0.1.3") (d #t) (k 0)))) (h "05m0f0q95c0f2cc36y3drfl0vnvxm8bkkvgnz6wbgrfd2ay6zxjp") (r "1.76")))

(define-public crate-directcpp-0.1.4 (c (n "directcpp") (v "0.1.4") (d (list (d (n "directcpp-macro") (r "^0.1.4") (d #t) (k 0)))) (h "032ga5ac4yh6f3yaw8qk9md9y6xn847r2772si6rx8ylhax5qj0g") (r "1.77")))

