(define-module (crates-io di re directree_macros) #:use-module (crates-io))

(define-public crate-directree_macros-0.1.0 (c (n "directree_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0303mwc58bzhy3mrrbllsj08ai917qz29wk7x114825r3n0xpk9a") (f (quote (("nightly"))))))

