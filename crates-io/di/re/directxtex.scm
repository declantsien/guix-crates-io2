(define-module (crates-io di re directxtex) #:use-module (crates-io))

(define-public crate-directxtex-1.0.0 (c (n "directxtex") (v "1.0.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "09v26y270hridd7b3q8z342j9amsh6y6jbdcjh78xflramz44321")))

