(define-module (crates-io di re direct_http) #:use-module (crates-io))

(define-public crate-direct_http-0.1.0 (c (n "direct_http") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rsl4abn4fzgj4psikyp7m1h7s5ln246r97j181k58pw7xhljx4d")))

(define-public crate-direct_http-0.2.0 (c (n "direct_http") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "046cf0j91qcawgppprszvcds1a65fwkwkvxkqas3xjnf1zn3jqq2")))

(define-public crate-direct_http-0.3.0 (c (n "direct_http") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pgvfdgbjdmkww0d0qdcz7481klw1qb5np0jk06z5d6gcl78jvvq")))

(define-public crate-direct_http-0.4.0 (c (n "direct_http") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "0gsii2mciwfci63v58pyflrijkl3pkrhyybmihh3g053dkvz8gvg")))

(define-public crate-direct_http-0.5.0 (c (n "direct_http") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgzhpzi5dfkfb7vz95sa10ckhm8vgcn9aps5i0nhr8imxhsfa2c")))

(define-public crate-direct_http-0.5.1 (c (n "direct_http") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)))) (h "1c3hy6drqdx4h363h9j58hp87xm1z50bh4gb7922q3karqidyxw6")))

(define-public crate-direct_http-0.5.2 (c (n "direct_http") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)))) (h "0fsm0iw2rkvban4hkn1fsyxvd9ywhqga89cb25br49blkz355bzw")))

(define-public crate-direct_http-0.5.3 (c (n "direct_http") (v "0.5.3") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)))) (h "1fakadwniqp3i1q30gav3pm12frdmaz4h14fd2xzkcq78bb7br2v")))

(define-public crate-direct_http-0.5.4 (c (n "direct_http") (v "0.5.4") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)))) (h "0bmhd2hlz5l9lnifv9fw26qh16nbjziqdrabg5qn4xgsp877xvb0")))

(define-public crate-direct_http-0.5.5 (c (n "direct_http") (v "0.5.5") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "smol") (r "^1.3.0") (d #t) (k 0)))) (h "15zk1aw0v52aj9jq3h25i8879b4p98k559wq87c7c44wgg7bzvdz")))

