(define-module (crates-io di re directories-next) #:use-module (crates-io))

(define-public crate-directories-next-0.0.0 (c (n "directories-next") (v "0.0.0") (h "0zy33l6fm4j6mj860lnr5jjmagwycnb0r8rzgsykhhwhh972hrcl") (y #t)))

(define-public crate-directories-next-1.0.0 (c (n "directories-next") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "= 0.1.9") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1533amv18z26a1zj2zrxk4ky3lfpw0ks4bxd2zd6ql41w258pszh")))

(define-public crate-directories-next-1.0.1 (c (n "directories-next") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "0j2sb1rmhn2lkfacq1qasa1rl7c8cv2iff1qqwnpxv2vji7a1si1")))

(define-public crate-directories-next-1.0.2 (c (n "directories-next") (v "1.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "10c0l8g8vcqawr71ylhqp2s72kdw2vrkghnscj68nl0in8bjk6p9")))

(define-public crate-directories-next-1.0.3 (c (n "directories-next") (v "1.0.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "0mmym1h9vlyapwlzygfas3q9mx03mki8cnf5y1bmr713q7mwqa4a")))

(define-public crate-directories-next-2.0.0 (c (n "directories-next") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs-sys-next") (r "^0.1") (d #t) (k 0)))) (h "1g1vq8d8mv0vp0l317gh9y46ipqg2fxjnbc7lnjhwqbsv4qf37ik")))

