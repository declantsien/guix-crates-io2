(define-module (crates-io di re directcpp-macro) #:use-module (crates-io))

(define-public crate-directcpp-macro-0.1.0 (c (n "directcpp-macro") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1h6gqacvc8bnz44523s3xw9p7jd7wk7r83vblkm63jb243lvk7x6") (r "1.76")))

(define-public crate-directcpp-macro-0.1.1 (c (n "directcpp-macro") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0amp6ql7jmzlb5z24ack0mp9m4wnasybpxva25xl31w4rnnmar7x") (r "1.76")))

(define-public crate-directcpp-macro-0.1.2 (c (n "directcpp-macro") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "084jdvjcvhbd34zmvnw8478m17w3fv48y3zzbymfkcbpwyhp8gxr") (r "1.76")))

(define-public crate-directcpp-macro-0.1.3 (c (n "directcpp-macro") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1m1zh56yackimiqzz6z2750rbrahcdla5dxhx0hxxclnizziz03f") (r "1.76")))

(define-public crate-directcpp-macro-0.1.4 (c (n "directcpp-macro") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0dn62bfwbyhbb3axzp9hwg4b92ajk2yx4ngam31wc7lf80wwfdnl") (r "1.76")))

