(define-module (crates-io di re direct-gui) #:use-module (crates-io))

(define-public crate-direct-gui-0.1.0 (c (n "direct-gui") (v "0.1.0") (d (list (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "00imjn4d0l5zh6ag67g42c5x6cqvgqxffrfca2zabcjpf9yj1zm3")))

(define-public crate-direct-gui-0.1.1 (c (n "direct-gui") (v "0.1.1") (d (list (d (n "blit") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0q6r56i52hnvgiaxx4mfsvrn2zbkdpbi41ba0idq7zyxz35hs05k")))

(define-public crate-direct-gui-0.1.2 (c (n "direct-gui") (v "0.1.2") (d (list (d (n "blit") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1ycx4zk4pskc8n2cz1326k3yska0ngqpl2ssd8834941w4rd4rsv")))

(define-public crate-direct-gui-0.1.3 (c (n "direct-gui") (v "0.1.3") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0sn1f8xkzzqsyz5ckb77j2zsbmrnlfdb4h9w2p55vjvmvbzwha1m")))

(define-public crate-direct-gui-0.1.4 (c (n "direct-gui") (v "0.1.4") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "03pvdfgxziai4z8746pa7xl704pm78w5n9wx6lb0zdwgjmx0flzb")))

(define-public crate-direct-gui-0.1.5 (c (n "direct-gui") (v "0.1.5") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "073ifpak1602rx3wxsrzysgh266s0563j34br0k68axlmzmv28vg")))

(define-public crate-direct-gui-0.1.6 (c (n "direct-gui") (v "0.1.6") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0d81n428y6j8w1k50ddjg7da16hc7xy47723lzwy8vb5aixradvg")))

(define-public crate-direct-gui-0.1.7 (c (n "direct-gui") (v "0.1.7") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1ywzz6xp5g8ag616rmzcy7kgh04acnpfgv2amy39h7mgsxidcdxi")))

(define-public crate-direct-gui-0.1.8 (c (n "direct-gui") (v "0.1.8") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "137aaimjpqyqv4jbx40wymylafrmlcqk7icwiykha2hld7h7j9b9")))

(define-public crate-direct-gui-0.1.9 (c (n "direct-gui") (v "0.1.9") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "06k0jyzwj6zdrc70yyp9qs80h7fmdwdwzfqznw4m53pwp89yykxn")))

(define-public crate-direct-gui-0.1.10 (c (n "direct-gui") (v "0.1.10") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0c9szyh0yzfhmi06h6gx55fch884w0ldc0qwzvhyppkay7ga2fny")))

(define-public crate-direct-gui-0.1.11 (c (n "direct-gui") (v "0.1.11") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "13y6b5nxbp9ihq3cs5nrbbzq1jkqq4hi6mk7yfm4yapc01vbhbxx")))

(define-public crate-direct-gui-0.1.12 (c (n "direct-gui") (v "0.1.12") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1g95qm4dk8yv40r8gzk8nq1wwjzq3z8z4kxvfagyngjcgays1198")))

(define-public crate-direct-gui-0.1.13 (c (n "direct-gui") (v "0.1.13") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0wx6z3jp7c6xxjsaw8xr6laxpa9bl8f6vkfzpd3s3dagfqgqnkcz")))

(define-public crate-direct-gui-0.1.14 (c (n "direct-gui") (v "0.1.14") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0igl7q6fh0iwyjpzbppxjf7faqfdnmdv94j1ql8gk6p8lxi343jk")))

(define-public crate-direct-gui-0.1.15 (c (n "direct-gui") (v "0.1.15") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1hnqnla4h3004vak2g7m385nf8fqwq1d00gjvjnlvj1x8gdag607")))

(define-public crate-direct-gui-0.1.16 (c (n "direct-gui") (v "0.1.16") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1k2i8218nnh1cfrkkcyg1bw9ahq5cfxsrxwwz0qijy7syq19lhq7")))

(define-public crate-direct-gui-0.1.17 (c (n "direct-gui") (v "0.1.17") (d (list (d (n "blit") (r "^0.4") (d #t) (k 0)) (d (n "blit") (r "^0.4") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0r5cahxxw67l04v4sikni4pqcw33sl1jbyjqwbp6fb5zrfr2hmh8")))

(define-public crate-direct-gui-0.1.18 (c (n "direct-gui") (v "0.1.18") (d (list (d (n "blit") (r "^0.5") (d #t) (k 0)) (d (n "blit") (r "^0.5") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0y1kqbp85hq311r57q76b400c9v9as25xmrhv3a0sss2igc6dznq")))

(define-public crate-direct-gui-0.1.19 (c (n "direct-gui") (v "0.1.19") (d (list (d (n "blit") (r "^0.5") (d #t) (k 0)) (d (n "blit") (r "^0.5") (d #t) (k 1)) (d (n "image") (r "^0.18") (d #t) (k 0)) (d (n "image") (r "^0.18") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "0by6jh0jvc6rxlixrzwfc071fgjvq34aqcz6rdsw40yj51zbh0sl")))

(define-public crate-direct-gui-0.1.20 (c (n "direct-gui") (v "0.1.20") (d (list (d (n "blit") (r "^0.5") (d #t) (k 0)) (d (n "blit") (r "^0.5") (d #t) (k 1)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 1)) (d (n "minifb") (r "^0.10") (d #t) (k 2)))) (h "1laviry44cf381b8w9yibiv5pxcdxr0qr1qd6cvi706hcr38jdfx")))

(define-public crate-direct-gui-0.1.21 (c (n "direct-gui") (v "0.1.21") (d (list (d (n "blit") (r "^0.5") (d #t) (k 0)) (d (n "blit") (r "^0.5") (d #t) (k 1)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 1)) (d (n "minifb") (r "^0.11") (d #t) (k 2)))) (h "1w5469d5q8qzxgcdihm566ai6awd2h88w1jv5715y5rqa5jwv2dl")))

(define-public crate-direct-gui-0.1.22 (c (n "direct-gui") (v "0.1.22") (d (list (d (n "blit") (r "^0.5") (d #t) (k 0)) (d (n "blit") (r "^0.5") (d #t) (k 1)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 1)) (d (n "minifb") (r "^0.12") (d #t) (k 2)))) (h "1200qx6mssvy0rsgk21c9a2nch6fhxv3vivn5fy5vxvdg9kl3ims")))

(define-public crate-direct-gui-0.1.23 (c (n "direct-gui") (v "0.1.23") (d (list (d (n "blit") (r "^0.5.7") (d #t) (k 0)) (d (n "blit") (r "^0.5.7") (d #t) (k 1)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 1)) (d (n "minifb") (r "^0.15.3") (d #t) (k 2)))) (h "03a8b24p7w6dp0gd3m6wxhg48rzx3j4g6w4ygzv09agjf0y7hxvk")))

(define-public crate-direct-gui-0.1.24 (c (n "direct-gui") (v "0.1.24") (d (list (d (n "blit") (r "^0.5.9") (d #t) (k 0)) (d (n "blit") (r "^0.5.9") (d #t) (k 1)) (d (n "image") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 1)) (d (n "minifb") (r "^0.15.3") (d #t) (k 2)))) (h "01d8j9f6dsjk57g6ncv7vkdpsnvirxfcr1ipvg21s9kma34qayby")))

(define-public crate-direct-gui-0.1.25 (c (n "direct-gui") (v "0.1.25") (d (list (d (n "blit") (r "^0.5.12") (d #t) (k 0)) (d (n "blit") (r "^0.5.12") (d #t) (k 1)) (d (n "image") (r "^0.23.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.3") (d #t) (k 1)) (d (n "minifb") (r "^0.16.0") (d #t) (k 2)))) (h "0mfn48s98pkinz39rl942qvskvh526jm8ib13wqjp14750sqhljc") (f (quote (("file-loading" "image") ("default" "file-loading"))))))

(define-public crate-direct-gui-0.1.26 (c (n "direct-gui") (v "0.1.26") (d (list (d (n "blit") (r "^0.5.12") (d #t) (k 0)) (d (n "blit") (r "^0.5.12") (d #t) (k 1)) (d (n "image") (r "^0.23.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.3") (d #t) (k 1)) (d (n "minifb") (r "^0.16.0") (d #t) (k 2)))) (h "1x5zmzk83vj0r8cz7hlizba4sgnli0zmlv1ydsvgsygdv7y5c37h") (f (quote (("file-loading" "image") ("default" "file-loading"))))))

