(define-module (crates-io di re directory_manager) #:use-module (crates-io))

(define-public crate-directory_manager-0.1.0 (c (n "directory_manager") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "1jw6gwhn6skw92nb26yr6k4ng8lz55ckn4fsqdi7x0arg7wg2hyp") (r "1.74")))

(define-public crate-directory_manager-0.1.1 (c (n "directory_manager") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)))) (h "0fkn860w651n7simwp3fn8lgg4ac6psxbgmld2vxlsv08vzldibf") (r "1.74")))

