(define-module (crates-io di re directory-tree) #:use-module (crates-io))

(define-public crate-directory-tree-0.0.1 (c (n "directory-tree") (v "0.0.1") (d (list (d (n "neon") (r "^0.8") (f (quote ("napi-6"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "06f2xa10i75amkckw8i0mdmac2b4i69ibhxr7xhypyq69h7d6nqd")))

