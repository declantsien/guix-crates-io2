(define-module (crates-io di re directories) #:use-module (crates-io))

(define-public crate-directories-0.0.1 (c (n "directories") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0pzhzzfsprf2a2jik78hdhlpypz3vjkhl7i8qhln8pmzpqprz2xy")))

(define-public crate-directories-0.0.2 (c (n "directories") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1233kmzz9vk9fwp9zzdbc46z9rzsmjqgzy4d5v9snphs2vxdqskh")))

(define-public crate-directories-0.0.3 (c (n "directories") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "01sgk8ym3plfvlgaj7y3j42q3r3lhrm4mqsfjqk1dgnp7fdjmz31")))

(define-public crate-directories-0.1.0 (c (n "directories") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0xx25gy3vrk7f8vvvysb2rj0yqdmdjyxckiny45gzcp8v7wnknjr")))

(define-public crate-directories-0.1.1 (c (n "directories") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1jjmb6hxpn8h1kjx5a5m0i6chgxyg1ssxcx7vm1niaxlxs7c9jwb")))

(define-public crate-directories-0.1.2 (c (n "directories") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "01napg2sbjw7kb5in47m5aa08jrqlyjaw8ygadcqn7xpvw38hf4v")))

(define-public crate-directories-0.1.3 (c (n "directories") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0fbmy4fzm1i1jldcc3k7jv1s2kn78rhkdfdf8ncx8d9gds1raf35")))

(define-public crate-directories-0.1.4 (c (n "directories") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "136vz7gsiwzxhv4sfya3351dgs1wizgimi72r45pd55xhv8l8kfg")))

(define-public crate-directories-0.1.5 (c (n "directories") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1rnqz0igyyf3yxfw812r6q9vhr5j283lmnx6b3pxqhy0r3bjq1fg")))

(define-public crate-directories-0.1.6 (c (n "directories") (v "0.1.6") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1ppn5y97n45sybffnbrzpkpw3mpgdym2fkzcwps8fh875zc10n6c")))

(define-public crate-directories-0.1.7 (c (n "directories") (v "0.1.7") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0kdvy6lgnnvzxpnc0l09qq3z04gf6m0pp968aysvxwb668h2yb18")))

(define-public crate-directories-0.1.8 (c (n "directories") (v "0.1.8") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0c3299m1mxijw8w1lc63vf0dfrkcxwrg4zzsp750dsc6spj6l9h7")))

(define-public crate-directories-0.2.0 (c (n "directories") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1f45ds77hd1vnc8irv72qz3rmsfklvphc5lyc8dfk39741lr1n70")))

(define-public crate-directories-0.3.0 (c (n "directories") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0dqyxcca7pl5ahpr9a7p5i11l6lc31bpr7zfv8a5xxmw5cgvgqs0")))

(define-public crate-directories-0.3.1 (c (n "directories") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "1hf1qzxcp1ciii3mnrwqgi4k6pp6xla4mnswp63z5a4bzfswak9q")))

(define-public crate-directories-0.3.2 (c (n "directories") (v "0.3.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "040gkzblxq9qjazfsml8fpcx5xm62mwqywcs8kfakrp0bhqj4755")))

(define-public crate-directories-0.4.0 (c (n "directories") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "15k3i1dkm44kl2w6fiyhpmwbpy9mmvcil80acfm4lwd7s0087763")))

(define-public crate-directories-0.5.0 (c (n "directories") (v "0.5.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "01d2aj97pdyf0mvhx8h206wrsmf8c3a0cks1rjxipkmc5d60qk5v")))

(define-public crate-directories-0.6.0 (c (n "directories") (v "0.6.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0g1mbwjkkx0bjby8402rans10v66lks2pklryny2hf45zq8q8xxh")))

(define-public crate-directories-0.7.0 (c (n "directories") (v "0.7.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "05fnns3bns3i4brgym8jnl3zfy4j30qfdmnk20v6xlgxkgkmlk7i")))

(define-public crate-directories-0.7.1 (c (n "directories") (v "0.7.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "18by8smyawgsfmks0w6zk09p514r7gi8s9qcx4jn97cbmgqlj2kg")))

(define-public crate-directories-0.8.0 (c (n "directories") (v "0.8.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0mpv7zs13r8az1m6snd4w2n3phzdkc0dxiqycnajcw12kq28b8f1")))

(define-public crate-directories-0.8.1 (c (n "directories") (v "0.8.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0zpjmhrmys02j1kz49yy6zjssa2imwgaw01ikjb5m6ilh45411nc")))

(define-public crate-directories-0.8.2 (c (n "directories") (v "0.8.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "12ahaxcwx7gw2jibkkhyd1z56nphfqbzkyy7k7vf6gmjalrya4yf")))

(define-public crate-directories-0.8.3 (c (n "directories") (v "0.8.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0n526bqldyldw86csmzv4zpd2d3mg2hs6ybqlxxd902dm8m100qx")))

(define-public crate-directories-0.8.4 (c (n "directories") (v "0.8.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "17fyx9kzwky9ai8j7y0x8x8b0s87lsjgs71krf60pi5h09ba9cpl")))

(define-public crate-directories-0.8.5 (c (n "directories") (v "0.8.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "18fchj1hybkf2lqlagdk69ga9jbsag4l6swgydawi0lx3nbsm6va")))

(define-public crate-directories-0.9.0 (c (n "directories") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (k 0)))) (h "0l7dkjvpa9kcfwb8m4clq1gzcg8xmzpc8v87csqy7phpvrvfmgaj")))

(define-public crate-directories-0.10.0 (c (n "directories") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1gqf0niyi8pzwcra9yhdc7s3x16f53nnfszis0hi6vqv0bdn29gw")))

(define-public crate-directories-1.0.0 (c (n "directories") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19myi7pgj6pf2jhi0gw355756ll9kdm1lxax9bgnnm714gi1jdr9")))

(define-public crate-directories-1.0.1 (c (n "directories") (v "1.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f6khyrb71k3fz5answalr9s8pb3nwm37hz2qb367iznkf5a61mi")))

(define-public crate-directories-1.0.2 (c (n "directories") (v "1.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "03ybnr3afpspias9d5p12r4pzmax5jc7ijx2ri7psq4h86k3glvj")))

(define-public crate-directories-2.0.0 (c (n "directories") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1np6iq2dijrzvdm64kcajh68251ry8r4dp9afsdm9d2a6sddsdrc")))

(define-public crate-directories-2.0.1 (c (n "directories") (v "2.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.3") (d #t) (k 0)))) (h "1169ndy3zmscqxlx747znk6hq1rs9v9l9ijmh56bpkn357h87k1c")))

(define-public crate-directories-2.0.2 (c (n "directories") (v "2.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.4") (d #t) (k 0)))) (h "071pjx760m0dccdxlhwsz9m0zl180hxwlag62bydfl54fa0pf6jm")))

(define-public crate-directories-3.0.0 (c (n "directories") (v "3.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "cfg-if") (r "=0.1.9") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.3.5") (d #t) (k 0)))) (h "06fc9qs3db1symkd33i367vl8s5ych1y8yrfwrys2d4ins0px4ms")))

(define-public crate-directories-3.0.1 (c (n "directories") (v "3.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.3.5") (d #t) (k 0)))) (h "03ysv4m6mhsc3w1xnvncd5sxf7v2dz917awq6ksx0n0bsqwxdzpq")))

(define-public crate-directories-3.0.2 (c (n "directories") (v "3.0.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.3.6") (d #t) (k 0)))) (h "1mz1khca62zd48kphdy5hn4f8r55ywkyn23pjdbkj4h32zzh15p6")))

(define-public crate-directories-4.0.0 (c (n "directories") (v "4.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.3.6") (d #t) (k 0)))) (h "13a1108whdssf3fdgf7hvid1nlwgia5bswc5shxpiw78wbg3a12z") (y #t)))

(define-public crate-directories-4.0.1 (c (n "directories") (v "4.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.3.6") (d #t) (k 0)))) (h "045jbj5y2f1fmjs9rfcw95y0vjydb2rqqhz1sdnqhdmxv96ms77m")))

(define-public crate-directories-5.0.0 (c (n "directories") (v "5.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1acv0z7vi7b8z0qn2xd8zirw8axva9351p1vwjfhi3n117l3pgkl")))

(define-public crate-directories-5.0.1 (c (n "directories") (v "5.0.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0dba6xzk79s1clqzxh2qlgzk3lmvvks1lzzjhhi3hd70hhxifjcs")))

