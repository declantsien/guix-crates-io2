(define-module (crates-io di re directx) #:use-module (crates-io))

(define-public crate-directx-0.0.0 (c (n "directx") (v "0.0.0") (h "1qi6vmh70lq3gn9dkbd5kdawyz76w2xa55riwyj5q1i1z9h8853p")))

(define-public crate-directx-0.0.1 (c (n "directx") (v "0.0.1") (d (list (d (n "com-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "directx-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "kernel32-sys") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "user32-sys") (r "*") (o #t) (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "09c6m9mqzjzh1l29rnjckkj2534mvsq973cfdhcb8z6djgjwbpii") (f (quote (("window" "kernel32-sys" "user32-sys") ("default" "window"))))))

