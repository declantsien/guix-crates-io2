(define-module (crates-io di re dire) #:use-module (crates-io))

(define-public crate-dire-0.3.1 (c (n "dire") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3.2") (f (quote ("combaseapi" "shlobj" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ci0qx91h0nm778qmfw1hsz2qg5990kmg9vxqnr7bx7w1am6xmaa")))

(define-public crate-dire-0.3.2 (c (n "dire") (v "0.3.2") (d (list (d (n "winapi") (r "^0.3.2") (f (quote ("combaseapi" "shlobj" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0akcbi1ndy2qzq331278w2iwfy8rh1grjy4d4rzn20yqxwbnzcn4")))

