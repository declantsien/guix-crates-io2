(define-module (crates-io di re dirent-sys) #:use-module (crates-io))

(define-public crate-dirent-sys-0.1.0 (c (n "dirent-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0ajlf95ggzh52d7280j4gkszgng9cz1w2hrfs8vvk1whg52k2sa9")))

(define-public crate-dirent-sys-0.2.0 (c (n "dirent-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1w8as8900i3wn5rqgnilg7dbb1zfgwd3g4mjma2l678q1p8bbf18")))

