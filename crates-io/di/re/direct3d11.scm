(define-module (crates-io di re direct3d11) #:use-module (crates-io))

(define-public crate-direct3d11-0.1.0 (c (n "direct3d11") (v "0.1.0") (d (list (d (n "dxgi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1a5g999waccdhpchk3z7bi3lvxalcw67asnjlgw52cryfnam4rq7")))

(define-public crate-direct3d11-0.1.1 (c (n "direct3d11") (v "0.1.1") (d (list (d (n "dxgi") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "04ccdz06yx0sg8sfx5bjnxfim2ag1dmklald02g24frjrb53ycyx")))

(define-public crate-direct3d11-0.1.2 (c (n "direct3d11") (v "0.1.2") (d (list (d (n "dxgi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1gnm7y2gs7s7c3wfap6901jis1y5839xicmjj2234kv5davgm06z")))

(define-public crate-direct3d11-0.1.3 (c (n "direct3d11") (v "0.1.3") (d (list (d (n "dxgi") (r "^0.1.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "10ra66jq3cbmc0gkglh9w2k2lni0vapzjjrx39hmis6dg8jx3c5h")))

(define-public crate-direct3d11-0.1.4 (c (n "direct3d11") (v "0.1.4") (d (list (d (n "dxgi") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1y09d3vc2cc5j6n1p8qs9m3yyzqazmys6012lhpz5dk0rajbp6w8")))

(define-public crate-direct3d11-0.1.5 (c (n "direct3d11") (v "0.1.5") (d (list (d (n "dxgi") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1l0wbv1rhi01mli67ls77xdshkrk9nyjkhr9l848wwmi0749915p")))

(define-public crate-direct3d11-0.1.6 (c (n "direct3d11") (v "0.1.6") (d (list (d (n "dxgi") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0whck70dg5h4l9c21bx3p0qywqmhs6k82021a8rc7gy0jkf7w17s") (y #t)))

(define-public crate-direct3d11-0.1.7 (c (n "direct3d11") (v "0.1.7") (d (list (d (n "dxgi") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1ymfyjwjrr079m677dfr5zh1gx94mwi1nvxqdz5nd84bwqlsjnii")))

(define-public crate-direct3d11-0.3.0-alpha1 (c (n "direct3d11") (v "0.3.0-alpha1") (d (list (d (n "auto-enum") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "checked-enum") (r "^0.1.1-alpha1") (d #t) (t "cfg(windows)") (k 0)) (d (n "com-wrapper") (r "^0.1.0-alpha1") (d #t) (t "cfg(windows)") (k 0)) (d (n "derive-com-wrapper") (r "^0.1.0-alpha2") (d #t) (t "cfg(windows)") (k 0)) (d (n "dxgi") (r "^0.3.0-alpha2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0ny0ykwzsx7c73ckmakn3hidhajbhabyxqqhvrrkscigmhalq7bq")))

(define-public crate-direct3d11-0.3.0-alpha2 (c (n "direct3d11") (v "0.3.0-alpha2") (d (list (d (n "auto-enum") (r "^0.2.0-alpha1") (d #t) (t "cfg(windows)") (k 0)) (d (n "checked-enum") (r "^0.1.1-alpha1") (d #t) (t "cfg(windows)") (k 0)) (d (n "com-wrapper") (r "^0.1.0-alpha2") (d #t) (t "cfg(windows)") (k 0)) (d (n "dcommon") (r "^0.3.0-alpha1") (d #t) (t "cfg(windows)") (k 0)) (d (n "derive-com-wrapper") (r "^0.1.0-alpha4") (d #t) (t "cfg(windows)") (k 0)) (d (n "dxgi") (r "^0.3.0-alpha3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("d3d11" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "wio") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "03zn030zrs1kq9n3qvm2amj5s93pzgjl12ww3c2mi2b2qxy4b87j")))

