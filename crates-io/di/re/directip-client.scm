(define-module (crates-io di re directip-client) #:use-module (crates-io))

(define-public crate-directip-client-0.1.0 (c (n "directip-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directip") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wxs2816pgd5fdm2w2ga1sv4gs5xsd3mlh5wazc21r81fr4ik7m5")))

(define-public crate-directip-client-0.1.1 (c (n "directip-client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directip") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "156sabg6alk2i4lyjh0r9dm4k00z1nsvf7454yv9hfn6qs33cyj9")))

(define-public crate-directip-client-0.1.2 (c (n "directip-client") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directip") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09kixvs4sfigr96jfp958s01h6zbg7n4wzj509rdh0cf4ppbylfw")))

(define-public crate-directip-client-0.1.3 (c (n "directip-client") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directip") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1dl0fmjix5jax2vprpiri22nbkkn0jgklw2qi33jgbajy843kmv1")))

(define-public crate-directip-client-0.2.0 (c (n "directip-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directip") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1m6aafgk6v794h44qwjcc95751z0v064vdfs92gbw5gb7bn6xh0b")))

