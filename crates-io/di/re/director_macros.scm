(define-module (crates-io di re director_macros) #:use-module (crates-io))

(define-public crate-director_macros-0.1.0 (c (n "director_macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q2dwjlv0p968vcbk4l92j1am1xxpr0xmjm2qsj5yipq1swdg9kv")))

(define-public crate-director_macros-0.2.0 (c (n "director_macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13j6gh9gz878jmsn2kzwd797n4dgzflfx1jvf24i4glgbwq91mbk")))

(define-public crate-director_macros-0.3.0 (c (n "director_macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kj7hwk9y47h9sckhl1vbg5pivblg7pb2zhbvmvcm6m5gjc21jgl")))

(define-public crate-director_macros-0.4.0 (c (n "director_macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v1z2qkl9g0ryk22n9vljr6avgcnmy8rhw6lcq8mln3bcixf014p")))

