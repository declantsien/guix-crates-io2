(define-module (crates-io di re director_core) #:use-module (crates-io))

(define-public crate-director_core-0.1.0 (c (n "director_core") (v "0.1.0") (d (list (d (n "director_macros") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1fc81bkc8pphx1am6s1iak589wihbxvxvqnz5wb2v3lih18hibbi")))

(define-public crate-director_core-0.2.0 (c (n "director_core") (v "0.2.0") (d (list (d (n "director_macros") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1dgk8mvrsjmmd3pgypg08p0hnpw31slz0k923f8cimllm16pdckj")))

(define-public crate-director_core-0.3.0 (c (n "director_core") (v "0.3.0") (d (list (d (n "director_macros") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (f (quote ("spin_no_std"))) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "14fcn156zpn03y392v33k46xw8nj4aa6nn2pwzwgv57lnb0axgdg") (f (quote (("std") ("default" "std"))))))

(define-public crate-director_core-0.4.0 (c (n "director_core") (v "0.4.0") (d (list (d (n "director_macros") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0cgq1096yv4kvl0rfcdzw1z4dvj8g7j673g1j39b4abr08criqsh") (y #t)))

(define-public crate-director_core-0.5.0 (c (n "director_core") (v "0.5.0") (d (list (d (n "director_macros") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0xqjfrjyp5fv2v2hv3hkna9pp0chfnk94xwqc0c195cbncm2jkan")))

