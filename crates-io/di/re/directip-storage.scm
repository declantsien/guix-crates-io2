(define-module (crates-io di re directip-storage) #:use-module (crates-io))

(define-public crate-directip-storage-0.0.1 (c (n "directip-storage") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "directip") (r "^0.2.6") (d #t) (k 0)) (d (n "sqlx") (r "=0.7.0") (f (quote ("sqlite" "runtime-tokio-native-tls"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1f7rbjy571vp85a5k1sn3mds1h070nki6bmcj465zrf0997wqbdy") (s 2) (e (quote (("sqlite" "dep:sqlx"))))))

