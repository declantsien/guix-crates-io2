(define-module (crates-io di re director) #:use-module (crates-io))

(define-public crate-director-0.1.0 (c (n "director") (v "0.1.0") (d (list (d (n "director_core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "19dl1jl7s14qcl4rcqmd09w2q4szk4wlahrwql08rfndx7i4rn4d")))

(define-public crate-director-0.2.0 (c (n "director") (v "0.2.0") (d (list (d (n "director_core") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0cp7iwdgdj54yg4m3613jzjikdv369xl071049jnfz8dasyv5aam")))

(define-public crate-director-0.3.0 (c (n "director") (v "0.3.0") (d (list (d (n "director_core") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mr7w9jpwv5dhm8h0bfyl74ibcycsslh4sbv4cijlnflv6islp9g")))

(define-public crate-director-0.4.0 (c (n "director") (v "0.4.0") (d (list (d (n "director_core") (r "^0.3") (k 0)))) (h "069yfb7khwsjdh4rwbfd5akyz9w5hgmv80d1b42vgldqv7ar30n3") (f (quote (("std" "director_core/default") ("default" "std"))))))

(define-public crate-director-0.5.0 (c (n "director") (v "0.5.0") (d (list (d (n "director_core") (r "^0.5") (d #t) (k 0)))) (h "0jdnh9wjks3qd1s38sh5g9agw7rvzwhlvl585kdfnn77z8kpppms")))

