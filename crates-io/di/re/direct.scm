(define-module (crates-io di re direct) #:use-module (crates-io))

(define-public crate-direct-0.1.0 (c (n "direct") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 2)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)))) (h "0l93jxwi7pw02x14nly67r2xn2sf36vcq5bqjqcr3jvy46mznl07")))

(define-public crate-direct-0.1.1 (c (n "direct") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.2") (d #t) (k 2)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)))) (h "0r5jfx5wg065iyh1v74zgfccx4ii5i03b63zr5m06xd28bhs7l7l")))

