(define-module (crates-io di re direct-asm) #:use-module (crates-io))

(define-public crate-direct-asm-0.0.1-alpha (c (n "direct-asm") (v "0.0.1-alpha") (d (list (d (n "dynasm-lib") (r "^0.1.0-alpha") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full"))) (d #t) (k 0)))) (h "03qyj21aw5242prxfpjnf8wxyrc9qb2i49h91li2zjcjnqz0al2m") (f (quote (("dynasm" "dynasm-lib") ("default" "dynasm"))))))

