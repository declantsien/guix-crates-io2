(define-module (crates-io di re directory-union) #:use-module (crates-io))

(define-public crate-directory-union-0.1.0 (c (n "directory-union") (v "0.1.0") (d (list (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06d74y6xrj3gd3bbl0snkdh67s1349qaivms7x3822gw7fq4jr1i")))

