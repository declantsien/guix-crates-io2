(define-module (crates-io di re direnv) #:use-module (crates-io))

(define-public crate-direnv-0.1.0 (c (n "direnv") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.2") (d #t) (k 0)))) (h "1qss0igzkz7f8nynf65x7q2lh3j3f416vx57rdly5z47cc830vms")))

(define-public crate-direnv-0.1.1 (c (n "direnv") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.2") (d #t) (k 0)))) (h "00hqqsyyxjg5ygd6ic12f6p85w3752pazjb12f46p1184h3rh7b9")))

