(define-module (crates-io di ma dimasctl) #:use-module (crates-io))

(define-public crate-dimasctl-0.0.0 (c (n "dimasctl") (v "0.0.0") (h "1qvbji95257l8x1fb2b3g2jdcg3aphjn5izbhrvf8y445bxjdj9z")))

(define-public crate-dimasctl-0.2.0 (c (n "dimasctl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dimas-com") (r "^0.2") (d #t) (k 0)) (d (n "dimas-commands") (r "^0.2") (d #t) (k 0)) (d (n "dimas-config") (r "^0.2") (d #t) (k 0)) (d (n "dimas-core") (r "^0.2") (d #t) (k 0)))) (h "1a77vscay988vl5bssahk4jzdb7p7y5174ww3a7ad5zsnva4v184") (r "1.77.0")))

