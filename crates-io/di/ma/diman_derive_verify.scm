(define-module (crates-io di ma diman_derive_verify) #:use-module (crates-io))

(define-public crate-diman_derive_verify-0.2.0 (c (n "diman_derive_verify") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0108yslpjn62b4w42h67964r9872ffsrc08dbdw7j1bdscvpg5hl")))

(define-public crate-diman_derive_verify-0.3.0 (c (n "diman_derive_verify") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sbgr3ijm2rvhkpgkk30l6mprzpc7bznx90lj1g6yqsa28ia1jip")))

