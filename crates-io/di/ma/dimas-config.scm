(define-module (crates-io di ma dimas-config) #:use-module (crates-io))

(define-public crate-dimas-config-0.0.1 (c (n "dimas-config") (v "0.0.1") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "json5") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1m71nhgfsnwp54v98fssq863864azw205w079xafn8jpff6y2897") (r "1.77.0")))

(define-public crate-dimas-config-0.2.0 (c (n "dimas-config") (v "0.2.0") (d (list (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "json5") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "00dymlgdg3s7770ksi7gj0nkp9w3zznyk4z05cqxfa2hw2xc5qbh") (r "1.77.0")))

