(define-module (crates-io di ma dimas-commands) #:use-module (crates-io))

(define-public crate-dimas-commands-0.0.1 (c (n "dimas-commands") (v "0.0.1") (d (list (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "dimas-com") (r "^0.0.1") (d #t) (k 0)) (d (n "dimas-config") (r "^0.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1431jxbmir7iz43y8ydkzhn519mxmblk574zasrald4iv8ik1rpy") (r "1.77.0")))

(define-public crate-dimas-commands-0.2.0 (c (n "dimas-commands") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dimas-com") (r "^0.2.0") (d #t) (k 0)) (d (n "dimas-config") (r "^0.2.0") (d #t) (k 0)) (d (n "dimas-core") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0jwpzsz64icncc0j6i5hk6pa9qsq14hd2xw06bj8l4cqhm01dj67") (r "1.77.0")))

