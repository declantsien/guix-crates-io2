(define-module (crates-io di ma dimas-core) #:use-module (crates-io))

(define-public crate-dimas-core-0.0.1 (c (n "dimas-core") (v "0.0.1") (d (list (d (n "bitcode") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1hywxli2b823aqz92myi14f9i6v2220q6x4gxbg97rcd09q1rkq5") (r "1.77.0")))

(define-public crate-dimas-core-0.2.0 (c (n "dimas-core") (v "0.2.0") (d (list (d (n "bitcode") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "01agmalj9290i46dg543zgc8b2kjcdcqzqkd7kaxihqlykicg9rk") (r "1.77.0")))

