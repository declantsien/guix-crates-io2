(define-module (crates-io di ma dimacs) #:use-module (crates-io))

(define-public crate-dimacs-0.1.0 (c (n "dimacs") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.8.0") (d #t) (k 0)))) (h "0d41kbpbrbzc4k8h523v7pymrkr3l6al9hvxc1qsmjs7wmh8ll5l")))

(define-public crate-dimacs-0.2.0 (c (n "dimacs") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)))) (h "111zar9niwbjral77by6cl2z9way0wkqsafgqzrg2brr0ckw3xkm")))

