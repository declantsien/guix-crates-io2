(define-module (crates-io di ma dimas-com) #:use-module (crates-io))

(define-public crate-dimas-com-0.0.1 (c (n "dimas-com") (v "0.0.1") (d (list (d (n "bitcode") (r "^0.6") (d #t) (k 0)) (d (n "dimas-config") (r "^0.0.1") (d #t) (k 0)) (d (n "dimas-core") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1zf7a3mgwspr847c2wj7jvrrrmrbf2kxhyki68bc8z8fiddgdqm5") (r "1.77.0")))

(define-public crate-dimas-com-0.2.0 (c (n "dimas-com") (v "0.2.0") (d (list (d (n "bitcode") (r "^0.6") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "dimas-config") (r "^0.2.0") (d #t) (k 0)) (d (n "dimas-core") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "zenoh") (r "^0.11.0-rc") (f (quote ("unstable"))) (d #t) (k 0)))) (h "0p8rqwrlrkdzrbw5ba03vqg73c8127ca2w8fsxlklr5mx6hvk5ri") (r "1.77.0")))

