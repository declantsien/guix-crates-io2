(define-module (crates-io di sw diswh) #:use-module (crates-io))

(define-public crate-diswh-0.1.0 (c (n "diswh") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ygiishif1dgx7cwx5igas0b3kn9lpv1ykvvyy84c5kaxmy4jxf5") (y #t)))

(define-public crate-diswh-0.1.1 (c (n "diswh") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1wp5hp68bnqqbxd3lf30hqrm8s665mr4kgbwl9cii53za5w77s00")))

(define-public crate-diswh-0.2.0 (c (n "diswh") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0gskbxq2ifjk4zvxa7pswzfh10l45xfg4nz1495xq13482v7rx8z") (f (quote (("async"))))))

