(define-module (crates-io di sk disk_utils) #:use-module (crates-io))

(define-public crate-disk_utils-0.0.1 (c (n "disk_utils") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "crc") (r "^1.3.0") (d #t) (k 0)))) (h "05q10caa05kqkw006ylx9248ydk1yrspl9y7ig8ixpgsp3jvv3a5")))

