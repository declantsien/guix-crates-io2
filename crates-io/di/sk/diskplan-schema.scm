(define-module (crates-io di sk diskplan-schema) #:use-module (crates-io))

(define-public crate-diskplan-schema-0.1.0 (c (n "diskplan-schema") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s2x1nxqcrqajgjz0f624ap1kw9gn9ya27msc01ykpnpr1wbmwlj")))

