(define-module (crates-io di sk disktest-lib) #:use-module (crates-io))

(define-public crate-disktest-lib-1.0.0 (c (n "disktest-lib") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "disktest-rawio") (r "^1") (d #t) (k 0)) (d (n "movavg") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "ring") (r "^0.17") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0lxv7dzhnd92359gx90zirnqa03dq945zh5wfr6i9dj1m2p01740")))

