(define-module (crates-io di sk diskspace) #:use-module (crates-io))

(define-public crate-diskspace-0.2.0 (c (n "diskspace") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1b4pdyllx4qir1yds1y4i2axish4hh4hvrw36yrx4n769hqxb5xm") (f (quote (("multiple"))))))

(define-public crate-diskspace-0.4.0 (c (n "diskspace") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0ip971q7qyyxazcgbljrfhw3cb6740ps5w6xfvw0vkbrb3np36cm")))

(define-public crate-diskspace-0.5.0 (c (n "diskspace") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0jyp28x92y4jvsfk3cifha0gzj1m4nrgadpqnczvlxfsfss2nijh")))

