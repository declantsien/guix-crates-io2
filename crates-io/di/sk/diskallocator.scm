(define-module (crates-io di sk diskallocator) #:use-module (crates-io))

(define-public crate-diskallocator-0.1.0 (c (n "diskallocator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "1z0b7rlm1i7d25dryfdw83blqvmbcch7wf4my2bh6b9wih8p53si")))

(define-public crate-diskallocator-0.1.1 (c (n "diskallocator") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.141") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)))) (h "1nnv0i0hq6my3z3vga9bp9pvh5dx6ygfc9c3haz0w4c18vqgzdjz")))

