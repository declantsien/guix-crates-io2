(define-module (crates-io di sk diskplan-traversal) #:use-module (crates-io))

(define-public crate-diskplan-traversal-0.1.0 (c (n "diskplan-traversal") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "camino") (r "^1.1.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "diskplan-config") (r "^0.1.0") (d #t) (k 0)) (d (n "diskplan-filesystem") (r "^0.1.0") (d #t) (k 0)) (d (n "diskplan-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "137n46p9ihbh8hkbgk19665c490jm4lrw2ybmdkjq2k0n0ph71sh")))

