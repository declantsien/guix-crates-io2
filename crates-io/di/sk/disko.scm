(define-module (crates-io di sk disko) #:use-module (crates-io))

(define-public crate-disko-0.1.0 (c (n "disko") (v "0.1.0") (d (list (d (n "read_write_at") (r "^0.1.0") (d #t) (k 0)))) (h "1134zrmhfy018bi47idbasq5i4hr3cjnpgsl6apnlxwkhv8p4xyv") (y #t)))

(define-public crate-disko-0.1.1 (c (n "disko") (v "0.1.1") (d (list (d (n "read_write_at") (r "^0.1.0") (d #t) (k 0)))) (h "0ailsrsz8f0477nc7m6s4012ly3ksczzf998cng3q9z6k2plhabn") (y #t)))

(define-public crate-disko-0.1.2 (c (n "disko") (v "0.1.2") (d (list (d (n "read_write_at") (r "^0.1.0") (d #t) (k 0)))) (h "0frmvzjd5p98sbz73lhv2n9yjbmppcdcw24n5mcmfzm6pr5vzmgq") (y #t)))

