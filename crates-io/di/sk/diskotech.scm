(define-module (crates-io di sk diskotech) #:use-module (crates-io))

(define-public crate-diskotech-0.0.4 (c (n "diskotech") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("std" "backtrace"))) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("color" "derive" "std" "help"))) (k 0)) (d (n "jacklog") (r "^0.1.0") (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0vr01yqjarzkwsdxc7sv7rw4yvpcs7j36pmqpmf87r77q1h7dxpb")))

(define-public crate-diskotech-0.0.5 (c (n "diskotech") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.68") (f (quote ("std" "backtrace"))) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("color" "derive" "std" "help"))) (k 0)) (d (n "jacklog") (r "^0.1.0") (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (k 0)) (d (n "tracing") (r "^0.1.37") (f (quote ("std"))) (k 0)))) (h "0vakj4lvk6gwx0lcb7g3yhwczagmmqa9acqdka15hkxss8lskf26")))

