(define-module (crates-io di sk diskmap) #:use-module (crates-io))

(define-public crate-diskmap-0.1.0 (c (n "diskmap") (v "0.1.0") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "0spalngwvj6993zm8q05m1lmjrzh94gcn6iwh6y2bd0305i8f63r")))

(define-public crate-diskmap-0.1.1 (c (n "diskmap") (v "0.1.1") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "0qg53kwxv5fzbfkm4qsdjf1zzk2dxi1rr4n0k2dmlhq7wknyg7in")))

(define-public crate-diskmap-0.1.2 (c (n "diskmap") (v "0.1.2") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1bzr7l7kgvwfq40sr825q5r506pd3c4c3k65nf51f2hkfkpv1n02")))

(define-public crate-diskmap-0.1.3 (c (n "diskmap") (v "0.1.3") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "19dzk6s7yzlcrsy9xkly9pmmyz43q4lzq2mkr36pashv44vhwgiz")))

(define-public crate-diskmap-0.2.0 (c (n "diskmap") (v "0.2.0") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1j7ylbfhz773xba44afc3immhn88908gd76kgyxachi51dcb08k4")))

