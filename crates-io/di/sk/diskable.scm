(define-module (crates-io di sk diskable) #:use-module (crates-io))

(define-public crate-diskable-0.1.0 (c (n "diskable") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1rcbjhxds4vgjmm9vwn49xs29wwj6bhdvwfb36k8p2d7cc2qkj8p")))

(define-public crate-diskable-0.1.1 (c (n "diskable") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1w146ya4fz7hrz94bmlr787ndn8brhrkjnib9dkv5z6bbxy0s0kg")))

(define-public crate-diskable-0.1.2 (c (n "diskable") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0rcmvsiha78cqnn4yn2ga3hf3xmgqlgy57w06n3lxm62q7xh273p")))

(define-public crate-diskable-0.2.0 (c (n "diskable") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)))) (h "0x3m96ny4953zgkwjg046f0ml4bxkrmk4c5s72xdcy1d6v6i4fcm")))

