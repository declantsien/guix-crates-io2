(define-module (crates-io di sk diskplan-config) #:use-module (crates-io))

(define-public crate-diskplan-config-0.1.0 (c (n "diskplan-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "camino") (r "^1.1.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "diskplan-filesystem") (r "^0.1.0") (d #t) (k 0)) (d (n "diskplan-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "elsa") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1askgrxdhh770n284lj58z7jigqclq63jpnzya1ihmycgmq9bajr")))

