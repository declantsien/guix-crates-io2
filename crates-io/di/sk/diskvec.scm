(define-module (crates-io di sk diskvec) #:use-module (crates-io))

(define-public crate-diskvec-0.1.0 (c (n "diskvec") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0gnnypm633bki9cp8rdhrlrhylmsm6ls0qbm8rpz7mnxqv4qc27r")))

(define-public crate-diskvec-0.1.1 (c (n "diskvec") (v "0.1.1") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0q831yr2rnmyx9q6nyc36a6m9xxl190xv53vq4abj93yj41c6xbl")))

(define-public crate-diskvec-0.1.2 (c (n "diskvec") (v "0.1.2") (d (list (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1dbypqm25hizp44iykdiy69d9dzqdm3jcrg3wmv0fm4q3wrjsiys")))

