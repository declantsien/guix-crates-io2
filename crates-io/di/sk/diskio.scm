(define-module (crates-io di sk diskio) #:use-module (crates-io))

(define-public crate-diskio-0.1.0 (c (n "diskio") (v "0.1.0") (d (list (d (n "flagset") (r "^0.4.3") (d #t) (k 0)))) (h "01iv78g21b6lvpyp12iv95ifqf1mjxpbhj8g9scnbgg2f3zj70yl")))

(define-public crate-diskio-0.1.1 (c (n "diskio") (v "0.1.1") (d (list (d (n "flagset") (r "^0.4.3") (d #t) (k 0)))) (h "0d3lb3p1xhi3p515sayrxbakn633hybb7j6kx9nh0v4amwvpz2r4")))

(define-public crate-diskio-0.1.2 (c (n "diskio") (v "0.1.2") (d (list (d (n "flagset") (r "^0.4.3") (d #t) (k 0)))) (h "16s1rvjnm95njx6hfvi0i0ji06s3rhj2f8vjjm3acia1ngl7x5bk")))

