(define-module (crates-io di sk diskbench) #:use-module (crates-io))

(define-public crate-diskbench-0.1.0 (c (n "diskbench") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1jja43lfn04z0incwmnwwpswsq2gawyhwigdcnfyp75kn7k630l8")))

(define-public crate-diskbench-0.1.1 (c (n "diskbench") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1d7wg2av8z17a5lqwdpi8rzds7a9sp7a54kx9bcrcn6azv66sjbw")))

(define-public crate-diskbench-0.1.2 (c (n "diskbench") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)))) (h "1hn7908p3ps7knqvl3rhg0hffbiqjyac475vk5bdb04f655d94dx")))

(define-public crate-diskbench-0.2.0 (c (n "diskbench") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "seahorse") (r "^2.0.0") (d #t) (k 0)))) (h "014918154lav2yrwwi0wc2mm1sfab8qcjxyvn3l01dnhhzbfi6k7")))

(define-public crate-diskbench-0.2.1 (c (n "diskbench") (v "0.2.1") (d (list (d (n "fastrand") (r "^1.7.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "parse-size") (r "^1.0.0") (d #t) (k 0)) (d (n "seahorse") (r "^2.0.0") (d #t) (k 0)))) (h "11n20sxyazcphr8xdgkqcqhvyly3im0g5kfj2qdgbgj72df7aiaw")))

