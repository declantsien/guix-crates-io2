(define-module (crates-io di sk diskplan) #:use-module (crates-io))

(define-public crate-diskplan-0.1.0 (c (n "diskplan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "camino") (r "^1.1.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diskplan-config") (r "^0.1.0") (d #t) (k 0)) (d (n "diskplan-filesystem") (r "^0.1.0") (d #t) (k 0)) (d (n "diskplan-traversal") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt" "env-filter"))) (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0bgnnpxhr05skrxsvqg1npihxf9i9pzi0qgvarj5r0c2x65d5kv5")))

