(define-module (crates-io di sk disktest-rawio) #:use-module (crates-io))

(define-public crate-disktest-rawio-1.0.0 (c (n "disktest-rawio") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (t "cfg(target_os=\"windows\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "impl-default" "minwindef" "ntdef" "winerror" "errhandlingapi" "fileapi" "handleapi" "ioapiset" "winbase" "winnt" "winioctl"))) (d #t) (t "cfg(target_os=\"windows\")") (k 0)))) (h "00zj5c9nrrxqy9qvvfm3ij3knrrspzvkqp9yx8m9bnkvasavz9vn")))

