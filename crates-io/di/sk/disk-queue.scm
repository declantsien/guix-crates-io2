(define-module (crates-io di sk disk-queue) #:use-module (crates-io))

(define-public crate-disk-queue-0.1.0 (c (n "disk-queue") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "147nkwmx26x2iixnf0xy7h904wbsic968k6gbw814zvzvx7n0dj5")))

(define-public crate-disk-queue-0.1.1 (c (n "disk-queue") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0vqp0b9ia71arhrj06s87lyhxcjf7xxjpdj64yc8zkmxgy55jxxa")))

