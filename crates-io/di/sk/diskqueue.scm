(define-module (crates-io di sk diskqueue) #:use-module (crates-io))

(define-public crate-diskqueue-0.1.0 (c (n "diskqueue") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fs4") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17gka4cvl2fdi929yyvg4gkr3b4jj2hl6az5jqlcx9ikk5qd2fn1")))

