(define-module (crates-io di sk diskfire) #:use-module (crates-io))

(define-public crate-diskfire-0.1.0 (c (n "diskfire") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (f (quote ("crossbeam-channel"))) (d #t) (k 0)) (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)))) (h "1hgv8vy0ls4q86j59j3ccg9idwm1c156s31iiqq85iw63l22r8s4")))

