(define-module (crates-io di sk diskarbitration-sys) #:use-module (crates-io))

(define-public crate-diskarbitration-sys-0.0.1 (c (n "diskarbitration-sys") (v "0.0.1") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1qqlj9zj03hh3kyi5fwqy3h2i9n3a6slbv6qjbw6w03bmaznkqn3")))

(define-public crate-diskarbitration-sys-0.0.2 (c (n "diskarbitration-sys") (v "0.0.2") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1yqnw5lgdz679hdhpnf4iyshbysa18fvs7bww465z9lfq159fffv")))

(define-public crate-diskarbitration-sys-0.0.3 (c (n "diskarbitration-sys") (v "0.0.3") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1gmyrv3wl7hrr1kk03b855f9lmmm88xqpyy56ii0q9v9q0iv0bgw")))

(define-public crate-diskarbitration-sys-0.0.4 (c (n "diskarbitration-sys") (v "0.0.4") (d (list (d (n "core-foundation-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "1dgdifwnpad06lnx76mr4ycf5vn1w7b8jwz1nrhg2hjdx4m470lg")))

(define-public crate-diskarbitration-sys-0.0.5 (c (n "diskarbitration-sys") (v "0.0.5") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1xjlgid1ivg24r9zyx50s6a9l9qqr9bhn3b7bq7fjrj2s03vqnb4") (l "DiskArbitration.framework")))

(define-public crate-diskarbitration-sys-0.0.6 (c (n "diskarbitration-sys") (v "0.0.6") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1wj33ydh78jig15g3jfwxchp036q2kc61h6j2ssjw937ifxzl881") (l "DiskArbitration.framework")))

(define-public crate-diskarbitration-sys-0.0.7 (c (n "diskarbitration-sys") (v "0.0.7") (d (list (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1sq1wn8lc0ps3dj9a1d77f9f18dilwkhisds6qgpqsk5hlrc6avi") (l "DiskArbitration.framework")))

