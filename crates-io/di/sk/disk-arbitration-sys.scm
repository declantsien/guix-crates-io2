(define-module (crates-io di sk disk-arbitration-sys) #:use-module (crates-io))

(define-public crate-disk-arbitration-sys-0.1.0 (c (n "disk-arbitration-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "core-foundation-sys") (r "^0.8") (d #t) (k 0)) (d (n "io-kit-sys") (r "^0.4") (d #t) (k 0)))) (h "1zga1b5vhw4gb5ax1932zz2bb4h2vviybzciszhr0q5b5yqgmr4v") (f (quote (("macos_10_14_4_features") ("default"))))))

