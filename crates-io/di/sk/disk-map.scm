(define-module (crates-io di sk disk-map) #:use-module (crates-io))

(define-public crate-disk-map-0.1.0 (c (n "disk-map") (v "0.1.0") (d (list (d (n "advisory-lock") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1wp3zbimm6xxngvwxk9vww3lvah4nlh45pz25jggmjy6k05f0mlz") (y #t)))

