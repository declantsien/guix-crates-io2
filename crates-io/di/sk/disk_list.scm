(define-module (crates-io di sk disk_list) #:use-module (crates-io))

(define-public crate-disk_list-0.1.0 (c (n "disk_list") (v "0.1.0") (d (list (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)))) (h "0sdrjzrjjpnvcnq7if6jkgn80i2qyb0ixxxpb69yllxgvgz3w6b5")))

(define-public crate-disk_list-0.1.1 (c (n "disk_list") (v "0.1.1") (d (list (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)))) (h "1mkfwd4jl5irvvmsbl4p2zh3jsrc1qv4nksajxsrfzyi0k1ygzdm")))

(define-public crate-disk_list-0.1.2 (c (n "disk_list") (v "0.1.2") (d (list (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)))) (h "0czrlgwd759w3kkbgzzzx5y9qa2kixa595fhjsbin14zzrnbxlm4")))

(define-public crate-disk_list-0.1.3 (c (n "disk_list") (v "0.1.3") (d (list (d (n "systemstat") (r "^0.1.10") (d #t) (k 0)))) (h "1vsqrz7pjz1ss1s2rwmjfizxpbp7af7hscn9ml82nwyq6nf8wy9g")))

(define-public crate-disk_list-0.1.5 (c (n "disk_list") (v "0.1.5") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "01fjivpy9cp5x5jsw8lwnvx4r75q9pw2949aym4mhk2zr880ibw8")))

(define-public crate-disk_list-0.1.6 (c (n "disk_list") (v "0.1.6") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "0mr74invki46k37yw2c5zs6szkn425ygmzqhla0xaq3xaic07chz")))

(define-public crate-disk_list-0.1.7 (c (n "disk_list") (v "0.1.7") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "1jh4j68djz09gfvm4raxibcqsy61lnsf4ljcmfnf92sahr7a601h")))

(define-public crate-disk_list-0.2.0 (c (n "disk_list") (v "0.2.0") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "0sq936vyy58bv7fi00jhibwnxv02v6ivkq549v2iwln4scz9gni2")))

(define-public crate-disk_list-0.2.1 (c (n "disk_list") (v "0.2.1") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "0m80ak95g09jalc1jilqhrmq11ic5n4vi8i9rgqn5rgqmfp8swaz")))

(define-public crate-disk_list-0.2.2 (c (n "disk_list") (v "0.2.2") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "0vaazcyz4p3c8jvm654s2kypmbkqrs2b9xm3iqg8w1jxvd88j5wd")))

(define-public crate-disk_list-0.2.3 (c (n "disk_list") (v "0.2.3") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "12mk67p124j1k0z77xp7jws5fzzm5kf73m551v8az0lhlpz4w6x4")))

(define-public crate-disk_list-0.2.4 (c (n "disk_list") (v "0.2.4") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "1ml53kqr7a92kw1zwbqikhz55h6f3f8lkj5kg91v59xg3izw0686")))

(define-public crate-disk_list-0.2.5 (c (n "disk_list") (v "0.2.5") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "0jc51lywskqcd4lyr5nf9h5g6wkfl2gxabxfhzp72m3nxi6hjvyy")))

(define-public crate-disk_list-0.2.6 (c (n "disk_list") (v "0.2.6") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "06rkxafjyq472a2r4pp9lksn5cbkvk79qdrah54y5dlnskkm5yky")))

(define-public crate-disk_list-0.2.7 (c (n "disk_list") (v "0.2.7") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "087mg52jbph93xjgzjals470jgp6dld7f7cci20nycs99swpymqr")))

(define-public crate-disk_list-0.2.8 (c (n "disk_list") (v "0.2.8") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "008qym1a93w95y3rcmmdksdl7fcrayj2wbxfb314126f51yj2k7l")))

(define-public crate-disk_list-0.2.9 (c (n "disk_list") (v "0.2.9") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "1zw4rx5dj9fylbwyhjg2bppisbwy3hgpb6qb662q13flxxx55dm3")))

(define-public crate-disk_list-0.2.10 (c (n "disk_list") (v "0.2.10") (d (list (d (n "get_sys_info") (r "^0.1.7") (d #t) (k 0)))) (h "08xpw6as4hsm3cvv7vk64if4yiv6ngvaqhbx4m92nnpkd8q6048r")))

