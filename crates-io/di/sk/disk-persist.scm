(define-module (crates-io di sk disk-persist) #:use-module (crates-io))

(define-public crate-disk-persist-0.1.0 (c (n "disk-persist") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vknhpkfqz6qglicbhk1b589f3r6ygqdkxijbnh96dgi9nyz8q80")))

