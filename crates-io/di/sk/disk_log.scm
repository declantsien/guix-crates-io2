(define-module (crates-io di sk disk_log) #:use-module (crates-io))

(define-public crate-disk_log-0.1.0 (c (n "disk_log") (v "0.1.0") (d (list (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1hkm570nqqkq6rk4rpaqmzkkmxriw1d5iv0b6d1h8vmafaa0mydk")))

(define-public crate-disk_log-1.0.0 (c (n "disk_log") (v "1.0.0") (d (list (d (n "simple_wal") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "0fwb47shxbpwjffqijwzwwj5vvz11prb13h690grscxqhh3fywzc")))

