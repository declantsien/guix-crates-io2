(define-module (crates-io di sk diskit) #:use-module (crates-io))

(define-public crate-diskit-0.1.0 (c (n "diskit") (v "0.1.0") (d (list (d (n "either") (r "^1.7.0") (k 0)) (d (n "trash") (r "^2.1.5") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1w0yzghhr2vqybgp7ans6m9d5hf3fbrz551dhdrrmf5jgf8ijxjq") (s 2) (e (quote (("trash" "dep:trash"))))))

(define-public crate-diskit-0.1.1 (c (n "diskit") (v "0.1.1") (d (list (d (n "either") (r "^1.7.0") (k 0)) (d (n "trash") (r "^2.1.5") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "19hx2ircd18s3fnbxgjapkgl27yrygq25nniijrgsjld2ih86y7w") (s 2) (e (quote (("trash" "dep:trash"))))))

(define-public crate-diskit-0.1.2 (c (n "diskit") (v "0.1.2") (d (list (d (n "either") (r "^1.7.0") (k 0)) (d (n "trash") (r "^3.0.1") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rqnzb78fyp7vskpl6b6r0ql64fil193jlgipqa8ci6am9q8rskz") (s 2) (e (quote (("trash" "dep:trash"))))))

(define-public crate-diskit-0.1.3 (c (n "diskit") (v "0.1.3") (d (list (d (n "either") (r "^1.7.0") (k 0)) (d (n "trash") (r "^3.0.2") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "13fpp0l3mq3h2jn66razg9ipwkpcxm08wb1hhi167jqdpdn0m0jq") (s 2) (e (quote (("trash" "dep:trash"))))))

(define-public crate-diskit-0.1.4 (c (n "diskit") (v "0.1.4") (d (list (d (n "either") (r "^1.9.0") (k 0)) (d (n "trash") (r "^3.1.2") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "14ai7h5gf5l400s70gf5y32hk5x3smjds02ikmm21zpl697bmnhr") (s 2) (e (quote (("trash" "dep:trash"))))))

