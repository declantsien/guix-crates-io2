(define-module (crates-io di sk diskus) #:use-module (crates-io))

(define-public crate-diskus-0.3.0 (c (n "diskus") (v "0.3.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "13j5l1dm8w07gh7ghhr2ly7bhlqxc609fw6jwhyzj2bvfg7pdfz1")))

(define-public crate-diskus-0.4.0 (c (n "diskus") (v "0.4.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "000ijy0bgv4d7p1aq5ffahn64k7xdq05az9jld1s8q16n0qrimp8")))

(define-public crate-diskus-0.5.0 (c (n "diskus") (v "0.5.0") (d (list (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "09fix02x66ca7pddw54iiprbdwhycz9mkm942d6jgviqml07ak07")))

(define-public crate-diskus-0.6.0 (c (n "diskus") (v "0.6.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00gs59c22i2vlvvh4dym1l49h0qlylzyx7bgac9wik318a6fp58q")))

(define-public crate-diskus-0.7.0 (c (n "diskus") (v "0.7.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color" "wrap_help"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0z5nlw3d9l23qlk2arn1x7mfzn3xgmsykps5iid3v2ypx1wd7nak")))

