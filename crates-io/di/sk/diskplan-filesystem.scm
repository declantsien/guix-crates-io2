(define-module (crates-io di sk diskplan-filesystem) #:use-module (crates-io))

(define-public crate-diskplan-filesystem-0.1.0 (c (n "diskplan-filesystem") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "camino") (r "^1.1.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1xdz98w1923n8rk82gwx1xalhh96jxa3pw40c4mcfzn837bnfard")))

