(define-module (crates-io di sk disk-drive) #:use-module (crates-io))

(define-public crate-disk-drive-0.1.0 (c (n "disk-drive") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.0") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "129k04laqk92qafsf5ks4g23pzwi7asp3g5xvlihhvximaq5chsr")))

(define-public crate-disk-drive-0.1.1 (c (n "disk-drive") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.0") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1pz4c7gicv37dbdcrrjnzhypnfqxfnxz359p8bs475qqaavg1a2q")))

(define-public crate-disk-drive-0.1.2 (c (n "disk-drive") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.2") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1qqr6d04f2mq1y7xmyr30d78ymnh2nzh5v4yd0vcdizbm4s7xzdn")))

(define-public crate-disk-drive-0.1.3 (c (n "disk-drive") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.2") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1bdwkv6vjx11vh4a4jzkdw1scvmbbnwr7r530q1n56vlbiznsg3y")))

(define-public crate-disk-drive-0.1.4 (c (n "disk-drive") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.3") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1cc4jgk9z5l2570rjrza6x52n7pnfm6892i2hh7n0a316i2rjnnj")))

(define-public crate-disk-drive-0.1.5 (c (n "disk-drive") (v "0.1.5") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1infala93bg66qwcvys49i5im5ccsks1bhj65pi8hish9v1bn7hx")))

(define-public crate-disk-drive-0.1.6 (c (n "disk-drive") (v "0.1.6") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1510jd53fljr1sdc53ga7x6bfxidjyygrcra4n653ih2vs0himmy")))

(define-public crate-disk-drive-0.1.7 (c (n "disk-drive") (v "0.1.7") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1dnwmw5vcybs005cg5krmzdazqqf84pbpd5d9k3il7lb9vfbv48k")))

(define-public crate-disk-drive-0.1.8 (c (n "disk-drive") (v "0.1.8") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0ll8c370xn21kvqgq10kkxjbq1i2c64yqqlm6dn0sjcl4jrkzjzf")))

(define-public crate-disk-drive-0.1.9 (c (n "disk-drive") (v "0.1.9") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0x8bkqabzniqf1y5fjffnygaq7kj3mmzj3ryykq8lqjddjpihcd6")))

(define-public crate-disk-drive-0.1.10 (c (n "disk-drive") (v "0.1.10") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "floppy-disk") (r "^0.2.4") (d #t) (k 0)) (d (n "nyoom") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1lix4fk9b4q35z4jlcrdxcn0ac050c10gxmp55f13jvc02hlda21")))

