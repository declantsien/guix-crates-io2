(define-module (crates-io di p_ dip_task) #:use-module (crates-io))

(define-public crate-dip_task-0.2.0 (c (n "dip_task") (v "0.2.0") (d (list (d (n "dip_macro") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "sync" "macros" "fs"))) (k 0)))) (h "0ff9rzp8qhbgs0pg0gm3wivjlplrpf7776nqb3bfl3sygq99lpf2")))

