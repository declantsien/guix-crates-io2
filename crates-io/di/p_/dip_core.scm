(define-module (crates-io di p_ dip_core) #:use-module (crates-io))

(define-public crate-dip_core-0.1.0 (c (n "dip_core") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.6") (k 0)) (d (n "bevy_ecs") (r "^0.6") (k 0)) (d (n "bevy_log") (r "^0.6") (k 0)))) (h "19l9v0w9nz6glmqza590l3h31d4j4zclz4yh894xhj97a5d3n0h2")))

(define-public crate-dip_core-0.2.0 (c (n "dip_core") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "dioxus") (r "^0.2") (f (quote ("fermi"))) (d #t) (k 0)) (d (n "dip_config") (r "^0.2") (d #t) (k 0)) (d (n "dip_macro") (r "^0.2") (d #t) (k 0)) (d (n "dip_task") (r "^0.2") (d #t) (k 0)))) (h "0wfmb433073q0dlccwhynpqb56s1rs0laicxcplmlbm87kmpgfca")))

