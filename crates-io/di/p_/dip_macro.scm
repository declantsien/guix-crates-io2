(define-module (crates-io di p_ dip_macro) #:use-module (crates-io))

(define-public crate-dip_macro-0.2.0 (c (n "dip_macro") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1xrj84xj5w3qdhw4r4b55vabn7my3dpq9pdsawdsjw2vcqrxpbbc")))

