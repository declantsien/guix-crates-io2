(define-module (crates-io di p_ dip_config) #:use-module (crates-io))

(define-public crate-dip_config-0.1.1 (c (n "dip_config") (v "0.1.1") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dkjw96sgpy3zmcb5hmsgb9a89xcnagdr3pnpf78c9zdygyxz3m0")))

(define-public crate-dip_config-0.2.0 (c (n "dip_config") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04g9hypf4kavdwxngfrrck34s2ygmq4lxr5cm5xsm677qml40xhi")))

