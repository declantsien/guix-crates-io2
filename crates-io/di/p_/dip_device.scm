(define-module (crates-io di p_ dip_device) #:use-module (crates-io))

(define-public crate-dip_device-0.2.0 (c (n "dip_device") (v "0.2.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "hidapi") (r "^1.3") (f (quote ("linux-static-hidraw"))) (k 0)) (d (n "ledger-transport-hid") (r "^0.10") (d #t) (k 0)) (d (n "ledger-zondax-generic") (r "^0.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread" "sync" "macros" "fs"))) (k 0)))) (h "0p2vydh2lqmf2g57zpcpwnf9w2ks67q1z68j4iss8qfgk7xa6zi7")))

