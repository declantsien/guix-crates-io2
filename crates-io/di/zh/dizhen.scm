(define-module (crates-io di zh dizhen) #:use-module (crates-io))

(define-public crate-dizhen-0.1.0 (c (n "dizhen") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("alloc" "std" "serde"))) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1nl7k9qa4lxqqwh7bqp5rv3k7329wnak4mrbf63j6c9rjkh4w7bg")))

(define-public crate-dizhen-0.1.1 (c (n "dizhen") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("alloc" "std" "serde"))) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "01lpid136x7mqpjc19yp5pmknq5p63ng035cf4rixhb40y174aks")))

