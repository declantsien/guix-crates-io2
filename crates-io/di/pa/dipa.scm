(define-module (crates-io di pa dipa) #:use-module (crates-io))

(define-public crate-dipa-0.1.0 (c (n "dipa") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "dipa-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ng9vikf6720a2mrjcrrj8n2n9wzagyckxicdi1kq72nk89sglr4") (f (quote (("impl-tester" "bincode") ("derive" "dipa-derive"))))))

(define-public crate-dipa-0.1.1 (c (n "dipa") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "dipa-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0257r00aibs9bj43215g4r4gpfh8pbfqva4qxkzzm13vjb4d0zq3") (f (quote (("impl-tester" "bincode") ("derive" "dipa-derive"))))))

