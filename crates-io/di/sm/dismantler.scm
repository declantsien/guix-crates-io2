(define-module (crates-io di sm dismantler) #:use-module (crates-io))

(define-public crate-dismantler-0.0.1 (c (n "dismantler") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0bg9d3nimxvfgab96zhfgskcjkz8797dg8657ksz6izlky0fgnga")))

