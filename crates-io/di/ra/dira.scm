(define-module (crates-io di ra dira) #:use-module (crates-io))

(define-public crate-dira-0.1.0 (c (n "dira") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0zf5q0kdcx3pfada7rpbsql7aslsjg77kmbzcix26sddvvxlywik")))

(define-public crate-dira-0.2.0 (c (n "dira") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0bckx1mhyjiyc57wjr3b0fki3rcnldk1gl6razm0ghqla60isa8g")))

(define-public crate-dira-0.3.0 (c (n "dira") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "11if7qyh8wzl4c298y5pny9ap7v9kjwx1wkbiyzcpjzylg6cr3bd")))

