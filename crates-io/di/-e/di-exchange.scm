(define-module (crates-io di -e di-exchange) #:use-module (crates-io))

(define-public crate-di-exchange-0.1.0 (c (n "di-exchange") (v "0.1.0") (d (list (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.4") (d #t) (k 1)))) (h "0ajqjisnd6fp43fwklm3yj5y6p1rldpkmifp7bpk4xqdli0ffmm9") (y #t)))

(define-public crate-di-exchange-0.1.1 (c (n "di-exchange") (v "0.1.1") (d (list (d (n "prost") (r "^0.10.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.4") (d #t) (k 1)))) (h "148v1qhrrvd3xjyqi4syvha4v64ywpzh4vd6lhinbvnax7gcy7n3")))

