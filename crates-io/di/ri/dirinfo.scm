(define-module (crates-io di ri dirinfo) #:use-module (crates-io))

(define-public crate-dirinfo-0.1.0 (c (n "dirinfo") (v "0.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "18pr687jqcclxhkaq0bm4kad9sp9c8ymk6k59jyry9j2gjmn3kq2")))

(define-public crate-dirinfo-0.1.1 (c (n "dirinfo") (v "0.1.1") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "07sj3dblb73qp94w8ink7b4sh7yfaplvvnx0q252gyvw0grmb5cd")))

