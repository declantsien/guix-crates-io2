(define-module (crates-io di ri diridp) #:use-module (crates-io))

(define-public crate-diridp-0.1.1 (c (n "diridp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rsa") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0nm5mhpsbz3dykxk1fgc72sw3bdak96vyc33i352vsmdibydvkyy")))

