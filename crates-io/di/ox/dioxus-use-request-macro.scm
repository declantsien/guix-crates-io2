(define-module (crates-io di ox dioxus-use-request-macro) #:use-module (crates-io))

(define-public crate-dioxus-use-request-macro-0.1.0 (c (n "dioxus-use-request-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0bcqqb78ixf54ibq90lbr8vfir7dijxl9gkw8z8gms86ghqda5c2") (y #t)))

(define-public crate-dioxus-use-request-macro-0.1.1 (c (n "dioxus-use-request-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1ck29fprp842qn8v7my4bwj57np7a59ncs2lx1l9sdvwyci19hys") (y #t)))

(define-public crate-dioxus-use-request-macro-0.1.2 (c (n "dioxus-use-request-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0cvi44hkrxia618iqmma5qfrn14xd93pcqj5q2nnwm5ail93y12s") (y #t)))

(define-public crate-dioxus-use-request-macro-0.1.3 (c (n "dioxus-use-request-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "082xbs7442j7inys78f9yq68ds296y7m6if7yiv18jn8nx72p4h5") (y #t)))

(define-public crate-dioxus-use-request-macro-0.1.4 (c (n "dioxus-use-request-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "18yf4q7i66n11nyhsdmgb1rmhv4agbjh9zgp9x604rck6ym49ca4")))

(define-public crate-dioxus-use-request-macro-0.2.0-alpha03 (c (n "dioxus-use-request-macro") (v "0.2.0-alpha03") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1jxmkalcpxhn633rc3q5b2m67s8ykr7flsr4368wvan94ak4k967")))

(define-public crate-dioxus-use-request-macro-0.2.0-alpha05 (c (n "dioxus-use-request-macro") (v "0.2.0-alpha05") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "regex-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "00c19w1371bks2sn6439yywdm6xgrsx7fqkc104hl5w1b4dm5p8z")))

