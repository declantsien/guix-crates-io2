(define-module (crates-io di ox dioxus-radio) #:use-module (crates-io))

(define-public crate-dioxus-radio-0.1.0 (c (n "dioxus-radio") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4") (f (quote ("macro" "hooks"))) (k 0)) (d (n "dioxus") (r "^0.4") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 2)))) (h "0d4882lygkkshmmjsi7l39ba2z8jli64i2rvm0fhhyzh6firzgy2")))

(define-public crate-dioxus-radio-0.1.1 (c (n "dioxus-radio") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.4") (f (quote ("macro" "hooks"))) (k 0)) (d (n "dioxus") (r "^0.4") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 2)))) (h "0qkrwinf59df5s01g5ymbln7mkj9va3rffdjy090jqm5gncdy7lp")))

(define-public crate-dioxus-radio-0.2.0 (c (n "dioxus-radio") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.5") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "dioxus-lib") (r "^0.5") (f (quote ("macro" "hooks" "signals"))) (k 0)) (d (n "generational-box") (r "^0.5.0") (d #t) (k 0)))) (h "0js6v8b3b5m4qx6avr2h09myia61f0c63x0a3049gkbci2qhab57")))

(define-public crate-dioxus-radio-0.2.1 (c (n "dioxus-radio") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.5") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "dioxus-lib") (r "^0.5") (f (quote ("macro" "hooks" "signals"))) (k 0)) (d (n "generational-box") (r "^0.5.0") (d #t) (k 0)))) (h "1l8kj8gjpir13nkk0sc9ssl3r2y6dnkqvl5jd9y56951wn7bnl1a")))

(define-public crate-dioxus-radio-0.2.3 (c (n "dioxus-radio") (v "0.2.3") (d (list (d (n "dioxus") (r "^0.5") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "dioxus-lib") (r "^0.5") (f (quote ("macro" "hooks" "signals"))) (k 0)) (d (n "generational-box") (r "^0.5.0") (d #t) (k 0)))) (h "0rqza1j50w9fwcfn14wpmfqn5mc7g63r3pcfbppfq1d2vyl53rk7")))

(define-public crate-dioxus-radio-0.2.4 (c (n "dioxus-radio") (v "0.2.4") (d (list (d (n "dioxus") (r "^0.5") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "dioxus-lib") (r "^0.5") (f (quote ("macro" "hooks" "signals"))) (k 0)) (d (n "generational-box") (r "^0.5.0") (d #t) (k 0)))) (h "0igfsl0cbpfq49kmav7h5anj5s94nwzws4lyld4ch752mn9ih4pa")))

