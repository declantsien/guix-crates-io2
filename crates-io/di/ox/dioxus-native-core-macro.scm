(define-module (crates-io di ox dioxus-native-core-macro) #:use-module (crates-io))

(define-public crate-dioxus-native-core-macro-0.2.0 (c (n "dioxus-native-core-macro") (v "0.2.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 2)) (d (n "dioxus-core-macro") (r "^0.2.1") (d #t) (k 2)) (d (n "dioxus-html") (r "^0.2.1") (d #t) (k 2)) (d (n "dioxus-native-core") (r "^0.2.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0yynxy9qhghbp2v15maczhmfnx71ns4fvp9snf7i6dqv31xjpffy")))

(define-public crate-dioxus-native-core-macro-0.4.0 (c (n "dioxus-native-core-macro") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1sgva4291cxxas808hb2p8b6yk2g7ypp2770m1rhyap0n04k9g7c")))

(define-public crate-dioxus-native-core-macro-0.4.3 (c (n "dioxus-native-core-macro") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0r8lcn5631wwpzqfdx9mxpagvqc29na6vvn3y9wqn2r5l424x098")))

(define-public crate-dioxus-native-core-macro-0.5.0-alpha.0 (c (n "dioxus-native-core-macro") (v "0.5.0-alpha.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0j5r5j0sd96aa5kv1qd8xlh8mha0afgdh7234iw8pbiyafy0694d")))

