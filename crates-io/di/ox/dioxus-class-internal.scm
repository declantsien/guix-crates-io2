(define-module (crates-io di ox dioxus-class-internal) #:use-module (crates-io))

(define-public crate-dioxus-class-internal-0.5.0 (c (n "dioxus-class-internal") (v "0.5.0") (d (list (d (n "dioxus") (r "^0.5.0-alpha.2") (d #t) (k 0)))) (h "078fy36wds4mcmjs24i2sl50rgfrx3knk2zdlw4s2kz2181rdl0r")))

(define-public crate-dioxus-class-internal-0.7.0 (c (n "dioxus-class-internal") (v "0.7.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)))) (h "1fz7yhdqsv12crdl6s2ml9vsm2kp7y89z808la7ww6g67x4llbzv")))

