(define-module (crates-io di ox dioxus-mobile) #:use-module (crates-io))

(define-public crate-dioxus-mobile-0.0.1 (c (n "dioxus-mobile") (v "0.0.1") (d (list (d (n "dioxus-desktop") (r "^0.1.1") (d #t) (k 0)))) (h "1jk1a407f4g8gdp159l4aciiichfzzlgf8vy0z7pfv3vh1jlk5m0")))

(define-public crate-dioxus-mobile-0.0.2 (c (n "dioxus-mobile") (v "0.0.2") (d (list (d (n "dioxus-desktop") (r "^0.1.2") (d #t) (k 0)))) (h "18203nh384p9h721dwz085cbhbds1c0mvvb00zwv64bz1q9yynlc")))

(define-public crate-dioxus-mobile-0.0.3 (c (n "dioxus-mobile") (v "0.0.3") (d (list (d (n "dioxus-desktop") (r "^0.1.5") (d #t) (k 0)))) (h "0lxfsfidv1qamk8rmhxzpwncp9vk1hza3ar6jk1myarj5m789a35")))

(define-public crate-dioxus-mobile-0.3.0 (c (n "dioxus-mobile") (v "0.3.0") (d (list (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 0)))) (h "06nlp3agqg1dhjy9a5pqg3av258h97p2d957rvfdf2mh3dy99dms")))

(define-public crate-dioxus-mobile-0.4.0 (c (n "dioxus-mobile") (v "0.4.0") (d (list (d (n "dioxus-desktop") (r "^0.4.0") (d #t) (k 0)))) (h "05kc4502604kdyvkxzj6id0ksgid89jfvi2cbn0xzmc0vma4xfmb")))

(define-public crate-dioxus-mobile-0.4.3 (c (n "dioxus-mobile") (v "0.4.3") (d (list (d (n "dioxus-desktop") (r "^0.4.0") (d #t) (k 0)))) (h "1jdiyk6jbkbcrfy4qng4bmng1ja4si2cpscyx0ipcw1685l4h7lm")))

(define-public crate-dioxus-mobile-0.5.0-alpha.0 (c (n "dioxus-mobile") (v "0.5.0-alpha.0") (d (list (d (n "dioxus-desktop") (r "^0.5.0-alpha.0") (f (quote ("tokio_runtime"))) (d #t) (k 0)))) (h "1lpmmr9rnxvpcmrxl3yk53a3jac4cxqgqh0xf0mk9c4f1nm4jziz")))

(define-public crate-dioxus-mobile-0.5.0-alpha.1 (c (n "dioxus-mobile") (v "0.5.0-alpha.1") (d (list (d (n "dioxus-desktop") (r "^0.5.0-alpha.1") (f (quote ("tokio_runtime"))) (d #t) (k 0)))) (h "0klknzax0k076n3dra0as3pl03g6n0lhjp9n1jsspa4bfvg0aprb")))

(define-public crate-dioxus-mobile-0.5.0-alpha.2 (c (n "dioxus-mobile") (v "0.5.0-alpha.2") (d (list (d (n "dioxus-desktop") (r "^0.5.0-alpha.2") (f (quote ("tokio_runtime"))) (d #t) (k 0)))) (h "0qz3i97s415vs11kw6566wsbh2jyyarzigjvzd6a2ywwbj8lp1z7")))

(define-public crate-dioxus-mobile-0.5.0 (c (n "dioxus-mobile") (v "0.5.0") (d (list (d (n "dioxus-desktop") (r "^0.5.0") (f (quote ("tokio_runtime"))) (k 0)))) (h "1a6n57bhxcimpljdrb86yagn73yalvgqfwqc4ndw58y8p05x6jsr")))

(define-public crate-dioxus-mobile-0.5.1 (c (n "dioxus-mobile") (v "0.5.1") (d (list (d (n "dioxus-desktop") (r "^0.5.0") (f (quote ("tokio_runtime"))) (k 0)))) (h "0y1k1xcdlxi9vsjhykxfjjyrnq67qy49jrr3xhhhpfjzzmyq522j")))

