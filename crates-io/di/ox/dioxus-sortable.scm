(define-module (crates-io di ox dioxus-sortable) #:use-module (crates-io))

(define-public crate-dioxus-sortable-0.1.0 (c (n "dioxus-sortable") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.3.2") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 2)))) (h "18djn9j4sg6mqcyp4h8dzj8yhay784hs834lc9lnlhi62m2qrndb")))

(define-public crate-dioxus-sortable-0.1.1 (c (n "dioxus-sortable") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.3.2") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 2)))) (h "1f8y4i3hfbpr3v657zz8dy7j8gyirq83x5i1g90h9c3l60hwf813")))

(define-public crate-dioxus-sortable-0.1.2 (c (n "dioxus-sortable") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.4") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2") (d #t) (k 2)))) (h "0kvwqm8vp1k2sfslh07iw74jcfiphn7x2z4lx2ia6n98353yz9r8")))

