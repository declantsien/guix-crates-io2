(define-module (crates-io di ox dioxus-webview) #:use-module (crates-io))

(define-public crate-dioxus-webview-0.0.0 (c (n "dioxus-webview") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "dioxus-core") (r "^0.1.2") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0dd4yrzrn8fq5y886d1l0lb748pbcb76dg145r69magj33q771af")))

