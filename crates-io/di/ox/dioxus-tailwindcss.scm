(define-module (crates-io di ox dioxus-tailwindcss) #:use-module (crates-io))

(define-public crate-dioxus-tailwindcss-0.2.0 (c (n "dioxus-tailwindcss") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.2.0") (d #t) (k 0)))) (h "1k9h1i51yilz33i243vm6gdgvmdif23h24xx9kq2l20qczxf20w3")))

(define-public crate-dioxus-tailwindcss-0.2.2 (c (n "dioxus-tailwindcss") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.3") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.2.2") (d #t) (k 0)))) (h "0rpkhyrp43al9ypn4afhfkqy01pnh3bzg7jwyi53hmw9gk2c75zq")))

(define-public crate-dioxus-tailwindcss-0.3.0 (c (n "dioxus-tailwindcss") (v "0.3.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.3.0") (d #t) (k 0)))) (h "0q6alw4rzl4rgmypchalxfqrzpwghjk6cik3m0v6ci5x5xkrs7pv")))

(define-public crate-dioxus-tailwindcss-0.4.0 (c (n "dioxus-tailwindcss") (v "0.4.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.4.0") (d #t) (k 0)))) (h "03m7s8d51j2s8mm81z5f6lyynlc4a2v9q7w3yys0bxa730fg462q")))

(define-public crate-dioxus-tailwindcss-0.5.0 (c (n "dioxus-tailwindcss") (v "0.5.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.5.0") (d #t) (k 0)))) (h "0hbqyszcqkykg1ixna84b7cjvc685zkjrc2rm3wgkg33fzhlagcl") (f (quote (("build-classes" "dioxus-class/build-classes"))))))

(define-public crate-dioxus-tailwindcss-0.6.0 (c (n "dioxus-tailwindcss") (v "0.6.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.6.0") (d #t) (k 0)))) (h "0wiamrp4ysf3a085kqli8jxcs5kj7yh95i3srlqg4n91pc2bx3sb") (f (quote (("build-classes" "dioxus-class/build-classes"))))))

(define-public crate-dioxus-tailwindcss-0.7.0 (c (n "dioxus-tailwindcss") (v "0.7.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.7.0") (d #t) (k 0)))) (h "0zgy4jn9i2z3n97z479j2j4qlkaw3s0s45b4ivqlg7w0d015dklb") (f (quote (("build-classes" "dioxus-class/build-classes"))))))

