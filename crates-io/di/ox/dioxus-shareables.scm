(define-module (crates-io di ox dioxus-shareables) #:use-module (crates-io))

(define-public crate-dioxus-shareables-1.0.0 (c (n "dioxus-shareables") (v "1.0.0") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1dck2r3r3fbsch2pdb9nqv0j5mxb1lc5n2c8l47vjgqs6wwfcvp2") (f (quote (("debugging")))) (y #t)))

(define-public crate-dioxus-shareables-1.0.1 (c (n "dioxus-shareables") (v "1.0.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0x0lfms9xpjks0qpidi4h8anbcjpq9jgqjyh46c3gma7qfyxv19r") (f (quote (("debugging")))) (y #t)))

(define-public crate-dioxus-shareables-1.1.1 (c (n "dioxus-shareables") (v "1.1.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1p5i9smsqwb3irhiwy0nxsgpch55pzddxc552cg2qqkyb78rhqd3") (f (quote (("debugging")))) (y #t)))

(define-public crate-dioxus-shareables-0.2.4 (c (n "dioxus-shareables") (v "0.2.4") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 2) (p "dioxus")) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 0) (p "dioxus-core")) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1wxqzzvn1xx4kp52x2zpgcgybhx5isp5ccxlscdmp8n69hnm2csc") (f (quote (("dioxus-git") ("debug"))))))

(define-public crate-dioxus-shareables-0.3.0 (c (n "dioxus-shareables") (v "0.3.0") (d (list (d (n "dioxus-core") (r "^0.3.0") (d #t) (k 0) (p "dioxus-core")) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "dioxus") (r "^0.3.1") (d #t) (k 2) (p "dioxus")))) (h "12fy7qh6mkgz7mjp5sx9fa0c1cjpzy27b7fbsg8j4kp8vp3h1xwb") (f (quote (("dioxus-git") ("debug"))))))

