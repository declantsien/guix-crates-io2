(define-module (crates-io di ox dioxus-katex) #:use-module (crates-io))

(define-public crate-dioxus-katex-0.1.0 (c (n "dioxus-katex") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.9.1") (d #t) (k 0)))) (h "19w9dx0zmzw8zq6f26kjld44v0ki69wckhrp3fzpfavvi9kvrks8") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.1.1 (c (n "dioxus-katex") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.9.2") (d #t) (k 0)))) (h "1wnjf8g8qih8fv7xm6sg64356vcnzggs75s4556dfh8qlg4cq4w6") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.2.0 (c (n "dioxus-katex") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.9.3") (d #t) (k 0)))) (h "0lpgr3zfwx3m7srbaqlcplk5s32vzzq5hgqq6ms17b9x3086jqbk") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.2.1 (c (n "dioxus-katex") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)))) (h "012cvmciwks8pbxdkyvn4rr3x0p5ic1w9qkgwvlbbqp3ng910k3p") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.2.2 (c (n "dioxus-katex") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)))) (h "1vbfwyqvwf2v2fxfkxf9a8bgnd4hq4d8pavlxzj8w5idagywix50") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.3.2 (c (n "dioxus-katex") (v "0.3.2") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)))) (h "1mfnayp751gxjz1a39q0zm24gqqq4b2x6wj1qkz9nfz94x15h45f") (f (quote (("default"))))))

(define-public crate-dioxus-katex-0.4.0 (c (n "dioxus-katex") (v "0.4.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "katex-wasmbind") (r "^0.10.0") (d #t) (k 0)))) (h "123gz8kndfjk59wg4mh9rql5ag670pzi6nilwk2q2nsbdpc4hqsr") (f (quote (("default"))))))

