(define-module (crates-io di ox dioxus-use-document) #:use-module (crates-io))

(define-public crate-dioxus-use-document-0.2.0 (c (n "dioxus-use-document") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Window" "Navigator" "Document" "DocumentType" "MutationObserver"))) (d #t) (k 0)))) (h "15x6hymv9a469kkdcyf90l4drfcjfb4ivm2fj44ykhvdwwhjhzjs") (f (quote (("default"))))))

