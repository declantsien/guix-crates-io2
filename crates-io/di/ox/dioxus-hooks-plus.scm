(define-module (crates-io di ox dioxus-hooks-plus) #:use-module (crates-io))

(define-public crate-dioxus-hooks-plus-0.1.0 (c (n "dioxus-hooks-plus") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "UiEvent"))) (d #t) (k 0)))) (h "19blk1g83dfr57nzsbax0a2fpgvplbdgmlc61ynmibxx23in2ikh") (f (quote (("default"))))))

(define-public crate-dioxus-hooks-plus-0.1.1 (c (n "dioxus-hooks-plus") (v "0.1.1") (d (list (d (n "dioxus-use-clipboard") (r "0.1.*") (d #t) (k 0)) (d (n "dioxus-use-window") (r "0.4.*") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "UiEvent"))) (d #t) (k 0)))) (h "004jw9axa3nxpx5wk8y6sz8120kiaj98i2hxibxb02xf2c9xnar4") (f (quote (("default"))))))

(define-public crate-dioxus-hooks-plus-0.1.2 (c (n "dioxus-hooks-plus") (v "0.1.2") (d (list (d (n "dioxus-use-clipboard") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "dioxus-use-cursor") (r "0.2.*") (d #t) (k 0)) (d (n "dioxus-use-window") (r "0.5.*") (d #t) (k 0)))) (h "02w900hs4na7ll1phlkicyq7zhz0w2mp3y09mmraiq70xgzwhv60") (f (quote (("unstable" "dioxus-use-clipboard") ("default"))))))

(define-public crate-dioxus-hooks-plus-0.2.0 (c (n "dioxus-hooks-plus") (v "0.2.0") (d (list (d (n "dioxus-use-clipboard") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "dioxus-use-cursor") (r "0.2.*") (d #t) (k 0)) (d (n "dioxus-use-storage") (r "0.2.*") (d #t) (k 0)) (d (n "dioxus-use-window") (r "0.6.*") (d #t) (k 0)))) (h "1ygd43ldqilj5r50y062qrh6ddhc0fgk9plzxafbjl6h2d1izar6") (f (quote (("unstable" "dioxus-use-clipboard") ("default"))))))

(define-public crate-dioxus-hooks-plus-0.2.1 (c (n "dioxus-hooks-plus") (v "0.2.1") (d (list (d (n "dioxus-use-clipboard") (r "0.1.*") (o #t) (d #t) (k 0)) (d (n "dioxus-use-cursor") (r "0.2.*") (d #t) (k 0)) (d (n "dioxus-use-storage") (r "0.3.*") (d #t) (k 0)) (d (n "dioxus-use-window") (r "0.7.*") (d #t) (k 0)))) (h "0n2xp1prxqznssiyccygfjxci15r0crhlk3fkqfavf1ydw0vha7k") (f (quote (("unstable" "dioxus-use-clipboard") ("default"))))))

