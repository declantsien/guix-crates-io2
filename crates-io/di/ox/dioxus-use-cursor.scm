(define-module (crates-io di ox dioxus-use-cursor) #:use-module (crates-io))

(define-public crate-dioxus-use-cursor-0.1.0 (c (n "dioxus-use-cursor") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "UiEvent" "MouseEvent"))) (d #t) (k 0)))) (h "0pbxs6vp2phzkh6w8f5xj8z533rwi667bf6zdy77v51949gvkniz") (f (quote (("default"))))))

(define-public crate-dioxus-use-cursor-0.2.0 (c (n "dioxus-use-cursor") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "UiEvent" "MouseEvent"))) (d #t) (k 0)))) (h "1ag1b074m90k3xz97fmgdsnfgjs4ak17i9p0zkw5x4qjqdpwfcng") (f (quote (("default"))))))

(define-public crate-dioxus-use-cursor-0.2.1 (c (n "dioxus-use-cursor") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Window" "UiEvent" "PointerEvent"))) (d #t) (k 0)))) (h "05ynly3h61lz383vq3yrbdv6zdvz6cq3qlsilyb1681pkrhdjlbx") (f (quote (("default"))))))

