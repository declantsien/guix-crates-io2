(define-module (crates-io di ox dioxus-v04-optional-hooks) #:use-module (crates-io))

(define-public crate-dioxus-v04-optional-hooks-0.1.0 (c (n "dioxus-v04-optional-hooks") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4") (d #t) (k 0)))) (h "0jkk7rrsr37n4qsyg1ap1kpp0b58aq4v2m3487d3bd100j1n38xn")))

(define-public crate-dioxus-v04-optional-hooks-0.1.1 (c (n "dioxus-v04-optional-hooks") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.4") (d #t) (k 0)))) (h "0jwx5zrysih1q4axmaf0s81zrl3rg61aiacqs8p018dy8w0khy6b")))

