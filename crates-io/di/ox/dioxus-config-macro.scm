(define-module (crates-io di ox dioxus-config-macro) #:use-module (crates-io))

(define-public crate-dioxus-config-macro-0.5.0-alpha.0 (c (n "dioxus-config-macro") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "140836sszihxnmd99zxzbffahg69c4sz10s9zpfvn29wlc70yf1b") (f (quote (("web") ("tui") ("ssr") ("mobile") ("liveview") ("fullstack") ("desktop") ("default"))))))

(define-public crate-dioxus-config-macro-0.5.0-alpha.1 (c (n "dioxus-config-macro") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0pk5608apr5qlrzgw4jqf142v5v5f2z8pk0js470dd82h3j6fxia") (f (quote (("web") ("ssr") ("mobile") ("liveview") ("fullstack") ("desktop") ("default"))))))

(define-public crate-dioxus-config-macro-0.5.0-alpha.2 (c (n "dioxus-config-macro") (v "0.5.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0xn2842887vm4pydc9nmsnjvashm3h1v1bp3dqpl3myhx1bc2q73") (f (quote (("web") ("ssr") ("mobile") ("liveview") ("fullstack") ("desktop") ("default"))))))

(define-public crate-dioxus-config-macro-0.5.0 (c (n "dioxus-config-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0qr8v678facl7vvhsxysd89mih4qzs2jjx1qkygfq39jgccl9qpb") (f (quote (("web") ("ssr") ("mobile") ("liveview") ("fullstack") ("desktop") ("default"))))))

