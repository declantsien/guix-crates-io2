(define-module (crates-io di ox dioxus-class-macro) #:use-module (crates-io))

(define-public crate-dioxus-class-macro-0.5.0 (c (n "dioxus-class-macro") (v "0.5.0") (d (list (d (n "dioxus-class-internal") (r "^0.5.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bja4jf4rd9iyy1bhakaqxkmnawvfgkqk1mr4ziqhzi0rvyim89l") (f (quote (("build-classes" "lazy_static"))))))

(define-public crate-dioxus-class-macro-0.7.0 (c (n "dioxus-class-macro") (v "0.7.0") (d (list (d (n "dioxus-class-internal") (r "^0.7.0") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "026a30z6a6fm41msb3qpnkn0mfvqzy14f4ra6dil7v11g6cacs67") (f (quote (("build-classes" "lazy_static"))))))

