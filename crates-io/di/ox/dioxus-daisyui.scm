(define-module (crates-io di ox dioxus-daisyui) #:use-module (crates-io))

(define-public crate-dioxus-daisyui-0.1.0 (c (n "dioxus-daisyui") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.1.0") (k 0)))) (h "1w588rdzczbxhvva4ydwc4ls11rjayj69l5ax4nfhjxrpfc5fzy1") (f (quote (("default" "components") ("components" "dioxus-class/components"))))))

(define-public crate-dioxus-daisyui-0.1.1 (c (n "dioxus-daisyui") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.1.1") (d #t) (k 0)))) (h "1l6jbpqkxych96cplwaijgah2b6ail4jpnjhwkdwjy1ipmfk7xzi") (f (quote (("default" "components") ("components" "dioxus-class/components") ("build" "dioxus-class/build"))))))

(define-public crate-dioxus-daisyui-0.1.2 (c (n "dioxus-daisyui") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.1.1") (d #t) (k 0)))) (h "17wk613g1axklh97kypnbpa6z3wrjjsky6d5f9kp1b92c38j1gfl") (f (quote (("default" "dioxus-class/macro" "components") ("components" "dioxus-class/components") ("build" "dioxus-class/build"))))))

(define-public crate-dioxus-daisyui-0.1.3 (c (n "dioxus-daisyui") (v "0.1.3") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-class") (r "^0.1.2") (d #t) (k 0)))) (h "10q3k9vg5d95yk0hi5mjnzmm4dxkmmrv9kzvsvqf0azsa5xlahzq") (f (quote (("default" "components") ("components" "dioxus-class/components") ("build" "dioxus-class/build"))))))

(define-public crate-dioxus-daisyui-0.2.0 (c (n "dioxus-daisyui") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.2.0") (d #t) (k 0)))) (h "0mfakx46rgpx3qbhslrmj7skmy3l5chn180xi6b946yd3xj80dyi")))

(define-public crate-dioxus-daisyui-0.2.2 (c (n "dioxus-daisyui") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.3") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.2.2") (d #t) (k 0)))) (h "0nivh4rl1w8ygwmpvxv60v773ph5nxnf07n7kx3qgrqjlk96n3zj")))

(define-public crate-dioxus-daisyui-0.3.0 (c (n "dioxus-daisyui") (v "0.3.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1hz6kcsn3nxd08idy4gd835di99xlz4h1g14hygykppb2vpcc13v")))

(define-public crate-dioxus-daisyui-0.4.0 (c (n "dioxus-daisyui") (v "0.4.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ga1cmkmkgr6v32zg76frqss9f2idrkiid62rjxild4ic3ffqm74")))

(define-public crate-dioxus-daisyui-0.5.0 (c (n "dioxus-daisyui") (v "0.5.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dnradl0smhw3k2kbbd2v54l2c71kw1rmd9dnyh7mcypaq1n4yh5") (f (quote (("build-classes" "dioxus-tailwindcss/build-classes"))))))

(define-public crate-dioxus-daisyui-0.6.0 (c (n "dioxus-daisyui") (v "0.6.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i656ci6ymf71ix2cfb1ya1d176gcl55nilbhr7n0wf6c5vmrfmh") (f (quote (("build-classes" "dioxus-tailwindcss/build-classes"))))))

(define-public crate-dioxus-daisyui-0.7.0 (c (n "dioxus-daisyui") (v "0.7.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-tailwindcss") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qhbvhjr7jkki7v3i643kmc2kkdy8kq1grh9mqv12fp0mqxw6xky") (f (quote (("build-classes" "dioxus-tailwindcss/build-classes"))))))

