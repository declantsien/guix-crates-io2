(define-module (crates-io di ox dioxus-slides-macro) #:use-module (crates-io))

(define-public crate-dioxus-slides-macro-0.0.1 (c (n "dioxus-slides-macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12qwia45gqqi25c5zh6nb2q4dyd7snnbij3rcpd5s83qqjcbqgay")))

(define-public crate-dioxus-slides-macro-0.0.3 (c (n "dioxus-slides-macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "061nk5yzi7mjw4jks92fbzs2a8yf82s8yjrya6g5j82qvf99zd3x")))

(define-public crate-dioxus-slides-macro-0.0.4 (c (n "dioxus-slides-macro") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0gfyyxp5fw0ys0dyk8lihb0llazj3zw6mx83mk83a204gx5c28pn")))

(define-public crate-dioxus-slides-macro-0.0.5 (c (n "dioxus-slides-macro") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1v9q939q5awarkd4512cpn2n44h0w6pah3zhg69q6skkkg20h73g")))

(define-public crate-dioxus-slides-macro-0.1.0 (c (n "dioxus-slides-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ryhj3x64ilfagmj5fp27nmrv3ymp3lwihvnsgzjpm5kn1k4dnr6")))

(define-public crate-dioxus-slides-macro-0.1.1 (c (n "dioxus-slides-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rnc8gz545jwqjgaw1a5g11nsc9f8si75hxz5lcva6xjxfm67d5c")))

(define-public crate-dioxus-slides-macro-0.1.2 (c (n "dioxus-slides-macro") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qqwi1m34j3bsm1kl9qnwgvwlfgqj10nrr9rvbh6isyhkd7fq7j1")))

