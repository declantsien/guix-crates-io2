(define-module (crates-io di ox dioxus-helmet) #:use-module (crates-io))

(define-public crate-dioxus-helmet-0.1.1 (c (n "dioxus-helmet") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)))) (h "16808jkg7vfs55gwj6kz9pv30hh59kcp0s7hgv82mn5xckn1rm84") (f (quote (("web" "dioxus/web") ("desktop" "dioxus/desktop") ("default" "web"))))))

(define-public crate-dioxus-helmet-0.1.2 (c (n "dioxus-helmet") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)))) (h "176whpk72hx003hl5z9y4vsk1l427g2m204jjsn78vgx5xz3k9qy") (f (quote (("web" "dioxus/web") ("desktop" "dioxus/desktop") ("default" "web"))))))

(define-public crate-dioxus-helmet-0.1.3 (c (n "dioxus-helmet") (v "0.1.3") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement"))) (d #t) (k 0)))) (h "1pp60h09kvc00mribmnfnzi8p9zyb9iah20mf6s64zvnfhdk3v9p")))

(define-public crate-dioxus-helmet-0.2.0 (c (n "dioxus-helmet") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement" "HtmlCollection" "NamedNodeMap"))) (d #t) (k 0)))) (h "0r8cfyiw98qfc9gxbdiqns02llgnm9485k9xq7k6qd21p1wwms23")))

(define-public crate-dioxus-helmet-0.2.1 (c (n "dioxus-helmet") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement" "HtmlCollection" "NamedNodeMap"))) (d #t) (k 0)))) (h "0rryzwia1jqj2p38i214rg7a57ngxidm69954r3r8dgkdgy1yxyy")))

(define-public crate-dioxus-helmet-0.2.2 (c (n "dioxus-helmet") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement" "HtmlCollection" "NamedNodeMap"))) (d #t) (k 0)))) (h "0yrv64nj1cgbvbgnbhl5zygjb6vbxa5xa1z1xgm6rgcpvkaywiv2")))

(define-public crate-dioxus-helmet-0.2.3 (c (n "dioxus-helmet") (v "0.2.3") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement" "HtmlCollection" "NamedNodeMap" "NodeList"))) (d #t) (k 0)))) (h "0zhnj1jcl0s7gc2v86baxc2zyn3m73zybhpcclr1njqiar6gwq6k")))

(define-public crate-dioxus-helmet-0.2.4 (c (n "dioxus-helmet") (v "0.2.4") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.59") (f (quote ("Document" "Window" "Element" "HtmlHeadElement" "HtmlCollection" "NamedNodeMap" "NodeList"))) (d #t) (k 0)))) (h "1qii5k1gp6cf53kv7n2djgs54pqy9all0jhb5qzyfax7qnbr8gfc")))

