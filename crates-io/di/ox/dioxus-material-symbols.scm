(define-module (crates-io di ox dioxus-material-symbols) #:use-module (crates-io))

(define-public crate-dioxus-material-symbols-0.4.3 (c (n "dioxus-material-symbols") (v "0.4.3") (d (list (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 2)))) (h "0wbx23kwbn1igrqrn48iybhijb7iwwc7n7sc3i6pvc61xnxiqh1z")))

