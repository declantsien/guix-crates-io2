(define-module (crates-io di ox dioxus-timer) #:use-module (crates-io))

(define-public crate-dioxus-timer-0.1.0 (c (n "dioxus-timer") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 0)))) (h "1p0cqi35wd28g0b4xvd2nhrvmxim8f7x81bg6sdzi1b8201wyagn")))

(define-public crate-dioxus-timer-0.2.0 (c (n "dioxus-timer") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "dioxus") (r "^0.4") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0i20jdrvmnxfv5spr3r903a0jrgza35d9dhmiqxyxdiqlz93dz0v")))

