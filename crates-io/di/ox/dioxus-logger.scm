(define-module (crates-io di ox dioxus-logger) #:use-module (crates-io))

(define-public crate-dioxus-logger-0.1.0 (c (n "dioxus-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "09brb36w2pv4xl1rrgn29873ajw7lj2nwdkrp00x4ig6i18abfrg")))

(define-public crate-dioxus-logger-0.1.1 (c (n "dioxus-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "0s9cwdrp8cwkm6z97sn15rqkwgvyxkyh45s1ba8c6fd6cvhvy059")))

(define-public crate-dioxus-logger-0.1.2 (c (n "dioxus-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "0v8xy64ysyv59mkk8g7my80kd5066lkcch26ps2rjm9rddsiysz9")))

(define-public crate-dioxus-logger-0.2.0 (c (n "dioxus-logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "1c7i24al7l4phrb7klbplyijp4nrdi44j3ccfprpzx8izhygmjaf")))

(define-public crate-dioxus-logger-0.3.0 (c (n "dioxus-logger") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("std" "wasm-bindgen"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "0ira5cpwnmv3ynv87z49lj8pmniqjfsil8bcci2dxxqccmzfnpkp") (s 2) (e (quote (("timestamps" "dep:time"))))))

(define-public crate-dioxus-logger-0.4.0 (c (n "dioxus-logger") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("std" "wasm-bindgen"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "0b11jfdvvjf56b2rps8l05x96p8rvwx78x37dnxh3fpssy4b1av0") (s 2) (e (quote (("timestamps" "dep:time"))))))

(define-public crate-dioxus-logger-0.4.1 (c (n "dioxus-logger") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("std" "wasm-bindgen"))) (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.60") (f (quote ("console"))) (d #t) (k 0)))) (h "0m6g036jkff7g27n21frdhwbha937wgc5cqlkvz6142innqblz1x") (s 2) (e (quote (("timestamps" "dep:time"))))))

(define-public crate-dioxus-logger-0.5.0 (c (n "dioxus-logger") (v "0.5.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("registry" "std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1nivw23yjidpjlwkzs6hjcmj8whlkiawvl0g6mqz81hn53xl14f3")))

(define-public crate-dioxus-logger-0.5.1 (c (n "dioxus-logger") (v "0.5.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "dioxus") (r "^0.5") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("registry" "std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("fmt"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "tracing-wasm") (r "^0.2.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "104qwslx3hssjp7sbp5acy7x0mb57lh2k1fkn0dizp3kjzf0kzl1")))

