(define-module (crates-io di ox dioxus-use-navigator) #:use-module (crates-io))

(define-public crate-dioxus-use-navigator-0.2.0 (c (n "dioxus-use-navigator") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("Window" "Element" "Navigator" "Document" "DocumentType" "HtmlCollection" "MutationObserver"))) (d #t) (k 0)))) (h "166p2dci4iksbmm06d7g9c2ps3vylfsbqbnphnqc8bsvgy1wag7w") (f (quote (("default"))))))

