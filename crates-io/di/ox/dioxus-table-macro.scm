(define-module (crates-io di ox dioxus-table-macro) #:use-module (crates-io))

(define-public crate-dioxus-table-macro-0.1.0 (c (n "dioxus-table-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zgx5ngzrnmyxk5wh13z8g0s9fg81f6qxlcws3l0a7xis7x5b6pb")))

(define-public crate-dioxus-table-macro-0.1.1 (c (n "dioxus-table-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04spvz3pcgn79ilja7fdk187qgwhqjbnj3sr1vfqg6ph8plfk6xs")))

