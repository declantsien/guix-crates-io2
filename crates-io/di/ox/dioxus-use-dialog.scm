(define-module (crates-io di ox dioxus-use-dialog) #:use-module (crates-io))

(define-public crate-dioxus-use-dialog-0.1.0 (c (n "dioxus-use-dialog") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window"))) (d #t) (k 0)))) (h "1k0kfp2wz3v5jhvfy7jyq7gsc5sik8crrlvin93csdnr37b9mjq7") (f (quote (("default"))))))

