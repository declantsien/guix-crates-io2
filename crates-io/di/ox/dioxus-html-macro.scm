(define-module (crates-io di ox dioxus-html-macro) #:use-module (crates-io))

(define-public crate-dioxus-html-macro-0.1.0 (c (n "dioxus-html-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ngi3hwbncwyfvhrkr2h96lmgy4vvb0qz8jibhkr0k4w6jjnx0jh") (y #t)))

(define-public crate-dioxus-html-macro-0.2.0 (c (n "dioxus-html-macro") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "04avbk2dz12qxlff600r3aw7cds3vlnns4gz80lzvysx3imp1pvv")))

(define-public crate-dioxus-html-macro-0.3.0 (c (n "dioxus-html-macro") (v "0.3.0") (d (list (d (n "dioxus") (r "^0.3.2") (k 0)) (d (n "dioxus") (r "^0.3.2") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1zg4lygkw56c230mj7nz263w4xxpdfjss9a8fg3xfhm4jkr66lpx")))

