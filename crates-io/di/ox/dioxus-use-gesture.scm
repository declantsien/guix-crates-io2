(define-module (crates-io di ox dioxus-use-gesture) #:use-module (crates-io))

(define-public crate-dioxus-use-gesture-0.1.0 (c (n "dioxus-use-gesture") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-logger") (r "^0.4.1") (d #t) (k 0)) (d (n "dioxus-spring") (r "^0.1.0") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)))) (h "1vc4rypnvjj0k4m43r0i0kmvkpwsba4p1avn4hpdrfcilh4ys9ch")))

