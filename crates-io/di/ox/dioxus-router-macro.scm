(define-module (crates-io di ox dioxus-router-macro) #:use-module (crates-io))

(define-public crate-dioxus-router-macro-0.4.0 (c (n "dioxus-router-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "134dcyky18l8rjb0vdklg2wx9115xjbmnlcz0vcjrn1hz9bv671r") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.4.1 (c (n "dioxus-router-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1iib8i255k5bc80wmi635akqd67njfzc4z7889f9jxsbfqzcq70g") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.4.3 (c (n "dioxus-router-macro") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0r2gilfrlgjfhp56p5im1jdn5w61qm8cw0fmywn0iqacnwqbriw9") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.5.0-alpha.0 (c (n "dioxus-router-macro") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "176xzkyclk5kkxx94rq5fxb2vilns8zpa7b36jq8l9kw90b22g3v") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.5.0-alpha.1 (c (n "dioxus-router-macro") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1lhywxr4ddajrwcijihycvrrai227l98gcx34s7s9jx4r6bslkvj") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.5.0-alpha.2 (c (n "dioxus-router-macro") (v "0.5.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0v3wgmb1dzmfpby4d1jvk614qmy8v7ib7ky4gd3jqbkzzslkqks2") (f (quote (("default"))))))

(define-public crate-dioxus-router-macro-0.5.0 (c (n "dioxus-router-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08nzwik9hybyhxyga2fskdhrq8084m6la5l0i2nk2z0cyfg0h6qa") (f (quote (("web") ("default"))))))

(define-public crate-dioxus-router-macro-0.5.1 (c (n "dioxus-router-macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y50l85yb308vsc6a7f7bmlq59ymca2n633jr9mq3p03lbjbdz37") (f (quote (("web") ("default"))))))

