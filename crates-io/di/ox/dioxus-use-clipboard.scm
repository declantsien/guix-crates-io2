(define-module (crates-io di ox dioxus-use-clipboard) #:use-module (crates-io))

(define-public crate-dioxus-use-clipboard-0.1.0 (c (n "dioxus-use-clipboard") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.56") (f (quote ("Window" "Clipboard"))) (d #t) (k 0)))) (h "1xxdy8v4mgg8zpnysavh6ivbmsx9aal7a1lvwliw9zsj43y2jkgz") (f (quote (("default"))))))

