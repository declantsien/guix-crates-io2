(define-module (crates-io di ox dioxus-class) #:use-module (crates-io))

(define-public crate-dioxus-class-0.1.0 (c (n "dioxus-class") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)))) (h "096ladg8fbbimjz4jyfkrrpp61fn1hg7adx4mnz4i5184c25axkr") (f (quote (("default" "components") ("components"))))))

(define-public crate-dioxus-class-0.1.1 (c (n "dioxus-class") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)))) (h "18z33rdmjrzirgywwyjh84ifxpm8w1lv6fcbz0x3gqgj66q233ck") (f (quote (("macro") ("default" "macro" "components") ("components") ("build"))))))

(define-public crate-dioxus-class-0.1.2 (c (n "dioxus-class") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)))) (h "1vasar5gryfwyvdkcgnriv48v5sq8najp1dn53d0a8nfsj17jzkf") (f (quote (("default" "components") ("components") ("build"))))))

(define-public crate-dioxus-class-0.2.0 (c (n "dioxus-class") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1di6pn93d7mgf8iw4pv9q6cyb7yd2vajmjpxln8r4351ifads9kd")))

(define-public crate-dioxus-class-0.2.1 (c (n "dioxus-class") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "197i9qvqhp2dv2lzqwx7bahv7y4wm1zrb6hhsddkqbyjb8hfykc5")))

(define-public crate-dioxus-class-0.2.2 (c (n "dioxus-class") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0ab2xm7nqzlp9g9sgk1vblbc767girzg4z1z29lwiij9n3s7bqmh")))

(define-public crate-dioxus-class-0.3.0 (c (n "dioxus-class") (v "0.3.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0wh9skfjy5g6sv956ghcnmgcrzhzdhnhlah0ms9vrqjwk60agrhs")))

(define-public crate-dioxus-class-0.4.0 (c (n "dioxus-class") (v "0.4.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0fpg9fsaacrdql2rqvfrg4lxygjrwz9sc03jg477rj7viycmipix")))

(define-public crate-dioxus-class-0.5.0 (c (n "dioxus-class") (v "0.5.0") (d (list (d (n "dioxus") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "dioxus-class-internal") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1pmvqhwm6mili25wxgvbj5pk19xipsw6nz66gzvzj3aq4dk02klz") (f (quote (("build-classes" "dioxus-class-macro/build-classes"))))))

(define-public crate-dioxus-class-0.6.0 (c (n "dioxus-class") (v "0.6.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class-internal") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "0k8b1hqq004z6x22nyj2jfifqcc36m8hifskc5q8dmbacfypkm4s") (f (quote (("build-classes" "dioxus-class-macro/build-classes"))))))

(define-public crate-dioxus-class-0.7.0 (c (n "dioxus-class") (v "0.7.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-class-internal") (r "^0.7.0") (d #t) (k 0)) (d (n "dioxus-class-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)))) (h "1dk8zhxn2i5gdiajrygydckv7jjxjqr6675pqk2y9m3gxdvba4y4") (f (quote (("build-classes" "dioxus-class-macro/build-classes"))))))

