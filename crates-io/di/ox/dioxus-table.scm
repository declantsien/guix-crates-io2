(define-module (crates-io di ox dioxus-table) #:use-module (crates-io))

(define-public crate-dioxus-table-0.1.0 (c (n "dioxus-table") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "dioxus-table-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0j99r194sm9nhnss7klpb6wwqp96dc0c3j11r7dyqwqhvmrd91x7")))

(define-public crate-dioxus-table-0.1.1 (c (n "dioxus-table") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.2.4") (d #t) (k 0)) (d (n "dioxus-table-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1bspy01xwqjsckk199q4ikr080yjvqcbv8rfihfcxb0k8q6j6kcl")))

