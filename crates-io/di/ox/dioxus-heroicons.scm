(define-module (crates-io di ox dioxus-heroicons) #:use-module (crates-io))

(define-public crate-dioxus-heroicons-0.1.0 (c (n "dioxus-heroicons") (v "0.1.0") (d (list (d (n "dioxus") (r "~0.1") (f (quote ("html"))) (d #t) (k 0)))) (h "0yj9vzyb3xr2yqzfjji1w01x7ka66j2mvb0km1bfxdj90ifmskxn")))

(define-public crate-dioxus-heroicons-0.1.1 (c (n "dioxus-heroicons") (v "0.1.1") (d (list (d (n "dioxus") (r "~0.1") (f (quote ("html"))) (d #t) (k 0)))) (h "07iifiiagy6n1a7w474mlml2ln0yxp9d2jyjm3m8z526m1s4w626")))

(define-public crate-dioxus-heroicons-0.1.2 (c (n "dioxus-heroicons") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.2.0") (f (quote ("html"))) (d #t) (k 0)))) (h "1vmcs0a50bj0665jbhgpj2dbh7kjlrw821q53hcp78gd6rxn9xzb")))

(define-public crate-dioxus-heroicons-0.1.3 (c (n "dioxus-heroicons") (v "0.1.3") (d (list (d (n "dioxus") (r "^0.2.0") (f (quote ("html"))) (d #t) (k 0)))) (h "1d2q12bv2n5kmzakkxymp479dm7553as7aslz7cx1h1fjcg1jvf4")))

(define-public crate-dioxus-heroicons-0.1.4 (c (n "dioxus-heroicons") (v "0.1.4") (d (list (d (n "dioxus") (r "^0.2.0") (f (quote ("html"))) (d #t) (k 0)))) (h "052j30qnc0cfljy4q47zc08pdsfxs242liyccag3iqg26nvsqy8y")))

(define-public crate-dioxus-heroicons-0.2.0 (c (n "dioxus-heroicons") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.2.0") (f (quote ("html"))) (d #t) (k 0)))) (h "1f2lnh8v8psr13iavn45hzgp5j3j44s3p5hrkiqj1js4zh3s51sk")))

