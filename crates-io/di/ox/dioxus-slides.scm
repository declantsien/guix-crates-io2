(define-module (crates-io di ox dioxus-slides) #:use-module (crates-io))

(define-public crate-dioxus-slides-0.0.1 (c (n "dioxus-slides") (v "0.0.1") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-slides-macro") (r "^0.0.1") (d #t) (k 0)))) (h "09143gks5s9r7xbpvpsx3q5lnkn28c6jxwrsvvix7142k8gxs7da")))

(define-public crate-dioxus-slides-0.0.5 (c (n "dioxus-slides") (v "0.0.5") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-slides-macro") (r "^0.0.5") (d #t) (k 0)))) (h "0aknbmhqswmpn6awrayiqb8jzlsyy307w0ky7y57xx0bv298qp09")))

(define-public crate-dioxus-slides-0.1.0 (c (n "dioxus-slides") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-slides-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0js1qxc2ghn2gfzh27mfqsp85lbjn5hb6c9v0i58ywcxzwfql6ls")))

(define-public crate-dioxus-slides-0.1.1 (c (n "dioxus-slides") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-slides-macro") (r "^0.1") (d #t) (k 0)))) (h "1d9hizb3xvbj2h2xs49x77b88acnbzakp0lknkg0hsgiwhypirxk")))

(define-public crate-dioxus-slides-0.1.2 (c (n "dioxus-slides") (v "0.1.2") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-slides-macro") (r "^0.1") (d #t) (k 0)))) (h "0vpkl7nfk1wzlf276gm1dzv3pnf6zdwmfcfqmikwzy6sh9x329m3")))

