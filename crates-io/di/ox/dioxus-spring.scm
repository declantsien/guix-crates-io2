(define-module (crates-io di ox dioxus-spring) #:use-module (crates-io))

(define-public crate-dioxus-spring-0.1.0 (c (n "dioxus-spring") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.9.0") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-logger") (r "^0.4.1") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "interpolation") (r "^0.3.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)))) (h "18vjjlijzcrmym1zsxmx3xwkk82ikky213acpnmapysf2dpxs2cq")))

