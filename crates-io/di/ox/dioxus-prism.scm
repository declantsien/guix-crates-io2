(define-module (crates-io di ox dioxus-prism) #:use-module (crates-io))

(define-public crate-dioxus-prism-0.1.0 (c (n "dioxus-prism") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "prism-wasmbind") (r "^0.1.0") (d #t) (k 0)))) (h "17qgc9f8rxa4inxmfpmp8bb2whla1azzvvbwqfrrjgc5pcmyrigv") (f (quote (("default"))))))

(define-public crate-dioxus-prism-0.1.1 (c (n "dioxus-prism") (v "0.1.1") (d (list (d (n "dioxus") (r "^0.1.8") (d #t) (k 0)) (d (n "prism-wasmbind") (r "^0.1.0") (d #t) (k 0)))) (h "05rdm105gfysrn4h6bciag3m46np4c11f74ixa5xdkjy0bnp31gx") (f (quote (("default"))))))

(define-public crate-dioxus-prism-0.2.0 (c (n "dioxus-prism") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.2.3") (d #t) (k 0)) (d (n "prism-wasmbind") (r "^0.2.1") (d #t) (k 0)))) (h "137lrd4dvansjz1k64fyx2nzvbnz2whz1vck7vy49r2dhy7rybn7") (f (quote (("default"))))))

