(define-module (crates-io di ox dioxus-use-mounted) #:use-module (crates-io))

(define-public crate-dioxus-use-mounted-0.2.0-alpha.1 (c (n "dioxus-use-mounted") (v "0.2.0-alpha.1") (d (list (d (n "dioxus") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "dioxus-signals") (r "^0.5.0-alpha.2") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.5.0-alpha.2") (d #t) (k 0)))) (h "0svpyrxj5qfva608p0vg21m9s57ry39zfsn2j3blq6anwnphhqwg")))

(define-public crate-dioxus-use-mounted-0.2.0 (c (n "dioxus-use-mounted") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.5.0") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.5.0") (d #t) (k 2)))) (h "0kiczimjrv8wd95w6whm66sz7706ayj6yp0r9n07aaww4813xxjz")))

