(define-module (crates-io di jk dijkstra) #:use-module (crates-io))

(define-public crate-dijkstra-0.1.0 (c (n "dijkstra") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.10.1") (d #t) (k 0)))) (h "0bhbr7gpwy9x21wpvmm3ghcvdz63rs2swadzvsya8gjsl5911ygy")))

(define-public crate-dijkstra-0.1.2 (c (n "dijkstra") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.10.1") (d #t) (k 0)))) (h "1mi2bbn1vwvagy4cmrchvbzm27fqbrk5j9yzqarj1154qlqmvfv2")))

