(define-module (crates-io di ha dihardts_cstools) #:use-module (crates-io))

(define-public crate-dihardts_cstools-1.0.0 (c (n "dihardts_cstools") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "hdf5") (r "^0.8.1") (d #t) (k 0)) (d (n "murmur3") (r "^0.5.2") (d #t) (k 0)))) (h "1ardx31rp9qhf3yz638kwwjk8gh304x8p4si724qnwlbxk2akh3c")))

