(define-module (crates-io di mm dimmer) #:use-module (crates-io))

(define-public crate-dimmer-0.1.0 (c (n "dimmer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "1jqrs9wd20ygggrmcs749vw1gxj1gwzpzcg4g1gqz8zd5mqdx5ly")))

(define-public crate-dimmer-0.2.0 (c (n "dimmer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "1gb81cirmyi9pyh6rx50ylxamcxz7pbrqns8viw5mm6ky9qlpphv")))

(define-public crate-dimmer-0.2.1 (c (n "dimmer") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.5") (d #t) (k 0)))) (h "1n9lig9g8yydszcwpk7dgj0mrk1w1xb20k9cphq7snk5zyvx14nx")))

