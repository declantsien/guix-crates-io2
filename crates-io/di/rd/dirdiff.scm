(define-module (crates-io di rd dirdiff) #:use-module (crates-io))

(define-public crate-dirdiff-0.1.0-beta.0 (c (n "dirdiff") (v "0.1.0-beta.0") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0p8kxszwgk5nzj7r6fpqllgz5hb82m38fi0bav8389gvi70gbxrc")))

(define-public crate-dirdiff-0.1.0-beta.1 (c (n "dirdiff") (v "0.1.0-beta.1") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1kz2xwkq56i5i8cdbbg94id7zmn3cjqnarkkd7lfcnq223c7syi5")))

(define-public crate-dirdiff-0.1.0 (c (n "dirdiff") (v "0.1.0") (h "15ig2zxk6g00xq7z2fwh01yziq1db6q7jifvj4w2a531ny5i7v5m")))

