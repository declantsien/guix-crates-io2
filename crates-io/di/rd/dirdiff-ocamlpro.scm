(define-module (crates-io di rd dirdiff-ocamlpro) #:use-module (crates-io))

(define-public crate-dirdiff-ocamlpro-0.2.0 (c (n "dirdiff-ocamlpro") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.12") (d #t) (k 0)))) (h "026r52wwv2ikgvax9vmr38z5r070y8nkpdpd9a627s0b2v2awcnq")))

