(define-module (crates-io di co dicom-scpproxy) #:use-module (crates-io))

(define-public crate-dicom-scpproxy-0.1.0 (c (n "dicom-scpproxy") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.2.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "1z7mrcj21np9qlr0lym8125qiblccg0ab3kmnk5i9hprcidcqgjg")))

(define-public crate-dicom-scpproxy-0.2.0 (c (n "dicom-scpproxy") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.3.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.2.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1vyc43n8b5kxzwgwyv49fyj72f6adjcx99b9a79j74nn1bzflhz4")))

(define-public crate-dicom-scpproxy-0.3.0 (c (n "dicom-scpproxy") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.4.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.3.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "13w42b10zh3gik6plvn92a2dgmjdaqlcbc2zwpf91xyiv9ncqm6s")))

(define-public crate-dicom-scpproxy-0.4.0-rc.1 (c (n "dicom-scpproxy") (v "0.4.0-rc.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.4.0-rc.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1z3vn7r1032qckzfpaa0jvkbrb0bmmw15v613vpvp5abjhsh87vl")))

(define-public crate-dicom-scpproxy-0.4.0-rc.2 (c (n "dicom-scpproxy") (v "0.4.0-rc.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.4.0-rc.2") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1h67ph2v4sxnnwgj16v76ym7j20bi3bjklhwnj49hrhij9lrzmq5")))

(define-public crate-dicom-scpproxy-0.4.0 (c (n "dicom-scpproxy") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.4.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 0)))) (h "0ri47ixqgql5380hb9z73ibq05x14r0fg0d6bshn2dqhjvs2xf5s")))

(define-public crate-dicom-scpproxy-0.4.1 (c (n "dicom-scpproxy") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.4.3") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 0)))) (h "1hzr37gpdxad3i21jgbylnnss25w95nm5qjgnf1mvbqg1fkwfahn")))

(define-public crate-dicom-scpproxy-0.5.0 (c (n "dicom-scpproxy") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.6.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.5.0") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 0)))) (h "1jc9a9zjarl3j9ykim5ji8mszlbk9i65v2fgm9f547a4r28bw7pp")))

(define-public crate-dicom-scpproxy-0.7.0 (c (n "dicom-scpproxy") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "dicom-dictionary-std") (r "^0.7.0") (d #t) (k 0)) (d (n "dicom-ul") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 0)))) (h "00nf8qrs3hs6nn7fynais14yk6pzhbhaf6rjps9z6s7lg91ja0dr")))

