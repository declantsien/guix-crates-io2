(define-module (crates-io di co dicom-dictionary-std) #:use-module (crates-io))

(define-public crate-dicom-dictionary-std-0.1.0 (c (n "dicom-dictionary-std") (v "0.1.0") (d (list (d (n "dicom-core") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0sl2n520zw1yxqb1h2xhys65rwpl51m1apvi3mjjk8j77cny0wff")))

(define-public crate-dicom-dictionary-std-0.2.0 (c (n "dicom-dictionary-std") (v "0.2.0") (d (list (d (n "dicom-core") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1s963x8mnvicgpddnwyzzyzji7msmmap2z88czah19hasi2hzli7")))

(define-public crate-dicom-dictionary-std-0.3.0 (c (n "dicom-dictionary-std") (v "0.3.0") (d (list (d (n "dicom-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0nipvqys0v6hya4bppw3qm86z6nya6i5daq2fkq4rbwy4d9mjfiz")))

(define-public crate-dicom-dictionary-std-0.4.0 (c (n "dicom-dictionary-std") (v "0.4.0") (d (list (d (n "dicom-core") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0bwfs1ihb6857g58n4k0ajydgwy0a5rj33gcifphx89gybg16a0a")))

(define-public crate-dicom-dictionary-std-0.5.0-rc.1 (c (n "dicom-dictionary-std") (v "0.5.0-rc.1") (d (list (d (n "dicom-core") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1xr4882033nppja6pffcq6ycnf9h046whm4skc14g8pw6cj7abia")))

(define-public crate-dicom-dictionary-std-0.5.0-rc.2 (c (n "dicom-dictionary-std") (v "0.5.0-rc.2") (d (list (d (n "dicom-core") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "1gjy24sc0bqlbq2j9f4cvrgs8113yiakxg551visav6pxy5vbvp6")))

(define-public crate-dicom-dictionary-std-0.5.0 (c (n "dicom-dictionary-std") (v "0.5.0") (d (list (d (n "dicom-core") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0f0zx6dp5ksk6qdf3cmidk9v06smzm87zbww8qna2d738a0xvvnv")))

(define-public crate-dicom-dictionary-std-0.6.0 (c (n "dicom-dictionary-std") (v "0.6.0") (d (list (d (n "dicom-core") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "0i056zzd9w0njs5ivis1cfwbd1z4pvrc6fxq8gizhsnp950l96xy") (f (quote (("well-known-sop-instance") ("transfer-syntax") ("synchronization-frame-of-reference") ("sop-class") ("service-class") ("meta-sop-class") ("mapping-resource") ("ldap-oid") ("dicom-uid-as-coding-scheme") ("default") ("coding-scheme") ("application-hosting-model") ("application-context-name"))))))

(define-public crate-dicom-dictionary-std-0.6.1 (c (n "dicom-dictionary-std") (v "0.6.1") (d (list (d (n "dicom-core") (r "^0.6.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "1k6cwrkspyrzas60arg3szhhyv09ip7idcy71pxl21h3m3q34c1y") (f (quote (("well-known-sop-instance") ("transfer-syntax") ("synchronization-frame-of-reference") ("sop-class") ("service-class") ("meta-sop-class") ("mapping-resource") ("ldap-oid") ("dicom-uid-as-coding-scheme") ("default") ("coding-scheme") ("application-hosting-model") ("application-context-name"))))))

(define-public crate-dicom-dictionary-std-0.7.0 (c (n "dicom-dictionary-std") (v "0.7.0") (d (list (d (n "dicom-core") (r "^0.7.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)))) (h "18jp6qp26jmfvb64qb21kad9f5q338rwhfikzndxmqlfrysakkqn") (f (quote (("well-known-sop-instance") ("transfer-syntax") ("synchronization-frame-of-reference") ("sop-class") ("service-class") ("meta-sop-class") ("mapping-resource") ("ldap-oid") ("dicom-uid-as-coding-scheme") ("default") ("coding-scheme") ("application-hosting-model") ("application-context-name"))))))

