(define-module (crates-io di co dicom-ul) #:use-module (crates-io))

(define-public crate-dicom-ul-0.1.0 (c (n "dicom-ul") (v "0.1.0") (d (list (d (n "byteordered") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)))) (h "1zra29pikh2hsnldyakhhprlg8kaavdykmjydln2354s9gk9d3xq")))

(define-public crate-dicom-ul-0.2.0 (c (n "dicom-ul") (v "0.2.0") (d (list (d (n "byteordered") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.3.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "0k0xsvx9543v3z8nf52xj2wb88f1y9cajzixa7nadblwbc9dsgzk")))

(define-public crate-dicom-ul-0.3.0 (c (n "dicom-ul") (v "0.3.0") (d (list (d (n "byteordered") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.4.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.4.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1yqqk4q22mpbj7c705xxpq78qgb6xxagci8gkksd6mq6jd2mj3v6")))

(define-public crate-dicom-ul-0.4.0-rc.1 (c (n "dicom-ul") (v "0.4.0-rc.1") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1laf5a0rbfaz4yh5m9fqxi2r1gjckq8nvj4ygiamfwlfm9wcb7z5") (y #t)))

(define-public crate-dicom-ul-0.4.0-rc.2 (c (n "dicom-ul") (v "0.4.0-rc.2") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.0-rc.2") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "0ngb1q4pvzy2y7dv97aa844zi6xx0rvn4wcjhr6mlxmqw8pyq2h7") (y #t)))

(define-public crate-dicom-ul-0.4.0 (c (n "dicom-ul") (v "0.4.0") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "10ipaqry582dq5y2fy0r68rxyzmwjhzx5wav7vdplwnii8jhfmdf") (y #t)))

(define-public crate-dicom-ul-0.4.1 (c (n "dicom-ul") (v "0.4.1") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "1fn1wfir6y67zvzv2kfk3sl191vik3p03a0fmw76fhczl989ilck")))

(define-public crate-dicom-ul-0.4.2 (c (n "dicom-ul") (v "0.4.2") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.0") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "137vr37s1y5khrha4pamwbbrkkmdz7v4bmisnpm9qm69464gkbff")))

(define-public crate-dicom-ul-0.4.3 (c (n "dicom-ul") (v "0.4.3") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.2") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "1nq8qs3gk44llhlz71rgpwp2i1lxmmxj6050wsgwy5vcgrvkmj3r")))

(define-public crate-dicom-ul-0.4.4 (c (n "dicom-ul") (v "0.4.4") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.5.3") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.5.1") (d #t) (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "02b4dhkf4id0n7chs39vs96n61ng3n71y2wkhi7xrjg4jalpnkma")))

(define-public crate-dicom-ul-0.5.0 (c (n "dicom-ul") (v "0.5.0") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.6.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.6.0") (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "0j78hdi4xbgp52x7d4l10zm09gg0401q1yn4zmf9vw6am8ai410v")))

(define-public crate-dicom-ul-0.5.1 (c (n "dicom-ul") (v "0.5.1") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.6.1") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.6.0") (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "0bg87phl1bgvj9wi8pc1zr7xnipln4yhidnlirrz4mnl2118vk03")))

(define-public crate-dicom-ul-0.7.0 (c (n "dicom-ul") (v "0.7.0") (d (list (d (n "byteordered") (r "^0.6") (d #t) (k 0)) (d (n "dicom-encoding") (r "^0.7.0") (d #t) (k 0)) (d (n "dicom-transfer-syntax-registry") (r "^0.7.0") (k 0)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "snafu") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)))) (h "0zffl1n9p872a5p37ygkny6mmfd9ls8624q3dhlfc8yhjnqmkqa7")))

