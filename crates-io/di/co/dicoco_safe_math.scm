(define-module (crates-io di co dicoco_safe_math) #:use-module (crates-io))

(define-public crate-dicoco_safe_math-0.1.0 (c (n "dicoco_safe_math") (v "0.1.0") (h "06c6rynxan4dlqj7j7sncaqdrca16aihp8mxzs7hj9572b4imsy5")))

(define-public crate-dicoco_safe_math-0.1.1 (c (n "dicoco_safe_math") (v "0.1.1") (h "068fwrhgz53y58d2r9b8dd9g6cvqgmd0zbdybmlnkzm0cb0hmspp")))

