(define-module (crates-io di co dicomweb-client) #:use-module (crates-io))

(define-public crate-dicomweb-client-0.1.0 (c (n "dicomweb-client") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dicom") (r "^0.4.0") (d #t) (k 0)) (d (n "dicomweb-util") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1wyqh5ird97ar1bbqbr4ml3h61mdrynhn4rbrhyz558vjx32k3ir")))

