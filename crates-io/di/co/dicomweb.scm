(define-module (crates-io di co dicomweb) #:use-module (crates-io))

(define-public crate-dicomweb-0.1.0 (c (n "dicomweb") (v "0.1.0") (d (list (d (n "dicomweb-client") (r "^0.1.0") (d #t) (k 0)) (d (n "dicomweb-server") (r "^0.1.0") (d #t) (k 0)) (d (n "dicomweb-util") (r "^0.1.0") (d #t) (k 0)))) (h "0v7a987q3ws0r1zzm5xrl5yzwl00kjnhwp94zw0kmj3n98ckzijj")))

