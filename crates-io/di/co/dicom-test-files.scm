(define-module (crates-io di co dicom-test-files) #:use-module (crates-io))

(define-public crate-dicom-test-files-0.1.0 (c (n "dicom-test-files") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "ureq") (r "^1.1") (d #t) (k 0)))) (h "0d3bvggbf2fw6m29cbsf3prd4q4di1sxc11lfr885z0xyalvd80q")))

(define-public crate-dicom-test-files-0.1.1 (c (n "dicom-test-files") (v "0.1.1") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "ureq") (r "^1.1") (d #t) (k 0)))) (h "1jq4r72hfphwjl6cs4dhaqjqgggr0q0l3hzrnn8igl8zp9fvlhha")))

(define-public crate-dicom-test-files-0.1.2 (c (n "dicom-test-files") (v "0.1.2") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "ureq") (r "^1.1") (d #t) (k 0)))) (h "0277p3riq6pkhr7v1y8wf0azy79dlv13qmxi2wbzij1pqbs5p3xf")))

(define-public crate-dicom-test-files-0.2.0 (c (n "dicom-test-files") (v "0.2.0") (d (list (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "ureq") (r "^1.1") (d #t) (k 0)))) (h "0cdyiz8nqcxmga7b4nnvs88kbc2nrfzs6g7s5vfbl3v1fnqsnirf")))

(define-public crate-dicom-test-files-0.2.1 (c (n "dicom-test-files") (v "0.2.1") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)))) (h "0v4lxbdg1430abgdpmpmfp541k59v0iq6djf0aybybr461lbzkzp")))

(define-public crate-dicom-test-files-0.3.0 (c (n "dicom-test-files") (v "0.3.0") (d (list (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 1)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "ureq") (r "^2.4") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (o #t) (k 0)))) (h "19cvavbfh43gfcy3g94glyp2jghyb53c43vbc2jwaxrdwvdmz6z4") (f (quote (("default" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd"))))))

