(define-module (crates-io di co dicomweb-server) #:use-module (crates-io))

(define-public crate-dicomweb-server-0.1.0 (c (n "dicomweb-server") (v "0.1.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "dicom") (r "^0.4.0") (d #t) (k 0)) (d (n "dicomweb-util") (r "^0.1.0") (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "03faghb1zgif3n1xlr683jzzwm4frww70fd9pc6k6pis1jkp0mf4")))

