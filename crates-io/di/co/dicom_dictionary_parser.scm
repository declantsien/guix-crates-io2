(define-module (crates-io di co dicom_dictionary_parser) #:use-module (crates-io))

(define-public crate-dicom_dictionary_parser-0.1.0 (c (n "dicom_dictionary_parser") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "00fjk5zrcdqa3k7pbbjlb82i7wjjm0n4yg3izyhxvxrc0y15rjaj")))

(define-public crate-dicom_dictionary_parser-0.2.0 (c (n "dicom_dictionary_parser") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "1zcagaisk5hrib56s4djrbh72naskxr18lnspxvmzj0rj9gs1f1i")))

