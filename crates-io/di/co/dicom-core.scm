(define-module (crates-io di co dicom-core) #:use-module (crates-io))

(define-public crate-dicom-core-0.1.0 (c (n "dicom-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (d #t) (k 0)))) (h "1g09bqf8rhaql3js3p91xzg3q7pixixpvi326z0v62f7gdm5s72x")))

(define-public crate-dicom-core-0.2.0 (c (n "dicom-core") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "10gkli1cy428vl6h4zl5sa48yc940hw4a6mih19kkxsi6vcm61wp")))

(define-public crate-dicom-core-0.3.0 (c (n "dicom-core") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1vm595y5m2276037hk5s8spmp9757kacym4s0lr6vi97cjvhsimv")))

(define-public crate-dicom-core-0.4.0 (c (n "dicom-core") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "0hgwfimfykg49fas50zigckjz8v7cy9wqibmn1g3w1g6fh6m0ig8")))

(define-public crate-dicom-core-0.4.1 (c (n "dicom-core") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1zgdlds7mmn5v2kzwsfwji0w5s14zh7s7bcwlb2ff4lr443fzjh5")))

(define-public crate-dicom-core-0.5.0-rc.1 (c (n "dicom-core") (v "0.5.0-rc.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1px92barvnwa09a8f64v14x69hi98cvyh9fb6b5xynk4fv8w41ym")))

(define-public crate-dicom-core-0.5.0-rc.2 (c (n "dicom-core") (v "0.5.0-rc.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1ag4b7m744p7vzxj5h43knairwdjd8l14r06j3fxn9blnk439yjj")))

(define-public crate-dicom-core-0.5.0 (c (n "dicom-core") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "1g19xz9ysikryddphf16nlghmhzd3wniidkfi2dkq84svxmxzy6c")))

(define-public crate-dicom-core-0.5.1 (c (n "dicom-core") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)))) (h "18rssmk8l0yjp5kprch4rzcaga7fs2b2nxv9m1yf7w3d8khwhm8w")))

(define-public crate-dicom-core-0.5.2 (c (n "dicom-core") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "0djgnv6zmgah4kmn2r1z2zkpd74sfxhhzzxijizjz46fgsaxg036")))

(define-public crate-dicom-core-0.5.3 (c (n "dicom-core") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "0fjffibiywgqjaprsliwck2q444r92qq7wsbna37jidndcybcn8a")))

(define-public crate-dicom-core-0.5.4 (c (n "dicom-core") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "12mwabc6ycn5l3fj09fcf77fjxzxnn7jvm4h18vhr9i59iny5pbs")))

(define-public crate-dicom-core-0.6.0 (c (n "dicom-core") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "078f2lpi4wdsh6r2c373cskyvimwp27inp2pxjk9f0qa5ryvdzzl")))

(define-public crate-dicom-core-0.6.1 (c (n "dicom-core") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1r5dnc5nbcflaycqpdbfqgdw1335l4zppydfzlv8446456vs2ibf")))

(define-public crate-dicom-core-0.6.2 (c (n "dicom-core") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1iwvh0938wgky9pw233h2jiz62s4nv0rp7djjjiqx1981rz7ahmy")))

(define-public crate-dicom-core-0.6.3 (c (n "dicom-core") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std"))) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.3") (d #t) (k 0)))) (h "1bwhlir5cqx3qbg3qrfph1mzl1h563da0756c4zivda5rns62nck")))

(define-public crate-dicom-core-0.7.0 (c (n "dicom-core") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "clock"))) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "safe-transmute") (r "^0.11.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)) (d (n "snafu") (r "^0.8") (d #t) (k 0)))) (h "1h63q8p46jwcljrlrbyvpmxlxki43bwnq17mjv4a6a5m8f79mwb8")))

