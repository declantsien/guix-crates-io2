(define-module (crates-io di co dicom-gen-uid) #:use-module (crates-io))

(define-public crate-dicom-gen-uid-0.1.0 (c (n "dicom-gen-uid") (v "0.1.0") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1b6sjnbfc8lvh3r6j3x0hz69i41b5xdciimw62a6qbkamxvz2wy5")))

