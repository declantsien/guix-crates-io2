(define-module (crates-io di co dicomweb-util) #:use-module (crates-io))

(define-public crate-dicomweb-util-0.1.0 (c (n "dicomweb-util") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "dicom") (r "^0.4.0") (d #t) (k 0)) (d (n "dicom-object") (r "^0.4") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1j3gpwg1660a6zqjpijz7vd02fasqh7s3qy10vjarz0n8zhdgy0g")))

