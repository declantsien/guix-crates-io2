(define-module (crates-io di rh dirhash) #:use-module (crates-io))

(define-public crate-dirhash-0.2.0 (c (n "dirhash") (v "0.2.0") (d (list (d (n "seahash") (r "^3.0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.7") (d #t) (k 0)))) (h "1m7nhzhj6rjqv6l3y62nrx3hxkv1vwr0zz7lbd0008sn2qkpns98")))

