(define-module (crates-io di r- dir-walkin) #:use-module (crates-io))

(define-public crate-dir-walkin-0.1.0 (c (n "dir-walkin") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("fs" "sync" "rt"))) (d #t) (k 0)))) (h "0dl3dp09xniqa7k9r5nca647zffjcc843wl0ppjijrzhamqcr3ws")))

