(define-module (crates-io di r- dir-manager) #:use-module (crates-io))

(define-public crate-dir-manager-0.0.1 (c (n "dir-manager") (v "0.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)))) (h "1pspmiq8gm2nr885dhr94g2czgax62gglsrb9bjypnr81ff2dsnr") (r "1.62")))

