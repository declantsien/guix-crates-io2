(define-module (crates-io di r- dir-test) #:use-module (crates-io))

(define-public crate-dir-test-0.1.0 (c (n "dir-test") (v "0.1.0") (d (list (d (n "dir-test-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "01xilaz29sh61l4z6yjvbdcf9fcplqbbrvnfifnx4srlqmz6rwii")))

(define-public crate-dir-test-0.1.1 (c (n "dir-test") (v "0.1.1") (d (list (d (n "dir-test-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ydvz87gys974d7019w1fgyhk6rxkrwni5py92j1rjawmss1apq3")))

(define-public crate-dir-test-0.2.0 (c (n "dir-test") (v "0.2.0") (d (list (d (n "dir-test-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ny20w6ir060q82fqz9qafycpkf844v6929rxj5nc5aawi1q2vbq")))

(define-public crate-dir-test-0.2.1 (c (n "dir-test") (v "0.2.1") (d (list (d (n "dir-test-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v02wxp8a12ybiyws5z98grw9044v1q01876pxnss3rjq7s47jbg")))

(define-public crate-dir-test-0.3.0 (c (n "dir-test") (v "0.3.0") (d (list (d (n "dir-test-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1l8dxy770b0varb6gskk0gghlas521qmmcbyzcx25mcs67wvsi2w")))

