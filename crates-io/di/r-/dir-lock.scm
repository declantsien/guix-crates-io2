(define-module (crates-io di r- dir-lock) #:use-module (crates-io))

(define-public crate-dir-lock-0.1.0 (c (n "dir-lock") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (d #t) (k 0)))) (h "05a1qqk79sv7vs0w23sh7snbn9kfafihbxs6mmmi1fvmcz3hqg39")))

(define-public crate-dir-lock-0.1.1 (c (n "dir-lock") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (d #t) (k 0)))) (h "0zs9rhv3k6h8n5n4552z8hkry7m50vj6qm8cg6xgkialbs3qav6a")))

(define-public crate-dir-lock-0.2.0 (c (n "dir-lock") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (d #t) (k 0)))) (h "0ghcq5ksxhh2jh623ggk6aa96n9jc431xb6kkynhvl3hdy1n64lx")))

(define-public crate-dir-lock-0.2.1 (c (n "dir-lock") (v "0.2.1") (d (list (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "heim") (r "^0.0.10") (d #t) (k 0)))) (h "15w7p2apsjfk2mblzckdjjjk8gqd0xcaxydg0azfakmxn7zgfx90")))

(define-public crate-dir-lock-0.3.0 (c (n "dir-lock") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "heim") (r "^0.0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio02") (r "^0.2") (f (quote ("fs" "rt-threaded" "time"))) (o #t) (d #t) (k 0) (p "tokio")) (d (n "tokio03") (r "^0.3") (f (quote ("fs" "io-util" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0) (p "tokio")))) (h "15w6zr4kj6hyvb86rw27vn5x3f5az80f3pxzn9gf0105bl45kny7") (f (quote (("default" "tokio"))))))

