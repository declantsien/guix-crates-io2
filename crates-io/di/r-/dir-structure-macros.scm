(define-module (crates-io di r- dir-structure-macros) #:use-module (crates-io))

(define-public crate-dir-structure-macros-0.1.0 (c (n "dir-structure-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hl4byrn8znrv0w7lqya4dnws4msd56qpzyrav023g06avpz92cp")))

(define-public crate-dir-structure-macros-0.1.1 (c (n "dir-structure-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3g1w520zkkwnkqzp1zq2pqx7fw2rkccsp9wwkn6y6hwbyr1483")))

(define-public crate-dir-structure-macros-0.1.2 (c (n "dir-structure-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16vx8lcy43kcpyc673q2q29mwimjywxqxf3w5zg7711pa9slpmqd")))

(define-public crate-dir-structure-macros-0.1.3 (c (n "dir-structure-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1h39339sxgp9rvhvbim2120vzkldzzyfvkfdw03z7z9pv48p67s0")))

(define-public crate-dir-structure-macros-0.1.4 (c (n "dir-structure-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0larnggxzq04c4l8w1r4mv59mqjn4nks0s6kxl4sprd8jwwvrdpl")))

