(define-module (crates-io di r- dir-assert) #:use-module (crates-io))

(define-public crate-dir-assert-0.1.0 (c (n "dir-assert") (v "0.1.0") (d (list (d (n "test-case") (r "^1.0.0-alpha-1") (d #t) (k 2)))) (h "1v05b1rc5k4hvvkdfnw7j2wk4yc12k8jqij6azdmlfyk0rvyjkj0")))

(define-public crate-dir-assert-0.2.0 (c (n "dir-assert") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 2)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "168qkf40avq2ggy53bypfsmjv6kd55aj8xf4jll4zy19wd8ry1sj")))

