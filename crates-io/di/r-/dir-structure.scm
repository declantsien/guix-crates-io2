(define-module (crates-io di r- dir-structure) #:use-module (crates-io))

(define-public crate-dir-structure-0.1.0 (c (n "dir-structure") (v "0.1.0") (d (list (d (n "dir-structure-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0zplp7mhlwywkjl3q4i67fsx9kv3p75iq22g7qz1fx8xp3bx0ax0") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-dir-structure-0.1.1 (c (n "dir-structure") (v "0.1.1") (d (list (d (n "dir-structure-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1xihkks9ncg9d02bx1gf9z888rkl7gf19nzhhldpr28i6zm6pscr") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-dir-structure-0.1.2 (c (n "dir-structure") (v "0.1.2") (d (list (d (n "dir-structure-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "19asfhnz6lkvb2sx9n6msldwd5n1nk00xs4r13s59l60yn3fxnii") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-dir-structure-0.1.3 (c (n "dir-structure") (v "0.1.3") (d (list (d (n "dir-structure-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0z99pdcg0b83ar31g31i5k4wbw0kpmawkrihprfvrqqhjp4pnwrd") (f (quote (("json" "serde" "serde_json"))))))

(define-public crate-dir-structure-0.1.4 (c (n "dir-structure") (v "0.1.4") (d (list (d (n "dir-structure-macros") (r "=0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1wqs67dsi8p037q042vqrs7d8h63bxf7divr188i0rv6qlsj8nhw") (f (quote (("json" "serde" "serde_json"))))))

