(define-module (crates-io di r- dir-cmp) #:use-module (crates-io))

(define-public crate-dir-cmp-0.1.0 (c (n "dir-cmp") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bdfvdgyz37h08kwhs5xbdani9v1qkcrczf17mris0jqzcxlr4kn")))

