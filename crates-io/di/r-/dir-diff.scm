(define-module (crates-io di r- dir-diff) #:use-module (crates-io))

(define-public crate-dir-diff-0.1.0 (c (n "dir-diff") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0jslcacfc4ibz0zckvswb9g2kpbicm6wf9w0ipc7n605gd7na8bs")))

(define-public crate-dir-diff-0.2.0 (c (n "dir-diff") (v "0.2.0") (d (list (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "10kv5sk7r0wsry3nx8msz50gkhil6ags8467gzzsgri906x5psl6")))

(define-public crate-dir-diff-0.3.0 (c (n "dir-diff") (v "0.3.0") (d (list (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0pi3sc3v9fkagj5qxdvcm3m09h6cxrcc0i5dfmkrzdvha7cy3szh")))

(define-public crate-dir-diff-0.3.1 (c (n "dir-diff") (v "0.3.1") (d (list (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "02vzhqxsc1mfd1cx0p587fbcsy6w2j865xwk8x4iwc9nr986xkhw")))

(define-public crate-dir-diff-0.3.2 (c (n "dir-diff") (v "0.3.2") (d (list (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "02saicdxgdbpg5bcyp00z9v9wrldksni118jn95h0bkygmyl0q18")))

(define-public crate-dir-diff-0.3.3 (c (n "dir-diff") (v "0.3.3") (d (list (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "045xj1r98vmmmvczwfbxqx3prdisik2q2xjmsr83n9c4byzidbd7") (f (quote (("default")))) (r "1.66")))

