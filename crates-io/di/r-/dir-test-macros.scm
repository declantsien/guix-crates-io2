(define-module (crates-io di r- dir-test-macros) #:use-module (crates-io))

(define-public crate-dir-test-macros-0.1.0 (c (n "dir-test-macros") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06d92564z605z2g038wswaq5xlcmwbr8yf8wnim7fvp6hdsf8aq0")))

(define-public crate-dir-test-macros-0.1.1 (c (n "dir-test-macros") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1768r7sq2yrqrwrkv4rna5al5911fw93qkb2kcjs2n1zim09ys8m")))

(define-public crate-dir-test-macros-0.2.0 (c (n "dir-test-macros") (v "0.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09zx0gmr6x3af3bxmm3vl7rd95lb6bh8c4xpl5l8m4z7jfj4xzq7")))

(define-public crate-dir-test-macros-0.2.1 (c (n "dir-test-macros") (v "0.2.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdc5nd4dsdp6kdf26sws6vymmv80anhhbj1yz7sv4mfwqk5kklc")))

(define-public crate-dir-test-macros-0.3.0 (c (n "dir-test-macros") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09bbacic8gxss8ma6ny2xhk1jap5z4ilckg315xamprpf429ckv4")))

