(define-module (crates-io di r- dir-cache) #:use-module (crates-io))

(define-public crate-dir-cache-0.1.0 (c (n "dir-cache") (v "0.1.0") (d (list (d (n "lz4") (r "^1.24.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 2)))) (h "1ymkqprlnymkbdb36dlc1m41hfj0i3wr7cjxwvfhcz7pdd6x59g0") (f (quote (("default")))) (s 2) (e (quote (("lz4" "dep:lz4"))))))

