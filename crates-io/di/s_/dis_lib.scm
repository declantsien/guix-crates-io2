(define-module (crates-io di s_ dis_lib) #:use-module (crates-io))

(define-public crate-dis_lib-0.1.0 (c (n "dis_lib") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "dis_rs_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)))) (h "0ncrrwmzb17zn0xi4pz0nkcprsida1sfj1x07iv95iwd9120iyzf") (f (quote (("default"))))))

