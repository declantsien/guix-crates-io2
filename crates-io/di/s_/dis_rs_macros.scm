(define-module (crates-io di s_ dis_rs_macros) #:use-module (crates-io))

(define-public crate-dis_rs_macros-0.1.0 (c (n "dis_rs_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h78yy081h1zd56g8q4m02idmkfvxmzzyj3dyybkf5d76s5mphvr")))

