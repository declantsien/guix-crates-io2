(define-module (crates-io di ll dill) #:use-module (crates-io))

(define-public crate-dill-0.1.0 (c (n "dill") (v "0.1.0") (d (list (d (n "dill-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "multimap") (r "~0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pz0vkimnk9blgzxcgc5s1lvpxg5g7pq8s0iw9apadf6zdljvvg9")))

(define-public crate-dill-0.2.0 (c (n "dill") (v "0.2.0") (d (list (d (n "dill-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "multimap") (r "~0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ha1l0wx4qqdbjcmipsbgmnnyiippnsdw7jdvxdbcbh50gx4m82x")))

(define-public crate-dill-0.3.0 (c (n "dill") (v "0.3.0") (d (list (d (n "dill-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "multimap") (r "~0.8.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x24gfqdmd37zqfbnylzb4h60nnkiv5h8d1icpda4ldndav3b5yd")))

(define-public crate-dill-0.4.0 (c (n "dill") (v "0.4.0") (d (list (d (n "dill-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "multimap") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02riw159ky0nfl9ni7fm8n8zq8aqaz75l0wqfvk6yw6265b2xwfc")))

(define-public crate-dill-0.5.0 (c (n "dill") (v "0.5.0") (d (list (d (n "dill-impl") (r "^0.5.0") (d #t) (k 0)) (d (n "multimap") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01qgrr7vkbqasjzkmdbclqlcgfy6kqqwylrjlyk3q6wgdyqd3kai")))

(define-public crate-dill-0.5.1 (c (n "dill") (v "0.5.1") (d (list (d (n "dill-impl") (r "^0.5.1") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07mg33nigavx8lfp8r8hrc080pxii18xrvpxg11l8pb9nlxgf43g")))

(define-public crate-dill-0.5.2 (c (n "dill") (v "0.5.2") (d (list (d (n "dill-impl") (r "^0.5.2") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ibipfr58kgq0nhqqdzac6xhxxps3b13zi9al53rinzmbf61gxmy")))

(define-public crate-dill-0.5.3 (c (n "dill") (v "0.5.3") (d (list (d (n "dill-impl") (r "^0.5.3") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lwminzhp06g0w38qy6rcmadp7hn0q5nkghq5q8pkcl2hc1s217d")))

(define-public crate-dill-0.6.0 (c (n "dill") (v "0.6.0") (d (list (d (n "dill-impl") (r "^0.6.0") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14b24j11dkq7h9j796x3sw765qv1yhis2rqaz6qy17ya0vc3nh12")))

(define-public crate-dill-0.6.1 (c (n "dill") (v "0.6.1") (d (list (d (n "dill-impl") (r "^0.6.1") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cv5jk4z3pci0r7vlcmnb9k31qm6ysjqj1sbihkbc0cqi46cdv0k")))

(define-public crate-dill-0.7.0 (c (n "dill") (v "0.7.0") (d (list (d (n "dill-impl") (r "^0.7.0") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f4j8463lhkb4f1g36sr7by1xpcfjh1dsm2ciwdbnd80mikgxcz9")))

(define-public crate-dill-0.7.1 (c (n "dill") (v "0.7.1") (d (list (d (n "dill-impl") (r "^0.7.1") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b30bhrjgilvfdi86yh2w7zqbvk1hh83a11l2rgjmdlkf8hszjy6")))

(define-public crate-dill-0.7.2 (c (n "dill") (v "0.7.2") (d (list (d (n "dill-impl") (r "^0.7.2") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0js418bpzgbgsnxrvck2hgaqc51sncknxs863rxnkm4d1ggxiv7k")))

(define-public crate-dill-0.8.0 (c (n "dill") (v "0.8.0") (d (list (d (n "dill-impl") (r "^0.8.0") (d #t) (k 0)) (d (n "multimap") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1936fnamz6xxmf9r1c4xcgnnr0ldz2wza9ifbffkiw7hsvsmqin8")))

