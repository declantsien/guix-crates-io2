(define-module (crates-io di ll dill-impl) #:use-module (crates-io))

(define-public crate-dill-impl-0.1.0 (c (n "dill-impl") (v "0.1.0") (d (list (d (n "darling") (r "~0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y1zmf0673q0x78r8pdzzfizc7sdfv5w8yh23wlyzsd9kx74hp1b")))

(define-public crate-dill-impl-0.2.0 (c (n "dill-impl") (v "0.2.0") (d (list (d (n "darling") (r "~0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0slkad7fwqx0b7mkjbgylwsqkg9i31ilap1afnkwhwlsikrl797a")))

(define-public crate-dill-impl-0.3.0 (c (n "dill-impl") (v "0.3.0") (d (list (d (n "darling") (r "~0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "018xdg5gp4g74scrqs511knw0k5nnlvnk0k8yj64xnc5hhjhg9w5")))

(define-public crate-dill-impl-0.4.0 (c (n "dill-impl") (v "0.4.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vbmm6wf0x4q7zb7y8iffg7dzqxv7sy35f2j9rwl4ld5y2arh7jv")))

(define-public crate-dill-impl-0.5.0 (c (n "dill-impl") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "027vs3zfgx1r53wp9nf02bda06c4bb3yzyx8q7zr29821lrg00lx")))

(define-public crate-dill-impl-0.5.1 (c (n "dill-impl") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kvpnriick8lnxn8x6msq0g4baqnzg01pndc82x4gi0jxz786c6l")))

(define-public crate-dill-impl-0.5.2 (c (n "dill-impl") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0xl8jrp23kjrrby5138pm3002z5nipzbqls7qkr2jpw04744w5")))

(define-public crate-dill-impl-0.5.3 (c (n "dill-impl") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dglihf0bp5nfcj54wdbzi717hns6xxvxbh7k9pi3z5dc5x8vdys")))

(define-public crate-dill-impl-0.6.0 (c (n "dill-impl") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1fpk4ixa0nfbf4zp85l0qnsqfbqqs96g0xgz8jagm8g0dks8yrxp")))

(define-public crate-dill-impl-0.6.1 (c (n "dill-impl") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00fcp1ivq9083kl5cpv5zcf003r2q725ls7ak5rshwly556cgr25")))

(define-public crate-dill-impl-0.7.0 (c (n "dill-impl") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0dwqj5f2k2jpn8nqmg7mp5barnp99h6yr6vzy5zbjjg0586kba8a")))

(define-public crate-dill-impl-0.7.1 (c (n "dill-impl") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1s6cz79c72cjcb0h47kdak8aaam314575zawakfskx2wfrxs7kgz")))

(define-public crate-dill-impl-0.7.2 (c (n "dill-impl") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zhdnrh79aybl3mdb9jf6xsv52paaaq555hhmxqah3zm4kz76lc0")))

(define-public crate-dill-impl-0.8.0 (c (n "dill-impl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i3ll66p14si8vnph40hv58z90jgh4gama5y6inad15nv4zjc3sd")))

