(define-module (crates-io di at diatermi) #:use-module (crates-io))

(define-public crate-diatermi-0.1.0 (c (n "diatermi") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0gsr9pjm2cfz47cvksgsmqarjs799qc63b0xnsx171bm8hjg7gqx")))

(define-public crate-diatermi-0.1.1 (c (n "diatermi") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0nf7bd8fygkggcbxqmg5f0r43qzy0mqa5760b24ksmc6anh1038p")))

(define-public crate-diatermi-0.1.2 (c (n "diatermi") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "17yzcpgha436qqv4m34i509slf6qg340vb44svg7xwnv7a00h484")))

(define-public crate-diatermi-0.1.3 (c (n "diatermi") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1r717mps7cxqql6y74j7h6iblrsv7a52ny90l36l1z0ib40rj384")))

