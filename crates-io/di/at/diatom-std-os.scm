(define-module (crates-io di at diatom-std-os) #:use-module (crates-io))

(define-public crate-diatom-std-os-0.1.0 (c (n "diatom-std-os") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "diatom-core") (r "^0.6.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)))) (h "0hav0wi3am34k1n3rslgnijxvzlx0aznrn84yxl8yg2wf7i583rl")))

(define-public crate-diatom-std-os-0.1.1 (c (n "diatom-std-os") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "diatom-core") (r "^0.6.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "1dah1z7c5vxrx2akwb4z4p6y2g9q2sblfgicp9hqap7wvvcn8bhs")))

