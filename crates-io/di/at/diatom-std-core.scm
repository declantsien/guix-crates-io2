(define-module (crates-io di at diatom-std-core) #:use-module (crates-io))

(define-public crate-diatom-std-core-0.1.0 (c (n "diatom-std-core") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "diatom-core") (r "^0.6.0") (d #t) (k 0)))) (h "162xzvfn1bmsv9r5w5vbz2brhkv02pxkz0ylmdbjwyp8cwdbdrkd")))

(define-public crate-diatom-std-core-0.1.1 (c (n "diatom-std-core") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "diatom-core") (r "^0.6.0") (d #t) (k 0)))) (h "0aqw34r2vy9ryl4qdbly7bqh9fvbv3ix9ibin7jln5n9yj2in79b")))

