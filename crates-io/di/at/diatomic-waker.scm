(define-module (crates-io di at diatomic-waker) #:use-module (crates-io))

(define-public crate-diatomic-waker-0.1.0 (c (n "diatomic-waker") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(diatomic_waker_loom)") (k 0)) (d (n "waker-fn") (r "^1.1") (d #t) (t "cfg(diatomic_waker_loom)") (k 0)))) (h "1s9511f7lv0icp96xv3f0drz6m3j8dgmaxq8gg7mm0cxbasmy0i8") (r "1.56")))

