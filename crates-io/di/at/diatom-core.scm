(define-module (crates-io di at diatom-core) #:use-module (crates-io))

(define-public crate-diatom-core-0.6.0 (c (n "diatom-core") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0c447k1ih5jd8svl7ca2jcpxfcq406ix499nkfxgzymbq37kqhzl") (f (quote (("profile"))))))

(define-public crate-diatom-core-0.6.1 (c (n "diatom-core") (v "0.6.1") (d (list (d (n "ahash") (r "^0.8") (f (quote ("compile-time-rng" "std"))) (k 0)) (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1c2slvkhn6yp08mz24q5a80w2vgkkq3z5my5qrh1jp587nxnzfwf") (f (quote (("profile"))))))

