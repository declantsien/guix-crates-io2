(define-module (crates-io di at diath) #:use-module (crates-io))

(define-public crate-diath-0.1.0 (c (n "diath") (v "0.1.0") (h "19akdlvn0v0fcblv65dcpmhh38vc0vqkahv43k91rj6ibl6dq0p1")))

(define-public crate-diath-0.2.0 (c (n "diath") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync"))) (d #t) (k 0)))) (h "18n3ivq5bnrz4ckpk6342rff823p2vs6kysm0n6vr88g68f3kkw7")))

(define-public crate-diath-0.2.1 (c (n "diath") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync"))) (d #t) (k 0)))) (h "04kn1r401zch4yl6k0vah5b7krvvl9k0inr85glgq0k6wg9x3l99")))

(define-public crate-diath-0.2.2 (c (n "diath") (v "0.2.2") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "1ph4vzw8fha3qncqcws9sdpfaagchy8h25vbi62gh8fzki9mbzcr")))

(define-public crate-diath-0.2.3 (c (n "diath") (v "0.2.3") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0fyn0xfp23xi5smkpagj2yb2zf9hmdzwdns6rw6jjnrzi92abv95")))

(define-public crate-diath-0.3.0 (c (n "diath") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "031r2mqvf7l029xiwrxlh84nixf8aiw87785fxf4490dl6fcpz84")))

(define-public crate-diath-0.3.1 (c (n "diath") (v "0.3.1") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0i933cpyvqv8049d277y385j7yfvszs5ba00j3awpq7j01iwmylk")))

(define-public crate-diath-0.4.0 (c (n "diath") (v "0.4.0") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "1jmpf6iacmkpbg2pzi4j7qb6lg70vcy029axz211jrnym53z5qrw")))

(define-public crate-diath-0.4.1 (c (n "diath") (v "0.4.1") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0r42ql2r8vm4kiyxpylqzj0jm3n0q18pycvqsw9d40scwin331x8")))

(define-public crate-diath-0.5.0 (c (n "diath") (v "0.5.0") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0jxgv3nnjvwlhwfach9vk77zzcmp8zab7z305srmzdl96z97qrlp")))

(define-public crate-diath-0.5.1 (c (n "diath") (v "0.5.1") (d (list (d (n "tokio") (r "^1") (f (quote ("tokio-macros" "rt" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0a5xpf9liwf9vm2mib3mq2m235s3q5x4drzpxv7k0a08vyd76i6y")))

