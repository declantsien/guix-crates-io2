(define-module (crates-io di at diatom-cli) #:use-module (crates-io))

(define-public crate-diatom-cli-0.2.0 (c (n "diatom-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "diatom") (r "^0.6.0-alpha") (f (quote ("std-os"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)) (d (n "reedline") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "1084safccgh49lfyavijd70a9kw3fqlqj5izh1vzk1wfvjjv5zk1")))

(define-public crate-diatom-cli-0.2.1 (c (n "diatom-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (d #t) (k 0)) (d (n "diatom") (r "^0.6.0-alpha") (f (quote ("std-os"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nu-ansi-term") (r "^0.46") (d #t) (k 0)) (d (n "reedline") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0x7n5c1ad4hp4r6nd1dpib9ilnd3k717va271g1cspxn92ahdv9s")))

