(define-module (crates-io di ge digest-buffer) #:use-module (crates-io))

(define-public crate-digest-buffer-0.1.0 (c (n "digest-buffer") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "00d1a3k62afx2ipqm52sbhin3j1fw47ry6c3dg8j63sj60dv2q0i")))

(define-public crate-digest-buffer-0.1.1 (c (n "digest-buffer") (v "0.1.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1y4gjfp3dfiv4d6qnq2aakisyvxjaq6f4i7swd70x2ikl9aqwklf")))

(define-public crate-digest-buffer-0.1.2 (c (n "digest-buffer") (v "0.1.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0a1znq9w1v6zgjq8ypbvj8ksv9b9h5lj3msm5dc1limmx7yrsy2r")))

(define-public crate-digest-buffer-0.2.0 (c (n "digest-buffer") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "002zkda5fvxg3nlpwqb19iqmfihv91g5nb8j4cf1yjin8x5jnivr")))

(define-public crate-digest-buffer-0.3.0-alpha (c (n "digest-buffer") (v "0.3.0-alpha") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "11px1jjgdmrdkxn829056h4m0ks9q6qv6ngaq9b21pjjad47w8id")))

(define-public crate-digest-buffer-0.3.0 (c (n "digest-buffer") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "0vpwcyf0dq78c7gr7g3hjpflh30riig3a5zxzi7m6ybh769a21pr")))

(define-public crate-digest-buffer-0.3.1 (c (n "digest-buffer") (v "0.3.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "16nbyplli3sffagi9jradm01icj8shr0a9apj8axmlznx5j27faf")))

