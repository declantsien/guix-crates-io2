(define-module (crates-io di ge digest_auth) #:use-module (crates-io))

(define-public crate-digest_auth-0.1.0 (c (n "digest_auth") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "01alklcalmr9fak51pgnxvw7a05x3102ga84v8bh54zqf2qzdi4f")))

(define-public crate-digest_auth-0.1.1 (c (n "digest_auth") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0zypywzhcg7605m4bpinzj71jzng3c3qj91l8qxs5bxvdfazlgfc")))

(define-public crate-digest_auth-0.1.2 (c (n "digest_auth") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0g5764ahvw5kf8jqzfxz5hxn6v81jg34w71sjrsc81i29ipk0hwp")))

(define-public crate-digest_auth-0.1.3 (c (n "digest_auth") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0hscmxrx8gxjqbryajgyfwm5qhax5b4d4qxzs03sr17d48x83g06")))

(define-public crate-digest_auth-0.2.0 (c (n "digest_auth") (v "0.2.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "17dfnqajkfi9ncngajrsvzidk9z5bimy3bq5hnkfjflxygmgsp32")))

(define-public crate-digest_auth-0.2.1 (c (n "digest_auth") (v "0.2.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0b77jl50jpfzsciyp7s27vh0cnf17g46pr4ajls2rsrqrzcy9gb8")))

(define-public crate-digest_auth-0.2.2 (c (n "digest_auth") (v "0.2.2") (d (list (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "md-5") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1nbnsb3a9xn9c53z2wwvyri75ikrwcdxji12pvsvrz2909l5sb6b")))

(define-public crate-digest_auth-0.2.3 (c (n "digest_auth") (v "0.2.3") (d (list (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "10jlip6dd1sv6gdggbgqamrv5gjv5jg97g7wwc8d47a9p0bydg6l")))

(define-public crate-digest_auth-0.2.4 (c (n "digest_auth") (v "0.2.4") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0x0d6sh8rkvvcp9gi22bpki40mbp5m90l7lsymhg61wvchj5xz8j")))

(define-public crate-digest_auth-0.3.0 (c (n "digest_auth") (v "0.3.0") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "175w3ipivbq067nk6v4i8b9rigrrwy4hhjgyd27xikmji1wnac7s") (f (quote (("default"))))))

(define-public crate-digest_auth-0.3.1 (c (n "digest_auth") (v "0.3.1") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "155qpm01drp5n3adciw9x5xvmri2lnffkicn4y150pir3plg8m1h") (f (quote (("default"))))))

