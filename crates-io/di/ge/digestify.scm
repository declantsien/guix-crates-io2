(define-module (crates-io di ge digestify) #:use-module (crates-io))

(define-public crate-digestify-0.2.0 (c (n "digestify") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "15lk1smrc9nsl3pz1sn6npiads6134bn3jbc3sxrg4qnh9jk82fb")))

(define-public crate-digestify-0.2.1 (c (n "digestify") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "09fsz5n9bbg86xkhjivmdi3xzqnwnjn3bkvx7w4asaqc5fa2biqa")))

(define-public crate-digestify-0.3.0 (c (n "digestify") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.6") (d #t) (k 0)) (d (n "sha2") (r "^0.9.5") (d #t) (k 0)))) (h "06jp2ihbp09az8f6hjpp9a8qszi986ss1bff9d40yk5pwigh4hyn")))

(define-public crate-digestify-0.4.0 (c (n "digestify") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0x1bjbpv5f7lggn1w08mhf89bjzkfryks6fdnjqx6n4z7l2khnwg")))

