(define-module (crates-io di ge digest-hash) #:use-module (crates-io))

(define-public crate-digest-hash-0.1.0 (c (n "digest-hash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "1pkx6w13w66wb9m54rw60ybr6wgq4ajng36q4r4m53l047g80nhx")))

(define-public crate-digest-hash-0.2.0 (c (n "digest-hash") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 2)))) (h "0gp9kph02mk4n464rls2mp2j45jb8ayy71dwdnazlpv62mks61mq")))

(define-public crate-digest-hash-0.3.0 (c (n "digest-hash") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "1if8pmvdh9mi8m0vbs5xbspv56dd16xbg4ki3rl7y4pw1csibcvr")))

