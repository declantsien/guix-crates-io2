(define-module (crates-io di ge digestible-macros) #:use-module (crates-io))

(define-public crate-digestible-macros-0.1.0 (c (n "digestible-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h40ahy1k58mja253bc8rmid6dmj3mn0mghpiy3p9kjijlv11nnb")))

(define-public crate-digestible-macros-0.2.0-rc.1 (c (n "digestible-macros") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0smx4rj62qy1k0fvlnkiivxnwjb225j8yn8ljb2lqldiyviyx267")))

(define-public crate-digestible-macros-0.2.0-rc.2 (c (n "digestible-macros") (v "0.2.0-rc.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cldff2j62555djfwf99gi4x5j83klyw7zbvclr5x355xb7bg2vr")))

(define-public crate-digestible-macros-0.2.0 (c (n "digestible-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c2hxfxrfp9c4xcbdp1p4q16lg6jjdzihv23211wcfdb8yim5smz")))

(define-public crate-digestible-macros-0.2.1 (c (n "digestible-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a6n5nln35ifq23s7vgc0gi1ahh1gb5d5ywwnxa8blkqs164c3qa")))

(define-public crate-digestible-macros-0.2.2 (c (n "digestible-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01rbb3cn42l8m6sd9qrkj968cxli1n6zx2wr2h69qpcprd3d37xx")))

