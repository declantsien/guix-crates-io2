(define-module (crates-io di ge digenum) #:use-module (crates-io))

(define-public crate-digenum-0.1.0 (c (n "digenum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1qnshs79dbmbwfl5p6ii1yvlq6qxf0hax4rqx03wi5rf0w6q67r9")))

(define-public crate-digenum-0.1.1 (c (n "digenum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "07r2mngk2nkw3c9f623w105h0ay5a0lx2nv7y21n9gp34yr12hir")))

