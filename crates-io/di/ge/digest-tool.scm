(define-module (crates-io di ge digest-tool) #:use-module (crates-io))

(define-public crate-digest-tool-0.1.2 (c (n "digest-tool") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.1.0") (d #t) (k 0)))) (h "0ckwmb6w8akbhpm7x13vx1lcbf5cnilx3278x7353bsg242llixi")))

