(define-module (crates-io di ge digest-writer) #:use-module (crates-io))

(define-public crate-digest-writer-0.1.0 (c (n "digest-writer") (v "0.1.0") (d (list (d (n "digest") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.3.0") (d #t) (k 2)))) (h "081zf2dxcwxnshmprn5igh243sjhalvhwfsnasibr1s3k2nykw1j")))

(define-public crate-digest-writer-0.1.1 (c (n "digest-writer") (v "0.1.1") (d (list (d (n "digest") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.3.0") (d #t) (k 2)))) (h "0xifyzkcc86a54wn6zlv5598dws2pz0dk016g0rkfwaxidjhzyv8")))

(define-public crate-digest-writer-0.1.2 (c (n "digest-writer") (v "0.1.2") (d (list (d (n "digest") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.3.0") (d #t) (k 2)))) (h "1h8iz1vk5vzfrxw9zhccbjam1qcfg4vd0q2mij89c6f2n4gc7145")))

(define-public crate-digest-writer-0.2.0 (c (n "digest-writer") (v "0.2.0") (d (list (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "digest-writer") (r "^0.1.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 2)))) (h "1f050zljc7aj7chdabgh9sqabi1fqir5z0cdn3xfz3pahknlhpi0")))

(define-public crate-digest-writer-0.3.0 (c (n "digest-writer") (v "0.3.0") (d (list (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)))) (h "1mifrxxczqrgfsi7nsvr2hw25d07mpnvi4ph5l9iy3vsn7ncxv9z")))

(define-public crate-digest-writer-0.3.1 (c (n "digest-writer") (v "0.3.1") (d (list (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 2)))) (h "1mn720v6avisdjphafkmbzw6va7ckz8w8i0hga5d9q8liyapcmmc")))

