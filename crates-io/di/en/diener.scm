(define-module (crates-io di en diener) #:use-module (crates-io))

(define-public crate-diener-0.1.0 (c (n "diener") (v "0.1.0") (d (list (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "18967gm72jcpkhklbfg2vgmyj06hx9iji84apajr3dzjan8whxvz")))

(define-public crate-diener-0.2.0 (c (n "diener") (v "0.2.0") (d (list (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "14ijryp1nvsy0h4250d2kdr4p2vlzd20j6laxmf5vj2c6dvb8cd5")))

(define-public crate-diener-0.3.0 (c (n "diener") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0k7r31sw9f1zi912vx1qpvksf2hnqrh3zc203cp98sxd6nf4w2v3")))

(define-public crate-diener-0.3.1 (c (n "diener") (v "0.3.1") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1fi1djzld2d19zy8zp708x435afalxr2i3fhvwrll93x37b9sz35")))

(define-public crate-diener-0.3.2 (c (n "diener") (v "0.3.2") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ms57mgrcrxf9dxx149anhkzzp2bkxd646362a1y7l9xp8mwc7ax")))

(define-public crate-diener-0.3.3 (c (n "diener") (v "0.3.3") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0awa1a0pw9indqy8nv2hcj9ga17qlrq0ri49cdkc38327ksddi33")))

(define-public crate-diener-0.4.0 (c (n "diener") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0i398h1xxx022443bfxkvs321f0lvh4jqkhaz2404wf208qnq1pn")))

(define-public crate-diener-0.4.1 (c (n "diener") (v "0.4.1") (d (list (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1vxcjn2as9c2wwxka5m68jxc7lzg6alfnpk58h15mpspx04zvqmk")))

(define-public crate-diener-0.4.2 (c (n "diener") (v "0.4.2") (d (list (d (n "cargo_metadata") (r "^0.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0fjl5npq13xci3fvxk0fkf1lygfq4ng675clvwyhi72bki8df9a5")))

(define-public crate-diener-0.4.5 (c (n "diener") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml_edit") (r "^0.14.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1v52i70flv9iqcg0816ajryxs60ljcvwg5n8j41qc412x89dv23v")))

(define-public crate-diener-0.4.6 (c (n "diener") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "git-url-parse") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml_edit") (r "^0.16.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0xfdnv1i4bjy534yks04hd1454af9i2f2fwb8r610kimgqzkrl6v")))

