(define-module (crates-io di li dilib-macros) #:use-module (crates-io))

(define-public crate-dilib-macros-0.2.0-alpha (c (n "dilib-macros") (v "0.2.0-alpha") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "0b99gsvkgknvb1cs90ld3z3yzjl38j9q7ph12rgdf40n3jzqgfrk") (r "1.59.0")))

(define-public crate-dilib-macros-0.2.1-alpha (c (n "dilib-macros") (v "0.2.1-alpha") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1ha4dhby1fqj6xv1lz775lwx1mnfzp9sidwi0hsdhmx4ywrysrv3") (r "1.59.0")))

(define-public crate-dilib-macros-0.2.0 (c (n "dilib-macros") (v "0.2.0") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "01ypmjpwdaf9ywn8mkfk5mwyyq61y46gqs4p1ww7d2abdvdfdvzs") (r "1.59.0")))

(define-public crate-dilib-macros-0.2.1 (c (n "dilib-macros") (v "0.2.1") (d (list (d (n "const_format") (r "^0.2.22") (d #t) (k 0)) (d (n "dilib") (r "^0.2.0") (d #t) (k 2)) (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "07967ppmlxsmfqqh65nzb20wsa31iqq06v74kk59jln02njqkj1i") (r "1.59.0")))

