(define-module (crates-io di li diligent-date-parser) #:use-module (crates-io))

(define-public crate-diligent-date-parser-0.1.0 (c (n "diligent-date-parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1556s4mi43m0b4hwk5zydhi4n4jnk36mcq1w9fqh7ka6q7d2p16r")))

(define-public crate-diligent-date-parser-0.1.1 (c (n "diligent-date-parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1k2gy66b1hjybrv5y0xd6cnzjskiyzh28x5lrayj56sbnq7cmji8")))

(define-public crate-diligent-date-parser-0.1.2 (c (n "diligent-date-parser") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "113dxlw97d3zpjfxs95zvy7vz668wsl0dk8vyyhzr2qvy0laazp3")))

(define-public crate-diligent-date-parser-0.1.3 (c (n "diligent-date-parser") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "0817g67b11z0cmj1kimqwkfrpzvg8ql5cv4cb1n2sbn0qyazvl62")))

(define-public crate-diligent-date-parser-0.1.4 (c (n "diligent-date-parser") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "10hidg41gzgqykqlqfs4l1wjjxparmiwnjzqccij4ji7jki7zkzn")))

