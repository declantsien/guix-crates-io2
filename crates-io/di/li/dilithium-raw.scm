(define-module (crates-io di li dilithium-raw) #:use-module (crates-io))

(define-public crate-dilithium-raw-0.1.0 (c (n "dilithium-raw") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.5") (d #t) (k 2)))) (h "190z8zqj9v6zfc33vbdjz4dj01sqjix65ls9pdn51ncpb3mj95w6") (f (quote (("hazmat") ("dilithium5") ("dilithium3") ("dilithium2") ("default" "dilithium2" "dilithium3" "dilithium5" "avx2" "aarch64") ("avx2") ("aarch64")))) (s 2) (e (quote (("serde" "dep:serde"))))))

