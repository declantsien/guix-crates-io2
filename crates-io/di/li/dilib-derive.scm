(define-module (crates-io di li dilib-derive) #:use-module (crates-io))

(define-public crate-dilib-derive-0.1.0-alpha (c (n "dilib-derive") (v "0.1.0-alpha") (d (list (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("parsing"))) (d #t) (k 0)))) (h "133bkp7g2nc4ksc60c1xfrpk04l5fz17dm8jdg9nn8l30828vdyd")))

(define-public crate-dilib-derive-0.2.0-alpha (c (n "dilib-derive") (v "0.2.0-alpha") (d (list (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0gbirizzmww56ad4gigy1yix265iql1zl78pjxqjzm5hprdh3khf") (r "1.59.0")))

(define-public crate-dilib-derive-0.2.1-alpha (c (n "dilib-derive") (v "0.2.1-alpha") (d (list (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0z3hqcixm1v7cip6r99z8d5028jmdxv1z2m0qkqzq7cx2mf82zx3") (r "1.59.0")))

(define-public crate-dilib-derive-0.2.0 (c (n "dilib-derive") (v "0.2.0") (d (list (d (n "mattro") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1qp1pmw691hbcl916vlmni9llb7il52sv5x23zvqmb85jjs1d4ky") (r "1.59.0")))

