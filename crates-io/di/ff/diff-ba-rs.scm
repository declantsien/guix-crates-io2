(define-module (crates-io di ff diff-ba-rs) #:use-module (crates-io))

(define-public crate-diff-ba-rs-0.1.0 (c (n "diff-ba-rs") (v "0.1.0") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)))) (h "0bg8ykf8dvp2nkj62yv3whnb8c45dvb1rbihx8snq01wwpn6cf3n")))

(define-public crate-diff-ba-rs-0.1.1 (c (n "diff-ba-rs") (v "0.1.1") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)))) (h "0snmsfbaa7icjrp6ni5qy0nhihqsmyw5p1dg7c4z9gan8zcs4898")))

(define-public crate-diff-ba-rs-0.1.2 (c (n "diff-ba-rs") (v "0.1.2") (d (list (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)))) (h "0ag00dwglir9m6g1vzc5xj66ii513mf9pr9r72nry4iis7jrax75")))

