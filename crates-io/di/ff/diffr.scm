(define-module (crates-io di ff diffr) #:use-module (crates-io))

(define-public crate-diffr-0.1.0 (c (n "diffr") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1v3p3ssfrsn3yvzy2dbyhp79cxw2pvj8mcryag8ipigw6k705p8h")))

(define-public crate-diffr-0.1.1 (c (n "diffr") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0l1maca58lcayfagnrjr20arrnr41qp2d7h19dlf5nia8s4r270d")))

(define-public crate-diffr-0.1.2 (c (n "diffr") (v "0.1.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "diffr-lib") (r "^0.1.2") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1pnd4mw5grivb4vsg92a9lhhbrgql66h9kpg3illpf25np9mkmv7")))

(define-public crate-diffr-0.1.3 (c (n "diffr") (v "0.1.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "diffr-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "0v0dp3hs7hy6d8b44ynj9xvn8afd5s42v3nhvbj2szll3rmfzvfp")))

(define-public crate-diffr-0.1.4 (c (n "diffr") (v "0.1.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "diffr-lib") (r "^0.1.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1b0mz1ki2ksxni6g49x5l5j9ijpyhc11mywvxr9i9h3nr098nc5l")))

(define-public crate-diffr-0.1.5 (c (n "diffr") (v "0.1.5") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1kdngd5g1ssdiq7d10jr3jwg0sx740x3vmhq3j594a5kd467ikib")))

