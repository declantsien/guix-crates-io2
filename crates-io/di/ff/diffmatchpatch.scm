(define-module (crates-io di ff diffmatchpatch) #:use-module (crates-io))

(define-public crate-diffmatchpatch-0.0.0 (c (n "diffmatchpatch") (v "0.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1cjj16axcwfsxlfzf6k1kqw5w5xi2y79c1n06xrnrrbvdzaszijn")))

(define-public crate-diffmatchpatch-0.0.1 (c (n "diffmatchpatch") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)))) (h "0mwdhm0r9fc6qrl728lq29wsjmp9d5rafr3ck5wzfyfxqbz2vll0")))

(define-public crate-diffmatchpatch-0.0.2 (c (n "diffmatchpatch") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)))) (h "0rddqx25afcf8xh9mg9gsk9bynj99j50cfiz5dy5wa6fdymx2avw")))

(define-public crate-diffmatchpatch-0.0.3 (c (n "diffmatchpatch") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)))) (h "0ryx105vfv1z3y1amw3rc8bjn1ib3rddv7kr1gkr3zyzdn88zb87")))

(define-public crate-diffmatchpatch-0.0.4 (c (n "diffmatchpatch") (v "0.0.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1j76bv4hg6ijdc2gmcqk7kf19g69g7xhh7zh9dmkxhcfhj93dga2")))

