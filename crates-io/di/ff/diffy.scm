(define-module (crates-io di ff diffy) #:use-module (crates-io))

(define-public crate-diffy-0.0.0 (c (n "diffy") (v "0.0.0") (h "18m1a0qwax8pww303k61zhm3yaxjm1lqc9qx9g6wq7bghzi35yql")))

(define-public crate-diffy-0.1.0 (c (n "diffy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "0jygzfayb3mygklcvzi0hsypa2fqmf5qasrb4hycaynqya5vjx7x")))

(define-public crate-diffy-0.1.1 (c (n "diffy") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "0xs8nn4k5fz7pan3d8fsfap58v92xkrzb0wvdik5qdsj1hd58nlj")))

(define-public crate-diffy-0.2.0 (c (n "diffy") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "1rz1fak5ahi1ss8wab83smx4qr9amx63yrbqwclhgljl97lz01zq")))

(define-public crate-diffy-0.2.1 (c (n "diffy") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "0mpz1fkk4l098fl5z7nq66z3dl983w3rab4bz1c3339m7y7g87qc")))

(define-public crate-diffy-0.2.2 (c (n "diffy") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)))) (h "1nyk6jdlaz6p8srm2mc8sq3f8dbx3f3bnw35w1iw0qwsz37cfzn2")))

(define-public crate-diffy-0.3.0 (c (n "diffy") (v "0.3.0") (d (list (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)))) (h "145p68n751bk2vmsdm4cymak71c7c986y5bh97l5f8n9an8ya5p6")))

