(define-module (crates-io di ff diffuser-parser) #:use-module (crates-io))

(define-public crate-diffuser-parser-0.0.0 (c (n "diffuser-parser") (v "0.0.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "0dd7yzvgdzb7b3prgxdy5wcb1d7ylb6if3yvxbs2ck8dzyqc8s4w") (f (quote (("default"))))))

