(define-module (crates-io di ff differential-evolution) #:use-module (crates-io))

(define-public crate-differential-evolution-0.1.3 (c (n "differential-evolution") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "15ny36jcq1kykskmdv6fqzv0ix8p6wpygs1ssdwgrz575pkiv1pq")))

(define-public crate-differential-evolution-0.2.0 (c (n "differential-evolution") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0gd9kmjzf3hd8dn4m8yi9fv7njff9fjz9w6074zr2d0a0xlv331q")))

(define-public crate-differential-evolution-0.2.1 (c (n "differential-evolution") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "18f7c7n8zhzvlc4rk09hr4j4c4jb5g4p5qqy1abppzg8ls0l0k9d")))

(define-public crate-differential-evolution-0.2.2 (c (n "differential-evolution") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0l7zn8bgjv3rp7yb8wb2zai0ybq9r7dv7f626asiw846b2lmp7md")))

