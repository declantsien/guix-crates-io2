(define-module (crates-io di ff diff-struct) #:use-module (crates-io))

(define-public crate-diff-struct-0.1.0 (c (n "diff-struct") (v "0.1.0") (d (list (d (n "diff_derive") (r "^0.1.0") (d #t) (k 0)))) (h "132czic31hy16y83pvp7plng0b3v4azj6if5ba1jjwa4q4cmaa24")))

(define-public crate-diff-struct-0.2.0 (c (n "diff-struct") (v "0.2.0") (d (list (d (n "diff_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "038zjc4vs83pr7pxq1sqc00rmwaqmc16sc1zyy0ny74qw9gbgmwy")))

(define-public crate-diff-struct-0.2.1 (c (n "diff-struct") (v "0.2.1") (d (list (d (n "diff_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v6x7236naxj7fzyhrhcis3wrlpa5f1cgfpyb4i33a7ii97lax40")))

(define-public crate-diff-struct-0.3.0 (c (n "diff-struct") (v "0.3.0") (d (list (d (n "diff_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vqdcdic1s0w1iq03s2hp95hbprfmkxv07mjmn1pxbvhh8zyz1zh")))

(define-public crate-diff-struct-0.3.1 (c (n "diff-struct") (v "0.3.1") (d (list (d (n "diff_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09p8i3fzk4233dc1vqqpgzs4avv421rqkcc8hdchvnn8mw50y2pk")))

(define-public crate-diff-struct-0.4.0 (c (n "diff-struct") (v "0.4.0") (d (list (d (n "diff_derive") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0brr63yrz98psb4gi3bgmm53gknbx242nai4hgha86cvdq3kxmpv")))

(define-public crate-diff-struct-0.4.1 (c (n "diff-struct") (v "0.4.1") (d (list (d (n "diff_derive") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0lpmxmaa226xph4k74inpb6ldnrfc2fa06d8niq2n5i6f3fmy3fl")))

(define-public crate-diff-struct-0.4.2 (c (n "diff-struct") (v "0.4.2") (d (list (d (n "diff_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "01si771l0bj1jpnjdyfhd0vzqghgxybggvczc7amav8hg0mql4k2")))

(define-public crate-diff-struct-0.5.0 (c (n "diff-struct") (v "0.5.0") (d (list (d (n "diff_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z655vwfzn2fm8z60hcpqakaibmpg4im1psn2rzvrs19jyrf889p") (f (quote (("impl_num" "num") ("default" "impl_num"))))))

(define-public crate-diff-struct-0.5.1 (c (n "diff-struct") (v "0.5.1") (d (list (d (n "diff_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "147w6ipngfr2bgcsxxw4prr4za8cr6li1wz8sdpj5ikks8rrss9s") (f (quote (("impl_num" "num") ("default" "impl_num"))))))

(define-public crate-diff-struct-0.5.2 (c (n "diff-struct") (v "0.5.2") (d (list (d (n "diff_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18j1cqvw1n72z4j1pdjdfcksvasx0xm42n36f8z78cdjpw73cv6j") (f (quote (("impl_num" "num") ("default" "impl_num"))))))

(define-public crate-diff-struct-0.5.3 (c (n "diff-struct") (v "0.5.3") (d (list (d (n "diff_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_derive") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p0789g4kzbv8062asxhljbc4d0cvj9vh94bfv5zfc9b261w1akr") (f (quote (("impl_num" "num") ("default" "impl_num"))))))

