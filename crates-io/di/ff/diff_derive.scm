(define-module (crates-io di ff diff_derive) #:use-module (crates-io))

(define-public crate-diff_derive-0.1.0 (c (n "diff_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1483l6wbbkml1mqf15610wm49kw479q0qcjmgrg0ikhq7mdn5dl2")))

(define-public crate-diff_derive-0.1.1 (c (n "diff_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0vhh9fwlcn6zmqy4mmjg2zvg1pawjr31mjyfydb5675j09bgr3ih")))

(define-public crate-diff_derive-0.1.2 (c (n "diff_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1cwxwshdm2jvp6n009dw7mp1p4w8c8f456alg30xjmjl75yw7yan")))

(define-public crate-diff_derive-0.2.0 (c (n "diff_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "027h4qi33zfdfppwijp6a8y2zgkja4yby4vx8kj3m789d9rhfg8f")))

(define-public crate-diff_derive-0.2.1 (c (n "diff_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0yjfdhi7bj56q6a2dg0cvfal9r5s4lxykz07bzl6wp5l316zcwm1")))

(define-public crate-diff_derive-0.2.2 (c (n "diff_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0cf0jnsvfifynnspfy5gjlljj7z7sy9kxn8382mja1rynb816gd7")))

(define-public crate-diff_derive-0.2.3 (c (n "diff_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0bcilj26fmcwzyc70vg45h05qyqm45x1mqbw9ksbnsqrmmz5w5py")))

