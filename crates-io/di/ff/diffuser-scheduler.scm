(define-module (crates-io di ff diffuser-scheduler) #:use-module (crates-io))

(define-public crate-diffuser-scheduler-0.0.0 (c (n "diffuser-scheduler") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 2)))) (h "0v3z8iizkcins9j0pj1xwiyyyhgg6a4762200m920rpj1xbfr5yr") (f (quote (("default"))))))

