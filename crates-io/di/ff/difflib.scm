(define-module (crates-io di ff difflib) #:use-module (crates-io))

(define-public crate-difflib-0.1.0 (c (n "difflib") (v "0.1.0") (h "1nv6msd2k7ympj878bngzvcbv6bsag5afarf7hybi5nd3pd2zmi6")))

(define-public crate-difflib-0.2.0 (c (n "difflib") (v "0.2.0") (h "1wykcpn3djj6hzl6yw05naqazv99xnz3j2z99pkmg7w5l0zvg8jc")))

(define-public crate-difflib-0.3.0 (c (n "difflib") (v "0.3.0") (h "1kgvxy120la3zyrqvksrwdbdvb8az1aqv2xskvdln4qp76w5zvv4")))

(define-public crate-difflib-0.3.1 (c (n "difflib") (v "0.3.1") (h "1ga43r988yspbi8q7n5dnfmil8sm38wq9585nc9j5i3xdb6c4zfw")))

(define-public crate-difflib-0.4.0 (c (n "difflib") (v "0.4.0") (h "1s7byq4d7jgf2hcp2lcqxi2piqwl8xqlharfbi8kf90n8csy7131")))

