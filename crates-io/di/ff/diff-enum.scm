(define-module (crates-io di ff diff-enum) #:use-module (crates-io))

(define-public crate-diff-enum-0.1.0 (c (n "diff-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00n61amjj57sh9gxkzlbv5ahg6m0jd3dkskf7srw5j1blmpgxnpp")))

(define-public crate-diff-enum-0.1.1 (c (n "diff-enum") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "171i5d20h65jhpcyssf6zlwsxky4p79hj1akmli5lsfi3bx2gi5j")))

(define-public crate-diff-enum-0.1.2 (c (n "diff-enum") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0yzaiqyhvk7b38zimbaf63a2la7ylsw9ksb2dfwyz12n1rrxd1i4")))

(define-public crate-diff-enum-0.1.3 (c (n "diff-enum") (v "0.1.3") (d (list (d (n "cargo-husky") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "00dmnvwm7vpgw7n7kxxqkrklfswnf0742aki6i7dgf9cj9lgyv5c")))

(define-public crate-diff-enum-0.1.4 (c (n "diff-enum") (v "0.1.4") (d (list (d (n "cargo-husky") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "079npxm240wdz2hlx1lff32fmh5hda448cpiilggp4h0ciym6xrp")))

