(define-module (crates-io di ff differential-geometry) #:use-module (crates-io))

(define-public crate-differential-geometry-0.1.0 (c (n "differential-geometry") (v "0.1.0") (d (list (d (n "generic-array") (r ">= 0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.1") (d #t) (k 0)))) (h "0cn80ik46pjd8zwgwkkm3g3n9alcwyx64dihbmh7zdp4q7vxfksw")))

(define-public crate-differential-geometry-0.2.0 (c (n "differential-geometry") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)) (d (n "typenum") (r "^1.7") (d #t) (k 0)))) (h "0k3amk9zk68w3dyvj0icy016g1kq5dw67b36l0ah76bp2pp8c1lq")))

(define-public crate-differential-geometry-0.2.1 (c (n "differential-geometry") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.8.2") (d #t) (k 0)))) (h "1x7k0piak2mqds572zai6ayf350isx6f3n9n848jnc6rhsz7gxlv")))

(define-public crate-differential-geometry-0.2.2 (c (n "differential-geometry") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1kfa4xjvpmp0k2nq6654im72v3m88j9jc2zd4b4pvy2md0ngvgap")))

(define-public crate-differential-geometry-0.3.0 (c (n "differential-geometry") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "04q636lq92w27jcvyr99qwdg24ngg639c0gps67pfixnsm4c1v65")))

(define-public crate-differential-geometry-0.3.1 (c (n "differential-geometry") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1h3vvm6krvq3fifjw254pv4aybg7pbdjbwb2wrda3lffikwc47c0")))

