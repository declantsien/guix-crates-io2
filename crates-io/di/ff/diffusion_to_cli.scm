(define-module (crates-io di ff diffusion_to_cli) #:use-module (crates-io))

(define-public crate-diffusion_to_cli-0.1.0 (c (n "diffusion_to_cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diffusion_to") (r "^0.2.0") (f (quote ("clap"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread" "fs"))) (d #t) (k 0)))) (h "1smw0cwwbp63cj9g9lvcfj4pgfy9ff5fbvqmj2ml7c9d35sc9ym4")))

