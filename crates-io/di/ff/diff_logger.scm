(define-module (crates-io di ff diff_logger) #:use-module (crates-io))

(define-public crate-diff_logger-0.1.0 (c (n "diff_logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0mvj4shkfjlvqnfdsag9nwfansp46hykayr38jzg54hq5fak0fcs")))

