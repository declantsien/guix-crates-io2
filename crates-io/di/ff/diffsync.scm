(define-module (crates-io di ff diffsync) #:use-module (crates-io))

(define-public crate-diffsync-0.1.0 (c (n "diffsync") (v "0.1.0") (d (list (d (n "diff-struct") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "random_variant") (r "^0.2.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "1jchdg4wr8d72wsqzrm5l3kzqhr870rlj3r0p51j8zhb7pk1i958") (y #t)))

(define-public crate-diffsync-0.1.1 (c (n "diffsync") (v "0.1.1") (d (list (d (n "diff-struct") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "random_variant") (r "^0.2.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 2)))) (h "0ad3cafqmygdqak0fsj83b8rs6a23rnc4qchrbv64awcydxkdjr2") (y #t)))

