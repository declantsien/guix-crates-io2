(define-module (crates-io di ff difference_engine) #:use-module (crates-io))

(define-public crate-difference_engine-0.1.0 (c (n "difference_engine") (v "0.1.0") (d (list (d (n "clap") (r "~2.5.2") (d #t) (k 0)) (d (n "colored") (r "~1.2.0") (d #t) (k 0)) (d (n "diff") (r "~0.1.9") (d #t) (k 0)) (d (n "itertools") (r "~0.4.15") (d #t) (k 0)) (d (n "libloading") (r "~0.2.2") (d #t) (k 0)))) (h "00c4z21siwpz7iwv3ghirga6jczx47lafs65cqcifwa7sy723v8g")))

