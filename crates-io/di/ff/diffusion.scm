(define-module (crates-io di ff diffusion) #:use-module (crates-io))

(define-public crate-diffusion-0.5.1 (c (n "diffusion") (v "0.5.1") (h "1402qbdfvgasx0zgiim1pv8dckhffbml8b3a1nbqwm1i91ry9s3h")))

(define-public crate-diffusion-0.6.1 (c (n "diffusion") (v "0.6.1") (h "135ilqzllnlydi3d2pdvvlfd1qbphkkxnmzs2phb57ry3sbvy331")))

(define-public crate-diffusion-0.6.2 (c (n "diffusion") (v "0.6.2") (h "1cv3jsgj6wxs9j4v8l37v471y3q059cnzgbmnvb59gch07917s2v")))

(define-public crate-diffusion-0.7.0 (c (n "diffusion") (v "0.7.0") (h "1kg1lxp1zmsxh832fcwlpckm6hkbcrgl4hh2dc0mh9s99jdafip9")))

(define-public crate-diffusion-0.7.1 (c (n "diffusion") (v "0.7.1") (h "1yvla8jg5az3kbar7x42ik8ks5nf90scnb6rf2i4cjh3sdhbvgpq")))

(define-public crate-diffusion-0.7.2 (c (n "diffusion") (v "0.7.2") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1pljikd6ygavrmsaasyx25rv9922p45vvchs80nvb8np2mzk08n3")))

(define-public crate-diffusion-0.7.3 (c (n "diffusion") (v "0.7.3") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0lamw4gmzhwqcvr4wp8cq6azhwyncagmsqvcm40p54s5jadmg9nq")))

(define-public crate-diffusion-0.7.4 (c (n "diffusion") (v "0.7.4") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1gizr1jl8nwxrzaf3gpzyyxw78dvq4nnp6ripb56ljv864z0rg0m")))

(define-public crate-diffusion-0.8.0 (c (n "diffusion") (v "0.8.0") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1abzwybn48vib60c6w5cfx633qavjxn1wq7ppm8nvlk0b24gz0nr")))

(define-public crate-diffusion-0.8.1 (c (n "diffusion") (v "0.8.1") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0mz1m4ra6a251zwfpjhrzrr2kv6zncsag5dsc5dmps01vr2qp9s3")))

(define-public crate-diffusion-0.8.2 (c (n "diffusion") (v "0.8.2") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1b8y03bwaii10m1digwm5bafv0lj18dw0iybw1nalwd060cxkw0v")))

(define-public crate-diffusion-0.9.0 (c (n "diffusion") (v "0.9.0") (d (list (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "1qay7253vypj5lr4l7f10aanfx6n1fnb77yfqbxwxsy1jybg9lf7")))

