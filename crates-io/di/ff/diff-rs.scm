(define-module (crates-io di ff diff-rs) #:use-module (crates-io))

(define-public crate-diff-rs-0.1.0 (c (n "diff-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "diffs") (r "^0.2.1") (d #t) (k 0)))) (h "1a4fbpaclwji3qbx31gph2qaljvijkhf5yckqqcfcjhbigakzljh")))

(define-public crate-diff-rs-0.2.0 (c (n "diff-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "diffs") (r "^0.2.1") (d #t) (k 0)))) (h "0xv3k6ckghv0sibj9f42fmza8a3byvgjmcrb6vmnn8sks5b6zbxs")))

(define-public crate-diff-rs-0.2.1 (c (n "diff-rs") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "diffs") (r "^0.2.1") (d #t) (k 0)))) (h "1cg07p106n28d2dl24s6nrfmmjhvrjnplz125aq62mhgi67lz611")))

(define-public crate-diff-rs-0.2.2 (c (n "diff-rs") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "diffs") (r "^0.2.1") (d #t) (k 0)))) (h "04ina1yhjn8agm60zbgkinij7rcrvcrrls264ljrg30gm5p1ahi0")))

(define-public crate-diff-rs-0.2.3 (c (n "diff-rs") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "diffs") (r "^0.2.1") (d #t) (k 0)))) (h "0skvjs4rac8zbqqndslcnkk8kpkkzq8hfdqkrbvrn2zc4kcvb9qx")))

