(define-module (crates-io di ff diff-parse) #:use-module (crates-io))

(define-public crate-diff-parse-0.1.0 (c (n "diff-parse") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (f (quote ("std"))) (k 0)) (d (n "regex") (r "^1.5.5") (f (quote ("perf" "std"))) (k 0)))) (h "1a1qfwa55c8fbhbm34fbr8p043x3zfrxva9n0j3r3qp1ngkzn1bd")))

