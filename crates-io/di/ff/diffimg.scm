(define-module (crates-io di ff diffimg) #:use-module (crates-io))

(define-public crate-diffimg-0.1.0 (c (n "diffimg") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1mjc5a2sizjg74vki4wjsdbq1dhsvxvmd6mjivb1hnpq46sgqdc2")))

(define-public crate-diffimg-0.1.1 (c (n "diffimg") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "0in8lkbvxpffjmn6r1aicmn87rfps464x0mrmphzg6hzn8i47frg")))

(define-public crate-diffimg-0.1.2 (c (n "diffimg") (v "0.1.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (k 0)) (d (n "image") (r "^0.20") (f (quote ("png_codec"))) (k 2)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1rmksnk8s5lka7l020ydd16pw5x73a39jm4jnjbzljbckfdn7h5d")))

(define-public crate-diffimg-1.0.0 (c (n "diffimg") (v "1.0.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.20") (k 0)) (d (n "image") (r "^0.20") (f (quote ("png_codec"))) (k 2)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "0b7dr9rn63fypkrcs33i4bpaxfb70v0dmmih55v2kzz4k0kmr33h")))

(define-public crate-diffimg-2.0.0 (c (n "diffimg") (v "2.0.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "01xq10w9zi0gac7w25ydrqpdnaa2sbmwxqd6nzzfvwkcnkjwmmzx")))

(define-public crate-diffimg-2.1.0 (c (n "diffimg") (v "2.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1gg9awr63508gb51h3x52jvmdp2g0vwi2gjkkqrqw0x8636svnbi")))

