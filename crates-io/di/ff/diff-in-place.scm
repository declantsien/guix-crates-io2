(define-module (crates-io di ff diff-in-place) #:use-module (crates-io))

(define-public crate-diff-in-place-0.1.0 (c (n "diff-in-place") (v "0.1.0") (h "1c060mms1npz0szy0q51288n48s6bpi18g5lcdn3kpjjjd5f4w32")))

(define-public crate-diff-in-place-0.1.1 (c (n "diff-in-place") (v "0.1.1") (h "1mb97arsgvd0hp8w61fykg1nrmkbz0gy3ndcyd36bfpgjlrg3zbb")))

(define-public crate-diff-in-place-0.1.2 (c (n "diff-in-place") (v "0.1.2") (h "1lxp9kbd2w27hxvksim1s32b7rk5fbmd0hsln6ilg6z1x51p700v")))

