(define-module (crates-io di ff diffus-derive) #:use-module (crates-io))

(define-public crate-diffus-derive-0.1.0 (c (n "diffus-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k6c5rvr7d7inh7r0vvxcpvclmn601aymxcb4w3agcblhcqqhnsa")))

(define-public crate-diffus-derive-0.1.1 (c (n "diffus-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s0h9qdcwlva7xc9w8zdnymbl6y0lh82yvf5qksgaccj8ml7abmg")))

(define-public crate-diffus-derive-0.2.0 (c (n "diffus-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rznz9pl6sjh0b2msmx12gc0i7g53dfr350z69dgn4xb83vwl3sv")))

(define-public crate-diffus-derive-0.3.0 (c (n "diffus-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01jjnv6z7m42skgnslq2x6qmk7ibrx5d7gzrb2qw1frycwndg50p")))

(define-public crate-diffus-derive-0.3.1 (c (n "diffus-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fk6w78y6h7gikl19n9pr99v9c0p231frxdrx7qk63wa0b8pwq7a") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.4.0 (c (n "diffus-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13616y7pgbxc3w5chsf56nj8l7n23sg9cv679v7yijdy87p6ycg3") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.5.0 (c (n "diffus-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n5xwia9wlf2ydnajqgwpzl1xsdrfzafh4ih0c4pdl08k0gi4k0s") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.5.1 (c (n "diffus-derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1caxhvgcxiv9kpixv3zvjg2j46knp3h9drfy67ixrnz4607mnzl5") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.6.0 (c (n "diffus-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rhfa952l6ga0ib4bmpaspiwbag1kc4i8ahs6aclbkn0b21nln52") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.6.1 (c (n "diffus-derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "116zyk9rdcj0nka49r7yf44vlfzvgg0835bp873hlsjg0xgz913r") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.7.0 (c (n "diffus-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0llf9jb11g1bjiwgy3hnr8xc5zg1bmn9cpyvz08qy6fv9w5f5wlb") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.8.0 (c (n "diffus-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yqagj7b0hnsvcjv9cdkqk8x96w5gcgm8n4c9s7mp99srqar5phk") (f (quote (("serialize-impl")))) (y #t)))

(define-public crate-diffus-derive-0.8.1 (c (n "diffus-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02ddpyzdy9l4y940y309dgrvkaijf4q5mqc8fb6v8vinfvw4ajld") (f (quote (("serialize-impl")))) (y #t)))

(define-public crate-diffus-derive-0.8.2 (c (n "diffus-derive") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i91a8mfnadrb7z9hrr247b8wbiw6fzydk80sraxhg0byxgddlg5") (f (quote (("serialize-impl")))) (y #t)))

(define-public crate-diffus-derive-0.8.3 (c (n "diffus-derive") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w2d6ncimvsjphx6dri9y2w2hqagc8x6q0iiphpa1d3szvphy85b") (f (quote (("serialize-impl")))) (y #t)))

(define-public crate-diffus-derive-0.9.0 (c (n "diffus-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03ndzc9dhhf6ff7ingmhx1dwmr7m15ky1swv1n53q36inxf8z3cq") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.9.1 (c (n "diffus-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0al8imxch6d6z2ksy12xbkpi6cfr5qmgpk68jhkv8lxdhw6iav5i") (f (quote (("serialize-impl"))))))

(define-public crate-diffus-derive-0.10.0 (c (n "diffus-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a7sgbzhqa7gk3xhvkci91myc86gkwxs04vfxbl8f64y7l1jsfmr") (f (quote (("serialize-impl"))))))

