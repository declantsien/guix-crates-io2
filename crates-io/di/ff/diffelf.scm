(define-module (crates-io di ff diffelf) #:use-module (crates-io))

(define-public crate-diffelf-0.1.0 (c (n "diffelf") (v "0.1.0") (h "0w0lnapk2bz4sp9zg57i46xxvjbmqamrikvbgx59irzqkqjzni8c")))

(define-public crate-diffelf-0.1.1 (c (n "diffelf") (v "0.1.1") (h "0nmvhgqxjy4rfnx4xmcgx819qcyhc6ai2zs3dnim41lih0wdz369")))

