(define-module (crates-io di ff diffeq) #:use-module (crates-io))

(define-public crate-diffeq-0.1.0 (c (n "diffeq") (v "0.1.0") (d (list (d (n "alga") (r "^0.9") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y3b3wm8mx1ic8nky7lmal9iyh3qm1k4x51jzn40dvnjh9rks262") (f (quote (("serde0" "serde"))))))

