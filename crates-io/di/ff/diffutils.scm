(define-module (crates-io di ff diffutils) #:use-module (crates-io))

(define-public crate-diffutils-0.3.0 (c (n "diffutils") (v "0.3.0") (d (list (d (n "diff") (r "^0.1.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0mzqddpv65qmlysb0mf5daglgngc7y7p98sds3k931pbyrgx64ja")))

(define-public crate-diffutils-0.4.0 (c (n "diffutils") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0yw8lp0qhb5cf5f3869x2jh7g6gjknj3p9z1scbp6dgk2ynmb8yk")))

(define-public crate-diffutils-0.4.1 (c (n "diffutils") (v "0.4.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "diff") (r "^0.1.13") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1181inmgbk7gw7p6i8n7lj7v8mj3hvsmrvz218qnd23lmkxy5sva")))

