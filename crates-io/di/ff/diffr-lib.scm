(define-module (crates-io di ff diffr-lib) #:use-module (crates-io))

(define-public crate-diffr-lib-0.1.1 (c (n "diffr-lib") (v "0.1.1") (h "1aj4gk7a2cr301kl3faz0pw2c6p2b0j10q26ryz5jyzcl0chr2wm") (y #t)))

(define-public crate-diffr-lib-0.1.2 (c (n "diffr-lib") (v "0.1.2") (h "0wwlg4aiimlq5xgm94jf4ilwnkgv2igblz8jv5768h0c09pxn31a") (y #t)))

(define-public crate-diffr-lib-0.1.3 (c (n "diffr-lib") (v "0.1.3") (h "0id3hpblvvcw4ydcd1cc7wgcwqjbh3grlihrmd8zp7k0d2h47i3g") (y #t)))

