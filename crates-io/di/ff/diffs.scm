(define-module (crates-io di ff diffs) #:use-module (crates-io))

(define-public crate-diffs-0.1.0 (c (n "diffs") (v "0.1.0") (h "0y9bhr46iwg62cz6jcyc9jq4ashywpqx922fnz0gnixivq5s4g2l")))

(define-public crate-diffs-0.2.0 (c (n "diffs") (v "0.2.0") (h "1b4mfwgy4czv5r7sgb4q3v6fafkjcb95aq9v2q0z3ahkz7lqav33")))

(define-public crate-diffs-0.2.1 (c (n "diffs") (v "0.2.1") (h "0vlynbvaa9nnab552cqmg37pprpzvsymwqp8bf8j3vxln95ng1pr")))

(define-public crate-diffs-0.3.0 (c (n "diffs") (v "0.3.0") (h "036sqycmir4bbl4016jprsyjq4hicc31r68dyqadmc8ac9pk55d1")))

(define-public crate-diffs-0.4.0 (c (n "diffs") (v "0.4.0") (h "18l1w5plhhsj8v8mxckw5ikbhvra1096p4n0kli5wivvya14akmv")))

(define-public crate-diffs-0.4.1 (c (n "diffs") (v "0.4.1") (h "07cpi60h9mddk62lysrybkrqlca8v8ffxhifv649x3fw250pn4d8")))

(define-public crate-diffs-0.5.0 (c (n "diffs") (v "0.5.0") (h "1nw5swngv4rfn1ah313fc1fa8fykwisvlmczaq6j7qbcmpvygz67")))

(define-public crate-diffs-0.5.1 (c (n "diffs") (v "0.5.1") (h "01f8bp77kgahgvr3s2igmnmsxynjklq830lmp2wp2jyph6bnq4gz")))

