(define-module (crates-io di ff diff) #:use-module (crates-io))

(define-public crate-diff-0.1.0 (c (n "diff") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.12") (d #t) (k 2)))) (h "0z77gs0lpn9lz7mq1ds9afvrk80s3l5bsf7pr0pdg3qm31pbd8l1")))

(define-public crate-diff-0.1.1 (c (n "diff") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.13") (d #t) (k 2)))) (h "1v4d4zg0smdcplih3k4qmqnw2326w2g6fss556jhinxzybp89125")))

(define-public crate-diff-0.1.2 (c (n "diff") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.14") (d #t) (k 2)))) (h "0v9l8xmfgln08wgcs73pz85a2z8fdgzarkg1lgnswz9gv8bdpqqm")))

(define-public crate-diff-0.1.3 (c (n "diff") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.14") (d #t) (k 2)))) (h "1xbnklz2d9zrmrklhm6m32iz9s3i2308mj90b3yzvpq5hyfydsh9")))

(define-public crate-diff-0.1.4 (c (n "diff") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.16") (d #t) (k 2)))) (h "0rkh10f3a1mh31rr28ipxqh42fi95lwihw59qy689nfnhp2qzddw")))

(define-public crate-diff-0.1.5 (c (n "diff") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "1zd2bq2nq9lsdcgakw99jqrbvm9lja9jkg8ci21xg8k0cz154hpv")))

(define-public crate-diff-0.1.6 (c (n "diff") (v "0.1.6") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "1zknp4mqbj7n9f2hgpg5q2jp2mbabxqdnn1lbzbb5y8dmkxvhkgm")))

(define-public crate-diff-0.1.7 (c (n "diff") (v "0.1.7") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.17") (d #t) (k 2)))) (h "09wqqjmbxn2qd2v4kk6g1afjhslflwnwz96wxsg813bkg96m2r6w")))

(define-public crate-diff-0.1.8 (c (n "diff") (v "0.1.8") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.18") (d #t) (k 2)))) (h "0sh44p154n6bz4jx48hp422kvsg3mq3sal2xy537k6fzikmr05qy")))

(define-public crate-diff-0.1.9 (c (n "diff") (v "0.1.9") (d (list (d (n "quickcheck") (r "^0.2.16") (d #t) (k 2)) (d (n "speculate") (r "^0.0.19") (d #t) (k 2)))) (h "11vmr1fhihndgi9l5y9chbfvb4ddcc9jxp62c9s71dykqvp7g2g4")))

(define-public crate-diff-0.1.10 (c (n "diff") (v "0.1.10") (d (list (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "speculate") (r "^0.0.24") (d #t) (k 2)))) (h "0wllnjdmiw5iz1vbx3yyvkf6cqg856y2gv8chlcq9h68nrhm8l8a")))

(define-public crate-diff-0.1.11 (c (n "diff") (v "0.1.11") (d (list (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "speculate") (r "^0.0.25") (d #t) (k 2)))) (h "0fhavni46a2rib93ig5fgbqmm48ysms5sxzb3h9bp7vp2bwnjarw")))

(define-public crate-diff-0.1.12 (c (n "diff") (v "0.1.12") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "16b40bhsa2qgvgvxs983l625pkxyp6m0mzmpwg2605cvj53yl98f")))

(define-public crate-diff-0.1.13 (c (n "diff") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "1j0nzjxci2zqx63hdcihkp0a4dkdmzxd7my4m7zk6cjyfy34j9an")))

