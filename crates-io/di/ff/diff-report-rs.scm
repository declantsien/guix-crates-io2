(define-module (crates-io di ff diff-report-rs) #:use-module (crates-io))

(define-public crate-diff-report-rs-0.1.0 (c (n "diff-report-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12n6pbflwhc5j8kmbkmq58zqiyapr07p9p1xicrjg60mx8pbnnz2")))

