(define-module (crates-io di ff differential-growth) #:use-module (crates-io))

(define-public crate-differential-growth-0.1.0 (c (n "differential-growth") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kd-tree") (r "^0.4.1") (f (quote ("nalgebra"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)))) (h "1c9sffcc0gzlwsw9crmj7d66pnc1zn6xw1wqr3nalgi3sfhvj4zl") (f (quote (("point_generators") ("default" "point_generators"))))))

(define-public crate-differential-growth-0.2.0 (c (n "differential-growth") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kd-tree") (r "^0.4.1") (f (quote ("nalgebra"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)))) (h "1rbh9w44cf2jxkplq1ws25g6qky6ry5y4p2ahj7ijm92m2xf8kbr") (f (quote (("point_generators") ("default" "point_generators"))))))

(define-public crate-differential-growth-0.3.0 (c (n "differential-growth") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kd-tree") (r "^0.4.1") (f (quote ("nalgebra"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)))) (h "0cvhzfjm2d6m2gyn7zpmy1laq83l1qiz9h6hb56bzhr2k0ckz0fv") (f (quote (("point_generators") ("default" "point_generators"))))))

(define-public crate-differential-growth-0.3.1 (c (n "differential-growth") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "kd-tree") (r "^0.4.1") (f (quote ("nalgebra"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 2)) (d (n "typenum") (r "^1.15.0") (d #t) (k 0)))) (h "1i53bp1przsky1cz9jv7gd85xczi67271cz05k02cb3yrh42ds2g") (f (quote (("point_generators") ("default" "point_generators"))))))

