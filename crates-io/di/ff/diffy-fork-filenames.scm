(define-module (crates-io di ff diffy-fork-filenames) #:use-module (crates-io))

(define-public crate-diffy-fork-filenames-0.3.0 (c (n "diffy-fork-filenames") (v "0.3.0") (d (list (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)))) (h "1nzy8965rxh6wkjrpksyq405qxsjiyqzh8drxg4g9kw7v8xgwrfk") (y #t)))

(define-public crate-diffy-fork-filenames-0.4.0 (c (n "diffy-fork-filenames") (v "0.4.0") (d (list (d (n "nu-ansi-term") (r "^0.46.0") (d #t) (k 0)))) (h "0vi403jhlicv0v5g4qzv2imh882zyciwdcz0kw89v4djxmqv3if1")))

