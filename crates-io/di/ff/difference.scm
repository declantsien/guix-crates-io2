(define-module (crates-io di ff difference) #:use-module (crates-io))

(define-public crate-difference-0.4.0 (c (n "difference") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.2.7") (d #t) (k 0)))) (h "17smq2q7j8bjwfcc4a6ahk70jjzhy7f1d144dxrrydmyxjxh7nw3")))

(define-public crate-difference-0.4.1 (c (n "difference") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.2.7") (d #t) (k 2)))) (h "1sn5h4crfahj6hyrv5n7y39mr5f1kms1wvmxsmp906l89qa4rvzz")))

(define-public crate-difference-1.0.0 (c (n "difference") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "term") (r "^0.2.7") (d #t) (k 2)))) (h "1a5v0b73z7vywbclll32wjsfkdgh6wn9prnq91z0d3lag4clsc5k") (f (quote (("default") ("bin" "getopts"))))))

(define-public crate-difference-2.0.0 (c (n "difference") (v "2.0.0") (d (list (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "term") (r "^0.2.7") (d #t) (k 2)))) (h "1621wx4k8h452p6xzmzzvm7mz87kxh4yqz0kzxfjj9xmjxlbyk2j") (f (quote (("default") ("bin" "getopts"))))))

