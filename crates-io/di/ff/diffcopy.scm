(define-module (crates-io di ff diffcopy) #:use-module (crates-io))

(define-public crate-diffcopy-0.1.0 (c (n "diffcopy") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0sxgv9xlzia7kha1kw07n95q414v7vj0fzyp789068v07l11rvqx")))

(define-public crate-diffcopy-0.1.1 (c (n "diffcopy") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0b80325r99x7p90h4dhm6pi2c4s2n1cpnm8g6iikx04i0i6rg92b")))

