(define-module (crates-io di ff diffbot) #:use-module (crates-io))

(define-public crate-diffbot-0.1.0 (c (n "diffbot") (v "0.1.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0a1icic6zckz0nha63fixm2qm420b3hpga51d8ljk1vbvaa75xgj")))

(define-public crate-diffbot-0.2.0 (c (n "diffbot") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.61") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0id0yrpj9snxbzcmbz4ix0dn8h7dzmj3niv49rhm7dy3m9gdpdck") (f (quote (("real_test") ("default"))))))

(define-public crate-diffbot-1.0.0 (c (n "diffbot") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "13ahv26kf9psqbzgcccs8n389vs9fnf9frvwmamasygwh76pvvc5") (f (quote (("real_test") ("default"))))))

