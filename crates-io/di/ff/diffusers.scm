(define-module (crates-io di ff diffusers) #:use-module (crates-io))

(define-public crate-diffusers-0.1.0 (c (n "diffusers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1256mw33mmckplqzmsv68zq28yr6rlnmn7687y72gk1rp3msly7x")))

(define-public crate-diffusers-0.1.1 (c (n "diffusers") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hq4ir89d54bzjx2v6hivxv7ysxgkbi8pfh3ynr0dawp69ymszl3")))

(define-public crate-diffusers-0.1.2 (c (n "diffusers") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xiqshycpiy23c5r331fjq495bmcs3q4zpdd0hkd5l3kgz0kvh0i") (f (quote (("doc-only" "tch/doc-only"))))))

(define-public crate-diffusers-0.1.4 (c (n "diffusers") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qmlsgsfhb6ff5jq4xbbjv58zpzkqkh1sy0f3ddsvfnk01lahihm") (f (quote (("doc-only" "tch/doc-only"))))))

(define-public crate-diffusers-0.2.0 (c (n "diffusers") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18ryjrr3gbljvmk1ikf3j2cmdy16x95qyhkwwl616rbncjgjvk2p") (f (quote (("doc-only" "tch/doc-only"))))))

(define-public crate-diffusers-0.2.1 (c (n "diffusers") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bnniyc4skkkcycqdhvg86bkn0pjnfikii3p70svlgl9vbgxnavv") (f (quote (("doc-only" "tch/doc-only"))))))

(define-public crate-diffusers-0.3.0 (c (n "diffusers") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "torch-sys") (r "^0.13") (f (quote ("download-libtorch"))) (d #t) (k 0)))) (h "1dxqv2ji81pspvwyc49z6d2g6f8s9lgx0cj5hj6m8byi3kyw0j7g") (f (quote (("doc-only" "tch/doc-only"))))))

(define-public crate-diffusers-0.3.1 (c (n "diffusers") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tch") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "torch-sys") (r "^0.13") (f (quote ("download-libtorch"))) (d #t) (k 0)))) (h "05zk6l44kkj33lhfqikrirsqy48lvkbv2q257pq1rygbc37fvbv3") (f (quote (("doc-only" "tch/doc-only"))))))

