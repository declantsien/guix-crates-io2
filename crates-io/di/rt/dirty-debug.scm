(define-module (crates-io di rt dirty-debug) #:use-module (crates-io))

(define-public crate-dirty-debug-0.0.0 (c (n "dirty-debug") (v "0.0.0") (h "05hhslvq1abdcw075hjmffg1incba344v6pbk91fk2m87dngcj1x") (f (quote (("fatal-warnings"))))))

(define-public crate-dirty-debug-0.1.0 (c (n "dirty-debug") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "0vnfnm17vlk280ypbsadz5vlk0534a5rmjf53n84irs4dwnmx077") (f (quote (("fatal-warnings"))))))

(define-public crate-dirty-debug-0.2.0 (c (n "dirty-debug") (v "0.2.0") (d (list (d (n "dashmap") (r "^5.4.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.7") (d #t) (k 2)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "0bvjbb9kx6pmhpjdb1wg835sl6m046xxkllpgr8bwdjln9qkdfar") (f (quote (("fatal-warnings"))))))

