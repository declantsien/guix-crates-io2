(define-module (crates-io di rt dirty-tracker) #:use-module (crates-io))

(define-public crate-dirty-tracker-0.1.0 (c (n "dirty-tracker") (v "0.1.0") (d (list (d (n "inotify") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0f872gsj5a3h45ppwiwi2nqrib2a978ydwgh66k2vlxy5p24bdxd")))

