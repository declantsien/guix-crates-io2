(define-module (crates-io di rt dirty-dns) #:use-module (crates-io))

(define-public crate-dirty-dns-0.1.0 (c (n "dirty-dns") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1724j81580h53lv400vxhb5g6djagx37famf35053dcmxka2c4rb")))

