(define-module (crates-io di rt dirtel) #:use-module (crates-io))

(define-public crate-dirtel-1.0.0 (c (n "dirtel") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.25") (d #t) (k 0)) (d (n "term-table") (r "^1.2") (d #t) (k 0)))) (h "011vrdyl7d2yalkp51id1c93hz0fqcmbgz2f46zc15v8f7np51jb")))

(define-public crate-dirtel-1.0.1 (c (n "dirtel") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.25") (d #t) (k 0)) (d (n "term-table") (r "^1.2") (d #t) (k 0)))) (h "020nshlkd62d49fkhz417vv8ag8jsg7rx3rf88kxkpsiq0455f9a")))

(define-public crate-dirtel-1.0.2 (c (n "dirtel") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.25") (d #t) (k 0)) (d (n "term-table") (r "^1.2") (d #t) (k 0)))) (h "1sw9yn0j2ibxcdx5m2kfqnbx12bmp9fmdyz9r1vb88x237b7zwhh")))

(define-public crate-dirtel-1.0.4 (c (n "dirtel") (v "1.0.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "sqlite") (r "^0.25") (d #t) (k 0)) (d (n "term-table") (r "^1.2") (d #t) (k 0)))) (h "1anp2rs0djjm903q823np2p6g94nqkx2lkpyz5ciad3mgifjv18r")))

