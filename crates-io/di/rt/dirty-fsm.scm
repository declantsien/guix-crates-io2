(define-module (crates-io di rt dirty-fsm) #:use-module (crates-io))

(define-public crate-dirty-fsm-0.1.0 (c (n "dirty-fsm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "puffin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1jb3ralviblgv7y5bcjp6kcay5k0njf2mr758qvw868lv6q70w2f")))

(define-public crate-dirty-fsm-0.2.0 (c (n "dirty-fsm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "puffin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0s4zy7509rva9v0bji87xxxizph3634csl0djqyd52ggw1bp4jkq")))

(define-public crate-dirty-fsm-0.2.1 (c (n "dirty-fsm") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "puffin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "15n38yyiwp7sqdad8lgnn9ydc18448l9gabvh4wfx4qm8nckib04")))

(define-public crate-dirty-fsm-0.2.2 (c (n "dirty-fsm") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "puffin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1n5xwf69yl8g775q8kxzl7x6rgnlgj8x5qdbnza1l8mdda1khdaw")))

(define-public crate-dirty-fsm-0.2.3 (c (n "dirty-fsm") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "puffin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "18335yq0wp7wq8mpwixd8y06d2h5l57vk1wf5vwjqxpjr0mnkcaw")))

