(define-module (crates-io di rt dirty_static) #:use-module (crates-io))

(define-public crate-dirty_static-0.1.0 (c (n "dirty_static") (v "0.1.0") (h "1vh8134p639c3pp8s1wixpmz65lndg0zr21pdv3541i88h5b3glg") (f (quote (("force-static") ("force-dynamic") ("default"))))))

(define-public crate-dirty_static-0.1.1 (c (n "dirty_static") (v "0.1.1") (h "1adwqancz3vy4gf67n9bng2ispcgqfyj4zv08p30w6kvqad3rhj1") (f (quote (("force-static") ("force-dynamic") ("default"))))))

