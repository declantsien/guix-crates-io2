(define-module (crates-io di rt dirtytype) #:use-module (crates-io))

(define-public crate-dirtytype-0.1.0 (c (n "dirtytype") (v "0.1.0") (h "026bjja8z44amg1c04dx3kksgvyf67qv59ya9py2gsrrhwwrm4r0")))

(define-public crate-dirtytype-0.2.0 (c (n "dirtytype") (v "0.2.0") (h "160mqyrvfnqh48lljvx5q5a13gbnsh9wvjb2k0pyd37q8gkm27ad")))

(define-public crate-dirtytype-0.3.0 (c (n "dirtytype") (v "0.3.0") (h "0v47vfnahgljm0bzrlwa4ri6p82yap1bcbnmskxivp3z4hvdcnzw")))

(define-public crate-dirtytype-0.4.0 (c (n "dirtytype") (v "0.4.0") (h "1hb2i8vd08ahqr5hxag1mgppjmspw6rb31wjvl3xvf0c897a8z7k")))

(define-public crate-dirtytype-0.4.1 (c (n "dirtytype") (v "0.4.1") (h "00a4fdfldlxwr8d078f63x84rpmq3y9g370lj78rak1wgrvyscb1")))

(define-public crate-dirtytype-0.5.0 (c (n "dirtytype") (v "0.5.0") (h "01r85cmj977jscbln29ji4pnpjpn932gyz1wmw7p1c0w2xpgq2vq")))

(define-public crate-dirtytype-0.5.1 (c (n "dirtytype") (v "0.5.1") (h "0pp61fp7ivl3imi9gc3dvayzikiivp6s2gic02jdlww2xa4clvil")))

