(define-module (crates-io di rt dirty) #:use-module (crates-io))

(define-public crate-dirty-0.1.0 (c (n "dirty") (v "0.1.0") (h "11az4941g5jfkx4j8dzry2d6slac9girdhzvlnrcy2ydh82q2586")))

(define-public crate-dirty-0.1.1 (c (n "dirty") (v "0.1.1") (h "1f3sm98ysmmmzm9rb8ni7d5afgj1yqi1jmjjmv1is8k3s2nzk2x4")))

(define-public crate-dirty-0.2.0 (c (n "dirty") (v "0.2.0") (h "1rsfb5m18v06jk935kj8j681h6s0vcaxyklcn0jhcbv8aljagyrx")))

