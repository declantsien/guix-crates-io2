(define-module (crates-io di rt dirtcomp) #:use-module (crates-io))

(define-public crate-dirtcomp-0.1.0 (c (n "dirtcomp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "errno") (r "^0.3.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0vbdnspv96y3i0ck6jjjmj9byabl0ip3ahak8lfwsa04fc01f8d9")))

(define-public crate-dirtcomp-0.1.1 (c (n "dirtcomp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0g7rcihndjzva3nlhy6mdsxyr9avkymjdcrls0ihhpjggv6aj0c6")))

