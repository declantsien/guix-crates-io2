(define-module (crates-io di ny diny_derive) #:use-module (crates-io))

(define-public crate-diny_derive-0.0.2 (c (n "diny_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "041x98w6i12hifl78j4nkhv22yvxqz72a3i9xvmchi3zxniwrimn")))

(define-public crate-diny_derive-0.1.0 (c (n "diny_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mqrwb48p5z70bsscqnwiczrfp148j67cpdjhsmal4n82lga3kcq")))

(define-public crate-diny_derive-0.2.0 (c (n "diny_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pbx5438c7fhfiwblpzgb123yp7kikyhx2x452m8x4z001jxzqfl")))

(define-public crate-diny_derive-0.2.1 (c (n "diny_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k1lr90ylf8s9n2zl6zz7z5v5z4jn2nhbsiad958crrdlqzhrifb")))

