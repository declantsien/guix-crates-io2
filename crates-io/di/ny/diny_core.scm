(define-module (crates-io di ny diny_core) #:use-module (crates-io))

(define-public crate-diny_core-0.0.2 (c (n "diny_core") (v "0.0.2") (d (list (d (n "diny_derive") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "1v0z24lifzh8wyr2gl1p153aqib9lp6sapmgl227z8c0dcrb1p8n") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.1.0 (c (n "diny_core") (v "0.1.0") (d (list (d (n "diny_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0nllxaxl222zjgnlwjdg4adnx46rzn7ai674irk0cp8i9bjq79hr") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.2.0 (c (n "diny_core") (v "0.2.0") (d (list (d (n "diny_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "15w53i9bmcs5qah5k227yhxfxirbp7bn46s5nvnfi745km9ffhf8") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.2.1 (c (n "diny_core") (v "0.2.1") (d (list (d (n "diny_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "02x3p9l9k94ckvp429v68aj5rc86c9rx3xwkpbfqbq8sin0x10gn") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.2.2 (c (n "diny_core") (v "0.2.2") (d (list (d (n "diny_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0zi24lacab33lyg8dh655pnnnv5cj23n26lvagfr5lx2v5a65s1z") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.2.3 (c (n "diny_core") (v "0.2.3") (d (list (d (n "diny_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "181jy8smldy8mcdjg1fldwk7v2s08vj2garbk99gnny5wbgxkhfp") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

(define-public crate-diny_core-0.2.4 (c (n "diny_core") (v "0.2.4") (d (list (d (n "diny_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "diny_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "0cbzawzi38q9qn5l6dql6ykhy39x2yajkrxmb7n9kn27i3g16vjg") (f (quote (("unsafe_speed") ("std") ("derive" "diny_derive") ("default" "std") ("alloc"))))))

