(define-module (crates-io di t- dit-as-91896) #:use-module (crates-io))

(define-public crate-dit-as-91896-0.1.0 (c (n "dit-as-91896") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "map-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "154dr43piik62hrgbsnbw2k91wnmy2ywq7dczaka5xpnizkn0x2v")))

(define-public crate-dit-as-91896-0.1.1 (c (n "dit-as-91896") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "map-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "1mxpmzkbfsdapv8q4v7af4qxka9hxlghw82rjkyzczffgp451slb")))

(define-public crate-dit-as-91896-0.1.2 (c (n "dit-as-91896") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "map-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "0gma9h5w4chi71im5gcdh1p65bjrczrb7gfsh22s8bnin39a1ip9")))

(define-public crate-dit-as-91896-0.1.3 (c (n "dit-as-91896") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "map-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.12") (d #t) (k 0)))) (h "0f0g3m09fp659b9i6hhb1z8bapr9719r428gazr6qc9dv4m5k7ch")))

