(define-module (crates-io io nt iont) #:use-module (crates-io))

(define-public crate-iont-0.1.0 (c (n "iont") (v "0.1.0") (d (list (d (n "ions") (r "^0.1.2") (d #t) (k 0)))) (h "1y7s8gjbjnvg0cka7hymd5qrxd2bc4vajgw20v980jnn3j1mqb7i")))

(define-public crate-iont-0.1.1 (c (n "iont") (v "0.1.1") (d (list (d (n "ions") (r "^0.1") (d #t) (k 0)))) (h "11m19js34cw4lc30m6x3imf7v74qvg60jki8rcjmyhksbv0qp14h")))

(define-public crate-iont-0.2.0 (c (n "iont") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ions") (r "^0.1") (d #t) (k 0)))) (h "01pcbh7c4jmqw9661rfk6xp5y0fyrhbcw9d522haq6krj8pqdasw")))

(define-public crate-iont-0.2.1 (c (n "iont") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ions") (r "^0.1") (d #t) (k 0)))) (h "0p4yq1kaksafpm10n3xz1qfik5b9izpqlpnyqlm2ddvmrfkn11dd")))

(define-public crate-iont-0.2.2 (c (n "iont") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ions") (r "^0.1") (d #t) (k 0)))) (h "0gars2c8iwbbzqb301b7pdrc57rsa433394rw1am5vicni241p1h")))

(define-public crate-iont-0.2.3 (c (n "iont") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "ions") (r "^0.1") (d #t) (k 0)))) (h "1diswysw10n0qjvcdrqj4lid6w69492jkri5frw12byynwaks3di")))

