(define-module (crates-io io cu iocuddle) #:use-module (crates-io))

(define-public crate-iocuddle-0.1.0 (c (n "iocuddle") (v "0.1.0") (h "066c8gsc8d45x8jxmzcz3k5ggngy6wa9wx727m96hqyprmw91l4b")))

(define-public crate-iocuddle-0.1.1 (c (n "iocuddle") (v "0.1.1") (h "01z0kkxs475v0n5l56w0srbv9nbmnd6398a7acykah4rwrdjv5yq")))

