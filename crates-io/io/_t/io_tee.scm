(define-module (crates-io io _t io_tee) #:use-module (crates-io))

(define-public crate-io_tee-0.1.0 (c (n "io_tee") (v "0.1.0") (h "02dfi8dhmr01pwyqzzpx113k46iasd628wwg6kblrvm4g85mfjk5")))

(define-public crate-io_tee-0.1.1 (c (n "io_tee") (v "0.1.1") (h "013ka85akdcsj9rr92jrkm4jia9s8ihirpqi0ncqc6156kppqgsb")))

