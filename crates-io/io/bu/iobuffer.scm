(define-module (crates-io io bu iobuffer) #:use-module (crates-io))

(define-public crate-iobuffer-0.1.0 (c (n "iobuffer") (v "0.1.0") (h "140br30kk04c33fsfpv3ddwbqrzh1gc97bjqvb24j4f663d7kl44")))

(define-public crate-iobuffer-0.2.0 (c (n "iobuffer") (v "0.2.0") (h "171jiadifcm0ii7iar7czf41b6092c05k80h13gv813qfsfvdchr")))

