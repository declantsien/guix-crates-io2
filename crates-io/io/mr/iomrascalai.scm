(define-module (crates-io io mr iomrascalai) #:use-module (crates-io))

(define-public crate-iomrascalai-0.1.6 (c (n "iomrascalai") (v "0.1.6") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "strenum") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ygza8fmsnisfmcrs2yyyjxa0l1g9f6sfshhg6fb45qamk5bdrpy")))

(define-public crate-iomrascalai-0.1.7 (c (n "iomrascalai") (v "0.1.7") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "strenum") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0cp73prr9qvjdgwmy6a5cfrlnn62nwqjnq2xwhlvyxyp6kc11a2f")))

