(define-module (crates-io io t_ iot_center) #:use-module (crates-io))

(define-public crate-iot_center-0.0.1 (c (n "iot_center") (v "0.0.1") (d (list (d (n "chan-signal") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rumqtt") (r "^0.10.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "045fkrkfpn8ca8847bwxajvg6d9j2lixvilm84l62zibv55a6nx3")))

