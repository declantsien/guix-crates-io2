(define-module (crates-io io t_ iot_hub) #:use-module (crates-io))

(define-public crate-iot_hub-0.0.1 (c (n "iot_hub") (v "0.0.1") (h "0w2zm7ngb6gjpj0pdw12lwn2ad2a6vb4vyjh1yxg72kviljlr9ca")))

(define-public crate-iot_hub-0.0.2 (c (n "iot_hub") (v "0.0.2") (h "0jd03q9izjdrdy2lcfmwl6xhcsv58p0y2z11asmh63mv417xsqpf")))

