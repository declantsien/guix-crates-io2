(define-module (crates-io io wr iowrap) #:use-module (crates-io))

(define-public crate-iowrap-0.1.0 (c (n "iowrap") (v "0.1.0") (h "1lcbbsqzm7d4rdjb0maryj6c9mik4vw0817x3drfdl25285sdw6s")))

(define-public crate-iowrap-0.1.1 (c (n "iowrap") (v "0.1.1") (h "1s5h0j0pkak483s839lf86zhmmy9cd2yclrvm6scf7bc1ddzf2nk")))

(define-public crate-iowrap-0.1.2 (c (n "iowrap") (v "0.1.2") (h "0djjvja62bzcypawi8x2c0gachlsikjps0vdcd015wgsvy11cqd6")))

(define-public crate-iowrap-0.2.0 (c (n "iowrap") (v "0.2.0") (h "0qnf5b86frmf4vyqs4s594r6ni833km8z7b7g34x7a332l46xq5a")))

(define-public crate-iowrap-0.2.1 (c (n "iowrap") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "0nni5b9mgjvkwhdkn3d356wd41grbynf65r0ys8qs4zslkcqnxwd")))

