(define-module (crates-io io cp iocp-rs) #:use-module (crates-io))

(define-public crate-iocp-rs-0.1.0 (c (n "iocp-rs") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03kgcrrfwhra7nlqk06hrw2jfsbbx094004795bqhd5cxpj9g2cj")))

(define-public crate-iocp-rs-0.1.1 (c (n "iocp-rs") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1ld2z79barcnvwprvbmg2qinkkyxbcw244q96b9xxb3a1kvl6hd4")))

(define-public crate-iocp-rs-0.1.2 (c (n "iocp-rs") (v "0.1.2") (d (list (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1p0gj22zhkp8vvr8q5fmnnrnb0yyxfg6nm0ffhcwim949jr20x27")))

(define-public crate-iocp-rs-0.1.3 (c (n "iocp-rs") (v "0.1.3") (d (list (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1hgp4r6x18zq9x3i5kcb4cr26jdwkb0hn794a162y4av7dh5m0nb")))

