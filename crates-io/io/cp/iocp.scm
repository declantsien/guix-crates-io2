(define-module (crates-io io cp iocp) #:use-module (crates-io))

(define-public crate-iocp-0.0.2 (c (n "iocp") (v "0.0.2") (h "17554l2mv2674gd2jsd8cd6m4899jmdf9vf3rddppfbbd8rf7nh9")))

(define-public crate-iocp-0.0.3 (c (n "iocp") (v "0.0.3") (h "1i7gyixx8zagz6jw1yyqbgcmw2m0yp7q5z53gx365m91vnzwzfx4")))

(define-public crate-iocp-0.0.4 (c (n "iocp") (v "0.0.4") (h "18n7mms30hqlj1yb945z5hcp7b268vf15xwvvfgcg2bcbfnkfyzd")))

(define-public crate-iocp-0.0.5 (c (n "iocp") (v "0.0.5") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0pnfbmnf77n79hpnd7g1nwvs0mraqflihg8ggclbc5h0ln7gglci")))

(define-public crate-iocp-0.0.6 (c (n "iocp") (v "0.0.6") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1b7i4is2frwq5bf3lcs7gr3x43pncimq5hqvadg3n8rcv3g37wzq")))

