(define-module (crates-io io td iotdb-client) #:use-module (crates-io))

(define-public crate-iotdb-client-0.1.0 (c (n "iotdb-client") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 0)))) (h "14nnfih16pqdpkbbkrfckkk197brrxxrvpllpkgvd7k0kpqrv95m")))

(define-public crate-iotdb-client-0.1.1 (c (n "iotdb-client") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 0)))) (h "09j3f8ms2bbb3c87rnwdysl9gnw260m8swbgbidkx8rrf1qqpy0d")))

