(define-module (crates-io io td iotdb-rs) #:use-module (crates-io))

(define-public crate-iotdb-rs-0.0.1 (c (n "iotdb-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "06l436x9wa3dn90xjiq3r3gml6wmmi5kxrgyir1kfrkbl3h7i3i0")))

(define-public crate-iotdb-rs-0.0.2 (c (n "iotdb-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "087zl88832xjp8mmh1ya3bpll4kiz05wzrcrnvxdsk4x77s0cmhf")))

