(define-module (crates-io io td iotdb) #:use-module (crates-io))

(define-public crate-iotdb-0.0.1 (c (n "iotdb") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "1klxjhs7lbpyk5dsp138w3526wr9kz3b2xkxjv2fkw6p8mq3h8iv")))

(define-public crate-iotdb-0.0.2 (c (n "iotdb") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "1v5w5v9nkjww7173l2ag7ng8zy4rlgpkw6wqmppak6q6prbyrxf5")))

(define-public crate-iotdb-0.0.3 (c (n "iotdb") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.13") (d #t) (k 0)))) (h "0gicn58404g7lwbz4ckzr2pcmm6gs8s3hh4pih97dn9wik99awmm")))

(define-public crate-iotdb-0.0.4 (c (n "iotdb") (v "0.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.15") (d #t) (k 0)))) (h "0z5mrzxmi6py9m28kbka80hwfr7f6pya94fbixby3b8aznynly0v")))

(define-public crate-iotdb-0.0.5 (c (n "iotdb") (v "0.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "thrift") (r "^0.15") (d #t) (k 0)))) (h "15vdd3min6g3x4b16k0n8q8gvylsy35jj6wsrs2cmkbqw2m1vmiw")))

(define-public crate-iotdb-0.0.6 (c (n "iotdb") (v "0.0.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15") (d #t) (k 0)))) (h "0rv393dfw83wywsz320mh55l368kv93bwgwxl3c2h7m6zrv32lx6")))

(define-public crate-iotdb-0.0.7 (c (n "iotdb") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1") (k 0)) (d (n "polars") (r "^0.19.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15") (d #t) (k 0)))) (h "1c4ki9xa8hc74knf19fhmg1xgn13j4n7pfynpxfaxnzr87s999jb")))

