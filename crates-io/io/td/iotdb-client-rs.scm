(define-module (crates-io io td iotdb-client-rs) #:use-module (crates-io))

(define-public crate-iotdb-client-rs-0.1.0 (c (n "iotdb-client-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.18") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1nil7f9g25xyaz87q0hwxpxd1k1xbz7xglfh6akmsjpkyjhan7ci") (y #t)))

(define-public crate-iotdb-client-rs-0.2.0 (c (n "iotdb-client-rs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0khpj0892wjrc4wlyw5bscmly8n7hp9gri2038wbr554q2449dwv") (y #t)))

(define-public crate-iotdb-client-rs-0.3.0 (c (n "iotdb-client-rs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0g6qrcq22fhqi5zdy2n89ajnq82f51jiwj4m4nxfrgyjhm3gbb1s") (y #t)))

(define-public crate-iotdb-client-rs-0.3.1 (c (n "iotdb-client-rs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0jllbaz2i7z9xpx1wy8h0qhc9zhrbqwlka4s87j89pcy7s7bzymi") (y #t)))

(define-public crate-iotdb-client-rs-0.3.2 (c (n "iotdb-client-rs") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0hjrkrwdnq4cna1pai7lcqvx2qcf7pcx11q38m9v37ndjpdmkk4d")))

(define-public crate-iotdb-client-rs-0.3.3 (c (n "iotdb-client-rs") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "10izwlvbq7mms5dhr9hqwagcl2fhpfd9fd2b3vj1l9pw7mrw202d")))

(define-public crate-iotdb-client-rs-0.3.4 (c (n "iotdb-client-rs") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "129zhzk0qxg2v2nrmh38cr7rzxqblzd7cgwm17qwy95aipsrmdc1")))

(define-public crate-iotdb-client-rs-0.3.5 (c (n "iotdb-client-rs") (v "0.3.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0jhfd9x78ajyi46njqpan971vqddszmnqbw60j3nphqyk36ab6m3") (y #t)))

(define-public crate-iotdb-client-rs-0.3.6 (c (n "iotdb-client-rs") (v "0.3.6") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0lyrcdz8x7iack9v0zsdznx2vk6j0n0r5ivkikjngiih1mr5impn")))

(define-public crate-iotdb-client-rs-0.3.7 (c (n "iotdb-client-rs") (v "0.3.7") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1z20mvpi5mlg5398zsh9w75r4qld1l6k4vzdg15d46czm8caj2n7")))

(define-public crate-iotdb-client-rs-0.3.8 (c (n "iotdb-client-rs") (v "0.3.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "1bivcyv5acjl76npl07rcf99i70x4v77mag3k236p0s4kcjgc3ch")))

(define-public crate-iotdb-client-rs-0.3.9 (c (n "iotdb-client-rs") (v "0.3.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.2.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.25") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "067f7dmnw87iwls9km3pvqx6bm8yn4ghkamklcp7yznw7cwz26wx")))

(define-public crate-iotdb-client-rs-0.3.10 (c (n "iotdb-client-rs") (v "0.3.10") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0wd8dq8lp23j1lyl8h60jc9i9x3i0p3wsij0mrb0g5qs3kzysz0x")))

(define-public crate-iotdb-client-rs-0.3.11 (c (n "iotdb-client-rs") (v "0.3.11") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "thrift") (r "^0.15.0") (d #t) (k 0)))) (h "0hvcnkaspinhibpb246hh2l2ig1acpvisr65zbv104y8mj2pfp7i")))

(define-public crate-iotdb-client-rs-0.3.12 (c (n "iotdb-client-rs") (v "0.3.12") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "permutation") (r "^0.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.0") (d #t) (k 0)))) (h "14c4vcn6rsbaj40h7a7xcnv1bygh0yp2cgvq8bh7kwknzpk7pcc1")))

