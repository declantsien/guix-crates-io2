(define-module (crates-io io td iotdb-cli) #:use-module (crates-io))

(define-public crate-iotdb-cli-0.0.1 (c (n "iotdb-cli") (v "0.0.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iotdb") (r "^0.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1l7kz79dcmjkqhn3zrsgwpsdhiwm5narlfckdjanss7r684ja1zp")))

(define-public crate-iotdb-cli-0.0.2 (c (n "iotdb-cli") (v "0.0.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iotdb") (r "^0.0.6") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "177c6h72qq651vzzdkrl74wy55n9mgr7rs0npc0v7mpb0pfny7gm")))

(define-public crate-iotdb-cli-0.0.3 (c (n "iotdb-cli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iotdb") (r "^0.0.7") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0sjmlnaw84j1lilr9nasykhqvcj7kgfqbixk5g3lp3yhy15y6rvn")))

(define-public crate-iotdb-cli-0.1.0 (c (n "iotdb-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iotdb") (r "^0.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rcvmkm1q7qniwgvy6pj1m69drpp7k5bjv4b5fqlh5mgpbnirnrs")))

