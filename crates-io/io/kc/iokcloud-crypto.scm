(define-module (crates-io io kc iokcloud-crypto) #:use-module (crates-io))

(define-public crate-iokcloud-crypto-0.1.0 (c (n "iokcloud-crypto") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1qjny9xzz3816iq95654b3lgfxk01z8q6dhr7qp7z05j84q7f5a5")))

