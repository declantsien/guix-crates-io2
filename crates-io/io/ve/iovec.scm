(define-module (crates-io io ve iovec) #:use-module (crates-io))

(define-public crate-iovec-0.1.0 (c (n "iovec") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1gi6md320ys3x0k6b3zvw6bwbvznr4s0zrvjw5dz5kgwc7p65l19")))

(define-public crate-iovec-0.1.1 (c (n "iovec") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1xxjhs9v477wz7bs4mbgzp4hcmmy69libwai25m6rkvz4k1bks5n")))

(define-public crate-iovec-0.1.2 (c (n "iovec") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "025vi072m22299z3fg73qid188z2iip7k41ba6v5v5yhwwby9rnv")))

(define-public crate-iovec-0.2.0 (c (n "iovec") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.3") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "ws2def"))) (d #t) (t "cfg(windows)") (k 0)))) (h "198hnlc5dzm9vs1y5dmhq1rhfrxli406569vmfp0kyrblbxscy9l") (y #t)))

(define-public crate-iovec-0.1.3 (c (n "iovec") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0rv5dc9zlzaibg9ch9q962xp0mlmrlbry6dvrl9yvzvkm806jqy9")))

(define-public crate-iovec-0.1.4 (c (n "iovec") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0ph73qygwx8i0mblrf110cj59l00gkmsgrpzz1rm85syz5pymcxj")))

