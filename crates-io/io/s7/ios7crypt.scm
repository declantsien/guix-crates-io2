(define-module (crates-io io s7 ios7crypt) #:use-module (crates-io))

(define-public crate-ios7crypt-0.0.1 (c (n "ios7crypt") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1fs901lis1smp9pi3ncj2czfwmcch8i1dahhcnw3d9n795w27mjz")))

(define-public crate-ios7crypt-0.0.2 (c (n "ios7crypt") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0krgqfxj2k6j9akibri1f4iw685f2pm5a6hvk16k70zf0vdp4s6a")))

(define-public crate-ios7crypt-0.0.3 (c (n "ios7crypt") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0ccps7m7cbb6cy7z0bgid3lzxxb60hqfv9f49jlmg5xdz7zg8pf0")))

(define-public crate-ios7crypt-0.0.4 (c (n "ios7crypt") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "166fmfrz82kl6jpxx164vg8gbnvf4fmpi4rrp9hl6nrqlv6wl3p1")))

(define-public crate-ios7crypt-0.0.5 (c (n "ios7crypt") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1gnlbagiwy6549pr340jdiqg9in7diq752qrg2263vngw4vs01ca")))

