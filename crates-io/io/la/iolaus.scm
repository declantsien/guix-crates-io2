(define-module (crates-io io la iolaus) #:use-module (crates-io))

(define-public crate-iolaus-0.1.0 (c (n "iolaus") (v "0.1.0") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0z6bmfn86s86lcyhjwfczr41xhbja7rbzak3aqzmdz1ylkda3zqc")))

(define-public crate-iolaus-0.1.1 (c (n "iolaus") (v "0.1.1") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0m6y835plxw1d5r50kn7hz7c85jra764cbmf6w3md1kawq2l38nf")))

(define-public crate-iolaus-0.1.2 (c (n "iolaus") (v "0.1.2") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0b31nnwix4b5sl5xfixrkc739s2ih5084krg0wd8478rbffy3f2x")))

(define-public crate-iolaus-0.1.3 (c (n "iolaus") (v "0.1.3") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0zwvk7a59cxzfvfz5sr0xafyi5lv2kml6mwpg293bmimym4mhwn2")))

(define-public crate-iolaus-0.1.4 (c (n "iolaus") (v "0.1.4") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0fg9i7v38hnibk1wrgvrnxcdbqknvnj85k6ia6n74wj46r67adx6")))

(define-public crate-iolaus-0.1.5 (c (n "iolaus") (v "0.1.5") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1h59c1s8xhy42fxb3fwbhhq7rizaxqyb37kk0c95rj7r913wpz67")))

(define-public crate-iolaus-0.1.6 (c (n "iolaus") (v "0.1.6") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0bggfqz9xmv8vnyaqvml8bkjz2yjrksbswr4phhjv4c3wp6sgzrb")))

(define-public crate-iolaus-0.1.7 (c (n "iolaus") (v "0.1.7") (d (list (d (n "pbr") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "05nz21prgbx8syni21njgmawbzi9dkj3pf97crv1w2i1kbgihp21")))

