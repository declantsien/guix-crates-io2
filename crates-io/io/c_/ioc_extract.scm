(define-module (crates-io io c_ ioc_extract) #:use-module (crates-io))

(define-public crate-ioc_extract-0.1.1 (c (n "ioc_extract") (v "0.1.1") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sanjrfq2rmwhjvi5grmns51gy701wamfic7wqbd0nwmvmbcpq69")))

(define-public crate-ioc_extract-0.1.2 (c (n "ioc_extract") (v "0.1.2") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jrbs6za3ma9j8s0iidi59hm4379xz6js7plix0zlp49rnnycwvw")))

(define-public crate-ioc_extract-0.1.3 (c (n "ioc_extract") (v "0.1.3") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vrasdbszgmkgq875s8x2giagx9lpyhkkbk8cszv980hg1yj16ly")))

(define-public crate-ioc_extract-0.1.4 (c (n "ioc_extract") (v "0.1.4") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z4l9phmvm7j0qjj5xadg3cndx96yyjmbdk9aysd321fx6q57z1a") (y #t)))

(define-public crate-ioc_extract-0.1.5 (c (n "ioc_extract") (v "0.1.5") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k1v2ly6mnhk0an4c9addi0dhi5r3jbpkwzv4qk26vvwfr0iydmm")))

(define-public crate-ioc_extract-0.1.6 (c (n "ioc_extract") (v "0.1.6") (d (list (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.0.1") (d #t) (k 0)))) (h "0d0ngrfirinjam3q3r2symvk5q421ynjapab2i3qv29cp5rvjff1")))

(define-public crate-ioc_extract-0.2.0 (c (n "ioc_extract") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "1hv9vhljlp51ql7j1ir84xpfz6ywxiak26v0q2k8lm6x7c8qzfa1")))

(define-public crate-ioc_extract-0.3.0 (c (n "ioc_extract") (v "0.3.0") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "0b01waja71yx6zfb5z09fbn7f47pxfyffdd3ccpig86mrrhwa1sv")))

(define-public crate-ioc_extract-0.3.1 (c (n "ioc_extract") (v "0.3.1") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "01kw3aghf7iqimz8g1w20r0d1jysnq9rzayipylzf8v79y96vdql")))

(define-public crate-ioc_extract-0.3.2 (c (n "ioc_extract") (v "0.3.2") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "0l6is2w5nl0ip5cah9apaq7r47panxsz4bps64bl1cn45zrq7q9x")))

(define-public crate-ioc_extract-0.3.3 (c (n "ioc_extract") (v "0.3.3") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "1sfx3l2ad1c9rcjrglifjs3s2j9v3iih531dsf0f343s2bk07qwh")))

(define-public crate-ioc_extract-0.4.0 (c (n "ioc_extract") (v "0.4.0") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "0vsi6kk76dnjz54fcds4g3vm72i5xg8z9imb75y029girg89zv90")))

(define-public crate-ioc_extract-0.4.1 (c (n "ioc_extract") (v "0.4.1") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "1mv1cn28ifilfjfl0jnnifrnrwpyh1vnbi9s3cs97zb6vw3rqf8l")))

(define-public crate-ioc_extract-0.4.2 (c (n "ioc_extract") (v "0.4.2") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.1") (f (quote ("with-db"))) (d #t) (k 0)))) (h "1rim1513qdrnv97i94z3fgdkabdmqxdwgbsiki826qc5i4h1fiir")))

(define-public crate-ioc_extract-0.4.3 (c (n "ioc_extract") (v "0.4.3") (d (list (d (n "fancy-regex") (r "^0.7") (d #t) (k 0)) (d (n "idna") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.2") (f (quote ("with-db"))) (d #t) (k 0)))) (h "18xnr4bfk5kkb9d0chgijyz1zskj45rcy3ahjlbv3mcavgvclwgm")))

(define-public crate-ioc_extract-0.4.4 (c (n "ioc_extract") (v "0.4.4") (d (list (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "idna") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.2") (f (quote ("with-db"))) (d #t) (k 0)))) (h "1yi81d46lqppfr7cv63y38xs4j1nwfk7j6q25ihq6r8n57vvqm0h")))

(define-public crate-ioc_extract-0.4.5 (c (n "ioc_extract") (v "0.4.5") (d (list (d (n "fancy-regex") (r "^0.13") (d #t) (k 0)) (d (n "idna") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tld_download") (r "^0.1.3") (f (quote ("with-db"))) (d #t) (k 0)))) (h "0xlzziyc2asq55k35wbc98ws16c5n6qinh9rwd9gmcwslaj7yqaa")))

