(define-module (crates-io io c_ ioc_container_rs) #:use-module (crates-io))

(define-public crate-ioc_container_rs-0.0.1 (c (n "ioc_container_rs") (v "0.0.1") (h "122k56dqiy8wbamps69lm144n96378lq0yy0zyn9p3hsd95x48fi")))

(define-public crate-ioc_container_rs-0.1.0 (c (n "ioc_container_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.77") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "181amcgzd2kprbxvg3q78n4g3sh1r725wx7xl71vjqh6ah2rm2y7")))

