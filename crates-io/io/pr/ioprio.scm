(define-module (crates-io io pr ioprio) #:use-module (crates-io))

(define-public crate-ioprio-0.1.0 (c (n "ioprio") (v "0.1.0") (d (list (d (n "iou_") (r "^0.3") (o #t) (d #t) (k 0) (p "iou")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "1wwm3gvn0iiwqqnxc9894k8j4kha83mg0033w234bkws24jsvz4c") (f (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

(define-public crate-ioprio-0.1.1 (c (n "ioprio") (v "0.1.1") (d (list (d (n "iou_") (r "^0.3") (o #t) (d #t) (k 0) (p "iou")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "0mwx94vjbd2grzprpzsr9b2wsr1w5kp1qqxmlc92fynzpfcc4aar") (f (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

(define-public crate-ioprio-0.2.0 (c (n "ioprio") (v "0.2.0") (d (list (d (n "iou_") (r "^0.3") (o #t) (d #t) (k 0) (p "iou")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "1z1fg2pwcv8nd2zk9k3yhphsw2dny569fkvcbj79aiqgjnd3rl5f") (f (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

