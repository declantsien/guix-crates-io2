(define-module (crates-io io ct ioctl-sys) #:use-module (crates-io))

(define-public crate-ioctl-sys-0.4.0 (c (n "ioctl-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03zh94bc2q6gsa488jwnva1rkvag1jq5bfwikcjn88m0h2h57l01") (f (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.4.1 (c (n "ioctl-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fqcfa82csviz06qdp2fvjac8z5q72fsscffzal6b1dibqby6sd0") (f (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5.0 (c (n "ioctl-sys") (v "0.5.0") (h "1jhzxjvgpc335i4zz3wlb03302aamcc1m01b1ryfxphvrl43abyv") (f (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5.1 (c (n "ioctl-sys") (v "0.5.1") (h "14jrrnhl0rrvpfjwixyd5643bjmb742f1v6kkwy247wxpr5xsppy") (f (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5.2 (c (n "ioctl-sys") (v "0.5.2") (h "0jjh6fggwd9akklwcgcqyz9ijd79k6yskkx7ijmfm5i46lk4nb2y") (f (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.6.0 (c (n "ioctl-sys") (v "0.6.0") (h "0bhqal9zbala895b5iwvwfi8k13jbxb2dz19jmk8iwjqlvzryhhw")))

(define-public crate-ioctl-sys-0.7.0 (c (n "ioctl-sys") (v "0.7.0") (h "0q02rpkkpry71cp0p3acdj31jsba95d81cqysnx0sk3jigiy8zpb")))

(define-public crate-ioctl-sys-0.7.1 (c (n "ioctl-sys") (v "0.7.1") (h "021vy96y6cq6n9rpj0f7axi455gx6vf0wn8himbqfm484dmhp7bz")))

(define-public crate-ioctl-sys-0.8.0 (c (n "ioctl-sys") (v "0.8.0") (h "0g0kjq308kdm81wq3f1p6q1v2x5sd2v31iwqzzsjch2354x1zlcb")))

