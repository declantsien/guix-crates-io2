(define-module (crates-io io ct ioctl-gen) #:use-module (crates-io))

(define-public crate-ioctl-gen-0.1.0 (c (n "ioctl-gen") (v "0.1.0") (h "1kihj6pbhmamf2kyy6ndfl03xiydblbnp0dsf3dwikzj6dl22kjz")))

(define-public crate-ioctl-gen-0.1.1 (c (n "ioctl-gen") (v "0.1.1") (h "1rvbj1rwn60ajrw8i76b75902nc9jmhfw7s9xnznc7y0vsfrfi8p")))

