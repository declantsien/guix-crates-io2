(define-module (crates-io io ct ioctl-macro) #:use-module (crates-io))

(define-public crate-ioctl-macro-0.0.1 (c (n "ioctl-macro") (v "0.0.1") (d (list (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0j4jfaqf6dy35hdfhgk2kbwf2aq9cfj4d0niaykpjiz01gg17656") (f (quote (("extern_fn") ("default"))))))

(define-public crate-ioctl-macro-0.0.2 (c (n "ioctl-macro") (v "0.0.2") (h "08bxaaxgnhrvi1qi6vizlqz5q0fn9lr7h28kw00yw8d097j1rv6d") (f (quote (("extern_fn") ("default")))) (r "1.64.0")))

