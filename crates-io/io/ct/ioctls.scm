(define-module (crates-io io ct ioctls) #:use-module (crates-io))

(define-public crate-ioctls-0.5.0 (c (n "ioctls") (v "0.5.0") (d (list (d (n "ioctl-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gvgl1ymqfj6hssvdxpg56dxbvvkip0mswng559ydp5fczjxhsy3") (f (quote (("unstable"))))))

(define-public crate-ioctls-0.5.1 (c (n "ioctls") (v "0.5.1") (d (list (d (n "ioctl-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00vn6giq76xgszkshh5vm0ky1jx7f4k1v093qi9avjnr2wzfri2a") (f (quote (("unstable"))))))

(define-public crate-ioctls-0.6.0 (c (n "ioctls") (v "0.6.0") (d (list (d (n "ioctl-sys") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yc9kmgh5s0vzq1d10ybjgngkrc0dgkc7ybg1g78bl3wxlb9d75c")))

(define-public crate-ioctls-0.6.1 (c (n "ioctls") (v "0.6.1") (d (list (d (n "ioctl-sys") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1liymfbgxy4airib1nfqlxavqjijsmbvciq6jhdylczpcak0qy65")))

(define-public crate-ioctls-0.6.2 (c (n "ioctls") (v "0.6.2") (d (list (d (n "ioctl-sys") (r ">=0.6, <=0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s3ivrhcyfqc3bmr30gpfpad8xc8pn18gh7vrz24fvqw1w71nnwk")))

