(define-module (crates-io io ct ioctl-rs) #:use-module (crates-io))

(define-public crate-ioctl-rs-0.1.0 (c (n "ioctl-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1zf6r1aa2vcqx142rn2l8y2zrqvv9qp09z2g9gi97jf9aqlnh7v9")))

(define-public crate-ioctl-rs-0.1.1 (c (n "ioctl-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "04skjwmsldf8cb8fkfaj13wl3jp29hd3a465zkg16plhcqcjn7va")))

(define-public crate-ioctl-rs-0.1.2 (c (n "ioctl-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1sycny0iflwgpc0z4rvh2w4qrcpa1sf4qmh7f9sw32lh9wvksga3")))

(define-public crate-ioctl-rs-0.1.3 (c (n "ioctl-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1cwxfg89cvjngikfgbzxy9is3phrkjr94ivm7ankym3kw8254ga7")))

(define-public crate-ioctl-rs-0.1.4 (c (n "ioctl-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hgxiwy9c3cv3grca6rckcc6klwgjj2njqcjb6px18f5cjhr5p4j")))

(define-public crate-ioctl-rs-0.1.5 (c (n "ioctl-rs") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rrjmwd4fccid00kdnikv53jrvir9j6w96mqzrdkdi40wrmvscy7")))

(define-public crate-ioctl-rs-0.1.6 (c (n "ioctl-rs") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zdrgqxblrwm4ym8pwrr7a4dwjzxrvr1k0qjx6rk1vjwi480b5zp")))

(define-public crate-ioctl-rs-0.2.0 (c (n "ioctl-rs") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0laishgm8pvhjkm1q6by804kqxnkql9xgk2m8rsfdzwa7ig0syv0")))

