(define-module (crates-io io n- ion-c-sys-macros) #:use-module (crates-io))

(define-public crate-ion-c-sys-macros-0.1.0 (c (n "ion-c-sys-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwx89kdambzlnrzi0wrkc774269shz945yadnpw7dpi99598byd")))

(define-public crate-ion-c-sys-macros-0.1.1 (c (n "ion-c-sys-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1k04va2a8d14w9a80x7m7wxzyfhq6aaqv1gdvyivhgqhphc18fbj")))

(define-public crate-ion-c-sys-macros-0.1.2 (c (n "ion-c-sys-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full"))) (d #t) (k 0)))) (h "1aklq5kzhfd54dvw1yccfcf72hxl5ilgnq0fs7bagb3rk9hjbagn")))

