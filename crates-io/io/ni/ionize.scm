(define-module (crates-io io ni ionize) #:use-module (crates-io))

(define-public crate-ionize-0.3.2 (c (n "ionize") (v "0.3.2") (h "0j2aaxl3vwqcgwwsp5nbcm53b34bhzyb490mz6p2rlixblmb06cm")))

(define-public crate-ionize-0.3.3 (c (n "ionize") (v "0.3.3") (h "142m1y027glmvkznhc27554wprci2n8a3vgrmybl1ysnfb5ngmnd")))

(define-public crate-ionize-0.3.4 (c (n "ionize") (v "0.3.4") (h "02pdk1zjfmpjvxkgpmscf8i7xjph3qj4acwxk1z237rac79s22zj")))

(define-public crate-ionize-0.3.5 (c (n "ionize") (v "0.3.5") (h "1wnj3fl53nsmvf5vdyyndp9n7fdcf0n75i3v5yqzc1pfab7fm174")))

(define-public crate-ionize-0.3.6 (c (n "ionize") (v "0.3.6") (h "1aryzilwjmz1ss94s1117svd7hc0l9pfzmv9r8l2vbrbh2jv7l4p")))

