(define-module (crates-io io _w io_wrapper_statistics) #:use-module (crates-io))

(define-public crate-io_wrapper_statistics-0.1.0 (c (n "io_wrapper_statistics") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0warf2g3v7rpdiwc2nv83yikrynvsb04kj0a0a095jzy18d8ahjl")))

(define-public crate-io_wrapper_statistics-0.1.1 (c (n "io_wrapper_statistics") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "019rxcilik26fsdyd9ycgbx5zmr3cj4nkirnqb98gi1krfq7rcxn")))

