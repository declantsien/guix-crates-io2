(define-module (crates-io io -u io-uring-epoll) #:use-module (crates-io))

(define-public crate-io-uring-epoll-0.1.0 (c (n "io-uring-epoll") (v "0.1.0") (d (list (d (n "io-uring") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "032ag9d2qjqaxis8p2m718i3alh8qzrv0hqd87hr46dfgzxl16v1") (f (quote (("default"))))))

