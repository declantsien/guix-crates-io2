(define-module (crates-io io tm iotmesh) #:use-module (crates-io))

(define-public crate-iotmesh-0.1.0 (c (n "iotmesh") (v "0.1.0") (h "0pip0wq2ppafg5cxf1r3v9qp5fx9mvpbc420k3xxqwa1mzqszjnr")))

(define-public crate-iotmesh-0.1.1 (c (n "iotmesh") (v "0.1.1") (h "081qwv5ycxfzlgdhx6ngdr609saigzy97hm2zajs0r8dm6nwvjwx") (f (quote (("default") ("all"))))))

