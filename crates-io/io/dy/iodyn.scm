(define-module (crates-io io dy iodyn) #:use-module (crates-io))

(define-public crate-iodyn-0.2.0 (c (n "iodyn") (v "0.2.0") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jyk6f4mdmfzmy1k46kxbr8k23v925hix6mvmh3dpb347abkfh0h")))

(define-public crate-iodyn-0.2.1 (c (n "iodyn") (v "0.2.1") (d (list (d (n "adapton") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0pf8pnns57d97mp91j4s7xcvr3w0w95a4s6a23w5mb397alav1ih")))

