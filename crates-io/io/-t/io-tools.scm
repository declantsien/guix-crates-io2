(define-module (crates-io io -t io-tools) #:use-module (crates-io))

(define-public crate-io-tools-0.1.0 (c (n "io-tools") (v "0.1.0") (h "1rwzbwzpfhfaxygcam9pvrw0m1h9cmkjwdwv5y18anm0z4sc5hdi")))

(define-public crate-io-tools-0.1.1 (c (n "io-tools") (v "0.1.1") (h "1ydl5hq37kszjk97z04xzxi99168zdc32lnb49jkfj7l6v6qr4pb")))

