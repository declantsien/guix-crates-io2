(define-module (crates-io io -t io-trait) #:use-module (crates-io))

(define-public crate-io-trait-0.0.0 (c (n "io-trait") (v "0.0.0") (h "1ayc1bmfys2w2qazsd4nq2j62pzwaigcj2j3qiph4dqyqp0xxbqq") (y #t)))

(define-public crate-io-trait-0.1.0 (c (n "io-trait") (v "0.1.0") (h "18li24s0yqssyn602d1jvwvg72gi9z510yk4l68gmazmgfzk6qnz")))

(define-public crate-io-trait-0.1.1 (c (n "io-trait") (v "0.1.1") (h "1h7diqs3cx0ksa1s563zrg83hs6ykwrq62jr1jrqzn54k5mff5gh")))

(define-public crate-io-trait-0.1.2 (c (n "io-trait") (v "0.1.2") (h "07niv9mp506ngb0i78p0hc5z7h426jl3a49yl6m2hr3ppa78b99r")))

(define-public crate-io-trait-0.1.3 (c (n "io-trait") (v "0.1.3") (h "1i5afljjvwgh79ivj8vw185s59i7vlbkvdcimyh69ks0sj0wdh9a")))

(define-public crate-io-trait-0.1.4 (c (n "io-trait") (v "0.1.4") (h "0r8bdhrps15sg7v1qkx4wip51bp7pvkynlsxybggcd9fkzwxkjwp")))

(define-public crate-io-trait-0.3.0 (c (n "io-trait") (v "0.3.0") (h "11zypqn59aawlv9srw52p016fgibn97m5wkjvkrpi458v4f5bdgd")))

(define-public crate-io-trait-0.4.0 (c (n "io-trait") (v "0.4.0") (h "1dbacsm1jymicrb3d57i5ivja58xppmkpw0b6igpz0m68saa1lqv")))

(define-public crate-io-trait-0.5.0 (c (n "io-trait") (v "0.5.0") (h "0i8pbias0rpj5ka3njl0awdd2mzdzab4f17wi6rbrf0pwxfy16s9")))

(define-public crate-io-trait-0.6.0 (c (n "io-trait") (v "0.6.0") (h "126ya3fbarws4hs3wnhrqh6jpf7bz99s5h6zh810zk6agfw6ccyl")))

(define-public crate-io-trait-0.7.0 (c (n "io-trait") (v "0.7.0") (h "11j3idadijjfxxl2875n20bs0s22acbf05w2j7m5zdpwrkavgi20")))

(define-public crate-io-trait-0.8.0 (c (n "io-trait") (v "0.8.0") (h "0yq496drq86i1h4yijajhp6czlfbvwjsmamvc62mc9hk86r66l2q")))

(define-public crate-io-trait-0.9.0 (c (n "io-trait") (v "0.9.0") (h "0197r8x0x0k93c7k98dfdhi7d4k6k2cy02bl632s1pxkb4zmz4mc")))

(define-public crate-io-trait-0.10.0 (c (n "io-trait") (v "0.10.0") (h "08q717lgkdvzf265s7vy4n3sa75xd732x9p3mfkfbjna7zvrqzv3")))

(define-public crate-io-trait-0.11.0 (c (n "io-trait") (v "0.11.0") (h "0hivwshn6bq8rwbmaamfr401l1wqrn3lp8irv6pvlblj2x99xhzy")))

