(define-module (crates-io io -t io-test) #:use-module (crates-io))

(define-public crate-io-test-0.0.0 (c (n "io-test") (v "0.0.0") (h "0rwnm1in7h0biwx6p5qcry3qignf8bwvmjwk32cb9x21spiqm3y0")))

(define-public crate-io-test-0.1.0 (c (n "io-test") (v "0.1.0") (d (list (d (n "io-trait") (r "^0") (d #t) (k 0)))) (h "0w4ghq5fpmwjj6g3r6x3iqjl1vr1fvp4y26rwwv5vvacfc6bx5xm")))

(define-public crate-io-test-0.1.1 (c (n "io-test") (v "0.1.1") (d (list (d (n "io-trait") (r "^0.1") (d #t) (k 0)))) (h "1w9mfa1vzcmfr2m69n5i3ahbqaw2kwps1d2mw888yh4z3ymi54m3")))

(define-public crate-io-test-0.1.2 (c (n "io-test") (v "0.1.2") (d (list (d (n "io-trait") (r "^0.1.1") (d #t) (k 0)))) (h "1n96dlgfvcllxcw5f8bivjsa1w0ldhhb3hf5qxz5zgdy7jv36fwk")))

(define-public crate-io-test-0.1.3 (c (n "io-test") (v "0.1.3") (d (list (d (n "io-trait") (r "^0.1.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "00ppavhkibpl1p3vwy7gw865pwzdvr78vxklj8x054qavhai4kf0")))

(define-public crate-io-test-0.1.4 (c (n "io-test") (v "0.1.4") (d (list (d (n "io-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1smhnszb77hyz5wbhjxhpsafhyac68q8a821brbl86sdi5ci3gzb")))

(define-public crate-io-test-0.1.5 (c (n "io-test") (v "0.1.5") (d (list (d (n "io-trait") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1kg5yh7qr881zb1dcpa1vmgc8y48f55fn9x4paq61nk84788r0fh")))

(define-public crate-io-test-0.2.0 (c (n "io-test") (v "0.2.0") (d (list (d (n "io-trait") (r "^0.1.3") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1x9m1ck4p42pybhbghm1497hsx2agwri9dv7ahnagnwz4qhrb768")))

(define-public crate-io-test-0.3.0 (c (n "io-test") (v "0.3.0") (d (list (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "07wyc1709zvjbnqyyg86ncm7js4074kf2imk91sf0a6k45rk14b2")))

(define-public crate-io-test-0.4.0 (c (n "io-test") (v "0.4.0") (d (list (d (n "io-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1pmf4wfvm6rx7d251nnxywybgd489kahbfg7jvbgjlxqfhm3vbnx")))

(define-public crate-io-test-0.5.0 (c (n "io-test") (v "0.5.0") (d (list (d (n "io-trait") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1pxrbda26zrmsxawqybcmfq3ag2gxrhl1z40iim7n3gdggxwc45h")))

(define-public crate-io-test-0.6.0 (c (n "io-test") (v "0.6.0") (d (list (d (n "io-trait") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "13a3gd8zsgwmg6cam8cvpzxn4imybx0sq9g8clp22d5fc2dqhcig")))

(define-public crate-io-test-0.7.0 (c (n "io-test") (v "0.7.0") (d (list (d (n "io-trait") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "1zmnh4zvnhrzaip3yf2dpggznm9hnsfdlspx4zsfc1cjrzggr817")))

(define-public crate-io-test-0.8.0 (c (n "io-test") (v "0.8.0") (d (list (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "1mjv2yvivyiba4ylmx1v8117yz18j81gjld2kbnfjhv3lwql0rl1")))

(define-public crate-io-test-0.8.1 (c (n "io-test") (v "0.8.1") (d (list (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "0sc4kyfv5nmh42g64psg983aw14xj38p55vrpycp5zn4vhc0fc5q")))

(define-public crate-io-test-0.9.0 (c (n "io-test") (v "0.9.0") (d (list (d (n "io-trait") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1l615c3bzpy7vcva49chmh12di7qbdi3qdw9zv7jn23m2bmlfrd4")))

(define-public crate-io-test-0.10.0 (c (n "io-test") (v "0.10.0") (d (list (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "14vig2q19rd2abplndw3zx76bg4hm4lazc90a6ldnap1z3brny4b")))

(define-public crate-io-test-0.10.1 (c (n "io-test") (v "0.10.1") (d (list (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "18qh34mxpvps6dj2ly0sj85mal5hfycqsqqsz54yn189vlb76vxs")))

(define-public crate-io-test-0.11.0 (c (n "io-test") (v "0.11.0") (d (list (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1nwdrvgy4y98x7vf74jzbak1xwbl3q414a5z4dnkfs57c24jqk4y")))

