(define-module (crates-io io -t io-tubes) #:use-module (crates-io))

(define-public crate-io-tubes-0.0.1 (c (n "io-tubes") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v90vbwgqk67bndh9vcknqix8dnnhvi3cgp3ijc22m5lgim272kx")))

(define-public crate-io-tubes-0.0.2 (c (n "io-tubes") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12gz8vnv2787m3l267z65k4l7lqkrdc7frgcgq4hwdfx950mc46g")))

(define-public crate-io-tubes-0.0.3 (c (n "io-tubes") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kksg0vcxzd9sl3yck3pd1c5p6h8d6hsqnzzn3bmaqjlsdkqd0fh")))

(define-public crate-io-tubes-0.1.0 (c (n "io-tubes") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pgv0h3zii8iy2csbrqz9zccdagvn3d2kpp11496lp69gx4446kx")))

(define-public crate-io-tubes-0.1.1 (c (n "io-tubes") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sj3vp1gn8bpvr51pywsj6wyrz1g8jqv5ybbbj1f85zqkh6fk3i9")))

