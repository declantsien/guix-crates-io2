(define-module (crates-io io _s io_self_derive) #:use-module (crates-io))

(define-public crate-io_self_derive-0.1.0 (c (n "io_self_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1fcxv4x5yrf7plkw6jsc12v74r9dpcyhmk0jvd5ll4p7hvhvip9j")))

(define-public crate-io_self_derive-0.2.0 (c (n "io_self_derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18xsvcyb816lyc7b9rp57blhg4zrzbqc8alyw6490s6nm0pz6n0g")))

