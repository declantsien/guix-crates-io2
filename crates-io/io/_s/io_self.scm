(define-module (crates-io io _s io_self) #:use-module (crates-io))

(define-public crate-io_self-0.1.0 (c (n "io_self") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "01xldnfcsji5qqwnlf7ygs8whpgjhwcqkb6md2h0rd4d77gdpsmv") (f (quote (("default"))))))

(define-public crate-io_self-0.1.1 (c (n "io_self") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "0k0syh5zqxp7l6mdify0al5x9iwykh59s8crj1hd3wciqxfp8k50") (f (quote (("default"))))))

