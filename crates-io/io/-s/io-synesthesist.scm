(define-module (crates-io io -s io-synesthesist) #:use-module (crates-io))

(define-public crate-io-synesthesist-0.1.0 (c (n "io-synesthesist") (v "0.1.0") (h "1frf7yn188y71zvsa9bicjd2885lg1m1i2xmhfvsaic72ds1skc1") (f (quote (("synesthesia"))))))

(define-public crate-io-synesthesist-0.1.1 (c (n "io-synesthesist") (v "0.1.1") (h "0w3qakvqaq27qffd3wzy4vn5nxz937gdkgm13dkvl1njci00hm5g") (f (quote (("synesthesia"))))))

