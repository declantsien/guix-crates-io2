(define-module (crates-io io _p io_parser) #:use-module (crates-io))

(define-public crate-io_parser-0.1.4 (c (n "io_parser") (v "0.1.4") (h "1kf72x9b6w6mw5kilr7l2xspqjhgjjah4ai3izhza5jyi3d6wx48") (y #t)))

(define-public crate-io_parser-0.1.5 (c (n "io_parser") (v "0.1.5") (h "1vx3dqqx3ygzzjv9fbbaih3v89dgr748bi3bpqzgqnfjdyg4agyd")))

