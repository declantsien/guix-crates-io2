(define-module (crates-io io _o io_operations) #:use-module (crates-io))

(define-public crate-io_operations-0.0.0 (c (n "io_operations") (v "0.0.0") (h "1r3cky3nlplnqrxvx5gjy8ylybhi5mxb81469c2pfp79vphkc88b")))

(define-public crate-io_operations-0.1.0 (c (n "io_operations") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0q41ip4khz0nf3kvzxxmnfj0cc1xdcja4pllckixi8fmj7zm226b")))

(define-public crate-io_operations-0.2.0 (c (n "io_operations") (v "0.2.0") (h "08dmvq8r1ml5bz61mz3206k4mx52b880ivqk1insmpxacw3yzfn7")))

