(define-module (crates-io io p- iop-keyvault-wasm) #:use-module (crates-io))

(define-public crate-iop-keyvault-wasm-0.0.2 (c (n "iop-keyvault-wasm") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "iop-keyvault") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.65") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0xhm6gkbgi8snciivvbkyvdjybjvdy449pygmh66a59qx9x7l029")))

(define-public crate-iop-keyvault-wasm-0.0.3 (c (n "iop-keyvault-wasm") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "iop-keyvault") (r "^0.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.65") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0qf0i960kxr1alsjczmxdz0ikwizs8mfh6qg281pnbygn7yq5nw8")))

(define-public crate-iop-keyvault-wasm-0.0.4 (c (n "iop-keyvault-wasm") (v "0.0.4") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "iop-keyvault") (r "^0.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.65") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1pwyw4faj6lzlqx2xm2h4blm03nhkxhh1n87rkgyh3n6ap0xkmzz")))

(define-public crate-iop-keyvault-wasm-0.0.5 (c (n "iop-keyvault-wasm") (v "0.0.5") (d (list (d (n "iop-keyvault") (r "^0.0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.65") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0l2v1s8kbi67qa9flrcjlriidwvyf6xm2vfnm1f5cnv33jqm7sly")))

(define-public crate-iop-keyvault-wasm-0.0.12 (c (n "iop-keyvault-wasm") (v "0.0.12") (d (list (d (n "iop-keyvault") (r "^0.0.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.65") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1fbhj964z1nyg2d5w7ns4m15ckx3bx2g19j1z6kmw3kcnipgh6jl")))

(define-public crate-iop-keyvault-wasm-0.0.15 (c (n "iop-keyvault-wasm") (v "0.0.15") (d (list (d (n "iop-keyvault") (r "^0.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1sd8n4s2lgfbw2l65n6a7z3dbjfrdx63r27g66cxzcziiad6gm40")))

(define-public crate-iop-keyvault-wasm-0.0.16 (c (n "iop-keyvault-wasm") (v "0.0.16") (d (list (d (n "iop-keyvault") (r "^0.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.81") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0qszcfnm6qwcjci1nl7nhz6759kz5wdc75wq3hpm8sr4f9gzi6pa")))

