(define-module (crates-io io p- iop-journal-proto) #:use-module (crates-io))

(define-public crate-iop-journal-proto-0.0.15 (c (n "iop-journal-proto") (v "0.0.15") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "0wws8zh402laq7lxr04zdhcj97rghb87wb6lj7gv9fwc2vq2c9kv")))

(define-public crate-iop-journal-proto-0.0.16 (c (n "iop-journal-proto") (v "0.0.16") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "varint") (r "^0.9.0") (d #t) (k 0)))) (h "08m4p1i7z9fywzx6bijam862x8nq9z69cjrc2dkk7dr1w006aasw")))

