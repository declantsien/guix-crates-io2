(define-module (crates-io io fs iofs) #:use-module (crates-io))

(define-public crate-iofs-0.0.0 (c (n "iofs") (v "0.0.0") (h "01vx2bzw6bn6nrirfwk51gmsqpx9xnr5rj0pprqrcm18zkd8vixf") (f (quote (("hook_key"))))))

(define-public crate-iofs-0.0.1 (c (n "iofs") (v "0.0.1") (h "0pfm8r0qi4yc75ln9slpzbwaxkvq1d4c7h7f09yksxywhpfsgyxk") (f (quote (("hook_key"))))))

(define-public crate-iofs-0.0.2 (c (n "iofs") (v "0.0.2") (h "110db0anns10x9j5zm8bvw2xwz12ma3hcr057p21ykyspkirlczh")))

(define-public crate-iofs-0.0.3 (c (n "iofs") (v "0.0.3") (h "0a5csknw0z9pi3f83wnc4pdlb7cxkw2lg6k51gzaal1nfblqjyzh")))

(define-public crate-iofs-0.0.5 (c (n "iofs") (v "0.0.5") (h "18sd99y9s6h7fw5q8kscrzagl2j7w3swr0w3wjlam748lasrnkq2")))

(define-public crate-iofs-0.0.6 (c (n "iofs") (v "0.0.6") (d (list (d (n "http") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "1s8f0w7j7v1kzyffby7wsm5y7gbhk0a2ig56pqa4wnhc7l1s0dss") (f (quote (("web_warp" "warp" "http"))))))

(define-public crate-iofs-0.0.7 (c (n "iofs") (v "0.0.7") (d (list (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (d #t) (k 0)))) (h "0mlfm9wyybj2v1ncrnzfxbv8w0p70kbwz441nppvalbzhx37407y")))

(define-public crate-iofs-0.0.8 (c (n "iofs") (v "0.0.8") (d (list (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "063drqln6p0i1irq54mwwy395sny6822djfln279j0lvg4xxgnsg") (f (quote (("web_warp" "warp" "url"))))))

