(define-module (crates-io io _b io_bluetooth) #:use-module (crates-io))

(define-public crate-io_bluetooth-0.1.0 (c (n "io_bluetooth") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libbluetooth") (r "^0.1") (f (quote ("impl-default"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("impl-default" "guiddef" "handleapi" "processthreadsapi" "winbase" "winerror" "winnt" "winsock2" "ws2def"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winbluetooth") (r "^0.1") (f (quote ("impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "12qidv0l3x2wpf29gjbnkhvs2d10wsrzq1s34smba1vzir9g7z16")))

