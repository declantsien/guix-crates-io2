(define-module (crates-io io ta iota-crypto-preview) #:use-module (crates-io))

(define-public crate-iota-crypto-preview-0.1.0 (c (n "iota-crypto-preview") (v "0.1.0") (d (list (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.1") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0c1b7zdwsgb5jicvdm71ymhdgnihzr6mmxfphwqqz03s4nn7i2lm")))

