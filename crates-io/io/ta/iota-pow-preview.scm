(define-module (crates-io io ta iota-pow-preview) #:use-module (crates-io))

(define-public crate-iota-pow-preview-0.1.0 (c (n "iota-pow-preview") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "iota-common-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "1y2k409dig6dw539rr3x6bg39chl6hhl3l738wg1b2qhxqkpaixy")))

