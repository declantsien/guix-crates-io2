(define-module (crates-io io ta iota-identity) #:use-module (crates-io))

(define-public crate-iota-identity-0.1.0 (c (n "iota-identity") (v "0.1.0") (d (list (d (n "account") (r "^0.1") (d #t) (k 0) (p "identity_account")) (d (n "communication") (r "^0.1") (d #t) (k 0) (p "identity_communication")) (d (n "doc_manager") (r "^0.1") (d #t) (k 0) (p "identity_doc_manager")) (d (n "integration") (r "^0.1") (d #t) (k 0) (p "identity_integration")) (d (n "resolver") (r "^0.1") (d #t) (k 0) (p "identity_resolver")) (d (n "schema") (r "^0.1") (d #t) (k 0) (p "identity_schema")) (d (n "vc") (r "^0.1") (d #t) (k 0) (p "identity_vc")))) (h "1mc2dkpinnn2if4cfn8a9vs9y8x5g0a3c4zl3r4sgcwwvwpghyys")))

