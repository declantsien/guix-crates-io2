(define-module (crates-io io ta iota-ternary-preview) #:use-module (crates-io))

(define-public crate-iota-ternary-preview-0.1.0 (c (n "iota-ternary-preview") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "iota-common-preview") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14bslxw55k1g70slakbkqffrknfz8frnfqjbxdinqif4af3jlz7m") (f (quote (("serde1" "serde"))))))

