(define-module (crates-io io ta iota-rs) #:use-module (crates-io))

(define-public crate-iota-rs-0.1.0 (c (n "iota-rs") (v "0.1.0") (h "1iyh8rql1j4mbbmgaqwd0ir5cd69a29kminapxkmghdp0ffmhppk")))

(define-public crate-iota-rs-0.1.1 (c (n "iota-rs") (v "0.1.1") (h "0zkpwvn6nnzfg7qlllfvl6h3yhnb67nq7430xvg9s3jp1dlbccgh")))

(define-public crate-iota-rs-0.1.2 (c (n "iota-rs") (v "0.1.2") (h "1cbig6l4y4bj92bm5s4v0569nd2zh0p5m81964b3cz2spspbazpg")))

