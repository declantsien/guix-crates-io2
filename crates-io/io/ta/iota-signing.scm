(define-module (crates-io io ta iota-signing) #:use-module (crates-io))

(define-public crate-iota-signing-0.1.0 (c (n "iota-signing") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-validation") (r "^0.1.0") (d #t) (k 0)))) (h "0nkdwqdc8x26qcbf4il53zj96phdyfarj8jrkwkfypvkcxb43qs0")))

(define-public crate-iota-signing-0.2.0 (c (n "iota-signing") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-validation") (r "^0.2.0") (d #t) (k 0)))) (h "1vbyvq76vcrqih9wcmixzyskrkgvxgbrn2spy63mkfapwiigc2cm")))

(define-public crate-iota-signing-0.4.0 (c (n "iota-signing") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-validation") (r "^0.4.0") (d #t) (k 0)))) (h "07qhv0zzq9lmy2zcyaiaqsb9ldzc4ixi10iz3wp8bq90pp7xy1qw") (y #t)))

(define-public crate-iota-signing-0.2.1 (c (n "iota-signing") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-validation") (r "^0.2.1") (d #t) (k 0)))) (h "02rh8nz6br5l2ll79k2qlwh256gm4khcrvx6aflksc360pf47l0j")))

