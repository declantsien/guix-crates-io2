(define-module (crates-io io ta iota-conversion) #:use-module (crates-io))

(define-public crate-iota-conversion-0.1.0 (c (n "iota-conversion") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0al7mj81rwq67zx9xsjvb1abwjhrg5z5in7r4cvdw7vchd8iahry")))

(define-public crate-iota-conversion-0.2.0 (c (n "iota-conversion") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0ddf1h5h91p5bx3vc6gb3m4h8b8xgm53dsrrz904jj392njlz968")))

(define-public crate-iota-conversion-0.4.0 (c (n "iota-conversion") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1i0lxk5hn44dcl7h2pklf3swhgkh58cgvr5hw4k05rvjwk6xhpv3")))

(define-public crate-iota-conversion-0.3.0 (c (n "iota-conversion") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "07dhf37rrp7b8lqbby6jplwxincw334s63s4byvbiw7zc8mm1ikl")))

(define-public crate-iota-conversion-0.5.0 (c (n "iota-conversion") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1m3pqcmcrq2xkwg61mfzp3j0i6gib9yqrfp1yvmjlhvcaqfbkkmj")))

(define-public crate-iota-conversion-0.5.1 (c (n "iota-conversion") (v "0.5.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1zyfap6ds54wqpfx1vdvzmp21r82klf2r45v9wk5ldjj7b5kf8v7")))

