(define-module (crates-io io ta iota-editor) #:use-module (crates-io))

(define-public crate-iota-editor-0.1.0 (c (n "iota-editor") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.47") (d #t) (k 0)) (d (n "gapbuffer") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rustbox") (r "^0.7.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.1") (d #t) (k 0)))) (h "0n7rabj3rwa3x8iirlq5pis88nwilw1h8mcra6zi6il0qsr19ip5")))

