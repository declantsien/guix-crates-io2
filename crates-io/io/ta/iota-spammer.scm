(define-module (crates-io io ta iota-spammer) #:use-module (crates-io))

(define-public crate-iota-spammer-0.0.1 (c (n "iota-spammer") (v "0.0.1") (d (list (d (n "iota-lib-rs") (r "^0.0.24") (d #t) (k 0)))) (h "0jqwhxlvj3v2caflk1410ljk8pzgvdwq802fcdw19v5r3b3r294f")))

(define-public crate-iota-spammer-0.0.2 (c (n "iota-spammer") (v "0.0.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.24") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0ybdjb35yfiyz4vlyrkx4hd907ilrfjldy7qzq1azrahh4r0pllb")))

(define-public crate-iota-spammer-0.0.3 (c (n "iota-spammer") (v "0.0.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.26") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "17i4r4cqk62yykflhyri3cgflf4y3q9zb4yppgmcv03zb17d9p94")))

(define-public crate-iota-spammer-0.0.4 (c (n "iota-spammer") (v "0.0.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.26") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0zmgxjpprcx5q9ix4hf5rrbl20q3190kl2njd1yv1qsc7aiq3js4")))

(define-public crate-iota-spammer-0.0.5 (c (n "iota-spammer") (v "0.0.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.26") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "04yq5yd1xkwlmkazvc4vy94glg6yviynsplsxbqlkjm45x7145vx")))

(define-public crate-iota-spammer-0.0.6 (c (n "iota-spammer") (v "0.0.6") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.27") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0snbppf0c68wnp89sgda6my300m36j6rvj8ncniv49xvv4gcbyd1")))

(define-public crate-iota-spammer-0.0.7 (c (n "iota-spammer") (v "0.0.7") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.27") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1mdm3c1lnna9c6g4kfv14pbzzfg3xznfn0zyhrkhmc313v7yys56")))

(define-public crate-iota-spammer-0.0.8 (c (n "iota-spammer") (v "0.0.8") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.28") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0xwq2x6lif694d15b19a5a52jqipaafaw41c208090s06q0bgq8m")))

(define-public crate-iota-spammer-0.0.10 (c (n "iota-spammer") (v "0.0.10") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-lib-rs") (r "^0.0.34") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1w1vyi8skv7l541cq6a6c564fz006lxflz5am585acw6wwkv8wxm")))

(define-public crate-iota-spammer-0.0.11 (c (n "iota-spammer") (v "0.0.11") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.2") (d #t) (k 0)) (d (n "iota-lib-rs-preview") (r "^0.1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "005r340zy0v63rlcxycqip0p2s5mbwz70w629h8ziy97j8csgsvm")))

