(define-module (crates-io io ta iota-signing-preview) #:use-module (crates-io))

(define-public crate-iota-signing-preview-0.1.0 (c (n "iota-signing-preview") (v "0.1.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7") (d #t) (k 0)))) (h "1vingihpmryf8byak4yymviy3flkjrr3z6csz6y4g8jj64i4x0kw")))

