(define-module (crates-io io ta iota-core) #:use-module (crates-io))

(define-public crate-iota-core-0.1.0 (c (n "iota-core") (v "0.1.0") (h "02ygd6vpl30i9hr7cksxqxadpigq8d722f01m7jzqdsjhxky6k39")))

(define-public crate-iota-core-0.1.0-alpha.1 (c (n "iota-core") (v "0.1.0-alpha.1") (d (list (d (n "iota-bundle-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-client") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.5.0") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-signing-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)))) (h "1crs894r519y9yb7sv8r9x26pdwz6qmhv0l4qjaq670dhpjyrn1s")))

(define-public crate-iota-core-0.2.0-alpha.1 (c (n "iota-core") (v "0.2.0-alpha.1") (d (list (d (n "iota-bundle-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-client") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.5.0") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-signing-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)))) (h "0asch3lf4ydifapchlshf935k6h56yy0ps6p96j9czly7hq0rgiz")))

(define-public crate-iota-core-0.2.0-alpha.2 (c (n "iota-core") (v "0.2.0-alpha.2") (d (list (d (n "iota-bundle-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-client") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.5.1") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-signing-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)))) (h "0h8vshqqkrla7vd77y7kdjhsn85mh9az8lvw250qw5j4gzi97mn7") (f (quote (("wasm" "iota-client/wasm"))))))

(define-public crate-iota-core-0.2.0-alpha.3 (c (n "iota-core") (v "0.2.0-alpha.3") (d (list (d (n "iota-bundle-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-client") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.5.1") (d #t) (k 0)) (d (n "iota-crypto-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-signing-preview") (r "^0.1") (d #t) (k 0)) (d (n "iota-ternary-preview") (r "^0.1") (d #t) (k 0)))) (h "0rnwf9cw0wpfl44jczhpf0l5xx38zfyyjqb7zwiisnijjcw6k0w2") (f (quote (("wasm" "iota-client/wasm") ("quorum" "iota-client/quorum"))))))

