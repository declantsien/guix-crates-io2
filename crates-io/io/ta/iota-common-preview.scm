(define-module (crates-io io ta iota-common-preview) #:use-module (crates-io))

(define-public crate-iota-common-preview-0.1.0 (c (n "iota-common-preview") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1cmkbzqa0p7prj61p0q40p22q5x5qjabf7gds91i6r05nnav1fa8")))

