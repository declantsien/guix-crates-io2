(define-module (crates-io io ta iota) #:use-module (crates-io))

(define-public crate-iota-0.1.0 (c (n "iota") (v "0.1.0") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "1qy007r2zwb6yrgndphbids310bcsm4dnb1i9b2kagkb7pry0zid") (y #t)))

(define-public crate-iota-0.1.1 (c (n "iota") (v "0.1.1") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "1andaks3i8656ddc2m4p88gw9x8cj8q2clskqfd21rlng3x22qh1") (y #t)))

(define-public crate-iota-0.1.2 (c (n "iota") (v "0.1.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "0p05a0dggx9v1riy4qryyf2s4r67d35727d26kcdax5xcjs75yd6") (y #t)))

(define-public crate-iota-0.1.3 (c (n "iota") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "08ijlsg1i8wd6c5vmgfqc59zn9pzwffh827ghgx1w4gircpbnskd") (y #t)))

(define-public crate-iota-0.1.4 (c (n "iota") (v "0.1.4") (h "1xkkbs0v55mkcjv6qvn74dsrl0056j3zmybm4h2c3gcxcbw1yf8i")))

(define-public crate-iota-0.2.0 (c (n "iota") (v "0.2.0") (h "1vjp5m5iyq31ghakbjjxy2x8sz5mvsyg3ac3plw0cqk4z80v147q")))

(define-public crate-iota-0.2.1 (c (n "iota") (v "0.2.1") (h "1qxq73phx85s8zjzykkyxzl6ham3fi9h4aaf06h621ih5nxiaq1w")))

(define-public crate-iota-0.2.2 (c (n "iota") (v "0.2.2") (h "0pv3a7913waf8iqd65sbhp5lr6411pdizvk28bk480f48pzk1rw9")))

(define-public crate-iota-0.2.3 (c (n "iota") (v "0.2.3") (h "0ksx8v4qi7nca3a0yyydkl4n69hv3fjks29vlqhby99da3axc3b8") (r "1.31")))

