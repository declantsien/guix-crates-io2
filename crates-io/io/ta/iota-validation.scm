(define-module (crates-io io ta iota-validation) #:use-module (crates-io))

(define-public crate-iota-validation-0.1.0 (c (n "iota-validation") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0ap496ib6r85jbqksg58l52h8i7cfsyp4s8kmi9idh77bl4h7xsx")))

(define-public crate-iota-validation-0.2.0 (c (n "iota-validation") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "00pwivfmpkg7cvwfjrhsg34nwwbw6pvnm2ngr734dq7z7p4yndqh")))

(define-public crate-iota-validation-0.4.0 (c (n "iota-validation") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1kfginvk35zxas8d1cmhw2fs9dl5js10mhsg1ncxsxfbg1dkfprs") (y #t)))

(define-public crate-iota-validation-0.2.1 (c (n "iota-validation") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-model") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0lhk8p4sprz5jby4m62hny4jg2slya46bmldvhnci1b1q0jcbw56")))

