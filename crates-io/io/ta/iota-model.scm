(define-module (crates-io io ta iota-model) #:use-module (crates-io))

(define-public crate-iota-model-0.1.0 (c (n "iota-model") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.1.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hzpbaq7z5gsrva5yxw8i20srv8p3rwk48y5imgac4akb823rr42")))

(define-public crate-iota-model-0.2.0 (c (n "iota-model") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.2.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mxsvq38rlfykwrhrqijfrgvsz1xzsr2zrpfj25jpgxfcs6i23iz")))

(define-public crate-iota-model-0.4.0 (c (n "iota-model") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.4.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fx45q1976g12hvnliyvbjbx06dazlv2vwb48xvs806p67gpk8rn") (y #t)))

(define-public crate-iota-model-0.3.0 (c (n "iota-model") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "iota-constants") (r "^0.2.1") (d #t) (k 0)) (d (n "iota-conversion") (r "^0.3.0") (d #t) (k 0)) (d (n "iota-crypto") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05w3f10jhf8158968d4gnsc7rxjr3mj446k6nfa8zvfbr7n6kgwx")))

