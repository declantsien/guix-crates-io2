(define-module (crates-io io ta iota-sc-hname-generator) #:use-module (crates-io))

(define-public crate-iota-sc-hname-generator-1.0.0 (c (n "iota-sc-hname-generator") (v "1.0.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "0m0nyinb1mzgi9mqsicy8m7pxlmmlgrhjf96zrxa543q5n4vi2pq")))

(define-public crate-iota-sc-hname-generator-1.1.0 (c (n "iota-sc-hname-generator") (v "1.1.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1wiaan6yqi0l7rsz1mv5c2fjrzlxmgam65asadg68mvwy0xnfl96")))

(define-public crate-iota-sc-hname-generator-1.1.1 (c (n "iota-sc-hname-generator") (v "1.1.1") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (d #t) (k 0)))) (h "1w0nwxq0f7jhdv82fama7p42cj24w6gd4kmkanrf4vp6mzczqjci")))

(define-public crate-iota-sc-hname-generator-1.1.2 (c (n "iota-sc-hname-generator") (v "1.1.2") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.64") (d #t) (k 0)))) (h "1r10zh8wd997f4w4h5355p77cy5ph9k4d43a0m39y4yvp2pcv74a")))

(define-public crate-iota-sc-hname-generator-1.1.3 (c (n "iota-sc-hname-generator") (v "1.1.3") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1r3pmk7xhjcm5lvb6s2ad4000wsv5332dgif01d1cgqd4shl9k1q")))

(define-public crate-iota-sc-hname-generator-1.1.4 (c (n "iota-sc-hname-generator") (v "1.1.4") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1248wrihkcz37bbknld5grcvhkhf66vawpnxq2hqjp6jhsmyr8iv")))

(define-public crate-iota-sc-hname-generator-1.1.5 (c (n "iota-sc-hname-generator") (v "1.1.5") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0ra8nd2p8daigzsa9dlalxghmqfhslmncmls7w8zyf00wli6wbmx")))

(define-public crate-iota-sc-hname-generator-1.1.6 (c (n "iota-sc-hname-generator") (v "1.1.6") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1s6lik5x5aqjncwjvf2m2n9ygkxwqjfvxfxynbn2h9lc8zjiwagd")))

