(define-module (crates-io io -c io-cast) #:use-module (crates-io))

(define-public crate-io-cast-0.1.0 (c (n "io-cast") (v "0.1.0") (h "1a22dj3svgwj0y7wywba1y9cbm9zkias6fc6agb0dhgh9q3gkwvv")))

(define-public crate-io-cast-0.1.1 (c (n "io-cast") (v "0.1.1") (h "13s5p2nydr6fi619grqpgk64qyl8ijh0n3vvyl6c8qvy13smvf07")))

