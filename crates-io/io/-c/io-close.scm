(define-module (crates-io io -c io-close) #:use-module (crates-io))

(define-public crate-io-close-0.1.0 (c (n "io-close") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1a6p6vy8sxkpn3hdh2gqk43848wjsklj29q71bgk5xn4d03dhj0y")))

(define-public crate-io-close-0.1.1 (c (n "io-close") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0naqj96dsdm6h4y9x3vax5fd35051pqnrfnr5zxbfxrlyns4v984")))

(define-public crate-io-close-0.1.2 (c (n "io-close") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0yajg8wmnvkf0pc86bll125gc9rv5qp6r1jkgjjn263sb2h08v4x")))

(define-public crate-io-close-0.1.3 (c (n "io-close") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1hs7v26jcmymvjgs5aazb0bx6ddc091haw42a2y9nk12y1bbkcjg")))

(define-public crate-io-close-0.1.4 (c (n "io-close") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1xy3sk5q7dr0wmhlzw6simjmi86xx9vdgjj2c2y50nrglnbyjp7f")))

(define-public crate-io-close-0.1.5 (c (n "io-close") (v "0.1.5") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "0h00940vnmbcy404w8zblfyr3wj4pam4rmifalc30kcsc3ddgv31")))

(define-public crate-io-close-0.1.6 (c (n "io-close") (v "0.1.6") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "0mxvvp78vdm9rlqy761xyvyifflkj78x6h335ksabypiyyy4yn4s")))

(define-public crate-io-close-0.1.7 (c (n "io-close") (v "0.1.7") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "09qr4j6s6z5rjp7j8rjc1amq2k1rvcx7gr35bp0b1g3bz9f9dni5")))

(define-public crate-io-close-0.1.8 (c (n "io-close") (v "0.1.8") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "06q099bni2msdm26r3li5y7r6slv0fqq29gldxbd3laikkxm6fip")))

(define-public crate-io-close-0.1.9 (c (n "io-close") (v "0.1.9") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "1wbn3r7jc0q31w5lv8vvh59vcy6lsy4lisk4h35khh4pzic0k63v")))

(define-public crate-io-close-0.2.0 (c (n "io-close") (v "0.2.0") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "12c4lwgz50jp47aqxv5hisa1ijirkym2sgjfmin8y3gxznij3v8j")))

(define-public crate-io-close-0.2.1 (c (n "io-close") (v "0.2.1") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "0qk08s19xfh0w4p9rdyb85iljdqhply5gvx4spaix41l0nxjx004")))

(define-public crate-io-close-0.2.2 (c (n "io-close") (v "0.2.2") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "1kl7qkhmpc96mnxzhjsjx99nl71f53fxpmw6q631wylc2gncl05f")))

(define-public crate-io-close-0.2.3 (c (n "io-close") (v "0.2.3") (d (list (d (n "kernel32-sys") (r ">=0.2.2, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)))) (h "1rbh8fq3jxfnc19gv7jirxn5ca9lhxzsw9df6aw0gdqw19xfafmx")))

(define-public crate-io-close-0.3.0 (c (n "io-close") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "05f709v605vhv42ac6zn4b907jisyms0ncdf1nypnjpklk3rd8nw")))

(define-public crate-io-close-0.3.1 (c (n "io-close") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f50v4bnsxxmvc57l6lbks9ai5085dclnhlyl9vvdscdyxi5l26m")))

(define-public crate-io-close-0.3.2 (c (n "io-close") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0mv6qay1qbbhx0lrc9mml5h1rrjq4gsngp3am51p855rvrs3m180")))

(define-public crate-io-close-0.3.3 (c (n "io-close") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sbnb1ywzxiz2pkbpdksl0qrdk9gpjz940fl5kkiwbq9n2x7w8cb")))

(define-public crate-io-close-0.3.4 (c (n "io-close") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1qpwcgp36bmmnfhsy920zzpgdqyrfkpq9rgfl8f8qxlv854i3ckw")))

(define-public crate-io-close-0.3.5 (c (n "io-close") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10wjqn73jggx43rzvl8lkbih86s81cd3rlsj9k4famds8jrj0dik")))

(define-public crate-io-close-0.3.6 (c (n "io-close") (v "0.3.6") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dgvkgq46cdhlk34w2j84zzjmqlpx6bglm3vwj7q6z28198j640a")))

(define-public crate-io-close-0.3.7 (c (n "io-close") (v "0.3.7") (d (list (d (n "libc") (r "^0.2.80") (d #t) (t "cfg(unix)") (k 0)) (d (n "os_pipe") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("handleapi" "std" "winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g4hldfn436rkrx3jlm4az1y5gdmkcixdlhkwy64yx06gx2czbcw")))

