(define-module (crates-io io -c io-context) #:use-module (crates-io))

(define-public crate-io-context-0.1.0 (c (n "io-context") (v "0.1.0") (h "0g1qyhpv2cfl4nmir6ggnskigjqa1rp5v77phvq2d3qpi1raphi4")))

(define-public crate-io-context-0.2.0 (c (n "io-context") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1378v98xxakiq4y90zwrzsbi83al0pa75zlwhacbbd33d04givvd") (f (quote (("default") ("context-future" "futures"))))))

