(define-module (crates-io io -r io-result-optional) #:use-module (crates-io))

(define-public crate-io-result-optional-0.1.0 (c (n "io-result-optional") (v "0.1.0") (h "1syrz6ba3ixjk1d259va0wf7khpnwx2hx5jdrklcm54appczflfk") (y #t)))

(define-public crate-io-result-optional-0.1.1 (c (n "io-result-optional") (v "0.1.1") (h "1h80087qyiszp8yjpc72a6w3i9yw0wwb32ac9a1p0piy2n6bjal7")))

(define-public crate-io-result-optional-0.1.2 (c (n "io-result-optional") (v "0.1.2") (h "0pqkdzqq49w68kj910m9mrlhn1dznkg3x750bpfmfxl37zpwszkf")))

(define-public crate-io-result-optional-0.1.3 (c (n "io-result-optional") (v "0.1.3") (h "0dfx5gc7nnv7m7mni79f8cqsgf1hb36x3vhwmxk91mjs9b82fvzs")))

