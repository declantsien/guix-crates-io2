(define-module (crates-io io -r io-read-line-prototype) #:use-module (crates-io))

(define-public crate-io-read-line-prototype-1.0.0 (c (n "io-read-line-prototype") (v "1.0.0") (h "1054sp36y1ywp2x0rhbf4ni3h1kfyp99vgv62akj9vsw8g0zr3ws")))

(define-public crate-io-read-line-prototype-1.0.1 (c (n "io-read-line-prototype") (v "1.0.1") (h "1ahc4gk5s89jvzjjrn7qgf2ang5y5rwrn1593my65zr42g6l4cdx")))

