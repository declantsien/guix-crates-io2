(define-module (crates-io io st iostream) #:use-module (crates-io))

(define-public crate-iostream-0.1.0 (c (n "iostream") (v "0.1.0") (h "1h57jqks3iig34z1bwy2v3ds0a2akilh1fps0x8nqn0qm1xrr088") (y #t)))

(define-public crate-iostream-0.1.1 (c (n "iostream") (v "0.1.1") (h "0s09xii32c8vs9l31vc6gzmilam923ij4hdzw8cqh3j7mknlhilx") (y #t)))

(define-public crate-iostream-0.1.2 (c (n "iostream") (v "0.1.2") (h "1l4al8i8dkfjscz70gz7kcfr9xamls2cfj6qzc352vrc2hqjc1a7") (y #t)))

(define-public crate-iostream-0.1.3 (c (n "iostream") (v "0.1.3") (h "1njyhfj0kajgrc11fzdcdfvm0mxgxmn15fyfv5l1kb19wih5s4hp")))

