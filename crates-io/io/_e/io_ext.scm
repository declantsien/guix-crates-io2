(define-module (crates-io io _e io_ext) #:use-module (crates-io))

(define-public crate-io_ext-0.1.0 (c (n "io_ext") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "03dbfxfn2cfapwn03qx7am3a53m27f9qy2amgvfqlky334w9wswb")))

(define-public crate-io_ext-0.2.0 (c (n "io_ext") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1833s26r1n83qrlbs6kpwmaqfgv4mgw14sxscnrhcp5kn42hp3n5")))

