(define-module (crates-io io -a io-at) #:use-module (crates-io))

(define-public crate-io-at-0.1.0 (c (n "io-at") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1inpr911zcifh20w8zccbsxqmhkgk2cp544avznszc1lf1fjx3ls")))

(define-public crate-io-at-0.1.1 (c (n "io-at") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1yp1rmdcn1gfsmwb8jy16pfx7sdvlj8wh538w0wnhxjzb0vvrgas")))

(define-public crate-io-at-0.1.2 (c (n "io-at") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "014wj91lmgxlcrz7jlprwpdcmia6bl6nv38pdlmwwvxcynix1gd1")))

(define-public crate-io-at-0.1.3 (c (n "io-at") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1lg8v24x7vkw1saadap36p78rghdx9vzwnvpikwp6zyshb3zxki8")))

(define-public crate-io-at-0.1.4 (c (n "io-at") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1369n6fqs6sd6k8g5frf48nq3jwcv4ypvg5zlxr2lfrhph1305hq")))

(define-public crate-io-at-0.1.5 (c (n "io-at") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1ks728ml6ld4m1ad3qiqwcjj89ivc7y0jhz1xs3dpjh8ss3vxhqj")))

(define-public crate-io-at-0.1.6 (c (n "io-at") (v "0.1.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1y2gpfm1541vxswz9978dg9622clmgydp0c0z0yprvmkcmnb608b")))

(define-public crate-io-at-0.1.7 (c (n "io-at") (v "0.1.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "1m3cbwrhix6b9pbhzyddfij0d8kjjvbcbkbr4q6z278734wshq2y")))

(define-public crate-io-at-0.1.8 (c (n "io-at") (v "0.1.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 2)))) (h "103mnmxk4611czmcgz1m8fnsnghc68p20jhw2p4hx5isrd0q6v41")))

(define-public crate-io-at-0.2.0 (c (n "io-at") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "057x2ijf9vf70i0lr2z5b1gmvwk827i8sbaf96f0hsc5vaw3m28g")))

(define-public crate-io-at-0.3.0 (c (n "io-at") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1jfdjrdg804a91pcjvy47ppqw8kx6cf2lqq543mxak1dll6jri83")))

(define-public crate-io-at-0.3.1 (c (n "io-at") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0n923jn123gvnyv13x0sk6j32gwzdikp6aplkh0qq8kk1ggb84vk")))

(define-public crate-io-at-0.4.0 (c (n "io-at") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0y2crwm26ygar18hk8ccrm9zs1iikc9ydbr0fqym3l20nz1bd6ah")))

(define-public crate-io-at-0.4.1 (c (n "io-at") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^2") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0mbrkwhxfij77d26lnavq8dlvfvipqmnj9j33fnrja1hwgx3fslz")))

