(define-module (crates-io io -a io-arg) #:use-module (crates-io))

(define-public crate-io-arg-0.1.0 (c (n "io-arg") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)))) (h "08vs1nma1ycxvdg9k8d4c4wijkq1agl1yn40ypwi81hmnwgfvklk")))

(define-public crate-io-arg-0.1.1 (c (n "io-arg") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)))) (h "19z0kjxp9qg3l4b80cpnh7zbv1wqqxmddqgivzinqb3nq4d6q7b0")))

(define-public crate-io-arg-0.1.2 (c (n "io-arg") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)))) (h "1yrx3xsg1snir99611g5wlw6pfzcg9ph16ip8cady7lbdzw184dw")))

(define-public crate-io-arg-0.1.3 (c (n "io-arg") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)))) (h "0h91d5pblkxibdfqkc90qjdnrqrh3v77vmrhvyachdgdzsv1z72f")))

(define-public crate-io-arg-0.1.4 (c (n "io-arg") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)))) (h "01vdyrijb2hmli2ssb3jx1ksnr1kvlvprl7xfzrsnzn4s6vwlis6")))

(define-public crate-io-arg-0.2.0 (c (n "io-arg") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)))) (h "0wy4njg73j4hn2wa92429k6r5pdm38pfwpgfvn68cb5y2ypz6c3a")))

(define-public crate-io-arg-0.2.1 (c (n "io-arg") (v "0.2.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 2)) (d (n "clap") (r "^4.0.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 2)))) (h "115ki864xss9ard53d9rxn9pcd2d3p3v1f2d0qynjh6fmvg8620l")))

