(define-module (crates-io io -a io-adapters) #:use-module (crates-io))

(define-public crate-io-adapters-0.0.0 (c (n "io-adapters") (v "0.0.0") (h "0zgxbmh8zcqqlp0n5xwgipk03n6w8fa2kcskg63zfzvh4dgq50rq") (y #t)))

(define-public crate-io-adapters-0.1.0 (c (n "io-adapters") (v "0.1.0") (d (list (d (n "supercilex-tests") (r "^0.4.2") (f (quote ("api"))) (k 2)))) (h "1q6x6bww756a5qpfylnypm6nzb7r5xd2da73wk8gfr8dj3z5dfnc") (r "1.65")))

(define-public crate-io-adapters-0.2.0 (c (n "io-adapters") (v "0.2.0") (d (list (d (n "supercilex-tests") (r "^0.4.2") (f (quote ("api"))) (k 2)))) (h "1gvcq3653nxxsb77s91690gxs5yqplf8w699xxf9a19j7xdf3i9r") (r "1.65")))

(define-public crate-io-adapters-0.3.0 (c (n "io-adapters") (v "0.3.0") (d (list (d (n "supercilex-tests") (r "^0.4.4") (f (quote ("api"))) (k 2)))) (h "0075qvp5apmv11yk6df240g915h2nn0fhfg00g3lsm270ky03a0g") (r "1.65")))

