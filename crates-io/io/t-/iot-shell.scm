(define-module (crates-io io t- iot-shell) #:use-module (crates-io))

(define-public crate-iot-shell-0.1.0 (c (n "iot-shell") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0xv0yldi41w4f76pg7ipzp5y04bbf8v8pg4a2d2yfmrvdhf9f9zj")))

