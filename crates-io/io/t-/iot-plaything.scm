(define-module (crates-io io t- iot-plaything) #:use-module (crates-io))

(define-public crate-iot-plaything-0.1.0 (c (n "iot-plaything") (v "0.1.0") (d (list (d (n "rocket") (r "^0.4.7") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.7") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0mqss6pm57llsqfxigsbybx14x4zix9jp96d9dlmzvqa5yz4pwxf")))

