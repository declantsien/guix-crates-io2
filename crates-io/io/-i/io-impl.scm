(define-module (crates-io io #{-i}# io-impl) #:use-module (crates-io))

(define-public crate-io-impl-0.1.0 (c (n "io-impl") (v "0.1.0") (d (list (d (n "io-trait") (r "^0.1.1") (d #t) (k 0)))) (h "0w5s5skjwxn2rq17z0pvd6vanw57kwwb888ixdar6n89547mm0da")))

(define-public crate-io-impl-0.1.1 (c (n "io-impl") (v "0.1.1") (d (list (d (n "io-trait") (r "^0.1") (d #t) (k 0)))) (h "1mz9853a230cd088qxbl456ijvzspn1z7yz6drsyyzpxxxrnqvx2")))

(define-public crate-io-impl-0.1.2 (c (n "io-impl") (v "0.1.2") (d (list (d (n "io-trait") (r "^0.1.1") (d #t) (k 0)))) (h "0cv16qwcydrzdyw9pz0775pn7p75ijvbqm16z2hv4s35iy3vfs5i")))

(define-public crate-io-impl-0.1.3 (c (n "io-impl") (v "0.1.3") (d (list (d (n "io-trait") (r "^0.1.2") (d #t) (k 0)))) (h "1ami627cnl4daz3k6yjgymzjxfzv92q3bip7aqy5nr2xr3dic2i2")))

(define-public crate-io-impl-0.1.4 (c (n "io-impl") (v "0.1.4") (d (list (d (n "io-trait") (r "^0.1.3") (d #t) (k 0)))) (h "0186jlfhyyj203fxhiqcbhl53xih0778f4avx958vlc2dmn7wmlz")))

(define-public crate-io-impl-0.1.5 (c (n "io-impl") (v "0.1.5") (d (list (d (n "io-trait") (r "^0.1.4") (d #t) (k 0)))) (h "1vpcnkrgfp6inv5l320j28p6gvra8ibfpaincy66kvaazg46pp42")))

(define-public crate-io-impl-0.3.0 (c (n "io-impl") (v "0.3.0") (d (list (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1f1xgbp431n8yaiqrar56jw5cxkfhp0l95j83dna3gack4ggddi8")))

(define-public crate-io-impl-0.3.1 (c (n "io-impl") (v "0.3.1") (d (list (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1q2ilamj6q79i9aqhqhx0mz6khlixwgcnzcf9xzn6zpkcpma7nlx")))

(define-public crate-io-impl-0.3.2 (c (n "io-impl") (v "0.3.2") (d (list (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1025262jxd5hxr1d86fs99nzpqs92ahrk2gj3isck01n58zi19hm")))

(define-public crate-io-impl-0.4.0 (c (n "io-impl") (v "0.4.0") (d (list (d (n "io-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1dy6kn8sjbrg1mdyfwr2kvc27a9rlay80kgwkdyx0pdaci9mv4n6")))

(define-public crate-io-impl-0.5.0 (c (n "io-impl") (v "0.5.0") (d (list (d (n "io-trait") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1a3ky6nbk9nswf7ah0q58m3dmf15c3czsns6jm95pa51smkivcl6")))

(define-public crate-io-impl-0.6.0 (c (n "io-impl") (v "0.6.0") (d (list (d (n "io-trait") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0ayjhdyp9xvcdzap9ipss44r6pmnwlbxci0z2d3n8jcy2xm37mhf")))

(define-public crate-io-impl-0.7.0 (c (n "io-impl") (v "0.7.0") (d (list (d (n "io-trait") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1d8whwd155f3phflys921nh884zc7qblp65981lxc8zqv5pd2si5")))

(define-public crate-io-impl-0.8.0 (c (n "io-impl") (v "0.8.0") (d (list (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "0vrd3qsxmh0zhkrxrdv375znvx391f9kbyvw4if2nz4rw03kpbwa")))

(define-public crate-io-impl-0.8.1 (c (n "io-impl") (v "0.8.1") (d (list (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "1d6fpqr9cy9j4725syp2al9f9mz1v4f1rjffmxkh6lxfqhhm2k6x")))

(define-public crate-io-impl-0.9.0 (c (n "io-impl") (v "0.9.0") (d (list (d (n "io-trait") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1362nqr91l5nkyjm8x8cdrg1dlywdn857056jx0zf71gv069lhyf")))

(define-public crate-io-impl-0.10.0 (c (n "io-impl") (v "0.10.0") (d (list (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "1wcqxnbr8zamd8dzk07wpr3f3mr4l3a59l119azizys28fszn7vp")))

(define-public crate-io-impl-0.11.0 (c (n "io-impl") (v "0.11.0") (d (list (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "12p27amfbilfsp1ljf26pld7ddgc5pl9nrwc5qlbknvkr09b4zd4")))

