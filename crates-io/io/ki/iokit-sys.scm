(define-module (crates-io io ki iokit-sys) #:use-module (crates-io))

(define-public crate-IOKit-sys-0.1.0 (c (n "IOKit-sys") (v "0.1.0") (d (list (d (n "CoreFoundation-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)))) (h "1ldrhd52k8zx33z7qkcggpfyds84imcp03jvivhf7749w57dxhcn")))

(define-public crate-IOKit-sys-0.1.1 (c (n "IOKit-sys") (v "0.1.1") (d (list (d (n "CoreFoundation-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "mach") (r "^0.0.3") (d #t) (k 0)))) (h "0jl5w238q3cb2474il9hrfjvp23d2mfvmszr3fc5rj2b4898nglw")))

(define-public crate-IOKit-sys-0.1.2 (c (n "IOKit-sys") (v "0.1.2") (d (list (d (n "CoreFoundation-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "mach") (r "^0.0.3") (d #t) (k 0)))) (h "0rz67cff01fsf0rp5imjib2vk4gzw1z3ks1d34fm9l0gifa32xjn")))

(define-public crate-IOKit-sys-0.1.3 (c (n "IOKit-sys") (v "0.1.3") (d (list (d (n "CoreFoundation-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mach") (r "^0.0.4") (d #t) (k 0)))) (h "08s7ka8vfzvs8lndfgvw1m4z9mjr6wgn5s2s05j9wkxbqdh7zra1")))

(define-public crate-IOKit-sys-0.1.4 (c (n "IOKit-sys") (v "0.1.4") (d (list (d (n "CoreFoundation-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mach") (r "^0.0.5") (d #t) (k 0)))) (h "14q1fkq4nzrydxqqhx2frkcddpbxmjrazaxqvnhfcwbkjkl00b0r")))

(define-public crate-IOKit-sys-0.1.5 (c (n "IOKit-sys") (v "0.1.5") (d (list (d (n "CoreFoundation-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mach") (r "^0.1.1") (d #t) (k 0)))) (h "0jmzyc55r312c5y9v2963bjhryr7ssrvsxl06v96kxmsihwnqscr")))

