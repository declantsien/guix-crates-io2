(define-module (crates-io io _c io_check) #:use-module (crates-io))

(define-public crate-io_check-0.1.0 (c (n "io_check") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.0.0") (d #t) (k 0)))) (h "0npdbvqjl1y0ic1b3ydg3y71a4bs4ml47vb2qlj89cd5vk9hs2rd") (y #t)))

(define-public crate-io_check-0.1.1 (c (n "io_check") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.0.0") (d #t) (k 0)))) (h "1m2cvp02qsnwvbmv8hki9zipslrfmkbrn82dp7skchsspf18lb2v") (f (quote (("default" "backtrace"))))))

(define-public crate-io_check-0.1.2 (c (n "io_check") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.66") (o #t) (d #t) (k 0)) (d (n "either") (r "^1.0.0") (d #t) (k 0)))) (h "1a3ich73limsr1iqc01zcdv8zi4z4wqzcdv01v7l1i55rdb0r1mj") (f (quote (("default" "backtrace"))))))

