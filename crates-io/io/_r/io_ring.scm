(define-module (crates-io io _r io_ring) #:use-module (crates-io))

(define-public crate-io_ring-0.0.1 (c (n "io_ring") (v "0.0.1") (d (list (d (n "rustix") (r "^0.38") (f (quote ("io_uring"))) (k 0)))) (h "1d25dka9gv04ifqk13nm5swhi169670ghm3s2ch4pvs1v1c68rw9") (l "uring-ffi")))

(define-public crate-io_ring-0.0.2 (c (n "io_ring") (v "0.0.2") (d (list (d (n "rustix") (r "^0.38") (f (quote ("io_uring"))) (k 0)))) (h "0j4zgbjixwqzs6p3w5apfw3jz013a2sqg49w5917d0qp593b6ijd") (l "uring-ffi")))

