(define-module (crates-io io -p io-providers) #:use-module (crates-io))

(define-public crate-io-providers-0.1.0 (c (n "io-providers") (v "0.1.0") (h "0qnh0fhl5q5jrayk8blcyany8pg1453mkwgn15lrly14kq9b57yk")))

(define-public crate-io-providers-0.1.1 (c (n "io-providers") (v "0.1.1") (h "00qwpwlac321rbqpwd5lxfrc8fb9zgf186xc0v62yfgyni2id2x2")))

(define-public crate-io-providers-0.1.2 (c (n "io-providers") (v "0.1.2") (h "16in70szx31fsb2l3501xwc7si3iwbawc6l7qc7mxzfn0rbvi65r")))

(define-public crate-io-providers-0.2.0-beta (c (n "io-providers") (v "0.2.0-beta") (d (list (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "1hrj5w8w8pdn98bkmm6dfx0z96p8v8m7phphcp7896dxvrzp6h6y")))

(define-public crate-io-providers-0.2.0-beta.2 (c (n "io-providers") (v "0.2.0-beta.2") (d (list (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "183rg6xc7hdkrrzh5qvpv32s1gxcvdc7z55pk5cb26qxyx0yw2rs")))

(define-public crate-io-providers-0.2.0-beta.3 (c (n "io-providers") (v "0.2.0-beta.3") (d (list (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "1fa3ffdwykw4pai7hqfxqijbh1fhjvfb1aswiaqcm2mly1mhb83h")))

