(define-module (crates-io io en ioendian) #:use-module (crates-io))

(define-public crate-ioendian-0.1.0 (c (n "ioendian") (v "0.1.0") (h "1gzv7vng0cwsrpca5ibinl706nv0pp7yc1aqddqczn66jg1hr020")))

(define-public crate-ioendian-0.1.1 (c (n "ioendian") (v "0.1.1") (h "0lwrnsf6vk4pws5cxmyhvqml781vlk9dqqmqzzchqhwdm7s7j5jl")))

(define-public crate-ioendian-0.1.2 (c (n "ioendian") (v "0.1.2") (h "0z2c1ca7d1snp0mfjm6s2dvdji91vgj5mgiz06pp79sigcv6gzzz")))

(define-public crate-ioendian-0.1.4 (c (n "ioendian") (v "0.1.4") (h "0zlqvmlw1pvj3kn4z6m6r1fz9syxk694r787p83klph2132zxrnl")))

