(define-module (crates-io io _d io_de_ser) #:use-module (crates-io))

(define-public crate-io_de_ser-0.0.3 (c (n "io_de_ser") (v "0.0.3") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "deser") (r "^0.0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1mwjwrwh7jkbxr1y0kpy510r6lz723z3wq1zqpfzf478amr0k06v") (y #t)))

(define-public crate-io_de_ser-0.1.1 (c (n "io_de_ser") (v "0.1.1") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "io_deser") (r "^0.0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "02gj56j0c7gr5bwydfwnc506g8wqn4q6v8nnnh55zzrry1ah5d9i") (y #t)))

(define-public crate-io_de_ser-0.1.2 (c (n "io_de_ser") (v "0.1.2") (d (list (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "io_deser") (r "^0.0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1vbvw0l5hsf2dnx30gnrqvmykpgzhfh904hrp9854gxdlcygrd9c") (y #t)))

