(define-module (crates-io io _d io_deser) #:use-module (crates-io))

(define-public crate-io_deser-0.0.2 (c (n "io_deser") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cnh6kmm1dry15yr80nddbb5bsd91nym7pwp2ky71jngvzd68r9k")))

(define-public crate-io_deser-0.0.3 (c (n "io_deser") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bpr5wqiyx6rg8vqc53yln37z2g5s9fpk4g46vf0s19a22pmajnc")))

(define-public crate-io_deser-0.3.0 (c (n "io_deser") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d7r4i63qilbw8wj06yz2yfjpl6hqam1pkghlzgynp94mn1az53c")))

(define-public crate-io_deser-0.3.2 (c (n "io_deser") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1adisjpvibpffm91vhf31ah0aiva8b6gq7d68rhgqqykxqh74hck")))

(define-public crate-io_deser-0.3.3 (c (n "io_deser") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b7n2bds59qbfqc4fki9fn31crg3k6fdch3gi7wximky1ccipdk0")))

(define-public crate-io_deser-0.4.0 (c (n "io_deser") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12jrvw22wx0ypwp4s9b6n5gy7kf39d41siaagm9lr28ic95lgzs9")))

