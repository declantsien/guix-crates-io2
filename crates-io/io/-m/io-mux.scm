(define-module (crates-io io -m io-mux) #:use-module (crates-io))

(define-public crate-io-mux-0.1.0 (c (n "io-mux") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0pp69c6fb35x5a0k0jl194c489w502qay3wcsmmi8m6z8b3rfxkq")))

(define-public crate-io-mux-0.1.1 (c (n "io-mux") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "01sdpaxx21k4g505yq6kyhyc96padhb2vj70hdz46i9bpbyv5f85")))

(define-public crate-io-mux-1.0.0 (c (n "io-mux") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0jfvbvczm43l5wylhzpiwbsyprd4bj0h9x9s4284hxm2xcy5fp9k") (y #t)))

(define-public crate-io-mux-1.0.1 (c (n "io-mux") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0g5z2p07af5nkwddmm7px02j67g1ajpr1qhhv5yfggnlbwcjjb1n") (f (quote (("test-portable") ("experimental-unix-support"))))))

(define-public crate-io-mux-1.1.0 (c (n "io-mux") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0pc8hvhjk14v09cb6bcnfsj6kpcvydi9nw6yzz3jkc8d74g31qyq") (f (quote (("test-portable") ("experimental-unix-support"))))))

(define-public crate-io-mux-1.2.0 (c (n "io-mux") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0wc2abafmn88m1a4qf8fhv8p5c1vnq2gf8kzfqxhgvql5q35hip7") (f (quote (("test-portable") ("experimental-unix-support"))))))

(define-public crate-io-mux-1.3.0 (c (n "io-mux") (v "1.3.0") (d (list (d (n "async-io") (r "^0.1.11") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^0.1.1") (d #t) (k 2)) (d (n "futures-lite") (r "^0.1.11") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1iqj6kws03jnfhn0js7wz8ilwj76xrbnnsqccg3whm9k8cbcvaq4") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-1.4.0 (c (n "io-mux") (v "1.4.0") (d (list (d (n "async-io") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "11dfbk4zb2dx9jqqyg0awf5zg0bfrdwj1zll27n42c9lpw19khyg") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-1.5.0 (c (n "io-mux") (v "1.5.0") (d (list (d (n "async-io") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 2)) (d (n "rustix") (r "^0.37.19") (f (quote ("net"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "12q482fwk8yik9hb49cw6sr1vzr3lb1jd4n62iq13q0jqvkcxlba") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-2.0.0 (c (n "io-mux") (v "2.0.0") (d (list (d (n "async-io") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^1.0.1") (d #t) (k 2)) (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.2") (d #t) (k 2)) (d (n "rustix") (r "^0.37.19") (f (quote ("net"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1d5rm5h071c8d2fkmrdsj4b239p943srbzzbg47fyjgn2fqaj2sg") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-2.1.0 (c (n "io-mux") (v "2.1.0") (d (list (d (n "async-io") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^1.7.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("net"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 0)))) (h "1wj31h34gmxf4jb1ili6a1y8570pjc4g8fi0y9cnljpazq52ywjv") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-2.1.1 (c (n "io-mux") (v "2.1.1") (d (list (d (n "async-io") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^1.7.0") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("net"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.6") (d #t) (k 0)))) (h "0fklnm39gmfl0rwkm7cxj6ma3hjvgz683vs213xp1xn1dabfc29s") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

(define-public crate-io-mux-2.2.0 (c (n "io-mux") (v "2.2.0") (d (list (d (n "async-io") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "async-process") (r "^2.0.1") (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "futures-lite") (r "^2.2.0") (d #t) (k 2)) (d (n "rustix") (r "^0.38") (f (quote ("net"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9") (d #t) (k 0)))) (h "0lzkx4a9kp3x4k4w2wly40kz15c0zjbqbr9qj9i8ymaz2lby0n5s") (f (quote (("test-portable") ("experimental-unix-support") ("async" "async-io"))))))

