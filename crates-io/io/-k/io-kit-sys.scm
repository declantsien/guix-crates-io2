(define-module (crates-io io -k io-kit-sys) #:use-module (crates-io))

(define-public crate-io-kit-sys-0.1.0 (c (n "io-kit-sys") (v "0.1.0") (d (list (d (n "core-foundation-sys") (r "^0.6") (d #t) (k 0)) (d (n "mach") (r "^0.2") (d #t) (k 0)))) (h "186h7gm6kf1d00cb3w5mpyf9arcdkxw7jzhl1c4wvm2xk5scq7gj")))

(define-public crate-io-kit-sys-0.2.0 (c (n "io-kit-sys") (v "0.2.0") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "mach") (r "^0.3.2") (d #t) (k 0)))) (h "068r17sns736ayykdxw4w9v9wxfyaa8xc2ai9wb9cvv8r7rzg2bp")))

(define-public crate-io-kit-sys-0.3.0 (c (n "io-kit-sys") (v "0.3.0") (d (list (d (n "core-foundation-sys") (r "^0.8.3") (d #t) (k 0)) (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "1pj8cc7wmpnm7w1bfni45hmv1gcp95dk5q7cpl7zzpn1mhll8bcv")))

(define-public crate-io-kit-sys-0.4.0 (c (n "io-kit-sys") (v "0.4.0") (d (list (d (n "core-foundation-sys") (r "^0.8.4") (d #t) (k 0)) (d (n "mach2") (r "^0.4.1") (d #t) (k 0)))) (h "0h2674vwf4z1hpipsmja04x74iwcyya3w3bkqq7p3wfwwlqcnsa7")))

(define-public crate-io-kit-sys-0.4.1 (c (n "io-kit-sys") (v "0.4.1") (d (list (d (n "core-foundation-sys") (r "^0.8.6") (d #t) (k 0)) (d (n "mach2") (r "^0.4.2") (d #t) (k 0)))) (h "0ysy5k3wf54yangy25hkj10xx332cj2hb937xasg6riziv7yczk1")))

