(define-module (crates-io io _i io_interner) #:use-module (crates-io))

(define-public crate-io_interner-0.1.0 (c (n "io_interner") (v "0.1.0") (h "0d6gpn6yg1b9ai7dlnpj2dilaqfzdwx125f793jzxzfjh9q18pmc") (y #t)))

(define-public crate-io_interner-0.1.1 (c (n "io_interner") (v "0.1.1") (h "0wzd1x2n0hax2vf5b3m9fhmy6z7i3gjs211ybfrdczhlncd8k61y")))

(define-public crate-io_interner-0.1.2 (c (n "io_interner") (v "0.1.2") (h "19zvp76aafcj97p75lqc715px1ipqw8xy1lqxx3lsbxj2jbjjkgv")))

(define-public crate-io_interner-0.2.0 (c (n "io_interner") (v "0.2.0") (h "16fv429wm8b9yay7frcy9ml58r02fzf7l5y357rrf8ba0fdkmv1c") (y #t)))

(define-public crate-io_interner-0.2.1 (c (n "io_interner") (v "0.2.1") (h "1i1l2z59kgqzaf602irywv4z4kz6h2rwf12ivghh7gbzig2aba00") (y #t)))

(define-public crate-io_interner-0.2.2 (c (n "io_interner") (v "0.2.2") (h "0c18yh36wp51cgd83lfz6p88wn5g74r1c40srxbalhihdjxx6am3") (y #t)))

(define-public crate-io_interner-0.2.3 (c (n "io_interner") (v "0.2.3") (h "1km42gh3llnp5clyfadsj6x9j0c38hrabblxwq0g64y7vpr5mwn4")))

(define-public crate-io_interner-0.3.0 (c (n "io_interner") (v "0.3.0") (h "0xix923kwvyp5qggmf4ms3qzxx83iw77nyfc5ihfz762d4w5wwjp")))

(define-public crate-io_interner-0.4.0 (c (n "io_interner") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "sync_2") (r "^1") (d #t) (k 0)))) (h "01abvldg4ir8g0sk500v442k1gnrpsmlpwqcrwzqxqzg96zchdcy") (f (quote (("serde_support" "serde" "serde_derive" "lazy_static"))))))

(define-public crate-io_interner-0.4.1 (c (n "io_interner") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "memchr") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1h0r6y24l3bcai41pmh6sfid1lgq820w1lm7jj06y24dzhpp5g16") (f (quote (("serde_support" "serde" "serde_derive" "lazy_static" "memchr") ("nightly"))))))

