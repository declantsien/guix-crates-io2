(define-module (crates-io io -e io-extra) #:use-module (crates-io))

(define-public crate-io-extra-0.1.0 (c (n "io-extra") (v "0.1.0") (h "1g1s6lmznkiq9jfmb6k4bv7v1z4vpv8p8xqh9x5p9ykhw8cwvg07")))

(define-public crate-io-extra-0.2.0 (c (n "io-extra") (v "0.2.0") (h "00qh7yhkrf3mjnj654cjp3qwf6c9mf43171685mmk6vk0w0ilz3q")))

(define-public crate-io-extra-0.3.0 (c (n "io-extra") (v "0.3.0") (h "1hypzv7wwf1fllklsms4pxk4m619g3m3n1kb0d3y1sdrn1bzav5n")))

