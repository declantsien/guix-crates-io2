(define-module (crates-io io -e io-error) #:use-module (crates-io))

(define-public crate-io-error-0.1.0 (c (n "io-error") (v "0.1.0") (h "0pvcs9cgiv71k6172b65vsasp0p6mf3w02plcn04czb7qs3bdnfv")))

(define-public crate-io-error-0.1.1 (c (n "io-error") (v "0.1.1") (h "10ssqqnmmd2sjnwlj9z8cz4af3nxpjdykw7vsmag18f07lhqd4x8")))

