(define-module (crates-io io -e io-enum) #:use-module (crates-io))

(define-public crate-io-enum-0.1.0 (c (n "io-enum") (v "0.1.0") (d (list (d (n "derive_utils") (r "^0.6.2") (d #t) (k 0)))) (h "1iz40srx8kna20mn5xhi41vpvpncn94wrnz2avcmrycci75jxrpm") (f (quote (("read_initializer") ("default"))))))

(define-public crate-io-enum-0.1.1 (c (n "io-enum") (v "0.1.1") (d (list (d (n "derive_utils") (r "^0.6.2") (d #t) (k 0)))) (h "15nk14q2l6438m49ij61vn9v22yhy2y38z7cqd3iahyylmjcn3xm") (f (quote (("read_initializer") ("default"))))))

(define-public crate-io-enum-0.1.2 (c (n "io-enum") (v "0.1.2") (d (list (d (n "derive_utils") (r "^0.6.3") (d #t) (k 0)))) (h "0v2rb792r3dnbikv893xhcqcldyqvdz2rx3r9ysvyk38wjgaci1i") (f (quote (("read_initializer") ("default"))))))

(define-public crate-io-enum-0.1.3 (c (n "io-enum") (v "0.1.3") (d (list (d (n "derive_utils") (r "^0.6.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0wi0b6v03xlj3mlvvnpq5nx21dkka20n9cyhr224ppads2xjclwv") (f (quote (("read_initializer") ("iovec") ("default"))))))

(define-public crate-io-enum-0.2.0 (c (n "io-enum") (v "0.2.0") (d (list (d (n "derive_utils") (r "^0.7.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full"))) (d #t) (k 0)))) (h "066rmv3zgcdc8abga8yda0i04k1slzlqrgmasg880qnq2ndgl44s") (f (quote (("read_initializer") ("default"))))))

(define-public crate-io-enum-0.2.1 (c (n "io-enum") (v "0.2.1") (d (list (d (n "derive_utils") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "103p380vf47xvi566072dkii90qqhn7d3nx1wwzcyaq9igh0q2zd") (f (quote (("read_initializer"))))))

(define-public crate-io-enum-0.2.2 (c (n "io-enum") (v "0.2.2") (d (list (d (n "derive_utils") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "151i5wfn43klaqhy7an8s0hk20fbvmi913mwd5cwffklihan4mr8")))

(define-public crate-io-enum-0.2.3 (c (n "io-enum") (v "0.2.3") (d (list (d (n "derive_utils") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dkk52r1dcqpv5izh8dbgh9z4qrlk7bwqqhbhi4irjlw808gqrz4")))

(define-public crate-io-enum-0.2.4 (c (n "io-enum") (v "0.2.4") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1j66g9a9sx13lhywfalxr940zb135z2inhyw0d9qhdqkpl8v2qqm")))

(define-public crate-io-enum-0.2.5 (c (n "io-enum") (v "0.2.5") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "12diah406igjyw6j0lqmjdjz2mzp06vd0wwcg2r2qs2lp6c9qkr3")))

(define-public crate-io-enum-0.2.6 (c (n "io-enum") (v "0.2.6") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1xww7arfcna3xnwvksflxycyvm9hhqcybvyxjx6x6vcq5b9lylwl")))

(define-public crate-io-enum-1.0.0 (c (n "io-enum") (v "1.0.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0yiylsg3w724bcc93ypa9phjfw80lhbijjyakq0bpdq87zzsbkn0")))

(define-public crate-io-enum-1.0.1 (c (n "io-enum") (v "1.0.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "derive_utils") (r "^0.11") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.8") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0sw05plh36p5vihscw6jz8h297x5s7av0gjnf8las2i61xmk1qq3")))

(define-public crate-io-enum-1.0.2 (c (n "io-enum") (v "1.0.2") (d (list (d (n "derive_utils") (r "^0.12") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vjy2jz1wj768vw0l21r5cf8cgkq6l589i2d5cl6dcccjmxd9c74") (r "1.31")))

(define-public crate-io-enum-1.1.0 (c (n "io-enum") (v "1.1.0") (d (list (d (n "derive_utils") (r "^0.13") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1gdggm85najvxyrw7yqdnaj7vcmy8d0i7ndzwx1gbjf9971n5ih1") (r "1.56")))

(define-public crate-io-enum-1.1.1 (c (n "io-enum") (v "1.1.1") (d (list (d (n "derive_utils") (r "^0.13.1") (d #t) (k 0)) (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "07p9v3wvkgzh9626qdsdy5wmi7wrgrhhgkhmmrr00ikvl9zma1ak") (r "1.56")))

(define-public crate-io-enum-1.1.2 (c (n "io-enum") (v "1.1.2") (d (list (d (n "derive_utils") (r "^0.14") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "1i4i7gasf8sh2dxqqr745gcmnszkfdz5zkbgl107qnnbcawsmm80") (r "1.56")))

(define-public crate-io-enum-1.1.3 (c (n "io-enum") (v "1.1.3") (d (list (d (n "derive_utils") (r "^0.14.1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "0vlafdnv61nbzyxszviq5la5503z0pk4yppfb7n3x9wr5mqkvdak") (r "1.56")))

