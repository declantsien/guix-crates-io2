(define-module (crates-io io _u io_utils) #:use-module (crates-io))

(define-public crate-io_utils-0.1.0 (c (n "io_utils") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "04v1byj57553ab91rjz20y1n1md5xxqsrdca8a2phq6vky0i6s7v")))

(define-public crate-io_utils-0.2.0 (c (n "io_utils") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "15s6dl6phcx3mwzy7z9lycdrxcswwicqfsgnhgkw776558cnk5ag")))

(define-public crate-io_utils-0.2.1 (c (n "io_utils") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1n1pvs3nbnmyrk6zl6f7a5l3xw7yxnbs4zrmgbm58ck9d82yqndp")))

(define-public crate-io_utils-0.2.2 (c (n "io_utils") (v "0.2.2") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "0gi262ig0m28mrdgr56kvy7yk24bgykiff8gqxsxwfwywamfpand")))

(define-public crate-io_utils-0.2.3 (c (n "io_utils") (v "0.2.3") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "0vzzry2i1lwavs9q42hgm6xhw82qmn1f8pcpr405nr0c41kz1jzw")))

(define-public crate-io_utils-0.2.4 (c (n "io_utils") (v "0.2.4") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.12") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.0") (d #t) (k 0)))) (h "08gs01620vwba3isamyl49gnwi9w6xx8j2bak4yrdc1472gkdb5w")))

(define-public crate-io_utils-0.2.5 (c (n "io_utils") (v "0.2.5") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0zvbql69fjm80p9dm660mh16msb50pd1ps01lpayr21wxspiha4g")))

(define-public crate-io_utils-0.2.6 (c (n "io_utils") (v "0.2.6") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0s8jl2cm5x4hzp4fdg5viidachwqbixjlygsq52nlqxhp9im1law")))

(define-public crate-io_utils-0.2.7 (c (n "io_utils") (v "0.2.7") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1j2bbwjqlnh9k8zqczdyh3c8s3i9ls3qm01fwxy1bpx0c7wav8yn")))

(define-public crate-io_utils-0.2.8 (c (n "io_utils") (v "0.2.8") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.16") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1dsdq335bhbr0b852py1nklzc0mgz09v7drl3vlkgridr2ak54q0")))

(define-public crate-io_utils-0.2.9 (c (n "io_utils") (v "0.2.9") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1f5ag3qq03w93ww4c3hjg23l1n4b2c2wmr0ckh0pjalsfxq8pj1a")))

(define-public crate-io_utils-0.2.10 (c (n "io_utils") (v "0.2.10") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "1qbf9qx1blfc725f3kyrr4d23drbl15izxrxng6vrw9l0c5aylas")))

(define-public crate-io_utils-0.2.11 (c (n "io_utils") (v "0.2.11") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0kx6knl8lm0ifjixq0qvahmsa458b1j5kfplc6w9b2cxp5537xrc")))

(define-public crate-io_utils-0.2.12 (c (n "io_utils") (v "0.2.12") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)) (d (n "vector_utils") (r "^0.1") (d #t) (k 0)))) (h "1bdpi3b4cskak0gwzxyhlbbk948c1s8y4va692z17agd2ibh9j6x")))

(define-public crate-io_utils-0.3.0 (c (n "io_utils") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1qqabqvvwp0hbmwcdfl4wnf1shaqvk17k4lss6lz7dcskyrbb7fg")))

(define-public crate-io_utils-0.3.1 (c (n "io_utils") (v "0.3.1") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "lz4") (r "^1.23.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "0zcmkpsmnkb4b1dk9mj39s23vnpn7cxgczvqycj66a5vzrxxlmx8")))

(define-public crate-io_utils-0.3.2 (c (n "io_utils") (v "0.3.2") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lz4") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "string_utils") (r "^0.1") (d #t) (k 0)))) (h "1bnixqcp4cpx5x2gp6l6yc5gf9gffllspyki1l7hzsgj12dj736q")))

