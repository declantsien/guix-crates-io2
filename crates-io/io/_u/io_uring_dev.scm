(define-module (crates-io io _u io_uring_dev) #:use-module (crates-io))

(define-public crate-io_uring_dev-0.0.1 (c (n "io_uring_dev") (v "0.0.1") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "rawring") (r "^2") (d #t) (k 0)))) (h "002h4fiizfd4j8nnn0s0v5c34n9wy591v7af6a5mhmn6fmycz5zl") (y #t)))

(define-public crate-io_uring_dev-0.1.0 (c (n "io_uring_dev") (v "0.1.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rawring") (r "^2") (d #t) (k 0)))) (h "16a1d58vhshyldvpqncy2h84d845p9pvjl2li21zirsy7psklq1m") (y #t)))

(define-public crate-io_uring_dev-0.2.0 (c (n "io_uring_dev") (v "0.2.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "rawring") (r "^2") (d #t) (k 0)))) (h "1wxkfyxkac4s75in05liqgizx54qfln0zx3da7myvki4mni46byd") (y #t)))

(define-public crate-io_uring_dev-0.3.0 (c (n "io_uring_dev") (v "0.3.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "1lg29zvm40l9myynalx0y9kqazdy3r0zci6y1sy78ymjjvhm9kl0") (y #t)))

(define-public crate-io_uring_dev-0.4.0 (c (n "io_uring_dev") (v "0.4.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0pbw93ydky91aq6iygnqv1ph6pdhfnvp45x4p6rxrb9bgg0zq77z") (y #t)))

(define-public crate-io_uring_dev-0.4.1 (c (n "io_uring_dev") (v "0.4.1") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0fyfxl9xlvlyiaw5dq2kdsrd8mchhql6pvfaday7adsv57ksiqih") (y #t)))

(define-public crate-io_uring_dev-0.5.0 (c (n "io_uring_dev") (v "0.5.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "1xwgdfryipz5qhjijhfqr9yi1d9frdaicga7cym2y92grcn39zln") (y #t)))

(define-public crate-io_uring_dev-0.5.1 (c (n "io_uring_dev") (v "0.5.1") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "09a123s5ryd1fd79f10q0x0xs9wiwhzch27h1jrrynvfj6l1p051") (y #t)))

(define-public crate-io_uring_dev-0.6.0 (c (n "io_uring_dev") (v "0.6.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0iadm2sww2nvjzw367qlm2016n2wawm7cwbbbaajs46ka55vw3ix") (y #t)))

(define-public crate-io_uring_dev-0.7.0 (c (n "io_uring_dev") (v "0.7.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "06pjvxgcm36i79pclgv0r8yv5yklg8xpxp4kp050004zsgsp2ha3") (y #t)))

(define-public crate-io_uring_dev-0.7.1 (c (n "io_uring_dev") (v "0.7.1") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0bw73jf3sa2f3a5mwwablzj7phlr2ihnnw8v5la23cxf0bbmrlxq") (y #t)))

(define-public crate-io_uring_dev-0.7.2 (c (n "io_uring_dev") (v "0.7.2") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "1bf6qdggiki5pd22av7sa3r1xqkb11qcdnklq12f088wawwnns4v") (y #t)))

(define-public crate-io_uring_dev-0.8.0 (c (n "io_uring_dev") (v "0.8.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0pak5yx3bjmq6giq6wkfd9j7aqrjd8p56r7zpkrn6ikavd71zyas") (y #t)))

(define-public crate-io_uring_dev-0.9.0 (c (n "io_uring_dev") (v "0.9.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_mmap") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_setup") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_wakeup") (r "^0.1") (d #t) (k 0)))) (h "1sz76zc0yiyyqjlz1vhgaxn83j5k2fp0hhhxrpjlpvzisanq9v9d") (y #t)))

(define-public crate-io_uring_dev-0.9.1 (c (n "io_uring_dev") (v "0.9.1") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_mmap") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_setup") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_wakeup") (r "^0.1") (d #t) (k 0)))) (h "16k3h28xfkwz2c61jz3nbbc9xna9b2szp5p12m2nlq0zfmimbdjz") (y #t)))

(define-public crate-io_uring_dev-0.9.2 (c (n "io_uring_dev") (v "0.9.2") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_mmap") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_setup") (r "^0.2") (d #t) (k 0)) (d (n "io_uring_wakeup") (r "^0.1") (d #t) (k 0)))) (h "0q7aj3fzb39n5gfdv8281nzsi2n4519crin4ca6k9jbw3s69l38k") (y #t)))

(define-public crate-io_uring_dev-1.0.0 (c (n "io_uring_dev") (v "1.0.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_mmap") (r "^1") (d #t) (k 0)) (d (n "io_uring_setup") (r "^1") (d #t) (k 0)) (d (n "io_uring_wakeup") (r "^1") (d #t) (k 0)))) (h "07nq9z663lbipv5cas802b3ny72fbqvr8b9xnb5hmijpz010yksj") (y #t)))

