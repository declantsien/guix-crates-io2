(define-module (crates-io io _u io_uring_setup) #:use-module (crates-io))

(define-public crate-io_uring_setup-0.1.0 (c (n "io_uring_setup") (v "0.1.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)))) (h "18hn7qjx7vvg9vpx38r9jnrxs0978x8sjp1xjryl8v1mzz0vwg3l") (y #t)))

(define-public crate-io_uring_setup-0.1.1 (c (n "io_uring_setup") (v "0.1.1") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "0194sb3nx71db01a6kchmjapwix9r6ndd3x14brl274l2yjq5vnx") (y #t)))

(define-public crate-io_uring_setup-0.1.2 (c (n "io_uring_setup") (v "0.1.2") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "0866sylap459q3k6pf076ls0fvhzv6z8jb9v8z41k4alv7k9lnak") (y #t)))

(define-public crate-io_uring_setup-0.1.3 (c (n "io_uring_setup") (v "0.1.3") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "0wndlcln0pn1s0k5diqfv5kv3y3n4gnkiyni70jpvi9cgnjk7njb") (y #t)))

(define-public crate-io_uring_setup-0.2.0 (c (n "io_uring_setup") (v "0.2.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "06wkbq4dbkinprx4gv40qq2dk5g13mlcdw2s54qwm79iahjidi8n") (y #t)))

(define-public crate-io_uring_setup-1.0.0 (c (n "io_uring_setup") (v "1.0.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "13bsc13cb1kf0rnkv9mx25ks0awzn6lci6liwnmpbpfzwqirph8k")))

