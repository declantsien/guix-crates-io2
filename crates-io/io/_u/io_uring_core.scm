(define-module (crates-io io _u io_uring_core) #:use-module (crates-io))

(define-public crate-io_uring_core-0.1.0 (c (n "io_uring_core") (v "0.1.0") (d (list (d (n "io_uring_dev") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jv07jfnjpb9g8nyblakj2v6wifxp7x9jzsg8a6kqbiahak24v7r") (y #t)))

(define-public crate-io_uring_core-0.1.1 (c (n "io_uring_core") (v "0.1.1") (d (list (d (n "io_uring_dev") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cbwi3yi81pz5ywfavpwr2k6l0y5fy7fsvwjvjqr85qhvkvhipj1") (y #t)))

(define-public crate-io_uring_core-0.1.2 (c (n "io_uring_core") (v "0.1.2") (d (list (d (n "io_uring_dev") (r "^0.6") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nfklvyhijzaxg1dl7blhq5c70c6q4r3s2h8jf2mpxd98nq1asrk") (y #t)))

(define-public crate-io_uring_core-0.1.3 (c (n "io_uring_core") (v "0.1.3") (d (list (d (n "io_uring_dev") (r "^0.7") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ajrnm45cddi2p8wkgxfnwq8i0my746rp85bf7vrzlc0nf315a2r") (y #t)))

(define-public crate-io_uring_core-0.1.4 (c (n "io_uring_core") (v "0.1.4") (d (list (d (n "io_uring_dev") (r "^0.7") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cql2mc8ic91yrlw9rgz4bpnzdfidcaha3h5l12ls5za2zyrd8j8") (y #t)))

(define-public crate-io_uring_core-0.1.5 (c (n "io_uring_core") (v "0.1.5") (d (list (d (n "io_uring_dev") (r "^0.7") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rsz164fmx5phsdjfda887icna7miwj9nn7afwfag4mby98n2m2r") (y #t)))

(define-public crate-io_uring_core-0.2.0 (c (n "io_uring_core") (v "0.2.0") (d (list (d (n "io_uring_dev") (r "^0.8") (d #t) (k 0)) (d (n "io_uring_sys") (r "^0.1") (d #t) (k 0)))) (h "0a84g02xm0l9bxly2zddrsqxlgplmfv74rbhjnh1ab00bz7ggk9l") (y #t)))

