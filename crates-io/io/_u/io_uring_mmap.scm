(define-module (crates-io io _u io_uring_mmap) #:use-module (crates-io))

(define-public crate-io_uring_mmap-0.1.0 (c (n "io_uring_mmap") (v "0.1.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "0in3j4vvqk8nhcxm07fhg0p10n7zbg5db54cl06qs16mnhgl523s") (y #t)))

(define-public crate-io_uring_mmap-0.1.1 (c (n "io_uring_mmap") (v "0.1.1") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "0aq9hjv438m6dvcp7daxgwm8j1rwid6gi5j47k9cl5xhbhz4zbp8") (y #t)))

(define-public crate-io_uring_mmap-0.1.2 (c (n "io_uring_mmap") (v "0.1.2") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "0shvm28h8mp73d3sjx2v8z2amvawn542qv7sgc9apqa8q1dj9vsq") (y #t)))

(define-public crate-io_uring_mmap-0.1.3 (c (n "io_uring_mmap") (v "0.1.3") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "06501gqb3wxdawk9jb0q1f89x55m98a494jj867hpil075vpwy37") (y #t)))

(define-public crate-io_uring_mmap-0.1.4 (c (n "io_uring_mmap") (v "0.1.4") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "1z1hwxjgyq5ggsyj5dyhiy0vj4xp56p8bkanysdpxjf42bpmlq92") (y #t)))

(define-public crate-io_uring_mmap-0.2.0 (c (n "io_uring_mmap") (v "0.2.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "0kvwcl6ijylvgwdxw7ncsflks4g687zgkwkfrnn5s0i8xglc4hf4") (y #t)))

(define-public crate-io_uring_mmap-0.2.1 (c (n "io_uring_mmap") (v "0.2.1") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)) (d (n "szof") (r "^0.2") (d #t) (k 0)))) (h "0d7vnr254xrf5pwplzis9b7fig8va98kipchw4dv8rj8k96wwmbi") (y #t)))

(define-public crate-io_uring_mmap-1.0.0 (c (n "io_uring_mmap") (v "1.0.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "0p563lnilgfrqajy46qnxxzq40smnh7djcijm6q770hzyj23mlql")))

