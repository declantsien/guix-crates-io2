(define-module (crates-io io _u io_uring_wakeup) #:use-module (crates-io))

(define-public crate-io_uring_wakeup-0.1.0 (c (n "io_uring_wakeup") (v "0.1.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)))) (h "0rz0nymkv784isdlvvaqz6jfy4syjnl3zgdqjn2a0likjdsv54d3") (y #t)))

(define-public crate-io_uring_wakeup-0.1.1 (c (n "io_uring_wakeup") (v "0.1.1") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "1jmljhz3w7y7xhyf70jj87yzhsi7h53sbbzvsl2acscqb0pm7sms") (y #t)))

(define-public crate-io_uring_wakeup-1.0.0 (c (n "io_uring_wakeup") (v "1.0.0") (d (list (d (n "io_uring_header") (r "^0.1") (d #t) (k 0)) (d (n "io_uring_syscall") (r "^0.1") (d #t) (k 0)) (d (n "map_err") (r "^0.1") (d #t) (k 0)))) (h "09w6b3zmpnfpiyra6rsky4x92ifmw8wzzbj9aw461h1vi553id6f")))

