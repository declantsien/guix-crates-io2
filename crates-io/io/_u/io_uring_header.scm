(define-module (crates-io io _u io_uring_header) #:use-module (crates-io))

(define-public crate-io_uring_header-0.1.0 (c (n "io_uring_header") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0any77827c2w1sic78gg4h7z7kmbr87rxw99vgn09rbdg75phw2v") (y #t)))

(define-public crate-io_uring_header-0.1.1 (c (n "io_uring_header") (v "0.1.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "18hyb6nk364bfnakb9zjxl1ckdkxcb8523v9yh9zgkip2iz9yh8k") (y #t)))

(define-public crate-io_uring_header-0.1.2 (c (n "io_uring_header") (v "0.1.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1vsby5j8rxjk36nys6kngyfi8pzpiw4pq0byawbrdaf4xxwd5058") (y #t)))

(define-public crate-io_uring_header-0.1.3 (c (n "io_uring_header") (v "0.1.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "01l7bk7bxnm7pxga8324s1jfd59gsldxfk82smj2v3vvd4zw2w3h")))

