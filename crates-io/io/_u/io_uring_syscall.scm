(define-module (crates-io io _u io_uring_syscall) #:use-module (crates-io))

(define-public crate-io_uring_syscall-0.1.0 (c (n "io_uring_syscall") (v "0.1.0") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)))) (h "0r1yyrcvfpzsxn9496ffz4cj586cmf0ab4wlxim55wl3bggfypmh") (y #t)))

(define-public crate-io_uring_syscall-0.1.1 (c (n "io_uring_syscall") (v "0.1.1") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)))) (h "0ip6bfl6vv0mgrn5z0667r0gvww0pbgqsvlr031k996gn1l43zzx") (y #t)))

(define-public crate-io_uring_syscall-0.1.2 (c (n "io_uring_syscall") (v "0.1.2") (d (list (d (n "do_syscall") (r "^1") (d #t) (k 0)))) (h "01wv3v8rr715fjrs8iifpf8qnkdgwn3c20801zbr26am3rq0jpfr")))

