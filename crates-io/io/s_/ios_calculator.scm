(define-module (crates-io io s_ ios_calculator) #:use-module (crates-io))

(define-public crate-ios_calculator-0.1.0 (c (n "ios_calculator") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "0h58m9sbsxhlw5vzfvs4199479rnls55ynkcamixwvx66c9ycsm2")))

(define-public crate-ios_calculator-0.2.0 (c (n "ios_calculator") (v "0.2.0") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "0qbj0xmvgn4f4s3d7v7cq695x3bl1rpr2sgm5cgygd0iqknj7kdw")))

(define-public crate-ios_calculator-0.2.1 (c (n "ios_calculator") (v "0.2.1") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "029c3ysbgqqdkkhkl9h7n1pfbw2mf10zgspkb0c2ha4cmjrg89kc")))

