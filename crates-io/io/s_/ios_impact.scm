(define-module (crates-io io s_ ios_impact) #:use-module (crates-io))

(define-public crate-ios_impact-0.1.0 (c (n "ios_impact") (v "0.1.0") (d (list (d (n "icrate") (r "^0.0.4") (f (quote ("Foundation"))) (d #t) (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (k 0)))) (h "0yy9bcc82zj6zfpyrzpmq047cp6ym6pqby90yzvm4li90m3n20bv")))

(define-public crate-ios_impact-0.1.1 (c (n "ios_impact") (v "0.1.1") (d (list (d (n "icrate") (r "^0.0.4") (f (quote ("Foundation"))) (d #t) (k 0)) (d (n "objc2") (r "^0.4.1") (d #t) (k 0)))) (h "1ia356nxxg4p2jywfndylphx46878brmprnz54gd9xkhhkaj3dhx")))

