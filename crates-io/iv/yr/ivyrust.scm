(define-module (crates-io iv yr ivyrust) #:use-module (crates-io))

(define-public crate-ivyrust-0.1.0 (c (n "ivyrust") (v "0.1.0") (h "1f7qwwlg0i1c0k0q7rb5f4q63y25d2kmzfgzwx5d397dvzs9wkg8")))

(define-public crate-ivyrust-0.1.1 (c (n "ivyrust") (v "0.1.1") (h "0sazr9zvx98vdb9rzm9vaxr83fyv9kdx9d6x4a7wga11dy6cz85l")))

