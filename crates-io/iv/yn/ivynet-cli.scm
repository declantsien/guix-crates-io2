(define-module (crates-io iv yn ivynet-cli) #:use-module (crates-io))

(define-public crate-ivynet-cli-0.0.1 (c (n "ivynet-cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ivynet-core") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jdzsz3g40hwxzd6nr06z4k2x6r8mgfmy0zgfjcbdkpvnqn5ciym")))

