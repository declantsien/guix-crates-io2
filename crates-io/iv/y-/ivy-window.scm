(define-module (crates-io iv y- ivy-window) #:use-module (crates-io))

(define-public crate-ivy-window-0.10.0 (c (n "ivy-window") (v "0.10.0") (d (list (d (n "ash") (r "^0.35.0") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "glfw") (r "^0.43.0") (d #t) (k 0)) (d (n "ivy-base") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-vulkan") (r "^0.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10xwbl9m6gwwlls315s2n5qwh67ln02rfcvgsg905ycgfdcdcxl8")))

(define-public crate-ivy-window-0.10.3 (c (n "ivy-window") (v "0.10.3") (d (list (d (n "ash") (r "^0.35.0") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "glfw") (r "^0.43.0") (d #t) (k 0)) (d (n "ivy-base") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-vulkan") (r "^0.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06xj1mhicwk20sgyp3zpwqj5lisbjn99y16l5d6b9az1hdmz3hpc")))

