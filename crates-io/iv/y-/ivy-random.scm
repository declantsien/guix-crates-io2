(define-module (crates-io iv y- ivy-random) #:use-module (crates-io))

(define-public crate-ivy-random-0.10.0 (c (n "ivy-random") (v "0.10.0") (d (list (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1509jm8669q383ws1cc4ykvz2x2qaiadhc64vnx2j39sfnxd5gx2")))

(define-public crate-ivy-random-0.10.3 (c (n "ivy-random") (v "0.10.3") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "09v7f3599fgb2l2blxdilpqr3k7i69mclpm8m1hk5q0i3aqrdbwy")))

