(define-module (crates-io iv y- ivy-input) #:use-module (crates-io))

(define-public crate-ivy-input-0.10.0 (c (n "ivy-input") (v "0.10.0") (d (list (d (n "flume") (r "^0.10.9") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "glfw") (r "^0.43.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "ivy-base") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-resources") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-window") (r "^0.10.0") (d #t) (k 0)))) (h "0whqf33dvghlj8mndsalwri4q68mali4az0cwhsg1zmizchfq5az")))

(define-public crate-ivy-input-0.10.3 (c (n "ivy-input") (v "0.10.3") (d (list (d (n "flume") (r "^0.10.10") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "glfw") (r "^0.43.0") (f (quote ("vulkan"))) (d #t) (k 0)) (d (n "ivy-base") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-resources") (r "^0.10.0") (d #t) (k 0)) (d (n "ivy-window") (r "^0.10.0") (d #t) (k 0)))) (h "0w4msbf33wimkjkszw79w8sj21r3ig2lf564ig40nkgqn4jwq490")))

