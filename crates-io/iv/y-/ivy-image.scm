(define-module (crates-io iv y- ivy-image) #:use-module (crates-io))

(define-public crate-ivy-image-0.10.0 (c (n "ivy-image") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "libc") (r "^0.2.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0q1akwh9xa4msq2n9fahplrxgi3vsfqqaam1qvrps5948j1z0pgx")))

