(define-module (crates-io iv or ivory_kinematics) #:use-module (crates-io))

(define-public crate-ivory_kinematics-0.1.0 (c (n "ivory_kinematics") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vasjfzyviavffrhcjqgw7y73450ss6wg4s8bc81y104wkws4qw5")))

(define-public crate-ivory_kinematics-0.1.1 (c (n "ivory_kinematics") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "05pw5i0ahx65980mnp38xy729nglf16zwjghbmmkk8rv2rr0sxmy")))

(define-public crate-ivory_kinematics-0.1.2 (c (n "ivory_kinematics") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0b2wdgbws8q2yxn7wfjil69lkl5xb4x0aahwsr9xprzdki9qp4vi")))

(define-public crate-ivory_kinematics-0.1.3 (c (n "ivory_kinematics") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vcccbmjrjf8dssfw21pwy5fxrnyfcbyx7171i8k1qp47rh54als")))

(define-public crate-ivory_kinematics-0.1.4 (c (n "ivory_kinematics") (v "0.1.4") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "14sgyj6q1jh35gyhxxhyg3c82xzdzpxasj236xksrk8ns2jk3pcc")))

(define-public crate-ivory_kinematics-0.1.5 (c (n "ivory_kinematics") (v "0.1.5") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1yd9cfrgdk6x6dwqchavnry89znpayczdz2h09gf2da66z40q1ra")))

(define-public crate-ivory_kinematics-0.1.6 (c (n "ivory_kinematics") (v "0.1.6") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1g7pcc8hjx5ihhyjvpydyhg87kkjhvnyz8avsqh6ba05bbnqirhh")))

(define-public crate-ivory_kinematics-0.1.7 (c (n "ivory_kinematics") (v "0.1.7") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1fhyp5fhd3v6nf7mi5km86ic6knqsb4hf5d1sllvwhbdbv7grz6y")))

(define-public crate-ivory_kinematics-0.1.8 (c (n "ivory_kinematics") (v "0.1.8") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "02bld69w4jf2rx4micfvayy32g0h10768jps19z14ypi9qk5z2km")))

(define-public crate-ivory_kinematics-0.1.9 (c (n "ivory_kinematics") (v "0.1.9") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0pyc4hl1j9fzxg5nqssqzkgnfd1shx7hd9b9snp762psdj38hfrz")))

(define-public crate-ivory_kinematics-0.2.0 (c (n "ivory_kinematics") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "01i1lw6v4n2dxprmv7iffwk8vcnkcvgz73br4rmvdx0f4b60xdgj")))

(define-public crate-ivory_kinematics-0.2.1 (c (n "ivory_kinematics") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vlh72gqxfm19kc21ha85r9manrqwq28qwimswckvpk94y9r319g")))

(define-public crate-ivory_kinematics-0.2.2 (c (n "ivory_kinematics") (v "0.2.2") (d (list (d (n "ivory_constants") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1di2vhwi62v8pm4avg3kzk6zwfalk6hzcbd888mahp6ranz3kzlv")))

(define-public crate-ivory_kinematics-0.2.3 (c (n "ivory_kinematics") (v "0.2.3") (d (list (d (n "ivory_constants") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "18sj2h7h1imb00xqrhy1vmg7dyfxrq0q561nnalzdj6b9znc1vsl")))

