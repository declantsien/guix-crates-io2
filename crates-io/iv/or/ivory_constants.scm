(define-module (crates-io iv or ivory_constants) #:use-module (crates-io))

(define-public crate-ivory_constants-0.1.0 (c (n "ivory_constants") (v "0.1.0") (h "1drwn7z0mm0fdh4cj2dwfrp6a9n13xpwgk87bilbq8rnv2m7xfb4")))

(define-public crate-ivory_constants-0.1.1 (c (n "ivory_constants") (v "0.1.1") (h "006ishwjb6xx4f5888xl1m17bmva2vsd7ic87fdr107awmc12312")))

