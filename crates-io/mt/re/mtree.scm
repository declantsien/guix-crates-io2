(define-module (crates-io mt re mtree) #:use-module (crates-io))

(define-public crate-mtree-0.1.0 (c (n "mtree") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "173jqx0pfxf5w6v7bpr7ysmc3hvzsjihjfbh50nmrgkpfr7iv9dv")))

(define-public crate-mtree-0.1.1 (c (n "mtree") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1mq2imlvgbyylqwxxhvah0bc6hc8rfhb91gjy5r05srvk36p4397")))

(define-public crate-mtree-0.1.2 (c (n "mtree") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "19cv697xgxcx2h7aa8028i7jpbipq0mv3bsjnapfx4gchc1rl6sk")))

(define-public crate-mtree-0.1.3 (c (n "mtree") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0n726cx8v21q2r0ib189b579zr5yinpi080p3ynsl5gh3065im2l")))

(define-public crate-mtree-0.1.4 (c (n "mtree") (v "0.1.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "10bxl566lvy24p5hgllib4bqvqjv1b2xald6kqlx6nvyh7g7f7vr")))

(define-public crate-mtree-0.2.0 (c (n "mtree") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "15520cz7aryj7gz4922v5yc6xjj1q72rmkf0mcklqgd1slxmar8h")))

(define-public crate-mtree-0.2.1 (c (n "mtree") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0zbdvwbzbd55lrwm2miql0z1crm9yr87xh236dnm1ciahd6zpzbl")))

(define-public crate-mtree-0.3.0 (c (n "mtree") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0b66h2y4rh24iq0bdzffk249q8w39cs6h5jrbxq5z1r238s74gqh")))

(define-public crate-mtree-0.3.1 (c (n "mtree") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0lzd5cav7vj85l2gywycnr5n91pw26ks2c51n72l11s7x4i8ya2b")))

(define-public crate-mtree-0.4.0 (c (n "mtree") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "179zwg59829rdz0vp7wky7gcz2ibms9r8npgiag5fx1bmc4jnmqi")))

(define-public crate-mtree-0.4.1 (c (n "mtree") (v "0.4.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0cc785aff4ns5mv6mpjj14rsx66sm6dr0br0hqi45m14ncgldrvz")))

(define-public crate-mtree-0.4.2 (c (n "mtree") (v "0.4.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0dl8a18pxbwkan6vziiyws6yazjd70n8ma89vvsn8q7llham1c4v")))

(define-public crate-mtree-0.4.3 (c (n "mtree") (v "0.4.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1x2476zz94jxq5kmgdqj5jibxsjvyxmmmiqslfixfw7fj96whvxq")))

(define-public crate-mtree-0.5.0 (c (n "mtree") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "newtype_array") (r "^0.1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0g5r91ffc6qf0wwr1bcx1vh0j0cl6nmy85qnggs9i1y1kian8awx")))

