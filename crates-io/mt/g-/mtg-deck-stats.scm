(define-module (crates-io mt g- mtg-deck-stats) #:use-module (crates-io))

(define-public crate-mtg-deck-stats-0.1.0 (c (n "mtg-deck-stats") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)))) (h "0ffq7k2iz2qhs04a1xa7hi1qq9vi3j06v2gfivzffni6d6ay6066")))

