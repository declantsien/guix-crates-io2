(define-module (crates-io mt im mtime) #:use-module (crates-io))

(define-public crate-mtime-0.1.0 (c (n "mtime") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1pb5iq1vb3yp2xpfkgynx4ny3rpy0n8faa4lim6p170lqxg9wybr")))

