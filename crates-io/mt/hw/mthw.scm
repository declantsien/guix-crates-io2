(define-module (crates-io mt hw mthw) #:use-module (crates-io))

(define-public crate-MTHW-0.1.0 (c (n "MTHW") (v "0.1.0") (d (list (d (n "aurelius") (r "^0.7.5") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("rt" "macros" "io-util" "process" "rt-multi-thread"))) (d #t) (k 0)) (d (n "wry") (r "^0.27.0") (d #t) (k 0)))) (h "07g6d0jjampzyy50ladvriwjwdlzcbp6811za3nrsvs8p488clwp")))

