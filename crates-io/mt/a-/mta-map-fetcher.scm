(define-module (crates-io mt a- mta-map-fetcher) #:use-module (crates-io))

(define-public crate-mta-map-fetcher-0.1.0 (c (n "mta-map-fetcher") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros" "fs"))) (d #t) (k 0)))) (h "13yqsic2dr4f1ldaz5adbsj77ffnr4wj3z7s9pxjcfqnrd59cjy1") (r "1.78")))

(define-public crate-mta-map-fetcher-0.1.1 (c (n "mta-map-fetcher") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("rustls-tls"))) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt" "macros" "fs"))) (d #t) (k 0)))) (h "02w7zpqbbqyiny4q02gazmzbb40wslb0ihygn7nshda93qxs11b5") (r "1.78")))

