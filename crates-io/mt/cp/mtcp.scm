(define-module (crates-io mt cp mtcp) #:use-module (crates-io))

(define-public crate-mtcp-0.1.0 (c (n "mtcp") (v "0.1.0") (d (list (d (n "android_logger") (r "^0.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "md5") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16slx9dr4pamk5yx0qmafnwcq0lk1z38gfl92kshqixjs5fsqvfk") (f (quote (("http_proxy") ("default" "http_proxy"))))))

