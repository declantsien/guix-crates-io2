(define-module (crates-io mt l- mtl-info) #:use-module (crates-io))

(define-public crate-mtl-info-0.2.0 (c (n "mtl-info") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "~2") (d #t) (k 0)) (d (n "colored") (r "~1") (d #t) (k 0)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "simple_logger") (r "~1") (d #t) (k 0)))) (h "1b9y4xlb03x4jvnn0bzvm13m7z4y46cym661n9yy8dfwfaymbsfm")))

