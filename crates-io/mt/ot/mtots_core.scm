(define-module (crates-io mt ot mtots_core) #:use-module (crates-io))

(define-public crate-mtots_core-0.1.0 (c (n "mtots_core") (v "0.1.0") (h "0mbs2xm68g96hddyrygdvxkp0x3z5fn312n5zmd5mvnzjykmz2rm")))

(define-public crate-mtots_core-0.1.1 (c (n "mtots_core") (v "0.1.1") (h "1v6ja5a1ggxxbnka4zn6yd38q0mgbv29ywcc0248yqa39bh4mhcb")))

(define-public crate-mtots_core-0.1.2 (c (n "mtots_core") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^6.2") (o #t) (d #t) (k 0)) (d (n "xref") (r "^0.1") (d #t) (k 0)))) (h "051p2bbcc82gvilwkxkpqg0saiyk7a9j0l8m7s3ydpac6y3fi8v2") (f (quote (("line" "rustyline") ("default"))))))

