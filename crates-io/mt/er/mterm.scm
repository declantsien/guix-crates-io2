(define-module (crates-io mt er mterm) #:use-module (crates-io))

(define-public crate-mterm-0.1.0 (c (n "mterm") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.9") (d #t) (k 0)) (d (n "winit") (r "^0") (d #t) (k 0)))) (h "0cv7x409l5qvifvs0vq7mh1lycqaldb0sw0151sl2h0zfphk0l0i")))

(define-public crate-mterm-0.1.1 (c (n "mterm") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.9") (d #t) (k 0)) (d (n "winit") (r "^0") (d #t) (k 0)))) (h "1v8nipic6njibbg5wd0fwld246d0k5r3y07lzkfinyw9lg5rmch6")))

