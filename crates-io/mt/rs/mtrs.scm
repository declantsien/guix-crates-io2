(define-module (crates-io mt rs mtrs) #:use-module (crates-io))

(define-public crate-mtrs-0.1.0 (c (n "mtrs") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08169zw97bg8zc9d74qzh5rcp1qwy0zqxxi4zv9cqxfygphpvcdq")))

(define-public crate-mtrs-0.1.1 (c (n "mtrs") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mwibnqqbjxjh6qhsbl2mv5s78dgwnkh8806rxp7k7j5qkmdxfj3")))

(define-public crate-mtrs-0.1.2 (c (n "mtrs") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "041sgb6rdcirhgh1gp6gbsqjgvlchl51lfmz7857243arrzsg6nh")))

(define-public crate-mtrs-0.1.3 (c (n "mtrs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gm0akp8qza5xwqj7b4ir2xiv8px529iyvcpk3xk4vhc8h4g4ghw")))

(define-public crate-mtrs-0.1.4 (c (n "mtrs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12h237mr30ff8ksnwg9dy0wnmaaw4wprd70s5hx5h23pcm9kzmfj")))

(define-public crate-mtrs-0.2.0 (c (n "mtrs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0p2cjpfbqzqq62dn4a2wf9zyzrj73kk0j4mnbn7b4305njql0acl")))

