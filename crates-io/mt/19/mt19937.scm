(define-module (crates-io mt #{19}# mt19937) #:use-module (crates-io))

(define-public crate-mt19937-0.1.0 (c (n "mt19937") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1a1b2sg37zclrfpmm450qh6vlm182pr5lwf4hzjxkfndykmq9wxs")))

(define-public crate-mt19937-1.0.0 (c (n "mt19937") (v "1.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "18p4i7m9vyr2w335zkhrm5lz8834583d3g5r02sash91jlbcgyhj")))

(define-public crate-mt19937-1.0.1 (c (n "mt19937") (v "1.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1gq8anmd5xn2rqpg3513lpj7xlhmr81chcx6iw53c1n7m8yjjx66")))

(define-public crate-mt19937-2.0.0 (c (n "mt19937") (v "2.0.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0n21hcw8v61ni64a16l1njabn8s6v42k4k5dqk0gpnrf8ssd2v9y")))

(define-public crate-mt19937-2.0.1 (c (n "mt19937") (v "2.0.1") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "18g3i5b5lz9kf2b8hbskiay6bs4766l1dv6am68mj39pxli7zjhj")))

