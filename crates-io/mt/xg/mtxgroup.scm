(define-module (crates-io mt xg mtxgroup) #:use-module (crates-io))

(define-public crate-mtxgroup-0.1.0 (c (n "mtxgroup") (v "0.1.0") (d (list (d (n "spin") (r "^0.9.8") (k 0)))) (h "0w1jys8p14wqjy5axnbq5xrmp2c1jrsvy0b7sg6a2f1b1h9lqbm8") (f (quote (("std") ("spin" "spin/mutex" "spin/spin_mutex") ("default" "std")))) (y #t)))

(define-public crate-mtxgroup-0.1.1 (c (n "mtxgroup") (v "0.1.1") (d (list (d (n "spin") (r "^0.9.8") (k 0)))) (h "1bjv48ijxf550kfpmyx2p0fzc1nbq8i1nmkkmq03pv95289cr02y") (f (quote (("std") ("spin" "spin/mutex" "spin/spin_mutex") ("default" "std"))))))

