(define-module (crates-io mt ag mtag-cli) #:use-module (crates-io))

(define-public crate-mtag-cli-0.1.0 (c (n "mtag-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)))) (h "098bgn6ym58kh5w8r333105f3cxmxpqmrc5fy22k1fdiya9ny5n3")))

(define-public crate-mtag-cli-0.1.1 (c (n "mtag-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)))) (h "1xpcssrdzywsqra6c1i6x1fm16k9k6k7x5715hjjxs8pc7hdr7gy")))

(define-public crate-mtag-cli-0.1.2 (c (n "mtag-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)))) (h "1sydb56nz4kkmclanzyhcw4mc26ja6ykwb4fw4qd73wzkli6n94b")))

(define-public crate-mtag-cli-0.1.3 (c (n "mtag-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "lofty") (r "^0.15.0") (d #t) (k 0)))) (h "1fqycln8qr1zqd07gl05rdd1c5c36n8ni5wrlilv0p2lnx32bs14")))

