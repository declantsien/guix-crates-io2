(define-module (crates-io mt ag mtag) #:use-module (crates-io))

(define-public crate-mtag-0.4.3 (c (n "mtag") (v "0.4.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)))) (h "0q4bq80bjiwk655y0af9qa4aw2rcnqzn4zsf29kjgikqwj7srpnv")))

(define-public crate-mtag-0.5.1 (c (n "mtag") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "env" "unicode" "wrap_help"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "env" "unicode" "wrap_help"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.0.2") (d #t) (k 1)) (d (n "mp4ameta") (r "^0.11.0") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "0lrbz7zgnhya4nn8wnr7a53w1bxm7a63pscwbshprazskw5wak2s")))

