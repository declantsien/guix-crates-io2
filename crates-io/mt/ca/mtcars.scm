(define-module (crates-io mt ca mtcars) #:use-module (crates-io))

(define-public crate-mtcars-0.0.1 (c (n "mtcars") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0s55020z9q7bd2jgcax20hsw1bcp55nbngm43knbygvymg090mrx")))

