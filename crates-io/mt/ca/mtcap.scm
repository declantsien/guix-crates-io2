(define-module (crates-io mt ca mtcap) #:use-module (crates-io))

(define-public crate-mtcap-0.1.0 (c (n "mtcap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "0hcd5i71pirx5h8599kqfkbm5kdcdwljpanklc4wqb3l84ncx5m4")))

(define-public crate-mtcap-0.1.1 (c (n "mtcap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)))) (h "1ih3pj071lwicv6np2byk05xajkb9fqlka5bj4i6jkmi2ndbib8g")))

(define-public crate-mtcap-0.2.0 (c (n "mtcap") (v "0.2.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ab60cwn0nbs376d1h6fhh6pb5d09l4fzai24f2ix6cb921fa42h")))

(define-public crate-mtcap-0.3.0 (c (n "mtcap") (v "0.3.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "012yzajm4mykp3csy9cshpjvqyv9y7ljbllz658hgv4ahl5fmw98")))

