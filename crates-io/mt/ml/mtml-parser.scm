(define-module (crates-io mt ml mtml-parser) #:use-module (crates-io))

(define-public crate-mtml-parser-0.0.1 (c (n "mtml-parser") (v "0.0.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)))) (h "1ghlmwzl7p2si54fvamir3lp4pkqp7vfi5jxbskkc1dz0v8wgvnb")))

(define-public crate-mtml-parser-0.0.2 (c (n "mtml-parser") (v "0.0.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gda6hff02969xj43hsgjl1i86v1b3k78rfsn35j3sz6jpmwjfyb")))

(define-public crate-mtml-parser-0.0.3 (c (n "mtml-parser") (v "0.0.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12akm32danzipjyfnwnhh8rs2ivzinjlg3mnmxps18ir0h2yqqyy")))

(define-public crate-mtml-parser-0.0.5 (c (n "mtml-parser") (v "0.0.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0vmsmn3n7ml1qyy50ahb362hqlxig64j2k0rqy1bgvccrfbb62nn")))

(define-public crate-mtml-parser-0.0.6 (c (n "mtml-parser") (v "0.0.6") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1f9zya8wz9qnvjkkkwc4ibh865y710s22bjmm3kmbfhrn1r6x5ff")))

(define-public crate-mtml-parser-0.0.7 (c (n "mtml-parser") (v "0.0.7") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jhd3zlyryzc84psd69rhwg4kz7x54zavdx32v83dfmjv90wq3v7")))

