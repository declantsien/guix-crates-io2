(define-module (crates-io mt bl mtbl-sys) #:use-module (crates-io))

(define-public crate-mtbl-sys-0.1.0 (c (n "mtbl-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "^1.1.1") (d #t) (k 2)))) (h "0pqfxx7kyb2ip9pfzd5gqff0dvsvvwljhyixqxdw6a13qajwmpni")))

(define-public crate-mtbl-sys-0.1.1 (c (n "mtbl-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.2") (d #t) (k 2)))) (h "0bbk0l4y8vm7d7hc6wwic3qf1m2i41qvn7110wb3xp8dkbwkv71v")))

(define-public crate-mtbl-sys-0.2.0 (c (n "mtbl-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.19") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 2)))) (h "0wq782kq6xb8dnzdbf2y6hq3xfpr8rkj9wbfhg380nab6d1098c3")))

