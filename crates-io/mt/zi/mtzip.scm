(define-module (crates-io mt zi mtzip) #:use-module (crates-io))

(define-public crate-mtzip-0.1.0 (c (n "mtzip") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "03xfk5zqlkbb70gkjx0nn2ywwrf2gwabh8ig9npsjq4inwzy8zr3")))

(define-public crate-mtzip-0.1.1 (c (n "mtzip") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1hwzqbarygz0ck2k7lr1kscnddi5cc0w047agjs0lyfmszppw506")))

(define-public crate-mtzip-0.2.0 (c (n "mtzip") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0kmpmpjaksa5w58xcj564fikd961aiznafsk2cv0fsv56z6kr8fk")))

(define-public crate-mtzip-0.2.1 (c (n "mtzip") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1ww2qxqqvrzkvzw5c0z8zmznis58zcbkzdhylh78hs2mb8w5ms9y")))

(define-public crate-mtzip-0.3.0 (c (n "mtzip") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "13ryxlpwafjvi549rpp190zn0872f7g7vkfb0k8y045lsvvnsryi")))

(define-public crate-mtzip-0.4.0 (c (n "mtzip") (v "0.4.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1v4zq2xg4jyykdirkvgvciv80nwqdzc8prhydl2h581mfz7pl0b9")))

(define-public crate-mtzip-0.4.1 (c (n "mtzip") (v "0.4.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1z8r4c1hshfyk15nqqm1ir8xyda3jyc8zk07qiyzxxbaplkyz2jw")))

(define-public crate-mtzip-0.5.0 (c (n "mtzip") (v "0.5.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1s4cjsmqy0d7fp4bz0yla1jinxgpgim8mk09qnwn7v1jp9lvjyrb")))

(define-public crate-mtzip-0.6.0 (c (n "mtzip") (v "0.6.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25") (o #t) (d #t) (k 0)))) (h "073cns15xl0j0scvvyxlfja2qbmn4axg0yc88vkcvkq0qs0dqml5") (f (quote (("default" "auto-threading") ("auto-threading" "sysinfo"))))))

(define-public crate-mtzip-0.6.1 (c (n "mtzip") (v "0.6.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0xwmaxhr07xwv5diqp9ncpdabwx5j8gcdrbhh4hr21nnjq9n5kk6") (f (quote (("default" "auto-threading") ("auto-threading" "sysinfo"))))))

(define-public crate-mtzip-0.7.0 (c (n "mtzip") (v "0.7.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1x5c7px2cfpzgkybkgd07gbwqnnnc1p761dzzbzf629m4y3kbhcf")))

(define-public crate-mtzip-0.7.1 (c (n "mtzip") (v "0.7.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1ywi17qyfzv7y22pwdaj4s48vsarr91n3ns9gvzwy1r9rwsi4k0m") (y #t)))

(define-public crate-mtzip-0.7.2 (c (n "mtzip") (v "0.7.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "16j6ny1qf3n7d13z9jrmj099b0zwqdvdj5r9aga7l0izs39fk8g1")))

(define-public crate-mtzip-0.7.3 (c (n "mtzip") (v "0.7.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1n33lx97w3iy673c1gpmh98dglb2j51f115yh4kq4xw1bg0ljdmk")))

(define-public crate-mtzip-1.0.0 (c (n "mtzip") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0cc6vd1g89cv6fhm60ras91fcpc0nrvrd1h6m0mk041hlarbf4dy")))

(define-public crate-mtzip-1.1.0 (c (n "mtzip") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0m3z54b0ck6p0k4wjw5jy0gqjfxbn34y6pvj9yzzzrrba62is89x")))

(define-public crate-mtzip-1.2.0 (c (n "mtzip") (v "1.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "18rp02xmrsxcnwc745h8yn3dm7sfg3zg7q8jnmjnyxinl627afya")))

(define-public crate-mtzip-1.3.0 (c (n "mtzip") (v "1.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1pckbq59l5s4bznxj1737ikals9xg8d6dkr1qwmflvj8z8xz8n43")))

(define-public crate-mtzip-2.0.0 (c (n "mtzip") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (k 0)))) (h "0mchjn51zcj549kx8ldygk1x1glxcq6ammllqs3fqddcawlc9pwr") (f (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend"))))))

(define-public crate-mtzip-2.0.1 (c (n "mtzip") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (k 0)))) (h "0pfcw9v9hnrh5cck67vzsd3hnbid3wk17a2gnn3hmvwsjgcm415h") (f (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend"))))))

(define-public crate-mtzip-3.0.0 (c (n "mtzip") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (k 0)) (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "176q355r22y8j8i8hskk7fak4vxwnc4xlf5r32sf6mf4jni3xj1s") (f (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (y #t) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-mtzip-3.0.1 (c (n "mtzip") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (k 0)) (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "0zm86r07naswaxbq1hg0i3wjz52iqibmkxy2jifmn47wc07iwd5y") (f (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (y #t) (s 2) (e (quote (("rayon" "dep:rayon"))))))

(define-public crate-mtzip-3.0.2 (c (n "mtzip") (v "3.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (k 0)) (d (n "rayon") (r "^1.10.0") (o #t) (d #t) (k 0)))) (h "0cg6nxycanshncgw0v0hbl5kc7fdc11zcxdbsb6cbjwjs4yr3n5l") (f (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (s 2) (e (quote (("rayon" "dep:rayon"))))))

