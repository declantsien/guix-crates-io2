(define-module (crates-io mt cn mtcnn) #:use-module (crates-io))

(define-public crate-mtcnn-0.1.0 (c (n "mtcnn") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tensorflow") (r "^0.16.0") (d #t) (k 0)))) (h "0wcvfzr8zws3ilc0333n6iy5nqfs82r6ymb32n2faihqvnl1s8kp")))

