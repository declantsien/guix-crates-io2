(define-module (crates-io mt in mtinit) #:use-module (crates-io))

(define-public crate-mtinit-0.1.0 (c (n "mtinit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "048nh58iy975jxi4cbijdh1i8wx6zw85yv06blhhgwcrjs180yw8")))

