(define-module (crates-io mt em mtemp) #:use-module (crates-io))

(define-public crate-mtemp-0.1.0 (c (n "mtemp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0g9h8ax1lz5xbhgfg8ap3217cp9z84wc7msn8hcmbgfx5s0fjaix")))

(define-public crate-mtemp-0.1.1 (c (n "mtemp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "09fhp9czfc8x2iwk88bxp8qjlv8alv4g3anq0s0fzs2j1l7jr2s7")))

(define-public crate-mtemp-0.1.2 (c (n "mtemp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1dxsasc7wcxazr1mfkh6rvr0jwk1a22yk0fd365hb4ym2bc99zj5")))

(define-public crate-mtemp-0.1.21 (c (n "mtemp") (v "0.1.21") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "07nnlfcbbn0n0lmsdnadhfhj5cgd2r9lk9cdnxhsm8a6v6az1n2x")))

