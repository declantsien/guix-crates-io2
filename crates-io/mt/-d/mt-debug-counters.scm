(define-module (crates-io mt -d mt-debug-counters) #:use-module (crates-io))

(define-public crate-mt-debug-counters-0.1.0 (c (n "mt-debug-counters") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0d387sg2pv286mdj4w9rraq1vqwrrji6w1w7sv34hqn86hyxr1w1")))

(define-public crate-mt-debug-counters-0.1.1 (c (n "mt-debug-counters") (v "0.1.1") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "166p3sv3hr0kanmh813rnx85i80za5d7b2l63vnggrk8jmwv0r84")))

(define-public crate-mt-debug-counters-0.1.2 (c (n "mt-debug-counters") (v "0.1.2") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1hhvsb7bcmr23jjwfkb0ffxbqpf8dzhz6s9w2h5bkhnpgvza9q6b")))

(define-public crate-mt-debug-counters-0.1.3 (c (n "mt-debug-counters") (v "0.1.3") (d (list (d (n "bytesize") (r "^1.2.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1vszzadqdvgz2kr4mpwly6vs113s5gvvpryr20f0amsr9a057c0v")))

