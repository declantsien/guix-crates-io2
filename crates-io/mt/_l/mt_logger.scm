(define-module (crates-io mt _l mt_logger) #:use-module (crates-io))

(define-public crate-mt_logger-1.0.2 (c (n "mt_logger") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "19w0lnbda8rb4yr8mfzr14hh7pjpaak2d4y8pmbp0wh4ksdwnafp")))

(define-public crate-mt_logger-2.0.0 (c (n "mt_logger") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "0nxzw7kfdms3ajp0d5ins7682v5fsf2a424iyxmh8y7q42pk3m54") (y #t)))

(define-public crate-mt_logger-2.0.1 (c (n "mt_logger") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "0qrr7crkq2bnskkq0nqx0kzsmw6pji4xgpp76nxdbxfzl0avhj57") (y #t)))

(define-public crate-mt_logger-2.0.2 (c (n "mt_logger") (v "2.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "03hs4sqik8cpl4ln8n9p2n71m39c7ybaskcn14cvha3x8mqf2x9c") (y #t)))

(define-public crate-mt_logger-2.1.0 (c (n "mt_logger") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "0w27ds3v8knmd1pz4qc4mihq7yix09c4f5rryh2dhcyvyk7az022") (y #t)))

(define-public crate-mt_logger-2.1.1 (c (n "mt_logger") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "11x8dsjkrd8xcxn8iqyk9yjd4yax4mdwr013bsc359anb1z7zqpx") (y #t)))

(define-public crate-mt_logger-2.2.0 (c (n "mt_logger") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "1jkpamwdj1bp7lipm5fvkbsz3p4gyrskspjqc9fi4k935w6sh30l") (y #t)))

(define-public crate-mt_logger-2.2.1 (c (n "mt_logger") (v "2.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "1lxwhrmcqcc7846s6cncanfi6jb369lf1w7sij1137bh6cvjgn5q") (y #t)))

(define-public crate-mt_logger-3.0.0 (c (n "mt_logger") (v "3.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "0j6ggy23rm2rbxx2c3262a8c90xjl717nm0kp08l0n6lyjc93h7h") (y #t)))

(define-public crate-mt_logger-3.0.1 (c (n "mt_logger") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "0mivrlp8y05c58gfkwdx71yf5l84jyw0zci9clcfnsysv216p4wb") (y #t)))

(define-public crate-mt_logger-3.0.2 (c (n "mt_logger") (v "3.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)))) (h "14dwb528fnhp9yh51qj9i8hb3szfqdg29y1cp02b4w724cs8s9ic")))

