(define-module (crates-io mt rx mtrx-rs) #:use-module (crates-io))

(define-public crate-mtrx-rs-0.1.0 (c (n "mtrx-rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "matrix-sdk") (r "^0.5.0") (f (quote ("e2e-encryption" "sled" "markdown"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "00zqmn6c9xnp6i9fpdh47jna9461miak2dlp1zkx5lbqmjh28xnv")))

