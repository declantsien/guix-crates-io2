(define-module (crates-io mt g_ mtg_color) #:use-module (crates-io))

(define-public crate-mtg_color-1.0.0 (c (n "mtg_color") (v "1.0.0") (h "143z5fsr5a88s6lpr3xczl2i8isrrfdsyl6znr5mq0csqp4l9w9h")))

(define-public crate-mtg_color-1.0.1 (c (n "mtg_color") (v "1.0.1") (h "17yjd2fmf9lgdgrs1r0yl1r13qwg6x47mh69lkxkpm7gw44bacxp")))

(define-public crate-mtg_color-1.1.0 (c (n "mtg_color") (v "1.1.0") (h "18xj13yfdp7fk7h4m9qm4q2iir05mfzbsvm7vry7xsym3wnyzrzv")))

