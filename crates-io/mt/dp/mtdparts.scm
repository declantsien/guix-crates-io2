(define-module (crates-io mt dp mtdparts) #:use-module (crates-io))

(define-public crate-mtdparts-0.1.0 (c (n "mtdparts") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1is8q4lj3f7m8ra0affh6lqpd5r1ba190n54q8f37kbr5vg2pwwj")))

(define-public crate-mtdparts-0.1.1 (c (n "mtdparts") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1dshv873sm16pzc2kdfkz19kyhxspnjx6grnzkmaljqicn584q1f")))

(define-public crate-mtdparts-0.2.0 (c (n "mtdparts") (v "0.2.0") (h "0s1x6368xzvkn35yis3dbmpm7v8glrznd6wsymc8br20f6v9lhpq")))

