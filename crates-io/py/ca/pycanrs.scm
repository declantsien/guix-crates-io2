(define-module (crates-io py ca pycanrs) #:use-module (crates-io))

(define-public crate-pycanrs-0.1.0 (c (n "pycanrs") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.18.1") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 2)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)))) (h "01aq29bv1mfgl7hxgdyj9z91yhpz9px3bnzwnr5mv1xmyx25jlc1")))

