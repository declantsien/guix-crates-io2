(define-module (crates-io py ca pycall) #:use-module (crates-io))

(define-public crate-pycall-0.1.0 (c (n "pycall") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "18nxadlixb8md58dpxfi0fv34x8mjny6nlxqb54xq94y8zxxwkg2")))

(define-public crate-pycall-0.2.0 (c (n "pycall") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "00p7a7p86ybk55r18lvg36p7wxi3jr96fvjfgismhyq16z986113")))

(define-public crate-pycall-0.2.1 (c (n "pycall") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0iblqy5ixk75mfk6z2ca12x5qnjpfwy2h57r0w5nzxxplqdkwgy6")))

(define-public crate-pycall-0.2.2 (c (n "pycall") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0l569sgkh6jjsh0wg92kpw11f7m094nj2y5dci10mmm3vfj7z2hc")))

(define-public crate-pycall-0.2.3 (c (n "pycall") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "12fv1lna8mp8ppslyw4v324sv0sj5f4mqqbp2liwrsf6fl7x5lzp")))

(define-public crate-pycall-0.3.0 (c (n "pycall") (v "0.3.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1n31ivgnkf30pa55xwwr0ikc0sdhk0vxhw9glgpx8980z8i3j090")))

