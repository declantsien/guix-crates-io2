(define-module (crates-io py wa pywavers) #:use-module (crates-io))

(define-public crate-pywavers-0.1.0 (c (n "pywavers") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.18.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "wavers") (r "^0.1.0") (d #t) (k 0)))) (h "072a699y2lcb2kpzdbcp6jna51ghh17bb1kj4vpx2fd4r2h3iwhn")))

(define-public crate-pywavers-0.1.1 (c (n "pywavers") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "numpy") (r "^0.18.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "wavers") (r "^0.1.1") (d #t) (k 0)))) (h "1mi096czlr2i7igris7wl4gd1i7qalsmnfqigrdnqjjahdkjamyl")))

