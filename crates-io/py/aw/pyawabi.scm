(define-module (crates-io py aw pyawabi) #:use-module (crates-io))

(define-public crate-pyawabi-0.1.1 (c (n "pyawabi") (v "0.1.1") (d (list (d (n "awabi") (r "^0.2.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "16hzjv07wvladi4ax5i9kp3snyz8ddhwrn7yn1fhql6p8pw2ia81")))

(define-public crate-pyawabi-0.2.0 (c (n "pyawabi") (v "0.2.0") (d (list (d (n "awabi") (r "^0.2.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "106jk6gvf8f3wan92bx59rfdsy8abn9r2fglf5ln55y164qx8sbl")))

(define-public crate-pyawabi-0.2.2 (c (n "pyawabi") (v "0.2.2") (d (list (d (n "awabi") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1f8sf6vj85kp6lv2wjzj786jq2p17l4k5nms46fdq4xrb71ybz05")))

