(define-module (crates-io py lu pyluwen) #:use-module (crates-io))

(define-public crate-pyluwen-0.5.2 (c (n "pyluwen") (v "0.5.2") (d (list (d (n "luwen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "luwen-if") (r "^0.4.4") (d #t) (k 0)) (d (n "luwen-ref") (r "^0.3.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("extension-module" "multiple-pymethods"))) (d #t) (k 0)) (d (n "ttkmd-if") (r "^0.1.0") (d #t) (k 0)))) (h "0xfssayfk9gp2l7sr85hxs1nzciz57xdh4s2s20vi518xy6mx1gj")))

