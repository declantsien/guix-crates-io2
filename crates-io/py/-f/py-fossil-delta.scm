(define-module (crates-io py -f py-fossil-delta) #:use-module (crates-io))

(define-public crate-py-fossil-delta-0.1.0 (c (n "py-fossil-delta") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.5.2") (d #t) (k 1)) (d (n "fossil-delta") (r "^0.1.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "16gp1nxhlkw70kmvsccrpxqxmwfmrzc9qp2iacn1jbs8qhdndikg")))

(define-public crate-py-fossil-delta-0.1.1 (c (n "py-fossil-delta") (v "0.1.1") (d (list (d (n "cbindgen") (r "^0.5.2") (d #t) (k 1)) (d (n "fossil-delta") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1c3lbb4mh1zmcgz3ck04fsl7z4pxzdwi5hyhvzryzkzaya0hzhv0")))

(define-public crate-py-fossil-delta-0.1.2 (c (n "py-fossil-delta") (v "0.1.2") (d (list (d (n "cbindgen") (r "^0.5.2") (d #t) (k 1)) (d (n "fossil-delta") (r "^0.1.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0nvc7nn6zhy5knnmsvnghimz3pjs5dkxbnmap7kkvqfmyvh2ammg")))

(define-public crate-py-fossil-delta-0.1.3 (c (n "py-fossil-delta") (v "0.1.3") (d (list (d (n "fossil-delta") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "cbindgen") (r "^0.5.2") (d #t) (k 1)))) (h "0pl8f75rmzqdcrkqiw1xxgrgknmsn8bj0y4wdxwcv9n9dpgknc6v")))

