(define-module (crates-io py wr pywrapper-macro) #:use-module (crates-io))

(define-public crate-pywrapper-macro-0.1.2 (c (n "pywrapper-macro") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0jd6q8k3k2dr6mz9hqwpifrwdcl6wcimwb8r2b48c8nsgli4lsad")))

(define-public crate-pywrapper-macro-0.1.3 (c (n "pywrapper-macro") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "01yhd0qkrwb34b5w2iymw12f7mh8a7pgmkf5h7qci38yxxg8wir5")))

(define-public crate-pywrapper-macro-0.2.0 (c (n "pywrapper-macro") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1mj834nqz5cvc5mz0bxfs92pa4x98fj49wfmd7q2i25y8k33azh7")))

(define-public crate-pywrapper-macro-0.3.0 (c (n "pywrapper-macro") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "02qjdzmhicr0a6hzdwfy3nrxfhbzgyl2pk4f4glh2a02csrxpkcd")))

(define-public crate-pywrapper-macro-0.3.1 (c (n "pywrapper-macro") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0jcy30ikfwk02y1i72yd6sld0imk895phnmvbr5l2m26r5wy8gl7")))

