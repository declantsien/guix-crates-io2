(define-module (crates-io py #{27}# py27-marshal) #:use-module (crates-io))

(define-public crate-py27-marshal-0.3.4 (c (n "py27-marshal") (v "0.3.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12i97738a08mxylkw7nbnxszykwh84d1kf9inldy9z6sr8qs99mp") (f (quote (("python27") ("default"))))))

(define-public crate-py27-marshal-0.4.0 (c (n "py27-marshal") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "bstr") (r "^1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jy8slv9w33vv44cl1zfwm9a0r7s2mzdc2lbxj6pvg3wl4jmspc6") (f (quote (("default"))))))

