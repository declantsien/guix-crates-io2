(define-module (crates-io py -c py-comp) #:use-module (crates-io))

(define-public crate-py-comp-0.1.0 (c (n "py-comp") (v "0.1.0") (h "0m3x7kmmbvnqa281rqhajaw1a1rw74jd6p9jvkn4lgdifimx5p98")))

(define-public crate-py-comp-0.1.1 (c (n "py-comp") (v "0.1.1") (h "05gicj5g7c47smljgsgy35kr85ywhkwbx52qxakxnzxha0q3p11q")))

(define-public crate-py-comp-0.1.2 (c (n "py-comp") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 0)))) (h "1xsx744gkyhsafh57y48fgd9bzk8jhy45zkb3b2lgpwnz6ricsj8")))

(define-public crate-py-comp-0.1.3 (c (n "py-comp") (v "0.1.3") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 0)))) (h "0acpnd3cvhs38ri0m48whl2j5h6f1m0nn97nqn4z6gksbdj41w0s")))

