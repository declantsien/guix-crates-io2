(define-module (crates-io py ru pyruvate) #:use-module (crates-io))

(define-public crate-pyruvate-0.1.0 (c (n "pyruvate") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "glucose") (r "^0.1.4") (d #t) (k 0)) (d (n "iced") (r "^0.2.0") (d #t) (k 0)) (d (n "iced_web") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "10akfx5w89jb1d7wwy8gbyvbklac35dc17nmr3dvr36mxpky2gmz")))

(define-public crate-pyruvate-0.1.1 (c (n "pyruvate") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "glucose") (r "^0.1.8") (d #t) (k 0)) (d (n "iced") (r "^0.2.0") (d #t) (k 0)) (d (n "iced_web") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0g3az56d6h366s8q652sck4d0gpdrhc41ah7i1498fwng3l9sjas")))

(define-public crate-pyruvate-0.1.2 (c (n "pyruvate") (v "0.1.2") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "glucose") (r "^0.1.8") (d #t) (k 0)) (d (n "iced") (r "^0.2.0") (d #t) (k 0)) (d (n "iced_web") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "02ia4hvd3a3bggy0dlj3kj7556rlr66ncb98jphx1l249dmzqamj")))

(define-public crate-pyruvate-0.1.3 (c (n "pyruvate") (v "0.1.3") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "glucose") (r "^0.1.8") (d #t) (k 0)) (d (n "iced") (r "^0.2.0") (d #t) (k 0)) (d (n "iced_web") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0cgblv5wqcbhi3dhanlsg0i2r9gcxcqcdqm430jn37aggkbzmyhw")))

(define-public crate-pyruvate-0.1.4 (c (n "pyruvate") (v "0.1.4") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "glucose") (r "^0.1.8") (d #t) (k 0)) (d (n "iced") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)))) (h "0bgwbgw6prlwqlaanf40flhzpqdhjm9g6h8qgq5z2mnrsnnjigh9")))

