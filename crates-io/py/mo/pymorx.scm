(define-module (crates-io py mo pymorx) #:use-module (crates-io))

(define-public crate-pymorx-0.1.0 (c (n "pymorx") (v "0.1.0") (d (list (d (n "morx") (r "^0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "numpy") (r "^0.16") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "12ma5pbsmk9vb0gs3lnlws2b09gsfnyrzcv8dj05mnj2ka9x7y2g")))

