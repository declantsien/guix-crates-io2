(define-module (crates-io py to pytorch-cpuinfo) #:use-module (crates-io))

(define-public crate-pytorch-cpuinfo-0.1.0 (c (n "pytorch-cpuinfo") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0rx5yx3qr8x6g8zvbkp8g2f41w64hqfn9k4j0sq2q9x42hm8h4nc") (y #t)))

(define-public crate-pytorch-cpuinfo-0.1.1 (c (n "pytorch-cpuinfo") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "07mhb6y863bnzfqxy3d8sflpmy8pfcmsyjbmh6h8npws4x8c4q43")))

(define-public crate-pytorch-cpuinfo-0.1.2 (c (n "pytorch-cpuinfo") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "0avm252wdqi77577pf5lzncaxr4jnnr5i1kz5vaa53rb9jq094ba")))

