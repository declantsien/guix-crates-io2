(define-module (crates-io py to pytorch-cpuinfo-pieces) #:use-module (crates-io))

(define-public crate-pytorch-cpuinfo-pieces-0.0.1 (c (n "pytorch-cpuinfo-pieces") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)))) (h "1sa8dsljan8hi4k6n0fhzz0lk6q57xwv5vk9f1h52qq47x207md2")))

