(define-module (crates-io py to pytools-rs) #:use-module (crates-io))

(define-public crate-pytools-rs-0.1.0 (c (n "pytools-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "10jzpaza0hr7dx9avg2j703plc068a030x0z1y13j1ms0bgryblq")))

(define-public crate-pytools-rs-0.1.1 (c (n "pytools-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "107lrirzgxvagpyy878z2kv9cbjasszfd16wi6bmqvcfjfrzxsrp")))

