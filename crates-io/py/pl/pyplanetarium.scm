(define-module (crates-io py pl pyplanetarium) #:use-module (crates-io))

(define-public crate-pyplanetarium-0.1.0 (c (n "pyplanetarium") (v "0.1.0") (d (list (d (n "planetarium") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)))) (h "0db7wp6jx9k6laf0xn1zb55211bxxa81cadbzd0kcdir339rngbb")))

(define-public crate-pyplanetarium-0.1.1 (c (n "pyplanetarium") (v "0.1.1") (d (list (d (n "planetarium") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module" "abi3-py37"))) (d #t) (k 0)))) (h "15kv9cl5piafkxnnk2rxldjl4q7zhc3dgval5120j2ma22p2x163")))

(define-public crate-pyplanetarium-0.1.5 (c (n "pyplanetarium") (v "0.1.5") (d (list (d (n "planetarium") (r "^0.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.4") (f (quote ("extension-module" "abi3-py37" "generate-abi3-import-lib"))) (d #t) (k 0)))) (h "0mmvrn7k58qy57agnb4a0vdn8bsxkisdlc303vczd6siybh55i8w")))

(define-public crate-pyplanetarium-0.1.6 (c (n "pyplanetarium") (v "0.1.6") (d (list (d (n "planetarium") (r "^0.1.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (d #t) (k 0)))) (h "00nnjrp5cmfg3d197nf59g3awww2hsd5h5hfqdcrr3045nba4zca")))

(define-public crate-pyplanetarium-0.2.0 (c (n "pyplanetarium") (v "0.2.0") (d (list (d (n "planetarium") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (d #t) (k 0)))) (h "0pjmizkwrcl4b4bg395h6lgqc9rwyfvgdzq6wj7li3w50rbvrm78")))

(define-public crate-pyplanetarium-0.2.1 (c (n "pyplanetarium") (v "0.2.1") (d (list (d (n "planetarium") (r "^0.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.18") (f (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (d #t) (k 0)))) (h "1in7pf8z3gf52gjx0wp9w9pcibiizfb9yzqj1kzwxmhrybdazpgk")))

