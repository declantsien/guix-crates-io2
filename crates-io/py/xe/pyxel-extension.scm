(define-module (crates-io py xe pyxel-extension) #:use-module (crates-io))

(define-public crate-pyxel-extension-1.8.2 (c (n "pyxel-extension") (v "1.8.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "173nn7pjmsk6nb2z2gc6646cj02xiraqzf541314gjlak9zshpxw")))

(define-public crate-pyxel-extension-1.8.3 (c (n "pyxel-extension") (v "1.8.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1m0j8qd28mws817xffkgn2qwyid8xdvc8hqmyxpjm7wfikzc4rkf")))

(define-public crate-pyxel-extension-1.8.4 (c (n "pyxel-extension") (v "1.8.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0ar54y0d05pdplm6vva2pk4gzpnh6mpg2fnl6f94wis7xjx87gb6")))

(define-public crate-pyxel-extension-1.8.5 (c (n "pyxel-extension") (v "1.8.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0z0098d8acgv7896csfash2bhgs6pck4ziiwk65k0as465mwq0xh")))

(define-public crate-pyxel-extension-1.8.6 (c (n "pyxel-extension") (v "1.8.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0jirxfaddx64zh8smx9cvdlc2x7dfrk6mflwm2nlxdxr6hsl825i")))

(define-public crate-pyxel-extension-1.8.7 (c (n "pyxel-extension") (v "1.8.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0mv2wqin3a2hgn0x71sr6h84g1wzqpfbgp1qng7fyydsh6ppaway")))

(define-public crate-pyxel-extension-1.8.8 (c (n "pyxel-extension") (v "1.8.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1albvaz7sh78v396psvffsjjbprk0kgf7plzxkh300havz5zm6nk")))

(define-public crate-pyxel-extension-1.8.9 (c (n "pyxel-extension") (v "1.8.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1bfvf0i4cqvp7f8r1w00raz9v5hmrpkwiigw9pny4ynfvv3fxrxc")))

(define-public crate-pyxel-extension-1.8.10 (c (n "pyxel-extension") (v "1.8.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0a2j4a26k777hml25150pa9rxg4dnjy5563hh4xz70kl2dgmn3mj")))

(define-public crate-pyxel-extension-1.8.11 (c (n "pyxel-extension") (v "1.8.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.11") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1bmlxfggimdhrsrccmypylxfqa0dqhigga15qbk889p33gx4y3zl")))

(define-public crate-pyxel-extension-1.8.12 (c (n "pyxel-extension") (v "1.8.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "00s51lzlsqwmygf6mnwlm3lx6zqlnnvj9255fz6m1w3i1j3rhsbk")))

(define-public crate-pyxel-extension-1.8.13 (c (n "pyxel-extension") (v "1.8.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.13") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "011hp05jpg1lrkpjqdxa0szkvdhrx4l470042x1b5q9lvfjw7v0y")))

(define-public crate-pyxel-extension-1.8.14 (c (n "pyxel-extension") (v "1.8.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.14") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1nwkip0axn18rlckkzpyyrrmymhd7c3a4n63hk1mv04gs669b3xh")))

(define-public crate-pyxel-extension-1.8.15 (c (n "pyxel-extension") (v "1.8.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.15") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0gxlqmbz3x0y98slwy9sxh8hfvkc3z8k7q1m78i1s2l8rix9cirv")))

(define-public crate-pyxel-extension-1.8.16 (c (n "pyxel-extension") (v "1.8.16") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.16") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0hxspp3m3rih6kzv730kbv5ynli9m5717kvjdk2gi9kn5y2b6hzk")))

(define-public crate-pyxel-extension-1.8.17 (c (n "pyxel-extension") (v "1.8.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.17") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1x3168x8j70li7lkr1j084gl77apgk2qhk7ksrsa2b46yy7ii508")))

(define-public crate-pyxel-extension-1.8.18 (c (n "pyxel-extension") (v "1.8.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.18") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1d6nsqxlc6dd07m1s99lpbjj7j9c0xmha71wc2g6mlazj281wcmd")))

(define-public crate-pyxel-extension-1.8.19 (c (n "pyxel-extension") (v "1.8.19") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.19") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0idsgmcyzyg04jsz5g77z55a8xm86lp0h5ff5jz54f9hijr2aqwz")))

(define-public crate-pyxel-extension-1.8.20 (c (n "pyxel-extension") (v "1.8.20") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.20") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1x8blln75sp4lz9sx0kwhj6hng3n32x9jnsl0yz07p7svi00agdb")))

(define-public crate-pyxel-extension-1.8.21 (c (n "pyxel-extension") (v "1.8.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.21") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "07miwf30b73c2qs7pc7lik7zsp39syfvrb8jcbh7bw4w6h4z2s8y")))

(define-public crate-pyxel-extension-1.8.22 (c (n "pyxel-extension") (v "1.8.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.8.22") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "16498lbgsz6smd9shg2v2pzqyzj5fdc9l3anbcv4jjiki53hnp0g")))

(define-public crate-pyxel-extension-1.9.0 (c (n "pyxel-extension") (v "1.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1k1ycaavg36fhgiy305ns83b3bdszpmslj9c5rc1azg1y04hz7c3")))

(define-public crate-pyxel-extension-1.9.1 (c (n "pyxel-extension") (v "1.9.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0irpis0a7wivh5hpbc6bay6zpxf395g0x29z6wsmcmjibxxj2hjv")))

(define-public crate-pyxel-extension-1.9.2 (c (n "pyxel-extension") (v "1.9.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0gx1f59c2d9xdldy601w620rql8q1ifxxnxn84xvjnbbzm42z4sa")))

(define-public crate-pyxel-extension-1.9.3 (c (n "pyxel-extension") (v "1.9.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1z39xw1snskzii9y16q69ijmka7cjkkbcrxfp0p60kwwvndhr7ws")))

(define-public crate-pyxel-extension-1.9.4 (c (n "pyxel-extension") (v "1.9.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1dcashsg2iiz9iq3xdsihdhmmbi4c1nr3ag2752n14cn362779vr")))

(define-public crate-pyxel-extension-1.9.5 (c (n "pyxel-extension") (v "1.9.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1rz2ixyl5lxvcj0hf091rgp32v4gckx3naajx1c03zgrpa9l03bh")))

(define-public crate-pyxel-extension-1.9.6 (c (n "pyxel-extension") (v "1.9.6") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0zxzlzz4lzl4and6mcx6nvpkkggpm24c0x5lz423xh22ch395iwy")))

(define-public crate-pyxel-extension-1.9.7 (c (n "pyxel-extension") (v "1.9.7") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0yy8d01nmwjs79m1m2gdv29sv6fydjid354mvd50jkqr901kq89l")))

(define-public crate-pyxel-extension-1.9.8 (c (n "pyxel-extension") (v "1.9.8") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "189khi9q82p942829djnmf8i6p60wgl4msw9rgbx4nw13bpnqd9g")))

(define-public crate-pyxel-extension-1.9.9 (c (n "pyxel-extension") (v "1.9.9") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1109z86r6b3nfyyrwfxplaxly75597d685mxzf2lcsli3f2wc7gq")))

(define-public crate-pyxel-extension-1.9.10 (c (n "pyxel-extension") (v "1.9.10") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0c9fyh1ikciz586zn3k40dxra80r350anxfrz2jv4d69k8bmrfav")))

(define-public crate-pyxel-extension-1.9.11 (c (n "pyxel-extension") (v "1.9.11") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.11") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1dz2b9ffsnz7lsi06szhx97xx4c3d8bfrmknac630jl3izwqzasl")))

(define-public crate-pyxel-extension-1.9.12 (c (n "pyxel-extension") (v "1.9.12") (d (list (d (n "pyo3") (r "^0.17") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "097rgz9c3csbpgp5wfx29lw5i2lq6jxxf5klg8zsx57qzr3dqx8a")))

(define-public crate-pyxel-extension-1.9.13 (c (n "pyxel-extension") (v "1.9.13") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.13") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1bg8lsnigan9y77712p434krsbd4d5bpqkj5m52s5jqkhkllrygd")))

(define-public crate-pyxel-extension-1.9.14 (c (n "pyxel-extension") (v "1.9.14") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-core") (r "^1.9.14") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0r94jhxh4ia49cbgm70njsckgwg8siw9psl8vdyg79m2gniyx5zy")))

(define-public crate-pyxel-extension-2.0.0 (c (n "pyxel-extension") (v "2.0.0") (h "0g6ijfcpnk0vkbnphcq86j7d016vsm8p5ybb4c3yn7a7pxgwd3an")))

