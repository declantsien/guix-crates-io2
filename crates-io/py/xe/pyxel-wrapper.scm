(define-module (crates-io py xe pyxel-wrapper) #:use-module (crates-io))

(define-public crate-pyxel-wrapper-1.5.1 (c (n "pyxel-wrapper") (v "1.5.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.1") (d #t) (k 0)))) (h "0w78i0b0d1lw0kypv8wvm70ai9md8cv7cfrqkdgynviixdj9qklg")))

(define-public crate-pyxel-wrapper-1.5.2 (c (n "pyxel-wrapper") (v "1.5.2") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.2") (d #t) (k 0)))) (h "0mpwsqb6h4x2mq2h27mv9zg95xscrr6rq2bv5l2lmjlxsav3as35")))

(define-public crate-pyxel-wrapper-1.5.3 (c (n "pyxel-wrapper") (v "1.5.3") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.3") (d #t) (k 0)))) (h "00x628swb3ak476i15h7lwry6andpsffcr3j984ipmfigssv252g")))

(define-public crate-pyxel-wrapper-1.5.4 (c (n "pyxel-wrapper") (v "1.5.4") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.4") (d #t) (k 0)))) (h "1bspy1zj5xjds43qazh7fqdl6lj705hj0r37775q7rx8a9rqs6q6")))

(define-public crate-pyxel-wrapper-1.5.5 (c (n "pyxel-wrapper") (v "1.5.5") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.5") (d #t) (k 0)))) (h "156hf8fkqjx415xpxj362k44g2lfwwll53d0qdx70skpkmfy3nnl")))

(define-public crate-pyxel-wrapper-1.5.6 (c (n "pyxel-wrapper") (v "1.5.6") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.6") (d #t) (k 0)))) (h "0nj1wdwzjz3yr992k4gj2xdcshav2w9x5npjbk43hgydbjaqg6pa")))

(define-public crate-pyxel-wrapper-1.5.7 (c (n "pyxel-wrapper") (v "1.5.7") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.7") (d #t) (k 0)))) (h "0alk6mrjwyvxhvih1dn3iizicmrscxi6487v3lv7zp1qfcc760si")))

(define-public crate-pyxel-wrapper-1.5.8 (c (n "pyxel-wrapper") (v "1.5.8") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.5.8") (d #t) (k 0)))) (h "1mi0faa9kz1pyhfi7055m0x0gxnradhkr582rlw3agwx508kw8d7")))

(define-public crate-pyxel-wrapper-1.6.0 (c (n "pyxel-wrapper") (v "1.6.0") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.0") (d #t) (k 0)))) (h "0pcy243fcfk55ry4qidd7kqhk26mbcark33rfwbn3h97gbwikd98")))

(define-public crate-pyxel-wrapper-1.6.1 (c (n "pyxel-wrapper") (v "1.6.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.1") (d #t) (k 0)))) (h "02ncvmh0f520la8f5hg0k5ydln7ffz4w78avv2dq42f2kb4vxj1n")))

(define-public crate-pyxel-wrapper-1.6.3 (c (n "pyxel-wrapper") (v "1.6.3") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.3") (d #t) (k 0)))) (h "0dij8izimijw55lm3q33p0js2jidfck2741pf02zwphfijr3jq7y")))

(define-public crate-pyxel-wrapper-1.6.5 (c (n "pyxel-wrapper") (v "1.6.5") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.5") (d #t) (k 0)))) (h "1xw2wwkj0yzxnf1xci56jiklnpp3qdl8ybsn5jjpgv9qiqmr01k8")))

(define-public crate-pyxel-wrapper-1.6.6 (c (n "pyxel-wrapper") (v "1.6.6") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.6") (d #t) (k 0)))) (h "1ir2x4mj8wayf7hyxsqb3ksmx7dn15vlj4llvawsvkckwiczf6ps")))

(define-public crate-pyxel-wrapper-1.6.7 (c (n "pyxel-wrapper") (v "1.6.7") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.7") (d #t) (k 0)))) (h "1glb0zy14pv8bbifgdlkb7fxg4jw84j25wysbkmg5r9xpzayg7h3")))

(define-public crate-pyxel-wrapper-1.6.8 (c (n "pyxel-wrapper") (v "1.6.8") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.8") (d #t) (k 0)))) (h "1nxgpi206lyx564zxjf2rlfldhw58dldcmpqqj88j2nbvp5zdjsh")))

(define-public crate-pyxel-wrapper-1.6.9 (c (n "pyxel-wrapper") (v "1.6.9") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.6.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.0") (d #t) (k 0)))) (h "099zdqfbigddwag6dk7h772knqj6f2dcm3v8ycds9rrilv2yshpg")))

(define-public crate-pyxel-wrapper-1.7.0 (c (n "pyxel-wrapper") (v "1.7.0") (d (list (d (n "pyo3") (r "^0.16.4") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.7.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.11") (d #t) (k 0)))) (h "0y4cr8bmsprcrdyas7zq2vm4203qc4njxvjc966k7v8psfdzg1c6")))

(define-public crate-pyxel-wrapper-1.7.1 (c (n "pyxel-wrapper") (v "1.7.1") (d (list (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.7.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "0lm3ffa1n4fickg2djc9ilcfhlm9lizchf42y5wlf88vipd8wlc3")))

(define-public crate-pyxel-wrapper-1.7.2 (c (n "pyxel-wrapper") (v "1.7.2") (d (list (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.7.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "0wi8xwq6yl2hgyz7gnnqxnd3k5gr3gyz17x20q10kg1c6fhfw7bw")))

(define-public crate-pyxel-wrapper-1.8.0 (c (n "pyxel-wrapper") (v "1.8.0") (d (list (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.8.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1m9kcq7yin9nlgfsaqhd0m08mppb9wmlwz6i9mncmq2fvxbn620g")))

(define-public crate-pyxel-wrapper-1.8.1 (c (n "pyxel-wrapper") (v "1.8.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.8.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "02mlljbjydlv02k5q01qf8hykqfchfwqm469mws9nwcwapy7l4gn")))

(define-public crate-pyxel-wrapper-1.8.2 (c (n "pyxel-wrapper") (v "1.8.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pyo3") (r "^0.16") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^1.8.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1cyfngv51l1dxik9bmlmnypm1bx139hl7n80pgj2x6dv9035xkrh")))

(define-public crate-pyxel-wrapper-2.0.0 (c (n "pyxel-wrapper") (v "2.0.0") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0grjsprhr55yhlhjy4mv42vs23b13w9kjrq0lgbwjliq7mw6viy8")))

(define-public crate-pyxel-wrapper-2.0.1 (c (n "pyxel-wrapper") (v "2.0.1") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "161yyv8pabvhia0z0pmrrak9ic6bxgq3cdnm4h1xg0wccrla2nly")))

(define-public crate-pyxel-wrapper-2.0.2 (c (n "pyxel-wrapper") (v "2.0.2") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.2") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1gc1ai6wxckhvgsldmvj9rd9ny7wgkl03wrxqipar30dv4gfa80a")))

(define-public crate-pyxel-wrapper-2.0.3 (c (n "pyxel-wrapper") (v "2.0.3") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.3") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "048qrkkjbw61g19q5cbx6vp1afvyy6zjxqzvrq45410zrixk3wgn")))

(define-public crate-pyxel-wrapper-2.0.4 (c (n "pyxel-wrapper") (v "2.0.4") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0nv3ibqj608gb9fhc0y1cp43pnipinfy68vd8yb3lx71hm0k5r0r")))

(define-public crate-pyxel-wrapper-2.0.5 (c (n "pyxel-wrapper") (v "2.0.5") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.5") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0d941pbf0yfgc9nga5cdq38sn2z9q9n93f66pi7qbyk4vzn44kwb")))

(define-public crate-pyxel-wrapper-2.0.6 (c (n "pyxel-wrapper") (v "2.0.6") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0z24wvdwba320n50wjw2r05j4z744v20k2a6d2qmbf065qglf98r")))

(define-public crate-pyxel-wrapper-2.0.7 (c (n "pyxel-wrapper") (v "2.0.7") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.7") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "14x9gmxhrhjs4m2cb5gkh5bhqcqpfr17i62lwbcnrskrxwlkph0d")))

(define-public crate-pyxel-wrapper-2.0.8 (c (n "pyxel-wrapper") (v "2.0.8") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0gjnrldplbsl41gc2cdvlv1m2v0bickzc3nf0n9g3zng0zabsyv3")))

(define-public crate-pyxel-wrapper-2.0.9 (c (n "pyxel-wrapper") (v "2.0.9") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "0g60mnpb54n2j294q9r2rk4yys50mzdhhk66g14nq364gs8wl476")))

(define-public crate-pyxel-wrapper-2.0.10 (c (n "pyxel-wrapper") (v "2.0.10") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "18ll1s13yi71kspb2gjyyqhmykwar2l3p2rh060k3qxh1mz1f148")))

(define-public crate-pyxel-wrapper-2.0.11 (c (n "pyxel-wrapper") (v "2.0.11") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("abi3-py37" "extension-module" "gil-refs"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.11") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1gkdyhd304iqlzqach4gg5a136jqbj3cf7pmkniafnyqs4qqy8hz")))

(define-public crate-pyxel-wrapper-2.0.12 (c (n "pyxel-wrapper") (v "2.0.12") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("abi3-py37" "extension-module" "gil-refs"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.12") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1giv0dglpi4028jj9k3ry6p0bxw5fbmqdypbv3iaxyqqa7jcfmf4")))

(define-public crate-pyxel-wrapper-2.0.13 (c (n "pyxel-wrapper") (v "2.0.13") (d (list (d (n "pyo3") (r "^0.21") (f (quote ("abi3-py37" "extension-module" "gil-refs"))) (d #t) (k 0)) (d (n "pyxel-engine") (r "^2.0.13") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)))) (h "1bhc39d30f0f89xvlbv3z18hbgk4czkps49q34slxj9b40x20181")))

