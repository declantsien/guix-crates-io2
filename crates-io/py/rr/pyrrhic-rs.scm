(define-module (crates-io py rr pyrrhic-rs) #:use-module (crates-io))

(define-public crate-pyrrhic-rs-0.1.0 (c (n "pyrrhic-rs") (v "0.1.0") (d (list (d (n "cozy-chess") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)))) (h "12ibccf6s41sq8h52irilxhi2fgfyzb1dvprxshaf54i2p5id7kj")))

(define-public crate-pyrrhic-rs-0.2.0 (c (n "pyrrhic-rs") (v "0.2.0") (d (list (d (n "cozy-chess") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)))) (h "16g2lhs1m7cxvaldid4gaifa2mp4h9yv8q8fzdshsvmq7c2hz4g3")))

