(define-module (crates-io py go pygo) #:use-module (crates-io))

(define-public crate-pygo-0.0.1 (c (n "pygo") (v "0.0.1") (d (list (d (n "clap") (r "~2.33.3") (f (quote ("suggestions" "color"))) (k 0)))) (h "1pk3fka38gc34m83cy5x8gqjg37f595jh86km8dd0bw98bjmyqkf")))

(define-public crate-pygo-0.1.0 (c (n "pygo") (v "0.1.0") (d (list (d (n "clap") (r "~2.33.3") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0i9khmnwb1p8f4l23ld8zmyca0xvhhm9czs801xmh4ga5988aggz")))

(define-public crate-pygo-0.1.1 (c (n "pygo") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "~2.33.3") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "084xz8yky9y3mqjdivqxzxbb4m662m4dc7xapl167m3w48h4ak4y")))

