(define-module (crates-io py po pyportus) #:use-module (crates-io))

(define-public crate-pyportus-0.4.0 (c (n "pyportus") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "portus") (r "^0.4.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.2.7") (f (quote ("extension-module"))) (k 0)) (d (n "simple-signal") (r "^1") (d #t) (k 0)) (d (n "slog") (r "^2") (d #t) (k 0)) (d (n "slog-async") (r "^2") (d #t) (k 0)) (d (n "slog-term") (r "^2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y5a9hj3a4ya7iwgag2f87y6fykfiym1zqxihqqlv842pbh2g1gk") (f (quote (("bench"))))))

(define-public crate-pyportus-0.5.0 (c (n "pyportus") (v "0.5.0") (d (list (d (n "portus") (r "^0.6") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("extension-module" "macros"))) (k 0)) (d (n "simple-signal") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "090v4n698q4bakgwnsnik1pf5sx39srz806sq92135zcj45w0n1h") (f (quote (("bench"))))))

(define-public crate-pyportus-0.6.0 (c (n "pyportus") (v "0.6.0") (d (list (d (n "portus") (r "^0.7") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (f (quote ("extension-module" "macros"))) (k 0)) (d (n "simple-signal") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "1bkby4ps1igm7ac6hj8285s8mjfv3947j5hkngxp9dz6hm0kmbcm") (f (quote (("bench"))))))

