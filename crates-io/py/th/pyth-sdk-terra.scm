(define-module (crates-io py th pyth-sdk-terra) #:use-module (crates-io))

(define-public crate-pyth-sdk-terra-0.1.0 (c (n "pyth-sdk-terra") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x963r1gvh0yzh69xzlhnhb8nqaf978div2b1mib5h0nklhpf7p4")))

(define-public crate-pyth-sdk-terra-0.2.0 (c (n "pyth-sdk-terra") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mq63sdakrsb4hvcbmnjf17ngdnjgk7nnmabs41ixrpbk2b7ym79")))

(define-public crate-pyth-sdk-terra-0.3.0 (c (n "pyth-sdk-terra") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pddvlvww7fbskaw01z1qaxfmy5zk5rl6p3bzcr8k792ynqqlfb3")))

(define-public crate-pyth-sdk-terra-0.3.1 (c (n "pyth-sdk-terra") (v "0.3.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.4.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "11zdpfk4zaa7xnrfhaj1hcpcn20zszisphqs43iw4z9cp8i4ix0j")))

(define-public crate-pyth-sdk-terra-0.4.0 (c (n "pyth-sdk-terra") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.4.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0941qrx6vgkd2h70yqs7dhcdl1gpsq0xdlw3br5h88wajwacsz6b")))

