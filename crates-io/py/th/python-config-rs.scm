(define-module (crates-io py th python-config-rs) #:use-module (crates-io))

(define-public crate-python-config-rs-0.1.0 (c (n "python-config-rs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "17bw73a0k794pfslnpg993rq6sdaxw16p1ndl773fm7ah5azzzxp")))

(define-public crate-python-config-rs-0.1.1 (c (n "python-config-rs") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "1gzbq0q856yx5hya05x35jalk7ahsf20fl1j7sjkvl0s5d9l539m")))

(define-public crate-python-config-rs-0.1.2 (c (n "python-config-rs") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "114l3d38hciynfaz83n6a8wm6sszs9b19j2nfb9fcdhklmlnjcr4")))

