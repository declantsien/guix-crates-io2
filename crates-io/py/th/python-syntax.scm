(define-module (crates-io py th python-syntax) #:use-module (crates-io))

(define-public crate-python-syntax-0.1.0 (c (n "python-syntax") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.17.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "unicode_names2") (r "^0.2.2") (d #t) (k 0)))) (h "072nhhsqfcn6qdra0767v5ij7jr82szz93apdi09l1gcrdaf34gx")))

