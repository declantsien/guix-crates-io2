(define-module (crates-io py th pythagore-wasm) #:use-module (crates-io))

(define-public crate-pythagore-wasm-0.1.1 (c (n "pythagore-wasm") (v "0.1.1") (d (list (d (n "pythagore") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.36") (d #t) (k 2)))) (h "1kmabyyi1shgmg048jjqnxybxgszp9pnfmly34wskjxya49w0pv6")))

