(define-module (crates-io py th python3-config) #:use-module (crates-io))

(define-public crate-python3-config-0.1.0 (c (n "python3-config") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.2") (d #t) (k 0)))) (h "1g76l06w2ks1sgzyg6rzh0l221dbh1yvahrgzxfdvk8wvq0kx2j4")))

(define-public crate-python3-config-0.2.0 (c (n "python3-config") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.2") (d #t) (k 0)))) (h "06rh69l7n95qv2dqgl8njfgnhr1wn3qcqd8icfwmfz85fqdyylkc")))

(define-public crate-python3-config-0.2.1 (c (n "python3-config") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rustpython-parser") (r "^0.1.2") (d #t) (k 0)))) (h "1rbf5lz2r9nwlixwpiiq63i69ylb6w5fgvj5pypxs55g9qwgcprm")))

