(define-module (crates-io py th pyth) #:use-module (crates-io))

(define-public crate-pyth-0.1.0 (c (n "pyth") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.11.1") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4.0") (d #t) (k 0)))) (h "1ih80xw605r8w6q9mdrfdyzvjrps9zdnhkw36mvajsvw4qlicg2s") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

