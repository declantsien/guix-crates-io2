(define-module (crates-io py th python_rub) #:use-module (crates-io))

(define-public crate-python_rub-0.0.1 (c (n "python_rub") (v "0.0.1") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "0xzja61hnldsisf6inq6hii585f6lp73x9dnj0a86k99iz2cd9rp")))

(define-public crate-python_rub-0.0.2 (c (n "python_rub") (v "0.0.2") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "059y4zi1w5jbwrnli3bbfja6md9w66avkia8dpcb4a8r4r6n678v")))

(define-public crate-python_rub-0.0.3 (c (n "python_rub") (v "0.0.3") (d (list (d (n "buildable") (r "*") (d #t) (k 0)) (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "scm") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)) (d (n "utils") (r "*") (d #t) (k 0)))) (h "1s9gynhcsnp6s6hqxlm2a4k8vcqa97j2jmc26iwb5vb9wg1vf9vp")))

