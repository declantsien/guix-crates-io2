(define-module (crates-io py th python-core) #:use-module (crates-io))

(define-public crate-python-core-0.4.4 (c (n "python-core") (v "0.4.4") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module" "macros"))) (d #t) (k 0)) (d (n "wcore") (r "^0.4.4") (d #t) (k 0)))) (h "0n9qfr8vrb01iyq8kjjaq1717g3dixz0lwa8pdvvfg2d6145ylq0")))

(define-public crate-python-core-0.5.1 (c (n "python-core") (v "0.5.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module" "macros"))) (d #t) (k 0)) (d (n "wcore") (r "^0.5.1") (d #t) (k 0)))) (h "0nkk3sv79fwpznr0wj5asj1xf5gdmpsy5hlvz7wwjd8zyynf52yv")))

