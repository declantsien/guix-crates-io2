(define-module (crates-io py th python3-sys) #:use-module (crates-io))

(define-public crate-python3-sys-0.1.0 (c (n "python3-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r4i0pabg8gzhfvjnmm4gblcyq7cwljc301gmk2z6vxc4hlv0l5p") (f (quote (("python3_4" "python3_3") ("python3_3") ("default" "WITH_THREAD") ("WITH_THREAD") ("Py_TRACE_REFS"))))))

(define-public crate-python3-sys-0.1.1 (c (n "python3-sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "regex") (r "^0.1.8") (d #t) (k 1)))) (h "16ly38bkl5vl8wmlxq0bky127l3cwyn6g8r7b5kncr60yq375qar") (f (quote (("python-3-5") ("python-3-4") ("python-3") ("default" "python-3"))))))

(define-public crate-python3-sys-0.1.2 (c (n "python3-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "03q7sm6v73g9nks3iq2y7baq6ss8l07bpgyy9jwxqdf3wkq2wqbw") (f (quote (("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("default" "python-3"))))))

(define-public crate-python3-sys-0.1.3 (c (n "python3-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "16pkbxjjkd8lph4nrk3nx1m1gwpmy4m2qkb1zk6c55kh6aw2f75c") (f (quote (("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("extension-module") ("default" "python-3"))))))

(define-public crate-python3-sys-0.2.0 (c (n "python3-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 1)))) (h "0zwn832jvx6q7nk0adpp8mhxqq7qybkxvnkw2hs5ac3rcagijw1s") (f (quote (("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("extension-module") ("default" "python-3"))))))

(define-public crate-python3-sys-0.2.1 (c (n "python3-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0i04vizrnr33l6m2x3xnqirgczcxmjr6ql19whvxcgw37z2amr31") (f (quote (("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.3.0 (c (n "python3-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1snqczrpi0kq0g5szwp9fn6cs45wvc4hd5pkg6hzs08j7znypa7k") (f (quote (("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.4.0 (c (n "python3-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1lsgy8xvr912ggjl8i6amnd95r4pb76kxzjhmrxxgf8dzw7mb5nj") (f (quote (("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.4.1 (c (n "python3-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1m3ngjan71bj383kq98f7g9pkb2r7ckjsy30mwqfb88mjmvi3bwh") (f (quote (("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.5.0 (c (n "python3-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "01rhpxm86nsvyyaslzm12gfn8j79pfgppsvs42ilv9qgbshxr3cn") (f (quote (("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.5.1 (c (n "python3-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "16nhgl242gy6703ix5gvr42ammh34pw01v2jwdmc65bvmrads8yg") (f (quote (("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.5.2 (c (n "python3-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1nq6m1cx1rfk8w1yif29q7ph5pxwr0jx0vmznfz05sv8d2fbjaav") (f (quote (("python-3-9") ("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.6.0 (c (n "python3-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0sivixk6mq9sjkp81xy2vrvz18zhxywwknn3gyj52jar54dz52mp") (f (quote (("python-3-9") ("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.7.0 (c (n "python3-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0zmprvnajh1jczg6y7sjl3vkclynvpfxfi26yi2m0g8h9kk352xi") (f (quote (("python-3-9") ("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3-10") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

(define-public crate-python3-sys-0.7.1 (c (n "python3-sys") (v "0.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0lmb38pmzglqwx7f5spy7c4jrmxv36zyw0x4b9riac7vf86vby29") (f (quote (("python-3-9") ("python-3-8") ("python-3-7") ("python-3-6") ("python-3-5") ("python-3-4") ("python-3-11") ("python-3-10") ("python-3") ("pep-384") ("link-mode-unresolved-static") ("link-mode-default") ("extension-module") ("default" "python-3")))) (l "python3")))

