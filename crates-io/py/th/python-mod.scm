(define-module (crates-io py th python-mod) #:use-module (crates-io))

(define-public crate-python-mod-0.1.0 (c (n "python-mod") (v "0.1.0") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0dgj99rgd2pzz6cf6rfr5lzwvqcshmqh8qjkgfghsi8aynalqd4v")))

(define-public crate-python-mod-0.1.2 (c (n "python-mod") (v "0.1.2") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "02v5vca8y835frfagwd5da7yihgww660j2qb5pgrpk96xq2pwr71")))

(define-public crate-python-mod-0.1.3 (c (n "python-mod") (v "0.1.3") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1xsf4n7fgqki7xp1894zdrh7pfzm70ljhi0rvpwgn7bz2nidziim")))

(define-public crate-python-mod-0.1.4 (c (n "python-mod") (v "0.1.4") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ykyglyqlmpm4064wdh3cqvizbs69w6lcm3apbf9vbmhyb9i9bdk")))

(define-public crate-python-mod-0.1.5 (c (n "python-mod") (v "0.1.5") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0wcf9d8rr05d0hhj46rg8gqcx297lr2nzvjxm5vzr0igs7z2n174")))

(define-public crate-python-mod-0.1.9 (c (n "python-mod") (v "0.1.9") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1vbaypymwfsdhvcz8s8dwrp5hvbwk48igdjmklymig7m15i2h7s6")))

(define-public crate-python-mod-0.1.10 (c (n "python-mod") (v "0.1.10") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0bwcmk6jkc4scqdcbnv78d9v1mkr0paxxrvy2zd3xsf5zglw1fdr")))

(define-public crate-python-mod-0.1.11 (c (n "python-mod") (v "0.1.11") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1hnzn6s1166w7nayj05ybb5ny62niiijfxi29l52jc4sggi1ldlw")))

(define-public crate-python-mod-0.1.12 (c (n "python-mod") (v "0.1.12") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "python-ast") (r "^0.1.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10pfssimqiy7v8p1px34cyj6zr917d0y0wi9fppq4c4g3jr18lc7")))

(define-public crate-python-mod-0.2.1 (c (n "python-mod") (v "0.2.1") (d (list (d (n "path_macro") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "python-ast") (r "^0.2.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1scxy9y478mcmdwg61zqgbs451w8m5zbzhjxkvxqc1v1cb744vlw")))

