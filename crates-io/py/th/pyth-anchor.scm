(define-module (crates-io py th pyth-anchor) #:use-module (crates-io))

(define-public crate-pyth-anchor-0.1.0 (c (n "pyth-anchor") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.16.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.16.2") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)))) (h "01ivxy7nydzlplm1rf9bdqa9p8r03qiignlwyjanrqq9cfi973zi") (f (quote (("testnet") ("no-idl") ("no-entrypoint") ("mainnet") ("localnet") ("devnet") ("default" "localnet") ("cpi" "no-entrypoint"))))))

