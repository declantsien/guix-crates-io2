(define-module (crates-io py th pyth-solana-receiver-sdk) #:use-module (crates-io))

(define-public crate-pyth-solana-receiver-sdk-0.1.0 (c (n "pyth-solana-receiver-sdk") (v "0.1.0") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "hex") (r ">=0.4.3") (d #t) (k 0)) (d (n "pythnet-sdk") (r "^2.0.0") (d #t) (k 0)) (d (n "solana-program") (r ">=1.16.0") (d #t) (k 0)))) (h "0fa3dn2dccdf6rnz4dmnv7yckcx5f6izdnir0jlskclg2jaqaz4k")))

(define-public crate-pyth-solana-receiver-sdk-0.2.0 (c (n "pyth-solana-receiver-sdk") (v "0.2.0") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "hex") (r ">=0.4.3") (d #t) (k 0)) (d (n "pythnet-sdk") (r "^2.1.0") (f (quote ("solana-program"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.16.0") (d #t) (k 0)))) (h "06pb447fw2zmghaiszkv1bfrfzfcdp4wkxy1qxhzymzgk87cgs4r")))

(define-public crate-pyth-solana-receiver-sdk-0.3.0 (c (n "pyth-solana-receiver-sdk") (v "0.3.0") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "hex") (r ">=0.4.3") (d #t) (k 0)) (d (n "pythnet-sdk") (r "^2.1.0") (f (quote ("solana-program"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.16.0") (d #t) (k 0)))) (h "1l1w8lrwhl0z6w876zy369rsw158yr8r49lkyav7gczh8fb5brli")))

