(define-module (crates-io py th python-input) #:use-module (crates-io))

(define-public crate-python-input-0.5.0 (c (n "python-input") (v "0.5.0") (h "01rx0l9wbi9yi58lbvq7jqbz29vsqpg6pa7y4jdhm93a6i53dp66")))

(define-public crate-python-input-0.6.0 (c (n "python-input") (v "0.6.0") (h "1zv5dm5s35nzm8qdlx1wgrxy9hvbrrwdz2f93liqhjjs3s7wj3k0")))

(define-public crate-python-input-0.7.0 (c (n "python-input") (v "0.7.0") (h "1gk0jzsnag0x0bjhlw4y0vg7lng7r4bdyp2l5dyylgsd5fz6hchd")))

(define-public crate-python-input-0.8.0 (c (n "python-input") (v "0.8.0") (h "0bbqh1mn7bxrpb5qjs3a2swnmf8kji9ix7787zqvqk4bn2hf8izw")))

