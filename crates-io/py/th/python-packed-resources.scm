(define-module (crates-io py th python-packed-resources) #:use-module (crates-io))

(define-public crate-python-packed-resources-0.1.0 (c (n "python-packed-resources") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1jl6xpayihbbyizh48ndb5rlmikwi400ac00ar1677bwf8fkbzrg")))

(define-public crate-python-packed-resources-0.2.0 (c (n "python-packed-resources") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0157dp240qw2yxwzk52gwjhq9bhdb10gg42pb5x64wcvi1zlla74")))

(define-public crate-python-packed-resources-0.3.0 (c (n "python-packed-resources") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "10lz9k5hdgdim08wc6ksg2vghq27x52knzcc51vczf8wnbc5dsan")))

(define-public crate-python-packed-resources-0.4.0 (c (n "python-packed-resources") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0065g7dbwm8rv67vaq8i9b2ar6sd8ifnrildd383dh013incb1zc")))

(define-public crate-python-packed-resources-0.5.0 (c (n "python-packed-resources") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "09q5aq4ichjsf0mx1yyar51zmgqwp94wx1kmf47k170m3rl13h1y")))

(define-public crate-python-packed-resources-0.6.0 (c (n "python-packed-resources") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1yfczg77j8byi53mlzff8wj7ag6gqmag7asvsb3xwnlhivcqpxpx")))

(define-public crate-python-packed-resources-0.7.0 (c (n "python-packed-resources") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1kbpjm4y5q3k66c0033iklsardzdafgikf5k67s50agplb74vzqm")))

(define-public crate-python-packed-resources-0.8.0 (c (n "python-packed-resources") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0jp54bzs5ng6yncjnncglgdcqcbbcvpcv79v26jmb9xg0pm98y6i")))

(define-public crate-python-packed-resources-0.9.0 (c (n "python-packed-resources") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "19ki7kfzd8ppk7zy3f65sa7lcymql2mr22nykd3zz5a08c5681sk")))

(define-public crate-python-packed-resources-0.10.0 (c (n "python-packed-resources") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "12paw48yywc9ncam9m2q1z7ciz5fzx08yp1b7iajh521abpb26pf")))

(define-public crate-python-packed-resources-0.11.0 (c (n "python-packed-resources") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "08ld2pgnjkhrf4kwyni35wkzg9vn9jq7hnb3c3gy98w5dgmpfpb9")))

(define-public crate-python-packed-resources-0.12.0 (c (n "python-packed-resources") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0r4haskx90hjhflbfjxbcfn23h0srk8xf0gs40cj7bqaiwbdrw6a")))

