(define-module (crates-io py th python_comm_macros) #:use-module (crates-io))

(define-public crate-python_comm_macros-0.1.0 (c (n "python_comm_macros") (v "0.1.0") (h "1mzmkpc9bmy9cg76d8l8r3ajkv6sfcs3dhizdj1n7add684wp94j")))

(define-public crate-python_comm_macros-0.1.1 (c (n "python_comm_macros") (v "0.1.1") (h "0l7g1fmjgp2m28rlfljc58kpc8k3jnd093dgxi6ka3vb5zy8l2l7")))

(define-public crate-python_comm_macros-0.1.2 (c (n "python_comm_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqanjqcv1khkx6v5m5swqa88yn5lqblj3hds1wgm1pvwl2yaywr")))

(define-public crate-python_comm_macros-0.1.3 (c (n "python_comm_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06fnknzd26sszmdxm49h5hix06ypzmlb1xg4qfh1pxk0xgqg7irm")))

(define-public crate-python_comm_macros-0.1.4 (c (n "python_comm_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g9fwixi63ifki6z1hf8slgg5856i1acblg62v493y8iyia70ya4")))

(define-public crate-python_comm_macros-0.1.5 (c (n "python_comm_macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14n8p6jmw8gi3brl3v77wcvlnw7zgvbq66kv2qsdyy3bvy0my6xf")))

(define-public crate-python_comm_macros-0.2.0 (c (n "python_comm_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rp5gsdlcpv8ksawa95p1zdhwx52b0zyxdhsipvr60f3br160rm4")))

(define-public crate-python_comm_macros-0.2.1 (c (n "python_comm_macros") (v "0.2.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0scdi08czayb8rn48kc622izpn6kmz8czslda9xygap94fkdlx9s")))

(define-public crate-python_comm_macros-0.2.2 (c (n "python_comm_macros") (v "0.2.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02fpbmw6cadxrnwwn15c9a02w039ccg3hc706m1wan3zm46aj589")))

(define-public crate-python_comm_macros-0.2.3 (c (n "python_comm_macros") (v "0.2.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05914hkr56na23pvxl47f19xih1ak5hrrz0rpga8azjfnc24d2h7")))

(define-public crate-python_comm_macros-0.2.4 (c (n "python_comm_macros") (v "0.2.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g3pdbay0y5iiwgdsa47pgsk9wxs0wl7gwjrffzjih9qj9b1maya")))

(define-public crate-python_comm_macros-0.2.5 (c (n "python_comm_macros") (v "0.2.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zd2na3rg1l22qhw9i66g7nyr8ryrvvjk2knnwslfyj2ml3afxwh")))

(define-public crate-python_comm_macros-0.2.6 (c (n "python_comm_macros") (v "0.2.6") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qy4cf5wlganvninvwrw6wkssravbyvpnixvs0874jzxcfrhkvm8")))

(define-public crate-python_comm_macros-0.3.0 (c (n "python_comm_macros") (v "0.3.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i39s0hwf0jzmg1knb5ivkh17paypd91sss66xwlc6r96m7i6g3c")))

(define-public crate-python_comm_macros-0.3.1 (c (n "python_comm_macros") (v "0.3.1") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17z2a0f78357kg88fcymiymc00h88k0ns67rxr8g9adlg8wg95vf")))

(define-public crate-python_comm_macros-0.3.2 (c (n "python_comm_macros") (v "0.3.2") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gr2m4l53ya9maic52rxvax5x58z1z0mm5jljzgr3yhkq64pbwm0")))

(define-public crate-python_comm_macros-0.3.3 (c (n "python_comm_macros") (v "0.3.3") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yvwri3inywisrv491i4b73fyl93wlw6i3g94z94mdjlbag1w2vy")))

(define-public crate-python_comm_macros-0.3.4 (c (n "python_comm_macros") (v "0.3.4") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08l0nzb6m8bbyry51p4ks27ya5i0lyp6kyy27swqn01d9qsjvg9i")))

(define-public crate-python_comm_macros-0.3.5 (c (n "python_comm_macros") (v "0.3.5") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dmc7px4xbvmm0argnjnnnjk1v24z7q6fvir3df8c0jliq6x3fsn")))

(define-public crate-python_comm_macros-0.3.6 (c (n "python_comm_macros") (v "0.3.6") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17q07prb7ljhbhmz82npiw12j3pqr6x5zai9b7djprsrkg9n2zbl")))

(define-public crate-python_comm_macros-0.3.7 (c (n "python_comm_macros") (v "0.3.7") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zibsgifjp4hffqdvp2l94fx0ii8w7xwq16iyr5pxr7abyydzd9s")))

(define-public crate-python_comm_macros-0.3.8 (c (n "python_comm_macros") (v "0.3.8") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03l2z9z68xix0l9svlx73g42dfi17z2r9xc6i52d39x426wi8plq")))

(define-public crate-python_comm_macros-0.3.9 (c (n "python_comm_macros") (v "0.3.9") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fzxkffnjdbnahx4z3a84cd11b3xgk6d7cr0907xgxvm2clgh85q")))

(define-public crate-python_comm_macros-0.4.0 (c (n "python_comm_macros") (v "0.4.0") (d (list (d (n "chrono") (r "~0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19092ycv8incprbh93nsj821psywkp43fclsjbx0jv2jqycyqbd8")))

