(define-module (crates-io py th pythagoras) #:use-module (crates-io))

(define-public crate-pythagoras-0.1.0 (c (n "pythagoras") (v "0.1.0") (h "1c68s7acz429y5vj9qpz2y89v0rqinvk3mvi8ph74jql92aq4h8q")))

(define-public crate-pythagoras-0.1.1 (c (n "pythagoras") (v "0.1.1") (h "181lx3nhgsicax9bninizfnz0vnf03gskjm6gvnhjibfv4gd658h")))

