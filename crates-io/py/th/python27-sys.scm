(define-module (crates-io py th python27-sys) #:use-module (crates-io))

(define-public crate-python27-sys-0.0.1 (c (n "python27-sys") (v "0.0.1") (h "1v9yjvdj5vlms5raxlg8adqf8x74nd9q5jmbrjikjq4i3v33ma6f") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.0.2 (c (n "python27-sys") (v "0.0.2") (h "0j5xas8k80glkrpgqq2d024306bfhz8njhjz56h7p7fxrcfar16b") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.0.3 (c (n "python27-sys") (v "0.0.3") (h "1cf1pq9d12s1l48svxb066pg63nsx0piqc9klsl6jhqc2vv0y61x") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.0.4 (c (n "python27-sys") (v "0.0.4") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "1mzy913bpbhvvicw3jvv84cyijcifqp2sb5za65009jjw7rnz18c") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.0.5 (c (n "python27-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)))) (h "0p10ghniz09plka2zvhhs96diymc82k7sb9l5qkcvh7gnwkzrx9y") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.0.6 (c (n "python27-sys") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0g6zzjcbpmxd914ha1pyjbb24cbmg3rircp335q61bmaw06y68iq") (f (quote (("default" "Py_USING_UNICODE" "Py_UNICODE_WIDE" "WITH_THREAD") ("WITH_THREAD") ("Py_USING_UNICODE") ("Py_UNICODE_WIDE" "Py_USING_UNICODE") ("Py_TRACE_REFS" "Py_REF_DEBUG") ("Py_REF_DEBUG") ("Py_DEBUG" "Py_TRACE_REFS") ("COUNT_ALLOCS"))))))

(define-public crate-python27-sys-0.1.0 (c (n "python27-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "regex") (r "^0.1.8") (d #t) (k 1)))) (h "12jrf5n14cy4i36plrzs89zgy5882pr7q1j4va7s7hmsdg3qi9g4") (f (quote (("python-2-7") ("default" "python-2-7"))))))

(define-public crate-python27-sys-0.1.1 (c (n "python27-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "1i24cn95i8g3q8hbwwxalr93h6j1d4ps42lrhmvachm6zlhpblrz") (f (quote (("python-2-7") ("default" "python-2-7"))))))

(define-public crate-python27-sys-0.1.2 (c (n "python27-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "0zww72zp9x4ickk1fjsi7fxzk1ln8zbsjbv7ss38m536r0rkmnzh") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7"))))))

(define-public crate-python27-sys-0.2.0 (c (n "python27-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 1)))) (h "1hrq09wflgqhcx1dzgfr96zf4shpvyxf5kh5r7r5a7i4zq5w7bjd") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7"))))))

(define-public crate-python27-sys-0.2.1 (c (n "python27-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0zjjb0i6frwyrga1jsykr4d8g352h9vxy2a0swk2ba6wshvlq4an") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.3.0 (c (n "python27-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0nyxzynxlr2n0z4kwdj8zhla29cdvi004f21nsg11j3bibl5a99p") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.4.0 (c (n "python27-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "06glkvsdza9r30k7bw7jgj1fk3q5vlxy1xmn56hal1yzjvrdphpc") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.4.1 (c (n "python27-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "132crrq1nzzddx1530hi6bhcc9azmxkmdmsd4azi2lb1x0fh9jv7") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.5.0 (c (n "python27-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1m4lyxahnjn5yjbfy9jvfj83hq808c2m9hfm9svbl1gz6fdnssv4") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.5.1 (c (n "python27-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0xd0wch4776khgvmnjwnkppswlqyrgpgkyncad6qwkvk5q4dy0zg") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.5.2 (c (n "python27-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0m100c3qnawmfd6m45jm6m9j17zg01w44kkw64r513q4sxz8k1gl") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.6.0 (c (n "python27-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1arjd3f28ypvh1gd8aqr5h4lkfshpwjxq3q4j820psv6afyds9jq") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

(define-public crate-python27-sys-0.7.0 (c (n "python27-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1lfzyhbghlnb6a62ffng7nn5d7gwdfxlr1m5h7g0sc34w9a06rwl") (f (quote (("python-2-7") ("extension-module") ("default" "python-2-7")))) (l "python27")))

