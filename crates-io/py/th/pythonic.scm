(define-module (crates-io py th pythonic) #:use-module (crates-io))

(define-public crate-pythonic-0.2.0 (c (n "pythonic") (v "0.2.0") (h "1gj0gih4jpairgdaxdi5xvzkib3dvq46mdxaaq9i6cm3mkl1v4nl")))

(define-public crate-pythonic-0.2.1 (c (n "pythonic") (v "0.2.1") (h "1mc1vw0hn8hp4883xxm6npkj0w7rb80pg616kb59vwxc9ai5dgma")))

(define-public crate-pythonic-0.2.2 (c (n "pythonic") (v "0.2.2") (h "1yhldi65pxn9x03ad7xky2cwc9k5n7sj0vj78nh890ibs19l36jh")))

(define-public crate-pythonic-0.2.3 (c (n "pythonic") (v "0.2.3") (h "10y2lxi5bsqh50zah6hi2qx115jdw8rs8p53kznryl38yanmybqw")))

(define-public crate-pythonic-0.2.4 (c (n "pythonic") (v "0.2.4") (h "1ca2kzn000xrab0fzxqzi0dmnrspmlfj4s9cpf0pkg1qpac2zs02")))

(define-public crate-pythonic-0.3.0 (c (n "pythonic") (v "0.3.0") (h "0zfghxwij004l9anhdjf34xcw1hrqs09faky97k0a33hlp5gza58")))

