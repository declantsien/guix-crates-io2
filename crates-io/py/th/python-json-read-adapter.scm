(define-module (crates-io py th python-json-read-adapter) #:use-module (crates-io))

(define-public crate-python-json-read-adapter-0.1.0 (c (n "python-json-read-adapter") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.33") (o #t) (d #t) (k 0)) (d (n "serde_self") (r "^1.0.82") (o #t) (d #t) (k 0) (p "serde")))) (h "1pm9gnjlk4jzqykfyf033ngdf8414f8azn1fywhmw4chrj2d12wi") (f (quote (("serde" "serde_self" "serde_json"))))))

