(define-module (crates-io py th python3-dll-a) #:use-module (crates-io))

(define-public crate-python3-dll-a-0.1.0 (c (n "python3-dll-a") (v "0.1.0") (h "1psr4jkyapymy7l3gldals7qz4mdsk3n9mfdvcxs4zxfq3xs76nh")))

(define-public crate-python3-dll-a-0.1.1 (c (n "python3-dll-a") (v "0.1.1") (h "06mw2s0cx72cm5ibn2vwf3s5g3slnqv6c2xqb7rb0y5hjsbhpc0m")))

(define-public crate-python3-dll-a-0.1.2 (c (n "python3-dll-a") (v "0.1.2") (h "11dapb1vfkjabhpg75xiqh6jyx0p6i0wlqjphabn5if9d8fr944n")))

(define-public crate-python3-dll-a-0.2.0 (c (n "python3-dll-a") (v "0.2.0") (h "1cbhn91mgsg36dgkjxvr7hfjww8kcc5fb68zba2b3gwcbk6zm505")))

(define-public crate-python3-dll-a-0.2.1 (c (n "python3-dll-a") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "06jcjjwsd3g0zwnnig6sdcn4346r2cnfvirfwqjqjpjzmw2vbdf3")))

(define-public crate-python3-dll-a-0.2.2 (c (n "python3-dll-a") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "1rm1xgg3hdwxdhcrdh1vkw4w3zakmr879psqhggir0pmyjwc8vbm")))

(define-public crate-python3-dll-a-0.2.3 (c (n "python3-dll-a") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "034r1rdfiwx1kblzdxjd9g5m1m70dilnvk2cbz83g2mm8aw7yn76")))

(define-public crate-python3-dll-a-0.2.4 (c (n "python3-dll-a") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "02l0hiswhggjsf471f6bx6s2khplqb9fqbyarjkggrvkcay3f80w")))

(define-public crate-python3-dll-a-0.2.5 (c (n "python3-dll-a") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "01k2nf4wq4f4xnaq3fy1gx12v10pndwmdrjq4x4pshhjxxnrpml9")))

(define-public crate-python3-dll-a-0.2.6 (c (n "python3-dll-a") (v "0.2.6") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "1a676r8xlbkijdagywwz838rbdnc9h28lgmx1ccvyqj9h9rbs5d9")))

(define-public crate-python3-dll-a-0.2.7 (c (n "python3-dll-a") (v "0.2.7") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "08n4g0ky5rpbypafk56nb04z5pv50yy4n3azxlpa5qvh09a78b91")))

(define-public crate-python3-dll-a-0.2.8 (c (n "python3-dll-a") (v "0.2.8") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "1xx89zfh9r9vhnjshr1pgdaxmpm38z5w43vchrjjaqh6bhymdqva")))

(define-public crate-python3-dll-a-0.2.9 (c (n "python3-dll-a") (v "0.2.9") (d (list (d (n "cc") (r "^1.0.73") (d #t) (t "cfg(windows)") (k 0)))) (h "0grlz344xq435qgs5mic0yz1p643qh3h1m11lw4zms1b87a7rw6m")))

