(define-module (crates-io py th pythonic-helper) #:use-module (crates-io))

(define-public crate-pythonic-helper-0.1.0 (c (n "pythonic-helper") (v "0.1.0") (h "189p94bj8ddxhbrhlqg4i4xrxxlgqwa8hccfhmb95iri3rxm4b24")))

(define-public crate-pythonic-helper-0.1.1 (c (n "pythonic-helper") (v "0.1.1") (h "1szy1610sbv4gyfyxs84vjwfzhnsgi7mrcisf9v6ppxfsrdvfmhk")))

(define-public crate-pythonic-helper-0.1.2 (c (n "pythonic-helper") (v "0.1.2") (h "156lgc4457sdxd4faw42q845nvglahl73gx0hgynkvh7c84hjq1l")))

(define-public crate-pythonic-helper-0.1.3 (c (n "pythonic-helper") (v "0.1.3") (h "0400yrcvymnnkn4d58p6s2l6c11g2x5ra12ggl8fp2b91kpva81q")))

