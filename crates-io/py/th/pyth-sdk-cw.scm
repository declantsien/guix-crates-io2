(define-module (crates-io py th pyth-sdk-cw) #:use-module (crates-io))

(define-public crate-pyth-sdk-cw-0.1.0 (c (n "pyth-sdk-cw") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.4.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "17iakmq2kf7d9kwa69li3d1cisafr0qhxnl0wbsqlh6l91mmsn7a")))

(define-public crate-pyth-sdk-cw-0.2.0 (c (n "pyth-sdk-cw") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jrkb0dchm33gnrdxqvwxvy8p1n3ibdfapxa22l3242hizdpa5ag")))

(define-public crate-pyth-sdk-cw-0.3.0 (c (n "pyth-sdk-cw") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.7.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bapwj5v5fza8m5fx2fi8i5qcyzbirnv5h25chsd6jxd5i7q3w3v")))

(define-public crate-pyth-sdk-cw-1.0.0 (c (n "pyth-sdk-cw") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0x56qnmja6jxpi634f13q5741xjmx2sqdxdjk8asfgzxn5fyxms1")))

(define-public crate-pyth-sdk-cw-1.2.0 (c (n "pyth-sdk-cw") (v "1.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "pyth-sdk") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0j6y035j592bhb9gkrb96vldskbxxrddpz4sn09yzqdwc4lrykn0") (f (quote (("osmosis"))))))

