(define-module (crates-io py th python_mixin) #:use-module (crates-io))

(define-public crate-python_mixin-0.0.1 (c (n "python_mixin") (v "0.0.1") (d (list (d (n "regex") (r "^0") (d #t) (k 0)) (d (n "regex_macros") (r "^0") (d #t) (k 0)))) (h "0m7gzhmgq4ls3mw7hzaff008cjb35s5da2h7pfd855sdza76ar5p") (f (quote (("default") ("compile_error")))) (y #t)))

(define-public crate-python_mixin-0.0.2 (c (n "python_mixin") (v "0.0.2") (h "0inma3nyqw6ypwpqw0cgmsdx15cfydcnyigfrgkkl60j5q9n433m") (f (quote (("default") ("compile_error")))) (y #t)))

