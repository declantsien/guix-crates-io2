(define-module (crates-io py th pythagore) #:use-module (crates-io))

(define-public crate-pythagore-0.1.0 (c (n "pythagore") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "16y7zajlrmca4kkivz56px4g08khjrkg4z8cdydyw8czg2jkyaxs")))

(define-public crate-pythagore-0.1.1 (c (n "pythagore") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1yf24ywzxczpi8k030h7672p2i1sfnvpagiwa7a13migs0b3c1bf")))

(define-public crate-pythagore-0.1.2 (c (n "pythagore") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0gcr243amh388gd5xmsxqs1yfd0whqigfla3n8p4lsyf5ly9v7ng")))

(define-public crate-pythagore-0.1.3 (c (n "pythagore") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "14qwvdz2dxhzphy8n8yicarb9ji1bs2rdp1h27618nkd5daxq8ak")))

(define-public crate-pythagore-0.2.0 (c (n "pythagore") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0yfhb50px6sm4z0snx949wj38511imjvhwm5iq076akb6q74aakx")))

(define-public crate-pythagore-0.2.1 (c (n "pythagore") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1z3490pkxazbh5cxxqlyc63g96fvviznfmrxbkxvchzmrbkfps7h")))

(define-public crate-pythagore-0.3.0 (c (n "pythagore") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "098a7f8wc41bgbs5nk7dqldgwxjkffrmirpnwc28dmd1dnmwpl4d") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.4.0 (c (n "pythagore") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1na4mz4w7b0h720hvk4cqwjsyvsapbhjk59rgn1g2fjmy7h48qii") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.4.1 (c (n "pythagore") (v "0.4.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0xr8gwdmjzrgpask1vf8i0wp0q7yp4vmygvscp3j1gcfrcibchab") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.5.0 (c (n "pythagore") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0mcc4a5fzwcva1l40sp8jmpb6hs4sr4l76l0c73s6ykhqwc2bfwc") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.5.1 (c (n "pythagore") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "18hy43iviy6x5m4bq4m4phq3f0h1bjn4ikfawg2b4gjyinabax0f") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.5.2 (c (n "pythagore") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1lx1gmdjyn8hm7xaz3jp76gcapbb645rr6b9k1xykffxj0wl5kqd") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.6.0 (c (n "pythagore") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1vrx0nh5xqr0px45wvj2hgnh2pvkx3aq55rkzd9026w05h3fwlcc") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.6.1 (c (n "pythagore") (v "0.6.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1l10ix2fgmw8bzp0lw4dzcifn6cwzijxl7hkc9hyr1sgckyvannq") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.7.0 (c (n "pythagore") (v "0.7.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1pxc02h8hzwh0svziyvbv6sq8nj7hh82dak560ijz65icnz7jcrq") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.7.1 (c (n "pythagore") (v "0.7.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0zd6f3wc4hhd36qifibv2lhlzlpmnvzkyvcjscgddxr5abn7apzv") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.7.2 (c (n "pythagore") (v "0.7.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1nhpkgbk24rn8hcv313c0vcyjx31m24i2sx85y8ylpk1hr1g8m7j") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.7.3 (c (n "pythagore") (v "0.7.3") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0xl6215l6slx679ini8w5qxjdzscaqixs7p1n9bhik2cri4xv3pi") (f (quote (("wasm-vector" "wasm") ("wasm-point" "wasm")))) (s 2) (e (quote (("wasm" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.8.0 (c (n "pythagore") (v "0.8.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1af4y2h5lfzpcq6nw5sisbjpkg931yjg1qd7y4qvk0nysiy8wz38") (f (quote (("wasm-vector" "wasm-vector-int" "wasm-vector-real") ("wasm-point" "wasm-point-int" "wasm-point-real") ("wasm" "wasm-point" "wasm-vector")))) (s 2) (e (quote (("wasm-vector-real" "dep:wasm-bindgen") ("wasm-vector-int" "dep:wasm-bindgen") ("wasm-point-real" "dep:wasm-bindgen") ("wasm-point-int" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.8.1 (c (n "pythagore") (v "0.8.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0zr5zizrc8833fqbskk0a82qajv7dibbj8lzbsvcbpj386hlmim2") (f (quote (("wasm-vector" "wasm-vector-int" "wasm-vector-real") ("wasm-point" "wasm-point-int" "wasm-point-real") ("wasm" "wasm-point" "wasm-vector")))) (s 2) (e (quote (("wasm-vector-real" "dep:wasm-bindgen") ("wasm-vector-int" "dep:wasm-bindgen") ("wasm-point-real" "dep:wasm-bindgen") ("wasm-point-int" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.8.2 (c (n "pythagore") (v "0.8.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "1iyn7zs54wh6nsmj45706f8xs00qdlfavniczazjysbnrix5gplw") (f (quote (("wasm-vector" "wasm-vector-int" "wasm-vector-real") ("wasm-point" "wasm-point-int" "wasm-point-real") ("wasm" "wasm-point" "wasm-vector")))) (s 2) (e (quote (("wasm-vector-real" "dep:wasm-bindgen") ("wasm-vector-int" "dep:wasm-bindgen") ("wasm-point-real" "dep:wasm-bindgen") ("wasm-point-int" "dep:wasm-bindgen"))))))

(define-public crate-pythagore-0.8.3 (c (n "pythagore") (v "0.8.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0wy7kxajfn9q6x0bl1lic2cx6xx3cd9j5syabrvfcfmzbdxr680a") (f (quote (("wasm-vector" "wasm-vector-int" "wasm-vector-real") ("wasm-point" "wasm-point-int" "wasm-point-real") ("wasm" "wasm-point" "wasm-vector")))) (s 2) (e (quote (("wasm-vector-real" "dep:wasm-bindgen") ("wasm-vector-int" "dep:wasm-bindgen") ("wasm-point-real" "dep:wasm-bindgen") ("wasm-point-int" "dep:wasm-bindgen"))))))

