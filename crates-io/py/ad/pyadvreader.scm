(define-module (crates-io py ad pyadvreader) #:use-module (crates-io))

(define-public crate-pyadvreader-1.2.0 (c (n "pyadvreader") (v "1.2.0") (d (list (d (n "advreader") (r "^1.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1h9gcmm9yz5a99x8y9q8lyy2ipzh1nsa72g012lgc7dkqkcayj04")))

(define-public crate-pyadvreader-2.0.0 (c (n "pyadvreader") (v "2.0.0") (d (list (d (n "advreader") (r "^2.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "08bmnisr578s6az1d3qxfsykxb73ddrlrxv7sjq6rnbnj9bgw39g")))

(define-public crate-pyadvreader-2.1.0 (c (n "pyadvreader") (v "2.1.0") (d (list (d (n "advreader") (r "^2.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1clr222q8rqmcsy2xlfrzxy2z7f1vg8j5zh85gw924iqzc95dfq4")))

(define-public crate-pyadvreader-2.1.1 (c (n "pyadvreader") (v "2.1.1") (d (list (d (n "advreader") (r "^2.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "10k9j425lh060s9mwczys7jq9f4dl2pnrl760v7gq6ia342zycrq")))

(define-public crate-pyadvreader-2.2.0 (c (n "pyadvreader") (v "2.2.0") (d (list (d (n "advreader") (r "^2.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1q4smw0rz4r8ml1ipwb9c52i7m2a754d97vfi2diwh1r9z1qmpvf")))

