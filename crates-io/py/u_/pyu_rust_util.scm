(define-module (crates-io py u_ pyu_rust_util) #:use-module (crates-io))

(define-public crate-pyu_rust_util-0.1.0 (c (n "pyu_rust_util") (v "0.1.0") (h "1lhb1s536r0wsqp1zr68zqlsb2cr2rf4hlyivp3ghkyk5ikgnlsj")))

(define-public crate-pyu_rust_util-0.1.2 (c (n "pyu_rust_util") (v "0.1.2") (h "1b3xi0safv6cy8mhmizkzzdzcdi0clzjazxpkqyh3s6k080ik5dg")))

(define-public crate-pyu_rust_util-0.1.3 (c (n "pyu_rust_util") (v "0.1.3") (h "0bd6nm9zmnb18a9b2mrvicqxxsyjcsgz9n97h67km62y7zn6441x")))

(define-public crate-pyu_rust_util-0.1.4 (c (n "pyu_rust_util") (v "0.1.4") (h "0pwxgh8n601km834h84f25zhwyh29ccrjc3ygsbpjwaqap3famlg")))

(define-public crate-pyu_rust_util-0.1.5 (c (n "pyu_rust_util") (v "0.1.5") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1jdpywnv5016sa2i7blkayx0lc90kxwzj8119wjq85zvvkbjdnig")))

(define-public crate-pyu_rust_util-0.1.6 (c (n "pyu_rust_util") (v "0.1.6") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0assk2053jg61mfa4pxg9is9fsy6bp07kbsdiqap1xis9qan8gps")))

(define-public crate-pyu_rust_util-0.1.61 (c (n "pyu_rust_util") (v "0.1.61") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0kgpsygqk48w2vdqjxzxky4w4hc256nqs9bigb52grlbhwannka8")))

(define-public crate-pyu_rust_util-0.1.62 (c (n "pyu_rust_util") (v "0.1.62") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "04qhqay10gav948z93vkqv5835gxhk0gqrza012fypnnz93vhczn")))

(define-public crate-pyu_rust_util-0.1.63 (c (n "pyu_rust_util") (v "0.1.63") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1gjgs783kcfxdbkp0bcrzr1gmwhq8h5ylv7vm2p4i6m2bwij3007")))

(define-public crate-pyu_rust_util-0.1.64 (c (n "pyu_rust_util") (v "0.1.64") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1fg15h3yzi9b4g3adx3ax1jlz38rjmfv4jmyvwqb2l2pw73mnny8")))

(define-public crate-pyu_rust_util-0.1.66 (c (n "pyu_rust_util") (v "0.1.66") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1sx4sm8f0b99iz810g5njh5dfx894xlww4jxsf8904cc0dg236r1")))

(define-public crate-pyu_rust_util-0.1.7 (c (n "pyu_rust_util") (v "0.1.7") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0hzv95zcvn893chprsyg1jh6p95ylbidmzlv96awxaf7qj7v0szp")))

(define-public crate-pyu_rust_util-0.1.70 (c (n "pyu_rust_util") (v "0.1.70") (d (list (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "10jkny1pjx8dsafpzal7073wa3w7n0gy8rn273ml22rvbap106kh")))

(define-public crate-pyu_rust_util-0.1.71 (c (n "pyu_rust_util") (v "0.1.71") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0mjy5yg4nr9pr3g4jgih1lji9gy3gbd7fbgk8bdl7h3hm0kxvhy2")))

(define-public crate-pyu_rust_util-0.1.80 (c (n "pyu_rust_util") (v "0.1.80") (d (list (d (n "rodio") (r "^0.17.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "116jnzkvswgsv6inyay4byl7r936s7baqp5l5f2szvrd7irymgkq")))

