(define-module (crates-io py _s py_sql) #:use-module (crates-io))

(define-public crate-py_sql-1.0.0 (c (n "py_sql") (v "1.0.0") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "rexpr") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0mnryf90g7bj1zarfv67w6p65r6r835qiw9mgz2x4yj61k04199c")))

(define-public crate-py_sql-1.0.1 (c (n "py_sql") (v "1.0.1") (d (list (d (n "dashmap") (r "^4") (d #t) (k 0)) (d (n "rexpr") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q301bn821nyfkhz1s08dylrh1zddbqkxs4i6glydk8g6rn70ss0")))

