(define-module (crates-io py #{03}# py03_helper_macros) #:use-module (crates-io))

(define-public crate-py03_helper_macros-0.0.0 (c (n "py03_helper_macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "19iincd1nn30wyl0h2dxilg367shag1wm1s8ia72ifnfi87rxlab") (f (quote (("multiple_pymethod") ("default")))) (y #t)))

(define-public crate-py03_helper_macros-0.0.1 (c (n "py03_helper_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wgvf6galncz0wjqqama1y15h2djnv61gx3vciqcqava67whfnnc") (f (quote (("multiple_pymethod") ("default")))) (y #t)))

(define-public crate-py03_helper_macros-0.0.2 (c (n "py03_helper_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fc8s8xpwxndm72zhwbfzf2aqywd8fknncs6yihp2rpdcxrra6kv") (f (quote (("multiple_pymethod") ("default")))) (y #t)))

