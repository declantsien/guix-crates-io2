(define-module (crates-io py de pydestiny) #:use-module (crates-io))

(define-public crate-pydestiny-0.1.0 (c (n "pydestiny") (v "0.1.0") (d (list (d (n "destiny") (r "^0.2.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.11.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "04n3dx8jkxygdvdvp117rf009p6lvc60yybgzgm6ga6mc6vdxqd3")))

