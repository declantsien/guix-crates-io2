(define-module (crates-io py de pydeco) #:use-module (crates-io))

(define-public crate-pydeco-0.1.0 (c (n "pydeco") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.20") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1357ja1ahv85ls9kd6cxcw9ng7v70z6b3n79ppl5jzwmsfwxzdm0")))

