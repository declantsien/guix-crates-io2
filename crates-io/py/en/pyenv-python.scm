(define-module (crates-io py en pyenv-python) #:use-module (crates-io))

(define-public crate-pyenv-python-0.1.0 (c (n "pyenv-python") (v "0.1.0") (d (list (d (n "exec") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "print_bytes") (r "^0.2.0") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "1g8v92r54ynvvpv5zfvc79x5sl6ipkbvz28hy1k3x59sf5xpgj5z")))

(define-public crate-pyenv-python-0.1.1 (c (n "pyenv-python") (v "0.1.1") (d (list (d (n "exec") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "print_bytes") (r "^0.2.0") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)))) (h "0q18fcs1pi1kq6ygq1m325jizlk4hd3lsq9ig3arf3m4179pifwk")))

(define-public crate-pyenv-python-0.2.0 (c (n "pyenv-python") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "print_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1w0y5fhz1di7iplmwp909byli0f5vpavq099kay1zhfcpi12rnm9")))

(define-public crate-pyenv-python-0.3.0 (c (n "pyenv-python") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "print_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0rzsd7b589wmln7psny1d20pv0mxr5bpjj2nh4gz2i7293w3qx5j")))

(define-public crate-pyenv-python-0.3.1 (c (n "pyenv-python") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "is_executable") (r "^0.1.2") (d #t) (k 0)) (d (n "print_bytes") (r "^0.3.1") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0ysn5735pcxl3mfm1g82j919bclpvkchvqqzqmj85kzrcshkvlyc")))

(define-public crate-pyenv-python-0.4.0 (c (n "pyenv-python") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "apply") (r "^0.3.0") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0.0") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.1") (d #t) (k 0)) (d (n "print_bytes") (r "^0.4.2") (d #t) (k 0)) (d (n "same-file") (r "^1.0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0v4ww3b2clhaw2b65mqcgqf7p7kawmxmykdihm1d2vgqmicpc1rb")))

