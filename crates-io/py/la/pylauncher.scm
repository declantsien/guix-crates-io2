(define-module (crates-io py la pylauncher) #:use-module (crates-io))

(define-public crate-pylauncher-0.1.0 (c (n "pylauncher") (v "0.1.0") (d (list (d (n "dbg") (r "^1.0.4") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)))) (h "1vjzi0q0sfa7qjcz0r5rr4fk0jmwfij3szbf49r5mrfv0jq794pb")))

(define-public crate-pylauncher-0.1.1 (c (n "pylauncher") (v "0.1.1") (d (list (d (n "dbg") (r "^1.0.4") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.39") (d #t) (k 0)))) (h "1v9kdjxwxv2mzc1m4qi1s4mfgsphgyp73d9kz8nq950r69ifv82c")))

