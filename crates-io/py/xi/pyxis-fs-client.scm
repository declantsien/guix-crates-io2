(define-module (crates-io py xi pyxis-fs-client) #:use-module (crates-io))

(define-public crate-pyxis-fs-client-0.0.0 (c (n "pyxis-fs-client") (v "0.0.0") (h "1zwh9b079dy4vyc27c52xn73vrn3n3lriknkrjin72ikb0139q0p") (y #t)))

(define-public crate-pyxis-fs-client-0.1.0 (c (n "pyxis-fs-client") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "fuser") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "pyxis-fs-common") (r "^0.1.0") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0bz1q6fd09d6hagv3dqw7fz33wdnii1rd75sm1z98fir91bvr3hw") (y #t)))

(define-public crate-pyxis-fs-client-1.0.0 (c (n "pyxis-fs-client") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "fuser") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "pyxis-fs-common") (r "^0.1.0") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rustls") (r "^0.20.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0dbbh5nr1qjl8nazh6k2nx06aq315cpylxcrccvyhdjfgqjb0bam")))

