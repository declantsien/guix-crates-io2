(define-module (crates-io py xi pyxis-fs-common) #:use-module (crates-io))

(define-public crate-pyxis-fs-common-0.1.0 (c (n "pyxis-fs-common") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fuser") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "pyxis-parcel") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sncb0wzvmnxgy1hfw9cyxys4h9xdvnm5y25ymsnfak5zjig6q69") (f (quote (("parcel" "pyxis-parcel") ("certs" "rcgen"))))))

