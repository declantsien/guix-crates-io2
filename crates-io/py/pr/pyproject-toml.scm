(define-module (crates-io py pr pyproject-toml) #:use-module (crates-io))

(define-public crate-pyproject-toml-0.1.0 (c (n "pyproject-toml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1hdnhx11616zxpjwwq109yiiaxmv7q3wdinvxy1w9wima3algvhq")))

(define-public crate-pyproject-toml-0.1.1 (c (n "pyproject-toml") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0p8mkkpdk3y1pnd8h714l28l888cpk3zsh8glsj3dl29yx8b48z6")))

(define-public crate-pyproject-toml-0.2.0 (c (n "pyproject-toml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mjakg1nrjg390hbqdcc005fcfprvi6dpkpz1hi4d6gfndy16isk")))

(define-public crate-pyproject-toml-0.3.0 (c (n "pyproject-toml") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0fxpaxpjp95x5c9k9xwskl0dsimsvhzjww0pw5wbmm3cygahp251")))

(define-public crate-pyproject-toml-0.3.1 (c (n "pyproject-toml") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1yx8m8a6hf578dxdpv9irrfin2fyfsdyq6bgkxwab24ayznha11q")))

(define-public crate-pyproject-toml-0.3.2 (c (n "pyproject-toml") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.6.0") (d #t) (k 0)))) (h "0j2xkh3nxnfg9bmhwr2ba21jwwqsx5ixdlmkzaklsll9z2jrq40v")))

(define-public crate-pyproject-toml-0.3.3 (c (n "pyproject-toml") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "1nz8vl63qsz2kbj9p1h7444y9x362hc63islxpb8kd87dhk7d17f")))

(define-public crate-pyproject-toml-0.4.0 (c (n "pyproject-toml") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "1rhlsjfr28wg2gnkgaa98gm2bs34sf2bivrfk11hg21iijfbahjg")))

(define-public crate-pyproject-toml-0.5.0 (c (n "pyproject-toml") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "1pgs4gcd62gcwjqp8j9qfrismdjy8g5by6li3kc86bvr84ifdzsx")))

(define-public crate-pyproject-toml-0.5.1 (c (n "pyproject-toml") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "0zx3hyy9x8dn2mx0fyzvi3rgwkdfw6k6xy2vyjsy049zzbclmhki")))

(define-public crate-pyproject-toml-0.5.2 (c (n "pyproject-toml") (v "0.5.2") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "0jh4la05aklyzspxyrzxkw4clm81yh6issmjwbsm4h5vy98w8nfs")))

(define-public crate-pyproject-toml-0.4.1 (c (n "pyproject-toml") (v "0.4.1") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "059153b7kgvjlx5i6f19gx2xhm9p0220435fx7h35xk4v8hbcijh")))

(define-public crate-pyproject-toml-0.6.0 (c (n "pyproject-toml") (v "0.6.0") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "06rjffk7s36b82gmd9s3h9bj7lzy68x9gkf78cwmi25x6srvnkgh")))

(define-public crate-pyproject-toml-0.6.1 (c (n "pyproject-toml") (v "0.6.1") (d (list (d (n "indexmap") (r "^1.9.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "0pywp6ml15jlv9yxfjcvrs3fgd3xnq8fc6a2wcbw9q9iknmgwygf")))

(define-public crate-pyproject-toml-0.7.0 (c (n "pyproject-toml") (v "0.7.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "1c345r3x8jcmvasmc89mmn10y9n5hz8p5dpqbpn8rsrjs6f2b7jn") (r "1.64")))

(define-public crate-pyproject-toml-0.8.0 (c (n "pyproject-toml") (v "0.8.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.0") (f (quote ("parse"))) (k 0)))) (h "1zwihg9iw5hpf2f85h12lbvf7kzgg94hq18wg6sfpdxqy0zw2x07") (r "1.64")))

(define-public crate-pyproject-toml-0.8.1 (c (n "pyproject-toml") (v "0.8.1") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.3.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "010fl8m9cx1a5iapcpy53dabl16ij5saa3maz0lkmwl7j7kabm26") (r "1.64")))

(define-public crate-pyproject-toml-0.8.2 (c (n "pyproject-toml") (v "0.8.2") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "0c3ckhgxpzhqpjv38rjz2jcprx95pjfnfq53xi4qp31gd84swqgg") (y #t) (r "1.64")))

(define-public crate-pyproject-toml-0.9.0 (c (n "pyproject-toml") (v "0.9.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "1ndb0bkn22ynhv69qx7xd8fjz36iyxchic3v9dakralrbxsdvhwm") (r "1.64")))

(define-public crate-pyproject-toml-0.10.0 (c (n "pyproject-toml") (v "0.10.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "1648m8pyl4r0rcvl21z1hlfnxpcm7yq7sb1sjvwc64ylns4zi01v") (f (quote (("tracing" "pep440_rs/tracing" "pep508_rs/tracing")))) (r "1.64")))

(define-public crate-pyproject-toml-0.11.0 (c (n "pyproject-toml") (v "0.11.0") (d (list (d (n "indexmap") (r "^2.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep440_rs") (r "^0.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pep508_rs") (r "^0.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (f (quote ("parse"))) (k 0)))) (h "0sm3ncm57hgcyladl55w59ycl39vq4crigjb9bya0n6b7c162w7g") (f (quote (("tracing" "pep440_rs/tracing" "pep508_rs/tracing")))) (r "1.64")))

