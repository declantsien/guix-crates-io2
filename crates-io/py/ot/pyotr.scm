(define-module (crates-io py ot pyotr) #:use-module (crates-io))

(define-public crate-pyotr-0.1.0 (c (n "pyotr") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (f (quote ("windows-console-colors"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("process" "full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0aw2qyhglbn716rzhx2fjzafv2q15h5d48hpka6jf91jmp2h19lc")))

