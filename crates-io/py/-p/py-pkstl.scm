(define-module (crates-io py -p py-pkstl) #:use-module (crates-io))

(define-public crate-py-pkstl-0.1.0 (c (n "py-pkstl") (v "0.1.0") (d (list (d (n "pkstl") (r "^0.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)))) (h "14b9fp1ngmfi3pmvwmpv2bhjnm8ib66lzp0s15940f1hmrwrd68c")))

