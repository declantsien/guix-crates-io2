(define-module (crates-io py lo pylon-token) #:use-module (crates-io))

(define-public crate-pylon-token-0.2.0 (c (n "pylon-token") (v "0.2.0") (d (list (d (n "cosmwasm-bignumber") (r "^2.2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw2") (r "^0.8.0") (d #t) (k 0)) (d (n "cw20") (r "^0.8.0") (d #t) (k 0)) (d (n "pylon-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)))) (h "03nm6larnrznzllfh1qw7qdlal1rqgj6kp4vbg5r14sia85k7g17") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

