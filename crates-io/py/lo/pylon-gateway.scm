(define-module (crates-io py lo pylon-gateway) #:use-module (crates-io))

(define-public crate-pylon-gateway-0.2.0 (c (n "pylon-gateway") (v "0.2.0") (d (list (d (n "cosmwasm-bignumber") (r "^2.2.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.16.0") (f (quote ("iterator"))) (d #t) (k 0)) (d (n "cw20") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "terra-cosmwasm") (r "^2.2.0") (d #t) (k 0)))) (h "10b2yz3p505drzwz6dp30qgmrc8h2pdrrccddyrph29lqpwx74ns") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

