(define-module (crates-io py lo pylon-sys) #:use-module (crates-io))

(define-public crate-pylon-sys-0.1.0 (c (n "pylon-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0z5sdkvbs29y1cq7clqsgxgp7vgh4q4zg9wxy7pb2jvvvz11bblg")))

(define-public crate-pylon-sys-0.1.1 (c (n "pylon-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0n2xzr92hm73scjzk2cp0vnslra4qwbkicbd0mix7kns0d4yafwf")))

(define-public crate-pylon-sys-0.1.2 (c (n "pylon-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "0hjsxyhvqhq4k1qjrzfwqv6naz7p8r778fwijvfhg5b5489zhms9")))

(define-public crate-pylon-sys-0.1.3 (c (n "pylon-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)))) (h "06n8d7bac37rl0ib99dzk65kj7yw5qp5nkxfn3gfkfhk8w6k9rf2")))

