(define-module (crates-io py lo pylon) #:use-module (crates-io))

(define-public crate-pylon-0.1.0 (c (n "pylon") (v "0.1.0") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "pylon-sys") (r "^0.1") (d #t) (k 0)))) (h "1hiad021jl5hn7kd0zlfrwlic31bd853flp2rjqywzdcgh0q6gpp")))

(define-public crate-pylon-0.1.1 (c (n "pylon") (v "0.1.1") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "pylon-sys") (r "^0.1") (d #t) (k 0)))) (h "1983p1l2pybhqlclradrxd4139krhbja7grvwaw9s3j9grsqm1xs")))

(define-public crate-pylon-0.1.2 (c (n "pylon") (v "0.1.2") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "pylon-sys") (r "^0.1") (d #t) (k 0)))) (h "0qzswwirp9hknxn3123ikm32rdl7r0kw89p8vjk9cgj9qgybpyl1")))

(define-public crate-pylon-0.2.0 (c (n "pylon") (v "0.2.0") (d (list (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "imgref") (r "^1.3") (d #t) (k 0)) (d (n "pylon-sys") (r "^0.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)))) (h "1p41pqp3h0lqcpy29n18505dpfcscb389jaymabd636ca8j5r79i")))

