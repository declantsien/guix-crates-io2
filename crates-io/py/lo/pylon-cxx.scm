(define-module (crates-io py lo pylon-cxx) #:use-module (crates-io))

(define-public crate-pylon-cxx-0.1.0 (c (n "pylon-cxx") (v "0.1.0") (d (list (d (n "cxx") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.4") (d #t) (k 1)))) (h "1324541vrfbhmikd4690s7nbz0ik3wgp0p2dlbaj796rnwxda6gj")))

(define-public crate-pylon-cxx-0.1.1 (c (n "pylon-cxx") (v "0.1.1") (d (list (d (n "cxx") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^0.4") (d #t) (k 1)))) (h "0j7p3nwlz9qhprpfm0v78pjlrihvs9f0mwiycj6zmb698vbxl76m")))

(define-public crate-pylon-cxx-0.1.2 (c (n "pylon-cxx") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0wprsp0j11jbr2i6ysfl5vh9i7wnv7868vx0lzb7lxbk1wsyn9hy")))

(define-public crate-pylon-cxx-0.1.3 (c (n "pylon-cxx") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1jq955r4wgg2rglrb4yvx04n2xyddzgk7kpqy94xg0739qqf0vhi") (f (quote (("backtrace" "anyhow/backtrace")))) (y #t)))

(define-public crate-pylon-cxx-0.1.4 (c (n "pylon-cxx") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0sclmrsk84jv4lj0qbnpmj3kzsdsbsbask6ib71p969dghgh7lc6") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.1.5 (c (n "pylon-cxx") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "18mjis5jriwd60ahdq2yzx6pq9xyj8zlc0lvzzwn4r584rym5i0s") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.2.0 (c (n "pylon-cxx") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "01kbgnap128ilbxbrwzj4pzwkivrlc6lf922jgbibl8wga58n23q") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.2.1 (c (n "pylon-cxx") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0mqmqn8w14x6zn9l15gny2gx5nv6jky2ypwbx7q55vvcbar9ks8c") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.2.2 (c (n "pylon-cxx") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1cn7vfpmzk9xdds7pwiim3xy275yk4gbr71jrdxnjzn8sy50v0j6") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.3.0 (c (n "pylon-cxx") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "01xqgm6vzr5yrrq3kh7ijyqw2lsahg7zjwkv2f02dxrl0z81hxmh") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.3.1 (c (n "pylon-cxx") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1px3i56ypik8hr5nl0wc5sz4vvmgs57dpicl0x5bsxc6jcwazgx5") (f (quote (("backtrace"))))))

(define-public crate-pylon-cxx-0.3.2 (c (n "pylon-cxx") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "08j58vg7p1pvn6fyii8g7c39kxnvab1vpr5na0lqvp8pjr4d22ab") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.3 (c (n "pylon-cxx") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0ar3w1kkdaw5ij068hqhl8waqvjgz5hc57xh1w646yyb6306q6z1") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.4 (c (n "pylon-cxx") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0ppd7lrvdky1rzyhd5v6blnz4fh34kp9b006n7v28462lj4cdk9p") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.5 (c (n "pylon-cxx") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1nrj2wycjmpw4v2b80xdd1fnplmf37k6ar990gxmgpy09rwxmirr") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.6 (c (n "pylon-cxx") (v "0.3.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0klbbrpgic7as6ixrpzipfamnlvgbma5q477c38np5y0rj6w5wl1") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.7 (c (n "pylon-cxx") (v "0.3.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0ia4z42f8kgzh1yniaq6sxvfl9x2d9h7bi52w74b3xksw188l9fr") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.3.8 (c (n "pylon-cxx") (v "0.3.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1zbd1cixhn5mzrfz6p9gm194gsn9ma08348c0g1cwclfdyqlp6z3") (f (quote (("stream" "tokio" "tokio-stream") ("backtrace"))))))

(define-public crate-pylon-cxx-0.4.0 (c (n "pylon-cxx") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cxx") (r "^1.0.65") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.65") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "net"))) (o #t) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "0fs3yqm92dqpyail12zkmay3h5m2naxmbw5xajv7wydcidyh22p9") (f (quote (("backtrace")))) (s 2) (e (quote (("stream" "dep:tokio" "dep:tokio-stream"))))))

