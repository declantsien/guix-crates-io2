(define-module (crates-io py li pylib) #:use-module (crates-io))

(define-public crate-pylib-0.1.0 (c (n "pylib") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0q5g32ar58619cz4qhrl0p8mf8kx9cq84hjpaz6ii09ypncwyy6a")))

(define-public crate-pylib-0.2.0 (c (n "pylib") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1vsiv5rdwlb8ab5slla2fk5cfv5qw9rxkhmdp5m0j5l7ban680vk")))

(define-public crate-pylib-0.3.0 (c (n "pylib") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "05fnr6vpkzy6sghjaqbynx0liq9giz4fg8l1nqpcb1q6aw4zcnah")))

