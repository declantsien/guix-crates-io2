(define-module (crates-io py ra pyramid_grok) #:use-module (crates-io))

(define-public crate-pyramid_grok-0.1.0 (c (n "pyramid_grok") (v "0.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "image") (r "^0.14.0") (d #t) (k 0)))) (h "1pz4sd3ig03p1wdx3kqik823966r57siqxaz4aw11pc71z7lqbdn")))

(define-public crate-pyramid_grok-0.2.0 (c (n "pyramid_grok") (v "0.2.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "image") (r "^0.14.0") (d #t) (k 0)))) (h "1ic98azwbd14h62i0790364vcnw96n28n0qj4hivb5vimv6k6c2j")))

