(define-module (crates-io py ra pyrand) #:use-module (crates-io))

(define-public crate-pyrand-0.1.0 (c (n "pyrand") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0swpppig39a38qgjqn47swwnci9599jpklbinis8x4vw76m2gzy5") (f (quote (("default" "rand"))))))

(define-public crate-pyrand-0.1.1 (c (n "pyrand") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1fbh1ik4phq01mvphjq30c1ylqffwhg92smka704akc695yi2r4y") (f (quote (("default" "rand"))))))

