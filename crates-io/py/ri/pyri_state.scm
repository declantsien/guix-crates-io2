(define-module (crates-io py ri pyri_state) #:use-module (crates-io))

(define-public crate-pyri_state-0.1.0 (c (n "pyri_state") (v "0.1.0") (d (list (d (n "bevy_app") (r "^0.13") (o #t) (k 0)) (d (n "bevy_ecs") (r "^0.13") (k 0)) (d (n "bevy_reflect") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "pyri_state_derive") (r "^0.1") (d #t) (k 0)))) (h "0v2qw489wjn2yww8ls5mibw58qprdhfvalinpad46h2jfjx7asxq") (f (quote (("default" "bevy_app" "bevy_reflect")))) (s 2) (e (quote (("bevy_reflect" "dep:bevy_reflect" "bevy_ecs/bevy_reflect") ("bevy_app" "dep:bevy_app"))))))

