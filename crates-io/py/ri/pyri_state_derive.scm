(define-module (crates-io py ri pyri_state_derive) #:use-module (crates-io))

(define-public crate-pyri_state_derive-0.1.0 (c (n "pyri_state_derive") (v "0.1.0") (d (list (d (n "bevy_macro_utils") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0ar376yijf615zapf1mbj2ciks6g88n7qjdgb01lknk1g782pxmd")))

