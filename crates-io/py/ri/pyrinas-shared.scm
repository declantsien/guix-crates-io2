(define-module (crates-io py ri pyrinas-shared) #:use-module (crates-io))

(define-public crate-pyrinas-shared-0.3.2 (c (n "pyrinas-shared") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "15p4d4wn0cc7fhwjj54mq471cz8ycwy2f98vqnjb1wrphd1j47xg")))

