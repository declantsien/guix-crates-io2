(define-module (crates-io py ri pyright-add-noqa) #:use-module (crates-io))

(define-public crate-pyright-add-noqa-0.1.0 (c (n "pyright-add-noqa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pyright") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1b3iyhshy7xbcsbi2y0d1lgzrz2kpz5lhl60xh7qhav2kprf9nvx")))

(define-public crate-pyright-add-noqa-0.2.0 (c (n "pyright-add-noqa") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pyright") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "10dlmm2ngwi2bdvp0syppl5zlz8aiv42m683g2nmq6dqackhi5zw")))

(define-public crate-pyright-add-noqa-0.2.1 (c (n "pyright-add-noqa") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "pyright") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0wynq4bdbiwr9g7vawky8fysh9jr955yj6klw06mjnssxkv465r1")))

