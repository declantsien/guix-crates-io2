(define-module (crates-io py ri pyright) #:use-module (crates-io))

(define-public crate-pyright-0.1.0 (c (n "pyright") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1xnw120adl666qgm9nwfrrc43j11qwhcsz5rgss8m2nzwsvc1dr7")))

(define-public crate-pyright-0.2.0 (c (n "pyright") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0hhbyq7vn75yykc88cac7x2lahx06n74xhmpvq5rv7d7lagns8hf")))

(define-public crate-pyright-0.2.1 (c (n "pyright") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1zlpkgalw9n53s1smh7zb05rzc5gg81f5r45dfrskai6kh28gdar")))

