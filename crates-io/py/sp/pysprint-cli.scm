(define-module (crates-io py sp pysprint-cli) #:use-module (crates-io))

(define-public crate-pysprint-cli-0.1.0-alpha.0 (c (n "pysprint-cli") (v "0.1.0-alpha.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.13") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "tera") (r "^1.7.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)) (d (n "wildmatch") (r "^2.0.0") (d #t) (k 0)))) (h "128vd2rszl3367bg1afljrcgnyl91q02h9v0gk65d14qj6d7hlx7")))

