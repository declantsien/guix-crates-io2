(define-module (crates-io py qi pyqie) #:use-module (crates-io))

(define-public crate-pyqie-0.0.4 (c (n "pyqie") (v "0.0.4") (d (list (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)) (d (n "pyqie-engine") (r "^0.0.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (d #t) (k 0)))) (h "1k0pxw7nnhimgyjcfg6g423d2bwnvbsm1lgl7l41fmqnbrc8fsm5")))

