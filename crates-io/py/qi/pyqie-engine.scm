(define-module (crates-io py qi pyqie-engine) #:use-module (crates-io))

(define-public crate-pyqie-engine-0.0.4 (c (n "pyqie-engine") (v "0.0.4") (d (list (d (n "noise") (r "^0.7") (d #t) (k 0)) (d (n "pyqie-platform") (r "^0.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 0)))) (h "0zcilq3g1g207sf1rnxzzhcbxyg7jm7mvdq855nv1lfdwc7zl8y7")))

