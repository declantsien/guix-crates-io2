(define-module (crates-io py o3 pyo3_decimal) #:use-module (crates-io))

(define-public crate-pyo3_decimal-0.1.0 (c (n "pyo3_decimal") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.16.4") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1.23.1") (d #t) (k 0)))) (h "0x0axrsxc9p65gdx6rjvh1pxdhgp5j4hihz5mq8ikj1pbg2r3cp7") (y #t)))

