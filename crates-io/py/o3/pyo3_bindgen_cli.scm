(define-module (crates-io py o3 pyo3_bindgen_cli) #:use-module (crates-io))

(define-public crate-pyo3_bindgen_cli-0.1.0 (c (n "pyo3_bindgen_cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1hxhkcif6cfcd6gm11xnafr2fnnnrz96pf0rnin9ppifm6fzlq3h") (r "1.70")))

(define-public crate-pyo3_bindgen_cli-0.1.1 (c (n "pyo3_bindgen_cli") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "134zig4zc6rl9wfkwbbdxzp8mmkmxygd3qg3kjl0v5a7m3zrm8l1") (r "1.70")))

(define-public crate-pyo3_bindgen_cli-0.2.0 (c (n "pyo3_bindgen_cli") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1jxn3hcxb6hn6m46p0h86hxz8wsl1xhalpm8ahh88r5d1g9qfhx9") (r "1.70")))

(define-public crate-pyo3_bindgen_cli-0.3.0 (c (n "pyo3_bindgen_cli") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qylk0gbvh33cpm2qjkxw2ga99ajfdhfc1qzzh6r8g81yhiy3m0w") (r "1.74")))

(define-public crate-pyo3_bindgen_cli-0.3.1 (c (n "pyo3_bindgen_cli") (v "0.3.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pmfgca5i6dkyf5d9llcsi14xv50xnqafaimfwv79ii6gbhyxfjs") (r "1.74")))

(define-public crate-pyo3_bindgen_cli-0.4.0 (c (n "pyo3_bindgen_cli") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1g6af6q2l0l4friqywm359dnqvxrvz9bnhdafvq966b7z58m7f7k") (r "1.74")))

(define-public crate-pyo3_bindgen_cli-0.5.0 (c (n "pyo3_bindgen_cli") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "pyo3_bindgen") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1m1x1i1bggwa1hsr18q64x16vzqgbgv9v2v9nqpyxcfp3z9af246") (r "1.74")))

