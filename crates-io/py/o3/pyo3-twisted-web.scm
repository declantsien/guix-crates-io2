(define-module (crates-io py o3 pyo3-twisted-web) #:use-module (crates-io))

(define-public crate-pyo3-twisted-web-0.1.0 (c (n "pyo3-twisted-web") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.11") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (k 0)) (d (n "pyo3-asyncio") (r "^0.17") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.11") (f (quote ("util"))) (d #t) (k 0)))) (h "0is1z8y5q5sknpn0iq08glq8ph20sds0rpfz7sgai52xl5s2vi7m") (r "1.56")))

(define-public crate-pyo3-twisted-web-0.1.1 (c (n "pyo3-twisted-web") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.11") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body") (r "^0.4.4") (d #t) (k 0)) (d (n "pyo3") (r ">=0.17, <0.19") (k 0)) (d (n "pyo3-asyncio") (r ">=0.17, <0.19") (f (quote ("tokio-runtime"))) (d #t) (k 0)) (d (n "tower") (r "^0.4.11") (f (quote ("util"))) (d #t) (k 0)))) (h "02irl4p8i4qv1fd8m02fpid61kydpf8rpmfyq3sb7hfxbmwvxz6j") (r "1.56")))

