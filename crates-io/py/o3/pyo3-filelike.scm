(define-module (crates-io py o3 pyo3-filelike) #:use-module (crates-io))

(define-public crate-pyo3-filelike-0.1.0 (c (n "pyo3-filelike") (v "0.1.0") (d (list (d (n "pyo3") (r ">=0.19.0") (d #t) (k 0)))) (h "19d6y9dgjxb54vjd862qx66cnflcckd55yf0d3lybraaryjb6yiw")))

(define-public crate-pyo3-filelike-0.2.0 (c (n "pyo3-filelike") (v "0.2.0") (d (list (d (n "pyo3") (r ">=0.19.0") (d #t) (k 0)))) (h "18qgaw5aq3x4hy5dvm9bafxlwynw92mrzpddipx5q1bjq7jhyfyd")))

