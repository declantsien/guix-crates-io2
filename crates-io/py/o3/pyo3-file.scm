(define-module (crates-io py o3 pyo3-file) #:use-module (crates-io))

(define-public crate-pyo3-file-0.1.0 (c (n "pyo3-file") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.6.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "19q6sq0zjl3ws19df1mq88sp7srm6415kr44wg62g92zlshmli73")))

(define-public crate-pyo3-file-0.2.0 (c (n "pyo3-file") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.6.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1qvapds8k26lsqm4vmh0jr0dyxa6gkqafdr6xy6sgzmn10qjp52i")))

(define-public crate-pyo3-file-0.3.0 (c (n "pyo3-file") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1qydgvac02vgri14h0c2bp1ckmaad58kf08sq49xpw8jd48k6406")))

(define-public crate-pyo3-file-0.3.1-alpha.1 (c (n "pyo3-file") (v "0.3.1-alpha.1") (d (list (d (n "pyo3") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "147iydpygb381z078xgni68a3x89629j8hj0h4g5y625kkm7sj12")))

(define-public crate-pyo3-file-0.3.1 (c (n "pyo3-file") (v "0.3.1") (d (list (d (n "pyo3") (r "^0.9.2") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "07p1wil53221a9j2vgik0m42qp5228ff4z07v4fdbigqc70rq5i7")))

(define-public crate-pyo3-file-0.3.2 (c (n "pyo3-file") (v "0.3.2") (d (list (d (n "pyo3") (r "^0.11.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0hbx0i8i45hyhmi43cz5k8cg9dgl62r60l5j552pmkn3qkz38ap2")))

(define-public crate-pyo3-file-0.3.3 (c (n "pyo3-file") (v "0.3.3") (d (list (d (n "pyo3") (r "^0.12.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)))) (h "1r5bx0vb15kjng52hqxmpsf0csjaha7xpw529gjhxnvnycijifzw")))

(define-public crate-pyo3-file-0.4.0 (c (n "pyo3-file") (v "0.4.0") (d (list (d (n "pyo3") (r "^0.14.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "1ip3dknaldjranlgjqvyrdzswwj70kpwi3wa8y827i45lf8357yb")))

(define-public crate-pyo3-file-0.5.0 (c (n "pyo3-file") (v "0.5.0") (d (list (d (n "pyo3") (r "^0.16.4") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "1h7cd9dgnaygndbrwhr4rq8mldvp7cg5g577w8bcn0dgfxy3rb7q")))

(define-public crate-pyo3-file-0.7.0 (c (n "pyo3-file") (v "0.7.0") (d (list (d (n "pyo3") (r ">=0.18.1, <0.20") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "0m1naihpcb7bvlsyrjh4czlg7md1jzgdl2f02411iq7brql54rcw")))

(define-public crate-pyo3-file-0.8.0 (c (n "pyo3-file") (v "0.8.0") (d (list (d (n "pyo3") (r ">=0.21, <0.22") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.7") (d #t) (k 2)))) (h "0rpy9xrdqri00p0vgjaa7r0hch0ihwasm7yvzfq6wm1wf9znb1gq")))

