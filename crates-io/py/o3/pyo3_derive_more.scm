(define-module (crates-io py o3 pyo3_derive_more) #:use-module (crates-io))

(define-public crate-pyo3_derive_more-0.1.0 (c (n "pyo3_derive_more") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pm0nbkpz6w9cqxb2sxhx8fj7j50ghl9akws0xk9cs0nf0vcljb1") (r "1.64")))

