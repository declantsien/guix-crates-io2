(define-module (crates-io py o3 pyo3-arraylike) #:use-module (crates-io))

(define-public crate-pyo3-arraylike-0.1.0 (c (n "pyo3-arraylike") (v "0.1.0") (d (list (d (n "ndarray") (r ">=0.13, <0.16") (d #t) (k 0)) (d (n "numpy") (r "^0.19.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("auto-initialize"))) (k 2)))) (h "12zzsb29q462g8b73ygk83r3baigczkj4qmdy72d852h2x2kw48w")))

(define-public crate-pyo3-arraylike-0.1.1 (c (n "pyo3-arraylike") (v "0.1.1") (d (list (d (n "ndarray") (r ">=0.13, <0.16") (d #t) (k 0)) (d (n "numpy") (r "^0.21") (d #t) (k 0)) (d (n "pyo3") (r "^0.21") (f (quote ("auto-initialize"))) (k 2)))) (h "1czwxzcy39l8mikdnl8hn0n5rk4m90nc2cbwksg8mz9qbj64zhmk")))

