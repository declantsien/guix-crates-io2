(define-module (crates-io py o3 pyo3-setup) #:use-module (crates-io))

(define-public crate-pyo3-setup-0.1.0 (c (n "pyo3-setup") (v "0.1.0") (d (list (d (n "cargo-edit") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0y76hnjv7r4z2c3mpnay3681z6acjwdgx1kd7hg60zw3zxlw661l")))

(define-public crate-pyo3-setup-0.1.1 (c (n "pyo3-setup") (v "0.1.1") (d (list (d (n "cargo-edit") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06s61cxnxibbmgsq23w5s47q7h149d89mqjib4c23zgdzqm7smip")))

