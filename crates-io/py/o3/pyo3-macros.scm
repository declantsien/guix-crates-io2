(define-module (crates-io py o3 pyo3-macros) #:use-module (crates-io))

(define-public crate-pyo3-macros-0.13.0-alpha (c (n "pyo3-macros") (v "0.13.0-alpha") (d (list (d (n "pyo3-macros-backend") (r "=0.13.0-alpha") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v7sc5kvbz8l3mn0iwi4fp46nzaaxqxfwkvbwjahm46gaynaflqz")))

(define-public crate-pyo3-macros-0.13.0 (c (n "pyo3-macros") (v "0.13.0") (d (list (d (n "pyo3-macros-backend") (r "=0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05kdwkdk8psfb7plcw4wjmazsb7j1jds2nfm87w59qqkkmv1i0kz")))

(define-public crate-pyo3-macros-0.13.1 (c (n "pyo3-macros") (v "0.13.1") (d (list (d (n "pyo3-macros-backend") (r "=0.13.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13zgvcmzfxl01q2man8aiknd96hvlgkp20mynjjqjrysvcbcafj8")))

(define-public crate-pyo3-macros-0.13.2 (c (n "pyo3-macros") (v "0.13.2") (d (list (d (n "pyo3-macros-backend") (r "=0.13.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fxi5lx5dl7xh469gr5xckyjy3r3c5dqypzxcj0fbhzf1hq2qzx4")))

(define-public crate-pyo3-macros-0.14.0 (c (n "pyo3-macros") (v "0.14.0") (d (list (d (n "pyo3-macros-backend") (r "=0.14.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g2yn293pd18j3wpkqqym6r96v9vbjgbaqn0wgv9jb64a0kp0crx")))

(define-public crate-pyo3-macros-0.14.1 (c (n "pyo3-macros") (v "0.14.1") (d (list (d (n "pyo3-macros-backend") (r "=0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pvaa7ysmx4ap44qx5ni1ji2qaa9dplpl46cr2zc9v6h88d6gf6g")))

(define-public crate-pyo3-macros-0.14.2 (c (n "pyo3-macros") (v "0.14.2") (d (list (d (n "pyo3-macros-backend") (r "=0.14.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16mjcmk0n8zj3v9gj4d31kxcdslkx8l6rnabh1w2r47xrimv6cfd")))

(define-public crate-pyo3-macros-0.14.3 (c (n "pyo3-macros") (v "0.14.3") (d (list (d (n "pyo3-macros-backend") (r "=0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yqwbhwwcrhdbjibj1aglxyfkl64fn1dadvw6q59sy7r4dfgnsij")))

(define-public crate-pyo3-macros-0.14.4 (c (n "pyo3-macros") (v "0.14.4") (d (list (d (n "pyo3-macros-backend") (r "=0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wz5ii5rhs78hkrkpw5zjl5ylcayf20bslxwdrdfqvik3sf6bmlj")))

(define-public crate-pyo3-macros-0.14.5 (c (n "pyo3-macros") (v "0.14.5") (d (list (d (n "pyo3-macros-backend") (r "=0.14.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a4fh24c5q85f31n2rwbqrai2bjprf9kzh6xvpgj8j3hblhwa2zw")))

(define-public crate-pyo3-macros-0.15.0 (c (n "pyo3-macros") (v "0.15.0") (d (list (d (n "pyo3-macros-backend") (r "=0.15.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d6yck1rqjbag6vkjs65lpncfgb9invy754b0419qyl8lg822kr4") (f (quote (("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.15.1 (c (n "pyo3-macros") (v "0.15.1") (d (list (d (n "pyo3-macros-backend") (r "=0.15.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mfp3yz6743vrsp2vh8is3gbyzlxzx4bam5wnhi9g6hz5friww37") (f (quote (("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.16.0 (c (n "pyo3-macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bg5526wqybn162d81fsrix5gliycqrsqy6c7fjnhjckg4akmzyv") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.16.1 (c (n "pyo3-macros") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nd36bsb3rwsy194b5a9vdy38iwl1ipjq3l4ncspv8iav8zgfid8") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.16.2 (c (n "pyo3-macros") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16zskklmikgf3n0irbiz6mzshm1126kh3d98r94v7ar06cnyb5gp") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.16.3 (c (n "pyo3-macros") (v "0.16.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1alnf8bncdxasvzrbq8s4y86cwjw6jk92cr42nvczc99j428958b") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.15.2 (c (n "pyo3-macros") (v "0.15.2") (d (list (d (n "pyo3-macros-backend") (r "=0.15.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fmrzl185i00c6kdvy7icmhhc99c51pyha46incqggk4qvl4gch0") (f (quote (("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.16.4 (c (n "pyo3-macros") (v "0.16.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03sd33lgwd7k4w5wiz1649hhh84bsbs0nqv5azki0klzy90ycz1i") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.16.5 (c (n "pyo3-macros") (v "0.16.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xwh7sl4n73746q80n5m5afd261zg0kxcqfnlr89ik7vbd4c8kr8") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.16.6 (c (n "pyo3-macros") (v "0.16.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.16.6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "089z1x8fm7078kyhpjcsaipd6745jf2rp86rpbkppbam5504k1sn") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.17.0 (c (n "pyo3-macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.17.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hskrz64l2dn6a4ahs6s8pgpl6a5vx0a3q8gx3d5s90w5qz9fqci") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.17.1 (c (n "pyo3-macros") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.17.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iq5ygyg21dd38w6n11xipp14vg46pgi4m363gf1dwn2pkaci09f") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.17.2 (c (n "pyo3-macros") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.17.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17fp5ip368q44pl69w6s7qg6q03gsxnml2j8rjzqi7id9a04pcky") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.17.3 (c (n "pyo3-macros") (v "0.17.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.17.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a3bz4g8dhy7620zca4pb39yipm96pf3c8b86b4v2dp2cq94l54l") (f (quote (("pyproto" "pyo3-macros-backend/pyproto") ("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.18.0 (c (n "pyo3-macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.18.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i57g2rl12pbvla3lgmk77sb56s82329n46j8h566q4xpaxj5f0n") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.18.1 (c (n "pyo3-macros") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.18.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ibc1py7a53jyb5y9lr4p6pyy1pjwjdmals61ivakiknfhhcyi5x") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.18.2 (c (n "pyo3-macros") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.18.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f54sifkmrsg4w6vjs1r7i7dqiki8rdgydgjw64x7v352kk1i3cp") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.18.3 (c (n "pyo3-macros") (v "0.18.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.18.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13b454p012l1487lbfqkr952qifsl88arldv4m5mmz5kv9arrlx9") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.19.0 (c (n "pyo3-macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.19.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "185fv1604l8rjkg55l68xdzj9ycan06b4y9n8qf7ispjk44dr3xp") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.19.1 (c (n "pyo3-macros") (v "0.19.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.19.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18n57jpi3292jhzmg5bjr1343zl93gmvxz21m1j5jdfxl73awp4a") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.19.2 (c (n "pyo3-macros") (v "0.19.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.19.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lggr5pnpmdj0cznlhw7ykm1qka3wlymwzfxqql6a4vyb6clrsyz") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.20.0 (c (n "pyo3-macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.20.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0frqz162dyfv7klnyjjgmz3vfxf8x2fv6cmq9px1pahpyxr31ifs") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.20.1 (c (n "pyo3-macros") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m03ky3ysxic4y7fyk8xqr4i5irli7zwgj0imd7n0y55xb3hsjqz") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.20.2 (c (n "pyo3-macros") (v "0.20.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.20.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lsfyx1shdyi7zybs3maakh2h3zsij3l5wap35qval0dwjs3ixq5") (f (quote (("multiple-pymethods") ("abi3" "pyo3-macros-backend/abi3"))))))

(define-public crate-pyo3-macros-0.20.3 (c (n "pyo3-macros") (v "0.20.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.20.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n61s98qb2qc1wlda3bz4r0wi0vsr9p4lj2yr5g0bf01z8hcf1bk") (f (quote (("multiple-pymethods"))))))

(define-public crate-pyo3-macros-0.21.0-beta.0 (c (n "pyo3-macros") (v "0.21.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.21.0-beta.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jrfimm88y1qhndhsy3rm6yqfnkajwpzijckncgfnck85kg8gdzh") (f (quote (("multiple-pymethods") ("experimental-declarative-modules") ("experimental-async" "pyo3-macros-backend/experimental-async"))))))

(define-public crate-pyo3-macros-0.21.0 (c (n "pyo3-macros") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.21.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pnf06qg7qh05capncliagm4x2ijn83sd0h7gms4myb5b4y4k02f") (f (quote (("multiple-pymethods") ("experimental-declarative-modules") ("experimental-async" "pyo3-macros-backend/experimental-async"))))))

(define-public crate-pyo3-macros-0.21.1 (c (n "pyo3-macros") (v "0.21.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.21.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04ym6vg3mn4z199lm01swh883abfhcl74f396n7b5sqirsgik2jb") (f (quote (("multiple-pymethods") ("experimental-declarative-modules") ("experimental-async" "pyo3-macros-backend/experimental-async"))))))

(define-public crate-pyo3-macros-0.21.2 (c (n "pyo3-macros") (v "0.21.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-macros-backend") (r "=0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g65z9yj2iffjrkrkzan9hwhhj7rrchh7lfv64dy30h6zill1cvp") (f (quote (("multiple-pymethods") ("experimental-declarative-modules") ("experimental-async" "pyo3-macros-backend/experimental-async"))))))

