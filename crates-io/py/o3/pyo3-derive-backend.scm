(define-module (crates-io py o3 pyo3-derive-backend) #:use-module (crates-io))

(define-public crate-pyo3-derive-backend-0.3.0 (c (n "pyo3-derive-backend") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0a7j4ldf1z62f5y5yxwgqvig6bfjy79rbvhxkbli9an8njm4bjw6")))

(define-public crate-pyo3-derive-backend-0.3.1 (c (n "pyo3-derive-backend") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0xn28ki6llb4nmz15xv7fw55vshg9j2fyp2mhpz201vlqvd5qszh")))

(define-public crate-pyo3-derive-backend-0.3.2 (c (n "pyo3-derive-backend") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "03187irxps6zkk6w6996rz62w9j1jyl9j9h67byyza3ypqxdvjn9")))

(define-public crate-pyo3-derive-backend-0.4.0 (c (n "pyo3-derive-backend") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.9") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1ypjkl3ry1i0zvmg2hnigcxw9v66b2af4k9k2kydvc93ldban7zj")))

(define-public crate-pyo3-derive-backend-0.4.1 (c (n "pyo3-derive-backend") (v "0.4.1") (d (list (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j0h2h6r78nh238a05960kwmfc4jdxv1xrqaz145xwlhjdq5n2hg")))

(define-public crate-pyo3-derive-backend-0.5.0-alpha.1 (c (n "pyo3-derive-backend") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l726n8kzjjm7vinhkc4crjiy73rdd9908wjcvfy30cr5598zq4f")))

(define-public crate-pyo3-derive-backend-0.5.0-alpha.2 (c (n "pyo3-derive-backend") (v "0.5.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zd2173zrizvk93b54r2y8qlqcvbkad3d6f8slm6hnm76qvrwnf7")))

(define-public crate-pyo3-derive-backend-0.5.0-alpha.3 (c (n "pyo3-derive-backend") (v "0.5.0-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1agx2kgbics9axm3hv965nc5b7hsjiyaqwkifiws40833aza9xxq")))

(define-public crate-pyo3-derive-backend-0.5.0 (c (n "pyo3-derive-backend") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zxbckqmaxsbs4daj73xcq91n2jx53pssdbvmig4ji5xrdf7m4h0")))

(define-public crate-pyo3-derive-backend-0.5.1 (c (n "pyo3-derive-backend") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ahycqq16ampv697z5pccws8f2d9my5bb2iw8ms0hzmvsbjiazpq") (y #t)))

(define-public crate-pyo3-derive-backend-0.5.2 (c (n "pyo3-derive-backend") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dygz4978n10vwkkandn6i7sbblnbcrip61xbdhhmr0arg7wj3f1")))

(define-public crate-pyo3-derive-backend-0.5.3 (c (n "pyo3-derive-backend") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l8lw1zx2xb790prks01nh7g1r534wavpfixbfc1lg08gp1gyx4i")))

(define-public crate-pyo3-derive-backend-0.6.0-alpha.1 (c (n "pyo3-derive-backend") (v "0.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07yxard49yy9y0wimgvqzascrgrph04lzkmd7x7yqgvnyjlpdpw9")))

(define-public crate-pyo3-derive-backend-0.6.0-alpha.2 (c (n "pyo3-derive-backend") (v "0.6.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f5yz4qily9dsvwc5f5lwa378hx1x2mf02433z6wjfg71i771jx9")))

(define-public crate-pyo3-derive-backend-0.5.4 (c (n "pyo3-derive-backend") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06m6gm76hz2vwlnd12n88iddiddn0dx5nrxls3x599aqx7avd7lf")))

(define-public crate-pyo3-derive-backend-0.6.0-alpha.4 (c (n "pyo3-derive-backend") (v "0.6.0-alpha.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j9k03aq06xpjagcrxlmppc7mrn5b297ylaa8fmlxi2ibiggwpny")))

(define-public crate-pyo3-derive-backend-0.6.0 (c (n "pyo3-derive-backend") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cgavfz2y4594bdy57dkf71c0lyrykzw6mj9wwcqpw36qphkjdkq")))

(define-public crate-pyo3-derive-backend-0.7.0-alpha.1 (c (n "pyo3-derive-backend") (v "0.7.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14n00b9pxwp3w5p56gglc4lrf39pdqyj4sdm2gq9p1pa54jf5h0g")))

(define-public crate-pyo3-derive-backend-0.7.0 (c (n "pyo3-derive-backend") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11mij5fa8g8j7scnkp02qrb2mqamkgj8rn42gk6iax81nf5axmyr")))

(define-public crate-pyo3-derive-backend-0.8.0 (c (n "pyo3-derive-backend") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0js8x6rv5si0s1bdxmz3w4xnrh6a815ppm6s64rsc1idz95d3mi0") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.8.1 (c (n "pyo3-derive-backend") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02pgpdhy130nanw81c6s06725bi5zdwd0acf91dwlvwqrdhalz0s") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.8.2 (c (n "pyo3-derive-backend") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16k5ac0bx1kjrgwxnmb9vc7inpmsv9ac58xm8qg2dzhgzg1cln74") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.8.3 (c (n "pyo3-derive-backend") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gjk7npbdrm82jjc4nw9bpzdvs1pcpk65xcn1nh08b589598131v") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.8.4 (c (n "pyo3-derive-backend") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cchywl7kl0c4zk9v53mdcn416c3aya6y0vxz2l49cwp7vxmcvlz") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.8.5 (c (n "pyo3-derive-backend") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0my0zj3z5wy7yrzg8mpx604gg7xb1pz05f2wq8y3giyqgwixi0j8") (f (quote (("unsound-subclass"))))))

(define-public crate-pyo3-derive-backend-0.9.0-alpha.1 (c (n "pyo3-derive-backend") (v "0.9.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a58hp2k36jx9vhsc6h1x23csyi5dqw35v0rqafrnnj1yfc37h30")))

(define-public crate-pyo3-derive-backend-0.9.0 (c (n "pyo3-derive-backend") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "072awc805jlmqny1a99i0i9vgggp47vysjw15gqf3cl9nz2csx7x")))

(define-public crate-pyo3-derive-backend-0.9.1 (c (n "pyo3-derive-backend") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0la8q3lpjmbxcwzn4ay9q32icy4jxjcw5qmn76286jcc3afnnrdq")))

(define-public crate-pyo3-derive-backend-0.9.2 (c (n "pyo3-derive-backend") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0r0v5mqkvr4w25blxx5sw8fihk26ab0ldz63nxh2apb8xfn7lg2f")))

(define-public crate-pyo3-derive-backend-0.10.0 (c (n "pyo3-derive-backend") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1g66bg0fpryz2cg9xfb2pbcd2q4ll68d1c2jwx3y1fqfw15ggak1")))

(define-public crate-pyo3-derive-backend-0.10.1 (c (n "pyo3-derive-backend") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0vb43p11nkr1ghpfids0m7m2s1j98ab6ragwdmash9ps6lp5skgs")))

(define-public crate-pyo3-derive-backend-0.11.0 (c (n "pyo3-derive-backend") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1ij9bq8g51f6v3i9qkvydw1v1xim6mg9hnbxkwj6lwmzp75g5fyr")))

(define-public crate-pyo3-derive-backend-0.11.1 (c (n "pyo3-derive-backend") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1cy2ghl0mp7sxbxrlvkj590fify6z7y1z4vlx8lhsywnyq5hgbaq")))

(define-public crate-pyo3-derive-backend-0.12.0 (c (n "pyo3-derive-backend") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1l5khvmzqm0qpghpba1p1vwnrbah6988hgrwmwpfxbi2bxdna8fh")))

(define-public crate-pyo3-derive-backend-0.12.1 (c (n "pyo3-derive-backend") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "026hp8d51ijn9fmcx1ryqhgcidbi5drdc3js2qkbbzq132swp9rs")))

(define-public crate-pyo3-derive-backend-0.12.2 (c (n "pyo3-derive-backend") (v "0.12.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1j8dzznrhrwymsyvg9khmw0xp8b0ai73cfzm5cvsrjayikcaz3mm")))

(define-public crate-pyo3-derive-backend-0.12.3 (c (n "pyo3-derive-backend") (v "0.12.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0sh4j66gqvan9fk4wjycqx5qrnlmirycqnp8nxd8ijss17xwkqnf")))

(define-public crate-pyo3-derive-backend-0.12.4 (c (n "pyo3-derive-backend") (v "0.12.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0brj5a68y09c8lf0w022hxg3x7p9p6sp0hsgbfbdkcypdvmx1v0h")))

(define-public crate-pyo3-derive-backend-0.12.5 (c (n "pyo3-derive-backend") (v "0.12.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "02laxpajichx0nn0g9qpnfym8d8dr4j9baqxn9f0ixh61hd5mpxg")))

