(define-module (crates-io py o3 pyo3-prost) #:use-module (crates-io))

(define-public crate-pyo3-prost-0.1.0 (c (n "pyo3-prost") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "12bdxhypfkzkzwy4zlag67xpcj6h5vha4qr6z6gq30gk94hcxga5")))

(define-public crate-pyo3-prost-0.2.0 (c (n "pyo3-prost") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9dicv1226lcy53hmg3wspf6yxy7yx7lc44dys2mcp0qb98709i")))

