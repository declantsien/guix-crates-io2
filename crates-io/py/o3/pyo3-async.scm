(define-module (crates-io py o3 pyo3-async) #:use-module (crates-io))

(define-public crate-pyo3-async-0.1.0 (c (n "pyo3-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)))) (h "0pgj6kvdvjp34lwwlddx7bavf44vp3zm81cr401waj6wxg85hh6j")))

(define-public crate-pyo3-async-0.2.0 (c (n "pyo3-async") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)))) (h "1h63ygpa2nxvi1n3kckrbqz7y0nbzwwljfqrwjx8y4cjcylxl5d1") (f (quote (("default" "unbind-gil")))) (s 2) (e (quote (("unbind-gil" "dep:pin-project"))))))

(define-public crate-pyo3-async-0.3.0 (c (n "pyo3-async") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "pyo3-async-macros") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "062g02vglkxycgy47slxs3g909d0szdk5n1dnvxmiksvfx0y5m83") (f (quote (("default" "macros" "allow-threads")))) (s 2) (e (quote (("macros" "dep:pyo3-async-macros") ("allow-threads" "dep:pin-project"))))))

(define-public crate-pyo3-async-0.3.1 (c (n "pyo3-async") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "pyo3-async-macros") (r "=0.3.1") (o #t) (d #t) (k 0)))) (h "1vh3qqlipjc5z813g1sncw5s2klklmashrh9jv38w90z1jwrg42c") (f (quote (("default" "macros" "allow-threads")))) (s 2) (e (quote (("macros" "dep:pyo3-async-macros") ("allow-threads" "dep:pin-project"))))))

(define-public crate-pyo3-async-0.3.2 (c (n "pyo3-async") (v "0.3.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (o #t) (d #t) (k 0)) (d (n "pyo3") (r ">=0.18, <0.21") (d #t) (k 0)) (d (n "pyo3-async-macros") (r "=0.3.2") (o #t) (d #t) (k 0)))) (h "1ch6q25jl1jl1f2wyrr09g78in5b6ird3i2sqay9w9s79j3z3hmb") (f (quote (("default" "macros" "allow-threads")))) (s 2) (e (quote (("macros" "dep:pyo3-async-macros") ("allow-threads" "dep:pin-project"))))))

