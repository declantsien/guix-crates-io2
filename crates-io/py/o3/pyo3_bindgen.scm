(define-module (crates-io py o3 pyo3_bindgen) #:use-module (crates-io))

(define-public crate-pyo3_bindgen-0.1.0 (c (n "pyo3_bindgen") (v "0.1.0") (d (list (d (n "pyo3_bindgen_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1vz34zj2qd6nm800vcxyps4nda5h98k9di6hyf0nqzscbm5lkd9c") (f (quote (("macros" "pyo3_bindgen_macros") ("default" "macros")))) (r "1.70")))

(define-public crate-pyo3_bindgen-0.1.1 (c (n "pyo3_bindgen") (v "0.1.1") (d (list (d (n "pyo3_bindgen_engine") (r "^0.1.1") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "14m6akhxnamf5p5kxflmdq8ckk3gjqjm6c2q7p0i34sz1ddm3sg5") (f (quote (("macros" "pyo3_bindgen_macros") ("default" "macros")))) (r "1.70")))

(define-public crate-pyo3_bindgen-0.2.0 (c (n "pyo3_bindgen") (v "0.2.0") (d (list (d (n "pyo3_bindgen_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1pahwnqslmkgpw84w177732ar7r1755h0x5kfjjsyj2qhlap95m5") (f (quote (("macros" "pyo3_bindgen_macros") ("default" "macros")))) (r "1.70")))

(define-public crate-pyo3_bindgen-0.3.0 (c (n "pyo3_bindgen") (v "0.3.0") (d (list (d (n "pyo3_bindgen_engine") (r "^0.3.0") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0n17ci38c481h2pc5isdqbry0kn773lbmy45kbh2yp7vdfzqpi5i") (f (quote (("macros" "pyo3_bindgen_macros") ("default" "macros")))) (r "1.74")))

(define-public crate-pyo3_bindgen-0.3.1 (c (n "pyo3_bindgen") (v "0.3.1") (d (list (d (n "pyo3_bindgen_engine") (r "^0.3.1") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "07v51isawi2x3xndn54rx4d13m8d2msvl7ggc4mq06p1271ny5v2") (f (quote (("macros" "pyo3_bindgen_macros") ("default")))) (r "1.74")))

(define-public crate-pyo3_bindgen-0.4.0 (c (n "pyo3_bindgen") (v "0.4.0") (d (list (d (n "pyo3") (r "^0.20") (k 0)) (d (n "pyo3_bindgen_engine") (r "^0.4.0") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1illr05wynfrjk3450agszsgnj2fpi8p4rp0akxnlmvsaxa20v26") (f (quote (("macros" "pyo3_bindgen_macros") ("default" "macros")))) (r "1.74")))

(define-public crate-pyo3_bindgen-0.5.0 (c (n "pyo3_bindgen") (v "0.5.0") (d (list (d (n "pyo3") (r "^0.21") (k 0)) (d (n "pyo3_bindgen_engine") (r "^0.5.0") (d #t) (k 0)) (d (n "pyo3_bindgen_macros") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1l64ji67yrkrjg8pkjv26z4my3ycv1ik1qw8z6qccd9l29xhb7jg") (f (quote (("numpy" "pyo3_bindgen_engine/numpy") ("default")))) (s 2) (e (quote (("macros" "dep:pyo3_bindgen_macros")))) (r "1.74")))

