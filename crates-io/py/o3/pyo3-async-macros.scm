(define-module (crates-io py o3 pyo3-async-macros) #:use-module (crates-io))

(define-public crate-pyo3-async-macros-0.3.0 (c (n "pyo3-async-macros") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.19") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1v7dc264c2d1f83hapvrwmnnpvmbf21hvahvs6n99k41j7vvs4iz")))

(define-public crate-pyo3-async-macros-0.3.1 (c (n "pyo3-async-macros") (v "0.3.1") (d (list (d (n "pyo3") (r "^0.19") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mlqvgmwf6zjp6870bvw8dsdxcp2ivh4cy1fcx3w5a9fdnqx0434")))

(define-public crate-pyo3-async-macros-0.3.2 (c (n "pyo3-async-macros") (v "0.3.2") (d (list (d (n "pyo3") (r ">=0.18, <0.21") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0g41cdgx99m549l2dkpi5vpmxj1j507s895rgfc9x0n0570av5yw")))

