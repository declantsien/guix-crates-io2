(define-module (crates-io py o3 pyo3-anyio) #:use-module (crates-io))

(define-public crate-pyo3-anyio-0.1.0 (c (n "pyo3-anyio") (v "0.1.0") (d (list (d (n "async-oneshot") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "pyo3") (r "0.16.*") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1s6bp657l67wyrm6f0v9kpgfqwq7mklpp49vdq65gmp35dbgjvxg")))

(define-public crate-pyo3-anyio-0.2.0 (c (n "pyo3-anyio") (v "0.2.0") (d (list (d (n "async-oneshot") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "pyo3") (r "0.16.*") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0clfjd15hcfxcjph96vjlrymg30mdqvn3c8lwnaj07yihzbmi2z6")))

(define-public crate-pyo3-anyio-0.3.0 (c (n "pyo3-anyio") (v "0.3.0") (d (list (d (n "async-oneshot") (r "^0.5.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "pyo3") (r "0.16.*") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0p0dvkl5w75bg5wf4p96scc423ws40a5xqh3ymim6mq1nfcr2x0p")))

