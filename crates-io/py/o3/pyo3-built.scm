(define-module (crates-io py o3 pyo3-built) #:use-module (crates-io))

(define-public crate-pyo3-built-0.1.0 (c (n "pyo3-built") (v "0.1.0") (d (list (d (n "built") (r "^0.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0") (d #t) (k 0)))) (h "1l53hqwgcwc1l49xax3pwiwibbjzphn3amrifg4pd6lrpsbv51wg")))

(define-public crate-pyo3-built-0.2.0 (c (n "pyo3-built") (v "0.2.0") (d (list (d (n "built") (r "^0.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.7.0") (d #t) (k 0)))) (h "0q184p8dhww8465inblnra0y15inmnld4ymm68kdija5mqcjp219")))

(define-public crate-pyo3-built-0.3.0 (c (n "pyo3-built") (v "0.3.0") (d (list (d (n "built") (r "^0.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)))) (h "0y51ghw9cx81y5ylfdsh5g6y363zd2yi5fgv7ia3167n6dnddsgj")))

(define-public crate-pyo3-built-0.4.0 (c (n "pyo3-built") (v "0.4.0") (d (list (d (n "built") (r "^0.4.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)))) (h "0bfn9v8szpzgxpy3lp6w7d8786yi4m8lh1wbxy75xp3dzyhbbrmd")))

(define-public crate-pyo3-built-0.4.1 (c (n "pyo3-built") (v "0.4.1") (d (list (d (n "built") (r "^0.4.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (d #t) (k 0)))) (h "17bnp1qd359smmvkhigr4wg77glf59a12b3h7z2j2rfcvhzckb8f")))

(define-public crate-pyo3-built-0.4.2 (c (n "pyo3-built") (v "0.4.2") (d (list (d (n "built") (r "^0.4.0") (f (quote ("chrono"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.11.0") (d #t) (k 0)))) (h "0v320cv2x38ffyr8rqn3li8ki7w178bsnc0qhjazrdq3g60jsnkp")))

(define-public crate-pyo3-built-0.4.3 (c (n "pyo3-built") (v "0.4.3") (d (list (d (n "pyo3") (r "^0.11.0") (d #t) (k 0)))) (h "0air9hbyw4mgxq408cvy7c36rmy9fmpb41rqz51b9pvqyjvnc59g")))

(define-public crate-pyo3-built-0.4.4 (c (n "pyo3-built") (v "0.4.4") (d (list (d (n "pyo3") (r "^0.12.0") (d #t) (k 0)))) (h "131ca3bqlnp20c3w01hxmmfbm2z7r0q67ijws6mkjrvj6cv9xdi6")))

(define-public crate-pyo3-built-0.4.5 (c (n "pyo3-built") (v "0.4.5") (h "1jyrcqvhn6ag4bwjcsxbh93sf182ivg8ca2mwwm1sncdg4wrl7xh")))

(define-public crate-pyo3-built-0.4.6 (c (n "pyo3-built") (v "0.4.6") (h "1kb6m2vr82528lavh6mwl8nyb8b8jsk6ifp6y5z03593jzvmng44")))

(define-public crate-pyo3-built-0.4.7 (c (n "pyo3-built") (v "0.4.7") (h "1pm51k7cx3n0m4pvdsd9kx8p9ja5z35l1dpf3vfjrawc1x75fvdy")))

(define-public crate-pyo3-built-0.5.0 (c (n "pyo3-built") (v "0.5.0") (h "1pkc74nx65va3n4pcw4lkccxsmvqw8wv856pl5jnc5llvid6bvim")))

