(define-module (crates-io py o3 pyo3io-macros) #:use-module (crates-io))

(define-public crate-pyo3io-macros-0.1.0 (c (n "pyo3io-macros") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("smallvec" "macros"))) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1ja9xwz4zqxqyqxbgdzvdjx30p7siz6l6j175xcjg0cg0hr5v2zz") (r "1.75")))

(define-public crate-pyo3io-macros-0.1.1 (c (n "pyo3io-macros") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("smallvec" "macros"))) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "04wmlw40sb5lyq67wjlsiw9l98gdzb29by8gc39dq9f2v2nwgws1") (r "1.75")))

(define-public crate-pyo3io-macros-0.1.2 (c (n "pyo3io-macros") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("smallvec" "macros"))) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "09zh46z5gppqqq72g43mfl5kja7dja9353p006m3kidgl75wbmqc") (r "1.75")))

