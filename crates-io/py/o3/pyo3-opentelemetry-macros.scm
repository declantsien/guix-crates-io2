(define-module (crates-io py o3 pyo3-opentelemetry-macros) #:use-module (crates-io))

(define-public crate-pyo3-opentelemetry-macros-0.1.0 (c (n "pyo3-opentelemetry-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0qciwrksa8hs2nhc8mwn0qfif663nnm7a0yiig5c0ckk4vw37ycn") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.2.0-rc.0 (c (n "pyo3-opentelemetry-macros") (v "0.2.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0gbs2bj2afgsx2xyl71air8ya8z12l038k3c27bmadc694bfp1a1") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.2.0 (c (n "pyo3-opentelemetry-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1n5ww7gsdigbgbm5knjjjd9ldji9swqnjzh5bd0s80zzwl4vypyg") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.0-rc.0 (c (n "pyo3-opentelemetry-macros") (v "0.3.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "19kvkb787xx73lnsnasxlf8ydyyf1glfadph7jnb79kjr1saylvl") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.0 (c (n "pyo3-opentelemetry-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "10fvan6lx3h9nz36j7cz7sny1w75rj7l38dbmy34n7nx808d30cm") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.1-rc.0 (c (n "pyo3-opentelemetry-macros") (v "0.3.1-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1cmv6bz3f1j7yd0hcwm5q3vww9kjnr2pldn1ygjrdba9pzny8cfh") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.1-rc.1 (c (n "pyo3-opentelemetry-macros") (v "0.3.1-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "1wblairb1rnj9hmm5knn09rzzdh7vkxxqqnaqnj73m3xy7ji2xqk") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.1 (c (n "pyo3-opentelemetry-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0n4cyhyvi49c11spx1yygm0i4l6dq7l6r99znr4c484yjfkvrlwa") (r "1.65.0")))

(define-public crate-pyo3-opentelemetry-macros-0.3.2-rc.0 (c (n "pyo3-opentelemetry-macros") (v "0.3.2-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.14") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "10hizyi0ja1yvp5h7nyib2x4fyzyzlavq2wsyhrmq4agb75xmrsi") (r "1.65.0")))

