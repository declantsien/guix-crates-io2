(define-module (crates-io py o3 pyo3-testing) #:use-module (crates-io))

(define-public crate-pyo3-testing-0.1.0 (c (n "pyo3-testing") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "1026aqkivpngp2jfam9g0y9z7a00qbrgc38lrvlpdbgj3rgzw9sw")))

(define-public crate-pyo3-testing-0.2.0 (c (n "pyo3-testing") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "1qhyvkf9l3zfsw92jb24mjfr7b79rh6570n34fslf9whzd6i6j77")))

(define-public crate-pyo3-testing-0.3.0 (c (n "pyo3-testing") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "105zapnhhiv2n8ll53y7xz6rsiwb62qzq3wza4nlavqklfxy4lv7")))

(define-public crate-pyo3-testing-0.3.1 (c (n "pyo3-testing") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "1z307w676rs2hynrh2gsphq6jmg8dn6zj58ldrj4i9gp66v88iw5")))

(define-public crate-pyo3-testing-0.3.2 (c (n "pyo3-testing") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "0idnjf2zs0hxf4p21rqsx3i9nxz5m6py2wc1jlgzh6kiv8bklajv")))

(define-public crate-pyo3-testing-0.3.3 (c (n "pyo3-testing") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "0hvrmvr4cpf69kjm134dbwciabyjlbykylyym02zm5yng7hgfyi8")))

(define-public crate-pyo3-testing-0.3.4 (c (n "pyo3-testing") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 0)))) (h "08clnyxmhvf4f5mm2lmy4b0lbd4p8r5fmfp4fh9cdja3mn0baywr")))

