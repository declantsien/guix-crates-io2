(define-module (crates-io py o3 pyo3-macros-backend) #:use-module (crates-io))

(define-public crate-pyo3-macros-backend-0.13.0-alpha (c (n "pyo3-macros-backend") (v "0.13.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1njnzmzc7c4rv7mkk8ancbykixzcvqi5g0fc401xj3wpngk9qd3i")))

(define-public crate-pyo3-macros-backend-0.13.0 (c (n "pyo3-macros-backend") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1qnc3r1v0ghj7zmxxphn1zwaccpqnfv2r3v93jbsbw3nvyzs0kgw")))

(define-public crate-pyo3-macros-backend-0.13.1 (c (n "pyo3-macros-backend") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1ybxrwifk8633qgmbrj63gpbbjwly10ar2zdclsz0250rnmhq8qm")))

(define-public crate-pyo3-macros-backend-0.13.2 (c (n "pyo3-macros-backend") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0rjxayd78l10hnyphk03bcvhm0jpsvnzn07lczhy7jsgv3jrgc47")))

(define-public crate-pyo3-macros-backend-0.14.0 (c (n "pyo3-macros-backend") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.0") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "10p0c252x7qs42kaf16igc64j82m1pmjkj6y56lyd90vzknwszd0")))

(define-public crate-pyo3-macros-backend-0.14.1 (c (n "pyo3-macros-backend") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.1") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0j4ziavfgs9gjyhwj33qqgil7yklxsah36f0kjad819g8bfgdpcs")))

(define-public crate-pyo3-macros-backend-0.14.2 (c (n "pyo3-macros-backend") (v "0.14.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.2") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0wmrarmajmw8gnv8arfrn4qa00fxn1k2frhwjcz8576v6sx5j762")))

(define-public crate-pyo3-macros-backend-0.14.3 (c (n "pyo3-macros-backend") (v "0.14.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.3") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "172gwvf22vbnc1fvf1i7jva4c5hsh4093lc4ljxik0pr12d1cf3y")))

(define-public crate-pyo3-macros-backend-0.14.4 (c (n "pyo3-macros-backend") (v "0.14.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.4") (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1gkx7h20cc739iqjkar42dx2hc6fzwy9093x120cnfjjfm4mlhhb")))

(define-public crate-pyo3-macros-backend-0.14.5 (c (n "pyo3-macros-backend") (v "0.14.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.14.5") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1ps068jqyq1275zxxbzn6hyz9lkfz35az8waj6mzlji2jg2kyqki")))

(define-public crate-pyo3-macros-backend-0.15.0 (c (n "pyo3-macros-backend") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.15.0") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0wpn55dnh0qgckrccdq1ka8b9j7q75pdai96p2dij18xqj5d3lxk")))

(define-public crate-pyo3-macros-backend-0.15.1 (c (n "pyo3-macros-backend") (v "0.15.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.15.1") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "12i1n1j8l4q4lzalsvvlw2pak1h8wnz3xcn7y82s2jgf4pl0jkzl")))

(define-public crate-pyo3-macros-backend-0.16.0 (c (n "pyo3-macros-backend") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.16.0") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1a9yqfrqq3y9i4bb7va93jwld1lzfajj1pclyx3ppb61r0k42r4h") (f (quote (("pyproto"))))))

(define-public crate-pyo3-macros-backend-0.16.1 (c (n "pyo3-macros-backend") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.16.1") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "000cz1r5s56bpf73js5l5arwdiha814z28lvdnvdrhlnr9wyywjf") (f (quote (("pyproto"))))))

(define-public crate-pyo3-macros-backend-0.16.2 (c (n "pyo3-macros-backend") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.16.2") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0nxpdiajcpsrpiqldxgfyjfi6aih73gqi04mh9lvkdrvgajkmq1r") (f (quote (("pyproto"))))))

(define-public crate-pyo3-macros-backend-0.16.3 (c (n "pyo3-macros-backend") (v "0.16.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1g4d36viqwz422f30i8zsb62rxpp3x4gb8gdb2cimiamd9z73i5n") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.15.2 (c (n "pyo3-macros-backend") (v "0.15.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "^0.15.2") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "15bhc1xib9yz4l1sd2lk3nc7scbqsjfvgvlr3mj0xq0jqh92i32s")))

(define-public crate-pyo3-macros-backend-0.16.4 (c (n "pyo3-macros-backend") (v "0.16.4") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "08km86i2s2rr7gv3w9cp2p5hyzzn0sfj10cdirl86nd0b372yd2r") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.16.5 (c (n "pyo3-macros-backend") (v "0.16.5") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1bvzvdx2a6hhliny12n2vy7v7gbsgzanxjckjr1cbxbkizss1gak") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.16.6 (c (n "pyo3-macros-backend") (v "0.16.6") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1f0y9vxmyq1cidk544pwg3my30f6xfqfgf42grw4gx4q5pl687v1") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.17.0 (c (n "pyo3-macros-backend") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "097l7j694qj8kg0r3cymp81ikdrnkgjg7pc70nsdndd1xm23gnzy") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.17.1 (c (n "pyo3-macros-backend") (v "0.17.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1117i7x73pi8vc9lnaqi5qxkfzfd4nv2mk3jhakrk4z1grv2lxc5") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.17.2 (c (n "pyo3-macros-backend") (v "0.17.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "1mm6prr8jlhx70nahr5fq8v5ziidby3scrxcsx9w4j3kdagv8azj") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.17.3 (c (n "pyo3-macros-backend") (v "0.17.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0zvpjwcvhlypn1qzwnqsf0a17cbkxl335c5bxg6z1lm2g3lrppy8") (f (quote (("pyproto") ("abi3"))))))

(define-public crate-pyo3-macros-backend-0.18.0 (c (n "pyo3-macros-backend") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "02pivx0kxypd1zfas782p5pas3g4ydq0f5iicaq9vjgp0j0qkbl4") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.18.1 (c (n "pyo3-macros-backend") (v "0.18.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0k0armdwc109d3z0i4l2b7069vaxz36331nia1iz6q04wgc467yw") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.18.2 (c (n "pyo3-macros-backend") (v "0.18.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0ql40lj33xhk7s09gkj83jf6i8l942m0hpj3dskgrqswz0l123lf") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.18.3 (c (n "pyo3-macros-backend") (v "0.18.3") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "062rxf7cj1dn82yiaws3dmxw5qq9ssccq92jgdc210y4lh4gznlp") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.19.0 (c (n "pyo3-macros-backend") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "03qax3j186jm7p1a3q5l524lrksd4zg0adarwwpkwz12y627pnk0") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.19.1 (c (n "pyo3-macros-backend") (v "0.19.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "12zs1vx0h4hainb0lpnw8knd9i9l0g2rdzdnrmb1bnv0n75qrdz0") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.19.2 (c (n "pyo3-macros-backend") (v "0.19.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0dlm4pg29hjmlqx15gcy9cmnabvc8ycy60hcvjg8hm62flhw2zcl") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.20.0 (c (n "pyo3-macros-backend") (v "0.20.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "096lvwppj703rpkhhbn76sd9bgi0a6a0s7rvh1gz5m1b52lbax3p") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.20.1 (c (n "pyo3-macros-backend") (v "0.20.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "193z4k1q1da7bjlyxpqdmmn5bialsl04dsfz7hpapp99krq4ysl1") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.20.2 (c (n "pyo3-macros-backend") (v "0.20.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "07w8x1wxm1ksx72jb0w1p2ssmg9zh95dsv4xmxyq4iqqhpa11j8g") (f (quote (("abi3"))))))

(define-public crate-pyo3-macros-backend-0.20.3 (c (n "pyo3-macros-backend") (v "0.20.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "=0.20.3") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "11b1z7qnbdnd9hy74ds3xcjx3mjkz43mvpnan32ljccwpdl9nzkw")))

(define-public crate-pyo3-macros-backend-0.21.0-beta.0 (c (n "pyo3-macros-backend") (v "0.21.0-beta.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "=0.21.0-beta.0") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "04djmldcik81glzl12pk1npxhjl29sb72n44zqm6sy2psgp7nfsy") (f (quote (("experimental-async"))))))

(define-public crate-pyo3-macros-backend-0.21.0 (c (n "pyo3-macros-backend") (v "0.21.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "=0.21.0") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0nvcj2sy995gx9w83vyxdyg34y6qkc8si9hj1qh2ah0089pximzw") (f (quote (("experimental-async"))))))

(define-public crate-pyo3-macros-backend-0.21.1 (c (n "pyo3-macros-backend") (v "0.21.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "=0.21.1") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0fycsj0b8ajz2rnb002sjhm3dkcdi0mi5jqkcl1i0gamxgbvzywk") (f (quote (("experimental-async"))))))

(define-public crate-pyo3-macros-backend-0.21.2 (c (n "pyo3-macros-backend") (v "0.21.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "pyo3-build-config") (r "=0.21.2") (f (quote ("resolve-config"))) (d #t) (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "parsing" "printing" "clone-impls" "full" "extra-traits"))) (k 0)))) (h "0p58yp8ajlc8bq56wghw1syrjszmadasasdfpsjy3d9dychhf9h8") (f (quote (("experimental-async"))))))

