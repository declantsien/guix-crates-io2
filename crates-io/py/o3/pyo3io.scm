(define-module (crates-io py o3 pyo3io) #:use-module (crates-io))

(define-public crate-pyo3io-0.1.0 (c (n "pyo3io") (v "0.1.0") (d (list (d (n "pyo3-asyncio") (r "^0.20") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pyo3io-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0bx1f2lhxcjr7x19343qdbxkdfax5jrr2l2b044wm7wyci17bwgq") (f (quote (("tokio" "pyo3-asyncio/tokio-runtime") ("default") ("async-std" "pyo3-asyncio/async-std-runtime")))) (r "1.75")))

(define-public crate-pyo3io-0.1.1 (c (n "pyo3io") (v "0.1.1") (d (list (d (n "pyo3-asyncio") (r "^0.20") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pyo3io-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1akq8zf4zplzfcj02rxmpnhdx3im01mjwqb1w8vgzq2cnmzi6j63") (f (quote (("tokio" "pyo3-asyncio/tokio-runtime") ("default") ("async-std" "pyo3-asyncio/async-std-runtime")))) (r "1.75")))

(define-public crate-pyo3io-0.1.2 (c (n "pyo3io") (v "0.1.2") (d (list (d (n "pyo3-asyncio") (r "^0.20") (f (quote ("attributes"))) (o #t) (d #t) (k 0)) (d (n "pyo3io-macros") (r "^0.1.0") (d #t) (k 0)))) (h "19zq4gpbw5s0wkwa57dlz6v1mb64hqhxybm9rhbpjhhcz9s6v2lj") (f (quote (("tokio" "pyo3-asyncio/tokio-runtime") ("default") ("async-std" "pyo3-asyncio/async-std-runtime")))) (r "1.75")))

