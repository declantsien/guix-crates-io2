(define-module (crates-io py o3 pyo3-tch) #:use-module (crates-io))

(define-public crate-pyo3-tch-0.13.0 (c (n "pyo3-tch") (v "0.13.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "tch") (r "^0.13.0") (f (quote ("python-extension"))) (d #t) (k 0)) (d (n "torch-sys") (r "^0.13.0") (f (quote ("python-extension"))) (d #t) (k 0)))) (h "1h0icig1pk68hbbn0qhimmapa6kq7rklrav6lx9p878j7371xriq")))

(define-public crate-pyo3-tch-0.14.0 (c (n "pyo3-tch") (v "0.14.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "tch") (r "^0.14.0") (f (quote ("python-extension"))) (d #t) (k 0)) (d (n "torch-sys") (r "^0.14.0") (f (quote ("python-extension"))) (d #t) (k 0)))) (h "0s5drz3pa4kzjmv20ib957pp1frzhvanp350vckrg4z298vpiww8")))

(define-public crate-pyo3-tch-0.15.0 (c (n "pyo3-tch") (v "0.15.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "tch") (r "^0.15.0") (f (quote ("python-extension"))) (d #t) (k 0)) (d (n "torch-sys") (r "^0.15.0") (f (quote ("python-extension"))) (d #t) (k 0)))) (h "0azblc0dbkrdi0kqv34sid8phq5qbyxycii15cxf79xgmmldfhsi")))

