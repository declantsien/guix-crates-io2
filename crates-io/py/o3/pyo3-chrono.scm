(define-module (crates-io py o3 pyo3-chrono) #:use-module (crates-io))

(define-public crate-pyo3-chrono-0.1.0 (c (n "pyo3-chrono") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.13") (d #t) (k 0)))) (h "1444qydfkhbpnjks6v2j0ami91qlcbc14qnac0g4awkawvqrpvn9")))

(define-public crate-pyo3-chrono-0.2.0 (c (n "pyo3-chrono") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("auto-initialize"))) (d #t) (k 2)))) (h "18hfdrdahwqv03v7sqidzvb5d5pn0p96z3hz8r46bk17zg0mr73m")))

(define-public crate-pyo3-chrono-0.2.1 (c (n "pyo3-chrono") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "05hcrr14m92k6rpdlmn5iwfrj2mi7x3kvvakwc8ch7aihzhp2lh6") (f (quote (("serde" "serde_" "chrono/serde"))))))

(define-public crate-pyo3-chrono-0.2.2 (c (n "pyo3-chrono") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (d #t) (k 0)) (d (n "pyo3") (r "^0.14") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "1gr7sa4zs6dv079zv1fyazld79nw71g6m4x78k9wnial4hd9sciz") (f (quote (("serde" "serde_" "chrono/serde"))))))

(define-public crate-pyo3-chrono-0.3.0 (c (n "pyo3-chrono") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "16wrwdix3bvk479c4pzla6035fd4f4k65bh3sa4bd1v687xww9bb") (f (quote (("serde" "serde_" "chrono/serde"))))))

(define-public crate-pyo3-chrono-0.4.0 (c (n "pyo3-chrono") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (d #t) (k 0)) (d (n "pyo3") (r "^0.16") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "1d5lawhxs8djq3kaqhdv4a0l1jhjib307kfp0a2j0q5za2hn5wiw") (f (quote (("serde" "serde_" "chrono/serde"))))))

(define-public crate-pyo3-chrono-0.5.0 (c (n "pyo3-chrono") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (f (quote ("auto-initialize"))) (d #t) (k 2)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")))) (h "1gqbj1ssvg5r9haz1vfjcq5350zihj2kl49h3lv8vp435q5367nm") (f (quote (("serde" "serde_" "chrono/serde"))))))

(define-public crate-pyo3-chrono-0.5.1 (c (n "pyo3-chrono") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)) (d (n "serde_") (r "^1.0") (o #t) (d #t) (k 0) (p "serde")) (d (n "pyo3") (r "^0.17") (f (quote ("auto-initialize"))) (d #t) (k 2)))) (h "0cvkf1lsvx9gz5bfxl72rp410lr9jipvrbcyrj5aqwv6f9dvdiwc") (f (quote (("serde" "serde_" "chrono/serde"))))))

