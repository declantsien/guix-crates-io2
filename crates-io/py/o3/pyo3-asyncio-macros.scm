(define-module (crates-io py o3 pyo3-asyncio-macros) #:use-module (crates-io))

(define-public crate-pyo3-asyncio-macros-0.13.0 (c (n "pyo3-asyncio-macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nhg0j68dw6fapal5mi7a7bn9ycvjir6izqkzvnk73223sixd6s2")))

(define-public crate-pyo3-asyncio-macros-0.13.1 (c (n "pyo3-asyncio-macros") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gczf2lv8lhfdf1ydaqkmzp3zxjw5rp5xipxacrbrvq75jl57y44")))

(define-public crate-pyo3-asyncio-macros-0.13.2 (c (n "pyo3-asyncio-macros") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17q9ddqpnwcamg20pzwc148jhgvgw5bs0vms0jshl4za7qiw9y7v")))

(define-public crate-pyo3-asyncio-macros-0.13.3 (c (n "pyo3-asyncio-macros") (v "0.13.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08pbn78adny1pcad0lsn47y62bxm4bsaxbycfr5idv3dk3w249mb")))

(define-public crate-pyo3-asyncio-macros-0.13.4 (c (n "pyo3-asyncio-macros") (v "0.13.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m6lynq3g0hpr2kd74nlkwa1pr6h6mj0wkrp59c9pvhfavad1bl8")))

(define-public crate-pyo3-asyncio-macros-0.13.5 (c (n "pyo3-asyncio-macros") (v "0.13.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11wdgmfv9vpn5k3zs70vfk5lpm8cqaz0ffbkdgf8ncinbw8p5cag")))

(define-public crate-pyo3-asyncio-macros-0.14.0 (c (n "pyo3-asyncio-macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qcrm3qix3bl6bnrcvy78hj61nlibsd4mdmnn1yr95civbrhignk")))

(define-public crate-pyo3-asyncio-macros-0.14.1 (c (n "pyo3-asyncio-macros") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16g666vryiy5ihm4vid6gjwfff4za9i38sxkxk95d0yn430di4wa")))

(define-public crate-pyo3-asyncio-macros-0.15.0 (c (n "pyo3-asyncio-macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nv2zmpzapy03akcj8j70f6b57hzsbiz6l4v3yy6svfvc1rdmajd")))

(define-public crate-pyo3-asyncio-macros-0.16.0 (c (n "pyo3-asyncio-macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16d66jrj7ka66y84da1jzvngp7c8vnb19r2sf2g4z6kr114k6xv2")))

(define-public crate-pyo3-asyncio-macros-0.17.0 (c (n "pyo3-asyncio-macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0s94kq91dzk8lqxxhnx824ycvglm7i307w50d9hn3f84ldz1c2r7")))

(define-public crate-pyo3-asyncio-macros-0.18.0 (c (n "pyo3-asyncio-macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1parsnwyydxw70v8vff6fb8bsbqq68whvlnhdcq30xd28g6x8wmy")))

(define-public crate-pyo3-asyncio-macros-0.19.0 (c (n "pyo3-asyncio-macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cr6rrlyslrn2f06f0cn8aqcivw27jqw1xcn8ng1fiwm8835y174")))

(define-public crate-pyo3-asyncio-macros-0.20.0 (c (n "pyo3-asyncio-macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fasshclxhjxy7gabp41j4p78gqkif4wz7n2jln2b9hxiqbngi2n")))

