(define-module (crates-io py o3 pyo3-pylogger) #:use-module (crates-io))

(define-public crate-pyo3-pylogger-0.1.0 (c (n "pyo3-pylogger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)))) (h "1x8yapkhhwms8y9dhjkkpd2mv55g1qfxafgmf8jz83b73vjfmdih") (y #t)))

(define-public crate-pyo3-pylogger-0.1.1 (c (n "pyo3-pylogger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)))) (h "1yand5sbnnbsyvd2v3x4b7icggffqbj7mychrsjg1qv550yi7iv8") (y #t)))

(define-public crate-pyo3-pylogger-0.2.0 (c (n "pyo3-pylogger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (d #t) (k 0)))) (h "1p8nak1jqbw4zmhlkhdfy0fzpn5fwqqs2iq82kqrnydb7cl4gn1s")))

(define-public crate-pyo3-pylogger-0.2.1 (c (n "pyo3-pylogger") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r ">0.17") (d #t) (k 0)))) (h "0cnx0wc6iw84fcwdymndxq333jwa9s5k8zmxsvimzrxwks71l3nq")))

(define-public crate-pyo3-pylogger-0.2.2 (c (n "pyo3-pylogger") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r ">0.17") (d #t) (k 0)))) (h "0bk0j6vpqg9fh361imnkk83bvxqpyqcd936lqf23qbrgy60g5bfx")))

