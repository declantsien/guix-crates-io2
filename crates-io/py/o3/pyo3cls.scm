(define-module (crates-io py o3 pyo3cls) #:use-module (crates-io))

(define-public crate-pyo3cls-0.1.0 (c (n "pyo3cls") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "195nzkjn18z6ix0z19jgiii4381nrnzvq2bmsfk9vzcg30rv2fzj")))

(define-public crate-pyo3cls-0.2.0 (c (n "pyo3cls") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "097672i27b1n64n6shmjjsgv45bhfhv9rfjjb4nsi607d48cn6mi")))

(define-public crate-pyo3cls-0.2.1 (c (n "pyo3cls") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1idlli1kl9ldd3whf80y2j2hjmilz1ydghmdwxdpg5idcahhn68c")))

(define-public crate-pyo3cls-0.2.7 (c (n "pyo3cls") (v "0.2.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1c2rjc4ljdli0n92951iwpx6mlqz179iqg64lvhix7iq7411yr8l")))

(define-public crate-pyo3cls-0.3.0 (c (n "pyo3cls") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0bnfhg6zaqmjrvbvxa066d0dxis94qbjlz3gw8ln40xqvxs29p6f")))

(define-public crate-pyo3cls-0.3.1 (c (n "pyo3cls") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1k83imn8grxmy71cblkm683yr4j8zj55gvqbjr410c5xv9zl2mks")))

(define-public crate-pyo3cls-0.3.2 (c (n "pyo3cls") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "^0.3.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0w1zdgjxggwy6l4ii57i34rfx1gb0a014g23czm4r93mfswm5pkl")))

(define-public crate-pyo3cls-0.4.0 (c (n "pyo3cls") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0hg5xvk9kn0jb0711fbqgf2pz5l14a1s89lcfvp2kx5ihcp8j05c")))

(define-public crate-pyo3cls-0.4.1 (c (n "pyo3cls") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.4.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.6") (d #t) (k 0)) (d (n "syn") (r "^0.14.8") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mw0k2b8bp8yaivgqqlam46y0d8q5p4zizr523algjsxgaf6ngfg")))

(define-public crate-pyo3cls-0.5.0-alpha.1 (c (n "pyo3cls") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qlcvsxgcdvsbd8gqyd9sv4q9s3n79xv1mh2j079mfpd3ablh78m")))

(define-public crate-pyo3cls-0.5.0-alpha.2 (c (n "pyo3cls") (v "0.5.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.0-alpha.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pcypjm6cxm93lblr6q7wbi9d42x4fsza9vv3w99vw8qrpmngshj")))

(define-public crate-pyo3cls-0.5.0-alpha.3 (c (n "pyo3cls") (v "0.5.0-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.0-alpha.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lbncdi2yb0nq6rv12wgl9cbf1gr578zpj404w2dxb5492qp28as")))

(define-public crate-pyo3cls-0.5.0 (c (n "pyo3cls") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k83wv0lx30d55msrik8hb7l082cksa6yr5i3n1n26gfd42nv8vf")))

(define-public crate-pyo3cls-0.5.1 (c (n "pyo3cls") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0skw0mngnm79y43vlnf9wb3amg5212xcp87jp6s5kb5p849a9crx") (y #t)))

(define-public crate-pyo3cls-0.5.2 (c (n "pyo3cls") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f9ppgb2k2gdd4845wcrcan5f92mqd30s4aw5b49k9qmfnwhp8fl")))

(define-public crate-pyo3cls-0.5.3 (c (n "pyo3cls") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.3") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jy0l2a8z0lp3adwxhs27qf31q79n10bs0j2af9fx38izk2q8bbn")))

(define-public crate-pyo3cls-0.6.0-alpha.1 (c (n "pyo3cls") (v "0.6.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.6.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dy3jw8a4igix0x59x7m462ll2dx59g2v4wkv7zs7gl4bq33c3hr")))

(define-public crate-pyo3cls-0.6.0-alpha.2 (c (n "pyo3cls") (v "0.6.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.6.0-alpha.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "198h9hl9zrmxw0cjy02q7y3nynr0cvfrrpwma526vgk2yflw3yrx")))

(define-public crate-pyo3cls-0.5.4 (c (n "pyo3cls") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.5.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08n1i6pakhdjjq7nyhbdx9vixg29f9032hgwg81i2vcn8jq82xs2")))

(define-public crate-pyo3cls-0.6.0-alpha.4 (c (n "pyo3cls") (v "0.6.0-alpha.4") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.6.0-alpha.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0f9x8zhpa1z8hg41gr0kfmvjw8k7a0sbssvxqfjj2zns3axr4a0m")))

(define-public crate-pyo3cls-0.6.0 (c (n "pyo3cls") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11las2aw7ndny3q1v9zkvah06cd9yycsqacw478snk1rwnw1xyf4")))

(define-public crate-pyo3cls-0.7.0-alpha.1 (c (n "pyo3cls") (v "0.7.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.7.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n1abq7fbak5k9x0n0sym5icn50h2g849cr6csp53lwkkg4s1qy0")))

(define-public crate-pyo3cls-0.7.0 (c (n "pyo3cls") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.7.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h2gsm3bafgzxhfrrrg5rmvg8w761ywzn00gqmn0jwsv3wbgi564")))

(define-public crate-pyo3cls-0.8.0 (c (n "pyo3cls") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13f7r9x0378x9rb9pz5j69g938azrsxjl0vx9wwvvzhn4jf54faf") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.8.1 (c (n "pyo3cls") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17ykf9ajn0qz9x4wzm26m58xbr8x7x5xc6lz0w2xidfr9rigmk55") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.8.2 (c (n "pyo3cls") (v "0.8.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hmw9gxkyxs8jw32wvv8ap9rpp9blybrw7j77xfkayjzr7jmjjng") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.8.3 (c (n "pyo3cls") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08m1p394c5b1cmqddz2kvsyadjxxyyppizzk350h8ix599hssx9m") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.8.4 (c (n "pyo3cls") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s3466z8wfhrg0hz05bmip47zz11g2h68rn0qpmjf5llyp6jsicp") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.8.5 (c (n "pyo3cls") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.8.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1npcpqvk7pprmcpqgx8kxmfny4z545nwhcw7548p8psmmg7j3wzx") (f (quote (("unsound-subclass" "pyo3-derive-backend/unsound-subclass"))))))

(define-public crate-pyo3cls-0.9.0-alpha.1 (c (n "pyo3cls") (v "0.9.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3-derive-backend") (r "= 0.9.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hal0l399dpjxqxcxx5j7klln46j6kcdhhvqr04v2kic5wk75m19")))

(define-public crate-pyo3cls-0.9.0 (c (n "pyo3cls") (v "0.9.0") (d (list (d (n "pyo3-derive-backend") (r "= 0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w9rjw4p384pr8cnsmdkbwvnlix73v6a1xsgxsvgc05g4mycjayj")))

(define-public crate-pyo3cls-0.9.1 (c (n "pyo3cls") (v "0.9.1") (d (list (d (n "pyo3-derive-backend") (r "= 0.9.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jnrksplgc62yc0r23fs2m519b4wv07s43gldbamzpil4qyq54db")))

(define-public crate-pyo3cls-0.9.2 (c (n "pyo3cls") (v "0.9.2") (d (list (d (n "pyo3-derive-backend") (r "= 0.9.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18873df8ignv80b5ln146258xhgsmsq8yx98sbl27n4pd0a6cg85")))

(define-public crate-pyo3cls-0.10.0 (c (n "pyo3cls") (v "0.10.0") (d (list (d (n "pyo3-derive-backend") (r "= 0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07ncpjcik4f0i4gmyzsbbql68y24zbmpzkfc0wkkv3iq4qp3hycd")))

(define-public crate-pyo3cls-0.10.1 (c (n "pyo3cls") (v "0.10.1") (d (list (d (n "pyo3-derive-backend") (r "= 0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a4rbk6ap7vcimq5fx4r2nikqdqzac5vqqj7gp0q9h6vhz22hihz")))

(define-public crate-pyo3cls-0.11.0 (c (n "pyo3cls") (v "0.11.0") (d (list (d (n "pyo3-derive-backend") (r "=0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "073rkd2yxd1r48ndygnhxm6dsmp1akw1dlphkp8wgzi0nc9as52l")))

(define-public crate-pyo3cls-0.11.1 (c (n "pyo3cls") (v "0.11.1") (d (list (d (n "pyo3-derive-backend") (r "=0.11.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ajbqshvn6jl9iwz2xnxamga0743xag2l3y0nzrhp7anxbhigyn3")))

(define-public crate-pyo3cls-0.12.0 (c (n "pyo3cls") (v "0.12.0") (d (list (d (n "pyo3-derive-backend") (r "=0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1shkyv8ji87bw92dy31lgk470gi1iy9cvn1j02vlzi8aq0bfb3i2")))

(define-public crate-pyo3cls-0.12.1 (c (n "pyo3cls") (v "0.12.1") (d (list (d (n "pyo3-derive-backend") (r "=0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kww39pix9h0nx5371a393xj1gz84s63bgrbj02mwfd0if180ili")))

(define-public crate-pyo3cls-0.12.2 (c (n "pyo3cls") (v "0.12.2") (d (list (d (n "pyo3-derive-backend") (r "=0.12.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j1ickzbzr4xw0gd376ax1351idjckj9n0q5gzq26338m4z9fsz3")))

(define-public crate-pyo3cls-0.12.3 (c (n "pyo3cls") (v "0.12.3") (d (list (d (n "pyo3-derive-backend") (r "=0.12.3") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0l3g428dvshd9kmfsw81af4jwyvin4yrx0grjcy00z915y5dsbzi")))

(define-public crate-pyo3cls-0.12.4 (c (n "pyo3cls") (v "0.12.4") (d (list (d (n "pyo3-derive-backend") (r "=0.12.4") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "180ipg079nzrb8s8knz8sbd03q9gl5711zr02zfhcjl3damgsi6k")))

(define-public crate-pyo3cls-0.12.5 (c (n "pyo3cls") (v "0.12.5") (d (list (d (n "pyo3-derive-backend") (r "=0.12.5") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1853kq4484hgdymgch0sh3rkllkbn0iad81cnrlixbbqzzjlrj31")))

