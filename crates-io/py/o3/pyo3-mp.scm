(define-module (crates-io py o3 pyo3-mp) #:use-module (crates-io))

(define-public crate-pyo3-mp-0.1.0 (c (n "pyo3-mp") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "0vsgdvrpqqzzh36l5zf0c4lk13bj12072308anpp8gy0w1ly7cy4")))

(define-public crate-pyo3-mp-0.1.1 (c (n "pyo3-mp") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "0m3qlvlp2hb2isyqirh96n9ibjpnldpdahwhwflipzhpij058p1n")))

(define-public crate-pyo3-mp-0.1.2 (c (n "pyo3-mp") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.12") (d #t) (k 0)))) (h "1pf6831lxb4zja5b8xw9bwin3vslfn7hf7d58yxnl9z57x8wyi7f")))

(define-public crate-pyo3-mp-0.1.3 (c (n "pyo3-mp") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1p1bv6ss8y8sisqxa8bcdfg7z0s826gyxfhkpvbamb5sy9wxw932")))

