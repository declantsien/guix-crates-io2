(define-module (crates-io py o3 pyo3_bindgen_macros) #:use-module (crates-io))

(define-public crate-pyo3_bindgen_macros-0.1.0 (c (n "pyo3_bindgen_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (k 2)) (d (n "pyo3_bindgen_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wnclvlw2l7pzfhvjskd1i2nk9p9kypjldhhhvlf251n5cj6qhd0") (r "1.70")))

(define-public crate-pyo3_bindgen_macros-0.1.1 (c (n "pyo3_bindgen_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (k 2)) (d (n "pyo3_bindgen_engine") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ygfwlanisgkjc7wp1p9wvwll1f10b5qz6xh6cjifn0chkq0njhf") (r "1.70")))

(define-public crate-pyo3_bindgen_macros-0.2.0 (c (n "pyo3_bindgen_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (k 2)) (d (n "pyo3_bindgen_engine") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "029bf9nv4qirnvd812bw1xy422yi7s3s8475nnv9h6fxxnwq56f2") (r "1.70")))

(define-public crate-pyo3_bindgen_macros-0.3.0 (c (n "pyo3_bindgen_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (k 2)) (d (n "pyo3_bindgen_engine") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0z7gy5gcb3vm2ifa6jhrd7x5ydghv1aghz8bglknsc7245rhn8xj") (r "1.74")))

(define-public crate-pyo3_bindgen_macros-0.3.1 (c (n "pyo3_bindgen_macros") (v "0.3.1") (d (list (d (n "pyo3") (r "^0.20") (k 2)) (d (n "pyo3_bindgen_engine") (r "^0.3.1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "06bmqmc9gvhsknvjbwm56xyjh85sqnd0a7pv89vaz7l1j29l3gyc") (r "1.74")))

(define-public crate-pyo3_bindgen_macros-0.4.0 (c (n "pyo3_bindgen_macros") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pyo3") (r "^0.20") (t "cfg(unix)") (k 0)) (d (n "pyo3") (r "^0.20") (t "cfg(not(unix))") (k 2)) (d (n "pyo3-build-config") (r "^0.20") (f (quote ("resolve-config"))) (d #t) (k 1)) (d (n "pyo3_bindgen_engine") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0i5ma1mnkp3nsfy4xdgjk3b2kg8mqjfz1l8zraz7kx7h4krjfkdp") (r "1.74")))

(define-public crate-pyo3_bindgen_macros-0.5.0 (c (n "pyo3_bindgen_macros") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "pyo3") (r "^0.21") (t "cfg(unix)") (k 0)) (d (n "pyo3") (r "^0.21") (t "cfg(not(unix))") (k 2)) (d (n "pyo3-build-config") (r "^0.21") (f (quote ("resolve-config"))) (d #t) (k 1)) (d (n "pyo3_bindgen_engine") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0l17zyq3031bahh5n6jrwbyw0b95d6i5lz1cvpxbpk5v0y0vdk6q") (r "1.74")))

