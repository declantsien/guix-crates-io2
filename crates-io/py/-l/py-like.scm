(define-module (crates-io py -l py-like) #:use-module (crates-io))

(define-public crate-py-like-0.1.0 (c (n "py-like") (v "0.1.0") (h "199ppj819qra696nvs2dai16k89s1f1h80k3zrg0i8xn43xch57x")))

(define-public crate-py-like-0.1.1 (c (n "py-like") (v "0.1.1") (h "1vw2vbki07v9kmnam8l8kmvccmdlsqv8k2nmka9jrx7mgs0h05c9")))

