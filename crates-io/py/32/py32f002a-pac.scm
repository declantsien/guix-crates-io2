(define-module (crates-io py #{32}# py32f002a-pac) #:use-module (crates-io))

(define-public crate-py32f002a-pac-0.1.0 (c (n "py32f002a-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "06vb5k05hprvmajnh72l6abhjkjgzg1ni756v47r2kv9p8isr8v1") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

