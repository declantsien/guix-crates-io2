(define-module (crates-io py #{32}# py32f0) #:use-module (crates-io))

(define-public crate-py32f0-0.0.1 (c (n "py32f0") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0x8ncmi5crnds9wn932jifm6khhcw4z8fi9rmm9s17i52acprvn8") (f (quote (("rt" "cortex-m-rt/device") ("py32f030") ("py32f003") ("py32f002a") ("default" "critical-section" "rt"))))))

