(define-module (crates-io py rp pyrpds) #:use-module (crates-io))

(define-public crate-pyrpds-0.0.0 (c (n "pyrpds") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0cy9q9ia2s4vbky8pi1y29jgr8anyr5f4raikflhrh3fwvcgn5q1")))

(define-public crate-pyrpds-0.0.1 (c (n "pyrpds") (v "0.0.1") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mac9p345p1kx13j9533hh1wdl2ybvl3nvmj5xs0jg298c5pb1ds")))

(define-public crate-pyrpds-0.0.2 (c (n "pyrpds") (v "0.0.2") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0jq9qgy6q5gpm3frp2dymhkw5rpbx29fi1v77yy5hxsr400qinjp")))

(define-public crate-pyrpds-0.0.3 (c (n "pyrpds") (v "0.0.3") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "09wjcm9ppzlx1wcrk47fcbrxj919a1vr2kyh23q12kixl9kg60lj")))

(define-public crate-pyrpds-0.0.4 (c (n "pyrpds") (v "0.0.4") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0vcnbfj2fq2rfh0w5mzfsw48c87zs9mdgz012y86rlsjdxw8hql1")))

(define-public crate-pyrpds-0.0.5 (c (n "pyrpds") (v "0.0.5") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1sx6kk4hxa3a82qpmz2jak9ipbfqxfrgji2r9rmv17mgga1wn6bd")))

(define-public crate-pyrpds-0.0.6 (c (n "pyrpds") (v "0.0.6") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1c3czmapvz2z9gljm0d64gjzni363wrw2h1w553pp58l7ihyglqq")))

(define-public crate-pyrpds-0.0.7 (c (n "pyrpds") (v "0.0.7") (d (list (d (n "pyo3") (r "^0.9.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rpds") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "07avh7a5bq7y3s5g824xd3x3yjhsac3dnxkhv909zfgrwl4wjqcw")))

