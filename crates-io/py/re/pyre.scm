(define-module (crates-io py re pyre) #:use-module (crates-io))

(define-public crate-pyre-0.1.0 (c (n "pyre") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "106a8bbww82z1z7dkyh32h1l8223l20gzabb9pps2wq40hhgqnzq")))

(define-public crate-pyre-0.1.3 (c (n "pyre") (v "0.1.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1lq3f0cc7bzgcxz8ms6pla8ic6kvgqv6jb8jrk69m9jx1cxgi5jc")))

(define-public crate-pyre-0.1.4 (c (n "pyre") (v "0.1.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1dk5wpy1vq3z6sbwap0737gxfhip8lzdyqwbk5l0916837nccwza")))

(define-public crate-pyre-0.1.41 (c (n "pyre") (v "0.1.41") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "080giswk8jwmz46haykzp7s9bs7s2gzxaj1g42mxfwlmmbs84v3r")))

(define-public crate-pyre-0.1.43 (c (n "pyre") (v "0.1.43") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13xv0x27sbrd7cmnjl4ngr2i2iri38jwkjcqx3iff7vz3fmivcd1")))

(define-public crate-pyre-0.1.44 (c (n "pyre") (v "0.1.44") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "160gl22dbd0nvggsifg1w3jx1x7kmmd97i583ikxq9q4prc7lyq2")))

(define-public crate-pyre-0.1.5 (c (n "pyre") (v "0.1.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0xm8f75p3sv652s7a6xrm7v7iad43l5gv066xnaa1zqnpv7cwyqn")))

(define-public crate-pyre-0.1.6 (c (n "pyre") (v "0.1.6") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1cdgqqgjs6brd962qjkvj75dggvfik77jj6shg9j18grljnnx56q")))

