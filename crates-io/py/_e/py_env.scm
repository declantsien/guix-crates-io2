(define-module (crates-io py _e py_env) #:use-module (crates-io))

(define-public crate-py_env-1.0.0 (c (n "py_env") (v "1.0.0") (h "1w3hljx92xpdgf18xc33absy40sjcwxfyyhjh5j8kvd39yycnb6g") (y #t)))

(define-public crate-py_env-1.0.1 (c (n "py_env") (v "1.0.1") (h "13s9h203fgshqm2k25j44ivgq5r5wkiladpj75vf18ja7r5f89l2")))

(define-public crate-py_env-1.1.0 (c (n "py_env") (v "1.1.0") (h "1x5my2jniqhwf134hl1li6a450zjrkls0jzwbg1g4rg32p43k08r") (y #t)))

(define-public crate-py_env-1.1.1 (c (n "py_env") (v "1.1.1") (h "1nvxciifv5my6q3qxcl87kipb8cswm9nchjdgy1j21yihyqc7xvj") (y #t)))

(define-public crate-py_env-1.1.2 (c (n "py_env") (v "1.1.2") (h "0bxakw1igcxjnbw96yh9l5c5z05giv8nnj8b07xgqag17siz6gpy")))

(define-public crate-py_env-2.0.0 (c (n "py_env") (v "2.0.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1mnha69dqw3hs1iil7prq155x3g8pq04zan77b2afmf1dl6bkvzm") (y #t)))

(define-public crate-py_env-2.0.1 (c (n "py_env") (v "2.0.1") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "1xm0rkbgadfivlliqbdc93n3m9qbk1vgmj32xnjjlpysg67x24i6") (y #t)))

(define-public crate-py_env-2.0.2 (c (n "py_env") (v "2.0.2") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "16r1an6h0129k7n1bw8dfgylxwrw4dq487zsqvhc7zg2h0216arq") (y #t)))

(define-public crate-py_env-1.2.0 (c (n "py_env") (v "1.2.0") (h "0fpjknsy206mvqd03r76i9gk2ziabfn8gk4rb1kndqnw8bda1az6")))

