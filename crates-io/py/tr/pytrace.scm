(define-module (crates-io py tr pytrace) #:use-module (crates-io))

(define-public crate-pytrace-0.2.1 (c (n "pytrace") (v "0.2.1") (d (list (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pytrace_core") (r "0.2.*") (d #t) (k 0)))) (h "0yypw8b7l7p6pywvc1imnl9k7c3w01il63xyna14z61rrfms1bqj")))

(define-public crate-pytrace-0.3.0 (c (n "pytrace") (v "0.3.0") (d (list (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pytrace_core") (r "^0.2.5") (d #t) (k 0)))) (h "00brh2l01b1m10hz27plywqqy2fr84w5c3sf1ipzkpiv4khdhm7x")))

(define-public crate-pytrace-0.3.1 (c (n "pytrace") (v "0.3.1") (d (list (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pytrace_core") (r "^0.2.5") (d #t) (k 0)))) (h "1aj3nq3nb4158kl4v48nkyygdr3jmq2i2s0qa0mgnfbkxgk0wx1l")))

(define-public crate-pytrace-0.3.2 (c (n "pytrace") (v "0.3.2") (d (list (d (n "ctrlc") (r "3.1.*") (d #t) (k 0)) (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pytrace_core") (r "^0.2.5") (d #t) (k 0)))) (h "1rw95vz5faxyav4ldnzzg9gjzsj86r018hx9xgkf4b4g3i5962ag") (y #t)))

(define-public crate-pytrace-0.3.3 (c (n "pytrace") (v "0.3.3") (d (list (d (n "ctrlc") (r "3.1.*") (d #t) (k 0)) (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pytrace_core") (r "^0.2.5") (d #t) (k 0)))) (h "07k6j8ab67yag6ks1mrghn3gkwl060bfy30m8hzb556cv0x8bdwi")))

