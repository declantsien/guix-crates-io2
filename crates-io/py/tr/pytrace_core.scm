(define-module (crates-io py tr pytrace_core) #:use-module (crates-io))

(define-public crate-pytrace_core-0.2.0 (c (n "pytrace_core") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "08f4n5bripm2qf4np58cslwrzxn3n3d94a9iagx9gkab9681nljg")))

(define-public crate-pytrace_core-0.2.1 (c (n "pytrace_core") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "13f7k80ndykcvaycjyaciyjpbvwdd2a68dj82f8hvfwcy1xsh35s")))

(define-public crate-pytrace_core-0.2.2 (c (n "pytrace_core") (v "0.2.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "1vkfyxcmlsrpnjk3hv630ijra2wrvp4i4rpq6xi2b768a4rbkd7i")))

(define-public crate-pytrace_core-0.2.3 (c (n "pytrace_core") (v "0.2.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "0fh54da6mq89pchh5817gw5yd4jgkc1snv6p4dplx2l7kz1fcz37")))

(define-public crate-pytrace_core-0.2.4 (c (n "pytrace_core") (v "0.2.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "0ps5h5icwpd7i2mxxpznszkylcbx7b7h37sp10f56lz4iqvjf0n4")))

(define-public crate-pytrace_core-0.2.5 (c (n "pytrace_core") (v "0.2.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "1.8.*") (d #t) (k 0)))) (h "1744zlfhad4b6vs6sgrlkzm8ma4bp4v8az5al08yzhdhrk4qcvgj")))

