(define-module (crates-io py st pystval) #:use-module (crates-io))

(define-public crate-pystval-0.0.1 (c (n "pystval") (v "0.0.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "grex") (r "^1.4.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.1") (f (quote ("abi3" "abi3-py37" "generate-import-lib" "multiple-pymethods"))) (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (o #t) (d #t) (k 0)))) (h "0h6hi3fxrmln97n2v7zbjqi2g8kgpzzv0qd3kkkmzan6vsn23d7y") (y #t) (s 2) (e (quote (("wasm" "dep:wasm-bindgen") ("python-lib" "dep:pyo3")))) (r "1.60")))

(define-public crate-pystval-0.0.2 (c (n "pystval") (v "0.0.2") (h "10961p4ax33nqfqc9imlw6d4176qvsynwvgzzfy0f7lxlifs2mld") (y #t)))

(define-public crate-pystval-0.0.3 (c (n "pystval") (v "0.0.3") (h "0n6spwz2ygd2l0prwzyi011nzcm9ax07c2rpqgl5b6igc3h3xjwx") (y #t)))

