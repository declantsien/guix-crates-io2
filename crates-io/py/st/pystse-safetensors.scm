(define-module (crates-io py st pystse-safetensors) #:use-module (crates-io))

(define-public crate-pystse-safetensors-0.0.1 (c (n "pystse-safetensors") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memmap2") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19scdwbrayvj6x37k0dmw4rchcd3mk43jgj9fcf0q2msvdk7a2rc")))

