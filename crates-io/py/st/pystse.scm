(define-module (crates-io py st pystse) #:use-module (crates-io))

(define-public crate-pystse-0.0.1 (c (n "pystse") (v "0.0.1") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "pystse-safetensors") (r "^0.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dpczm616x9xh1adq7a0v0zsl5d0rcqpx0v2qfb6wbvywk23aixw")))

