(define-module (crates-io py ro pyroscope_pyspy) #:use-module (crates-io))

(define-public crate-pyroscope_pyspy-0.1.0 (c (n "pyroscope_pyspy") (v "0.1.0") (d (list (d (n "py-spy") (r "^0.3.11") (d #t) (k 0)) (d (n "pyroscope") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "038m0xalr828lv7sikjhq858gxlf6lwq0xxpm2w11q789dckpl4q")))

(define-public crate-pyroscope_pyspy-0.2.0 (c (n "pyroscope_pyspy") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "py-spy") (r "^0.3.11") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vydry5k0q7fs2jbxcdjsvpa676hdx9w3n4c328wap3js7k34bhi")))

(define-public crate-pyroscope_pyspy-0.2.1 (c (n "pyroscope_pyspy") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "py-spy") (r "^0.3.11") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sy3ivkz1wy5s5q3g4v85v7pkp29972y54plpb05rihm6sgb5rdw")))

(define-public crate-pyroscope_pyspy-0.2.2 (c (n "pyroscope_pyspy") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "py-spy") (r "^0.3.12") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qbl0kyqprm8v3i94c7gxah3gwz43svwlkb2rjf68j45rb5lshcm")))

(define-public crate-pyroscope_pyspy-0.2.3 (c (n "pyroscope_pyspy") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "py-spy") (r "^0.3.12") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vhh9bc1csngj40hm5m4bp744vxkizha7vmzxxhkvq0pj633vv6a")))

