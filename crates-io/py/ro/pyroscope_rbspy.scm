(define-module (crates-io py ro pyroscope_rbspy) #:use-module (crates-io))

(define-public crate-pyroscope_rbspy-0.1.0 (c (n "pyroscope_rbspy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyroscope") (r "^0.4") (d #t) (k 0)) (d (n "rbspy") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f7x1py2hvdbk5l984n0m502xykb2q1p52y06rc13rxszhxv9hz4")))

(define-public crate-pyroscope_rbspy-0.2.0 (c (n "pyroscope_rbspy") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5") (d #t) (k 0)) (d (n "rbspy") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14y9dal6c9hz6n6n5zb0w71ryjz05m6mh6bvq8vkr446qfqsqf5h")))

(define-public crate-pyroscope_rbspy-0.2.1 (c (n "pyroscope_rbspy") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.2") (d #t) (k 0)) (d (n "rbspy") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02msp6n5kxd4ywik8nvd6isvky63h0rvgcbc17xdc7xy6wmaln9k")))

(define-public crate-pyroscope_rbspy-0.2.2 (c (n "pyroscope_rbspy") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.3") (d #t) (k 0)) (d (n "rbspy") (r "^0.12.1") (d #t) (k 0) (p "rbspy-oncpu")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jn0xyf5mw253d3dacpc381nfxwshmprsakwj05fnf558s4v0cfc")))

