(define-module (crates-io py ro pyroscope_pprofrs) #:use-module (crates-io))

(define-public crate-pyroscope_pprofrs-0.1.0 (c (n "pyroscope_pprofrs") (v "0.1.0") (d (list (d (n "pprof") (r "^0.7") (d #t) (k 0)) (d (n "pyroscope") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vn5yl6n043zx3arw04cv2dlqy5ylf89y3ysf31vjgjndl140s8b")))

(define-public crate-pyroscope_pprofrs-0.2.0 (c (n "pyroscope_pprofrs") (v "0.2.0") (d (list (d (n "pprof") (r "^0.8") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g80b4gq135ifdlhvrzzrgij6zj3qazj0ygfdr8xb4yafras85x9")))

(define-public crate-pyroscope_pprofrs-0.2.1 (c (n "pyroscope_pprofrs") (v "0.2.1") (d (list (d (n "pprof") (r "^0.8") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qnq47vja5y8kp5sklxqvybabmmxxq3zpa3pbm8rbw1n5hv3cg2d")))

(define-public crate-pyroscope_pprofrs-0.2.2 (c (n "pyroscope_pprofrs") (v "0.2.2") (d (list (d (n "pprof") (r "^0.9.1") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yhm21cav54ci3kla018x1z7v5wai43s8acvyfvzm8hl42j4gkqn")))

(define-public crate-pyroscope_pprofrs-0.2.3 (c (n "pyroscope_pprofrs") (v "0.2.3") (d (list (d (n "pprof") (r "^0.10.0") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "019fg0h7h9xq5fz2qyizg8k0glc4kj1p0zlnb4jfp0nql5ix1sf3")))

(define-public crate-pyroscope_pprofrs-0.2.5 (c (n "pyroscope_pprofrs") (v "0.2.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (d #t) (k 0)) (d (n "pyroscope") (r "^0.5.5") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ifc15bfvd5gjvxgawmahyhrdgdifx6r9mbkflx1p93x7szrkrhl") (f (quote (("default" "pyroscope/default"))))))

(define-public crate-pyroscope_pprofrs-0.2.6 (c (n "pyroscope_pprofrs") (v "0.2.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pprof") (r "^0.11") (d #t) (k 0)) (d (n "pprof") (r "^0.11") (f (quote ("frame-pointer"))) (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "pyroscope") (r "^0.5.5") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jhiy0z2kb3v1kp9gb313fbjd494wf43sp9sj6838y2pm9fx9bap") (f (quote (("default" "pyroscope/default"))))))

(define-public crate-pyroscope_pprofrs-0.2.7 (c (n "pyroscope_pprofrs") (v "0.2.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pprof") (r "^0.12") (d #t) (k 0)) (d (n "pprof") (r "^0.12") (f (quote ("frame-pointer"))) (d #t) (t "aarch64-apple-darwin") (k 0)) (d (n "pyroscope") (r "^0.5.7") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x4ga6jslimivh99bn52dqhba87517rja3v5k92gi9w1m6r11w23") (f (quote (("frame-pointer" "pprof/frame-pointer") ("default" "pyroscope/default"))))))

