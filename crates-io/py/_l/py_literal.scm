(define-module (crates-io py _l py_literal) #:use-module (crates-io))

(define-public crate-py_literal-0.1.0 (c (n "py_literal") (v "0.1.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0h3389acvmjr9x6h69b15rb8nycpbr2ijrvvj58swgizljjiw205")))

(define-public crate-py_literal-0.1.1 (c (n "py_literal") (v "0.1.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0wmbarfp2q305sa2lbagnz29yhhffjzlgjs0vvmzvps3wvrl68d4")))

(define-public crate-py_literal-0.2.0 (c (n "py_literal") (v "0.2.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0mqh751nhbswnhhy5xr00yyvgnlqas6ssw49y8fjm7c933jzvjyd")))

(define-public crate-py_literal-0.2.1 (c (n "py_literal") (v "0.2.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "10nc7a2wxhbvs4iff2rms53j868prycsmv4p5ca4mbsd58qjb5m1")))

(define-public crate-py_literal-0.2.2 (c (n "py_literal") (v "0.2.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "12i5rjyfy8dxcai0rpspvma0562vz403hn83r9bfn5ib9nnv3nz8")))

(define-public crate-py_literal-0.3.0 (c (n "py_literal") (v "0.3.0") (d (list (d (n "num") (r "^0.3") (f (quote ("alloc"))) (k 2)) (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-complex") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1nbmq97q4gzn3ansrshwpmnc0xfwhwpnvr1qxawdwqs0fw13iaz3")))

(define-public crate-py_literal-0.4.0 (c (n "py_literal") (v "0.4.0") (d (list (d (n "num") (r "^0.4") (f (quote ("alloc"))) (k 2)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-complex") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1qd8j3a9zlpq6rjaxabpc9sacw62dn1cr38p3y4x7fbdsjizfb8h")))

