(define-module (crates-io py ph pyphen-rs) #:use-module (crates-io))

(define-public crate-pyphen-rs-0.1.0 (c (n "pyphen-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "1qlgbjwg9lrkwzkp73w9vs6p8vg05z12zpgycz5jr2qky6w27pzz")))

