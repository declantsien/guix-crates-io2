(define-module (crates-io py ve pyver) #:use-module (crates-io))

(define-public crate-pyver-0.1.0 (c (n "pyver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aajsmvzs6c7ljvi42jy7gnzgz82gxzsvnxzqaf7qg42lv5rpry1")))

(define-public crate-pyver-1.0.0 (c (n "pyver") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "031myyhb5a0a4sgc26428l3f1b33mw7k2vn1z1yzqbd7xyvr301b")))

(define-public crate-pyver-1.0.1 (c (n "pyver") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nc3w4d0lmk5nvlrf4fm9yvshmyfldl4a2fld3lhldz7vijpfhx8")))

