(define-module (crates-io py pe pype) #:use-module (crates-io))

(define-public crate-pype-0.1.0 (c (n "pype") (v "0.1.0") (h "07sk3lmlrwx4w4ykrc4cjxmk41kkhf615hg6sjvq769fgrqklm4l")))

(define-public crate-pype-0.2.0 (c (n "pype") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0z161rx84m81zqsmyrag6y2hb796q0rndvg2459ngpzxl6dsnn3i")))

(define-public crate-pype-0.2.1 (c (n "pype") (v "0.2.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "18asl71yb2c3g8c5f51vslx1b29xs4ny10l1lydfs4rgngs0vayv")))

