(define-module (crates-io py ch pychan) #:use-module (crates-io))

(define-public crate-pychan-0.1.0 (c (n "pychan") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3.11") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.0-beta.0") (f (quote ("experimental-async" "extension-module"))) (d #t) (k 0)))) (h "0jnf0c9vsy0qn5dz172zfr6ls7shgj8i9nv6in7iy1v7mayiyzkb")))

