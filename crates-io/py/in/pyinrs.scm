(define-module (crates-io py in pyinrs) #:use-module (crates-io))

(define-public crate-pyinrs-0.1.0 (c (n "pyinrs") (v "0.1.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1bxj9br43in6icsgp4fl361av6z7dhymsnjkrv5mf6nahw0kqn40")))

(define-public crate-pyinrs-0.2.0 (c (n "pyinrs") (v "0.2.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "02gfy32gl8ww4cdlm7gvac0rkq92r7x9xqcd8clsdplk3k4m0hxk")))

(define-public crate-pyinrs-0.3.0 (c (n "pyinrs") (v "0.3.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1ry1572yv3nsxnkmpp27kjx70237i91qa6z1gianq82yg44s13db")))

(define-public crate-pyinrs-0.4.0 (c (n "pyinrs") (v "0.4.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1mm8dsqd6rfwfnfb6hrhnwsj1iy06yqygc7l4k7cgr1rxy5irhyg")))

(define-public crate-pyinrs-0.5.0 (c (n "pyinrs") (v "0.5.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0vrnazpcs36wcicb0cymxqmkd3hqpap1xb3fmhzy8m9yqq6sa1wc")))

(define-public crate-pyinrs-0.6.0 (c (n "pyinrs") (v "0.6.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1297l535b34s95cswn5ric93mb4cpcjha6fyyxz2d45pqkgad54b")))

(define-public crate-pyinrs-0.7.0 (c (n "pyinrs") (v "0.7.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "13rwd5zyy7zcbg8d5x0jx2kpq0sxka7yq49fqlic0aldln13qnw9")))

(define-public crate-pyinrs-1.0.0 (c (n "pyinrs") (v "1.0.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0pihynrw89vry9a7g8zvwk5fpp25aczz649smvzcg0kp0lwa3i6l")))

(define-public crate-pyinrs-1.1.0 (c (n "pyinrs") (v "1.1.0") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "00bhrpg65ynnzhf6kgn5vnbq5g9m9cj0ld83g4g0c630b7lfmmna")))

(define-public crate-pyinrs-1.2.0 (c (n "pyinrs") (v "1.2.0") (d (list (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0hfz2fnv06ca3bhfyzq4bzik74dya1l9jdhkljji4zfwz6hpwx92")))

(define-public crate-pyinrs-1.3.0 (c (n "pyinrs") (v "1.3.0") (d (list (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1vwyj8pmfm2bf5plvg2lg86knv4vriizbkrbs7zbg4p22zin7b77")))

(define-public crate-pyinrs-1.4.0 (c (n "pyinrs") (v "1.4.0") (d (list (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1pbxslszgh9vx9brgnhs80q0xa28p88bvq4dxhp7bj1s67n22cg9")))

(define-public crate-pyinrs-1.4.1 (c (n "pyinrs") (v "1.4.1") (d (list (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1kiikq8sw4cpr9n00p6lvdcvfg59pwq3nq2khk39qr1kv2xw3kpz")))

(define-public crate-pyinrs-1.5.0 (c (n "pyinrs") (v "1.5.0") (d (list (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "0klcbg47d0ci1b6k2l85mnx9g89c2x5zv0lrci1c7ics2sd5wvvw")))

(define-public crate-pyinrs-1.6.0 (c (n "pyinrs") (v "1.6.0") (d (list (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "0n3xlnakh8ixaszab8wykm9cs61mp57q2xwk203pn07392vj6jxv")))

(define-public crate-pyinrs-1.7.0 (c (n "pyinrs") (v "1.7.0") (d (list (d (n "criterion") (r "^0") (d #t) (k 2)) (d (n "mymatrix") (r "^0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0") (d #t) (k 2)))) (h "0mnrhk1wy7y5b5xpr55gi0jj4isrl4886amj34m7jpl53p9h1f6j")))

