(define-module (crates-io py da pydantic-logfire) #:use-module (crates-io))

(define-public crate-pydantic-logfire-0.0.0 (c (n "pydantic-logfire") (v "0.0.0") (h "0x2hmp8ry1saijblvsyw2n06vp17xd9p7ib582p3b387g6ik4cyf")))

(define-public crate-pydantic-logfire-0.0.1 (c (n "pydantic-logfire") (v "0.0.1") (h "0cmlzg9qybkpay1iaia21bdh3fxph9z8fyq7pmh4llv946780aig")))

