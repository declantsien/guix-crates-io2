(define-module (crates-io py -s py-sr25519-bindings) #:use-module (crates-io))

(define-public crate-py-sr25519-bindings-0.1.2 (c (n "py-sr25519-bindings") (v "0.1.2") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "pyo3") (r "^0.9.2") (d #t) (k 0)) (d (n "schnorrkel") (r "^0.9.1") (d #t) (k 0)))) (h "0w2sdshjclbg51aj30rpw7mv1is9n1lmzs27g3ivfi81x4cfnb9n") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

