(define-module (crates-io py gm pygmentize) #:use-module (crates-io))

(define-public crate-pygmentize-0.1.0 (c (n "pygmentize") (v "0.1.0") (d (list (d (n "winapi-util") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "05la80raccw3v268yj8ssyc45968zmbcr1gf04i7wzg5dhgg7x0z")))

(define-public crate-pygmentize-0.2.0 (c (n "pygmentize") (v "0.2.0") (d (list (d (n "winapi-util") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0lxld6z88bb4scq9j4alcqblbs2qpmcfr1n8rr1iqnh43vx1azgf")))

