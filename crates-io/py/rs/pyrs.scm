(define-module (crates-io py rs pyrs) #:use-module (crates-io))

(define-public crate-pyrs-0.1.0 (c (n "pyrs") (v "0.1.0") (d (list (d (n "natural") (r "^0.4.0") (f (quote ("serde_support"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.17") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lf45zm6yv5azx7vj8y9dq6p6apyj43rcwdi9vmvj6fp0gr2yh89")))

