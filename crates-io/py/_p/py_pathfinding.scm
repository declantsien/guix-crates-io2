(define-module (crates-io py _p py_pathfinding) #:use-module (crates-io))

(define-public crate-py_pathfinding-0.1.0 (c (n "py_pathfinding") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "19w1j0q2kvjzlxxh50db5968iky7x6h83gr63y1x84kld9c1yvyy")))

(define-public crate-py_pathfinding-0.1.1 (c (n "py_pathfinding") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "08h81lnkjg6508hlkwbil80b7d1bnfm4pzn0zzhm8242a2r079b6")))

(define-public crate-py_pathfinding-0.1.2 (c (n "py_pathfinding") (v "0.1.2") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "0v8hm0lxz6m3bakl358cwz41sji5v7v1gghbzmk0krwb47d4npya")))

(define-public crate-py_pathfinding-0.1.3 (c (n "py_pathfinding") (v "0.1.3") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "1bm33y17k7888r9765hnhqkj175fhdjflg11ymjp0l14k3lqjb3b")))

(define-public crate-py_pathfinding-0.1.4 (c (n "py_pathfinding") (v "0.1.4") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "17n2mjvrilfnsykkb3cca93w31kc283axhwj9ln7yxd64x6wl598")))

(define-public crate-py_pathfinding-0.1.5 (c (n "py_pathfinding") (v "0.1.5") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)))) (h "0yxlbjjm98hv2hgnfjrrpxfhjnd2dx2kzlivlkh51qlg83var84p")))

