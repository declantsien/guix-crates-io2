(define-module (crates-io bl ue blue_engine_egui) #:use-module (crates-io))

(define-public crate-blue_engine_egui-0.1.0 (c (n "blue_engine_egui") (v "0.1.0") (d (list (d (n "blue_engine") (r "^0.4.11") (d #t) (k 0)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.19") (d #t) (k 0)) (d (n "egui_winit_platform") (r "^0.16") (d #t) (k 0)) (d (n "epi") (r "^0.17") (d #t) (k 0)))) (h "0dlbxsa7l4wbsjaj86l7ww7ri07f8lak9k3maddzrmi15mk2iw40")))

(define-public crate-blue_engine_egui-0.2.0 (c (n "blue_engine_egui") (v "0.2.0") (d (list (d (n "blue_engine") (r "^0.4.12") (d #t) (k 0)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.19.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.19.0") (d #t) (k 0)))) (h "10pv9alannw8qax1yz1g19xfrw3hdc2ss3fcwp00v6xxrpni5pxa")))

(define-public crate-blue_engine_egui-0.3.0 (c (n "blue_engine_egui") (v "0.3.0") (d (list (d (n "blue_engine") (r "^0.4.13") (d #t) (k 0)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.19.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.19.0") (d #t) (k 0)))) (h "1qpg7kw05k740m1rnj1v1mmyavkwzfcjzh94n8d0b2njqgzda7np")))

(define-public crate-blue_engine_egui-0.3.1 (c (n "blue_engine_egui") (v "0.3.1") (d (list (d (n "blue_engine") (r "^0.4.14") (d #t) (k 0)) (d (n "egui") (r "^0.19") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.19.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.19.0") (d #t) (k 0)))) (h "0f9xfmi3wq54jkjffxg2dhjr21h4s73nw1sfj4animh3i78v1aij")))

(define-public crate-blue_engine_egui-0.4.0 (c (n "blue_engine_egui") (v "0.4.0") (d (list (d (n "blue_engine") (r "^0.4.16") (k 0)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.20.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.20.1") (d #t) (k 0)))) (h "0yal1v8xq6fzljxn15gd9d4hmdvk2pl2m7nf2pmla7c2y19da58s")))

(define-public crate-blue_engine_egui-0.4.1 (c (n "blue_engine_egui") (v "0.4.1") (d (list (d (n "blue_engine") (r "^0.4") (k 0)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.20.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.20.1") (d #t) (k 0)))) (h "0b008358r7qypclmm035qy90npikpqyqn78kx663qnakw6ywxdkw")))

(define-public crate-blue_engine_egui-0.4.2 (c (n "blue_engine_egui") (v "0.4.2") (d (list (d (n "blue_engine") (r "^0.4") (k 0)) (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.20.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.20.1") (d #t) (k 0)))) (h "18iqamd9b4fvnps2ql3s1n7xl5cy9gf6ywvcn1p06mk2gnar8drc")))

(define-public crate-blue_engine_egui-0.4.3 (c (n "blue_engine_egui") (v "0.4.3") (d (list (d (n "blue_engine") (r "^0.4.27") (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.22.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.22.0") (d #t) (k 0)))) (h "1sfy6gf3iacv8c7g8lvf2djybll5zbcz13jawpda2mra51049d3k")))

(define-public crate-blue_engine_egui-0.4.4 (c (n "blue_engine_egui") (v "0.4.4") (d (list (d (n "blue_engine") (r "^0.4.28") (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.22.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.22.0") (d #t) (k 0)))) (h "0h93n1b7hhkgzv5ji4b5afwnb2bj3agqq2hk5a7bybz94y5jfmdn")))

(define-public crate-blue_engine_egui-0.5.0 (c (n "blue_engine_egui") (v "0.5.0") (d (list (d (n "blue_engine") (r "^0.5.0") (k 0)) (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.23.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.23.0") (d #t) (k 0)))) (h "05ph3z2rcy05nh1yq3cqikv7bd4v31r670c2c72j2dbnsf5bs44f")))

(define-public crate-blue_engine_egui-0.5.2 (c (n "blue_engine_egui") (v "0.5.2") (d (list (d (n "blue_engine") (r "^0.5.2") (k 0)) (d (n "egui") (r "^0.25.0") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.25.0") (f (quote ("winit"))) (d #t) (k 0)) (d (n "egui-winit") (r "^0.25.0") (d #t) (k 0)))) (h "1ycil74b7mk12nqnxn4c7s461n4wwrsqwyndr6xqyracj6mxia47")))

