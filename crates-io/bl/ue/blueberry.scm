(define-module (crates-io bl ue blueberry) #:use-module (crates-io))

(define-public crate-blueberry-0.1.0 (c (n "blueberry") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0876607jdnnxays0hrrad32q59x2i8424spp7bxrl5bzwzcrlyxy")))

