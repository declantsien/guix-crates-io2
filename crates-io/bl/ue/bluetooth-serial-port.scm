(define-module (crates-io bl ue bluetooth-serial-port) #:use-module (crates-io))

(define-public crate-bluetooth-serial-port-0.2.0 (c (n "bluetooth-serial-port") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "nix") (r "^0.4.1") (d #t) (k 0)))) (h "08sp65as29gm32a1gyykjcjf7zh9r2i58fgllhz119m7c8jrgw8x") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.2.1 (c (n "bluetooth-serial-port") (v "0.2.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "nix") (r "^0.4.1") (d #t) (k 0)))) (h "1nzv34ar73pip3clkvw1dnvhda1a4q3zy2q3yqjlp9rb3bks1zaa") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.3.0 (c (n "bluetooth-serial-port") (v "0.3.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "0x1cfg2lyxlzapylkcq77i19gmb8pgxpy8qfg22apmfmwfdsb41f") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.4.0 (c (n "bluetooth-serial-port") (v "0.4.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "15z36ws1086xf0brk2zpbl0kjnns56lycn377nvdmx636qw2jkw4") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.4.1 (c (n "bluetooth-serial-port") (v "0.4.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "0rsv3lqmq0wy27q5x9al5cg09r2nxa26f9znsr6ifsn1qjfwvy52") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.5.0 (c (n "bluetooth-serial-port") (v "0.5.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "1j6rz8n64fkx7y77l1sqrynkhzkfdxpldjz9cayn0yz35d152lsn") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.5.1 (c (n "bluetooth-serial-port") (v "0.5.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "1djb98wn70qapa4gbjbq111x1n0bh5w0khsxv1sd9wxrikhdffym") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-0.6.0 (c (n "bluetooth-serial-port") (v "0.6.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "16wa9pbh14bc48fi4wl8r0vhnf5rq818w1db1sgy79ai7ywgi9x5") (f (quote (("test_without_hardware"))))))

