(define-module (crates-io bl ue bluejay-printer) #:use-module (crates-io))

(define-public crate-bluejay-printer-0.1.0-alpha.2 (c (n "bluejay-printer") (v "0.1.0-alpha.2") (d (list (d (n "bluejay-core") (r "^0.1.0-alpha.2") (d #t) (k 0)))) (h "1z4x108pxn0bhgav9np0zard7b4md8nza19g8vxhkkqxgvwfwzqc")))

(define-public crate-bluejay-printer-0.1.0-alpha.3 (c (n "bluejay-printer") (v "0.1.0-alpha.3") (d (list (d (n "bluejay-core") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "insta") (r "^1.28") (f (quote ("glob"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "0hpavwcb70rclpaj4wys3d0j20vi92d9bb23gna5bs3lik2lp0nm")))

(define-public crate-bluejay-printer-0.1.0-alpha.4 (c (n "bluejay-printer") (v "0.1.0-alpha.4") (d (list (d (n "bluejay-core") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "insta") (r "^1.28") (f (quote ("glob"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "0cfbvqzy5g0qlfafrir80ajw50p2f3bacy0b8ya31f6m22nzr645")))

(define-public crate-bluejay-printer-0.1.0-alpha.5 (c (n "bluejay-printer") (v "0.1.0-alpha.5") (d (list (d (n "bluejay-core") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "insta") (r "^1.28") (f (quote ("glob"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "0lla2k2cs1xqzhavifpzk4ppy2f14ljc126dfi32rc1zfnp1qnvb")))

(define-public crate-bluejay-printer-0.1.0 (c (n "bluejay-printer") (v "0.1.0") (d (list (d (n "bluejay-core") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.28") (f (quote ("glob"))) (d #t) (k 2)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "1dzwgh0k9fzabca441y3y9nc9zmx44hcjc82lnnj72ngds6vrx3q")))

