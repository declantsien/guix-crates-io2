(define-module (crates-io bl ue bluetooth_client) #:use-module (crates-io))

(define-public crate-bluetooth_client-0.0.1 (c (n "bluetooth_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1wvav7nb1p0g73hajxynbll7xqyxsnmvzkjv4xlb2l278r3s5kjh")))

(define-public crate-bluetooth_client-0.0.1-SNAPSHOT (c (n "bluetooth_client") (v "0.0.1-SNAPSHOT") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0v5i6czr7476v0hisb5hwsc87yvw08vm9lmfchfh0qjmlx5p0jb6") (y #t)))

(define-public crate-bluetooth_client-0.0.2 (c (n "bluetooth_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "1vqv89258y17awr80idr06drllqhnjy5rvjrz6nw5i4m8hcwjg93")))

(define-public crate-bluetooth_client-0.0.3 (c (n "bluetooth_client") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0sn68m8bgyb7cl8xl43n3qk71wjb8mg1f9x7iipx1rydw24gz9mc")))

(define-public crate-bluetooth_client-0.0.4 (c (n "bluetooth_client") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1lpcndlf9yd7167q0vhxw1ryb7984ghpy1k5hc1iwy52a905860v")))

(define-public crate-bluetooth_client-0.0.5 (c (n "bluetooth_client") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0sksi735znsxpmxpkmv0cafdy7wj30qjvk4m105ilagqg9cgl5s2")))

(define-public crate-bluetooth_client-0.0.6 (c (n "bluetooth_client") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1xdmqsbq7mp886sqybwyldk3vvrljrjgcv5fkb89skh1sya3fbld")))

(define-public crate-bluetooth_client-0.0.7 (c (n "bluetooth_client") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "08hrfpawg2rnzbj0jb35icl42y9yi3ak2acgw3z8bgj1sw035kxj")))

(define-public crate-bluetooth_client-0.0.8 (c (n "bluetooth_client") (v "0.0.8") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.40") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0kd4hazzpbpzh4hrrrv35hh62bgmn1xh0p2qv2m6pb6k2cm35ddz")))

(define-public crate-bluetooth_client-0.0.9 (c (n "bluetooth_client") (v "0.0.9") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.41") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1nl20rh8mkbyc6pmmhiir23wvbv76yryg8ih34f3f61yn5gx75dr")))

(define-public crate-bluetooth_client-0.0.10 (c (n "bluetooth_client") (v "0.0.10") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.43") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "1pa43bb6z2bh3xmznrwsmh0qnifa1a2nhqm1fvsj74lxyvf9669m")))

(define-public crate-bluetooth_client-0.0.12 (c (n "bluetooth_client") (v "0.0.12") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0rxc1npl6rljrx5yxqzrvs54923h1wa1gyx83inid3z9fwl1nxza")))

