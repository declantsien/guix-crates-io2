(define-module (crates-io bl ue bluebird) #:use-module (crates-io))

(define-public crate-bluebird-0.1.0 (c (n "bluebird") (v "0.1.0") (d (list (d (n "nofmt") (r "^1.0") (d #t) (k 0)))) (h "1mjh1822w8zk0h1qibpg3r7m2w5r8q0n1vf06d0r0ab5wf95fd0c")))

(define-public crate-bluebird-0.2.0 (c (n "bluebird") (v "0.2.0") (d (list (d (n "nofmt") (r "^1.0") (d #t) (k 0)))) (h "14wc1bnj9lm58x3dpnaj3p6n5pbjc80lk5lbca6f52sm07y71yvw")))

(define-public crate-bluebird-0.3.0 (c (n "bluebird") (v "0.3.0") (d (list (d (n "nofmt") (r "^1.0") (d #t) (k 0)))) (h "1bk9z5n2cl9g8s2jkk7wh5wlimyzprkf7alcchm5a9gpph1jzvqy")))

