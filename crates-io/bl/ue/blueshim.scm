(define-module (crates-io bl ue blueshim) #:use-module (crates-io))

(define-public crate-blueshim-0.0.0 (c (n "blueshim") (v "0.0.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "plist") (r "^1.6.0") (d #t) (k 0)) (d (n "rsa") (r "^0.9.6") (f (quote ("hazmat"))) (d #t) (k 0)) (d (n "rustls") (r "^0.22.1") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "serde"))) (d #t) (k 0)) (d (n "x509-cert") (r "^0.2.4") (f (quote ("builder" "hazmat"))) (d #t) (k 0)))) (h "0j5zvf627pghzx1az7jvjngaw7w3bnjs37fr2xn8hg7cjfpcmz1v")))

