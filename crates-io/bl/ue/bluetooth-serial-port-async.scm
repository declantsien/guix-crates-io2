(define-module (crates-io bl ue bluetooth-serial-port-async) #:use-module (crates-io))

(define-public crate-bluetooth-serial-port-async-0.6.0 (c (n "bluetooth-serial-port-async") (v "0.6.0") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1k3zj7m3mz8gshrs5ywkcfpyv662rl3p1afns0wlcp8aj6d3zmji") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-async-0.6.1 (c (n "bluetooth-serial-port-async") (v "0.6.1") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1qgpm90n7ndf128k33r6kkb78j4z1rcvrqc6zgi761dhw382zgp3") (f (quote (("test_without_hardware"))))))

(define-public crate-bluetooth-serial-port-async-0.6.2 (c (n "bluetooth-serial-port-async") (v "0.6.2") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "1f5fv790237zckv0qpzyh8982gxfd01hsgqlkr3hwjjnbld1yxky")))

(define-public crate-bluetooth-serial-port-async-0.6.3 (c (n "bluetooth-serial-port-async") (v "0.6.3") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "mio") (r "^0.6.0") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)))) (h "1s4rnncqscqfvmmwkhj3inqmdn3pdpgh0q25imnclf46pqpymhk0")))

