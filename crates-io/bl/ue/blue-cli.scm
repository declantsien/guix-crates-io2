(define-module (crates-io bl ue blue-cli) #:use-module (crates-io))

(define-public crate-blue-cli-0.0.1 (c (n "blue-cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "1fyly5mi0grafg8fiash0mx377xz954s9s2klxb1azmn1m4a1nsr") (y #t)))

