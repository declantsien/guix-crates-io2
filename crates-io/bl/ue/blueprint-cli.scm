(define-module (crates-io bl ue blueprint-cli) #:use-module (crates-io))

(define-public crate-blueprint-cli-0.1.0 (c (n "blueprint-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1dm87hx8xhna6karmpbw904y11gc8lsc89zwhsdd6fi5rlsz4d0i")))

(define-public crate-blueprint-cli-0.1.2 (c (n "blueprint-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ryx29dwii9d9j0l8p2qka0mq4qmm8hpysvbjvb0g8hl8in3xb40")))

(define-public crate-blueprint-cli-0.1.3 (c (n "blueprint-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qd81j43l6lcgxb42kf1gbv7sfg11c9rhi9jbp3yp8mk1mqf5gsr")))

(define-public crate-blueprint-cli-0.1.4 (c (n "blueprint-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1xplrx5b5vgi6sl9b78lz8yppnmx6g3c1iq2azdfjbwc3m0zfvi6")))

(define-public crate-blueprint-cli-1.0.0 (c (n "blueprint-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "141w2v2ikj05p290yfz4kgb135sbwfb0wyp9sim9vgr7b5szynn1")))

