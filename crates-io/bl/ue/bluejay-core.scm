(define-module (crates-io bl ue bluejay-core) #:use-module (crates-io))

(define-public crate-bluejay-core-0.1.0-alpha.0 (c (n "bluejay-core") (v "0.1.0-alpha.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "01xazq1bbq5lbpk8rfaxdi3vlfjjd4aaajmqyxcrx27lb9ay87f9")))

(define-public crate-bluejay-core-0.1.0-alpha.1 (c (n "bluejay-core") (v "0.1.0-alpha.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nsjxmac329yc31l4zz0wjj97hiqs7p2rjbglfplzkqgz6k0110v")))

(define-public crate-bluejay-core-0.1.0-alpha.2 (c (n "bluejay-core") (v "0.1.0-alpha.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sp8xcsjdknah8sdah59ph12npqv72fka3yr2csg1hwbv8ygkgln")))

(define-public crate-bluejay-core-0.1.0-alpha.3 (c (n "bluejay-core") (v "0.1.0-alpha.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hrdl7szqnn77vz5x6dhka5lm240c70lgyzx1kbsl9ddx553k3dp")))

(define-public crate-bluejay-core-0.1.0-alpha.4 (c (n "bluejay-core") (v "0.1.0-alpha.4") (d (list (d (n "enum-as-inner") (r "^0.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hy31jj4h0cjw09nq87sv6shx11p0bz1lyfcp8jm2zp9n2s4s6rg") (s 2) (e (quote (("serde_json" "dep:serde_json"))))))

(define-public crate-bluejay-core-0.1.0-alpha.5 (c (n "bluejay-core") (v "0.1.0-alpha.5") (d (list (d (n "enum-as-inner") (r "^0.5") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gpjwnspp58cc7xv1yg8899dz5afvppvpwbnny6z3fbyysqx5xmj") (s 2) (e (quote (("serde_json" "dep:serde_json"))))))

(define-public crate-bluejay-core-0.1.0 (c (n "bluejay-core") (v "0.1.0") (d (list (d (n "enum-as-inner") (r "^0.6") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p5bx17rwl43ziv72v4jmv0s1h1sn5192amki2cqp4zfm2h8wd9f") (s 2) (e (quote (("serde_json" "dep:serde_json"))))))

