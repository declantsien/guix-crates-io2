(define-module (crates-io bl ue bluenoise) #:use-module (crates-io))

(define-public crate-bluenoise-0.1.0 (c (n "bluenoise") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "glam") (r "^0.9.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "173qv3fj7kg45mvm1qvvh7k0isb1wh85jsjgycz4rb0rg1fpbz6p")))

(define-public crate-bluenoise-0.1.1 (c (n "bluenoise") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "glam") (r "^0.9.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "0xrs4s48dffisjwghpbpwgb71fb8bdfx1ia7w215imgc9aszpls3")))

(define-public crate-bluenoise-0.1.2 (c (n "bluenoise") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "glam") (r "^0.9.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "0awm0i0l8046d6avc3nnhjyziqqwdsz4y6xl9pmwm9nrh79rkcrd")))

(define-public crate-bluenoise-0.2.0 (c (n "bluenoise") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "glam") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 2)))) (h "0z454i50k54w6aw74i7y0120bpqa9q4p79s4v1q0bqqffq13q3a3")))

(define-public crate-bluenoise-0.2.1 (c (n "bluenoise") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "glam") (r "^0.13.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 2)))) (h "0vxas3pj9nwav7rkcac3v8lddjkzi8hsm430dp0jlry3h8bvv25z")))

