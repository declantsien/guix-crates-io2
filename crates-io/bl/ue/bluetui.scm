(define-module (crates-io bl ue bluetui) #:use-module (crates-io))

(define-public crate-bluetui-0.1.0 (c (n "bluetui") (v "0.1.0") (d (list (d (n "bluer") (r "^0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hprlmni20xx662hmx5nn9jqyk6q2qpb869cqgsr99im2psaj7zk")))

(define-public crate-bluetui-0.2.0 (c (n "bluetui") (v "0.2.0") (d (list (d (n "async-channel") (r "^2.2.0") (d #t) (k 0)) (d (n "bluer") (r "^0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bkbrbck7v377k3pb45f11cv3j07mi8ffjd1mym2q8r18r3k00f6")))

(define-public crate-bluetui-0.4.0 (c (n "bluetui") (v "0.4.0") (d (list (d (n "async-channel") (r "^2") (d #t) (k 0)) (d (n "bluer") (r "^0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1dc277am49sbgafzq3dh08mx9rgahafqrqdscyhs2znxjkhjamd0")))

