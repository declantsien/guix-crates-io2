(define-module (crates-io bl ue bluetooth-hci) #:use-module (crates-io))

(define-public crate-bluetooth-hci-0.0.1 (c (n "bluetooth-hci") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1xskrpnllkdh1afrk3zfchkjbp4a6sbq5j6f05gxq3rkrc37lzby") (f (quote (("version-5-0") ("version-4-2") ("version-4-1") ("default-features" "version-4-1"))))))

(define-public crate-bluetooth-hci-0.0.2 (c (n "bluetooth-hci") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "1bh2gxk3lciwlpj2df8sqwwsdzm42fsrc63b9nfbjv9anim1y5rm") (f (quote (("version-5-0") ("version-4-2") ("version-4-1") ("default-features" "version-4-1"))))))

(define-public crate-bluetooth-hci-0.0.3 (c (n "bluetooth-hci") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "0rv5g3hmh9m2m1v12lydfqg39k9sxy9hzgari9a5i1i4hxwm2rcm") (f (quote (("version-5-0") ("version-4-2") ("version-4-1") ("default-features" "version-4-1"))))))

(define-public crate-bluetooth-hci-0.1.0 (c (n "bluetooth-hci") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0chhxf708d86x762hphz1x11ffb7q76h2hlq1jf3d2k4x0h2p35s") (f (quote (("version-5-0") ("version-4-2") ("version-4-1") ("default" "version-4-1"))))))

