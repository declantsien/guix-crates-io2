(define-module (crates-io bl ue blue-noise) #:use-module (crates-io))

(define-public crate-blue-noise-0.1.0 (c (n "blue-noise") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "0y94cqb0q3b6hj8xf5zhaj9qnqr5llqaw0m86pbk0m6k1c0b80lb")))

(define-public crate-blue-noise-0.1.1 (c (n "blue-noise") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "0x3a38d5rd3winbp4j0bc0hmg4zwxw4px0g2nzngk4w0mj9m7279")))

