(define-module (crates-io bl ue bluenoisers) #:use-module (crates-io))

(define-public crate-bluenoisers-1.0.0 (c (n "bluenoisers") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "154lrwwm9dih718c6z3rwgvrxxxbg4gfxadva1shf0aikmn5kvfp")))

(define-public crate-bluenoisers-1.1.0 (c (n "bluenoisers") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0lc1i775gi4cz2b9al0lfx803hx0v6sfbni9klllahxsab37qx4j") (y #t)))

(define-public crate-bluenoisers-1.1.1 (c (n "bluenoisers") (v "1.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06cbp084li4f88zsa1z957pv2ysi54976zd2lc9hd87ar6yrb6w0")))

