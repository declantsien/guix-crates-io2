(define-module (crates-io bl ue blue_engine_imgui) #:use-module (crates-io))

(define-public crate-blue_engine_imgui-0.1.0 (c (n "blue_engine_imgui") (v "0.1.0") (d (list (d (n "blue_engine") (r "^0.4.9") (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8.1-alpha.0") (d #t) (k 0)))) (h "1n5cs2hrz7h9xzigalyphr89qa3npb55gbb79x7f7cf06z7hj9sx")))

(define-public crate-blue_engine_imgui-0.3.0 (c (n "blue_engine_imgui") (v "0.3.0") (d (list (d (n "blue_engine") (r "^0.4.13") (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8.1-alpha.0") (d #t) (k 0)))) (h "016g568b5nwa2l943wyqgiz8z3jslf0aipw05nhm69lchqqklc9b")))

(define-public crate-blue_engine_imgui-0.3.1 (c (n "blue_engine_imgui") (v "0.3.1") (d (list (d (n "blue_engine") (r "^0.4.14") (d #t) (k 0)) (d (n "imgui") (r "^0.8.2") (d #t) (k 0)) (d (n "imgui-wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "imgui-winit-support") (r "^0.8.1-alpha.0") (d #t) (k 0)))) (h "0c0kgij8mwp29622l8j3cvq83yg56zvmqnf4k2djjv541x7q02c2")))

