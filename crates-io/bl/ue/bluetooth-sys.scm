(define-module (crates-io bl ue bluetooth-sys) #:use-module (crates-io))

(define-public crate-bluetooth-sys-0.1.0 (c (n "bluetooth-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "16ldch9j6k09p1jww057apwfd2zg01yxf712dvvc23sc92qxw4km")))

(define-public crate-bluetooth-sys-0.1.1 (c (n "bluetooth-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "1f962dkrvxffpvaqk9kkgnpn1xl497s1567vxz9vjpsv586hz535") (y #t) (l "bluetooth")))

(define-public crate-bluetooth-sys-0.1.3 (c (n "bluetooth-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "1hynn72aa8icj83mv41935ss1zalln916l3s07m96iq35c90dmgl") (l "bluetooth")))

