(define-module (crates-io bl ue blue_engine_core) #:use-module (crates-io))

(define-public crate-blue_engine_core-0.1.0 (c (n "blue_engine_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bytemuck") (r "^1.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (f (quote ("vulkan-portability"))) (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.9.0") (d #t) (k 0)))) (h "0m4xkqcm9b81dzsjrippy238nx2x7rjpkkigpy8c73g49hjg6wm7") (y #t)))

