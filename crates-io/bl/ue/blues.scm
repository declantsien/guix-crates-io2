(define-module (crates-io bl ue blues) #:use-module (crates-io))

(define-public crate-blues-0.0.0 (c (n "blues") (v "0.0.0") (h "0j8v0yh5yh98cik85pkv3ccdall46kx3ywji4xym60sxnfl4ah8n")))

(define-public crate-blues-0.1.0 (c (n "blues") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)) (d (n "zbus") (r "^3.14.1") (k 0)))) (h "0f5rpkpglahrks68v0riwzviffwc7w8j2mvwz68jvmygpdnhgd79") (f (quote (("tokio" "zbus/tokio") ("default" "zbus/async-io"))))))

