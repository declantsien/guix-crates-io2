(define-module (crates-io bl ue bluejay-schema-comparator) #:use-module (crates-io))

(define-public crate-bluejay-schema-comparator-0.1.0 (c (n "bluejay-schema-comparator") (v "0.1.0") (d (list (d (n "bluejay-core") (r "^0.1.0") (d #t) (k 0)) (d (n "bluejay-printer") (r "^0.1.0") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wz03r3izgs6a8mj2rwi72fw5ii7ky287d87w6872vmlazgh5gd2")))

