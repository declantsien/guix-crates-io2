(define-module (crates-io bl ue bluenrg) #:use-module (crates-io))

(define-public crate-bluenrg-0.0.1 (c (n "bluenrg") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bluetooth-hci") (r "^0.0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "15cyjq289kq4grvx20zdngg0fjm0ps5zpb58wigz5lq40ay92zgv") (f (quote (("ms") ("default" "ms"))))))

(define-public crate-bluenrg-0.0.2 (c (n "bluenrg") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bluetooth-hci") (r "^0.0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "06f52gprpzasvpvy46bfnnvrv1gg906d3hwyxizc953dxnkryiwv") (f (quote (("ms") ("default" "ms"))))))

(define-public crate-bluenrg-0.0.3 (c (n "bluenrg") (v "0.0.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bluetooth-hci") (r "^0.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "13qjhhjy0yrwdh7gs84kazxxb63skx3x2fp5aa3bzyxqlwdnsnwb") (f (quote (("ms") ("default" "ms"))))))

(define-public crate-bluenrg-0.1.0 (c (n "bluenrg") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bluetooth-hci") (r "^0.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "06fqk1wwlnanviacj722cv8wh5zh9l316i0dxcrg2myc8y9lmh89") (f (quote (("ms") ("default" "ms"))))))

