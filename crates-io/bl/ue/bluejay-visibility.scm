(define-module (crates-io bl ue bluejay-visibility) #:use-module (crates-io))

(define-public crate-bluejay-visibility-0.1.0-alpha.5 (c (n "bluejay-visibility") (v "0.1.0-alpha.5") (d (list (d (n "bluejay-core") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "elsa") (r "^1.8.1") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5") (d #t) (k 0)) (d (n "insta") (r "^1.28") (d #t) (k 2)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "0y6xmbdxi60z9nq6rqi6d0f9gwn5vhip2x4rjccnlwn67rmdpy43")))

(define-public crate-bluejay-visibility-0.1.0 (c (n "bluejay-visibility") (v "0.1.0") (d (list (d (n "bluejay-core") (r "^0.1.0") (d #t) (k 0)) (d (n "elsa") (r "^1.8.1") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.6") (d #t) (k 0)) (d (n "insta") (r "^1.28") (d #t) (k 2)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)))) (h "1adl5cv7vigz33cb4ss9ilm1vy2h6x43lzaaq7b4wrpxm4pmyvkq")))

