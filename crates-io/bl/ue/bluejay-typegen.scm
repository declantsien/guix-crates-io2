(define-module (crates-io bl ue bluejay-typegen) #:use-module (crates-io))

(define-public crate-bluejay-typegen-0.1.0-alpha.4 (c (n "bluejay-typegen") (v "0.1.0-alpha.4") (d (list (d (n "bluejay-typegen-macro") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0vq5mc2ncqqqbs6nw85rs6gwwh2zyvwzpgi7kmg6kxx651ga492j")))

(define-public crate-bluejay-typegen-0.1.0-alpha.5 (c (n "bluejay-typegen") (v "0.1.0-alpha.5") (d (list (d (n "bluejay-typegen-macro") (r "^0.1.0-alpha.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14r004qg6cabb3f0i42cmal2dim78nw2c0jpllzi2yca9kak2yr9")))

(define-public crate-bluejay-typegen-0.1.0 (c (n "bluejay-typegen") (v "0.1.0") (d (list (d (n "bluejay-typegen-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "mnsrd") (r "^0.1.39") (o #t) (d #t) (k 0) (p "miniserde")) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "srd") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "11c7mcd7j5rrdz1bv8nxr78cbflcd0r0194d9jhkahb094wks3bq") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:srd" "bluejay-typegen-macro/serde") ("miniserde" "dep:mnsrd" "bluejay-typegen-macro/miniserde"))))))

