(define-module (crates-io bl #{70}# bl702-pac) #:use-module (crates-io))

(define-public crate-bl702-pac-0.0.1 (c (n "bl702-pac") (v "0.0.1") (d (list (d (n "riscv") (r "^0.6.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0q4xmn0krpjb30b7izs1lwypxv8xm26h15n7ibig121yldang77b")))

(define-public crate-bl702-pac-0.0.2 (c (n "bl702-pac") (v "0.0.2") (d (list (d (n "riscv") (r "^0.8.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1yp8b132vbzv0kix0mi616av4y9r44maks1r79k9ph4fp215c59v")))

(define-public crate-bl702-pac-0.0.3 (c (n "bl702-pac") (v "0.0.3") (d (list (d (n "critical-section") (r "^1.1.1") (o #t) (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0q0j9fbhg0xk6dvi9wfr18dp4a5yjhmsd9q42psnqvhicgdyk6rv")))

