(define-module (crates-io bl ob blobd-token) #:use-module (crates-io))

(define-public crate-blobd-token-0.0.1 (c (n "blobd-token") (v "0.0.1") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1j3kbcwrpm2idyd87dwy4p054idrrs81jgg30g8xwjkx4zn5gcwz")))

(define-public crate-blobd-token-0.1.0 (c (n "blobd-token") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0rk31q779lzfkjdz29awc1ngx3rmhpxn5vhnvdjcv819j4jjaj5q")))

(define-public crate-blobd-token-0.2.0 (c (n "blobd-token") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "18v8k18c5lghs6p8p2c5ckypjj4ihla64fk7s5lbk4ng7iivsiq4")))

