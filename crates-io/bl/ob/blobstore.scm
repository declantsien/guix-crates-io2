(define-module (crates-io bl ob blobstore) #:use-module (crates-io))

(define-public crate-blobstore-0.1.0 (c (n "blobstore") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 0)))) (h "12mw0ndb8x9fxsvc24bxms1qm14ghjax31srbwmhlx6xbw3ba2v2")))

(define-public crate-blobstore-0.1.1 (c (n "blobstore") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 0)))) (h "0jm40swq63cznb8815r8c1p000wp6hxh7pmv1ax78fbrsn37rq4p")))

