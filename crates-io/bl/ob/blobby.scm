(define-module (crates-io bl ob blobby) #:use-module (crates-io))

(define-public crate-blobby-0.1.0 (c (n "blobby") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "04vyjjnk222rdfqpyv7aknqarsvvyh1jkjhza8j058msh3zpp7nd")))

(define-public crate-blobby-0.1.1 (c (n "blobby") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1cg3l0a2kcfyzv7nnn1ikjqv9fg24bl0fkrffwg0i6q49vap91sl")))

(define-public crate-blobby-0.1.2 (c (n "blobby") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "byteorder") (r "^1") (f (quote ("std"))) (d #t) (k 2)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1xicpf3s2mi5xqnx8ps5mdych4ib5nh2nfsbrsg8ar8bjk1girbg")))

(define-public crate-blobby-0.2.0 (c (n "blobby") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("std"))) (d #t) (k 2)) (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1ab3ghdgf0r5x0aw192p9knbbv31x3yfx99clgv4frdbwbm7azji")))

(define-public crate-blobby-0.3.0 (c (n "blobby") (v "0.3.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)))) (h "1s2f3a7lx5rd26554d9940basff7qpyf1y8gkc309cgc8csmalpw")))

(define-public crate-blobby-0.3.1 (c (n "blobby") (v "0.3.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1v7a6lzxbvrxpnk2jv895315v7yas1cvk26mmbl90ylp1719ax44")))

