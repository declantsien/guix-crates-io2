(define-module (crates-io bl ob blobber) #:use-module (crates-io))

(define-public crate-blobber-0.1.0 (c (n "blobber") (v "0.1.0") (h "0anrnjvbd707rh4n0a3awnkgplfv79rxqjv60xyc55200a2gilrj")))

(define-public crate-blobber-0.1.1 (c (n "blobber") (v "0.1.1") (h "1kjlg0rznghmf3y7r710i46jc5z3svgc6dq4llwn6vqdw9dvdn3z")))

(define-public crate-blobber-0.1.3 (c (n "blobber") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06wca6krvfkhw281zipm5fihgdasq999s2q8n6qp95pyjz3qyaii")))

(define-public crate-blobber-0.1.4 (c (n "blobber") (v "0.1.4") (h "0qb5pjsv49cnpl2yadbw096h6380cxza0wj6dj39c3h7zja69fh9")))

(define-public crate-blobber-0.1.5 (c (n "blobber") (v "0.1.5") (h "1n0a1islqnkca0k032wd59wf8cdz4asnsk6yzj3s29bx6h9cay1y")))

(define-public crate-blobber-0.1.6 (c (n "blobber") (v "0.1.6") (h "12nmbh63gymahjilh9vzgcjyccafrd4g2cbkw06idsy7740a52ah")))

(define-public crate-blobber-0.1.7 (c (n "blobber") (v "0.1.7") (h "1hgafw0k1cspqsgyifprna8cia10l3z130yra3khb3awsn685ms0")))

