(define-module (crates-io bl ob blob-store) #:use-module (crates-io))

(define-public crate-blob-store-0.1.0 (c (n "blob-store") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.2.0") (d #t) (k 0)))) (h "1a75hkj8zg8369apqw12gh94chvc5gnf05vqmxqsizv0g5an5hs1") (y #t)))

(define-public crate-blob-store-0.1.1 (c (n "blob-store") (v "0.1.1") (h "03c4a5ykc901r2rwsjh9yyxxlhsk3bnbyyzmrp6vjhsyhvv6hj26") (y #t)))

