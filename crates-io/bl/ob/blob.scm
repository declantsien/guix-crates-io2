(define-module (crates-io bl ob blob) #:use-module (crates-io))

(define-public crate-blob-0.1.0 (c (n "blob") (v "0.1.0") (d (list (d (n "base64") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 2)))) (h "1jfnpib3a80z512v31l8r91na34wfrk2r15znc350nbfh5zfnd6h")))

(define-public crate-blob-0.1.1 (c (n "blob") (v "0.1.1") (d (list (d (n "base64") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.6") (d #t) (k 2)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 2)))) (h "0zhxv26wdfhx40a0dffs7dc8nc4fsdkjb03bpzx96kd5ppsjdf65")))

(define-public crate-blob-0.2.0 (c (n "blob") (v "0.2.0") (d (list (d (n "base64") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 2)))) (h "1x54dwwks7chcv1qqqnwi5dygmh5h6gdnj66a4m2v0lxjjikyb0j")))

(define-public crate-blob-0.3.0 (c (n "blob") (v "0.3.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 2)))) (h "1fzddvqiicgd1rn5bb78gsp3c6vgblwkjvdcbl6jfsyrq4dnffah")))

(define-public crate-blob-0.2.1 (c (n "blob") (v "0.2.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 2)))) (h "14lb2k5wfnnr5nbl7r9jqdyylf5nvjnyysg3pcik3d7q9yj3m00r")))

