(define-module (crates-io bl ob blob-uuid) #:use-module (crates-io))

(define-public crate-blob-uuid-0.1.0 (c (n "blob-uuid") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x96hx4xw39kbawc6m99dp3xbzbspd0k39cvr9lfmpmn150g6jl8")))

(define-public crate-blob-uuid-0.1.1 (c (n "blob-uuid") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "18cc4z0n3fs31fyxmw72rizina2qsfgxkmmignafxgmmm5qba96a")))

(define-public crate-blob-uuid-0.1.2 (c (n "blob-uuid") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1adyrfx8vzh42zq5ahan6lprs87qm37qzl2560bwgyx08ayhdl6j")))

(define-public crate-blob-uuid-0.2.0 (c (n "blob-uuid") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "0k5qqjsxl0qsz1xxr2hfn1sdfyrs5xwjzhsr1g33w9r9rmjpm5hd")))

(define-public crate-blob-uuid-0.2.1 (c (n "blob-uuid") (v "0.2.1") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "10pj1pk2hvca0mcphpdk91d5rn2z3qkcaa94a8wbq8f8vsrpfcqw")))

(define-public crate-blob-uuid-0.3.0 (c (n "blob-uuid") (v "0.3.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "06272fnsmx5d4gndi16ihqjw6230ja6087sclb5pkzs21hv9xdfm")))

(define-public crate-blob-uuid-0.3.1 (c (n "blob-uuid") (v "0.3.1") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "04vdajh8sz6h80iq2gk4ganpsy2ysczr6zdyzj2np4xmy1rngqvj")))

(define-public crate-blob-uuid-0.4.0 (c (n "blob-uuid") (v "0.4.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1m1g8z87h015zqcmr07iwq0999wql4kawh59dh8jhf0hkcj1aj5h")))

(define-public crate-blob-uuid-0.5.0 (c (n "blob-uuid") (v "0.5.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qf5fjb7agdslkd48v58a4zx7ssdzbspfkk1yn035cvi662ibz43")))

