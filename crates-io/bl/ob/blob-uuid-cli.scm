(define-module (crates-io bl ob blob-uuid-cli) #:use-module (crates-io))

(define-public crate-blob-uuid-cli-0.3.0 (c (n "blob-uuid-cli") (v "0.3.0") (d (list (d (n "blob-uuid") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1scp5nvagcj2ybs5alv17ra9ki0kcrmaqpjdghmdnrxj7c8ap9ir")))

(define-public crate-blob-uuid-cli-0.4.0 (c (n "blob-uuid-cli") (v "0.4.0") (d (list (d (n "blob-uuid") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0yii5p42d8jvlpm6njwi3cjalg2lpgmwrdd3x7cqb58a6i05c1jk")))

