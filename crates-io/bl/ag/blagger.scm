(define-module (crates-io bl ag blagger) #:use-module (crates-io))

(define-public crate-blagger-0.1.0 (c (n "blagger") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0980kfcx0x089qsv3vakyvvgnrc89zfp03kkwmcmqx5ffbq5didl")))

(define-public crate-blagger-0.1.1 (c (n "blagger") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0sg1pji9sf0j8017ijr6lk4dwllkg51k355r9iq3qq2ahmp6hvhy") (y #t)))

(define-public crate-blagger-0.1.2 (c (n "blagger") (v "0.1.2") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0k9qmx5aslhyrs4v44xg752ahgjj5rp9ll97ckq1jj98r0j0zgbj")))

(define-public crate-blagger-0.1.3 (c (n "blagger") (v "0.1.3") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0fa5ys40bxkwq8rpl9v36cazzzrgyvylx8qkqsmcy6cbwagn8c0d")))

(define-public crate-blagger-0.2.0 (c (n "blagger") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0x3bq2y2prjmak2pyzad30m9qjch1ipgq15flqwj8nryifnm88s1")))

(define-public crate-blagger-0.3.0 (c (n "blagger") (v "0.3.0") (d (list (d (n "pulldown-cmark") (r "^0.7") (d #t) (k 0)) (d (n "rss") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "019m4pfdp1cp435x1jq4lkm5lgn3hzpghrp90kfrdrprng9apkj4")))

