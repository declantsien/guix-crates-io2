(define-module (crates-io bl ct blctl) #:use-module (crates-io))

(define-public crate-blctl-0.1.0 (c (n "blctl") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.2") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0hbbnqfrl6b5jp11crksdp30gz54sqqdwh5xr1zi3rvkaixp0bgr")))

(define-public crate-blctl-0.1.1 (c (n "blctl") (v "0.1.1") (d (list (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.2") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "03ciygyl73fwzf2igwhwhw6ry1zv4rzmgdzisl3bps5n460ykdh9")))

(define-public crate-blctl-0.1.2 (c (n "blctl") (v "0.1.2") (d (list (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.2") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "1jnz9hvp9ddn2q05s39j2dba6h29xa8ixj5bx424w228dwj2z2qp")))

