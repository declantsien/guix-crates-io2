(define-module (crates-io bl ok bloke) #:use-module (crates-io))

(define-public crate-bloke-0.1.0 (c (n "bloke") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d0k3g9vs1ww9r844knl33z60mgv4cak0w79fcvi9c86i4gnldaf")))

