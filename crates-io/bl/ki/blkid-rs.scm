(define-module (crates-io bl ki blkid-rs) #:use-module (crates-io))

(define-public crate-blkid-rs-0.1.0 (c (n "blkid-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "0yq2g9fdb24qi0jj0qkflhnl5hbs0fbzzd3fzcmamfcdnji2ygzw")))

(define-public crate-blkid-rs-0.2.0 (c (n "blkid-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.6") (d #t) (k 0)))) (h "1inmxa5nilwbs8rhgf0zhn0agv50myj55gj37qxxynyb5k50ghlv")))

(define-public crate-blkid-rs-0.1.2 (c (n "blkid-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "uuid") (r "^0.7.4") (d #t) (k 0)))) (h "1l0xwyhnw8n73q04i9yi8rsfc6a5iwd337yzv84zfy4qlqbyamjh")))

(define-public crate-blkid-rs-0.3.0-beta1 (c (n "blkid-rs") (v "0.3.0-beta1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0pwr8grdhnlrc643niavz7ysx016rrij3ys96rk8d11a59zyhi1w")))

(define-public crate-blkid-rs-0.3.0 (c (n "blkid-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "uuid") (r "^1.1.2") (d #t) (k 0)))) (h "1lyr409r3qh0h4v10b8mxb384f91w9j4h14az057vnylwgrgjim2")))

