(define-module (crates-io bl ki blkid-sys) #:use-module (crates-io))

(define-public crate-blkid-sys-0.1.0 (c (n "blkid-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)))) (h "18xrr0mrmfbmly1plhf7wmjswjril7jmaaxpfvd8fpdsllrsadqp")))

(define-public crate-blkid-sys-0.1.1 (c (n "blkid-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)))) (h "0bjga4g17ykqvmp35bgvb97z9ivaj9xbg0a69f6f9kqrivgnyis5")))

(define-public crate-blkid-sys-0.1.2 (c (n "blkid-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0bvhwlfp9r8wbnqc1967gkpfysfy9gpq7imn38v9kral01dc67qx")))

(define-public crate-blkid-sys-0.1.3 (c (n "blkid-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.33.2") (d #t) (k 1)))) (h "15b9lc4qaxkicf4g6qf6ggzgm3lklikkjh4kaakbhz5fvg8bm76j")))

(define-public crate-blkid-sys-0.1.4 (c (n "blkid-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "~0.40") (d #t) (k 1)))) (h "123mrmbcj4i8f2x8h7xbf77crgb9zbmzxd13nph3q92443iz8zh8")))

(define-public crate-blkid-sys-0.1.5 (c (n "blkid-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "~0.53") (d #t) (k 1)))) (h "04gjly7siavs7c54nab4mbbm9g6gkgxyn28jyp9j83z9k4430alz")))

(define-public crate-blkid-sys-0.1.6 (c (n "blkid-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)))) (h "17j8g9y7611fdqvv02n7b5nv9cmmapmp1dj4likpsaw7pdah47nk")))

(define-public crate-blkid-sys-0.1.7 (c (n "blkid-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "~0.65") (d #t) (k 1)))) (h "0r8m27fdpna6vd3rzpgga0cszj4cl2f76zlar5pdzzzlk0j9b77w")))

