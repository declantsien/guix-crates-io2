(define-module (crates-io bl ki blkid) #:use-module (crates-io))

(define-public crate-blkid-0.1.0 (c (n "blkid") (v "0.1.0") (d (list (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dh2hh4yslgqik2bqyv7nc9pzphycjlk37cr9w3a94p1r61w2m6s")))

(define-public crate-blkid-0.1.1 (c (n "blkid") (v "0.1.1") (d (list (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01rwkvac3wd2484yr8kfcr3383147c4crmkkqpw7fd2dgmc8l9xg")))

(define-public crate-blkid-0.2.0 (c (n "blkid") (v "0.2.0") (d (list (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p19dk1ym53b0hzmvbw5byf4222sqipxbcdyljmgm6307xydr7vv")))

(define-public crate-blkid-0.2.1 (c (n "blkid") (v "0.2.1") (d (list (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17h9nyxvaql759155gg3f0bymj6a180ibngw674p1yzmcz7kxdnk")))

(define-public crate-blkid-0.3.0 (c (n "blkid") (v "0.3.0") (d (list (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vx3bxqr53xwq0apcpq6ykb3y00fran5gdcg7z2rpsppadl4x3qk")))

(define-public crate-blkid-0.4.0 (c (n "blkid") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n0hsp9r9ax7f9p1kwrp9xyg44n3l7k8b1bnyav87v7qmclivx2j") (y #t)))

(define-public crate-blkid-1.0.0 (c (n "blkid") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wnd0q67yfmykakkvbna7g6lwm95id539vlpm83indrqmav2g7kz")))

(define-public crate-blkid-1.0.1 (c (n "blkid") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "blkid-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "strum") (r "^0.23") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1drbf1vifr79yqb9zx9i5kjwfcvg64lpgrpqarxakmh6zl6l8s2g")))

