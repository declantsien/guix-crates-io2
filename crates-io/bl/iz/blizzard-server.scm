(define-module (crates-io bl iz blizzard-server) #:use-module (crates-io))

(define-public crate-blizzard-server-0.1.0 (c (n "blizzard-server") (v "0.1.0") (d (list (d (n "blizzard-engine") (r "^0.1") (d #t) (k 0)) (d (n "blizzard-engine_derive") (r "^0.1") (d #t) (k 0)) (d (n "blizzard-id") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f050c0azmjn00kl9wxw7afai1y02yn1aswjvdyqf4nxrrgdsfl0")))

