(define-module (crates-io bl s- bls-signatures-rs) #:use-module (crates-io))

(define-public crate-bls-signatures-rs-0.1.0 (c (n "bls-signatures-rs") (v "0.1.0") (d (list (d (n "bn") (r "^0.4.5") (d #t) (k 0) (p "witnet-bn")) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1s2jri1vghmxp4hljbbdqh4j6wjf080yh5hb34fx4gkwhqgmzmsj")))

