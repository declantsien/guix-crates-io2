(define-module (crates-io bl s- bls-like) #:use-module (crates-io))

(define-public crate-bls-like-0.0.1 (c (n "bls-like") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ff") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1.0") (d #t) (k 0)) (d (n "paired") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1fg31pshiymp2msjn84bj88i6zsgggzapvxifyin73pz3rbp7g60") (y #t)))

(define-public crate-bls-like-0.0.3 (c (n "bls-like") (v "0.0.3") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ff") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "merlin") (r "^1.1.0") (d #t) (k 0)) (d (n "paired") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "10sggmalrfr3wfq30fv0zl2knxl93j8nqfyfdwcyk7qckgqlb2xs") (y #t)))

(define-public crate-bls-like-0.1.0 (c (n "bls-like") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "ff") (r "^0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paired") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "sha3") (r "^0.8") (d #t) (k 0)))) (h "1idbk1r4gmq825ndp9n65sfgmprxhs93hpvk53lkr7r5qp7dr3dm") (y #t)))

