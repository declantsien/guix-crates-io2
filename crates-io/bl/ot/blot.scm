(define-module (crates-io bl ot blot) #:use-module (crates-io))

(define-public crate-blot-0.1.0 (c (n "blot") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "blot-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zzhzg1pirzxaqnn9yq18p80n6hb4yw1kirx3zlb7vya2mszlrpw")))

(define-public crate-blot-0.1.1 (c (n "blot") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "blot-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g978dgw732c10v07rzgsb2imhx9ilqapjqgdlbf99yhsrpvd8f9")))

(define-public crate-blot-0.1.2 (c (n "blot") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "blot-lib") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10k1xqvvlsym7jjsr7sf4q9wfhv02jwa8clsjddwv03wn6255acc")))

