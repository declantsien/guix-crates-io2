(define-module (crates-io bl ad blade-egui) #:use-module (crates-io))

(define-public crate-blade-egui-0.1.0 (c (n "blade-egui") (v "0.1.0") (d (list (d (n "blade-graphics") (r "^0.2") (d #t) (k 0)) (d (n "blade-macros") (r "^0.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "egui") (r "^0.22") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0wc5jjws89ml6zdbnbxb19rbc2avmp0w8briq1kfbkfv2masqrb1")))

(define-public crate-blade-egui-0.2.0 (c (n "blade-egui") (v "0.2.0") (d (list (d (n "blade-graphics") (r "^0.3") (d #t) (k 0)) (d (n "blade-macros") (r "^0.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "egui") (r "^0.23") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "077y4p2ww4qdbkg7cv4xxz6wmx20cvf9qzw0npicl70j5a14r6gw")))

(define-public crate-blade-egui-0.3.0 (c (n "blade-egui") (v "0.3.0") (d (list (d (n "blade-graphics") (r "^0.4") (d #t) (k 0)) (d (n "blade-macros") (r "^0.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "egui") (r "^0.26") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 0)))) (h "1rwjlmbkjhdvgxvzl36lc5r3s8cg9rn362055vm244g1m40x69bw")))

