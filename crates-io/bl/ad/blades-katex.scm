(define-module (crates-io bl ad blades-katex) #:use-module (crates-io))

(define-public crate-blades-katex-0.1.2 (c (n "blades-katex") (v "0.1.2") (d (list (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "blades") (r "^0.3") (k 0)) (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "katex") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.12") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13yf4ip9hndf7cnq73lihcyppsgcyh8q7wb31nx5x5nkr0mcdhpg")))

