(define-module (crates-io bl ad bladerf-bindings) #:use-module (crates-io))

(define-public crate-bladerf-bindings-0.0.1 (c (n "bladerf-bindings") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "11siig4v89v1x073kkc1hii876znjrb9cikj14bvyrxmrqmh3szi")))

(define-public crate-bladerf-bindings-0.0.2 (c (n "bladerf-bindings") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1pxqfg7z3j222bxjrjk7316j51gp2wpl89aiikv5v23rkgb0929b")))

(define-public crate-bladerf-bindings-0.0.3 (c (n "bladerf-bindings") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1zkdnyfalw6crg6py1vb4dj0hnax0a2j8jdpglrxk1as3wi992w2")))

(define-public crate-bladerf-bindings-0.0.4 (c (n "bladerf-bindings") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)))) (h "0myxw11lfvl89lvsmmk3xc18hkqcrw7z5ymwdfi3jgkh48jkhhnr")))

(define-public crate-bladerf-bindings-0.0.5 (c (n "bladerf-bindings") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)))) (h "1g0fp2h8chld00f7pvw6fnhmqysbi32ybzjqw3bmpwpfzxz1za1d")))

(define-public crate-bladerf-bindings-0.0.6 (c (n "bladerf-bindings") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)))) (h "1mivlh8mwhf55yij5v6773m18fq4935nmr35fq8w09i2wd7x0z53")))

(define-public crate-bladerf-bindings-0.0.7 (c (n "bladerf-bindings") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)))) (h "0f7dw513qc68zhhgirj85g6rkhbk1vy637yqr92afwz8bnaqybzq")))

(define-public crate-bladerf-bindings-0.0.8 (c (n "bladerf-bindings") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)))) (h "1x4vvz0xblcxhrnw0qkmjx8qks2xvrz434h25g9bhdm5qw3byb9n")))

(define-public crate-bladerf-bindings-0.0.9 (c (n "bladerf-bindings") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "10b11j0bqx6hmy6k548qnmbqqqkwhi5rihcrcinwxckc9xgvl9vv")))

(define-public crate-bladerf-bindings-0.0.10 (c (n "bladerf-bindings") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0gh516jmw4ws1i59z6fcllw576zh59n3ys12s5rg9ms3xxx0lqlp")))

(define-public crate-bladerf-bindings-0.0.11 (c (n "bladerf-bindings") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "198ji2j5q8l6rmm0cn2a2a1279zkiwa48xggvz96vpcw192yza4c")))

(define-public crate-bladerf-bindings-0.0.12 (c (n "bladerf-bindings") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "0bwcdw3fcxdl31chh7y70j0622d0z3r4wcbgxchi2nk4bgffywqy")))

(define-public crate-bladerf-bindings-0.0.13 (c (n "bladerf-bindings") (v "0.0.13") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.12.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "17iiiz66f369l2xvxy12k1ql79fazxfwn8nc0c8g8jirq11apdn9")))

