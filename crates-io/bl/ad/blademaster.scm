(define-module (crates-io bl ad blademaster) #:use-module (crates-io))

(define-public crate-blademaster-0.1.0 (c (n "blademaster") (v "0.1.0") (h "0l1dsbjzslipkz6chz2ds1852c8kkcp9psq260mcd6j4s0q4vm5y")))

(define-public crate-blademaster-0.2.0 (c (n "blademaster") (v "0.2.0") (d (list (d (n "legion") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.8") (d #t) (k 0)))) (h "1rq1kd2crkdiq8v6v916xrhgbl8yhr7cwvxksw4spl56ckpxcpks")))

(define-public crate-blademaster-0.2.1 (c (n "blademaster") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "legion") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tui") (r "^0.8") (f (quote ("crossterm"))) (k 0)))) (h "0ifx8hkaswlnqfaz0sq0vzn510i5amgddvzsflppcbb112hzii6z")))

