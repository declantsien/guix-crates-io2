(define-module (crates-io bl y- bly-dx2d) #:use-module (crates-io))

(define-public crate-bly-dx2d-0.1.0 (c (n "bly-dx2d") (v "0.1.0") (d (list (d (n "bly-ac") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)) (d (n "windows") (r "^0.38.0") (f (quote ("Foundation_Numerics" "Win32_Foundation" "Win32_Graphics_Direct2D_Common" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11" "Win32_Graphics_Dxgi_Common" "Win32_System_Performance" "Win32_System_SystemInformation" "Win32_UI_Animation" "Win32_UI_WindowsAndMessaging"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vdya29n8wr89anz8s43nhzilzmpdisdi835rppjlmy982rlq187")))

