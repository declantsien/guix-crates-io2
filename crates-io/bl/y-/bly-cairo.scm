(define-module (crates-io bl y- bly-cairo) #:use-module (crates-io))

(define-public crate-bly-cairo-0.1.0 (c (n "bly-cairo") (v "0.1.0") (d (list (d (n "bly-ac") (r "^0.1.0") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.17.0") (f (quote ("xlib"))) (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (d #t) (k 0)))) (h "12jalg9r17y1ibcz9gqal5qc3x83q750j1xax9kpxjcina5hyvfl")))

