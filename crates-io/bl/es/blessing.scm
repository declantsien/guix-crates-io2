(define-module (crates-io bl es blessing) #:use-module (crates-io))

(define-public crate-blessing-0.1.0 (c (n "blessing") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "1mm6l43am2plvd4f0flcvrxwn3v7h1h0h5dj5aj973ckxcfysn84") (y #t)))

(define-public crate-blessing-0.2.0 (c (n "blessing") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "193cs14wfq56gslvsqpgqld0n78r2b7nmmh6apy2pdhjqimdf7cd") (y #t)))

(define-public crate-blessing-0.1.1 (c (n "blessing") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "1qcj5hchhix7cyhlaym5ki9glppss3inhb4h3pki38rxdx7cr7gj")))

(define-public crate-blessing-0.1.8 (c (n "blessing") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "0a87ddj0bli5mhwlzzdsq5m0cvq2sx52lh5p5ra75jdy5csspm25")))

(define-public crate-blessing-0.1.3 (c (n "blessing") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "0hdkzz5hddiakanj01rwj2c5lvl9yjz612bw6kjcq0rnqvbi5jk4")))

