(define-module (crates-io bl ip blip_buf-rs) #:use-module (crates-io))

(define-public crate-blip_buf-rs-0.1.0 (c (n "blip_buf-rs") (v "0.1.0") (h "1qi6y17s2iv4jngviy4x7xbhy1srgs6z6a8bhvjp93mi8gx85j65")))

(define-public crate-blip_buf-rs-0.1.1 (c (n "blip_buf-rs") (v "0.1.1") (h "0mfxn393zgxypqdfdz5869yrp73zrzv6cbczaz7r3a6l6py0p0k9")))

