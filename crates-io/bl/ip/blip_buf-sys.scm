(define-module (crates-io bl ip blip_buf-sys) #:use-module (crates-io))

(define-public crate-blip_buf-sys-0.1.0 (c (n "blip_buf-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "12w8d2n2zh26iw8n7k53nsdfh994fxgb8h04jx1x3zv2amzaphh4")))

(define-public crate-blip_buf-sys-0.1.1 (c (n "blip_buf-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0nw6mcf12mynyi0m90sy9p5y3cdyysjyyn66k7lv58bwppd1236w")))

(define-public crate-blip_buf-sys-0.1.2 (c (n "blip_buf-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vf8mvmrgbfy66h3iw20vnc8y5n8986k2b0n7mjhm4cs5g98d5bs")))

(define-public crate-blip_buf-sys-0.1.3 (c (n "blip_buf-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08yg3d4s4bmcw0dyw3lpdswjdbf3nxq5743jw7vwnx98vcm9dbks")))

(define-public crate-blip_buf-sys-0.1.4 (c (n "blip_buf-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bjjsh60gzkij4haw0ma0vy2a12qq3di1jzmh9q1296dg10nb11v")))

