(define-module (crates-io bl ip blipper-shared) #:use-module (crates-io))

(define-public crate-blipper-shared-0.1.0 (c (n "blipper-shared") (v "0.1.0") (d (list (d (n "heapless") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "infrared") (r "^0.8") (f (quote ("remotes" "std"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "postcard") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serialport") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "1rmdx5ialvaj14njlgdzbqnrs3gnvrn07wxpv4yg3v2h01gg5bds") (f (quote (("utils" "serialport" "heapless" "postcard" "infrared") ("default"))))))

