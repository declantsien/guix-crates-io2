(define-module (crates-io bl ip blip_buf) #:use-module (crates-io))

(define-public crate-blip_buf-0.1.0 (c (n "blip_buf") (v "0.1.0") (d (list (d (n "blip_buf-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1s2zaaphi00cbqrs20y3n9xwjdibdmhl1jbnqraxwnqm66c14hlq")))

(define-public crate-blip_buf-0.1.1 (c (n "blip_buf") (v "0.1.1") (d (list (d (n "blip_buf-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1ibcpa7qf2kzq2hpssvz5x2mls29nh21vq2c0m08akka1065g2hh")))

(define-public crate-blip_buf-0.1.2 (c (n "blip_buf") (v "0.1.2") (d (list (d (n "blip_buf-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "177r5hsvgm5ph7n49347h0pr806mjfcw55yqglllk6jp01ki4kj4")))

(define-public crate-blip_buf-0.1.3 (c (n "blip_buf") (v "0.1.3") (d (list (d (n "blip_buf-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1q9gcvd2zg2w9wnplqiy5al6ilil8fv6bipng0vid1fz3ys2602c")))

(define-public crate-blip_buf-0.1.4 (c (n "blip_buf") (v "0.1.4") (d (list (d (n "blip_buf-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0za1v9z003jq5szgy7vllrb9l2mmx1mql00mwizw9l78a37kdha5")))

