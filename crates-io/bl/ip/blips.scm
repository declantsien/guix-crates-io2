(define-module (crates-io bl ip blips) #:use-module (crates-io))

(define-public crate-blips-0.1.0 (c (n "blips") (v "0.1.0") (d (list (d (n "graphql_client") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (k 0)) (d (n "pem") (r "^1.1") (o #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "ring") (r "^0.16") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "0va493r3xjq5abl4d1z7rzw8nazklnxsg37vy09z3ck6sb6ppax2") (f (quote (("rustls-tls" "reqwest/rustls-tls" "ring" "pem") ("native-tls" "reqwest/default-tls" "openssl") ("default" "rustls-tls"))))))

