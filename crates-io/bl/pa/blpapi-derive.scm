(define-module (crates-io bl pa blpapi-derive) #:use-module (crates-io))

(define-public crate-blpapi-derive-0.0.1 (c (n "blpapi-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "087fb9l9q2xy3njvdz83gclqr8fr88xa4vqvf653vgnlqfw1kadh")))

