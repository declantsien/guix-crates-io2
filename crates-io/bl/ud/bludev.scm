(define-module (crates-io bl ud bludev) #:use-module (crates-io))

(define-public crate-bludev-0.1.0 (c (n "bludev") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0mmwh3lj0yxnax0h567mvrgm3s5v2l5ii57ppafc77bqysmbqjjg")))

