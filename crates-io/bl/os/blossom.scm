(define-module (crates-io bl os blossom) #:use-module (crates-io))

(define-public crate-blossom-0.1.0 (c (n "blossom") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0621f1zqc5s58dn8k6vf2zp1297apph8v079qkr88l38s0ibl8xl")))

(define-public crate-blossom-0.1.1 (c (n "blossom") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1272cp4gaswhy29c0bgxn4jiwxfzsvxjwrcllqpy60nz260ifikp")))

(define-public crate-blossom-0.1.2 (c (n "blossom") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1s9qgrsdli3xwqdz5kfbx7mcbk5hrq0ha2qzrgfgcjj73gz2s3nm")))

(define-public crate-blossom-0.1.3 (c (n "blossom") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1wngl7j5g5dppcz9az2ahwj1nfp18aj4xn9a960rlp04v7q2bkzd")))

(define-public crate-blossom-0.2.0 (c (n "blossom") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1qvmywgrcav04z11jjlip7mla32570sgpcgg0a47k87hbx3i1jzq")))

(define-public crate-blossom-0.3.0 (c (n "blossom") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0kagwx99ci6vxp7awqhgdac2prrf9dl4flns9fg8rkpcl2mzahyh")))

(define-public crate-blossom-0.3.1 (c (n "blossom") (v "0.3.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1xh9hnlcdxkvmk5zcwqxspdd9skgyvak7i9yvzaagbn6srikxiq1")))

(define-public crate-blossom-0.3.2 (c (n "blossom") (v "0.3.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "04ncpc1qwn4115a2smf3bfk1qdy3hy10ahkqpdahlakjibzp8mii")))

(define-public crate-blossom-0.3.3 (c (n "blossom") (v "0.3.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "12jgwa4513b04rm6i9qwpai9zij3v7srrl5a00mfaci9d5v03975")))

(define-public crate-blossom-0.4.0 (c (n "blossom") (v "0.4.0") (d (list (d (n "criterion") (r ">=0.3.3, <0.4.0") (d #t) (k 2)) (d (n "rand_core") (r ">=0.5.1, <0.6.0") (d #t) (k 2)) (d (n "rand_xorshift") (r ">=0.2.0, <0.3.0") (d #t) (k 2)))) (h "0rdsn7dhwgl6fcw9mscaf0zs31ilvvlhfg6dn58qmslsb0xrlaix")))

