(define-module (crates-io bl os blosc-src) #:use-module (crates-io))

(define-public crate-blosc-src-0.1.0 (c (n "blosc-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0xv61k8l86jiap5qaxrwaf5wycrng0icwq7dc55yrmhdxiqs6mk7") (l "blosc")))

(define-public crate-blosc-src-0.1.1 (c (n "blosc-src") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "1x387zhypkv39w6yad70v45wchmcpwf1d5p5rxksf91gyjm74crw") (l "blosc")))

(define-public crate-blosc-src-0.2.0 (c (n "blosc-src") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.46") (d #t) (k 1)))) (h "0v4954bkzaspmybxabrcv0cmc4inf0v5ha9glihapda6biwfiaxr") (y #t) (l "blosc")))

(define-public crate-blosc-src-0.2.1 (c (n "blosc-src") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ajy6rfc4m4dd0854qsn52bhpnm44ml14ahy0yyhryb5hsf50sx7") (f (quote (("zstd") ("zlib") ("lz4")))) (l "blosc")))

(define-public crate-blosc-src-0.2.2 (c (n "blosc-src") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vaj0f47d5y5xnmi9mdbsqhp6s15n8dfbzm54n3hfzhi485gkv50") (f (quote (("zstd") ("zlib") ("lz4")))) (l "blosc")))

(define-public crate-blosc-src-0.3.0 (c (n "blosc-src") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1.12") (f (quote ("static" "libc"))) (o #t) (k 0)) (d (n "lz4-sys") (r "^1.9.4") (o #t) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.9") (o #t) (d #t) (k 0)))) (h "1bgrkgihmh5ynz3vwwz1hkkqmqzbwr6cpx588w2j5c04g8bxs53w") (l "blosc") (s 2) (e (quote (("zstd" "dep:zstd-sys") ("zlib" "dep:libz-sys") ("lz4" "dep:lz4-sys"))))))

(define-public crate-blosc-src-0.3.1 (c (n "blosc-src") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1.12") (f (quote ("static" "libc"))) (o #t) (k 0)) (d (n "lz4-sys") (r "^1.9.4") (o #t) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.9") (o #t) (d #t) (k 0)))) (h "0f8ywpp6axn0hwdnlm244r9hi5wwqc3gp85nybqphi2y4dh6wvhn") (l "blosc") (s 2) (e (quote (("zstd" "dep:zstd-sys") ("zlib" "dep:libz-sys") ("lz4" "dep:lz4-sys"))))))

(define-public crate-blosc-src-0.3.2 (c (n "blosc-src") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1.12") (f (quote ("static" "libc"))) (o #t) (k 0)) (d (n "lz4-sys") (r "^1.9.4") (o #t) (d #t) (k 0)) (d (n "zstd-sys") (r "^2.0.9") (o #t) (d #t) (k 0)))) (h "17fr4pbl98xkwndjs1nsq4ynvk4d3cwk6q1izcradlq480jks2yd") (l "blosc") (s 2) (e (quote (("zstd" "dep:zstd-sys") ("zlib" "dep:libz-sys") ("lz4" "dep:lz4-sys"))))))

