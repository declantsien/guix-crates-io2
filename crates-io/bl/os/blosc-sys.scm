(define-module (crates-io bl os blosc-sys) #:use-module (crates-io))

(define-public crate-blosc-sys-1.7.0 (c (n "blosc-sys") (v "1.7.0") (d (list (d (n "cmake") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1wmcybg5598js4sqcnyyxnxj82p9ggld4zczffx2b9kaji9a351z") (f (quote (("unstable")))) (y #t)))

(define-public crate-blosc-sys-1.7.0-rust1 (c (n "blosc-sys") (v "1.7.0-rust1") (d (list (d (n "cmake") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1j9f75v23pap04821cabj9smpin4slbn96wrqnbc56f5cgwrl67p") (f (quote (("unstable"))))))

(define-public crate-blosc-sys-1.14.2 (c (n "blosc-sys") (v "1.14.2") (h "1zrm9ssk4729871jlln5y9zsnb4lh5rbx0m87mxsf0w4zs0r6frh")))

(define-public crate-blosc-sys-1.14.4 (c (n "blosc-sys") (v "1.14.4") (h "1j0bm70gbg888xyvin7vvjfknqx12hanf6gfv1ljaf5pwcc7vd2w")))

(define-public crate-blosc-sys-1.21.0 (c (n "blosc-sys") (v "1.21.0") (h "1s9pkmag5rv8kfw4wsjzzpsgklplzp6l6wy3qmwwj0sjcb59lmkn") (l "blosc")))

