(define-module (crates-io bl os blosc2-src) #:use-module (crates-io))

(define-public crate-blosc2-src-0.1.0 (c (n "blosc2-src") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rxd0amp7wx8vxyczvpqcdkvrn2mslgnvyndgwzj9f3pkr5ljprn") (f (quote (("zstd") ("bindgen")))) (l "blosc2")))

(define-public crate-blosc2-src-0.1.1 (c (n "blosc2-src") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "17vljxy3l5r9kkxah18nf41k523dfwaa5y3wr4i0qi2710gmxh6z") (f (quote (("zstd") ("bindgen")))) (l "blosc2")))

(define-public crate-blosc2-src-0.1.2 (c (n "blosc2-src") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i3sl1mbzphicgr0p53p06rmazyam2h7pqkjvic5dqfn1i3ibxk3") (f (quote (("zstd") ("bindgen")))) (l "blosc2")))

(define-public crate-blosc2-src-0.1.3 (c (n "blosc2-src") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1c5r5r13pqg5i4d64vyzkq344kgpbx8lq1xs9xmavgcjyawk4ich") (f (quote (("zstd")))) (l "blosc2") (s 2) (e (quote (("bindgen" "dep:bindgen"))))))

