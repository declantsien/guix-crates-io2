(define-module (crates-io bl os blosc) #:use-module (crates-io))

(define-public crate-blosc-0.1.0 (c (n "blosc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0lwyh90n664iwirwq9g8gg35snhpvv0511av4gz7341r47f6i6hi")))

(define-public crate-blosc-0.1.1 (c (n "blosc") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1imwqbrpvpl1i4mbxlmypayi9c2rrcc0fi3s6q5wf8q6cbb85vwl")))

(define-public crate-blosc-0.1.2 (c (n "blosc") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.14.4") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "17rf42mhc7xgbr3skxi2d6zqsszhmgkz5m984bww2sv8qj7yjh7s")))

(define-public crate-blosc-0.1.3 (c (n "blosc") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.14.4") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "0mywn1nvjsv4hr9n0lz4xkmll1zxzv4bsn6bdcjk6ld1yqzvx92s")))

(define-public crate-blosc-0.2.0 (c (n "blosc") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.21.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1znjqvvhzc0jp3ra729r14knmf4k9p49ckg7yjj0z2w54ana2np3")))

(define-public crate-blosc-0.2.1 (c (n "blosc") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "blosc-sys") (r "^1.21.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "062dxfv0isv5ygy6fzm5wmkjsnnpmk2yirzcld162hw52xp1f2xn")))

