(define-module (crates-io bl e- ble-data-struct) #:use-module (crates-io))

(define-public crate-ble-data-struct-0.1.0 (c (n "ble-data-struct") (v "0.1.0") (d (list (d (n "uuid") (r "^1.4.1") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Foundation_Collections"))) (d #t) (k 0)))) (h "1xyxr799aj8a6rdmxjmd0zfvkwggbnhk8m9yfkcwrvcwaii0jrjq")))

(define-public crate-ble-data-struct-0.2.0 (c (n "ble-data-struct") (v "0.2.0") (d (list (d (n "uuid") (r "^1.4.1") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Foundation_Collections"))) (d #t) (k 0)))) (h "098dzg533ilf1arpxrnlaz2jmb87qk1za9wkxk2gwkhh9js4kfcx")))

(define-public crate-ble-data-struct-0.2.1 (c (n "ble-data-struct") (v "0.2.1") (d (list (d (n "uuid") (r "^1.4.1") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Devices_Bluetooth_GenericAttributeProfile" "Foundation_Collections"))) (d #t) (k 0)))) (h "0b6bs6vs04zrhj3a5a1520bx7wp6fcc0kpm2qj3mnfvycphbnpzj")))

