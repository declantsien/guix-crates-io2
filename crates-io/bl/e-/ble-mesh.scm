(define-module (crates-io bl e- ble-mesh) #:use-module (crates-io))

(define-public crate-ble-mesh-0.1.0 (c (n "ble-mesh") (v "0.1.0") (h "12qsr0z7xgca7ycyij3an3x7sy01ch7ga2f22l24abk187v168d2")))

(define-public crate-ble-mesh-0.1.1 (c (n "ble-mesh") (v "0.1.1") (h "0dx7lcpc43270h0l7qqfrasv420ghrcsd65r6zqqlls17xlxix7n")))

