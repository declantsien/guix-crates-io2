(define-module (crates-io bl e- ble-ledly) #:use-module (crates-io))

(define-public crate-ble-ledly-0.1.0-alpha.0 (c (n "ble-ledly") (v "0.1.0-alpha.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0f736cfcrhci3h3kl2a4dl8p7a172912gn664aqxbrrkg19pq3r4")))

(define-public crate-ble-ledly-0.1.0-alpha.1 (c (n "ble-ledly") (v "0.1.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0qm70ydarf66rhppycixzdf13f9lq2mljh6aizbd3zx5y2bxx8rd")))

(define-public crate-ble-ledly-0.2.0 (c (n "ble-ledly") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0wfjad61ppdmpw24zsa9xpkyx2g8zqw7n2cx3xid7cg83npbazg5") (f (quote (("sw_animate") ("light") ("hw_animate") ("default" "all") ("color") ("brightness") ("all" "light" "color" "brightness" "hw_animate" "sw_animate"))))))

(define-public crate-ble-ledly-0.3.0 (c (n "ble-ledly") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "btleplug") (r "^0.9") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0p10xbwp4j6ca5c134awa3d447mmql25i8xhh5ziqrkl8m7bz6wx") (f (quote (("sw_animate") ("light") ("hw_animate") ("default" "all") ("color") ("brightness") ("all" "light" "color" "brightness" "hw_animate" "sw_animate"))))))

