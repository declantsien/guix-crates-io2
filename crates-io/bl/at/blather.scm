(define-module (crates-io bl at blather) #:use-module (crates-io))

(define-public crate-blather-0.1.0 (c (n "blather") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0i1mv4v06b58v07yw2wkn58cz84w7fjsdi38akribg1xkc9pf8pl")))

(define-public crate-blather-0.1.1 (c (n "blather") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1d1y5dhzkx6b5iz156d68fah0z26l2ww5a7vayficwxz4g0fhfcz")))

(define-public crate-blather-0.1.3 (c (n "blather") (v "0.1.3") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0s1858vgnrsjf3qkkgag8f14zxdkcplf52yypca1jc2f2xhp6q38")))

(define-public crate-blather-0.1.4 (c (n "blather") (v "0.1.4") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1jk2q5mqwxj8533myr45svd84crysbxyns550w752i96qjnys4jz")))

(define-public crate-blather-0.2.0 (c (n "blather") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0957r2hp4qf5z9m8ib9nlpz724vlxjbpf98rjy0260znc59p1ji2")))

(define-public crate-blather-0.2.1 (c (n "blather") (v "0.2.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0622ibisyppi1bmfhjcj9kgk4ka0c2mzch402k9b114c3w0ybxaj")))

(define-public crate-blather-0.3.0 (c (n "blather") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "15rvdsrybknirnyd87js453zwi45zrw4k3bdwvxndqnmaq96mly5")))

(define-public crate-blather-0.4.0 (c (n "blather") (v "0.4.0") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0s5x4k5rbnd513pw6wdgzqjxixpybf954ihysnj5182h1dp36k6p")))

(define-public crate-blather-0.4.1 (c (n "blather") (v "0.4.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "080ar0y6ylwzfsbvar1z60c1rjfz7a823b26pjf6v5vkj487whwl")))

(define-public crate-blather-0.5.0 (c (n "blather") (v "0.5.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0779b55nsbqmvb3p3mv8hw34vdlipjcyzp92dxsd8slzg1nsr0zn")))

(define-public crate-blather-0.5.1 (c (n "blather") (v "0.5.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0p58y9cgi71sw3imjdmzl1n2irjs3fxmgqhkr4gx9a392dp16q4y")))

(define-public crate-blather-0.5.2 (c (n "blather") (v "0.5.2") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "16dvacggpd3niihp6pxmx9kvdx5w7grrav34s0fnarxyvbvq7by4")))

(define-public crate-blather-0.5.3 (c (n "blather") (v "0.5.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "0a8l7b9rx9xl8w53qbxkpmcm5jkpiyirkv0wzy9x4153m838wl2v")))

(define-public crate-blather-0.6.0 (c (n "blather") (v "0.6.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "macros" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)))) (h "081myd6qcc0vl8kqlrvd4drnvlsikbr993am94db94mwg2jlz7nr")))

(define-public crate-blather-0.7.0 (c (n "blather") (v "0.7.0") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1x4pfbmyyiy61rkmfx9p8qjs6iq879hcw5f13pyq9kcjk25jjids")))

(define-public crate-blather-0.7.1 (c (n "blather") (v "0.7.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0z6s8972y8ssflr3l47fdybxiha0iv9mzq34c9w56k04ibp9as8b")))

(define-public crate-blather-0.8.0 (c (n "blather") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "1wsja9jcxhz7g5v1d4p3j3n3pp1xbn3hyr1wb19chvm6042lfw2y")))

(define-public crate-blather-0.9.0 (c (n "blather") (v "0.9.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "net"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.8") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("codec"))) (d #t) (k 0)))) (h "0mnd3kgzys1fdjfa6ml79s0f68jbyfdkc8mkx9gkar3a47y3j5d3")))

(define-public crate-blather-0.10.0 (c (n "blather") (v "0.10.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "net"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1.14") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("codec"))) (d #t) (k 0)))) (h "08zrbh1j0v3qla7hdas4dzsbmj41jkc74d1wd42zhnn65b2hrgjw")))

