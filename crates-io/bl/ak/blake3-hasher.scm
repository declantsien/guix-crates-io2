(define-module (crates-io bl ak blake3-hasher) #:use-module (crates-io))

(define-public crate-blake3-hasher-0.1.0 (c (n "blake3-hasher") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "hash-db") (r "^0.15.2") (k 0)) (d (n "hash256-std-hasher") (r "^0.15.2") (d #t) (k 0)))) (h "021nl7bsqygcmjw2ap7pfdvwwf6yj6g1mp5n0fflii4cn3diphrk")))

