(define-module (crates-io bl ak blake2b-pow) #:use-module (crates-io))

(define-public crate-blake2b-pow-0.1.0 (c (n "blake2b-pow") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)))) (h "11h41xpwafm5a8iqs0nnfhwz2d2093j4xgx3c0vbv39assvhhy5i")))

(define-public crate-blake2b-pow-0.1.1 (c (n "blake2b-pow") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)))) (h "0xln7h0nrmw5bwwkmmv64cqcky96vmll78lny0q5linpsg23m8yi")))

