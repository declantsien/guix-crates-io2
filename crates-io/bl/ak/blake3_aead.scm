(define-module (crates-io bl ak blake3_aead) #:use-module (crates-io))

(define-public crate-blake3_aead-0.1.0 (c (n "blake3_aead") (v "0.1.0") (d (list (d (n "blake3") (r "^1.4.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "03hphi8w81c1lm1iaqpzbc3k7cj5khagab630kxgnr4ym9gc53l0") (f (quote (("std" "blake3/std") ("default" "std"))))))

