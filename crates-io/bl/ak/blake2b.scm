(define-module (crates-io bl ak blake2b) #:use-module (crates-io))

(define-public crate-blake2b-0.1.0 (c (n "blake2b") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 2)))) (h "042gg64kj2aaccy6zkmzgqbmibwz6qbc9wyv7rd8vrkfxmh6ykqp") (y #t)))

(define-public crate-blake2b-0.2.0 (c (n "blake2b") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 2)))) (h "0hkxpl32hqr345cnmj38xcg66cigs6fwwzc0wvdi5zvypbv4qj3l") (y #t)))

(define-public crate-blake2b-0.3.0 (c (n "blake2b") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 2)))) (h "0niandn21lyba0p3dycis49s7n1m0fvmmasx48fykkwybp2y06m5") (y #t)))

(define-public crate-blake2b-0.3.1 (c (n "blake2b") (v "0.3.1") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 2)))) (h "0j43jc8dkxh2whf36avra3az0lrz9vn8m8xspyd4fmlb58m0bmzn") (y #t)))

(define-public crate-blake2b-0.3.2 (c (n "blake2b") (v "0.3.2") (d (list (d (n "blake2-rfc") (r "^0.2.17") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 2)))) (h "0ih4bc6iqvlf7w3bwrzdqzfms22vj3icg5y2k7sp8z9xq02080cg") (y #t)))

(define-public crate-blake2b-0.4.0 (c (n "blake2b") (v "0.4.0") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "02wy16ff86niada4q7zknlbabrmrb6zgwkk2ychqi8gk68w3a6p3") (y #t)))

(define-public crate-blake2b-0.5.0 (c (n "blake2b") (v "0.5.0") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1xkzqv89np33im3dzfjgvhm37ahkyjf8ny442xwhs9jkzj62iq38") (y #t)))

(define-public crate-blake2b-0.5.1 (c (n "blake2b") (v "0.5.1") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0305w9hfiv7whxnq2a23f14b0d2j7pbna9ydmqy1png2nh34dswp") (y #t)))

(define-public crate-blake2b-0.5.2 (c (n "blake2b") (v "0.5.2") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1n3yrspn70nxmk7bxl2jz1avlqwd2p601jcf9vag1wqfgjpws4gz") (y #t)))

(define-public crate-blake2b-0.6.0 (c (n "blake2b") (v "0.6.0") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "01hhlzqqc4cz7bqjm49xymaysg747qi4had5r2j1npid1q3qsprx") (y #t)))

(define-public crate-blake2b-0.6.1 (c (n "blake2b") (v "0.6.1") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1q8ig9h1xfdqnqibh32q2bzfjhk9cmrgl5x513z6i4zqax91fsja") (y #t)))

(define-public crate-blake2b-0.7.0 (c (n "blake2b") (v "0.7.0") (d (list (d (n "blake2-rfc") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0jz04ar5n5l5a3900svica7kgn4k3yn4nlv88rzj7iwpqbgn0n95") (f (quote (("i128")))) (y #t)))

(define-public crate-blake2b-0.99.0 (c (n "blake2b") (v "0.99.0") (h "03g95s5m76i9fqd18x51wvvy9v90jbnpzmpbya45a7hnkp399kl1")))

