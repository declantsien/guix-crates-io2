(define-module (crates-io bl ak blake2-rfc) #:use-module (crates-io))

(define-public crate-blake2-rfc-0.1.0 (c (n "blake2-rfc") (v "0.1.0") (d (list (d (n "constant_time_eq") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0k6i99kjawjbgak3fnnlzpsjzgxkz882i4jgr62mx0zl1hc17qm4")))

(define-public crate-blake2-rfc-0.2.0 (c (n "blake2-rfc") (v "0.2.0") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1vn5j7vy1m5ncdhgvj9adxrq0ix8xn0lrb0yryz92n80v345cxa7")))

(define-public crate-blake2-rfc-0.2.1 (c (n "blake2-rfc") (v "0.2.1") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "07fjbqinhcb0kx5jjpianqxpzh1l6wblvm2ap5fc390fhk5rm18p")))

(define-public crate-blake2-rfc-0.2.2 (c (n "blake2-rfc") (v "0.2.2") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "056ca37nbbl8wffk7dhn0krjb7xs6c4p4dkv6f4128brjhj5xp58") (f (quote (("bench"))))))

(define-public crate-blake2-rfc-0.2.3 (c (n "blake2-rfc") (v "0.2.3") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1k4rp1yjkxcyi04nhxylsbv7pp553dmxclvbzjx58d6z41x06wnj") (f (quote (("bench"))))))

(define-public crate-blake2-rfc-0.2.4 (c (n "blake2-rfc") (v "0.2.4") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "0kvgfprwzjq7gw7w40fddgvr80qyzbbbdmn0jmkfyka1sipay98a") (f (quote (("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.5 (c (n "blake2-rfc") (v "0.2.5") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "028k7ckl8q4q28v498scvrf0j85kw4rpk4xg81kjx79cmq8gwq55") (f (quote (("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.6 (c (n "blake2-rfc") (v "0.2.6") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1sab5ghfqlj14j98lgcc8acwvyxwhb1xf7zf5c2dzb78sc3k18b3") (f (quote (("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.7 (c (n "blake2-rfc") (v "0.2.7") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "1k0hdc4jd7n4nxg6qjmih20d942v41pmgavfpfxqy26hk3w7qn2b") (f (quote (("simd_asm" "simd") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.8 (c (n "blake2-rfc") (v "0.2.8") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)) (d (n "simdty") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "181dpx5amx3yimdlfdr9f27vgk0aqnyp6fxff4nip0irflcc78mm") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd" "simdty") ("bench"))))))

(define-public crate-blake2-rfc-0.2.9 (c (n "blake2-rfc") (v "0.2.9") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)) (d (n "simdty") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "1q13x2mj4j47mg8daxa70n3zmd3izdxkf1h8jcj29n2vrnqszfx3") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd" "simdty") ("bench"))))))

(define-public crate-blake2-rfc-0.2.10 (c (n "blake2-rfc") (v "0.2.10") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)) (d (n "simdty") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "0r2skhmnmas5293w4k0jz2wz96ddw3inbzpzz10700r5sdcwkdx1") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd" "simdty") ("bench"))))))

(define-public crate-blake2-rfc-0.2.11 (c (n "blake2-rfc") (v "0.2.11") (d (list (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 2)))) (h "175zp0priznzkymnhdn0c35nbkmzncmclz765i564xbykvm0jgzx") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.12 (c (n "blake2-rfc") (v "0.2.12") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "1sr97g95zrc2rp2r35a7xxrlwvf726nggdi3ga62zq2nbpmqz1f1") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.13 (c (n "blake2-rfc") (v "0.2.13") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "1wz7d0p5v8n9g6iw1hx5mybk95i14ql34wh61zn1kwwg52pp6qp1") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.14 (c (n "blake2-rfc") (v "0.2.14") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "0hx3cbhbmhy68vi6qbd4wa31dmwzfmh10c7nrfqpyha0a56pyhwf") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.15 (c (n "blake2-rfc") (v "0.2.15") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "1gpq0bgg6iryg15rh1kkfl6mg51zy9y58p7216jnm0va0vxf2qxa") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench")))) (y #t)))

(define-public crate-blake2-rfc-0.2.16 (c (n "blake2-rfc") (v "0.2.16") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "07qrjmxphl5b6765hwid1ry07xq1i98y7ha590fj5l47v7ykqm9q") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.17 (c (n "blake2-rfc") (v "0.2.17") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^1.1.0") (d #t) (k 2)))) (h "0n9dbvaakwhz8kw2d8alfxi0ja42746rvy3124pl1wzy69plfshc") (f (quote (("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("bench"))))))

(define-public crate-blake2-rfc-0.2.18 (c (n "blake2-rfc") (v "0.2.18") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0") (d #t) (k 2)))) (h "0034g47hyq2bzmk40895ill1mbnpmmjakdq3dmm9clidvl5m6vax") (f (quote (("std") ("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("default" "std") ("bench"))))))

