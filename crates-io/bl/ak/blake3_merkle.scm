(define-module (crates-io bl ak blake3_merkle) #:use-module (crates-io))

(define-public crate-blake3_merkle-0.0.2 (c (n "blake3_merkle") (v "0.0.2") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0yfcpw3p0f7bhfnz96pnagf4hdmpv5l3kqm86vcq2a9q5mjxr5c9")))

(define-public crate-blake3_merkle-0.0.3 (c (n "blake3_merkle") (v "0.0.3") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xfc3i7h6yhz843qb9wbcmkbbyiyajpkqjzkqljl5pkfyliyc1hw")))

(define-public crate-blake3_merkle-0.0.4 (c (n "blake3_merkle") (v "0.0.4") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vsyr7kjc1nmmc8k0mc23fnbp8ypnawyf3hsi7apmgzib9mqw4m2")))

(define-public crate-blake3_merkle-0.0.5 (c (n "blake3_merkle") (v "0.0.5") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0b2pyldcra1xka7619gijd6x03nsdxrm3j1k91shd0synf0912pz")))

(define-public crate-blake3_merkle-0.0.6 (c (n "blake3_merkle") (v "0.0.6") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16nv95v4vc2yr1wrfy3bwckzyshqq5gmcr9w99f4ax0xzjiaa8p0")))

