(define-module (crates-io bl ak blake2b_simd) #:use-module (crates-io))

(define-public crate-blake2b_simd-0.1.0 (c (n "blake2b_simd") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1a3fxm6c85hj3rlini70fw2zcbgx8d2h41h916jad3acx4i3ca1i") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.2.0 (c (n "blake2b_simd") (v "0.2.0") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1hwgyl87jhxw7f9panz9xvpbxc8fgn80j78vmg8ajhkp7sklbcsh") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.2.1 (c (n "blake2b_simd") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1xblz527fxgv65dsk1k5frk7c3prxi5h1i08ak5yaryrf2lwfq1w") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.2.2 (c (n "blake2b_simd") (v "0.2.2") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0lsy95830b4z2xg0i376r0w3zc1xwp7vciw932yl8k4db6z173gb") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.2.3 (c (n "blake2b_simd") (v "0.2.3") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1yw7p6am6sdvs4sv83bh9az194iqkmxgwcjxx2y4xd7xra9sbsdk") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.2.4 (c (n "blake2b_simd") (v "0.2.4") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0i056njkm6vc9jl2ysf7x67xzbq9zy1c28j8p20bmqggijwbwh0p") (f (quote (("std") ("default" "std") ("blake2bp" "std" "rayon"))))))

(define-public crate-blake2b_simd-0.2.5 (c (n "blake2b_simd") (v "0.2.5") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1j00i8gw0pj3ipz9vhhsqwwwvyhc15zbb5b41rhymmy2a2r9n1p1") (f (quote (("std") ("default" "std") ("blake2bp" "std" "rayon"))))))

(define-public crate-blake2b_simd-0.3.0 (c (n "blake2b_simd") (v "0.3.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "02scdjgwxk6hp89mv28pz050pm90gkxqk99vl2xf5fl5sdvmzqcm") (f (quote (("std") ("default" "std") ("blake2bp" "std" "rayon"))))))

(define-public crate-blake2b_simd-0.3.1 (c (n "blake2b_simd") (v "0.3.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0s1k7nwr3b614mv0ybnm4kdg9n3iv7q2f0q6rfgi1h60kz1w9wam") (f (quote (("std") ("default" "std") ("blake2bp" "std" "rayon"))))))

(define-public crate-blake2b_simd-0.4.0 (c (n "blake2b_simd") (v "0.4.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 2)))) (h "1ai2g5qkn72x9gz3mhkdazmhfdms77ain5dw29s60s84zgicz9ks") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.4.1 (c (n "blake2b_simd") (v "0.4.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 2)))) (h "0vh59prlcm63h9d8wj9waryaaaycq6achz4plbd70ik3rnk729ff") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.0 (c (n "blake2b_simd") (v "0.5.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.7") (f (quote ("use_union"))) (k 0)) (d (n "byteorder") (r "^1.2.4") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.30") (d #t) (k 2)))) (h "1dvm62ds00mimga2b09iw0r4a5cp1sn9h9lmyy6y65nb5c24whjb") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.1 (c (n "blake2b_simd") (v "0.5.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0byv8scsmfyygrm0c109rfsk6wj0r65q4f4nvvkmg3ljappzj2fr") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.2 (c (n "blake2b_simd") (v "0.5.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "10p170cg98sv7a2w7v66ajnalny5g5wf7lc9spdh1rw8f46kbxzy") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.3 (c (n "blake2b_simd") (v "0.5.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1p0lw5hp290ldp4sfij6yw710frq0fc91pr28w4fap75a50bi9bq") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.4 (c (n "blake2b_simd") (v "0.5.4") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0a32349pravdljbgapq5nlfpr41a7bb8icavgds1gxjh6g368fwg") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.5 (c (n "blake2b_simd") (v "0.5.5") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1m89vfjwp14j5xvwpbw6n0gnnnxl7dr6c3y47zx1hcf19xj94y32") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.6 (c (n "blake2b_simd") (v "0.5.6") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1cnnprc45vv4lbz2anfjm4g5zwazka57il7pxcfhrdwfka3ln7s6") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.7 (c (n "blake2b_simd") (v "0.5.7") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1684dlyqv3ifz3q75g0xs51qmc672q1w42hpzwh4wiidpf0mlxxz") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.8 (c (n "blake2b_simd") (v "0.5.8") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "10iiyv22rknspcrkpn49ka3widw29gv4q0ah0bfrbx2j2ppawl2q") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.9 (c (n "blake2b_simd") (v "0.5.9") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1h6hhzgvvmp9jb4pvb48h5j0w5vb1n02ahi0g26p2wg6n6m7nfxq") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.10 (c (n "blake2b_simd") (v "0.5.10") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "12icvk8ixlivv3jv5nyrg01sajp4s279zb1kmif0nfja4ms2vyyq") (f (quote (("uninline_portable") ("std") ("default" "std"))))))

(define-public crate-blake2b_simd-0.5.11 (c (n "blake2b_simd") (v "0.5.11") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "11y5nm06lpypz65dbxgncs12ckx24i5i4a777ckfhfxd93ili9xg") (f (quote (("uninline_portable") ("std") ("default" "std"))))))

(define-public crate-blake2b_simd-1.0.0 (c (n "blake2b_simd") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "09v1qymsplpivdxsgd0p8rxlkdc0cjsq70y3s4vggy67mzj6x4vj") (f (quote (("uninline_portable") ("std") ("default" "std"))))))

(define-public crate-blake2b_simd-1.0.1 (c (n "blake2b_simd") (v "1.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.2.4") (d #t) (k 0)))) (h "1g04mc4gf6jyymyj41749jhhplm3ymnc6z7rhkc1fqwclv4hsbrw") (f (quote (("uninline_portable") ("std") ("default" "std"))))))

(define-public crate-blake2b_simd-1.0.2 (c (n "blake2b_simd") (v "1.0.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.3.0") (d #t) (k 0)))) (h "102pfciq6g59hf47gv6kix42cgpqw8pjyf9hx0r3jyb94b9mla13") (f (quote (("uninline_portable") ("std") ("default" "std"))))))

