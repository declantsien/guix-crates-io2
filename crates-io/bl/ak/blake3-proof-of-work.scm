(define-module (crates-io bl ak blake3-proof-of-work) #:use-module (crates-io))

(define-public crate-blake3-proof-of-work-0.1.0 (c (n "blake3-proof-of-work") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0jnv86dk35qzqv872y76a45pvm3jbgm0a0kn7hc1mdrib7q2rbab")))

(define-public crate-blake3-proof-of-work-0.2.0 (c (n "blake3-proof-of-work") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0f4nb085ql4ihl5k9nrc7ryivhix3jznkvi14m62awshg3jykh3d")))

(define-public crate-blake3-proof-of-work-0.2.1 (c (n "blake3-proof-of-work") (v "0.2.1") (d (list (d (n "blake3") (r "^1.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1yr7zymg05vkbaixfpw2zk29id6brn674lplxhz9rnpmmlrimd00")))

