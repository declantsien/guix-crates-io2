(define-module (crates-io bl ak blake2s-cli) #:use-module (crates-io))

(define-public crate-blake2s-cli-0.1.0 (c (n "blake2s-cli") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8.1") (d #t) (k 0)))) (h "1dvydh1z16qzgwiy8j5pf6v7shjqw1rn9scz1zaz690lm2nkl60w")))

