(define-module (crates-io bl ak blake2s_simd) #:use-module (crates-io))

(define-public crate-blake2s_simd-0.5.1 (c (n "blake2s_simd") (v "0.5.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0lla6nb367j2gs6cfv66shcc1r0kmsxypda4ajhd1rpiz47nc87s") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.2 (c (n "blake2s_simd") (v "0.5.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "062gy95bjjn6vr6d5h7ly844xivnycwl0znxfy92fndz83ril5zb") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.3 (c (n "blake2s_simd") (v "0.5.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "09f29mm528419v6h20afp609dad3dgwrj50y1i5989xysqvcjhlc") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.4 (c (n "blake2s_simd") (v "0.5.4") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0wnr6zrz9g2jw4x271symzw741nm0cd5wp1kfg5dxsggmxrpkd9m") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.5 (c (n "blake2s_simd") (v "0.5.5") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "188iw6vllcrkbf8fg806l4l20fpzxj1pagq84l7bizkivjnzh4cr") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.6 (c (n "blake2s_simd") (v "0.5.6") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0ajcfjmh88fz429k3g3ax2qwxm9ksl7sjcbk6mykd98q9dhx511s") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.7 (c (n "blake2s_simd") (v "0.5.7") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "00yndl5m9p064p5ivk8741z286z2hqm2lmayfj4ry4wrvm6fjlwp") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.8 (c (n "blake2s_simd") (v "0.5.8") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.4.10") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1sphhq9zjjccxdzyxvks2nhgxc0jmivyl1g036zdd5y82g7a17cp") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.9 (c (n "blake2s_simd") (v "0.5.9") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1ycwnfc9firr3mgjg55g2a37fvkrmf24y865sj40j8nvbdxgs3h5") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.10 (c (n "blake2s_simd") (v "0.5.10") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "0i6g2zbcqf8ix3xcld8xp36xm1gmnjnn8z7b9ii7k4l25cshg7mb") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-0.5.11 (c (n "blake2s_simd") (v "0.5.11") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.5.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "18p63b19zhs348jcq23cn092kcrhcwpfwmyb98d22nz86iq1lily") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-1.0.0 (c (n "blake2s_simd") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1m1hhzciq9fylbxgkq3x5hisb3pdglnzk7ndy4hkc07nnp19qlyv") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-1.0.1 (c (n "blake2s_simd") (v "1.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.2.4") (d #t) (k 0)))) (h "0kylvy5xwfjxjv0x7y5izzmg7qfyza2sisdsvkdzl7g6p54g8dv6") (f (quote (("std") ("default" "std"))))))

(define-public crate-blake2s_simd-1.0.2 (c (n "blake2s_simd") (v "1.0.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.0") (k 0)) (d (n "constant_time_eq") (r "^0.3.0") (d #t) (k 0)))) (h "1bjnywrhn17ndnzzp40z6cjif3m2fxfyl4yz4c6r5fcmwchh88wl") (f (quote (("std") ("default" "std"))))))

