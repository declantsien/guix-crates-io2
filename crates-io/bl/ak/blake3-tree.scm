(define-module (crates-io bl ak blake3-tree) #:use-module (crates-io))

(define-public crate-blake3-tree-0.1.0 (c (n "blake3-tree") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fleek-blake3") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "127vx0g41dscjfxl15lzk67az673lz1hqi013da38ayhsadqasnx") (f (quote (("all-tests"))))))

