(define-module (crates-io bl ak blake3l) #:use-module (crates-io))

(define-public crate-blake3l-0.0.0 (c (n "blake3l") (v "0.0.0") (h "0z938s9f8p2rjxb6a10yv6c5hcvhhzkbpnm8vj6ki69hkrp1syc6")))

(define-public crate-blake3l-0.0.1 (c (n "blake3l") (v "0.0.1") (h "0f2gqiwm5slfn4mv6n8r8q1mhcc45sid9g3qykl9lmkhxv7jf5xz")))

