(define-module (crates-io bl ak blake2b-ref) #:use-module (crates-io))

(define-public crate-blake2b-ref-0.1.0 (c (n "blake2b-ref") (v "0.1.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05r1k4zmqnqb0kiv2jp9xqki7p95207b7fkhwipfv5rh3svvgxnr")))

(define-public crate-blake2b-ref-0.2.0 (c (n "blake2b-ref") (v "0.2.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x7sllxd6l27r5827lcdvxh6yry3h2h7lkqfxawgak1c750flmiw")))

(define-public crate-blake2b-ref-0.2.1 (c (n "blake2b-ref") (v "0.2.1") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0yryxkxk1g8ssq1z4yw1m42rfr8hsln3y6zbljc60xcqqyc6k4cm")))

(define-public crate-blake2b-ref-0.3.0 (c (n "blake2b-ref") (v "0.3.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1kdv4riijlyii6xwspzb01q6nsiplfddm9nh7v0xjjk3lwk62xc8")))

(define-public crate-blake2b-ref-0.3.1 (c (n "blake2b-ref") (v "0.3.1") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16ik3r6bpx34f6wvpplwfy1j0ryhd0ii3aiwfsnrz98b5v3ifk99")))

