(define-module (crates-io bl ak blake2-rfc_bellman_edition) #:use-module (crates-io))

(define-public crate-blake2-rfc_bellman_edition-0.0.1 (c (n "blake2-rfc_bellman_edition") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.4.0") (k 0)) (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "constant_time_eq") (r "^0.1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0") (d #t) (k 2)))) (h "05a9syacw463n3shmhhvhak30s4bbjz1vscq7fqksz3c51807ipx") (f (quote (("std") ("simd_opt" "simd") ("simd_asm" "simd_opt") ("simd") ("default" "std"))))))

