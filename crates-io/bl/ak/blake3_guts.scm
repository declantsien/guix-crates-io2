(define-module (crates-io bl ak blake3_guts) #:use-module (crates-io))

(define-public crate-blake3_guts-0.0.0 (c (n "blake3_guts") (v "0.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1rrs18vskmr4zinqam90zy5hpqvkbn0fpy2kr14zg4skrl48786x") (f (quote (("std") ("default" "std"))))))

