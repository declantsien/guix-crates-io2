(define-module (crates-io bl ak blake2_c) #:use-module (crates-io))

(define-public crate-blake2_c-0.1.0 (c (n "blake2_c") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4.5") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1s0sg9cz7aycsdg74g0ij5130c9fy305l0pp4826i88dh669vw7i") (f (quote (("native"))))))

(define-public crate-blake2_c-0.1.1 (c (n "blake2_c") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.5") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1plzc2ll9d42jmxsg2ymha0jxbw2malrxw3assqmagvlyqma30lk") (f (quote (("native"))))))

(define-public crate-blake2_c-0.1.2 (c (n "blake2_c") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4.5") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)))) (h "1nf8imn3hdbnx3r1gx13i1jqhcxsnl0yf6rmrmbyds9sb6zc68rv") (f (quote (("native"))))))

(define-public crate-blake2_c-0.2.0 (c (n "blake2_c") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "0fw7iv0cdkcvlv5zzhy9jj2l6nzvcvqzb0cvjkhxyrca06q2vis5") (f (quote (("std") ("native") ("default" "std"))))))

(define-public crate-blake2_c-0.3.0 (c (n "blake2_c") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "1576wmbslljvj6m311yxnvfszzy0bxqf35df3majkci4ljddaxqk") (f (quote (("std") ("native") ("default" "std"))))))

(define-public crate-blake2_c-0.3.1 (c (n "blake2_c") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "0z1hj8ask6xrdc1m17vl09cxf4fi44djr3saxrvsmk0fix2n624f") (f (quote (("std") ("native") ("default" "std"))))))

(define-public crate-blake2_c-0.3.2 (c (n "blake2_c") (v "0.3.2") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "1wp4rhsk0j5nzldjc39brszxy44cd88r0r995bs9xmclckrps8wv") (f (quote (("std") ("native") ("default" "std"))))))

(define-public crate-blake2_c-0.3.3 (c (n "blake2_c") (v "0.3.3") (d (list (d (n "arrayvec") (r "^0.4.6") (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "constant_time_eq") (r "^0.1.3") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "03q0278i1zfcly12vqhl8rvsy4z6zzkfgjzf4jdgwfq5l39vwjm4") (f (quote (("std") ("native") ("default" "std"))))))

