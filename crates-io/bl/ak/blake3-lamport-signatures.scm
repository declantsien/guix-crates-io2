(define-module (crates-io bl ak blake3-lamport-signatures) #:use-module (crates-io))

(define-public crate-blake3-lamport-signatures-0.1.0 (c (n "blake3-lamport-signatures") (v "0.1.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "07q6sa8i4wfzdy4991jlnqc40sxwjpr9c3mza4xag47pgvx163y1")))

(define-public crate-blake3-lamport-signatures-0.2.0 (c (n "blake3-lamport-signatures") (v "0.2.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1vkyh1kprsw18bfl4jfb36ylj20869ryknf639f1qd6mgq663mk3")))

(define-public crate-blake3-lamport-signatures-0.3.0 (c (n "blake3-lamport-signatures") (v "0.3.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0d2pzixirxazh9i4d4aq4imlpil1l7lc80z48cbsygj18xwlj2pq")))

(define-public crate-blake3-lamport-signatures-0.3.1 (c (n "blake3-lamport-signatures") (v "0.3.1") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "10y3x8lf3xzjpjkaq6ca3sirjnpiz3z402i4688ay3zxfd60djhc")))

(define-public crate-blake3-lamport-signatures-0.3.2 (c (n "blake3-lamport-signatures") (v "0.3.2") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0j8z2awgaz44jmi1mj9r0nwrqqwjs5d8pxp17yrsmp4v7zqwfrss")))

(define-public crate-blake3-lamport-signatures-0.3.3 (c (n "blake3-lamport-signatures") (v "0.3.3") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xlv65q3jw4klmwnwydjk9248dzzi986xyl7rakqigfxzqzca0bj")))

(define-public crate-blake3-lamport-signatures-0.3.4 (c (n "blake3-lamport-signatures") (v "0.3.4") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0rdbffi8b5cm3gcbsx6j9gdn8idcpgma55319wlg9dr5kych48v7")))

(define-public crate-blake3-lamport-signatures-0.3.5 (c (n "blake3-lamport-signatures") (v "0.3.5") (d (list (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0w35isk6mqgil286ic0acp030lw5f5mphyar4pc8plk1q43fkj8g")))

