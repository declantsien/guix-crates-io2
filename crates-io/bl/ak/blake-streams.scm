(define-module (crates-io bl ak blake-streams) #:use-module (crates-io))

(define-public crate-blake-streams-0.1.0 (c (n "blake-streams") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "async-global-executor") (r "^2.0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "blake-streams-core") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "ipfs-embed") (r "^0.22.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.18") (d #t) (k 2)) (d (n "zerocopy") (r "^0.5.0") (d #t) (k 0)))) (h "1yfk0f4d7pa40r1alg8bjyrksxn84w6y4na7kq69k6pnaddyphid")))

