(define-module (crates-io bl ak blake-hash) #:use-module (crates-io))

(define-public crate-blake-hash-0.2.0 (c (n "blake-hash") (v "0.2.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)))) (h "0shbp5cd6gmdw8ppmmc4cjsayrj85yi14v1by2gr4fnfvdc8a8ad")))

(define-public crate-blake-hash-0.3.1 (c (n "blake-hash") (v "0.3.1") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "crypto-simd") (r "^0.1") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "packed_simd_crate") (r "^0.3.2") (o #t) (d #t) (k 0) (p "packed_simd")) (d (n "ppv-null") (r "^0.1") (d #t) (k 0)) (d (n "simd") (r "^0.1.0") (o #t) (d #t) (k 0) (p "ppv-lite86")))) (h "1nfa3y1g7j72dw09q8kjw7vd4gz9skvizgzjvrw95syzk6bccja4") (f (quote (("std") ("packed_simd" "packed_simd_crate" "crypto-simd/packed_simd") ("default" "simd" "std"))))))

(define-public crate-blake-hash-0.3.2 (c (n "blake-hash") (v "0.3.2") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "simd") (r "^0.2.6") (o #t) (d #t) (k 0) (p "ppv-lite86")))) (h "00cl0qvv7v11v4mkcaqqdaybnghf6dp82c12yawzaxgnbpvjympn") (f (quote (("std") ("default" "simd" "std"))))))

(define-public crate-blake-hash-0.4.0 (c (n "blake-hash") (v "0.4.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "simd") (r "^0.2.6") (o #t) (d #t) (k 0) (p "ppv-lite86")))) (h "1l7zvykhjhawrr339kqmyrqrd6wvhx19g9vm05l36cijpcjqfb9i") (f (quote (("std") ("default" "simd" "std"))))))

(define-public crate-blake-hash-0.4.1 (c (n "blake-hash") (v "0.4.1") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "simd") (r "^0.2.16") (o #t) (d #t) (k 0) (p "ppv-lite86")))) (h "1by63502xqmadraz3wb5f7b2v2jfawnk43ll30agd1y8320rilcl") (f (quote (("std") ("default" "simd" "std"))))))

