(define-module (crates-io bl ak blake2b256-balloon) #:use-module (crates-io))

(define-public crate-blake2b256-balloon-0.1.0 (c (n "blake2b256-balloon") (v "0.1.0") (d (list (d (n "balloons") (r "~0.1") (d #t) (k 0) (p "balloons")) (d (n "blake2b") (r "~0.1") (d #t) (k 0) (p "blake2b-rs")) (d (n "slices") (r "~0.1") (d #t) (k 2)))) (h "1l1z49iwj1wharsh3b70radzxakxdhhk26xm7kmj32wva5wzmz4k")))

