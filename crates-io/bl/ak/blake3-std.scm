(define-module (crates-io bl ak blake3-std) #:use-module (crates-io))

(define-public crate-blake3-std-0.0.1 (c (n "blake3-std") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "blake2") (r "^0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md5") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "0cj5nyff064d6whcjak1qnspyj911x0m9hidvm2kvksblkimy8av")))

(define-public crate-blake3-std-0.0.2 (c (n "blake3-std") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "blake2") (r "^0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md5") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "1q1qc11yz58waw0ri191gg7kixhmjgy2dbdsa6c0m1xra5nw72zh")))

(define-public crate-blake3-std-0.1.0 (c (n "blake3-std") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "blake2") (r "^0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "md5") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "177c5b3npp8z4brc4sgf3hj0b6h2w6jh0vycs22r0xazy7f95h8l")))

