(define-module (crates-io bl ak blake) #:use-module (crates-io))

(define-public crate-blake-0.1.0 (c (n "blake") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10639bqkpwkg4yb9khnvkgc0mpjlixkbmxvsmzpqnzjrfyg6kqxz")))

(define-public crate-blake-1.0.0 (c (n "blake") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01zapr9midvv3n8dhswrwjcbzcwhwc2iyw7mm3cc52f01plj8g56")))

(define-public crate-blake-1.0.1 (c (n "blake") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mp3xnv9dgjm575kkldbxiixmh05a8xbizzy3k8zypbn5zwhrg5p")))

(define-public crate-blake-1.0.2 (c (n "blake") (v "1.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a7nzv074fq5cv5rlj6qircm518837l2lqhv26dn5df8qx8xikpr")))

(define-public crate-blake-1.1.0 (c (n "blake") (v "1.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x4cwwqqgq4aq886kkn1vhmisjkvbxz05kmcvs3fzc0zk95lkand")))

(define-public crate-blake-1.1.1 (c (n "blake") (v "1.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19kpxl72lcs22f07893w7vjm69sm5gf23xhi0vwgrxxn6jl5zngf")))

(define-public crate-blake-2.0.0 (c (n "blake") (v "2.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10qbwz7ggfsmww02am7xhd14wld1pirx5rgxzi6n9gzr0n7mw310")))

(define-public crate-blake-2.0.1 (c (n "blake") (v "2.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10da2p8gs5s7w9nhdnj4lr40cic8z5mm97i53lgx0d9rb0izyv7p")))

(define-public crate-blake-2.0.2 (c (n "blake") (v "2.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "036dar1lc98ml1639wccqdnzij5i22dv5ksnhdyja7xy6g5fjmgf")))

