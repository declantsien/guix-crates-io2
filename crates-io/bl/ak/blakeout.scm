(define-module (crates-io bl ak blakeout) #:use-module (crates-io))

(define-public crate-blakeout-0.1.0 (c (n "blakeout") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0vzlqg2vsl5w1ryi4chwjskl35q37jyc0gwqcpvkc1ifdqb6mc4y")))

(define-public crate-blakeout-0.2.0 (c (n "blakeout") (v "0.2.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)))) (h "10240bkx6blczhah68mp5wicgmxld2kf50a07qcsj9vcjq8kvh88")))

(define-public crate-blakeout-0.3.0 (c (n "blakeout") (v "0.3.0") (d (list (d (n "blake2") (r "^0.9.1") (d #t) (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)))) (h "0dxcg3sjxd82mn7mq4ammrfpidqf9zsagvhfzgblsi8g4b2fgvw1")))

