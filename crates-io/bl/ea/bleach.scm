(define-module (crates-io bl ea bleach) #:use-module (crates-io))

(define-public crate-bleach-0.1.0 (c (n "bleach") (v "0.1.0") (d (list (d (n "stemmer") (r "^0.3.2") (d #t) (k 0)))) (h "0zcym1jdxz0adrlvm2h54bvk1ly5rm71zx66w0wman2sqgddrwmv")))

(define-public crate-bleach-0.1.1 (c (n "bleach") (v "0.1.1") (d (list (d (n "stemmer") (r "^0.3.2") (d #t) (k 0)))) (h "17ci3kkhx2rbrw00vgs86c3f54qdvk0x6f1rwm9ik14fypa2jba8")))

(define-public crate-bleach-0.1.2 (c (n "bleach") (v "0.1.2") (d (list (d (n "stemmer") (r "^0.3.2") (d #t) (k 0)))) (h "16bm1yadkgs9rrrfhd99rxbmr1ph49c0a4qpbl94wa3zxfcyv6k0")))

