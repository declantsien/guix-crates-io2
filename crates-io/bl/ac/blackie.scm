(define-module (crates-io bl ac blackie) #:use-module (crates-io))

(define-public crate-blackie-0.1.0 (c (n "blackie") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1pfrjdfdv2p1a1qrvjj1f9x82i2gxz192d6zmv7rjmzxmwl30aiw")))

(define-public crate-blackie-0.2.0 (c (n "blackie") (v "0.2.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1cp1cwfivmh9ka2sdmxyhbgxnwzzdn31lfr7n5szniqpj89hy45i")))

(define-public crate-blackie-0.3.0 (c (n "blackie") (v "0.3.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0xw4nydy95rdfgbi9xrhqqqvfpdl2xghhxy44b0ip29dip9mf20h")))

