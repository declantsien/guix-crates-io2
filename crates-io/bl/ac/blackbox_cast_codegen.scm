(define-module (crates-io bl ac blackbox_cast_codegen) #:use-module (crates-io))

(define-public crate-blackbox_cast_codegen-0.1.0 (c (n "blackbox_cast_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "16qb19hg7f4h2v8vs2zi2q4rzsc5ivpxrckmqgfw5byvzgpdc8aj")))

(define-public crate-blackbox_cast_codegen-0.1.1 (c (n "blackbox_cast_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "09v1w1b67ylkllhz4szz5llfc4234pi1p7q8jbfk5ci70nn5jrw7")))

