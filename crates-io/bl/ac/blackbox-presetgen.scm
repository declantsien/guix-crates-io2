(define-module (crates-io bl ac blackbox-presetgen) #:use-module (crates-io))

(define-public crate-blackbox-presetgen-0.1.0 (c (n "blackbox-presetgen") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hpibippfbymx5ib7yclqlbc9mjb4agnksvfggvry079fip5zhd2")))

(define-public crate-blackbox-presetgen-0.1.1 (c (n "blackbox-presetgen") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xh59hpnvb8s1b40rr06fiya0jsdh3ybiywma2wnfsfwn8na4mgh")))

(define-public crate-blackbox-presetgen-0.1.2 (c (n "blackbox-presetgen") (v "0.1.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)))) (h "12jxyija7i8kcsyrwf57sph7z8k70ddrrjl71i542b327d7kn9sj")))

