(define-module (crates-io bl ac blackscholes_wasm) #:use-module (crates-io))

(define-public crate-blackscholes_wasm-0.10.6 (c (n "blackscholes_wasm") (v "0.10.6") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)))) (h "145x26d3zm1naa5ba7i4yk79a6clc8z6y2jkj4zin3j2fvn0qrai")))

(define-public crate-blackscholes_wasm-0.10.7 (c (n "blackscholes_wasm") (v "0.10.7") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("js"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)))) (h "0ffa9mfar1krgq0pz390hng97cm7gnkqhzrisbb12rj8jmipwhjw")))

(define-public crate-blackscholes_wasm-0.18.0 (c (n "blackscholes_wasm") (v "0.18.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1zyajlikwsw9mbx26mh8rw6x8g599pk2yndis7pbsbgdsdzy0jhl")))

(define-public crate-blackscholes_wasm-0.18.1 (c (n "blackscholes_wasm") (v "0.18.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "073m0y7d7x1p2skvn1x3zmpf4a98zf0y86mp3d6zqcbkcah1w5r8")))

