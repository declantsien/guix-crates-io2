(define-module (crates-io bl ac blackbox) #:use-module (crates-io))

(define-public crate-blackbox-0.1.0 (c (n "blackbox") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (d #t) (k 0)))) (h "0q3vjq9bygr18j6zw9y06gff9g06f6m6j6jszizqb1lmxszlrz0s")))

(define-public crate-blackbox-0.2.0 (c (n "blackbox") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "probability") (r "^0.15.12") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rusty-machine") (r "^0.5.4") (d #t) (k 0)) (d (n "slog") (r "^2.4.1") (d #t) (k 0)) (d (n "slog-async") (r "^2.3.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.4.0") (d #t) (k 2)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "slog-term") (r "^2.5.0") (d #t) (k 2)))) (h "0zsgs6m8j2kg8l9mxypxxzbgapvi61k33dqwjfm5nxqr1ajrsj5d")))

