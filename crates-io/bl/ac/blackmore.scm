(define-module (crates-io bl ac blackmore) #:use-module (crates-io))

(define-public crate-blackmore-0.1.0 (c (n "blackmore") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.12") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0nl3f4g3v21bw5ykxs0lapa0kyrnglakrwr7dq8lpi7zv22nxjhq")))

