(define-module (crates-io bl ac blackhole) #:use-module (crates-io))

(define-public crate-blackhole-0.3.0 (c (n "blackhole") (v "0.3.0") (h "0fvpmgjd5lfynwzsq667n9f8ca6vz9926ii52bgg1gy08qgll30m")))

(define-public crate-blackhole-0.3.1 (c (n "blackhole") (v "0.3.1") (h "194njfh5h2nbpl561zw9r7bbv6fraqk2355s4yd2b53j7zlgai1m")))

(define-public crate-blackhole-0.3.2 (c (n "blackhole") (v "0.3.2") (h "181ip6q38krfp5y908dayiy31jcmdcdkal3sn5b2g4a2plhciziv")))

(define-public crate-blackhole-0.4.0 (c (n "blackhole") (v "0.4.0") (h "0zfsz5mq7q9yyjxdwabfawd8wda2yamlqvvzw727mgmcwprylxma")))

(define-public crate-blackhole-0.5.0 (c (n "blackhole") (v "0.5.0") (h "00k980b30p5y5m379sv4xd5drzy2391c1siy5dy8rrinqy7yim86")))

(define-public crate-blackhole-0.5.1 (c (n "blackhole") (v "0.5.1") (h "1nrjv9x77b7xywhbriv57dp7r38ma5k3vjb57vg9v7jbqfd3s30l")))

(define-public crate-blackhole-0.6.0 (c (n "blackhole") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1nzi60v5nlscznqri5ls2xyxxnrypzqnxn9x24nk1dfchdz91lrf")))

(define-public crate-blackhole-0.7.0 (c (n "blackhole") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0j63qab14ggl0miqm5skxvck25lbxcj148192kd70f78sl8nbcx5")))

(define-public crate-blackhole-0.8.0 (c (n "blackhole") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "127kpzkn940pp378psa59js1mx37xhfxgbnl8dxgh606cfwhz495")))

(define-public crate-blackhole-0.9.0 (c (n "blackhole") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0dcyr2j6abvrb6gadsrnqx7kdwsnx8gh5fyzmhf4zpi8s28pkm73")))

(define-public crate-blackhole-0.10.0 (c (n "blackhole") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0zqsd5cfzg9i7k5n16k9s95xal762vw1ilbcw3r790lycqway825")))

(define-public crate-blackhole-0.11.0 (c (n "blackhole") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "132skgh41kiph2f2bqy8hz1yiwj62is4fc6rlm9pi65h2s3ir5bf")))

(define-public crate-blackhole-0.12.0 (c (n "blackhole") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1h1r34r6mf7jzjc4cn5i7399lf0dvf2r3rcya8jgmmrlwv1k654s")))

(define-public crate-blackhole-0.13.0 (c (n "blackhole") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "05447hiav4kc0ya5lkmp6g5ff6x9yglhqn54qkk9f4f9vqmgxkd2")))

(define-public crate-blackhole-0.14.0 (c (n "blackhole") (v "0.14.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0n1fxrv0qfw304w9vpnmi9plkq3ikqg57w099a179ni42ivd566m")))

(define-public crate-blackhole-0.15.0 (c (n "blackhole") (v "0.15.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0xcxz6bppymqdcpzm35wfblsmij3d1qz3ag6yka3kk78hfzqbq4y")))

(define-public crate-blackhole-0.16.0 (c (n "blackhole") (v "0.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "18gxqjrha9b7wxcngli9c45vc1hr3r2kcnhqg8im22n7v6znyj9a")))

(define-public crate-blackhole-0.17.0 (c (n "blackhole") (v "0.17.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0ssx3p609cvxs6a9kgxsxrlwlvhhvzsy4kkk7d3fz5kmywb4lihd")))

(define-public crate-blackhole-0.17.1 (c (n "blackhole") (v "0.17.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0mqv79lg39jy1pr9093nqmrbxmp0pacslvhwil944i47c7fzkzac")))

(define-public crate-blackhole-0.18.0 (c (n "blackhole") (v "0.18.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0y6ky8dlzh4jhaf8kcvsm798vagn0hmsmsmbh5b7lpkmchfhsg23")))

(define-public crate-blackhole-0.19.0 (c (n "blackhole") (v "0.19.0") (d (list (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "0ww4hs7znvzsq8hkdv9rqv7df8xr8l8ff3g3lmvfqhwivj8c6nln")))

(define-public crate-blackhole-0.19.1 (c (n "blackhole") (v "0.19.1") (d (list (d (n "libc") (r ">=0.2, <0.3") (d #t) (k 2)))) (h "19v61kblbv7ipx061agdkn8k7ljbdw5718r4yzxngy2143akv73r")))

(define-public crate-blackhole-0.20.0 (c (n "blackhole") (v "0.20.0") (h "1511n8p4nhmb7nf1hn2iq5dwgnckqqllvv3sb4il8aiqpx0arlpa")))

(define-public crate-blackhole-0.20.1 (c (n "blackhole") (v "0.20.1") (h "13dn8a0iqsymrg1q4d7nhhfd4310d5mp7x3xp3sfg4fs8bxg6v0k")))

(define-public crate-blackhole-0.20.2 (c (n "blackhole") (v "0.20.2") (h "00434bgg1z3f5banldw5pqrag0rrbrc9m2imfj687kw6xhwjq91x")))

