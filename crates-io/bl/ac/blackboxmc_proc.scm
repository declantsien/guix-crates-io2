(define-module (crates-io bl ac blackboxmc_proc) #:use-module (crates-io))

(define-public crate-blackboxmc_proc-0.1.0 (c (n "blackboxmc_proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "10yavlhivvqndczc9j7zlxpak8sqd03xicg28ny562gffbzq3z44")))

(define-public crate-blackboxmc_proc-0.2.0 (c (n "blackboxmc_proc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0nbkghwdxfjifj13dxysfriwyf9r0yp0qpb3j6i5xc5l9s6yg0m8")))

(define-public crate-blackboxmc_proc-0.3.0 (c (n "blackboxmc_proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0gvbcliqfxlccr83rlwkdjiniczsgy47myba31i5bwcfh8k24nfa")))

(define-public crate-blackboxmc_proc-0.3.1 (c (n "blackboxmc_proc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0m33w12c3f09sb9q78wjfrw4xrgqx2mvhjsyd4kp5r30qrxwqazd")))

(define-public crate-blackboxmc_proc-0.4.0 (c (n "blackboxmc_proc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "082c7xfy9f32hp8bdaj005rscgzybwj07zn2fh4v3ivqq4z73x1n")))

(define-public crate-blackboxmc_proc-0.4.1 (c (n "blackboxmc_proc") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1knbbivvxv7d9h6yqmqb303hl0pl5ims27vvixpfx253kyflwb0l")))

(define-public crate-blackboxmc_proc-0.5.0 (c (n "blackboxmc_proc") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "192vx2ybhgfp43nxijzx7i394d5zgfvngxlp8s5v19a1jhgdbsd1")))

(define-public crate-blackboxmc_proc-0.5.1 (c (n "blackboxmc_proc") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0a2lkwxzqjwxz48zsnazxlf8snbcr3agsb795npdcrw5q13jx4zp")))

