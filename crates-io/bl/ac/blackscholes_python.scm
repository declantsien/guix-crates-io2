(define-module (crates-io bl ac blackscholes_python) #:use-module (crates-io))

(define-public crate-blackscholes_python-0.10.4 (c (n "blackscholes_python") (v "0.10.4") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0280chlv1llk0vli80p2gisy1d77qngq1kb9dpb507skcyakdbz0")))

(define-public crate-blackscholes_python-0.10.6 (c (n "blackscholes_python") (v "0.10.6") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0ilmgcarfh83mf8fwl4vwdr59mr4h1pf0i7z9pk0j0bmvd2a8pdw")))

(define-public crate-blackscholes_python-0.10.7 (c (n "blackscholes_python") (v "0.10.7") (d (list (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0585208zm1q9582gps1hngd8v9d6kbks8jdwfmw8bchq1hd2klc6")))

