(define-module (crates-io bl ac blacksmith_macros) #:use-module (crates-io))

(define-public crate-blacksmith_macros-0.1.0 (c (n "blacksmith_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "080yffrrgsicip4xic45f4fcg767s7naln7m53bjlvm0kil2h5xf")))

(define-public crate-blacksmith_macros-0.1.1 (c (n "blacksmith_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "1ygbb14nhn1i9gkypnas8vvj64j96bxa0xdbzj00qi20w8nd64q3")))

