(define-module (crates-io bl ac blackbox_derive) #:use-module (crates-io))

(define-public crate-blackbox_derive-0.1.0 (c (n "blackbox_derive") (v "0.1.0") (d (list (d (n "blackbox") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (d #t) (k 0)))) (h "0i6kyd2sv3fvp47lj6nwwkvbba8nzvqi3mys0pd7adgxy1hiylc7")))

