(define-module (crates-io bl ac blackbox-tool) #:use-module (crates-io))

(define-public crate-blackbox-tool-0.1.0 (c (n "blackbox-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "fc-blackbox") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1gnykz2qk9lxc3razzx5w6pq5wgxn0mszcz5qrjdr4qz9pcslksr")))

