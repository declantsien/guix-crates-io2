(define-module (crates-io bl ac black_grimoire) #:use-module (crates-io))

(define-public crate-black_grimoire-0.1.0 (c (n "black_grimoire") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "gamemath") (r "^0.4.0") (d #t) (k 0)) (d (n "gameprng") (r "^0.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.30.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "1mfijc3zn2sx4zhnrpfab0g5iij4cwph5v8b5pd9fajwxa0m75g2")))

(define-public crate-black_grimoire-0.1.1 (c (n "black_grimoire") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "gamemath") (r "^0.4.0") (d #t) (k 0)) (d (n "gameprng") (r "^0.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glutin") (r "^0.30.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "lodepng") (r "^3.7.2") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "01d1r50xvzqay4m9kyw5vrsw8b93b4wwykcpcmcp8zallr0rxib8")))

