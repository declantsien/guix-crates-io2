(define-module (crates-io bl ac blackd-client) #:use-module (crates-io))

(define-public crate-blackd-client-0.0.3 (c (n "blackd-client") (v "0.0.3") (d (list (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "minreq") (r "^2.3.1") (d #t) (k 0)))) (h "1zc4whlq8mmbaj5z152wsx8nxxaiv622hdmicmcbwwyiq49xs0wg")))

(define-public crate-blackd-client-0.0.4 (c (n "blackd-client") (v "0.0.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "minreq") (r "^2.3.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1r8s0np21n5pxz76gjs1cjabcpwwzrsrr5kgijvbvq4gcad67l4n")))

(define-public crate-blackd-client-0.0.5 (c (n "blackd-client") (v "0.0.5") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "minreq") (r "^2.3.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "0kv891vhhpfcla8m31rxspldamhqnqzclhy695d2hv8adgl3s11g")))

(define-public crate-blackd-client-0.0.6 (c (n "blackd-client") (v "0.0.6") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "minreq") (r "^2.3.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "08n5gbs47qp0ycch63afcw1874f76fd1zgxbpfa0wjn060c8i35r")))

(define-public crate-blackd-client-0.1.0 (c (n "blackd-client") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "httpmock") (r "^0.5.8") (d #t) (k 2)) (d (n "minreq") (r "^2.3.1") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)))) (h "1di4xjwi2xwakffy8n19rpsj0dd2880fchagqa5j2sfnk9yiwyxg")))

