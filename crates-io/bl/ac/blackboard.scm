(define-module (crates-io bl ac blackboard) #:use-module (crates-io))

(define-public crate-blackboard-0.1.0 (c (n "blackboard") (v "0.1.0") (h "0fsv7mf3xzglbdv3p5lvgd45g79qzvardgvns5cmdqqvjpikm8iy")))

(define-public crate-blackboard-1.0.0 (c (n "blackboard") (v "1.0.0") (h "1fp1h0a4466nfmz0g6lncdi2sl2x9f4swsa0dhxbak1hrql7yjpa")))

(define-public crate-blackboard-1.1.0 (c (n "blackboard") (v "1.1.0") (h "1pb00i0xd6rxdhrz13gyl6dqdph1djigfz3fazci68zsc3jcfj3q")))

