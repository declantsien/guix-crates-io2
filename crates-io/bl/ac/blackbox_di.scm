(define-module (crates-io bl ac blackbox_di) #:use-module (crates-io))

(define-public crate-blackbox_di-0.1.0 (c (n "blackbox_di") (v "0.1.0") (d (list (d (n "blackbox_core") (r "^0.1.0") (d #t) (k 0)))) (h "0aj4786c5yf7flj1xc3xaj328gqm1632l1ijd620d5y186fn9m5r")))

(define-public crate-blackbox_di-0.1.1 (c (n "blackbox_di") (v "0.1.1") (d (list (d (n "blackbox_core") (r "^0.1.1") (d #t) (k 0)))) (h "0cq5cb40nckwdwwmq1k9qqb3pkj71ji2ijh1xlwdwdx1il8y3m1g")))

