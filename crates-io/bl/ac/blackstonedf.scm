(define-module (crates-io bl ac blackstonedf) #:use-module (crates-io))

(define-public crate-blackstonedf-0.1.0 (c (n "blackstonedf") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.2.0") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "chumsky") (r "^1.0.0-alpha.3") (f (quote ("regex"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libflate") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1qjrkfgmryaqdxbnhcw8q1piy6xyn6sr7rr9fwm78fyx4dvx6g0x")))

