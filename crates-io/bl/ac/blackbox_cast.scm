(define-module (crates-io bl ac blackbox_cast) #:use-module (crates-io))

(define-public crate-blackbox_cast-0.1.0 (c (n "blackbox_cast") (v "0.1.0") (d (list (d (n "blackbox_cast_codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "linkme") (r "^0.3.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "1q9gbj0h3bx5n8597djd807xn24bhbik1grgksd5dakk3z8lmz13")))

(define-public crate-blackbox_cast-0.1.1 (c (n "blackbox_cast") (v "0.1.1") (d (list (d (n "blackbox_cast_codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "linkme") (r "^0.3.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "18jv103yfjjfydvqhw097shhwhddz6yp03fb8x0ia0k6yj8iarbi")))

