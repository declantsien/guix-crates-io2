(define-module (crates-io bl ac blackjack) #:use-module (crates-io))

(define-public crate-blackjack-0.1.0 (c (n "blackjack") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0n54zl99hn5jkgxn61wy9xw3fjxgs016x86s3daisyxnwm5q80yx")))

(define-public crate-blackjack-0.1.1 (c (n "blackjack") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0xiyzqml00bva8c6hxm4dr17ivfwanzdzxjvbxacf3f21clrvc4r")))

