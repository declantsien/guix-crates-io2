(define-module (crates-io bl ac black76) #:use-module (crates-io))

(define-public crate-black76-0.18.0 (c (n "black76") (v "0.18.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "02wflrzsj30zrz2zk72jk183is81lxmyklbklal1ph4dp48m5lg7")))

(define-public crate-black76-0.19.0 (c (n "black76") (v "0.19.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0ql8gkyhmlhhgq729b18hcdcl5fsbp6zc41g7b6vjlmrdak14nbv")))

(define-public crate-black76-0.19.1 (c (n "black76") (v "0.19.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0rnpzz92abjjlxngkclgvw20g8mq6c9q8hry54jgqn6pfmzm9sv2")))

(define-public crate-black76-0.19.2 (c (n "black76") (v "0.19.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1sb67v6zjf65dyddim735riqlayq6xv2sagb4m61whaxb56rsz0k")))

(define-public crate-black76-0.19.3 (c (n "black76") (v "0.19.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1khhhr39rpqlchl6vnjy83041n2n7psisgxb261nw2kh7jvbw33y")))

(define-public crate-black76-0.20.0 (c (n "black76") (v "0.20.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0yl4w8fbcqimsz11cm8ig8mp25h20yjnb47kiwa5jimzy4ynsv5c")))

(define-public crate-black76-0.21.0 (c (n "black76") (v "0.21.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1xw50rgls3m8lhbdc5f86dkvlgmzspvsj0k0v8q4hs2kgmdmmjph")))

(define-public crate-black76-0.22.0 (c (n "black76") (v "0.22.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "105dy7piv9pmfpa004v7y82kinv8s0yv2zq4i7knwipn5bwfccnp")))

(define-public crate-black76-0.22.1 (c (n "black76") (v "0.22.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "183l6d4v9y63h7bmpq7pvkjjwc0d6fsfp3vwzfgw8f1dpiql018j")))

(define-public crate-black76-0.23.0 (c (n "black76") (v "0.23.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1nmi9sc5pbw91z9ksiahh36v5is7cy1iychkj6j6lfhdyr1b4viw")))

(define-public crate-black76-0.23.1 (c (n "black76") (v "0.23.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "01d7fllhaly4vkh45gnifzhi0y05ia02ibbzwxgbsr2lz263jp9b")))

(define-public crate-black76-0.23.2 (c (n "black76") (v "0.23.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1hrdgsh2g5bs32bwqrsyvvihrnpamv3zx9pnxb26p9jgkd1n8qhv")))

(define-public crate-black76-0.23.3 (c (n "black76") (v "0.23.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1733yzgn2w2m3c5zbabrqp2n7xd9fza235jk5rpi65gvbybmlirk")))

(define-public crate-black76-0.24.0 (c (n "black76") (v "0.24.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0952nv0hgcmblqvdax9h28kcwl3vff08848hyb3a71nfj98nzgym")))

(define-public crate-black76-0.24.1 (c (n "black76") (v "0.24.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0l8ld6qv4q4dfpwylbaljcvxm2br6ydnwj5gp72pdfsf83jcc7b1")))

(define-public crate-black76-0.24.2 (c (n "black76") (v "0.24.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1z1qnxprmz0d18bnkibxjg4br7fsmxcy6fgj7dc64ww6lp4v3y18")))

