(define-module (crates-io bl ac blackscholes) #:use-module (crates-io))

(define-public crate-blackscholes-0.10.0 (c (n "blackscholes") (v "0.10.0") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1n5knn8krry670j1y60qhhkpdym9q4k9kp7n9d0idjygw0ipjy64")))

(define-public crate-blackscholes-0.10.1 (c (n "blackscholes") (v "0.10.1") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1pyir6wvn7s4im5afmf1sp4yzm3fid0ffdv6k34g0p6p8ddwvj7i")))

(define-public crate-blackscholes-0.10.2 (c (n "blackscholes") (v "0.10.2") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0ykhknfs24dyllr5hfd747jk74abjzsm9nj6kfm495m4zqvba426")))

(define-public crate-blackscholes-0.10.3 (c (n "blackscholes") (v "0.10.3") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0092lh3h0fd6w9yayy57yymcpihsv4jbj2gbw7fm2g3x71phm0km")))

(define-public crate-blackscholes-0.10.5 (c (n "blackscholes") (v "0.10.5") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0xxzka2x5i7m52ar6v7325dg7k1vgh43jxbx3qra38k15njlk1kp")))

(define-public crate-blackscholes-0.11.0 (c (n "blackscholes") (v "0.11.0") (d (list (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0mrziy66k93qd2z645lxbxvp3gqzbkxivqsviyzv52w28kg4imis")))

(define-public crate-blackscholes-0.12.0 (c (n "blackscholes") (v "0.12.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0aaqi3p7pd4azzbrnd8blc7khzbr47f0jg30zgdvf3shbjc87q0x")))

(define-public crate-blackscholes-0.13.0 (c (n "blackscholes") (v "0.13.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "149jjsa2gnsmawsyb6zj5y3k628h4lkx2p8bfhb2zypnqrknsvqj")))

(define-public crate-blackscholes-0.13.1 (c (n "blackscholes") (v "0.13.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0wp2xz5rrw8js9ynraqjhvm4lvgqy98axm50xbfhc2jdvcx0q8fr")))

(define-public crate-blackscholes-0.16.0 (c (n "blackscholes") (v "0.16.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "164lm7mmh04hanrk0lmh9as9hz322hck49g3wl4a2gi9bdkbqq42")))

(define-public crate-blackscholes-0.17.0 (c (n "blackscholes") (v "0.17.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "01aw6jbfx01ayyjzbgk90ryf4c3lf4a7nbpffwcpbl5as9ifr5cz")))

(define-public crate-blackscholes-0.17.1 (c (n "blackscholes") (v "0.17.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "09majbji8iqpz8b51vk99w4h3690dsvbdhi845xxh2rnlvdyb10f")))

(define-public crate-blackscholes-0.17.2 (c (n "blackscholes") (v "0.17.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0cn4z6xkj22l6m26cpmf2zkkb6k8r539sza35fk9xhw1h0whxrsc")))

(define-public crate-blackscholes-0.17.3 (c (n "blackscholes") (v "0.17.3") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0nwvqphysj7a75sv2b0bnd6cv9427l02shckjz6vypbjphvg2gwn")))

(define-public crate-blackscholes-0.17.4 (c (n "blackscholes") (v "0.17.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "13c8ll6cpsxiyw1ml33yi9v5i5g1p33vqxsy2acxdxdahcd96rjg")))

(define-public crate-blackscholes-0.17.5 (c (n "blackscholes") (v "0.17.5") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0bl7m2li39wrr9iryvw253hz1p1d3hdyg6r79aamayb5bd55v1vl")))

(define-public crate-blackscholes-0.17.6 (c (n "blackscholes") (v "0.17.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0ml0km2ga4mwskr70y9wjig79d8hwdyp21ga6lywhznqdkk9ngwa")))

(define-public crate-blackscholes-0.18.0 (c (n "blackscholes") (v "0.18.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0zz4l4f73qkyn7qll6diqdjdiv14i17h0nfj1078vd45pghhg2sj")))

(define-public crate-blackscholes-0.22.1 (c (n "blackscholes") (v "0.22.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1lf9yczli57nw0icbsqvlg4q1fx9h5iq39cjm5382b7ay412mf8p")))

(define-public crate-blackscholes-0.22.2 (c (n "blackscholes") (v "0.22.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1n5jy0w4zp2yb6b645h596k9ccl5p2xwd76ryp9z91jfaidws09y")))

(define-public crate-blackscholes-0.23.0 (c (n "blackscholes") (v "0.23.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1sxwf6dkdydaf5zjl3xs9927k3cvylbr7rns8b97mz9yhvx2hq09")))

(define-public crate-blackscholes-0.23.1 (c (n "blackscholes") (v "0.23.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "06lfwnaazs3ja8hb1ka9w01lf5532fvk20f6i2mcdpq2xnw4cc8b")))

(define-public crate-blackscholes-0.23.2 (c (n "blackscholes") (v "0.23.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0y73ryj4agpfv2i316s48acjachvnhhywb96jyhx43i8a78gqr6c")))

(define-public crate-blackscholes-0.23.3 (c (n "blackscholes") (v "0.23.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0mh6i8srkdisz3gw8y42ai4c7gmfsav5av5lpx2dsnskdb872n44")))

(define-public crate-blackscholes-0.24.0 (c (n "blackscholes") (v "0.24.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "statrs") (r "^0.16") (d #t) (k 0)))) (h "08717mc47jcg23q3fr6rm8mnr5db0asl5aw356kldp3ibdhb9z1v")))

