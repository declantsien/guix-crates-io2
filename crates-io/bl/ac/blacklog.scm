(define-module (crates-io bl ac blacklog) #:use-module (crates-io))

(define-public crate-blacklog-0.1.0 (c (n "blacklog") (v "0.1.0") (d (list (d (n "peg") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)))) (h "009dzx8nvrj07j23flicwk97lgr5rkx6q519i5m64y7409j78jbv") (f (quote (("benchmark"))))))

(define-public crate-blacklog-0.1.1 (c (n "blacklog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "peg") (r "^0.3") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)))) (h "1qy5znqvp7ry6fkpxpslf1hjdi9akflgg8m7dhccjvzix04jg23p") (f (quote (("benchmark"))))))

