(define-module (crates-io bl ac black76_wasm) #:use-module (crates-io))

(define-public crate-black76_wasm-0.20.0 (c (n "black76_wasm") (v "0.20.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "00bzyz1bpijzs1n0vwl1vshmhwxhn43m1ms95rigif18pc5hjwnl")))

