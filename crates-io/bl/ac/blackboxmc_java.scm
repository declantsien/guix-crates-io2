(define-module (crates-io bl ac blackboxmc_java) #:use-module (crates-io))

(define-public crate-blackboxmc_java-0.1.0 (c (n "blackboxmc_java") (v "0.1.0") (d (list (d (n "blackboxmc_general") (r "^0.1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "062ajan9cqjm5rha7697j7nrvr74dwiksdpr6hidv67m5iwn4vgk")))

(define-public crate-blackboxmc_java-0.2.0 (c (n "blackboxmc_java") (v "0.2.0") (d (list (d (n "blackboxmc_general") (r "^0.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "0sx2k3zfdrs5w6h4zmif82i7pavdbil75xwdrx9sg31d1ws7xl52")))

(define-public crate-blackboxmc_java-0.3.0 (c (n "blackboxmc_java") (v "0.3.0") (d (list (d (n "blackboxmc_general") (r "^0.3.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (k 0)))) (h "0fdbvpd6qdz0f0gypw9wxlqirw240wsb5j450ri3pfy842k6ap77")))

(define-public crate-blackboxmc_java-0.3.1 (c (n "blackboxmc_java") (v "0.3.1") (d (list (d (n "blackboxmc_general") (r "^0.3.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (k 0)))) (h "0hfv7nvn874m7z7fcn7rmilm3v67l250k048cakflcfjr8giskcm")))

(define-public crate-blackboxmc_java-0.4.0 (c (n "blackboxmc_java") (v "0.4.0") (d (list (d (n "blackboxmc_general") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (k 0)))) (h "064fg0wxqipn9vrcjh1jyr1lijcqmml18lpzjahlv2ihq6n4x6mj")))

(define-public crate-blackboxmc_java-0.4.1 (c (n "blackboxmc_java") (v "0.4.1") (d (list (d (n "blackboxmc_general") (r "^0.4.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (k 0)))) (h "1h45cl9cb073zhhm4a1l0b3h0i9ms1rim3ybw18kfqifdw38738g")))

(define-public crate-blackboxmc_java-0.5.0 (c (n "blackboxmc_java") (v "0.5.0") (d (list (d (n "blackboxmc_general") (r "^0.5.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0rllr50d3g9ds25hqsz73n806gyg1y1vgawl2glyijzdccy53ab1")))

(define-public crate-blackboxmc_java-0.5.1 (c (n "blackboxmc_java") (v "0.5.1") (d (list (d (n "blackboxmc_general") (r "^0.5.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1b0xkhpjxfiim576y06z2p4n1al8icyxx588bqkvdy75azdqhrg3")))

