(define-module (crates-io bl ac blackhole-compress) #:use-module (crates-io))

(define-public crate-blackhole-compress-0.1.0 (c (n "blackhole-compress") (v "0.1.0") (h "1257bnbnx6nff3r2yrfa19iri2ph58xw4995syzcc4yskpph06xg")))

(define-public crate-blackhole-compress-0.1.1 (c (n "blackhole-compress") (v "0.1.1") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "16wpkkwlmm58c1ixw6fy87ak01q254arlxyp4z2p9f3k4sc4ywfh")))

(define-public crate-blackhole-compress-0.1.2 (c (n "blackhole-compress") (v "0.1.2") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0q5359mydfbzi6in1rmhhkpz1jlpqr785afbnapm3134w9ffddhh")))

(define-public crate-blackhole-compress-0.1.3 (c (n "blackhole-compress") (v "0.1.3") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)))) (h "0s0h33pcjxjqirzrlkgpf4h4nsr55dasvvd1g8wvxsvkh57q9c0c")))

