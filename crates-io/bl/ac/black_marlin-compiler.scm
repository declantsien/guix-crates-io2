(define-module (crates-io bl ac black_marlin-compiler) #:use-module (crates-io))

(define-public crate-black_marlin-compiler-0.3.4 (c (n "black_marlin-compiler") (v "0.3.4") (d (list (d (n "filetime") (r "^0.2.15") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r ">=1.0.11, <=1.0.36") (f (quote ("span-locations"))) (k 0)) (d (n "quote") (r "^1.0.14") (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("parsing" "full" "visit-mut" "printing"))) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1vwgmm1r6yyydawhgy6fn7yx625d30scxp26nvxskgcs64qwp0rq") (f (quote (("procmacro") ("default" "config") ("config" "yaml-rust")))) (y #t)))

