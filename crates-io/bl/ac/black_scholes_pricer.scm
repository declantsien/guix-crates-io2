(define-module (crates-io bl ac black_scholes_pricer) #:use-module (crates-io))

(define-public crate-black_scholes_pricer-0.2.0 (c (n "black_scholes_pricer") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "wide") (r "^0.5") (d #t) (k 0)))) (h "1vmqrlq5aj02q95z6jp19qsxs2ma96z3xbm4478q7gpcbb0n8gvk")))

(define-public crate-black_scholes_pricer-0.2.1 (c (n "black_scholes_pricer") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "wide") (r "^0.5") (d #t) (k 0)))) (h "03pdn971j51gaj667wailqr0xbsr3daps94vrva846wsqa4qbzwp")))

