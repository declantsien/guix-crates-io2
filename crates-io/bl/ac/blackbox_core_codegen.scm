(define-module (crates-io bl ac blackbox_core_codegen) #:use-module (crates-io))

(define-public crate-blackbox_core_codegen-0.1.0 (c (n "blackbox_core_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15hhbrjpvlz2x46sdf0yl1f226kvl5cc4zpzps7l5w50mncfcv9k")))

(define-public crate-blackbox_core_codegen-0.1.1 (c (n "blackbox_core_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04z4g85z4i1d8w2xcqpxmh8xhlcx6w54aixq9aqf5qcqj69yx99j")))

