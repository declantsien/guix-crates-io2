(define-module (crates-io bl ac blackbox-generator) #:use-module (crates-io))

(define-public crate-blackbox-generator-0.0.1 (c (n "blackbox-generator") (v "0.0.1") (h "1i1hkfxnph8m6c44rp59bgsxmwwjp8gh3x81wd85zrpffxd99k14")))

(define-public crate-blackbox-generator-0.0.2 (c (n "blackbox-generator") (v "0.0.2") (h "00ic8z8v7scacapmg00yfq19knb51wanzdryzbxna1vfk1n95l6l")))

(define-public crate-blackbox-generator-0.0.3 (c (n "blackbox-generator") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "12yp0if3wdzpxc2dxs3hbyrszdhs5vkzl5licqsaqkm7daljbv41")))

(define-public crate-blackbox-generator-0.0.4 (c (n "blackbox-generator") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "02zs16118zd4fk8sy553z6pjnlqpfs1iff58syxddz45bdkw3s52")))

(define-public crate-blackbox-generator-0.0.6 (c (n "blackbox-generator") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1hd9myn8a5va711i1z4xwn9iacsvnmgk9n19cfkk5ap9asj6rcmv")))

