(define-module (crates-io bl ac black_marlin) #:use-module (crates-io))

(define-public crate-black_marlin-0.3.4 (c (n "black_marlin") (v "0.3.4") (d (list (d (n "black_marlin-macros") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "itoap") (r "^1.0.1") (d #t) (k 0)) (d (n "ryu") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9.4") (d #t) (k 1)))) (h "1dpspcikxq04v8i0s21kl8al7spn08xpjg522aa49q83f362jzvp") (f (quote (("perf-inline") ("json" "serde" "serde_json") ("derive" "black_marlin-macros") ("default" "derive" "perf-inline")))) (y #t)))

