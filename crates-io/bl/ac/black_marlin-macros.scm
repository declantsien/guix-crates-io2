(define-module (crates-io bl ac black_marlin-macros) #:use-module (crates-io))

(define-public crate-black_marlin-macros-0.3.4 (c (n "black_marlin-macros") (v "0.3.4") (d (list (d (n "black_marlin-compiler") (r "^0.3.4") (f (quote ("procmacro"))) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)))) (h "1w7ls85rh0h7d0l04j9h5hshny1fpilywg9q73n2h9d1z839z3i6") (f (quote (("default" "config") ("config" "black_marlin-compiler/config")))) (y #t)))

