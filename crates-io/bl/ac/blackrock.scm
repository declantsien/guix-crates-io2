(define-module (crates-io bl ac blackrock) #:use-module (crates-io))

(define-public crate-blackrock-0.1.0 (c (n "blackrock") (v "0.1.0") (d (list (d (n "bitreel") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)))) (h "06j6v54lrb0qdvffg0nsig6al9nmsvl4sn9l34vpzkp1485295a5")))

(define-public crate-blackrock-0.2.0 (c (n "blackrock") (v "0.2.0") (d (list (d (n "bitreel") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.7") (d #t) (k 0)))) (h "0an08zm6yxc1n4vgg46cyrypyjyihs4j7lmilb6yjagn97pvz7zf")))

