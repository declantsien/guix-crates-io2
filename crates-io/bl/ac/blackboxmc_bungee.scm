(define-module (crates-io bl ac blackboxmc_bungee) #:use-module (crates-io))

(define-public crate-blackboxmc_bungee-0.1.0 (c (n "blackboxmc_bungee") (v "0.1.0") (d (list (d (n "blackboxmc_general") (r "^0.1.0") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.1.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "02z3rhm80b34mlcvh24aj1z1g8zn896q2sh589r2idl5w4n2g2zs")))

(define-public crate-blackboxmc_bungee-0.2.0 (c (n "blackboxmc_bungee") (v "0.2.0") (d (list (d (n "blackboxmc_general") (r "^0.2.0") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.2.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "1d6xddndjwz4ayadsyx5qskwn0385s65my805mikj7fij5dllin6")))

(define-public crate-blackboxmc_bungee-0.3.0 (c (n "blackboxmc_bungee") (v "0.3.0") (d (list (d (n "blackboxmc_general") (r "^0.3.0") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.3.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "1d75ac260kyjbnq94y7d4flvg6b40kzg9bszph6kik0qifzywf07")))

(define-public crate-blackboxmc_bungee-0.3.1 (c (n "blackboxmc_bungee") (v "0.3.1") (d (list (d (n "blackboxmc_general") (r "^0.3.1") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.3.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "0pilh04hfp7qy2zvcz9xgxbnvmzmvsd0mz7xs56443mmbswl9i1d")))

(define-public crate-blackboxmc_bungee-0.4.0 (c (n "blackboxmc_bungee") (v "0.4.0") (d (list (d (n "blackboxmc_general") (r "^0.4.0") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "1ppj16zrg64cm4i9z3i4jwb7dv6r5pa8njv82s2lbcqzcb2l6q0q")))

(define-public crate-blackboxmc_bungee-0.4.1 (c (n "blackboxmc_bungee") (v "0.4.1") (d (list (d (n "blackboxmc_general") (r "^0.4.1") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.4.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "15mra6wxwiv5aam3a4sdspnsn37v5gd63ram6n3pi22xprwrdlld")))

(define-public crate-blackboxmc_bungee-0.5.0 (c (n "blackboxmc_bungee") (v "0.5.0") (d (list (d (n "blackboxmc_general") (r "^0.5.0") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.5.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "049hdswcrpcmx70fm5yhahc85snzjikcw64z75wjhwgqsyilr0hv")))

(define-public crate-blackboxmc_bungee-0.5.1 (c (n "blackboxmc_bungee") (v "0.5.1") (d (list (d (n "blackboxmc_general") (r "^0.5.1") (d #t) (k 0)) (d (n "blackboxmc_java") (r "^0.5.1") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "187ackrsc1pz2sbrs5qv31lmgf9nmshqmybx732lpq17qdz6dczw")))

