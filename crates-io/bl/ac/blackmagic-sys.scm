(define-module (crates-io bl ac blackmagic-sys) #:use-module (crates-io))

(define-public crate-blackmagic-sys-0.1.0 (c (n "blackmagic-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "08hcb0qbdsdk1d0fhpxg4g99sxz0ysk1q6xjp5xbmcqa26vx8zy7")))

