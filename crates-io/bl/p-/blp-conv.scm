(define-module (crates-io bl p- blp-conv) #:use-module (crates-io))

(define-public crate-blp-conv-0.1.0 (c (n "blp-conv") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "image-blp") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0yr73bzs9v9mxpani9zr5gwx9c5ld05n0nhpjigqdk4x2m37h4py")))

(define-public crate-blp-conv-0.1.1 (c (n "blp-conv") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "image-blp") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "09y9qf75abd5cc2gm24h94dzc3warlss8yqpgd4cd8dy49prfqqn")))

