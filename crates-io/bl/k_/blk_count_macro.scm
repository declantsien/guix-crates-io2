(define-module (crates-io bl k_ blk_count_macro) #:use-module (crates-io))

(define-public crate-blk_count_macro-0.1.0 (c (n "blk_count_macro") (v "0.1.0") (h "1cizgdyiyrsybdsw24331cqg8vjq8q6311rf1hy1xf97z5hxvxhs")))

(define-public crate-blk_count_macro-0.1.1 (c (n "blk_count_macro") (v "0.1.1") (h "13q048ja7c8icn7cx4z85aq1wj509bac7ab8lzpb5i74z10yzkn5")))

