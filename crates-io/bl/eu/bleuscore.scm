(define-module (crates-io bl eu bleuscore) #:use-module (crates-io))

(define-public crate-bleuscore-0.1.0 (c (n "bleuscore") (v "0.1.0") (d (list (d (n "cached") (r "^0.49.3") (d #t) (k 0)) (d (n "counter") (r "^0.5.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1kf0kn4pjkhhyclrwqilk82hc4dzw51dwx6v5r3fn5va0d8ddvnv")))

(define-public crate-bleuscore-0.1.1 (c (n "bleuscore") (v "0.1.1") (d (list (d (n "cached") (r "^0.50.0") (d #t) (k 0)) (d (n "counter") (r "^0.5.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0mwhwvizj1c7id8cyv48ywxg9y95ziad6n7xdgn1gr4pbrzaiiy7")))

(define-public crate-bleuscore-0.1.2 (c (n "bleuscore") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "cached") (r "^0.50.0") (d #t) (k 0)) (d (n "counter") (r "^0.5.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1ngp9afqcj92rj4602b3rkd72sq6j5jpx286c82f6iflxwplhkfl")))

(define-public crate-bleuscore-0.1.3 (c (n "bleuscore") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "cached") (r "^0.51.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "06yzci4njw4c08jf3qlvwazpz5rhf57p04j3m6iaq35aghf1q1bz")))

