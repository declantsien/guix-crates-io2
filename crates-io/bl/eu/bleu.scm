(define-module (crates-io bl eu bleu) #:use-module (crates-io))

(define-public crate-bleu-0.0.1 (c (n "bleu") (v "0.0.1") (d (list (d (n "bimap") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "04wjjxqq9a4r0yqrkl7c3adxnixnr29081g9yrisgcnpkf1c5xrg")))

(define-public crate-bleu-0.0.2 (c (n "bleu") (v "0.0.2") (d (list (d (n "bimap") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0qxqrfx65g792mpqqy8l1n2c66csqvqjjmm35gc6l8fi1z0mm3xa")))

(define-public crate-bleu-0.0.3 (c (n "bleu") (v "0.0.3") (d (list (d (n "bimap") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "12zr94my1s7k9nvgmk7ilann23g9pcx1qpimdg21gpdz16jq6h7m")))

(define-public crate-bleu-0.0.4 (c (n "bleu") (v "0.0.4") (d (list (d (n "bimap") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0a97jprkldvs7h3iz2i4sxhzlz86ai6906q1dls4lhpbax637x3a")))

(define-public crate-bleu-0.0.5 (c (n "bleu") (v "0.0.5") (d (list (d (n "bimap") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "blurz") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1gqnhqh0sw4l697hmcf29r07ivii3aa745pldryxfz575xm9y5nl")))

