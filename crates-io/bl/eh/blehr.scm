(define-module (crates-io bl eh blehr) #:use-module (crates-io))

(define-public crate-blehr-0.1.0 (c (n "blehr") (v "0.1.0") (d (list (d (n "bleasy") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "15v3jx7ijq1ylz7qyfdyjc4lyp3rx5v7s74nil0d9ig8mhimmpl8")))

