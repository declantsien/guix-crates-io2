(define-module (crates-io bl ar blarg_derive) #:use-module (crates-io))

(define-public crate-blarg_derive-1.0.0 (c (n "blarg_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0jj4l1f55vn9vz110ygyyq5vcqq3ki9idgkigy0jnw4qcnlz46hx") (y #t)))

(define-public crate-blarg_derive-1.0.1 (c (n "blarg_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgm7gjs3zj49xn44y2dfvz69xgb13x2df5xvx1si80rsr98wp78") (y #t)))

(define-public crate-blarg_derive-1.0.2 (c (n "blarg_derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0w714w38r2b2lhgx5625zpa19hsys31baff5cll6h1qk80mdg4s4")))

(define-public crate-blarg_derive-1.0.3 (c (n "blarg_derive") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1swbsad4srmlf5161mq4rr4w81ixw2ji9vzsjizdajbkm5qzmsab")))

(define-public crate-blarg_derive-1.0.4 (c (n "blarg_derive") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0.62") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0k6gmmi6bgs5z2w453hvfm7jg3h13iq8jx7pb3f0bbnxl93k8sph")))

