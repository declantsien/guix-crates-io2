(define-module (crates-io bl ar blarf) #:use-module (crates-io))

(define-public crate-blarf-1.0.0 (c (n "blarf") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "17law75nbs7q30k19zhas0kvqykzjimmxwkgiki6a9f61m04148k")))

(define-public crate-blarf-1.0.1 (c (n "blarf") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "1dvh9qd74pq8wbln673hbhgi4mj66s8yvliqfs7k7vq6ba3y4cyd")))

(define-public crate-blarf-1.0.2 (c (n "blarf") (v "1.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "07vx10bdkzwh7bibp6p0hm34l1zf697pizhbajd6ccybvs021jrq")))

(define-public crate-blarf-1.0.3 (c (n "blarf") (v "1.0.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "03fpjq2liy1blhm8657i7hhcmchfy9fiq2p4q6chy92kirkpmf8n")))

(define-public crate-blarf-1.0.4 (c (n "blarf") (v "1.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "0cp8jg8bcc79lhp8ycw18yi2p1d49y8q65dmb89b46xngp8h8rjr")))

(define-public crate-blarf-1.0.5 (c (n "blarf") (v "1.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "1zpv6zbzim6jwbjw63vkff1nn94sq1kln8m424wicfvpklbqyfjx")))

(define-public crate-blarf-1.0.6 (c (n "blarf") (v "1.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "1vv7702lfw7gzib5gr9d9jmy8af50b7lm6h26by4shbqrl8wn002")))

(define-public crate-blarf-1.0.7 (c (n "blarf") (v "1.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "0rnxi9fkj1g9l0vf12jg9hc25fvk3cjy3capda5bqd8ckg6jwcc8")))

(define-public crate-blarf-1.0.8 (c (n "blarf") (v "1.0.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "0wwhbxaam7m7zxbwgl10d34ng8vh095kqxfhbw5hvbz5jz2gaa4f")))

