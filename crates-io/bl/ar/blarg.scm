(define-module (crates-io bl ar blarg) #:use-module (crates-io))

(define-public crate-blarg-0.1.0 (c (n "blarg") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "empty") (r "^0.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09yqi183xwb6wnp9lyh7w42d156ifv8wr6kcnwshcn9falzxx3ps")))

(define-public crate-blarg-0.2.0 (c (n "blarg") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "empty") (r "^0.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h2mxzhdkspb3ppl1b3b3850vpls1n8ng4v1fpsmgs1hxcc6sy0v")))

(define-public crate-blarg-0.2.1 (c (n "blarg") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "empty") (r "^0.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vvfkz6wbq9pc3bk7ydh2ag8m66r4y1cdfr6hqhka4v60syh6hgd") (f (quote (("unit_test"))))))

(define-public crate-blarg-0.2.2 (c (n "blarg") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "empty") (r "^0.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07z72x65fr4f3p31y0wacs3kwl2p30khc45jsd12pbndsy67634a") (f (quote (("unit_test")))) (y #t)))

(define-public crate-blarg-0.2.3 (c (n "blarg") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "empty") (r "^0.0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.15") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "187fgjww39819hawg5596i0g0cim04lgdwdixs6fnamwfiy7dnpm") (f (quote (("unit_test"))))))

(define-public crate-blarg-1.0.0 (c (n "blarg") (v "1.0.0") (d (list (d (n "blarg_builder") (r "^1.0.0") (d #t) (k 0)) (d (n "blarg_derive") (r "^1.0.0") (d #t) (k 0)))) (h "0p5g4bmc5k2lvzghbm143b0srgi4zkjr2nhpin06wccsf411gylx") (f (quote (("unit_test" "blarg_builder/unit_test")))) (y #t)))

(define-public crate-blarg-1.0.1 (c (n "blarg") (v "1.0.1") (d (list (d (n "blarg_builder") (r "^1.0.1") (d #t) (k 0)) (d (n "blarg_derive") (r "^1.0.1") (d #t) (k 0)))) (h "0h0bsgfbggwadcppblfw2w7s3azy1krfy4civhi8f7i2g8hks4ab") (f (quote (("unit_test" "blarg_builder/unit_test")))) (y #t)))

(define-public crate-blarg-1.0.2 (c (n "blarg") (v "1.0.2") (d (list (d (n "blarg_builder") (r "^1.0.2") (d #t) (k 0)) (d (n "blarg_derive") (r "^1.0.2") (d #t) (k 0)))) (h "0wk650ff4mhh3dpig5yqp9932pkwpawszcxv6186sq62wq4sp5zl") (f (quote (("unit_test" "blarg_builder/unit_test"))))))

(define-public crate-blarg-1.0.3 (c (n "blarg") (v "1.0.3") (d (list (d (n "blarg_builder") (r "^1.0.3") (d #t) (k 0)) (d (n "blarg_derive") (r "^1.0.3") (d #t) (k 0)))) (h "035cid68747szzxs7p1bqgzf2w966589m1fr302gm6zkjx3xa58g") (f (quote (("unit_test" "blarg_builder/unit_test"))))))

(define-public crate-blarg-1.0.4 (c (n "blarg") (v "1.0.4") (d (list (d (n "blarg_builder") (r "^1.0.4") (d #t) (k 0)) (d (n "blarg_derive") (r "^1.0.4") (d #t) (k 0)))) (h "1lhn768lblydrgaw6zm83356qwnq6shl9razgx4dxlmb0cqlvayy") (f (quote (("unit_test" "blarg_builder/unit_test") ("tracing_debug" "blarg_builder/tracing_debug"))))))

