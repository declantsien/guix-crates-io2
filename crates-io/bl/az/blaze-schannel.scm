(define-module (crates-io bl az blaze-schannel) #:use-module (crates-io))

(define-public crate-blaze-schannel-0.1.0 (c (n "blaze-schannel") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography" "Win32_Security_Authentication_Identity" "Win32_Security_Credentials" "Win32_System_Memory"))) (d #t) (k 0)))) (h "081dkasr5cqvh8959vglv7wh7x5r3z7mzwkcvan2r39cr5n0ax6v")))

