(define-module (crates-io bl az blaze-ssl) #:use-module (crates-io))

(define-public crate-blaze-ssl-0.1.0 (c (n "blaze-ssl") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pem") (r "^1.1.0") (d #t) (k 0)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1h2bvbgrr23basf5dy7ypdlwagysj86mpqilrr1magbipjjqqsh2")))

(define-public crate-blaze-ssl-0.1.1 (c (n "blaze-ssl") (v "0.1.1") (d (list (d (n "der") (r "^0.6.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pem") (r "^1.1.0") (d #t) (k 0)) (d (n "pkcs1") (r "^0.4.1") (d #t) (k 0)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "x509-cert") (r "^0.1.0") (d #t) (k 0)))) (h "026rwn5gsqn2wqx01a971klnqbwm7vzvywhql3l92gck35qxknmw")))

(define-public crate-blaze-ssl-0.1.2 (c (n "blaze-ssl") (v "0.1.2") (d (list (d (n "der") (r "^0.6.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pem") (r "^1.1.0") (d #t) (k 0)) (d (n "pkcs1") (r "^0.4.1") (d #t) (k 0)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "x509-cert") (r "^0.1.0") (d #t) (k 0)))) (h "006nc5iazd0cqcph2fq5wqkfm0akkbg5la9xwm3mbyzp8d5dkzc7")))

(define-public crate-blaze-ssl-0.1.3 (c (n "blaze-ssl") (v "0.1.3") (d (list (d (n "der") (r "^0.6.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pem") (r "^1.1.0") (d #t) (k 0)) (d (n "pkcs1") (r "^0.4.1") (d #t) (k 0)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "x509-cert") (r "^0.1.0") (d #t) (k 0)))) (h "0b6d1asv1yh5j8ih1vpvpqd7d4yizzl0266pny3blhzzri9vd7yw")))

