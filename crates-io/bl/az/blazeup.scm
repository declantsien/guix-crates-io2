(define-module (crates-io bl az blazeup) #:use-module (crates-io))

(define-public crate-blazeup-0.1.0 (c (n "blazeup") (v "0.1.0") (d (list (d (n "kv") (r "^0.22.0") (f (quote ("bincode-value"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0gsi8p65q3v9smi2n3201lq6vsp1vdwjf2pvrb3y66mp8pf6ln7r")))

(define-public crate-blazeup-0.1.1 (c (n "blazeup") (v "0.1.1") (d (list (d (n "kv") (r "^0.22.0") (f (quote ("bincode-value"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0gps6psmxfad5rmgyg7a9rs4hfh9dd1y285bg318cdz96rvqpl0g")))

(define-public crate-blazeup-0.1.2 (c (n "blazeup") (v "0.1.2") (d (list (d (n "kv") (r "^0.22.0") (f (quote ("bincode-value"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "08x7xbnv48bcl6fp3nxa10d950lcv6qyi0gqbl1mwzq15ig3nslw")))

(define-public crate-blazeup-0.1.3 (c (n "blazeup") (v "0.1.3") (d (list (d (n "kv") (r "^0.22.0") (f (quote ("bincode-value"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "wildmatch") (r "^2.1.0") (d #t) (k 0)))) (h "0lad9s8zpq4nzmzdmapaami9j7q3c41x6ysph62xgncmi58fpvqh")))

