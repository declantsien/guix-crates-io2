(define-module (crates-io bl az blazefuck) #:use-module (crates-io))

(define-public crate-blazefuck-1.0.0 (c (n "blazefuck") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.17") (f (quote ("derive"))) (d #t) (k 0)))) (h "129ik8dnl7grw8gj296x5r81j0p83h8fki35d97rzsbwzrnp6y1k")))

