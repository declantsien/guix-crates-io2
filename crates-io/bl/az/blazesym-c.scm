(define-module (crates-io bl az blazesym-c) #:use-module (crates-io))

(define-public crate-blazesym-c-0.0.0 (c (n "blazesym-c") (v "0.0.0") (h "19dfdyy7y056h6j087igxg26n4acmbplm4c0zs8v4hqij5zbdg8g") (y #t)))

(define-public crate-blazesym-c-0.1.0-alpha.0 (c (n "blazesym-c") (v "0.1.0-alpha.0") (d (list (d (n "blazesym") (r "=0.2.0-alpha.11") (f (quote ("apk" "demangle" "dwarf" "gsym"))) (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 2)) (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)) (d (n "which") (r "^6.0.0") (o #t) (d #t) (k 1)))) (h "0lanqf8klg2shycwnsp7ignv3h6wfz0sm02fy0ggr3pabfdxly2z") (f (quote (("generate-c-header" "cbindgen" "which") ("check-doc-snippets")))) (r "1.65")))

(define-public crate-blazesym-c-0.1.0-alpha.1 (c (n "blazesym-c") (v "0.1.0-alpha.1") (d (list (d (n "blazesym") (r "=0.2.0-alpha.11") (f (quote ("apk" "demangle" "dwarf" "gsym"))) (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 2)) (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.14") (f (quote ("trace"))) (k 2)) (d (n "which") (r "^6.0.0") (o #t) (d #t) (k 1)))) (h "08pjwsnx2ylind4p5j6p4xc7iy6iy2hx5pc2mim5mxqk5nv5pz90") (f (quote (("generate-c-header" "cbindgen" "which") ("check-doc-snippets")))) (r "1.65")))

