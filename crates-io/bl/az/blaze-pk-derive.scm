(define-module (crates-io bl az blaze-pk-derive) #:use-module (crates-io))

(define-public crate-blaze-pk-derive-0.1.0 (c (n "blaze-pk-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01br2hpl0xq2j3qaj36bykpgb12rap9clhfcn3dvxm1gm0ig3818")))

(define-public crate-blaze-pk-derive-0.1.1 (c (n "blaze-pk-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1c9h0161xqbrv8dgkxxj752ffyp8aaqbj7y5fif91pg1xfc4azc7")))

