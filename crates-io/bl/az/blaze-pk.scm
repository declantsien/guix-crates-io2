(define-module (crates-io bl az blaze-pk) #:use-module (crates-io))

(define-public crate-blaze-pk-0.1.0 (c (n "blaze-pk") (v "0.1.0") (h "00k59vsilwnv4jv66walagkmygly87k91q3d02dqhzx05ljyk550")))

(define-public crate-blaze-pk-0.1.1 (c (n "blaze-pk") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "1hywjf0zi7caaclmm1m2in5b9vcf3j6zbs5bfag2axfbp5nkg1v6")))

(define-public crate-blaze-pk-0.1.2 (c (n "blaze-pk") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "0fwz08h8gs5irzai4gzm7kzf064fkvsmpxc0g42drckxa279chk5")))

(define-public crate-blaze-pk-0.2.0 (c (n "blaze-pk") (v "0.2.0") (h "01k6iy9kca7zgah4yxqv03k5d06biqcsgmdwfqn3gxry4asd04j3")))

(define-public crate-blaze-pk-0.2.1 (c (n "blaze-pk") (v "0.2.1") (h "0bkq9m38m6rlwdkslbbh13fnyrk7xf2c5rimw9k2w5vkv2467r04")))

(define-public crate-blaze-pk-0.2.2 (c (n "blaze-pk") (v "0.2.2") (h "1j87x2w52kp1h30mibmnyyszqjfbxckv7gvr9dxr5r7h11sbgwj1")))

(define-public crate-blaze-pk-0.2.3 (c (n "blaze-pk") (v "0.2.3") (h "0jqjisc3xfdqv495fngzvnymga8pc9zn5qis0pm85dpx8x3mw4vm")))

(define-public crate-blaze-pk-0.2.4 (c (n "blaze-pk") (v "0.2.4") (h "04rjdrbbmvasqqzc19qv34hljlmfacndqrdgxpkqzrk23sklhyyd")))

(define-public crate-blaze-pk-0.2.5 (c (n "blaze-pk") (v "0.2.5") (h "09zvphi0b3xzg5wmv6npgimlpzs7mvy35h7p0g0h88ilzr3q0nw2")))

(define-public crate-blaze-pk-0.2.6 (c (n "blaze-pk") (v "0.2.6") (h "0bb3nj0frya3a3mbm0s934ziw9hr6drhq0abmnc2ar7ac1b124ii")))

(define-public crate-blaze-pk-0.2.7 (c (n "blaze-pk") (v "0.2.7") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1bi0bzzip5hm63dbqkwqn95i93y9aiznfny13dqng61mw2jgx0x7") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.2.8 (c (n "blaze-pk") (v "0.2.8") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1qx7wf7ljwrlp3d2685n2wk6zvqi4p27h6jp9bv7b9zk5xn8lva8") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.2.9 (c (n "blaze-pk") (v "0.2.9") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0m4j7f1zy3i23hcimpi0iy7gac3h5v8fgd52faxz35shx7ahwdyg") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.0 (c (n "blaze-pk") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1m6xa17m7l72j3phfy6g4p97lwdvb7n11qd4vp2anddny802hfda") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.1 (c (n "blaze-pk") (v "0.3.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0sgca2swvj5qs3lmb7gd1p4xxk98v3da3whhf712d9sm1w6liki0") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.2 (c (n "blaze-pk") (v "0.3.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ls41p1dbjyg6pxylpdcmpjvaz413whqafxn3i8lxyrny2vblkv0") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.3 (c (n "blaze-pk") (v "0.3.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "17jpsz1nhki5rz9pwqyvb3mwvrzp7diy74v3yzpdjxbbk31ggf21") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.4 (c (n "blaze-pk") (v "0.3.4") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "16yz7qcksssnp7hq6vz1rp02cmlh0z6d6iq37xdp0i6kkaqcxp60") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.5 (c (n "blaze-pk") (v "0.3.5") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "14p3ysqgwp8ndlhxdfrwypyrfn8ycp5haf194qi1q62gglzk7syw") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.6 (c (n "blaze-pk") (v "0.3.6") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ak6k06ahcg4czijwbcvf3winkjgnsm5yd0z7s2fbqs1qsxylzkw") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.7 (c (n "blaze-pk") (v "0.3.7") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10ah1887daxxli4nbxj2c0d8g0rsr4awhg7nrfrm4h6qxwqahd52") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.8 (c (n "blaze-pk") (v "0.3.8") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1629wyr4b1j9ybljzw5xpyqqsqjn85mnzj5pjpi8wbyxvzl73k7v") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.3.9 (c (n "blaze-pk") (v "0.3.9") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "17nybjk5qjk3jbskpq5nxc0vlngp22kq7g73a9hbz5yhyc72miym") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.0 (c (n "blaze-pk") (v "0.4.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0s0qkks3fnrqsyyzn0cmmjy863cxlmlxkgvnxfp8d6w724g3j99x") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.1 (c (n "blaze-pk") (v "0.4.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0na15amyffv59cg3c799g2jy02d9dc6fa4j34fb3sld5vgf8qvv8") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.2 (c (n "blaze-pk") (v "0.4.2") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12kjayz9lvd33zjahfr9qn93cxli7lcs6i06dxbhgs8b8lcdx7lk") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.3 (c (n "blaze-pk") (v "0.4.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1kd11rws050zn62a1r7gr04nfkpi00n5vksq8arfsfgwdq6c5zfz") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.4 (c (n "blaze-pk") (v "0.4.4") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1iv60vyx1invqdzkcla87i1pz5jdn1xp3wjpj4hpg0qnxhbnc89w") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.5 (c (n "blaze-pk") (v "0.4.5") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1s5pq1xnrr59pla8nazb0rp7w0z7l3h83f3xgsf8y286125xzxvn") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.6 (c (n "blaze-pk") (v "0.4.6") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1nqd47l73y50ppdaz2474ypivwawdy8bmwwjr7jqy1ir7nl7cg17") (f (quote (("default") ("async" "tokio"))))))

(define-public crate-blaze-pk-0.4.7 (c (n "blaze-pk") (v "0.4.7") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "18k6nyz2c663f99av05sw1ab718xqaj66pra6vfrcwik4zyqyb0c") (f (quote (("default" "blaze-ssl-async") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl-async" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.4.8 (c (n "blaze-pk") (v "0.4.8") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "169wnrr15w5g7dhf20jj4ifqysiijibb6k2gsviqzv5n95i6lwvy") (f (quote (("default" "blaze-ssl-async") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl-async" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.0 (c (n "blaze-pk") (v "0.5.0") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0a46lk164rnas813ajqhyhryxijfqr3jl8bww5sia88vh90vi7h1") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.1 (c (n "blaze-pk") (v "0.5.1") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0ihrqc6b33p98zjvd1301v3mrr0j8xjnhqphb90q1k11w7n03ga7") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.2 (c (n "blaze-pk") (v "0.5.2") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "03gpjqkx0nmjhix27zrqr99gj333xkd3kzfdxs0vgi387vqpk6a1") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.3 (c (n "blaze-pk") (v "0.5.3") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0nkvjkd9gdxcgrvkwaidm1501kzqi7mcj6z2zz51pni9pkh3l35s") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.4 (c (n "blaze-pk") (v "0.5.4") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0v2rj0idx1172jyhyxv552khh05bj37y3qk8q4f688gxlnbywrnz") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.5 (c (n "blaze-pk") (v "0.5.5") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "035nslmyzy28s84nchnbxl5a5v56fkrx107q7kvkmpwzijy2f6cd") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.5.6-alpha (c (n "blaze-pk") (v "0.5.6-alpha") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "11a2ndnkmh5q88lrhjyxqhfzddp1ybshdbhnk0p1vkzrfjfp6nsb") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha (c (n "blaze-pk") (v "0.6.0-alpha") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0ckixk45gswn9k93am9xzxrd8w62zhlvqpcn9z494w77fxjdb2hy") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-1 (c (n "blaze-pk") (v "0.6.0-alpha-1") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1h9a9slnrx7vi2di3n0zgy3xn915p3z2702n373wisdv9425myvl") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-2 (c (n "blaze-pk") (v "0.6.0-alpha-2") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ahczb4xrqzaxjr7slsgb0b0f769zmmh145gkg3aiqizfr7zxpxz") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-3 (c (n "blaze-pk") (v "0.6.0-alpha-3") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1d3ij15zzlp0vz1py0y495abjhrjljwjv3csvq93avs3yd4433aw") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-4 (c (n "blaze-pk") (v "0.6.0-alpha-4") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "16b8r838j0qf136vf8hph9iw24ia19f6v3gkyijkkck8i849nf0q") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-5 (c (n "blaze-pk") (v "0.6.0-alpha-5") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0dzc6zvwl00ni541srz377pxkmgqih7py6zhx3vd54w0cj7wy4dc") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0-alpha-6 (c (n "blaze-pk") (v "0.6.0-alpha-6") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0g60v493ahx0y9bdh0qxzzf814hdv350xi4yayqbs02ki60g2inz") (f (quote (("sync") ("default" "sync") ("async" "tokio")))) (s 2) (e (quote (("blaze-ssl" "async" "dep:blaze-ssl-async"))))))

(define-public crate-blaze-pk-0.6.0 (c (n "blaze-pk") (v "0.6.0") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "04mifz8v62j4lbpd1nmr31kany1h35ygda6v5a868x7gbm3xzis5") (f (quote (("sync") ("default" "blaze-ssl" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("blaze-ssl" "async" "dep:blaze-ssl-async") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.6.1-alpha (c (n "blaze-pk") (v "0.6.1-alpha") (d (list (d (n "actix-codec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0g31phwpaq942asczyq9a7zv377cag0l8m6ix9l9j7fv221cmva8") (f (quote (("sync") ("default" "blaze-ssl" "serde" "actix")))) (s 2) (e (quote (("serde" "dep:serde") ("blaze-ssl" "async" "dep:blaze-ssl-async") ("async" "dep:tokio") ("actix" "dep:actix-codec"))))))

(define-public crate-blaze-pk-0.6.1-alpha-2 (c (n "blaze-pk") (v "0.6.1-alpha-2") (d (list (d (n "actix-codec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0394h8hgcpf1h6jzi9l8ks3lffqd2lzn63a3x7ca0xlqbfr9ylcq") (f (quote (("sync") ("default" "blaze-ssl" "serde" "actix")))) (s 2) (e (quote (("serde" "dep:serde") ("blaze-ssl" "async" "dep:blaze-ssl-async") ("async" "dep:tokio") ("actix" "dep:actix-codec"))))))

(define-public crate-blaze-pk-0.6.1 (c (n "blaze-pk") (v "0.6.1") (d (list (d (n "actix-codec") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "1x46sj5sjhl1ala2q6yyw3zrqz4pj7m003mbak5bc12qsk8ala4n") (f (quote (("sync") ("default" "blaze-ssl" "serde" "actix")))) (s 2) (e (quote (("serde" "dep:serde") ("blaze-ssl" "async" "dep:blaze-ssl-async") ("async" "dep:tokio") ("actix" "dep:actix-codec"))))))

(define-public crate-blaze-pk-0.6.2 (c (n "blaze-pk") (v "0.6.2") (d (list (d (n "blaze-ssl-async") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0cnws1s8dacpnph39gwikfl5a4w4zxidqi8xkk32965nlylb5fyv") (f (quote (("sync") ("default" "blaze-ssl" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("blaze-ssl" "async" "dep:blaze-ssl-async") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.7.0-alpha (c (n "blaze-pk") (v "0.7.0-alpha") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "19gi3x0v8r32x35kgmgip7pa3aqyyrpyzq9q7q1x4bj6j8cyv2ad") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.7.0 (c (n "blaze-pk") (v "0.7.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "07cp8xkh2lfbca0kc7h89k4znrq22vlswk8knhz4nlpypj21fg22") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.0 (c (n "blaze-pk") (v "0.8.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "1m8cd8dyncxdvxhybhxpzjavnqwxg88x6g07gybgvrhp7f8lpy4g") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.1 (c (n "blaze-pk") (v "0.8.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (k 0)))) (h "0y93k2vgbichycnql277si95zc1xlm0mvwl0viphfjd2jrvf0268") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.2 (c (n "blaze-pk") (v "0.8.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0d665qxip15aj90v6cadjs2a12fj7ljqlihc0anadkpi285z26xl") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.3 (c (n "blaze-pk") (v "0.8.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "043zbx4wgch72dk6nrisiryd9q8gh1rvh4x503ysx0d69z9fznym") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.4 (c (n "blaze-pk") (v "0.8.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1qm8paclk4vadx2z46sj7dmnbx7vjsbda91z0inn4sq9mirmvp96") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.5 (c (n "blaze-pk") (v "0.8.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "17dp94mxmipv5c8xi5716027x8c25k7rjmjrlbr1n27ira791ndf") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.6 (c (n "blaze-pk") (v "0.8.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0m443qxm6mbddx3rxxfl7wamnn7dhcy2ihk1scpbi1z6mkbbzdnv") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.8.7 (c (n "blaze-pk") (v "0.8.7") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0dvs004bn78bk0ifbx4gpb2k2fij3j4j6yahg80sqjfzmslrzmyf") (f (quote (("sync") ("default" "async" "serde")))) (s 2) (e (quote (("serde" "dep:serde") ("async" "dep:tokio"))))))

(define-public crate-blaze-pk-0.9.0 (c (n "blaze-pk") (v "0.9.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0l960klr6irgw31dnpninam52w31g04dw4fv9517v5gxl3bkyyfi") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.0.0 (c (n "blaze-pk") (v "1.0.0") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1rb76bi0nm89a645j50ib04avlkr4l068rji3fv67my7aznhbmhz") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.0.1 (c (n "blaze-pk") (v "1.0.1") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1fgqyhkg4hjqfw56w3wyxkrzin1sm73hs25r92a1k3andg2pvs9q") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.0.2 (c (n "blaze-pk") (v "1.0.2") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1grv667z27ynhs0hay4d4vygl90vwv47gn24n1ps51cbiim4i1gn") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.0.3 (c (n "blaze-pk") (v "1.0.3") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1a1y28vfspzi7yd8v4j7afwdrx7qwl39rpbqx7730sl5d131qaqa") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.1.0 (c (n "blaze-pk") (v "1.1.0") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0bcxfgfgjphfiyiqwd7irwgbakc00a2v79xqii4v4hmis5adgpnb") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.1.1 (c (n "blaze-pk") (v "1.1.1") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1b5qr7pv6gv3hr8bvdxx3y52k6vf2dlc4cmsgh8z2xs7vg2xpgg3") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.2.0 (c (n "blaze-pk") (v "1.2.0") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1pap21v43m5zlqs5iw580rm7xj5kkrl8snvxw082rgqakmcjyim5") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-blaze-pk-1.3.0 (c (n "blaze-pk") (v "1.3.0") (d (list (d (n "blaze-pk-derive") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "09zzp51cg9wkkgxqwcbirnvlhxja9lm0s2j6sip09x26f9gr9zya") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

