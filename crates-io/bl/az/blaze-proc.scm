(define-module (crates-io bl az blaze-proc) #:use-module (crates-io))

(define-public crate-blaze-proc-0.0.1 (c (n "blaze-proc") (v "0.0.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "elor") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cbkmgw6ksnpl2l0smzzmsjffy8gpqj3ikq85wzpq6vg6n9x7rwz")))

(define-public crate-blaze-proc-0.1.0 (c (n "blaze-proc") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "elor") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bjippdl3w21rk1mn5hz1bmkflshm11109xydwkia5fyjnzqj5f7")))

(define-public crate-blaze-proc-0.1.1 (c (n "blaze-proc") (v "0.1.1") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "elor") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fw5yswdhd30bx7xbp13b55pd3lf2fxqrccnqbvnkn7small4w1i")))

(define-public crate-blaze-proc-1.0.0 (c (n "blaze-proc") (v "1.0.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "elor") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a80v6n8lw6g69aakrwmf83ig62rfxrw24pknqldvw727wcdvr18")))

