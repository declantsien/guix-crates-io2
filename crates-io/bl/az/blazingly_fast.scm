(define-module (crates-io bl az blazingly_fast) #:use-module (crates-io))

(define-public crate-blazingly_fast-1.0.0 (c (n "blazingly_fast") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0920qq4c6af8wc5f1lc4yy2yf4jjdz2j8p3bl6ym8lgb2y6dszv9")))

(define-public crate-blazingly_fast-1.0.1 (c (n "blazingly_fast") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bg92l0h8fw2ycccbavi7wm2zc9mw1172sxs2wy03vf34031qal1")))

(define-public crate-blazingly_fast-1.0.2 (c (n "blazingly_fast") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bzfihfz3g1fzci8mfzsmkhn5w19c026i39qxssxff5ca8b5w7ka")))

