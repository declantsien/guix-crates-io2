(define-module (crates-io bl it blitz-path) #:use-module (crates-io))

(define-public crate-blitz-path-0.1.0 (c (n "blitz-path") (v "0.1.0") (d (list (d (n "movingai") (r "^1.1") (d #t) (k 0)))) (h "17vpavwx2j1y87gn37p1ddrrpgp38vjpxf84rxkr096iiv2nplmd")))

(define-public crate-blitz-path-0.1.1 (c (n "blitz-path") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "movingai") (r "^1.1") (d #t) (k 0)))) (h "1rs288p73pqgg8g9469lfp9h470zabr9zyaf5pxjqhx8y64kj43g")))

(define-public crate-blitz-path-0.2.0 (c (n "blitz-path") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "movingai") (r "^1.1") (d #t) (k 0)))) (h "0qlyxkl99hnncnjgzgwg5z5incml6iianxlhk9d3ggih3sr9ss2z")))

