(define-module (crates-io bl it blithaven) #:use-module (crates-io))

(define-public crate-blithaven-0.1.0 (c (n "blithaven") (v "0.1.0") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "1r4pjhdbjavviry91nl8pnf4zx4v0ywms8ny4ggkqw5qagfzd8n7")))

(define-public crate-blithaven-0.1.1 (c (n "blithaven") (v "0.1.1") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "13rdm6kyi84lagc1k6awmxbmkfy5cck0iwx6anz8gwjf6fn1qnnq")))

(define-public crate-blithaven-0.2.0 (c (n "blithaven") (v "0.2.0") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "01jvsn3wbq38f2xhan5m6iibp7z7rbgjf2k8767c781nk8lkkrrj")))

(define-public crate-blithaven-0.2.1 (c (n "blithaven") (v "0.2.1") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "03ir888kwb5asgnfpjsn5bp113ygfswa7mi9kgbvisarjcnvrh13")))

(define-public crate-blithaven-0.3.1 (c (n "blithaven") (v "0.3.1") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04qn2yw1y7aycllsp3g49y3pk0b9fkdw0wdpb6japrx56j9b1zib")))

(define-public crate-blithaven-0.3.2 (c (n "blithaven") (v "0.3.2") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mv9q88qqchifg1kamxm3lxlqkz3nld23sfmi06jllddr4kjn6ac")))

(define-public crate-blithaven-0.3.3 (c (n "blithaven") (v "0.3.3") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "specs") (r "^0.19.0") (d #t) (k 0)))) (h "1hw65vx40wc4000w7vrw40ibvksq8dhlp3pmjbcl76mfmmhgdg83")))

(define-public crate-blithaven-0.4.0 (c (n "blithaven") (v "0.4.0") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wi5l9kli3hsyrpvs2px0rqv0k9y3im48xp5awrsg8ijpjfykfgv")))

(define-public crate-blithaven-0.4.1 (c (n "blithaven") (v "0.4.1") (d (list (d (n "earcutr") (r "^0.4.2") (d #t) (k 0)) (d (n "glium") (r "^0.32.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15dnh0f89xdf14sm8klqpmsx9km3p78xgn8g4zdcqfpwb6q0ilcg")))

