(define-module (crates-io bl it blitsort-sys) #:use-module (crates-io))

(define-public crate-blitsort-sys-0.1.0 (c (n "blitsort-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1acdm5k9m0hszh1nm197w3cwy3w13wkircpa6ym64b5fa2lfkfww")))

