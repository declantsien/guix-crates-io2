(define-module (crates-io bl it blitzkrieg) #:use-module (crates-io))

(define-public crate-blitzkrieg-0.1.0 (c (n "blitzkrieg") (v "0.1.0") (h "1z2lslbxyvd8d176dw3sq2sa375q8mf496i26yw66c9hlkciqzph")))

(define-public crate-blitzkrieg-0.1.1 (c (n "blitzkrieg") (v "0.1.1") (h "0m2lynx47z2h7rpkic29q6rambb5sv0wcs07r08rnjmz862664ic")))

(define-public crate-blitzkrieg-0.1.2 (c (n "blitzkrieg") (v "0.1.2") (h "0avjszi45f12l7qb3j5hh00m8wqd7zp1nx6my10ij5fv4daaf6iv")))

(define-public crate-blitzkrieg-0.1.3 (c (n "blitzkrieg") (v "0.1.3") (h "19r3v81scpzi9ddqj67j9zrwjn35311rm1kspwqqkvinfhnvh1vx")))

(define-public crate-blitzkrieg-0.1.4 (c (n "blitzkrieg") (v "0.1.4") (h "1s83rzavpl34k6jg63n17n27klss33vaxavryxz143f6kpa5xffn")))

(define-public crate-blitzkrieg-0.1.5 (c (n "blitzkrieg") (v "0.1.5") (h "1x4h22izxb04rd7843n8jx6mz5ralq5q121cvndjqkmccfvlb3sn")))

(define-public crate-blitzkrieg-0.1.6 (c (n "blitzkrieg") (v "0.1.6") (h "1xvcb2havvr4wxc87h5g4q56gg3lg0bdzfkgsd0vv2gmp10i6r46")))

(define-public crate-blitzkrieg-0.1.7 (c (n "blitzkrieg") (v "0.1.7") (h "1awrif8nxhxihhykw2gpmiwpmh8vpbb9hfs1pxj4p0y6smk9234l")))

(define-public crate-blitzkrieg-0.1.8 (c (n "blitzkrieg") (v "0.1.8") (h "025065ckg8pb5dlp0ncspv2k9jilkbzhjx5nyjvq505818zh2cnf")))

(define-public crate-blitzkrieg-0.1.9 (c (n "blitzkrieg") (v "0.1.9") (h "1y7iv28ajgvgj24j18708n6ab1vy1gvwzlphyv2q0q03fh14pal8")))

(define-public crate-blitzkrieg-0.2.0 (c (n "blitzkrieg") (v "0.2.0") (h "03d5m99awgc1rn5cgzbdwvcq1ivzln8g6dkhy68gc8w5jh6j65hp")))

(define-public crate-blitzkrieg-0.2.1 (c (n "blitzkrieg") (v "0.2.1") (h "0xscj3kmqjy5f217m6riawhrr8ld2gd9r88jnyzfimjafvnp73xx")))

(define-public crate-blitzkrieg-0.2.2 (c (n "blitzkrieg") (v "0.2.2") (h "143vmvzwf6wqk1144rdfa2qsllzvjkcnp1hm6j6wyhxv71n3iy1v")))

(define-public crate-blitzkrieg-0.2.3 (c (n "blitzkrieg") (v "0.2.3") (h "15jikbk3xc7gy6jnj6hqgs1iv5yx8rn3z8l2m5hscz7f3kz7s4zf")))

