(define-module (crates-io bl it blitter) #:use-module (crates-io))

(define-public crate-blitter-0.1.0 (c (n "blitter") (v "0.1.0") (h "1wmx3rxmb8j7cgpiyk136abv4mnk6p7mf7q4iki63grw1mr1pn31") (y #t)))

(define-public crate-blitter-0.2.0 (c (n "blitter") (v "0.2.0") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 2)))) (h "1yghq9rpcxamay5jlz6i78j4bfn1x68rn25qlbmhfffdf8nfxv1g") (y #t)))

(define-public crate-blitter-0.3.0 (c (n "blitter") (v "0.3.0") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 2)) (d (n "png") (r "^0.15.3") (o #t) (d #t) (k 0)))) (h "1r05fhivbwc8ajdg2kdh5drm220lvvr4gqykmlivss5662qgg75j") (f (quote (("png-decode" "png"))))))

(define-public crate-blitter-0.4.0 (c (n "blitter") (v "0.4.0") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 2)) (d (n "png") (r "^0.15.3") (o #t) (d #t) (k 0)))) (h "0649z4fzhpkhzdvlzayjvnxf3dw7w735pydvnxq81wqp88xybpd8") (f (quote (("png-decode" "png"))))))

(define-public crate-blitter-0.5.0 (c (n "blitter") (v "0.5.0") (d (list (d (n "minifb") (r "^0.15.3") (d #t) (k 2)) (d (n "png") (r "^0.15.3") (o #t) (d #t) (k 0)))) (h "1dmrf0lp60dd3njvgcx8cp74q1s7rwhvzdmcjg1i90k7yxxrnqbc") (f (quote (("png-decode" "png"))))))

(define-public crate-blitter-0.6.0 (c (n "blitter") (v "0.6.0") (d (list (d (n "minifb") (r "^0.15.3") (d #t) (k 2)) (d (n "png") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "1qpcybgg947x6b2qwzibbqns3yx6mgpbcs15kfn0jzm5y3vvr94i") (f (quote (("png-decode" "png"))))))

(define-public crate-blitter-0.6.1 (c (n "blitter") (v "0.6.1") (d (list (d (n "minifb") (r "^0.15.3") (d #t) (k 2)) (d (n "png") (r "^0.16.1") (o #t) (d #t) (k 0)))) (h "1h02q1svmk2pmvznx9skxf7y11vs0kqr7vjra4igkr6xg6j704hi") (f (quote (("png-decode" "png"))))))

