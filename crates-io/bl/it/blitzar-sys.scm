(define-module (crates-io bl it blitzar-sys) #:use-module (crates-io))

(define-public crate-blitzar-sys-0.1.0 (c (n "blitzar-sys") (v "0.1.0") (h "0i7ykvpn7m8zyaqq9favnjag3s6agnnfvhzd197mr2vwgb895j9r") (y #t)))

(define-public crate-blitzar-sys-0.0.2 (c (n "blitzar-sys") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "022lm42irqcnbk888d7rv5n8cwyvrrs5lmg1bgnsfc0r8dxviqp7") (y #t) (r "1.71.1")))

(define-public crate-blitzar-sys-0.2.0 (c (n "blitzar-sys") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1znkl7wvdyczq5izb43mm55w7y0gx6gq7mbg2k8cdq3v9j5cgfp0") (r "1.71.1")))

(define-public crate-blitzar-sys-0.2.1 (c (n "blitzar-sys") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "156y97v0p1xy9i4qb5nm5fd28rrc5z6baa6jaw1lnhl1c0q79ljq") (y #t) (r "1.71.1")))

(define-public crate-blitzar-sys-0.2.2 (c (n "blitzar-sys") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0cp8mhy3myksnc0plk1428sgq3fplcvw5vfzvwq3wqvrwn7rn5yz") (r "1.71.1")))

(define-public crate-blitzar-sys-0.2.3 (c (n "blitzar-sys") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0bj4vr1dxj6qfikm1jwbj2m6hf9qhydxhsji7lsz8a53fd940l10")))

(define-public crate-blitzar-sys-0.2.4 (c (n "blitzar-sys") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0nzvw652f353hlwvhhd8dgq5nw5rva2wg5wzzmvwjlqac4p0fxb4")))

(define-public crate-blitzar-sys-0.3.0 (c (n "blitzar-sys") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0hi31dr4i80j6p1nk6mgdpsq9k0wb7lv10larqafnmxaflazf9qv")))

(define-public crate-blitzar-sys-0.4.0 (c (n "blitzar-sys") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0s9hagpnijd4l8jgh83vwrff3gdd7f4gvawwb7bh100bdgg13rrj")))

(define-public crate-blitzar-sys-0.4.1 (c (n "blitzar-sys") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0x12h72zr9b1fpfr4zf1y9843ndnfbw3ra0p41s2pgihxcg9frls")))

(define-public crate-blitzar-sys-0.5.0 (c (n "blitzar-sys") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1a0yqbjfyax5mmhy2lw7pr8rd91h9a288jz1w9sn8pw0dwkdbpmp")))

(define-public crate-blitzar-sys-0.5.1 (c (n "blitzar-sys") (v "0.5.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1bqf4im7k87w733hzrxn8vsglj5rqpmba0nlkz6nkshs494my5rf")))

(define-public crate-blitzar-sys-0.6.0 (c (n "blitzar-sys") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1c69h9gh1mc4kwvhjhrn6cn99r833qjkx2b7fax6cw6zdcxvfcjq")))

(define-public crate-blitzar-sys-1.0.0 (c (n "blitzar-sys") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1rid47g7q2jfz5sdjgcvi39pzz8gd2bmi0vkkycismy3syksa907")))

(define-public crate-blitzar-sys-1.1.0 (c (n "blitzar-sys") (v "1.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1m3wna91pqc17w5jk832im191zhacrk626xc5snyzm459k0dvi55")))

(define-public crate-blitzar-sys-1.2.0 (c (n "blitzar-sys") (v "1.2.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1nzzbd2ws9isy6f8azx6627mhb506acjpg9q5qmvdmhih4p63lp6")))

(define-public crate-blitzar-sys-1.3.0 (c (n "blitzar-sys") (v "1.3.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0h9885969f3kkn3z30x77dl7j35ca8hra35gr0dc1qfi38fmc75d")))

(define-public crate-blitzar-sys-1.3.1 (c (n "blitzar-sys") (v "1.3.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0bym2c2sz2wrg2js31jgaqwwfysmxb4zh999zpccf3fpj6p72xjw")))

(define-public crate-blitzar-sys-1.3.2 (c (n "blitzar-sys") (v "1.3.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0xww190dvxn4ksv874xq1iic8v9i03kniqy52ibavx5m9qls9n9q")))

(define-public crate-blitzar-sys-1.3.3 (c (n "blitzar-sys") (v "1.3.3") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1xhccnn06i70lfa0wcyikp7wkrnrpsa2dqfa7rq295700s6v6r7i")))

(define-public crate-blitzar-sys-1.3.4 (c (n "blitzar-sys") (v "1.3.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0qmzx9ffnmczzy4xmz66lk9vd2j49470kkz9rv3pmf0j8mx1l6sp")))

(define-public crate-blitzar-sys-1.4.0 (c (n "blitzar-sys") (v "1.4.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0fb2xd2aw58g9fgavpbvrqjiwcppv97nnm9yi2lw5ha4lvbp90s4")))

(define-public crate-blitzar-sys-1.4.1 (c (n "blitzar-sys") (v "1.4.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0spf4yd7rz18zlygynqw90cnnv0nhaglcip0ymlijbiyzvgzy3i6")))

(define-public crate-blitzar-sys-1.5.0 (c (n "blitzar-sys") (v "1.5.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0hqnvycr2alykj7ciip326x4bsmy0dd67fsi693qlf7f4hfh33m4")))

(define-public crate-blitzar-sys-1.6.0 (c (n "blitzar-sys") (v "1.6.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1wvsnjvsg94vk1236xkzmkhbad1m6dpz4qmb45nyy5jm96d52wg0")))

(define-public crate-blitzar-sys-1.7.0 (c (n "blitzar-sys") (v "1.7.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "14qqbi9yfflb221rgcgyp9ahnf10yj6ckjk8a9rffy77yzf088wk")))

(define-public crate-blitzar-sys-1.8.0 (c (n "blitzar-sys") (v "1.8.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1fvxmymmdsqb1m7pwnmh9anpc662vsph3d7353gac6m2lm9lymjj")))

(define-public crate-blitzar-sys-1.9.0 (c (n "blitzar-sys") (v "1.9.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0cdfpf1ydbprffrky52dxn42b6y5i4hji83rxrm6jijnl1ylwf8f")))

(define-public crate-blitzar-sys-1.10.0 (c (n "blitzar-sys") (v "1.10.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "00lh7hkzayis4bs8ldaaf5jnjnyhjrmv7kgdm8vdg75vwb9zj2zd")))

(define-public crate-blitzar-sys-1.11.0 (c (n "blitzar-sys") (v "1.11.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0kdm5drclqjmxa2fdvxqg23g8rkik2h2ijxlzlnnjp5lxryrvk36")))

(define-public crate-blitzar-sys-1.12.0 (c (n "blitzar-sys") (v "1.12.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0dfmi50fg3iksgsssgbgn6jqcic47ipvz497blslzvyb0ig25zzb")))

(define-public crate-blitzar-sys-1.13.0 (c (n "blitzar-sys") (v "1.13.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0yj6hk190pjv37ggngb9y236x0rggl9v2cnziqd0g5xwn39531di")))

(define-public crate-blitzar-sys-1.14.0 (c (n "blitzar-sys") (v "1.14.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1jm7hh8b4zlgv2a45lhz5h9qmhwbkkgk5xx5ary0ywxxj4lm51p7")))

(define-public crate-blitzar-sys-1.15.0 (c (n "blitzar-sys") (v "1.15.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "13hsdy5b14fhmq2qwxrdg8sq7279hpz70grdn3hz453phg8jmdjj")))

(define-public crate-blitzar-sys-1.15.1 (c (n "blitzar-sys") (v "1.15.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "08bbfcvlb0hsadvhrbrpjr9456skid11xk5ii4snlvmr7mkw3vdj")))

(define-public crate-blitzar-sys-1.16.0 (c (n "blitzar-sys") (v "1.16.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "159qqxsc0kbqfhfi073q4smprwx4msmi6729y2pq44dpl36xribf")))

(define-public crate-blitzar-sys-1.17.0 (c (n "blitzar-sys") (v "1.17.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "05w50msb2s0ng29i529nq0nlh0smccgbpdp7p2ga8q8l7n5qpaik")))

(define-public crate-blitzar-sys-1.17.1 (c (n "blitzar-sys") (v "1.17.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0a4fcb4r4rjmrhphd5s8xvcxvnmg5n0zzqy04hqb2h3qzhrs9xc7")))

(define-public crate-blitzar-sys-1.18.0 (c (n "blitzar-sys") (v "1.18.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1wd48n6m8hrjp6867mjj6wyga5kc4552vc7y3227jni51f31vaz7")))

(define-public crate-blitzar-sys-1.19.0 (c (n "blitzar-sys") (v "1.19.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "08kgy1phsy65srgmf4dg70ipysy80sihyqdxn5q90jpnisq5x5kj")))

(define-public crate-blitzar-sys-1.20.0 (c (n "blitzar-sys") (v "1.20.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0pn917b3x4zz67p8icgk1z00mncwbilblfqs40lmlp3l8wgmhj4q")))

(define-public crate-blitzar-sys-1.21.0 (c (n "blitzar-sys") (v "1.21.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0hz32zl4hkbk8qy8g3rbrswymsdgh8n7hpb9s3i43sk5kagi3if5")))

(define-public crate-blitzar-sys-1.22.0 (c (n "blitzar-sys") (v "1.22.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "06fkiyjkm2mfbdb80xa5mgjjhwgbg9inqs7vfi65xf1n163919yd")))

(define-public crate-blitzar-sys-1.23.0 (c (n "blitzar-sys") (v "1.23.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1qx58lwwq9878nafxqzrx1w86xy2f00z01izrb0lvn17i0pch1cq")))

(define-public crate-blitzar-sys-1.24.0 (c (n "blitzar-sys") (v "1.24.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "07jlarav6y03hjvd5s1x8jg42919hlimrgvkg5bdaggviygz4wmv")))

(define-public crate-blitzar-sys-1.25.0 (c (n "blitzar-sys") (v "1.25.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0zpnk667n313z833dhvqhcbv9d8dsknhf6rl1r9jjf94010v1dab")))

(define-public crate-blitzar-sys-1.26.0 (c (n "blitzar-sys") (v "1.26.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1pmdqb7hz2cc619bb9lbyn07s4pbln1v5kxf24if4w0icynhn612")))

(define-public crate-blitzar-sys-1.27.0 (c (n "blitzar-sys") (v "1.27.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1y8l4fgl6i2s31rkf8ldppn7nv8aza9ds03ydh7vx4d5q483kn4c")))

(define-public crate-blitzar-sys-1.28.0 (c (n "blitzar-sys") (v "1.28.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "09phq18xk47dnc8in9cvmpyg5wczzxd2fgk3vs7ddxyj5nnf5z65")))

(define-public crate-blitzar-sys-1.29.0 (c (n "blitzar-sys") (v "1.29.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "08agf3f3hihskk6hp081kp2k2m53i08g2vf4052pyavn3pz9sl9f")))

(define-public crate-blitzar-sys-1.30.0 (c (n "blitzar-sys") (v "1.30.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1lnwbi9navq3r6bz45q563q8d3ckiqmf2y92k851shhnar486rpb")))

(define-public crate-blitzar-sys-1.31.0 (c (n "blitzar-sys") (v "1.31.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "10ws0hpwscqiz73s6wv77ihdq7dinwn89kynkr586rwzmcb114ik")))

(define-public crate-blitzar-sys-1.31.1 (c (n "blitzar-sys") (v "1.31.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1zr55hj7z7a1rdd03i1jq7qwd5mxi2jh0zh54hi8j6azv1ph2a6p")))

(define-public crate-blitzar-sys-1.32.0 (c (n "blitzar-sys") (v "1.32.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "16d40fdzr7bqv1ih059cm1m81bjgkig3p3y2r05pscd13n7din8f")))

(define-public crate-blitzar-sys-1.33.0 (c (n "blitzar-sys") (v "1.33.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0gjxk4fm90n0yhlnbjzs83kf2wimlwlla3k6dj92jvwlspv7yq7v")))

(define-public crate-blitzar-sys-1.34.0 (c (n "blitzar-sys") (v "1.34.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1vm0jlhhh25i8p1s6ifbz7fn0kygklpcg3gs8jnqiqx17cdm100w")))

(define-public crate-blitzar-sys-1.35.0 (c (n "blitzar-sys") (v "1.35.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1h78ik0alxllgw5jr5dpizc6db8vvfyv5gy13yrmfmd82wcds5fw")))

(define-public crate-blitzar-sys-1.36.0 (c (n "blitzar-sys") (v "1.36.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "028q7qiry1lvlfi6k5z3cymzsfj5s5h123z77n4h7mvfrgd0ss4y")))

(define-public crate-blitzar-sys-1.37.0 (c (n "blitzar-sys") (v "1.37.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "05lacbl1criw0mxvx89f72r7350ic3y4jfa83l5vshzcl4mqisd7")))

(define-public crate-blitzar-sys-1.37.1 (c (n "blitzar-sys") (v "1.37.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0y0gb1vrcqj5pa8d746133iwkp7rm7y2pw8c4146nfik5g8rgsnq")))

(define-public crate-blitzar-sys-1.38.0 (c (n "blitzar-sys") (v "1.38.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0b3hs9ly08026avzrg3k6jmniag8a6jaz7xfk1rdn0bscln7jvyy")))

(define-public crate-blitzar-sys-1.39.0 (c (n "blitzar-sys") (v "1.39.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0vxsskcrsp0rjgg0j834swcadc80wndmpms9hqyakzwnyxc0zhfs")))

(define-public crate-blitzar-sys-1.40.0 (c (n "blitzar-sys") (v "1.40.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "116brwmvji7k3g4mbllwjr9m33dpqfrqckimzqda6isnnrgiz45i")))

(define-public crate-blitzar-sys-1.41.0 (c (n "blitzar-sys") (v "1.41.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "011ikcky1skxvxi8q8l27p90c0a9mpgbpcnai9sm37v7ccfcjhn6")))

(define-public crate-blitzar-sys-1.42.0 (c (n "blitzar-sys") (v "1.42.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1cmrx3fgzgyhysc1cpbf1pyzc712c4in6vxydw7cdv5gbzr8qa4y")))

(define-public crate-blitzar-sys-1.43.0 (c (n "blitzar-sys") (v "1.43.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1lwsm6gmnf62vk4cijs232yby7lzkqy0yrwvis4ilz9ffhpm795m")))

(define-public crate-blitzar-sys-1.44.0 (c (n "blitzar-sys") (v "1.44.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0fphklk1vfwdr2d5i0s9gbaj82bvwvz1vfd5lzcj2f35a4qgxllh")))

(define-public crate-blitzar-sys-1.45.0 (c (n "blitzar-sys") (v "1.45.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0wgfad1zida8x3y7kg4am09sgawgm0kj0aqx3xrkl2x1byjcx8fm")))

(define-public crate-blitzar-sys-1.46.0 (c (n "blitzar-sys") (v "1.46.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0bw848zmfxl9hb2yf36yhgfc445g9zmqx4mjp0rfyaa684fycg8x")))

(define-public crate-blitzar-sys-1.47.0 (c (n "blitzar-sys") (v "1.47.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "01fkblkmq544r4f5i6aqd9n4npvc5nps675r62s4j0r2xs4m1rj0")))

(define-public crate-blitzar-sys-1.48.0 (c (n "blitzar-sys") (v "1.48.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "11fjll2pbmyqc7d1gnm7k7wgh0nd5ph8ixnjrfsrgkqs94kwmncx")))

(define-public crate-blitzar-sys-1.49.0 (c (n "blitzar-sys") (v "1.49.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1x68ippp90vlrjqbnw3mwvg26hwzidh2qz023vwm535annsxjphi")))

(define-public crate-blitzar-sys-1.49.1 (c (n "blitzar-sys") (v "1.49.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "00q30r6pp2qva6lc0bldi7w19d1pdq6033wcjyk1rwvc2jsz7227")))

(define-public crate-blitzar-sys-1.50.0 (c (n "blitzar-sys") (v "1.50.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "146bwkkpaxic1fqf0r2nagfy01590awmn2b68fsachg7zll01xc7")))

(define-public crate-blitzar-sys-1.51.0 (c (n "blitzar-sys") (v "1.51.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0hinjlwwxs48xxjy3inwnsfvvsx68299dh86bkpwvrgxpgm1b62c")))

(define-public crate-blitzar-sys-1.52.0 (c (n "blitzar-sys") (v "1.52.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1rds32qjivklxazqdh7la89jvwjs9b7i7n981d2a6azl2da8bahi")))

(define-public crate-blitzar-sys-1.53.0 (c (n "blitzar-sys") (v "1.53.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1ql6x6c6i20hqhjcgp9m96mddml2mgzy4m5f4fz4dga92c2kwylp")))

(define-public crate-blitzar-sys-1.54.0 (c (n "blitzar-sys") (v "1.54.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "1g5hrw664fx8gs8fpp5786fs09w00v11d6l7gw3mzk964ikm3vvj")))

(define-public crate-blitzar-sys-1.55.0 (c (n "blitzar-sys") (v "1.55.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "0lg529f896c9cdbd86m3r9g3y7a0k0hc4hrgir8r7m5b018rv32z")))

(define-public crate-blitzar-sys-1.56.0 (c (n "blitzar-sys") (v "1.56.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "rustls-tls"))) (k 1)))) (h "14rc4hjwq4pjrnknfzmccx7020h1dgzgyv32ry5l189j500810dy")))

