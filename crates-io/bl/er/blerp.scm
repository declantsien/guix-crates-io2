(define-module (crates-io bl er blerp) #:use-module (crates-io))

(define-public crate-blerp-0.1.0 (c (n "blerp") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b10j846ygj2hj9k66y9c7jzycjc94lh5rdffx5bii44312krdy3")))

