(define-module (crates-io bl im blimp) #:use-module (crates-io))

(define-public crate-blimp-0.1.0 (c (n "blimp") (v "0.1.0") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1dyn3233qk5g1kbrh2qhy0s51wlp6yrgvdmmr1fas1ki2ghkld9j")))

(define-public crate-blimp-0.1.1 (c (n "blimp") (v "0.1.1") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1p2naagvcjcwy9xcklf499wwlhq0h9i0dygvcmdll6s9f3rgqz86")))

(define-public crate-blimp-0.1.2 (c (n "blimp") (v "0.1.2") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0m8hp299ggishcy5x320hhm9jlc5xw2443f4hnvh4l0fdw6yzp0f")))

(define-public crate-blimp-0.1.3 (c (n "blimp") (v "0.1.3") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)))) (h "0kyx5lslyn7s3l4xpnph27l8avkk115b8xsvrasd96ynil59hm3b")))

(define-public crate-blimp-0.1.4 (c (n "blimp") (v "0.1.4") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)))) (h "0br0hsyc6h0h21hszfbb2mdh4fgc2dq2a6b5nar18lg2qkjm308y")))

(define-public crate-blimp-0.1.5 (c (n "blimp") (v "0.1.5") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)))) (h "0hf5fz5bg5hcd6cd6smm7fa1a4pz2nyvzbxjf819k7a3219vw3gw")))

(define-public crate-blimp-0.1.6 (c (n "blimp") (v "0.1.6") (d (list (d (n "config") (r "^0.13.2") (f (quote ("toml"))) (k 0)))) (h "1qcamy71blw512jvrgj9phyx7xgkdirizgkwn1zxcjfi9nvy75g0")))

