(define-module (crates-io bl #{60}# bl602-pac) #:use-module (crates-io))

(define-public crate-bl602-pac-0.0.0 (c (n "bl602-pac") (v "0.0.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1xj1d1z3fmbg6y1yy2cj8a6vbp10xaklffzg8ara9affmw12dgfk") (f (quote (("rt" "riscv-rt"))))))

