(define-module (crates-io bl #{60}# bl602-sdk) #:use-module (crates-io))

(define-public crate-bl602-sdk-0.0.1 (c (n "bl602-sdk") (v "0.0.1") (d (list (d (n "bl602-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "1nmb1gkvz656fgjjj8k7gzlsq7hdg8z0yd29y3kzh090bi09f29f") (f (quote (("default"))))))

(define-public crate-bl602-sdk-0.0.2 (c (n "bl602-sdk") (v "0.0.2") (d (list (d (n "bl602-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "123rcin8w0vprshhasxks1lzk30vpfkd8d8v5hrz8zivmj6jql01") (f (quote (("default"))))))

(define-public crate-bl602-sdk-0.0.3 (c (n "bl602-sdk") (v "0.0.3") (d (list (d (n "bl602-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "0q17ainlcmp8mh4l87vk8jlwqmnlsk5gx1m2q50bl0cnrc5vk029") (f (quote (("default"))))))

(define-public crate-bl602-sdk-0.0.4 (c (n "bl602-sdk") (v "0.0.4") (d (list (d (n "bl602-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "192g6w1hhrkbi7qs5xzi1vp0z1xyw88j4v6p6h26i6xf1vhm72d1") (f (quote (("default"))))))

(define-public crate-bl602-sdk-0.0.5 (c (n "bl602-sdk") (v "0.0.5") (d (list (d (n "bl602-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "0zw4gz844851kj1w6qvqdmkibs4lxfkvwaf4fcb38qkw4kk5a80m") (f (quote (("default"))))))

(define-public crate-bl602-sdk-0.0.6 (c (n "bl602-sdk") (v "0.0.6") (d (list (d (n "bl602-macros") (r "^0.0.2") (d #t) (k 0)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.3") (d #t) (k 0)))) (h "1r1dagjpy2i3ly3fwx4qjj2l7s19y0fzwv61fxbyh620nh4gri4x") (f (quote (("default"))))))

