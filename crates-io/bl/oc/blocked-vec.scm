(define-module (crates-io bl oc blocked-vec) #:use-module (crates-io))

(define-public crate-blocked-vec-0.1.0 (c (n "blocked-vec") (v "0.1.0") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1mrl70h5rsqrbx9p8210x6k6x0vdc4q4vyd9d23dga2yipdwhbyp") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.1 (c (n "blocked-vec") (v "0.1.1") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1lmxcs221ri2i3zxyfscvy16vg0rriv6sa9cxmc7z23cnicdb69i") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.2 (c (n "blocked-vec") (v "0.1.2") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1a7abzpmy0s1wr66fvalqxglxdjc6pqr25zg8dmj27sx511gb5lw") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.3 (c (n "blocked-vec") (v "0.1.3") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yy0x1ck6jgn48xgms6qm7929vd4bgmfxhcwyddpwy495qr9nddz") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.4 (c (n "blocked-vec") (v "0.1.4") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "052gb2pdwkqqhijnxv3w8fpraw5q5pkx8dzrvxv9i1vq0al3l637") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.5 (c (n "blocked-vec") (v "0.1.5") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0q5fxs0x1z4w7svwazlwr9jva3dq92sbrmdlan6bna3y8lp6fydw") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.6 (c (n "blocked-vec") (v "0.1.6") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1gpjzksn15isx1f6lch9g7cjfyhc2im08z88mmdn2dchns7y3385") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.7 (c (n "blocked-vec") (v "0.1.7") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1f6i7lpsvrkhbvhkyxibc2piz6hkahskgi19ybfd1bfy3m3i66ni") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

(define-public crate-blocked-vec-0.1.8 (c (n "blocked-vec") (v "0.1.8") (d (list (d (n "page_size") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0ls45a8zf9w9gskvs86vg6k04dccd37ss0vxz031ssd9firgxarr") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:page_size"))))))

