(define-module (crates-io bl oc blockchain-core) #:use-module (crates-io))

(define-public crate-blockchain-core-0.1.0 (c (n "blockchain-core") (v "0.1.0") (h "0xiqkg585axdyv8nzakaf5782q0r61b40v5axcgd1kmjsqi3b9gc") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockchain-core-0.1.1 (c (n "blockchain-core") (v "0.1.1") (h "0p8fcvmnww50yifb9wvx4gsyxywsamj0n4b7r86wb78g59v4qbkz") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockchain-core-0.1.2 (c (n "blockchain-core") (v "0.1.2") (h "0v1m4w6xyr79y1ba38ljxy39xcrg4nfhpqy9z2wgyydsx0d8msig") (f (quote (("std") ("default" "std"))))))

