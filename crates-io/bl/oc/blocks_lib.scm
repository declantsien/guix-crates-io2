(define-module (crates-io bl oc blocks_lib) #:use-module (crates-io))

(define-public crate-blocks_lib-0.3.0 (c (n "blocks_lib") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gawisv6xyjjwpkcainscnhqvlyfaznhhsiqg3dkkxc874wrrw9n")))

(define-public crate-blocks_lib-0.3.1 (c (n "blocks_lib") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m9kxd6w3dahs1k9vrrv337grfgcxiv5jl9fikqky1980fpp9mva")))

(define-public crate-blocks_lib-0.3.2 (c (n "blocks_lib") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g2412p0ka09hcfiqkcb08479jjv2rn8m7dzlay9wgvqz0pb4gjg")))

(define-public crate-blocks_lib-0.3.3 (c (n "blocks_lib") (v "0.3.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07clm9agjixx4w90xgd1y3ahb18pd7zf87x2gs00vr6n0fgb1f7n")))

