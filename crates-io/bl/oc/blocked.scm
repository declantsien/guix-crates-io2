(define-module (crates-io bl oc blocked) #:use-module (crates-io))

(define-public crate-blocked-0.0.0 (c (n "blocked") (v "0.0.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.5") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "18byib40a4959rvib2f3lb3xqjs0c95dlsw81b30mnlvkjqhd5gd")))

(define-public crate-blocked-0.1.0 (c (n "blocked") (v "0.1.0") (d (list (d (n "ci-detective") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.5") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1sn0c11j3fbrjnjyncyxw865qs6w9zx015s3myay0ip69g102dqb")))

