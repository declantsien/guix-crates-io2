(define-module (crates-io bl oc blockcounter) #:use-module (crates-io))

(define-public crate-blockcounter-0.2.1 (c (n "blockcounter") (v "0.2.1") (h "0lrn4cia3fialjsmk5wb69kzig8kqdm8d9whrqkqkilsd50078sy")))

(define-public crate-blockcounter-0.2.2 (c (n "blockcounter") (v "0.2.2") (h "023mm3s0grqyad6mzjb939pb9bhm8zf6chkfjdsjy6y9bbpb96v8")))

(define-public crate-blockcounter-0.2.3 (c (n "blockcounter") (v "0.2.3") (h "1w8s7qdld8hy5pnj9502py67hscnm9wyzqw6lfjs2dbk0lxbgr0c")))

(define-public crate-blockcounter-0.2.4 (c (n "blockcounter") (v "0.2.4") (h "0qkrd2c2vgkwh64yq948zwk3v19mp8xpw5xp40ilwvnl0s725hxy")))

(define-public crate-blockcounter-0.3.0 (c (n "blockcounter") (v "0.3.0") (h "0q7ii1yvbsyjkdkp36khcmf64qii20hpn1ckmh3sayw05dpbpy3b")))

(define-public crate-blockcounter-0.3.1 (c (n "blockcounter") (v "0.3.1") (h "0rh3042mn38gsj3i72hsglkp2gjs9ixf3hgipvwyrd6h5ybinshj")))

(define-public crate-blockcounter-0.3.2 (c (n "blockcounter") (v "0.3.2") (h "12qfrfhd61n0ravscpkbbr8i9yqx1czfibg54qsgj7905dwvw5np")))

