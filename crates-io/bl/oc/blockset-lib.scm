(define-module (crates-io bl oc blockset-lib) #:use-module (crates-io))

(define-public crate-blockset-lib-0.3.4 (c (n "blockset-lib") (v "0.3.4") (d (list (d (n "io-test") (r "^0.3.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1a62q4k6y7hm0czcr5jj3ay8b7ihp6wz4q5vslyvzkyb9yn1w4zx") (y #t)))

(define-public crate-blockset-lib-0.3.5 (c (n "blockset-lib") (v "0.3.5") (d (list (d (n "io-test") (r "^0.3.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1b63xvjy0ajqv4pygngivmkywbwyp7w6sxagw1hd2675klf75c65")))

(define-public crate-blockset-lib-0.3.6 (c (n "blockset-lib") (v "0.3.6") (d (list (d (n "io-test") (r "^0.4.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1n022jxj03j0fv43n46vlcgk3rjc2qknknxh0d17ra55sd2s1c9a")))

(define-public crate-blockset-lib-0.3.7 (c (n "blockset-lib") (v "0.3.7") (d (list (d (n "io-test") (r "^0.6.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "10xhaznf7x9ji4nhqrx8jhcfkk4jy6h0bcangwqj4b046zpafclb")))

(define-public crate-blockset-lib-0.4.0 (c (n "blockset-lib") (v "0.4.0") (d (list (d (n "io-test") (r "^0.6.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "1lqln8gi4hsmx6sxx832989m9hn2zw6vaw0q8nq74igwa0fci7pr")))

(define-public crate-blockset-lib-0.4.1 (c (n "blockset-lib") (v "0.4.1") (d (list (d (n "io-test") (r "^0.8.1") (d #t) (k 2)) (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.38") (d #t) (k 2)))) (h "1z704ncls27w6kpq2lprbx9ww2m88vqwz1nlia33mhj06l2vp5cs")))

(define-public crate-blockset-lib-0.4.2 (c (n "blockset-lib") (v "0.4.2") (d (list (d (n "io-test") (r "^0.8.1") (d #t) (k 2)) (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.39") (d #t) (k 2)))) (h "0r1hw96ankfh51pv7pb6pssvfnkvgwh2xmyqxbg1wmldj4p9jf44")))

(define-public crate-blockset-lib-0.5.0 (c (n "blockset-lib") (v "0.5.0") (d (list (d (n "io-test") (r "^0.10.1") (d #t) (k 2)) (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1frpxdjhwhlrfi5cy7h8jjyql5pr822m3dzg8mg787j952g0gliy")))

(define-public crate-blockset-lib-0.5.1 (c (n "blockset-lib") (v "0.5.1") (d (list (d (n "io-test") (r "^0.10.1") (d #t) (k 2)) (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "0nbaij1avhzr46qsdmgz3f7qrwawdg4grny23d4im23ig57hs19h")))

(define-public crate-blockset-lib-0.5.2 (c (n "blockset-lib") (v "0.5.2") (d (list (d (n "io-test") (r "^0.11.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1sbar2g9mqdgx146plw4bd8kc3l0dpisdj7z3a52h5lwg58bk9v3")))

(define-public crate-blockset-lib-0.5.3 (c (n "blockset-lib") (v "0.5.3") (d (list (d (n "io-test") (r "^0.11.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "15hf5r5rm95mzaayqc1bcwva6wrvlqa6viq3glbz3bdd01rmgvid")))

(define-public crate-blockset-lib-0.5.4 (c (n "blockset-lib") (v "0.5.4") (d (list (d (n "io-test") (r "^0.11.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "16wv9s79rl9vmr3ca29h1pmdpx6cks9303mbnapxn4kbaypzanrb")))

(define-public crate-blockset-lib-0.6.0 (c (n "blockset-lib") (v "0.6.0") (d (list (d (n "io-test") (r "^0.11.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.5") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1wdnfjlrm6dsf70i97vggxp07ga57i3bmvrv718k93i7dfbc3qpp")))

