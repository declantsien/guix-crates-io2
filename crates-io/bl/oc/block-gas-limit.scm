(define-module (crates-io bl oc block-gas-limit) #:use-module (crates-io))

(define-public crate-block-gas-limit-0.1.0 (c (n "block-gas-limit") (v "0.1.0") (d (list (d (n "client-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)) (d (n "vapabi-contract") (r "^9.0.0") (d #t) (k 0)) (d (n "vapabi-derive") (r "^9.0.1") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "0jsk83ak5sgx2ln7669kq0f4qkbsbckjz0xl2r2fxcy11fbd3504")))

