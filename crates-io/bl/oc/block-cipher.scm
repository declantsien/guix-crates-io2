(define-module (crates-io bl oc block-cipher) #:use-module (crates-io))

(define-public crate-block-cipher-0.0.0 (c (n "block-cipher") (v "0.0.0") (h "0kgfp53sxd110r5x731diqa81l0iiypblnjqmwvhc6isgqwddcj3") (y #t)))

(define-public crate-block-cipher-0.7.0 (c (n "block-cipher") (v "0.7.0") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1lanw69849mgfyh5yxifg641fsx7zncva4a4wbkkpgs47fraldzq") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-block-cipher-0.7.1 (c (n "block-cipher") (v "0.7.1") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "043zgfz1x4sxkdcsyabrcr440fcwhfpcqqa54jm7zp35wx4n84zs") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-block-cipher-0.8.0 (c (n "block-cipher") (v "0.8.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "103yripk26f7wkr15ar3fsgh8ddpqzxcjaz0fkmm15k0vbka6dzk") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-block-cipher-0.99.0 (c (n "block-cipher") (v "0.99.0") (h "1av6m7a2wjsh8ajsjacy5cijn601z3p5vybi7x5n2jgxck4l2k07") (y #t)))

(define-public crate-block-cipher-0.99.9 (c (n "block-cipher") (v "0.99.9") (h "1kv97m0wljzzmg4n863z4y1i76mv4azjal9vw4mhp0iahlp74k4y") (y #t)))

(define-public crate-block-cipher-0.99.99 (c (n "block-cipher") (v "0.99.99") (h "18fwc2i3rfnwvs50b83zdkjxwmd3lkl4b2pfbzyh502f5h2zlm6f")))

