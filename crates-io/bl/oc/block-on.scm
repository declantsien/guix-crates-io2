(define-module (crates-io bl oc block-on) #:use-module (crates-io))

(define-public crate-block-on-0.1.0 (c (n "block-on") (v "0.1.0") (h "0spzl57gj9y2rwrngc4q805cxh6q72zc6mw9gjc63njcw660rw81") (y #t)))

(define-public crate-block-on-0.2.0 (c (n "block-on") (v "0.2.0") (h "0j0v28ccpzznc9xi6ly6dq1nnjckqsjlrgq1yxw5ngr75pxkwl9g")))

(define-public crate-block-on-0.3.0 (c (n "block-on") (v "0.3.0") (h "0v3qd3pn5i1sc7ky3cdw5xhin345f8rfhm153k55x4jz4mwhm4sb")))

