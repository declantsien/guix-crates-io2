(define-module (crates-io bl oc blockhash) #:use-module (crates-io))

(define-public crate-blockhash-0.1.0 (c (n "blockhash") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "19pxw31g18rmz0jk2ispv5jx3k0zdmyf94dwin9fk4clxdm0gxdg") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockhash-0.2.0 (c (n "blockhash") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "1j664w0mmcsx424l2f11alandnm1bd0kml2k3y3817mmddb2j7dz") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockhash-0.3.0 (c (n "blockhash") (v "0.3.0") (d (list (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "0z7y6c9w7fq7lf0sig10vcza9n1ma3qrxnmggj88ixz63gry5a4z") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockhash-0.4.0 (c (n "blockhash") (v "0.4.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)))) (h "0207fh05zsjj9nasrxyizda6wfxzvgr10pxbhbndnvrsfrr3kjgp") (f (quote (("std") ("default" "std"))))))

(define-public crate-blockhash-0.5.0 (c (n "blockhash") (v "0.5.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)))) (h "1m8am6grsf1i4hj1ihkabmfwmmj3gpxy96r0hp5d8zbqx39bcvaj") (f (quote (("std") ("default" "std" "image")))) (r "1.56.1")))

(define-public crate-blockhash-1.0.0 (c (n "blockhash") (v "1.0.0") (d (list (d (n "image") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0bhwpkqyly1zbx3w3fc7nz7s04hx3rd1z12gy30llgk3kyal9ig4") (f (quote (("std") ("default" "std" "image")))) (r "1.70.0")))

