(define-module (crates-io bl oc blockz-futures) #:use-module (crates-io))

(define-public crate-blockz-futures-0.1.0 (c (n "blockz-futures") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vmnd897g4ma143jf32a40hzdpvdi0w63n3wl50bgm6hd56fqn6v")))

