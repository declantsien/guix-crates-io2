(define-module (crates-io bl oc block-device-driver) #:use-module (crates-io))

(define-public crate-block-device-driver-0.1.0 (c (n "block-device-driver") (v "0.1.0") (h "057jiyk1n4a037qmg5ncd49a8rcax4nw8p5xpiy23pwlq1irxhwi") (r "1.75")))

(define-public crate-block-device-driver-0.1.1 (c (n "block-device-driver") (v "0.1.1") (h "1qajqbvwlkqxj35y4p4i71i84w4i08qi49pilzp8ymglzmiby5qk") (r "1.75")))

(define-public crate-block-device-driver-0.2.0 (c (n "block-device-driver") (v "0.2.0") (d (list (d (n "aligned") (r "^0.4.2") (d #t) (k 0)))) (h "13i5pv1mfkxygbxlqm0z5kvhdf29aa9w9d146l2nizjr5xcm3h24") (r "1.75")))

