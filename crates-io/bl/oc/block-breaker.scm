(define-module (crates-io bl oc block-breaker) #:use-module (crates-io))

(define-public crate-block-breaker-0.1.0 (c (n "block-breaker") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1fk4mkmp8xxyv3qid7whglzylz627piasr1xx7gkc37nxb62k9yy")))

(define-public crate-block-breaker-0.2.0 (c (n "block-breaker") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0w31i5rs26aamal67yygs8kzk7j2yjf5x42y3w0accbh5ajr9ci4")))

(define-public crate-block-breaker-0.2.1 (c (n "block-breaker") (v "0.2.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "146l3a6g142aifak238g14pykqzjbm16l5lbfk4nydgfii57lwgw")))

