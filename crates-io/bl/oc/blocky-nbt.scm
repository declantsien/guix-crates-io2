(define-module (crates-io bl oc blocky-nbt) #:use-module (crates-io))

(define-public crate-blocky-nbt-0.1.0 (c (n "blocky-nbt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1ab850kw1ws28y7b14h6rcr3j8v9s15hbr1crfmapvwavwk2dza1") (f (quote (("preserve-order") ("default" "preserve-order"))))))

