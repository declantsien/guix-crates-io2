(define-module (crates-io bl oc blockfrost-openapi) #:use-module (crates-io))

(define-public crate-blockfrost-openapi-0.0.1 (c (n "blockfrost-openapi") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "14lgwclva0wd3w01pffrjxnnmd1rw77ivv41yihzq1bw2srbqc1f")))

(define-public crate-blockfrost-openapi-0.0.2 (c (n "blockfrost-openapi") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "06ysdkpi2q0vdd7kla8ia4cmmzclrhx935nbhsf2zrdbk9wnq21r")))

(define-public crate-blockfrost-openapi-0.0.3 (c (n "blockfrost-openapi") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)))) (h "17vkmqvhn7x3s0j9npqh675x1qn9m1p44a2dgry2vzy8d82djiyy")))

