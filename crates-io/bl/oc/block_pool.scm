(define-module (crates-io bl oc block_pool) #:use-module (crates-io))

(define-public crate-block_pool-0.1.0 (c (n "block_pool") (v "0.1.0") (h "1sbqhqbaf1s46521blg6ly4czswikm7gskfqfmgpmv60l0mpcpb2")))

(define-public crate-block_pool-0.1.1 (c (n "block_pool") (v "0.1.1") (h "1rf4jl3s9zmd8kgdrg85fmlrd0m0i0644cqpv3kmz7rcdd51lmhf")))

(define-public crate-block_pool-0.1.2 (c (n "block_pool") (v "0.1.2") (h "0l395g5941qyj5ms5nrvm9dbz6dqidak7q1g3yciw2hhj2cpfivw")))

