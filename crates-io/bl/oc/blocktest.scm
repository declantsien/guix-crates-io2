(define-module (crates-io bl oc blocktest) #:use-module (crates-io))

(define-public crate-blocktest-0.1.0 (c (n "blocktest") (v "0.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "162fw84qxsfzzhqxi5nxylzg4qnsi9g4xysn1f1hhbrfvqmhv59f")))

(define-public crate-blocktest-0.1.1 (c (n "blocktest") (v "0.1.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0cnnzkm3yz3qdmm9sgmylkifk7sw7ra27kyr7qg9v3aiq9ggy5p4")))

