(define-module (crates-io bl oc block-breaker-tui) #:use-module (crates-io))

(define-public crate-block-breaker-tui-0.1.0 (c (n "block-breaker-tui") (v "0.1.0") (d (list (d (n "block-breaker") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.9.6") (d #t) (k 0)) (d (n "tui") (r "^0.6.1") (f (quote ("crossterm"))) (k 0)))) (h "1b141gcqk99wcp9cd3sjz8cihnc9d1lzfzm4xxziw0n4m5km83vy")))

