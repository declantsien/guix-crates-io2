(define-module (crates-io bl oc block-modes) #:use-module (crates-io))

(define-public crate-block-modes-0.0.0 (c (n "block-modes") (v "0.0.0") (h "16p6f7wrycr48b8dcq9hyz6nwk0ssf7l8imnpb033gkndqv8i8r1")))

(define-public crate-block-modes-0.1.0 (c (n "block-modes") (v "0.1.0") (d (list (d (n "aes-soft") (r "^0.1") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)))) (h "1psnky60ndhxnx0mwj98i3vpj5snbg1gvz8rq5fk18wi1ys129c6")))

(define-public crate-block-modes-0.2.0 (c (n "block-modes") (v "0.2.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)))) (h "0c1aqqzqlrfmrb6bkm3zj1l5skq7zhl57wrbj74flvq22ima0gr8")))

(define-public crate-block-modes-0.3.0 (c (n "block-modes") (v "0.3.0") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "1adql3vyr2ffsyh76x05mx5dbnk82w9z961qn9m33y75gd26md6j") (f (quote (("std") ("default" "std"))))))

(define-public crate-block-modes-0.3.1 (c (n "block-modes") (v "0.3.1") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "1fgkgd0d6ql510p1098nl6l2hghpm78ldakqzandslaim4yiw2yr") (f (quote (("std") ("default" "std"))))))

(define-public crate-block-modes-0.3.2 (c (n "block-modes") (v "0.3.2") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "0fckfgazcz1zxab6i3knlbv4mlmscphgva7qmjn8rg1b8wy7nsz5") (f (quote (("std") ("default" "std"))))))

(define-public crate-block-modes-0.3.3 (c (n "block-modes") (v "0.3.3") (d (list (d (n "aes-soft") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "0af562hvwgbvn38npmrw5rybvma819rvb7wh6avzsfay14889aii") (f (quote (("std") ("default" "std"))))))

(define-public crate-block-modes-0.4.0 (c (n "block-modes") (v "0.4.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "14injpjl005hdkfrbi38yi3n8hkxw71sjrs0aj2yj7554ny6d2sk") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.5.0 (c (n "block-modes") (v "0.5.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1s2sazql0la8b6nkgs750lr1pz6wcf05agqg2kca7w5dibx46wfw") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.6.0 (c (n "block-modes") (v "0.6.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "090bclpkmm679cjhzpqll5gj3yjjca5x351d7m40vkqxssblrrbc") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.6.1 (c (n "block-modes") (v "0.6.1") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0wh9cn59pcikxgwrxzggqvwdr4grdilihq2bimafcfa7ibyi96qc") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.7.0 (c (n "block-modes") (v "0.7.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1w3jc3n7k4xq98b9mfina4wwpg1fq1s3b0mm5whqialb7q3yi82p") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.8.0 (c (n "block-modes") (v "0.8.0") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0k24xpfp6dyni2ycgscaj354q35j6an9dr94n4iznf5fblip8d3q") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.8.1 (c (n "block-modes") (v "0.8.1") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "block-padding") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "13id7rw1lhi83i701za8w5is3a8qkf4vfigqw3f8jp8mxldkvc1c") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-block-modes-0.9.0 (c (n "block-modes") (v "0.9.0") (h "130sai2cx341y14lqm1gf4ww6w6fwif4k4ghl7c5gf1qd9zjcwxg")))

(define-public crate-block-modes-0.9.1 (c (n "block-modes") (v "0.9.1") (h "07pjna64v0ng30j8ss9w7rv7k7l7gsii37yxm011a1kzh6q128ly") (r "1.56")))

