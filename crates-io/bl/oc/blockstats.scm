(define-module (crates-io bl oc blockstats) #:use-module (crates-io))

(define-public crate-blockstats-0.1.0 (c (n "blockstats") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codec") (r "^3") (d #t) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "subxt") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "12awxj7wfacnd0glngnc5d8j1fg7ln5fyx9jcy952pdq4x13889j") (r "1.56.1")))

(define-public crate-blockstats-0.2.0 (c (n "blockstats") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "codec") (r "^3") (d #t) (k 0) (p "parity-scale-codec")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "subxt") (r "^0.23") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1r6z9psvxhgszrf9bdghgspzjj7ddpj509kpm459vg677zb639k4") (r "1.56.1")))

