(define-module (crates-io bl oc block-buffer) #:use-module (crates-io))

(define-public crate-block-buffer-0.1.0 (c (n "block-buffer") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1bqmgqn4bly1qdd3b9sc9a1b4sh8xclfw4rjyp491aiy73xba0mx")))

(define-public crate-block-buffer-0.2.0 (c (n "block-buffer") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "055q90a6vk80j85i3fixjd5ci0dznrm9lkdd6xbjk7sx5w2a2f8k")))

(define-public crate-block-buffer-0.3.0 (c (n "block-buffer") (v "0.3.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)))) (h "0ycwc9ayw9i64c9g0gjx4x1z98bin74qafid3fjpmngra2xp8lii")))

(define-public crate-block-buffer-0.3.1 (c (n "block-buffer") (v "0.3.1") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)))) (h "02wqh3fw1k9saq6mqrf1acjc5iy72f55iq40q0l9dp47c6n5kz44")))

(define-public crate-block-buffer-0.3.2 (c (n "block-buffer") (v "0.3.2") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)))) (h "0j9j6f3ill5qgfjyr483s68y9dyiw5nl062r52y8234g55hpwl3s")))

(define-public crate-block-buffer-0.3.3 (c (n "block-buffer") (v "0.3.3") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)))) (h "1axki8f4rhnxvk6hlli4i53sgmi7c15ffryrv4757nzcp6cc4xm0")))

(define-public crate-block-buffer-0.2.0-tmp (c (n "block-buffer") (v "0.2.0-tmp") (d (list (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "124jp9mgbrknmjchhf2v0inssk174xia58yprvmmgwrvgjhf3swj")))

(define-public crate-block-buffer-0.5.0 (c (n "block-buffer") (v "0.5.0") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)))) (h "0fdmv8aqmxanprf380pzk4cz2x7m6rnp3qaqpxjq9vxaj9r0vz1v")))

(define-public crate-block-buffer-0.5.1 (c (n "block-buffer") (v "0.5.1") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)))) (h "0637737c6hyzxn19df5mpxai2zk08pcvrbp0i70ax8i66662l3yp")))

(define-public crate-block-buffer-0.6.0 (c (n "block-buffer") (v "0.6.0") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)))) (h "00vhsp0d6w04qwnkii9qs34js1hr7misi628vwrsv3nrlph8rr56")))

(define-public crate-block-buffer-0.7.0 (c (n "block-buffer") (v "0.7.0") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "03fnf1c4k2rpin05dggfmgqrylqvx5ikfpgs65sqa077w1i5qrj9") (y #t)))

(define-public crate-block-buffer-0.7.1 (c (n "block-buffer") (v "0.7.1") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "13vizwcd62vk44caiapvvaam72ir9km9xdr6xsjj6rz6paapgp27") (y #t)))

(define-public crate-block-buffer-0.7.2 (c (n "block-buffer") (v "0.7.2") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1nj0ay71zk4hsrl11a13ff1n18gawz7imiprmim2pnd6rh9yb7ah") (y #t)))

(define-public crate-block-buffer-0.7.3 (c (n "block-buffer") (v "0.7.3") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "12v8wizynqin0hqf140kmp9s38q223mp1b0hkqk8j5pk8720v560")))

(define-public crate-block-buffer-0.8.0 (c (n "block-buffer") (v "0.8.0") (d (list (d (n "block-padding") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.1") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0c9x5b8pk25i13bajqjkzf03bm5hx2y8pi9llfvjpy3nhr295kyv")))

(define-public crate-block-buffer-0.9.0 (c (n "block-buffer") (v "0.9.0") (d (list (d (n "block-padding") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1r4pf90s7d7lj1wdjhlnqa26vvbm6pnc33z138lxpnp9srpi2lj1")))

(define-public crate-block-buffer-0.10.0-pre (c (n "block-buffer") (v "0.10.0-pre") (d (list (d (n "block-padding") (r "^0.3.0-pre") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "182jrp17b3z0mj7rq828b7zj7njvf1ay3fq58lz5xmv195xffnjs")))

(define-public crate-block-buffer-0.10.0-pre.1 (c (n "block-buffer") (v "0.10.0-pre.1") (d (list (d (n "block-padding") (r "^0.3.0-pre") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "00jzqzjf3sw1jvkc6rs02cp1wzjpnd1yiv7bm5vjdj5kinsnnr5f")))

(define-public crate-block-buffer-0.10.0-pre.2 (c (n "block-buffer") (v "0.10.0-pre.2") (d (list (d (n "block-padding") (r "^0.3.0-pre") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "09x8qm6bb7ppf8a7l7d7v20yjcppick8k0v3r5yy07r7k4q89nlv")))

(define-public crate-block-buffer-0.10.0-pre.3 (c (n "block-buffer") (v "0.10.0-pre.3") (d (list (d (n "block-padding") (r "^0.3.0-pre") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0vr6gwfsy7fdrgg484ns93hxcyfpn9i31kswzdam2d01c21bb1zy")))

(define-public crate-block-buffer-0.10.0-pre.4 (c (n "block-buffer") (v "0.10.0-pre.4") (d (list (d (n "block-padding") (r "^0.3.0-pre") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1krd09dgrxzl37vw4l2vwqg85swmfwyf420iaiddaj0bki13rcfl")))

(define-public crate-block-buffer-0.10.0 (c (n "block-buffer") (v "0.10.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "15cbh9jbcfcbbi863dlmamjka2f8l55ld915vr0b0xlf0l16mlzi") (y #t)))

(define-public crate-block-buffer-0.10.1 (c (n "block-buffer") (v "0.10.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "02qwqdzhk9gh7ddzzd31adwqbdsk0gar0058w9ixgriaqra8wn03")))

(define-public crate-block-buffer-0.10.2 (c (n "block-buffer") (v "0.10.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "097k9xkd8gqrl03qg4fwhjvanp3ac0pq4drg8pynk9cyhi8zxxqb")))

(define-public crate-block-buffer-0.10.3 (c (n "block-buffer") (v "0.10.3") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0zmy5vjwa6pbrhlgk94jg2pz08w5dd9nw2j7jfwrg3s96w3y5k39")))

(define-public crate-block-buffer-0.11.0-pre (c (n "block-buffer") (v "0.11.0-pre") (d (list (d (n "crypto-common") (r "^0.2.0-pre") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "0aw9jmk5im5c9rgbdvx35v57hy9x08ynsmn4l15z57lfclys7di3")))

(define-public crate-block-buffer-0.10.4 (c (n "block-buffer") (v "0.10.4") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0w9sa2ypmrsqqvc20nhwr75wbb5cjr4kkyhpjm1z1lv2kdicfy1h")))

(define-public crate-block-buffer-0.11.0-pre.1 (c (n "block-buffer") (v "0.11.0-pre.1") (d (list (d (n "crypto-common") (r "=0.2.0-pre.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "0pzswyms94c0hwy3dw5j2df2ij6zgw39sq0yaj1kfwvmag6wncqk") (r "1.65")))

(define-public crate-block-buffer-0.11.0-pre.2 (c (n "block-buffer") (v "0.11.0-pre.2") (d (list (d (n "crypto-common") (r "=0.2.0-pre.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "12j1jri43d684wads9x31l67br9nw8zqhaxq8sh52z64qja1sq0a") (r "1.65")))

(define-public crate-block-buffer-0.11.0-pre.3 (c (n "block-buffer") (v "0.11.0-pre.3") (d (list (d (n "crypto-common") (r "=0.2.0-pre.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "1897wa2ywr9hdjzsj1hdrr0lsqsi38zs4lagddhp61xk86749g3j") (r "1.65")))

(define-public crate-block-buffer-0.11.0-pre.4 (c (n "block-buffer") (v "0.11.0-pre.4") (d (list (d (n "crypto-common") (r "=0.2.0-pre.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "0s65fx54wa2923axfd0a80gj1bwh0pn276jg8ffv8hq2ivgdpnhf") (r "1.65")))

(define-public crate-block-buffer-0.11.0-pre.5 (c (n "block-buffer") (v "0.11.0-pre.5") (d (list (d (n "crypto-common") (r "=0.2.0-pre.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "zeroize") (r "^1.4") (o #t) (k 0)))) (h "03m1nh7rypjin51n2f8kgpwvr8jdg7pp968dk3mhh201890niv9x") (r "1.65")))

