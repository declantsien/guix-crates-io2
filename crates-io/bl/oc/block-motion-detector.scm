(define-module (crates-io bl oc block-motion-detector) #:use-module (crates-io))

(define-public crate-block-motion-detector-0.1.0 (c (n "block-motion-detector") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0ifnxg60aj36q2x8h9b6yn6r4hh3y46iaw1l4fqam889h5vqdng2")))

