(define-module (crates-io bl oc blocking-threadpool) #:use-module (crates-io))

(define-public crate-blocking-threadpool-1.0.0 (c (n "blocking-threadpool") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)))) (h "1p7m8nyw0nxj3sijc2wzjgbqbx5jk8pxk456w1wdd7z018m5rqdy") (r "1.56.1")))

(define-public crate-blocking-threadpool-1.0.1 (c (n "blocking-threadpool") (v "1.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)))) (h "0scqhwph1jrr5rxdyr0cknzgyma126khap5b814hlyz0vxp4vfn4") (r "1.56.1")))

