(define-module (crates-io bl oc blockishfire) #:use-module (crates-io))

(define-public crate-blockishfire-0.2.0 (c (n "blockishfire") (v "0.2.0") (d (list (d (n "blockish") (r "^0.0.8") (d #t) (k 0)) (d (n "crossterm") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1n33snhbysa3rs4jhg0i06dpqdl6qj6r4cnd5kqdm3xciyri68s2")))

(define-public crate-blockishfire-0.3.0 (c (n "blockishfire") (v "0.3.0") (d (list (d (n "blockish") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0f5md08scpk9z7mg4l2naz6kl9c70apwdlayh9j53cdyy8vz61hk")))

