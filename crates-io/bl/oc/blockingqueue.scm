(define-module (crates-io bl oc blockingqueue) #:use-module (crates-io))

(define-public crate-blockingqueue-0.1.0 (c (n "blockingqueue") (v "0.1.0") (h "1f3wl2k3w35kqh6w5jxgjyc9c79xg40pyp6b61vhamkd43hsnd96")))

(define-public crate-blockingqueue-0.1.1 (c (n "blockingqueue") (v "0.1.1") (h "1zy4y89xsgn10dyapwjlqdiypmw3bg8yd5l7qrlv6rbc367v6fvn")))

