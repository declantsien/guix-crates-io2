(define-module (crates-io bl oc block_macro) #:use-module (crates-io))

(define-public crate-block_macro-0.1.0 (c (n "block_macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "17gqxzahbmbmsjriqbqp5i8l7kya5f67c9v5s0r2hybyl7r3yiip") (y #t)))

