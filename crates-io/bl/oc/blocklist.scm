(define-module (crates-io bl oc blocklist) #:use-module (crates-io))

(define-public crate-blocklist-0.1.0 (c (n "blocklist") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (k 0)) (d (n "phf") (r "^0.11.2") (k 1)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls-tls" "gzip" "json"))) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 1)))) (h "0h4vqd569jhcflp480qspck4vj1n944042q6mlzmwi8prc4lamd5")))

(define-public crate-blocklist-0.2.0 (c (n "blocklist") (v "0.2.0") (d (list (d (n "fst") (r "^0.4") (d #t) (k 0)) (d (n "fst") (r "^0.4") (d #t) (k 1)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("rustls-tls" "gzip" "json"))) (k 1)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (k 1)))) (h "1jj1fpcmfyn0s5i02hd3a3w7ilsh2j44znba0ryh3an095np6llx")))

