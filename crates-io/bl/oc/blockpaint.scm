(define-module (crates-io bl oc blockpaint) #:use-module (crates-io))

(define-public crate-blockpaint-0.2.0 (c (n "blockpaint") (v "0.2.0") (d (list (d (n "bracket-geometry") (r "^0.8.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "01114hdln37610axgjkymxgnjfhprfwdl5b9yhxrd8mkp42m3db3")))

(define-public crate-blockpaint-0.2.1 (c (n "blockpaint") (v "0.2.1") (d (list (d (n "bracket-geometry") (r "^0.8.7") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (t "cfg(not(target = \"redox\"))") (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (t "cfg(target = \"redox\")") (k 0)))) (h "0lcgxaw4xmzqv372whv818l06z67ffsvkxnygcq2vyny6arval58")))

