(define-module (crates-io bl oc block-mesh) #:use-module (crates-io))

(define-public crate-block-mesh-0.1.0 (c (n "block-mesh") (v "0.1.0") (d (list (d (n "ilattice") (r "^0.1") (d #t) (k 0)) (d (n "ndcopy") (r "^0.2") (d #t) (k 0)) (d (n "ndshape") (r "^0.2") (d #t) (k 0)))) (h "047s6fc4vy2km0x0hkk4v4r1s7jny2h4ax9nfh5yisnj8ah49r6j")))

(define-public crate-block-mesh-0.2.0 (c (n "block-mesh") (v "0.2.0") (d (list (d (n "ilattice") (r "^0.1") (d #t) (k 0)) (d (n "ndcopy") (r "^0.3") (d #t) (k 0)) (d (n "ndshape") (r "^0.3") (d #t) (k 0)))) (h "13wp3falx15787v42pqvjd8475vsishpimab2mypv9ss1d94a0rw")))

