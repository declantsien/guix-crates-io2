(define-module (crates-io bl oc blockchain) #:use-module (crates-io))

(define-public crate-blockchain-0.1.0 (c (n "blockchain") (v "0.1.0") (h "0bxarrzlc6znilarripc03l3qqsks2qj6ghs8gh9kkcpjc5k9rns")))

(define-public crate-blockchain-0.1.1 (c (n "blockchain") (v "0.1.1") (h "1zy0dnrrd0kgqgq6xgy6mdpycbl4cjd4q6clwahz4dp4rram3xhh")))

(define-public crate-blockchain-0.1.2 (c (n "blockchain") (v "0.1.2") (h "1k2g2zsz99aq0kqp40rkazmzbc94yg72wj6c5hm3f6v18nrs58xa")))

(define-public crate-blockchain-0.1.3 (c (n "blockchain") (v "0.1.3") (h "1470m9vh181g5qafc8gsq0favci9dfsqswzrfnzfjyvzhyizsy5w")))

(define-public crate-blockchain-0.1.4 (c (n "blockchain") (v "0.1.4") (h "1jkr04q4x89jj480gfhvgldswvvp43j5nrq3vkkkpzhib441jxv0")))

(define-public crate-blockchain-0.1.5 (c (n "blockchain") (v "0.1.5") (h "0bdsgzszlyv8yy41129dgq36fqgki3j1qkgv91b16sa8prym4w6x")))

(define-public crate-blockchain-0.1.6 (c (n "blockchain") (v "0.1.6") (h "1wnid262y5f5z7l215yhhdn91x5ijja4l5cdzagnd3mcdaj5375i")))

(define-public crate-blockchain-0.1.7 (c (n "blockchain") (v "0.1.7") (h "19h4n249i1128wxvs04wbfnwxgjv8136c1kl111ysm6b0f371cza")))

(define-public crate-blockchain-0.1.8 (c (n "blockchain") (v "0.1.8") (h "13grlpdk8229b47i2skg8yz8yfrlbilfkgcg4hnyk4w70080d43d")))

(define-public crate-blockchain-0.1.9 (c (n "blockchain") (v "0.1.9") (h "1df129whhbcg8vbwmb3jq7infz51z5qaa2afg6fh5f636j44r4kx")))

(define-public crate-blockchain-0.2.0 (c (n "blockchain") (v "0.2.0") (h "0qf5nk9q77w19lpk06yysz29wriz82m3fx3xm07fy2s1g9r0yp5j")))

(define-public crate-blockchain-0.3.0 (c (n "blockchain") (v "0.3.0") (h "068fapljgpia0jm9zp6b3c987w2qlxfhm5h705r9qd1zijpp9ws1")))

(define-public crate-blockchain-0.3.1 (c (n "blockchain") (v "0.3.1") (h "128b9qxkvk9bswazkg6j5l7x3pjaby3nwrnnqjj2kdn4dcy0rqzx")))

(define-public crate-blockchain-0.4.0 (c (n "blockchain") (v "0.4.0") (h "1x2xii48jn35aay9b5gsbzqh6cnywq0l3j8kp2s2yxrzn4qgxx07")))

(define-public crate-blockchain-0.5.0 (c (n "blockchain") (v "0.5.0") (h "0pas880vnkxwjkxfbcmskicrnfrpgmwa4ar3ybhi7275095kknl3")))

(define-public crate-blockchain-0.5.1 (c (n "blockchain") (v "0.5.1") (h "0y688gjkq7jb8a7xdr5z9dwzkx3rvczfy6glwmsf8fvdy43awnjx")))

(define-public crate-blockchain-0.5.2 (c (n "blockchain") (v "0.5.2") (h "0xdxv2pfr7aj50yvnylb8mqnid9fdi7ijsvrzmi0g164pvliryfy")))

(define-public crate-blockchain-0.5.3 (c (n "blockchain") (v "0.5.3") (h "0azbbaa1f09nqzknz1v1zwj4d36vhyxjwap4in0sckyq6gamy15n")))

(define-public crate-blockchain-0.5.4 (c (n "blockchain") (v "0.5.4") (h "0h4m9b6vnz4bl1qi414dwahnqwbhsg76n8pyw4wy0b401vmzlpac")))

(define-public crate-blockchain-0.5.5 (c (n "blockchain") (v "0.5.5") (h "1hx5yrsicw5vn5jqbb7irnpmiqmd08lylladk5dic95q3nrzkhzb")))

(define-public crate-blockchain-0.5.6 (c (n "blockchain") (v "0.5.6") (h "1lw0323iwirnh8njmykripmx1cby56x2i3p9n9p43x4j5ggf0s5z")))

(define-public crate-blockchain-0.5.7 (c (n "blockchain") (v "0.5.7") (h "1n9kginxcddjm72a40fs34ang8sdwdi9hwb4maa0wi2s3giz50yv")))

(define-public crate-blockchain-0.5.8 (c (n "blockchain") (v "0.5.8") (h "1hz7100cjwg1ij8yl67ka0654bqv2hlknsidjlb3rq2hck7182si")))

(define-public crate-blockchain-0.5.9 (c (n "blockchain") (v "0.5.9") (h "0icwfirf1nvrsy7yfjz7kjcav9wk4slyg1pdyzrn7jykhd4ix634")))

(define-public crate-blockchain-0.5.10 (c (n "blockchain") (v "0.5.10") (h "1hf16m6yrj8j8d80nw5vs5hy57xwj930agkbpq2a2kzf09gashvm")))

(define-public crate-blockchain-0.5.11 (c (n "blockchain") (v "0.5.11") (h "17phf4crz397ysli4n3333jqsqz60cl1zawq7x1alw429ahhsm4g")))

(define-public crate-blockchain-0.6.0 (c (n "blockchain") (v "0.6.0") (h "0svsb11nipfwdcz6m7k0hcag3wadh1ci9ynz04s7ivi8d1d0v1hg")))

(define-public crate-blockchain-0.6.1 (c (n "blockchain") (v "0.6.1") (h "0vhrf6l1gdzhzgnkxxc39zbahz4zff2i0j0xkrg4fjd7ybpx182y")))

(define-public crate-blockchain-0.7.0 (c (n "blockchain") (v "0.7.0") (h "1winkmf3ms9n0n3651qvzzzs4lql5ijm0q4bi3bfrxj0813mm00c")))

(define-public crate-blockchain-0.7.1 (c (n "blockchain") (v "0.7.1") (h "0fsdnr7cs8v90dmag2ksybzxx6yyx1r08w2b127p0h98ffrivv7f")))

(define-public crate-blockchain-0.7.2 (c (n "blockchain") (v "0.7.2") (h "1a76v8s4rgd0lrbfiq2iwi0hykgn8k5krf3k9mv6p9ypm8pwrj3n")))

(define-public crate-blockchain-0.7.3 (c (n "blockchain") (v "0.7.3") (h "0mcb5bgafxd2kgzmhdmr71aix1yllhhblwv2xqzw2dp1jbs44kc0")))

(define-public crate-blockchain-0.7.4 (c (n "blockchain") (v "0.7.4") (h "06svsw1s4zzfi8hwdj0d0lsr681wq52fyknjx9ind071vl9srkjn")))

(define-public crate-blockchain-0.8.0 (c (n "blockchain") (v "0.8.0") (h "0dq6856qmwhwf2rvd71smgpsm1rdrlgvphig3xqc8n7md5611p5w")))

(define-public crate-blockchain-0.8.1 (c (n "blockchain") (v "0.8.1") (h "1xlnr34hlv5y9kbyglg27s2plyixgyq3q07wsqlkwxhv880zpb5y")))

(define-public crate-blockchain-0.9.0 (c (n "blockchain") (v "0.9.0") (d (list (d (n "blockchain-core") (r "^0.1") (d #t) (k 0)))) (h "0bab2gxgav060h27q045vnyz2gq1xck1m6g2d3k7psmh2jgkw8y1")))

(define-public crate-blockchain-0.9.1 (c (n "blockchain") (v "0.9.1") (d (list (d (n "blockchain-core") (r "^0.1") (d #t) (k 0)))) (h "0ivilaql8nj3v246nddbb1djmzqfbdf0c2w7wm2i8jf3wxl24gqv")))

(define-public crate-blockchain-0.9.2 (c (n "blockchain") (v "0.9.2") (d (list (d (n "blockchain-core") (r "^0.1") (d #t) (k 0)))) (h "1m5kr3jgkpah44qfzvxj6l0a5s3d4cs4va1zqzqya9qba10xgs0v")))

