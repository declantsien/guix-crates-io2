(define-module (crates-io bl oc blockpulsar_client_rust) #:use-module (crates-io))

(define-public crate-blockpulsar_client_rust-1.0.0 (c (n "blockpulsar_client_rust") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.34") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "171dbzpwhfnsxqqs890m0az9yiym64r1iysfkdy2ywsv7862alqx")))

