(define-module (crates-io bl oc blocknative-flows) #:use-module (crates-io))

(define-public crate-blocknative-flows-0.1.0 (c (n "blocknative-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1v3zabpzwx59fq7wnsckl3z45y4sgsbj37sdghz8jjlq4zm0wp8a")))

(define-public crate-blocknative-flows-0.1.1 (c (n "blocknative-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0s2n1n1y7jc2bvbnrf1fg75443576skwnk2xppmg8bqjfvldcrhf")))

(define-public crate-blocknative-flows-0.1.2 (c (n "blocknative-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0kwwkqsnnfk576y9m4xw6a26cphdnzs3ydfyjs96ywjyc9jnrpbm")))

