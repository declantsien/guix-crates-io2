(define-module (crates-io bl oc blockconvert) #:use-module (crates-io))

(define-public crate-blockconvert-0.1.0 (c (n "blockconvert") (v "0.1.0") (d (list (d (n "adblock") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1ys99s8ag1lhil03l5sgz3a358srq0yhg0f5nb78wfbs6aff3azw")))

(define-public crate-blockconvert-0.2.0 (c (n "blockconvert") (v "0.2.0") (d (list (d (n "adblock") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1d5glwr00k3hsjkph06i6iymcwqxsc6790j2glc58lnfa6rnv59i")))

(define-public crate-blockconvert-0.2.1 (c (n "blockconvert") (v "0.2.1") (d (list (d (n "adblock") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0n0zq8iy3mv5k34hk1jnc2x8hns60fx0618gd39mmwsm9mgz3bfl")))

(define-public crate-blockconvert-0.2.2 (c (n "blockconvert") (v "0.2.2") (d (list (d (n "adblock") (r "^0.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1cqmwx9sagmy2jipa3x20gcxn8mps47l5pqm0khpw9jpkgmi22ba")))

(define-public crate-blockconvert-0.3.0 (c (n "blockconvert") (v "0.3.0") (d (list (d (n "adblock") (r "^0.3") (f (quote ("full-regex-handling" "embedded-domain-resolver"))) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "19s4zaww0s7crh1krz8nsxvi6kvq6zxcmmcj8g80fkm5gj0jn7yg")))

(define-public crate-blockconvert-0.4.0 (c (n "blockconvert") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "0mf6iahw0kyg12mzdzp5zys49vrwib6im1zvmpzzrzadk1nicfkg")))

(define-public crate-blockconvert-0.4.1 (c (n "blockconvert") (v "0.4.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "021x9a9z63wa6x82lhrpj97syqpa7xk9ibk9x8m2v60c0lsyp2xc")))

(define-public crate-blockconvert-0.4.3 (c (n "blockconvert") (v "0.4.3") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "1q19qxn8yxry67k2lwpp5qx056yf7fwha5917wyrv5lh5vgf5ydy")))

(define-public crate-blockconvert-0.4.4 (c (n "blockconvert") (v "0.4.4") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "0h5qd40alrcw1cpz6mxdwz8qn6jaqq6brgrqg9q5pn8k4dz1iizm")))

(define-public crate-blockconvert-0.4.5 (c (n "blockconvert") (v "0.4.5") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "03qp61asnkd90bl5pg3d2fbpc7qfk1jip95wsrf9m83mirjc0wfn")))

(define-public crate-blockconvert-0.5.0 (c (n "blockconvert") (v "0.5.0") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "1l387lja3p50v3spwgm8jxl98mn19vr4izqc20cqncf05m4yfsp0")))

(define-public crate-blockconvert-0.5.1 (c (n "blockconvert") (v "0.5.1") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "1950r7qbpiwwfvg293nphamzrawcn8g3h9hx1ibppm58akcgvmsn")))

(define-public crate-blockconvert-0.5.2 (c (n "blockconvert") (v "0.5.2") (d (list (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1.4") (f (quote ("std" "perf"))) (d #t) (k 0)))) (h "094avh8qa3fkwymqla0lvyzildpj6kzzs25iw5i1fa1dnyk2gakg")))

