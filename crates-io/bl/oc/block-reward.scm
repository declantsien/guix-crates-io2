(define-module (crates-io bl oc block-reward) #:use-module (crates-io))

(define-public crate-block-reward-0.0.0 (c (n "block-reward") (v "0.0.0") (h "0riw1vhn8p1kid4jw2n4kjyy7spvz25vm8zagd44170y1i08f8n4")))

(define-public crate-block-reward-0.1.0 (c (n "block-reward") (v "0.1.0") (d (list (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (d #t) (k 0)) (d (n "mashina") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "vapabi") (r "^9.0.1") (d #t) (k 0)) (d (n "vapabi-contract") (r "^9.0.0") (d #t) (k 0)) (d (n "vapabi-derive") (r "^9.0.1") (d #t) (k 0)) (d (n "vapcore-trace") (r "^0.1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "0dhjbc669z910x0xsqa0s5a4k4bgl857kg0mfi7n6a3icgg7bcg5")))

