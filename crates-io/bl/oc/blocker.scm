(define-module (crates-io bl oc blocker) #:use-module (crates-io))

(define-public crate-blocker-0.1.0 (c (n "blocker") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (k 0)))) (h "04z997cqv4g875gbgm2lry2v1jy304iicll2f6hi7ijl8nnjwx3j") (f (quote (("thread_yield"))))))

(define-public crate-blocker-0.1.1 (c (n "blocker") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (k 0)))) (h "1492cm5zfgchvnwi02jnp6p2iqfk8cbs5904xglqy5rqnnwyfzb1") (f (quote (("thread_yield"))))))

(define-public crate-blocker-0.1.2 (c (n "blocker") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (k 0)))) (h "1fz4n0rfdh7ci4y5n2gw51c465bdbsyb4iqmjn1i5rwsrg5sq368") (f (quote (("thread_yield"))))))

