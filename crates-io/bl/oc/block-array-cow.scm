(define-module (crates-io bl oc block-array-cow) #:use-module (crates-io))

(define-public crate-block-array-cow-0.1.0 (c (n "block-array-cow") (v "0.1.0") (h "0hw067l783a0m61qblc8490djyc7myifc7wc8c5k493xy3wygkyy")))

(define-public crate-block-array-cow-0.1.1 (c (n "block-array-cow") (v "0.1.1") (h "1w4if2d5yrrhfxmvmlchyhn9yyh33if8qp90j5f7asap5svx5v5j")))

(define-public crate-block-array-cow-0.1.3 (c (n "block-array-cow") (v "0.1.3") (h "1ha4vv62sxx03gzzl7xifmxmmapc2n662hz2ywsqn2f28pppv2pb")))

(define-public crate-block-array-cow-0.1.4 (c (n "block-array-cow") (v "0.1.4") (h "07jqmjya6d5sd07vjgf1ky6day7brnrfl8g50k2l1z6i58vq77ba")))

