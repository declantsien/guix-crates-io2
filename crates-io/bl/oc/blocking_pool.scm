(define-module (crates-io bl oc blocking_pool) #:use-module (crates-io))

(define-public crate-blocking_pool-0.1.0 (c (n "blocking_pool") (v "0.1.0") (d (list (d (n "aliasable") (r "^0.1.3") (d #t) (k 0)) (d (n "atomic-waker") (r "^1.0.0") (d #t) (k 0)) (d (n "completion") (r "^0.2.0") (d #t) (k 2)) (d (n "completion-core") (r "^0.2.0") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.4") (d #t) (k 0)))) (h "0rrjdyvg06z4917cy68xncv0bzvc86mnihhaj8vs8jb5zhwj2rcx")))

(define-public crate-blocking_pool-0.2.0 (c (n "blocking_pool") (v "0.2.0") (d (list (d (n "completion-core") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0i5wf1iv9pc2bldyv71s6721362yqxyrhvsphp2aqkqqx3ry2x79")))

