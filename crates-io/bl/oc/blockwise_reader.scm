(define-module (crates-io bl oc blockwise_reader) #:use-module (crates-io))

(define-public crate-blockwise_reader-0.1.0 (c (n "blockwise_reader") (v "0.1.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "1y06mwl1672qgmjvw8drphaq463c22zab631xvii8ycmbjdig9fk")))

(define-public crate-blockwise_reader-0.2.0 (c (n "blockwise_reader") (v "0.2.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "1hzxkh4qmbaapprfgrs590q6a1j2q9h2xgnpzpkqkkcwl13x0svy")))

(define-public crate-blockwise_reader-0.2.1 (c (n "blockwise_reader") (v "0.2.1") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "10ia3wiz2gv9mzq7l53s3cympixmlhx480krqkhcs3bpld095v0j")))

(define-public crate-blockwise_reader-0.2.2 (c (n "blockwise_reader") (v "0.2.2") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "086vfr3s4grav2ijm4jnlm216ahfa9i3qm48xlqwmi3fsi7ka6xh")))

(define-public crate-blockwise_reader-0.3.0 (c (n "blockwise_reader") (v "0.3.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "0nhxdw81z7zp931l79gj43ifjr55xp7x798iffxi3sb9z2k6pamb")))

(define-public crate-blockwise_reader-0.3.1 (c (n "blockwise_reader") (v "0.3.1") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "08mg4sskbcjljlnaa97xh4injbbzarndc6b30r9vw9rwg5r7wjxg")))

(define-public crate-blockwise_reader-0.4.0 (c (n "blockwise_reader") (v "0.4.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "0qh8jmgnm48rvizlqy99mcmxdr4j4jbv50qgw9xq240x06k8pvw5")))

(define-public crate-blockwise_reader-0.5.0 (c (n "blockwise_reader") (v "0.5.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "1spg1l24whps68cija0v91y95xqg5r591ksqdrh12idh50z73b92")))

(define-public crate-blockwise_reader-0.6.0 (c (n "blockwise_reader") (v "0.6.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "1wx6qv9gvl5916d1x8gz74d0fgip5snkjif7q94m9bwcnnbsv6q4")))

(define-public crate-blockwise_reader-0.7.0 (c (n "blockwise_reader") (v "0.7.0") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "135v6f0y4rccbhsch2zh28wx1wlj4bck3lb17nqh6k8ldc1w6q58")))

(define-public crate-blockwise_reader-0.7.2 (c (n "blockwise_reader") (v "0.7.2") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "0s3iz30y2364r3k3y2a0cpxznglp1kpybsqs5xzn6r8xrqf1d2xj")))

(define-public crate-blockwise_reader-0.7.3 (c (n "blockwise_reader") (v "0.7.3") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "024b5avs0w121l1ssms5vy0jv0i93pp9b6kqxw4442l0lxlm7j25")))

(define-public crate-blockwise_reader-0.7.4 (c (n "blockwise_reader") (v "0.7.4") (d (list (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)))) (h "1mdkw81bds8h15w8fxpzzywqr4fns9sycn4ffz39wi4gybpbq1dh")))

