(define-module (crates-io bl oc block-grid) #:use-module (crates-io))

(define-public crate-block-grid-0.1.0 (c (n "block-grid") (v "0.1.0") (h "03k9ny2crclirl6iqm427abdbl74gnq3j2qwcnlargd098s3hxiz")))

(define-public crate-block-grid-0.1.1 (c (n "block-grid") (v "0.1.1") (h "1hc45lixiaz02v8xmcgr0f9yip8jqjwp3rhsh604fmbalyyjzjkc")))

(define-public crate-block-grid-0.1.2 (c (n "block-grid") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "09ynwvg2sf9k2iwi5x1c0nclzzgzakprkzg60hmsxixxv87rfn39")))

(define-public crate-block-grid-0.1.3 (c (n "block-grid") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "0yxgl33b35mbhb5ywp1c21a7p2rn264yy0d5642zzmb1s1f002gq")))

