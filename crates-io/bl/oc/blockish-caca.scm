(define-module (crates-io bl oc blockish-caca) #:use-module (crates-io))

(define-public crate-blockish-caca-0.0.1 (c (n "blockish-caca") (v "0.0.1") (d (list (d (n "blockish") (r "^0.0.2") (d #t) (k 0)) (d (n "blockish-player") (r "^0.0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "redhook") (r "^1.0") (d #t) (k 0)))) (h "0rnqba092jy5aqdmr4cgqsgk70d109j8frgk7n6k62i6qgx7jhsr")))

(define-public crate-blockish-caca-0.0.2 (c (n "blockish-caca") (v "0.0.2") (d (list (d (n "blockish") (r "^0.0.2") (d #t) (k 0)) (d (n "blockish-player") (r "^0.0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "redhook") (r "^1.0") (d #t) (k 0)))) (h "06w4qc4gl2cbah29zfrsbq50jyjs4cbr2qgvl7znf4v02z272bv4")))

(define-public crate-blockish-caca-0.0.3 (c (n "blockish-caca") (v "0.0.3") (d (list (d (n "blockish") (r "^0.1.2") (d #t) (k 0)) (d (n "blockish-player") (r "^0.0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "redhook") (r "^1.0") (d #t) (k 0)))) (h "0ghcm8knzk0w8ywl4dc6m4plc8xsr5d10r9y2l19gzs699xx44cz")))

(define-public crate-blockish-caca-0.0.4 (c (n "blockish-caca") (v "0.0.4") (d (list (d (n "blockish") (r "^0.1.2") (d #t) (k 0)) (d (n "blockish-player") (r "^0.0.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "redhook") (r "^1.0") (d #t) (k 0)))) (h "0l61vkk1pl0mv69wfjhiy2c87padridl29y62q34ajanrqay4iqx")))

(define-public crate-blockish-caca-0.0.6 (c (n "blockish-caca") (v "0.0.6") (d (list (d (n "blockish") (r "^0.1.2") (d #t) (k 0)) (d (n "blockish-player") (r "^0.0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "redhook") (r "^1.0") (d #t) (k 0)))) (h "0xgjkrfk2rvpsgmpjvnf98sy0xdh0d98mrsa7p0xg0yncw3505sw")))

