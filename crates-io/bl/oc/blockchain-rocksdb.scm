(define-module (crates-io bl oc blockchain-rocksdb) #:use-module (crates-io))

(define-public crate-blockchain-rocksdb-0.1.0 (c (n "blockchain-rocksdb") (v "0.1.0") (d (list (d (n "blockchain") (r "^0.8") (d #t) (k 0)) (d (n "parity-codec") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rocksdb") (r "^0.12") (d #t) (k 0)))) (h "1cwgj0gq8386qbngy73vm0m071zzyw86vsggc485v3g9fzjv6028")))

