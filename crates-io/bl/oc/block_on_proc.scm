(define-module (crates-io bl oc block_on_proc) #:use-module (crates-io))

(define-public crate-block_on_proc-0.1.0 (c (n "block_on_proc") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "09b7n8x8z7slr6sgmggxkdlnfz2fsrs13lzd2ry9zz64a0780m9b")))

(define-public crate-block_on_proc-0.2.0 (c (n "block_on_proc") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0rx1q793i4484ficgdhf5l66i4ydq56ijl9vww73gd7fir9g6wmq")))

