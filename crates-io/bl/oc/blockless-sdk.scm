(define-module (crates-io bl oc blockless-sdk) #:use-module (crates-io))

(define-public crate-blockless-sdk-0.1.0 (c (n "blockless-sdk") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0yh546n26spnw5qvpq60ss94yx9sl549l8fm6215bdh82jkw56v8")))

(define-public crate-blockless-sdk-0.1.1 (c (n "blockless-sdk") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "14fad55bans4vmfqnq1d13nbbp20i5n1j2jyghdr0km3b6fagyz9")))

(define-public crate-blockless-sdk-0.1.2 (c (n "blockless-sdk") (v "0.1.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1nb1q85r8zv7y79dlr19achmzbd3xv9rl7v0s4mw05z4syhzgx1n")))

(define-public crate-blockless-sdk-0.1.3 (c (n "blockless-sdk") (v "0.1.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0mzf8an79slq0d2naaa58x5n7pqm4cb1n9qd1nwgi4bn7awr6msm")))

