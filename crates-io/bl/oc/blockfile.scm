(define-module (crates-io bl oc blockfile) #:use-module (crates-io))

(define-public crate-blockfile-0.1.0 (c (n "blockfile") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "fs" "macros" "io-std" "io-util"))) (o #t) (d #t) (k 0)))) (h "11sr6fq6f080z2xla976c81qa58h9g05a3v3dpb4n86q9qvm8f68") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-blockfile-0.1.1 (c (n "blockfile") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "fs" "macros" "io-std" "io-util"))) (o #t) (d #t) (k 0)))) (h "05cxj23ra0r1bb2xkfvrhgvc80y60ajsakqndybqa03i1nf9z64s") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:tokio"))))))

(define-public crate-blockfile-0.1.2 (c (n "blockfile") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "fs" "macros" "io-std" "io-util"))) (o #t) (d #t) (k 0)))) (h "14kzhh1lzrgy81dp67yq2v4qy9x8qf0sxan6q2p1g1ha9x0yalm5") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:tokio"))))))

