(define-module (crates-io bl oc blockset) #:use-module (crates-io))

(define-public crate-blockset-0.1.0 (c (n "blockset") (v "0.1.0") (h "1ixvnprjxwc4i2j20qapbidjxz57v17mzrzdlpy6jgd7r8b5lnxw") (y #t)))

(define-public crate-blockset-0.2.0 (c (n "blockset") (v "0.2.0") (d (list (d (n "wasm-bindgen-test") (r "^0.3.16") (d #t) (k 2)))) (h "1p40ay5vgmhg3miyw6yhyjng6n50hfrrgfil92cm2kvj8g97gi2i")))

(define-public crate-blockset-0.2.1 (c (n "blockset") (v "0.2.1") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0vhdafdwy30adxfjpj7g9wkijxv0j58zy820by11npbwa4ipl2rd")))

(define-public crate-blockset-0.2.2 (c (n "blockset") (v "0.2.2") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "098svfjvg762pcf35hdrv5xfrksbi03zd1j1xqbz8v9g7l8z9hdl")))

(define-public crate-blockset-0.2.3 (c (n "blockset") (v "0.2.3") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "0a0qw8r9d005nvj1hw6vb7wj0866mrhlrxcib8xhynwdc0wzbxcs")))

(define-public crate-blockset-0.2.4 (c (n "blockset") (v "0.2.4") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1d5izsvbm3ca5zvi3hs1aspdl82l34lkihmgc7s0sh1znx7g5f1m")))

(define-public crate-blockset-0.3.0 (c (n "blockset") (v "0.3.0") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1j7z4bpayxqx69zr68bkg94cvxjdmdidgiid62yjbbdki3316kyf")))

(define-public crate-blockset-0.3.1 (c (n "blockset") (v "0.3.1") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "066za37azfkfq02avawbrcs614prn7lbbxhggs0g1myn8ndpjfhz")))

(define-public crate-blockset-0.3.2 (c (n "blockset") (v "0.3.2") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "1jx57kc36qp4pnbav7zfv80kyabx59c27f7wfgzcjg5nyn74pqh4")))

(define-public crate-blockset-0.3.3 (c (n "blockset") (v "0.3.3") (d (list (d (n "wasm-bindgen-test") (r "^0.3.37") (d #t) (k 2)))) (h "18k6d75d3k92wy7fknxxidyk0ff57jmybx66p3akqhvkv2sglv0g")))

(define-public crate-blockset-0.3.4 (c (n "blockset") (v "0.3.4") (d (list (d (n "blockset-lib") (r "^0") (d #t) (k 0)) (d (n "io-impl") (r "^0.3.1") (d #t) (k 0)))) (h "0gx505ajl577056lk5lq67rfl3d1y15kdivi8gs9fv43jlgix28k") (y #t)))

(define-public crate-blockset-0.3.5 (c (n "blockset") (v "0.3.5") (d (list (d (n "blockset-lib") (r "=0.3.5") (d #t) (k 0)) (d (n "io-impl") (r "^0.3.1") (d #t) (k 0)))) (h "06ma1896pjnpbkdiy00wsdp6zbxxyf871px5amahx7rqiy5hq8c7")))

(define-public crate-blockset-0.3.6 (c (n "blockset") (v "0.3.6") (d (list (d (n "blockset-lib") (r "=0.3.6") (d #t) (k 0)) (d (n "io-impl") (r "^0.4.0") (d #t) (k 0)))) (h "02fwyglrfx0jmq8jl13j9026wnqbvgnj7lid99lmajibcxb7h6kz")))

(define-public crate-blockset-0.3.7 (c (n "blockset") (v "0.3.7") (d (list (d (n "blockset-lib") (r "=0.3.7") (d #t) (k 0)) (d (n "io-impl") (r "^0.6.0") (d #t) (k 0)))) (h "1hxrbqq6va971jh8w3hhwsdl0a2d1yr2nr97mgzl3rdfnlw7y4vj")))

(define-public crate-blockset-0.4.0 (c (n "blockset") (v "0.4.0") (d (list (d (n "blockset-lib") (r "=0.4.0") (d #t) (k 0)) (d (n "io-impl") (r "^0.6.0") (d #t) (k 0)))) (h "089ij5j7dxk0lc5g2qr4z9nxcavw8mrw2pagzxnfiscqah01jh81")))

(define-public crate-blockset-0.4.1 (c (n "blockset") (v "0.4.1") (d (list (d (n "blockset-lib") (r "=0.4.1") (d #t) (k 0)) (d (n "io-impl") (r "^0.8.0") (d #t) (k 0)))) (h "086g94319v0ndrbjpid1cdgkj9wca7vagfdh0w5xj56hzc93s44d")))

(define-public crate-blockset-0.4.2 (c (n "blockset") (v "0.4.2") (d (list (d (n "blockset-lib") (r "=0.4.2") (d #t) (k 0)) (d (n "io-impl") (r "^0.8.1") (d #t) (k 0)))) (h "05zrjd5v8223phwzpnz2qcja6kjlnb9h73va6gg7dss33hlxm15b")))

(define-public crate-blockset-0.5.0 (c (n "blockset") (v "0.5.0") (d (list (d (n "blockset-lib") (r "^0.5.0") (d #t) (k 0)) (d (n "io-impl") (r "^0.10.0") (d #t) (k 0)))) (h "0chf7lvb6447xmh13qqhqcys94din0hbkjl5079phyq2m6liix1l")))

(define-public crate-blockset-0.5.1 (c (n "blockset") (v "0.5.1") (d (list (d (n "blockset-lib") (r "^0.5.1") (d #t) (k 0)) (d (n "io-impl") (r "^0.10.0") (d #t) (k 0)))) (h "0jhfw0wj0hw1msl1b44r4c4zy5f8w9fvfmwl2lvfb946gsa9bkbk")))

(define-public crate-blockset-0.5.2 (c (n "blockset") (v "0.5.2") (d (list (d (n "blockset-lib") (r "^0.5.2") (d #t) (k 0)) (d (n "io-impl") (r "^0.11.0") (d #t) (k 0)))) (h "1jh2n6plm4f80x9v348m8an927h8sxg4jyhnnb9fg1pbdzf04bsf")))

(define-public crate-blockset-0.5.3 (c (n "blockset") (v "0.5.3") (d (list (d (n "blockset-lib") (r "^0.5.3") (d #t) (k 0)) (d (n "io-impl") (r "^0.11.0") (d #t) (k 0)))) (h "01zvhkkj2zddasx5z362j606r906vasdbrml43wsw4ya84ak2r0y")))

(define-public crate-blockset-0.5.4 (c (n "blockset") (v "0.5.4") (d (list (d (n "blockset-lib") (r "^0.5.4") (d #t) (k 0)) (d (n "io-impl") (r "^0.11.0") (d #t) (k 0)))) (h "0y60q6csq2vm70191yjm0bsxpzngjhmqdqbvk7vci2gbwj0s2j65")))

(define-public crate-blockset-0.6.0 (c (n "blockset") (v "0.6.0") (d (list (d (n "blockset-lib") (r "^0.6.0") (d #t) (k 0)) (d (n "io-impl") (r "^0.11.0") (d #t) (k 0)))) (h "0xd1qdpgs3kk0zdpx9bi786xj7f51jp27ngbbg0gf0v184zrayvq")))

