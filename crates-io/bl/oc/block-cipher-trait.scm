(define-module (crates-io bl oc block-cipher-trait) #:use-module (crates-io))

(define-public crate-block-cipher-trait-0.1.0 (c (n "block-cipher-trait") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0pbz38c33cbyc65pdnhq64nx9fp8v2pfzg5p2nanmbf94nfi2bxa") (y #t)))

(define-public crate-block-cipher-trait-0.2.0 (c (n "block-cipher-trait") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1q62xbmwhvq5fvbk6bsivg8gp3swvvd7x26q6445i5890f2q9821") (y #t)))

(define-public crate-block-cipher-trait-0.3.0 (c (n "block-cipher-trait") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "13vw76cpmcjrwwa1z8nacj6zbzrdi0zgadbxqb9zvpmzi1nqmp51") (y #t)))

(define-public crate-block-cipher-trait-0.4.0-alpha (c (n "block-cipher-trait") (v "0.4.0-alpha") (d (list (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "11bc6q64mns8avlmiax3ifwc6br3yxzq5p2p6i2mbm4rl689gkbc") (y #t)))

(define-public crate-block-cipher-trait-0.4.0-beta (c (n "block-cipher-trait") (v "0.4.0-beta") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1zmyaldkaflvm6g2vci4qqlqx3rkp5whpg67namr552j1baw9ygy") (y #t)))

(define-public crate-block-cipher-trait-0.4.0-gamma (c (n "block-cipher-trait") (v "0.4.0-gamma") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0aslpf25pmyq4pzl62q0vq8kf3hgs2cla4mib0615gm11r4bza4s") (y #t)))

(define-public crate-block-cipher-trait-0.4.0 (c (n "block-cipher-trait") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "0bmly37k8lzrbyhg5pph9z36qvp08zx0r444xkj3ladgghx9w53j") (y #t)))

(define-public crate-block-cipher-trait-0.4.1 (c (n "block-cipher-trait") (v "0.4.1") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1wxq4i5rq5as557wy3hvbp6838x06zhsq6gsx2pm0gr3fk7jyd01") (y #t)))

(define-public crate-block-cipher-trait-0.4.2 (c (n "block-cipher-trait") (v "0.4.2") (d (list (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "10qmg8vphqmfllb9a2yx6s7r66jh1wh33clhsawq7ikg2wgz2p6q") (y #t)))

(define-public crate-block-cipher-trait-0.5.0 (c (n "block-cipher-trait") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "1lxs636xgc36vpg5swsxszrr9sjma8ri8qd3xwr3bbl06a06s4yn") (f (quote (("dev")))) (y #t)))

(define-public crate-block-cipher-trait-0.5.1 (c (n "block-cipher-trait") (v "0.5.1") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "1pbm5dym6jzjqfig9nzc9am87451zm4myjs784y9919im1nn5g11") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-block-cipher-trait-0.5.2 (c (n "block-cipher-trait") (v "0.5.2") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "1ggb7j6py8grzzbk3mh2pvrvrra5jnlygzpm8l1x39lr4q04akmx") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-block-cipher-trait-0.5.3 (c (n "block-cipher-trait") (v "0.5.3") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "1d3a33gmfd9vag62kphfnzc07znxfs9ysa44svgz6nclgd1j811p") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-block-cipher-trait-0.6.0 (c (n "block-cipher-trait") (v "0.6.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0r92558m6qk9sc5r1idcv68nnmbmb4pvb4rrdy20xww8hsirf8a1") (f (quote (("std") ("dev")))) (y #t)))

(define-public crate-block-cipher-trait-0.6.1 (c (n "block-cipher-trait") (v "0.6.1") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1pm3h3swwnqm0kngxvjqf6l18i0adfwk8iscdynrb9jwq96qlrih") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-block-cipher-trait-0.6.2 (c (n "block-cipher-trait") (v "0.6.2") (d (list (d (n "blobby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0x273w6fwka0i48nrv428birdrs2jz6jdnmc0dhc1rq9pm4lv4hw") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-block-cipher-trait-0.7.0 (c (n "block-cipher-trait") (v "0.7.0") (h "1k14854x0jvxy4c2yb6hyqyg8rbd39n0zkbqg1596l0vca48bhsc") (y #t)))

(define-public crate-block-cipher-trait-0.99.0 (c (n "block-cipher-trait") (v "0.99.0") (h "0b12kdgmlifzvvkak4inv4y9lv261wnmcby181rm8gy3l4j859vm") (y #t)))

