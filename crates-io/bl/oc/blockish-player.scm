(define-module (crates-io bl oc blockish-player) #:use-module (crates-io))

(define-public crate-blockish-player-0.0.1 (c (n "blockish-player") (v "0.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1axry77j34ra069vzk7mv278ff1f8d06gai23drjrp2g09jd33xp")))

(define-public crate-blockish-player-0.0.2 (c (n "blockish-player") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "19xhj8kb6cwl9ix8jj250h2pfd76x0rcnh7v5lc876w8p9w3xiqn")))

(define-public crate-blockish-player-0.0.3 (c (n "blockish-player") (v "0.0.3") (d (list (d (n "crossterm") (r "^0.14") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1idwsf8yqm7632jr56xq3dkcgqnljm1l0admlhk5gidm9kcn0xbw")))

(define-public crate-blockish-player-0.0.4 (c (n "blockish-player") (v "0.0.4") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0bhh83xf532maxibg76qjs08b8aba2mbbrh1k6bqphrkd2hmbldb")))

(define-public crate-blockish-player-0.0.5 (c (n "blockish-player") (v "0.0.5") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1r4sc0vsgdxiij976x93109kd0d2yvz6y54gfhs8a8zrfcwmnj0b")))

