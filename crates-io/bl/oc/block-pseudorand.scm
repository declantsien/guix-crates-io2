(define-module (crates-io bl oc block-pseudorand) #:use-module (crates-io))

(define-public crate-block-pseudorand-0.1.0 (c (n "block-pseudorand") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "chiapos-chacha8") (r "^0.1.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (d #t) (k 0)) (d (n "transvec") (r "^0.3.6") (f (quote ("std"))) (k 0)))) (h "1az7vkf7f3198wxl1dazhkpj0a38nv312whz796qgp21p9rlk7ah")))

(define-public crate-block-pseudorand-0.1.1 (c (n "block-pseudorand") (v "0.1.1") (d (list (d (n "chiapos-chacha8") (r "^0.1.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (d #t) (k 0)))) (h "17j2hbxm7q1dvz05fi048wjc7wyblax5m602d7ja3z3rnh7pdpd0")))

(define-public crate-block-pseudorand-0.1.2 (c (n "block-pseudorand") (v "0.1.2") (d (list (d (n "chiapos-chacha8") (r "^0.1.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.6.1") (d #t) (k 0)))) (h "19hm05q3nfp5akn9vkq8f09hhimsxlzd3x266xja0i6jjn23b5r0")))

