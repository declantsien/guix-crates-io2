(define-module (crates-io bl oc blockchain_types) #:use-module (crates-io))

(define-public crate-blockchain_types-0.1.0 (c (n "blockchain_types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19dssn40dbvsm1i47frck8618klv72dpa013n989cbhg9jz9iy35") (y #t)))

(define-public crate-blockchain_types-0.0.1 (c (n "blockchain_types") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rcgchwbaq5yyfja8yag7blq88qlxji5bvhp5rhvqnvzshl1gzqh")))

(define-public crate-blockchain_types-0.0.2 (c (n "blockchain_types") (v "0.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xpxfb3b21w1vkcm8qghnyl6v9qx0mkv29ismh34hpfjwia9pj06")))

