(define-module (crates-io bl oc block_effects) #:use-module (crates-io))

(define-public crate-block_effects-0.1.0 (c (n "block_effects") (v "0.1.0") (h "1xr5j30cxk9vxicxgdknnypjv1idikkixmmilviq1ka8mdkkkghm")))

(define-public crate-block_effects-0.1.1 (c (n "block_effects") (v "0.1.1") (h "1z32x5snqk2877b4hnyvn2gi6pbbx06c8nbdzc0ldqmffk42sx8b")))

