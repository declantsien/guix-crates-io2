(define-module (crates-io bl oc blocking-reader) #:use-module (crates-io))

(define-public crate-blocking-reader-0.1.0 (c (n "blocking-reader") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moka") (r "^0.11") (f (quote ("future"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qlqa8dgdks7a78z8c92imkbgihpr78s09y7210npiynaqpmy5h8")))

