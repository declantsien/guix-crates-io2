(define-module (crates-io bl oc blocking_future) #:use-module (crates-io))

(define-public crate-blocking_future-0.1.0 (c (n "blocking_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1.14") (d #t) (k 0)))) (h "1xpp9kfn42fnbmkivnpwckfyr09qrhq8pc5ryg8ngp0k751sqki4")))

