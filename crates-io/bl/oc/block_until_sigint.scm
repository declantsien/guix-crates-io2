(define-module (crates-io bl oc block_until_sigint) #:use-module (crates-io))

(define-public crate-block_until_sigint-0.1.0 (c (n "block_until_sigint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "signal-hook-tokio") (r "^0.3.1") (f (quote ("futures-v0_3"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hih9ka7gcdqvx8ka0197rih8zl3b43zl8w189igmln4y8zx3dkw")))

