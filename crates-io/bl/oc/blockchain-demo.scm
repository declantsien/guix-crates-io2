(define-module (crates-io bl oc blockchain-demo) #:use-module (crates-io))

(define-public crate-blockchain-demo-0.1.0 (c (n "blockchain-demo") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bitcoincash-addr") (r "^0.5.2") (d #t) (k 0)) (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "merkle-cbt") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)))) (h "0wkpxl44bski32c3pcl3l2cbfp5a9h4v2qinn3vcy7f6pm715wzk")))

