(define-module (crates-io bl oc blockstorm) #:use-module (crates-io))

(define-public crate-blockstorm-0.1.0 (c (n "blockstorm") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.3.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("termion"))) (d #t) (k 0)))) (h "0nm834pcfb3a049dslcs9q33sz80jx8bzzmrqqdm2b5ky5k3xbp7")))

(define-public crate-blockstorm-0.2.0 (c (n "blockstorm") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ruscii") (r "^0.3.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("termion"))) (d #t) (k 0)))) (h "01rq31a6vi3n8lbl6vx1y4jj5hvvbqwk3csylk2r2440ms0rh61b")))

