(define-module (crates-io bl oc block_allocator) #:use-module (crates-io))

(define-public crate-block_allocator-0.1.0 (c (n "block_allocator") (v "0.1.0") (d (list (d (n "iobuf") (r "*") (d #t) (k 0)) (d (n "mmap") (r "*") (d #t) (k 0)))) (h "0fwdl6a2kz1kd32aq2wgkvng05psaalmpc35h44qn1245j9zab4p")))

(define-public crate-block_allocator-0.1.1 (c (n "block_allocator") (v "0.1.1") (d (list (d (n "iobuf") (r "*") (d #t) (k 0)) (d (n "mmap") (r "*") (d #t) (k 0)))) (h "1r5q1f238j1xvs0p4r6isx6v7c7afqg1fmy78hcbpcfbm9j38nd2")))

(define-public crate-block_allocator-0.2.0 (c (n "block_allocator") (v "0.2.0") (d (list (d (n "mmap") (r "~0.1.1") (d #t) (k 0)))) (h "0wnyr8gvp6pimbqcbwc147f6z3q6a13w9j3wnsldpgjsyjm5w9qc")))

(define-public crate-block_allocator-0.2.1 (c (n "block_allocator") (v "0.2.1") (d (list (d (n "memmap") (r "~0.4") (d #t) (k 0)))) (h "1658h7p0ws8vl8pvjjfgwqvamfrfnknrczvblbrspfy7140simw2")))

(define-public crate-block_allocator-0.2.2 (c (n "block_allocator") (v "0.2.2") (d (list (d (n "memmap") (r "~0.4") (d #t) (k 0)))) (h "1yvi8xq7kcd85n1g4bbjmlqrjxrhm7iqa4pz8zhzpbx9hjfzpgd3")))

