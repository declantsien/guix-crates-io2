(define-module (crates-io bl oc block-toolbox) #:use-module (crates-io))

(define-public crate-block-toolbox-0.1.0 (c (n "block-toolbox") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "web3") (r "^0.17.0") (d #t) (k 0)))) (h "146xlr2shlpx43z93mccixqvw44z89167ajhyvbmnrhn8816aj7x")))

(define-public crate-block-toolbox-0.2.0 (c (n "block-toolbox") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "web3") (r "^0.17.0") (d #t) (k 0)))) (h "0i9ssgzb5hl4rnvwrzf2ixfjszk2d0h2vcwb1zpaha6yns064scd")))

