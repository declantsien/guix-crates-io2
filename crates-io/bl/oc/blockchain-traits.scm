(define-module (crates-io bl oc blockchain-traits) #:use-module (crates-io))

(define-public crate-blockchain-traits-0.1.0 (c (n "blockchain-traits") (v "0.1.0") (h "1yi4gbg322qy1s57sban1fgwn27xbb6bba0rm719s3igw1g8njww")))

(define-public crate-blockchain-traits-0.2.0 (c (n "blockchain-traits") (v "0.2.0") (h "0kwvikw11g0c0z9qa71qasfm5j70bdqkac5cz2vzhjvli5g7mp38")))

(define-public crate-blockchain-traits-0.2.1 (c (n "blockchain-traits") (v "0.2.1") (h "117yb4hqpl4smy76lg47nkjpn4srvjah7a3k60mr3jf3lmvqj2nk")))

(define-public crate-blockchain-traits-0.2.2 (c (n "blockchain-traits") (v "0.2.2") (d (list (d (n "oasis-types") (r "^0.2") (d #t) (k 0)))) (h "06zg9jfb019xgw35hbw3jbbfmsjj8mdb12s1qs2ka040096yrrza")))

(define-public crate-blockchain-traits-0.2.3 (c (n "blockchain-traits") (v "0.2.3") (d (list (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "131kp5sgjzqkwrn2pxpc7mxyq63nhkrck78rcw7yyvxxfgnhnjdx")))

(define-public crate-blockchain-traits-0.3.0 (c (n "blockchain-traits") (v "0.3.0") (d (list (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "032h3sm34fmcqv9h8j1dlv3vb5znxxiyfrsdi0xm3y9yxs154js0")))

(define-public crate-blockchain-traits-0.4.0 (c (n "blockchain-traits") (v "0.4.0") (d (list (d (n "oasis-types") (r "^0.3") (d #t) (k 0)))) (h "1b15v9msq908bgmyj1c0nzz1rp60phpxgml04y53cx94n5fsh97d")))

(define-public crate-blockchain-traits-0.4.1 (c (n "blockchain-traits") (v "0.4.1") (d (list (d (n "oasis-types") (r "^0.4") (d #t) (k 0)))) (h "1c7pyq3rmx1rw7sav8w2lcjkx1cmr5wri6y80laqvqzrcr4yhwaj")))

