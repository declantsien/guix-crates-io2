(define-module (crates-io bl oc block-tools) #:use-module (crates-io))

(define-public crate-block-tools-0.1.0 (c (n "block-tools") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "r2d2" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)))) (h "1kiskanrjf674mzcix43r672wc4npgr86n0l8gh8nbr8r652lfjj") (y #t)))

(define-public crate-block-tools-0.1.2 (c (n "block-tools") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "diesel") (r "^1.4.5") (f (quote ("postgres" "r2d2" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^7") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sbp5g372yi4d8vdx87x608l3hwqzj0fxf7zbpqql0jlfn82ch73") (y #t)))

