(define-module (crates-io bl oc blocky) #:use-module (crates-io))

(define-public crate-blocky-0.1.0 (c (n "blocky") (v "0.1.0") (h "0x3a2d1dw5yi5zdxqval0f9485gll72an1i34k10znvpk796f0j7")))

(define-public crate-blocky-0.1.1 (c (n "blocky") (v "0.1.1") (h "0mw9nhapy7b8cjqm8zl4r3sh2b0nys4k6xrav3580l55kmralwgk")))

