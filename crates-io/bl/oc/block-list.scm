(define-module (crates-io bl oc block-list) #:use-module (crates-io))

(define-public crate-block-list-1.0.0 (c (n "block-list") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "08s85dhlnvh9cnr1pdy9gq9w343lsyf6i911g1prmnfbh8k7gj1s")))

(define-public crate-block-list-1.1.0 (c (n "block-list") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "1p4j6ic8a536y6drmas3ma9rq78cmbi0x4lsnk04p5s6zrdz00zv")))

(define-public crate-block-list-1.1.1 (c (n "block-list") (v "1.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "0p5v8pyzmzd2fwcasbyyf73mj7l9l29b45j6z2cb9n9r6nr023x3")))

(define-public crate-block-list-1.1.2 (c (n "block-list") (v "1.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "1zc58zgmafrzxil6qbasqbmk078gycwds75q43j2b9nyxhgvkcz6")))

(define-public crate-block-list-1.1.3 (c (n "block-list") (v "1.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "10w9jzl5n7x72n6l0zd5p32cr92vwbj56l6jvjfc1j0b92c50b8w")))

(define-public crate-block-list-1.1.4 (c (n "block-list") (v "1.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "curl") (r "^0.4.38") (d #t) (k 0)))) (h "1ijq695pdwxh88bi3cv0f6gvr8lcx5kayfz28db9q8i64v7p2g0h")))

