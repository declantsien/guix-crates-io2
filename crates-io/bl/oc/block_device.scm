(define-module (crates-io bl oc block_device) #:use-module (crates-io))

(define-public crate-block_device-0.1.0 (c (n "block_device") (v "0.1.0") (h "0cpl0wwnbzdfb22igfbis04waldwh0rg81j2m2ly1bhyzbwqlq2m")))

(define-public crate-block_device-0.1.1 (c (n "block_device") (v "0.1.1") (h "02jl003ml75s9yms6zv6gnrphal3j3klb074lj977xnvcysyf2ap")))

(define-public crate-block_device-0.1.2 (c (n "block_device") (v "0.1.2") (h "1bdq7whd8zlmp0al0x2s2000n99i7zzmx9c02p0l6qn7bj1mag6p")))

(define-public crate-block_device-0.1.3 (c (n "block_device") (v "0.1.3") (h "1mmyaqab2w1221xnbgfw4q596pg0nsrwp98s0gljzp7l9kqvrpil")))

