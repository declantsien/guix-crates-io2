(define-module (crates-io bl oc blockade) #:use-module (crates-io))

(define-public crate-blockade-0.1.0 (c (n "blockade") (v "0.1.0") (h "1z7c4n15ykdz1sg822bscz1qrwpdgx7h6kpki1zvk09cil4bcj0b")))

(define-public crate-blockade-0.1.1 (c (n "blockade") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09fz04bsjqrq3ydnjp5scj3jk9ymjlja56ag5xa7hwr5s161lwy8")))

(define-public crate-blockade-0.1.2 (c (n "blockade") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x3bva6ijjs5wjp3ksamkfd317aka8923pwz3faxp71sc6l2qvz2")))

(define-public crate-blockade-0.1.3 (c (n "blockade") (v "0.1.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0chd5cs3zbr2h7n55v4pgy5szggi5hakj2dsvyn33bp3qzr7k1w2")))

(define-public crate-blockade-0.1.4 (c (n "blockade") (v "0.1.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hf1k8a7wl8dndd9a6fw4ysrfdgd9rsxbrhlx1vkja6pch2yzjpn")))

(define-public crate-blockade-0.1.5 (c (n "blockade") (v "0.1.5") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "179vd8kmr3ha1cj735zi74ca5mz8xpvb9v3wy71cc3rgj0828f9f")))

(define-public crate-blockade-0.1.6 (c (n "blockade") (v "0.1.6") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aqk98y9wkafx6dq5g5vd5zihdhz0dy24hm6vwfw2rd9l1jw58vv")))

(define-public crate-blockade-0.1.7 (c (n "blockade") (v "0.1.7") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pgd8vwyz6h3sfcphkfjdbx7zmdwjn0ki04c4pfha2iqdpdrp4kj")))

(define-public crate-blockade-0.1.8 (c (n "blockade") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18rj0by9sj93da9i19ad5y969lxp1p8q9ya0dp00adydp5hkpw8d")))

(define-public crate-blockade-0.1.9 (c (n "blockade") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0abr78r9x4511frad6595p89vxxn77qb3ginydmiplfnch46720x")))

(define-public crate-blockade-0.1.10 (c (n "blockade") (v "0.1.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i6nrcyzqbyd6anw8rk44j1qharppf67q5p5bijsc4fkykbl61sg")))

