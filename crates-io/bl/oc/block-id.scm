(define-module (crates-io bl oc block-id) #:use-module (crates-io))

(define-public crate-block-id-0.1.0 (c (n "block-id") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "165aa3c9ppkiqzvjf8qc50ganjkl966hw182fkrln70p42s57a0g")))

(define-public crate-block-id-0.1.1 (c (n "block-id") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0grhl3bbygcg0s9s8vcf5zwc603r3j2zsqzl5348953f8n8zskvi")))

(define-public crate-block-id-0.1.2 (c (n "block-id") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1n8b56il0zbpzv0djr1yd34mqz70n4c5xw96y1ry56zd4m89bzij")))

(define-public crate-block-id-0.2.0 (c (n "block-id") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "1j435syjx8x1n44ggns523bkc3hfs5fhdsp5lkipgj5g0afw0n4n")))

(define-public crate-block-id-0.2.1 (c (n "block-id") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 0)))) (h "0qq2cn0y5xsf32ymz8nb97c9l5cy3lwz8kki0sdi1yblwawr7br8")))

