(define-module (crates-io bl oc blockchain_addresses) #:use-module (crates-io))

(define-public crate-blockchain_addresses-0.1.0 (c (n "blockchain_addresses") (v "0.1.0") (h "02lgmwxqi39gyncxrp9f89k4ch8x4fz1679wkqwjlkxblg28nqwb")))

(define-public crate-blockchain_addresses-0.1.1 (c (n "blockchain_addresses") (v "0.1.1") (h "0gfydzind72wcx9fqld21gpb8g1kjalxn9vbfc4v328z8mc95v28")))

(define-public crate-blockchain_addresses-0.1.3 (c (n "blockchain_addresses") (v "0.1.3") (h "1bkgh4bcc52ykak7saif3kgw40s9pgdcjdjwfvxj3hjmvj354px1")))

