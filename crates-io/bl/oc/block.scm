(define-module (crates-io bl oc block) #:use-module (crates-io))

(define-public crate-block-0.0.1 (c (n "block") (v "0.0.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1kq97qr3qi5jh9nbywzbyrdidc1y78gs08xskcawzad8apjsxl0k")))

(define-public crate-block-0.0.2 (c (n "block") (v "0.0.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "07bgkhj5r07337mnrz68kj92yddcl08lv39ps1lyqw3s14bvcqfl")))

(define-public crate-block-0.1.0 (c (n "block") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "0lv27ynqzyjgxw7h33h2vzdcr3l3mm74zrj33kdd51zwiv4rirli")))

(define-public crate-block-0.1.1 (c (n "block") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "118sg2d67fmwxrl9i1b9g1rd8qls257lnmai5j4i8kdv7kg9kl0h")))

(define-public crate-block-0.1.2 (c (n "block") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "19sh5bfcs0vlrl21s9bc1kxk6kil8b3d6id8w265qq2b1mkkaa0c")))

(define-public crate-block-0.1.3 (c (n "block") (v "0.1.3") (d (list (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1wwnjfqj1pnv4d03z5mk172lia0jrkmlxwgic4dy5lwssgxl7xzl")))

(define-public crate-block-0.1.4 (c (n "block") (v "0.1.4") (d (list (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "0cbs91j9dhm5k4w3y5501w6g68n2z3qm8n4xi97bbs1ja224n2zl")))

(define-public crate-block-0.1.5 (c (n "block") (v "0.1.5") (d (list (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "15zxf6yh0783lhlz2y5vqixdbfsw5h8yndrxy2d77csx4ybrlbnw")))

(define-public crate-block-0.1.6 (c (n "block") (v "0.1.6") (d (list (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "16k9jgll25pzsq14f244q22cdv0zb4bqacldg3kx6h89d7piz30d")))

