(define-module (crates-io bl oc blockscout-display-bytes) #:use-module (crates-io))

(define-public crate-blockscout-display-bytes-1.0.0 (c (n "blockscout-display-bytes") (v "1.0.0") (d (list (d (n "bytes") (r "^1.3") (d #t) (k 0)) (d (n "ethers-core") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0phvxnk7lwxb5ryv39v6hrvqysaapk6i1341byjmbpnin4mwqjlg")))

