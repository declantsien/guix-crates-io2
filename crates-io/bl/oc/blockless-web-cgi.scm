(define-module (crates-io bl oc blockless-web-cgi) #:use-module (crates-io))

(define-public crate-blockless-web-cgi-0.1.0 (c (n "blockless-web-cgi") (v "0.1.0") (d (list (d (n "blockless-sdk") (r "^0.1.1") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "1d3bszy98dyqnalsjn63jjv6s3z4ql7xb080qv0kbvg88j3mpcgy")))

(define-public crate-blockless-web-cgi-0.1.1 (c (n "blockless-web-cgi") (v "0.1.1") (d (list (d (n "blockless-sdk") (r "^0.1.2") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4.15") (d #t) (k 0)) (d (n "httpcodec") (r "^0.2.3") (d #t) (k 0)))) (h "1zchmqzp4zmjha1sgv51h5c7d10npilv4qkw4kydqh6m1yp6ndxk")))

