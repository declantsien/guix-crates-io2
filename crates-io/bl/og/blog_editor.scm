(define-module (crates-io bl og blog_editor) #:use-module (crates-io))

(define-public crate-blog_editor-0.1.0 (c (n "blog_editor") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)))) (h "0m5yksxd663wslk2x8aq0fpyhd5x2qh2lx1kxbkarc5f3sxbrzxj")))

(define-public crate-blog_editor-0.2.0 (c (n "blog_editor") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)))) (h "1a7j9zjj6gjp70kxxqf2f6sls2hjiad6x59r6v6rpynjvdj98ijy")))

