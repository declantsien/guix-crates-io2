(define-module (crates-io bl og blog-login-enum) #:use-module (crates-io))

(define-public crate-blog-login-enum-0.1.0 (c (n "blog-login-enum") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bz8d9wysfz12w8pj4rpd81xi1rr26dy1jif26wjwh326x0pvxq9")))

