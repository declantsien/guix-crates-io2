(define-module (crates-io bl og blogs-md-easy) #:use-module (crates-io))

(define-public crate-blogs-md-easy-0.1.0 (c (n "blogs-md-easy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1msfzd8l3q4y9d05n2m2c9wd3kqvv3assjbh8976n3iqbh74x2vp")))

(define-public crate-blogs-md-easy-0.1.1 (c (n "blogs-md-easy") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0cbnpcnbmdkfxp0zjzaj3b3f83in4s0vk5q65l8wjsyimk2ibgjj") (y #t)))

(define-public crate-blogs-md-easy-0.1.2 (c (n "blogs-md-easy") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1gdsg67bvxkmjichn4ygafm1xbqq8nvjaisxhx1528zcf8da61wf")))

(define-public crate-blogs-md-easy-0.1.3 (c (n "blogs-md-easy") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0h3h27d9n88n0h49cdi5lljn33wvgrn6znpgbw8s71gh3zgfgdbd")))

(define-public crate-blogs-md-easy-0.1.4 (c (n "blogs-md-easy") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "171wxzwlsc5hqwdm0g5fn5vp70s50cfgqr6nz7sc0akjaizxnw60")))

(define-public crate-blogs-md-easy-0.1.5 (c (n "blogs-md-easy") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0wbr6ivn9z3ihgs62dr2mg6r7wqh1al2rkwfz5wl16n4bmbfhkmb")))

(define-public crate-blogs-md-easy-0.2.0 (c (n "blogs-md-easy") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1shj3jzapv1l9882igxlwdxc6i8x0nsnkbnjh04ggzn6lsbbmwwl")))

(define-public crate-blogs-md-easy-0.2.1 (c (n "blogs-md-easy") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0bsh5pifsb5zsy4m17jb9c92xccx7bkqx97b6rblm0k6r2513njh")))

(define-public crate-blogs-md-easy-0.3.0 (c (n "blogs-md-easy") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "078aqf356crx5vzyxjy0k1va1wb75yygqlir81axlygy04jgvzyl")))

(define-public crate-blogs-md-easy-0.3.1 (c (n "blogs-md-easy") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "16d54rs76q2vy7mhcda25clpfp4nik84xja6naz4x5gg6cnfskyf") (y #t)))

(define-public crate-blogs-md-easy-0.3.2 (c (n "blogs-md-easy") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0md1vyagz3xnsshac9spwjrpnvk0mq9p7kbbwxd1g0cjxjqp8hsk")))

(define-public crate-blogs-md-easy-0.3.3 (c (n "blogs-md-easy") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "0bajak2bqh8f3gvcfv5ivjiilw4ci0r7ik1karkf2vd3qlzhvdma")))

(define-public crate-blogs-md-easy-0.3.4 (c (n "blogs-md-easy") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "01g6r4lkz8wcxjbl28g5i0rhfnmi6kwp7jfa3ndl7kyd2yq2g2qh")))

(define-public crate-blogs-md-easy-0.3.5 (c (n "blogs-md-easy") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1qc578d2s98bvgd8g6n70cq1wl5xzmvfq74q9p8wfdv0r4pfvkwk")))

(define-public crate-blogs-md-easy-0.3.6 (c (n "blogs-md-easy") (v "0.3.6") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "052za4gv6k35c3vvzyrkg02h7cf96p12rdy4ncb3i5r6lzhf4c2x")))

(define-public crate-blogs-md-easy-0.3.7 (c (n "blogs-md-easy") (v "0.3.7") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1i560swlb1bz2zsjwh8iszdbk1hw0xy27i7a6vbz48k8v4kzdjbf")))

(define-public crate-blogs-md-easy-0.3.8 (c (n "blogs-md-easy") (v "0.3.8") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.16") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2.0") (d #t) (k 0)))) (h "1pfj18v5gk2hzx0ixx65lql1gas6zxhjml1mp7bb1ba8rf0kixvc")))

