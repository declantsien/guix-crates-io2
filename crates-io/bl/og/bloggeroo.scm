(define-module (crates-io bl og bloggeroo) #:use-module (crates-io))

(define-public crate-bloggeroo-0.1.0 (c (n "bloggeroo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.11") (d #t) (k 0)) (d (n "tera") (r "^1.19.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1wqbncc1f6fbgqffc6dqhf1fsc9bdbicb03xz0ajnfb9dhxgdqi0")))

