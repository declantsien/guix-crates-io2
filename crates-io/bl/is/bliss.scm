(define-module (crates-io bl is bliss) #:use-module (crates-io))

(define-public crate-bliss-0.1.0 (c (n "bliss") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)))) (h "18vgsg35bhcc0pr5y0gckvrlyhqhzfkx766x5j61rpqfcx23zijj")))

(define-public crate-bliss-0.1.1 (c (n "bliss") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)))) (h "1y7an5vwab5n0rh8jznqy1544k1v9l6ckc1cazs72d290n0bfp70")))

(define-public crate-bliss-0.1.2 (c (n "bliss") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)))) (h "0jzj94gs9fc2s5a0wlql3akifpjgl7phc8x2ys6bzwgs6pfw9b8c")))

(define-public crate-bliss-0.1.3 (c (n "bliss") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)))) (h "02x3w8s76h2b5lhad486bawjllk2sd2p014f57xlqf339njrzl14")))

