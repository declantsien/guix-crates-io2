(define-module (crates-io bl is blissb) #:use-module (crates-io))

(define-public crate-blissb-0.1.0 (c (n "blissb") (v "0.1.0") (d (list (d (n "bitpack") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "1qfpr8z497yypqbjyhqb8xmbdxm0fabk4a8315xxivmd4gp2ssk8") (f (quote (("o") ("iv") ("iii") ("ii") ("i") ("default" "i"))))))

(define-public crate-blissb-0.2.0 (c (n "blissb") (v "0.2.0") (d (list (d (n "bitpack") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "0gx9fhxz57pqdkz26xcfwhpvas91p2fywxzy9xmrfxccff46fdh2") (f (quote (("o") ("iv") ("iii") ("ii") ("i") ("default" "i"))))))

(define-public crate-blissb-0.2.1 (c (n "blissb") (v "0.2.1") (d (list (d (n "bitpack") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "15nqvy7fipr581ji814sim0144gpm6rw3k06c9dz1mpp1xlng8mp") (f (quote (("o") ("iv") ("iii") ("ii") ("i") ("default" "i"))))))

(define-public crate-blissb-0.2.2 (c (n "blissb") (v "0.2.2") (d (list (d (n "bitpack") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "19vmsrc6wzpb7x6iq2sbpiyvcy5scw6glyyqfqzxan8xsf718rsz") (f (quote (("iv") ("iii") ("ii") ("i") ("default" "i"))))))

(define-public crate-blissb-0.2.3 (c (n "blissb") (v "0.2.3") (d (list (d (n "bitpack") (r "^0.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.1") (d #t) (k 0)))) (h "0hiw07d7n5930ha1s6v63sp1brw6q9q2vrj4r91a0xak2mcrbjlj") (f (quote (("iv") ("iii") ("ii") ("i") ("default" "i"))))))

