(define-module (crates-io bl is blisp-repl) #:use-module (crates-io))

(define-public crate-blisp-repl-0.3.7 (c (n "blisp-repl") (v "0.3.7") (d (list (d (n "blisp") (r "^0.3.7") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "07kd5d6i738gi3y3bmkcn10a4m03rjbqm41s9f0w00xf1n8y4mi6")))

(define-public crate-blisp-repl-0.3.8 (c (n "blisp-repl") (v "0.3.8") (d (list (d (n "blisp") (r "^0.3.8") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "19cvkx7zi8f1pryjijyhdbp6a2nqbm4w7snncja29lxn5fl6vwmq")))

(define-public crate-blisp-repl-0.3.9 (c (n "blisp-repl") (v "0.3.9") (d (list (d (n "blisp") (r "^0.3.9") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "0y8mrps1kdrag1536p0y9qfk96z8yx0i086kkf3r4kbrfk8mf58p")))

(define-public crate-blisp-repl-0.4.0 (c (n "blisp-repl") (v "0.4.0") (d (list (d (n "blisp") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "rustyline") (r "^11.0") (d #t) (k 0)))) (h "13sy1b5i7f3lxixl75c2b73474mdanzzx9qjyv2ywal4dr53107y")))

