(define-module (crates-io bl is blis-sys) #:use-module (crates-io))

(define-public crate-blis-sys-0.1.0 (c (n "blis-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1a0rj5rvyvy9g0xhl5psanb9sz57x9dk3dzvqw2713v9n0dkq62n") (f (quote (("static") ("serial") ("pthreads") ("openmp") ("default" "openmp" "static"))))))

(define-public crate-blis-sys-0.1.1 (c (n "blis-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2") (o #t) (d #t) (k 0)))) (h "0zmiyxq0nhjcczka6m2x1rrwl1xf8h99sv9r708vizcxn1fv8mpa") (f (quote (("static") ("serial") ("pthreads") ("openmp" "openmp-sys") ("default" "openmp" "static"))))))

(define-public crate-blis-sys-0.1.2 (c (n "blis-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2") (o #t) (d #t) (k 0)))) (h "17zj1p21an5aw05w44dlbf8cg5rps93m45z181qr26a5n4brf4gk") (f (quote (("static") ("serial") ("pthreads") ("openmp" "openmp-sys") ("default" "openmp" "static"))))))

(define-public crate-blis-sys-0.2.0 (c (n "blis-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2") (o #t) (d #t) (k 0)))) (h "02zccv5x3lg3a47xkq3wsj59bxrrq2974apddwfmkpbagh3x45qi") (f (quote (("static") ("serial") ("pthreads") ("openmp" "openmp-sys") ("default" "openmp" "static"))))))

(define-public crate-blis-sys-0.3.0 (c (n "blis-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "openmp-sys") (r "^1.2") (o #t) (d #t) (k 0)))) (h "1q56qc9vggcxmgpvbdkyy6x5fxxsxrpmd83xza0fj22y0cxkp6qx") (f (quote (("static") ("runtime-dispatch") ("parallel-pthreads") ("parallel-openmp" "openmp-sys") ("default" "static"))))))

