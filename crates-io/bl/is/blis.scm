(define-module (crates-io bl is blis) #:use-module (crates-io))

(define-public crate-blis-0.1.0 (c (n "blis") (v "0.1.0") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1b5k28ikf1cbbgpj4n8qxx63b51n7pd8jscfj9jabd1mnlic3y63")))

(define-public crate-blis-0.1.1 (c (n "blis") (v "0.1.1") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1a51g2nppg7lyks9x73zszn3gykmq4jliwiha4ydz2fsrmlk5gmq")))

(define-public crate-blis-0.2.0 (c (n "blis") (v "0.2.0") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "0sp5i97vc4gwzv6k8p362w7wvyrjnh770w4s8liq86aj1kmcpyc0")))

(define-public crate-blis-0.3.0 (c (n "blis") (v "0.3.0") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1bz3k1p5fzlc9dn0v73jabisg4376z2p379y5sgsn5573bnhhir6")))

(define-public crate-blis-0.3.1 (c (n "blis") (v "0.3.1") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "1yc0kvjzc47wh1jl61zl0x7mrmm488s5m03yz58czrfq18iicpms")))

(define-public crate-blis-0.4.0 (c (n "blis") (v "0.4.0") (d (list (d (n "blis-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.2.0") (d #t) (k 0)))) (h "0lawvc9hknnnhqm72j3xb1n3mx7744ij7z82wsfvhgyagqxxq841")))

(define-public crate-blis-0.5.0 (c (n "blis") (v "0.5.0") (d (list (d (n "blis-sys") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "reborrow") (r "^0.2") (d #t) (k 0)))) (h "0kl3y225zjy3pkv7wgklpqk361fqxwaj9h8m518ldc74hl23rwar")))

