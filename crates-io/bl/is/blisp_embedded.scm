(define-module (crates-io bl is blisp_embedded) #:use-module (crates-io))

(define-public crate-blisp_embedded-0.1.0 (c (n "blisp_embedded") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17243fzg7c1y1s956r6h3wsv0802b593pamar3wjqcc4cz1kr5z6")))

(define-public crate-blisp_embedded-0.1.1 (c (n "blisp_embedded") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15x7jah9rdxahxnpnpwmxycdhxk279nwsy3aqfgqz14kh0p2kxhm")))

