(define-module (crates-io bl is blis-src) #:use-module (crates-io))

(define-public crate-blis-src-0.1.0 (c (n "blis-src") (v "0.1.0") (d (list (d (n "blas-sys") (r "^0.7") (d #t) (k 2)) (d (n "cblas-sys") (r "^0.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.1") (d #t) (k 1)) (d (n "git2") (r "^0.7") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 1)) (d (n "tar") (r "^0.4.15") (d #t) (k 1)))) (h "1k931wi0wz8zj833aibhbghi9s6lsvjkshsgv7pajb63y8ckzdwa") (f (quote (("system") ("static") ("default" "cblas") ("cblas"))))))

(define-public crate-blis-src-0.2.0 (c (n "blis-src") (v "0.2.0") (d (list (d (n "blas-sys") (r "^0.7") (d #t) (k 2)) (d (n "cblas-sys") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (k 2)))) (h "0ca0p43rzv15yni7c855zyd5n9ikdldpw4hccz57n2846qr8s4pg") (f (quote (("system") ("static") ("serial") ("pthreads") ("openmp") ("default" "pthreads" "cblas") ("cblas")))) (l "blis")))

(define-public crate-blis-src-0.2.1 (c (n "blis-src") (v "0.2.1") (d (list (d (n "blas-sys") (r "^0.7") (d #t) (k 2)) (d (n "cblas-sys") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (k 2)))) (h "11nhccs4zplyc2fl4vm8gmm4vs026lv3afp1bbj6cym6rnr823w9") (f (quote (("system") ("static") ("serial") ("pthreads") ("openmp") ("default" "pthreads" "cblas") ("cblas")))) (l "blis")))

(define-public crate-blis-src-0.2.2 (c (n "blis-src") (v "0.2.2") (d (list (d (n "blas-sys") (r "^0.7") (d #t) (k 2)) (d (n "cblas-sys") (r "^0.1.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.154") (d #t) (k 2)))) (h "0ykj5gcs17ifs43awhpjpnlq27q58f8g8ajh08qhd2yfc5krn4fw") (f (quote (("system") ("static") ("serial") ("pthreads") ("openmp") ("default" "pthreads" "cblas") ("cblas")))) (l "blis")))

