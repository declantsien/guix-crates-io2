(define-module (crates-io bl is blisp) #:use-module (crates-io))

(define-public crate-blisp-0.2.0 (c (n "blisp") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "1pshjyk5m9b2fasghymqzgrlc3aq27y6ivks1hg3wr9jkfxpjhh3")))

(define-public crate-blisp-0.2.1 (c (n "blisp") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.3") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "0xnb1zdkr2ja58hwc9hyyal1pmqpzl3jmn4d0j4chird48w0kjnc")))

(define-public crate-blisp-0.3.0 (c (n "blisp") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "078w25gjahxwhhqvzzqz5kxwz72k4cyvmc3m87mdv4ciys4i4m47")))

(define-public crate-blisp-0.3.1 (c (n "blisp") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "171xhn4anrx4a0l6fv4h6f48r5bndxg7frafyvg0dk7lzv766jp1")))

(define-public crate-blisp-0.3.2 (c (n "blisp") (v "0.3.2") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "15130dv4xhn99sqni5hnq8x0dnr0sm96slb009g9g4valwq6wlgm")))

(define-public crate-blisp-0.3.3 (c (n "blisp") (v "0.3.3") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "1ypwamqdii2wizdj7gvbrgbdjpphrp499r641ispzg7iahkf3pmh")))

(define-public crate-blisp-0.3.4 (c (n "blisp") (v "0.3.4") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "11v7smi1pxz3cid7aqkll60m146lyl8p6m23v3f17s9vfdm1in6y")))

(define-public crate-blisp-0.3.5 (c (n "blisp") (v "0.3.5") (d (list (d (n "num-bigint") (r "^0.3.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "036zw1lmdj46d5vwbyas4v73g1k5p1l3pizvkahq9hh1h3gg5awn")))

(define-public crate-blisp-0.3.6 (c (n "blisp") (v "0.3.6") (d (list (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "112p9xc8wzwdzbml5fy38sbgy77zv3ymmjbadm26k7v1281yjay7")))

(define-public crate-blisp-0.3.7 (c (n "blisp") (v "0.3.7") (d (list (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "04hvw8jq2mbnmanppi9dvfl42hnw9yj2y8lhm10z8j7rrlrc596n")))

(define-public crate-blisp-0.3.8 (c (n "blisp") (v "0.3.8") (d (list (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "0gv83afxhkb6x6543fihrhjc0xvwhpyi9cc2r4gp19w24fc1n1f9")))

(define-public crate-blisp-0.3.9 (c (n "blisp") (v "0.3.9") (d (list (d (n "num-bigint") (r "^0.4.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)))) (h "0z1rjblac54y74r2wglm8gpynqyv37ihcbvsa9d37mn5mybvjrmh")))

(define-public crate-blisp-0.4.0 (c (n "blisp") (v "0.4.0") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0174mrfb3wg76w3bvqcb99lbkgya4wi1s2fpkh8fi7ar61q5ijd0")))

(define-public crate-blisp-0.4.1 (c (n "blisp") (v "0.4.1") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1ynbanbp6mw5gffrgzbh94dhm022sknfyfyznz302zfdpwad8s83")))

(define-public crate-blisp-0.4.2 (c (n "blisp") (v "0.4.2") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1kbyikkvvmppj3s6pi8x58c73gj7f8ds3mwh3sv7ds9mzvvz04gv")))

(define-public crate-blisp-0.4.3 (c (n "blisp") (v "0.4.3") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1w3mhn6dnxc05xkadrw05l8amkcapdiprk18qd4942a7xialq6p3")))

(define-public crate-blisp-0.4.4 (c (n "blisp") (v "0.4.4") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0qks5bh6172jqp9m1d6wj5szsdb0krf5zkgmbs7cpnx0mxnvlkk4")))

(define-public crate-blisp-0.4.5 (c (n "blisp") (v "0.4.5") (d (list (d (n "blisp_embedded") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "07pww1pm635h596v4w4n8l809bg93xmag3q014i2b5kws8wj2myz")))

