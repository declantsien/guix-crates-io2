(define-module (crates-io bl is blis-sys2) #:use-module (crates-io))

(define-public crate-blis-sys2-0.2.1 (c (n "blis-sys2") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "blis-src") (r "^0.2.1") (k 0)) (d (n "blis-src") (r "^0.2.1") (f (quote ("serial"))) (k 1)))) (h "0adbz2haq3krg49692py47svkid64lqa5zih92hn706i3x5s266p")))

(define-public crate-blis-sys2-0.2.2 (c (n "blis-sys2") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "blis-src") (r "^0.2.1") (k 0)) (d (n "blis-src") (r "^0.2.1") (f (quote ("serial"))) (k 1)))) (h "0zl2dj0829x7rb1qpkslib58z7jnhcbpc5bcc0aiba3lclk43ncs")))

