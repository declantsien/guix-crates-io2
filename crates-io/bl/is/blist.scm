(define-module (crates-io bl is blist) #:use-module (crates-io))

(define-public crate-blist-0.0.1 (c (n "blist") (v "0.0.1") (d (list (d (n "linked-list") (r "*") (d #t) (k 0)) (d (n "traverse") (r "*") (d #t) (k 0)))) (h "160ci5l33q3g1q52hflah5y2n1i5jswf26nlnlzfmbi1pm5hy07i")))

(define-public crate-blist-0.0.2 (c (n "blist") (v "0.0.2") (d (list (d (n "linked-list") (r "*") (d #t) (k 0)) (d (n "traverse") (r "*") (d #t) (k 0)))) (h "141xgshcx4k0sfh79c5nxg3kyi43djndysf255485xpph4agi8l3")))

(define-public crate-blist-0.0.3 (c (n "blist") (v "0.0.3") (d (list (d (n "linked-list") (r "*") (d #t) (k 0)) (d (n "traverse") (r "*") (d #t) (k 0)))) (h "0pzzg9vmfsw4l761qjd0mka7kwzwzpg7kjvm12zvxfc2ifj2gddb") (f (quote (("nightly"))))))

(define-public crate-blist-0.0.4 (c (n "blist") (v "0.0.4") (d (list (d (n "linked-list") (r "*") (d #t) (k 0)) (d (n "traverse") (r "*") (d #t) (k 0)))) (h "1xdxzkxhf4nflmv9akcaj66awmg0l4x1alczfdxbswg539vyj24j") (f (quote (("nightly"))))))

