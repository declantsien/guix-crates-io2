(define-module (crates-io bl is blissify-rs) #:use-module (crates-io))

(define-public crate-blissify-rs-0.1.0 (c (n "blissify-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bliss-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1brmaxz8m7afck8iyn1adav012ymp6hcm4pvxmqfds4kv8l6l6gh")))

(define-public crate-blissify-rs-0.1.2 (c (n "blissify-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "bliss-audio") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0074lj87rwmsqsyg93400z2sayx7i0gb2qal7wjfxri8a2zmcglj")))

