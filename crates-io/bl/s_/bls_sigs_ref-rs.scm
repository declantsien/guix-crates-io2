(define-module (crates-io bl s_ bls_sigs_ref-rs) #:use-module (crates-io))

(define-public crate-bls_sigs_ref-rs-0.3.0 (c (n "bls_sigs_ref-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "ff") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "pairing-plus") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0v8x0q9b835yy6ia69pq6v4ny41qb4vnmbrb3zcdyxv21jjqmpvk") (y #t)))

(define-public crate-bls_sigs_ref-rs-0.1.0 (c (n "bls_sigs_ref-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "ff") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "pairing-plus") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0kca5jcav1g79b55wjm0mj0dzl46v89w492ib9ia9pv2z0bfn84d") (y #t)))

