(define-module (crates-io bl s_ bls_acml) #:use-module (crates-io))

(define-public crate-bls_acml-0.4.0 (c (n "bls_acml") (v "0.4.0") (d (list (d (n "amcl_wrapper") (r "^0.1.6") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "039smxxvpk31zj7f4bpd1pyrsjvq4vvc85jk9h85w1nigjd049pg") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

(define-public crate-bls_acml-0.5.0 (c (n "bls_acml") (v "0.5.0") (d (list (d (n "amcl_wrapper") (r "^0.2.1") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0w1g5ym89mpcqw4w08cdwik31wqgxb4c1nqd36ly3ncd2vffcp20") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

