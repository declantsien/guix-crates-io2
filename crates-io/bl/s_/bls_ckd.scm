(define-module (crates-io bl s_ bls_ckd) #:use-module (crates-io))

(define-public crate-bls_ckd-0.2.0 (c (n "bls_ckd") (v "0.2.0") (d (list (d (n "curv") (r "^0.10") (f (quote ("num-bigint"))) (k 0) (p "curv-kzen")) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1xjdzbbhv60386gs15nc7w718vxhhxp7zb46abd6z6czmv6m3vqb")))

