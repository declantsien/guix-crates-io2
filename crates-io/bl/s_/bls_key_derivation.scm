(define-module (crates-io bl s_ bls_key_derivation) #:use-module (crates-io))

(define-public crate-bls_key_derivation-0.1.0 (c (n "bls_key_derivation") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1g66gg94xgcyha4abl5g0zmi9rw5jzw2fma5hbrbxnz2874w6z50") (y #t)))

(define-public crate-bls_key_derivation-0.1.1 (c (n "bls_key_derivation") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "19zq5giykly58rvp3h1i9mavj3k77gq3gy46zzaxfnw8318abb1m")))

