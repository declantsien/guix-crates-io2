(define-module (crates-io bl s_ bls_sigs_ref) #:use-module (crates-io))

(define-public crate-bls_sigs_ref-0.1.0 (c (n "bls_sigs_ref") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "ff") (r "^0.4") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "pairing-plus") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "10c060q9c2j1g48h1yhclxzdwplck089das9xpyjzip4wxj6vmv1")))

(define-public crate-bls_sigs_ref-0.2.0 (c (n "bls_sigs_ref") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "ff-zeroize") (r "^0.6.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "pairing-plus") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0mljzjgvnsaj77pmizmp9bc7zb0cgphcpxw7zjb1k0777lb2b2id")))

(define-public crate-bls_sigs_ref-0.3.0 (c (n "bls_sigs_ref") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "ff-zeroize") (r "^0.6.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "pairing-plus") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0kqri797gszmqi5ad2wl9gr2i1i7gjjzjslmkjdrkwbhnxnb84p5")))

