(define-module (crates-io bl s_ bls_ringct) #:use-module (crates-io))

(define-public crate-bls_ringct-0.1.0 (c (n "bls_ringct") (v "0.1.0") (d (list (d (n "bls_bulletproofs") (r "^0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1wgjgcrgchcfcss0s8i5j232ys47ch93kwdscyxq9fajg6jys8cp")))

(define-public crate-bls_ringct-0.2.0 (c (n "bls_ringct") (v "0.2.0") (d (list (d (n "bls_bulletproofs") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "0svf88fl6q7j7yk74s74yrrsix0lm9shwinlpw2qi4y52iqhs3ga")))

(define-public crate-bls_ringct-0.2.1 (c (n "bls_ringct") (v "0.2.1") (d (list (d (n "bls_bulletproofs") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "0y496i2r53mhp901rnps7lfc46yvqamp9a3siymy8gwrc0k32hdn")))

(define-public crate-bls_ringct-1.1.0 (c (n "bls_ringct") (v "1.1.0") (d (list (d (n "bls_bulletproofs") (r "^0.2.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "1frpm659kgkijx033nv9jy6rlvv19dfrf5dl74ifx8k95vsvgwyq")))

(define-public crate-bls_ringct-1.1.1 (c (n "bls_ringct") (v "1.1.1") (d (list (d (n "bls_bulletproofs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "113rbab9zr4bgxcg5wd0y3bqy3axii8b1mx02l3jqnsc77jkz6l0")))

(define-public crate-bls_ringct-1.1.2 (c (n "bls_ringct") (v "1.1.2") (d (list (d (n "bls_bulletproofs") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("sha3"))) (d #t) (k 0)))) (h "0z2rfrsdn1w2i1m7zr9gcxbdrqj6awysb5xzp9d2wrap75qkn4p2")))

