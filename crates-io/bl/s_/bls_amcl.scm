(define-module (crates-io bl s_ bls_amcl) #:use-module (crates-io))

(define-public crate-bls_amcl-0.6.0 (c (n "bls_amcl") (v "0.6.0") (d (list (d (n "amcl_wrapper") (r "^0.2.1") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00sxxylf2rfwwv2makp48jarygxmlmcqbxgxcffv1xhd8rxvgzc5") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

(define-public crate-bls_amcl-0.7.0 (c (n "bls_amcl") (v "0.7.0") (d (list (d (n "amcl_wrapper") (r "^0.2.1") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bng2f75hj3k3f4difa42qkiyqg3s22n6lcnlm3yjri98h53w2ym") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

