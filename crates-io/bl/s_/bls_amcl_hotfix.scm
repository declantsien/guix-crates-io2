(define-module (crates-io bl s_ bls_amcl_hotfix) #:use-module (crates-io))

(define-public crate-bls_amcl_hotfix-0.7.1 (c (n "bls_amcl_hotfix") (v "0.7.1") (d (list (d (n "amcl_wrapper") (r "^0.3") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1qhpfnvkhy2sa8zdil4qxv8va8ni93bk86awcs6bmp3man30xal3") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

(define-public crate-bls_amcl_hotfix-0.7.2 (c (n "bls_amcl_hotfix") (v "0.7.2") (d (list (d (n "amcl_wrapper") (r "^0.3") (f (quote ("bls381"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "secret_sharing") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "16minrh5gy8h5miy3vn55g7gg49j8dp2nf97dmgdkyia80108kh1") (f (quote (("default" "SignatureG2") ("SignatureG2") ("SignatureG1"))))))

