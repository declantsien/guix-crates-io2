(define-module (crates-io bl ih blih) #:use-module (crates-io))

(define-public crate-blih-0.0.1 (c (n "blih") (v "0.0.1") (d (list (d (n "blih_rs") (r "^0.0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (d #t) (k 0)))) (h "0vpcyiqx8m5px21bh5c1ih07bn9r7aj7cfjc13p800v3lcmyrg0z")))

(define-public crate-blih-0.0.2 (c (n "blih") (v "0.0.2") (d (list (d (n "blih_rs") (r "^0.0.4") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (d #t) (k 0)))) (h "1gx7cr7scr49ss37i4136sxh80jdc7z67cisw1h4b5id6xqibgl6")))

(define-public crate-blih-0.0.4 (c (n "blih") (v "0.0.4") (d (list (d (n "blih_rs") (r "^0.0.6") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0yn6ixc5c7m5pxq3d3518w50ddq521hdfdjz98xdch3d521fgwdm")))

(define-public crate-blih-0.1.0 (c (n "blih") (v "0.1.0") (d (list (d (n "blih_rs") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0kq85z4l1hcr8gh3pfvzn63naq8b4z8wkgjyq7cyy6ffarcmsrh0")))

(define-public crate-blih-0.1.1 (c (n "blih") (v "0.1.1") (d (list (d (n "blih_rs") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1i0qf5my7dj2whzs99zrrf70v7s50i6b62n293kvx59bqravg736") (f (quote (("default") ("config"))))))

(define-public crate-blih-0.1.2 (c (n "blih") (v "0.1.2") (d (list (d (n "blih_rs") (r "~0.1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "1lxm9qj1v62f64j21y6vkcg7q1ywzryl6858iw5b4kavdsir356m") (f (quote (("default") ("config"))))))

(define-public crate-blih-0.1.3 (c (n "blih") (v "0.1.3") (d (list (d (n "blih_rs") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "06dj860hkx3xhis56l00lskydm0yjpfv012a59c7iw4h7yzgjm8a") (f (quote (("default") ("config")))) (y #t)))

(define-public crate-blih-0.1.4 (c (n "blih") (v "0.1.4") (d (list (d (n "blih_rs") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)))) (h "0z0nqvr4mh4vkhlyk0hzh76prqi4jcb2lksisgsc9l5s1iw83qli") (f (quote (("default") ("config"))))))

