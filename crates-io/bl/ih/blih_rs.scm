(define-module (crates-io bl ih blih_rs) #:use-module (crates-io))

(define-public crate-blih_rs-0.0.1 (c (n "blih_rs") (v "0.0.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0bbbpfn7dahawz6hsimmf3r4596grnpacdfjmfgv7hfhp3wgr7zl")))

(define-public crate-blih_rs-0.0.2 (c (n "blih_rs") (v "0.0.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0x2rcppaxkmgpxm0dwhd20na6z2xnf7r0rfai4aka6gbp6b3vpvx")))

(define-public crate-blih_rs-0.0.3 (c (n "blih_rs") (v "0.0.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1p3499gbr5gh03hhw1z5dplxl7kq3mbx27pg8vf3arw7id6gigka")))

(define-public crate-blih_rs-0.0.4 (c (n "blih_rs") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1gqkplfgpmv3498sn0n74llb3k4cybzdypxs8ygxhir8bqvp15kx")))

(define-public crate-blih_rs-0.0.6 (c (n "blih_rs") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "native-tls"))) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1hbq60kjz3qs8hkg4b3ci8jrxn9cjqlahjwn9dixknkzamf1lngz")))

(define-public crate-blih_rs-0.1.0 (c (n "blih_rs") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "native-tls"))) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0m7pvlhg132qg8p6h0bkdi7dgvhcqfi71smzbiyqcsgr1r2wdq68")))

(define-public crate-blih_rs-0.1.1 (c (n "blih_rs") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "native-tls"))) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0yxx0jf2kvirfpvkidaakk9y6h0rl1dbiyp5pr8il1kwp2m49d0l")))

(define-public crate-blih_rs-0.1.2 (c (n "blih_rs") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "native-tls"))) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "09igmnvjhmzjz400xxmjzlazh58hxqxckndkf7pn9zjsgwlhqxil")))

(define-public crate-blih_rs-0.2.0 (c (n "blih_rs") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "~0.10") (f (quote ("blocking" "native-tls"))) (k 0)) (d (n "rpassword") (r "^4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1lrpfn422mwnva9qj3vb42r236c0y63d3f4ccwjxis4ysfd9rf7w")))

