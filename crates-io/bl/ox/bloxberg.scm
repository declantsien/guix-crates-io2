(define-module (crates-io bl ox bloxberg) #:use-module (crates-io))

(define-public crate-bloxberg-0.1.0 (c (n "bloxberg") (v "0.1.0") (d (list (d (n "capstone") (r "^0.7.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0qmiwldza1zdh84r0x2skrc679fcm25aq8h2pf6vbdvkxqfq202f")))

