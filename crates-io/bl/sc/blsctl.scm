(define-module (crates-io bl sc blsctl) #:use-module (crates-io))

(define-public crate-blsctl-0.2.0 (c (n "blsctl") (v "0.2.0") (d (list (d (n "serial_test") (r "^0.2") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.7") (d #t) (k 2)))) (h "0s175isjlmxihc8ia5msfd74ykx39ngdhx629m43cbj5609d4wyl")))

(define-public crate-blsctl-0.2.1 (c (n "blsctl") (v "0.2.1") (d (list (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "085jmq5dkas9qnd6qxkkbfhl2mr04mjihvwlpni9si7cyz9664nx")))

(define-public crate-blsctl-0.2.2 (c (n "blsctl") (v "0.2.2") (d (list (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0pcx0agj4sl7q97dy5f9hrfwymznwkh31xqjkwddv6dn5ac8xp49")))

(define-public crate-blsctl-0.2.3 (c (n "blsctl") (v "0.2.3") (d (list (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "serial_test_derive") (r "^0.5") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0i75gp72ib1qw479y107x700p9vafmafmy1xvykyfryra90m1yv5")))

