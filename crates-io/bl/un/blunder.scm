(define-module (crates-io bl un blunder) #:use-module (crates-io))

(define-public crate-blunder-0.1.0 (c (n "blunder") (v "0.1.0") (h "0gfnha6kgc7gfsl3kv6aw1a39zcsp1pwyyhzjd7y055bl6bnnnw6")))

(define-public crate-blunder-0.1.1 (c (n "blunder") (v "0.1.1") (h "10837j509zwc1af1jzygkpwr3qis9xmn0982fhvch1l3zfkdl3pb")))

(define-public crate-blunder-0.1.2 (c (n "blunder") (v "0.1.2") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1h0gqps3bxjcmy1ssbkb2lhd7kjdggbcj4dbc737j6ksnxgbkr5m")))

(define-public crate-blunder-0.1.3 (c (n "blunder") (v "0.1.3") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "15axda1xmymkry538fj0dz8gakr0gnxrh4bcmajdnnnmjw172nr6")))

(define-public crate-blunder-0.1.4 (c (n "blunder") (v "0.1.4") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "09236ijrxlm67b3v1f5i3fl5ygy00yq01c4xacs8p0g6nqg77als")))

(define-public crate-blunder-0.1.5 (c (n "blunder") (v "0.1.5") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0hqxwr54f4fp2wnlzp3blazgs018ab5zp0ijkv2pqr5klncc10y7")))

(define-public crate-blunder-0.1.7 (c (n "blunder") (v "0.1.7") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1qm9pinmh7cbavr9jmbnh6wbw5ay3w2rg2d59q9b6c26an2q4zbr")))

(define-public crate-blunder-0.2.0 (c (n "blunder") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "132jzzfq5cq63np5hs93d39n3jpxns2f05ylh611lrmrmgr4q3kg")))

(define-public crate-blunder-0.2.1 (c (n "blunder") (v "0.2.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0hv3088iqxhm3syqiqq6jd0s6fcz3fsg6snhmwjx1p6a7jcnqvad")))

