(define-module (crates-io bl un blunders-engine) #:use-module (crates-io))

(define-public crate-blunders-engine-0.1.0 (c (n "blunders-engine") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "04mf70ca8cvab3q7j1pykcppf3vb829q3c8jsns2g668k7k66ppj")))

