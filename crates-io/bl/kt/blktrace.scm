(define-module (crates-io bl kt blktrace) #:use-module (crates-io))

(define-public crate-blktrace-0.1.0 (c (n "blktrace") (v "0.1.0") (h "0rllmb69pwh809wq2pfrl5098ks4x32245m5shzz4ikgjgbnygjn") (y #t)))

(define-public crate-blktrace-0.1.1 (c (n "blktrace") (v "0.1.1") (h "0rsy5jynri121lfimiswl8rajakl101yh7xl8wgcqa2rrhjjlibk")))

(define-public crate-blktrace-0.1.2 (c (n "blktrace") (v "0.1.2") (h "0iddifa7l4wh1n8c1936f71hdy9f8v8kcwhqq85gmd8nd5i017yg")))

