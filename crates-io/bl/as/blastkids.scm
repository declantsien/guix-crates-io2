(define-module (crates-io bl as blastkids) #:use-module (crates-io))

(define-public crate-blastkids-0.1.0 (c (n "blastkids") (v "0.1.0") (d (list (d (n "bls12_381_plus") (r "^0.8.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "112ap50k6w82yahvl702gb0y3cbv5ly5cwh9mvqghvnim4m6mv9n")))

(define-public crate-blastkids-0.1.1 (c (n "blastkids") (v "0.1.1") (d (list (d (n "bls12_381_plus") (r "^0.8.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1qk5jpj141zkq7acxmqiq6p2c1b6ln535wnfm629vsv89m1sn6zp")))

(define-public crate-blastkids-0.1.2 (c (n "blastkids") (v "0.1.2") (d (list (d (n "bls12_381_plus") (r "^0.8.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0mqps1wf6sxdi8017iw1cvhrh7zbrd23ihq9m00x085gmhdmi9wx")))

(define-public crate-blastkids-0.1.3 (c (n "blastkids") (v "0.1.3") (d (list (d (n "bls12_381_plus") (r "^0.8.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1zvsk0kv72jdfi0vqf4m0f3f2fyay1by5ybrr03fxi00snnwc741")))

(define-public crate-blastkids-0.1.4 (c (n "blastkids") (v "0.1.4") (d (list (d (n "bls12_381_plus") (r "^0.8.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1y8nn3whm3gdc8nnkvsj5sv6arffxpr82r88pzhxzfqfvfj90b0n")))

(define-public crate-blastkids-0.1.5 (c (n "blastkids") (v "0.1.5") (d (list (d (n "bls12_381_plus") (r "^0.8.13") (f (quote ("expose-fields" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0vz4znxg2ywm3axrrk2vi3847qzm9hwak7rcl1mg0mhip5k125v8")))

(define-public crate-blastkids-0.1.6 (c (n "blastkids") (v "0.1.6") (d (list (d (n "bls12_381_plus") (r "^0.8.13") (f (quote ("expose-fields" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0ixw5fycq5r0alsfbjqsyy1ipzhximbk63az00l61jcznn5pfsrc")))

