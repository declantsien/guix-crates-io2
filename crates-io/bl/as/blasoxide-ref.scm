(define-module (crates-io bl as blasoxide-ref) #:use-module (crates-io))

(define-public crate-blasoxide-ref-0.1.0 (c (n "blasoxide-ref") (v "0.1.0") (h "1a6zbisybjsp94aiqqrav073bfwgv2sdsfn8l0wkpxi0zzhlcpxk")))

(define-public crate-blasoxide-ref-0.2.0 (c (n "blasoxide-ref") (v "0.2.0") (h "0a12nb65syfh3j1wz49gxcpij7z9qh54nzsfln3q792qcg8kz3ll")))

