(define-module (crates-io bl as blastdb-sequence-util) #:use-module (crates-io))

(define-public crate-blastdb-sequence-util-0.1.0 (c (n "blastdb-sequence-util") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1q97dvqfkrkgq0qx5cn5zi4mmhx137qcdw69939jv989gxh001g5")))

