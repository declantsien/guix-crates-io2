(define-module (crates-io bl as blaster) #:use-module (crates-io))

(define-public crate-blaster-0.0.0 (c (n "blaster") (v "0.0.0") (d (list (d (n "blas") (r "^0.20.0") (d #t) (k 2)) (d (n "openblas-src") (r "^0.7.0") (d #t) (k 2)))) (h "0zw28p8pfhh051f41h56aa3pd8ljx512l9kyf9cj7awd597c4w56")))

