(define-module (crates-io bl as blasphemy) #:use-module (crates-io))

(define-public crate-blasphemy-0.0.1 (c (n "blasphemy") (v "0.0.1") (d (list (d (n "rustml") (r "^0.0.7") (d #t) (k 0)))) (h "1v3fjdcbbmxgwfmr4v79idr6kdbaf31s4wxvcfg2fc7y39v2r2p8")))

(define-public crate-blasphemy-0.0.2 (c (n "blasphemy") (v "0.0.2") (d (list (d (n "rustml") (r "^0.0.7") (d #t) (k 0)))) (h "1zw23db8p11kq15klfi711s2s2j93qrmlsbpnjq5gjwykdsv2ghi")))

