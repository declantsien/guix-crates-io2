(define-module (crates-io bl as blasoxide-cpuinfo) #:use-module (crates-io))

(define-public crate-blasoxide-cpuinfo-0.1.0 (c (n "blasoxide-cpuinfo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("winnt" "sysinfoapi" "errhandlingapi" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0mw1cbq047r2s97xjr8vsrz5ck89n7vybr0dv68h0vdrbdyi82qr")))

(define-public crate-blasoxide-cpuinfo-0.2.0 (c (n "blasoxide-cpuinfo") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.58") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0)) (d (n "winapi") (r "^0.3.7") (f (quote ("winnt" "sysinfoapi" "errhandlingapi" "winerror"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0f0fw5g7v79c5dinkbnwky316176fyky5j7nskyy5kpd6s4h2j05")))

