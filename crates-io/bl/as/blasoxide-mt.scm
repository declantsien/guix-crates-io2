(define-module (crates-io bl as blasoxide-mt) #:use-module (crates-io))

(define-public crate-blasoxide-mt-0.1.0 (c (n "blasoxide-mt") (v "0.1.0") (d (list (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0f14vcxbrk18hvwyvhqji3gmabn4dwvlrkbns6sqcxf47yqc3jxs")))

(define-public crate-blasoxide-mt-0.1.1 (c (n "blasoxide-mt") (v "0.1.1") (d (list (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1pp1i2b38lpdyqhnacywfqq5x4jsxvn5gfnmrr7mcpifj6pvzw4j")))

