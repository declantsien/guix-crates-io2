(define-module (crates-io bl as blas) #:use-module (crates-io))

(define-public crate-blas-0.0.1 (c (n "blas") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "1af7vxr7rgg6sizsff5p5c205yhjabib7rcknamvfqidsmyf38hv")))

(define-public crate-blas-0.0.2 (c (n "blas") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)) (d (n "libblas-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1z4rnbq1bvnibbjjv3bhbd5iz4p0xjiakh11q2vw6vsaa4zzrfzq")))

(define-public crate-blas-0.0.3 (c (n "blas") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0jgbd92qiqhhwncsrkw86py6g7sy31jkapdc0xxfrwp303ksxlgr")))

(define-public crate-blas-0.0.4 (c (n "blas") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1gic69mm0zh70snxrx4c4p8jbm8k7svfm115faagk82rkxl7n7q1")))

(define-public crate-blas-0.0.5 (c (n "blas") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.4") (d #t) (k 0)))) (h "00i9pjgbbahh9h4ljahg65r806wdg29zc70mmcdm9hcnicz9z4fb")))

(define-public crate-blas-0.0.6 (c (n "blas") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.5") (d #t) (k 0)))) (h "11jzfsfcgdl5gw67l1mdq638ylmwrvlr1g4bqn2i8940w4zzaf4d")))

(define-public crate-blas-0.0.7 (c (n "blas") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0j89iia46s3kipkasw8himpw39gvghb089i1dnwsncaayp5ilsvc")))

(define-public crate-blas-0.0.8 (c (n "blas") (v "0.0.8") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0limmlwd2v1ngnxhn608d7zc6c4ndny13pigcj1lfphj9rykpb51")))

(define-public crate-blas-0.0.9 (c (n "blas") (v "0.0.9") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0kpm1y31h6f79v98da0sxjrfmjnwffxl1ncvkn13zclk8hfgrxr6")))

(define-public crate-blas-0.0.10 (c (n "blas") (v "0.0.10") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0wcviql2i488fqm2skvfn2j4jfhrr6jpyskj09iy85zh707wfvv8")))

(define-public crate-blas-0.0.11 (c (n "blas") (v "0.0.11") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.8") (d #t) (k 0)))) (h "1k2idfy1gb0m4qrx9b8x5i5nd3qms3iar61dr3k952gdpkmysnl6")))

(define-public crate-blas-0.0.12 (c (n "blas") (v "0.0.12") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.12") (d #t) (k 0)))) (h "1jpfxlw5nww2va3wb8kv9yv6q4r3i9fm71lk6sn08ghnlbl0fj38")))

(define-public crate-blas-0.0.13 (c (n "blas") (v "0.0.13") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.13") (d #t) (k 0)))) (h "055jwdpb34fs9ngwd69mb3b52fz1w364rqsdp42ys39ddip0j74x")))

(define-public crate-blas-0.0.14 (c (n "blas") (v "0.0.14") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "^0.0.14") (d #t) (k 0)))) (h "026skxlz6cll11clrl9z694zh8qsnjbskfbbyc7mba4039fzck27")))

(define-public crate-blas-0.0.16 (c (n "blas") (v "0.0.16") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0p50gnzg7rylx1adp0s88xmgw10k1qhqjp5lcsi3gv1r7rylm4q0")))

(define-public crate-blas-0.0.17 (c (n "blas") (v "0.0.17") (d (list (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0q2nhhw7ps1c1fkml1bs32pdqsd3myc8vm0b0sfrbkx1v4bcpvqb")))

(define-public crate-blas-0.0.18 (c (n "blas") (v "0.0.18") (d (list (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1amj44603wcfgg968xy5rbrx048205m63vr3bb09yysbvh3axhhd")))

(define-public crate-blas-0.0.19 (c (n "blas") (v "0.0.19") (d (list (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1d2m4i9bmab2bsafysj3in7cr91gnjkj8pw22w3pxd138yfkiamf")))

(define-public crate-blas-0.1.0 (c (n "blas") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1nfk77yn1ib7lgp0q1vv8zxxkcv9v67fnhvj7azp1860g7y4j6y5")))

(define-public crate-blas-0.2.0 (c (n "blas") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0wc38jaby5ny1zziyk2b9whbp3pa0sngpj9q31nsv62mp4gfy8wr")))

(define-public crate-blas-0.3.0 (c (n "blas") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0g17954a5yviq40c6vdrn9xg45wc8iwly9ipga51syr23giw8n3l")))

(define-public crate-blas-0.3.1 (c (n "blas") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libblas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "16fcn9pm0dk1v0bq4r0dk5x5c2a10gyfrcnn4z8bj8n84ss83h8f")))

(define-public crate-blas-0.3.2 (c (n "blas") (v "0.3.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0lndhjhqfdj8x6i0qlwhwm1zhfjb4ak9nyq0k78xhlvi5hgipay4")))

(define-public crate-blas-0.4.0 (c (n "blas") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "04k71mg6nqadg44hzm9m5j1cw750vbnxryl3pxb05cw37schdb7g")))

(define-public crate-blas-0.5.0 (c (n "blas") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "complex") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1707gsba27w0jga79sczjwvwbmz9lkvmlydqg38ffshnnff1x3xw")))

(define-public crate-blas-0.6.0 (c (n "blas") (v "0.6.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "complex") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0rhjwjnfzy855whpp3i3ir6y5d1ck74sapgg7k2hnwhzag6yr805")))

(define-public crate-blas-0.6.1 (c (n "blas") (v "0.6.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "complex") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1k8rhivdfv04lkj6hfrcs5i2xmzgcrivrafl71gv28awy78mqjrw")))

(define-public crate-blas-0.6.2 (c (n "blas") (v "0.6.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas-sys") (r "*") (d #t) (k 0)) (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0jqjvkhw5nsqbdjrm6hlqyr37y8nmk0ac9jqhf5l275dhwa6gisy")))

(define-public crate-blas-0.7.0 (c (n "blas") (v "0.7.0") (d (list (d (n "blas-sys") (r "^0.1") (d #t) (k 0)) (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1k8160iqcm51rfsxwi4b4cfijlb269xbmvf219yg7wbh43vz2j8x")))

(define-public crate-blas-0.8.0 (c (n "blas") (v "0.8.0") (d (list (d (n "blas-sys") (r "^0.2") (k 0)) (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1bndfxqcn9hm5nnmpwmhdb6lmp5nv4sdagz55igxkvlblxh5wwp7") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.9.0 (c (n "blas") (v "0.9.0") (d (list (d (n "blas-sys") (r "^0.2") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "13prwjpm1q7v0waasyr58n45xfqhi1a1w9fqvlzkb2jjl8yv0spl") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.9.1 (c (n "blas") (v "0.9.1") (d (list (d (n "blas-sys") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "0g45128n729mkbigqz27k0iwpxaglzmbl3910yzrb8crjkdh8cas") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.10.0 (c (n "blas") (v "0.10.0") (d (list (d (n "blas-sys") (r "^0.3") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1i884740gq3r403b5lgdkchyq688092rvbiqgm3x3pi496rwir5j") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.11.0 (c (n "blas") (v "0.11.0") (d (list (d (n "blas-sys") (r "^0.4") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "11am07iwnigp92r8nybljardh184mah30c1i7zdz4q745hj2biz3") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.12.0 (c (n "blas") (v "0.12.0") (d (list (d (n "blas-sys") (r "^0.4") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1p0wm8kiiaxzk91vzfnrnnx7v5aawbv9pqw2xv7vb1mmz8ysjpxl") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.13.0 (c (n "blas") (v "0.13.0") (d (list (d (n "blas-sys") (r "^0.5") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1jkwj5asy377z6n0h7sh6pxv72lg6xgmq0qvld82wlb85rv2w2x1") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.14.0 (c (n "blas") (v "0.14.0") (d (list (d (n "blas-sys") (r "^0.5") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1y5arksfk9w0hg9vfwb14zxb0m4q5blvdpx6dags1aqrf1g3ma5i") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.0 (c (n "blas") (v "0.15.0") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "0vmm5rsfc90x8hva0jbkwxi3w9rik1g4fxj172cxvicmgcs6kjlx") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.1 (c (n "blas") (v "0.15.1") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0c8d2h06ghvrimq46wk6civ0k57m4rgh9ndmk01rsx0nq5g3zxh4") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.2 (c (n "blas") (v "0.15.2") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0f7nw9k438r979swh1mmpi3rrr4rc4jfzxgjz529p6y9m3sdxric") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.3 (c (n "blas") (v "0.15.3") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0cxfb8qmkqxqf7jp9m64ck6v1jk2m9dfpfxxxm7a4k2x57b1b4nq") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.4 (c (n "blas") (v "0.15.4") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "06rvhv7chd7mnl0ys7ca7baw1xdnclprkln6zmrlx6czb7z5icjy") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.15.5 (c (n "blas") (v "0.15.5") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "151zc8c4vsdbsyh0rq4glrd4i1bdba5zqp4ys0mna0s7wf7wxz4r") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.16.0 (c (n "blas") (v "0.16.0") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1zs9p2cvdbmq96rk7vz443br87ski75p9rd90qlym85n7qmnw7d2") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.16.1 (c (n "blas") (v "0.16.1") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0vz678vyzbpb7ngmjqrm6h7nq3bbmx75qfnyzzxv5kyh8l96v7br") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.17.0 (c (n "blas") (v "0.17.0") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1i1cdajarcmkl2lriww191q4a44rvwnnp54ah802aaixwlc76b94") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.18.0 (c (n "blas") (v "0.18.0") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1xr6aa0yhdbdn79vs7vg9v9ybd8pr29qqbl40v22b9mcldcm632g") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.18.1 (c (n "blas") (v "0.18.1") (d (list (d (n "blas-sys") (r "^0.6") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0sr10pfad33z1hg1g9l1amii19jpi2dzv2h90651i7l8a8z13569") (f (quote (("openblas" "blas-sys/openblas") ("netlib" "blas-sys/netlib") ("default" "openblas") ("accelerate" "blas-sys/accelerate"))))))

(define-public crate-blas-0.19.0 (c (n "blas") (v "0.19.0") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1m0j48agychgj795fbpc1wgd0n7lq50f4gjhq28kz3xx2q6vlzrr")))

(define-public crate-blas-0.19.1 (c (n "blas") (v "0.19.1") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0523dqirfnjdnr3jwl4skwnmfqw14hbmbnqnl90fal7w1dxrssnd")))

(define-public crate-blas-0.19.2 (c (n "blas") (v "0.19.2") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "1y7ay0k7xakmaf377nnkis21qv69g0107vyh9pzmf1iszdd329yw") (y #t)))

(define-public crate-blas-0.20.0 (c (n "blas") (v "0.20.0") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "19qvy3sw4kzrc0w5q070fgialw27lrwf41hbg07ygrigkwwvdcz4")))

(define-public crate-blas-0.21.0 (c (n "blas") (v "0.21.0") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (k 0)))) (h "0q6h6jx1b8fi0xx4shgbfw5wzgsdc00mq6pvwix8qrngh6j5bi3z")))

(define-public crate-blas-0.22.0 (c (n "blas") (v "0.22.0") (d (list (d (n "blas-sys") (r "^0.7") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "1p1rs9y8fpxmrh9jj1rf4q517x5h960v4jf30f1gwnr1qdshz65f")))

