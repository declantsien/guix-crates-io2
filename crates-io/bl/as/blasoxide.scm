(define-module (crates-io bl as blasoxide) #:use-module (crates-io))

(define-public crate-blasoxide-0.1.0 (c (n "blasoxide") (v "0.1.0") (h "00vzb3rqdsnrqxzbd8v0z9abjdl087fb4wxgr6dqlah629ghnqjv")))

(define-public crate-blasoxide-0.1.1 (c (n "blasoxide") (v "0.1.1") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0pb7kgkp98chfr748flgzzmcnwfi5kznj29px2mqian51h6h64il")))

(define-public crate-blasoxide-0.1.2 (c (n "blasoxide") (v "0.1.2") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1m8ycjsd3lqpg2syasqfgm0vyx6bc7n880gwdirqzr03zy1hkdvj")))

(define-public crate-blasoxide-0.1.3 (c (n "blasoxide") (v "0.1.3") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1zxr5c8x2c3dw4mg40wbn53l3gkscv3lba3xa2yg3f7pvmrn02ni")))

(define-public crate-blasoxide-0.2.0 (c (n "blasoxide") (v "0.2.0") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1a64isp8ymg4wm8inwajpr02wiqc5l3c48fyf7bwl50g0gbgl3a9")))

(define-public crate-blasoxide-0.3.0 (c (n "blasoxide") (v "0.3.0") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0x76na2q7kg1b4ad50b4qrv88gsp2j7c20a8df5zgr2jxjmvql7a")))

(define-public crate-blasoxide-0.3.1 (c (n "blasoxide") (v "0.3.1") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1aygqabrqwr1rz2gdprl613f4f71vs9maxqiwl4licslrkfjx11f")))

(define-public crate-blasoxide-0.3.2 (c (n "blasoxide") (v "0.3.2") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1w4iy468h78x20aqi400ryqkjixxnaw21p3ddlfzagq4jlcsc95s")))

