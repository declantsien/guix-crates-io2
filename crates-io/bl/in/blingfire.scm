(define-module (crates-io bl in blingfire) #:use-module (crates-io))

(define-public crate-blingfire-0.1.0 (c (n "blingfire") (v "0.1.0") (d (list (d (n "blingfire-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "failchain") (r "^0.1015.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0arggc4gmnx30g2fsxnj6pllkc00vhkqbnrld6graaz23arl3snn")))

(define-public crate-blingfire-0.2.0 (c (n "blingfire") (v "0.2.0") (d (list (d (n "blingfire-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "failchain") (r "^0.1015.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)))) (h "1ixz1n1rmdndcsa3ikacrhd1afkjkglxk301i5p7s3kidky8h46m")))

(define-public crate-blingfire-0.2.1 (c (n "blingfire") (v "0.2.1") (d (list (d (n "blingfire-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "failchain") (r "^0.1015.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)))) (h "1qrpc98f65x3kr475hiavb4lr2ynwyh9c12rs0v205758q8d3ykn")))

(define-public crate-blingfire-0.3.0 (c (n "blingfire") (v "0.3.0") (d (list (d (n "blingfire-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)) (d (n "snafu") (r "^0.4.3") (d #t) (k 0)))) (h "0v8qswsl0hbkrz9hcwj8f9igsni4j5h43043krjx1f8vx1rvxlrk")))

(define-public crate-blingfire-1.0.0 (c (n "blingfire") (v "1.0.0") (d (list (d (n "blingfire-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1zlhaxvpy22qa80z4shavs7lphr4h0jlqp0s77r187fnp8sqkyh3")))

