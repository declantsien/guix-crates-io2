(define-module (crates-io bl in blinkstick-rs) #:use-module (crates-io))

(define-public crate-blinkstick-rs-1.0.0 (c (n "blinkstick-rs") (v "1.0.0") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "125p2s7x9531jq6q00i4i42qngwplpc512di1scqzlsw1022a64r") (y #t)))

(define-public crate-blinkstick-rs-1.0.1 (c (n "blinkstick-rs") (v "1.0.1") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0m33zkxbzpl8w3zs7nksiladkmvdy8m9n58wrx4z3zk9fmvb7arj") (y #t)))

(define-public crate-blinkstick-rs-1.0.2 (c (n "blinkstick-rs") (v "1.0.2") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0phl7dyg3wigmbvn1c9aq9ylh3916agrvlssyc7az0c3dagsf43q") (y #t)))

(define-public crate-blinkstick-rs-0.1.2 (c (n "blinkstick-rs") (v "0.1.2") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "1nr1p14waxf6fzy507cjlksaswmblrdh6zpwp8z64mkfpnagk5r5")))

(define-public crate-blinkstick-rs-0.2.0 (c (n "blinkstick-rs") (v "0.2.0") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0wgpw63r7vbz1jvfc36gdaqc17fpvvvxzngk8znygdq0z94dqw5m")))

(define-public crate-blinkstick-rs-0.2.1 (c (n "blinkstick-rs") (v "0.2.1") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0rh9lvvdh4jsvpz551v2s6cwls910wmy79c32psp55030l55dq0v")))

(define-public crate-blinkstick-rs-0.2.2 (c (n "blinkstick-rs") (v "0.2.2") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0rsmvma655a9x0bm1n8ax24z88sfs1if4qjpmm1aj0diq16fvs4c")))

(define-public crate-blinkstick-rs-0.2.3 (c (n "blinkstick-rs") (v "0.2.3") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ryvsy1rs93s16cps4gxbl31fl857n1ld04d7dm545hbjrv54wsa")))

(define-public crate-blinkstick-rs-0.3.0 (c (n "blinkstick-rs") (v "0.3.0") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0lm0j70ckrq29f8vf31yibjcyk9ml0mr72w0rg44sz0awabvk4gb")))

(define-public crate-blinkstick-rs-0.3.1 (c (n "blinkstick-rs") (v "0.3.1") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ldwbwc9l12qsk66dszwxcdp6r8f6f07a2n7fmdskwa7771mxysk")))

(define-public crate-blinkstick-rs-0.3.2 (c (n "blinkstick-rs") (v "0.3.2") (d (list (d (n "hidapi") (r "^1.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1qpajn0nlkkbg2sfa8jbv6wmawq2bf7b4dhhwfpb0j21kg1khw2k")))

