(define-module (crates-io bl in blinkrs) #:use-module (crates-io))

(define-public crate-blinkrs-0.1.0 (c (n "blinkrs") (v "0.1.0") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0gyi68lfqlkh6qsa7ivci7srxq4y7xd8z8jam5ppdhsss6d9sqyx")))

(define-public crate-blinkrs-0.2.0 (c (n "blinkrs") (v "0.2.0") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0r91p6ql9yvd742klkhscp6lfyrh4cxsp4kgxhkmf9mxfpzc4mnh")))

(define-public crate-blinkrs-0.2.1 (c (n "blinkrs") (v "0.2.1") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1pbv5kvkq7iybr52khidhcjkvb0r18lv9dwmvxlgnzrk74wbr52n")))

(define-public crate-blinkrs-1.0.0 (c (n "blinkrs") (v "1.0.0") (d (list (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1narzcs8lsvcphnpxx9q41h0m5hvlxpzfjaixyv65pvj5pjvs58v")))

(define-public crate-blinkrs-1.0.1 (c (n "blinkrs") (v "1.0.1") (d (list (d (n "rusb") (r "^0.6.5") (d #t) (k 0)))) (h "0ac40qxppa255d8h0sqxllhazbyqlj9bbn7wnc27naj3f2yfpw96")))

(define-public crate-blinkrs-1.0.2 (c (n "blinkrs") (v "1.0.2") (d (list (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "1svq5mp5s0sxp7pl7siw07hd98z7gnxxrydhg79z8v0j74iaabfj")))

(define-public crate-blinkrs-2.0.0 (c (n "blinkrs") (v "2.0.0") (d (list (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1b7gahz9ibya9jl7ipa2wdia89p2337a2pfn2325gbxn5dzrg88g")))

(define-public crate-blinkrs-2.0.1 (c (n "blinkrs") (v "2.0.1") (d (list (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "0hhhk25542rkkn2xdrw65rs7jhj8sask1dj0323glxylhc16v9db")))

