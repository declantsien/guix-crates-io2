(define-module (crates-io bl in blinkt_cdev) #:use-module (crates-io))

(define-public crate-blinkt_cdev-0.1.0 (c (n "blinkt_cdev") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1asyaq6nqjvdb13ps9g3mlqsjf3isbl56y8cpflpy8ifp67hgjik")))

