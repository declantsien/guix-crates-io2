(define-module (crates-io bl in blind_typing) #:use-module (crates-io))

(define-public crate-blind_typing-0.1.0 (c (n "blind_typing") (v "0.1.0") (d (list (d (n "chatgpt_rs") (r "^1.2.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ml6751zavafn7lrxx0jj77jxg6bak4fdl5d3823vn477hx3p79n")))

