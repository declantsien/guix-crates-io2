(define-module (crates-io bl in blinmaker) #:use-module (crates-io))

(define-public crate-blinmaker-2.1.0 (c (n "blinmaker") (v "2.1.0") (h "1934ww2yjiav67m8l48x0287jxif5bh7c2a4242wx9hhxfndhvv9") (y #t)))

(define-public crate-blinmaker-2.1.1 (c (n "blinmaker") (v "2.1.1") (h "1f01j0qrsq3wi51zhs9d0554nd22myac9vhgih4wahxwm2s38ias") (y #t)))

