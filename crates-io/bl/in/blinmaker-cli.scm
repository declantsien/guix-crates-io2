(define-module (crates-io bl in blinmaker-cli) #:use-module (crates-io))

(define-public crate-blinmaker-cli-1.0.0 (c (n "blinmaker-cli") (v "1.0.0") (d (list (d (n "blinmaker") (r "^2.1.1") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0w5501dmwbk7x257yg79v3bfzn4q6a0ji3l2byjjamdypdx6gbm2") (y #t)))

