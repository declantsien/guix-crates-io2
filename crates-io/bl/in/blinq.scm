(define-module (crates-io bl in blinq) #:use-module (crates-io))

(define-public crate-blinq-0.1.0 (c (n "blinq") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "083b4yb8ymlb62kv3bl1wk67fmrzm2prj7zx31zv594n50yqqhlb")))

(define-public crate-blinq-0.1.1 (c (n "blinq") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "0arw445l5m1gl760pwqwccpjn1ifjglxz4himf835snirqagr8jp")))

(define-public crate-blinq-0.1.2 (c (n "blinq") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "1n03fqjhfwqvf5k1riiv1fq7382z02bi006n29z22g333qx00c8y")))

(define-public crate-blinq-0.2.0 (c (n "blinq") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (d #t) (k 0)))) (h "0dbrkg3vmffmk50k277317j61w3a1hcgsqrkd078jxvdk59i3h3z")))

