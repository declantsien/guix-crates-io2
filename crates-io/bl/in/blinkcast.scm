(define-module (crates-io bl in blinkcast) #:use-module (crates-io))

(define-public crate-blinkcast-0.1.0 (c (n "blinkcast") (v "0.1.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)))) (h "0kcp1cqnrhrrm07afbax7ydxxpzbzvq2vx1kwv5krcmzfll631q5") (f (quote (("unstable")))) (r "1.60.0")))

(define-public crate-blinkcast-0.1.1 (c (n "blinkcast") (v "0.1.1") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)))) (h "0r3sy9kcvxgx5609lvgnjgzgpz8jvc0l50m8zr3qndmahviv99sp") (f (quote (("unstable")))) (r "1.60.0")))

(define-public crate-blinkcast-0.2.0 (c (n "blinkcast") (v "0.2.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)))) (h "1n86bs4wvhgxdpyqp7984qkh1n6bjzfqxcn51dzs9gzr64w75gl7") (f (quote (("unstable") ("default" "alloc") ("alloc")))) (r "1.61.0")))

