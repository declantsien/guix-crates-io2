(define-module (crates-io bl in blingfire-sys) #:use-module (crates-io))

(define-public crate-blingfire-sys-0.1.0 (c (n "blingfire-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "1cri79j0vc2hbxva42p3gr9ysvng0qldicwdgwbxz4y0rq2ais1q") (l "blingfiretokdll")))

(define-public crate-blingfire-sys-0.1.1 (c (n "blingfire-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "1npkf0kk1bkmhx9m10kz57yig10aszcpgrdpcdqnf4180i61sviy") (l "blingfiretokdll")))

(define-public crate-blingfire-sys-0.2.0 (c (n "blingfire-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "0zh3dyvvckvdvch3xiw82c9kaqfkfkdchmb800wvrlvg88w071j3") (l "blingfiretokdll")))

(define-public crate-blingfire-sys-0.2.1 (c (n "blingfire-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.40") (d #t) (k 1)))) (h "0265fplkqc8agq8kwmf6c8irc9xxkw1vkn8sg3az3mlmarnv25s5") (l "blingfiretokdll")))

(define-public crate-blingfire-sys-1.0.1 (c (n "blingfire-sys") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "18p5r4xsf3r69crg9gvp359yxngk82b6ii09b3njbc4j02ld1pg2") (l "blingfiretokdll")))

