(define-module (crates-io bl in blink1) #:use-module (crates-io))

(define-public crate-blink1-0.1.0 (c (n "blink1") (v "0.1.0") (d (list (d (n "hidapi") (r "^0.5.0") (d #t) (k 0)))) (h "0rl4lplry3bj8mp4q17s946826wj4j4dzaga4dsbf3z7nwkcknql")))

(define-public crate-blink1-0.1.1 (c (n "blink1") (v "0.1.1") (d (list (d (n "hidapi") (r "^0.5.0") (d #t) (k 0)))) (h "139x1zv5f6amyg3zcwv8ipc32crsa0zmf5kr5x0lr1l9zdz6aas8")))

(define-public crate-blink1-0.1.2 (c (n "blink1") (v "0.1.2") (d (list (d (n "hidapi") (r "^0.5.0") (d #t) (k 0)))) (h "13i0air4194j6hv8q3413a6jzp08jxlia2s5861bhw3sxgz7k8jl")))

