(define-module (crates-io bl in blink-channel) #:use-module (crates-io))

(define-public crate-blink-channel-0.1.0 (c (n "blink-channel") (v "0.1.0") (d (list (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 0)))) (h "06fr59yysz5r3kdqn9rcc155basc7q0nkz3hj7sz5b6zw73mg6gs") (f (quote (("unstable")))) (y #t) (r "1.60.0")))

