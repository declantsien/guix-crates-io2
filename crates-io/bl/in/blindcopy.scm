(define-module (crates-io bl in blindcopy) #:use-module (crates-io))

(define-public crate-blindcopy-0.1.0 (c (n "blindcopy") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^5.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "wl-clipboard-rs") (r "^0.8.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0pnh9p4aqdfdpa7k1n4k07rkf3hfn3z2w7gdhkmshawswcbx0w6a")))

(define-public crate-blindcopy-0.1.1 (c (n "blindcopy") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^5.2.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc-foundation") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc_id") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "wl-clipboard-rs") (r "^0.8.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "012nbg33lxh748zl9k7ib88gvz131ph4w85lghshr019xdykmpma")))

