(define-module (crates-io bl in blinkt) #:use-module (crates-io))

(define-public crate-blinkt-0.1.0 (c (n "blinkt") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0svzg4g5vxx5bswd3290na49cx11vpmsc2wsnj06d847mf5lgp4x")))

(define-public crate-blinkt-0.1.1 (c (n "blinkt") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0kf81calzfmd4lzh6nw12wsb5kffplfhnl1m9px8vy0n4ys6mkz4")))

(define-public crate-blinkt-0.1.2 (c (n "blinkt") (v "0.1.2") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rppal") (r "^0.1") (d #t) (k 0)))) (h "0yc77gn2ibj5lzmjlr16pm29d3sx5hag8n50h6pksnlfd0wj1xjb")))

(define-public crate-blinkt-0.2.0 (c (n "blinkt") (v "0.2.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rppal") (r "^0.2") (d #t) (k 0)))) (h "1hvhnhyyw1fi5n7rl3kgid04r6fj3kgpyc1c25ai2gs5j0h9rd7p")))

(define-public crate-blinkt-0.3.0 (c (n "blinkt") (v "0.3.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rppal") (r "^0.3") (d #t) (k 0)))) (h "0y83gjalrn34sp87xg8nbgq7x6552j2rdw41c6rwb1lzxgm4fhzj")))

(define-public crate-blinkt-0.4.0 (c (n "blinkt") (v "0.4.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rppal") (r "^0.4") (d #t) (k 0)) (d (n "spidev") (r "^0.3") (d #t) (k 0)))) (h "054rfm542z4rxib9j8kb7im0mkmmlgl2z6w7c8bgscrmk9rkb3jj")))

(define-public crate-blinkt-0.5.0 (c (n "blinkt") (v "0.5.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rppal") (r "^0.9") (d #t) (k 0)))) (h "1wy8b24w8g7n4gm7nnn2zlh04ml18h12i2v0j08aaxbml2lmxlbr")))

(define-public crate-blinkt-0.6.0 (c (n "blinkt") (v "0.6.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)))) (h "1z5vmjj5a7wi84szzww205nazbkfyb74cv30ic43kx5sffg4fi02")))

(define-public crate-blinkt-0.7.0 (c (n "blinkt") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rppal") (r "^0.13.1") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)))) (h "1s576v1iz3ymnm534cwgw50raiijf1bwsb45d538y6nwf35v500q") (r "1.56.0")))

(define-public crate-blinkt-0.7.1 (c (n "blinkt") (v "0.7.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rppal") (r "^0.15.0") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 2)))) (h "0vq34641nwhmvfcqrw9jigd2py7jn1vdnv6l1rp4xm2jj3shi55f") (r "1.56.0")))

