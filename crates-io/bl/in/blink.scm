(define-module (crates-io bl in blink) #:use-module (crates-io))

(define-public crate-blink-0.1.1 (c (n "blink") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0q8h7hmxgqs362b5nl14p24m2vcyw2hz1rmwn918yni3pd1fkxzg")))

