(define-module (crates-io bl in blink-cli) #:use-module (crates-io))

(define-public crate-blink-cli-0.0.0 (c (n "blink-cli") (v "0.0.0") (h "1m1yijlchc6hj1hia0i5kvp4whq99ffpaslrb263z6nsbpaav6qb") (y #t)))

(define-public crate-blink-cli-0.0.1 (c (n "blink-cli") (v "0.0.1") (h "12jwvflcfwnjlf3dhidamm606l3phvfb22bc8ylwvsldg1m9jch5")))

