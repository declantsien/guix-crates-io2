(define-module (crates-io bl ys blyss-rs) #:use-module (crates-io))

(define-public crate-blyss-rs-0.2.0 (c (n "blyss-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("multipart" "rustls-tls"))) (k 0)) (d (n "ruint") (r "^1.2.0") (f (quote ("serde" "num-bigint" "ark-ff"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "spiral-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 0)))) (h "158b494j247f48fd3s5fr08sn3bfg9y92hawhc85zny2l0pq19f8")))

