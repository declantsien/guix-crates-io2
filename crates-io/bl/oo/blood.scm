(define-module (crates-io bl oo blood) #:use-module (crates-io))

(define-public crate-blood-0.1.0 (c (n "blood") (v "0.1.0") (h "18w6vg89hil4scn1ijqxidx8q1civ4p6zs1sf2m8azshv0pxf1jy")))

(define-public crate-blood-0.1.1 (c (n "blood") (v "0.1.1") (h "0ckzi40vjhkncv39mn4nj9gqykd46r0qp19hxkzxw7c8ipxzfmk5")))

