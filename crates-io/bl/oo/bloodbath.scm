(define-module (crates-io bl oo bloodbath) #:use-module (crates-io))

(define-public crate-bloodbath-0.1.0 (c (n "bloodbath") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cp15088shzsifw3s66phc4gw75jmil4gsgd5dshfbpwwx5l942p")))

(define-public crate-bloodbath-0.1.1 (c (n "bloodbath") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q7cm9z6493grbwnhrv7ld5030lp69gvw73izbq3n1hw4ygq4py2")))

(define-public crate-bloodbath-0.1.2 (c (n "bloodbath") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2k3ygbxxw60h5j9v24whf4mw0sp3igi17zdq553gmndhz9nbhw")))

(define-public crate-bloodbath-0.1.3 (c (n "bloodbath") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sgfgmcrbxxwljs9sbg394z1kk8ybzslszsjg414c3js9ykvz6rg")))

(define-public crate-bloodbath-0.1.4 (c (n "bloodbath") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19s62q1zysilk1kg4ciamk0gy3fvb37j6fy12bjr9pw41qhca9vs")))

