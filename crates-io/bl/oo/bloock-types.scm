(define-module (crates-io bl oo bloock-types) #:use-module (crates-io))

(define-public crate-bloock-types-0.1.0 (c (n "bloock-types") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cswcws6qydpk9jwci18mw7jb6cb3k8j347i56l3474jlj6vf5k1") (f (quote (("h256") ("h128"))))))

