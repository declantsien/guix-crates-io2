(define-module (crates-io bl oo bloomfilter) #:use-module (crates-io))

(define-public crate-bloomfilter-0.0.1 (c (n "bloomfilter") (v "0.0.1") (h "00gz0q71pzjjypqlcqb0fzvdk3q2rx2g9c16ydklm2q3ill79d03")))

(define-public crate-bloomfilter-0.0.2 (c (n "bloomfilter") (v "0.0.2") (h "1hjfbbam7iqlmk796c4ivpavkwclkv1d8rmi79awvcj950a5qnsx")))

(define-public crate-bloomfilter-0.0.3 (c (n "bloomfilter") (v "0.0.3") (h "132f97y6bdx02yhgif4sw483wilbsxbcaz8287zvs3908prx5wk3")))

(define-public crate-bloomfilter-0.0.4 (c (n "bloomfilter") (v "0.0.4") (h "0g1wpblhn1xhvlmaiq3lixgyavvizr3g2lrgzdqca6clgz1wy7gs")))

(define-public crate-bloomfilter-0.0.5 (c (n "bloomfilter") (v "0.0.5") (h "061x2sm5dc4acfcrhr7rbgc63c2w9qnccxnpp41rc0z223r4md1x")))

(define-public crate-bloomfilter-0.0.6 (c (n "bloomfilter") (v "0.0.6") (h "066x9b6zpi748vjd5il10yf3fr21jq0ihf759iv1hbqqikrn186n")))

(define-public crate-bloomfilter-0.0.7 (c (n "bloomfilter") (v "0.0.7") (h "0rk901d0v3piyb4iqrzvmqqbzlm9668ggsq2s6jd1rdjxa2hrgyj")))

(define-public crate-bloomfilter-0.0.8 (c (n "bloomfilter") (v "0.0.8") (h "0fvri0nl5p3xr294zcx1d0znxa9hlc0y0rc9sgv9xczbg9bmllrg")))

(define-public crate-bloomfilter-0.0.9 (c (n "bloomfilter") (v "0.0.9") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0sd3gq8aj8p27dpi6g5xg90ri32dmyjq11502khpim6yg084wr9a")))

(define-public crate-bloomfilter-0.0.10 (c (n "bloomfilter") (v "0.0.10") (d (list (d (n "bit-vec") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1lxsvz41c75b5zxli9jvhfy0smn6ishzhbsgrh3hhxz7fkp9x0li")))

(define-public crate-bloomfilter-0.0.12 (c (n "bloomfilter") (v "0.0.12") (d (list (d (n "bit-vec") (r "~0.4") (d #t) (k 0)) (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "rand") (r "~0.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.2.2") (d #t) (k 0)))) (h "01yxb0a6q1bhb9wp37s1pa5mdf343di089q51iy8wl59y99926m7")))

(define-public crate-bloomfilter-1.0.1 (c (n "bloomfilter") (v "1.0.1") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.1") (d #t) (k 0)))) (h "08jmvr5fh6k3xxl61fl6aw39ay0jad3l7hxbbsa7z79vfm07ysby")))

(define-public crate-bloomfilter-1.0.2 (c (n "bloomfilter") (v "1.0.2") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.1") (d #t) (k 0)))) (h "0a44823xvnqf1mr39wy7z8gyi3zxd3bfqkkd6qlx953vzzkkhhmi")))

(define-public crate-bloomfilter-1.0.3 (c (n "bloomfilter") (v "1.0.3") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "siphasher") (r "^0.3") (d #t) (k 0)))) (h "19sq6gn143fqbjij8f8n6i5km0imw3kq9ma8rb8h2lxkrfq7yhz6")))

(define-public crate-bloomfilter-1.0.4 (c (n "bloomfilter") (v "1.0.4") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.2") (d #t) (k 0)))) (h "0967pi3mqvjvz8jzsjy8ziv5wsv9dw0gd21jhlsd2prh72fqxnpv") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("defaults"))))))

(define-public crate-bloomfilter-1.0.5 (c (n "bloomfilter") (v "1.0.5") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.5") (d #t) (k 0)))) (h "1d352wzl3pwpwxixjh8b8v4sqaglgkzp0rfv1q068c8piddfizqw") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("defaults"))))))

(define-public crate-bloomfilter-1.0.6 (c (n "bloomfilter") (v "1.0.6") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "siphasher") (r "^0.3.6") (d #t) (k 0)))) (h "17iwyyp61fl5rgvr7rjri4zl0b0b5qfinvm4ciad3s9s5pcyggzi") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("defaults"))))))

(define-public crate-bloomfilter-1.0.7 (c (n "bloomfilter") (v "1.0.7") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (d #t) (k 0)))) (h "0viiirnwhh90g49j48z8yhvf3shhpvqnwq27bh2330cnrhw4sfwn") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("defaults" "random")))) (y #t)))

(define-public crate-bloomfilter-1.0.8 (c (n "bloomfilter") (v "1.0.8") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (d #t) (k 0)))) (h "0c99r3p5x0il28j4ywzihc4dd8x366glhf41xl3ga9mv38kqi1fg") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

(define-public crate-bloomfilter-1.0.9 (c (n "bloomfilter") (v "1.0.9") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^0.3.7") (d #t) (k 0)))) (h "1c7b51s08kn0d318zkvd6cg83fh49n0pwn5wdnphq6qc6jmw0ac1") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

(define-public crate-bloomfilter-1.0.10 (c (n "bloomfilter") (v "1.0.10") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "12jnn73xld8rrk6g8vdarq4wpqqn4qm8w65l5ll7556vv3j8f6wr") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

(define-public crate-bloomfilter-1.0.11 (c (n "bloomfilter") (v "1.0.11") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "15439sygbiiibl2m4bkp70wj2gssw118i4hyzxarv4k6svazk1s2") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

(define-public crate-bloomfilter-1.0.12 (c (n "bloomfilter") (v "1.0.12") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "0d6r0vlzfgbycpvbg7iwb62102j4i38ys2hx3i5qp2s3bnbbfbdr") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

(define-public crate-bloomfilter-1.0.13 (c (n "bloomfilter") (v "1.0.13") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (o #t) (d #t) (t "cfg(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\"))") (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (t "cfg(not(all(any(target_arch = \"wasm32\", target_arch = \"wasm64\"), target_os = \"unknown\")))") (k 0)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "1kxv0839yvsmfrd3bjmfmnl6ppzk54ag3s42y0ixfkvzgbj58kdn") (f (quote (("serde" "siphasher/serde_std" "bit-vec/serde") ("random" "getrandom") ("default" "random"))))))

