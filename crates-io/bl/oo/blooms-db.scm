(define-module (crates-io bl oo blooms-db) #:use-module (crates-io))

(define-public crate-blooms-db-0.1.0 (c (n "blooms-db") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "vapbloom") (r "^0.8.0") (d #t) (k 0)))) (h "1i6i55wmkrncarmwkbb7s778gks15ivjzz9b8q65dv43f87gwpwj")))

