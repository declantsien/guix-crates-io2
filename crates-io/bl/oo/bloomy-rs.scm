(define-module (crates-io bl oo bloomy-rs) #:use-module (crates-io))

(define-public crate-bloomy-rs-0.1.0 (c (n "bloomy-rs") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fastbloom-rs") (r "^0.5.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0f279dfsmjrpxvm84wf213fpnkhhln3wfv4nlffrnyfk8dzcz3p3")))

