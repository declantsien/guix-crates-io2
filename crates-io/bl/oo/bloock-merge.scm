(define-module (crates-io bl oo bloock-merge) #:use-module (crates-io))

(define-public crate-bloock-merge-0.1.0 (c (n "bloock-merge") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^0.5.10") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.1") (f (quote ("keccak"))) (o #t) (d #t) (k 0)))) (h "0cns68h2nhg3qhqlpqsfmn8jb756pcq0narl9jsnffq66d55ng05") (f (quote (("keccak" "tiny-keccak") ("blake2b" "blake2b_simd"))))))

