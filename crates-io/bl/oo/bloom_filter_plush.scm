(define-module (crates-io bl oo bloom_filter_plush) #:use-module (crates-io))

(define-public crate-bloom_filter_plush-0.1.0 (c (n "bloom_filter_plush") (v "0.1.0") (h "1zgwjmanzwf0jwvkslahhb8iq835hba8795kmb61zxr3d16rp2yw")))

(define-public crate-bloom_filter_plush-0.1.1 (c (n "bloom_filter_plush") (v "0.1.1") (h "1m90bqpw3picxcp1wiwybk77fmbnkn67chl05ggnn9klczqas76c")))

(define-public crate-bloom_filter_plush-0.1.2 (c (n "bloom_filter_plush") (v "0.1.2") (h "10alaqplaxg4pjq0cl1yj0n8xiiygna2awsf8x50s5wm7v0vw4vs")))

(define-public crate-bloom_filter_plush-0.1.3 (c (n "bloom_filter_plush") (v "0.1.3") (h "014i9bgxqkqz76mcjqcjzp9515rimdky00w5mkljilbv2vh0fi1a")))

(define-public crate-bloom_filter_plush-0.1.6 (c (n "bloom_filter_plush") (v "0.1.6") (h "1p4wcmxpa81vd6k2sylvg4808l45wr8v2lgj0628ypzck1gfrsv6")))

