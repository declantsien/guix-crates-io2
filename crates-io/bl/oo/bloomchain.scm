(define-module (crates-io bl oo bloomchain) #:use-module (crates-io))

(define-public crate-bloomchain-0.1.0 (c (n "bloomchain") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1395kkjzmbiw8bdinawii5vbw1nzj0mr2gzvsd6c4vx7saai0hiz")))

(define-public crate-bloomchain-0.2.0 (c (n "bloomchain") (v "0.2.0") (d (list (d (n "ethbloom") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "0277p6j6aafsgrc0dpjfp4c4psqv58hjwgymr1laz4an83gq4xi8")))

