(define-module (crates-io bl oo bloock-blake-rs) #:use-module (crates-io))

(define-public crate-bloock-blake-rs-0.1.0 (c (n "bloock-blake-rs") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0gzvs9lxij3qhjaz9pby43xp9vcfr25didc90z8mmmw74kmj3hl8")))

