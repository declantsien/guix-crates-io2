(define-module (crates-io bl oo bloom_filter) #:use-module (crates-io))

(define-public crate-bloom_filter-0.1.0 (c (n "bloom_filter") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "siphasher") (r "^0.2.0") (d #t) (k 0)))) (h "02zsxa4mhvz7wk5qixgkz00r7bnkx3bcfj6hk1lmwi9z0g2h4kj5") (f (quote (("nightly"))))))

