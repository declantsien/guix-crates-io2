(define-module (crates-io bl oo bloomfilter-rs) #:use-module (crates-io))

(define-public crate-bloomfilter-rs-0.1.0 (c (n "bloomfilter-rs") (v "0.1.0") (d (list (d (n "cli-table") (r "^0.4.6") (d #t) (k 0)) (d (n "fasthash-fork") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w0p7yq6bdvfsxyvzj1j1s80hd6lya80134px0fjz13j90w20k0j")))

