(define-module (crates-io bl oo bloom-cli) #:use-module (crates-io))

(define-public crate-bloom-cli-0.1.0 (c (n "bloom-cli") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh32" "const_xxh32"))) (d #t) (k 0)))) (h "0pl4hb9gcqgy5agzfw3f791jbs6g23v37xcmkci1j7ycbybd9iix")))

(define-public crate-bloom-cli-0.1.1 (c (n "bloom-cli") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh32" "const_xxh32"))) (d #t) (k 0)))) (h "070k43dh7bb9iclybj5h31cj5rnn5yijmwz5cy5yp582qgbcr8dp")))

(define-public crate-bloom-cli-0.1.2 (c (n "bloom-cli") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh32" "const_xxh32"))) (d #t) (k 0)))) (h "1w5bldv0ah7m6zhm8yx56bhq3zvlcb68yv19pqm0nwg5xw5hd9kq")))

