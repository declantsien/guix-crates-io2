(define-module (crates-io bl oo bloom-filter-rs) #:use-module (crates-io))

(define-public crate-bloom-filter-rs-0.1.0 (c (n "bloom-filter-rs") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "murmur3") (r "^0.4.1") (d #t) (k 0)))) (h "1ga4c19hyhxkq5bflipnmdsqh3jwkqxz5cp3rm3z6mc8b2jlih7q")))

