(define-module (crates-io bl oo bloom2) #:use-module (crates-io))

(define-public crate-bloom2-0.1.0 (c (n "bloom2") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "013qr3np3pks0khkzwnxhqcdjrvswlz843103bvwr48fvamwc54g")))

(define-public crate-bloom2-0.2.0 (c (n "bloom2") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n4rpp4cchpvwnfwajcl1gcw3qpv1avm7nzpd2wblzkyw5nxhw1z")))

(define-public crate-bloom2-0.2.1 (c (n "bloom2") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mqpqzvjy4xmk8wqvsdxvh7m9gbllgqjx1fw86jxy4f4bqvhdlps")))

(define-public crate-bloom2-0.3.0 (c (n "bloom2") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0aw9wzrdw56sbs12ycbc224dsa23qnfifa9xwvi3ppy6bs2xwpmx")))

(define-public crate-bloom2-0.3.1 (c (n "bloom2") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kpfda6nhnsn1widxp0cp4mvg1dlwz2qhy5ppiv5dyrj7pqy32y9")))

