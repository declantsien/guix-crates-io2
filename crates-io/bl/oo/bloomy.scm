(define-module (crates-io bl oo bloomy) #:use-module (crates-io))

(define-public crate-bloomy-1.0.0 (c (n "bloomy") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "1g9p4bn2zb0wa9rl4vw5yg2rilnmbmak0ydbkjrpi3f7ii70n0sz")))

(define-public crate-bloomy-1.0.1 (c (n "bloomy") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "1bgl9bv5vv2sbnl76h1hzsi8a4r7gjjby5n15gdh54p7kn1y24sp")))

(define-public crate-bloomy-1.1.0 (c (n "bloomy") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "0pkwqzhw6vbvzqigpxc8rl4vdx72ibnhy681fxyhzq7bxich13c0")))

(define-public crate-bloomy-1.2.0 (c (n "bloomy") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4.0") (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "siphasher") (r "^0.3.10") (d #t) (k 0)))) (h "1p9vxbv0kws99ilkk97kd8dhc4pr3rmaqww28y38mdsjg3sjm7a8")))

