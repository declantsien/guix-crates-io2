(define-module (crates-io bl oo bloomcalc) #:use-module (crates-io))

(define-public crate-bloomcalc-0.1.0 (c (n "bloomcalc") (v "0.1.0") (d (list (d (n "arrrg") (r "^0.1.0") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1pwmns2aqqlcphbcqypx3svys7giichppas92rhgmpbqxzvfbjy4")))

(define-public crate-bloomcalc-0.1.1 (c (n "bloomcalc") (v "0.1.1") (d (list (d (n "arrrg") (r "^0.1") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "15ajzcnrrjs5sxza0w110yfwyqjipjnyy08wk3jlkj21cfg4bzd6")))

(define-public crate-bloomcalc-0.2.0 (c (n "bloomcalc") (v "0.2.0") (d (list (d (n "arrrg") (r "^0.2") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0j5wi884xr7a8xvf3d4qswzp4mgb5lh5gwz9m1yli9qd6zqnr7hf")))

(define-public crate-bloomcalc-0.3.0 (c (n "bloomcalc") (v "0.3.0") (d (list (d (n "arrrg") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1kwpg2slzpk1q0jdr2z9faky1w9jc199ig84zhycgw3qr48nk9hj") (f (quote (("default" "binaries") ("binaries" "command_line")))) (s 2) (e (quote (("command_line" "dep:arrrg" "dep:arrrg_derive" "dep:getopts"))))))

