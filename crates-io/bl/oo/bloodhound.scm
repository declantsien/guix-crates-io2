(define-module (crates-io bl oo bloodhound) #:use-module (crates-io))

(define-public crate-bloodhound-0.0.1 (c (n "bloodhound") (v "0.0.1") (h "0yyvqq6w9hhlvygv0iizs7mmckyi5xh8ckyb31bwzs7ah9da77r4")))

(define-public crate-bloodhound-0.1.0 (c (n "bloodhound") (v "0.1.0") (h "0ksj04qwvwmsdlqzfhaqxkfxqvq4g68y5abs0lmal10wjmgj9gv4")))

(define-public crate-bloodhound-0.1.1 (c (n "bloodhound") (v "0.1.1") (h "0smriz7xg6ppw9lzh4z862nz3znd31z1h5cl4wk3664v6rds4w9m")))

(define-public crate-bloodhound-0.1.2 (c (n "bloodhound") (v "0.1.2") (h "1pdp6g5ky5014iwaj3sbjvii8afw6gv163ix52wwgzvrz1rsxwrq")))

(define-public crate-bloodhound-0.2.0 (c (n "bloodhound") (v "0.2.0") (h "0bpnfqikc27b6s25dy7hs3aw5dsslsjdwkzhzbnfavg56298ad0d")))

(define-public crate-bloodhound-0.2.1 (c (n "bloodhound") (v "0.2.1") (h "1illqmckln0mas8vlsk3j94mkkzh76zivj7ykrhbgbiby3rcppr9")))

(define-public crate-bloodhound-0.2.2 (c (n "bloodhound") (v "0.2.2") (d (list (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1a9mhrr55l88pzz86pnivji3zj1arm1lydga9f4yy5nqsaa1ks8c")))

(define-public crate-bloodhound-0.2.3 (c (n "bloodhound") (v "0.2.3") (d (list (d (n "fragment") (r "^0.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1h98cmj5zpx7bhx336srww7lh41mdg3yrzmqsv6sjvf49ffck3v0")))

(define-public crate-bloodhound-0.2.4 (c (n "bloodhound") (v "0.2.4") (d (list (d (n "fragment") (r "^0.1.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1iqr8n4kh30lzn0amr72ivzhygbrimj3mplbp0fl6xnfp2v7l2zf")))

(define-public crate-bloodhound-0.3.0 (c (n "bloodhound") (v "0.3.0") (d (list (d (n "fragment") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "00cvac7mjfririlsk9zgvbhqz1zwwd5awj36x7c4p111b8aswd99")))

(define-public crate-bloodhound-0.4.0 (c (n "bloodhound") (v "0.4.0") (d (list (d (n "fragment") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1ysbsfq90bm1vn0l9x5xwz050c6d2d8llk37h4hmh15gmm7wh4cy")))

(define-public crate-bloodhound-0.5.0 (c (n "bloodhound") (v "0.5.0") (d (list (d (n "fragment") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0yxlp6z5di4gi3gh8xabjmj3xkcdhw89r79fyh4sl1y41mijpfqn")))

(define-public crate-bloodhound-0.5.1 (c (n "bloodhound") (v "0.5.1") (d (list (d (n "fragment") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "0r99r12cvy7lxaq554qs2i1x61fz1fqf1khf5ks23sr5jpj49z5r")))

(define-public crate-bloodhound-0.5.2 (c (n "bloodhound") (v "0.5.2") (d (list (d (n "fragment") (r "^0.3.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1y6ng8p983bmlrv5xv7l6icg9iz5syjy08rpwq6jgqskpqdm32qa")))

(define-public crate-bloodhound-0.5.3 (c (n "bloodhound") (v "0.5.3") (d (list (d (n "fragment") (r "^0.3.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "11aq63ps1shlxf7v5awzgllsbjfy53637kcbvk70ja7g0k8jh3m1")))

(define-public crate-bloodhound-0.5.4 (c (n "bloodhound") (v "0.5.4") (d (list (d (n "fragment") (r "^0.3.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.0.1") (d #t) (k 0)))) (h "1jqkz3cc37c3b3i6vx5k4fnf3l33v034l4znpfrmx5f60lxdxbf3")))

