(define-module (crates-io bl oo bloom_filter_simple) #:use-module (crates-io))

(define-public crate-bloom_filter_simple-0.1.0 (c (n "bloom_filter_simple") (v "0.1.0") (d (list (d (n "ahash") (r "^0.6.1") (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.0-beta.3") (f (quote ("xxh3"))) (d #t) (k 2)))) (h "1qc39kr0p3j1bqld4gql0sb0xdzj4gj8322hhxpqydkjilx80pai")))

