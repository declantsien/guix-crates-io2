(define-module (crates-io bl oo bloomberg) #:use-module (crates-io))

(define-public crate-bloomberg-0.2.0 (c (n "bloomberg") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1f5rp96p53janbz47s4i24y8f74x3imsgrgb97qd80wsdikp0ah1")))

