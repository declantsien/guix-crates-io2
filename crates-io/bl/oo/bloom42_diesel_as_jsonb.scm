(define-module (crates-io bl oo bloom42_diesel_as_jsonb) #:use-module (crates-io))

(define-public crate-bloom42_diesel_as_jsonb-0.1.2 (c (n "bloom42_diesel_as_jsonb") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.23") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("derive"))) (d #t) (k 0)))) (h "10ikwbz46j213gpbiyj23cja5kwgad0x6h8npvx78297b89wfjam")))

