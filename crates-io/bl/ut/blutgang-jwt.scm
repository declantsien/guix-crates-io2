(define-module (crates-io bl ut blutgang-jwt) #:use-module (crates-io))

(define-public crate-blutgang-jwt-0.1.0 (c (n "blutgang-jwt") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^9.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "14lkmw4m2qx4p8f5kfc5g2rky3r4c57my7qsry3ncvzg74plfp8g")))

