(define-module (crates-io bl ut blutils-proc-adapter) #:use-module (crates-io))

(define-public crate-blutils-proc-adapter-3.0.1 (c (n "blutils-proc-adapter") (v "3.0.1") (d (list (d (n "blutils-core") (r "^3.0.1") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1ix6nv76h26skrlrq7bw76aghqnb9cxgjq54d722bi2ykwb8m5i3")))

(define-public crate-blutils-proc-adapter-4.0.0 (c (n "blutils-proc-adapter") (v "4.0.0") (d (list (d (n "blutils-core") (r "^4.0.0") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "154r0n110adcknh85r63issm0lk3czaqchg1gfasjnk7358mswnc")))

(define-public crate-blutils-proc-adapter-5.0.0 (c (n "blutils-proc-adapter") (v "5.0.0") (d (list (d (n "blutils-core") (r "^5.0.0") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0cwwc800176xl4k5qq365phsxbw8z2wg915in4c319sp5pgsfi3f")))

(define-public crate-blutils-proc-adapter-5.0.1 (c (n "blutils-proc-adapter") (v "5.0.1") (d (list (d (n "blutils-core") (r "^5.0.1") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "083sh6ns02jvyvqss27lniays6jzd09r518chc2mnvcnarqmkp20")))

(define-public crate-blutils-proc-adapter-6.0.0 (c (n "blutils-proc-adapter") (v "6.0.0") (d (list (d (n "blutils-core") (r "^6.0.0") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "14r4g8lhzy2msl5xx3cy5s9prlsdcqid8ng0k3ydbrcrxi50a0xw")))

(define-public crate-blutils-proc-adapter-6.1.0 (c (n "blutils-proc-adapter") (v "6.1.0") (d (list (d (n "blutils-core") (r "^6.1.0") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "153pa9yan3mv6i7r1m54qvf8b68xgsk40nwagmnh725canncz460")))

(define-public crate-blutils-proc-adapter-6.1.1 (c (n "blutils-proc-adapter") (v "6.1.1") (d (list (d (n "blutils-core") (r "^6.1.1") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0mjsd5bwm1slwq01giw7l3md3250g6s9lixaq5gzsfzbmbgy5lky")))

(define-public crate-blutils-proc-adapter-6.1.2 (c (n "blutils-proc-adapter") (v "6.1.2") (d (list (d (n "blutils-core") (r "^6.1.2") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0pb10k1vz6w5fkkmn568zanj8h5h98x51jy4p62abp2mfgi56lk3")))

(define-public crate-blutils-proc-adapter-6.2.0 (c (n "blutils-proc-adapter") (v "6.2.0") (d (list (d (n "blutils-core") (r "^6.2.0") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1703v8465kq14mbd5izzj3z8hhrfn5aqdi4rhcmnrpinfdanyddx")))

(define-public crate-blutils-proc-adapter-6.3.2 (c (n "blutils-proc-adapter") (v "6.3.2") (d (list (d (n "blutils-core") (r "^6.3.2") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0x5c16zb5mf0lhy54kclh2q3qlvl6f1icmwlddxmrvdr7i88zcmg")))

(define-public crate-blutils-proc-adapter-6.3.3 (c (n "blutils-proc-adapter") (v "6.3.3") (d (list (d (n "blutils-core") (r "^6.3.3") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1wzvg5asm1jl75rkyizjgipm3fvzi2hqg5f26vknyiikjnpkksjh")))

(define-public crate-blutils-proc-adapter-7.0.0 (c (n "blutils-proc-adapter") (v "7.0.0") (d (list (d (n "blutils-core") (r "^7.0.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1fxzqcxr0nny58zm9rmhv3ca1v3q5sqv363wbzmqk10c62av8ycs")))

(define-public crate-blutils-proc-adapter-7.0.1 (c (n "blutils-proc-adapter") (v "7.0.1") (d (list (d (n "blutils-core") (r "^7.0.1") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "12qccr8m24rgk1lb9wygqrxf1rgmdm14163w9jr7ridibvg85af8")))

(define-public crate-blutils-proc-adapter-7.0.2 (c (n "blutils-proc-adapter") (v "7.0.2") (d (list (d (n "blutils-core") (r "^7.0.2") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1ddjw5c1d1dsblzi5fg2v20gpz17hxi6ixj5vcvaznyn6qxjlqap")))

(define-public crate-blutils-proc-adapter-7.0.3 (c (n "blutils-proc-adapter") (v "7.0.3") (d (list (d (n "blutils-core") (r "^7.0.3") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "15crxp9qni66izis66pvx2pp0yigcf1axpcis1q3fiwba0vwwq14")))

(define-public crate-blutils-proc-adapter-7.0.4 (c (n "blutils-proc-adapter") (v "7.0.4") (d (list (d (n "blutils-core") (r "^7.0.4") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "183qhp0590q759csqy56l45xy2gd3a255ypk0ifnz45mmiyvjadq")))

(define-public crate-blutils-proc-adapter-7.0.5 (c (n "blutils-proc-adapter") (v "7.0.5") (d (list (d (n "blutils-core") (r "^7.0.5") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1yz0yms5av5756qf4qx8afzhyq5f6v4238dia675psh5ykb77lhl")))

(define-public crate-blutils-proc-adapter-7.1.0 (c (n "blutils-proc-adapter") (v "7.1.0") (d (list (d (n "blutils-core") (r "^7.1.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0ycw70c8zwr55wp7bm90ipa7x4cp4p6bslb4b1jl1wbdz3h8dl2c")))

(define-public crate-blutils-proc-adapter-7.1.1 (c (n "blutils-proc-adapter") (v "7.1.1") (d (list (d (n "blutils-core") (r "^7.1.1") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0vmfqka68nscvmjanc15lk8i5d8wgx9xw5p5d9fms2xg80ab6pqn")))

(define-public crate-blutils-proc-adapter-7.1.2 (c (n "blutils-proc-adapter") (v "7.1.2") (d (list (d (n "blutils-core") (r "^7.1.2") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1wz2293vfghidlpvhfx618g21qfimz0mqck99qz6n8v0q3zgzv08")))

(define-public crate-blutils-proc-adapter-7.1.3 (c (n "blutils-proc-adapter") (v "7.1.3") (d (list (d (n "blutils-core") (r "^7.1.3") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "165s7s29s4sb423hmz89vzydl95dfz915ilzyxfhzxsnngf299r5")))

(define-public crate-blutils-proc-adapter-7.2.0 (c (n "blutils-proc-adapter") (v "7.2.0") (d (list (d (n "blutils-core") (r "^7.2.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1w23x5cylspby514p8d3kqgm5ywlhkj229l2kkix706hfk1s2njw")))

(define-public crate-blutils-proc-adapter-8.0.0 (c (n "blutils-proc-adapter") (v "8.0.0") (d (list (d (n "blutils-core") (r "^8.0.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0w2a92y0vv8grn5gk6qw71vpmp86cz0sf6f525sldv2qfpwahmd5")))

(define-public crate-blutils-proc-adapter-8.0.1 (c (n "blutils-proc-adapter") (v "8.0.1") (d (list (d (n "blutils-core") (r "^8.0.1") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1503f2bnsj22gpiyz92b51rqn7hf4jq20bygdsmfh19jwrpsg0hv")))

(define-public crate-blutils-proc-adapter-8.1.0 (c (n "blutils-proc-adapter") (v "8.1.0") (d (list (d (n "blutils-core") (r "^8.1.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0yhx8lv59mfdrvqnaldrv38vlh0awfwffdwwncxdb4firavclyms")))

(define-public crate-blutils-proc-adapter-8.1.1 (c (n "blutils-proc-adapter") (v "8.1.1") (d (list (d (n "blutils-core") (r "^8.1.1") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1mhpj4lmansiidllgnb1mpqk2dvbsjzgjv52f9pfjdnq6x9qvs2p")))

(define-public crate-blutils-proc-adapter-8.1.3 (c (n "blutils-proc-adapter") (v "8.1.3") (d (list (d (n "blutils-core") (r "^8.1.3") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0s3xmb80lpal0pcq54dw85bym4affml57l2hyc66il8jkqhk2mc7")))

(define-public crate-blutils-proc-adapter-8.1.4 (c (n "blutils-proc-adapter") (v "8.1.4") (d (list (d (n "blutils-core") (r "^8.1.4") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0xby4gw3h0k0gbqmhcp4jbc77yh62z61by1v9misam6bxwhj61ww")))

(define-public crate-blutils-proc-adapter-8.1.5 (c (n "blutils-proc-adapter") (v "8.1.5") (d (list (d (n "blutils-core") (r "^8.1.5") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "09mf05vivjfkznhl3s0y7zrfki6gw7b7n6wgzx7q8y0xmgpsmlp5")))

(define-public crate-blutils-proc-adapter-8.2.0 (c (n "blutils-proc-adapter") (v "8.2.0") (d (list (d (n "blutils-core") (r "^8.2.0") (d #t) (k 0)) (d (n "mycelium-base") (r "^4.9.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "17zkwh3wyiq925gk2ziig70lphi5nfyh2qp26h19iwsh7kzin798")))

