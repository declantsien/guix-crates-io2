(define-module (crates-io bl ut blutils-adapters) #:use-module (crates-io))

(define-public crate-blutils-adapters-3.0.1 (c (n "blutils-adapters") (v "3.0.1") (d (list (d (n "blutils-core") (r "^3.0.1") (d #t) (k 0)) (d (n "clean-base") (r "^1.0.1") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "10x7hldal6v6cync3gcjrgrin2s80r1czdy2kbzn0sahhljrzc6b") (y #t)))

