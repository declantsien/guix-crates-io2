(define-module (crates-io bl ai blair_mountain) #:use-module (crates-io))

(define-public crate-blair_mountain-0.1.0 (c (n "blair_mountain") (v "0.1.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1xc3nbyld481sxlmvi8ifyra2wxjl4b1sd5n2lwwzl5x22hcs1r9")))

(define-public crate-blair_mountain-0.1.1 (c (n "blair_mountain") (v "0.1.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "12r44xxan3y2r20nc1xb6kg1bplhpscjjlcr235jjq0q0n3hkvxb")))

(define-public crate-blair_mountain-0.2.0 (c (n "blair_mountain") (v "0.2.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "11fjjf69g31xl2f8ndgg70l6z6dwpc3jk35kpbdqqd4kxa3kj69y")))

(define-public crate-blair_mountain-0.3.0 (c (n "blair_mountain") (v "0.3.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "00hlb9m3i5d1qin5snj4baxa4avd9443iqh2i6vmlf0b8swr8fgj")))

