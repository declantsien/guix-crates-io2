(define-module (crates-io bl en blend) #:use-module (crates-io))

(define-public crate-blend-0.6.0 (c (n "blend") (v "0.6.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1gy6xn4s95sasah75j6608wqna9nrncs7zjz257mnydh5qarh9zy")))

(define-public crate-blend-0.6.1 (c (n "blend") (v "0.6.1") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0ii5zbarzv6kcaa3wp0vy3afn9bm744s9nyqnpykqx2jxxfhdq64")))

(define-public crate-blend-0.7.0 (c (n "blend") (v "0.7.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "16sxd8f3v4y2zkz05mkhqzs6la2hp6a7p6973n1ggphyiim3wnq0")))

(define-public crate-blend-0.8.0 (c (n "blend") (v "0.8.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1cm5f6q1i9b0vw0cvjx2scm4g8kd9p1mx9z706wyvrkwdig6y0sn")))

