(define-module (crates-io bl en blend-converter) #:use-module (crates-io))

(define-public crate-blend-converter-0.1.0 (c (n "blend-converter") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0p3kbwqz8725cr6m0hnf4a9nigfivkp8zsp8wpzdvprfnpzsf690")))

(define-public crate-blend-converter-0.2.0 (c (n "blend-converter") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0fhiv9xqa0rqwnzf8j3w9csm023wc3b03by1kq8y9dk3xwvi0ral")))

(define-public crate-blend-converter-0.3.0 (c (n "blend-converter") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "10rcfy83rasfgbs3h5qk00ismzr6cz5w1j80v2l5f9smizwc0m8d")))

(define-public crate-blend-converter-0.4.0 (c (n "blend-converter") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1izlq4845yn48k1bxbaqsl43vbv3r5760jpcr7jck07dnab7fr8x")))

