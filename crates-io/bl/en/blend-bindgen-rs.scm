(define-module (crates-io bl en blend-bindgen-rs) #:use-module (crates-io))

(define-public crate-blend-bindgen-rs-0.1.0 (c (n "blend-bindgen-rs") (v "0.1.0") (d (list (d (n "blend-inspect-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0mffxmqz8rq4nqsc85qf4mani0gp2ms7f7jb9b8yl4zm0ifsdhcc")))

(define-public crate-blend-bindgen-rs-0.2.0 (c (n "blend-bindgen-rs") (v "0.2.0") (d (list (d (n "blend-inspect-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0isdchhf7cawnir700q6v4srv6w3anhqfgs4d2ll2lym5frqxsl9")))

(define-public crate-blend-bindgen-rs-0.3.0 (c (n "blend-bindgen-rs") (v "0.3.0") (d (list (d (n "blend-inspect-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1dam74cjk64dnihhavc8i2zhkqm9fy0dzb55131xgj9r5jv22ard")))

(define-public crate-blend-bindgen-rs-0.4.0 (c (n "blend-bindgen-rs") (v "0.4.0") (d (list (d (n "blend-inspect-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0sczzmsl73ciqn6lb2qx2d5c7wjgiidnqb4lqr6idqn6ivipix14")))

(define-public crate-blend-bindgen-rs-0.5.0 (c (n "blend-bindgen-rs") (v "0.5.0") (d (list (d (n "blend-inspect-rs") (r "^0.3.0") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0nc5y30cvn2wsi631w6apk38vx8q9m9c21zj1z0m17r9k2mivh9w")))

