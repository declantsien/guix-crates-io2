(define-module (crates-io bl en blend-contract-sdk) #:use-module (crates-io))

(define-public crate-blend-contract-sdk-0.1.0 (c (n "blend-contract-sdk") (v "0.1.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "16bizpyl1b0wvjvccqz86bz05gclhrisqkacnrqhmg9apg340sli") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-blend-contract-sdk-1.0.0 (c (n "blend-contract-sdk") (v "1.0.0") (d (list (d (n "soroban-sdk") (r "^20.5.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.5.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "06wikqk79x3cdq70jmciahfri4qll21wiwnvw8pjrxbn2h11hk26") (f (quote (("testutils" "soroban-sdk/testutils"))))))

