(define-module (crates-io bl en blend-srgb) #:use-module (crates-io))

(define-public crate-blend-srgb-0.1.0 (c (n "blend-srgb") (v "0.1.0") (d (list (d (n "libm") (r "~0.2.2") (o #t) (d #t) (k 0)))) (h "1cnb90rqqpyxyqmqx19x7al5ahvj5pw5pqih4dk8ss0zmdl4lq8j") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-blend-srgb-0.1.1 (c (n "blend-srgb") (v "0.1.1") (d (list (d (n "libm") (r "~0.2.2") (o #t) (d #t) (k 0)))) (h "09k45sd3lwh68j8xjqdgivlwpxfi3fzm062b6ks1a2iv47sqg8dq") (f (quote (("std") ("default" "std")))) (r "1.56")))

