(define-module (crates-io bl en blend-inspect-rs) #:use-module (crates-io))

(define-public crate-blend-inspect-rs-0.1.0 (c (n "blend-inspect-rs") (v "0.1.0") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1zyxav89nrccq4xm3khjz598176bvy64hfmqh6p74jzs52gighh9")))

(define-public crate-blend-inspect-rs-0.2.0 (c (n "blend-inspect-rs") (v "0.2.0") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0r1gamf47hazpkib4r04q29i2iqf78hac76vc7rhsrfwnabdjgdq")))

(define-public crate-blend-inspect-rs-0.3.0 (c (n "blend-inspect-rs") (v "0.3.0") (d (list (d (n "hamcrest2") (r "^0.3.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "19x3rpfkmlyjg9d5qkq114wddqbmg9d55wqw9jvc8nqdjvb8hq14")))

