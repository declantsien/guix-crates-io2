(define-module (crates-io bl en blender-exporter) #:use-module (crates-io))

(define-public crate-blender-exporter-0.0.1 (c (n "blender-exporter") (v "0.0.1") (d (list (d (n "blender-armature") (r "^0.0.1") (d #t) (k 0)) (d (n "blender-mesh") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "00m39g9p4gcbb3gmx9dbpdbkda5vz29hiwriwgpycnxk2jyfiy3d")))

