(define-module (crates-io bl en blend-formula) #:use-module (crates-io))

(define-public crate-blend-formula-0.1.0 (c (n "blend-formula") (v "0.1.0") (d (list (d (n "blend-formula-proc-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1gw1x0znb8637jibwyxfwxyjy1mwk4i777cki37iiwk14krz8q3c") (s 2) (e (quote (("wgpu" "dep:wgpu"))))))

(define-public crate-blend-formula-0.1.1 (c (n "blend-formula") (v "0.1.1") (d (list (d (n "blend-formula-proc-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (o #t) (d #t) (k 0)))) (h "18zjv0f3fi2s60j98mfcbrcqwx8wa7n46hvffqc0m5dclm6h1vs8") (s 2) (e (quote (("wgpu" "dep:wgpu"))))))

(define-public crate-blend-formula-0.1.2 (c (n "blend-formula") (v "0.1.2") (d (list (d (n "blend-formula-proc-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0ibiya4a2v5aqald76jsjq4s8a2d0hrd9ysqqh67g1v5fdx584wd") (s 2) (e (quote (("wgpu" "dep:wgpu"))))))

