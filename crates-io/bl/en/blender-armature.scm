(define-module (crates-io bl en blender-armature) #:use-module (crates-io))

(define-public crate-blender-armature-0.0.1 (c (n "blender-armature") (v "0.0.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1iv6zk2chi61avybij2r5xra1drhwn8lqvmikc1f0j5ca33h7wcg")))

(define-public crate-blender-armature-0.1.0 (c (n "blender-armature") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0h1zc8jis99x03xcavcn080s6wr5h43j6dqr95w6wg0vilg0ra66")))

(define-public crate-blender-armature-0.1.1 (c (n "blender-armature") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "160q59rqzby04lk3b67mkyxkyspqisqyyggdr2fg0is211jw2d5l")))

(define-public crate-blender-armature-0.1.2 (c (n "blender-armature") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cihbvdsfc0kamjqz9kq6wlm6d9p2yz30jzv45xr4ajg2d8bkfki")))

(define-public crate-blender-armature-0.1.3 (c (n "blender-armature") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yz7d947y60c6kglhxd96a2i26syai7j8vphvnv9xfbh12kpxjyl")))

(define-public crate-blender-armature-0.1.4 (c (n "blender-armature") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ncn1m21y75gdr32gbkyimwnq4ikb5bdlhp47s3fzsxmn6rqmmfw")))

(define-public crate-blender-armature-0.1.5 (c (n "blender-armature") (v "0.1.5") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gdfzjz2cjjp439rngskw52k31k2f3i2ibg3xy8j0bs6llxcb0vq")))

(define-public crate-blender-armature-0.1.6 (c (n "blender-armature") (v "0.1.6") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sj9w93i3l9d8rr76cfyxpgqgkp927hl1kbwaakq5kcrzxqfvkn6")))

(define-public crate-blender-armature-0.1.7 (c (n "blender-armature") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b1qyw20fix96m0py2mb8l3j1sc4b3hdh2g6z2r7nv9rs52hqqk0")))

(define-public crate-blender-armature-0.1.8 (c (n "blender-armature") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16bw0g8aa05xakmflrvxmrzdj98z28ribpd7iygk8bw5r95ww8r6")))

(define-public crate-blender-armature-0.1.9 (c (n "blender-armature") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g6dvk43sg74brh1r03bywcq519wjj57mdn173snqq56zhmgdqrn")))

(define-public crate-blender-armature-0.1.10 (c (n "blender-armature") (v "0.1.10") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0b7wmj2xh3p68nw3wyc7w5nn47g4iwlb1p8rx2h9s6ld1crwn9dk")))

(define-public crate-blender-armature-0.1.11 (c (n "blender-armature") (v "0.1.11") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p6nvmiixk4rphpbw7zib75f8ba6zkva3l0vcy8acpshgh018smb")))

(define-public crate-blender-armature-0.1.12 (c (n "blender-armature") (v "0.1.12") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06jmffrb3hyy3v08bpvmrnqvrfrvrlyzf0arwjkqr1pniqmjfyvq")))

(define-public crate-blender-armature-0.1.13 (c (n "blender-armature") (v "0.1.13") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1lwlcidyz76vh323a9gh15wkcnpb9si48s75ln3l1wy7ijgja2hm")))

(define-public crate-blender-armature-0.2.0 (c (n "blender-armature") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "103xkif9sdg10khx0ypcp8di8svw0gg6rjm11r368gx35m0bx1wm")))

(define-public crate-blender-armature-0.2.1 (c (n "blender-armature") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hal0858kwxjbcwqgli3789djkfs7kj1pb0zg0n6fcq8850r7rkp")))

(define-public crate-blender-armature-0.2.2 (c (n "blender-armature") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b8k4w4cbvz2fd94jvsw1sidphfsbh8jsjpph5w1ndyg4wzd2zs6")))

(define-public crate-blender-armature-0.2.3 (c (n "blender-armature") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wj8zzmkn086mb3jj4acdwn8982cjjq6z8lgiri1s37kviyzqmam")))

(define-public crate-blender-armature-0.2.4 (c (n "blender-armature") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1m0cgja8g05177h00zx6fjwqx5ypb514fcb278xmlqyv9g2xsciv")))

(define-public crate-blender-armature-0.2.5 (c (n "blender-armature") (v "0.2.5") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wn4za04b7z76jh72z5wn5imw2a5kvwfyrfk31z45m1xc0i156jp")))

(define-public crate-blender-armature-0.2.6 (c (n "blender-armature") (v "0.2.6") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07jrn3pwg4v8rg2q7hpfcig25s1iqrnvk5ilgpwbpdf80dpjisvz")))

(define-public crate-blender-armature-0.3.0 (c (n "blender-armature") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03xdpi8ydrkq59bv770151lzdrxn6lnivk9nf7kzvvcj6jkzlqqn")))

(define-public crate-blender-armature-0.3.1 (c (n "blender-armature") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "175ipm5piqcqph5qypsnzlhw1925yhn6b377spmbnn2gqbifhr6y")))

(define-public crate-blender-armature-0.3.2 (c (n "blender-armature") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18nkc49mjbif7wdqzhh0lc2cif428d260n4vi84ycsq1gvjc4lni")))

(define-public crate-blender-armature-0.3.3 (c (n "blender-armature") (v "0.3.3") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15k7wyg22bl8wqzm8v1izqcfv22lwlwall5bq5hj8x648wmswn33")))

(define-public crate-blender-armature-0.3.4 (c (n "blender-armature") (v "0.3.4") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ikhngzw7k4826bfx4959hihnidhvdy2zf5b1vk5j5dlllf6c83n")))

(define-public crate-blender-armature-0.3.5 (c (n "blender-armature") (v "0.3.5") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06mzsfs68z3dv3771qzcywlf95jgk2cjmv418wjgahisz45k1d4g")))

(define-public crate-blender-armature-0.4.0 (c (n "blender-armature") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wjmhsdxfmrx68j5dx44d795za7ap64lnzav4kfgjnrm19v6317f")))

(define-public crate-blender-armature-0.4.1 (c (n "blender-armature") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1byzc9k57gjl7z3mn4y62nanh0srsng46h2wc1ajxcsflp50h116")))

(define-public crate-blender-armature-0.4.2 (c (n "blender-armature") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05jbn6i7jmx5rpnlhrga6021l3a127qbma8wwi60v9hd7dai0cvn")))

(define-public crate-blender-armature-0.4.3 (c (n "blender-armature") (v "0.4.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zvcjjd74id14apfr5rvns20gfhql5glsdpwyd8dnl9bfcy3183f")))

(define-public crate-blender-armature-0.5.0 (c (n "blender-armature") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0b4ad0l4sd9vsnfkasaj4qqyl3vyh5hpfqs26v2ynklak86hxd12")))

(define-public crate-blender-armature-0.5.1 (c (n "blender-armature") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xw4hjx699fklfp6d18wfg36j2mwmb95p6wfhz9h1np748gsw2kq")))

(define-public crate-blender-armature-0.5.2 (c (n "blender-armature") (v "0.5.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "135cnj6rg9aqxszi7hhzz0w5ms1q3yjnppkm5lh9yvichdl6h91y")))

(define-public crate-blender-armature-0.5.3 (c (n "blender-armature") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01gmwg54mm13rzd7941j246l1vxaipqfb1xkhwn01x8rhvmawlzg")))

(define-public crate-blender-armature-0.6.0 (c (n "blender-armature") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jpcwlw5g1lxgh3g3g37dvmmip7r8zl0x243yy6rdca8gqavj6wk")))

(define-public crate-blender-armature-0.6.1 (c (n "blender-armature") (v "0.6.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wknq2mvmq39sx5jv6vqhk3g1a9683xcjgz4b7dris1sqfpvvz0z")))

(define-public crate-blender-armature-0.6.2 (c (n "blender-armature") (v "0.6.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03fq2h61qd1di5r0fbccldj3xrvhh7v94iavyyrpvyim26n6gzrq")))

(define-public crate-blender-armature-0.9.1 (c (n "blender-armature") (v "0.9.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0afjzp1haappxf8j7c93njdhg7w1bn2vmn9sdpibpsixnlsr49z2")))

(define-public crate-blender-armature-0.9.2 (c (n "blender-armature") (v "0.9.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pbvlm5a64m6nr6a6v22vq8cdvwv076w6nh08pdmqyivm4qsshzr")))

