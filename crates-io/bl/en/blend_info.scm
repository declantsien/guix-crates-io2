(define-module (crates-io bl en blend_info) #:use-module (crates-io))

(define-public crate-blend_info-0.1.0 (c (n "blend_info") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0lg46c6nfyffzlny0di7gka2rnkxc2flkw1qbdgmmh3q7blmmffq")))

(define-public crate-blend_info-0.1.1 (c (n "blend_info") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0dqbp9r8fcvg499k1v4b3lh67an19knbgrh864h4kxhwxd1kyf8d")))

(define-public crate-blend_info-0.2.0 (c (n "blend_info") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0hxam3szfpw0hzsckw1l9y0lf48ydk78wkr11w3dzjwrkgpmpb65")))

(define-public crate-blend_info-0.2.1 (c (n "blend_info") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0byi4swrai6wqx3rblzkdq6s49r1l8gghjlzx5ycf0g6dzghafs0")))

(define-public crate-blend_info-0.2.2 (c (n "blend_info") (v "0.2.2") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "101dx9b55c8gl6wijrbpcayg8m74w93fx1nhm46yn5h45b0349f0")))

(define-public crate-blend_info-0.2.3 (c (n "blend_info") (v "0.2.3") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1kg0zlz9vazc31wbvzh05w84vspnxbgkck6hmpz66i2r20rsq1qi")))

(define-public crate-blend_info-0.2.4 (c (n "blend_info") (v "0.2.4") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "129p8y1ngwrgfx5wjal587s79415c3hzrbndmn6jyl4kxiz46isv")))

(define-public crate-blend_info-0.2.5 (c (n "blend_info") (v "0.2.5") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0fza9c6vsnrvd14k4nca871wrjp99ylnh5bqsg0if74xpl9hard2")))

(define-public crate-blend_info-0.2.6 (c (n "blend_info") (v "0.2.6") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0l4xk4xw87n0y6y69pgsg379f0lfgwcmlz7bma9dihcfmvkva5r2")))

(define-public crate-blend_info-0.2.7 (c (n "blend_info") (v "0.2.7") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0bnph334jdf7b3kldqbhy15pfm7a9bl2j1hvgbl8n1hfjl1x1kmf")))

(define-public crate-blend_info-0.2.8 (c (n "blend_info") (v "0.2.8") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0l1bkr6j5n57kl48fjgclq1f2n8w7k2wvlcy4hw1qlj58i3vacwl")))

(define-public crate-blend_info-0.2.9 (c (n "blend_info") (v "0.2.9") (d (list (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0awd07shsk9msa511329ir2frgj2x8b1rry5r6jwki5dkrigzzps")))

