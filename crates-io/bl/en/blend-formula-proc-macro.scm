(define-module (crates-io bl en blend-formula-proc-macro) #:use-module (crates-io))

(define-public crate-blend-formula-proc-macro-0.1.0 (c (n "blend-formula-proc-macro") (v "0.1.0") (h "1c104fvhk2w3qn296gvxn90wf0464w81ii5g3skfhha334k4ffbi")))

(define-public crate-blend-formula-proc-macro-0.1.1 (c (n "blend-formula-proc-macro") (v "0.1.1") (h "00iy94cc6dfypaywkz64hggbh743wyi8vscmd6pi6h42p3ljl0ss")))

(define-public crate-blend-formula-proc-macro-0.1.2 (c (n "blend-formula-proc-macro") (v "0.1.2") (h "1prrp9l872xih3h0c4vcgijbh4avnp54d82xaqgg8zbkblnhk4jq")))

