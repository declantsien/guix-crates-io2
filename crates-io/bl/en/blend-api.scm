(define-module (crates-io bl en blend-api) #:use-module (crates-io))

(define-public crate-blend-api-2.0.0 (c (n "blend-api") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ps5f5nd9bjl81fh2yvmbvsdijavf4cs8dsn1f3dfbr29pzagy6y")))

