(define-module (crates-io bl fi blfilter) #:use-module (crates-io))

(define-public crate-blfilter-0.1.0 (c (n "blfilter") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (k 0)) (d (n "farmhash") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (k 0)))) (h "135ffd74bn8chrv811yq3qlsi9cknz4c7m5rg49ghcx3iccx5waf") (f (quote (("std" "bit-vec/std" "bit-vec/serde" "serde/std") ("default" "std"))))))

(define-public crate-blfilter-0.2.0 (c (n "blfilter") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "farmhash") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (k 0)))) (h "0c8j483v46plcid0azxln2df0dvv0j0334ns5ikr2vmx9mp4rpdp") (f (quote (("std" "serde/std" "serde/derive") ("default" "std"))))))

