(define-module (crates-io bl #{61}# bl616-pac) #:use-module (crates-io))

(define-public crate-bl616-pac-0.0.0 (c (n "bl616-pac") (v "0.0.0") (d (list (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^1.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0s43plsib3kk4iba6hjivmgwmwlgfbd8qjlhsj1jpynqsgd794vx") (f (quote (("rt" "riscv-rt"))))))

