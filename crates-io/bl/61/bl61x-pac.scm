(define-module (crates-io bl #{61}# bl61x-pac) #:use-module (crates-io))

(define-public crate-bl61x-pac-0.1.0 (c (n "bl61x-pac") (v "0.1.0") (d (list (d (n "critical_section") (r "^1.1.2") (o #t) (d #t) (k 0) (p "critical-section")) (d (n "riscv") (r "^0.11.1") (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1gjxyh5nhkdraldxv6hq3jnlyb8gd51rh9rh3wms4xw07zkb9vyg") (f (quote (("rt") ("critical-section" "critical_section" "riscv/critical-section-single-hart"))))))

