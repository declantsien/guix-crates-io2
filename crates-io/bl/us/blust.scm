(define-module (crates-io bl us blust) #:use-module (crates-io))

(define-public crate-blust-0.1.0 (c (n "blust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1shsak4m5anqxvg723y8gbp9vwyfqkpm3l006pms8h1hl4v42cll")))

(define-public crate-blust-0.1.1 (c (n "blust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1nnpjfcyfw5lz9lxis7fhlwk6w9sfpkjdpd4dgq93k5h85h1m4vv")))

(define-public crate-blust-0.1.2 (c (n "blust") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.3.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (f (quote ("simd"))) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "toml_edit") (r "^0.19.8") (d #t) (k 0)))) (h "1w9f786bywal9yj1hrmyfphk099v1impwgdsa4dmr13cwbxy8r0l")))

