(define-module (crates-io bl ow blowfish) #:use-module (crates-io))

(define-public crate-blowfish-0.0.0 (c (n "blowfish") (v "0.0.0") (h "0kxqmgvkys35nsc3yn4y1p5z05i05nxk1ary0x1cwgkygswccabg")))

(define-public crate-blowfish-0.1.0 (c (n "blowfish") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.1") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0fqzdf14pqa9hdrlfkvlxsm2ns2ilgv1klh5r7p8h6r6plq7p93g") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.2.1 (c (n "blowfish") (v "0.2.1") (d (list (d (n "block-cipher-trait") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2.4") (d #t) (k 2)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "057kryk3sjdp3i0xkbakpxwhfv4xp5y04r51xpgv3qa7pslq70bz") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.3.0 (c (n "blowfish") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1n66ngpbxpc2sgl0z50cm1kmrv044qsslfc4ax619x6rf9vf1vcm") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.3.1 (c (n "blowfish") (v "0.3.1") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "0xlk7087lhzzh5wl0zj676rcmw11abx668k92i1my1pl4rmvlgv1") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.4.0 (c (n "blowfish") (v "0.4.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1hswv302cl4vm4a28642g8g11yscjyyql1j2hndlb2161z881sva") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.5.0 (c (n "blowfish") (v "0.5.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0jqv2k80p346771wfzv2im8dzqmgaswdcj49aai9kn0dfn917l4i") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.6.0 (c (n "blowfish") (v "0.6.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "01mcj2n6cf5fhidv2qw840vrsvgwhvqs9h1cicw5kg39m45qa1hg") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.7.0 (c (n "blowfish") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0nwa0h9jkjgvvf1bwl1pnzbs6fz20g99dr02l2mppqr42436myij") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.8.0 (c (n "blowfish") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1ax736islxcbghc2lqq4vy7zn6qdigrls71lwg11m3743pyg6gzy") (f (quote (("bcrypt"))))))

(define-public crate-blowfish-0.9.0 (c (n "blowfish") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.1") (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0ka9z9hkjzz7sgzpqjzxqlsa4njq835473bn96f0dawwq1irq1wf") (f (quote (("zeroize" "cipher/zeroize") ("bcrypt")))) (r "1.56")))

(define-public crate-blowfish-0.9.1 (c (n "blowfish") (v "0.9.1") (d (list (d (n "byteorder") (r "^1.1") (k 0)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "1mw7bvj3bg5w8vh9xw9xawqh7ixk2xwsxkj34ph96b9b1z6y44p4") (f (quote (("zeroize" "cipher/zeroize") ("bcrypt")))) (r "1.56")))

