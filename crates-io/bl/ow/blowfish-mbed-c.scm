(define-module (crates-io bl ow blowfish-mbed-c) #:use-module (crates-io))

(define-public crate-blowfish-mbed-c-0.1.0 (c (n "blowfish-mbed-c") (v "0.1.0") (d (list (d (n "blowfish-mbed-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_65"))) (k 0)))) (h "041bzf07q6ilfglhb652yiza4sang8m55zfb56mcxbv1gi0xb3w2")))

(define-public crate-blowfish-mbed-c-0.1.1 (c (n "blowfish-mbed-c") (v "0.1.1") (d (list (d (n "blowfish-mbed-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "snafu") (r "^0.8.0") (f (quote ("rust_1_65"))) (k 0)))) (h "0646y253yd65pn0as4r5lkfsip9yf4sp2frg0xdibsma1ny2sw4y")))

