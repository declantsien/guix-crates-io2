(define-module (crates-io bl ow blowfish_rs) #:use-module (crates-io))

(define-public crate-blowfish_rs-0.1.0 (c (n "blowfish_rs") (v "0.1.0") (d (list (d (n "blowfish") (r "^0.9") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0lfd43ym9q1vjcp6x0f245iw59bh0ac07j8llf4l22h968i2b0hz")))

