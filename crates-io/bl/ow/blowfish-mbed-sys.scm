(define-module (crates-io bl ow blowfish-mbed-sys) #:use-module (crates-io))

(define-public crate-blowfish-mbed-sys-0.1.0 (c (n "blowfish-mbed-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1ds5bwf3x51p1qc24pzk9pp4i4gy44f0lhr0kqdxd7ycqhk372qm") (l "mbed-blowfish")))

(define-public crate-blowfish-mbed-sys-0.1.1 (c (n "blowfish-mbed-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)))) (h "13kl94pc2b4fa0r5nqpzvapvgjvnhqmk0rml2cia1a4shh80rpwm") (l "mbed-blowfish")))

