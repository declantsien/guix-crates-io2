(define-module (crates-io bl ow blowtorch) #:use-module (crates-io))

(define-public crate-blowtorch-0.1.0 (c (n "blowtorch") (v "0.1.0") (d (list (d (n "convolutions-rs") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ghsnpzh9r0m2q75r1sc1aagzp4w1y51gbyrca0jk828hl2k1sy1")))

(define-public crate-blowtorch-0.1.1 (c (n "blowtorch") (v "0.1.1") (d (list (d (n "convolutions-rs") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "120ka98vv6x9kpc0589b3lyzw61pnhxs2m0ay4vl5pbvp8jzb0j2")))

