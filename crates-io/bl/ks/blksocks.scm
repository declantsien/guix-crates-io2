(define-module (crates-io bl ks blksocks) #:use-module (crates-io))

(define-public crate-blksocks-1.0.0 (c (n "blksocks") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("socket" "net"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3") (f (quote ("timestamps"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "0dpkqspy4q5syx0vhkvmks9im9r1my2hmpz3ggj1svbmy1qskidx")))

