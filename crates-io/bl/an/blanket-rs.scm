(define-module (crates-io bl an blanket-rs) #:use-module (crates-io))

(define-public crate-blanket-rs-0.1.1 (c (n "blanket-rs") (v "0.1.1") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0w1v9xjq9agz58c0p1byp4li7hrgl629qvddrw7h2cm3wl1wk7rz") (y #t)))

(define-public crate-blanket-rs-0.1.2 (c (n "blanket-rs") (v "0.1.2") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "12qhfcqixcp64xzcxh1vas9gqhwlnppws8nmfy1lr2zkqzgjx9pn")))

(define-public crate-blanket-rs-1.0.0 (c (n "blanket-rs") (v "1.0.0") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1fzr3yqccksj0p1cl6860hm6wjcg6dbrbcr21jkmjpn1cmpncqj9")))

(define-public crate-blanket-rs-1.0.1 (c (n "blanket-rs") (v "1.0.1") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1slsxqmzzghhaazd7g5xjfyif0n2hlirzmmygjzy9dq3nvx6n8q5")))

(define-public crate-blanket-rs-2.0.0-alpha-rc2 (c (n "blanket-rs") (v "2.0.0-alpha-rc2") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1mawp040729qf7cx1fbfbv4byb6jk5i17s7mdzy52c35vlg8n3g7")))

(define-public crate-blanket-rs-2.0.0-alpha-rc3 (c (n "blanket-rs") (v "2.0.0-alpha-rc3") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1z2j80ci8bx75q7bd2bprw6dblwgp8i0g8vxcxiza5y47v017n44") (y #t)))

(define-public crate-blanket-rs-2.0.0-alpha-rc4 (c (n "blanket-rs") (v "2.0.0-alpha-rc4") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0j3430mknvbycf5830l1h4cb9q8ffh5mraw61aygr5l1nvxjhl17")))

