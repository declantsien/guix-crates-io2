(define-module (crates-io bl an blanka) #:use-module (crates-io))

(define-public crate-blanka-0.1.0 (c (n "blanka") (v "0.1.0") (h "1rcy00d1wmr126ax7xaw4r8x6p0dr24gp6nmxhshdy7xxhl7fjyn") (y #t)))

(define-public crate-blanka-0.1.1 (c (n "blanka") (v "0.1.1") (h "191nv6pv65b2z1lkp954pjbxymilmpwyn911sifpfyv92dj1l622") (y #t)))

