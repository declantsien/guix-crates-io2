(define-module (crates-io bl ek blek) #:use-module (crates-io))

(define-public crate-blek-0.1.0 (c (n "blek") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tera") (r "^1.5.0") (d #t) (k 0)))) (h "0az1aig7hsmxay5b2zf12b6w18sqzmac4nw1japjjyaaq9v5iqqf")))

