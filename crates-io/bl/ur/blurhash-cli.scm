(define-module (crates-io bl ur blurhash-cli) #:use-module (crates-io))

(define-public crate-blurhash-cli-0.1.0 (c (n "blurhash-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "blurhash") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "0p9awzv1za8rd2pzifspjypcv5x1z5r7i7nd6pmx4c166wqvx5v4")))

(define-public crate-blurhash-cli-0.1.1 (c (n "blurhash-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "blurhash") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)))) (h "1aaq3bn9za103ys33jaz3mykhzbqk6w5ilf4bchk6hs3p4hvapfj")))

