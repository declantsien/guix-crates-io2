(define-module (crates-io bl ur blurhash-wasm) #:use-module (crates-io))

(define-public crate-blurhash-wasm-0.1.0 (c (n "blurhash-wasm") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "13in7mdg8qprr9n4n2rxmmmm40wv3zcfnd2inrb50sb3pr2qgv3i") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-blurhash-wasm-0.2.0 (c (n "blurhash-wasm") (v "0.2.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.22.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "19grd6m7y0680yql7bv8cps0ms66wdl08xjn3fz4zrqd352966iv") (f (quote (("default" "console_error_panic_hook"))))))

