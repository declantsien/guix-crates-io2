(define-module (crates-io bl ur blurmock) #:use-module (crates-io))

(define-public crate-blurmock-0.1.0 (c (n "blurmock") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1932afv4vxnkrsja65b4rlk6f3yjj4a7y4amp6q4c4jcapxjkj9w")))

(define-public crate-blurmock-0.1.1 (c (n "blurmock") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1cf20n771qgh4w4d6b0inp6d8g04n19fkdqnm7h52ydwf9rlq0y3")))

(define-public crate-blurmock-0.1.2 (c (n "blurmock") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1nm8dh9gikl05hw4yc36qc4d5cg2z166qdnx7cyhzd1v7bd75pb8")))

(define-public crate-blurmock-0.1.3 (c (n "blurmock") (v "0.1.3") (d (list (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1m3mqdjiqnc7ypy4wx5f3fpyh1s5g0551gcvj4hx23w32zb0y5cw")))

