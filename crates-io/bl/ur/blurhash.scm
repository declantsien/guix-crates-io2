(define-module (crates-io bl ur blurhash) #:use-module (crates-io))

(define-public crate-blurhash-0.1.0 (c (n "blurhash") (v "0.1.0") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 2)))) (h "01jypnzrxla2c5dzpw8pq971pcv31x38fbzh67hdd5ixxvd0zgjy")))

(define-public crate-blurhash-0.1.1 (c (n "blurhash") (v "0.1.1") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 2)))) (h "1d2b958s816iwlad2lhsp7z5v97jabhwir3zl957iy2rpz4f8wc6")))

(define-public crate-blurhash-0.2.0 (c (n "blurhash") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gdk-pixbuf") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)))) (h "1pjd2qjv4x8khrb9ac9dv4zicc67a306ql4kbanf5qyabgrdypda") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image") ("gdk-pixbuf" "dep:gdk-pixbuf"))))))

(define-public crate-blurhash-0.2.1 (c (n "blurhash") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "gdk-pixbuf") (r ">=0.18, <=0.19") (o #t) (d #t) (k 0)) (d (n "image") (r ">=0.23, <=0.24") (o #t) (d #t) (k 0)) (d (n "image") (r ">=0.23, <=0.24") (d #t) (k 2)))) (h "178w00qfdhibnx8liyjrzylrigl775wzz1xwqna4kwir63h7hnsn") (f (quote (("default")))) (s 2) (e (quote (("image" "dep:image") ("gdk-pixbuf" "dep:gdk-pixbuf"))))))

