(define-module (crates-io bl ur blurry) #:use-module (crates-io))

(define-public crate-blurry-0.1.0 (c (n "blurry") (v "0.1.0") (d (list (d (n "crunch") (r "^0.5.3") (d #t) (k 0)) (d (n "glow") (r "^0.12.1") (d #t) (k 2)) (d (n "glutin") (r "^0.29.1") (d #t) (k 2)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.18.1") (d #t) (k 0)))) (h "0fi0k2dsfaa1gav11mpvx5l0nah8szb2n4m3s0j6xh6qj1x27q2m")))

(define-public crate-blurry-0.2.0 (c (n "blurry") (v "0.2.0") (d (list (d (n "crunch") (r "^0.5.3") (d #t) (k 0)) (d (n "glow") (r "^0.12.1") (d #t) (k 2)) (d (n "glutin") (r "^0.29.1") (d #t) (k 2)) (d (n "png") (r "^0.17.7") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.18.1") (d #t) (k 0)))) (h "1cqk8vcnl43pnnpmadmw5nfb92nlimbh2d7zxdg7my60parljbp6")))

