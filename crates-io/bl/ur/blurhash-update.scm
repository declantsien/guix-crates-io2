(define-module (crates-io bl ur blurhash-update) #:use-module (crates-io))

(define-public crate-blurhash-update-0.1.0 (c (n "blurhash-update") (v "0.1.0") (d (list (d (n "blurhash") (r "^0.2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "image") (r "^0.24.8") (d #t) (k 2)))) (h "1s4fz6dqnag05p8yjwvwsmrncjk7rx7fcd286gfz44mrmr2ymddz")))

