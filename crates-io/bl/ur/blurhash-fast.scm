(define-module (crates-io bl ur blurhash-fast) #:use-module (crates-io))

(define-public crate-blurhash-fast-0.1.0 (c (n "blurhash-fast") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0fzjksb5bl8ci6x7jxcwha25kkzvr5mdymk33f5ndac79g8j4dn6")))

