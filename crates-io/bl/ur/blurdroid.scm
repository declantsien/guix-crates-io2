(define-module (crates-io bl ur blurdroid) #:use-module (crates-io))

(define-public crate-blurdroid-0.1.0 (c (n "blurdroid") (v "0.1.0") (h "02p7bswyladxg5b1qmqi506qw33b5rn4qmhyqqbk8hvndjbzv4vn")))

(define-public crate-blurdroid-0.1.1 (c (n "blurdroid") (v "0.1.1") (h "17n7ry6c909y3krhdkdw24lv8lrf4m13map196fkwn3b6silxkjz")))

(define-public crate-blurdroid-0.1.2 (c (n "blurdroid") (v "0.1.2") (h "1f3y3wnv6di9fpslkksapxnfhbvlqzx6n0hf858girqq72xnza7l")))

(define-public crate-blurdroid-0.1.3 (c (n "blurdroid") (v "0.1.3") (h "0r5wyilka1m43c3c9fhhgp6r35025a772vqpzkllm0lp3bmdv04q")))

(define-public crate-blurdroid-0.1.4 (c (n "blurdroid") (v "0.1.4") (h "0s2scp663b16d5c6icmr67s46sx1i2pra9rhgpyfpgi9km8vmnnp")))

(define-public crate-blurdroid-0.1.5 (c (n "blurdroid") (v "0.1.5") (h "0qzkmhw4zq4q1frbwvxq155gnx1ih8wgspxjbp37z5zhv8ycaxim")))

(define-public crate-blurdroid-0.1.6 (c (n "blurdroid") (v "0.1.6") (h "15nj0xgvdvhlhxvakm32ml6z5ghncj0v57cg2ablfw17vmbkbchr")))

