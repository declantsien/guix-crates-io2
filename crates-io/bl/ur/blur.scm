(define-module (crates-io bl ur blur) #:use-module (crates-io))

(define-public crate-blur-0.0.1 (c (n "blur") (v "0.0.1") (h "1nc2hj1dqmxrfnda5y6izpvjwg0x5zh1bmzy43b0xlsw3fa5xbr7") (y #t)))

(define-public crate-blur-0.0.4 (c (n "blur") (v "0.0.4") (h "1nh5ip8ghfnc50x6ghbcmxfihb93g5lx9k6amcbn5w66g3irjhf7") (y #t)))

