(define-module (crates-io bl ur blurz) #:use-module (crates-io))

(define-public crate-blurz-0.1.0 (c (n "blurz") (v "0.1.0") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0fjkdpwdyp8qmpbp9igi2z411pdxvccbyxiwbgd7klvam31qvlsi")))

(define-public crate-blurz-0.1.1 (c (n "blurz") (v "0.1.1") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1x5izay2fy3d163nh2a7yzasf676prvyr4z80gxcrmsq9f7inlak")))

(define-public crate-blurz-0.1.2 (c (n "blurz") (v "0.1.2") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wx9h1wcn245skydcyk6hxphxij9f55vnfx4ay0p0wbm2kjwpqwl") (y #t)))

(define-public crate-blurz-0.1.3 (c (n "blurz") (v "0.1.3") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07xyx3srq8s6pz3vjmhxpgspa3jh4fc9y72im4nm02ck8vzyqa0g")))

(define-public crate-blurz-0.1.4 (c (n "blurz") (v "0.1.4") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1illakfga0svc4wln5d2sb4knjmzflzpr6kxvinkxmcxl2pglwwp")))

(define-public crate-blurz-0.1.5 (c (n "blurz") (v "0.1.5") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "14zfs6bmq667d7sarpfiah8ak9ab9f5ya99djpdgxazlq77b58xc")))

(define-public crate-blurz-0.1.6 (c (n "blurz") (v "0.1.6") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1lk919x4cjqadmffy4wwy5fzpff4x1lf6gw4v6yqi1xbk916bc1a")))

(define-public crate-blurz-0.1.7 (c (n "blurz") (v "0.1.7") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v84h1agak3i48sc668nr44whzwviwg5dbbpbw9qvxdpdppvr3x1")))

(define-public crate-blurz-0.1.8 (c (n "blurz") (v "0.1.8") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06ka35ahqhz7v8wfdsjk91a0s8r2zrr7qqc69dibqii83n938sqp") (y #t)))

(define-public crate-blurz-0.2.0 (c (n "blurz") (v "0.2.0") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1p3kywcrjj2c5mf0xnc3sg0xfrbxc6b41nyvc5br59igqrm4y4wn")))

(define-public crate-blurz-0.2.1 (c (n "blurz") (v "0.2.1") (d (list (d (n "dbus") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "02160zv8sghxkyv6bxvpqj3jk15bsipf91l65dmmy6ssimn7jjad")))

(define-public crate-blurz-0.2.2 (c (n "blurz") (v "0.2.2") (d (list (d (n "dbus") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wsbp7viwdvka59fxfc6irqyc1zmyfbh01sifc23miki9h7xlfz7")))

(define-public crate-blurz-0.2.3 (c (n "blurz") (v "0.2.3") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)))) (h "02s77mf63g9ajx8095s0whvnkmw9srp3jr4zl0bpcxzzmg2sysp4") (y #t)))

(define-public crate-blurz-0.2.4 (c (n "blurz") (v "0.2.4") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "1j0967d1i7ij1n1jncjgizyv94b4vpzhjj1nsk3bk49cfh973d1s") (y #t)))

(define-public crate-blurz-0.3.0 (c (n "blurz") (v "0.3.0") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "02n5qfcg2hlzgq0vdm05a9m3wx85aq8s0a4ssbmfhzzngwryinpn")))

(define-public crate-blurz-0.4.0 (c (n "blurz") (v "0.4.0") (d (list (d (n "dbus") (r "^0.6") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)))) (h "0nga752pfi014kbdm7nn0cgvmxd3mik9jbb74jrp7lzis3s2vg5i")))

