(define-module (crates-io bl _s bl_save) #:use-module (crates-io))

(define-public crate-bl_save-0.1.0 (c (n "bl_save") (v "0.1.0") (h "1wh0gqjh44yj4l9m34vnhfiykx9m31dlq0zrhrh0vjircyrhwsrs")))

(define-public crate-bl_save-0.2.0 (c (n "bl_save") (v "0.2.0") (h "03y58yd7jj7wzh6im29dz9fs3674s0swdzajv6nfqvagljsanr6b")))

