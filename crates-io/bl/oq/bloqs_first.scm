(define-module (crates-io bl oq bloqs_first) #:use-module (crates-io))

(define-public crate-bloqs_first-0.1.0 (c (n "bloqs_first") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10") (d #t) (k 0)) (d (n "cw2") (r "^0.10") (d #t) (k 0)) (d (n "schemars") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "134572jdir7jysln5ai0mbmd8m4sr2vphycw0j58v9dbl0xng5mh") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

