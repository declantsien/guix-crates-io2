(define-module (crates-io xq ue xquery) #:use-module (crates-io))

(define-public crate-xquery-0.1.0 (c (n "xquery") (v "0.1.0") (h "1j0cyi1h4qz8rq1q8s2n4fidggbbax40hqrr6gndjh5m8bxx265x")))

(define-public crate-xquery-0.1.1 (c (n "xquery") (v "0.1.1") (h "00wxxln8f52w1k92ysbbf9349xbziqchwscrdiwwi8czsfbi5daa")))

