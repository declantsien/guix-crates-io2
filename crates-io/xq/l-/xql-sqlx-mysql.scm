(define-module (crates-io xq l- xql-sqlx-mysql) #:use-module (crates-io))

(define-public crate-xql-sqlx-mysql-0.1.0 (c (n "xql-sqlx-mysql") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "mysql"))) (k 0)))) (h "1dj2ff1xn1322qzvgq9h5dzc3g5a7rlpmc0kxghgzr6y3vwvcy4d")))

(define-public crate-xql-sqlx-mysql-0.2.0 (c (n "xql-sqlx-mysql") (v "0.2.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "mysql"))) (k 0)))) (h "0f3nkal1cs5g726j35zaa95pfk38csnncbk5092hd2ggdb6vzdaz")))

