(define-module (crates-io xq l- xql-sqlx-postgres) #:use-module (crates-io))

(define-public crate-xql-sqlx-postgres-0.1.0 (c (n "xql-sqlx-postgres") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres"))) (k 0)))) (h "04bky7mlxj8pfhcrzwh4p5k6djkpzng9jrrlpzbvj0irynvj5l43")))

(define-public crate-xql-sqlx-postgres-0.2.0 (c (n "xql-sqlx-postgres") (v "0.2.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres"))) (k 0)))) (h "0zvfp31icqmci315bv1vvhrzi6jnag4cnjkm1sbr97qc5vwxbx2a")))

