(define-module (crates-io xq l- xql-sqlx-sqlite) #:use-module (crates-io))

(define-public crate-xql-sqlx-sqlite-0.1.0 (c (n "xql-sqlx-sqlite") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite"))) (k 0)))) (h "0qif72fqaq67a02z7631xpx0c59hm2y32ryyrh8h9ihhcgnhi8md")))

(define-public crate-xql-sqlx-sqlite-0.2.0 (c (n "xql-sqlx-sqlite") (v "0.2.0") (d (list (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "sqlite"))) (k 0)))) (h "0lry38zwy9bg207dsqaxi97acf1jmr46kj9zrmmq56qsp9vc5fk8")))

