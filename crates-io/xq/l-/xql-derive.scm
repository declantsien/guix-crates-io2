(define-module (crates-io xq l- xql-derive) #:use-module (crates-io))

(define-public crate-xql-derive-0.1.0 (c (n "xql-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "05qm0fap3z9wbyf4hkkab3dl734zqxaajngp528gxla73v0nmy2v")))

