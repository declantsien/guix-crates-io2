(define-module (crates-io xq _n xq_notification) #:use-module (crates-io))

(define-public crate-xq_notification-0.0.1 (c (n "xq_notification") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0s7azzcqyhs9xl8v4vy8y50w08avzkjj1i2d8c845xsaimhx525b")))

