(define-module (crates-io xq -l xq-lang) #:use-module (crates-io))

(define-public crate-xq-lang-0.0.1 (c (n "xq-lang") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (d #t) (k 0)) (d (n "lexgen") (r "^0.10.0") (d #t) (k 0)) (d (n "lexgen_util") (r "^0.10.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12hrihxdskz7rav3ffippckii0ld0g4big7wky1g8ddkidjid2g3")))

