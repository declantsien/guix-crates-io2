(define-module (crates-io xq -d xq-derive) #:use-module (crates-io))

(define-public crate-xq-derive-0.1.0 (c (n "xq-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "=1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.63") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qr6d4v38fikfvjzhb1v6mzihyk2kw83gjb06p9xygzjzrpdxggk")))

