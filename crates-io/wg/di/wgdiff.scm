(define-module (crates-io wg di wgdiff) #:use-module (crates-io))

(define-public crate-wgdiff-0.1.0 (c (n "wgdiff") (v "0.1.0") (h "0ai1j8f5x83k994qbc08i8r8g4nx97v3qdi6chmn9dwc5qz9wdrz")))

(define-public crate-wgdiff-0.2.0 (c (n "wgdiff") (v "0.2.0") (h "0r5713y3m9nd6s487x7vv7jjl6qkm25zphpawkycsjz4z28xdrh0")))

(define-public crate-wgdiff-0.3.0 (c (n "wgdiff") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0q0ldqgmdw0jd91h7ywxizhln4phk364gdm9dplznyffpmyfnmz8")))

(define-public crate-wgdiff-0.4.0 (c (n "wgdiff") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "08xy0cmlq5bbc2llpwn41yw3gqwcla1hsiwk4ddh8dhjmlqm55mc") (y #t)))

(define-public crate-wgdiff-0.4.1 (c (n "wgdiff") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0ybmipgb8nrphpynglxjg9z1h7m74dg7jnqjck6pz8sx4gqbdfhz") (y #t)))

(define-public crate-wgdiff-0.4.2 (c (n "wgdiff") (v "0.4.2") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1j7ajsfddyz635zyl5l8q46ngp1rq3bmll6kgzzvdyvxm4iyd4h8") (y #t)))

(define-public crate-wgdiff-0.4.3 (c (n "wgdiff") (v "0.4.3") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1i578rvsmbdawf85dnr5xi8galc53qqm93zrlyw0b1w4pwlsyj62")))

