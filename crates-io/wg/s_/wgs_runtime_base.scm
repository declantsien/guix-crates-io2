(define-module (crates-io wg s_ wgs_runtime_base) #:use-module (crates-io))

(define-public crate-wgs_runtime_base-0.0.1 (c (n "wgs_runtime_base") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wgs_core") (r "^0.0.1") (d #t) (k 0)))) (h "0lp1827yg4cx7x0r61lcsxy29gxwxiak57n6m479h8xq8qpsa7is")))

(define-public crate-wgs_runtime_base-0.0.2 (c (n "wgs_runtime_base") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wgs_core") (r "^0.0.2") (d #t) (k 0)))) (h "1fb896w6q28v6x3h15d8h4x5mi0pchij3gc5whrlr5pjsba7i1p4")))

(define-public crate-wgs_runtime_base-0.1.0 (c (n "wgs_runtime_base") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wgs_core") (r "^0.1") (d #t) (k 0)))) (h "15p4b77y7cvk38vlv57748y12p292qd6190l97b4pgkzym5i8jp6")))

(define-public crate-wgs_runtime_base-0.1.1 (c (n "wgs_runtime_base") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wgs_core") (r "^0.1") (d #t) (k 0)))) (h "0zv943hn5fnlx3a7a13winzky9s2ghq0b7wkiyx2d4dpbbydyqwj")))

(define-public crate-wgs_runtime_base-0.1.2 (c (n "wgs_runtime_base") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wgs_core") (r "^0.1") (d #t) (k 0)))) (h "1c90ssl4b1vymzzzm7qlr9gpn5di3vn41xz2b2k8psq87iifj3bf")))

