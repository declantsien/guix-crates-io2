(define-module (crates-io wg im wgimage) #:use-module (crates-io))

(define-public crate-wgimage-0.1.0 (c (n "wgimage") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.1") (d #t) (k 0)))) (h "0hsygd6bnkcbcd8c2dsqygcpz76kayr5d967h29ix6z1bfc58ms3")))

(define-public crate-wgimage-0.2.0 (c (n "wgimage") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.1") (d #t) (k 0)))) (h "0mi97nng80hc44zwf8iq7i7c13dkv0qm94ipwxfra1pwn9i90ch5")))

