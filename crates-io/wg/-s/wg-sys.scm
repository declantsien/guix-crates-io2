(define-module (crates-io wg -s wg-sys) #:use-module (crates-io))

(define-public crate-wg-sys-0.0.1 (c (n "wg-sys") (v "0.0.1") (h "1jqsmmnf1m50zhm0iq1953hmd6ddj1x84v31xx3x0kxf7xz4fl93")))

(define-public crate-wg-sys-0.1.0 (c (n "wg-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)))) (h "0p5jyf4wvlnrj97i8q064jq0x8vxlp0hf2spyz9lllgwa2hc8m4i")))

