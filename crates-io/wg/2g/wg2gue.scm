(define-module (crates-io wg #{2g}# wg2gue) #:use-module (crates-io))

(define-public crate-wg2gue-0.1.0 (c (n "wg2gue") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "boringtun") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "net" "sync" "macros" "io-util" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (o #t) (d #t) (k 0)))) (h "1wyay8g9rcpm1k764y8zbzls6hblgqanc9083a2gl7wb90qdg6f3") (f (quote (("default" "tracing-subscriber" "tracing/release_max_level_debug"))))))

