(define-module (crates-io wg -t wg-toolkit-cli) #:use-module (crates-io))

(define-public crate-wg-toolkit-cli-0.1.0 (c (n "wg-toolkit-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "wg-toolkit") (r "^0.2") (d #t) (k 0)))) (h "0rj396032bkfnkrwycrb50cmkrndp7yf5cp49qyvbf5jbsac9wb7")))

(define-public crate-wg-toolkit-cli-0.4.0 (c (n "wg-toolkit-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "wg-toolkit") (r "^0.4.0") (d #t) (k 0)))) (h "0j9ihq3a2avan1wsd1nw2ijnhrg4qlal1kf6nld1843rk3v1vclc")))

(define-public crate-wg-toolkit-cli-0.4.1 (c (n "wg-toolkit-cli") (v "0.4.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "wg-toolkit") (r "^0.4.0") (d #t) (k 0)))) (h "17wr8n26ii2lscgg8syhlgby1jg3r3g4k0bf2h66sds89n74kcgw")))

