(define-module (crates-io wg re wgrepp) #:use-module (crates-io))

(define-public crate-wgrepp-0.1.0 (c (n "wgrepp") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.7.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.10.1") (d #t) (k 0)))) (h "1akra7pi6j95wvgacy39nwsxc725ahb5d23bl4d5p0f5q1l0lh23")))

