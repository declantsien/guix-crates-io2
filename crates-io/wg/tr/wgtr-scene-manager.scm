(define-module (crates-io wg tr wgtr-scene-manager) #:use-module (crates-io))

(define-public crate-wgtr-scene-manager-0.1.0 (c (n "wgtr-scene-manager") (v "0.1.0") (h "0kl4ghll3alr17f9f25zbclqki01ck6gxfpx5ilpgqw5qqmg220g")))

(define-public crate-wgtr-scene-manager-0.2.0 (c (n "wgtr-scene-manager") (v "0.2.0") (h "1jpwq25fa7d0lxssilx3nh7dgp6w08fm6fr9sswbhccqqy2kfbr0")))

