(define-module (crates-io wg tr wgtr-ecs) #:use-module (crates-io))

(define-public crate-wgtr-ecs-0.1.0 (c (n "wgtr-ecs") (v "0.1.0") (h "03byyvz7zxwf3s7i5abqdg5lxn99zp0glrzgw4kafslgxf81mvz3")))

(define-public crate-wgtr-ecs-0.2.0 (c (n "wgtr-ecs") (v "0.2.0") (h "0fjcwycrlsr8xzscbbbr6zb6x8h20a1s0j0a4qgcx3dckggxg714")))

