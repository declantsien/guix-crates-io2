(define-module (crates-io wg pu wgpu-conveyor) #:use-module (crates-io))

(define-public crate-wgpu-conveyor-0.1.0 (c (n "wgpu-conveyor") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "wgpu") (r "^0.6") (d #t) (k 0)))) (h "1i4zr3a62wcwjhqksraxc60xwydm25mk0h4v8bl2zscg1ak4zqpb")))

(define-public crate-wgpu-conveyor-0.2.0 (c (n "wgpu-conveyor") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "wgpu") (r "^0.7") (d #t) (k 0)))) (h "13xaqzfygr6rr82ddg6b222i8x6qr7sl9iry1kxf4r4rrd7v89gv")))

