(define-module (crates-io wg pu wgpu_sort) #:use-module (crates-io))

(define-public crate-wgpu_sort-0.1.0 (c (n "wgpu_sort") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "float-ord") (r "^0.3.2") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)))) (h "15zyl6abyvrdqyhb7nnma1bq22kdcjnbyscjcvdlpbrr40dy91j0")))

