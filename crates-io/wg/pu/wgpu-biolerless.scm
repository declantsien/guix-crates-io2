(define-module (crates-io wg pu wgpu-biolerless) #:use-module (crates-io))

(define-public crate-wgpu-biolerless-0.1.0 (c (n "wgpu-biolerless") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.4.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "0xd9pgfl3k865kfm4bxq5f31yqy1cm1f9kmhm6xry8wfqh5j66al")))

(define-public crate-wgpu-biolerless-0.1.1 (c (n "wgpu-biolerless") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.4.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)))) (h "149qjn707cpmqm3mjiwxvbrs7rj2ix7hf1vy2kdcs7z4a81g0jhh")))

(define-public crate-wgpu-biolerless-0.1.2 (c (n "wgpu-biolerless") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.4.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.13.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (o #t) (d #t) (k 0)))) (h "1ny7vysbjkg2hhsj6m0ha372mgs116nbj3927mrd6w522szd5wad") (f (quote (("debug_labels")))) (s 2) (e (quote (("winit" "dep:winit"))))))

