(define-module (crates-io wg pu wgpu-font-renderer) #:use-module (crates-io))

(define-public crate-wgpu-font-renderer-0.1.0 (c (n "wgpu-font-renderer") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.16.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owned_ttf_parser") (r "^0.21.0") (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "swash") (r "^0.1.15") (d #t) (k 0)) (d (n "wgpu") (r "^0.20") (d #t) (k 0)) (d (n "wgpu") (r "^0.20") (d #t) (k 2)) (d (n "winit") (r "^0.29.10") (f (quote ("rwh_05"))) (d #t) (k 2)))) (h "062gi0rs9j67zwb1yfz3nq14h3g5gxh1lq0624bhig5zw6nmfycv")))

