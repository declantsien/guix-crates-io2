(define-module (crates-io wg pu wgpu-noboiler) #:use-module (crates-io))

(define-public crate-wgpu-noboiler-0.1.0 (c (n "wgpu-noboiler") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "06j5xzan26frkg1229j1ky0m9m7rbr8l4cxbzqv9k3ymbzmhjbfm")))

(define-public crate-wgpu-noboiler-0.1.1 (c (n "wgpu-noboiler") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "019181rk0v4f9d63jf5kfvjvg179ba802l1pjn19fm3jjn6w99p9")))

(define-public crate-wgpu-noboiler-0.1.2 (c (n "wgpu-noboiler") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "07zpbhsby177y2v29w2d82yb062lh81j5pkg2sz3d5n54cqx3zn2")))

(define-public crate-wgpu-noboiler-0.1.3 (c (n "wgpu-noboiler") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1hgw2djq4fz0p0yn22gcx0345z5ssn42xh0md59adpwb3x1arap4")))

(define-public crate-wgpu-noboiler-0.1.4 (c (n "wgpu-noboiler") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "0zxzwfni7xkk6jpa4g7naaqcg5p79ykqyv69gh65rk9w1cnasbdi")))

(define-public crate-wgpu-noboiler-0.1.5 (c (n "wgpu-noboiler") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "1sbr8mw8xar7gzhdfak0jicwci26z2ficw0q1ahb7zlghnwhyis3")))

(define-public crate-wgpu-noboiler-0.1.6 (c (n "wgpu-noboiler") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.12.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 0)))) (h "06wcxa81i9hm70v2mkcsarlv1nmz15hri6gmxn1ikrgd72f7lg5a")))

(define-public crate-wgpu-noboiler-0.2.0 (c (n "wgpu-noboiler") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "11kj1v8m2wz2facp6yjsbp23w0c9i5n75y5dvd15nvi92bi3l5zd")))

(define-public crate-wgpu-noboiler-0.3.0 (c (n "wgpu-noboiler") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 0)))) (h "04bi010b587366x1yzzm73cwwkhbcqkwl6g407dkmzx5l9n48h68")))

(define-public crate-wgpu-noboiler-0.3.1 (c (n "wgpu-noboiler") (v "0.3.1") (d (list (d (n "bytemuck") (r "^1.13.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.15.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 0)))) (h "17b9zyg72lafvdh92f86vbyy1xb230cakkdwsfs37dw17rpwaaph")))

