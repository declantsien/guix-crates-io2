(define-module (crates-io wg pu wgpu-traits) #:use-module (crates-io))

(define-public crate-wgpu-traits-0.0.0 (c (n "wgpu-traits") (v "0.0.0") (d (list (d (n "gltf") (r "^0.14.0") (f (quote ("extras" "names"))) (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.1") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "00n1875y45lbj8bf27d0nlkcwq5dyx4mbdyjx53cnzy79spr847f")))

