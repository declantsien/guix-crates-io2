(define-module (crates-io wg pu wgpu-mipmap) #:use-module (crates-io))

(define-public crate-wgpu-mipmap-0.1.0 (c (n "wgpu-mipmap") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (f (quote ("png"))) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "renderdoc") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "wgpu") (r "^0.6") (d #t) (k 0)))) (h "1w1dymsfd1xbgz5sa4q8zhi9fg0h2fhxbvgh5z2fi7dql6d5ss2m") (f (quote (("debug" "renderdoc"))))))

