(define-module (crates-io wg pu wgpu-subscriber) #:use-module (crates-io))

(define-public crate-wgpu-subscriber-0.1.0 (c (n "wgpu-subscriber") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thread-id") (r "^3") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-log") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "09zdjmi3dhy4ff3dq4260nf6kl5cslfhs2xxgdbnxn40ch6mp5ci")))

