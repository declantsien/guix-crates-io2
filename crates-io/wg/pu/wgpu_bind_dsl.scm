(define-module (crates-io wg pu wgpu_bind_dsl) #:use-module (crates-io))

(define-public crate-wgpu_bind_dsl-0.1.0 (c (n "wgpu_bind_dsl") (v "0.1.0") (d (list (d (n "wgpu") (r "^0.5.0") (d #t) (k 2)))) (h "1cf1jya0004nallrslk6kscnspslc4z2s6ra2sifyl2b5szz9zgv")))

(define-public crate-wgpu_bind_dsl-0.1.1 (c (n "wgpu_bind_dsl") (v "0.1.1") (d (list (d (n "wgpu") (r "^0.5.0") (d #t) (k 2)))) (h "00yfsw0arfsy9vw64388lgajjc5j2rc3jr1n00fa1jff7kk1271l")))

