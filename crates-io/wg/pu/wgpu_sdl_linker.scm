(define-module (crates-io wg pu wgpu_sdl_linker) #:use-module (crates-io))

(define-public crate-wgpu_sdl_linker-1.0.0 (c (n "wgpu_sdl_linker") (v "1.0.0") (d (list (d (n "sdl2") (r "^0.36.0") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "0a3wcsmgn3203s2nqmx9g5nnjk60dg2cpkvh1fjwx6iq5ncqc54v")))

(define-public crate-wgpu_sdl_linker-1.1.0 (c (n "wgpu_sdl_linker") (v "1.1.0") (d (list (d (n "sdl2") (r "^0.36.0") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "0i8xn53dswf82ybi5kpr5xcip85l5mxjkl0m9i973f0i7xzrfzig")))

(define-public crate-wgpu_sdl_linker-1.2.0 (c (n "wgpu_sdl_linker") (v "1.2.0") (d (list (d (n "sdl2") (r "^0.36.0") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "0z6n0j2yg5130kr29xip5pk7r7xpkgd4xdspnx0bkzdrmzb2gsa5")))

(define-public crate-wgpu_sdl_linker-1.3.0 (c (n "wgpu_sdl_linker") (v "1.3.0") (d (list (d (n "sdl2") (r "^0.36.0") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "1l6rrgslmnw3lr9q37z4gghcv4d0b2rjmrxrsl0l1g032pinfp1i")))

