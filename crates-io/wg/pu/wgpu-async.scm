(define-module (crates-io wg pu wgpu-async) #:use-module (crates-io))

(define-public crate-wgpu-async-0.16.2 (c (n "wgpu-async") (v "0.16.2") (d (list (d (n "pollster") (r "^0.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.16.2") (d #t) (k 0)))) (h "1xbw8k2bng9qvqpyva6fhdq375ddfwg6p7hcyc5bfj6cvn6h4kz8") (y #t)))

(define-public crate-wgpu-async-0.16.3 (c (n "wgpu-async") (v "0.16.3") (d (list (d (n "pollster") (r "^0.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.16.2") (d #t) (k 0)))) (h "15w0jbiiv698nan31fqngbik6yxw5309wd9kg5q4fqjrdxhi49h6")))

(define-public crate-wgpu-async-0.16.4 (c (n "wgpu-async") (v "0.16.4") (d (list (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "wgpu") (r "^0.16.2") (d #t) (k 0)))) (h "1cc4lr2l6842bda9lkvk28d4vszpicwns4mj8i1sxs6m79ff8kdg")))

(define-public crate-wgpu-async-0.17.1 (c (n "wgpu-async") (v "0.17.1") (d (list (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "wgpu") (r "^0.17.1") (d #t) (k 0)))) (h "090c81jn710rqzp46p9f9s37kbphb3hvihmzq0c2shsaaxjmfl2w")))

(define-public crate-wgpu-async-0.18.0 (c (n "wgpu-async") (v "0.18.0") (d (list (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "1z9zlpr26zf21fa0mhysyvqq7zrl0cxx45mhhblg6wpr0p6b56qq")))

(define-public crate-wgpu-async-0.18.1 (c (n "wgpu-async") (v "0.18.1") (d (list (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "06rwqivg4bn62sgra3g4gdsp8bc8zzxmkig50bp3llyaq2gcfp49")))

(define-public crate-wgpu-async-0.19.0 (c (n "wgpu-async") (v "0.19.0") (d (list (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wgpu") (r "^0.19.0") (d #t) (k 0)))) (h "0wv72a5w6sp6bnrhfb19nqngkg0sxv4hbprg32cc21f1k94j316b")))

(define-public crate-wgpu-async-0.19.1 (c (n "wgpu-async") (v "0.19.1") (d (list (d (n "pollster") (r "^0.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)))) (h "0jjqd15dgfmwaih641f3s9k85jdkn8kww104si6ljwzbnsg4b4vz")))

