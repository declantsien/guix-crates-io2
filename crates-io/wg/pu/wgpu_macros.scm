(define-module (crates-io wg pu wgpu_macros) #:use-module (crates-io))

(define-public crate-wgpu_macros-0.1.0 (c (n "wgpu_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "extra-traits"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.12.0") (d #t) (k 2)))) (h "0n18rlnij9x3vggdbcl9xia0ilsf4hpi9cy80isaiswmgvik1axs")))

