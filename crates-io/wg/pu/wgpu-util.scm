(define-module (crates-io wg pu wgpu-util) #:use-module (crates-io))

(define-public crate-wgpu-util-0.0.0 (c (n "wgpu-util") (v "0.0.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.0") (d #t) (k 0)))) (h "08a3wdwjgbvqjpr2n8nfdfzd1i46wida875kxrd3bzp4ir856h5y") (y #t)))

(define-public crate-wgpu-util-0.0.0-alpha.0 (c (n "wgpu-util") (v "0.0.0-alpha.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.1") (d #t) (k 0)))) (h "1kyzr77lyx3skqv0d5wmx3rzric5kqbp6xjjgz6xnh2b02jr7zhg")))

(define-public crate-wgpu-util-0.0.0-alpha.1 (c (n "wgpu-util") (v "0.0.0-alpha.1") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.7.1") (d #t) (k 0)))) (h "14qqm1jzs7234cr85zmi681npwc8is0spf4bqdqahlg23hfr589n")))

(define-public crate-wgpu-util-0.0.0-alpha.2 (c (n "wgpu-util") (v "0.0.0-alpha.2") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.0") (d #t) (k 0)))) (h "176jnxs2rmszrgvhlmxs4gsq9lnn2rccb7n4frd4dqfajy5i642y")))

(define-public crate-wgpu-util-0.0.0-alpha.3 (c (n "wgpu-util") (v "0.0.0-alpha.3") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)))) (h "05637chwd9xzmadz5lsxkcwgs38mmc3xs37qs7xbszgpa18sc5zn")))

(define-public crate-wgpu-util-0.0.0-alpha.4 (c (n "wgpu-util") (v "0.0.0-alpha.4") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.8.1") (d #t) (k 0)))) (h "0rc9glahdyxba714gyy20bziza47g87v8vg83d9acp91ji4nh5zh")))

(define-public crate-wgpu-util-0.0.0-alpha.5 (c (n "wgpu-util") (v "0.0.0-alpha.5") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.9.0") (d #t) (k 0)))) (h "117hga36fc0vbdsdii518bsa48h6ffbqjlmwwwxibrfpdarp8kqi")))

(define-public crate-wgpu-util-0.1.0 (c (n "wgpu-util") (v "0.1.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.12.0") (d #t) (k 0)))) (h "1m8yxhqry2n5hcg8y7hjl160n9bq26max0gx8j9hgb5bvdnrv4xn")))

(define-public crate-wgpu-util-0.2.0 (c (n "wgpu-util") (v "0.2.0") (d (list (d (n "replace_with") (r "^0.1.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.12.0") (d #t) (k 0)))) (h "0x2v21c13v7sza1n3nzf3v777zn87ldqi31r55hd7fxy6ichgydm")))

