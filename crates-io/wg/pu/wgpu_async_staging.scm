(define-module (crates-io wg pu wgpu_async_staging) #:use-module (crates-io))

(define-public crate-wgpu_async_staging-0.1.0 (c (n "wgpu_async_staging") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.5.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.7") (d #t) (k 0)) (d (n "wgpu") (r "^0.11") (d #t) (k 0)))) (h "1dhj206scq5sjzmvdjijdgz0kh3g534nfw38yikfnhw91561mz6k")))

