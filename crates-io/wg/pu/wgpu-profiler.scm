(define-module (crates-io wg pu wgpu-profiler) #:use-module (crates-io))

(define-public crate-wgpu-profiler-0.2.0 (c (n "wgpu-profiler") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.7") (d #t) (k 0)))) (h "0hw9c1yxyx8kncsa6scfhdk9z6834r64b1ayzm9jww42fwk9inh4")))

(define-public crate-wgpu-profiler-0.2.1 (c (n "wgpu-profiler") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.7") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "18ssglq3k6r2s0k37ajx6a4bb03hx63frml50dk0yhipcrshwn4q")))

(define-public crate-wgpu-profiler-0.3.0 (c (n "wgpu-profiler") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.7") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "0x2pn0sclfwh63kyzswzyfyz7fzx7q9hkm0aj223aclyj72a319m")))

(define-public crate-wgpu-profiler-0.3.1 (c (n "wgpu-profiler") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.7") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "01sl768fx61q1p5g20mcl3cb2c61x2z8s4bjc7n1gyaw0d79wsxr")))

(define-public crate-wgpu-profiler-0.4.0 (c (n "wgpu-profiler") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.8") (d #t) (k 0)) (d (n "winit") (r "^0.24.0") (d #t) (k 2)))) (h "01pwdhpx146pf58b99g8wjgrkmvr37kkdiabgbq7nab3slzis4p8")))

(define-public crate-wgpu-profiler-0.5.0 (c (n "wgpu-profiler") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.9") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "09pccrd7yjlkqgyzys26fvqcm5d28l6i6kd4lwpg2nb8xmr3k0mb")))

(define-public crate-wgpu-profiler-0.6.0 (c (n "wgpu-profiler") (v "0.6.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wgpu") (r "^0.10") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "1zxn4h9kmxxxr685qv7ziq319k9vzn9fxc3ciskp2ql9rzmlih13")))

(define-public crate-wgpu-profiler-0.6.1 (c (n "wgpu-profiler") (v "0.6.1") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.10") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "07d0hqaamc0bik4k3w004rlvc94wawb4gwh4wmy2fyrxzhpzp3jr")))

(define-public crate-wgpu-profiler-0.7.0 (c (n "wgpu-profiler") (v "0.7.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.11") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 2)))) (h "00wh1xbb1sirnl9d30v035bdc5accaarh81kpdvpfwf1yw49dfsv")))

(define-public crate-wgpu-profiler-0.8.0 (c (n "wgpu-profiler") (v "0.8.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 2)))) (h "0q5f8zri1ilryfvp0q9vsaqgpcp1r8qkbw4pzjwpchswivx4x4dd")))

(define-public crate-wgpu-profiler-0.9.0 (c (n "wgpu-profiler") (v "0.9.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.13") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 2)))) (h "0sl3gzdn38l5n1xhw4y7llaqy7pb1y2dr8hygpjdyh6rfrjkg8zg")))

(define-public crate-wgpu-profiler-0.9.1 (c (n "wgpu-profiler") (v "0.9.1") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.13") (d #t) (k 0)) (d (n "winit") (r "^0.26.0") (d #t) (k 2)))) (h "0hc8gh9gvbwdkckkq03h6dmnvbkfzfjf36jz49qj2yjnb1v9q90n")))

(define-public crate-wgpu-profiler-0.10.0 (c (n "wgpu-profiler") (v "0.10.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.14.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "0s2kc9w6a4mqn656x4na2jaharnxqwsnw9av0534w1qjpkflsfbd")))

(define-public crate-wgpu-profiler-0.11.0 (c (n "wgpu-profiler") (v "0.11.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.15") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "1mchjcwzdxc6v2amifg4np7iypr4dxb6xj44bmij374ak1qcxfv8")))

(define-public crate-wgpu-profiler-0.12.0 (c (n "wgpu-profiler") (v "0.12.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "0idx5dp0xvl9i3znrmpz3pbwczfjhyzw7ngaxyqydjq0vc9fcr59")))

(define-public crate-wgpu-profiler-0.12.1 (c (n "wgpu-profiler") (v "0.12.1") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 2)))) (h "15j7crv468s8m02q464azx7r03ljlv1anq48522fipimb4v4b1dg")))

(define-public crate-wgpu-profiler-0.13.0 (c (n "wgpu-profiler") (v "0.13.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "0l111a5al42s4a79jb86ddpw4z4c6rm7hrr21y6dm94jhbpz8zfw")))

(define-public crate-wgpu-profiler-0.14.0 (c (n "wgpu-profiler") (v "0.14.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "tracy-client") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.15") (d #t) (k 2)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1xhp6ch9cfmbwjwhxc0d0d68j73dh1hds0yxshpm10367qa152b9") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy")))) (y #t)))

(define-public crate-wgpu-profiler-0.14.1 (c (n "wgpu-profiler") (v "0.14.1") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "tracy-client") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "0rskv50c1mn64y7hiybq5jhddbwp602wkaw51z7fnxmh3arjv97r") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.14.2 (c (n "wgpu-profiler") (v "0.14.2") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "tracy-client") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1r6jfyzxpjlz26bv958pfn70l3sr22hyb7nzm620g4kqrbbvsx21") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.15.0 (c (n "wgpu-profiler") (v "0.15.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 2)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracy-client") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1gdpaglc2c981gznpjfb5p391g0zhqf8x6ksln36p83i368pip6b") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.16.0 (c (n "wgpu-profiler") (v "0.16.0") (d (list (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracy-client") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.19") (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0yxm6hk14kxqfy6c4a06xwkkbfvx29r5wb3npgyy5l1pl7l5r9l6") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.16.1 (c (n "wgpu-profiler") (v "0.16.1") (d (list (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracy-client") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "web-sys") (r "=0.3.67") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wgpu") (r "^0.19") (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "1b5dmrgx2hjmd7mjz6caai4m5g5l80dq2w05pkchlpv2p6y2cl4x") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.16.2 (c (n "wgpu-profiler") (v "0.16.2") (d (list (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracy-client") (r "^0.16") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.16.1") (d #t) (k 2)) (d (n "wgpu") (r "^0.19.3") (d #t) (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "14k1w348iq65v0wjil9gc3xqykpb60v9p0mb45p7qlhbjjhrd2qr") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

(define-public crate-wgpu-profiler-0.17.0 (c (n "wgpu-profiler") (v "0.17.0") (d (list (d (n "futures-lite") (r "^2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "profiling") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracy-client") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "tracy-client") (r "^0.17.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.20.0") (d #t) (k 0)) (d (n "winit") (r "^0.30") (d #t) (k 2)))) (h "0jwjq2fw3a1sxbvk2k99vzkx18jwpcjc272rvsqb189bsdxzyrc0") (f (quote (("tracy" "tracy-client" "profiling/profile-with-tracy"))))))

