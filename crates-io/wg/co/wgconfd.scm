(define-module (crates-io wg co wgconfd) #:use-module (crates-io))

(define-public crate-wgconfd-0.1.1 (c (n "wgconfd") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "06dz6bc9rz4d9bjl5zpqmd05qz3011asil20d17yazy9qd43p6p3")))

(define-public crate-wgconfd-0.1.2 (c (n "wgconfd") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0lrg2f1jjmcdn0gxr0si5cs5kf20w3q5hinjzkyba7v19hg2ws5h") (f (quote (("default" "toml"))))))

(define-public crate-wgconfd-0.2.1 (c (n "wgconfd") (v "0.2.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1zd4h0v9ccqmywsgxj4d4y8m70hq4rd25dcy7c37zqbzsw1vyzjx") (f (quote (("default" "toml"))))))

(define-public crate-wgconfd-0.3.0 (c (n "wgconfd") (v "0.3.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "1y5l9n20fbdsw8w84c7dmxzfh9jdy1fg1icsqd9fs5jy62ysrqrm") (f (quote (("default" "toml"))))))

