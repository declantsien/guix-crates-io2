(define-module (crates-io wg ct wgctrl-sys) #:use-module (crates-io))

(define-public crate-wgctrl-sys-0.1.0 (c (n "wgctrl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.39") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xg9332h9skvnac95m4mnm0rfq58qkgfzcgxn1j94ql2r2s6paln")))

