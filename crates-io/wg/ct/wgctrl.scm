(define-module (crates-io wg ct wgctrl) #:use-module (crates-io))

(define-public crate-wgctrl-0.0.1 (c (n "wgctrl") (v "0.0.1") (h "001cagj44ca69lrags8r6mvrb8lf9gydmlnnh2gwcak7pcwwxz0r")))

(define-public crate-wgctrl-0.0.2 (c (n "wgctrl") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (f (quote ("static_secrets"))) (d #t) (k 0)))) (h "16vnmydf7y5fdizld4wmrkirkrzpnq8saal33kbqnhqrrvmx9jgn")))

(define-public crate-wgctrl-0.0.3 (c (n "wgctrl") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (f (quote ("static_secrets"))) (d #t) (k 0)))) (h "0sy24jplyvapygwx0wrqkd8b831isqkaqzhnf102p8a78k5nappw")))

