(define-module (crates-io wg ct wgctrl-rs) #:use-module (crates-io))

(define-public crate-wgctrl-rs-0.1.0 (c (n "wgctrl-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "wgctrl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0q03861m6cy5vx1afqngb5vvf93fbh844lgkdrgiqdbzpkw1byv8")))

