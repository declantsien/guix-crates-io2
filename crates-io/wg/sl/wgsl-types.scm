(define-module (crates-io wg sl wgsl-types) #:use-module (crates-io))

(define-public crate-wgsl-types-0.0.1 (c (n "wgsl-types") (v "0.0.1") (d (list (d (n "bytes-kman") (r "^0.1.9") (d #t) (k 0)))) (h "1075y63br3fz0ykpq0wrq6pip1p21m9nmz0plny2b3w9k42xws7v")))

(define-public crate-wgsl-types-0.0.2 (c (n "wgsl-types") (v "0.0.2") (d (list (d (n "bytes-kman") (r "^0.1.9") (d #t) (k 0)))) (h "05d2zyhry9ycvaapbmg72vppjimfhdjvvqkkl1x7h2fsqyj5nx13")))

(define-public crate-wgsl-types-0.0.3 (c (n "wgsl-types") (v "0.0.3") (d (list (d (n "bytes-kman") (r "^0.1.9") (d #t) (k 0)))) (h "0ffg486f885s5ckwa0xyb6jaf0gfwc2pqba3x5xr3cxgfhs7vi11")))

(define-public crate-wgsl-types-0.0.4 (c (n "wgsl-types") (v "0.0.4") (d (list (d (n "bytes-kman") (r "^0.1.9") (d #t) (k 0)))) (h "1r1kr2jpynk6mdz6kc4znjp8j2c29qv58yfnwdj76k6pkp90f35k")))

