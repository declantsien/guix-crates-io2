(define-module (crates-io wg r_ wgr_thumbnail) #:use-module (crates-io))

(define-public crate-wgr_thumbnail-0.1.0 (c (n "wgr_thumbnail") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.1.1") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "1i44hrqvx5v3v1dcd40292qmygd0zk2y83wrgz8i3sai7nf6rlrr")))

(define-public crate-wgr_thumbnail-0.1.1 (c (n "wgr_thumbnail") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ffmpeg-next") (r "^6.1.1") (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "pdfium-render") (r "^0.8.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0ka8r7rn428yrflbz9av1kjp3bx488khy8byjc1vxi5jmcb4valw")))

