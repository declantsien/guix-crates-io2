(define-module (crates-io wg bi wgbindraw-sys) #:use-module (crates-io))

(define-public crate-wgbindraw-sys-0.1.0 (c (n "wgbindraw-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "0kg41ja70hh9bf54sgwmajm9j2kbzs2fzpb0z47h7yna8pvnpa5r")))

(define-public crate-wgbindraw-sys-0.2.0 (c (n "wgbindraw-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "07fxz7vz3x8q3c8k1pry39v00lp9rr3dydqjfxfn82wzaa0rgm8j")))

(define-public crate-wgbindraw-sys-0.2.1 (c (n "wgbindraw-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "1x74ijrw2n5qr5rml6fbzqi2i21n2938h1mdqmn6mnx54m8yd1mb")))

