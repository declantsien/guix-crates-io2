(define-module (crates-io wg et wget2hugo) #:use-module (crates-io))

(define-public crate-wget2hugo-0.1.0 (c (n "wget2hugo") (v "0.1.0") (d (list (d (n "chardetng") (r "^0.1.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "html2md") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0xvkgkmp4zbkmv4jmbvzig0cgvj6cjq6y7rnx231d34ykg9ljcl8")))

(define-public crate-wget2hugo-0.1.1 (c (n "wget2hugo") (v "0.1.1") (d (list (d (n "chardetng") (r "^0.1.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "html2md") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0pmc6nvsw0vgc4yz5vzxcjsy6pwi4dzznsap21mgd58nkz42pjj0")))

(define-public crate-wget2hugo-0.1.2 (c (n "wget2hugo") (v "0.1.2") (d (list (d (n "chardetng") (r "^0.1.14") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "html2md") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kf5m2fq8gjvxfrmpw5mf2m11ndkpax4pj245d55fy0a23km9amv")))

