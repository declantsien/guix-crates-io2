(define-module (crates-io m4 ri m4ri-sys) #:use-module (crates-io))

(define-public crate-m4ri-sys-0.3.0 (c (n "m4ri-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hzw0c6yf27h9j69pf7yb2px2pip62py3wq9mg1dlpnfgs8mmlzw")))

(define-public crate-m4ri-sys-0.3.1 (c (n "m4ri-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pm2vgcnflslk94ijbfq2lzkghm53kbv5ic50qq7iay0yrhhxydy")))

(define-public crate-m4ri-sys-0.3.2 (c (n "m4ri-sys") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r70q50ci73bp9p1bzz0nl10zw9xz5awvpp1rnlfsa95q8b0m8lv")))

(define-public crate-m4ri-sys-0.3.3 (c (n "m4ri-sys") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1aipa7yxfl4i8s6agjcy4x49b3ryvvlg1jl6dj93i2m6s1bbk20b")))

