(define-module (crates-io m4 ri m4ri-rust) #:use-module (crates-io))

(define-public crate-m4ri-rust-0.0.1 (c (n "m4ri-rust") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17hj4bbqks0w1rc51166p9pr1qf3w618jrp23qh86knvrf9hp6h5")))

(define-public crate-m4ri-rust-0.0.2 (c (n "m4ri-rust") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hy47srd1xjj6hcac02xlygvy04yazbzrbdfjqvmv0ijip4kmivb")))

(define-public crate-m4ri-rust-0.0.3 (c (n "m4ri-rust") (v "0.0.3") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17qv1gcldbia285kvn2gpi57zlzafm58lycsxpfd3gj4gppkql8k")))

(define-public crate-m4ri-rust-0.0.4 (c (n "m4ri-rust") (v "0.0.4") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q2190clghl40hq1lkdmbcg3wwk1cjhll6igg0502gvvz8g3xnlr")))

(define-public crate-m4ri-rust-0.0.5 (c (n "m4ri-rust") (v "0.0.5") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "045llcfri71g0gvr038hzrcv51qccrj7908y88zahwvq0yf2b96i")))

(define-public crate-m4ri-rust-0.0.6 (c (n "m4ri-rust") (v "0.0.6") (d (list (d (n "bit-vec") (r "^0.4.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hxz962vy6nhdq3rqmhfz7qcz6vmhpa2jf1wv0w19ch9h8hhcjf0")))

(define-public crate-m4ri-rust-0.0.7 (c (n "m4ri-rust") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vob") (r "^1.1.0") (d #t) (k 0)))) (h "0xny2n107gc8lfpwysn8kcyjs8c35ig91by8vxx7rr20f2dq2rm5")))

(define-public crate-m4ri-rust-0.0.8 (c (n "m4ri-rust") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "vob") (r "^1.1.0") (d #t) (k 0)))) (h "0a0w0r0hgvq4kf9fw8hhnxknygmkil3hb7l92yrqg65jw4gpsc5a")))

(define-public crate-m4ri-rust-0.0.9 (c (n "m4ri-rust") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "vob") (r "^1.1.0") (d #t) (k 0)))) (h "1gbrd1576bjbmnli78mcs3yfdv69icwyi5lzvq9jc2mr6sxng5mp")))

(define-public crate-m4ri-rust-0.0.10 (c (n "m4ri-rust") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "vob") (r "^1.3") (f (quote ("unsafe_internals"))) (d #t) (k 0)))) (h "1mq8lqx3xafxl0g96zwyj4l65r75jmiazwqngp518a0zm6pz90pk") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.1.0 (c (n "m4ri-rust") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "vob") (r "^1.3") (f (quote ("unsafe_internals"))) (d #t) (k 0)))) (h "00n3jrcz4sjh1ya51k8n84s9brqfm9gy379rbmwv9kad235v97n0") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.2.0 (c (n "m4ri-rust") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^1.3") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "0ldl621k07bqchzq4lnkns339ihd2416yrmagh6n7wals93l9fja") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.2.1 (c (n "m4ri-rust") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "vob") (r "^2") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "1fvmfpih9nfn7nmpgfa2d38lihj75rgbfiipsz2jpl1xnparglap") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.2.2 (c (n "m4ri-rust") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "vob") (r "^2") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "0lrmwg1930yw0n9mmzn77045ml3hzvr64ia6nbldlya1x8sa1f16") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.3.0 (c (n "m4ri-rust") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "m4ri-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "vob") (r "^2") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "1wb1a013ikp21w1b4b52hgqr38l23r58k3707fn8m2pl1c35ac6l") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.3.2 (c (n "m4ri-rust") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "m4ri-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "vob") (r "^2") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "1yjk8npf7dzpsd8iraix00c1xzyrw7nmvkr664n4im1aib3zv9nd") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

(define-public crate-m4ri-rust-0.4.0 (c (n "m4ri-rust") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "m4ri-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "vob") (r "^2.0.6") (f (quote ("unsafe_internals" "serde"))) (d #t) (k 0)))) (h "0vf6cmyvj6brpy3xbx8k048mqzniswnmz8dy5hs0dmghhcr7aar9") (f (quote (("system_alloc") ("strassen_mul") ("naive_mul") ("m4rm_mul"))))))

