(define-module (crates-io wi tn witnet-vanity) #:use-module (crates-io))

(define-public crate-witnet-vanity-0.1.0 (c (n "witnet-vanity") (v "0.1.0") (d (list (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.2") (f (quote ("rand-std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "0k31bkfjcljn1xzxqyb0j26gjr5qmzj04hpblxkpq8dx7k5nd0ka")))

