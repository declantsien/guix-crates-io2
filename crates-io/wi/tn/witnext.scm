(define-module (crates-io wi tn witnext) #:use-module (crates-io))

(define-public crate-witnext-0.10.0-beta1 (c (n "witnext") (v "0.10.0-beta1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^33.0.0") (k 0)))) (h "17ckllld2w2x7v05xdfhc5lbw7glfrgmps8yzgbbhrc6hc23hqhr")))

(define-public crate-witnext-0.10.0-beta2 (c (n "witnext") (v "0.10.0-beta2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^32.0.0") (k 0)))) (h "1yv71hhmz81fjgrzrj9nwxy768svf313x38ss5hcaf9wgn4720ia")))

(define-public crate-witnext-0.10.0-beta3 (c (n "witnext") (v "0.10.0-beta3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^36.0.0") (k 0)))) (h "17giqxyg7cyx4gdkxbfm8szkibqh7hjd3nlrhfjwklyvhdjrjwnd")))

