(define-module (crates-io wi nd window_events) #:use-module (crates-io))

(define-public crate-window_events-0.1.0 (c (n "window_events") (v "0.1.0") (h "12hw03sxy8wz4qmmpa2cisf00bv0xydbz2kh3h4i68yd8liirkny")))

(define-public crate-window_events-0.1.1 (c (n "window_events") (v "0.1.1") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)))) (h "16g3kykzpmjisiljiy22kakvh0zdbjvhi392ns11c4vijjdcrbrr")))

(define-public crate-window_events-0.1.2 (c (n "window_events") (v "0.1.2") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)))) (h "0kdcb61wszppyd0h6mlf7bymw9y8v6qrcrma8axjcld0nsx6r51q")))

(define-public crate-window_events-0.1.3 (c (n "window_events") (v "0.1.3") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)))) (h "0rlqrq7nwfwqsfml7gz4awyf8xgafa3kqzripam8w2ymf5pmb7ah")))

(define-public crate-window_events-0.1.4 (c (n "window_events") (v "0.1.4") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1gpywmwzc43i7pi1shz5g3vp2yzikafhidgvaif0klpa7bm5qdq8")))

(define-public crate-window_events-0.1.5 (c (n "window_events") (v "0.1.5") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1xaqmpgz6cx9a7nyl7ifdn66vag5a0pvw0bhay2b73ny6d4hbnjc")))

(define-public crate-window_events-0.1.6 (c (n "window_events") (v "0.1.6") (d (list (d (n "events_loop") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "01hvwhhhrfqy7nvnj9sdlszxln8g38cqck6adr0gyy3mnfzb74n2")))

