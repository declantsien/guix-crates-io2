(define-module (crates-io wi nd windows-version) #:use-module (crates-io))

(define-public crate-windows-version-0.0.0 (c (n "windows-version") (v "0.0.0") (h "11hswgq97q2frrwr4j4nlb0gwfzjx1y25jwzpslvycwkh5bd0gcy")))

(define-public crate-windows-version-0.1.0 (c (n "windows-version") (v "0.1.0") (d (list (d (n "windows-bindgen") (r "^0.52.0") (d #t) (k 2)) (d (n "windows-targets") (r "^0.52.0") (d #t) (k 0)))) (h "1d5rp1bx1651r0gxgmlr8rlg84h3kp1kjmzz9b9hl24fk1601akm") (r "1.56")))

(define-public crate-windows-version-0.1.1 (c (n "windows-version") (v "0.1.1") (d (list (d (n "windows-bindgen") (r "^0.56.0") (d #t) (k 2)) (d (n "windows-targets") (r "^0.52.5") (d #t) (k 0)))) (h "05a5hia3d2vxd16vj2hxiyicxaqdjcm9sgpip4pzza4vgi2sm639") (r "1.60")))

