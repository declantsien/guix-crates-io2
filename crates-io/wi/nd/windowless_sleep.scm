(define-module (crates-io wi nd windowless_sleep) #:use-module (crates-io))

(define-public crate-windowless_sleep-0.1.1 (c (n "windowless_sleep") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (k 0)))) (h "04nm97kk1zjfndw7d4swlfqz52w61hb836i805c0lviykglh6g9s")))

