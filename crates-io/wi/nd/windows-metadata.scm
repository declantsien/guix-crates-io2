(define-module (crates-io wi nd windows-metadata) #:use-module (crates-io))

(define-public crate-windows-metadata-0.0.0 (c (n "windows-metadata") (v "0.0.0") (h "06xib973qmccw3ikmycivdnm2644jm1il0j8vby5pbvmj1j0xbwk")))

(define-public crate-windows-metadata-0.31.0 (c (n "windows-metadata") (v "0.31.0") (h "0rhh5z8ggciffidwz5spqa20skni42jh8g7kfncz75i462kg4qf2")))

(define-public crate-windows-metadata-0.32.0 (c (n "windows-metadata") (v "0.32.0") (h "07yfzp2vfym83prwdql5hy6x6bpn3hnjbhb52wkwbw07xj9q6kpf")))

(define-public crate-windows-metadata-0.33.0 (c (n "windows-metadata") (v "0.33.0") (h "13d3xcjl5zmk5k8ba4m7csbf39l9nla4ahdy80rg1a838cvgrcvx")))

(define-public crate-windows-metadata-0.34.0 (c (n "windows-metadata") (v "0.34.0") (h "1gvyk14j6arjkyf3rc9dpjjyjd1w24jbgr8ryf6vl54m8n0bkg4h")))

(define-public crate-windows-metadata-0.35.0 (c (n "windows-metadata") (v "0.35.0") (h "1fk5d6sa56gn6chqf0k6hnqc2fhw2f29l4h45k3nh9020v6dajc4")))

(define-public crate-windows-metadata-0.36.0 (c (n "windows-metadata") (v "0.36.0") (h "1vqsmyr56lwh7pzb9f78lx7akv6d2vfzy1zdcwswimxhimcf008d")))

(define-public crate-windows-metadata-0.36.1 (c (n "windows-metadata") (v "0.36.1") (h "1y5v1w9szvwsmdrbsha4wy4zkdn2klp9rhc6h5bmvjbhsaa4zs2m")))

(define-public crate-windows-metadata-0.37.0 (c (n "windows-metadata") (v "0.37.0") (h "0qq4pd87g94r1s5bznc5jvy4iw065lkgzd8sqily6r361awz4csg")))

(define-public crate-windows-metadata-0.38.0 (c (n "windows-metadata") (v "0.38.0") (d (list (d (n "windows-sys") (r "^0.36.1") (f (quote ("Win32_System_SystemServices" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "1v346q4yx95krh4z3br7xqhgsrw6nn2ypd2aayqvgdgk6asj2zin")))

(define-public crate-windows-metadata-0.39.0 (c (n "windows-metadata") (v "0.39.0") (h "0y22is2bkkd5b7ypribn3jm1zgrmnvhk8kyi83icc1qz4dsy5rcy")))

(define-public crate-windows-metadata-0.40.0 (c (n "windows-metadata") (v "0.40.0") (h "0i5b2gmrs31qhxdnq7jbczfhf0p82v7hq08apkzrs6kmyf8fcm3x")))

(define-public crate-windows-metadata-0.41.0 (c (n "windows-metadata") (v "0.41.0") (h "1cp6y2k1yjkndnhswnsifb1i04chnc1i76s36avap5l2d02f3hni")))

(define-public crate-windows-metadata-0.42.0 (c (n "windows-metadata") (v "0.42.0") (h "0dw9ldd6pz9affmzqpl1d213bxi41pdm47rinh9k75smlj7si2fq")))

(define-public crate-windows-metadata-0.43.0 (c (n "windows-metadata") (v "0.43.0") (h "063axswxv564dxakr5lgsjwrcbq45aidy1nh7xcshaw5m2j5126p")))

(define-public crate-windows-metadata-0.44.0 (c (n "windows-metadata") (v "0.44.0") (h "1dp2jsyp1vcgd1ak26lginnn55iqjnyv1iykv4d2rqsc7wg92y7f")))

(define-public crate-windows-metadata-0.46.0 (c (n "windows-metadata") (v "0.46.0") (h "09wva6jw8rfzyg83m0alvg82qyq509fa3dp603w4q4ckarj262ha")))

(define-public crate-windows-metadata-0.47.0 (c (n "windows-metadata") (v "0.47.0") (h "1ja9jbf4wwhgf0w78y4xzh4574r43ymq287gb3yhrwhpfk67al84")))

(define-public crate-windows-metadata-0.48.0 (c (n "windows-metadata") (v "0.48.0") (h "0ba3647h1azn2nfdyhypbhai5mxdm3wzznxdnqmkgk72y3jy0bj2")))

(define-public crate-windows-metadata-0.49.0 (c (n "windows-metadata") (v "0.49.0") (h "119kzn67xw85d8dhzv45x0g62fss4w0ncasjfsiydw9blfaclnrg")))

(define-public crate-windows-metadata-0.51.1 (c (n "windows-metadata") (v "0.51.1") (h "03h0c6qs1yyl0z69p4k1hdq636j868qdxnri1dy47nprjvckacbm")))

(define-public crate-windows-metadata-0.52.0 (c (n "windows-metadata") (v "0.52.0") (h "1vz49s2mm74fmjabh3kxxhzbz16ys41b78jgi6xwssp2069db3r1") (r "1.64")))

(define-public crate-windows-metadata-0.53.0 (c (n "windows-metadata") (v "0.53.0") (h "1zkb4xbqn3nmkxc6xv32kf6jhy7sbj22ymz8wn5kzbvslj47c9q5") (r "1.70")))

(define-public crate-windows-metadata-0.54.0 (c (n "windows-metadata") (v "0.54.0") (h "1hh4bpima19p18kr5a2ss46hgmgafjkqzyfzhm0dazvx6sw70hz4") (r "1.70")))

(define-public crate-windows-metadata-0.55.0 (c (n "windows-metadata") (v "0.55.0") (h "0z8sjsaiv4hr944rgzgyk1hgrbjv496hs11mgb2iyahpa18660mn") (r "1.70")))

(define-public crate-windows-metadata-0.56.0 (c (n "windows-metadata") (v "0.56.0") (h "0d1vizbp6b1wjh3qnjrh120w1iwqal3lfj52wdac847zgy1gg4rr") (r "1.70")))

