(define-module (crates-io wi nd windows_dpi) #:use-module (crates-io))

(define-public crate-windows_dpi-0.1.0 (c (n "windows_dpi") (v "0.1.0") (d (list (d (n "gdi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1mip1hksl6z9w0zwjmhg44v3akra3av2xcic4fkxxmilq1csl6rf")))

(define-public crate-windows_dpi-0.1.1 (c (n "windows_dpi") (v "0.1.1") (d (list (d (n "gdi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1nh633dfr4g4ns7iirmgzfj0zsfb29g01qsc4mkgm59la9329091")))

(define-public crate-windows_dpi-0.1.2 (c (n "windows_dpi") (v "0.1.2") (d (list (d (n "gdi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "16byrg0d5k7jrwy33f09ypkd0rdd5q3y3s6s4cksbik72h7hizhw") (y #t)))

(define-public crate-windows_dpi-0.1.3 (c (n "windows_dpi") (v "0.1.3") (d (list (d (n "gdi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "19xc8vpi7pwx2f3czwmyab9gjjrk9qlm56jzj2v18jr2g5j00phs") (y #t)))

(define-public crate-windows_dpi-0.1.4 (c (n "windows_dpi") (v "0.1.4") (d (list (d (n "gdi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0cc88h7lpcbc49qhcff9xkwbyn5yvfsqp3cxi1lfninvz6g2hpnm")))

(define-public crate-windows_dpi-0.2.0 (c (n "windows_dpi") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("minwindef" "windef" "winerror" "wingdi" "winuser"))) (d #t) (k 0)))) (h "1707mfakagsgh2hkp9j60kfa1f75n65iilj8f6j3q8djwk7xvyzj")))

(define-public crate-windows_dpi-0.3.0 (c (n "windows_dpi") (v "0.3.0") (d (list (d (n "cocoa") (r "^0.14") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "objc") (r "^0.2.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("minwindef" "windef" "winerror" "wingdi" "winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1v4wlz8z5w8p7ncd9isrn19k71rxipg1bj3csi3rq5fvrqsakq15")))

