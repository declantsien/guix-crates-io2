(define-module (crates-io wi nd windows-devices-sys) #:use-module (crates-io))

(define-public crate-windows-devices-sys-0.0.0 (c (n "windows-devices-sys") (v "0.0.0") (h "1k44bkig2w7afqgzvpsm02xf2a1vdmwj39psv4nqqdw4qyxbmm7v") (r "1.46")))

(define-public crate-windows-devices-sys-0.0.1 (c (n "windows-devices-sys") (v "0.0.1") (h "0sj4a9gys3ai8israapa8vxihn66f41rqp5b0dg7nblia2wz9j8r") (r "1.46")))

(define-public crate-windows-devices-sys-0.0.2 (c (n "windows-devices-sys") (v "0.0.2") (h "167422xs33bx2hm477p7cf9c5zx458fn2zvmsvk6fq2rkywnfcjw") (r "1.46")))

(define-public crate-windows-devices-sys-0.1.0 (c (n "windows-devices-sys") (v "0.1.0") (h "1vm5xz6qvjacfp7mirmpp8hq74v0cj2n1qlg2wi8d01kx9ryrvdw")))

