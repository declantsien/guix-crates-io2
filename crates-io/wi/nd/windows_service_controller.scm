(define-module (crates-io wi nd windows_service_controller) #:use-module (crates-io))

(define-public crate-windows_service_controller-0.1.1 (c (n "windows_service_controller") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_Services" "Win32_Security"))) (d #t) (k 0)))) (h "1f7cqdgk050qgqgahkqpq596w3by2lj3j3jvgcmyxrc7l7zgskhb") (y #t)))

(define-public crate-windows_service_controller-0.1.2 (c (n "windows_service_controller") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_Services" "Win32_Security"))) (d #t) (k 0)))) (h "1ydgh5zjw46nw00wcb7y447s1q3yr5lav45la7m3fzpanjnmrx43")))

(define-public crate-windows_service_controller-0.1.3 (c (n "windows_service_controller") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lers_windows_macro") (r "^0") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_System_Services" "Win32_Security"))) (d #t) (k 0)))) (h "08agdmsqpy0cjkaywqafc13krvhbah5l0snnkcs9q36fhbmwwc1d")))

