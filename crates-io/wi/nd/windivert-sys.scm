(define-module (crates-io wi nd windivert-sys) #:use-module (crates-io))

(define-public crate-windivert-sys-0.1.0 (c (n "windivert-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "09kiaslrr4fkbm057sjih08wzm290nakn2jmwhpam46p64q595jz") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.2.0 (c (n "windivert-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "1npcir1banzsg973jhv7ina20fgh797sd6xp39nq1b2y7v6fkpm4") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.2.1 (c (n "windivert-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "0vkh24wic2x18dy89ijz5hj0pvmgyja5x67w2i3b0k196kkkiayb") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.2.2 (c (n "windivert-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "0p12g232w2fwmag8cyb289ajcr8nxvdwyqmq585vdl18swgxm70m") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.3.0 (c (n "windivert-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "1izia03sdia9dpj7jndbx0gc5nk0v99i2704v8s9695wcwcs710y") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.4.0 (c (n "windivert-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "0wbs07r8xfj5jgqvdmk3v14658h02vip625rndhdahr73ddc3x1a") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.5.0 (c (n "windivert-sys") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "132bqmgrskrzbmm8170c3pzcw1i01k8n1afgghm9gs6n3glmgpm9") (l "WinDivert")))

(define-public crate-windivert-sys-0.5.1 (c (n "windivert-sys") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "0s74spjrxa0vndfll0kv351w8qr6nwhx84wbr7ghx3abmvrss85f") (l "WinDivert")))

(define-public crate-windivert-sys-0.5.2 (c (n "windivert-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwinbase" "minwindef" "ntdef"))) (d #t) (k 0)))) (h "108i9z6cvphdpn34qlk22k2x485gm1skphc4qazkk8kkbga6qpan") (l "WinDivert")))

(define-public crate-windivert-sys-0.6.0 (c (n "windivert-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1bpljs04q5y5clv76cd4v1yg5h7b57c2mlm11slf8ingxjq258pi") (l "WinDivert")))

(define-public crate-windivert-sys-0.6.1 (c (n "windivert-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)))) (h "091m7kmzi6yvd7a1b65issjj7n1n8cqwzdn2w1zqc1p5jkzjvj5y") (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.7.0 (c (n "windivert-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "windows") (r "^0.29.0") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0md0iqhg2b0ndxq0cmzpdv25nq9rqaq8rplkn1glvli659w4vv18") (l "WinDivert")))

(define-public crate-windivert-sys-0.8.0 (c (n "windivert-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "windows") (r "^0.30.0") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0zfpkhpnsrm3iwnr7zbdxccvvrhwi4wah4vd4b98ii5lp5q9la92") (l "WinDivert")))

(define-public crate-windivert-sys-0.9.0 (c (n "windivert-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "1881lq2n3ylbh1sv68d2y2v7p6qskk7g4cv1gl88diqg8wwdm75c") (f (quote (("vendored") ("default")))) (y #t) (l "WinDivert")))

(define-public crate-windivert-sys-0.9.1 (c (n "windivert-sys") (v "0.9.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0kb4rijpg6w22qaf7qbanh3mahx4q7dg1f57dd5329w1xw3xzfy6") (f (quote (("vendored") ("default")))) (y #t) (l "WinDivert") (r "1.64")))

(define-public crate-windivert-sys-0.9.2 (c (n "windivert-sys") (v "0.9.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.43") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0szn25frci5si9k2js9fyid3nlv7dknmghfpx9fj6vxq9czbm2dl") (f (quote (("vendored") ("default")))) (l "WinDivert") (r "1.64")))

(define-public crate-windivert-sys-0.9.3 (c (n "windivert-sys") (v "0.9.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.43") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0ri3v6bif874jjl8qf1a7rb7na7f02x3k9jcz4nz0xwxyapa17kp") (f (quote (("vendored") ("default")))) (l "WinDivert") (r "1.64")))

(define-public crate-windivert-sys-0.10.0 (c (n "windivert-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_IO"))) (d #t) (k 0)))) (h "0m3fp8l6v6wvw78cnvs88sahii0dw6mklnrrcj58libjjapw8aw3") (f (quote (("vendored") ("static" "vendored") ("default")))) (l "WinDivert") (r "1.64")))

