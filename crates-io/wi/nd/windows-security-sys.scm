(define-module (crates-io wi nd windows-security-sys) #:use-module (crates-io))

(define-public crate-windows-security-sys-0.0.2 (c (n "windows-security-sys") (v "0.0.2") (h "0ay1lfns0dn8mq41q8a90d1wy8p7ddird2vcgzgqky7a0c8gxzny") (r "1.46")))

(define-public crate-windows-security-sys-0.23.0 (c (n "windows-security-sys") (v "0.23.0") (h "149c2yd9mfp6s3fsmym5klwppwc7pmsybmq03zzpspgwal5a4hm6")))

