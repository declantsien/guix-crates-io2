(define-module (crates-io wi nd windows-perception-sys) #:use-module (crates-io))

(define-public crate-windows-perception-sys-0.0.2 (c (n "windows-perception-sys") (v "0.0.2") (h "0xqri4bmkjwxpxsj12fhxiwcvrxg6284vwzrlgblkdr4gn2sqnsf") (r "1.46")))

(define-public crate-windows-perception-sys-0.23.0 (c (n "windows-perception-sys") (v "0.23.0") (h "0hnbc0kfdp5jh56wzyy2796bxh56fljr05lb96rxg7lsnphqbs1c")))

