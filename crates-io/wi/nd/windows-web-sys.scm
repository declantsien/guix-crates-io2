(define-module (crates-io wi nd windows-web-sys) #:use-module (crates-io))

(define-public crate-windows-web-sys-0.0.2 (c (n "windows-web-sys") (v "0.0.2") (h "0x3zyxn64wvhj930ryjqyrmx5g1h2ngrvjbzmqjphkwl94iqnc9v") (r "1.46")))

(define-public crate-windows-web-sys-0.23.0 (c (n "windows-web-sys") (v "0.23.0") (h "0w6v9qziwp1bx71n9xdy473x14xl7g23jrhmjk87j85bgszzmrja")))

