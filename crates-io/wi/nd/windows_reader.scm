(define-module (crates-io wi nd windows_reader) #:use-module (crates-io))

(define-public crate-windows_reader-0.19.0 (c (n "windows_reader") (v "0.19.0") (h "1axcm8wl8wjp1gw19qvwll6rgch6c8z74ijwii1q705h7zw5r39c")))

(define-public crate-windows_reader-0.20.0 (c (n "windows_reader") (v "0.20.0") (h "0l55yhsn9wcxapfmazar4bf07xavl6kp12f1m4axlvf24srl0l67")))

(define-public crate-windows_reader-0.20.1 (c (n "windows_reader") (v "0.20.1") (h "1fvbhzjy3s5411xwfpvq8fgpfn87lbfawy9sv46g5y7jb3bmpaix")))

(define-public crate-windows_reader-0.21.0 (c (n "windows_reader") (v "0.21.0") (h "113chvvwqbyarn296zd5nczj04dldnhyf5925is5zcvc4d7brqq2")))

(define-public crate-windows_reader-0.21.1 (c (n "windows_reader") (v "0.21.1") (h "0ywxpmkqjs5msw07bvph5421vlj22in8q3fsnmyylrh7nkl56yr3")))

(define-public crate-windows_reader-0.22.0 (c (n "windows_reader") (v "0.22.0") (h "0my97w3nm4x2szf2a5fcipnp1fhl9zmiqxr13fypkfb00pjmkmwi")))

(define-public crate-windows_reader-0.22.1 (c (n "windows_reader") (v "0.22.1") (h "19yr4k27fm04h464mnxx6l78grrhq14422qid3sfklx915dm4qwv")))

(define-public crate-windows_reader-0.23.0 (c (n "windows_reader") (v "0.23.0") (h "1dnb0ziqj1s3fjjgr08b3l2gq01lhh4h4d7prrlm1smcq7bais3z")))

(define-public crate-windows_reader-0.24.0 (c (n "windows_reader") (v "0.24.0") (h "12dz2wipijzyv5cbdk8dk6bv5ngs70ij9ww1sfv2z2wrxm67ps4y")))

(define-public crate-windows_reader-0.25.0 (c (n "windows_reader") (v "0.25.0") (h "1fdngcs560z8hf13na4dds50hp6idf0sylq0l8p8agchfzxwjcl1")))

(define-public crate-windows_reader-0.26.0 (c (n "windows_reader") (v "0.26.0") (h "068x30ja4hhn8ghi82j8rhnnp4s8h5h94g8fdgzliagkyblzsijn")))

(define-public crate-windows_reader-0.27.0 (c (n "windows_reader") (v "0.27.0") (h "099qllfzx2qq7rvbvjklzb50rir91jax9g054njclmhgyh96zhw2")))

(define-public crate-windows_reader-0.28.0 (c (n "windows_reader") (v "0.28.0") (h "06kdasip3jnlxl81afq070k0vgi51gzsybj1lh5j1axmm5amvr00")))

(define-public crate-windows_reader-0.29.0 (c (n "windows_reader") (v "0.29.0") (h "11sp959c5qjc9yivvqrlns5j4269cb91xzkg8g2x7fjp8k038yyq")))

(define-public crate-windows_reader-0.30.0 (c (n "windows_reader") (v "0.30.0") (h "0widi9913b1yvlbs0cd0cynhwamv5wkm5j2skicdlg4rvbq6skp4")))

(define-public crate-windows_reader-0.31.0 (c (n "windows_reader") (v "0.31.0") (h "1793nvx6gjm6rsrswd1mcbghy3362350mmygfanszdjzjpsr8ws6")))

