(define-module (crates-io wi nd windows-service-detector) #:use-module (crates-io))

(define-public crate-windows-service-detector-0.1.0 (c (n "windows-service-detector") (v "0.1.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_System_Kernel" "Win32_System_WindowsProgramming"))) (d #t) (k 0)) (d (n "windows-service") (r "^0.6.0") (d #t) (k 2)))) (h "0yh6wjbfxzjynvw9z9cyk0idy3hs2vz5yhrlj6pyjyi14wnc0qja")))

