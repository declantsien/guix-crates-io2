(define-module (crates-io wi nd windows-sys-security) #:use-module (crates-io))

(define-public crate-windows-sys-security-0.22.5 (c (n "windows-sys-security") (v "0.22.5") (h "1z3bsvk393jv8cmg1r6pahqvqb6agxhvcgijisdrh6vhrnc608pi") (r "1.46")))

(define-public crate-windows-sys-security-0.22.6 (c (n "windows-sys-security") (v "0.22.6") (h "06ac5kag9vlaq6iqnb67zd7v9z3h9cfp3dbhhkbr5jlffpfwk8xz") (r "1.46")))

(define-public crate-windows-sys-security-0.23.0 (c (n "windows-sys-security") (v "0.23.0") (h "1y3kwprxi3mgnfwvywj43cd2z4ibfmhhiidg2yg69zg0is2rim7a")))

