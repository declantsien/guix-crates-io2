(define-module (crates-io wi nd windows-sys-web) #:use-module (crates-io))

(define-public crate-windows-sys-web-0.22.5 (c (n "windows-sys-web") (v "0.22.5") (h "0qdnzg146qsbvwhk4i3iza0p67rlkpd16brnzi9p9f2zgjcq5mvg") (r "1.46")))

(define-public crate-windows-sys-web-0.22.6 (c (n "windows-sys-web") (v "0.22.6") (h "1a4rq7rk2b633ajh5lxix3n3f317lv5adr9p8jspf6ivad4jw5y1") (r "1.46")))

(define-public crate-windows-sys-web-0.23.0 (c (n "windows-sys-web") (v "0.23.0") (h "05dkrymmfj59y2fl4fnypwd1hxszmmgnpf3i23rv1js10phxysbi")))

