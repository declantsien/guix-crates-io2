(define-module (crates-io wi nd windows_audio) #:use-module (crates-io))

(define-public crate-windows_audio-0.1.0 (c (n "windows_audio") (v "0.1.0") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "12phq9rlf66y55q8gs689wls77idc93azwrhshazf8wlmpkxyh8v")))

(define-public crate-windows_audio-0.1.1 (c (n "windows_audio") (v "0.1.1") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "1dy92l7kim2sr2l018mp9v7affsim4qy7krhd8mids26bplj1yp3")))

(define-public crate-windows_audio-0.1.2 (c (n "windows_audio") (v "0.1.2") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "17zn3avszir5xgr1iqdsi7lbiw5djj4s52prq20ciflwvp5iklhw")))

(define-public crate-windows_audio-0.1.3 (c (n "windows_audio") (v "0.1.3") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "1w1rnn5d9jj2fva1fldq1sq4i05whv4bxvhip62q62y1lv2n14v5")))

(define-public crate-windows_audio-0.1.4 (c (n "windows_audio") (v "0.1.4") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "1n2j30crpi4a5g62kh6lh5sd8r9grij303gy3sqwxmf8xpws4yf4")))

(define-public crate-windows_audio-0.1.5 (c (n "windows_audio") (v "0.1.5") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "1faclf2vic8yvib9vb065l8c3lshjg1hzmxhd0wlgacm856z9zkj")))

(define-public crate-windows_audio-0.1.6 (c (n "windows_audio") (v "0.1.6") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "03km5bispyw22dxcxbjkk21mp01zb4064viv6pmxhkiihcaz86lz")))

(define-public crate-windows_audio-0.1.7 (c (n "windows_audio") (v "0.1.7") (d (list (d (n "rodio") (r "^0.16.0") (d #t) (k 0)))) (h "1vh2ikbqmp0vf3gqyyq9j4jzqcfrzvv9p6ynszxqgnm6m8jy42gl")))

