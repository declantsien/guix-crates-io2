(define-module (crates-io wi nd windows-win) #:use-module (crates-io))

(define-public crate-windows-win-0.1.0 (c (n "windows-win") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0lx017rcjpcccjp7617yh4dc3j7bffv6cxpbfhj5fi5q9dwfhgds")))

(define-public crate-windows-win-0.2.0 (c (n "windows-win") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0ixxm3gwxz49g6pz6w5l8hrs5qa08ns270nnk95s7ilawmni71v6")))

(define-public crate-windows-win-0.2.1 (c (n "windows-win") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0s45nnamk56jz025l6fn0ndgp9ijia0mfzyrz8iwi1y9yj43bs1z")))

(define-public crate-windows-win-0.3.0 (c (n "windows-win") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0dd4gyp58m3fb6d0wzclihz95n8p9bzq55j10l0h12h1ghbcb0ky")))

(define-public crate-windows-win-0.3.1 (c (n "windows-win") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "1haszihsl6k3gn9l29crd5v1ny9bkavnxsrcbhmyaq4b5b5vpl6f")))

(define-public crate-windows-win-0.4.0 (c (n "windows-win") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "1xvmvmc41lvkyjqcyn9zdp55chqml02xsp68xrkqhp7bb7xc9a3h")))

(define-public crate-windows-win-0.5.0 (c (n "windows-win") (v "0.5.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "12295xhxdfxd9b5j37r2szcz71ymbxhg2xlmqdha3mdxj2bp3q91")))

(define-public crate-windows-win-0.6.0 (c (n "windows-win") (v "0.6.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0qhixkjahq4dsyzdbq6k7ky2yfn9bcmsn15d5ly7wsiakp49hn53")))

(define-public crate-windows-win-0.7.0 (c (n "windows-win") (v "0.7.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "0d41lzrbap9kmwx884hpqza10pvahx1qzkn6irdgajinar4k249c")))

(define-public crate-windows-win-0.8.0 (c (n "windows-win") (v "0.8.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (k 0)))) (h "012wv85lyvg2mb403q27myzxiiag0qz21wbs3yvlc9vgl3jplqm8")))

(define-public crate-windows-win-0.8.1 (c (n "windows-win") (v "0.8.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-error") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "06cp49c06l1r3cnzxn39jxi4sypjravrc0v3c8bi7aar5ld5x7g6")))

(define-public crate-windows-win-1.0.0-pre.1 (c (n "windows-win") (v "1.0.0-pre.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1msmrgynak1mhin9cw5vnv9j2nn5b345xzpxyqdj85jqwg5ikybd")))

(define-public crate-windows-win-1.0.0-pre.2 (c (n "windows-win") (v "1.0.0-pre.2") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "159kd31ym8mik92f4zx56m8ax5l1s4qpf0hyi5vkp2ajn525ynm8")))

(define-public crate-windows-win-1.0.0 (c (n "windows-win") (v "1.0.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1p5caf2h3ikv9qxk85a6smh3wq924jacsrndr2qjnncy3lkf99i7")))

(define-public crate-windows-win-1.1.0 (c (n "windows-win") (v "1.1.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "104m37i35fpc1gwldhqqb4bpk1g0fl40a7bn129gs2klw3f9j7mk")))

(define-public crate-windows-win-2.0.0 (c (n "windows-win") (v "2.0.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "12ih19xqilhfmblihwx4iyw1n3amdh6vq9jhmcmww2pgaf6fll86")))

(define-public crate-windows-win-2.0.1 (c (n "windows-win") (v "2.0.1") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "10acy8p1bddgd1h5h762018dmrbdzcjpbkx7csh4drpasl7q0mbf")))

(define-public crate-windows-win-2.1.0 (c (n "windows-win") (v "2.1.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi"))) (t "cfg(windows)") (k 0)))) (h "086vmw0l102nhsg8qxiy69kxzkqis5bdjgagx7gc85q7s2jb4rm2")))

(define-public crate-windows-win-2.2.0 (c (n "windows-win") (v "2.2.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset"))) (t "cfg(windows)") (k 0)))) (h "05qr5r241lzin03dx5sa3ch8xpbf0dg03c3za7xkfizqaz8403bj")))

(define-public crate-windows-win-2.2.1 (c (n "windows-win") (v "2.2.1") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset"))) (t "cfg(windows)") (k 0)))) (h "01hjc83amxgz3vfc5xyhs7grzwivmll1psiw5qfbwljyw70k0cwn")))

(define-public crate-windows-win-2.2.2 (c (n "windows-win") (v "2.2.2") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset"))) (t "cfg(windows)") (k 0)))) (h "0z0fnwkf0r9l02z9y0jvbqy1gfbk2izzpm2caabzphas36fl2mh0")))

(define-public crate-windows-win-2.3.0 (c (n "windows-win") (v "2.3.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset"))) (t "cfg(windows)") (k 0)))) (h "1rrvn1cpcwhmcwibjak3gg6291i08fkqx572y8rjq6ckjmakyhdh")))

(define-public crate-windows-win-2.4.0 (c (n "windows-win") (v "2.4.0") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset"))) (t "cfg(windows)") (k 0)))) (h "1a8g2q4zzmvb9zzz36cml2afkwz66abzsj3xr00ilwrmn23nknip")))

(define-public crate-windows-win-2.4.1 (c (n "windows-win") (v "2.4.1") (d (list (d (n "clipboard-win") (r "^2") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("std" "windef" "winerror" "handleapi" "basetsd" "winnt" "minwindef" "winuser" "processthreadsapi" "handleapi" "memoryapi" "winbase" "wincon" "fileapi" "memoryapi" "libloaderapi" "profileapi" "threadpoollegacyapiset" "securitybaseapi"))) (t "cfg(windows)") (k 0)))) (h "1h40fhkgs5mwzhy24ikabwkbiwbkx6hc1cv8wssfkr5g4gn46hld")))

(define-public crate-windows-win-3.0.0 (c (n "windows-win") (v "3.0.0") (d (list (d (n "clipboard-win") (r "^5") (d #t) (t "cfg(windows)") (k 2)) (d (n "error-code") (r "^3") (d #t) (k 0)))) (h "1p7jbk3i7wj1i6w7chfp4rpbyd6ckgncp6h493wm4frbc8rkxqjq")))

