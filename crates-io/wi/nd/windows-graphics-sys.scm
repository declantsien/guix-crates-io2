(define-module (crates-io wi nd windows-graphics-sys) #:use-module (crates-io))

(define-public crate-windows-graphics-sys-0.0.2 (c (n "windows-graphics-sys") (v "0.0.2") (h "1zr544nazkqrykpd1pvr58n5hb66qq3bzczbl5b2h7cnydzz7ski") (r "1.46")))

(define-public crate-windows-graphics-sys-0.1.0 (c (n "windows-graphics-sys") (v "0.1.0") (h "0gfc6xgia6rmnsx04a0br7adrn83zq74kx33c6z8mq7jnyafkn4n")))

