(define-module (crates-io wi nd windows-volume-control) #:use-module (crates-io))

(define-public crate-windows-volume-control-0.1.0 (c (n "windows-volume-control") (v "0.1.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_Media_Audio" "Win32_System_Com" "Win32_Media_Audio_Endpoints" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell_PropertiesSystem" "Win32_Devices_FunctionDiscovery" "Win32_Foundation" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)))) (h "13xjslrbrwagbn5rsdmpm62z50vn1w94mzrqy7mzi4wjc2xrx47x")))

(define-public crate-windows-volume-control-0.1.1 (c (n "windows-volume-control") (v "0.1.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("implement" "Win32_Media_Audio" "Win32_System_Com" "Win32_Media_Audio_Endpoints" "Win32_System_Com_StructuredStorage" "Win32_UI_Shell_PropertiesSystem" "Win32_Devices_FunctionDiscovery" "Win32_Foundation" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)))) (h "03bl37bjdxn4dhljrwpygm4d76913a10if4wd2g9ls8fg6inqmbg")))

