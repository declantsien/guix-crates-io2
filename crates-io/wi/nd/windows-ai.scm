(define-module (crates-io wi nd windows-ai) #:use-module (crates-io))

(define-public crate-windows-ai-0.7.0 (c (n "windows-ai") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0w019l1mssm3wdwhm7b6z4lnc4gbqbsg7h7dhql38qskwm1f910i") (y #t)))

(define-public crate-windows-ai-0.22.0 (c (n "windows-ai") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "110hr1cngv2cncjdswsk4ivm2ryi4ks8pj2bbyi67fl7vyaxklzj") (y #t)))

(define-public crate-windows-ai-0.22.1 (c (n "windows-ai") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)))) (h "0zls386xgd091rl86ifiqs65bd2x1bb5s91f1p874w0n5nzhg0x6") (y #t)))

(define-public crate-windows-ai-0.22.2 (c (n "windows-ai") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)))) (h "0v7ycinsgh0z6afy1v8681ahv7q7ic7qqs87is1cpp0fmvqq8dnp") (y #t)))

(define-public crate-windows-ai-0.22.3 (c (n "windows-ai") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "19g1di5gqgp2sgxlz6w5ri6474632q8fimmfrixyyfkwiaklkcqb") (y #t)))

(define-public crate-windows-ai-0.22.4 (c (n "windows-ai") (v "0.22.4") (h "04dvn3v2x9cv40gih9dzl45a4lrgg01ffgd3s5pmj33lyf8ssbw2") (r "1.46")))

(define-public crate-windows-ai-0.22.5 (c (n "windows-ai") (v "0.22.5") (h "14xa4692kzk0nsj2gz7d7k2h8bxdv015p6gmad8g6v7zyqxi4mas") (r "1.46")))

(define-public crate-windows-ai-0.22.6 (c (n "windows-ai") (v "0.22.6") (h "0mk6nfsp1kcaqjbm02fsi5l3h5m1f5yqkgani93jq808f1b4v4bm") (r "1.46")))

(define-public crate-windows-ai-0.1.0 (c (n "windows-ai") (v "0.1.0") (h "14fmdfdhjlvy3ry4492k0rxv9r0vlkiff23ys1hc9pmngzawz2az")))

(define-public crate-windows-ai-0.23.0 (c (n "windows-ai") (v "0.23.0") (h "0g8flqknpjhc7wy1zc63k1aalsi3fbwpj48mcscar9pjd4ysay04")))

