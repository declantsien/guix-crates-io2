(define-module (crates-io wi nd windows-win32-gaming) #:use-module (crates-io))

(define-public crate-windows-win32-gaming-0.22.1 (c (n "windows-win32-gaming") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-win32-foundation") (r "^0.22.1") (d #t) (k 0)))) (h "0yyvi01sylmk48sgvbfnp5q6m56x2akwvkrq3mp0m1b05xdmp85n")))

(define-public crate-windows-win32-gaming-0.22.2 (c (n "windows-win32-gaming") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-win32-foundation") (r "^0.22.2") (d #t) (k 0)))) (h "0r0c885z72g57y9igkq5lgc4mnj33n53dpnbcbz4ncx36lc7kg0v")))

(define-public crate-windows-win32-gaming-0.22.3 (c (n "windows-win32-gaming") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0s4sj5b55cbql86ilb1zz2f3j2g34097xxqymb0f7v2yand5x40q")))

(define-public crate-windows-win32-gaming-0.22.5 (c (n "windows-win32-gaming") (v "0.22.5") (h "17rgg0wr753w29p2hdcwhmrnafvfxkzv4k814nbi7c9rwy3y49fx") (r "1.46")))

(define-public crate-windows-win32-gaming-0.22.6 (c (n "windows-win32-gaming") (v "0.22.6") (h "0v364cpgdb71i0097dmw7nf7pcyglip7yxa1d967p8cgim3sfyw4") (r "1.46")))

(define-public crate-windows-win32-gaming-0.23.0 (c (n "windows-win32-gaming") (v "0.23.0") (h "0i7abqj3rb4il29qsdpq96gk6z2pmq3l0czndd1zgc6lv5q1p0pc")))

