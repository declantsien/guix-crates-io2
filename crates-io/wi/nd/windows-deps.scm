(define-module (crates-io wi nd windows-deps) #:use-module (crates-io))

(define-public crate-windows-deps-0.1.0 (c (n "windows-deps") (v "0.1.0") (h "17a8bpkxpxrdabhbbjafkvq1ffhgallc0mwkgmh7fj0ixw42b9x6")))

(define-public crate-windows-deps-0.2.0 (c (n "windows-deps") (v "0.2.0") (h "0lw3n18s2akvc2n2z56f00a73f12m8kwgzg5i4yd5m6851h0bbvl")))

(define-public crate-windows-deps-0.3.0 (c (n "windows-deps") (v "0.3.0") (d (list (d (n "windows_gen") (r "^0.13.0") (d #t) (k 1)))) (h "0j4aj5sc8l90izyx4fvlm6f81y754wqvp4k97fm4l9l64cmp6rl4")))

