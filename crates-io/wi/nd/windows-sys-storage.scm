(define-module (crates-io wi nd windows-sys-storage) #:use-module (crates-io))

(define-public crate-windows-sys-storage-0.22.5 (c (n "windows-sys-storage") (v "0.22.5") (h "0drvm6dj2bx2645w12frfnsasqdy5p1xxjx8iil279vrl0fgm6qp") (r "1.46")))

(define-public crate-windows-sys-storage-0.22.6 (c (n "windows-sys-storage") (v "0.22.6") (h "1vs7538x0a18cg65cy2g3kscxcljsfy792ji0q98akz9626k507g") (r "1.46")))

(define-public crate-windows-sys-storage-0.23.0 (c (n "windows-sys-storage") (v "0.23.0") (h "0kysxj37fb3305am6n07y8k6vmzp8xm9lk65h1zrsbpfnncnj5v0")))

