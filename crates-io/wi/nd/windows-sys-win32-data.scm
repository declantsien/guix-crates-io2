(define-module (crates-io wi nd windows-sys-win32-data) #:use-module (crates-io))

(define-public crate-windows-sys-win32-data-0.22.5 (c (n "windows-sys-win32-data") (v "0.22.5") (h "1zbh3b37g0m3brlsm5nbrg6579h3ghb8x078b7ncvi9bm7jy5ir3") (r "1.46")))

(define-public crate-windows-sys-win32-data-0.22.6 (c (n "windows-sys-win32-data") (v "0.22.6") (h "1j8n1ydgp6nga0y50nri1aim2kw1n23z9fbq7ngyddvgxfkvn1i9") (r "1.46")))

(define-public crate-windows-sys-win32-data-0.23.0 (c (n "windows-sys-win32-data") (v "0.23.0") (h "1c0lfxjp7z1w3ibwqpnb4wn19ygwfj6fy6dwqwhaj7wpkrb8n5lx")))

