(define-module (crates-io wi nd windows-security) #:use-module (crates-io))

(define-public crate-windows-security-0.7.0 (c (n "windows-security") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1jn7qafnf135c84m4jbsbjk2bsdsx2i3r0c1vjv58fw5lc4f3j9i")))

(define-public crate-windows-security-0.22.0 (c (n "windows-security") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0rmznzdjzwn3slclwisjkll7xmngv1w1dfpvvwf09082q46gd20l")))

(define-public crate-windows-security-0.22.1 (c (n "windows-security") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "0gvi0wnnpxa5x6738bvnh2j1mwmd2rmlv8pyszvny6fnh0avizdm")))

(define-public crate-windows-security-0.22.3 (c (n "windows-security") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1vg1ryfav5n1zp1f6glrkafk13p8jz39v9shbs3dkyz59ihs8w1i")))

(define-public crate-windows-security-0.22.5 (c (n "windows-security") (v "0.22.5") (h "0n5azv4if0bafgl9izq2qak1qi7bab5iwrmz6j5lnkcshfls1sia") (r "1.46")))

(define-public crate-windows-security-0.22.6 (c (n "windows-security") (v "0.22.6") (h "0fhy78smvm276jjzgyj5yyhpqszssdw1kgzsa9cjl3sb1mypabld") (r "1.46")))

(define-public crate-windows-security-0.23.0 (c (n "windows-security") (v "0.23.0") (h "0qvrbfifpsm06scicn1iqfbdjrgzdg0g1hh1i4haygfxgch97rjr")))

