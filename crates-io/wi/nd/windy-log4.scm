(define-module (crates-io wi nd windy-log4) #:use-module (crates-io))

(define-public crate-windy-log4-0.1.0 (c (n "windy-log4") (v "0.1.0") (h "0qilwvhpbgjzgkmp4anj1pgcy1j9g7nqrgmb9mvcc76xqi792m1s")))

(define-public crate-windy-log4-0.1.1 (c (n "windy-log4") (v "0.1.1") (h "1m46byc4gzl9cimrqacg8pbl3b706dj3y73y7zm9jby938qif8rn")))

(define-public crate-windy-log4-0.1.2 (c (n "windy-log4") (v "0.1.2") (h "04yr6vfgxam1w05zw2z93ja1jrlfgx7bqnd885iqpzgn3aykvzw0")))

(define-public crate-windy-log4-0.1.3 (c (n "windy-log4") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)))) (h "1y59j0a982dqs7immn3ir8irw1h4pzc79hil05l3a1vqhskhzirg")))

(define-public crate-windy-log4-0.1.4 (c (n "windy-log4") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)))) (h "09wi9yihfaq2xwbvd3q6wjh2100lhx75b8miqv3wwg8i42z18ydj")))

(define-public crate-windy-log4-0.1.5 (c (n "windy-log4") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.5.2") (d #t) (k 2)))) (h "0jnb3v6xhw8fdxs252l0rl4ghfcfh5z8j227kyznmx3s8bcqxnb6")))

