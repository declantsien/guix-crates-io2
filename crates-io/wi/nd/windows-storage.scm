(define-module (crates-io wi nd windows-storage) #:use-module (crates-io))

(define-public crate-windows-storage-0.7.0 (c (n "windows-storage") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0sscgdfa1l9xcv7pl9hxaig64pkaqxh63nx2cj1q6pk4xkl424b4")))

(define-public crate-windows-storage-0.22.0 (c (n "windows-storage") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "09n6zx8ik0al8yc8ksipm0jlpsx75jbf6jiayaakwn2vn0m76yj2")))

(define-public crate-windows-storage-0.22.1 (c (n "windows-storage") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)))) (h "1pbf8fk2va7hj2zjfqi01lbaf5nw3bq3scpia01lh2l5hk08k45c")))

(define-public crate-windows-storage-0.22.2 (c (n "windows-storage") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)))) (h "0ryhkg4jx4i4b8p7ppk4rgx3ggd0dzcvl8ccl1cv2ixknl7kr75v")))

(define-public crate-windows-storage-0.22.3 (c (n "windows-storage") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0s53lm18mvbsia6cf9gh4f7amxqxhxrbjl52w4gysbkww9anny17")))

(define-public crate-windows-storage-0.22.5 (c (n "windows-storage") (v "0.22.5") (h "1zy0141lykddb7asml1jhp1s1a9adiqkzq0px90znc3szfj7gp6h") (r "1.46")))

(define-public crate-windows-storage-0.22.6 (c (n "windows-storage") (v "0.22.6") (h "18xvvlv45xld3hkivil59hb3pkcxch622qijvc7gdmzvlvvy3vwm") (r "1.46")))

(define-public crate-windows-storage-0.23.0 (c (n "windows-storage") (v "0.23.0") (h "00bjckya4zab1v0b8hr2yrsnwz58b1pgwpn5v3bmbpfk2j1m9296")))

