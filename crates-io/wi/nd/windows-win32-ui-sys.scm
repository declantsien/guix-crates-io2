(define-module (crates-io wi nd windows-win32-ui-sys) #:use-module (crates-io))

(define-public crate-windows-win32-ui-sys-0.0.2 (c (n "windows-win32-ui-sys") (v "0.0.2") (h "1zv2841m76hymk0634l6fj0i9v5dz10m8mnq1wy5zs0ms4p4i94r") (r "1.46")))

(define-public crate-windows-win32-ui-sys-0.23.0 (c (n "windows-win32-ui-sys") (v "0.23.0") (h "1dc537mi86aggn36a26f8hl9zy281l63y8xv5jgl1cdamlsz5inf")))

