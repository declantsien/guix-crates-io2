(define-module (crates-io wi nd windy-daily-utils-lib) #:use-module (crates-io))

(define-public crate-windy-daily-utils-lib-0.1.0 (c (n "windy-daily-utils-lib") (v "0.1.0") (h "0nd21rg9xbkxs4pnxqfh0576c6gr4p198ayyxflkvn8nv8madv44")))

(define-public crate-windy-daily-utils-lib-0.1.1 (c (n "windy-daily-utils-lib") (v "0.1.1") (h "1y3nlns54x27lr6rg8w1mswy6wf3hfg2rzpac4yb0675jxvrxkg3")))

