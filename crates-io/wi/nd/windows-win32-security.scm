(define-module (crates-io wi nd windows-win32-security) #:use-module (crates-io))

(define-public crate-windows-win32-security-0.22.0 (c (n "windows-win32-security") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1ciyxfzkn9rd983lpkxpxg62inz8argpk40zyjrg7qy0lbb6kc3b")))

(define-public crate-windows-win32-security-0.22.3 (c (n "windows-win32-security") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1cd1ipsldpjr8rg56flj46f1pkgwykxzgzp254cz412wacrp2fhn")))

(define-public crate-windows-win32-security-0.22.5 (c (n "windows-win32-security") (v "0.22.5") (h "0nc2f0nshik1la0przb1wydzj58kc6pmvw0algxb2sdr8saiqgky") (r "1.46")))

(define-public crate-windows-win32-security-0.22.6 (c (n "windows-win32-security") (v "0.22.6") (h "0cn9c0s4bm89zf84wnb3ffs9xk5jgz1l7p9xkfkm83h3shlw7p89") (r "1.46")))

(define-public crate-windows-win32-security-0.23.0 (c (n "windows-win32-security") (v "0.23.0") (h "08lxvrsfkqhq4s37dymr34llbgb6w2zb8shj4g92abd3kj6j5dgz")))

