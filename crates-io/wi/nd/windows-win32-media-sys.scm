(define-module (crates-io wi nd windows-win32-media-sys) #:use-module (crates-io))

(define-public crate-windows-win32-media-sys-0.0.2 (c (n "windows-win32-media-sys") (v "0.0.2") (h "041c8i25vivgks8cwi926snmic97v20h25dgpck3p13pn5yhad3y") (r "1.46")))

(define-public crate-windows-win32-media-sys-0.23.0 (c (n "windows-win32-media-sys") (v "0.23.0") (h "0pwlgqdqfl8yw8dyvimqcwblgr05abd1y7l0x1572j0zlmai3hzh")))

