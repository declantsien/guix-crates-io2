(define-module (crates-io wi nd windows-ext) #:use-module (crates-io))

(define-public crate-windows-ext-0.0.0 (c (n "windows-ext") (v "0.0.0") (d (list (d (n "windows") (r "^0.52") (o #t) (d #t) (k 0)) (d (n "windows-sys") (r "^0.52") (o #t) (d #t) (k 0)))) (h "04wdgyb7wk024h4yhx47l0yscx3rkb4i3pnfi8ky40dq2dnwpzb7") (f (quote (("no-minwindef") ("default" "winrs")))) (y #t) (s 2) (e (quote (("winsys" "dep:windows-sys") ("winrs" "dep:windows"))))))

(define-public crate-windows-ext-0.0.1 (c (n "windows-ext") (v "0.0.1") (d (list (d (n "windows") (r "^0.52") (o #t) (d #t) (k 0)) (d (n "windows-sys") (r "^0.52") (o #t) (d #t) (k 0)))) (h "1vjnxjm5difn1km883a6cmsxg3nq9lpc0nqc5xcfzk0cg9n1czz2") (f (quote (("no-minwindef") ("default" "winrs")))) (s 2) (e (quote (("winsys" "dep:windows-sys") ("winrs" "dep:windows"))))))

(define-public crate-windows-ext-0.0.2 (c (n "windows-ext") (v "0.0.2") (d (list (d (n "windows") (r "^0.54") (d #t) (k 0)))) (h "1wivrvnndv95d5nv86qcbmhq2r44k2jp1g0z7f0hhks1hxgmzxcy") (f (quote (("minwindef") ("ext-impls") ("default" "ext-impls" "minwindef"))))))

