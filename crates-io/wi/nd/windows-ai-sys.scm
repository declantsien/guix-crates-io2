(define-module (crates-io wi nd windows-ai-sys) #:use-module (crates-io))

(define-public crate-windows-ai-sys-0.0.0 (c (n "windows-ai-sys") (v "0.0.0") (h "0vpazg8i71glbj8qinbz5vkxg0sgdwaykcfajpklgqhx4y092j9v") (r "1.46")))

(define-public crate-windows-ai-sys-0.0.1 (c (n "windows-ai-sys") (v "0.0.1") (h "11viivk41vply19cs7j9dzlrk6psldxf691dyr0q1kmsqvc3311j") (r "1.46")))

(define-public crate-windows-ai-sys-0.0.2 (c (n "windows-ai-sys") (v "0.0.2") (h "16dcams17ylqna1zh91wc9kb283fr3cxc9s6j304a2v8drgrw8si") (r "1.46")))

(define-public crate-windows-ai-sys-0.1.0 (c (n "windows-ai-sys") (v "0.1.0") (h "0n24hmf82ll0ybigbjhqx87xpl134b5np353lbmx3fqck97rq9di")))

(define-public crate-windows-ai-sys-0.23.0 (c (n "windows-ai-sys") (v "0.23.0") (h "1q5bmh24a4lcwrxlw3jdnalaf0svzqzbz1n0j709cxbwwh4s9jdk")))

