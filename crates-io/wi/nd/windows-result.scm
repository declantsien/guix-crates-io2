(define-module (crates-io wi nd windows-result) #:use-module (crates-io))

(define-public crate-windows-result-0.0.0 (c (n "windows-result") (v "0.0.0") (h "0ir7rjk0r2q5pda1hi5fbrpx88zvf85048ax3l19cds0c8n6f4mw") (r "1.60")))

(define-public crate-windows-result-0.1.0 (c (n "windows-result") (v "0.1.0") (d (list (d (n "windows-bindgen") (r "^0.53.0") (d #t) (k 2)) (d (n "windows-targets") (r "^0.52.1") (d #t) (k 0)))) (h "0r1z5903al7pzv1jsvbmnqnsn8nlp38x2hy3xl5gp38nwmwdy6fd") (r "1.60")))

(define-public crate-windows-result-0.1.1 (c (n "windows-result") (v "0.1.1") (d (list (d (n "windows-bindgen") (r "^0.56.0") (d #t) (k 2)) (d (n "windows-targets") (r "^0.52.5") (d #t) (k 0)))) (h "0fw1j5birb2nhp2s2wcg0fwyv7hbvp548bld1mh2xn3jrjlhv7vl") (r "1.60")))

