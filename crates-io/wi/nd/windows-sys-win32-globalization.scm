(define-module (crates-io wi nd windows-sys-win32-globalization) #:use-module (crates-io))

(define-public crate-windows-sys-win32-globalization-0.22.5 (c (n "windows-sys-win32-globalization") (v "0.22.5") (h "0jn1l5vi7snypyrraq8z731x8qn91l0pnxl38v7xvbb7hi7l3x07") (r "1.46")))

(define-public crate-windows-sys-win32-globalization-0.22.6 (c (n "windows-sys-win32-globalization") (v "0.22.6") (h "0qy097541ry2imppk01jkraxhcc2yhvr5xm0xqlx8zi3krvp9d6z") (r "1.46")))

(define-public crate-windows-sys-win32-globalization-0.23.0 (c (n "windows-sys-win32-globalization") (v "0.23.0") (h "1cn1akb2nffm5zjxlbljw2r5zjrcr0afib3qg9yzgzmayzbsxyx7")))

