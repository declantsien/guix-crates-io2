(define-module (crates-io wi nd windows-webview2) #:use-module (crates-io))

(define-public crate-windows-webview2-0.1.0 (c (n "windows-webview2") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 1)) (d (n "windows") (r "^0.7.0") (d #t) (k 0)) (d (n "windows") (r "^0.7.0") (d #t) (k 1)))) (h "0lbqpwa5cvw0pw23b4kq1iw8mgk3xdkz0j0j0xrj856m9bgka7i9")))

(define-public crate-windows-webview2-0.1.1 (c (n "windows-webview2") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 1)) (d (n "windows") (r "^0.9.1") (d #t) (k 0)) (d (n "windows") (r "^0.9.1") (d #t) (k 1)))) (h "0kjbvpbi82jl3d2cxf6svqb5qkpngx36cyxi290mspifyq5jrsbs") (y #t)))

