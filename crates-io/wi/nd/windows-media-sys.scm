(define-module (crates-io wi nd windows-media-sys) #:use-module (crates-io))

(define-public crate-windows-media-sys-0.0.2 (c (n "windows-media-sys") (v "0.0.2") (h "02l73n21pj48jnyj4q8rlqk4cl19libcj1lk97xrf3lg4xbyr5b1") (r "1.46")))

(define-public crate-windows-media-sys-0.23.0 (c (n "windows-media-sys") (v "0.23.0") (h "1659h2kdfp2j476pi4pbrskyf5774a2iddb7yj7js935ddrwzwk2")))

