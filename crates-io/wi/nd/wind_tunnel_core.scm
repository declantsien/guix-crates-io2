(define-module (crates-io wi nd wind_tunnel_core) #:use-module (crates-io))

(define-public crate-wind_tunnel_core-0.1.0-alpha.2 (c (n "wind_tunnel_core") (v "0.1.0-alpha.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jynbhhzpkfajvc8mhxz5hqaimwhs952930ing94fsasd5qgs4d6")))

(define-public crate-wind_tunnel_core-0.1.0-alpha.3 (c (n "wind_tunnel_core") (v "0.1.0-alpha.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bclgs2fkbp61jqfy6dg5m6vnw174f4s3i12zfx7pkqwcnbibh56")))

(define-public crate-wind_tunnel_core-0.2.0-alpha.1 (c (n "wind_tunnel_core") (v "0.2.0-alpha.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4c0f0wyb98zhsaah4hwq8vdnpbzwp1daa6g5m2kp2vr5fc24m0")))

(define-public crate-wind_tunnel_core-0.2.0-alpha.2 (c (n "wind_tunnel_core") (v "0.2.0-alpha.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvckvl7vpvgc4nrgrwqznfax8bw8gz3pszsvvqxw5rar2ja063s")))

