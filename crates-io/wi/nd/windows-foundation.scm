(define-module (crates-io wi nd windows-foundation) #:use-module (crates-io))

(define-public crate-windows-foundation-0.7.0 (c (n "windows-foundation") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1xf6cikarpvn1s0gbhy74x2yivnwh8fvgv1g65di9m6xlr62n6z6")))

(define-public crate-windows-foundation-0.22.0 (c (n "windows-foundation") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1xgx27j5y34i2bvfkbmazk5fr6bv8b4pik0v20m9abbl9bxqfqli")))

(define-public crate-windows-foundation-0.22.1 (c (n "windows-foundation") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)))) (h "1ljgblvsjk4yi2rb3488ghlcbrxvr89lwvwbm0rr2raa1d92vzln")))

(define-public crate-windows-foundation-0.22.2 (c (n "windows-foundation") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)))) (h "11z93cqs9ijcvl8q2hn3kwvv6967svzx09v33p0r9px7rayzbz6v")))

(define-public crate-windows-foundation-0.22.3 (c (n "windows-foundation") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0cjcc7ip3d1v3xkjnx2s038adwdx92yyqg0r72dd59c95mdv6dy2")))

(define-public crate-windows-foundation-0.22.4 (c (n "windows-foundation") (v "0.22.4") (h "137mp5c7434gbc7b2q85cj50vzyhhpii7h4fzwv853as2zk1248q") (r "1.46")))

(define-public crate-windows-foundation-0.22.5 (c (n "windows-foundation") (v "0.22.5") (h "1hkc32zjf3hmal0irjl1f542qikfr814k21ax3rvsic7nwv6prjm") (r "1.46")))

(define-public crate-windows-foundation-0.22.6 (c (n "windows-foundation") (v "0.22.6") (h "01ngxc39mj5f2kz4za8cfc6xjysgcjirz9n0yip336mg4cz1ddz4") (r "1.46")))

(define-public crate-windows-foundation-0.1.0 (c (n "windows-foundation") (v "0.1.0") (h "1vbdq215iq94w5b9n8sss2sdr2mxxysd7caf080a3973ic3s2hrv")))

(define-public crate-windows-foundation-0.23.0 (c (n "windows-foundation") (v "0.23.0") (h "11sgajb5cp2qbhx2bahanzd8cif60zx51rnl42qqvxlz96hzyz7k")))

