(define-module (crates-io wi nd windows-sys-win32-media) #:use-module (crates-io))

(define-public crate-windows-sys-win32-media-0.22.5 (c (n "windows-sys-win32-media") (v "0.22.5") (h "0w7aklc1ppz76xa6337nzs05v0n7q853sw1v1jf2nhqn4097fc6c") (r "1.46")))

(define-public crate-windows-sys-win32-media-0.22.6 (c (n "windows-sys-win32-media") (v "0.22.6") (h "1hk81a85yfm8krh3bypz9gm1nn2lac0x9z67dccnby0qafxs65af") (r "1.46")))

(define-public crate-windows-sys-win32-media-0.23.0 (c (n "windows-sys-win32-media") (v "0.23.0") (h "0id8374mi1rspyl6zbvngbyi5qxzx7cmj8xihmvi9cyaj9sqcckd")))

