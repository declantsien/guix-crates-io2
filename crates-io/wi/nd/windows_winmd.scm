(define-module (crates-io wi nd windows_winmd) #:use-module (crates-io))

(define-public crate-windows_winmd-0.1.2 (c (n "windows_winmd") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "windows_winmd_macros") (r "^0.1.2") (d #t) (k 0)))) (h "166h7xq1q7dip18g6nz50r0mibknfcc5hksa9sqbgc10p5ln8p8r")))

(define-public crate-windows_winmd-0.1.3 (c (n "windows_winmd") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "windows_winmd_macros") (r "^0.1.3") (d #t) (k 0)))) (h "061h963j4f1016qjjrhc65nb91hrkzrywm183n8f4r0s7bfdhsp4")))

(define-public crate-windows_winmd-0.1.4 (c (n "windows_winmd") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "windows_winmd_macros") (r "^0.1.4") (d #t) (k 0)))) (h "0jdl1k6b4ad3vqdkfbdasplgppd92cx6wnnw3kkc93x2iywdbcrs")))

(define-public crate-windows_winmd-0.2.1 (c (n "windows_winmd") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "windows_winmd_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1h6n84mm1f69bj7yk251xmdcm2cwwhygjbz9d7mr4inhradpd820")))

(define-public crate-windows_winmd-0.3.0 (c (n "windows_winmd") (v "0.3.0") (d (list (d (n "windows_winmd_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1n0z9h2smvxpgkxsjiyahpndfssdpzhf4zlkk6cg799vhnhkdq3d")))

(define-public crate-windows_winmd-0.3.1 (c (n "windows_winmd") (v "0.3.1") (d (list (d (n "windows_winmd_macros") (r "^0.3.1") (d #t) (k 0)))) (h "127wbx0y8nv1fv1xs0zwvda3fi9wrvbgb66m5qqi75scs0klbm0n")))

