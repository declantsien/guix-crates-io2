(define-module (crates-io wi nd windows-win32-media) #:use-module (crates-io))

(define-public crate-windows-win32-media-0.22.0 (c (n "windows-win32-media") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1n9cnwah35yxf9v3hlzfdm8qrs1nr7rj8cbpznkzj6db2x8vn6mi")))

(define-public crate-windows-win32-media-0.22.3 (c (n "windows-win32-media") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "06zq7yil1h7da3c674zf59jk3z1s2rs3nxnx1r5baa69caizw7xm")))

(define-public crate-windows-win32-media-0.22.5 (c (n "windows-win32-media") (v "0.22.5") (h "1sr8ic6gvvvv2bllriphn2pxard91a2wpmn7jqpcmm9hn90giyb0") (r "1.46")))

(define-public crate-windows-win32-media-0.22.6 (c (n "windows-win32-media") (v "0.22.6") (h "1d8qh9df0nz8w3c9gn3aidqxfz1w6kmzfyg7yw9kblib2j04w3sk") (r "1.46")))

(define-public crate-windows-win32-media-0.23.0 (c (n "windows-win32-media") (v "0.23.0") (h "0kl53q9mjbi2ds6hy2n017zdlxpwv98i60sj40nrzwpn6avsrzqr")))

