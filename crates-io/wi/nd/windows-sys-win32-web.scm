(define-module (crates-io wi nd windows-sys-win32-web) #:use-module (crates-io))

(define-public crate-windows-sys-win32-web-0.22.4 (c (n "windows-sys-win32-web") (v "0.22.4") (h "1262mm54a7kzvv1vajpx70x8mx6w6x0d34vad1ylgz1ryv0la3iq") (r "1.46")))

(define-public crate-windows-sys-win32-web-0.22.5 (c (n "windows-sys-win32-web") (v "0.22.5") (h "031dslpfjacxg13k7f5ffwx2i71ywy7qarsqfp0k2kv56zyqfrv8") (r "1.46")))

(define-public crate-windows-sys-win32-web-0.22.6 (c (n "windows-sys-win32-web") (v "0.22.6") (h "1xg9vv4xjb4h92jqb5211fjycr11j5adhxgzfpvkpk1302fwyp8g") (r "1.46")))

(define-public crate-windows-sys-win32-web-0.23.0 (c (n "windows-sys-win32-web") (v "0.23.0") (h "14jb3238210f5y1wjcs8w41r5r6xxr3jj9bagd0ppy2bg31dx5yf")))

