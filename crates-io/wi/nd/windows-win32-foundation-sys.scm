(define-module (crates-io wi nd windows-win32-foundation-sys) #:use-module (crates-io))

(define-public crate-windows-win32-foundation-sys-0.0.2 (c (n "windows-win32-foundation-sys") (v "0.0.2") (h "16l6fci5pmi6kwvdj0ay9yjc3kh9lmqrwzyi0iss671084qnw64s") (r "1.46")))

(define-public crate-windows-win32-foundation-sys-0.23.0 (c (n "windows-win32-foundation-sys") (v "0.23.0") (h "1x3gpnkv91ckgpil8lmjpk7j20alc4b9m3wrl52816vpimqcf21k")))

