(define-module (crates-io wi nd windows-win32-management-sys) #:use-module (crates-io))

(define-public crate-windows-win32-management-sys-0.0.2 (c (n "windows-win32-management-sys") (v "0.0.2") (h "1bdjagvkkd9azwb9mh676xq1z3pgchw6yims20vly603rk875msh") (r "1.46")))

(define-public crate-windows-win32-management-sys-0.23.0 (c (n "windows-win32-management-sys") (v "0.23.0") (h "160yzx0gr1jlxrjnqgng0n0f0sdjyqzffx8y3mi8nki11zbl61s6")))

