(define-module (crates-io wi nd windows-sys-foundation) #:use-module (crates-io))

(define-public crate-windows-sys-foundation-0.22.4 (c (n "windows-sys-foundation") (v "0.22.4") (h "1kcp638a18pdjyapc4l3h09b1ihlvi1rzharvy56qma5a66d799i") (r "1.46")))

(define-public crate-windows-sys-foundation-0.22.5 (c (n "windows-sys-foundation") (v "0.22.5") (h "0w2z990qs43z683pmjvssing1g6xw7pcm4k0krr754k4p6jqkndl") (r "1.46")))

(define-public crate-windows-sys-foundation-0.22.6 (c (n "windows-sys-foundation") (v "0.22.6") (h "1n0rbqwad43wk40l000cxcm3jafgrvjddd8qsw0cac614rixgd8x") (r "1.46")))

(define-public crate-windows-sys-foundation-0.23.0 (c (n "windows-sys-foundation") (v "0.23.0") (h "1wgvkxbbwpdfpjhdlvbhi9q5ch0xvy698fqdhkhhx3ry6anw2w5p")))

