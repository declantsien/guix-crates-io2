(define-module (crates-io wi nd windows-bindgen) #:use-module (crates-io))

(define-public crate-windows-bindgen-0.0.0 (c (n "windows-bindgen") (v "0.0.0") (h "1aff5abhd4xp6q09v6dzd7hx10zwfnyadz260krfnjnn9dsvgmbc")))

(define-public crate-windows-bindgen-0.29.0 (c (n "windows-bindgen") (v "0.29.0") (d (list (d (n "quote") (r "^0.29.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.29.0") (d #t) (k 0) (p "windows_reader")))) (h "0kkcrdk6p3bn30ybr22z1x6556qsyva6zf54dsb86d9k8szkh4dh")))

(define-public crate-windows-bindgen-0.30.0 (c (n "windows-bindgen") (v "0.30.0") (d (list (d (n "quote") (r "^0.30.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.30.0") (d #t) (k 0) (p "windows_reader")))) (h "1ii73s1bd95zrg6wyvj12hvzad5slfdbdy08hd46dpg9r9gm8k4l")))

(define-public crate-windows-bindgen-0.31.0 (c (n "windows-bindgen") (v "0.31.0") (d (list (d (n "metadata") (r "^0.31.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.31.0") (d #t) (k 0) (p "windows-tokens")))) (h "1kwcjw93k7vkb43wl2b98l6kchlmzpg836031i0s1x4sadhaqlyk")))

(define-public crate-windows-bindgen-0.32.0 (c (n "windows-bindgen") (v "0.32.0") (d (list (d (n "metadata") (r "^0.32.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.32.0") (d #t) (k 0) (p "windows-tokens")))) (h "0xvwcslb12rnbzkhsfja3x32hmybr7xyzmakigi450qzdicmvmz7")))

(define-public crate-windows-bindgen-0.33.0 (c (n "windows-bindgen") (v "0.33.0") (d (list (d (n "metadata") (r "^0.33.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.33.0") (d #t) (k 0) (p "windows-tokens")))) (h "0bvf42rjggjlmj0rbgl13mz8m018x6b6idyj6c8yb96xdm80mvdx")))

(define-public crate-windows-bindgen-0.34.0 (c (n "windows-bindgen") (v "0.34.0") (d (list (d (n "metadata") (r "^0.34.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.34.0") (d #t) (k 0) (p "windows-tokens")))) (h "1zvfjkyrnddqzw8dzaydyjc4ib3yiir0h25zdz5xnxqsdcmk70gd")))

(define-public crate-windows-bindgen-0.35.0 (c (n "windows-bindgen") (v "0.35.0") (d (list (d (n "metadata") (r "^0.35.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.35.0") (d #t) (k 0) (p "windows-tokens")))) (h "0fnj2lvvjywjgky82amh9vpfhs4kfsgzg4hf5ypvbscx4r8jgf2b")))

(define-public crate-windows-bindgen-0.36.0 (c (n "windows-bindgen") (v "0.36.0") (d (list (d (n "metadata") (r "^0.36.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.36.0") (d #t) (k 0) (p "windows-tokens")))) (h "1b0z2038675qxymf9n85jim1q62dvs0ihidzxdwij7la1xmq2cyx")))

(define-public crate-windows-bindgen-0.36.1 (c (n "windows-bindgen") (v "0.36.1") (d (list (d (n "metadata") (r "^0.36.1") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.36.1") (d #t) (k 0) (p "windows-tokens")))) (h "0rifk2bxd7l4zjsd753wk202ifhig20r4zrz96d5m41kp45f502g")))

(define-public crate-windows-bindgen-0.37.0 (c (n "windows-bindgen") (v "0.37.0") (d (list (d (n "metadata") (r "^0.37.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.37.0") (d #t) (k 0) (p "windows-tokens")))) (h "1s2qb1zpj94vlnw5j67in4i5s06cxpll6lwvxj7z02ny3bippv8b")))

(define-public crate-windows-bindgen-0.38.0 (c (n "windows-bindgen") (v "0.38.0") (d (list (d (n "metadata") (r "^0.38.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.38.0") (d #t) (k 0) (p "windows-tokens")))) (h "1mcy4l8p84h3rbvynd0zdc144z7rzs3qhb5min39vc5nqh6v58b1")))

(define-public crate-windows-bindgen-0.39.0 (c (n "windows-bindgen") (v "0.39.0") (d (list (d (n "metadata") (r "^0.39.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.39.0") (d #t) (k 0) (p "windows-tokens")))) (h "0hfb8wdriamsrhvxzlw1b4xc8dyf9c7j8fdrhpxw1arq1syks038")))

(define-public crate-windows-bindgen-0.40.0 (c (n "windows-bindgen") (v "0.40.0") (d (list (d (n "metadata") (r "^0.40.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.40.0") (d #t) (k 0) (p "windows-tokens")))) (h "1249g2ysk1nv4xmjyf4fbxv7ak5vhvcz47ygx7mg8bkfrcdhs4sd")))

(define-public crate-windows-bindgen-0.41.0 (c (n "windows-bindgen") (v "0.41.0") (d (list (d (n "metadata") (r "^0.41.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.41.0") (d #t) (k 0) (p "windows-tokens")))) (h "1i05sjxhr263vgbbqxv1d3nmnf9iw3z07zqx1lsiywk1xllsrhj3")))

(define-public crate-windows-bindgen-0.42.0 (c (n "windows-bindgen") (v "0.42.0") (d (list (d (n "metadata") (r "^0.42.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.42.0") (d #t) (k 0) (p "windows-tokens")))) (h "1akwm9f5jhv0c5jsc65pdd8j9swyxbrvnxwz3j884qpm8y41643d")))

(define-public crate-windows-bindgen-0.43.0 (c (n "windows-bindgen") (v "0.43.0") (d (list (d (n "metadata") (r "^0.43.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.42.0") (d #t) (k 0) (p "windows-tokens")))) (h "09arkvj1r7kkzia146f8ds4nsapyqk67p9zvvf8fdm6yvsy3p997")))

(define-public crate-windows-bindgen-0.44.0 (c (n "windows-bindgen") (v "0.44.0") (d (list (d (n "metadata") (r "^0.44.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.44.0") (d #t) (k 0) (p "windows-tokens")))) (h "18v1w0ji9403139bfjkvd5aqi7yaxyxs3d489lm3h8b5ykn088i2")))

(define-public crate-windows-bindgen-0.46.0 (c (n "windows-bindgen") (v "0.46.0") (d (list (d (n "metadata") (r "^0.46.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.46.0") (d #t) (k 0) (p "windows-tokens")))) (h "00mzdqbwfm98ispbawlply7mx3g8bjsx96hjzkq93gs8krpcap8d")))

(define-public crate-windows-bindgen-0.47.0 (c (n "windows-bindgen") (v "0.47.0") (d (list (d (n "metadata") (r "^0.47.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.47.0") (d #t) (k 0) (p "windows-tokens")))) (h "1myzny9qi3s2yyc6vlzxn209jsbc124sk4cxgcznrvds0r5j3l0n") (y #t)))

(define-public crate-windows-bindgen-0.48.0 (c (n "windows-bindgen") (v "0.48.0") (d (list (d (n "metadata") (r "^0.48.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.48.0") (d #t) (k 0) (p "windows-tokens")))) (h "1rjswxh5wnxqcwlxb374ig4r1289brh43w36plnk3dslpivimqhz")))

(define-public crate-windows-bindgen-0.49.0 (c (n "windows-bindgen") (v "0.49.0") (d (list (d (n "metadata") (r "^0.49.0") (d #t) (k 0) (p "windows-metadata")) (d (n "tokens") (r "^0.48.0") (d #t) (k 0) (p "windows-tokens")))) (h "0q1b427fwa6cd9sk8lvwp2zdzpzmfns1h9g9ka95gvl4kfq5z4xn")))

(define-public crate-windows-bindgen-0.51.1 (c (n "windows-bindgen") (v "0.51.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.51.1") (d #t) (k 0)))) (h "0xfdq4q958qal5iks8xkaanf7w3akzfxc58dxvz7amhjg2vic7xw") (f (quote (("metadata") ("default" "metadata"))))))

(define-public crate-windows-bindgen-0.52.0 (c (n "windows-bindgen") (v "0.52.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.52.0") (d #t) (k 0)))) (h "071lrbhbvh0l8m1wf5000xxmcry1gjpqdxcqm23qmss9d05zn3lp") (f (quote (("metadata") ("default" "metadata")))) (r "1.56")))

(define-public crate-windows-bindgen-0.53.0 (c (n "windows-bindgen") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.53.0") (d #t) (k 0)))) (h "0qnjnivps648m01sykqzsphgc0rj41ci51aj7kkih6p7hr706phm") (f (quote (("metadata") ("default" "metadata")))) (r "1.70")))

(define-public crate-windows-bindgen-0.54.0 (c (n "windows-bindgen") (v "0.54.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.54.0") (d #t) (k 0)))) (h "1hid039rnygimc2kxkzfc892j6hcdjpza2490ggz35r8fjs7csfq") (f (quote (("metadata") ("default" "metadata")))) (r "1.70")))

(define-public crate-windows-bindgen-0.55.0 (c (n "windows-bindgen") (v "0.55.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.55.0") (d #t) (k 0)))) (h "0k4akghvmc1zv1n9rq19vvhlxq75bz6x42bqanfj7bgbhsjghgq7") (f (quote (("metadata") ("default" "metadata")))) (r "1.70")))

(define-public crate-windows-bindgen-0.56.0 (c (n "windows-bindgen") (v "0.56.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windows-metadata") (r "^0.56.0") (d #t) (k 0)))) (h "0inv2w78qv6375ndrgm9vilkgscwak80igz8vkf7zw8c6fk3x3m2") (f (quote (("metadata") ("default" "metadata")))) (r "1.70")))

