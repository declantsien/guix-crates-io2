(define-module (crates-io wi nd windows-sys-management) #:use-module (crates-io))

(define-public crate-windows-sys-management-0.22.5 (c (n "windows-sys-management") (v "0.22.5") (h "1am92vwsr4hs8mc293sg4s1nyy0xxhwhjasqzfrnyxqf1rckih64") (r "1.46")))

(define-public crate-windows-sys-management-0.22.6 (c (n "windows-sys-management") (v "0.22.6") (h "1krwd04lgprlm5p1qsqwlij1pgjdksc3w6p3j51w287cm7z3yaxq") (r "1.46")))

(define-public crate-windows-sys-management-0.23.0 (c (n "windows-sys-management") (v "0.23.0") (h "1c1whyyrdadc9alvw14bmpqjp4v0b4b978bmp08qkmji5915hvr3")))

