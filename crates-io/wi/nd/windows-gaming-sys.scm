(define-module (crates-io wi nd windows-gaming-sys) #:use-module (crates-io))

(define-public crate-windows-gaming-sys-0.0.2 (c (n "windows-gaming-sys") (v "0.0.2") (h "1r1mq3dpyqnpqmgvss4mq0clx63v9c097nfyacvbr52mjf5271zl") (r "1.46")))

(define-public crate-windows-gaming-sys-0.1.0 (c (n "windows-gaming-sys") (v "0.1.0") (h "01wpa5wap8yi43ajgwq3azdgh9w6cxmcn9id2ng4jc5l77vl662v")))

