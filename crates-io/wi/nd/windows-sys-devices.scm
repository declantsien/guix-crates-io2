(define-module (crates-io wi nd windows-sys-devices) #:use-module (crates-io))

(define-public crate-windows-sys-devices-0.22.4 (c (n "windows-sys-devices") (v "0.22.4") (h "005srs9qh411k6z6prvl2yvylcyz6lj7w1q04q7affk36r9vy6la") (r "1.46")))

(define-public crate-windows-sys-devices-0.22.5 (c (n "windows-sys-devices") (v "0.22.5") (h "04ywfdfbqzjyr971yncwndvhhx86n9pg65ls5jj2pxwirpd2l3dx") (r "1.46")))

(define-public crate-windows-sys-devices-0.22.6 (c (n "windows-sys-devices") (v "0.22.6") (h "08hqi1km0pfkq20sknll331799a49166nk8jyymwzmxjcl8vqziy") (r "1.46")))

(define-public crate-windows-sys-devices-0.23.0 (c (n "windows-sys-devices") (v "0.23.0") (h "0cczkdsjdrv6kf7hwy0f1rf2l65zm9q4nbm0j7n81kikx7l8fwvf")))

