(define-module (crates-io wi nd windirs) #:use-module (crates-io))

(define-public crate-windirs-0.1.0 (c (n "windirs") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (k 0)))) (h "05mg3xfig4xnpnz48bp9q4k4xkskygc2r9522wlcq4vhxqcd9zr3")))

(define-public crate-windirs-1.0.0 (c (n "windirs") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (k 0)))) (h "0xibqnl2l71vsxcnlc8ylfj9vfcqwrhac9ryknkdi9dwpyz0kfqr")))

(define-public crate-windirs-1.0.1 (c (n "windirs") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("knownfolders" "objbase" "shlobj" "winbase" "winerror"))) (d #t) (k 0)))) (h "1g0rfi9nmasdcq507jl10s4kqj77hwnbwjghxdkyyr4kbkcs5jw5")))

