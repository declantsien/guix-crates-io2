(define-module (crates-io wi nd windows-sys-win32-networkmanagement) #:use-module (crates-io))

(define-public crate-windows-sys-win32-networkmanagement-0.22.5 (c (n "windows-sys-win32-networkmanagement") (v "0.22.5") (h "0d32ra6n6gy3l4d4j83q4lmyvvq1zd603c5frvr97dkh96g092g0") (r "1.46")))

(define-public crate-windows-sys-win32-networkmanagement-0.22.6 (c (n "windows-sys-win32-networkmanagement") (v "0.22.6") (h "1zimb1lj6xskpibrbwdkvq08f8ji3bihh6bz1yj778a10yv85rkn") (r "1.46")))

(define-public crate-windows-sys-win32-networkmanagement-0.23.0 (c (n "windows-sys-win32-networkmanagement") (v "0.23.0") (h "0464wkr91kcscjzvnpyqrgh3xd4z4bpz1cz6w17x1cyjdfwm3kwa")))

