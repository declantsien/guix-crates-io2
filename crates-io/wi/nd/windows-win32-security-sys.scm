(define-module (crates-io wi nd windows-win32-security-sys) #:use-module (crates-io))

(define-public crate-windows-win32-security-sys-0.0.2 (c (n "windows-win32-security-sys") (v "0.0.2") (h "1kgm8jh9wyn3flxzdng6l6gfb6wjg8p7dsl1ghvnmv4w1zjmdcib") (r "1.46")))

(define-public crate-windows-win32-security-sys-0.23.0 (c (n "windows-win32-security-sys") (v "0.23.0") (h "09ynnqbfyragrcvshy70fx0jl6m2cmz0n9cnxf4aa6slgain2l6k")))

