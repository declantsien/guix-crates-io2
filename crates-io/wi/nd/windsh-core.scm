(define-module (crates-io wi nd windsh-core) #:use-module (crates-io))

(define-public crate-windsh-core-0.1.0-alpha.0 (c (n "windsh-core") (v "0.1.0-alpha.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1nc8dhrmqbfs2z71zvmwqnlfjyg96msvyzg5aqjc5wxgxyc06rd6")))

(define-public crate-windsh-core-0.1.1-alpha.0 (c (n "windsh-core") (v "0.1.1-alpha.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18pl8lhlbk9a0wi5snc12fn1hv7cjpi0hia1c1zagyh2pf1r6rlf") (y #t)))

(define-public crate-windsh-core-0.1.0-alpha.1 (c (n "windsh-core") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)))) (h "0n1447cx3z2c0jqwkhhd9ffvcqhnpgijv3nm6kkjw7awl56iixpn")))

