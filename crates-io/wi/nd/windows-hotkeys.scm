(define-module (crates-io wi nd windows-hotkeys) #:use-module (crates-io))

(define-public crate-windows-hotkeys-0.1.0 (c (n "windows-hotkeys") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (k 0)))) (h "19j5f0251qw6mxk7m2vmj5jil0avhfayajax2whs9287cn6n07cc")))

(define-public crate-windows-hotkeys-0.1.1 (c (n "windows-hotkeys") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (k 0)))) (h "1qc6wjjq67rmc80h66afyxq9grd0f4h83g7n12ja7s04ld1r8qxz")))

(define-public crate-windows-hotkeys-0.2.0 (c (n "windows-hotkeys") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (k 0)))) (h "1hhyq21d5nhbz8w03spvlw18s6dahqjkdg63py1s0m9gvgvzchdf") (f (quote (("threadsafe") ("default" "threadsafe"))))))

(define-public crate-windows-hotkeys-0.2.1 (c (n "windows-hotkeys") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "libloaderapi"))) (d #t) (k 0)))) (h "19va38vw3na2j8csbjyww1zrzfswjc2v95p7a1q3zcxr71l0n0p0") (f (quote (("threadsafe") ("default" "threadsafe"))))))

