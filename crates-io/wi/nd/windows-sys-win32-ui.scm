(define-module (crates-io wi nd windows-sys-win32-ui) #:use-module (crates-io))

(define-public crate-windows-sys-win32-ui-0.22.5 (c (n "windows-sys-win32-ui") (v "0.22.5") (h "0ar42d9jzimz0j43rsiirhfn6fksh01ff51y3x4kgc8jyik2fw3f") (r "1.46")))

(define-public crate-windows-sys-win32-ui-0.22.6 (c (n "windows-sys-win32-ui") (v "0.22.6") (h "0fc4j9sz4mij2axyga7bx0mqah1yz15ranhqdbv6p7rkkawwbsc3") (r "1.46")))

(define-public crate-windows-sys-win32-ui-0.23.0 (c (n "windows-sys-win32-ui") (v "0.23.0") (h "0bhx9xg4pd1x8bvv1mwlvb92cr38w5n3knijf5kj9w6y4vvnzd56")))

