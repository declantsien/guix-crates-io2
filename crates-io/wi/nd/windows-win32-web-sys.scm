(define-module (crates-io wi nd windows-win32-web-sys) #:use-module (crates-io))

(define-public crate-windows-win32-web-sys-0.0.2 (c (n "windows-win32-web-sys") (v "0.0.2") (h "0jcamazmv6sm8icggrywwnxhcrmjzvlbgdg9ns50ig379hl3biij") (r "1.46")))

(define-public crate-windows-win32-web-sys-0.23.0 (c (n "windows-win32-web-sys") (v "0.23.0") (h "126cp0cr61y9xw84h9sh12h9r92lfvp5d5cjhv0k5vx26qn4h0g3")))

