(define-module (crates-io wi nd windows-sys-graphics) #:use-module (crates-io))

(define-public crate-windows-sys-graphics-0.22.5 (c (n "windows-sys-graphics") (v "0.22.5") (h "1iicwj97c190llp5mrnag5ngsxwr77yzji1z31c2qs09l3p2fcv0") (r "1.46")))

(define-public crate-windows-sys-graphics-0.22.6 (c (n "windows-sys-graphics") (v "0.22.6") (h "1c2yqpgm5rx6byk0hkvq5r5js5jfs6pfzkgskgkxifha8sy6yzz6") (r "1.46")))

(define-public crate-windows-sys-graphics-0.23.0 (c (n "windows-sys-graphics") (v "0.23.0") (h "0bs6pvijs59b1gj3xmz5wadw3paggxnldbra4singxmv9xybbabi")))

