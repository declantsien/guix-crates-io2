(define-module (crates-io wi nd windows-sys-win32-ai) #:use-module (crates-io))

(define-public crate-windows-sys-win32-ai-0.22.5 (c (n "windows-sys-win32-ai") (v "0.22.5") (h "0d5wcc6ysasrh1sffzpg9gfarwddsb777i8bcakz0jz5rmyymjgh") (r "1.46")))

(define-public crate-windows-sys-win32-ai-0.22.6 (c (n "windows-sys-win32-ai") (v "0.22.6") (h "05a481n8ihgcvmb3pgz59bg32h0wbvdx9vy6f5m5286nspzs1by9") (r "1.46")))

(define-public crate-windows-sys-win32-ai-0.23.0 (c (n "windows-sys-win32-ai") (v "0.23.0") (h "12ajj490jv5xydkzvy5ji08sy6lzfibim2sd2dr0c3f76zprhbl4")))

