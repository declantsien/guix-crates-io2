(define-module (crates-io wi nd windows-executor) #:use-module (crates-io))

(define-public crate-windows-executor-0.2.0 (c (n "windows-executor") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.30") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "impl-default" "libloaderapi" "processthreadsapi" "winuser"))) (d #t) (k 0)))) (h "13i9s3akvyjxkrd4k7a30r5lpp40spk2j8lwy1xqdwkpj35nz60y")))

