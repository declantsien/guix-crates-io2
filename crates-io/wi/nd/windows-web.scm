(define-module (crates-io wi nd windows-web) #:use-module (crates-io))

(define-public crate-windows-web-0.7.0 (c (n "windows-web") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1flf4ypxb9w6f798vqz71yc3chl5lf6fbra4f97ydjsxrl975pk1")))

(define-public crate-windows-web-0.22.0 (c (n "windows-web") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1dbnz1vyc1wz067dsn76ih6ay4580ygflglyxw4wsgjc333zr9vp")))

(define-public crate-windows-web-0.22.1 (c (n "windows-web") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "0l8lgqm52s18jaxykwzyh88plxilvi2zqsda01czlma29ar08vih")))

(define-public crate-windows-web-0.22.2 (c (n "windows-web") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "0h30zc13nxc3v3q2l9csp3byzlpxwwka4a0y40c4xma9788g3bvq")))

(define-public crate-windows-web-0.22.3 (c (n "windows-web") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "113hp29rf2kdvdacqj0rsck95n58cv1sp9n86gb22mnwiba2bpjf")))

(define-public crate-windows-web-0.22.5 (c (n "windows-web") (v "0.22.5") (h "1fvb8ig0nyvpqxhpvxb7nkp9vjz9xwsk13hlrkb8vq4ima37v1hd") (r "1.46")))

(define-public crate-windows-web-0.22.6 (c (n "windows-web") (v "0.22.6") (h "0yw7mhfjawpp44kinwb00khibxfa412qrkn8qhb43a8ip6n7ddjy") (r "1.46")))

(define-public crate-windows-web-0.23.0 (c (n "windows-web") (v "0.23.0") (h "1wcx7k35vslf5a1w236jb47ss4cmk9g3c9vl4hkx9yi0m38dl2lk")))

