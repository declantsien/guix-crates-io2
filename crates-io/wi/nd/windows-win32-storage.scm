(define-module (crates-io wi nd windows-win32-storage) #:use-module (crates-io))

(define-public crate-windows-win32-storage-0.22.0 (c (n "windows-win32-storage") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0j12r76mg24q48ap90j10wf9yzsa3k7g99nklkhg9qvs2f631wax")))

(define-public crate-windows-win32-storage-0.22.3 (c (n "windows-win32-storage") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0pwcll9dpm76cwykplq791hxw449i7v2687yg27pkfw32qjj5cm8")))

(define-public crate-windows-win32-storage-0.22.5 (c (n "windows-win32-storage") (v "0.22.5") (h "0cfyhr3m9c8np05649drcxcj6m3m5qrxypspnmjdnk1mi68s7vjv") (r "1.46")))

(define-public crate-windows-win32-storage-0.22.6 (c (n "windows-win32-storage") (v "0.22.6") (h "0yal8y7m74wfzhnvy0qjg62x0zhzqmkl654hjr2y92gn44r2baz9") (r "1.46")))

(define-public crate-windows-win32-storage-0.23.0 (c (n "windows-win32-storage") (v "0.23.0") (h "17vscmjlfzq4m3qxy51gib536bssgdhwy9jc0brvlhjsx6w37vr6")))

