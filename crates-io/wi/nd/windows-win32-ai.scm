(define-module (crates-io wi nd windows-win32-ai) #:use-module (crates-io))

(define-public crate-windows-win32-ai-0.22.0 (c (n "windows-win32-ai") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1balq5k99qnddj8fjh7cimiqdd05cxwsfnnqwvn6j7wj9wxwvybx")))

(define-public crate-windows-win32-ai-0.22.2 (c (n "windows-win32-ai") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-win32-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-win32-graphics") (r "^0.22.2") (d #t) (k 0)))) (h "0l84yvaz1rv1simb5kzbiy3bssqfbd9s6lfj87wmfzx6b3a0lvq7")))

(define-public crate-windows-win32-ai-0.22.3 (c (n "windows-win32-ai") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0k6vcdn41qxcs0041as6v1pa3vjzi1y1ahpw3pkr7q0nzkqgbrp5")))

(define-public crate-windows-win32-ai-0.22.5 (c (n "windows-win32-ai") (v "0.22.5") (h "0zgvnf7k3s3shspbp6zbyfyb7srayb35g110dc41rxm0gya6zlgl") (r "1.46")))

(define-public crate-windows-win32-ai-0.22.6 (c (n "windows-win32-ai") (v "0.22.6") (h "0xfvhjgb3pwqx1rcljm70g07m84z8fjyndg27vjz0x1rdljbcpn0") (r "1.46")))

(define-public crate-windows-win32-ai-0.23.0 (c (n "windows-win32-ai") (v "0.23.0") (h "0v7fffkhjw3vw234xdvcihfi0wpm3snvb99m77ia83cxfabf1dac")))

