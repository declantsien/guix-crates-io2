(define-module (crates-io wi nd windows_types_registry) #:use-module (crates-io))

(define-public crate-windows_types_registry-0.1.0 (c (n "windows_types_registry") (v "0.1.0") (d (list (d (n "binread") (r "^2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winstructs") (r "^0.3.0") (d #t) (k 0)))) (h "11ldwfzfavga0522vq9my2i7b7rqzcsl7pcpl6rg8jr0l1kp8h4k")))

