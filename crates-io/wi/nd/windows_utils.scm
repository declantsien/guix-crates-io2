(define-module (crates-io wi nd windows_utils) #:use-module (crates-io))

(define-public crate-windows_utils-0.1.0 (c (n "windows_utils") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi"))) (d #t) (k 0)))) (h "0n9m5qxclq9grldijp339xqpizia6ib452ccasgfpngkzgriizxz")))

(define-public crate-windows_utils-0.2.0 (c (n "windows_utils") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (k 0)))) (h "1dpwrd151gc3yqhcxnh9cv3v3jqgjy94509ahjdjm7ws434hw5i2")))

(define-public crate-windows_utils-0.3.0 (c (n "windows_utils") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi" "winbase" "datetimeapi" "errhandlingapi" "winnls"))) (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (k 0)))) (h "10h94jkrm73w4mas2wpv5q877xj55d7g91ysbr709w6fyjfa6nar")))

(define-public crate-windows_utils-0.4.0 (c (n "windows_utils") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "shellapi" "winbase" "datetimeapi" "errhandlingapi" "winnls" "psapi" "processthreadsapi"))) (d #t) (k 0)) (d (n "winreg") (r "^0.6") (d #t) (k 0)))) (h "0ms2i5n8nl4ga712brcqm3gp27n5kpwnfdl3khadhvmzp7nilc3y")))

