(define-module (crates-io wi nd windows-globalization) #:use-module (crates-io))

(define-public crate-windows-globalization-0.7.0 (c (n "windows-globalization") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0xgmkw54846spy20fb6irhb9yg0ypfkr4kqwcbfn2qw64gx1fldl")))

(define-public crate-windows-globalization-0.22.0 (c (n "windows-globalization") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1yfw9lc7dxcg32p3dlgg939c7caw51maplmza80xiq2hnsgaarmm")))

(define-public crate-windows-globalization-0.22.1 (c (n "windows-globalization") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "1d6qcgvghypkbjncxbdmb7jdsx6dyl3fhaww92vim1kynr9dp208")))

(define-public crate-windows-globalization-0.22.2 (c (n "windows-globalization") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "001kymd2qwnrsk2880py9vishpdda6zhh3j01ha3kshg0r8jm351")))

(define-public crate-windows-globalization-0.22.3 (c (n "windows-globalization") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1dwfzxb1i1b9c3widivkkwgigchgmaxcvmai7ka0fd3pjjs2gr5r")))

(define-public crate-windows-globalization-0.22.5 (c (n "windows-globalization") (v "0.22.5") (h "1a3k4hg59np3gp3rhr6z2h60sa0i4xzkp8r4ma8i79z7wfz34jrm") (r "1.46")))

(define-public crate-windows-globalization-0.22.6 (c (n "windows-globalization") (v "0.22.6") (h "0p5x4m2iy1dp65rw51137fbf21hhyisx5r4k9n586bsj6n8clw51") (r "1.46")))

(define-public crate-windows-globalization-0.1.0 (c (n "windows-globalization") (v "0.1.0") (h "1mz68g4qdwsmkq3y2n5kiw4mimmind62f6vrczlzp2f88y7sbxa6")))

(define-public crate-windows-globalization-0.23.0 (c (n "windows-globalization") (v "0.23.0") (h "08lzag54rfii7vrwhsxy5klkzhjj9ix9b2nv77xkv6jljc5szqk8")))

