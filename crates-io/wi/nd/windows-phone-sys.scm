(define-module (crates-io wi nd windows-phone-sys) #:use-module (crates-io))

(define-public crate-windows-phone-sys-0.0.2 (c (n "windows-phone-sys") (v "0.0.2") (h "07gnhwv1my834bqq5ls2xfw4fvrpr5yinbjmkjmd12pq4a073z25") (r "1.46")))

(define-public crate-windows-phone-sys-0.23.0 (c (n "windows-phone-sys") (v "0.23.0") (h "1kba9ll2gbri1g8hczp1y0ir7w7h4pdv0ppmjnkx2sl045lnlqjm")))

