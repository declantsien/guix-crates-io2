(define-module (crates-io wi nd windows_gen) #:use-module (crates-io))

(define-public crate-windows_gen-0.1.2 (c (n "windows_gen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.2") (d #t) (k 0)))) (h "1zrilbhp5qsd5g97yd4z8qazaxm0gp44sxhlwfh787d475grc4hm")))

(define-public crate-windows_gen-0.1.3 (c (n "windows_gen") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.3") (d #t) (k 0)))) (h "1nvk8sx59pd1icgiap3cfa4r15y4m2nbgdv2w4dsnv80rc1d5l42")))

(define-public crate-windows_gen-0.1.4 (c (n "windows_gen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.4") (d #t) (k 0)))) (h "0gmssn4s422y7kc9ldmqqhnm2mls84kdjfq853gh07dx17k5r825")))

(define-public crate-windows_gen-0.2.1 (c (n "windows_gen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.2.1") (d #t) (k 0)))) (h "1rqyjcf2qac6gjyqys015dmlb8jhn7gcrx7ygr37h9pqbdvf48yy")))

(define-public crate-windows_gen-0.3.1 (c (n "windows_gen") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.3.1") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.3.1") (d #t) (k 0)))) (h "0xvxmldajqx37zn2cn9cb13mcya1zqgqx6j5wlpg8ixn0q7qzb3p")))

(define-public crate-windows_gen-0.4.0 (c (n "windows_gen") (v "0.4.0") (d (list (d (n "macros") (r "^0.4.0") (d #t) (k 0) (p "windows_gen_macros")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08ihczxvj4a5g7zbw7f4p8c285ff5m4fgf1aw4fdcpigr5c8f0kh")))

(define-public crate-windows_gen-0.5.0 (c (n "windows_gen") (v "0.5.0") (d (list (d (n "macros") (r "^0.5.0") (d #t) (k 0) (p "windows_gen_macros")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s02y8gsv8d5mj8rdbyjk8vhng6sy3capvs4dsrynmjwr39pakik")))

(define-public crate-windows_gen-0.6.0 (c (n "windows_gen") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen_macros") (r "^0.6.0") (d #t) (k 0)))) (h "1k1wg11242q3a9d2lscn08xj8sg95s13mcy5pi4asnvm539c0vwv")))

(define-public crate-windows_gen-0.7.0 (c (n "windows_gen") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "10gl1i5vdn3c1avabjv3z846pwjkq3f03mxnfigp6gj4c9z2wcwi")))

(define-public crate-windows_gen-0.8.0 (c (n "windows_gen") (v "0.8.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "1iaykmizfaz67ihjls09wsx8gyzlxdd3b0qq9rwcqsjn86i3045l")))

(define-public crate-windows_gen-0.9.0 (c (n "windows_gen") (v "0.9.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "1kb0bjqx1igbzqjcff56882xw482c7nlsmzlal7wpinzi2zbr0pw")))

(define-public crate-windows_gen-0.9.1 (c (n "windows_gen") (v "0.9.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (k 0)))) (h "0lh492px26rrna0harikyy4p7nk520pw2lv0dczp4n2xa6y4s5al")))

(define-public crate-windows_gen-0.10.0 (c (n "windows_gen") (v "0.10.0") (h "05ja2fnpcr5vfbcqgc1y0v65i1q5927ljjn66575kcrr19bq6qmw")))

(define-public crate-windows_gen-0.11.0 (c (n "windows_gen") (v "0.11.0") (h "1vvyxh0bvpwf4d4h9f06id21nkd3laa28x8i1z4a76xn9qal1ga4")))

(define-public crate-windows_gen-0.12.0 (c (n "windows_gen") (v "0.12.0") (h "1kgfkc7xri7301sjhn8m9jzyqiy6dcpcaivpf22v54rcc4wr6mcq")))

(define-public crate-windows_gen-0.13.0 (c (n "windows_gen") (v "0.13.0") (h "0iinrx22gmr46fhfk1xijah5isrnid4pqri8jpwln29171p1awb4")))

(define-public crate-windows_gen-0.14.0 (c (n "windows_gen") (v "0.14.0") (h "0iqr367flrv2j1i47acjc4w3ziw3d3fi87114hcxhqs0pkr9k7pi")))

(define-public crate-windows_gen-0.14.1 (c (n "windows_gen") (v "0.14.1") (h "08f7j2vwp47i97d2fp27zxgp6x1c7pw79606fhmzjvki705as2d1")))

(define-public crate-windows_gen-0.15.0 (c (n "windows_gen") (v "0.15.0") (h "17nwdcr4g66q1xmgflsj8gwg821m70k40jr9vp70sbslvk1fs7ib")))

(define-public crate-windows_gen-0.15.1 (c (n "windows_gen") (v "0.15.1") (h "0p4x3plivkamv4977k1bjcq6dw2sfvlnaq47f98hf08dl3qmxmqg")))

(define-public crate-windows_gen-0.15.2 (c (n "windows_gen") (v "0.15.2") (h "1yjxb70zzz8fcqmd2z0973432a9pgjd0qq78mad2mzqijjb5i7a1")))

(define-public crate-windows_gen-0.15.3 (c (n "windows_gen") (v "0.15.3") (h "1v0x3zhn164z3vzxk00062bpzwr5rykh7g4y25qsvak7rqfw0170")))

(define-public crate-windows_gen-0.15.4 (c (n "windows_gen") (v "0.15.4") (h "0nz5alx53mm2cy760838g5fh1g6m9g43z676sm6p5v6k4v485x0l")))

(define-public crate-windows_gen-0.15.5 (c (n "windows_gen") (v "0.15.5") (h "1qksqmhpa1s1gi9id8mn74cbgjymsr5lc8qmapzinc78smlkkjl6")))

(define-public crate-windows_gen-0.15.6 (c (n "windows_gen") (v "0.15.6") (h "04hvq6p5s603n9qj5ifzllkhcvdlcgn3bfl9y84ls1m1kxlrhwkz")))

(define-public crate-windows_gen-0.15.7 (c (n "windows_gen") (v "0.15.7") (h "1agbklqx250xizbx0x4dd25gfdyndzbl27bpsxp5m8n668rjgmgp")))

(define-public crate-windows_gen-0.16.0 (c (n "windows_gen") (v "0.16.0") (h "1s4giyxmm3xbq8ag0qsfpgd0kq3r6p0xfbdsb4h7qmffandsss74")))

(define-public crate-windows_gen-0.17.0 (c (n "windows_gen") (v "0.17.0") (h "1dlcag169ahp80xsv2nmrkd11sq5m89rj35z1zdbkiagbdspdspy")))

(define-public crate-windows_gen-0.17.1 (c (n "windows_gen") (v "0.17.1") (h "1abz5flikajrwpwiqic9jiqpnwh17rq4fkl27ci4355hk3p7ywv1")))

(define-public crate-windows_gen-0.17.2 (c (n "windows_gen") (v "0.17.2") (h "0zk0w3l84wnlzmfp94cb3zhp5qhcv5k84097rmglpj2bn37zmxap")))

(define-public crate-windows_gen-0.18.0 (c (n "windows_gen") (v "0.18.0") (h "078j868r76mr6wnix8sq60bgvxqma19qwdav0chy0gj2vhi36n6g") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.19.0 (c (n "windows_gen") (v "0.19.0") (d (list (d (n "quote") (r "^0.19.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.19.0") (d #t) (k 0) (p "windows_reader")))) (h "0pbyiabf73x77j61hf4jxpwwxh2c9dazz9kj5cih3s7miwdv4yxc") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.20.0 (c (n "windows_gen") (v "0.20.0") (d (list (d (n "quote") (r "^0.20.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.20.0") (d #t) (k 0) (p "windows_reader")))) (h "0i51kprwvnbzss6zavll585z9dldk28f3jq4i1hfq4nlx9zy24vj") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.20.1 (c (n "windows_gen") (v "0.20.1") (d (list (d (n "quote") (r "^0.20.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.20.1") (d #t) (k 0) (p "windows_reader")))) (h "0drg59xlf9a4mdm2azacr703bii15lmianyx1h7gr5nykcc4mr2b") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.21.0 (c (n "windows_gen") (v "0.21.0") (d (list (d (n "quote") (r "^0.21.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.21.0") (d #t) (k 0) (p "windows_reader")))) (h "04n41cs4zf5pcfg57njcvp8fi2zd4v4wd7pm9xsim5g6d118qqh2") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.21.1 (c (n "windows_gen") (v "0.21.1") (d (list (d (n "quote") (r "^0.21.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.21.1") (d #t) (k 0) (p "windows_reader")))) (h "04m9iahxf6cl01k2mwsp5mki0d3hsq3q8q6cg1vki0c45zs98sby") (f (quote (("raw_dylib"))))))

(define-public crate-windows_gen-0.22.0 (c (n "windows_gen") (v "0.22.0") (d (list (d (n "quote") (r "^0.22.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.22.0") (d #t) (k 0) (p "windows_reader")))) (h "0nb0yx21sd121hnybwjk5n4l9y1fih7k8h1jr796cjdy51nf3zy4")))

(define-public crate-windows_gen-0.22.1 (c (n "windows_gen") (v "0.22.1") (d (list (d (n "quote") (r "^0.22.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.22.1") (d #t) (k 0) (p "windows_reader")))) (h "0qzv9y8z0cnfd7h6hqcr0mzhsylpnlsibngcxyvc5873knazspjw")))

(define-public crate-windows_gen-0.23.0 (c (n "windows_gen") (v "0.23.0") (d (list (d (n "quote") (r "^0.23.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.23.0") (d #t) (k 0) (p "windows_reader")))) (h "00js637m2x7zv1zypr3vjcicwlxrhdrn3g17xwpgv49fq4d096yx")))

(define-public crate-windows_gen-0.24.0 (c (n "windows_gen") (v "0.24.0") (d (list (d (n "quote") (r "^0.24.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.24.0") (d #t) (k 0) (p "windows_reader")))) (h "0l31985hw8p4nwrrq8zlrqyw3rcnjvdc2fcs8w2lwi108l63z2b7")))

(define-public crate-windows_gen-0.25.0 (c (n "windows_gen") (v "0.25.0") (d (list (d (n "quote") (r "^0.25.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.25.0") (d #t) (k 0) (p "windows_reader")))) (h "077y8rr82b6nb0f9p2vkc60shc3hgj0i9xz05pwj81wm1vjg1q2l")))

(define-public crate-windows_gen-0.26.0 (c (n "windows_gen") (v "0.26.0") (d (list (d (n "quote") (r "^0.26.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.26.0") (d #t) (k 0) (p "windows_reader")))) (h "113sjdmmwhq72bn9xk2mw4gzv7jl428vqk9jy4djwj3jpglgfdpg")))

(define-public crate-windows_gen-0.27.0 (c (n "windows_gen") (v "0.27.0") (d (list (d (n "quote") (r "^0.27.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.27.0") (d #t) (k 0) (p "windows_reader")))) (h "0rdas8pivriyi5ipfw62w4gc9b808swi397rh3drmna896pha5nr")))

(define-public crate-windows_gen-0.28.0 (c (n "windows_gen") (v "0.28.0") (d (list (d (n "quote") (r "^0.28.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.28.0") (d #t) (k 0) (p "windows_reader")))) (h "0v86y70991pq1b241hp380x0r3n1rxrw06yaiqzpykm3ag6zfdbh")))

(define-public crate-windows_gen-0.29.0 (c (n "windows_gen") (v "0.29.0") (d (list (d (n "quote") (r "^0.29.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.29.0") (d #t) (k 0) (p "windows_reader")))) (h "02yz65cld8w4xl8bq4bmb345h7zcf07qajk0pc8rj0hsyjgbd7p5")))

(define-public crate-windows_gen-0.30.0 (c (n "windows_gen") (v "0.30.0") (d (list (d (n "quote") (r "^0.30.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.30.0") (d #t) (k 0) (p "windows_reader")))) (h "1h4lpi4v98c418r34njikah1djqkncmnzdllpcl0cli23pcz9prh")))

(define-public crate-windows_gen-0.31.0 (c (n "windows_gen") (v "0.31.0") (h "1dx3nw8hwz40anx706s2w012y1zxxysv3xshy9ypgncfh3n8p5bq")))

