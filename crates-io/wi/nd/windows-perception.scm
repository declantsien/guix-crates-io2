(define-module (crates-io wi nd windows-perception) #:use-module (crates-io))

(define-public crate-windows-perception-0.7.0 (c (n "windows-perception") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "040pl8zhcxfjwxrg4id47g7lbd2n3d3dgslhy65c06szz9mh6i76")))

(define-public crate-windows-perception-0.22.0 (c (n "windows-perception") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "10vwjvrcwa07irdzwass6hqp2l8gyzq5y1q94wg6gvq6p16dvh4m")))

(define-public crate-windows-perception-0.22.1 (c (n "windows-perception") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "1qa7n8ggaw4ardsd03gdx8zd3956gm7wjq13h2aj1hpv7mw5njh7")))

(define-public crate-windows-perception-0.22.2 (c (n "windows-perception") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "1qj10xsm0r22ghh48br6724sijz30xrcq9fnpnqydjrw6drrv6wb")))

(define-public crate-windows-perception-0.22.3 (c (n "windows-perception") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "19y3snvrcn0xy482x8skd4ml031pgi2i6gnyj08g4ivfmq0dajip")))

(define-public crate-windows-perception-0.22.5 (c (n "windows-perception") (v "0.22.5") (h "1rwgldsbm6f138bd2wv2mdlj2sg2pkkr5kb6g4xz7nzb5pn9jv67") (r "1.46")))

(define-public crate-windows-perception-0.22.6 (c (n "windows-perception") (v "0.22.6") (h "0gqsj0iv1zm274nhzy33lnq23mn4dq4pw0x7cflh5g278xlnl443") (r "1.46")))

(define-public crate-windows-perception-0.23.0 (c (n "windows-perception") (v "0.23.0") (h "1bp1xxw5ja30rdij0zvz2yqvxn0nhzzx09vbcl7921bhj2bya739")))

