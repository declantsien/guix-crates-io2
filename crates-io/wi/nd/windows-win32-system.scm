(define-module (crates-io wi nd windows-win32-system) #:use-module (crates-io))

(define-public crate-windows-win32-system-0.22.3 (c (n "windows-win32-system") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0k3jwbl9gnp1nrwwqsh1y0w8gyk2hk1zxpwp0psg1b33776fhfa5")))

(define-public crate-windows-win32-system-0.22.5 (c (n "windows-win32-system") (v "0.22.5") (h "0m8pc6yrc9n4i37syjqj01jdm1najsxvnsscyqmrsdlwa5jfr4jr") (r "1.46")))

(define-public crate-windows-win32-system-0.22.6 (c (n "windows-win32-system") (v "0.22.6") (h "0nx5knmyfmfz4l6jk5jppz6dzs272ilxg8cdx5ch5866d9xbm1za") (r "1.46")))

(define-public crate-windows-win32-system-0.23.0 (c (n "windows-win32-system") (v "0.23.0") (h "18m529yb1zd5gyr4219k9gk74gv401vxp2sp8mwh960bmdkb4hsx")))

