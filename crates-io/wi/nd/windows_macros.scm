(define-module (crates-io wi nd windows_macros) #:use-module (crates-io))

(define-public crate-windows_macros-0.1.2 (c (n "windows_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen") (r "^0.1.2") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.2") (d #t) (k 0)))) (h "085cigwma61kiig0g9klzmdwa2p64xhm64n1vd9xq3vh9mjhvfcx")))

(define-public crate-windows_macros-0.1.3 (c (n "windows_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen") (r "^0.1.3") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.3") (d #t) (k 0)))) (h "0ilgjqzkg21gh96qjmjyy9g2b4c2lhkawgnxl45ixm6k2gyx2gmv")))

(define-public crate-windows_macros-0.1.4 (c (n "windows_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen") (r "^0.1.4") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.1.4") (d #t) (k 0)))) (h "1xpdx4jrz234qd0f0ri8zqn5yy905pp2ppfimbniv15w6qzvjc9c")))

(define-public crate-windows_macros-0.2.1 (c (n "windows_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen") (r "^0.2.1") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.2.1") (d #t) (k 0)))) (h "1izhjwd28bg9cyzqxr1rgh2f1fnl1v299hgr8c53f4c4cl9crvda")))

(define-public crate-windows_macros-0.3.1 (c (n "windows_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "windows_gen") (r "^0.3.1") (d #t) (k 0)) (d (n "windows_winmd") (r "^0.3.1") (d #t) (k 0)))) (h "1yz1628cypzn8zvls2p5i4s1d44zznq5lhivhb3hdhgf8k4mgp4y")))

(define-public crate-windows_macros-0.4.0 (c (n "windows_macros") (v "0.4.0") (d (list (d (n "gen") (r "^0.4.0") (d #t) (k 0) (p "windows_gen")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mm8dln76ga0sypj9dy095sc0cb82jkagsimpwcfq1sn4irbirc7")))

(define-public crate-windows_macros-0.5.0 (c (n "windows_macros") (v "0.5.0") (d (list (d (n "gen") (r "^0.5.0") (d #t) (k 0) (p "windows_gen")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09lphvirmq2616dnb5cjfky356h5cvcp97spsj9snwg6a15pf4c4")))

(define-public crate-windows_macros-0.6.0 (c (n "windows_macros") (v "0.6.0") (d (list (d (n "gen") (r "^0.6.0") (d #t) (k 0) (p "windows_gen")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lnbm5cybl66nirdbln9z3av4js0qg3sgs5b5blf8agb20p6vb90")))

(define-public crate-windows_macros-0.7.0 (c (n "windows_macros") (v "0.7.0") (d (list (d (n "gen") (r "^0.7.0") (d #t) (k 0) (p "windows_gen")) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "squote") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing"))) (k 0)))) (h "1j4f94x6g25qm7gdykzxzq9vyykvp1rxr9aia20c0y8nw57wbmy9")))

(define-public crate-windows_macros-0.8.0 (c (n "windows_macros") (v "0.8.0") (d (list (d (n "gen") (r "^0.8.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing"))) (k 0)))) (h "02227v926na9crfa32b2n827gd839fdvm638iz6qhjkn28zibsr5")))

(define-public crate-windows_macros-0.9.0 (c (n "windows_macros") (v "0.9.0") (d (list (d (n "gen") (r "^0.9.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing"))) (k 0)))) (h "1zqhjhd7maqdcr69plhgxpvmc04985sv5kynwr7adym8zzsbcsi9")))

(define-public crate-windows_macros-0.9.1 (c (n "windows_macros") (v "0.9.1") (d (list (d (n "gen") (r "^0.9.1") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing"))) (k 0)))) (h "0xivsg3lf023hs83xiab2k40fmrl11nbihcdrdkc8pc4ab398xqg")))

(define-public crate-windows_macros-0.10.0 (c (n "windows_macros") (v "0.10.0") (d (list (d (n "gen") (r "^0.10.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "14s8aas3bfq3cmf4g8agzwj0wxvw7byg1c49kz73mww1bxkffmzp")))

(define-public crate-windows_macros-0.11.0 (c (n "windows_macros") (v "0.11.0") (d (list (d (n "gen") (r "^0.11.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0zsqnq8pkf0hj8byllvdszzsg43m3gnmvkxkyfxr4mxa0hp99rms")))

(define-public crate-windows_macros-0.12.0 (c (n "windows_macros") (v "0.12.0") (d (list (d (n "gen") (r "^0.12.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1ijcpgxz94rk3mk8q2qhks3bfip56lpbrqssx5ndwgil5r5xmyyn")))

(define-public crate-windows_macros-0.13.0 (c (n "windows_macros") (v "0.13.0") (d (list (d (n "gen") (r "^0.13.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0wxrskay01z38sjjrpnf0ps9skwk6qkqgv70jmnj51xszlk9kk8j")))

(define-public crate-windows_macros-0.14.0 (c (n "windows_macros") (v "0.14.0") (d (list (d (n "gen") (r "^0.14.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1clj6w10mx0piqxf282am16cjj1miiwm15b6r3h3qj4z57qi8wrv")))

(define-public crate-windows_macros-0.14.1 (c (n "windows_macros") (v "0.14.1") (d (list (d (n "gen") (r "^0.14.1") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0fnaq2cjhipdicsy8csjf8dlhxz733bzvjyy1iwaf3zrikmg5d1m")))

(define-public crate-windows_macros-0.15.0 (c (n "windows_macros") (v "0.15.0") (d (list (d (n "gen") (r "^0.15.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0z8zia0cbcdgbdm1wcp2375z6ci35b4ywskawf5vb38lds2bfqv3")))

(define-public crate-windows_macros-0.15.1 (c (n "windows_macros") (v "0.15.1") (d (list (d (n "gen") (r "^0.15.1") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1mzvrw3nccihwidvp9s17w9lcmsv45m2z3xrja4b25iq7sbkc0n7")))

(define-public crate-windows_macros-0.15.2 (c (n "windows_macros") (v "0.15.2") (d (list (d (n "gen") (r "^0.15.2") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0xix65zjpky3k4m69d4f2zhagn1n76zrwiw1rwa6jk6w53if9jlb")))

(define-public crate-windows_macros-0.15.3 (c (n "windows_macros") (v "0.15.3") (d (list (d (n "gen") (r "^0.15.3") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "11rvxncq805dz8rya8gri0m8vq4xgi9p262kamd68nmchhyv8q9g")))

(define-public crate-windows_macros-0.15.4 (c (n "windows_macros") (v "0.15.4") (d (list (d (n "gen") (r "^0.15.4") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0qbyxpzz11yqjc0wvkk34mgrnhv9lzxdrvqg54mf18xr00649i1m")))

(define-public crate-windows_macros-0.15.5 (c (n "windows_macros") (v "0.15.5") (d (list (d (n "gen") (r "^0.15.5") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0cpcgq8pqs9kalhyvlrsszb7w5r5flccvl43bz86fa77xmqi5jzn")))

(define-public crate-windows_macros-0.15.6 (c (n "windows_macros") (v "0.15.6") (d (list (d (n "gen") (r "^0.15.6") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1w2y5nfmk68q8qw3f2ap4f57d81f8zwnjggmsnqdjawnm8768ar5")))

(define-public crate-windows_macros-0.15.7 (c (n "windows_macros") (v "0.15.7") (d (list (d (n "gen") (r "^0.15.7") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1582vli9xni3dkgnafv5pa7iwgbpy08z4m1dqk6mjs3c4hwc7s9d")))

(define-public crate-windows_macros-0.16.0 (c (n "windows_macros") (v "0.16.0") (d (list (d (n "gen") (r "^0.16.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "15qkkjwa1p0z21bqylczvpy3l5iac4h7c1imjga8jqk5c62xvs4h")))

(define-public crate-windows_macros-0.17.0 (c (n "windows_macros") (v "0.17.0") (d (list (d (n "gen") (r "^0.17.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1qpjkc7jdf71fa0jdk9nsb9nwbwkxjxcndsizjs90g0iswxp0b8x")))

(define-public crate-windows_macros-0.17.1 (c (n "windows_macros") (v "0.17.1") (d (list (d (n "gen") (r "^0.17.1") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1ydl1qyrh2dvl08448h9ljab0n5mp1k7wxr4djjhidajdkysaypc")))

(define-public crate-windows_macros-0.17.2 (c (n "windows_macros") (v "0.17.2") (d (list (d (n "gen") (r "^0.17.2") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "16fjjbqwyrq5163sicma3xsrwr7li51jw3579w554ql26wy7ackw")))

(define-public crate-windows_macros-0.18.0 (c (n "windows_macros") (v "0.18.0") (d (list (d (n "gen") (r "^0.18.0") (d #t) (k 0) (p "windows_gen")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1wwqr7f4yj2krz1xjnqawq1zdhd16zjn3lbvk7w0gxz96a4gpb2q")))

(define-public crate-windows_macros-0.19.0 (c (n "windows_macros") (v "0.19.0") (d (list (d (n "gen") (r "^0.19.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.19.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.19.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0bs4w2vf0zw12lxqzixzmimg78rk2azqi9llj159wxhq272vhrjm")))

(define-public crate-windows_macros-0.20.0 (c (n "windows_macros") (v "0.20.0") (d (list (d (n "gen") (r "^0.20.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.20.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.20.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1mimmnsnvsjabwghpckbnrrx5l7sxn9c6nfbm5kd9ydrxphmc6k6")))

(define-public crate-windows_macros-0.20.1 (c (n "windows_macros") (v "0.20.1") (d (list (d (n "gen") (r "^0.20.1") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.20.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.20.1") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1phyf0mis66k4l8k971srj3mbkgzn8n3lf42la6mpm23ib77h7fc")))

(define-public crate-windows_macros-0.21.0 (c (n "windows_macros") (v "0.21.0") (d (list (d (n "gen") (r "^0.21.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.21.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.21.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1pivapn84i8rp0kw6wdiaqk4k4y2zvnmd6xw5szpixrpcynxfj9y")))

(define-public crate-windows_macros-0.21.1 (c (n "windows_macros") (v "0.21.1") (d (list (d (n "gen") (r "^0.21.1") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.21.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.21.1") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0g7wghy4k66awap3cknvx95zb00z0436s3nbavq1jg5hn5bj7k41")))

(define-public crate-windows_macros-0.22.0 (c (n "windows_macros") (v "0.22.0") (d (list (d (n "gen") (r "^0.22.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.22.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.22.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "15hqqz2kmxqwrg6qs35l18a6079q2fzw8y75j80x1iw9l076hzfx")))

(define-public crate-windows_macros-0.22.1 (c (n "windows_macros") (v "0.22.1") (d (list (d (n "gen") (r "^0.22.1") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.22.1") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.22.1") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1klai2wdh813sxmclb5g8psy7abnks7j1w025jzmp55r2fgwdhp6")))

(define-public crate-windows_macros-0.23.0 (c (n "windows_macros") (v "0.23.0") (d (list (d (n "gen") (r "^0.23.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.23.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.23.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "084d2q8gl2j918ha0g28nkbxnqc014hf0isbq3gvvkckb5ipd8r0")))

(define-public crate-windows_macros-0.24.0 (c (n "windows_macros") (v "0.24.0") (d (list (d (n "gen") (r "^0.24.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.24.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.24.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "13n5k0wbhqhl7f737f51wvwl2d2h2vf1kk0pmh7v4iia4qq7lr1f")))

(define-public crate-windows_macros-0.25.0 (c (n "windows_macros") (v "0.25.0") (d (list (d (n "gen") (r "^0.25.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.25.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.25.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0fwcgi805h7j38q6fx8x9fpyyd5blbc6fbcay3v6cv9rlzqvq0v1")))

(define-public crate-windows_macros-0.26.0 (c (n "windows_macros") (v "0.26.0") (d (list (d (n "gen") (r "^0.26.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.26.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.26.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "08plhlajjdnm805z734arzcrxx0x5bw4vb986hwwihd6dfy9264g")))

(define-public crate-windows_macros-0.27.0 (c (n "windows_macros") (v "0.27.0") (d (list (d (n "gen") (r "^0.27.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.27.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.27.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0sz3pyn34hsqf99qi0i10b29wpa8ja6jlhrb7kfmfkrna7b8snd2")))

(define-public crate-windows_macros-0.28.0 (c (n "windows_macros") (v "0.28.0") (d (list (d (n "gen") (r "^0.28.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.28.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.28.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1cbqlrv1295h56h167bh16qfw1w5lbr12di2skqd04v8zp2ah8w2")))

(define-public crate-windows_macros-0.29.0 (c (n "windows_macros") (v "0.29.0") (d (list (d (n "gen") (r "^0.29.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.29.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.29.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "18rrxxypm1gcj8mca461iv5xr07ng3p83z679hgyj33n3zvl6r3g")))

(define-public crate-windows_macros-0.30.0 (c (n "windows_macros") (v "0.30.0") (d (list (d (n "gen") (r "^0.30.0") (d #t) (k 0) (p "windows_gen")) (d (n "quote") (r "^0.30.0") (d #t) (k 0) (p "windows_quote")) (d (n "reader") (r "^0.30.0") (d #t) (k 0) (p "windows_reader")) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1lr5p3xc6b0l943fk6x0215id83w48nxb68df7z0b43yj6ml9bk2")))

(define-public crate-windows_macros-0.31.0 (c (n "windows_macros") (v "0.31.0") (h "08nxiqj2bdv7d0wmp48iyagjxxvaad9ki0zv283hrfnyy3b73ggj")))

