(define-module (crates-io wi nd window) #:use-module (crates-io))

(define-public crate-window-0.1.0 (c (n "window") (v "0.1.0") (h "1hqfycv01pzba2dg3azigyh5234p3q5cgxlmmpyd87rmlnmq3c49")))

(define-public crate-window-0.2.0 (c (n "window") (v "0.2.0") (h "1wbj330k6ivfxmymnvgf0pcc3fgms6jfxlhyi9my2rqkig1rm6nk")))

(define-public crate-window-0.3.0 (c (n "window") (v "0.3.0") (h "0mab6bi4h4fy3k5jsvc5cnfigc23nqladklhmcs3769qypwmycfw")))

(define-public crate-window-0.3.1 (c (n "window") (v "0.3.1") (h "19yg9y8baai04z7b14wlj3cdbad9jm1wxh3g424lj4p1ypdf5i2p")))

(define-public crate-window-0.4.0 (c (n "window") (v "0.4.0") (d (list (d (n "dl_api") (r "^0.4") (d #t) (k 0)) (d (n "human") (r "^0.1") (d #t) (k 0)) (d (n "pasts") (r "^0.4") (d #t) (k 0)))) (h "0r3kvhjwsy0jzkanjkknam61lrmfknsg53g78rz7ph2cnaf8vrds") (f (quote (("gpu-debugging") ("default"))))))

(define-public crate-window-0.4.1 (c (n "window") (v "0.4.1") (d (list (d (n "dl_api") (r "^0.4") (d #t) (k 0)) (d (n "human") (r "^0.1") (d #t) (k 0)) (d (n "pasts") (r "^0.5") (d #t) (k 0)))) (h "0qv92fgkvx1km5pp40glddxdmngmcq1bhd0pd4dnp6rd450yqh4s") (f (quote (("gpu-debugging") ("default"))))))

(define-public crate-window-0.5.0 (c (n "window") (v "0.5.0") (d (list (d (n "dl_api") (r "^0.4") (d #t) (k 0)) (d (n "human") (r "^0.2") (d #t) (k 0)))) (h "0jah4gigbl1zjrwrcl7n6zhr3q5hiw11dwlgayhds77a3yblw0lp") (f (quote (("gpu-debugging") ("default"))))))

