(define-module (crates-io wi nd windi) #:use-module (crates-io))

(define-public crate-windi-0.1.0 (c (n "windi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libwindi") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fvp0yq3hlw0idjdvjxq2q07a67z2h0sfbhzn55sdxj06qx87sa4")))

