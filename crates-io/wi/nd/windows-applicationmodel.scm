(define-module (crates-io wi nd windows-applicationmodel) #:use-module (crates-io))

(define-public crate-windows-applicationmodel-0.7.0 (c (n "windows-applicationmodel") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1i054mg4f2l5zvmk5rfj33c6pbvllyhx9qzcxbf6dzmz31z1nard") (y #t)))

(define-public crate-windows-applicationmodel-0.22.0 (c (n "windows-applicationmodel") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1qvx2j2ybh4x043kf2ryd40mb8h05lbvmy5s646g260v2q5zlhr0") (y #t)))

(define-public crate-windows-applicationmodel-0.22.1 (c (n "windows-applicationmodel") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-management") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui-xaml") (r "^0.22.1") (d #t) (k 0)))) (h "1lal0bvnclmip9micdjbln030jrdg4n7di27swznjwnnxwlg0ijh") (y #t)))

(define-public crate-windows-applicationmodel-0.22.2 (c (n "windows-applicationmodel") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-management") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui-xaml") (r "^0.22.2") (d #t) (k 0)))) (h "16sfwy53y2fr920lzhvsfi8y5jx1786jqpdn3j299lwnl7i5h74i") (y #t)))

(define-public crate-windows-applicationmodel-0.22.3 (c (n "windows-applicationmodel") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0bm3q07r99rmcfa7qzx6a1hi6ndcdzm36k0rq5dyj1gxab5wah6c") (y #t)))

(define-public crate-windows-applicationmodel-0.22.4 (c (n "windows-applicationmodel") (v "0.22.4") (h "1xdkdax4dklzlwp5akcln1xdyn2y8lkjrj0xxny7qlvyplrni836") (r "1.46")))

(define-public crate-windows-applicationmodel-0.22.5 (c (n "windows-applicationmodel") (v "0.22.5") (h "0mg0pm8ivv89rqbd47wvk8hbnjshya9psdb3cg6kyflbp7yfzina") (r "1.46")))

(define-public crate-windows-applicationmodel-0.22.6 (c (n "windows-applicationmodel") (v "0.22.6") (h "1a7mc3vc6cc2zya85rx3km1l3898iqplcs5gnp4rrpvibhh45y4r") (r "1.46")))

(define-public crate-windows-applicationmodel-0.1.0 (c (n "windows-applicationmodel") (v "0.1.0") (h "1h46qxw1n3ri6qlcf8cjxbvc49ywc9lr0w380sa4ikasshbmc7dj")))

(define-public crate-windows-applicationmodel-0.23.0 (c (n "windows-applicationmodel") (v "0.23.0") (h "0ggq3wmkf2z7bs584v2la6bg14q73asdg7zbjjd67i696j9vrpn3")))

