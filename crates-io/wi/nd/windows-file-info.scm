(define-module (crates-io wi nd windows-file-info) #:use-module (crates-io))

(define-public crate-windows-file-info-0.1.0 (c (n "windows-file-info") (v "0.1.0") (h "0jhc47k0npza147h3ici9g8mdkpqijkrap36nb1lviv3ms3651b5")))

(define-public crate-windows-file-info-0.2.0 (c (n "windows-file-info") (v "0.2.0") (h "19mnha2sxlxx6zbc21aw54al4fl9xw51srkfvwxxy489jrpz2wlx")))

(define-public crate-windows-file-info-0.3.0 (c (n "windows-file-info") (v "0.3.0") (h "0s3qss78pg94ib7m7rgqs3lrifapw66pscb3mii65shp0gi9paf3")))

