(define-module (crates-io wi nd windows-storage-sys) #:use-module (crates-io))

(define-public crate-windows-storage-sys-0.0.2 (c (n "windows-storage-sys") (v "0.0.2") (h "1a0h6mq93wp16l7kfr6jh2zncpwaimvnjwlbcghnnlaphv0kilr5") (r "1.46")))

(define-public crate-windows-storage-sys-0.23.0 (c (n "windows-storage-sys") (v "0.23.0") (h "09q884bxjbclnxf3vmn3bjhrlbifjp1wjp01hv8rd1w0ln1gl0mr")))

