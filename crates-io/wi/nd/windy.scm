(define-module (crates-io wi nd windy) #:use-module (crates-io))

(define-public crate-windy-0.1.0 (c (n "windy") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ywq2ypdhm42wldvl08l6l2yww7js46cc7j0y9cd4zkd66yfv1xb") (f (quote (("debug_insufficient_buffer"))))))

(define-public crate-windy-0.1.1 (c (n "windy") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19bv613k32p79f2jlfxjsk8g6pzvix46msxp18qs4b3z524cafzj") (f (quote (("debug_insufficient_buffer"))))))

(define-public crate-windy-0.1.2 (c (n "windy") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "01q6l15r3z38akjydb3qdknrd63mjji7n47wp7dz1gp064js911y") (f (quote (("no_std") ("debug_insufficient_buffer"))))))

(define-public crate-windy-0.1.3 (c (n "windy") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "windy-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0qan1pg1r1v60pr4hax1ahswgfgqx36r8k3wa2imi55n1krcgqlq") (f (quote (("no_std") ("macros" "windy-macros") ("debug_insufficient_buffer"))))))

(define-public crate-windy-0.1.4 (c (n "windy") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "windy-macros") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "09w5ccb300r4axfpb696i0snclk6f3lrlyy23rk1kcsrjnssnpa6") (f (quote (("std") ("macros" "windy-macros") ("default" "std") ("debug_insufficient_buffer"))))))

(define-public crate-windy-0.2.0 (c (n "windy") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0sl0dlp93vlxz356d46s250dhgy21qidr8malg73kr27cfr5vd3d") (f (quote (("std") ("default" "std") ("debug_insufficient_buffer"))))))

