(define-module (crates-io wi nd windows-sys-win32-devices) #:use-module (crates-io))

(define-public crate-windows-sys-win32-devices-0.22.5 (c (n "windows-sys-win32-devices") (v "0.22.5") (h "1v7plz92mvk2wz5ljjy20d7anhr2zajzfnvh3wby7kkpjlj0b2k9") (r "1.46")))

(define-public crate-windows-sys-win32-devices-0.22.6 (c (n "windows-sys-win32-devices") (v "0.22.6") (h "0zvzg24aw61fxn9qvhl530vk4i8sgwp30652g7sqs2l2701q7pk2") (r "1.46")))

(define-public crate-windows-sys-win32-devices-0.23.0 (c (n "windows-sys-win32-devices") (v "0.23.0") (h "0gadg1jjnvlb55jqi9kr6y2i9vlyy0q482padlrn559iw74jf23d")))

