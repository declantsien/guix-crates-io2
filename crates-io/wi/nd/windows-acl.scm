(define-module (crates-io wi nd windows-acl) #:use-module (crates-io))

(define-public crate-windows-acl-0.1.0 (c (n "windows-acl") (v "0.1.0") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "185g61i598ply4vl1v6b589kpniscz4zm9r3sa092r0gwfd310m0")))

(define-public crate-windows-acl-0.2.0 (c (n "windows-acl") (v "0.2.0") (d (list (d (n "field-offset") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "0kch4cvp3qi89lyjmvg6ppnw9653r73y2ijwm9027lxn7n7y59a9")))

(define-public crate-windows-acl-0.2.1 (c (n "windows-acl") (v "0.2.1") (d (list (d (n "field-offset") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "1pwbjk64k69rp5lg77frp4350m9rpar82v2ndik7c7s0lc2dhyv7")))

(define-public crate-windows-acl-0.2.2-rc.1 (c (n "windows-acl") (v "0.2.2-rc.1") (d (list (d (n "field-offset") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "0ap1v9fwyxahfq58rc2gjpxbzgavbmymqjgn3n8wpvdljpj8xj4g")))

(define-public crate-windows-acl-0.2.2-rc.2 (c (n "windows-acl") (v "0.2.2-rc.2") (d (list (d (n "field-offset") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "0x427hlzl1gvnjqyvw93qx0n3wdl4lppycl5a6s8qizd9jll9cm4")))

(define-public crate-windows-acl-0.2.2 (c (n "windows-acl") (v "0.2.2") (d (list (d (n "field-offset") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "1d5685y3k2md410bskg2b9hkrp468dmn75hd7xxbv93rgdsbc7k2")))

(define-public crate-windows-acl-0.3.0 (c (n "windows-acl") (v "0.3.0") (d (list (d (n "field-offset") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.5") (f (quote ("aclapi" "errhandlingapi" "handleapi" "sddl" "securitybaseapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "1hyfng4dagfndxpwxynjk9zird8lhrp7zrsqc1h4rjvbk0iifyqp")))

