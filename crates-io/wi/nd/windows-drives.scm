(define-module (crates-io wi nd windows-drives) #:use-module (crates-io))

(define-public crate-windows-drives-0.1.0 (c (n "windows-drives") (v "0.1.0") (d (list (d (n "gptman") (r "^0.7") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "0m3wm21cf91f9j5xvzpbb1r1xmz1hlgymji7id3l4p3lzdmakky0")))

(define-public crate-windows-drives-0.2.0 (c (n "windows-drives") (v "0.2.0") (d (list (d (n "gptman") (r "^0.7") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "01x29q2hamhqbrlyawxs40c3picrda18k96ap7pjawa4pq3ppq66")))

(define-public crate-windows-drives-0.2.1 (c (n "windows-drives") (v "0.2.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "1jhyirwq9wsqs4vplgvrxcghbs693l6f4mn9r178b50fa2k35006")))

(define-public crate-windows-drives-0.3.0 (c (n "windows-drives") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "1vh5ysib3jmfjax3bkvl6aj5y2ssxpr59lh27vj0agml472bwm6w")))

(define-public crate-windows-drives-0.3.1 (c (n "windows-drives") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "1pgfbgwz8l0zhsw09gczyvd1xv36sxmqzv7an92apsrzf0krnlf4")))

(define-public crate-windows-drives-0.4.0 (c (n "windows-drives") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "0f8ssdksnap1nz0af7lm8ksly8y1wr23m1i6khlmx64cx94h4x9w")))

(define-public crate-windows-drives-0.4.1 (c (n "windows-drives") (v "0.4.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "0r8hzplnk516ja0v2vhca3ds183b2syxr487aj9ly5nlk75iwqd3")))

(define-public crate-windows-drives-0.4.2 (c (n "windows-drives") (v "0.4.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "0zr2p9b7676a8aw76pz8q4vqagmh7njv6106gfg235kv524hfmjs")))

(define-public crate-windows-drives-0.4.3 (c (n "windows-drives") (v "0.4.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "1nh677cbbnykm2v1lq1h2sm4002d5yr3df5cjl33zwyzval6afcr")))

(define-public crate-windows-drives-0.5.0 (c (n "windows-drives") (v "0.5.0") (d (list (d (n "derive-try-from-primitive") (r "^1.0.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "fileapi" "handleapi" "impl-default" "ioapiset" "minwinbase" "ntdef" "winbase" "winioctl" "winnt"))) (d #t) (k 0)))) (h "0jy7lip5wv5vqxqls2chh0kznq5k4hrrjci7x9417ga5k773zkv0")))

