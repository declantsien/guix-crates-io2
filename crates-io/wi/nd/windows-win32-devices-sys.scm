(define-module (crates-io wi nd windows-win32-devices-sys) #:use-module (crates-io))

(define-public crate-windows-win32-devices-sys-0.0.2 (c (n "windows-win32-devices-sys") (v "0.0.2") (h "0pw9j17f7kr1pmjfshwd2zkypgys3fr1r0vmi637b90fn72kawc9") (r "1.46")))

(define-public crate-windows-win32-devices-sys-0.23.0 (c (n "windows-win32-devices-sys") (v "0.23.0") (h "0hbpkk0jbjb0ia25dwp3b4mpy9rfr1sa0m9vsw4s9s5lsbabcl44")))

