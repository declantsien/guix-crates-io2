(define-module (crates-io wi nd windows-sys-embedded) #:use-module (crates-io))

(define-public crate-windows-sys-embedded-0.22.4 (c (n "windows-sys-embedded") (v "0.22.4") (h "0y7v3ss3dvqa29426g18mcs3yl44pq4l987v74l1l37s0v7y39vf") (r "1.46")))

(define-public crate-windows-sys-embedded-0.22.5 (c (n "windows-sys-embedded") (v "0.22.5") (h "1jl45iy9zihd0ad6wkfcyj9693sz6rlapd5g1jqv769yr4knr9pg") (r "1.46")))

(define-public crate-windows-sys-embedded-0.22.6 (c (n "windows-sys-embedded") (v "0.22.6") (h "1cixxjy34d5whkpm5j30z1zcm1x39yvqydw45b111v91sl5hhf85") (r "1.46")))

(define-public crate-windows-sys-embedded-0.23.0 (c (n "windows-sys-embedded") (v "0.23.0") (h "04zbchwrs6gm7h1wsk2d2xhkxzsiam4mxm52bc5kg6ys7vs6f03i")))

