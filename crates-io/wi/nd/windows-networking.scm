(define-module (crates-io wi nd windows-networking) #:use-module (crates-io))

(define-public crate-windows-networking-0.7.0 (c (n "windows-networking") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1q94dhgcgqjarfbkdz8nrzjyzpkh85yp1bvlx8h8j9qv7n6plr45")))

(define-public crate-windows-networking-0.22.0 (c (n "windows-networking") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0saal9r6nyqkvn119anngidkgkqf6zy7nm5jk8d5rr0g0sfaq771")))

(define-public crate-windows-networking-0.22.1 (c (n "windows-networking") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "0ci7a0asm1plpkdcq2aybby73fijqa8b5ibf7kg0bymx9d8y71ls")))

(define-public crate-windows-networking-0.22.2 (c (n "windows-networking") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "1ayrvmwxb5iw7h8g1nnjysqh1g5xwqv2ymrnr6p0w4418mksypan")))

(define-public crate-windows-networking-0.22.3 (c (n "windows-networking") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1qldw0372i9ang7ck7y424zl84yah5h1rgxxkmibdmkf2pva90jh")))

(define-public crate-windows-networking-0.22.5 (c (n "windows-networking") (v "0.22.5") (h "0dcjbpgah99x8j8zhqidsv01dn1lxi3n7nsbmxy15xlicm31ywcj") (r "1.46")))

(define-public crate-windows-networking-0.22.6 (c (n "windows-networking") (v "0.22.6") (h "10jym2k376l3lv5xzbn1a1ndvcpfgcbwha5kg24x7rk32np65mal") (r "1.46")))

(define-public crate-windows-networking-0.23.0 (c (n "windows-networking") (v "0.23.0") (h "1gg3fjyvv03s7szq7b5385d0r3ad6qkjcw7kryxg939scq4dffym")))

