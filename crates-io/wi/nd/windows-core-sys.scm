(define-module (crates-io wi nd windows-core-sys) #:use-module (crates-io))

(define-public crate-windows-core-sys-0.0.0 (c (n "windows-core-sys") (v "0.0.0") (h "1i5znxsfls594m2xpx05ja1kxhq9jwb1pwfh9p33fl6q45cffb9b") (r "1.46")))

(define-public crate-windows-core-sys-0.1.0 (c (n "windows-core-sys") (v "0.1.0") (h "161c4dxvr17dfb7b0nfi0yppbpfvg0agzf4hrw0kgwqpfs1sl8by")))

(define-public crate-windows-core-sys-0.23.0 (c (n "windows-core-sys") (v "0.23.0") (h "1qgma7cpqyaynrqhbwzmq4454z5rb5x4crnp5klndc09c09356li")))

