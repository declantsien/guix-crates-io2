(define-module (crates-io wi nd windy-macros) #:use-module (crates-io))

(define-public crate-windy-macros-0.1.0 (c (n "windy-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windy") (r "^0.1.2") (d #t) (k 0)))) (h "14l9vd6y0ljpwd1jf7yji176anxf1mqmjvp61rpclycl68ygkhpg")))

(define-public crate-windy-macros-0.1.1 (c (n "windy-macros") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "windy") (r "^0.1") (d #t) (k 0)))) (h "1hgxc4yw9ddjg0rzc36lhy3vzsfcgzxfwikl4110hbnyffvllxb1")))

