(define-module (crates-io wi nd windows-metagen) #:use-module (crates-io))

(define-public crate-windows-metagen-0.0.0 (c (n "windows-metagen") (v "0.0.0") (h "1qma9gaah8kvl7zspykazhwgri518aa5fihl5gfnnk3wag9ra60p")))

(define-public crate-windows-metagen-0.23.0 (c (n "windows-metagen") (v "0.23.0") (h "1b6917c2bp7bp308lm5lzj3p5rhvnqnfz1awgdfqibzvfaa4xf5j")))

