(define-module (crates-io wi nd windows-media) #:use-module (crates-io))

(define-public crate-windows-media-0.7.0 (c (n "windows-media") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0v7arcimfz7v7iam5fkbxlpwr76qqdc70ws6h6jmrm567gsbsfd2")))

(define-public crate-windows-media-0.22.0 (c (n "windows-media") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0yz6wdwqpaxhhr1ym6av3b1nxizpmanipcxxmgqxs07r5v73gp76")))

(define-public crate-windows-media-0.22.1 (c (n "windows-media") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "055dc5xlhj8gby2kj921lkccnv3sr0jb6ssah2n9b37z013ckd71")))

(define-public crate-windows-media-0.22.2 (c (n "windows-media") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "0x0sc58q5yqyzr60w2xyi7bwm0rl3gy3pznrrc4719mjvssa0jsd")))

(define-public crate-windows-media-0.22.3 (c (n "windows-media") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1ynl775mp471pyf93xf31kfr3fxxcxp66jbxiywimd049xz1r0dy")))

(define-public crate-windows-media-0.22.5 (c (n "windows-media") (v "0.22.5") (h "1wb58xbq2f7gwb5ag7ldacjnyrh0j0g98696gxzfzj2yxvjvh6cy") (r "1.46")))

(define-public crate-windows-media-0.22.6 (c (n "windows-media") (v "0.22.6") (h "154y9kaik528cphjh7q8q87wg9n4zkgv2m3yk2wpy610vinixccq") (r "1.46")))

(define-public crate-windows-media-0.23.0 (c (n "windows-media") (v "0.23.0") (h "030m7yy08y8rxrw5v5adypv25y69d33nizdb8rqmsbsww3d2pwwx")))

