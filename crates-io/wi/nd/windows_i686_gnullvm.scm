(define-module (crates-io wi nd windows_i686_gnullvm) #:use-module (crates-io))

(define-public crate-windows_i686_gnullvm-0.0.0 (c (n "windows_i686_gnullvm") (v "0.0.0") (h "03mpqkfp1zbqgiwmg619p18468l4wmnr2zh71d3h40smgbc2j89p") (r "1.56")))

(define-public crate-windows_i686_gnullvm-0.52.5 (c (n "windows_i686_gnullvm") (v "0.52.5") (h "1s9f4gff0cixd86mw3n63rpmsm4pmr4ffndl6s7qa2h35492dx47") (r "1.56")))

