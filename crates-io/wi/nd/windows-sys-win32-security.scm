(define-module (crates-io wi nd windows-sys-win32-security) #:use-module (crates-io))

(define-public crate-windows-sys-win32-security-0.22.5 (c (n "windows-sys-win32-security") (v "0.22.5") (h "04j1q87gd7yz069zybx0xfb1bgd7vfwkyhxllnpz5ldrbabsgv2k") (r "1.46")))

(define-public crate-windows-sys-win32-security-0.22.6 (c (n "windows-sys-win32-security") (v "0.22.6") (h "107xbyd8l8h3gq94ypxx45wrp980d84c3g9qj9j91ypsrfhii803") (r "1.46")))

(define-public crate-windows-sys-win32-security-0.23.0 (c (n "windows-sys-win32-security") (v "0.23.0") (h "0cz5hc6w2a5masxqykkyrm7iw8swg9ff6vk8jvak7zgsqijh4gsm")))

