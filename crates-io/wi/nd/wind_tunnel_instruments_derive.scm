(define-module (crates-io wi nd wind_tunnel_instruments_derive) #:use-module (crates-io))

(define-public crate-wind_tunnel_instruments_derive-0.1.0-alpha.1 (c (n "wind_tunnel_instruments_derive") (v "0.1.0-alpha.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "175g7xwlhy325mm32zvjqlabp5jbl24iyfk0b2a308s124wlxpmd")))

(define-public crate-wind_tunnel_instruments_derive-0.1.0-alpha.2 (c (n "wind_tunnel_instruments_derive") (v "0.1.0-alpha.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3db39zc0zr5klvfs7ljfb3bdzkrdxgwmrxc47435apr6l5yaqj")))

(define-public crate-wind_tunnel_instruments_derive-0.1.0-alpha.3 (c (n "wind_tunnel_instruments_derive") (v "0.1.0-alpha.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1778x23nvqzyf8lxs6kjgnfganqwhbi3723xnikjfr10ky05swbp")))

(define-public crate-wind_tunnel_instruments_derive-0.2.0-alpha.1 (c (n "wind_tunnel_instruments_derive") (v "0.2.0-alpha.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iqzi190dps7akajpx0xf0jb2jbppg0m8yvnkg44c0a3naf9npy2")))

(define-public crate-wind_tunnel_instruments_derive-0.2.0-alpha.2 (c (n "wind_tunnel_instruments_derive") (v "0.2.0-alpha.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17bdiwvcwmhd2xf8pgpz1861ldyx0iimxfrag0lx7n52bqpfca4n")))

