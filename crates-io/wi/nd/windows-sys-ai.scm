(define-module (crates-io wi nd windows-sys-ai) #:use-module (crates-io))

(define-public crate-windows-sys-ai-0.22.4 (c (n "windows-sys-ai") (v "0.22.4") (h "0xk6fnhp85f0bs74gy0ck5z5drzcqljv8jlxm70zs190jq1lgx5w") (r "1.46")))

(define-public crate-windows-sys-ai-0.22.5 (c (n "windows-sys-ai") (v "0.22.5") (h "1in5f2pi2p73yrzi094zkwm9jampakcj8rksfnim46ggrina2r3k") (r "1.46")))

(define-public crate-windows-sys-ai-0.22.6 (c (n "windows-sys-ai") (v "0.22.6") (h "0jbhckrlnk26c6rzcrqns3qjl44pknh0vpj56if6vxc6xrj87rpg") (r "1.46")))

(define-public crate-windows-sys-ai-0.23.0 (c (n "windows-sys-ai") (v "0.23.0") (h "0xazm1yd6fl7yppwyrrhphrqbc1j2f94npdsqax0kqrnxpk6s322")))

