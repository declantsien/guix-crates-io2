(define-module (crates-io wi nd windows-gaming) #:use-module (crates-io))

(define-public crate-windows-gaming-0.7.0 (c (n "windows-gaming") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0p5hdmh3srhp1i8bdpci9ba3fdk8k0j2p4gf5a0py0ivnnclkxq3")))

(define-public crate-windows-gaming-0.22.0 (c (n "windows-gaming") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "05g932q3p8dhd6vwihy1sfh40ziza02bdm2zq38lh7c03hk3fxhb")))

(define-public crate-windows-gaming-0.22.1 (c (n "windows-gaming") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)))) (h "1lwy52jigma93g1mjvd7fwxzh49yf7qmka8pwjqmx3bhl1cq75fv")))

(define-public crate-windows-gaming-0.22.2 (c (n "windows-gaming") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)))) (h "1j5v283q2bzarzyhi4i0fyaclm5yb7mvjy42cdbawnij1ixxd4yd")))

(define-public crate-windows-gaming-0.22.3 (c (n "windows-gaming") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0qy7zjqwfy4xqryzrdzij5f2c26x503bnjc3bml97zzdzk9m4cw8")))

(define-public crate-windows-gaming-0.22.5 (c (n "windows-gaming") (v "0.22.5") (h "151bk8wp9422li8k01shvndpax3bcabz96h66faiiyp2chscpsp9") (r "1.46")))

(define-public crate-windows-gaming-0.22.6 (c (n "windows-gaming") (v "0.22.6") (h "1axw4pnapbps3ncm23zpya840d1qq5gc0k1bh3cipffalpypjjl3") (r "1.46")))

(define-public crate-windows-gaming-0.1.0 (c (n "windows-gaming") (v "0.1.0") (h "0b5v45gip6p9v0hwm1yagajq5wbnzaz44yp260lgrxvin2s78wfb")))

(define-public crate-windows-gaming-0.23.0 (c (n "windows-gaming") (v "0.23.0") (h "1v3m4qn92il8dsa4m01f4h92hrvmnipcblbwhvyj3hi4yvsrban0")))

