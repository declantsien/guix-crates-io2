(define-module (crates-io wi nd windows-applicationmodel-sys) #:use-module (crates-io))

(define-public crate-windows-applicationmodel-sys-0.0.0 (c (n "windows-applicationmodel-sys") (v "0.0.0") (h "0rdx3crhccg80jc911zy2ym9xv5c326anbg64mmhw9hgi3bg65hi") (r "1.46")))

(define-public crate-windows-applicationmodel-sys-0.0.1 (c (n "windows-applicationmodel-sys") (v "0.0.1") (h "05g4p0s2cm380yqg8giil7k1vp08szbxrfk5hs28dwmjqaaqnbaq") (r "1.46")))

(define-public crate-windows-applicationmodel-sys-0.0.2 (c (n "windows-applicationmodel-sys") (v "0.0.2") (h "08ddprwbx3ywqg6mv14b52v2ybzcl3b482a717nfhlr5hl6y1ap7") (r "1.46")))

(define-public crate-windows-applicationmodel-sys-0.1.0 (c (n "windows-applicationmodel-sys") (v "0.1.0") (h "13qanb3dlwar84kqqfrj7f8xcp0c16s8nsz2qxpiy2yj6v7s70qq")))

(define-public crate-windows-applicationmodel-sys-0.23.0 (c (n "windows-applicationmodel-sys") (v "0.23.0") (h "03d58hr80xspyxi7xp4kq28f2pmh1kp4agn1m53n3n9323a7jjlq")))

