(define-module (crates-io wi nd windows-sys-applicationmodel) #:use-module (crates-io))

(define-public crate-windows-sys-applicationmodel-0.22.4 (c (n "windows-sys-applicationmodel") (v "0.22.4") (h "0xkbasbglcd7nyas4g4xlcmx6c3lic6pa2gxhpialhbf1pzjqpm6") (r "1.46")))

(define-public crate-windows-sys-applicationmodel-0.22.5 (c (n "windows-sys-applicationmodel") (v "0.22.5") (h "19j2a86kykca4j0cd4mj2xnhsn8b1qyb23ak69n6nriykynyfbga") (r "1.46")))

(define-public crate-windows-sys-applicationmodel-0.22.6 (c (n "windows-sys-applicationmodel") (v "0.22.6") (h "05b0m0raf7rgkgdz2spjghm6kb2xd9viwkd99lnr5wmflkmjvhgf") (r "1.46")))

(define-public crate-windows-sys-applicationmodel-0.23.0 (c (n "windows-sys-applicationmodel") (v "0.23.0") (h "0r2jgfj1cvdbyxs3r9xz6kjij64kxzclcfrvcxsmzcm2bmycjh5a")))

