(define-module (crates-io wi nd windows-sys-win32-gaming) #:use-module (crates-io))

(define-public crate-windows-sys-win32-gaming-0.22.5 (c (n "windows-sys-win32-gaming") (v "0.22.5") (h "0nyyigyhp8za3gg4qz6fidaqrnqqvl4xaawq4yvp6v0pf471hx8j") (r "1.46")))

(define-public crate-windows-sys-win32-gaming-0.22.6 (c (n "windows-sys-win32-gaming") (v "0.22.6") (h "1pih4bghx28ma2206x1bcrvi0jr4q1s3dq77krkwrvg1x0kac8lb") (r "1.46")))

(define-public crate-windows-sys-win32-gaming-0.23.0 (c (n "windows-sys-win32-gaming") (v "0.23.0") (h "0cln7h01zh2b7gdwmxvih0x4iwa3gn4cpgvahgxdmwq3kc1v44f6")))

