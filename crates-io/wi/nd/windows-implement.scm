(define-module (crates-io wi nd windows-implement) #:use-module (crates-io))

(define-public crate-windows-implement-0.0.0 (c (n "windows-implement") (v "0.0.0") (h "1lwiiipc4ay23swhldbxd194jr13rrx89b5jpm18cw3kifa97pqp")))

(define-public crate-windows-implement-0.31.0 (c (n "windows-implement") (v "0.31.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.31.0") (d #t) (k 0) (p "windows-tokens")))) (h "12lqyw19nvqw77wx5bdgh3zyz02v3ajkq5b4c061yj3440v5p637")))

(define-public crate-windows-implement-0.32.0 (c (n "windows-implement") (v "0.32.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.32.0") (d #t) (k 0) (p "windows-tokens")))) (h "0f3lnjs9rlihin9cjf9y7np1x15c0v09v0cwlw1n7c30145xmciz")))

(define-public crate-windows-implement-0.33.0 (c (n "windows-implement") (v "0.33.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.33.0") (d #t) (k 0) (p "windows-tokens")))) (h "0ak26jwip398n0as697vhr60lkd9yxg4x6yb3jlpkiwfrn33hvbw")))

(define-public crate-windows-implement-0.34.0 (c (n "windows-implement") (v "0.34.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.34.0") (d #t) (k 0) (p "windows-tokens")))) (h "13amcdc4bv01xpb9bq9hyk2zqnmic2kf993m1gbfqdmmn19zykjm")))

(define-public crate-windows-implement-0.35.0 (c (n "windows-implement") (v "0.35.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.35.0") (d #t) (k 0) (p "windows-tokens")))) (h "0di05yd0j4nw2nlvq0bvkzcwmmsimrb7jma4m1jh1gyn0al5w5b8")))

(define-public crate-windows-implement-0.36.0 (c (n "windows-implement") (v "0.36.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.36.0") (d #t) (k 0) (p "windows-tokens")))) (h "18db23z8g2ig5pzvfzlm1gm95b1w39b72qbgcp62005j7i1y76gm")))

(define-public crate-windows-implement-0.36.1 (c (n "windows-implement") (v "0.36.1") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.36.1") (d #t) (k 0) (p "windows-tokens")))) (h "1m8c3ffrpp5akq9xvps91pw3xdwsrhmi5r671adk86pvhij5d6ww")))

(define-public crate-windows-implement-0.37.0 (c (n "windows-implement") (v "0.37.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.37.0") (d #t) (k 0) (p "windows-tokens")))) (h "055v4rdxj9p9s068bq4ya93imi3fgi7ysc0izmk9szazalp0d8b7")))

(define-public crate-windows-implement-0.38.0 (c (n "windows-implement") (v "0.38.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.38.0") (d #t) (k 0) (p "windows-tokens")))) (h "0cvbjh2qawby19rn486jrfwxpkp4lap4a74d1qkiw1c6yir5faz8") (r "1.61")))

(define-public crate-windows-implement-0.39.0 (c (n "windows-implement") (v "0.39.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)) (d (n "tokens") (r "^0.39.0") (d #t) (k 0) (p "windows-tokens")))) (h "1ryfy5sgf26xmflf33zabzqn10pp6pjrbz75yh2xrdcwa27zj0ds") (r "1.61")))

(define-public crate-windows-implement-0.40.0 (c (n "windows-implement") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1aiyh2hq1zn3r85v258cxr2k9040m59mfm0vhlr3kk0mwgqsyy1h") (r "1.61")))

(define-public crate-windows-implement-0.41.0 (c (n "windows-implement") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0mdcx3ilm6yf8lvs7qniz4fcv05iazcming23yk0rmncsp62h86j") (r "1.61")))

(define-public crate-windows-implement-0.42.0 (c (n "windows-implement") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0rqplra9iq22c64hdg4nx6kkv0rbh0nv4rlndkk9vgdd7syvcfcm") (r "1.61")))

(define-public crate-windows-implement-0.43.0 (c (n "windows-implement") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "01zqmrdnz2j5qd4ahqjsz724mdabi577f400yywcahy7dl2rpqmp") (r "1.64")))

(define-public crate-windows-implement-0.44.0 (c (n "windows-implement") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1ij5q9khlcfn43a1p3ypjbn711k50s9pc8la5bf04ys1wfl7rs3c") (r "1.64")))

(define-public crate-windows-implement-0.46.0 (c (n "windows-implement") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "09kw706qcms5yy34lf714zspj34v8jirfxv7yycavpcsa9czpd69") (r "1.64")))

(define-public crate-windows-implement-0.47.0 (c (n "windows-implement") (v "0.47.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1xpf2y104q3y1q8yy8v471n58mjzx4kb054qpdg9layaxrnn50x3") (r "1.64")))

(define-public crate-windows-implement-0.48.0 (c (n "windows-implement") (v "0.48.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1764n853zd7bb0wn94i0qxfs6kdy7wrz7v9qhdn7x7hvk64fabjy") (r "1.64")))

(define-public crate-windows-implement-0.51.0 (c (n "windows-implement") (v "0.51.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "13csskcqwzsy23k393avjdlqsifqv6w7mdpkd7as8f06v5491lhp") (r "1.64")))

(define-public crate-windows-implement-0.51.1 (c (n "windows-implement") (v "0.51.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0mg5q1rzfix05xvl4fhmp5b6azm8a0pn4dk8hkc21by5zs71aazv") (r "1.64")))

(define-public crate-windows-implement-0.52.0 (c (n "windows-implement") (v "0.52.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0il91jkdgnwl20gm8dwbjswsmiq7paif49dyk5kvhwv72wrqq5hj") (r "1.64")))

(define-public crate-windows-implement-0.53.0 (c (n "windows-implement") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1gd05sw9knn8i7n9ip1kdwpxqcwmldja3w32m16chjcjprkc4all") (r "1.64")))

(define-public crate-windows-implement-0.56.0 (c (n "windows-implement") (v "0.56.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "16rgkvlx4syqmajfdwmkcvn6nvh126wjj8sg3jvsk5fdivskbz7n") (r "1.62")))

