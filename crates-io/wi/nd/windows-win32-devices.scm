(define-module (crates-io wi nd windows-win32-devices) #:use-module (crates-io))

(define-public crate-windows-win32-devices-0.22.0 (c (n "windows-win32-devices") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0rzh1nf5mqy4sis635waz5ms2g4qgl6sxm86qdywm2vw20r353g6")))

(define-public crate-windows-win32-devices-0.22.3 (c (n "windows-win32-devices") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0h3p3bgsdzvbi6hr4fabv6xrd6h96fan7kl5pwbri10cfw01ypqq")))

(define-public crate-windows-win32-devices-0.22.5 (c (n "windows-win32-devices") (v "0.22.5") (h "0l1c9a0m5y57j96bd2j00x2sy1nwa8p16bygfhrbvpi4xw0qqki5") (r "1.46")))

(define-public crate-windows-win32-devices-0.22.6 (c (n "windows-win32-devices") (v "0.22.6") (h "1r4m49kwwqs1n611zs0yfxacdrzy81b7g2dz3f4giyr6k4w9zrxy") (r "1.46")))

(define-public crate-windows-win32-devices-0.23.0 (c (n "windows-win32-devices") (v "0.23.0") (h "0gycr6zlfkdi2sxlv1mqwlpanrs6xdjgvcca81xp3h2ybcfx9lvq")))

