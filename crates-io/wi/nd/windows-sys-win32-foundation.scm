(define-module (crates-io wi nd windows-sys-win32-foundation) #:use-module (crates-io))

(define-public crate-windows-sys-win32-foundation-0.22.5 (c (n "windows-sys-win32-foundation") (v "0.22.5") (h "1v904f42jqasfhwp4grwch9f69n5rhgmsrhi20j0vv967ya120r8") (r "1.46")))

(define-public crate-windows-sys-win32-foundation-0.22.6 (c (n "windows-sys-win32-foundation") (v "0.22.6") (h "0d7lb57pfv1qgwn9df63j36akzs4yma3w9zbl3k3wlbh6l6ls2ay") (r "1.46")))

(define-public crate-windows-sys-win32-foundation-0.23.0 (c (n "windows-sys-win32-foundation") (v "0.23.0") (h "1kcv0g7n944asvy62s9n8s0v4bqp22l6xqyxlw0clk0ngswx5h23")))

