(define-module (crates-io wi nd windows-sys-globalization) #:use-module (crates-io))

(define-public crate-windows-sys-globalization-0.22.5 (c (n "windows-sys-globalization") (v "0.22.5") (h "0vvan7sy8f5520mf92aq107l9dzmzbs0jp058d0bfliaqi3pm1c3") (r "1.46")))

(define-public crate-windows-sys-globalization-0.22.6 (c (n "windows-sys-globalization") (v "0.22.6") (h "10vfyinn4dnk1gxrmvb38g214l6vzwv0r425c86k094zqnx0bvj4") (r "1.46")))

(define-public crate-windows-sys-globalization-0.23.0 (c (n "windows-sys-globalization") (v "0.23.0") (h "1qkxm2v4kmxd3nl6bhpydhdcgwisd5zrsvgb8qs2zsadkd4z8m32")))

