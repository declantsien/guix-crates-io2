(define-module (crates-io wi nd windows_gen_macros) #:use-module (crates-io))

(define-public crate-windows_gen_macros-0.1.0 (c (n "windows_gen_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qz5nyd1cr2y5fpqdmdm38z4g58ghl3rqmi4743sq19glmq342p6")))

(define-public crate-windows_gen_macros-0.1.2 (c (n "windows_gen_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0azicg3id7c2q67pmhgks9q7qcd36wqwj92f1rjhxbhilwibc458")))

(define-public crate-windows_gen_macros-0.1.3 (c (n "windows_gen_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08ym8ychrfdxj5a3arr8rxj6vjsxwc3pvaf4m2gj1ascc5ldvndk")))

(define-public crate-windows_gen_macros-0.1.4 (c (n "windows_gen_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pm2k9m0cs8c4agfiax8hsma707vf5r2n0xs2vrs344fzij0j8f4")))

(define-public crate-windows_gen_macros-0.2.1 (c (n "windows_gen_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d5f0lx2is9b84a5nh4pzs61k09h8ysnjy2la4wcmv1xqpqcz3l7")))

(define-public crate-windows_gen_macros-0.3.0 (c (n "windows_gen_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zrgfxml7lsml2agfca7j964csl54l16s71n5r0kljcsh31ylj5f")))

(define-public crate-windows_gen_macros-0.3.1 (c (n "windows_gen_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yckp5iw7vkjdc19dfp4kzr2xaa92479wvs91j4kn5r0k8bc5si3")))

(define-public crate-windows_gen_macros-0.4.0 (c (n "windows_gen_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13f8cv2pamrki46mdwl9p79qil2q96qn4xkrpc933z08q6illkhs")))

(define-public crate-windows_gen_macros-0.5.0 (c (n "windows_gen_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0llhlsf5z7pyp8cm9ly1h5r1w3s3mann5sl9plks4p17lkd7fz59")))

(define-public crate-windows_gen_macros-0.6.0 (c (n "windows_gen_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nbxvbc5wdlr3ikl9qqclrn6sgjdfam8gwkmss78r3i5g06a9qv0")))

