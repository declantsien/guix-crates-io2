(define-module (crates-io wi nd windows-error) #:use-module (crates-io))

(define-public crate-windows-error-1.0.0 (c (n "windows-error") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0n1ynkr8aiqs0n4mgccdvfrwb0sav9rvfkxw7kk4ysxdh0qlp8yz")))

(define-public crate-windows-error-1.0.1 (c (n "windows-error") (v "1.0.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0i8ialhv2dgs0krqqmxifnqsklj7sf1kya5w86ihcxxy8wd08vsb")))

(define-public crate-windows-error-1.0.2 (c (n "windows-error") (v "1.0.2") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (t "cfg(windows)") (k 0)))) (h "0lgxzas8cg53haas10h1c1441hi9z9xrnsbsmiacn31yvqq3543b")))

(define-public crate-windows-error-1.1.0 (c (n "windows-error") (v "1.1.0") (h "0l1wl3a9nl76llanl289k4100xilw3ijmyzr91pi2rs3z1s08myq")))

