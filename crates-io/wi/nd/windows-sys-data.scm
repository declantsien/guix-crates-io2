(define-module (crates-io wi nd windows-sys-data) #:use-module (crates-io))

(define-public crate-windows-sys-data-0.22.4 (c (n "windows-sys-data") (v "0.22.4") (h "0jdr9l2q0ckq7p4kwmnhiwng41rq9bnbch3cxiqjjpzvc9g62sr6") (r "1.46")))

(define-public crate-windows-sys-data-0.22.5 (c (n "windows-sys-data") (v "0.22.5") (h "1hmmaskzyr7aqqcamn4galghnhq4wd812q8yvy89zbrir7zzlipl") (r "1.46")))

(define-public crate-windows-sys-data-0.22.6 (c (n "windows-sys-data") (v "0.22.6") (h "1j37qadn2maq7av1rjvpvnsy3lhpai7pshdr90lcf091jxna0jfk") (r "1.46")))

(define-public crate-windows-sys-data-0.23.0 (c (n "windows-sys-data") (v "0.23.0") (h "03vb0fp30vkr9m33wzjjf93w992wn6fxiacmxqiib5ivxkasrshy")))

