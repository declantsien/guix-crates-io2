(define-module (crates-io wi nd windows-win32-system-sys) #:use-module (crates-io))

(define-public crate-windows-win32-system-sys-0.0.2 (c (n "windows-win32-system-sys") (v "0.0.2") (h "0m57qbn0arppfzf58m0ybqpxcxmrmw1xpf6ljbqclbv7mz1jnbgx") (r "1.46")))

(define-public crate-windows-win32-system-sys-0.23.0 (c (n "windows-win32-system-sys") (v "0.23.0") (h "0p0jrx5aylghizc62hssiy2b7if7jxbr648nkl9s69qizbsmhblq")))

