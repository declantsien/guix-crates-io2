(define-module (crates-io wi nd windmark-comments) #:use-module (crates-io))

(define-public crate-windmark-comments-0.1.0 (c (n "windmark-comments") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)) (d (n "windmark") (r "^0.1.7") (d #t) (k 0)))) (h "0rfiwvs2xym0mfczh58rpzvxdgvx5j79d8fpxcwxgfa90k722lfm")))

(define-public crate-windmark-comments-0.1.1 (c (n "windmark-comments") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)) (d (n "windmark") (r "^0.1.7") (d #t) (k 0)))) (h "0x4bvnblp2i1fbic0ilaaa5lqmf2309hi252l0jxrlb0n0ww5ma2")))

(define-public crate-windmark-comments-0.1.2 (c (n "windmark-comments") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)) (d (n "windmark") (r "^0.1.8") (d #t) (k 0)))) (h "0233y0qdiaaik38dp9976mxnsgsfjkagbq22ys1mckniqd9jbl4f")))

(define-public crate-windmark-comments-0.1.3 (c (n "windmark-comments") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)) (d (n "windmark") (r "^0.1.9") (d #t) (k 0)))) (h "02hl3a4sk1r7rfvn7ps7mk0rvb1rv070bv3mwqa71lkn6arrxd94")))

