(define-module (crates-io wi nd windows-dll-codegen) #:use-module (crates-io))

(define-public crate-windows-dll-codegen-0.1.0 (c (n "windows-dll-codegen") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16sg4rbzm2qy9424a4553j1arbgcx17z8h66w84xhy6ql8sf199m")))

(define-public crate-windows-dll-codegen-0.1.1 (c (n "windows-dll-codegen") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bfrblqsq462l4vnhvfabj216bkl4lhkwws923mn12plm2y1l7g8")))

(define-public crate-windows-dll-codegen-0.2.0 (c (n "windows-dll-codegen") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "093gxp14map4z5cymqgcns2p2jcdd8k96faxlllvir93zf4xxb2b")))

(define-public crate-windows-dll-codegen-0.2.2 (c (n "windows-dll-codegen") (v "0.2.2") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kssyh5qc74qd1787hgfyxbmn5jgqlbkxzf4jf1fqs1ijlqx57ls")))

(define-public crate-windows-dll-codegen-0.2.4 (c (n "windows-dll-codegen") (v "0.2.4") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18jiwv6nqf36194yaklxjmg6k2y19240n9lwam1bz1ca19vipv4h")))

(define-public crate-windows-dll-codegen-0.3.0 (c (n "windows-dll-codegen") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1j1q12shpaq5xidmnl6g3mcisdzx4pisvm5l458jyi0s6vsjzh0s")))

(define-public crate-windows-dll-codegen-0.4.0 (c (n "windows-dll-codegen") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12r0dghciv39pks61qm21kpmylrij9y6lv6fy53j8sz1l41zghm7")))

