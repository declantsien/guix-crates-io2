(define-module (crates-io wi nd windowed-futures) #:use-module (crates-io))

(define-public crate-windowed-futures-0.1.0 (c (n "windowed-futures") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0d4qkd5yyf9gfzvybjlccs7s1k5q3c8f2gd0l6gngcb84z0dkkqb")))

