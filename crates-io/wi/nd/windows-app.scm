(define-module (crates-io wi nd windows-app) #:use-module (crates-io))

(define-public crate-windows-app-0.1.0 (c (n "windows-app") (v "0.1.0") (h "1ldmxklcphy64gqggl9slwrhclrzbnplfkdqv64wmky9kidq1bm2") (y #t)))

(define-public crate-windows-app-0.3.0 (c (n "windows-app") (v "0.3.0") (d (list (d (n "windows") (r "^0.14.0") (d #t) (k 0)) (d (n "windows") (r "^0.14.0") (d #t) (k 1)))) (h "12vizqrnli101c92nhg5rb0hbv6fr30mv3ldpvcxy3dw7sz6fm1x") (y #t)))

(define-public crate-windows-app-0.4.0 (c (n "windows-app") (v "0.4.0") (d (list (d (n "windows") (r "^0.15.0") (d #t) (k 0)) (d (n "windows") (r "^0.15.0") (d #t) (k 1)))) (h "0w41qd87fxhirqyiwrikk0yqixwfwgpwps6b0395hfpp6i4kpk15") (y #t)))

(define-public crate-windows-app-0.4.1 (c (n "windows-app") (v "0.4.1") (d (list (d (n "windows") (r "^0.15.1") (d #t) (k 0)) (d (n "windows") (r "^0.15.1") (d #t) (k 1)))) (h "1sksr0lpp2c4dr0p7mi38nq2anxrs2zwsnw5zlmnksay7ll41ann") (y #t)))

(define-public crate-windows-app-0.4.2 (c (n "windows-app") (v "0.4.2") (d (list (d (n "windows") (r "^0.15.2") (d #t) (k 0)) (d (n "windows") (r "^0.15.2") (d #t) (k 1)))) (h "0cfir89yvf9jdlrrf745rcmg08dhc5yww81hx632pb20z6zig5b9") (y #t)))

(define-public crate-windows-app-0.4.3 (c (n "windows-app") (v "0.4.3") (d (list (d (n "windows") (r "^0.15.3") (d #t) (k 0)) (d (n "windows") (r "^0.15.3") (d #t) (k 1)))) (h "1kzgbv92g8d8g3a8yc2b313fbnhzvmha0kwr5hqd5fwlvkf3ndw8") (y #t)))

(define-public crate-windows-app-0.4.4 (c (n "windows-app") (v "0.4.4") (d (list (d (n "windows") (r "^0.15.4") (d #t) (k 0)) (d (n "windows") (r "^0.15.4") (d #t) (k 1)))) (h "1ryk946i0n2d9hs34gkr2fa5nhkglyxd8zvv46sarmbdy19fyk7x") (y #t)))

(define-public crate-windows-app-0.4.5 (c (n "windows-app") (v "0.4.5") (d (list (d (n "windows") (r "^0.15.5") (d #t) (k 0)) (d (n "windows") (r "^0.15.5") (d #t) (k 1)))) (h "1rvgmd0mx5z3x5ybsl0kk471zbl04ihajc8lz2b88z2rv6sccf6d") (y #t)))

(define-public crate-windows-app-0.4.6 (c (n "windows-app") (v "0.4.6") (d (list (d (n "windows") (r "^0.15.6") (d #t) (k 0)) (d (n "windows") (r "^0.15.6") (d #t) (k 1)))) (h "1qhkf50f4v244jvvjfx12wc9kg59s9d1085clqxngq6lgcrh5hnp") (y #t)))

(define-public crate-windows-app-0.4.7 (c (n "windows-app") (v "0.4.7") (d (list (d (n "windows") (r "^0.15.7") (d #t) (k 0)) (d (n "windows") (r "^0.15.7") (d #t) (k 1)))) (h "0cn66ris7x0f3nxq4vl4mn1hkmmhcsv94a167pgvcpr9rxq1044a") (y #t)))

(define-public crate-windows-app-0.16.0 (c (n "windows-app") (v "0.16.0") (d (list (d (n "windows") (r "^0.16.0") (d #t) (k 0)) (d (n "windows") (r "^0.16.0") (d #t) (k 1)))) (h "0pgcl9zyvq77dc1rmyjgl79w93cdsrhxx0aq22j7xsq1xzrwyzq4") (y #t)))

(define-public crate-windows-app-0.17.0 (c (n "windows-app") (v "0.17.0") (d (list (d (n "windows") (r "^0.17.0") (d #t) (k 0)) (d (n "windows") (r "^0.17.0") (d #t) (k 1)))) (h "0c305388kbc6iy9yi5xkkbzyk9wn420r2jg5s2bri9mjv0c1xa8y") (y #t)))

(define-public crate-windows-app-0.17.1 (c (n "windows-app") (v "0.17.1") (d (list (d (n "windows") (r "^0.17.1") (d #t) (k 0)) (d (n "windows") (r "^0.17.1") (d #t) (k 1)))) (h "0kfvgxnhp77q9x28x9p5lyfsf8hs52fxa7k9bjczyml1zc84gc7f") (y #t)))

(define-public crate-windows-app-0.17.2 (c (n "windows-app") (v "0.17.2") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1gm2b3x1mykqfm6jk92wjby87cp1393m1bhxaqpfsp4z1y7bxm1x") (y #t)))

