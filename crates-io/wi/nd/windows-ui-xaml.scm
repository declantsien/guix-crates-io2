(define-module (crates-io wi nd windows-ui-xaml) #:use-module (crates-io))

(define-public crate-windows-ui-xaml-0.7.0 (c (n "windows-ui-xaml") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0agj430fpirmi02y7lzsa0x5rx3mknd36zxmwrcj8z99f5nfrl7q")))

(define-public crate-windows-ui-xaml-0.22.0 (c (n "windows-ui-xaml") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1wlbjmh3iyw3gqzz1gy7s5p7dm7k1h56yrshnss5mrs4k7b2mvci")))

(define-public crate-windows-ui-xaml-0.22.1 (c (n "windows-ui-xaml") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-services") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "1xj28zgma56y0q0aald6dpapyjhmmq7aqma8hj76py07bd5pfnxb")))

(define-public crate-windows-ui-xaml-0.22.2 (c (n "windows-ui-xaml") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-services") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "1iw4iia210sgc7d63wc4i0ybx25mdgp9b73b4lhgvjyznfzq1ah3")))

(define-public crate-windows-ui-xaml-0.22.3 (c (n "windows-ui-xaml") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1xiam149xzhrig0zn9lfjyjs34hwzj4vckimzk51hswbns9wb9xj")))

