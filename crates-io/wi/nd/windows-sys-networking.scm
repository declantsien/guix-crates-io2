(define-module (crates-io wi nd windows-sys-networking) #:use-module (crates-io))

(define-public crate-windows-sys-networking-0.22.5 (c (n "windows-sys-networking") (v "0.22.5") (h "0w7mg1k8ym1v4ffbbm38531nlm3fjf389q614b7g9dc4sxvbd94l") (r "1.46")))

(define-public crate-windows-sys-networking-0.22.6 (c (n "windows-sys-networking") (v "0.22.6") (h "195fn7q699bl1hwygdbmyxz5gilgw51a4pc8pi681gw2h4dgy1r1") (r "1.46")))

(define-public crate-windows-sys-networking-0.23.0 (c (n "windows-sys-networking") (v "0.23.0") (h "1dl8mrszljdcp94n9y1kddhpih82ymr8lnqh746908wia9n48l1i")))

