(define-module (crates-io wi nd windows-sys-win32-storage) #:use-module (crates-io))

(define-public crate-windows-sys-win32-storage-0.22.5 (c (n "windows-sys-win32-storage") (v "0.22.5") (h "02nhk6rzzxslvbdb7wix5m6bsb63i63hhvjbmdq6by5gn2cpwxfn") (r "1.46")))

(define-public crate-windows-sys-win32-storage-0.22.6 (c (n "windows-sys-win32-storage") (v "0.22.6") (h "1w0vg99db5cxxsxs9gpcyazmffnjnq5913mx3viyrka411390j42") (r "1.46")))

(define-public crate-windows-sys-win32-storage-0.23.0 (c (n "windows-sys-win32-storage") (v "0.23.0") (h "1xs42a3knkqkgh9dy0n4lfc2n7qhzrmr2xzxjsh2x80nbviydhkc")))

