(define-module (crates-io wi nd windows-thumbnail-preloader) #:use-module (crates-io))

(define-public crate-windows-thumbnail-preloader-0.1.0 (c (n "windows-thumbnail-preloader") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 0)) (d (n "thousands") (r "^0.2.0") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Foundation" "Win32" "Win32_Foundation" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com" "Win32_UI_Controls" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "windows-core") (r "^0.52.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1z8bqcprpni9q1i9w36gp0ijl9sjs3nsw9i64508blxlz0f206ri")))

