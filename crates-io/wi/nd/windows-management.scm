(define-module (crates-io wi nd windows-management) #:use-module (crates-io))

(define-public crate-windows-management-0.7.0 (c (n "windows-management") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0xqqcnb4d446x41kfphkcy5p757z3y373rkw5gakd4sijgxj87cg")))

(define-public crate-windows-management-0.22.0 (c (n "windows-management") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0imkxm7i2hz4xs6v8yyx7c9dv7xdn21agfwf60y4xll0aaycd01a")))

(define-public crate-windows-management-0.22.1 (c (n "windows-management") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)))) (h "1a1bw4vwwnldill03jh5pwd4cb5crlax8dg7jhn745ykx7mbmi6z")))

(define-public crate-windows-management-0.22.2 (c (n "windows-management") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)))) (h "03q1s53i0n3yk2hfwd8jj5gvzjnaxr8y67dhqhdhff4972wz5z0r")))

(define-public crate-windows-management-0.22.3 (c (n "windows-management") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "17av14f322mmm3bv841dgjkqhbbpchvq3s7jrhw7nvfhxplnms5b")))

(define-public crate-windows-management-0.22.5 (c (n "windows-management") (v "0.22.5") (h "096h7dn3l8dbf49s4b67sp7kl1phfj11497l2xh85dg09wqivqia") (r "1.46")))

(define-public crate-windows-management-0.22.6 (c (n "windows-management") (v "0.22.6") (h "0bvlisbxv94karfyswgn2h29vb9ahk116l5v38kr00z3m3m2vbk9") (r "1.46")))

(define-public crate-windows-management-0.1.0 (c (n "windows-management") (v "0.1.0") (h "13dmkhjdzvr1ykf5bkwhgxdl785qcjk8z25gria1ij07p8g1zh9j")))

(define-public crate-windows-management-0.23.0 (c (n "windows-management") (v "0.23.0") (h "116s32jhswxplnlnr2lxdiwq5py1aqz27q4dxbizwfqiy3z3ls8x")))

(define-public crate-windows-management-0.0.0 (c (n "windows-management") (v "0.0.0") (h "198fjxdcli9a9xpxm331xg9rpqn14k02dzkqcd8li7hjvny508ss") (r "1.60")))

