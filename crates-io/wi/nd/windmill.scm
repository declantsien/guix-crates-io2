(define-module (crates-io wi nd windmill) #:use-module (crates-io))

(define-public crate-windmill-0.1.0 (c (n "windmill") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4.0") (f (quote ("iso"))) (d #t) (k 0)))) (h "0a40jqz71l4yachyj7qhpjpjissjxng418wvp9jd7a2w9gq88ngl")))

