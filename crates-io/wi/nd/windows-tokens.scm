(define-module (crates-io wi nd windows-tokens) #:use-module (crates-io))

(define-public crate-windows-tokens-0.0.0 (c (n "windows-tokens") (v "0.0.0") (h "1l82dyhk5ivjyn920bnpil7jh22l5q4sjbx4fmjk5madsn3agq2y")))

(define-public crate-windows-tokens-0.31.0 (c (n "windows-tokens") (v "0.31.0") (h "17m11y9baqf9sr13j15y1dv02vkys9bzzg2wvb53dl3im505ky3v")))

(define-public crate-windows-tokens-0.32.0 (c (n "windows-tokens") (v "0.32.0") (h "1rrqbxjkyk6h6p6jjzbcxr0mhqbz0yfndd2s2dsgmbl75f4yy7gn")))

(define-public crate-windows-tokens-0.33.0 (c (n "windows-tokens") (v "0.33.0") (h "04zw91rd892yfpaidfm7nkzjpcaf853w7vcsk44qy0pnxnh26666")))

(define-public crate-windows-tokens-0.34.0 (c (n "windows-tokens") (v "0.34.0") (h "01f6ym6z5l1g2488za5w8p1scia5dzcrmk96gnf4pjgp8w4k1yq2")))

(define-public crate-windows-tokens-0.35.0 (c (n "windows-tokens") (v "0.35.0") (h "037nzyv1xccbp0i7rg5x9bgkvs20gvnxb6y4hs8cb66c8xr6hwgx")))

(define-public crate-windows-tokens-0.36.0 (c (n "windows-tokens") (v "0.36.0") (h "1lxyfnvq71i32gmzl1a4yn2lp5glgxnc2gkf9g2ks38ip00b4dmi")))

(define-public crate-windows-tokens-0.36.1 (c (n "windows-tokens") (v "0.36.1") (h "0ab6ghl6y1r0d08kbxxqywvk9gm630yz62in3kh79z6xz7xvllm0")))

(define-public crate-windows-tokens-0.37.0 (c (n "windows-tokens") (v "0.37.0") (h "0sbim19lgiik8g18s82wqf3aks29p430rwcgnyarjhbh25gx4qrj")))

(define-public crate-windows-tokens-0.38.0 (c (n "windows-tokens") (v "0.38.0") (h "02vl5dym7mn46ygv6ih7qvqc5wd1qv3yqib9bf5k8w887q7r7gaw")))

(define-public crate-windows-tokens-0.39.0 (c (n "windows-tokens") (v "0.39.0") (h "15zmsz8ji6z7471xwznrm4hqp6j94s7pjjz7i34vmrjzw4pxwf7q")))

(define-public crate-windows-tokens-0.40.0 (c (n "windows-tokens") (v "0.40.0") (h "1cjxa5v8sb3vam1kw2w005mszz0h78gch5q8xmc50r45lk8h2kf4")))

(define-public crate-windows-tokens-0.41.0 (c (n "windows-tokens") (v "0.41.0") (h "1q7hlsa0ffr0s3baylk9lzw5nfqzl7nlzy7f0d5jp9s3cx1ndjva")))

(define-public crate-windows-tokens-0.42.0 (c (n "windows-tokens") (v "0.42.0") (h "05j3h15lql330943spv1vjy21xfizkb1g4hj0phifkix27g3r4v3")))

(define-public crate-windows-tokens-0.44.0 (c (n "windows-tokens") (v "0.44.0") (h "16wmn01ii3dvzbri3y3y6r1ias2nkkkvvm65844d383m16852hps")))

(define-public crate-windows-tokens-0.46.0 (c (n "windows-tokens") (v "0.46.0") (h "1l6kgxkdsj96486mma6wnvl35m19qdk917jlxg91fiwwc4wrngyd")))

(define-public crate-windows-tokens-0.47.0 (c (n "windows-tokens") (v "0.47.0") (h "1qg48prh1zbq9d78qw9izf2hsclyg55wyd1cfi3wga9zl39lv8px")))

(define-public crate-windows-tokens-0.48.0 (c (n "windows-tokens") (v "0.48.0") (h "1d7n299vmcapd3kds8wrr3zqsd3rh6d7yvslhmrxnhfb50xrlk5k")))

