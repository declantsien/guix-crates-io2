(define-module (crates-io wi nd windows-management-sys) #:use-module (crates-io))

(define-public crate-windows-management-sys-0.0.2 (c (n "windows-management-sys") (v "0.0.2") (h "09llpfznc6zfn5xd7yp3grs9d3h3bzlggmxgghfzfmhc8b0z3l1d") (r "1.46")))

(define-public crate-windows-management-sys-0.23.0 (c (n "windows-management-sys") (v "0.23.0") (h "0x0c1p7dbgx4hdbxs760gvpq2069wcsljk5nggj1r9l0hbcax613")))

