(define-module (crates-io wi nd windows-win32-web) #:use-module (crates-io))

(define-public crate-windows-win32-web-0.22.3 (c (n "windows-win32-web") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0hy22ldgpr3z4ilspih84gh5rkjs7020iflixxsp0kxprnybqiwi")))

(define-public crate-windows-win32-web-0.22.5 (c (n "windows-win32-web") (v "0.22.5") (h "19phyc2jlzkzi0kzc6iahv3pznjdqfzr0p6813hszn64nw6dlr2y") (r "1.46")))

(define-public crate-windows-win32-web-0.22.6 (c (n "windows-win32-web") (v "0.22.6") (h "0yfkv4rxqkq9c94y71siraidm09wnapnhln2xc01ykbxm1n33wc7") (r "1.46")))

(define-public crate-windows-win32-web-0.23.0 (c (n "windows-win32-web") (v "0.23.0") (h "0lr0z1vs3769g4896bp4hxqh79qbmgljgigznixipym0h8c8m0yf")))

