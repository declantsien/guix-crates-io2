(define-module (crates-io wi nd windows-args) #:use-module (crates-io))

(define-public crate-windows-args-0.1.0 (c (n "windows-args") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1g4lw5cgygzkxlhjd2sy1p9x7m6ij3sdmghgj6vn19gfjdddl9ir") (f (quote (("_debug_compile_test"))))))

(define-public crate-windows-args-0.2.0 (c (n "windows-args") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)) (d (n "wtf8") (r "^0.0.3") (d #t) (k 0)))) (h "1rfpwfnkmas41bcwx1ll36j9nn0ha1h1g84prgwgvr4awdbvfqd4")))

