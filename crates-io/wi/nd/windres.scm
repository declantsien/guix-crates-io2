(define-module (crates-io wi nd windres) #:use-module (crates-io))

(define-public crate-windres-0.1.0 (c (n "windres") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "concat-string") (r "^1") (d #t) (t "cfg(windows)") (k 0)))) (h "0rf5glyllx74yhgnl50lvgwhrginbzaid8rd43xpnh0nm1fsqdy4")))

(define-public crate-windres-0.2.0 (c (n "windres") (v "0.2.0") (d (list (d (n "concat-string") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "find-winsdk") (r "^0.1") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"msvc\"))") (k 0)))) (h "13dmzzqwwnlcwb59i6pv21hrpgfkn2rm43i4wbr50w4vyikfrzkg")))

(define-public crate-windres-0.2.1 (c (n "windres") (v "0.2.1") (d (list (d (n "concat-string") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "find-winsdk") (r "^0.2") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"msvc\"))") (k 0)))) (h "0afpw6v9wxq08fg04r73i74chmjw0da771fz5c1snzsxf3zi6vnx")))

(define-public crate-windres-0.2.2 (c (n "windres") (v "0.2.2") (d (list (d (n "concat-string") (r "^1") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "find-winsdk") (r "^0.2") (d #t) (t "cfg(all(target_os = \"windows\", target_env = \"msvc\"))") (k 0)))) (h "1p2c1aip32kf1jhxc02s03wv2wfib608n21r3806caqv48cmc4c2")))

