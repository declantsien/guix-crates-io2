(define-module (crates-io wi nd windows-system-sys) #:use-module (crates-io))

(define-public crate-windows-system-sys-0.0.2 (c (n "windows-system-sys") (v "0.0.2") (h "1hfms4zxz2x91yj28p5c4vvlxldpmici44fg380zahd8fk057i3i") (r "1.46")))

(define-public crate-windows-system-sys-0.23.0 (c (n "windows-system-sys") (v "0.23.0") (h "07x44afzfh96fcw273ap2sv4ak24i60r8qv6np10w9g6aslf06hw")))

