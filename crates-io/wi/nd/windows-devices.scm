(define-module (crates-io wi nd windows-devices) #:use-module (crates-io))

(define-public crate-windows-devices-0.7.0 (c (n "windows-devices") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1gyp5siqxvz9z1yv8q5fdj6a0zs9vfx7v6aqjb5k7y9sgak39jf5")))

(define-public crate-windows-devices-0.22.0 (c (n "windows-devices") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0khh3smikamn0qrmslqsi654ipijw5wn9hcrl5993r3jk764pj3y")))

(define-public crate-windows-devices-0.22.1 (c (n "windows-devices") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "100984045ds5nbs6z0ji2hgfvb4whiigv6qqndzkagi97xrclp8n")))

(define-public crate-windows-devices-0.22.2 (c (n "windows-devices") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "06458bn342y23napqvsv68fa4vahy0gqfi5736hjr7qbnx1valkq")))

(define-public crate-windows-devices-0.22.3 (c (n "windows-devices") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0d09f9vqlw289649f8g6blbb4kgiq4bzrdzc8askqqw75zl487w7")))

(define-public crate-windows-devices-0.22.4 (c (n "windows-devices") (v "0.22.4") (h "155c31nb8br854bn1jbgi5m92vnxhy0032x9h8navbfcz3x651n3") (r "1.46")))

(define-public crate-windows-devices-0.22.5 (c (n "windows-devices") (v "0.22.5") (h "0p6r5vk1wcmnz08bsl02936zr7kd5mcqgjj7w81jz647jx42vqjl") (r "1.46")))

(define-public crate-windows-devices-0.22.6 (c (n "windows-devices") (v "0.22.6") (h "0awhf3lb4ycs6dhpkgpv5wrfmq950js8f3j5d98kimhl29cqk5v7") (r "1.46")))

(define-public crate-windows-devices-0.1.0 (c (n "windows-devices") (v "0.1.0") (h "13jj08917gg8148av0cqi3v5kwa71n5mb39nikrpf3w90s02hhkr")))

(define-public crate-windows-devices-0.23.0 (c (n "windows-devices") (v "0.23.0") (h "0byni0fsxr6a5r9hrbknaj1c2786yivz8apz88yfl8ncxzw220ic")))

