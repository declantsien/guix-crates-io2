(define-module (crates-io wi nd windows-sys-system) #:use-module (crates-io))

(define-public crate-windows-sys-system-0.22.5 (c (n "windows-sys-system") (v "0.22.5") (h "016qv2qpx1mqp2ih6rd0x386r386s2i2d07kp6m6psw2rhvzyn4j") (r "1.46")))

(define-public crate-windows-sys-system-0.22.6 (c (n "windows-sys-system") (v "0.22.6") (h "1pb96xfd30lyg35zv0z7n70ac3h1jyzil9kxcnh1kdszrmzlfkjc") (r "1.46")))

(define-public crate-windows-sys-system-0.23.0 (c (n "windows-sys-system") (v "0.23.0") (h "0yh7xmlmxppc00p5k8r297a2yyncp5gg1yh5fqp6xsp9323cclmn")))

