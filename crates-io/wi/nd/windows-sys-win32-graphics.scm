(define-module (crates-io wi nd windows-sys-win32-graphics) #:use-module (crates-io))

(define-public crate-windows-sys-win32-graphics-0.22.5 (c (n "windows-sys-win32-graphics") (v "0.22.5") (h "0wkzf440mbs1kz6wq05i950wjdzqvpzv9krw6zgb4nf9ir2fhjfi") (r "1.46")))

(define-public crate-windows-sys-win32-graphics-0.22.6 (c (n "windows-sys-win32-graphics") (v "0.22.6") (h "1s8rh6fvwcr3428b6m7iz1zj2czkdrszkq5hxzcibr27w7v2llzn") (r "1.46")))

(define-public crate-windows-sys-win32-graphics-0.23.0 (c (n "windows-sys-win32-graphics") (v "0.23.0") (h "0n0ba3nihxsjwkqbga7mkfyzyq668wa1m7ysmbb2rmjp2al8nii3")))

