(define-module (crates-io wi nd windows_safe_handle) #:use-module (crates-io))

(define-public crate-windows_safe_handle-0.1.0 (c (n "windows_safe_handle") (v "0.1.0") (d (list (d (n "windows") (r "^0.48") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography" "Win32_Graphics_Gdi"))) (d #t) (k 2)))) (h "1s6iq669vfdmw2fqzk0mg5zi0qasip39nk0dd4ab6a1jni8jyjwq")))

(define-public crate-windows_safe_handle-0.1.1 (c (n "windows_safe_handle") (v "0.1.1") (d (list (d (n "windows") (r "^0.48") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography" "Win32_Graphics_Gdi"))) (d #t) (k 2)))) (h "190xq0iivbyfmfjavfw8hi0svs0cl0zs42v0b4794hsyh8nz94yc")))

(define-public crate-windows_safe_handle-0.1.2 (c (n "windows_safe_handle") (v "0.1.2") (d (list (d (n "windows") (r "^0.48") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_Security_Cryptography" "Win32_Graphics_Gdi"))) (d #t) (k 2)))) (h "1k8k9gbvxmn4v2nv40yzsq8h7pjnhhlmvn81w26ynrdyrh70a5l2")))

