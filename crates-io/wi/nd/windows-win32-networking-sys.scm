(define-module (crates-io wi nd windows-win32-networking-sys) #:use-module (crates-io))

(define-public crate-windows-win32-networking-sys-0.0.2 (c (n "windows-win32-networking-sys") (v "0.0.2") (h "0yr6rgzjzl6ih3r2050cix5xmh2g07xvxrrdm9wk9j73ipvbnzbs") (r "1.46")))

(define-public crate-windows-win32-networking-sys-0.23.0 (c (n "windows-win32-networking-sys") (v "0.23.0") (h "00wwg916rlq2nlcglzv3zw634zdshq05aiqfcpnmfj15p9cgxpin")))

