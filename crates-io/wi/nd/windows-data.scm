(define-module (crates-io wi nd windows-data) #:use-module (crates-io))

(define-public crate-windows-data-0.7.0 (c (n "windows-data") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0456vmkdxjrfx1qzqvyhm84g26bkqigzc9s82aw7y68bndf290hx")))

(define-public crate-windows-data-0.22.0 (c (n "windows-data") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "09ph7g2qrrpq1nlsais46mn673ag7jw1xip5rrpqd14qfplfqvjb")))

(define-public crate-windows-data-0.22.1 (c (n "windows-data") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "00hqan5gi14jsqmxymj8dgv2l3a0x6wbqrr1c4dl5k1z65891qdf")))

(define-public crate-windows-data-0.22.2 (c (n "windows-data") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "1srw0qs18gpjjr69f053bgnkifgv6yskhbfsi6dn8ddjlnvhzlnf")))

(define-public crate-windows-data-0.22.3 (c (n "windows-data") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1ma3iv7k7k7zia2sdvvnichf9dfbiizcbhc4i3fn9j0gxfq5b9z3")))

(define-public crate-windows-data-0.22.4 (c (n "windows-data") (v "0.22.4") (h "18iym1kmxynjchv2w5033lnnd5j66r3q84zgz4ljj5k2alrnj4nm") (r "1.46")))

(define-public crate-windows-data-0.22.5 (c (n "windows-data") (v "0.22.5") (h "1ipc8jlfdgv5fmaa28x5jg683cf0dxsif69gznm89bcswd83bwv2") (r "1.46")))

(define-public crate-windows-data-0.22.6 (c (n "windows-data") (v "0.22.6") (h "0njj3n0dfs7559qdx7gwsynwdc7kmbvddwxv6biv1ria7d04dk83") (r "1.46")))

(define-public crate-windows-data-0.1.0 (c (n "windows-data") (v "0.1.0") (h "0lq6dncn7aiq70vcvpjiwyp8yii996bcxylp5amz0yn2d9xp771r")))

(define-public crate-windows-data-0.23.0 (c (n "windows-data") (v "0.23.0") (h "0kh2jw78css2623brxmg1yncg99hfcppx8l3jj7p4snma5jglfcv")))

