(define-module (crates-io wi nd windows-win32-management) #:use-module (crates-io))

(define-public crate-windows-win32-management-0.22.0 (c (n "windows-win32-management") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "13p2bj9vx6lcnvf6szscikna8ncn1mrwsynar04jjq43j2v4hhaz")))

(define-public crate-windows-win32-management-0.22.1 (c (n "windows-win32-management") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-win32-foundation") (r "^0.22.1") (d #t) (k 0)))) (h "170kwqicg3cp0dcwp2cpb9jj6nfymxsiyh3vsqig053jigjp4669")))

(define-public crate-windows-win32-management-0.22.2 (c (n "windows-win32-management") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-win32-foundation") (r "^0.22.2") (d #t) (k 0)))) (h "1w7hzklp7n8wh97gwnnls13y8xydg0aqy7kx3b2hq7l2ba4jyb50")))

(define-public crate-windows-win32-management-0.22.3 (c (n "windows-win32-management") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1x73j735vkisi941n3s5xbsjq3x3k8kmzkg7miz8xmhnjq36visg")))

(define-public crate-windows-win32-management-0.22.5 (c (n "windows-win32-management") (v "0.22.5") (h "07fxiccismdzbda046d8lganp4caxik7632nvvifp06p69kmd4hw") (r "1.46")))

(define-public crate-windows-win32-management-0.22.6 (c (n "windows-win32-management") (v "0.22.6") (h "173fhfbh6w73ivxkas8swiaiglbm2cmgllz1ny5lz57rps3ngdx2") (r "1.46")))

(define-public crate-windows-win32-management-0.23.0 (c (n "windows-win32-management") (v "0.23.0") (h "1vgrfxmfvs124rfk6px8p25pccwfsjz5hayzbmrd29m1b2cdz7a7")))

