(define-module (crates-io wi nd windows_i686_gnu) #:use-module (crates-io))

(define-public crate-windows_i686_gnu-0.23.0 (c (n "windows_i686_gnu") (v "0.23.0") (h "0nw4xh74yj2y3jb5swvsr1lpx6igahhsxbfy0ifc7bxf4cb67bl6")))

(define-public crate-windows_i686_gnu-0.24.0 (c (n "windows_i686_gnu") (v "0.24.0") (h "1z810n1yafcmv14himqz9snfmr83pyxr0x077bbsxagclc86b1n0")))

(define-public crate-windows_i686_gnu-0.25.0 (c (n "windows_i686_gnu") (v "0.25.0") (h "119yk483ywrwr7zm99zl8wg3rh9j40ah2hxbirq58rphxd75ic83")))

(define-public crate-windows_i686_gnu-0.26.0 (c (n "windows_i686_gnu") (v "0.26.0") (h "09l7g7zmi7s5qjpa6611vdhmxhn3sp6a26q3c6gmi3mbr8a149i9")))

(define-public crate-windows_i686_gnu-0.27.0 (c (n "windows_i686_gnu") (v "0.27.0") (h "1yh9fc9wywcdnsz4rdacnh39cznyc4zdzc7k74l31z4kkfsj1sxl")))

(define-public crate-windows_i686_gnu-0.28.0 (c (n "windows_i686_gnu") (v "0.28.0") (h "12hx7qpsjg9p7jggfcplqa3mf1mzr7k7s5ybzqwg1zmg4fn2aizm")))

(define-public crate-windows_i686_gnu-0.29.0 (c (n "windows_i686_gnu") (v "0.29.0") (h "0n5skfwab74n7f15zs4a36bk1fc7gmlv4lm6l7nh32wfgfgzb4w7")))

(define-public crate-windows_i686_gnu-0.30.0 (c (n "windows_i686_gnu") (v "0.30.0") (h "1f744iasfzibd9gi59c9iwd5d3vwp4pz6r0qip35cfd9kncf2i8i")))

(define-public crate-windows_i686_gnu-0.31.0 (c (n "windows_i686_gnu") (v "0.31.0") (h "1dhpapc0ggmhw48c9miq38r5zqi9j4xrgwpf84b2j7pk3w7p60kk")))

(define-public crate-windows_i686_gnu-0.32.0 (c (n "windows_i686_gnu") (v "0.32.0") (h "05g6kpdfxwxnw2gn1nrd7bsf5997rci0k3h3nqby168ph5l1qwba")))

(define-public crate-windows_i686_gnu-0.33.0 (c (n "windows_i686_gnu") (v "0.33.0") (h "03nsi8h0yd4n9wgpxcpynzwlnacihisgmh021kfb5fln79qczc6a")))

(define-public crate-windows_i686_gnu-0.34.0 (c (n "windows_i686_gnu") (v "0.34.0") (h "1vc8hr4hm1x89h997gwfq5q9kj1j5gj4pxdlv4lr3dxdb7kzsr15")))

(define-public crate-windows_i686_gnu-0.35.0 (c (n "windows_i686_gnu") (v "0.35.0") (h "0l1v8l7ws1k2r4l8xx06dgl3v80fn5w1nndq16q0ffplbgrschq3")))

(define-public crate-windows_i686_gnu-0.36.0 (c (n "windows_i686_gnu") (v "0.36.0") (h "0mv025b2snmhp79ygqm59d16vaw9krrn3r7zik4p0yfrwg39qx9v")))

(define-public crate-windows_i686_gnu-0.36.1 (c (n "windows_i686_gnu") (v "0.36.1") (h "1dm3svxfzamrv6kklyda9c3qylgwn5nwdps6p0kc9x6s077nq3hq")))

(define-public crate-windows_i686_gnu-0.37.0 (c (n "windows_i686_gnu") (v "0.37.0") (h "1qfr0yxg8ihg08lddg29xh1rfsgra262gdnl8h6p615qn385z4nk")))

(define-public crate-windows_i686_gnu-0.38.0 (c (n "windows_i686_gnu") (v "0.38.0") (h "059wsjax5gd9prjsmla8pq7xq5h3ixm8jhqgbrf9c8rc6zdz562c")))

(define-public crate-windows_i686_gnu-0.39.0 (c (n "windows_i686_gnu") (v "0.39.0") (h "06wynhxklmh3s1ril9bh00rhv1npppcyirsp60p09xx501qwagvn")))

(define-public crate-windows_i686_gnu-0.40.0 (c (n "windows_i686_gnu") (v "0.40.0") (h "1hgkx5g68qz2p3wirqryyqmmwhbyacjsbhiax9fzh38gszx0jnxa")))

(define-public crate-windows_i686_gnu-0.41.0 (c (n "windows_i686_gnu") (v "0.41.0") (h "02s4n5dymg33aky1dclpdnrvq0wcha3y8a5jnkw06bz3a4nxzd02")))

(define-public crate-windows_i686_gnu-0.42.0 (c (n "windows_i686_gnu") (v "0.42.0") (h "1rsxdjp50nk38zfd1dxj12i2qmhpvxsm6scdq8v1d10ncygy3spv")))

(define-public crate-windows_i686_gnu-0.42.1 (c (n "windows_i686_gnu") (v "0.42.1") (h "0h6n8mqjfq1rk4vpr9gz8md1b8f7bqksiymivdxvlc6mi998ff6y")))

(define-public crate-windows_i686_gnu-0.42.2 (c (n "windows_i686_gnu") (v "0.42.2") (h "0kx866dfrby88lqs9v1vgmrkk1z6af9lhaghh5maj7d4imyr47f6")))

(define-public crate-windows_i686_gnu-0.47.0 (c (n "windows_i686_gnu") (v "0.47.0") (h "1qvibmbfv8i36glcxhy1awqi4i2n3a3dp0x4acd208mvzpm5p4n1")))

(define-public crate-windows_i686_gnu-0.48.0 (c (n "windows_i686_gnu") (v "0.48.0") (h "0hd2v9kp8fss0rzl83wzhw0s5z8q1b4875m6s1phv0yvlxi1jak2")))

(define-public crate-windows_i686_gnu-0.48.2 (c (n "windows_i686_gnu") (v "0.48.2") (h "1k93ny5w8l315s6sbz2b8yw6draibs1x7n4bpkxvb38p7qiasa92")))

(define-public crate-windows_i686_gnu-0.48.3 (c (n "windows_i686_gnu") (v "0.48.3") (h "10vs3mgfp9lj5rwfayy3w2g6y54xsyjy2n2qiwll5f6jcl17qn30")))

(define-public crate-windows_i686_gnu-0.48.4 (c (n "windows_i686_gnu") (v "0.48.4") (h "1rdb06hkdmy0pd4ji0yb4p5hifn5babyjr05q816zmcrqc5174nr")))

(define-public crate-windows_i686_gnu-0.48.5 (c (n "windows_i686_gnu") (v "0.48.5") (h "0gklnglwd9ilqx7ac3cn8hbhkraqisd0n83jxzf9837nvvkiand7")))

(define-public crate-windows_i686_gnu-0.52.0 (c (n "windows_i686_gnu") (v "0.52.0") (h "04zkglz4p3pjsns5gbz85v4s5aw102raz4spj4b0lmm33z5kg1m2") (r "1.56")))

(define-public crate-windows_i686_gnu-0.52.1 (c (n "windows_i686_gnu") (v "0.52.1") (h "0zb41ic2r68vm9383j6dn01x26bsd6av4v8gii03j5kbrxd277v7") (y #t) (r "1.60")))

(define-public crate-windows_i686_gnu-0.52.3 (c (n "windows_i686_gnu") (v "0.52.3") (h "1yrkl23hcmknja9hyv3b6bc08d5c9q2f391q865llwxcgim9nkia") (r "1.60")))

(define-public crate-windows_i686_gnu-0.52.4 (c (n "windows_i686_gnu") (v "0.52.4") (h "1lq1g35sbj55ms86by4c080jcqrlfjy9bw5r4mgrkq4riwkdhx5l") (r "1.56")))

(define-public crate-windows_i686_gnu-0.52.5 (c (n "windows_i686_gnu") (v "0.52.5") (h "0w4np3l6qwlra9s2xpflqrs60qk1pz6ahhn91rr74lvdy4y0gfl8") (r "1.56")))

