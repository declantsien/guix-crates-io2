(define-module (crates-io wi nd windows_x86_64_gnu) #:use-module (crates-io))

(define-public crate-windows_x86_64_gnu-0.23.0 (c (n "windows_x86_64_gnu") (v "0.23.0") (h "0297z2icm7zyll61la7xdps36qpkb5123s4j8yw2nli7syfkjrhb")))

(define-public crate-windows_x86_64_gnu-0.24.0 (c (n "windows_x86_64_gnu") (v "0.24.0") (h "1i4di3s8rh9yp43qqrp7q6lfamig9a3bd4r9b8h4902q60v1fjiq")))

(define-public crate-windows_x86_64_gnu-0.25.0 (c (n "windows_x86_64_gnu") (v "0.25.0") (h "03mqkd8jp8lca63qzqrjxslfzbdnw0g2drymjx7kf02131vn3c1c")))

(define-public crate-windows_x86_64_gnu-0.26.0 (c (n "windows_x86_64_gnu") (v "0.26.0") (h "143irri16y9abad0djfa46g0a6b3fz85hgvab6g7c6ddw68gr01a")))

(define-public crate-windows_x86_64_gnu-0.27.0 (c (n "windows_x86_64_gnu") (v "0.27.0") (h "0jj6cp9ql4sa1k9d4y7a450hzia1lxgpr1wvgla0hcwwwf8d4dsr")))

(define-public crate-windows_x86_64_gnu-0.28.0 (c (n "windows_x86_64_gnu") (v "0.28.0") (h "0m79bhdr54g4h4wh2q8wkjlkypb5wvl7xzhc2csiaqb5yl4z8cdw")))

(define-public crate-windows_x86_64_gnu-0.29.0 (c (n "windows_x86_64_gnu") (v "0.29.0") (h "0m63i7l2qb3ayd9n8vl19yy9rwbgv3q3lcmksbbhngjl37s1bmpk")))

(define-public crate-windows_x86_64_gnu-0.30.0 (c (n "windows_x86_64_gnu") (v "0.30.0") (h "0nid8amrlgbpq6ggcpj19xprxih3mzkm1q0rq6s8vm900b5lz9lc")))

(define-public crate-windows_x86_64_gnu-0.31.0 (c (n "windows_x86_64_gnu") (v "0.31.0") (h "14jrxg6kwakkl13jqqa5fgy3lg77jfinrgn1hh17286m238sqqsr")))

(define-public crate-windows_x86_64_gnu-0.32.0 (c (n "windows_x86_64_gnu") (v "0.32.0") (h "1g34xhcayig9sndq3555w95q6lr7jr839zxv6l365ijlfhpv24n9")))

(define-public crate-windows_x86_64_gnu-0.33.0 (c (n "windows_x86_64_gnu") (v "0.33.0") (h "1127n961nib9338n0g0sp1464v8wnw0hvmw45sr7pkly1q69ppdl")))

(define-public crate-windows_x86_64_gnu-0.34.0 (c (n "windows_x86_64_gnu") (v "0.34.0") (h "1x71512gic645ri51y6ivw1wb72h38agrvqrdlsqvvi7wbm6vkng")))

(define-public crate-windows_x86_64_gnu-0.35.0 (c (n "windows_x86_64_gnu") (v "0.35.0") (h "1fyy9s69yxpxryy5sgqsks93lxx1rg0d2r12rmryr459qlj0jg49")))

(define-public crate-windows_x86_64_gnu-0.36.0 (c (n "windows_x86_64_gnu") (v "0.36.0") (h "0xhl35zfyi7rj8n0q5nwxvnfpwd0ms8m2y7z6fq86hxag3vwds2i")))

(define-public crate-windows_x86_64_gnu-0.36.1 (c (n "windows_x86_64_gnu") (v "0.36.1") (h "1qfrck3jnihymfrd01s8260d4snql8ks2p8yaabipi3nhwdigkad")))

(define-public crate-windows_x86_64_gnu-0.37.0 (c (n "windows_x86_64_gnu") (v "0.37.0") (h "0ggk1ahj7b2rk4i76qjmxz77gdfbgm142lvx8lr6nblghahgparb")))

(define-public crate-windows_x86_64_gnu-0.38.0 (c (n "windows_x86_64_gnu") (v "0.38.0") (h "1wd9gq5r8vfak2dfsn0f2ylbghz7sy81k36b69ps70435hk5hn4h")))

(define-public crate-windows_x86_64_gnu-0.39.0 (c (n "windows_x86_64_gnu") (v "0.39.0") (h "0r9b4lmapq66nn2dga7a0mkdv5sgbp184kfwx3hklrbxcdjw2s38")))

(define-public crate-windows_x86_64_gnu-0.40.0 (c (n "windows_x86_64_gnu") (v "0.40.0") (h "1ira2rf4nzm2piq417xfi037zqcq50c828lxnij81nkjvmzg63sj")))

(define-public crate-windows_x86_64_gnu-0.41.0 (c (n "windows_x86_64_gnu") (v "0.41.0") (h "157rm1q3m6vsxzzmq953rp4624wnwwx96v89dlklwi6z3an5sfgw")))

(define-public crate-windows_x86_64_gnu-0.42.0 (c (n "windows_x86_64_gnu") (v "0.42.0") (h "1vdh8k5a4m6pfkc5gladqznyqxgapkjm0qb8iwqvqb1nnlhinyxz")))

(define-public crate-windows_x86_64_gnu-0.42.1 (c (n "windows_x86_64_gnu") (v "0.42.1") (h "0icwd66wd6p9hhzds93333wy2knlqbyiynf2rfy7xqc528i0y161")))

(define-public crate-windows_x86_64_gnu-0.42.2 (c (n "windows_x86_64_gnu") (v "0.42.2") (h "0dnbf2xnp3xrvy8v9mgs3var4zq9v9yh9kv79035rdgyp2w15scd")))

(define-public crate-windows_x86_64_gnu-0.47.0 (c (n "windows_x86_64_gnu") (v "0.47.0") (h "0hinb8zzbn0b8znfsp9lnx8hlhxfvg1s0263c6dzjbhdrz1a1jks")))

(define-public crate-windows_x86_64_gnu-0.48.0 (c (n "windows_x86_64_gnu") (v "0.48.0") (h "1cblz5m6a8q6ha09bz4lz233dnq5sw2hpra06k9cna3n3xk8laya")))

(define-public crate-windows_x86_64_gnu-0.48.2 (c (n "windows_x86_64_gnu") (v "0.48.2") (h "12l68p4ymsi9glknw6v9pb16il2v93n5lpih1n77mys9iczzz6ga")))

(define-public crate-windows_x86_64_gnu-0.48.3 (c (n "windows_x86_64_gnu") (v "0.48.3") (h "1bg7hxi6mms9ai1lifbf5m7dkszp5l0vqb3ik7j8s20aynh55z32")))

(define-public crate-windows_x86_64_gnu-0.48.4 (c (n "windows_x86_64_gnu") (v "0.48.4") (h "0zh2h3dlrl6l1s5gklyhpv47phs60kzqcadwbxrsjj4a059jlr9h")))

(define-public crate-windows_x86_64_gnu-0.48.5 (c (n "windows_x86_64_gnu") (v "0.48.5") (h "13kiqqcvz2vnyxzydjh73hwgigsdr2z1xpzx313kxll34nyhmm2k")))

(define-public crate-windows_x86_64_gnu-0.52.0 (c (n "windows_x86_64_gnu") (v "0.52.0") (h "1zdy4qn178sil5sdm63lm7f0kkcjg6gvdwmcprd2yjmwn8ns6vrx") (r "1.56")))

(define-public crate-windows_x86_64_gnu-0.52.1 (c (n "windows_x86_64_gnu") (v "0.52.1") (h "0qrkqzfzvxywh9s2ik4x6lpyx01nx8ld0iybs41184hw4jmnpi22") (y #t) (r "1.60")))

(define-public crate-windows_x86_64_gnu-0.52.3 (c (n "windows_x86_64_gnu") (v "0.52.3") (h "050n225hmbpvj7snhi6gisqqj9b3srvj4j23rpbqjgm93dbk2hbh") (r "1.60")))

(define-public crate-windows_x86_64_gnu-0.52.4 (c (n "windows_x86_64_gnu") (v "0.52.4") (h "00qs6x33bf9lai2q68faxl56cszbv7mf7zqlslmc1778j0ahkvjy") (r "1.56")))

(define-public crate-windows_x86_64_gnu-0.52.5 (c (n "windows_x86_64_gnu") (v "0.52.5") (h "1n8p2mcf3lw6300k77a0knksssmgwb9hynl793mhkzyydgvlchjf") (r "1.56")))

