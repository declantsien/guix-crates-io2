(define-module (crates-io wi nd windows-win32-ai-sys) #:use-module (crates-io))

(define-public crate-windows-win32-ai-sys-0.0.2 (c (n "windows-win32-ai-sys") (v "0.0.2") (h "1agw25m5vrq12i8kvf3w7p7akfgwy3rzhzi6j7wq37zxsidsxmyf") (r "1.46")))

(define-public crate-windows-win32-ai-sys-0.23.0 (c (n "windows-win32-ai-sys") (v "0.23.0") (h "00clww615ibz5xkg4g9xc9fva0z0fbccx0pzxwwbiral2fdg1apg")))

