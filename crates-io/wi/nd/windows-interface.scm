(define-module (crates-io wi nd windows-interface) #:use-module (crates-io))

(define-public crate-windows-interface-0.0.0 (c (n "windows-interface") (v "0.0.0") (h "0xxqb246s4zlly4akb987ygxg3xlbx6nw01yjq7w7wqsp21v90zj")))

(define-public crate-windows-interface-0.33.0 (c (n "windows-interface") (v "0.33.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0i0raqv8avqajriy26y69pj243zpl9rb9hl11m7ni18ahda52k3b")))

(define-public crate-windows-interface-0.34.0 (c (n "windows-interface") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1whji6q84dy6cnk7s1035fcvvw4vl621yfibkpp0rl4vddmxacpm")))

(define-public crate-windows-interface-0.35.0 (c (n "windows-interface") (v "0.35.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0pipn7q4lkwm4wdxhbvi3b2arc79hpxhmm524gdy9vfix6xvmv0q")))

(define-public crate-windows-interface-0.36.0 (c (n "windows-interface") (v "0.36.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "095i7ww4yp1qh83yxq3zkq9nc7jh3qq647gx8nrk0hqscjdqklmy")))

(define-public crate-windows-interface-0.36.1 (c (n "windows-interface") (v "0.36.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0vb1imyn6yaswfr0nw098dyawjrqzz3apmzb427lad4bhd913y0p")))

(define-public crate-windows-interface-0.37.0 (c (n "windows-interface") (v "0.37.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "09frbkwg0f0akq13cvkpwcq18p96h3h2rggxjw5hj4nwj00449p4")))

(define-public crate-windows-interface-0.38.0 (c (n "windows-interface") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "031rc4zwwqnnq1hjph9laam5g2blxn1cydvbwd94hbsssvibjgdk")))

(define-public crate-windows-interface-0.39.0 (c (n "windows-interface") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "00h56znmak3p8bh28y3s48m5zv6q7dn40vnvf3dzf0sz5rszrym2")))

(define-public crate-windows-interface-0.40.0 (c (n "windows-interface") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "06q1n4335a0bjvcrk3dd7ynlzyk8abxz2vbicrjma53kihkiazdg") (r "1.61")))

(define-public crate-windows-interface-0.41.0 (c (n "windows-interface") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "13ghjj4yzrk4s438ychlgiw5i2fygi060vd11gfqrqr17b279xgr") (r "1.61")))

(define-public crate-windows-interface-0.42.0 (c (n "windows-interface") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1jqzc7c9s5bcm1axii3arwbqjx00pssm4klgsbzd1nwngb2y9xrr") (r "1.61")))

(define-public crate-windows-interface-0.43.0 (c (n "windows-interface") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "06drakcacg4d680qa2sk62kqn7ini00xw3zz0hwqwx1my2z4z3dw") (r "1.64")))

(define-public crate-windows-interface-0.44.0 (c (n "windows-interface") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0zwwwfzjdf087gvgy48bbfq9yd0fsh1fj5wzs88gim7cj6jnjgw5") (r "1.64")))

(define-public crate-windows-interface-0.46.0 (c (n "windows-interface") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0a87zxh3wq5ng1vvgqf7jhydsshrpc5w39pyvr0l1vyv3q5k67xc") (r "1.64")))

(define-public crate-windows-interface-0.47.0 (c (n "windows-interface") (v "0.47.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1nnps7ra27hwm5w40lmclv9c5npy442lzgs71xp9isd8q1y6j0si") (r "1.64")))

(define-public crate-windows-interface-0.48.0 (c (n "windows-interface") (v "0.48.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1iqcilw0hfyzwhk12xfmcy40r10406sgf4xmdansijlv1kr8vyz6") (r "1.64")))

(define-public crate-windows-interface-0.51.0 (c (n "windows-interface") (v "0.51.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "113w6ymhln2ah0grp4xvi2x1sm5cnzfvdp4n88yigw0q65cg4r9j") (r "1.64")))

(define-public crate-windows-interface-0.51.1 (c (n "windows-interface") (v "0.51.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0xps1k3ii3cdiniv896mgcv3mbmm787gl4937m008k763hzfcih5") (r "1.64")))

(define-public crate-windows-interface-0.52.0 (c (n "windows-interface") (v "0.52.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1la254wzd8qlbxplvb667z5mwdh9jngg1qyhxg6fx9wm00pc73cx") (r "1.64")))

(define-public crate-windows-interface-0.53.0 (c (n "windows-interface") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "0q4bb5zigzr3083kwb7qkhx63dlymwx8gy6mw7jgm25281qmacys") (r "1.64")))

(define-public crate-windows-interface-0.56.0 (c (n "windows-interface") (v "0.56.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "derive"))) (k 0)))) (h "1k2prfxna0mw47f8gi8qhw9jfpw66bh2cqzs67sgipjfpx30b688") (r "1.62")))

