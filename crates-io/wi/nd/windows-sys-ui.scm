(define-module (crates-io wi nd windows-sys-ui) #:use-module (crates-io))

(define-public crate-windows-sys-ui-0.22.5 (c (n "windows-sys-ui") (v "0.22.5") (h "13a546slzk0xzg86123j546jki0cy1n2znsh6icphd0h2xz0jqmd") (r "1.46")))

(define-public crate-windows-sys-ui-0.22.6 (c (n "windows-sys-ui") (v "0.22.6") (h "10384pzrkz9zm2awpxzjb0rl87vi4bmqyfa0z57ldw5ziyjr50wh") (r "1.46")))

(define-public crate-windows-sys-ui-0.23.0 (c (n "windows-sys-ui") (v "0.23.0") (h "0fq6nrr3km3cgr89vba06ywp0cp594kckf0dn8ywa3alyjfjlwmh")))

