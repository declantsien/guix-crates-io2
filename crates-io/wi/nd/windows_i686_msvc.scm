(define-module (crates-io wi nd windows_i686_msvc) #:use-module (crates-io))

(define-public crate-windows_i686_msvc-0.22.1 (c (n "windows_i686_msvc") (v "0.22.1") (h "0nrnyx2qi7dq3gz34gw1674bqrg9067a90lwh14svk70m8c0ycr3")))

(define-public crate-windows_i686_msvc-0.23.0 (c (n "windows_i686_msvc") (v "0.23.0") (h "06dr3jw1b0bqh58779absyacxaqx2a17vbh443r3si5yadmim4b5")))

(define-public crate-windows_i686_msvc-0.24.0 (c (n "windows_i686_msvc") (v "0.24.0") (h "1ilmbcklljd27bzjr7crb3mlisnfmqxaplkqj2i6k4vydgazw3xz")))

(define-public crate-windows_i686_msvc-0.25.0 (c (n "windows_i686_msvc") (v "0.25.0") (h "1mw9qflkx4rfijg4h0wxcf7icifcn1id8mzcjycl5bg9j5jz37gl")))

(define-public crate-windows_i686_msvc-0.26.0 (c (n "windows_i686_msvc") (v "0.26.0") (h "1fk03y3y43sd9w8l67dqbgrn1jzcmpkjds9a25132x24p6rlz623")))

(define-public crate-windows_i686_msvc-0.27.0 (c (n "windows_i686_msvc") (v "0.27.0") (h "13priyw52alwm6rmcsk2hi9nzf093xpf2zgbha4xrp74yf71scs0")))

(define-public crate-windows_i686_msvc-0.28.0 (c (n "windows_i686_msvc") (v "0.28.0") (h "0r0z8s1wcdwd20azsdfilf2a6bz68xkavl990wy64hyc8f51bmai")))

(define-public crate-windows_i686_msvc-0.29.0 (c (n "windows_i686_msvc") (v "0.29.0") (h "1r3ag5b8gaq1hkv5mizl2yj3pplmz7si5icn56z28w5n332gc0l6")))

(define-public crate-windows_i686_msvc-0.30.0 (c (n "windows_i686_msvc") (v "0.30.0") (h "19jlb6amw8rnlg71pbsbqk464hyd749iq5yv34qbfls71lx9x86l")))

(define-public crate-windows_i686_msvc-0.31.0 (c (n "windows_i686_msvc") (v "0.31.0") (h "0jf0q6536vniiv9ibzanacv5iamwjb7n9s8c7imgg6f5x2kwas3h")))

(define-public crate-windows_i686_msvc-0.32.0 (c (n "windows_i686_msvc") (v "0.32.0") (h "0wj1wi01fc8hrasbakjcq8y5a7ynw9l2mcw08svmsq823axi2v0l")))

(define-public crate-windows_i686_msvc-0.33.0 (c (n "windows_i686_msvc") (v "0.33.0") (h "1l3kwxgdfg4lnx2j5bkcx6cnvhxnpcsbqjm3idhwxmwsrj4vxzcc")))

(define-public crate-windows_i686_msvc-0.34.0 (c (n "windows_i686_msvc") (v "0.34.0") (h "0mk92rzdvjks01v8d5dkh1yp9syf9f0khkf168im4lq4lwmx7ncw")))

(define-public crate-windows_i686_msvc-0.35.0 (c (n "windows_i686_msvc") (v "0.35.0") (h "1r0278g7mayykl94sxf2bqkqcw9l8kkzx1my06jygn33rbscpk8s")))

(define-public crate-windows_i686_msvc-0.36.0 (c (n "windows_i686_msvc") (v "0.36.0") (h "1vyndw50pw3db428779scsy3hi13maq7s7pr25kqg0zjpy8yw33s")))

(define-public crate-windows_i686_msvc-0.36.1 (c (n "windows_i686_msvc") (v "0.36.1") (h "097h2a7wig04wbmpi3rz1akdy4s8gslj5szsx8g2v0dj91qr3rz2")))

(define-public crate-windows_i686_msvc-0.37.0 (c (n "windows_i686_msvc") (v "0.37.0") (h "0z650xnnk0r0vdsf5mc4phqv12srpzpq71i9q4jbacg39z3pm46f")))

(define-public crate-windows_i686_msvc-0.38.0 (c (n "windows_i686_msvc") (v "0.38.0") (h "1zx3n06k71bnspms6ibmpiy10zgszi0shsral6v3m1pjw2dmdw6d")))

(define-public crate-windows_i686_msvc-0.39.0 (c (n "windows_i686_msvc") (v "0.39.0") (h "01hiv9msxssy5iqxs7bczvf094k4mz56yi4z1bhj32c2b3zcpivv")))

(define-public crate-windows_i686_msvc-0.40.0 (c (n "windows_i686_msvc") (v "0.40.0") (h "0xnqs15hknjqxm75qbjz2fgp9jw9fhyx9n4ml4glk60s3h1x86ia")))

(define-public crate-windows_i686_msvc-0.41.0 (c (n "windows_i686_msvc") (v "0.41.0") (h "1xsy70vjcfs811l0agg54lgkh1w59lmxfzghcwr2y7sp6il9d2jn")))

(define-public crate-windows_i686_msvc-0.42.0 (c (n "windows_i686_msvc") (v "0.42.0") (h "0ii2hrsdif2ms79dfiyfzm1n579jzj42ji3fpsxd57d3v9jjzhc4")))

(define-public crate-windows_i686_msvc-0.42.1 (c (n "windows_i686_msvc") (v "0.42.1") (h "01966w2707qrw183lqg62i1as614i88m5a0s6pzxdpby64i12kdz")))

(define-public crate-windows_i686_msvc-0.42.2 (c (n "windows_i686_msvc") (v "0.42.2") (h "0q0h9m2aq1pygc199pa5jgc952qhcnf0zn688454i7v4xjv41n24")))

(define-public crate-windows_i686_msvc-0.47.0 (c (n "windows_i686_msvc") (v "0.47.0") (h "0cbbxk8fgnizzjv5iyrhr59f997mdicrpdnr8wwj46viy7rgi3is")))

(define-public crate-windows_i686_msvc-0.48.0 (c (n "windows_i686_msvc") (v "0.48.0") (h "004fkyqv3if178xx9ksqc4qqv8sz8n72mpczsr2vy8ffckiwchj5")))

(define-public crate-windows_i686_msvc-0.48.2 (c (n "windows_i686_msvc") (v "0.48.2") (h "11rjzvvgzagki9488rjvfz3ifbmw8a18zlf5vzp99ha0v3i5c2b0")))

(define-public crate-windows_i686_msvc-0.48.3 (c (n "windows_i686_msvc") (v "0.48.3") (h "1i0s73glfax5jnm4ghpzl0w011s3ixn050lzlak2wpgzzzhf0kr2")))

(define-public crate-windows_i686_msvc-0.48.4 (c (n "windows_i686_msvc") (v "0.48.4") (h "0w9zgap572s2c1ry19il1sqkyijgask7x5vp77376isbzb89cbln")))

(define-public crate-windows_i686_msvc-0.48.5 (c (n "windows_i686_msvc") (v "0.48.5") (h "01m4rik437dl9rdf0ndnm2syh10hizvq0dajdkv2fjqcywrw4mcg")))

(define-public crate-windows_i686_msvc-0.52.0 (c (n "windows_i686_msvc") (v "0.52.0") (h "16kvmbvx0vr0zbgnaz6nsks9ycvfh5xp05bjrhq65kj623iyirgz") (r "1.56")))

(define-public crate-windows_i686_msvc-0.52.1 (c (n "windows_i686_msvc") (v "0.52.1") (h "12x7jhy13clic8lfkdpmlyj5v0jnarj22p0kgr52gbjm9ccsr01l") (y #t) (r "1.60")))

(define-public crate-windows_i686_msvc-0.52.3 (c (n "windows_i686_msvc") (v "0.52.3") (h "0n1c4dgwmxqca1kwwniav9qi79fldbx5qxbq9brmza9c8affrc18") (r "1.60")))

(define-public crate-windows_i686_msvc-0.52.4 (c (n "windows_i686_msvc") (v "0.52.4") (h "00lfzw88dkf3fdcf2hpfhp74i9pwbp7rwnj1nhy79vavksifj58m") (r "1.56")))

(define-public crate-windows_i686_msvc-0.52.5 (c (n "windows_i686_msvc") (v "0.52.5") (h "1gw7fklxywgpnwbwg43alb4hm0qjmx72hqrlwy5nanrxs7rjng6v") (r "1.56")))

