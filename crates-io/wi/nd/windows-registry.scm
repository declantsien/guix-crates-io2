(define-module (crates-io wi nd windows-registry) #:use-module (crates-io))

(define-public crate-windows-registry-0.0.0 (c (n "windows-registry") (v "0.0.0") (h "1giqb5fkgy8s6whh8vkk3qs828k8lw87yn1x29vy8hadwbybwj8w") (r "1.60")))

(define-public crate-windows-registry-0.1.0 (c (n "windows-registry") (v "0.1.0") (d (list (d (n "windows-bindgen") (r "^0.53.0") (d #t) (k 2)) (d (n "windows-result") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.1") (d #t) (k 0)))) (h "070ck1hjczbyz3fxd625bj15iyb2zz0yly0v8pkhgbcczwnmv21y") (r "1.60")))

(define-public crate-windows-registry-0.1.1 (c (n "windows-registry") (v "0.1.1") (d (list (d (n "windows-bindgen") (r "^0.56.0") (d #t) (k 2)) (d (n "windows-result") (r "^0.1.1") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.5") (d #t) (k 0)))) (h "1kw1c9qqhy0f0pq52fyk4fq1yj62fv5lb9cmlfhhddggalpbq8gp") (r "1.60")))

