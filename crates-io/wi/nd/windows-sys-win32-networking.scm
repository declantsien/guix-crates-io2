(define-module (crates-io wi nd windows-sys-win32-networking) #:use-module (crates-io))

(define-public crate-windows-sys-win32-networking-0.22.5 (c (n "windows-sys-win32-networking") (v "0.22.5") (h "0jabqf65a2b41n6yjy28d927b1j8q4pr03sbqk5lpqjf8x34w84p") (r "1.46")))

(define-public crate-windows-sys-win32-networking-0.22.6 (c (n "windows-sys-win32-networking") (v "0.22.6") (h "0xa5mmf0457w76al7fipylc58qsb2lji5yk382f6x40yvp3mwl8z") (r "1.46")))

(define-public crate-windows-sys-win32-networking-0.23.0 (c (n "windows-sys-win32-networking") (v "0.23.0") (h "012xylmap37rwff6hr1w5ifip4m7l0l9hlb5dvhiy1w6x91cf9fk")))

