(define-module (crates-io wi nd windows-win32-networkmanagement-sys) #:use-module (crates-io))

(define-public crate-windows-win32-networkmanagement-sys-0.0.2 (c (n "windows-win32-networkmanagement-sys") (v "0.0.2") (h "0yp0zpgd5ngkks41xpnylck1ik6bw61qxak7pci6iwxgjhfzv9nl") (r "1.46")))

(define-public crate-windows-win32-networkmanagement-sys-0.23.0 (c (n "windows-win32-networkmanagement-sys") (v "0.23.0") (h "0g9ah91rbjc003jrxnvv2cpn74aplx38q50p6a4rbl49a54xxz5q")))

