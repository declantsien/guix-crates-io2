(define-module (crates-io wi nd windows-sys-media) #:use-module (crates-io))

(define-public crate-windows-sys-media-0.22.5 (c (n "windows-sys-media") (v "0.22.5") (h "0xr4am46nsnmkjz750hnfm71rmyqxp1vsh70609ig4wrbwn4p91f") (r "1.46")))

(define-public crate-windows-sys-media-0.22.6 (c (n "windows-sys-media") (v "0.22.6") (h "1bbdl6ndjfwgj0d64v7ax0fsxwzqlxb5awv8k281cf7j93zifcdz") (r "1.46")))

(define-public crate-windows-sys-media-0.23.0 (c (n "windows-sys-media") (v "0.23.0") (h "0yh7jldajg60yk7a135rx0g58wn6wsyspckxbc05nc2xqg154ha4")))

