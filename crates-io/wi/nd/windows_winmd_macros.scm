(define-module (crates-io wi nd windows_winmd_macros) #:use-module (crates-io))

(define-public crate-windows_winmd_macros-0.1.0 (c (n "windows_winmd_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05cjwza1650k8ahfdg2qdqqjclnkysbjyqg5670i0ijx5vl4p30z")))

(define-public crate-windows_winmd_macros-0.1.1 (c (n "windows_winmd_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbxs3r3dd41wy9w0l6rm15x3wb4hznpm206d6mbxdhv3i3ck996")))

(define-public crate-windows_winmd_macros-0.1.2 (c (n "windows_winmd_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15aga3xj2rk86a5ghy8lm5h86209f4n7vf7hcli1s4bfxczgkng6")))

(define-public crate-windows_winmd_macros-0.1.3 (c (n "windows_winmd_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rd3wxwmm411hpwq3gpbacm0r4fngk0rffhfsks0bz7lqzs51w88")))

(define-public crate-windows_winmd_macros-0.1.4 (c (n "windows_winmd_macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xgs2azckl1nx1ydaqa2q2pvvd54rga2k920x2qh5z4046b7bd09")))

(define-public crate-windows_winmd_macros-0.2.1 (c (n "windows_winmd_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nb51pjps4aaiaj75k946rl0s6fwp0rvxfkshjlfmgqv1l1cjzpn")))

(define-public crate-windows_winmd_macros-0.3.0 (c (n "windows_winmd_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nlgpl5dnwp2nkc0i7s6nfdfa466s50xy4w3ccphyx54snr34qf1")))

(define-public crate-windows_winmd_macros-0.3.1 (c (n "windows_winmd_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0diz7qq73rgyf22chwgxhhxsqcfbnvranxm12h34af19a4zz8lr3")))

