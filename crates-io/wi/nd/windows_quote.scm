(define-module (crates-io wi nd windows_quote) #:use-module (crates-io))

(define-public crate-windows_quote-0.19.0 (c (n "windows_quote") (v "0.19.0") (h "1773n3mvaa58zwy4grsfvyji499lifri3pawz5aqihwkjim27y2a")))

(define-public crate-windows_quote-0.20.0 (c (n "windows_quote") (v "0.20.0") (h "0rrp3m14mibsz2anmq5la7wqc2g7h2brpjk5yigkc2jvrc7aw5kd")))

(define-public crate-windows_quote-0.20.1 (c (n "windows_quote") (v "0.20.1") (h "04s90rd6mfh8fk2qanmf6bsn06xw5v4vd8iyzain8qcan62j3yji")))

(define-public crate-windows_quote-0.21.0 (c (n "windows_quote") (v "0.21.0") (h "14vhgn1j2j7iw3i8whgyqnc8h1j2mpk5cjmi8jsl9wl5bk0bhzki")))

(define-public crate-windows_quote-0.21.1 (c (n "windows_quote") (v "0.21.1") (h "0kby78b891lc3hh1mmyginvzhgpdydw8z116facy25cc52sqgybw")))

(define-public crate-windows_quote-0.22.0 (c (n "windows_quote") (v "0.22.0") (h "0mf999d1w0a7vs0ymfldv5gksnyjqdyfsbny9a5mdiivf0gm1819")))

(define-public crate-windows_quote-0.22.1 (c (n "windows_quote") (v "0.22.1") (h "0szhd469icdgvif48pgk1icvgalqra8n037vay4140mg8h3gwqmh")))

(define-public crate-windows_quote-0.23.0 (c (n "windows_quote") (v "0.23.0") (h "0cj5indy6h1fbhc8spj8sb8iq93ifms2cyyd1j9fg17ds552ymys")))

(define-public crate-windows_quote-0.24.0 (c (n "windows_quote") (v "0.24.0") (h "1l5sr61bfd8rhmm509jnrmm2b95bsxsbj1wqmjrvfarym9lkx6vc")))

(define-public crate-windows_quote-0.25.0 (c (n "windows_quote") (v "0.25.0") (h "0nl3ahqp9icaa2zic7zggb5zyph3mp9i9i7x2wiky0fjbn6xy574")))

(define-public crate-windows_quote-0.26.0 (c (n "windows_quote") (v "0.26.0") (h "0nzb3an8911qh020l1bm3xyp713bnq0ss5f0w41y6rkfj3sk52zr")))

(define-public crate-windows_quote-0.27.0 (c (n "windows_quote") (v "0.27.0") (h "0k9sva3pncdnl4afjnwgm3bbswfh9vwrb5mcv9yclfrnphgjq9pq")))

(define-public crate-windows_quote-0.28.0 (c (n "windows_quote") (v "0.28.0") (h "1sh1c1cglf82qakggd9584v9i5jpn8bz6f8krg1ssw31zsccczcd")))

(define-public crate-windows_quote-0.29.0 (c (n "windows_quote") (v "0.29.0") (h "0g42bwxhcn78f4apkqkb6nf0l7dpa448lzjs24xxr4f3swh3zn0x")))

(define-public crate-windows_quote-0.30.0 (c (n "windows_quote") (v "0.30.0") (h "0rv60g9szwyp32qjkqh5aah50bf308495aka430lhqkylx8jrw3i")))

