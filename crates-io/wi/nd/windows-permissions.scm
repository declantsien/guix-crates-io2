(define-module (crates-io wi nd windows-permissions) #:use-module (crates-io))

(define-public crate-windows-permissions-0.1.0 (c (n "windows-permissions") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.6") (f (quote ("aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "0w85i3zak68d2jamz6girp2pzd1hhyy7cg076i84nhglj6xfcwb9")))

(define-public crate-windows-permissions-0.1.1 (c (n "windows-permissions") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.6") (f (quote ("aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "0j52k9q4qbh9hi8z5xsh3x6wf67jzfcy0fqa9r4n7z9zsjm3kwcy")))

(define-public crate-windows-permissions-0.1.2 (c (n "windows-permissions") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.6") (f (quote ("aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "1rj1b53zg00wkf1ynsq7fipkq6hmj1k03h54iynqahbdwsrxj2y2")))

(define-public crate-windows-permissions-0.2.0 (c (n "windows-permissions") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "0hf5pb4gk03x7kcsmyfwv84wrs9fm99m769yghzfbz61hqy1zi9d")))

(define-public crate-windows-permissions-0.2.1 (c (n "windows-permissions") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "12hycfhmjisgrwma54y7xxzhpxvm9bmrl7ady3r8jzhapcfhpjrr")))

(define-public crate-windows-permissions-0.2.2 (c (n "windows-permissions") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "0mmh842pw04kp26wpcfqcacn0jvnk4cq5l876rjig6jp9b590wg1")))

(define-public crate-windows-permissions-0.2.3 (c (n "windows-permissions") (v "0.2.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "1gb468k0pp2fw80q973s1islzl0jbykbydalgbl7f03xvz86xvk0")))

(define-public crate-windows-permissions-0.2.4 (c (n "windows-permissions") (v "0.2.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "aclapi" "handleapi" "sddl" "securitybaseapi" "winerror" "winnt"))) (d #t) (k 0)))) (h "1gpr9ffw1zd5ibml1695plmf9n08vpx666q39q4llkdzqv1wsb4y")))

