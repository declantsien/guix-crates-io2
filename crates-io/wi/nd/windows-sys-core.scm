(define-module (crates-io wi nd windows-sys-core) #:use-module (crates-io))

(define-public crate-windows-sys-core-0.0.0 (c (n "windows-sys-core") (v "0.0.0") (h "19m4x9gzx1vbgh6hdmkbbdvw2rmsqasjbwxgk7ycgcyxzbxskpy7") (r "1.46")))

(define-public crate-windows-sys-core-0.23.0 (c (n "windows-sys-core") (v "0.23.0") (h "1a2ph8mn4lbyx07v16akbmcbznfv91kydfm01k0r2xhks2d0bj4i")))

