(define-module (crates-io wi nd windows-win32-gaming-sys) #:use-module (crates-io))

(define-public crate-windows-win32-gaming-sys-0.0.2 (c (n "windows-win32-gaming-sys") (v "0.0.2") (h "0ai1wrifapzmigagdh8r4mzwb2rq7647xa62d1j8rkdyzh88mf69") (r "1.46")))

(define-public crate-windows-win32-gaming-sys-0.23.0 (c (n "windows-win32-gaming-sys") (v "0.23.0") (h "1002ap9bssay9a4n1vxm70l2l7sicnjjpkjj1fpw8dfgz0m5707h")))

