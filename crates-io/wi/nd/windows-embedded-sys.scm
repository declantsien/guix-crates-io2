(define-module (crates-io wi nd windows-embedded-sys) #:use-module (crates-io))

(define-public crate-windows-embedded-sys-0.0.0 (c (n "windows-embedded-sys") (v "0.0.0") (h "13xngp4dnkqhgnzqmwmivxzr7p212aj425pwpfxxw0hja0fsxxmq") (r "1.46")))

(define-public crate-windows-embedded-sys-0.0.1 (c (n "windows-embedded-sys") (v "0.0.1") (h "039278jxkxjh00jarl4dg853lpj5namwkgdmgxiv6wncczc11vw4") (r "1.46")))

(define-public crate-windows-embedded-sys-0.0.2 (c (n "windows-embedded-sys") (v "0.0.2") (h "03jqci0wi4b3azb778nzgn8xkwrv4ghabf4apiw068bl9fwfm04i") (r "1.46")))

(define-public crate-windows-embedded-sys-0.1.0 (c (n "windows-embedded-sys") (v "0.1.0") (h "0c986nxgjfv3280gnfaffk7lj18y2ck7h3d4zd8d900fsdh57i8g")))

