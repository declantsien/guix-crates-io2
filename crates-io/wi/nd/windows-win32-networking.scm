(define-module (crates-io wi nd windows-win32-networking) #:use-module (crates-io))

(define-public crate-windows-win32-networking-0.22.0 (c (n "windows-win32-networking") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "18yb24g5hq5h6jkljb0qcs76rq84v10wm290a5fh3xz2nqv4fg1d")))

(define-public crate-windows-win32-networking-0.22.3 (c (n "windows-win32-networking") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1fb6yx6585i2bpajlnvih2ha07yik5vxyrmf4578pgj4h9ww35bm")))

(define-public crate-windows-win32-networking-0.22.5 (c (n "windows-win32-networking") (v "0.22.5") (h "072jmgg8sh8q2ighm2i8hlh3s6qrhw57wrbz46bjqzpqgasgqxwx") (r "1.46")))

(define-public crate-windows-win32-networking-0.22.6 (c (n "windows-win32-networking") (v "0.22.6") (h "04mdi1sknswkkbigj052g76m59rr9zjl4nxz6m9zjgxnlvp0j7zp") (r "1.46")))

(define-public crate-windows-win32-networking-0.23.0 (c (n "windows-win32-networking") (v "0.23.0") (h "1knddd7l0cqadhfmqlpgf0cy349p5igp45ysgl5wcbfnial7hlwa")))

