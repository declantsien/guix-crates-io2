(define-module (crates-io wi nd windows-graphics) #:use-module (crates-io))

(define-public crate-windows-graphics-0.7.0 (c (n "windows-graphics") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "06y6risn5was2ygzwsxh9x2xblr69rzz5fkdf0dcq88nzr1ydw64")))

(define-public crate-windows-graphics-0.22.0 (c (n "windows-graphics") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0vp8956msv9pq3pw7smyyxnc7lc717d620wkbc94sr79il3v1ppd")))

(define-public crate-windows-graphics-0.22.1 (c (n "windows-graphics") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "0l2w6g2v9mag94vspimiwjyf9sq9y8qq6p0nlivhpmbymdfd3vvc")))

(define-public crate-windows-graphics-0.22.2 (c (n "windows-graphics") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "18akqxqwwac4a82dpdrw6a4b2p53m0dlm6rfj9gzmzndskd604lf")))

(define-public crate-windows-graphics-0.22.3 (c (n "windows-graphics") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "16rfcc65zc61a1fmfhgxdxrb8hhscnyaqrpp1nbwnr1qgh52rlr3")))

(define-public crate-windows-graphics-0.22.5 (c (n "windows-graphics") (v "0.22.5") (h "0cgaz7clgl8khrc3xqb4c5s8ak0av64mjmwlsff5ddc4fc6v9yfv") (r "1.46")))

(define-public crate-windows-graphics-0.22.6 (c (n "windows-graphics") (v "0.22.6") (h "00by93xagq10227pqqfmkhzz601s22hqqyjd853f8nfj7s582pzi") (r "1.46")))

(define-public crate-windows-graphics-0.1.0 (c (n "windows-graphics") (v "0.1.0") (h "1b1gn7hkl94yz8vkgvyswblgb0la8892bpz8rn3pyyajnzgp8zms")))

(define-public crate-windows-graphics-0.23.0 (c (n "windows-graphics") (v "0.23.0") (h "16m9j8fjlkqmrj6ycrrpihyq5jmj9iz48accpp45b6y6zkcnfi20")))

