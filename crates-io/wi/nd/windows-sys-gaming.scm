(define-module (crates-io wi nd windows-sys-gaming) #:use-module (crates-io))

(define-public crate-windows-sys-gaming-0.22.4 (c (n "windows-sys-gaming") (v "0.22.4") (h "006wr3kxdha3fxkns16fgfw3pw1das0bspj13r0i1wh4aqjcvxw3") (r "1.46")))

(define-public crate-windows-sys-gaming-0.22.5 (c (n "windows-sys-gaming") (v "0.22.5") (h "0lbii9lvcgjds2mpz0wak643syl6d7nm22fc42l16ib8dqg2pg24") (r "1.46")))

(define-public crate-windows-sys-gaming-0.22.6 (c (n "windows-sys-gaming") (v "0.22.6") (h "1cqmgw284mb6asn5mavy5y8blakg07iy66xms4c67n9zhkzx3nic") (r "1.46")))

(define-public crate-windows-sys-gaming-0.23.0 (c (n "windows-sys-gaming") (v "0.23.0") (h "1365h136002xrwgn17839wchyyg3ppirr7q68mqix3fbzsdwgnqd")))

