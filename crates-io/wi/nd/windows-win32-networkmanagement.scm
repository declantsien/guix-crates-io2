(define-module (crates-io wi nd windows-win32-networkmanagement) #:use-module (crates-io))

(define-public crate-windows-win32-networkmanagement-0.22.0 (c (n "windows-win32-networkmanagement") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0k7q434kdd7z3gn8lc2wbs94izdcn78gw8qdcl8sq16dab6k6b2f")))

(define-public crate-windows-win32-networkmanagement-0.22.3 (c (n "windows-win32-networkmanagement") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "16ail2y0fkcf51kfqddbrwvq18igwvajlf50f3f130n2lnyfafy1")))

(define-public crate-windows-win32-networkmanagement-0.22.5 (c (n "windows-win32-networkmanagement") (v "0.22.5") (h "0m6l1nhas1s4pxvj59h2vh80hkk8lrl13vk2fjpypgw2k2sybaa4") (r "1.46")))

(define-public crate-windows-win32-networkmanagement-0.22.6 (c (n "windows-win32-networkmanagement") (v "0.22.6") (h "0svm0d09xzk4phhdnl9d40chzjzhbvap34a4n6gpq2irl6vq81hn") (r "1.46")))

(define-public crate-windows-win32-networkmanagement-0.23.0 (c (n "windows-win32-networkmanagement") (v "0.23.0") (h "03sg482iswq35qgpghm8qzfz4vmx0rp0szk74wqn8wpcm2g7lzsa")))

