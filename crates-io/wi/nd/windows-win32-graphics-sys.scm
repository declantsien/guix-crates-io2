(define-module (crates-io wi nd windows-win32-graphics-sys) #:use-module (crates-io))

(define-public crate-windows-win32-graphics-sys-0.0.2 (c (n "windows-win32-graphics-sys") (v "0.0.2") (h "1k691w58rps3m3f4pn4gvh10dzzkl13q0zwi3rfyhg2pxkz8s4y3") (r "1.46")))

(define-public crate-windows-win32-graphics-sys-0.23.0 (c (n "windows-win32-graphics-sys") (v "0.23.0") (h "0xy2s8w81ms0irz2yh84bk0kpz8fx3g4dkj4k56nqc8hw5xlc1wq")))

