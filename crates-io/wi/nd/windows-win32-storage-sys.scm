(define-module (crates-io wi nd windows-win32-storage-sys) #:use-module (crates-io))

(define-public crate-windows-win32-storage-sys-0.0.2 (c (n "windows-win32-storage-sys") (v "0.0.2") (h "0sddsa34apxsmbvm9283pw7l3ddfi956cs0mizb3slp2v3y68f04") (r "1.46")))

(define-public crate-windows-win32-storage-sys-0.23.0 (c (n "windows-win32-storage-sys") (v "0.23.0") (h "0j48dm73y527sqip2c7agdkglya325inwvkvv8vy3zwh3y7crzaj")))

