(define-module (crates-io wi nd windows_x86_64_msvc) #:use-module (crates-io))

(define-public crate-windows_x86_64_msvc-0.22.1 (c (n "windows_x86_64_msvc") (v "0.22.1") (h "0a4ns025l6i26zywf2bvv3vrgi9sqq5f6fyi89fqv943zrp3y9dy")))

(define-public crate-windows_x86_64_msvc-0.23.0 (c (n "windows_x86_64_msvc") (v "0.23.0") (h "0qwyd236xvwf1wngcq7xcbgwahc2mp5h6mfpymplxg4zf0nqx34c")))

(define-public crate-windows_x86_64_msvc-0.24.0 (c (n "windows_x86_64_msvc") (v "0.24.0") (h "16fk46ri9pzfyg44gj35m6rl7i8jps89mmsrh4slcm6av1ig1n4v")))

(define-public crate-windows_x86_64_msvc-0.25.0 (c (n "windows_x86_64_msvc") (v "0.25.0") (h "07q6ias7yqr8s60rzr1cqybjpxwrbivi8v73d4syvnrxng5pphn3")))

(define-public crate-windows_x86_64_msvc-0.26.0 (c (n "windows_x86_64_msvc") (v "0.26.0") (h "13rrr0y7h4369rvx9ql5y8za3h9g41zqkn1j5fnh5kx036nds96w")))

(define-public crate-windows_x86_64_msvc-0.27.0 (c (n "windows_x86_64_msvc") (v "0.27.0") (h "0pv3l7bbh582q4vnizjnxkkhvbrp7ik1n64afpaflasfximbgqfy")))

(define-public crate-windows_x86_64_msvc-0.28.0 (c (n "windows_x86_64_msvc") (v "0.28.0") (h "17z8q25pd3dp6b84qm9nlayd3ym78sbryxlqmgcxvz9vpmy8qarz")))

(define-public crate-windows_x86_64_msvc-0.29.0 (c (n "windows_x86_64_msvc") (v "0.29.0") (h "0q9m9brirmxh6jpnzjk54xxnpi5504raakc8h3174dpdv4hm9n8i")))

(define-public crate-windows_x86_64_msvc-0.30.0 (c (n "windows_x86_64_msvc") (v "0.30.0") (h "1wdmxdz63n0qbp3ywahxcs5y7z3firkc38f69gpqz13602gvrjh8")))

(define-public crate-windows_x86_64_msvc-0.31.0 (c (n "windows_x86_64_msvc") (v "0.31.0") (h "0j260z0d6m1fd2r3vd2pm5rrc331c5wn0hfbk7f0jdzqqsbfz1lq")))

(define-public crate-windows_x86_64_msvc-0.32.0 (c (n "windows_x86_64_msvc") (v "0.32.0") (h "05l392h518dxn808dc1zkv6d0r9z38q68qqc0ix9fs9741v28jjh")))

(define-public crate-windows_x86_64_msvc-0.33.0 (c (n "windows_x86_64_msvc") (v "0.33.0") (h "1akf81g0bh8mv8wjpiifd819r0hx3r0xrz9zgzn4hl298sk4l7pz")))

(define-public crate-windows_x86_64_msvc-0.34.0 (c (n "windows_x86_64_msvc") (v "0.34.0") (h "1scxv2b9id3gj6bvpzdax496lng6x8bnm3gqx8fx068qqb63i5fi")))

(define-public crate-windows_x86_64_msvc-0.35.0 (c (n "windows_x86_64_msvc") (v "0.35.0") (h "0jzzgspwm7xhza9jdzby9np2zgbq6glz5zd8k4n8574868gxd6x2")))

(define-public crate-windows_x86_64_msvc-0.36.0 (c (n "windows_x86_64_msvc") (v "0.36.0") (h "0a8gf3bxhmf5p8xbn8px22hwhap4zhljf6a7vgj32hzan6qaimhx")))

(define-public crate-windows_x86_64_msvc-0.36.1 (c (n "windows_x86_64_msvc") (v "0.36.1") (h "103n3xijm5vr7qxr1dps202ckfnv7njjnnfqmchg8gl5ii5cl4f8")))

(define-public crate-windows_x86_64_msvc-0.37.0 (c (n "windows_x86_64_msvc") (v "0.37.0") (h "1795abfwjzq4rnx675lfjj0igyqqcfwdabl26dxwz11dvz3nvpgl")))

(define-public crate-windows_x86_64_msvc-0.38.0 (c (n "windows_x86_64_msvc") (v "0.38.0") (h "0q85fck774sx33x74vczsxnxqr9ndqclfpvhiyrzyhfl85ikq349")))

(define-public crate-windows_x86_64_msvc-0.39.0 (c (n "windows_x86_64_msvc") (v "0.39.0") (h "02g8fy1sv8g0p4fi2yk62j9a5zwhcfknp9vwg1ifkjp97a440kay")))

(define-public crate-windows_x86_64_msvc-0.40.0 (c (n "windows_x86_64_msvc") (v "0.40.0") (h "0a6szrdzslz4vw21yqgkks0rrv6qpm2fy70kdskvzlaxymnrq31a")))

(define-public crate-windows_x86_64_msvc-0.41.0 (c (n "windows_x86_64_msvc") (v "0.41.0") (h "09krzmz8x1ckf8yc9kf5c6zpkrsybvigc4hmvwcr0rkb47q635wb")))

(define-public crate-windows_x86_64_msvc-0.42.0 (c (n "windows_x86_64_msvc") (v "0.42.0") (h "1xdnvhg8yj4fgjy0vkrahq5cbgfpcd7ak2bdv8s5lwjrazc0j07l")))

(define-public crate-windows_x86_64_msvc-0.42.1 (c (n "windows_x86_64_msvc") (v "0.42.1") (h "1zfgzsmc0j5p9yx25j15lb1yh45mazl4hhjdvfqqhcm16snn0xj4")))

(define-public crate-windows_x86_64_msvc-0.42.2 (c (n "windows_x86_64_msvc") (v "0.42.2") (h "1w5r0q0yzx827d10dpjza2ww0j8iajqhmb54s735hhaj66imvv4s")))

(define-public crate-windows_x86_64_msvc-0.47.0 (c (n "windows_x86_64_msvc") (v "0.47.0") (h "1rxw23liaz0y4iskcwnv747fcx6pz1yz2zbhr2w40vfwav164vlx")))

(define-public crate-windows_x86_64_msvc-0.48.0 (c (n "windows_x86_64_msvc") (v "0.48.0") (h "12ipr1knzj2rwjygyllfi5mkd0ihnbi3r61gag5n2jgyk5bmyl8s")))

(define-public crate-windows_x86_64_msvc-0.48.2 (c (n "windows_x86_64_msvc") (v "0.48.2") (h "1fg7lrqyphzky7v9dfqqb11bh2zsxk3dgri9dsb67dhnpad2a6fl")))

(define-public crate-windows_x86_64_msvc-0.48.3 (c (n "windows_x86_64_msvc") (v "0.48.3") (h "1wdm60vwm0qdq29sfjx39lh5h6hacsx597aalcmaw0mzr1dy9bdn")))

(define-public crate-windows_x86_64_msvc-0.48.4 (c (n "windows_x86_64_msvc") (v "0.48.4") (h "041bf8axadd2h3arf17rw3s70lbnkyb9ppk1jj2f9fyhkxnz67dx")))

(define-public crate-windows_x86_64_msvc-0.48.5 (c (n "windows_x86_64_msvc") (v "0.48.5") (h "0f4mdp895kkjh9zv8dxvn4pc10xr7839lf5pa9l0193i2pkgr57d")))

(define-public crate-windows_x86_64_msvc-0.52.0 (c (n "windows_x86_64_msvc") (v "0.52.0") (h "012wfq37f18c09ij5m6rniw7xxn5fcvrxbqd0wd8vgnl3hfn9yfz") (r "1.56")))

(define-public crate-windows_x86_64_msvc-0.52.1 (c (n "windows_x86_64_msvc") (v "0.52.1") (h "0g772cbapjdq29ikyif4715jdpksrhmfc24jw2zb113z8mh0qi4y") (y #t) (r "1.60")))

(define-public crate-windows_x86_64_msvc-0.52.3 (c (n "windows_x86_64_msvc") (v "0.52.3") (h "1mnjdqcr16bxzmcicgcni37svami5gysjgwvk2766w59c0yq6w07") (r "1.60")))

(define-public crate-windows_x86_64_msvc-0.52.4 (c (n "windows_x86_64_msvc") (v "0.52.4") (h "1n0yc7xiv9iki1j3xl8nxlwwkr7dzsnwwvycvgxxv81d5bjm5drj") (r "1.56")))

(define-public crate-windows_x86_64_msvc-0.52.5 (c (n "windows_x86_64_msvc") (v "0.52.5") (h "1w1bn24ap8dp9i85s8mlg8cim2bl2368bd6qyvm0xzqvzmdpxi5y") (r "1.56")))

