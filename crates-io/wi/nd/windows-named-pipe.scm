(define-module (crates-io wi nd windows-named-pipe) #:use-module (crates-io))

(define-public crate-windows-named-pipe-0.1.0 (c (n "windows-named-pipe") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1213q0kpicmb3xhpg4kisp47q68fhpiqxl0sjxjm9k467mdsd2w0")))

