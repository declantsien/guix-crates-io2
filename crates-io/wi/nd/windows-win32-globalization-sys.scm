(define-module (crates-io wi nd windows-win32-globalization-sys) #:use-module (crates-io))

(define-public crate-windows-win32-globalization-sys-0.0.2 (c (n "windows-win32-globalization-sys") (v "0.0.2") (h "05i4dwm33dkjcpqh00lbl7xk687sn3xjh3jdask70d97ky222i5m") (r "1.46")))

(define-public crate-windows-win32-globalization-sys-0.23.0 (c (n "windows-win32-globalization-sys") (v "0.23.0") (h "1c9gza0mwkmljdnm28mp4zhp17508zv9dgzkp4k8ynq0n45a6mxx")))

