(define-module (crates-io wi nd windows-system) #:use-module (crates-io))

(define-public crate-windows-system-0.7.0 (c (n "windows-system") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1qd4kmvgmsfm56nw8z4v9v0k82ai5w74mdjjml22shj87z4164k1")))

(define-public crate-windows-system-0.22.0 (c (n "windows-system") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0wdcszdl5dsssnrpd6xp5645z0cip1fyqpqz6spsj62qb6b8ivyh")))

(define-public crate-windows-system-0.22.1 (c (n "windows-system") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "0w840gmjm7fb5qwzgafbni2hmz5fw9cppwxa7gpign83j1gscx9f")))

(define-public crate-windows-system-0.22.2 (c (n "windows-system") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-networking") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "0h3lhkh9zjw67zlks2arnwnwx5wrwmjjzwpyc81a98csfs22v5qx")))

(define-public crate-windows-system-0.22.3 (c (n "windows-system") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0rk79psmh9xc2f090scf3vql29h4j0pb8pjrgprhsvqkj3bnxaaz")))

(define-public crate-windows-system-0.22.5 (c (n "windows-system") (v "0.22.5") (h "0g98bv5lf371lrdjawxpawal6x7misz76p65yq4lsw5i9647rl41") (r "1.46")))

(define-public crate-windows-system-0.22.6 (c (n "windows-system") (v "0.22.6") (h "1w9yqfpigcsrjybiq8kaafhf3vb9smciz5i904gqnh7zkbw2pvam") (r "1.46")))

(define-public crate-windows-system-0.23.0 (c (n "windows-system") (v "0.23.0") (h "020yp0w4w82h5rfk2ga58a66swv3v26j4pbak7y3ajrg8isgg1cg")))

