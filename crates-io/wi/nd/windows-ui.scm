(define-module (crates-io wi nd windows-ui) #:use-module (crates-io))

(define-public crate-windows-ui-0.7.0 (c (n "windows-ui") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1jyw5gpqprhdn554ql9z7idpszxijfari9nafvaj373yk4airjx0")))

(define-public crate-windows-ui-0.22.0 (c (n "windows-ui") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0iij20f7zfnk39iv0509b7v9cbpjhy9ljgjh4wac33j2if1rnkk5")))

(define-public crate-windows-ui-0.22.1 (c (n "windows-ui") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-gaming") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "05dq4jrlxqmdyamyy1i6l9ygqcri4p71sagd2617cp0p2k9hybl7")))

(define-public crate-windows-ui-0.22.2 (c (n "windows-ui") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-data") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-gaming") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-graphics") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-media") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-perception") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-security") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "0b63kpgh0xp0wzlvi3n91ckcm78cnqj2salhq46ykxl435wmh2n8")))

(define-public crate-windows-ui-0.22.3 (c (n "windows-ui") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "045qpb7yl4skywdzkypkb582f7148zsmkd842zdk3xkph6k5625x")))

(define-public crate-windows-ui-0.22.5 (c (n "windows-ui") (v "0.22.5") (h "19n0wc8wwnrylbjhfxc1nf47yym2p8pg8wxlv9gv8r0vq6hl25n4") (r "1.46")))

(define-public crate-windows-ui-0.22.6 (c (n "windows-ui") (v "0.22.6") (h "0q4vqcsr38paf6bf6gcf5fw7v7wjarhsd6ngrkwibwbfwbkrmcq4") (r "1.46")))

(define-public crate-windows-ui-0.23.0 (c (n "windows-ui") (v "0.23.0") (h "0c7lzy7qxg762igzv0dy2pkjb5arja53xfssq8rv5rlj6vbd7a2l")))

