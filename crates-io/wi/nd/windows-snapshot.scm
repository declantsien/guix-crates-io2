(define-module (crates-io wi nd windows-snapshot) #:use-module (crates-io))

(define-public crate-windows-snapshot-0.1.0 (c (n "windows-snapshot") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wmi") (r "^0.12.2") (d #t) (k 0)))) (h "1l6bwqm4gj84z79mfvhjw7hx4y80dbk5z5pymj2dxz2kbl7sa42x")))

(define-public crate-windows-snapshot-0.1.1 (c (n "windows-snapshot") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.159") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "tokio") (r "^1.28.1") (f (quote ("full"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "wmi") (r "^0.12.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0vb49b13jn5bnna9bbk3f0ilmrpxpsq20jz87xm3ad3zgailj3vv")))

