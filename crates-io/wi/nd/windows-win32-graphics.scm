(define-module (crates-io wi nd windows-win32-graphics) #:use-module (crates-io))

(define-public crate-windows-win32-graphics-0.22.0 (c (n "windows-win32-graphics") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0xzqrcv6mqy7vgcxmrbg3mgj74wj368yps62m7jwb5j399xkv81y")))

(define-public crate-windows-win32-graphics-0.22.3 (c (n "windows-win32-graphics") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0w3whw37ry3gvqnb0dvyr96x4g9a78x4ssl2lnnpn1f3pmzzv8qm")))

(define-public crate-windows-win32-graphics-0.22.5 (c (n "windows-win32-graphics") (v "0.22.5") (h "14chbfbaxwamvga38fgmgailv4w0brab45zjwqh6xx0lbmcdma3f") (r "1.46")))

(define-public crate-windows-win32-graphics-0.22.6 (c (n "windows-win32-graphics") (v "0.22.6") (h "1s4zz1gdwj1all9ff23b2f4idkvr50b293crir3k8jn11c0mc3x2") (r "1.46")))

(define-public crate-windows-win32-graphics-0.23.0 (c (n "windows-win32-graphics") (v "0.23.0") (h "0vhz1yzgsjgq4chgadk5j4yr9kb36sjgv4z090mb82q7wymv8lh2")))

