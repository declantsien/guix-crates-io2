(define-module (crates-io wi nd windows-services-sys) #:use-module (crates-io))

(define-public crate-windows-services-sys-0.0.2 (c (n "windows-services-sys") (v "0.0.2") (h "08c1108yig0njs33hpa7j2gwlwrsbi329vqdk6kcsvaj52vym9by") (r "1.46")))

(define-public crate-windows-services-sys-0.23.0 (c (n "windows-services-sys") (v "0.23.0") (h "06q4babn86bwddj93kny41f7ahi8gfliszg4mdaxxi02mlg14m5h")))

