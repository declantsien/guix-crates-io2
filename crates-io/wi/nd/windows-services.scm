(define-module (crates-io wi nd windows-services) #:use-module (crates-io))

(define-public crate-windows-services-0.7.0 (c (n "windows-services") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "1npl099cy7bdx3p8y3v5d35ydcih67s54zvx0zcw0m7m2w84bblq")))

(define-public crate-windows-services-0.22.0 (c (n "windows-services") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "0fn7zsi7jbmcni56511dazzyylj45j709lrfs9sppjniqvz1p1s8")))

(define-public crate-windows-services-0.22.1 (c (n "windows-services") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.1") (d #t) (k 0)))) (h "03dqyqfbnhyk9g3hlmcl5naxzffx3ws77wqyl12klvhi4ccg0qqz")))

(define-public crate-windows-services-0.22.2 (c (n "windows-services") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-devices") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-globalization") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-system") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-web") (r "^0.22.2") (d #t) (k 0)))) (h "02hd6k76y4kffz48kblkpvzfrdlxyf520dx1k2ww8qlsasjmjj7w")))

(define-public crate-windows-services-0.22.3 (c (n "windows-services") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0sj9vivzpq69f0964avzwqjm5cz8dyg5j52q0h3bw2daahpqbpm8")))

(define-public crate-windows-services-0.22.5 (c (n "windows-services") (v "0.22.5") (h "0zpy60w0x0mq0h70jcb8ny2pil5f9b9ppdqd2yskvn95avrxshim") (r "1.46")))

(define-public crate-windows-services-0.22.6 (c (n "windows-services") (v "0.22.6") (h "0d0i45g7zvwbp0s1lvs44kbgganwdgg38yjqa1b0ln6adrhbjh9z") (r "1.46")))

(define-public crate-windows-services-0.23.0 (c (n "windows-services") (v "0.23.0") (h "063fnmkqslc61sriyrz39nwp8sfbkg6n83whzbb3hjg7hsjvln3n")))

