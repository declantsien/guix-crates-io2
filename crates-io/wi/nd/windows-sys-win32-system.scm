(define-module (crates-io wi nd windows-sys-win32-system) #:use-module (crates-io))

(define-public crate-windows-sys-win32-system-0.22.5 (c (n "windows-sys-win32-system") (v "0.22.5") (h "17k9443b90nk5csg3xbb2impyd7s6qfmmaz5cgk6yw7g726qhham") (r "1.46")))

(define-public crate-windows-sys-win32-system-0.22.6 (c (n "windows-sys-win32-system") (v "0.22.6") (h "1xik0s9hnlvap29rc7v256kjpvhb7nnnvmr1kh5b6rbxycwg63yd") (r "1.46")))

(define-public crate-windows-sys-win32-system-0.23.0 (c (n "windows-sys-win32-system") (v "0.23.0") (h "1h23d7cdsl1z2z06i5jb6dzdav7flxl6ygr2yhfn5l9ryrplz2r9")))

