(define-module (crates-io wi nd windows-data-sys) #:use-module (crates-io))

(define-public crate-windows-data-sys-0.0.0 (c (n "windows-data-sys") (v "0.0.0") (h "1xn1hphbl6l7ynkcs4y0phnfhbdr3lqlc3gj2bmg0c66nc9ir6qr") (r "1.46")))

(define-public crate-windows-data-sys-0.0.1 (c (n "windows-data-sys") (v "0.0.1") (h "03ww7aw82nl61f0gfach1d2nvyy1yddzlrmyg7jbam63bwwnnp5d") (r "1.46")))

(define-public crate-windows-data-sys-0.0.2 (c (n "windows-data-sys") (v "0.0.2") (h "121c2r4sycjbqi68zvv8x0avqcmj33bs07l6rn4db34q6fdfbacj") (r "1.46")))

(define-public crate-windows-data-sys-0.1.0 (c (n "windows-data-sys") (v "0.1.0") (h "0zvlrngfsm1kizh8v1pxrjvny4lknxq2p63qp5cdbn2v15mj22qq")))

(define-public crate-windows-data-sys-0.23.0 (c (n "windows-data-sys") (v "0.23.0") (h "1f8v7iy7vqvqh18jdg90p3d6c92qxvqf6789l6hncrvp54xa0nch")))

