(define-module (crates-io wi nd windows-phone) #:use-module (crates-io))

(define-public crate-windows-phone-0.7.0 (c (n "windows-phone") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "0i6sv15dn5anxa65s9m4nsr2wrmhpd0phrv27dyg78cp0r0hx62z")))

(define-public crate-windows-phone-0.22.0 (c (n "windows-phone") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "1czncqdx6wf4z1irq35fw2k1bijyd5xmak38m8hzn5kgzbhpjy12")))

(define-public crate-windows-phone-0.22.1 (c (n "windows-phone") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-management") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.1") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.1") (d #t) (k 0)))) (h "1avb0mqhmxkma53mfyf3g3irh3may66sfsi7mlz7ay4p7v4gp1bz")))

(define-public crate-windows-phone-0.22.2 (c (n "windows-phone") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-applicationmodel") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-management") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-storage") (r "^0.22.2") (d #t) (k 0)) (d (n "windows-ui") (r "^0.22.2") (d #t) (k 0)))) (h "0a45fhym779azrr5ycbwnv5f3fv81zscy61cwv76r9w99rcx1ic0")))

(define-public crate-windows-phone-0.22.3 (c (n "windows-phone") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "17qkgklnwns5gmjzzj2n8dc4x2a0bg10cbzgdijcza34gi3hiq6d")))

(define-public crate-windows-phone-0.22.5 (c (n "windows-phone") (v "0.22.5") (h "152mk3d8mshpq1a21iarkgpk8cf63awmalfib5hk8vc0gqkdf2n3") (r "1.46")))

(define-public crate-windows-phone-0.22.6 (c (n "windows-phone") (v "0.22.6") (h "0k4mnj8m0habn2lchxskfqfwgqv1n6rqmjm1ya5z0yg9ggxindr0") (r "1.46")))

(define-public crate-windows-phone-0.23.0 (c (n "windows-phone") (v "0.23.0") (h "1kdv4kzngbhnsxc38hgzix9axqqkkr8d1mf17kl58dpjigfh140l")))

