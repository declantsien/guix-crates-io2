(define-module (crates-io wi nd windsh-cli) #:use-module (crates-io))

(define-public crate-windsh-cli-0.1.0-alpha.0 (c (n "windsh-cli") (v "0.1.0-alpha.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "windsh-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "052wsqczybmqpzv015g7m1lzrkgyyghgdivfzj9mmaxggylg862s")))

(define-public crate-windsh-cli-0.1.1-alpha.0 (c (n "windsh-cli") (v "0.1.1-alpha.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "windsh-core") (r "^0.1.1-alpha.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0j4nmychcc090smgx4164zlg5jdqly2qbp63gapj96asjhsidr26")))

(define-public crate-windsh-cli-0.1.0-alpha.1 (c (n "windsh-cli") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "windsh-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.11") (d #t) (t "cfg(windows)") (k 1)))) (h "139x6jcwi9d38fivh1sv6n3g3yl3kf7hdncqc3cjb37il0i5ls1x")))

