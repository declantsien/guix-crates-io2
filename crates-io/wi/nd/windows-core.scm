(define-module (crates-io wi nd windows-core) #:use-module (crates-io))

(define-public crate-windows-core-0.0.0 (c (n "windows-core") (v "0.0.0") (h "0sc48iix2h38xds77kfrq6qi2sg6fdfg2zdkzvc8bxw5qcy6pm54")))

(define-public crate-windows-core-0.50.0 (c (n "windows-core") (v "0.50.0") (d (list (d (n "windows-targets") (r "^0.48.0") (d #t) (k 0)))) (h "1zxv6wjjdz6nlmgw0g854vs9632g9zp0zp5cawdw51a4z2rl2q5g") (f (quote (("implement") ("default")))) (r "1.48")))

(define-public crate-windows-core-0.51.0 (c (n "windows-core") (v "0.51.0") (d (list (d (n "windows-targets") (r "^0.48.2") (d #t) (k 0)))) (h "0dnqv0i3rsqyy8kblr5di76r3ndqgsfxqm3rcdw5fdbn3rvm05mq") (f (quote (("implement") ("default")))) (r "1.56")))

(define-public crate-windows-core-0.51.1 (c (n "windows-core") (v "0.51.1") (d (list (d (n "windows-targets") (r "^0.48.3") (d #t) (k 0)))) (h "0r1f57hsshsghjyc7ypp2s0i78f7b1vr93w68sdb8baxyf2czy7i") (f (quote (("implement") ("default")))) (r "1.56")))

(define-public crate-windows-core-0.52.0 (c (n "windows-core") (v "0.52.0") (d (list (d (n "windows-targets") (r "^0.52.0") (d #t) (k 0)))) (h "1nc3qv7sy24x0nlnb32f7alzpd6f72l4p24vl65vydbyil669ark") (f (quote (("implement") ("default")))) (r "1.56")))

(define-public crate-windows-core-0.53.0 (c (n "windows-core") (v "0.53.0") (d (list (d (n "windows-result") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.1") (d #t) (k 0)))) (h "1pd58vj252p28xznq7mdcvdw0npxn7dcwnm5kymz2xv3ba4mpk4x") (f (quote (("implement") ("default")))) (r "1.62")))

(define-public crate-windows-core-0.54.0 (c (n "windows-core") (v "0.54.0") (d (list (d (n "windows-result") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.3") (d #t) (k 0)))) (h "0r8x2sgl4qq1h23ldf4z7cj213k0bz7479m8a156h79mi6f1nrhj") (f (quote (("implement") ("default")))) (r "1.62")))

(define-public crate-windows-core-0.55.0 (c (n "windows-core") (v "0.55.0") (d (list (d (n "windows-result") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.4") (d #t) (k 0)))) (h "083vmjd6dbz3m3f9i027yvw2krxs3yv3p2hr64dn9vmdn965b5p1") (f (quote (("implement") ("default")))) (r "1.62")))

(define-public crate-windows-core-0.56.0 (c (n "windows-core") (v "0.56.0") (d (list (d (n "windows-bindgen") (r "^0.56.0") (d #t) (k 2)) (d (n "windows-implement") (r "^0.56.0") (d #t) (k 0)) (d (n "windows-interface") (r "^0.56.0") (d #t) (k 0)) (d (n "windows-result") (r "^0.1.1") (d #t) (k 0)) (d (n "windows-targets") (r "^0.52.5") (d #t) (k 0)))) (h "19pj57bm0rzhlk0ghrccd3i5zvh0ghm52f8cmdc8d3yhs8pfb626") (r "1.62")))

