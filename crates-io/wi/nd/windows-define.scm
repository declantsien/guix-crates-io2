(define-module (crates-io wi nd windows-define) #:use-module (crates-io))

(define-public crate-windows-define-0.0.0 (c (n "windows-define") (v "0.0.0") (h "0wmd61d7ymhzw07a7p3c025sbm2z810kf888phbb93b290bnr995")))

(define-public crate-windows-define-0.23.0 (c (n "windows-define") (v "0.23.0") (h "0ymadfpxicqa1n0qr6i27pvd63wxkgvqx9d1fl51p14ial6xnhjm")))

