(define-module (crates-io wi nd windows-accesstoken) #:use-module (crates-io))

(define-public crate-windows-accesstoken-0.1.0 (c (n "windows-accesstoken") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "errhandlingapi" "handleapi" "winnt" "securitybaseapi" "winerror"))) (d #t) (k 0)))) (h "0w75bn5zbjc177bxz6v9iz6s0s53bgkxvfm07yxq9a06bfv6z0ix")))

