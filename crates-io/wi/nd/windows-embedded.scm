(define-module (crates-io wi nd windows-embedded) #:use-module (crates-io))

(define-public crate-windows-embedded-0.7.0 (c (n "windows-embedded") (v "0.7.0") (d (list (d (n "windows") (r "^0.7.0") (d #t) (k 0)))) (h "15g99v5lk8fcwiaf8ddxrkzima778agzi7b1ynn582r2mcn60ryd")))

(define-public crate-windows-embedded-0.22.0 (c (n "windows-embedded") (v "0.22.0") (d (list (d (n "windows") (r "^0.22.0") (k 0)))) (h "08v59w2qccy6xchxwl8z0p0f49krmsw8dqfxavy48xfa73w2sv86")))

(define-public crate-windows-embedded-0.22.1 (c (n "windows-embedded") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.1") (d #t) (k 0)))) (h "1y525i5y5bw2nr8hixm7w1w9n1hzywn1m2gfnlgwbkc083m621n7")))

(define-public crate-windows-embedded-0.22.2 (c (n "windows-embedded") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)) (d (n "windows-foundation") (r "^0.22.2") (d #t) (k 0)))) (h "04abc2nd9b3j74wy7rxdzqyjc6qayjijvz7k7c15my7mlj752hsk")))

(define-public crate-windows-embedded-0.22.3 (c (n "windows-embedded") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "07mdzkzak7k2r7ya8phll41fpwgfh6w0w8c8hhw4pkriav5qrb3f")))

(define-public crate-windows-embedded-0.22.4 (c (n "windows-embedded") (v "0.22.4") (h "18i3r0barw9d5mkscz7532ak9bg3337zpc2vifg7gn944i2il89h") (r "1.46")))

(define-public crate-windows-embedded-0.22.5 (c (n "windows-embedded") (v "0.22.5") (h "0byx8im1h5dcyk0ap0gi0rhs8y5mjrnjnahj8z5dmyis97p441cd") (r "1.46")))

(define-public crate-windows-embedded-0.22.6 (c (n "windows-embedded") (v "0.22.6") (h "15m648vl53yqrpb1ikil83hgy4d7fy057iifm0y8afqqll3034ib") (r "1.46")))

(define-public crate-windows-embedded-0.1.0 (c (n "windows-embedded") (v "0.1.0") (h "1wrqzjm4l56kcmdy0xhv8m098s2kdwbm6ddywgydwbg2sfimawwf")))

(define-public crate-windows-embedded-0.23.0 (c (n "windows-embedded") (v "0.23.0") (h "0xaz5scd99365h5n1flmw20vxfc45q8mkfgwvr7h5w2hk7k90j3r")))

