(define-module (crates-io wi nd windows-win32-foundation) #:use-module (crates-io))

(define-public crate-windows-win32-foundation-0.22.1 (c (n "windows-win32-foundation") (v "0.22.1") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "0aqmkmy3jhvd9bbssjbn9ay22rvhsb4fxnh0965f0fdyc7cr157q")))

(define-public crate-windows-win32-foundation-0.22.2 (c (n "windows-win32-foundation") (v "0.22.2") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1wn64gf48g2b390bbbc91jgxwwry7006qfp57y8cqrc0q3lfs2i2")))

(define-public crate-windows-win32-foundation-0.22.3 (c (n "windows-win32-foundation") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1bdl99kp8ml6rk990fc3a4il600cyvd8xf99k490f6vc3z4b659d")))

(define-public crate-windows-win32-foundation-0.22.5 (c (n "windows-win32-foundation") (v "0.22.5") (h "1603smshv2j5fdajxbqx1gpb559lbgdfxy6im6c8yla04ig61qzi") (r "1.46")))

(define-public crate-windows-win32-foundation-0.22.6 (c (n "windows-win32-foundation") (v "0.22.6") (h "1n15am4m3fcbljhmgj3fk7jm4g874z92nfq6326y4bamhandhngi") (r "1.46")))

(define-public crate-windows-win32-foundation-0.23.0 (c (n "windows-win32-foundation") (v "0.23.0") (h "14l6dngp0qi6b89lz6l6x9b7hvvfy25n151261dzdgb8gsvqirzn")))

