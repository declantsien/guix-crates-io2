(define-module (crates-io wi nd windows_aarch64_gnullvm) #:use-module (crates-io))

(define-public crate-windows_aarch64_gnullvm-0.0.0 (c (n "windows_aarch64_gnullvm") (v "0.0.0") (h "1ysd70shskrzmrcpva0ipbn0jcq4j3bzrpqf23s7sr3rriv5q3ps")))

(define-public crate-windows_aarch64_gnullvm-0.40.0 (c (n "windows_aarch64_gnullvm") (v "0.40.0") (h "046a6yxd842x9kh94sis0f1qawq3fj0v19iw68abfqb5l6hs9jpk")))

(define-public crate-windows_aarch64_gnullvm-0.41.0 (c (n "windows_aarch64_gnullvm") (v "0.41.0") (h "1zci61gds97n7v6pnfp75dd2xsdmna09wwg6nkn7h8jgfxhjfg8n")))

(define-public crate-windows_aarch64_gnullvm-0.42.0 (c (n "windows_aarch64_gnullvm") (v "0.42.0") (h "17m1p753qk02r25afg31dxym4rpy7kpr0z8nwl5f1jzhyrqsmlj1")))

(define-public crate-windows_aarch64_gnullvm-0.42.1 (c (n "windows_aarch64_gnullvm") (v "0.42.1") (h "0256d14kqpiniwcm6y2yfj7jbzdvvj4l6i65r5zyrza36bl6964c")))

(define-public crate-windows_aarch64_gnullvm-0.42.2 (c (n "windows_aarch64_gnullvm") (v "0.42.2") (h "1y4q0qmvl0lvp7syxvfykafvmwal5hrjb4fmv04bqs0bawc52yjr")))

(define-public crate-windows_aarch64_gnullvm-0.47.0 (c (n "windows_aarch64_gnullvm") (v "0.47.0") (h "143w0aadg3ifidypg6nrnj724q325dp8xdijncfwpwyladymc7c3")))

(define-public crate-windows_aarch64_gnullvm-0.48.0 (c (n "windows_aarch64_gnullvm") (v "0.48.0") (h "1g71yxi61c410pwzq05ld7si4p9hyx6lf5fkw21sinvr3cp5gbli")))

(define-public crate-windows_aarch64_gnullvm-0.48.2 (c (n "windows_aarch64_gnullvm") (v "0.48.2") (h "0bzvmxgcz586d60z05pr4hyjxv09yqx5jgg1jmhidxm7ifb0q3di")))

(define-public crate-windows_aarch64_gnullvm-0.48.3 (c (n "windows_aarch64_gnullvm") (v "0.48.3") (h "137n53d578x7f8axcjcl0ffccas3awndhrl5g9cnmrscmravpqgx")))

(define-public crate-windows_aarch64_gnullvm-0.48.4 (c (n "windows_aarch64_gnullvm") (v "0.48.4") (h "0ad9ggkr8snjhsygzc2cv0ppgjk7rsbz04hjf0473gkhd7lhwjyi")))

(define-public crate-windows_aarch64_gnullvm-0.48.5 (c (n "windows_aarch64_gnullvm") (v "0.48.5") (h "1n05v7qblg1ci3i567inc7xrkmywczxrs1z3lj3rkkxw18py6f1b")))

(define-public crate-windows_aarch64_gnullvm-0.52.0 (c (n "windows_aarch64_gnullvm") (v "0.52.0") (h "1shmn1kbdc0bpphcxz0vlph96bxz0h1jlmh93s9agf2dbpin8xyb") (r "1.56")))

(define-public crate-windows_aarch64_gnullvm-0.52.1 (c (n "windows_aarch64_gnullvm") (v "0.52.1") (h "1bpba77k7jrfgw5wqaf4fvy8x0jvcvvq60r9b7xgjnp788a9q9p7") (y #t) (r "1.60")))

(define-public crate-windows_aarch64_gnullvm-0.52.3 (c (n "windows_aarch64_gnullvm") (v "0.52.3") (h "1ikfdjjznh4yvji0w296zaidk1jdgdmfb1lgkkykmx8kjkxxrrb8") (r "1.60")))

(define-public crate-windows_aarch64_gnullvm-0.52.4 (c (n "windows_aarch64_gnullvm") (v "0.52.4") (h "1jfam5qfngg8v1syxklnvy8la94b5igm7klkrk8z5ik5qgs6rx5w") (r "1.56")))

(define-public crate-windows_aarch64_gnullvm-0.52.5 (c (n "windows_aarch64_gnullvm") (v "0.52.5") (h "0qrjimbj67nnyn7zqy15mzzmqg0mn5gsr2yciqjxm3cb3vbyx23h") (r "1.56")))

