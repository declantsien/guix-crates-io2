(define-module (crates-io wi nd windebug_logger) #:use-module (crates-io))

(define-public crate-windebug_logger-0.1.0 (c (n "windebug_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wchar") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "debugapi" "stringapiset" "winnls" "datetimeapi" "sysinfoapi"))) (d #t) (k 0)))) (h "0zjaq2sllr4wi54q1ijxxhf0h56q4f0gdlq9wpj3j6jpk7hn26b4") (y #t)))

(define-public crate-windebug_logger-0.1.1 (c (n "windebug_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wchar") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "debugapi" "stringapiset" "winnls" "datetimeapi" "sysinfoapi"))) (d #t) (k 0)))) (h "0zs81m0aydv3r4kl9brjv61is451mlgwxdzrz2law6cmzvrzdkaq") (y #t)))

(define-public crate-windebug_logger-0.1.2 (c (n "windebug_logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wchar") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "debugapi" "stringapiset" "winnls" "datetimeapi" "sysinfoapi"))) (d #t) (k 0)))) (h "06mhjlggpr7jn8fg04i94z5pw2zmh3fh54bjgx2d2h7i8rwv11ib")))

(define-public crate-windebug_logger-0.1.3 (c (n "windebug_logger") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wchar") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "debugapi" "stringapiset" "winnls" "datetimeapi" "sysinfoapi"))) (d #t) (k 0)))) (h "1wqrlycbfq2fzk49hccx07a06lay1hzqmdljn16g18z3hgzycmh5")))

(define-public crate-windebug_logger-0.1.4 (c (n "windebug_logger") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wchar") (r "^0.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "debugapi" "stringapiset" "winnls" "datetimeapi" "sysinfoapi"))) (d #t) (k 0)))) (h "086jgj9igri9ccss9ysyjdf265rlrl1k8ql31120vmy2r45p140b")))

