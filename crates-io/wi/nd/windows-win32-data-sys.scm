(define-module (crates-io wi nd windows-win32-data-sys) #:use-module (crates-io))

(define-public crate-windows-win32-data-sys-0.0.2 (c (n "windows-win32-data-sys") (v "0.0.2") (h "16ng2009xdqj45bllqpkzp7q9z1qn9qj25psgc1qimm698wl7964") (r "1.46")))

(define-public crate-windows-win32-data-sys-0.23.0 (c (n "windows-win32-data-sys") (v "0.23.0") (h "07xdjabswvcnarkbn3in3qf7f01j5gaxx3cmgl7a9xk6i7jwnq74")))

