(define-module (crates-io wi nd windows_memory_access) #:use-module (crates-io))

(define-public crate-windows_memory_access-0.3.2 (c (n "windows_memory_access") (v "0.3.2") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (k 0)))) (h "1g7g7rxc83a0hxwaz026cncvjq2ch6cvfmd06z0c898al5p8bvk0")))

(define-public crate-windows_memory_access-0.4.2 (c (n "windows_memory_access") (v "0.4.2") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (k 0)))) (h "1pjidcclba524473aj022hg7a2mqwz1j94sc17dbyd611k1k6r73")))

(define-public crate-windows_memory_access-0.5.0 (c (n "windows_memory_access") (v "0.5.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (d #t) (k 0)))) (h "03cq3f1v6ngigj3n5vjr9657kqnxzfsbmavxnv5csybb0zjld0j3")))

