(define-module (crates-io wi nd windows-sys-win32-management) #:use-module (crates-io))

(define-public crate-windows-sys-win32-management-0.22.5 (c (n "windows-sys-win32-management") (v "0.22.5") (h "0hrxvbz7sv3pwrbhflq4g0i1bm26cviy2nwsqb8qw3d0pz3k2yy2") (r "1.46")))

(define-public crate-windows-sys-win32-management-0.22.6 (c (n "windows-sys-win32-management") (v "0.22.6") (h "053626vpxg7fnkmh7a7ic1qg6pw4dvic4df4xq54gdzhd4y1ihy7") (r "1.46")))

(define-public crate-windows-sys-win32-management-0.23.0 (c (n "windows-sys-win32-management") (v "0.23.0") (h "16a7kdpjd78dzm9spy9g96wxdnhlmk5ym1244qc2xn8wjxlsnl72")))

