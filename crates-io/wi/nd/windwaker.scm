(define-module (crates-io wi nd windwaker) #:use-module (crates-io))

(define-public crate-windwaker-0.1.0 (c (n "windwaker") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)) (d (n "dolphin-memory") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ql0l7jvldfxqyv4lndglki1vxix6zdf2cv7nj76y6fgvf5mddyv")))

