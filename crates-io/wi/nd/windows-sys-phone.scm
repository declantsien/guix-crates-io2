(define-module (crates-io wi nd windows-sys-phone) #:use-module (crates-io))

(define-public crate-windows-sys-phone-0.22.5 (c (n "windows-sys-phone") (v "0.22.5") (h "0jvxl76yrv1fc6kc7bgizbgp9i9qblnc49a682cvsqjd6xklhy8s") (r "1.46")))

(define-public crate-windows-sys-phone-0.22.6 (c (n "windows-sys-phone") (v "0.22.6") (h "0icyjwd6z5szzyzh8isgyggri09qcj8nqk5xsykxsq3h8dcajd4w") (r "1.46")))

(define-public crate-windows-sys-phone-0.23.0 (c (n "windows-sys-phone") (v "0.23.0") (h "119ihln08jh8f0ms9f87cicfdmn3g1qvsmrp8vmhclx5jdg065r2")))

