(define-module (crates-io wi nd windows_aarch64_msvc) #:use-module (crates-io))

(define-public crate-windows_aarch64_msvc-0.25.0 (c (n "windows_aarch64_msvc") (v "0.25.0") (h "0yfz86719yhb83j7zics52wj23paf50v0gwkjmpypjhg01sd28ih")))

(define-public crate-windows_aarch64_msvc-0.26.0 (c (n "windows_aarch64_msvc") (v "0.26.0") (h "1nlh1gm6vl6pn713qb9c5syycb45yhqsvk2kmr3dbb92n238jxgp")))

(define-public crate-windows_aarch64_msvc-0.27.0 (c (n "windows_aarch64_msvc") (v "0.27.0") (h "19yd83g61nps8xixck8ms8ag4iz6pgvnx30lf7g2q8xbpd4iczgc")))

(define-public crate-windows_aarch64_msvc-0.28.0 (c (n "windows_aarch64_msvc") (v "0.28.0") (h "1hpk0n2z0jzzvwlvs98b75sa4q920953nqfc119rv19nwm0mlsaj")))

(define-public crate-windows_aarch64_msvc-0.29.0 (c (n "windows_aarch64_msvc") (v "0.29.0") (h "16sxpz2b2ljjrax6qy1m18ri4fz0xfv6lzdrpq61xc00blbjgl63")))

(define-public crate-windows_aarch64_msvc-0.30.0 (c (n "windows_aarch64_msvc") (v "0.30.0") (h "1jkbdfqzynwqpn3h5kl6519vmb97p6p1yz9wyrszfhnn6m27l9r9")))

(define-public crate-windows_aarch64_msvc-0.31.0 (c (n "windows_aarch64_msvc") (v "0.31.0") (h "19kql817s9w17hczpn3i3jnhmx0g1kh3wjimw3ww8mfw6qrnfvfa")))

(define-public crate-windows_aarch64_msvc-0.32.0 (c (n "windows_aarch64_msvc") (v "0.32.0") (h "1x8bnafz15ksgpbjbgk1l1j2jx4rq4a2ylzcahb1jhy4n59jgsfq")))

(define-public crate-windows_aarch64_msvc-0.33.0 (c (n "windows_aarch64_msvc") (v "0.33.0") (h "01q80v2zzfc144xsqj3yhd62rn1dy1kyamhyv0gcrf4sxg9iyxnd")))

(define-public crate-windows_aarch64_msvc-0.34.0 (c (n "windows_aarch64_msvc") (v "0.34.0") (h "07c8vcqcvkmm2d921488w05dyjl047jc03xddyszy6hj83kzpkqp")))

(define-public crate-windows_aarch64_msvc-0.35.0 (c (n "windows_aarch64_msvc") (v "0.35.0") (h "1rk3shfqh3rfcssc47x5wnp3679zg4lkbv6wcifxmq4c9q9wafyv")))

(define-public crate-windows_aarch64_msvc-0.36.0 (c (n "windows_aarch64_msvc") (v "0.36.0") (h "07a2bsw9397lxzib00ridlb0aygmg5ixc2ykhszikvxv4z9qrvjb")))

(define-public crate-windows_aarch64_msvc-0.36.1 (c (n "windows_aarch64_msvc") (v "0.36.1") (h "0ixaxs2c37ll2smprzh0xq5p238zn8ylzb3lk1zddqmd77yw7f4v")))

(define-public crate-windows_aarch64_msvc-0.37.0 (c (n "windows_aarch64_msvc") (v "0.37.0") (h "0flwfsknwn9kpccb5cb6dvdyvsydz4y0z32p7fx1dhnin9y2f8r6")))

(define-public crate-windows_aarch64_msvc-0.38.0 (c (n "windows_aarch64_msvc") (v "0.38.0") (h "1kf3rz5mrssw1p1f4r1skzbqaiv9nd146zjg7zzjy6gvwa3xsami")))

(define-public crate-windows_aarch64_msvc-0.39.0 (c (n "windows_aarch64_msvc") (v "0.39.0") (h "1wj0nfmyli4bn5af243r4s3zncxv0n4j6dd8zyb41gcnc1k12xzc")))

(define-public crate-windows_aarch64_msvc-0.40.0 (c (n "windows_aarch64_msvc") (v "0.40.0") (h "1d1shkdh4y9jygzvmimsavz45dp0fq311rx8maqhzigw5p37729j")))

(define-public crate-windows_aarch64_msvc-0.41.0 (c (n "windows_aarch64_msvc") (v "0.41.0") (h "030arpfkvvjda1dgxlsgfmcr8kq83765k8v686wd607bpkr5y07g")))

(define-public crate-windows_aarch64_msvc-0.42.0 (c (n "windows_aarch64_msvc") (v "0.42.0") (h "1d6d9ny0yl5l9vvagydigvkfcphzk2aygchiccywijimb8pja3yx")))

(define-public crate-windows_aarch64_msvc-0.42.1 (c (n "windows_aarch64_msvc") (v "0.42.1") (h "1iy6adiaz2956f0kcl2wfdjx49ayv2lp11glkxxc85pw7xkip2sc")))

(define-public crate-windows_aarch64_msvc-0.42.2 (c (n "windows_aarch64_msvc") (v "0.42.2") (h "0hsdikjl5sa1fva5qskpwlxzpc5q9l909fpl1w6yy1hglrj8i3p0")))

(define-public crate-windows_aarch64_msvc-0.47.0 (c (n "windows_aarch64_msvc") (v "0.47.0") (h "1pqwqnsgliadxj9w0j1llynaa3wgcbniwrii1r7wwq3w855dahka")))

(define-public crate-windows_aarch64_msvc-0.48.0 (c (n "windows_aarch64_msvc") (v "0.48.0") (h "1wvwipchhywcjaw73h998vzachf668fpqccbhrxzrz5xszh2gvxj")))

(define-public crate-windows_aarch64_msvc-0.48.2 (c (n "windows_aarch64_msvc") (v "0.48.2") (h "0n0h3y0pnx4bvmfap8kmfw4db2qfcs4zx7ls14r4jvgjc978s7ap")))

(define-public crate-windows_aarch64_msvc-0.48.3 (c (n "windows_aarch64_msvc") (v "0.48.3") (h "1m46ybqv29av8cnxzf4xls99vsf4wjsxcbrjszd8m9v5hgafh4qm")))

(define-public crate-windows_aarch64_msvc-0.48.4 (c (n "windows_aarch64_msvc") (v "0.48.4") (h "1jcjq1ngaw44f9rb4371rkgxwzp41qr1x50lc3ychh7l95yjfchk")))

(define-public crate-windows_aarch64_msvc-0.48.5 (c (n "windows_aarch64_msvc") (v "0.48.5") (h "1g5l4ry968p73g6bg6jgyvy9lb8fyhcs54067yzxpcpkf44k2dfw")))

(define-public crate-windows_aarch64_msvc-0.52.0 (c (n "windows_aarch64_msvc") (v "0.52.0") (h "1vvmy1ypvzdvxn9yf0b8ygfl85gl2gpcyvsvqppsmlpisil07amv") (r "1.56")))

(define-public crate-windows_aarch64_msvc-0.52.1 (c (n "windows_aarch64_msvc") (v "0.52.1") (h "1f49knxym5y2bz3kbyvj4j38r2i9llnhr44qvp5pwarkpz7b42pp") (y #t) (r "1.60")))

(define-public crate-windows_aarch64_msvc-0.52.3 (c (n "windows_aarch64_msvc") (v "0.52.3") (h "03xkcn4mxzmkqqc9pd30czj1qm4f629bzvk9kqqrhmy4pfg4dawd") (r "1.60")))

(define-public crate-windows_aarch64_msvc-0.52.4 (c (n "windows_aarch64_msvc") (v "0.52.4") (h "0xdn6db0rk8idn7dxsyflixq2dbj9x60kzdzal5rkxmwsffjb7ys") (r "1.56")))

(define-public crate-windows_aarch64_msvc-0.52.5 (c (n "windows_aarch64_msvc") (v "0.52.5") (h "1dmga8kqlmln2ibckk6mxc9n59vdg8ziqa2zr8awcl720hazv1cr") (r "1.56")))

