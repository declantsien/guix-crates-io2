(define-module (crates-io wi nd windows-win32-ui) #:use-module (crates-io))

(define-public crate-windows-win32-ui-0.22.3 (c (n "windows-win32-ui") (v "0.22.3") (d (list (d (n "windows") (r "^0.21") (k 0)))) (h "1zy4j2wf9wn870wpv7jj1bfzshqgfkifff14vk1aq53dmkjw2s1p")))

(define-public crate-windows-win32-ui-0.22.5 (c (n "windows-win32-ui") (v "0.22.5") (h "19p2vma2v9d4q0yjwkjj84xlkgwn9kxcgnrx3mv3j6v0z27pa9a0") (r "1.46")))

(define-public crate-windows-win32-ui-0.22.6 (c (n "windows-win32-ui") (v "0.22.6") (h "1bnn28zkc7mmrm6mcpwkpfb20vbpw36nzfvjji94q7gy8dfsmc8b") (r "1.46")))

(define-public crate-windows-win32-ui-0.23.0 (c (n "windows-win32-ui") (v "0.23.0") (h "0znqndwcgdfsgzi9lzz5565ngiig6xbgj32p416q649v4vwghaj2")))

