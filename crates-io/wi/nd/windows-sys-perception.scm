(define-module (crates-io wi nd windows-sys-perception) #:use-module (crates-io))

(define-public crate-windows-sys-perception-0.22.5 (c (n "windows-sys-perception") (v "0.22.5") (h "17h6xpyw2cm5ch3fgdjmi98cjccvvhnsmw8n0bj7sn5wy448wp42") (r "1.46")))

(define-public crate-windows-sys-perception-0.22.6 (c (n "windows-sys-perception") (v "0.22.6") (h "10vlsd3l09rpcpn9bpks5w2vsvg49bq9hy74f7vvzb1s4w3x6ifz") (r "1.46")))

(define-public crate-windows-sys-perception-0.23.0 (c (n "windows-sys-perception") (v "0.23.0") (h "1i0zkjnpv7mg5d9fbfvwbm53cgd2rn907gywhihdihb4mvvrg1sl")))

