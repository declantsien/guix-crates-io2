(define-module (crates-io wi lo wilo) #:use-module (crates-io))

(define-public crate-wilo-0.1.0 (c (n "wilo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "termwiz") (r "^0.8.0") (d #t) (k 0)))) (h "0aialc2ay26ax0ksjc4qxmf0jmchxy10nnbihwqdadzy3q6nqign")))

(define-public crate-wilo-0.2.0 (c (n "wilo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "termwiz") (r "^0.8.0") (d #t) (k 0)))) (h "1x5mdsp3ycn43lv734qxyx80qvk62h4rys1qpxc78mvjcrz6wq3h")))

(define-public crate-wilo-0.3.0 (c (n "wilo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "termwiz") (r "^0.9.0") (d #t) (k 0)))) (h "16gpiry0q796583krgqc5x2pk9ybpypjvahjj95pwcx7kkwl8f4m")))

