(define-module (crates-io wi dg widgetui) #:use-module (crates-io))

(define-public crate-widgetui-0.1.2 (c (n "widgetui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0fhbf0lainlrjinm7wvnw6rsjmrx7z0h9avwbvxmixy6sdpwqjcv")))

(define-public crate-widgetui-0.2.0 (c (n "widgetui") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1fxmii2n8f6ykk0h64hjp3wiyidcp42rc2avp67053zsjiqgmm4m")))

(define-public crate-widgetui-0.2.1 (c (n "widgetui") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.2.1") (d #t) (k 0)))) (h "099vq03ngbf42cm4mqbc4pk7ly19axrlsz48zsnb6q54c5x8658p")))

(define-public crate-widgetui-0.3.0 (c (n "widgetui") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0vy9bd4bip5lfq230rj9c9qnadjfkw2fcfhdiim6qiq9dwxkpk77")))

(define-public crate-widgetui-0.3.1 (c (n "widgetui") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.3.1") (d #t) (k 0)))) (h "07w2fhz07al7l1cx1dgrl2qrqrd36pp3iq75pb3bz32mhnqmsbpx") (y #t)))

(define-public crate-widgetui-0.3.2 (c (n "widgetui") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.3.2") (d #t) (k 0)))) (h "1ccm1b6dbkh2a7cgp5mv34g75rq1nh97llpbk3qhx3wawiypky2w") (y #t)))

(define-public crate-widgetui-0.3.3 (c (n "widgetui") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.3.3") (d #t) (k 0)))) (h "05jf13bz0i912cb3v7fb03yyccn3rxn2r3dwg98gwlzff5gdxa10")))

(define-public crate-widgetui-0.4.0 (c (n "widgetui") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0w4xpm0mf5y98ad5j04b41kj1dgd3gh7j55rl051nnajn25gj1vg")))

(define-public crate-widgetui-0.4.1 (c (n "widgetui") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.23.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.4.1") (d #t) (k 0)))) (h "08rc976m11l7jlb3viygi6q4fwivvywz8brj3a3cdwxpcwivbll4")))

(define-public crate-widgetui-0.4.2 (c (n "widgetui") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.4.2") (d #t) (k 0)))) (h "04yw10lr11whgnrn1mgvymk2vm6cyl7k9gw69m1y4jdrrfihmzvc")))

(define-public crate-widgetui-0.5.0 (c (n "widgetui") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.5.0") (d #t) (k 0)))) (h "0vwp22icsaih4k4xw9m0nccksd2wgf2frf1wvidwyz4hx40rpgx1")))

(define-public crate-widgetui-0.5.1 (c (n "widgetui") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.5.1") (d #t) (k 0)))) (h "1jhswvcq9i44n8cnzrazkxqw1nlz2xxvnb156rkq4rllia472q5q")))

(define-public crate-widgetui-0.5.2 (c (n "widgetui") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)) (d (n "tui-helper-proc-macro") (r "^0.5.2") (d #t) (k 0)))) (h "156vvkv0r2xcsgi41ar5y4k2w95zh4ialgrsaavgya4q0cnpv3hn")))

