(define-module (crates-io wi rd wirdigen) #:use-module (crates-io))

(define-public crate-wirdigen-0.1.0 (c (n "wirdigen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonschema") (r "^0.15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14by9iz48dnf84ip62fx0l8spv2vicvnn0wkblmfxmq4q9p78a73")))

(define-public crate-wirdigen-0.2.0 (c (n "wirdigen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonschema") (r "^0.15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0lbm89nzhwj260icmjiwq5fz43v1mpbd2s4ydy90rmr3bl39nar5")))

(define-public crate-wirdigen-0.3.0 (c (n "wirdigen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "jsonschema") (r "^0.15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "019jq3iw2k4hkljaymfjdjh5yjf82l2dqxvw8nqzvdcj62822k6l")))

