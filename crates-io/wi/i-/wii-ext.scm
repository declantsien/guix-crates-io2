(define-module (crates-io wi i- wii-ext) #:use-module (crates-io))

(define-public crate-wii-ext-0.1.0 (c (n "wii-ext") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0b2hzm6inf7d4x154cm8wafjghlxs1327cb77vfslm3v9wb5zyzy") (f (quote (("defmt_print" "defmt"))))))

(define-public crate-wii-ext-0.2.0 (c (n "wii-ext") (v "0.2.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0qj42mbm861lvjvbsrzdsyijgmfq6b0a0lw8bgn7dvarnpx7nlc0") (f (quote (("defmt_print" "defmt"))))))

(define-public crate-wii-ext-0.3.0 (c (n "wii-ext") (v "0.3.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0jbisjdb1fflvxjshalh1bpb8b7gvs96xi7jbwd19ykf7gkpw9xl") (f (quote (("defmt_print" "defmt"))))))

(define-public crate-wii-ext-0.4.0 (c (n "wii-ext") (v "0.4.0") (d (list (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "18xhjjp4nb8n2w4zaa2bl3c8bzz5xl4cgdp02s5d3ldq3s92ffin") (f (quote (("defmt_print" "defmt") ("default" "defmt_print"))))))

