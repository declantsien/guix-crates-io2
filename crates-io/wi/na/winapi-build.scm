(define-module (crates-io wi na winapi-build) #:use-module (crates-io))

(define-public crate-winapi-build-0.1.0 (c (n "winapi-build") (v "0.1.0") (h "05hhn8xdkc11ynbq63a6yzjz4ccmbcfk01wvizrba4cxad07k8z1")))

(define-public crate-winapi-build-0.1.1 (c (n "winapi-build") (v "0.1.1") (h "1g4rqsgjky0a7530qajn2bbfcrl2v0zb39idgdws9b1l7gp5wc9d")))

