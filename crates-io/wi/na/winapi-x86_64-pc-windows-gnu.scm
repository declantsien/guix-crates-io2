(define-module (crates-io wi na winapi-x86_64-pc-windows-gnu) #:use-module (crates-io))

(define-public crate-winapi-x86_64-pc-windows-gnu-0.3.0 (c (n "winapi-x86_64-pc-windows-gnu") (v "0.3.0") (h "078x7984yjzl9yi8wixifac5wc1q159qm894h87z5slc7jq9r8lc")))

(define-public crate-winapi-x86_64-pc-windows-gnu-0.3.1 (c (n "winapi-x86_64-pc-windows-gnu") (v "0.3.1") (h "0dykla091678lkijajhxvgma3z71clf5ayg399gk08gnry4ys958")))

(define-public crate-winapi-x86_64-pc-windows-gnu-0.3.2 (c (n "winapi-x86_64-pc-windows-gnu") (v "0.3.2") (h "0s36p4rwg7gd5b3p2j1b0hn2aj3ziw08xz9z5ifx0333n992rwcq")))

(define-public crate-winapi-x86_64-pc-windows-gnu-0.4.0 (c (n "winapi-x86_64-pc-windows-gnu") (v "0.4.0") (h "0gqq64czqb64kskjryj8isp62m2sgvx25yyj3kpc2myh85w24bki")))

