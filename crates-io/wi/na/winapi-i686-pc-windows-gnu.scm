(define-module (crates-io wi na winapi-i686-pc-windows-gnu) #:use-module (crates-io))

(define-public crate-winapi-i686-pc-windows-gnu-0.3.0 (c (n "winapi-i686-pc-windows-gnu") (v "0.3.0") (h "03r2arrkmnd234b3kfl9nbm32s1wzfqq4r0wfyqy50y8pwp8wsm1")))

(define-public crate-winapi-i686-pc-windows-gnu-0.3.1 (c (n "winapi-i686-pc-windows-gnu") (v "0.3.1") (h "0vn89k9nmhpkaz5z1i6472kgyq57wf63cvlhlzwvvklg3c5ssf6a")))

(define-public crate-winapi-i686-pc-windows-gnu-0.3.2 (c (n "winapi-i686-pc-windows-gnu") (v "0.3.2") (h "1k0lqrz91vi5j2anqswawg138hmlh4ys2qqyarfadv131kv6frpc")))

(define-public crate-winapi-i686-pc-windows-gnu-0.4.0 (c (n "winapi-i686-pc-windows-gnu") (v "0.4.0") (h "1dmpa6mvcvzz16zg6d5vrfy4bxgg541wxrcip7cnshi06v38ffxc")))

