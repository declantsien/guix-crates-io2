(define-module (crates-io wi na winapi-virtual-input) #:use-module (crates-io))

(define-public crate-winapi-virtual-input-0.1.0 (c (n "winapi-virtual-input") (v "0.1.0") (d (list (d (n "virtual-input") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "09qadid8w0z5r7xk8xxnm2zzd4h0015fqhhnd6jm2qsk971p7isx")))

(define-public crate-winapi-virtual-input-0.1.1 (c (n "winapi-virtual-input") (v "0.1.1") (d (list (d (n "virtual-input") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "1wzc3vvrmd50mjcjswihin3p2h4fwk9fyrssrag4a8xqxx8jqmkf")))

(define-public crate-winapi-virtual-input-0.1.2 (c (n "winapi-virtual-input") (v "0.1.2") (d (list (d (n "virtual-input") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "0qnxggm51cvizm54n8msnpi458caqg45b5yvfalhgy458zdqszcf")))

(define-public crate-winapi-virtual-input-0.1.3 (c (n "winapi-virtual-input") (v "0.1.3") (d (list (d (n "virtual-input") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("hidsdi" "cfgmgr32" "fileapi" "handleapi" "hidpi" "errhandlingapi" "winuser"))) (d #t) (k 0)))) (h "1psj4c3shkvisf0ahn7rawjf3lshy675ixbc9y2f6fmabgvfwb2x")))

