(define-module (crates-io wi na winamp-sys) #:use-module (crates-io))

(define-public crate-winamp-sys-0.1.0 (c (n "winamp-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "winuser" "guiddef" "debug"))) (d #t) (k 0)))) (h "1wlxrklx9f7yyczc982i36vp73gpy93yyn8pk737cjdrn22n8957") (f (quote (("wa-dlg") ("out") ("ipc-pe") ("in") ("gen") ("dsp"))))))

(define-public crate-winamp-sys-0.1.1 (c (n "winamp-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("minwindef" "windef" "winuser" "guiddef" "debug"))) (d #t) (k 0)))) (h "0skpczz7h6j6439hzld5c3mn7l7a2di885mfdp5raj9gz5v5dwws") (f (quote (("wa-dlg") ("out") ("ipc-pe") ("in") ("gen") ("dsp"))))))

