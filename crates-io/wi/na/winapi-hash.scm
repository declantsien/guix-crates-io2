(define-module (crates-io wi na winapi-hash) #:use-module (crates-io))

(define-public crate-winapi-hash-0.1.0 (c (n "winapi-hash") (v "0.1.0") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "188fc8dgk3f9gqrx2zshc280za0rvap6q8xzrclvvjs5d6k087z3") (y #t)))

(define-public crate-winapi-hash-0.1.1 (c (n "winapi-hash") (v "0.1.1") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "0jp8xq6h7isn808b0chn9haaiciwsljld0zv4kg6m76bfbqm78b9") (y #t)))

(define-public crate-winapi-hash-0.1.11 (c (n "winapi-hash") (v "0.1.11") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "04gkjbgryh5gd8qk9ij59cv34fppvzc7rg5k2vynh4q7y3ml6rzq") (y #t)))

(define-public crate-winapi-hash-0.1.12 (c (n "winapi-hash") (v "0.1.12") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "1029s7fyyxhfwpwc8ynpckzhk986bd6snzny01yiiaab66ww4cb8") (y #t)))

(define-public crate-winapi-hash-0.1.13 (c (n "winapi-hash") (v "0.1.13") (d (list (d (n "ntapi") (r "^0.3.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "0lyy9farmsgs2wh3qr3d1v8a1lkrd0yal07j2c1jhjq1risaf4yx") (y #t)))

(define-public crate-winapi-hash-0.1.2 (c (n "winapi-hash") (v "0.1.2") (d (list (d (n "ntapi") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "1za0kl2g3bxzywz9h0lx6g27rx84ny3aa491gg32rngjy6si81r0") (y #t)))

(define-public crate-winapi-hash-0.1.3 (c (n "winapi-hash") (v "0.1.3") (d (list (d (n "ntapi") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (k 0)))) (h "1i6cb7gp9927xy5ssqv0cp6ggjs4275i9yq6ca3baj79lr8ly8nw")))

(define-public crate-winapi-hash-0.0.0 (c (n "winapi-hash") (v "0.0.0") (h "03f67rvsxb5ah3w5nz44iic44m9kvlw6q4jj896wgkff390dxja9")))

