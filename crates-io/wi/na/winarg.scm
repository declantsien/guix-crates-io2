(define-module (crates-io wi na winarg) #:use-module (crates-io))

(define-public crate-winarg-0.1.0 (c (n "winarg") (v "0.1.0") (h "1p72bbp4hwpkzayy944166khlw049qpcbwx19c458zv9hx73w26l")))

(define-public crate-winarg-0.2.0 (c (n "winarg") (v "0.2.0") (h "0qqkg05jazlbksmc5w24lrz69r8077z799fsjw1yf3jimnhgfm0d")))

