(define-module (crates-io wi na winaudio) #:use-module (crates-io))

(define-public crate-winaudio-1.0.0 (c (n "winaudio") (v "1.0.0") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("mmsystem" "mmeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07cd86s090vap6jrybn3dfjjn1qna696h2hxjv9wz745cbvyx9f8")))

(define-public crate-winaudio-1.0.1 (c (n "winaudio") (v "1.0.1") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("mmsystem" "mmeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xarfzvv49c0qwvmyd2bygryfywxywpps0s0nnvwlxcyhixa59dw")))

(define-public crate-winaudio-1.0.2 (c (n "winaudio") (v "1.0.2") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("mmsystem" "mmeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bxjkzli8nafs2188zvlxpy2977zh6cgwvw9c2hlrmjmbjjswc2w")))

