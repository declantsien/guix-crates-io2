(define-module (crates-io wi na winapi-easy) #:use-module (crates-io))

(define-public crate-winapi-easy-0.1.0 (c (n "winapi-easy") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("objbase" "processthreadsapi" "shellapi" "shobjidl_core" "winbase" "wincon" "winerror" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "03lwsr677ncvsqssyr7zlfsdmldqvl9wkgknjh98yjzrqlcj53kv")))

