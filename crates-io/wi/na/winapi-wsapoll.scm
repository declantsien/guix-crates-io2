(define-module (crates-io wi na winapi-wsapoll) #:use-module (crates-io))

(define-public crate-winapi-wsapoll-0.1.0 (c (n "winapi-wsapoll") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19jwa76xqjwrmfkwzcxcbb37mcbm88c2nifmgw1vpjb8m16s41c1")))

(define-public crate-winapi-wsapoll-0.1.1 (c (n "winapi-wsapoll") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vnzlcm6yrlx0xdx4g7zr41n84aj73h0p8fwh0m60mbiyl873ha4")))

(define-public crate-winapi-wsapoll-0.1.2 (c (n "winapi-wsapoll") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winsock2"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0a1zxmpvxaw75y4lwavi6qbq95cnrz83a5p84rarjxn5g7vcbbqy")))

