(define-module (crates-io wi na winapi-util) #:use-module (crates-io))

(define-public crate-winapi-util-0.1.0 (c (n "winapi-util") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cnbg8qickp60zzc4943ig3bsjl60wflzy4nirjm4lc5irzjb9lb")))

(define-public crate-winapi-util-0.1.1 (c (n "winapi-util") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1av1nilnzv7xh5xa2kw6xh54ij43c0mqdsqzws2l5gy5b63m1idg")))

(define-public crate-winapi-util-0.1.2 (c (n "winapi-util") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j839dc6y8vszvrsb7yk0qvs0w6asnahxzbyans37vnsw6vbls3i")))

(define-public crate-winapi-util-0.1.3 (c (n "winapi-util") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "105dq898nah3dwrzr96vvb5srp6g2v5dl5vmzf211lba9iavzksc")))

(define-public crate-winapi-util-0.1.4 (c (n "winapi-util") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vj3984cxwnf1ys3fdz6bpl7p0kdsgykpzbhmcmwi759cd8mqlgs")))

(define-public crate-winapi-util-0.1.5 (c (n "winapi-util") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "winbase" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y71bp7f6d536czj40dhqk0d55wfbbwqfp2ymqf1an5ibgl6rv3h")))

(define-public crate-winapi-util-0.1.6 (c (n "winapi-util") (v "0.1.6") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "consoleapi" "errhandlingapi" "fileapi" "minwindef" "processenv" "sysinfoapi" "winbase" "wincon" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15i5lm39wd44004i9d5qspry2cynkrpvwzghr6s2c3dsk28nz7pj")))

(define-public crate-winapi-util-0.1.7 (c (n "winapi-util") (v "0.1.7") (d (list (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Console" "Win32_System_SystemInformation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15v6ajzp8pkki1hrw4kh341dci1sbp2sxpp8adjasisn7jhhchqk")))

(define-public crate-winapi-util-0.1.8 (c (n "winapi-util") (v "0.1.8") (d (list (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_Console" "Win32_System_SystemInformation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0svcgddd2rw06mj4r76gj655qsa1ikgz3d3gzax96fz7w62c6k2d")))

