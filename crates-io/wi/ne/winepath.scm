(define-module (crates-io wi ne winepath) #:use-module (crates-io))

(define-public crate-winepath-0.1.0 (c (n "winepath") (v "0.1.0") (h "0gx0r2zsq6w7gl5dix8p3z80gqq8r7a4lb9bfcd0pbg8wdcsjsxv")))

(define-public crate-winepath-0.1.1 (c (n "winepath") (v "0.1.1") (h "0dq2qacmqdl70ns3jpzkxk019j40vqsw71xbmrx8njjbb0fb4b6m")))

