(define-module (crates-io wi ne winex) #:use-module (crates-io))

(define-public crate-winex-0.1.0 (c (n "winex") (v "0.1.0") (d (list (d (n "cstr") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "winnt" "winbase" "windef" "minwindef" "ntdef" "winerror" "dwmapi" "uxtheme" "winuser" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "1swqzpn5v31dgs9vyv5gf5azip2hirqn7ykvxbha7032c45sx2ws")))

