(define-module (crates-io wi ne wine-rs) #:use-module (crates-io))

(define-public crate-wine-rs-0.0.1 (c (n "wine-rs") (v "0.0.1") (d (list (d (n "wine-macro") (r "^0.0.1") (d #t) (k 0)))) (h "1b8xk2hg844a0ms9s8p9vq7xpprfyzhfp8l579cgsfbahggfw6rb")))

(define-public crate-wine-rs-0.0.2 (c (n "wine-rs") (v "0.0.2") (d (list (d (n "wine-macro") (r "^0.0.1") (d #t) (k 0)))) (h "1q2zfn2igwvyhmxj9zwz4q2bvz9kabaa6b4qnah5g4b91q4ksy2f")))

