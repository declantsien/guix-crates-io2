(define-module (crates-io wi ne wine-macro) #:use-module (crates-io))

(define-public crate-wine-macro-0.0.1 (c (n "wine-macro") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.17.1") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1.0.57") (k 0)) (d (n "quote") (r "^1.0.27") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("parsing" "printing" "proc-macro" "full" "extra-traits"))) (k 0)))) (h "0cgl5fzwwyx7a7s74r09pg0ph2brcc5b0dzn3lfr9212pp4mn6yj")))

