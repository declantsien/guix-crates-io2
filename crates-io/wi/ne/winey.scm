(define-module (crates-io wi ne winey) #:use-module (crates-io))

(define-public crate-winey-0.1.0 (c (n "winey") (v "0.1.0") (d (list (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "safex") (r "^0.0.5") (f (quote ("xlib"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging" "Win32_Graphics_Dwm" "Win32_UI_Controls"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1270vbgw00bg9miz4x4d1wln6n2yajakghz717ap76my2mblqv5l")))

(define-public crate-winey-0.1.1 (c (n "winey") (v "0.1.1") (d (list (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "safex") (r "^0.0.5") (f (quote ("xlib"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging" "Win32_Graphics_Dwm" "Win32_UI_Controls" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dmy8fcpfd46qkkvwjwsdrbhiq5qmx1j39hn9lcsn4wafw3dih5x")))

