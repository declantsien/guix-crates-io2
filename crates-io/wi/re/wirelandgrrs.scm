(define-module (crates-io wi re wirelandgrrs) #:use-module (crates-io))

(define-public crate-wirelandgrrs-0.1.0 (c (n "wirelandgrrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "12l2r68aqarxnsg6y19xjbls7fmljq1gwq58wbgg8vaz1gh6lhmk")))

