(define-module (crates-io wi re wiretap) #:use-module (crates-io))

(define-public crate-wiretap-0.1.0 (c (n "wiretap") (v "0.1.0") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "01fsfmrvzgrnp6fq6699zwx4kswd76npnl7kz4d8ffjcpibrah57") (r "1.69")))

(define-public crate-wiretap-0.2.0 (c (n "wiretap") (v "0.2.0") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1fp1zhdga4g0hh0fcya1ppglqbv2fbhyzsjhl5dzx3i687jw65h6") (r "1.69")))

(define-public crate-wiretap-0.3.0 (c (n "wiretap") (v "0.3.0") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0c10picynl4hg9kml358cki625vrcng3qm419pa5m9vc16vhazj0") (r "1.69")))

(define-public crate-wiretap-0.3.1 (c (n "wiretap") (v "0.3.1") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "14hczm7vkcknrl050ydqgsmwi653yacaf7drjrshla1csnm1afp8") (r "1.69")))

(define-public crate-wiretap-0.3.2 (c (n "wiretap") (v "0.3.2") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "11lr3b9cp85k5yv8m36l3xzc4gbcd9smkyrd8fml7g6p8isfiwwa") (r "1.69")))

(define-public crate-wiretap-0.4.0 (c (n "wiretap") (v "0.4.0") (d (list (d (n "pnet") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1vnrkjnl946yjki32kwir0p7ym45lxixynv6j38a65kva5whs0vp") (r "1.69")))

