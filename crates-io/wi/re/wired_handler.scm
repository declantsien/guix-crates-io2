(define-module (crates-io wi re wired_handler) #:use-module (crates-io))

(define-public crate-wired_handler-0.1.0 (c (n "wired_handler") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "17qg47zcazf1qp43c21jba1njxpa486ck0bhrn1hgmd527h5rp50")))

(define-public crate-wired_handler-0.1.1 (c (n "wired_handler") (v "0.1.1") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "01wvm9ks0m6n6qv1kn5q1kypv8pxzblp779h281745yp3p94q5lh")))

(define-public crate-wired_handler-0.1.2 (c (n "wired_handler") (v "0.1.2") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "134vdpvw0f86jv2lflfz006y0yk2mlazybylc8vb5i0f0fjzm3sq")))

(define-public crate-wired_handler-0.1.3 (c (n "wired_handler") (v "0.1.3") (d (list (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "123lmsbj6bnwg74b1qdgakajmm3v7byd6wrphcdy0k90l7iv8z4b")))

(define-public crate-wired_handler-0.2.1 (c (n "wired_handler") (v "0.2.1") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1azihgh2ps3fx6xgwaa10ck590wq89vsh6gfp527j1ib366m99lp")))

(define-public crate-wired_handler-0.2.2 (c (n "wired_handler") (v "0.2.2") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "105isnhcxh14q8iamx28sprzmd93mijvr12qjg916vsbbi977ji5")))

(define-public crate-wired_handler-0.3.0 (c (n "wired_handler") (v "0.3.0") (d (list (d (n "futures-util") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1a6rsr0r7h5y5y73c7ywfa4i8k65va1470ribrwdx4fq8qcbkxvz")))

