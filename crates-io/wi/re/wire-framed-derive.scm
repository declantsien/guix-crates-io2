(define-module (crates-io wi re wire-framed-derive) #:use-module (crates-io))

(define-public crate-wire-framed-derive-0.1.0 (c (n "wire-framed-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "120wz50774l4pwnjpf23dvjgmm1xpd6y14rg3kascfddjqmd9yj0")))

(define-public crate-wire-framed-derive-0.1.1 (c (n "wire-framed-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ixmllh38gadnw2pyfririlxkrxkrnk4k1zzyfzspvvl412k41cm")))

(define-public crate-wire-framed-derive-0.1.2 (c (n "wire-framed-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04hnh7z24h98yh9yphlypknpsbqcf56ixzw7v5ll6vgqx0hvzwfi")))

(define-public crate-wire-framed-derive-0.1.3 (c (n "wire-framed-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06g6mfz81v609sv3qlvfvmbaqk73d4wx3z228zbk75mjkf4niz0x")))

(define-public crate-wire-framed-derive-0.1.4 (c (n "wire-framed-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12ibnfng4qvvmggmgvaz679ah0l23508rgn483mwkpca8aqd9sb1")))

