(define-module (crates-io wi re wiremock-multipart) #:use-module (crates-io))

(define-public crate-wiremock-multipart-0.1.0 (c (n "wiremock-multipart") (v "0.1.0") (d (list (d (n "async-std") (r "^1.10") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "lazy-regex") (r "^2.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "wiremock") (r "^0.5") (d #t) (k 0)))) (h "18v64pv85pbzr01fvwks9rz2iil3br1732rhj5a4j4hbf5wai8ab")))

