(define-module (crates-io wi re wirebolt) #:use-module (crates-io))

(define-public crate-wirebolt-0.1.0 (c (n "wirebolt") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "184gskdkk3xc0p8vhh459ikczz2xlkpwgbz3br6b8bn3xgn4bpdd")))

(define-public crate-wirebolt-0.1.1 (c (n "wirebolt") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b8xiqjcppkicx02nqzm5sza7acxg2rj2yhw0j2s3zv5a6yd1rh5")))

