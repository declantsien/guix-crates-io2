(define-module (crates-io wi re wirehair-wrapper) #:use-module (crates-io))

(define-public crate-wirehair-wrapper-0.1.0 (c (n "wirehair-wrapper") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0lln61ckkay9fy1md74n9x4jw2vsx9cd9y42dlgbac03lx76cswr")))

(define-public crate-wirehair-wrapper-0.1.1 (c (n "wirehair-wrapper") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0628p547ffvw3841s8m86wv9dh91l08rcnn2clm9x831c17i5rl0")))

(define-public crate-wirehair-wrapper-0.1.2 (c (n "wirehair-wrapper") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1jplhy4h6i4g179n2gs99xk9y42j5zds4j3yhl5lgl92wlkd3pyc")))

(define-public crate-wirehair-wrapper-0.1.3 (c (n "wirehair-wrapper") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1x3zfn01icxbmys0kx13sd1lf3sjf88agz86d7wm34lhz3xckjyc")))

