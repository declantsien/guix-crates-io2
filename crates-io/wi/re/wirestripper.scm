(define-module (crates-io wi re wirestripper) #:use-module (crates-io))

(define-public crate-wirestripper-0.1.0 (c (n "wirestripper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f4kdj1xpyzp4i4pnr2fhgwjlvr08a9ycrrpsx8d1qrmwxz0pgw0")))

(define-public crate-wirestripper-0.1.1 (c (n "wirestripper") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02vha78ffdi35wbbgw6p6bnxs9njg85flxyq7jfvmlw8nya3saby")))

(define-public crate-wirestripper-0.1.2 (c (n "wirestripper") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15sbvd94llm6v1vlm3ilgwvm54mc1b2n9l89c3inz0g4i8ih8hys")))

(define-public crate-wirestripper-0.1.3 (c (n "wirestripper") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "111r3yvfx46v98h4qgp12j37psviywpf8gfj39w4z16z4zlvv6pm")))

(define-public crate-wirestripper-0.1.4 (c (n "wirestripper") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ksacn2ksj12gs20r07lz899jzh7dpvpxzz2klpnm9rm2b2wrwj9")))

(define-public crate-wirestripper-0.1.5 (c (n "wirestripper") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2") (d #t) (k 0)) (d (n "rpcap") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "168v374jn07fa2njl9jv6pi054018akxhsb4jv4sqn8nhkqkrlbq")))

