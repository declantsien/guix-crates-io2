(define-module (crates-io wi re wire4) #:use-module (crates-io))

(define-public crate-wire4-0.0.1 (c (n "wire4") (v "0.0.1") (d (list (d (n "hash32") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5.2") (d #t) (k 0)))) (h "151v8vak9r4azi0c5p6mq3w3vvvrz29wlq54rrpk7k9zf4xavgr1")))

(define-public crate-wire4-0.0.2 (c (n "wire4") (v "0.0.2") (d (list (d (n "hash32") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5.2") (d #t) (k 0)))) (h "1rambdg1xlfvwi36w295j1h808pba09r68fjnab627j7hz4zcbkg")))

