(define-module (crates-io wi re wire-framed) #:use-module (crates-io))

(define-public crate-wire-framed-0.1.0 (c (n "wire-framed") (v "0.1.0") (d (list (d (n "wire-framed-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hwrfyra7bl4j4vsc7339ldx8ihwwn563iml4rm28wrrvdmx3ps8")))

(define-public crate-wire-framed-0.1.1 (c (n "wire-framed") (v "0.1.1") (d (list (d (n "wire-framed-core") (r "^0.1") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1") (d #t) (k 0)))) (h "1l93nyqc6nqrbs7gidv7ljqf0ds4dwbnsnmmk4by5paf56i6v71q")))

(define-public crate-wire-framed-0.1.2 (c (n "wire-framed") (v "0.1.2") (d (list (d (n "wire-framed-core") (r "^0.1") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1") (d #t) (k 0)))) (h "0pp49kkwivvb6czf42i5g62immwv354ji37ra7gpjf9jwmb748b3")))

(define-public crate-wire-framed-0.1.3 (c (n "wire-framed") (v "0.1.3") (d (list (d (n "wire-framed-core") (r "^0.1") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1") (d #t) (k 0)))) (h "1q4vc6a9lkq2sc8xkr6m845f6bskrxd78gah4xn9ng10g65qz0ns")))

(define-public crate-wire-framed-0.1.4 (c (n "wire-framed") (v "0.1.4") (d (list (d (n "wire-framed-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0c4i1kqj1kxdwl8f3p5lndgqvj9l9apn5qhc8sxlgcqc4qsv604v")))

(define-public crate-wire-framed-0.1.5 (c (n "wire-framed") (v "0.1.5") (d (list (d (n "wire-framed-core") (r "^0.1.2") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0c1mh1nin2a390bjwv9f6czsm1pdb7maaqdc9b3ri70hjmrhd6lx")))

(define-public crate-wire-framed-0.1.6 (c (n "wire-framed") (v "0.1.6") (d (list (d (n "wire-framed-core") (r "^0.1.3") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1ah69n31frhbwls4yh2ah8y4np7viajyfkiibdjny6zc93p7jffj")))

(define-public crate-wire-framed-0.1.7 (c (n "wire-framed") (v "0.1.7") (d (list (d (n "wire-framed-core") (r "^0.1.3") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1v02p3i4q2vv5pj2hjpj2pgzgrnvmdpznc3zj6xjssakhnfqklwk")))

(define-public crate-wire-framed-0.1.8 (c (n "wire-framed") (v "0.1.8") (d (list (d (n "wire-framed-core") (r "^0.1.4") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0rly1jx8ryws96n6c7kmanmvv414vzwmb8zc1qqv1hghis2yh7kx")))

(define-public crate-wire-framed-0.2.0 (c (n "wire-framed") (v "0.2.0") (d (list (d (n "wire-framed-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1gcz9hgg7nq9zkjlhgqj5qp6sl7d1xn1whafwvzsgzn351fbhrds")))

(define-public crate-wire-framed-0.3.0 (c (n "wire-framed") (v "0.3.0") (d (list (d (n "wire-framed-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1av42sxs8165hiddb4z2l0vnnbgi6j5rlv967v4ivihk6xp7ih7d")))

(define-public crate-wire-framed-0.4.0 (c (n "wire-framed") (v "0.4.0") (d (list (d (n "wire-framed-core") (r "^0.4.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1bk5l9lgbv7xmrklf3ms1m9mg5nbhp6v0gzixzjq5hpvmqi65jic")))

(define-public crate-wire-framed-0.4.1 (c (n "wire-framed") (v "0.4.1") (d (list (d (n "wire-framed-core") (r "^0.4.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.3") (d #t) (k 0)))) (h "14h5pxqvxmbk7912941fj1gm9pn505qvlx2ym74jqakgw38y56dv")))

(define-public crate-wire-framed-0.4.2 (c (n "wire-framed") (v "0.4.2") (d (list (d (n "wire-framed-core") (r "^0.4.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.4") (d #t) (k 0)))) (h "12kx7iglknvdydk9q616438z05r6nn260ldxn2aql207k1vn0qzl")))

(define-public crate-wire-framed-0.5.0 (c (n "wire-framed") (v "0.5.0") (d (list (d (n "wire-framed-core") (r "^0.5.0") (d #t) (k 0)) (d (n "wire-framed-derive") (r "^0.1.4") (d #t) (k 0)))) (h "0v4y28g17wbq9syg0x84vh4mqha0yvmhch96yl8njxmkx6fci4vf")))

