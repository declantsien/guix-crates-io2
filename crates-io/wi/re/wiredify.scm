(define-module (crates-io wi re wiredify) #:use-module (crates-io))

(define-public crate-wiredify-0.1.1 (c (n "wiredify") (v "0.1.1") (h "1pdf6j3nb71ssqkl09n8z7si8j9yjqf9g0d2x7hmy5a4xpxf0bln")))

(define-public crate-wiredify-0.2.0 (c (n "wiredify") (v "0.2.0") (h "1531jlcj56xr4ix0v1iz0pidaqy41f7pna82hjx3jaa7f2clfrn3")))

