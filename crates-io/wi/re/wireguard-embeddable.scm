(define-module (crates-io wi re wireguard-embeddable) #:use-module (crates-io))

(define-public crate-wireguard-embeddable-0.1.0 (c (n "wireguard-embeddable") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00nnqg8lalasvlzc9b7q06yp2fclz7a7kb60j1pip3zji0k1hq2h")))

(define-public crate-wireguard-embeddable-0.1.1 (c (n "wireguard-embeddable") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0c6clk2vzs4839jssbw74vq9kkz074dkj9ymqww7kw0gxs16c1rw")))

(define-public crate-wireguard-embeddable-0.1.2 (c (n "wireguard-embeddable") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lrahzl88qy3f05ccwr2hgpdw26gr8s6ijysyqryjsgzn8h2hw2w")))

(define-public crate-wireguard-embeddable-0.1.3 (c (n "wireguard-embeddable") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06yvqbabclnbci5x1n3ppaqj654cszcqvrrr37xnymly2cdkbqdl")))

