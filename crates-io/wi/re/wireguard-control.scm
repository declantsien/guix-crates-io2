(define-module (crates-io wi re wireguard-control) #:use-module (crates-io))

(define-public crate-wireguard-control-1.5.0-beta.4 (c (n "wireguard-control") (v "1.5.0-beta.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-pre.1") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "wireguard-control-sys") (r "^1.5.0-beta.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1mxvzzx291n40xklsdi3rfbr9r0pzzx8in0407mi1557pb6cszxs")))

(define-public crate-wireguard-control-1.5.0 (c (n "wireguard-control") (v "1.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^4.0.0-pre.1") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (t "cfg(not(target_os = \"linux\"))") (k 0)) (d (n "wireguard-control-sys") (r "^1.5.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "03bb4lvbsx98jnxj8hq83wghswhfqbr4iklhc3p4svmlajqzxqjb")))

