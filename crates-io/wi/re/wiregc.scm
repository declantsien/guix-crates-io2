(define-module (crates-io wi re wiregc) #:use-module (crates-io))

(define-public crate-wiregc-0.1.0 (c (n "wiregc") (v "0.1.0") (d (list (d (n "gdk-pixbuf") (r "^0.18.3") (d #t) (k 0)) (d (n "glib") (r "^0.18.4") (d #t) (k 0)) (d (n "gtk") (r "^0.18.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0chfk9z0gi0jjz0wzi208vbimi2zk6ks55hkyajc3zc34psy8c7y") (y #t)))

