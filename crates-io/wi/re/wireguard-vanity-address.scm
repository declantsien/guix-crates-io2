(define-module (crates-io wi re wireguard-vanity-address) #:use-module (crates-io))

(define-public crate-wireguard-vanity-address-0.2.0 (c (n "wireguard-vanity-address") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.3") (f (quote ("std" "u64_backend"))) (k 0)))) (h "0q3gbd75qd8zxxsnlg0bzsrrdgzz8qdjc113pr397iwlxyswc2zj")))

(define-public crate-wireguard-vanity-address-0.3.0 (c (n "wireguard-vanity-address") (v "0.3.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.5.1") (d #t) (k 0)))) (h "07mp7622pim2fgz1bfyfsn1ab8x8vhhyg0fk83maq9jslmkjajnz")))

(define-public crate-wireguard-vanity-address-0.3.1 (c (n "wireguard-vanity-address") (v "0.3.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.5.1") (d #t) (k 0)))) (h "1yjks6wnznsx8f1vkmj7pcgscw3alc02qi7kl3v2kx8217lrb5ya")))

(define-public crate-wireguard-vanity-address-0.4.0 (c (n "wireguard-vanity-address") (v "0.4.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.5.1") (d #t) (k 0)))) (h "10q1kqm453y5mjvzvjxd8r2fp7plqkc619v38nbdiz7vbi43gwcm")))

