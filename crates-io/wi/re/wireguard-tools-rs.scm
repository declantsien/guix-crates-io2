(define-module (crates-io wi re wireguard-tools-rs) #:use-module (crates-io))

(define-public crate-wireguard-tools-rs-0.1.0 (c (n "wireguard-tools-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "netns") (r "^0.1.0") (d #t) (k 2)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "sudo") (r "^0.3") (d #t) (k 2)))) (h "0vw208an86ys0lh2wnkm2rzzcjzsnwy9rdv5fzjabki4j76j481g")))

