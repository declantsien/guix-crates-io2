(define-module (crates-io wi re wire) #:use-module (crates-io))

(define-public crate-wire-0.0.1 (c (n "wire") (v "0.0.1") (d (list (d (n "bchannel") (r "^0.0.1") (d #t) (k 0)) (d (n "bincode") (r "^0.0.1") (d #t) (k 0)))) (h "1knzw9yj7ndg6kd8icf9m41i4ybgr1marrhwyw68rdcddpb4cn5z")))

(define-public crate-wire-0.0.2 (c (n "wire") (v "0.0.2") (d (list (d (n "bchannel") (r "~0.0.1") (d #t) (k 0)) (d (n "bincode") (r "~0.0.2") (d #t) (k 0)))) (h "0jpvcdqllxamdl3fh77d9272cm6yvc9ldxjsrm1naimwll33kffm")))

(define-public crate-wire-0.0.3 (c (n "wire") (v "0.0.3") (d (list (d (n "bchannel") (r "^0.0.1") (d #t) (k 0)) (d (n "bincode") (r "^0.0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.1.4") (d #t) (k 0)))) (h "1ip0yx4jqs3vhazv7mgn8mzx3949g7siijj449jy79snqfsl5ynf")))

(define-public crate-wire-0.0.4 (c (n "wire") (v "0.0.4") (d (list (d (n "bchannel") (r "~0.0.2") (d #t) (k 0)) (d (n "bincode") (r "~0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.3") (d #t) (k 0)))) (h "09bl43gbfjzx8b3d8pwrlknqpj30ifx8gnzi56jw33ym3d3rhhzv")))

(define-public crate-wire-0.0.5 (c (n "wire") (v "0.0.5") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.2.*") (d #t) (k 0)))) (h "0vsnh3kcsg8z5jhrx2jn1qmj9sssnz1rjhwnjzkaj4hh3jix738z")))

(define-public crate-wire-0.0.6 (c (n "wire") (v "0.0.6") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.2.*") (d #t) (k 0)))) (h "0d1hdqlkhcqs2b36vkkf35h9s90kq0dg8kg2af4s8cpscckr9rx4")))

(define-public crate-wire-0.0.7 (c (n "wire") (v "0.0.7") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1zkrv93x3f1rzyz7bwh9mpqplr80byxyl2qgpj00djdvc1k35g7z")))

(define-public crate-wire-0.0.8 (c (n "wire") (v "0.0.8") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0qqhld3l4cjl940sfcr243g4jwvadcfgdnbklk19x01gq64cqz1p")))

(define-public crate-wire-0.0.9 (c (n "wire") (v "0.0.9") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "^0.0.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "12xr9pfdw3l03cwg9npn20ypzg86f8dz9falin677vg4id6f3md7")))

(define-public crate-wire-0.0.11 (c (n "wire") (v "0.0.11") (d (list (d (n "bchannel") (r "*") (d #t) (k 0)) (d (n "bincode") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "unreliable-message") (r "^0.0.3") (d #t) (k 0)))) (h "18h0x3kibyrxc3lxq629v8x490hgp10h76n2h4xmhxla83jpz7wc")))

(define-public crate-wire-0.0.12 (c (n "wire") (v "0.0.12") (d (list (d (n "bchannel") (r "0.0.*") (d #t) (k 0)) (d (n "bincode") (r "0.5.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "unreliable-message") (r "^0.1.0") (d #t) (k 0)))) (h "1n162dg16nv0992dk8xh9hsydq15avpl7y9rlhccb5g3cgpb1xm2")))

