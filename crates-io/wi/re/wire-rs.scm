(define-module (crates-io wi re wire-rs) #:use-module (crates-io))

(define-public crate-wire-rs-0.1.0 (c (n "wire-rs") (v "0.1.0") (h "1a66mysins5ikjyzp5j8g91qw4gs77gpd4387l4b37j1y0g7c4fl") (f (quote (("std") ("default" "std"))))))

(define-public crate-wire-rs-0.1.1 (c (n "wire-rs") (v "0.1.1") (h "001x39hf830idwxgydn7g0kipjr40l5iwgiwj7bi9zy938g6x50c") (f (quote (("std") ("ioslice" "std") ("default" "std"))))))

(define-public crate-wire-rs-0.2.0 (c (n "wire-rs") (v "0.2.0") (h "0mc8nrjgvxk5cmxq0pm1ms39p7967nppdzp54195v5gv0xm0srd4") (f (quote (("std") ("ioslice" "std") ("default" "std"))))))

(define-public crate-wire-rs-0.2.1 (c (n "wire-rs") (v "0.2.1") (h "1h6vkv4wgmfk135fq4wvw26yarb1h4n3q02va7ljnw8fplblb7ah") (f (quote (("std") ("ioslice" "std") ("default" "std"))))))

