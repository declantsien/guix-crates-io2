(define-module (crates-io wi re wireguard_tools_rehtt) #:use-module (crates-io))

(define-public crate-wireguard_tools_rehtt-0.1.0 (c (n "wireguard_tools_rehtt") (v "0.1.0") (d (list (d (n "bytes_size") (r "^0.1.2") (d #t) (k 0)) (d (n "ini_lib") (r "^0.1.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "17m0xy5i559w1w0mvsyqz2brr2fsyds45qancvzz8hxmx72z3m6c")))

(define-public crate-wireguard_tools_rehtt-0.1.1 (c (n "wireguard_tools_rehtt") (v "0.1.1") (d (list (d (n "bytes_size") (r "^0.1.2") (d #t) (k 0)) (d (n "ini_lib") (r "^0.1.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.5.0") (d #t) (k 0)))) (h "1bkviibsj5fb7dcxzimwl88z0k8i64gq2ismfrdvxdc0p8wghw5n")))

