(define-module (crates-io wi re wireless) #:use-module (crates-io))

(define-public crate-wireless-0.1.0 (c (n "wireless") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "15pcv7lx1jmcy0h3spqyhkf7iqwaxlsgxk9p94qsi0xqgg4n2vgl")))

(define-public crate-wireless-0.1.1 (c (n "wireless") (v "0.1.1") (d (list (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0asbqdzaslzkdqkhic3jpmyyi3gmfx88nn2bwgdp9m45l4wvac0p")))

(define-public crate-wireless-0.1.2 (c (n "wireless") (v "0.1.2") (d (list (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14q9iqlx2hx0llj42nqwh0zc83v24g7xxg6y2v9qrvs92vb6sy8h")))

(define-public crate-wireless-0.2.0 (c (n "wireless") (v "0.2.0") (d (list (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l872c0l7wh68rwzap9ka2k8b2sp2bsrfp2zmhh5f4dj5grn1zr5")))

(define-public crate-wireless-0.2.1 (c (n "wireless") (v "0.2.1") (d (list (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "1fx5j3bkifa9s61gg3sk6ysixv1bysxm2ysghidj2vrzy2rfim9c")))

(define-public crate-wireless-0.2.2 (c (n "wireless") (v "0.2.2") (d (list (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.1") (d #t) (k 0)))) (h "05l93b8innla8qiq7ch1a1ih5zwlb504c21c54scbwpyfhfqgf2h")))

