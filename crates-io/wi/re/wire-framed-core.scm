(define-module (crates-io wi re wire-framed-core) #:use-module (crates-io))

(define-public crate-wire-framed-core-0.1.0 (c (n "wire-framed-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "18ddliw2lyi729nrh1prmwfbxz3rnhb6x89ms2d6a4rcrdnzg574")))

(define-public crate-wire-framed-core-0.1.1 (c (n "wire-framed-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1x0rbsmm19sk5igfhaq8zs83pb7vpiss6mjn6abhdxxjx80639wn")))

(define-public crate-wire-framed-core-0.1.2 (c (n "wire-framed-core") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0z0ypf7xjgr1f80wn4wlxybx7p77h0zv9va8akbgp36hcxafydm1")))

(define-public crate-wire-framed-core-0.1.3 (c (n "wire-framed-core") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "1p0brzw62pdfr74hdh4w317zr7hrpv8077farximh3bq4vjb3lqp")))

(define-public crate-wire-framed-core-0.1.4 (c (n "wire-framed-core") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0c978a6cl0qrvh8xn2s2yqncyk3gakg57sl6f70pam5zlxg0dbic")))

(define-public crate-wire-framed-core-0.2.0 (c (n "wire-framed-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0wjn2ixxlj9wp84clksip2grdqpc0axjpd7xs25xccic1pzgy5m0")))

(define-public crate-wire-framed-core-0.3.0 (c (n "wire-framed-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0sv633y11slv0djlldssrqjd6af80izijdx2yyqdp7hff8dblzq0")))

(define-public crate-wire-framed-core-0.4.0 (c (n "wire-framed-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0v1hgyz48xwa5iy5s6l4z5s4w51d1x63zh6d8pqy329mk0ydzkb8")))

(define-public crate-wire-framed-core-0.5.0 (c (n "wire-framed-core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "rassert-rs") (r "^3.0") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0mjb60yk6kygv17y3r3dq8i4kcgms4c2k3jd165if9p27vygaz88")))

