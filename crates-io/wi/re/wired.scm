(define-module (crates-io wi re wired) #:use-module (crates-io))

(define-public crate-wired-0.1.0 (c (n "wired") (v "0.1.0") (h "0668z9yyxfifzwcq0jgvrwc5xgzrwsj88r9amciskj1j3hxz4fvn")))

(define-public crate-wired-0.2.0 (c (n "wired") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "17vc6gvy0ik2d67mc5rzncja0ilbrafp23ld13labvf05zrs8d1r")))

(define-public crate-wired-0.3.0 (c (n "wired") (v "0.3.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "116ygk99fk66xb5hsq7i1myi681sl5ap3hs47l4rihl5yfngjk6j")))

(define-public crate-wired-0.4.0 (c (n "wired") (v "0.4.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0hj1p3jv8s2lc0qxxyiipyycay3lwkfxjla9kw36dxa9mspqqawh")))

(define-public crate-wired-0.4.1 (c (n "wired") (v "0.4.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "07xdsjjwmpwlw4crk256apnqn3zl4wkd7pjwpd7jz36gnjzjj03g")))

(define-public crate-wired-0.4.2 (c (n "wired") (v "0.4.2") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0pwgyysil88czjzy3s6vzjqy27jrk23gxclfikddda9bkh1rc4fm")))

(define-public crate-wired-0.4.4 (c (n "wired") (v "0.4.4") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1dryxcnsvzz0vlifin0fvy6rdbrnrpl8i43pa3xfy7a4pa8d8d4j")))

(define-public crate-wired-0.4.5 (c (n "wired") (v "0.4.5") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1zyazhmcrkk1v06hnrbagdvcbx6vpp4bkkm25qhy74sm7bs6dcsz")))

(define-public crate-wired-0.5.0 (c (n "wired") (v "0.5.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "05s4v7kblh4qpix5lib00y3p47bi0ia3zfdgcm3c4mslmx6fqkk3")))

(define-public crate-wired-0.5.1 (c (n "wired") (v "0.5.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.1.0") (d #t) (k 0)) (d (n "page_size") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wil9r1822zzc8rym4z06p3lbdp0dxgkz9zznjqvhvccw2pfmksr")))

