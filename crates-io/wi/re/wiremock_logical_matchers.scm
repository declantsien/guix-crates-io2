(define-module (crates-io wi re wiremock_logical_matchers) #:use-module (crates-io))

(define-public crate-wiremock_logical_matchers-0.1.0 (c (n "wiremock_logical_matchers") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 2)) (d (n "wiremock") (r "^0.4.6") (d #t) (k 0)))) (h "0m533xbx9s8xnbmhh4lkm8sb1xny11974vwya78q6lc43ybgbx89")))

(define-public crate-wiremock_logical_matchers-0.1.1 (c (n "wiremock_logical_matchers") (v "0.1.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 2)) (d (n "wiremock") (r "^0") (d #t) (k 0)))) (h "15wsaxbgb9j0jiys9viwgfprgp2g1n1nrl1qp60hlfsadqfmwb0l")))

(define-public crate-wiremock_logical_matchers-0.1.2 (c (n "wiremock_logical_matchers") (v "0.1.2") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 2)) (d (n "wiremock") (r "^0") (d #t) (k 0)))) (h "07l00m2zbpnkhv3bwfh4wrhhhwdl0m3dnrmc0mq0npybfv8gndvi")))

(define-public crate-wiremock_logical_matchers-0.1.3 (c (n "wiremock_logical_matchers") (v "0.1.3") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 2)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "wiremock") (r "^0") (d #t) (k 0)))) (h "11rblr5kazllg89w4nhvjlhcqsdzgjz4qgyyyv635grc5kxp7iax")))

(define-public crate-wiremock_logical_matchers-0.5.0 (c (n "wiremock_logical_matchers") (v "0.5.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5") (d #t) (k 0)))) (h "0bx716fb2xmi3rrs5zwgwlnvy773cn16mvq7g7ajw4z6yw8wy4fn") (r "1.68.2")))

(define-public crate-wiremock_logical_matchers-0.5.1 (c (n "wiremock_logical_matchers") (v "0.5.1") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.5.22") (d #t) (k 0)))) (h "15zq3s9l8vi2g9w0hjihvim7h91w785a8bjr28nmdydrmwb0hymx") (r "1.68.2")))

(define-public crate-wiremock_logical_matchers-0.6.0 (c (n "wiremock_logical_matchers") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.11.24") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "wiremock") (r "^0.6.0") (d #t) (k 0)))) (h "1982dv7idaiylmzdp3fl08c60v3mgn6nys0738i5adrazm04dszv") (r "1.68.2")))

