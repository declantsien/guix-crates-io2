(define-module (crates-io wi nh winhandle) #:use-module (crates-io))

(define-public crate-winhandle-0.1.0 (c (n "winhandle") (v "0.1.0") (d (list (d (n "kernel32-sys") (r ">= 0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3") (d #t) (k 0)) (d (n "winapi") (r ">= 0.2") (d #t) (k 0)))) (h "0fpd525imvp12mg6my18fgw7aspmhmga22ycjkabixm196m1pb89")))

(define-public crate-winhandle-0.2.0 (c (n "winhandle") (v "0.2.0") (d (list (d (n "advapi32-sys") (r ">= 0.2") (d #t) (k 2)) (d (n "kernel32-sys") (r ">= 0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3") (d #t) (k 0)) (d (n "user32-sys") (r ">= 0.2") (d #t) (k 2)) (d (n "widestring") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">= 0.2") (d #t) (k 0)))) (h "1c5fjq22q3igabxfs78ph3w2kjicaqil6fcnxqvc4vxz14lv31fw")))

(define-public crate-winhandle-0.3.0 (c (n "winhandle") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r ">= 0.3") (d #t) (k 0)) (d (n "widestring") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "winnt" "winbase" "unknwnbase" "winerror" "errhandlingapi" "handleapi" "winuser"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("namedpipeapi" "synchapi" "jobapi2"))) (d #t) (k 2)))) (h "0b2wihsn9rmx7ki1bxji5ks0krcab6nsscf44x0fbgjg8750197k")))

