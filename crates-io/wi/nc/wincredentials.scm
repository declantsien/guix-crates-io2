(define-module (crates-io wi nc wincredentials) #:use-module (crates-io))

(define-public crate-wincredentials-0.1.0 (c (n "wincredentials") (v "0.1.0") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "wincredentials-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)))) (h "0knfacza6bprkrw0z9r12i5khs4w71rk1afpayl4m5c740hzmjck")))

(define-public crate-wincredentials-0.1.1 (c (n "wincredentials") (v "0.1.1") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "wincredentials-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)))) (h "164vpfw3nqg5d0ijzzhqiic6ncgg1c2jxkn4jajv0awhmp5dydmq")))

(define-public crate-wincredentials-0.1.2 (c (n "wincredentials") (v "0.1.2") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "wincredentials-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)))) (h "00bjgz1f9x58lr3s0zdywwdvj9pjz0x7frsp41cnv77wbm8a1xx7")))

(define-public crate-wincredentials-0.2.0 (c (n "wincredentials") (v "0.2.0") (d (list (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "wincredentials-bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)))) (h "1jmpqr7nrq0zdrszb6ig90ynvj2nry1v22z4qmbpx6z9i82n5wbk")))

