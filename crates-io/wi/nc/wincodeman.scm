(define-module (crates-io wi nc wincodeman) #:use-module (crates-io))

(define-public crate-wincodeman-0.1.0 (c (n "wincodeman") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "embed-resource") (r "^1.5.1") (d #t) (k 1)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "1ch6qi86dh9nmwcqxl0gnaanlyxh34y3rl9ssz7k7gd153jph9wv")))

(define-public crate-wincodeman-0.1.1 (c (n "wincodeman") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "embed-resource") (r "^1.5.1") (d #t) (k 1)) (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation"))) (d #t) (k 0)))) (h "0nmhg2yg118csl1mds64gmd0vil15x20b2d4k32n3z5kg62hb8pg")))

