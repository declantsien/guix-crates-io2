(define-module (crates-io wi nc wincompatlib) #:use-module (crates-io))

(define-public crate-wincompatlib-0.1.0 (c (n "wincompatlib") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0l7j6dsj5jfw8i2w1ky8yj7pdgli4h551r45fcg764k3ik5kic8g") (s 2) (e (quote (("dxvk" "dep:regex"))))))

(define-public crate-wincompatlib-0.1.1 (c (n "wincompatlib") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "0fx79nigb0vh0ajkw15zn6r4w1psv6sc5nswilmy4zphiq34d7la") (s 2) (e (quote (("dxvk" "dep:regex"))))))

(define-public crate-wincompatlib-0.1.2 (c (n "wincompatlib") (v "0.1.2") (d (list (d (n "regex") (r "^1.6.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "05dp50j5a1p3khw9hcqjpb25incslrn74ipb1rw1c9j0262lrk9l") (s 2) (e (quote (("dxvk" "dep:regex"))))))

(define-public crate-wincompatlib-0.1.3 (c (n "wincompatlib") (v "0.1.3") (d (list (d (n "regex") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)))) (h "10iw4wjlkcjvwl6a5h46pnbzsfsmpfyi1av66pr6423c6xsh0ncp") (s 2) (e (quote (("dxvk" "dep:regex"))))))

(define-public crate-wincompatlib-0.2.0 (c (n "wincompatlib") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "0799dlk50mlwb6b5njjcfmdc5xgrbdqz4irv1ng6097yji4cdrlf") (f (quote (("default" "dxvk")))) (s 2) (e (quote (("dxvk" "dep:derive_builder"))))))

(define-public crate-wincompatlib-0.2.1 (c (n "wincompatlib") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "0bjvvr6r4lnj0cydwzlp77a6m37dvbqqbfdbznb36r3ajv8c92fw") (f (quote (("default" "dxvk")))) (s 2) (e (quote (("dxvk" "dep:derive_builder"))))))

(define-public crate-wincompatlib-0.2.2 (c (n "wincompatlib") (v "0.2.2") (d (list (d (n "derive_builder") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "06nijax1azb427rzb919cjy95w9bj8zq6w4gsf6yzm0wrdybf53x") (f (quote (("default" "dxvk")))) (s 2) (e (quote (("dxvk" "dep:derive_builder"))))))

(define-public crate-wincompatlib-0.3.0 (c (n "wincompatlib") (v "0.3.0") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1fjnc254h8cb8m97liq9iffsflvydgi9d3p8xk5w2kms8ryqj4fs") (f (quote (("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton"))))))

(define-public crate-wincompatlib-0.3.1 (c (n "wincompatlib") (v "0.3.1") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "04364ln6q2yhci28rbpidpxw01wmcya60a1xi3j13dwmdxrhz7d2") (f (quote (("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton"))))))

(define-public crate-wincompatlib-0.3.2 (c (n "wincompatlib") (v "0.3.2") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "0zc9xbbdkilj599pi57sz356q6i3cd4izwaizmlq9m7s07yhsv5a") (f (quote (("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton"))))))

(define-public crate-wincompatlib-0.4.0 (c (n "wincompatlib") (v "0.4.0") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "08xxw45vqhf8wdf84chk3crlwfr9rqclqz6ygj04fxpjmvbrh8g6") (f (quote (("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton"))))))

(define-public crate-wincompatlib-0.4.1 (c (n "wincompatlib") (v "0.4.1") (d (list (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "1y0dyw1k7f8dn4a3hzz6i6rl189bmmazvs2rl5hbw2p3i2gbp5ih") (f (quote (("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton"))))))

(define-public crate-wincompatlib-0.5.0 (c (n "wincompatlib") (v "0.5.0") (d (list (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "13nipss8y5x2g615clv7jvj98rn73myx1k9sablcqk0canmmf36c") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "winetricks"))))))

(define-public crate-wincompatlib-0.6.0 (c (n "wincompatlib") (v "0.6.0") (d (list (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1rw8xhx81xlgcvl62c0wjczy86v2x5xapihxb3wmwbaz8qc2kmqq") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "winetricks"))))))

(define-public crate-wincompatlib-0.6.1 (c (n "wincompatlib") (v "0.6.1") (d (list (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "150bpj5jq7zm4bxqs73jlyh9p6fv68kd1h155a3w7zb9kljnrwv6") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "winetricks"))))))

(define-public crate-wincompatlib-0.7.0 (c (n "wincompatlib") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8.1") (f (quote ("https-rustls" "https-rustls-probe"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "067h04i8c1i6kxczxgq5yk54h86nlnf72rs653ksf0f61s3ffh6b") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "wine-fonts" "winetricks")))) (s 2) (e (quote (("wine-fonts" "dep:minreq"))))))

(define-public crate-wincompatlib-0.7.1 (c (n "wincompatlib") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8.1") (f (quote ("https-rustls" "https-rustls-probe"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1f473xzx2ijxb60w73zh8ab78zf00xgxc63i84hvqw30428k446n") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "wine-fonts" "winetricks")))) (s 2) (e (quote (("wine-fonts" "dep:minreq"))))))

(define-public crate-wincompatlib-0.7.2 (c (n "wincompatlib") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8.1") (f (quote ("https-rustls" "https-rustls-probe"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "10a54p45fn7ll7zhnspx2c9pi5cak73z24vd6gf0ib8pnrqdzwfm") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "wine-fonts" "winetricks")))) (s 2) (e (quote (("wine-fonts" "dep:minreq"))))))

(define-public crate-wincompatlib-0.7.3 (c (n "wincompatlib") (v "0.7.3") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "minreq") (r "^2.8.1") (f (quote ("https-rustls" "https-rustls-probe"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "10zspda4s9sfcrrwwhgi4dmv0ixqqmx87zafvhxjf9997bb8vkc9") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "wine-fonts" "winetricks")))) (s 2) (e (quote (("wine-fonts" "dep:minreq"))))))

(define-public crate-wincompatlib-0.7.4 (c (n "wincompatlib") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "minreq") (r "^2.11.0") (f (quote ("https-rustls" "https-rustls-probe"))) (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1n18wkvpakrzlxmmh1a1nd60d29widjd8fv3h213fy3bq4y7dc87") (f (quote (("winetricks") ("wine-proton" "wine-bundles") ("wine-bundles") ("dxvk") ("all" "dxvk" "wine-bundles" "wine-proton" "wine-fonts" "winetricks")))) (s 2) (e (quote (("wine-fonts" "dep:minreq" "dep:blake3"))))))

