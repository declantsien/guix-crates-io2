(define-module (crates-io wi nc wincolor) #:use-module (crates-io))

(define-public crate-wincolor-0.1.0 (c (n "wincolor") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0bxbfwpvxchciiz0ha15rrr0cmf2cvp36f1cdl99m24h87nfbbsb")))

(define-public crate-wincolor-0.1.1 (c (n "wincolor") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0a859q9g3dc3b4p7bzrxrg9niz2avvn48j09sr4dz9jrsschs0f5")))

(define-public crate-wincolor-0.1.2 (c (n "wincolor") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0ky2g3x94m6v8rdlpljar74mq4jf8kzkzx55qbwv3ayjc1w6qglw")))

(define-public crate-wincolor-0.1.3 (c (n "wincolor") (v "0.1.3") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "16vpx4g2yvrq9qblkwbmd3hgqfnnljr0k2yqcrpnw5p24qaazhlr")))

(define-public crate-wincolor-0.1.4 (c (n "wincolor") (v "0.1.4") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "1c5qb07dca0nqnadjwf859vmf8xb2r11a87zj84l7xh8893f97m3")))

(define-public crate-wincolor-0.1.5 (c (n "wincolor") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "processenv" "winbase" "wincon"))) (d #t) (k 0)))) (h "00x2v2pbdb307ww7bcs5c919hw3rch6z6xhadh0d4f48m1zihy08")))

(define-public crate-wincolor-0.1.6 (c (n "wincolor") (v "0.1.6") (d (list (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "minwindef" "processenv" "winbase" "wincon"))) (d #t) (k 0)))) (h "0rvpvv26a8c4dla5i5hsxlkvjcjjbl0dylhhg4147m54lfcn9c7f")))

(define-public crate-wincolor-1.0.0 (c (n "wincolor") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "minwindef" "processenv" "winbase" "wincon"))) (d #t) (k 0)))) (h "06lqm9hffjwimrw8fiax78yjxlpaq4cldiahc6hvb66svjlkmp5r")))

(define-public crate-wincolor-1.0.1 (c (n "wincolor") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "wincon"))) (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.1") (d #t) (k 0)))) (h "1fp9sxq63kw3vjjcjrl3f7px082pplzxcr3qza2n2pa6mq0xj7jn")))

(define-public crate-wincolor-1.0.2 (c (n "wincolor") (v "1.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "wincon"))) (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.1") (d #t) (k 0)))) (h "1agaf3hcav113i86912ajnw6jxcy4rvkrgyf8gdj8kc031mh3xcn")))

(define-public crate-wincolor-1.0.3 (c (n "wincolor") (v "1.0.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "wincon"))) (d #t) (k 0)) (d (n "winapi-util") (r "^0.1.1") (d #t) (k 0)))) (h "017x33ljndwc76cp5z9llgndn0nh7v8jcjaykbizkawmwy9n3pyp")))

