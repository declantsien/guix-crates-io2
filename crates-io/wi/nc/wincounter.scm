(define-module (crates-io wi nc wincounter) #:use-module (crates-io))

(define-public crate-wincounter-0.1.0 (c (n "wincounter") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qa6rgqrz29l8c2ns5s3p3ldkshc93zgp6zxa80hx3ab09s2xh5h")))

(define-public crate-wincounter-0.1.1 (c (n "wincounter") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pc8i45pi7bq1car206wxrazpfay6lvvdfc80di3va1q5a9m29z9")))

