(define-module (crates-io wi nc winch-environ) #:use-module (crates-io))

(define-public crate-winch-environ-0.6.0 (c (n "winch-environ") (v "0.6.0") (d (list (d (n "wasmparser") (r "^0.102.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=8.0.0") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.6.0") (d #t) (k 0)))) (h "1i12a6fbiq5ra63sdbxmz3a0cdgz3g9rlfnnam003kw19gqfbd4j")))

(define-public crate-winch-environ-0.6.1 (c (n "winch-environ") (v "0.6.1") (d (list (d (n "wasmparser") (r "^0.102.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=8.0.1") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.6.1") (d #t) (k 0)))) (h "03j0lxmkgjmkzb5wj014mxgwzv9kv40fcjj7a1vymicaslihl3gm")))

(define-public crate-winch-environ-0.7.0 (c (n "winch-environ") (v "0.7.0") (d (list (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=9.0.0") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.7.0") (d #t) (k 0)))) (h "0hxpfx359n7j5d6k5z0lcjiqn2d10y725ag27y7nhhks8yndb96g")))

(define-public crate-winch-environ-0.7.1 (c (n "winch-environ") (v "0.7.1") (d (list (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=9.0.1") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.7.1") (d #t) (k 0)))) (h "1sdwq0mhxnbznk7ib8lzkd8942jazpwwqa8qzws0ml18i9ah9ih2")))

(define-public crate-winch-environ-0.7.2 (c (n "winch-environ") (v "0.7.2") (d (list (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=9.0.2") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.7.2") (d #t) (k 0)))) (h "0wzwvrch9hxy2238y4dp68m6ljmyqczbnbbf7zsbjz7k6k6w63pq")))

(define-public crate-winch-environ-0.7.3 (c (n "winch-environ") (v "0.7.3") (d (list (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=9.0.3") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.7.3") (d #t) (k 0)))) (h "0q9l8bfw95cx31q9lhilrwyd5iwn0qg1ixi5pi6y8madmb39prg7")))

(define-public crate-winch-environ-0.7.4 (c (n "winch-environ") (v "0.7.4") (d (list (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "=9.0.4") (d #t) (k 0)) (d (n "winch-codegen") (r "=0.7.4") (d #t) (k 0)))) (h "0ccl9mfzwddxa4742zqwmhw9rdwy3gv5hmplax4g7kdcimbwg9af")))

