(define-module (crates-io wi st wistime) #:use-module (crates-io))

(define-public crate-wistime-0.1.0 (c (n "wistime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.8.5") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1zlb2aly8zmff7zx3zr3nxir2xiszmy7yz7wziq0ly18d23d84sk")))

