(define-module (crates-io wi n_ win_fast_rm) #:use-module (crates-io))

(define-public crate-win_fast_rm-1.0.0 (c (n "win_fast_rm") (v "1.0.0") (d (list (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Shell" "Win32_Storage_FileSystem" "Win32_System_Com"))) (d #t) (k 0)))) (h "0c80hhk7nz4wnmmficbjwwnkclhmqlxl2ipzj7807hz1d4ig21l2")))

(define-public crate-win_fast_rm-1.0.1 (c (n "win_fast_rm") (v "1.0.1") (d (list (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_UI_Shell" "Win32_Storage_FileSystem" "Win32_System_Com"))) (d #t) (k 0)))) (h "1jhnm7j6djvn31mjg5pj7ayiwwpzr43hrv4n943nzrxpaq222vas")))

