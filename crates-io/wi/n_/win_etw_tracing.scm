(define-module (crates-io wi n_ win_etw_tracing) #:use-module (crates-io))

(define-public crate-win_etw_tracing-0.1.0 (c (n "win_etw_tracing") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (f (quote ("log-tracer" "std"))) (o #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.7") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.7") (d #t) (k 0)))) (h "0zyi3x8xmb70qppaxh2vp75dwc5h9b7rflnq46b7z5ydkgbr342l") (f (quote (("default" "tracing-log"))))))

(define-public crate-win_etw_tracing-0.1.1 (c (n "win_etw_tracing") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-log") (r "^0.1") (f (quote ("log-tracer" "std"))) (o #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.7") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.9") (d #t) (k 0)))) (h "08g9ff6d7cshlv4nv6az9zk0avy5f39ax9v6ai2g81w08imsv08x") (f (quote (("default" "tracing-log"))))))

