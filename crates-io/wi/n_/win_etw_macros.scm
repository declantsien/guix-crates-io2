(define-module (crates-io wi n_ win_etw_macros) #:use-module (crates-io))

(define-public crate-win_etw_macros-0.1.0 (c (n "win_etw_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.0") (d #t) (k 0)))) (h "19spdazsbifizwzanfxmq6iilay77a1i1dn6va6g49cs1m0c0y2p")))

(define-public crate-win_etw_macros-0.1.1 (c (n "win_etw_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)))) (h "02w7xb08wj3mirdk52h8xy6r3dkpjxfq0pzi1b6shm6f4sbm9gwl")))

(define-public crate-win_etw_macros-0.1.2 (c (n "win_etw_macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)))) (h "0sv08piclkda4ihf9gl667gjcj7w9xg9ra8bihqfgczj94wwa8g1")))

(define-public crate-win_etw_macros-0.1.3 (c (n "win_etw_macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)))) (h "1lhsd53g1klvxrzih9r4wz21sidypl01ygzs69qvsgayk0qj29z5")))

(define-public crate-win_etw_macros-0.1.6 (c (n "win_etw_macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)))) (h "02rvvr5r90fqz8rlfk25iah9kay0hv89fqds255dd4qmfq07vrbq")))

(define-public crate-win_etw_macros-0.1.7 (c (n "win_etw_macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)))) (h "0vkva4fmn3aax9fy5qshmf2dwz5qi4p2kysry50v7h877qby3zhq")))

(define-public crate-win_etw_macros-0.1.8 (c (n "win_etw_macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v5"))) (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)))) (h "0kgmbqa8rj8pizr4986zvap1a2d5w33n0yjz8kimhj6v3rclrg7i")))

(define-public crate-win_etw_macros-0.1.9 (c (n "win_etw_macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3") (f (quote ("v5"))) (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)))) (h "03jb53kb5xq1f55354wa22vrrwjkvy12z1fw756b7f2vc4g0gi9f")))

