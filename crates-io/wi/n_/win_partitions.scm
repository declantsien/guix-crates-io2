(define-module (crates-io wi n_ win_partitions) #:use-module (crates-io))

(define-public crate-win_partitions-0.1.0 (c (n "win_partitions") (v "0.1.0") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "0ky2m2mgc5qwa820pkd12z0lvbxas3ggmydbgzi5wvl1vi9sak1n")))

(define-public crate-win_partitions-0.2.0 (c (n "win_partitions") (v "0.2.0") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "0sw59ji2maiz1jhb6q58hv9w0f9wx0nib1vlf3s4h79r4igym3p8")))

(define-public crate-win_partitions-0.3.0 (c (n "win_partitions") (v "0.3.0") (d (list (d (n "windows") (r "^0.18.0") (d #t) (k 0)) (d (n "windows") (r "^0.18.0") (d #t) (k 1)))) (h "1iyvs2fpn3b299l0gkbq7s7m3sa2w1kh0k9p2ib7rqyg3byqaydl")))

