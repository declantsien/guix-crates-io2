(define-module (crates-io wi n_ win_dialog) #:use-module (crates-io))

(define-public crate-win_dialog-0.1.0 (c (n "win_dialog") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1zwya4j892j81k0rvbksd68fd3f4agifhk44pph7j3ivvq8aj9l2")))

(define-public crate-win_dialog-0.1.1 (c (n "win_dialog") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0vhh6swqlm8dgaxchjli1qkfignyfwrxq7q8a7ppm4v6s3kklh40")))

(define-public crate-win_dialog-0.1.2 (c (n "win_dialog") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "09f7ma27hp56hc4gprw57pffzq7dcvh7x878q3cz1kvxalg5c9p5")))

