(define-module (crates-io wi n_ win_run) #:use-module (crates-io))

(define-public crate-win_run-0.1.0 (c (n "win_run") (v "0.1.0") (d (list (d (n "sysinfo") (r "^0.27") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.43") (f (quote ("Win32" "Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_RemoteDesktop"))) (d #t) (k 0)))) (h "0cyb0h26wswswf8nz9hjrv134c8yw28cxw05wchkvpxvih0v8sks")))

(define-public crate-win_run-0.1.1 (c (n "win_run") (v "0.1.1") (d (list (d (n "sysinfo") (r "^0.27") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32" "Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_RemoteDesktop"))) (d #t) (k 0)))) (h "1zaqknzlqldhinzavmfy327mv5mw9dxa1lyhxm04mzqm0apg2xfi")))

(define-public crate-win_run-0.2.0 (c (n "win_run") (v "0.2.0") (d (list (d (n "sysinfo") (r "^0.28") (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.46") (f (quote ("Win32" "Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_RemoteDesktop"))) (d #t) (k 0)))) (h "1r6s02dj3fawfh90dk2f4vfm8ig03lvck7fnglbcs5na5h13yg7q")))

(define-public crate-win_run-0.2.1 (c (n "win_run") (v "0.2.1") (d (list (d (n "sysinfo") (r "^0.29") (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32" "Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_RemoteDesktop"))) (d #t) (k 0)))) (h "0viyn3k7xz4dk8xkaykqml9mfq1ms7gdfrf3g3hj7dlc0xbfandh")))

