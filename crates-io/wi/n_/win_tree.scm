(define-module (crates-io wi n_ win_tree) #:use-module (crates-io))

(define-public crate-win_tree-0.1.1 (c (n "win_tree") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0hha5g2q73lsb2jrqxsr6p4l7ypvv66vz1ivw1fjkjp92b8qjiwz")))

(define-public crate-win_tree-0.1.2 (c (n "win_tree") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0jjsgh6mz6bkyzw8c9dfn235d2b3ln8247cyfdnm6q9xcp4p4bzz")))

