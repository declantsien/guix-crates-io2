(define-module (crates-io wi n_ win_etw_logger) #:use-module (crates-io))

(define-public crate-win_etw_logger-0.1.1 (c (n "win_etw_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "win_etw_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "0w54y8hzi3kw4ygiz9jkk6nh1yqnma1ikhcivb2rpkx5ad1i9n2v")))

(define-public crate-win_etw_logger-0.1.2 (c (n "win_etw_logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "win_etw_macros") (r "^0.1.2") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "1zp247yjvxkx96558ydmrzpr2rq244s2d931wfwi4pi0jz8h26xm")))

(define-public crate-win_etw_logger-0.1.6 (c (n "win_etw_logger") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "win_etw_macros") (r "^0.1.6") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.1") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.6") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "00bjh484drfy2n31f8d0is0krqzscpvf9wqdzlqybs3kam1fxy3z")))

(define-public crate-win_etw_logger-0.1.7 (c (n "win_etw_logger") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "win_etw_macros") (r "^0.1.7") (d #t) (k 0)) (d (n "win_etw_metadata") (r "^0.1.2") (d #t) (k 0)) (d (n "win_etw_provider") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (d #t) (k 0)))) (h "1x40vdcja2nrkz2w2wchwlqazv7i0pwgii75s7z65pg3681lbdph")))

