(define-module (crates-io wi n_ win_ocr) #:use-module (crates-io))

(define-public crate-win_ocr-0.1.0 (c (n "win_ocr") (v "0.1.0") (d (list (d (n "win_ocr_bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)))) (h "1w0xyyqpvx6a0a3xnqmhpmc1m2fh27bxhyb42bjv20fxd90l3ypd")))

(define-public crate-win_ocr-0.1.1 (c (n "win_ocr") (v "0.1.1") (d (list (d (n "win_ocr_bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)))) (h "0fgf83z4ivp8qs7zjh487drq8p1v1m2qngjrr0589zinrl1mr7cq")))

(define-public crate-win_ocr-0.1.2 (c (n "win_ocr") (v "0.1.2") (d (list (d (n "win_ocr_bindings") (r "^0.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)))) (h "050b5jagwawa9qk54gap0a2q7xy3i3x37p39dgkw3ibsrm4dg7x8")))

(define-public crate-win_ocr-0.1.3 (c (n "win_ocr") (v "0.1.3") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Foundation_Collections" "Globalization" "Graphics_Imaging" "Media_Ocr" "Storage_Streams"))) (d #t) (k 0)))) (h "0fvm08fr89nd1v5z11ymfgn3wwihc9rhx72cazi0q008mcwbmswv")))

