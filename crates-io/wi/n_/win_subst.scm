(define-module (crates-io wi n_ win_subst) #:use-module (crates-io))

(define-public crate-win_subst-0.0.1 (c (n "win_subst") (v "0.0.1") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "0g5pawng3jaakg837a9ngjkwn50hvkhy2kxmpdmd4383li24vp3h")))

(define-public crate-win_subst-0.0.2 (c (n "win_subst") (v "0.0.2") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "1vva2z56cys7jbi7iykhk6s6x864vdwg27almkawg3hwcz1pmf9l")))

(define-public crate-win_subst-0.0.3 (c (n "win_subst") (v "0.0.3") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "0hga94cc42lpaw048ygfwyp6m80zjq50yhqsdrwrgx6v9dxlh7sb")))

