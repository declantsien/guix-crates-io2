(define-module (crates-io wi n_ win_inet) #:use-module (crates-io))

(define-public crate-win_inet-0.1.0 (c (n "win_inet") (v "0.1.0") (d (list (d (n "widestring") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1bzb1h5p2vnm8piqc6pyl69d2brfm9vzn8jy78rh60c3hwq2gzcn") (f (quote (("unicode" "widestring"))))))

(define-public crate-win_inet-0.1.1 (c (n "win_inet") (v "0.1.1") (d (list (d (n "widestring") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0dnw6wcibzwiz5bxv0kvngwqqgg9pjjbv3q03piylhb8bdawj344") (f (quote (("unicode" "widestring"))))))

