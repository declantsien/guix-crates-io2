(define-module (crates-io wi n_ win_etw_metadata) #:use-module (crates-io))

(define-public crate-win_etw_metadata-0.1.0 (c (n "win_etw_metadata") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0cd399vv93y2i6n6pf6ggcs4vrggpzq00jli2k23bmq7zf22sbii") (f (quote (("metadata_headers") ("default" "metadata_headers"))))))

(define-public crate-win_etw_metadata-0.1.1 (c (n "win_etw_metadata") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "16fskqmwadm27j27iw6pihk0zr4whvz6w70db7jgxxcxy1ww10fc") (f (quote (("metadata_headers") ("default" "metadata_headers"))))))

(define-public crate-win_etw_metadata-0.1.2 (c (n "win_etw_metadata") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0bv5dlh2lff9570mxfzd5nbs5sw1akxv90fjxzn1jfh3cnk0y3g5") (f (quote (("metadata_headers") ("default" "metadata_headers"))))))

