(define-module (crates-io wi ll will_exit) #:use-module (crates-io))

(define-public crate-will_exit-0.1.0 (c (n "will_exit") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "0zrlzj73hsn5scxvb4h7x17zzffp98szy01hl69hkqiwirqdkfgw")))

(define-public crate-will_exit-0.1.1 (c (n "will_exit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "0xjfym5by18k5s283wbjr0s6kywwy41m41m8qc9sc6g3kqralpi3")))

(define-public crate-will_exit-0.1.2 (c (n "will_exit") (v "0.1.2") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "0hr15sglb7vy97pb8zhq36cjz3vhk42ydzbf71ssd2fa5lzqhq9i") (f (quote (("default") ("async" "event-listener"))))))

(define-public crate-will_exit-0.2.0 (c (n "will_exit") (v "0.2.0") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "064swadiz62pdy3dpzlbgyfyz30famgqpigwbgqkxc1cjazikgjr") (f (quote (("default") ("async" "event-listener"))))))

(define-public crate-will_exit-0.3.0 (c (n "will_exit") (v "0.3.0") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "1jxj7sslri73iq7l8p87kdzv9lvy19z9g0nndi8zq4va2273ciyl") (f (quote (("default") ("async" "event-listener"))))))

(define-public crate-will_exit-0.3.1 (c (n "will_exit") (v "0.3.1") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "0wz8s2bz0smyqakcsl48vk689ym5vnmq3j36hwzn591bdw6p7grc") (f (quote (("default") ("async" "event-listener"))))))

(define-public crate-will_exit-0.3.2 (c (n "will_exit") (v "0.3.2") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "0jilz3czyv6v07gq96va2r19nvjmqcvr9zaachqrxdf5kc6va4c1") (f (quote (("default") ("async" "event-listener"))))))

(define-public crate-will_exit-0.3.3 (c (n "will_exit") (v "0.3.3") (d (list (d (n "event-listener") (r "^2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1") (d #t) (k 0)))) (h "12zks5dsa87q4ka2x6kar762fhifc11qdmmnm46zzwagbpwgrj6b") (f (quote (("default") ("async" "event-listener"))))))

