(define-module (crates-io wi ll wills-joke-cli) #:use-module (crates-io))

(define-public crate-wills-joke-cli-0.1.0 (c (n "wills-joke-cli") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.121") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.120") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1pyapmljmyc71grbdlw0ims9ccrf44z2pi2ffqrv6ngg8jpqpq18")))

