(define-module (crates-io wi ll willow-example) #:use-module (crates-io))

(define-public crate-willow-example-0.1.1 (c (n "willow-example") (v "0.1.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (d #t) (k 0)) (d (n "field-offset") (r "^0.3.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.27.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)) (d (n "wasm-logger") (r "^0.2.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.51") (f (quote ("Window" "Document"))) (d #t) (k 0)) (d (n "willow") (r "^0.0.3") (d #t) (k 0)))) (h "1ac9bj9yqvqsr4mkyw54zzk825rb9521rqk1sspsx4cnlyvpl0c6")))

