(define-module (crates-io wi ll willow-codegen) #:use-module (crates-io))

(define-public crate-willow-codegen-0.0.1 (c (n "willow-codegen") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1j92mz57mw6gz7ppkv8k7nv957sfp7gpafbblrbwyxhlmcpmzs86")))

(define-public crate-willow-codegen-0.0.2 (c (n "willow-codegen") (v "0.0.2") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxx9gnxs6y0nr91nhfw9m0r72fnz78gj7wpajzf4jhap1q6xnhi")))

(define-public crate-willow-codegen-0.0.3 (c (n "willow-codegen") (v "0.0.3") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0kw0yg6a6185k4nwapglblj0f53ykmn6d3v26jaxy896816zhdfm")))

