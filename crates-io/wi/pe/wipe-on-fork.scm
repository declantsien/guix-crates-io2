(define-module (crates-io wi pe wipe-on-fork) #:use-module (crates-io))

(define-public crate-wipe-on-fork-0.1.0 (c (n "wipe-on-fork") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 2)))) (h "1pr8hmafx752s9qlfyqnyy12xmlbinlcxknyxcqjcx1vb15i5lsk")))

(define-public crate-wipe-on-fork-0.2.0 (c (n "wipe-on-fork") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "0hv72a4xb6gahkzvf69d225i00cdkc529a4hkflp8d2ik8jfd02f")))

(define-public crate-wipe-on-fork-0.2.1 (c (n "wipe-on-fork") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "19pchn7n3z6i73nwhl8r6n64kgj4681j6ppclk5zpq3phv2l22b2")))

(define-public crate-wipe-on-fork-0.2.2 (c (n "wipe-on-fork") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "15f2ycf8h22lp7p308dzrj08w2yy56gk6y42vgq58jvpj81r8mzf")))

(define-public crate-wipe-on-fork-0.2.3 (c (n "wipe-on-fork") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.152") (d #t) (k 0)))) (h "1b5y6i7p7l6gq9pmchm1bs6wqh9fvkdbgv9110fs7z4lrmpnp8xv")))

