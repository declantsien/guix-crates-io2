(define-module (crates-io wi pe wipe_buddy) #:use-module (crates-io))

(define-public crate-wipe_buddy-0.1.0 (c (n "wipe_buddy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0y0qfbri77h8dfyx5wz5n8mf2hhyrn5myhr7yinqv4w9qvmnvw3z")))

(define-public crate-wipe_buddy-0.2.0 (c (n "wipe_buddy") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1mgwymasbbfc30bzyj9nq4ihjp1p7q1mv915rbaf4rz8nllrjsnk")))

