(define-module (crates-io wi nl winlog) #:use-module (crates-io))

(define-public crate-winlog-0.1.0 (c (n "winlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "1hk4lq7q0zqd8gdlwsqizwvynfhpzvxfjx3m527alswip12y2pkq")))

(define-public crate-winlog-0.1.1 (c (n "winlog") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "11fayf2svn6cq6zbw0l3mkz516y70vyfy022hwd94kqsdpj5fak4")))

(define-public crate-winlog-0.2.0 (c (n "winlog") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0d3h5h8whqd4364q90nrb0f0lmyqc6ra69j4hqpgqm7mzh1dihhd")))

(define-public crate-winlog-0.2.1 (c (n "winlog") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "15y6wkqis05dcmx60zxvcnlds4cs808mpg938x34c6f8xscsgxsx")))

(define-public crate-winlog-0.2.2 (c (n "winlog") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0qihms0rakrvdzng5ypckw85qv5fg6zbdqqwc0f4gn0jx793wcqk")))

(define-public crate-winlog-0.2.3 (c (n "winlog") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0digyqdpsbz2v35zqma5y5hp6n59xj2vfbyrqsca3xzy33ahviwq")))

(define-public crate-winlog-0.2.4 (c (n "winlog") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "sha2") (r "^0.8.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "0gvh2fya829mawzklgmlcbn2f8pqny1j8xpnisxq1xz0k5hxwb7v")))

(define-public crate-winlog-0.2.5 (c (n "winlog") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.6") (o #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "sha2") (r "^0.8.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "08b0w7a900gr3qyym1a2sykplcqhgv344wa7i64zs6s5h02wp7fh") (f (quote (("default"))))))

(define-public crate-winlog-0.2.6 (c (n "winlog") (v "0.2.6") (d (list (d (n "env_logger") (r "^0.6") (o #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex") (r "^1.0") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "sha2") (r "^0.8.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "winreg") (r "^0.5") (d #t) (k 0)))) (h "06grap2zjzi4vw4s6k7347ch4p4z5ifwvx9x3kpgxy2iq58f2ldn") (f (quote (("default"))))))

