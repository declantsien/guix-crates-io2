(define-module (crates-io wi nl winlock) #:use-module (crates-io))

(define-public crate-winlock-0.1.0 (c (n "winlock") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "embed-manifest") (r "^1.3.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging" "Win32_UI_Input_KeyboardAndMouse" "Win32_System_Shutdown" "Win32_System_Registry"))) (d #t) (k 0)))) (h "1gvv1abqw7v7lcixbagd9vcrfvv8gszb873vyza5qn3kbkm5pmx9")))

