(define-module (crates-io wi tx witx-lucet-bindings-gen) #:use-module (crates-io))

(define-public crate-witx-lucet-bindings-gen-0.1.0 (c (n "witx-lucet-bindings-gen") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "witx") (r "^0.8") (d #t) (k 0)))) (h "0r7h0kbzh4ng7njndliiz7hj3y6zivns4dslp732kmlxp50gzynq")))

