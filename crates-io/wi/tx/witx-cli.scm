(define-module (crates-io wi tx witx-cli) #:use-module (crates-io))

(define-public crate-witx-cli-0.9.1 (c (n "witx-cli") (v "0.9.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "11a9h0dgy2n7qfl0q77yc99d7721sqy4r75kasajbkyx0hskghlf")))

