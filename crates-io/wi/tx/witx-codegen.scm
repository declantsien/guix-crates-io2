(define-module (crates-io wi tx witx-codegen) #:use-module (crates-io))

(define-public crate-witx-codegen-0.2.0 (c (n "witx-codegen") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "113d8nvcqkj00w4fy6v61pigv6bgmc45gl01a7kqjmqbscp5wf15")))

(define-public crate-witx-codegen-0.2.1 (c (n "witx-codegen") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "0h2cdvsglf4k3js1jchpa6wn17ygs0h8kk9mny1jnq9l7zmik84c")))

(define-public crate-witx-codegen-0.3.0 (c (n "witx-codegen") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "1h7qwrl5n8bbay1g2drrncnjbvxgpkmwihib497y3mml1m3cjvp0")))

(define-public crate-witx-codegen-0.3.1 (c (n "witx-codegen") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "03qmjz1mby8a9nqk2kky1r66xvngqvydcrj078xhvmwy8n5qfnrx")))

(define-public crate-witx-codegen-0.3.2 (c (n "witx-codegen") (v "0.3.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "05nwg4vc16416k2p3nr3vkz03kg562qcx532zy49ksyaz5idl138")))

(define-public crate-witx-codegen-0.3.3 (c (n "witx-codegen") (v "0.3.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "13pfk2zp7x8lkflgl8w4dllvbinbjvr7nca7irv55ic029jiyv1i")))

(define-public crate-witx-codegen-0.3.4 (c (n "witx-codegen") (v "0.3.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "12va50150ssc0wqp1724pwzd5kpqd3r21m3fqx8a0ywk46v3vhsg")))

(define-public crate-witx-codegen-0.3.5 (c (n "witx-codegen") (v "0.3.5") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "14qqp3spqyph62n58b3wj3siqs3dryjl9hdh1h02507wqlq1kdsz")))

(define-public crate-witx-codegen-0.3.6 (c (n "witx-codegen") (v "0.3.6") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "120wrnxwj40c7zzcr0cjkfwgsa6x0f24wzcvs266rfc103zw1h5h")))

(define-public crate-witx-codegen-0.3.7 (c (n "witx-codegen") (v "0.3.7") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "0k6alx6y65k7hag9paakmcs09wgxn6i8miflf8ygkf5jalvxb43l")))

(define-public crate-witx-codegen-0.3.8 (c (n "witx-codegen") (v "0.3.8") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "1qqjcillvjf5ryb2ccmh2kxc2xsws4ydckw8yag8fckr3bcsqr2q")))

(define-public crate-witx-codegen-0.10.0 (c (n "witx-codegen") (v "0.10.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "1ii27adjdf3y3169bj9rfbv0p9d8gncxl69gn42c0w8paqa9d17s")))

(define-public crate-witx-codegen-0.10.1 (c (n "witx-codegen") (v "0.10.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "10pfvxhf4idh1h3z3dq2j4msvvah6b25l5jb5qgpv6sbsr50g6m4")))

(define-public crate-witx-codegen-0.10.2 (c (n "witx-codegen") (v "0.10.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "19m18iczj17b039knwlza9f0z0qbj5akaphjpw9qb2m8xfww7slp")))

(define-public crate-witx-codegen-0.10.3 (c (n "witx-codegen") (v "0.10.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "1vavbscq8sd29mgb3d4gpwj7yp5fmdnbfiddpb3wkgh8qa334fxj")))

(define-public crate-witx-codegen-0.10.4 (c (n "witx-codegen") (v "0.10.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta1") (d #t) (k 0) (p "witnext")))) (h "1ry3nq3pr55f5wgafscgh6wzr8ffiv4hnc5gqj53lkza0fcnyb4h")))

(define-public crate-witx-codegen-0.11.0 (c (n "witx-codegen") (v "0.11.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta2") (d #t) (k 0) (p "witnext")))) (h "0zyvwz0qlp39dqxisfjgkfqlzk90canxqmk4ykc8pzp1690qnpri")))

(define-public crate-witx-codegen-0.10.5 (c (n "witx-codegen") (v "0.10.5") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta2") (d #t) (k 0) (p "witnext")))) (h "068mqn7w9wz5v59ayc5ki2dg5a5zdybab6mncgxbmckkmvnnn26z")))

(define-public crate-witx-codegen-0.10.6 (c (n "witx-codegen") (v "0.10.6") (d (list (d (n "clap") (r "^2.33") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta3") (d #t) (k 0) (p "witnext")))) (h "071nk0w1hwzq0jma07ad7xfsa9r5p518r9dyybi8b7xv9426vqlz")))

(define-public crate-witx-codegen-0.10.7 (c (n "witx-codegen") (v "0.10.7") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "strum") (r "^0.22.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22.0") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta3") (d #t) (k 0) (p "witnext")))) (h "0yi1xzqwk7wg41p63q2a1qngf6r95y3mknbm0glsbxgb8b6il637")))

(define-public crate-witx-codegen-0.11.1 (c (n "witx-codegen") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta3") (d #t) (k 0) (p "witnext")))) (h "1z9a13dhj3syhly95fr5lsviv43xjkw02qn1llx3j9lyhc5bq0x0")))

(define-public crate-witx-codegen-0.11.2 (c (n "witx-codegen") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta3") (d #t) (k 0) (p "witnext")))) (h "1h0ywcxfsi99xwj1h4xq6yik1i2lpjfsxd849bj7vfb10pxbhi3x")))

(define-public crate-witx-codegen-0.11.3 (c (n "witx-codegen") (v "0.11.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "witx") (r "^0.10.0-beta3") (d #t) (k 0) (p "witnext")))) (h "0a9ql66gp3p7p8kx6a7isc8l9938vgbldzpll9k5nskqzjk7c5p5")))

