(define-module (crates-io wi tx witx-indent) #:use-module (crates-io))

(define-public crate-witx-indent-0.1.0 (c (n "witx-indent") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("wrap_help"))) (k 0)))) (h "065q87nqs3w3wl1iwlmkln2n415rgg04zbm86pikc55467ikbj2v")))

(define-public crate-witx-indent-0.1.1 (c (n "witx-indent") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("wrap_help"))) (k 0)))) (h "0d2k3bkja4n8iyymibl8pyq96svgqk49930pj18dj9zq75c64khl")))

(define-public crate-witx-indent-0.1.2 (c (n "witx-indent") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("wrap_help"))) (k 0)))) (h "1brmg9bivnz2kdi4nl086y38rbml9irlz5szbb84cd9l1gwfzj8d")))

(define-public crate-witx-indent-0.1.3 (c (n "witx-indent") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("std" "cargo" "wrap_help"))) (k 0)))) (h "0ygimx6gl1ycc5q8s4shwds3sm3v0p83hdq5jdfdj3mxn3yrcgx3")))

