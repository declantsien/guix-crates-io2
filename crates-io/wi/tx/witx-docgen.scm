(define-module (crates-io wi tx witx-docgen) #:use-module (crates-io))

(define-public crate-witx-docgen-0.1.0 (c (n "witx-docgen") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0w18glnk9ic0x447gilmidxkgjqnagsy01j0x9kxy4ibz937f28s")))

(define-public crate-witx-docgen-0.1.1 (c (n "witx-docgen") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "witx") (r "^0.8.6") (d #t) (k 0)))) (h "162n6hqqm0qa6ni6x9rvnclwvkv2z0kbax7vzjs54jh5w718rk3z")))

(define-public crate-witx-docgen-0.1.3 (c (n "witx-docgen") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "0l8jm1q1na8x2vql8wrrnl81zbx2vd7p7nshp19sjav8cqf5hb3h")))

(define-public crate-witx-docgen-0.2.0 (c (n "witx-docgen") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "1mfq51797xnzbfhh673a9ch85ah224wgxv8lpyfsv7szlg80db97")))

