(define-module (crates-io wi tx witx-generate-raw) #:use-module (crates-io))

(define-public crate-witx-generate-raw-0.1.0 (c (n "witx-generate-raw") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "1924x4cay5l162axk1z9gv5g3w7j4wg6zshv7i96lpi489x6l24f") (f (quote (("default"))))))

(define-public crate-witx-generate-raw-0.1.1 (c (n "witx-generate-raw") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "05n66lr6rwf276pr6zxy2wspk3zy96xax9ildy8c8cxax4h76i7s") (f (quote (("default"))))))

