(define-module (crates-io wi tx witx-bindgen) #:use-module (crates-io))

(define-public crate-witx-bindgen-0.1.0 (c (n "witx-bindgen") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "09569sy89w9y5rxyhhrp014q7cci84vdbgfckaqx2kzj824dhzyl") (f (quote (("multi-module") ("default"))))))

