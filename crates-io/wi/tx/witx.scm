(define-module (crates-io wi tx witx) #:use-module (crates-io))

(define-public crate-witx-0.1.0 (c (n "witx") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1hhh7l72zkvy6sngrs0knhkmpfdypwx0nnwm5598xqfvwqhwck30")))

(define-public crate-witx-0.2.0 (c (n "witx") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "120c6nq9w9fxcai95qv113shfjfsbh5f95565qv7xwlimx3w7ww4")))

(define-public crate-witx-0.3.0 (c (n "witx") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0v5yyxydcvybsysk74pad0pvpgji3cmdzb7mgdn22rw1mxiqa8y2")))

(define-public crate-witx-0.3.1 (c (n "witx") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "wast") (r "^3.0.1") (d #t) (k 0)))) (h "1lmnqxwdmqjdqnb0i3iq3x69jfrgrlf2322swz1gm3aqpcnnp4sg")))

(define-public crate-witx-0.4.0 (c (n "witx") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "wast") (r "^3.0.1") (d #t) (k 0)))) (h "0gca0qgnzh8cyi5s4kija990g3wm6klw7yajqi6dffrfsy9y523z")))

(define-public crate-witx-0.5.0 (c (n "witx") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "wast") (r "^3.0.1") (d #t) (k 0)))) (h "0q95247p2mxgn6fp8v1nkm7l3mn2k0bc2brhq0ridrgbmx2561vd")))

(define-public crate-witx-0.6.0 (c (n "witx") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 0)))) (h "0zcnnbbnk1q369f62807p4swhkv9qkhw7sqnd3h416dwgm9k5i5b")))

(define-public crate-witx-0.7.0 (c (n "witx") (v "0.7.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 0)))) (h "0ca9awp3vvx7899z95axg5vfvalykdyqqyv3snw3jkxr1jcjbvm6")))

(define-public crate-witx-0.8.0 (c (n "witx") (v "0.8.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 0)))) (h "1k23f52vsl6hw422xa3gh6gq3qsbz9767d26qr62w7v7b8v7ajdh")))

(define-public crate-witx-0.8.1 (c (n "witx") (v "0.8.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 0)))) (h "1kh540rg1idrbvvxml6wmrqs10hcbngd33j5ybfiaiv926ryi1d1")))

(define-public crate-witx-0.8.2 (c (n "witx") (v "0.8.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^8.0.0") (d #t) (k 0)))) (h "05skn3nn5d7y2an2814n10r1apklfmajc098mn2fbpr5drry8ci1")))

(define-public crate-witx-0.8.3 (c (n "witx") (v "0.8.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^9.0.0") (k 0)))) (h "15gd6lz37alwyffw20417a9n4r00cdx8pzxlwrj8mvb2cgjgcc4g")))

(define-public crate-witx-0.8.4 (c (n "witx") (v "0.8.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^9.0.0") (k 0)))) (h "1rxmncgypl1nnwpqh74d32a9136mw9rf656w6fx9jp4h6mhf22lp")))

(define-public crate-witx-0.8.5 (c (n "witx") (v "0.8.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^11.0.0") (k 0)))) (h "08cbv7fdf1acvgxia0v030hgc20bc38db1qdnv22f1fvj5lp1blw")))

(define-public crate-witx-0.8.6 (c (n "witx") (v "0.8.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^20.0.0") (k 0)))) (h "1nmr4cab6wcjnj2c1gikd1nrq15y6vs1mraxzi7clp5wjiw3v8w3")))

(define-public crate-witx-0.8.7 (c (n "witx") (v "0.8.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^22.0.0") (k 0)))) (h "06rys8jd0a78w7bwah02cih8cz8c6ch8j576qci3xcpjnghiy1z9")))

(define-public crate-witx-0.8.8 (c (n "witx") (v "0.8.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^22.0.0") (k 0)))) (h "1r3mllyk0fd2yhm9lfzs7ggd0qkzdpcnc423z7mxqzcvxlrj9q0v")))

(define-public crate-witx-0.9.0 (c (n "witx") (v "0.9.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^33.0.0") (k 0)))) (h "13iq0fghkfd1vyhv00lnwmnq5kxskan6iixgc83mx3dk7ph5hjnz")))

(define-public crate-witx-0.9.1 (c (n "witx") (v "0.9.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wast") (r "^35.0.2") (k 0)))) (h "0jzgmayh2jjbv70jzfka38g4bk4g1fj9d0m70qkxpkdbbixg4rp3")))

