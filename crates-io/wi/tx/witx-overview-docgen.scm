(define-module (crates-io wi tx witx-overview-docgen) #:use-module (crates-io))

(define-public crate-witx-overview-docgen-0.1.2 (c (n "witx-overview-docgen") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("wrap_help"))) (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0amr9w7l9b33v1amj5ccyydylhpxsbs4lxlp2nm5xlgp759ry5z0")))

