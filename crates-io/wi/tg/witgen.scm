(define-module (crates-io wi tg witgen) #:use-module (crates-io))

(define-public crate-witgen-0.1.0 (c (n "witgen") (v "0.1.0") (d (list (d (n "witgen_macro") (r "^0.1") (d #t) (k 0)))) (h "0v57avg0drfafp9yp098zdx3vw2lrylxh2rj4spgs300krrz44c2")))

(define-public crate-witgen-0.2.0 (c (n "witgen") (v "0.2.0") (d (list (d (n "witgen_macro") (r "^0.2") (d #t) (k 0)))) (h "0f3cavv0mdj6cyy6z1g63avb9z9ka3kdzpwhga04mj2hvh7b4vy2")))

(define-public crate-witgen-0.3.0 (c (n "witgen") (v "0.3.0") (d (list (d (n "witgen_macro") (r "^0.3") (d #t) (k 0)))) (h "07kisg4aiz7y7a8jlkjcbiwipgan9r1c7vz6l54lpiajnqinghdi")))

(define-public crate-witgen-0.4.0 (c (n "witgen") (v "0.4.0") (d (list (d (n "witgen_macro") (r "^0.4") (d #t) (k 0)))) (h "1fk2nk42qqpc99lc1cdjnn1apmlzn9w27sv4gsrqqv7dsar2k9j7")))

(define-public crate-witgen-0.5.0 (c (n "witgen") (v "0.5.0") (d (list (d (n "witgen_macro") (r "^0.5") (d #t) (k 0)))) (h "0xdy0n9yhyssx44x9ib0r3yvw8m2nrfv9k9r927pglr3z2bld3mv")))

(define-public crate-witgen-0.6.0 (c (n "witgen") (v "0.6.0") (d (list (d (n "witgen_macro") (r "^0.6") (d #t) (k 0)))) (h "0s90binl123vkbz9rg7ihdnp4wm8zj96d1m6rr2xwg3rag39d55j")))

(define-public crate-witgen-0.7.0 (c (n "witgen") (v "0.7.0") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "witgen_macro") (r "^0.7") (d #t) (k 0)))) (h "0n7k59b6av0bl38ci93xb0xka1yla71rxa2fxkp1wx15clh1haqg")))

(define-public crate-witgen-0.8.0 (c (n "witgen") (v "0.8.0") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "witgen_macro") (r "^0.8") (d #t) (k 0)))) (h "1yjff9v4f552bjsiaxfnwq0jrikxa78bh0pi87xxxwipl1iahvpv")))

(define-public crate-witgen-0.9.0 (c (n "witgen") (v "0.9.0") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "witgen_macro") (r "^0.9") (d #t) (k 0)))) (h "0b3j55jy8a9k2lg4lsfsgv4hdhfmkb9iqcw370vr55gh93sc6rnc")))

(define-public crate-witgen-0.10.0 (c (n "witgen") (v "0.10.0") (d (list (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "witgen_macro") (r "^0.10") (d #t) (k 0)))) (h "1w4bj02zz0p74whh810bangm4ypp9mgal20c856xwnqya5wczlm9")))

(define-public crate-witgen-0.11.0 (c (n "witgen") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.54") (d #t) (k 2)) (d (n "witgen_macro") (r "^0.11") (d #t) (k 0)))) (h "1rh2d8ad48kvmkicv2ywlvg343mj7k3irrrk70q10qzwdivxyrz9")))

(define-public crate-witgen-0.12.0 (c (n "witgen") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "cargo-witgen") (r "^0.12") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "witgen_macro") (r "^0.12") (d #t) (k 0)) (d (n "witgen_macro_helper") (r "^0.12") (d #t) (k 2)))) (h "0080y0jar839hrwwfsmsy9if1kpz7mfddmxzcxpnd5bksz0s4iap")))

(define-public crate-witgen-0.14.0 (c (n "witgen") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "cargo-witgen") (r "^0.13") (d #t) (k 2)) (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 2) (p "aha-wit-parser")) (d (n "witgen_macro") (r "^0.14") (d #t) (k 0)) (d (n "witgen_macro_helper") (r "^0.13") (d #t) (k 2)))) (h "15w4xs21wyqj9phqrnd213zbdipib4i69lyrak0cjdjvgs0sd8l1")))

(define-public crate-witgen-0.15.0 (c (n "witgen") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 2)) (d (n "cargo-witgen") (r "^0.15") (d #t) (k 2)) (d (n "k9") (r "^0.11.5") (d #t) (k 2)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 2)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 2) (p "aha-wit-parser")) (d (n "witgen_macro") (r "^0.15") (d #t) (k 0)) (d (n "witgen_macro_helper") (r "^0.15") (d #t) (k 2)))) (h "1yss8d1rbh4xnqs4n111y35sakww6mcgfizqd9y7nkjv6hfcx9pw")))

