(define-module (crates-io wi cr wicrs_common) #:use-module (crates-io))

(define-public crate-wicrs_common-0.1.0 (c (n "wicrs_common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0h5ibvfmbg1fsa3hmjgvg3qx0is8vc4774gvvik4k4vadf7lwccg")))

(define-public crate-wicrs_common-0.1.1 (c (n "wicrs_common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1zq7irhakyaff1wbnpldyjjcs3lskw9yb35vv36r1nzj1qyinlcs")))

(define-public crate-wicrs_common-0.1.2 (c (n "wicrs_common") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "sha2") (r "^0.9.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "14qi9l5ii5aab0dyz1gy13vh2ip4amx8g9636cxdvqj2k5jdkxyv")))

(define-public crate-wicrs_common-0.2.0 (c (n "wicrs_common") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0clphll4884flybanalis2rcsdrchm0ladjf924wdlqhp9qdpvri")))

(define-public crate-wicrs_common-0.2.1 (c (n "wicrs_common") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "073jq9jcqhcrf9sqir9zi2zi6p4zp683dj6rsjzcmv65vz121grf")))

