(define-module (crates-io wi cr wicrs_api) #:use-module (crates-io))

(define-public crate-wicrs_api-0.1.0 (c (n "wicrs_api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.3") (d #t) (k 2)) (d (n "wicrs_common") (r "^0.1") (d #t) (k 0)))) (h "1d8v4xjrshi72qqy85bl5mw5d31sjijniwga6xbnlqg9xji0vxhb")))

(define-public crate-wicrs_api-0.2.0 (c (n "wicrs_api") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "wicrs_server") (r "^0.8") (d #t) (k 0)))) (h "0j83dd6ppgwrkhci91gh9mzjc463r7g89gapkq5w3admxrk5dq45")))

(define-public crate-wicrs_api-0.2.1 (c (n "wicrs_api") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "wicrs_server") (r "^0.8") (d #t) (k 0)))) (h "0sz064wjb7vyam6j62k78f8ww6vr8xfwcw9spdsl7ghzn1193hxk")))

