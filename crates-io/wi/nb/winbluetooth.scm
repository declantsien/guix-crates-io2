(define-module (crates-io wi nb winbluetooth) #:use-module (crates-io))

(define-public crate-winbluetooth-0.1.0 (c (n "winbluetooth") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("impl-default" "guiddef" "handleapi" "processthreadsapi" "winbase" "winioctl" "winerror" "winnt" "winsock2" "ws2def"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1vskayad3y8qxmxjcw9lw5sik1ckyixcqvjs33nym4g1vpmbvsz7") (f (quote (("impl-default") ("impl-debug") ("debug" "impl-debug"))))))

