(define-module (crates-io wi iu wiiu_swizzle) #:use-module (crates-io))

(define-public crate-wiiu_swizzle-0.1.0 (c (n "wiiu_swizzle") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m3p0g9s9cynnvkl19x3w64za1709h761lipn56fblr0j85kvhc0")))

(define-public crate-wiiu_swizzle-0.2.0 (c (n "wiiu_swizzle") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "085jdj7mbk2y0c7dh1fn1jgpplgd9q3bpq9m6rjm46z1n184av7m")))

