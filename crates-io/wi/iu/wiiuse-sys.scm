(define-module (crates-io wi iu wiiuse-sys) #:use-module (crates-io))

(define-public crate-wiiuse-sys-0.1.0 (c (n "wiiuse-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "02b4f68gfxvpm46viwiybw4062hhk3qdd5c65q79pgh5adinfr1q")))

(define-public crate-wiiuse-sys-0.2.0 (c (n "wiiuse-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "0wdssnl4kc1c240qkph85gzi27lma8l66028b6p944dmdf4i2k5q")))

(define-public crate-wiiuse-sys-0.3.0 (c (n "wiiuse-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.33.1") (d #t) (k 1)))) (h "1xa6s56x945j9zncj6gyj5z4mw91yg0aagihfmlfbqjl00l69880")))

