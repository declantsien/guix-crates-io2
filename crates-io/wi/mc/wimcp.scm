(define-module (crates-io wi mc wimcp) #:use-module (crates-io))

(define-public crate-wimcp-0.0.0 (c (n "wimcp") (v "0.0.0") (d (list (d (n "aul") (r "^1.3.1") (d #t) (k 0)) (d (n "wbsl") (r "^0.0.2") (d #t) (k 0)) (d (n "whdp") (r "^1.1.9") (d #t) (k 0)) (d (n "wjp") (r "^1.0.2") (d #t) (k 0)))) (h "0l0ykkx8a04s63ksai3bwm5ffq6xdcq3rxx7vwzab0lnsv3awym0")))

(define-public crate-wimcp-0.0.1 (c (n "wimcp") (v "0.0.1") (d (list (d (n "wbdl") (r "^1.2.0") (d #t) (k 0)) (d (n "wimcm") (r "^0.2.6") (d #t) (k 0)) (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "0bfn61iar55z4da71jdhppb7qvci3hz3irrz374kcx87mjiqygnd")))

(define-public crate-wimcp-0.1.0 (c (n "wimcp") (v "0.1.0") (d (list (d (n "wbdl") (r "^1.2.0") (d #t) (k 0)) (d (n "wimcm") (r "^0.2.6") (d #t) (k 0)) (d (n "wjp") (r "^1.1.3") (d #t) (k 0)))) (h "0x3q5alq2bg3kivxn27sandxwllmffyawfylfhzk9nxw2ixq7hj6")))

