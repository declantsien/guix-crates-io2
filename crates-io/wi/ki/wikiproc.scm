(define-module (crates-io wi ki wikiproc) #:use-module (crates-io))

(define-public crate-wikiproc-0.0.1 (c (n "wikiproc") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0b2c1nrs5p69xq6d7z73djxk1g4yd1ipx2xvw5kzz7yzclmrk98d")))

