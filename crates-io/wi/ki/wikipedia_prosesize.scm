(define-module (crates-io wi ki wikipedia_prosesize) #:use-module (crates-io))

(define-public crate-wikipedia_prosesize-0.1.0-alpha.1 (c (n "wikipedia_prosesize") (v "0.1.0-alpha.1") (d (list (d (n "parsoid") (r "^0.8.0-alpha.3") (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17cbg7ik6glxmdhdnalbfki6gvr29vwf2k7r55nivv60j5j9i1g5") (r "1.60")))

(define-public crate-wikipedia_prosesize-0.1.0-alpha.2 (c (n "wikipedia_prosesize") (v "0.1.0-alpha.2") (d (list (d (n "parsoid") (r "^0.8.0-alpha.3") (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1qz3rqggbxp9h39k6lr6sfv603nlni85i53jdf9jbw82k93134r3") (r "1.60")))

(define-public crate-wikipedia_prosesize-0.1.0-alpha.3 (c (n "wikipedia_prosesize") (v "0.1.0-alpha.3") (d (list (d (n "parsoid") (r "^0.8.0-alpha.4") (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0phpih4cq243sqv8ficmwrsyzhpkrp3qxsld714xikcykvn3vblw") (r "1.60")))

(define-public crate-wikipedia_prosesize-0.1.0 (c (n "wikipedia_prosesize") (v "0.1.0") (d (list (d (n "parsoid") (r "^0.8.0") (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "06nna99w2kyylx9j3221h7d1z4b28rkk6syk9qs0h7ai4l4g5qxx") (r "1.63")))

(define-public crate-wikipedia_prosesize-0.2.0 (c (n "wikipedia_prosesize") (v "0.2.0") (d (list (d (n "parsoid") (r "^0.9.0") (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pxsrws72gvkql9pbg10i9628s0zkqwwfc0g7j4m9hvf2hnsqrr4") (f (quote (("serde-1" "serde")))) (r "1.67")))

