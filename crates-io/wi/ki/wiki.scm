(define-module (crates-io wi ki wiki) #:use-module (crates-io))

(define-public crate-wiki-0.1.0 (c (n "wiki") (v "0.1.0") (h "1wrd5zj9dhfrvxgs6qp9ir20nlsmppw1dk7fi6kdjj0ck3cz30rc") (y #t)))

(define-public crate-wiki-0.0.1 (c (n "wiki") (v "0.0.1") (h "0y07lfbg9qamfk5k2wbbnawxxgyg9q21rwhxmsr7i8822r9v3aq5") (y #t)))

(define-public crate-wiki-0.0.2 (c (n "wiki") (v "0.0.2") (h "0inrrjckbblk7f7pixrnbvghl27qfdh48jfdhcz0hfbypqwbjxhg") (y #t)))

(define-public crate-wiki-0.0.3 (c (n "wiki") (v "0.0.3") (d (list (d (n "async-sse") (r "^5.1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (f (quote ("io"))) (d #t) (k 0)) (d (n "http-types") (r "^2.12.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json" "multipart" "gzip" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)) (d (n "wikiproc") (r "^0.0.1") (d #t) (k 0)))) (h "19i7b82qng7spjj353hcka1v3kljyxx9m5xdwc533jvf7qx8q736") (f (quote (("default" "reqwest/cookies" "tokio"))))))

