(define-module (crates-io wi ki wiki-o) #:use-module (crates-io))

(define-public crate-wiki-o-0.1.0 (c (n "wiki-o") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "02f8szyagi9l1m1avq14b6vf8q19ysqls3ylflnaxlagi04y7vdj")))

