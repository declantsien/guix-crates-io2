(define-module (crates-io wi ki wikitrans) #:use-module (crates-io))

(define-public crate-wikitrans-0.1.0 (c (n "wikitrans") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "skim") (r "^0.5") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.2") (d #t) (k 0)))) (h "090195iwjizzvvak1fpasszjnq0g0iixkibfn8qpmr4ygy39cqd3")))

(define-public crate-wikitrans-0.1.1 (c (n "wikitrans") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "skim") (r "^0.5") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.2") (d #t) (k 0)))) (h "076a45ksiynkrb1nmrbbkn25wb8vx8wcscj101sdcp5mcjmdskc1")))

(define-public crate-wikitrans-0.1.2 (c (n "wikitrans") (v "0.1.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "skim") (r "^0.5") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.3") (d #t) (k 0)))) (h "1wc7hkfacj15ghpxjqsv8kr0m9nrcxxidsbkddlzd8zbdysabh3c")))

