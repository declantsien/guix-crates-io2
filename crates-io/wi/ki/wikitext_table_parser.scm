(define-module (crates-io wi ki wikitext_table_parser) #:use-module (crates-io))

(define-public crate-wikitext_table_parser-0.1.0 (c (n "wikitext_table_parser") (v "0.1.0") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)))) (h "1wpn2ilfgmqqd35p9gm4vkzhzc5nksf4fyvi319yl2b5jnhqizq7")))

(define-public crate-wikitext_table_parser-0.1.1 (c (n "wikitext_table_parser") (v "0.1.1") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)))) (h "1aakxik33z9n3j9l8xf4v6l286qi2afs5si3zm95g6ck2blgb7km")))

(define-public crate-wikitext_table_parser-0.2.0 (c (n "wikitext_table_parser") (v "0.2.0") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0xs35cfbmk478yzkk71gjibp5s2gj63zrc0q3j1d446i5lyzhzy0")))

(define-public crate-wikitext_table_parser-0.2.1 (c (n "wikitext_table_parser") (v "0.2.1") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "0j7krab8qrsykzmb80ln9szazqzismnb1sj19gc2knx09l44yg7w")))

(define-public crate-wikitext_table_parser-0.2.2 (c (n "wikitext_table_parser") (v "0.2.2") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1sr0ac28ifdvbjbfx4mkn7jga0rc9l3rgx5j25cjw4fi3g373g5k")))

(define-public crate-wikitext_table_parser-0.2.3 (c (n "wikitext_table_parser") (v "0.2.3") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1m7s7np6dmdj4g1rg2kwxwmfd6lw3zrkzwn4bjmvh6alv9xw7g1x")))

(define-public crate-wikitext_table_parser-0.2.4 (c (n "wikitext_table_parser") (v "0.2.4") (d (list (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1zq79zky1gn945cyldfl4ffwsjd7svvkswg0jgdg3h5pf5lzympr")))

(define-public crate-wikitext_table_parser-0.3.0 (c (n "wikitext_table_parser") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.21.0-dev") (d #t) (k 0)) (d (n "regex") (r "1.10.*") (d #t) (k 0)) (d (n "strum") (r "^0.26") (d #t) (k 0)) (d (n "strum_macros") (r "^0.26") (d #t) (k 0)))) (h "1kjphqxp3wnvp8x3md4g4ccd15ap2gagfhc7f2lq0g3kkc6k4k5l")))

