(define-module (crates-io wi ki wikipedia) #:use-module (crates-io))

(define-public crate-wikipedia-0.1.0 (c (n "wikipedia") (v "0.1.0") (d (list (d (n "hyper") (r "*") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "*") (o #t) (d #t) (k 0)))) (h "1bjgfvh6s77hgsb92jhh9y13y1d9rxw1j71zhccizj7gq324z3xn") (f (quote (("http-client" "hyper" "url") ("default" "http-client")))) (y #t)))

(define-public crate-wikipedia-0.2.0 (c (n "wikipedia") (v "0.2.0") (d (list (d (n "hyper") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^0.2.37") (o #t) (d #t) (k 0)))) (h "1lc5lcwmrh01wmkjdmzyzmmx52cfslkhkqrw2x72a16v0rn5x726") (f (quote (("http-client" "hyper" "url") ("default" "http-client"))))))

(define-public crate-wikipedia-0.3.1 (c (n "wikipedia") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0f2xskdakww8i2kxr5dqsg8yplzh0fhdc7qav2gfli43ydlhmdkx") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

(define-public crate-wikipedia-0.3.2 (c (n "wikipedia") (v "0.3.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.8") (d #t) (k 0)) (d (n "url") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0hqv3dk0sw441sxfpqhwr0d1ri7hqrp15avqlmicrph8xf0rd2xz") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

(define-public crate-wikipedia-0.3.3 (c (n "wikipedia") (v "0.3.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (o #t) (d #t) (k 0)))) (h "18ybcmjaxfg2jax9frj6wldbgn4kc8yx2g96wdxlhmzzppib5r64") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

(define-public crate-wikipedia-0.3.4 (c (n "wikipedia") (v "0.3.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (o #t) (d #t) (k 0)))) (h "0zp64gx5xvicw7zbmgb7hqmjhhdx4218c38wlli90ljwp2jj8mhr") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

(define-public crate-wikipedia-0.4.0 (c (n "wikipedia") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1x4k8cf0v9migfpab9gc7km4vf5v0bc5cg6pyyk2vcibddzyzkwp") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

