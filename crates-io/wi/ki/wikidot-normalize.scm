(define-module (crates-io wi ki wikidot-normalize) #:use-module (crates-io))

(define-public crate-wikidot-normalize-0.1.0 (c (n "wikidot-normalize") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "0zr4lvpg7wd45sdwcps8nx8gm90h0gi2mi4vz69551msydis95pb")))

(define-public crate-wikidot-normalize-0.1.1 (c (n "wikidot-normalize") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "0f69kkixv6q0d8s52y7i3swalqai1bdyxyx91gx4l03vz3rkfjg5")))

(define-public crate-wikidot-normalize-0.2.0 (c (n "wikidot-normalize") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "1mm4i60fzwi81zzgwdljbcaav1jzakhg9s8llky9f7b075rdlmhj")))

(define-public crate-wikidot-normalize-0.3.0 (c (n "wikidot-normalize") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "1ws3fz2aly4zcikfycr93095gapks3gq4k8q7a4fpgbi3lsjgz4c")))

(define-public crate-wikidot-normalize-0.4.1 (c (n "wikidot-normalize") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "0xxpjq8rh3nck1s4mgrxkv6vpn67wvjgksyldnb0yscglfmrzsfs")))

(define-public crate-wikidot-normalize-0.5.0 (c (n "wikidot-normalize") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)))) (h "13yywm9cjimchjdy8rzx3wq225l2kx1z5vwm1s2qlxmc15x6z4pv")))

(define-public crate-wikidot-normalize-0.6.1 (c (n "wikidot-normalize") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0aijgzrw4xhwfj2jwcmdmnnvfn1avp5labh3zjmpbalj3ysspnhb")))

(define-public crate-wikidot-normalize-0.6.2 (c (n "wikidot-normalize") (v "0.6.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^0.1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "188108v3a3syw653p404k66yw1w4r9lm17fagp1w1vbhyijgc7yp")))

(define-public crate-wikidot-normalize-0.6.3 (c (n "wikidot-normalize") (v "0.6.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1pyb6q7n7m4zx0hd6g80px1jf6wcjznkixyzh3ja77d2ir77iv34")))

(define-public crate-wikidot-normalize-0.7.0 (c (n "wikidot-normalize") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "1s676j3l2b7r2ij9lvs06w4bdphz35s3dm149ib0dvh6az1nkyvq")))

(define-public crate-wikidot-normalize-0.7.1 (c (n "wikidot-normalize") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0mzz56h7zfwscs16pwi6qgz2jg19bzl6szlr6jibdiam582689f3")))

(define-public crate-wikidot-normalize-0.8.0 (c (n "wikidot-normalize") (v "0.8.0") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "0mv6r6c9knz3sxz4bi8vl1dqvgh77krrp3qxv9jlmd9mgz5846ji")))

(define-public crate-wikidot-normalize-0.8.1 (c (n "wikidot-normalize") (v "0.8.1") (d (list (d (n "deunicode") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)))) (h "10fh743sglw014gmvnnh4xx6kpxbccpsbp447fmyx4jazw3z5pf9")))

(define-public crate-wikidot-normalize-0.9.0 (c (n "wikidot-normalize") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0krshrfafw8aqz64f0m7rxrq749wpcw5g4vwzw0sq0z2x5y21ffv")))

(define-public crate-wikidot-normalize-0.9.1 (c (n "wikidot-normalize") (v "0.9.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0340wikj01xq82827r6cb0w0z97y2vpc2cgj4y7s5y6kkwqn35a3")))

(define-public crate-wikidot-normalize-0.9.2 (c (n "wikidot-normalize") (v "0.9.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0xjxy693lnydh20n7yyq50wgkvfzakf0rfg3qdfql7m59aqzv26m")))

(define-public crate-wikidot-normalize-0.10.0 (c (n "wikidot-normalize") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0ym9brlw0qsdm343w83j9rln4habcgkl859g528b7ng5dvhsdf3g")))

(define-public crate-wikidot-normalize-0.11.0 (c (n "wikidot-normalize") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "03zksf4590xxcxqp8l3lalmp9iahgwkl06xs8h0zqpb1k77zlqi5")))

(define-public crate-wikidot-normalize-0.12.0 (c (n "wikidot-normalize") (v "0.12.0") (d (list (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "str-macro") (r "^1") (d #t) (k 2)) (d (n "trim-in-place") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1wc1j8j8daqj21h7wqm4sn4c55p06gmv5hlyqqp8gcsskadmwnl3")))

