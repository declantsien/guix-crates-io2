(define-module (crates-io wi ki wikipedia_liefland) #:use-module (crates-io))

(define-public crate-wikipedia_liefland-0.4.0 (c (n "wikipedia_liefland") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0kaxz281j7g4axjj3wb2v8ac7505dbyldzqamg1jlc2159yavw95") (f (quote (("http-client" "reqwest" "url") ("default" "http-client"))))))

