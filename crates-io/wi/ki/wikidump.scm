(define-module (crates-io wi ki wikidump) #:use-module (crates-io))

(define-public crate-wikidump-0.1.0 (c (n "wikidump") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "parse_wiki_text") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "1rihvkzxfh1091lz86bkamppnlilznhs5g8pjh33igpa9kzx0ia0")))

(define-public crate-wikidump-0.2.0 (c (n "wikidump") (v "0.2.0") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "parse_wiki_text") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "0d3xx2wva0hrqh2ajflbbz0bhd52589f1vj98q8gkzfm8pq4ysgm")))

(define-public crate-wikidump-0.2.1 (c (n "wikidump") (v "0.2.1") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "parse_wiki_text") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "0278g7i0b79izhnzh089djj8lr2x59wwwqx1s8rp4d5nr0y8psiz")))

(define-public crate-wikidump-0.2.2 (c (n "wikidump") (v "0.2.2") (d (list (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "parse_wiki_text") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "024f0mjbdzqalxrk8zn071vp19qjzk3p2a8rkn675a8slidmwmx9")))

(define-public crate-wikidump-0.3.0 (c (n "wikidump") (v "0.3.0") (d (list (d (n "bzip2") (r "^0.4.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "parse_wiki_text") (r "^0.1.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.16.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)))) (h "1sygskw05sv83zmdq92f8n4b8ik811prrzxqgnjaginv17qradcj")))

