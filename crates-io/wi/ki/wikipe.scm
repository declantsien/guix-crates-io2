(define-module (crates-io wi ki wikipe) #:use-module (crates-io))

(define-public crate-wikipe-0.1.0 (c (n "wikipe") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "wikipedia") (r "^0.3.1") (d #t) (k 0)))) (h "1fy13rhkxq2yzw0751ry0r4a9qygybkjs9fi49hq55lm6v9vpdvv")))

