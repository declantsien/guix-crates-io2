(define-module (crates-io wi ki wiki_reader) #:use-module (crates-io))

(define-public crate-wiki_reader-0.1.0 (c (n "wiki_reader") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "wiki_loader") (r "^0.1.0") (d #t) (k 0)))) (h "107dinnvz47amzyxhbcqkgpk85amd5i94b9lgsjnlqxpzm604lqs")))

(define-public crate-wiki_reader-0.1.1 (c (n "wiki_reader") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "wiki_loader") (r "^0.1.1") (d #t) (k 0)))) (h "1hn57c161j4lcpl5vx7s3w4gi0gpbvbykc989ksnscznw33d3cn8")))

(define-public crate-wiki_reader-0.1.2 (c (n "wiki_reader") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "wiki_loader") (r "^0.1.1") (d #t) (k 0)))) (h "08ra4qw2a4qxav2bn867qvkiizfhxfqcjkxfw188cdyqq8bsa5cv")))

