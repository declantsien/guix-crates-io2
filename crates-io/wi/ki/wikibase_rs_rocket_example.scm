(define-module (crates-io wi ki wikibase_rs_rocket_example) #:use-module (crates-io))

(define-public crate-wikibase_rs_rocket_example-0.1.0 (c (n "wikibase_rs_rocket_example") (v "0.1.0") (d (list (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (f (quote ("tera_templates"))) (d #t) (k 0)) (d (n "wikibase") (r "^0.1") (d #t) (k 0)))) (h "18axpi1aaxcnqgmsczarzj9n2mhn2jhmdy9kxwdknvgb6nkh9108")))

(define-public crate-wikibase_rs_rocket_example-0.1.1 (c (n "wikibase_rs_rocket_example") (v "0.1.1") (d (list (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (f (quote ("tera_templates"))) (d #t) (k 0)) (d (n "wikibase") (r "^0.1") (d #t) (k 0)))) (h "0dj8901dj0cqs81jrvjz0n1r27j838c0r1zi3nhckccpkpzrzdgj")))

(define-public crate-wikibase_rs_rocket_example-0.2.0 (c (n "wikibase_rs_rocket_example") (v "0.2.0") (d (list (d (n "rocket") (r "^0.3") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3") (f (quote ("tera_templates"))) (d #t) (k 0)) (d (n "wikibase") (r "^0.2") (d #t) (k 0)))) (h "0whcw6hc3iwyc0vcigar1j6chm2wnv2lhsy175ysf9i95885gg58")))

