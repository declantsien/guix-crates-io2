(define-module (crates-io wi ki wikidata) #:use-module (crates-io))

(define-public crate-wikidata-0.1.0 (c (n "wikidata") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "serde"))) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "13dvh8f1nvmcg35yw0f464i8cgk4i858rsfs5zbxscb3qc9mias4")))

(define-public crate-wikidata-0.1.1 (c (n "wikidata") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "serde"))) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qi3sj0df9mrkvaj2x2zi0vnrj5cgdimlz7ha4w9mbkbqgkfh00p")))

(define-public crate-wikidata-0.2.0 (c (n "wikidata") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "serde"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cihs9272p0svkx3ac4jr3mbslfqva38nfqbad820ailnr3nq3b5")))

(define-public crate-wikidata-0.2.1 (c (n "wikidata") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "serde"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0kp5z7z8pfq6y0d5g9cksvj08dgggj0064g57mlm2jqilp90dg5l")))

(define-public crate-wikidata-0.3.0 (c (n "wikidata") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("std" "serde"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0i75p2ljhkfw9gn8l72x28k6359aa5ipvrpb34ll04g72kbrml87")))

(define-public crate-wikidata-0.3.1 (c (n "wikidata") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "serde"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1wfjz6lhlaahr3m4xkxl4a23xsln6yq43q3ipdh75hz3gnmm0ha0") (y #t)))

(define-public crate-wikidata-1.0.0 (c (n "wikidata") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("std" "serde"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0dsq0v0p6vpim7l04c71vn8idm81lbdlf7f0k91c5kp8vpcamr3k")))

