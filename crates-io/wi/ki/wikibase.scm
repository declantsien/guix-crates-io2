(define-module (crates-io wi ki wikibase) #:use-module (crates-io))

(define-public crate-wikibase-0.1.1 (c (n "wikibase") (v "0.1.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18z0ahlsx170grqw98ydnc2prs8hvszzsvhxk81qcg3rwgnwb2my")))

(define-public crate-wikibase-0.1.2 (c (n "wikibase") (v "0.1.2") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yd7xifnyz67gnqglyam6dxq2qjzi2dxgsvvsqkf34469p26d58j")))

(define-public crate-wikibase-0.1.3 (c (n "wikibase") (v "0.1.3") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kz8hyhzy6bgz3fzbkrgj2c64a72lfgza92n1wr51p1ziqnixa1g")))

(define-public crate-wikibase-0.1.4 (c (n "wikibase") (v "0.1.4") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mj5sdasa43ws468mfaxm4haiwj7rq1hs10yfsspw2i4701ggw5r")))

(define-public crate-wikibase-0.1.5 (c (n "wikibase") (v "0.1.5") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09rvc9ac31x4sry9zzdck2c2ahr82as54zi0q0sp3wnk5dis4pas")))

(define-public crate-wikibase-0.2.0 (c (n "wikibase") (v "0.2.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jxmgsm4qzwc2514ml8hcnxxrifdf523dg9k0dsgvj1c6mpg7fqa")))

(define-public crate-wikibase-0.3.0 (c (n "wikibase") (v "0.3.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0phcqxi96lx0gkv02bd7gnpzgi0fkhrav03klzzhj8xqsrnd1k8m")))

(define-public crate-wikibase-0.3.1 (c (n "wikibase") (v "0.3.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1plwn2pgd5f4alkffva1rhs1p39frdx85kjflx4scmah9xbwz71w")))

(define-public crate-wikibase-0.3.2 (c (n "wikibase") (v "0.3.2") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qlgdvl4xijg7grhkixqr420v6vw4zm5750dpnjbkwwl72xkavvy")))

(define-public crate-wikibase-0.3.3 (c (n "wikibase") (v "0.3.3") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.7") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ccbi2nmwsf5nxswx4skbrf9wy903brbpl3ig6xy33c4cq1s6003")))

(define-public crate-wikibase-0.3.4 (c (n "wikibase") (v "0.3.4") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "155k5d95pp0i634lxa64wx2qfx00xhb7wwcb1jzpz08rawnakfha")))

(define-public crate-wikibase-0.3.5 (c (n "wikibase") (v "0.3.5") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xbccyzvrm0x8dmy23xaf4wxb8pqvp9czfmgjljy1hn9x1j9rabi")))

(define-public crate-wikibase-0.3.6 (c (n "wikibase") (v "0.3.6") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0chnnjhsr3kkxzsxc463pkr9rvgy7s865hmbdq5j83radark242i")))

(define-public crate-wikibase-0.3.7 (c (n "wikibase") (v "0.3.7") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.12") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1whkzpg7d0mqmlgxfrhy89izym428xyrs7p9wd3jyzl5mm35xvsr")))

(define-public crate-wikibase-0.4.0 (c (n "wikibase") (v "0.4.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k9wbpvw39p78jwjzdlwa85ss5rp11hc8lhl8jb7xh3vlznzwk8y")))

(define-public crate-wikibase-0.5.0 (c (n "wikibase") (v "0.5.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "mediawiki") (r "^0.1.16") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06hpks32pn5lgcn8pkswjqc31d6fvdsfyj9r7k38d12x7p7sic7z")))

