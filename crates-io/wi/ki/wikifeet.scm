(define-module (crates-io wi ki wikifeet) #:use-module (crates-io))

(define-public crate-wikifeet-1.0.0 (c (n "wikifeet") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0v8s6z65ya9hdcg5aymwgzqa2nwbw50pzh7cb39l3hmgm4iv9hiz")))

