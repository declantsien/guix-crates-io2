(define-module (crates-io wi ki wikidata-filter) #:use-module (crates-io))

(define-public crate-wikidata-filter-0.1.0 (c (n "wikidata-filter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rio_api") (r "^0.7.1") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1ki7askzph22390ffd4mfj0gyl3253l6ijx857xv8xq2d5120ygc")))

(define-public crate-wikidata-filter-0.2.0 (c (n "wikidata-filter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rio_api") (r "^0.7.1") (d #t) (k 0)) (d (n "rio_turtle") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "1qplfzbkai7sskgj5kkc4d4pdd6hxhgps7s9kk2r85gvxx42hwsl")))

