(define-module (crates-io wi ki wikimedia-api) #:use-module (crates-io))

(define-public crate-wikimedia-api-0.1.1 (c (n "wikimedia-api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1apjsywizs6zjvjcwfxbwswv2wh1w67s2q2fdvicvpa30vn0q3f3")))

