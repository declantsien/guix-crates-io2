(define-module (crates-io wi t- wit-bindgen-mbt) #:use-module (crates-io))

(define-public crate-wit-bindgen-mbt-0.1.0 (c (n "wit-bindgen-mbt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.16.0") (d #t) (k 0)))) (h "1gz0j6jrw4c84rpki2xzd3l4sjljc4rl42s52b4wal4irqfp9a46")))

