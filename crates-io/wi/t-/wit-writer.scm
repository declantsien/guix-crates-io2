(define-module (crates-io wi t- wit-writer) #:use-module (crates-io))

(define-public crate-wit-writer-0.1.0 (c (n "wit-writer") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)))) (h "1s2pydp55ppl80pr79c6zrrqpi7kb411k2nxilgyc0z1z66i5szb")))

(define-public crate-wit-writer-0.2.0 (c (n "wit-writer") (v "0.2.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)))) (h "028ssj09ijrqb5h7yxzc9ilylxip2mmfb286k9wzzg4wbsx03bf2")))

