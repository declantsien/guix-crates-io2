(define-module (crates-io wi t- wit-bindgen-rt) #:use-module (crates-io))

(define-public crate-wit-bindgen-rt-0.20.0 (c (n "wit-bindgen-rt") (v "0.20.0") (h "12nx31wjjfwl3mfwn2f1y04g5i2n7dg0kizvgyzb201k1g3qj6bq")))

(define-public crate-wit-bindgen-rt-0.21.0 (c (n "wit-bindgen-rt") (v "0.21.0") (h "15y4ami107gkq1bj2g035wbndsz0v55rbwilllgm84k7gyi28v82")))

(define-public crate-wit-bindgen-rt-0.22.0 (c (n "wit-bindgen-rt") (v "0.22.0") (h "0r7ff8r8arciw6k7hmairrcqkpdnq6vvng4p75kjsapkf2177f7w")))

(define-public crate-wit-bindgen-rt-0.23.0 (c (n "wit-bindgen-rt") (v "0.23.0") (d (list (d (n "bitflags") (r "^2.3.3") (o #t) (d #t) (k 0)))) (h "06y8da45hm00jgic57h9rb6p0piwfwv0rcyxfiacfrwnlv3d9k6n")))

(define-public crate-wit-bindgen-rt-0.24.0 (c (n "wit-bindgen-rt") (v "0.24.0") (d (list (d (n "bitflags") (r "^2.3.3") (o #t) (d #t) (k 0)))) (h "15pzcfcqg25hpndkg9zj48xmqv6mv260k6k81zbhwqs6f37q01rv")))

(define-public crate-wit-bindgen-rt-0.25.0 (c (n "wit-bindgen-rt") (v "0.25.0") (d (list (d (n "bitflags") (r "^2.3.3") (o #t) (d #t) (k 0)))) (h "19kxz9bagh86l72kj42hsy7hrdvq0fhkd8n2njal2v8593wy50zg")))

