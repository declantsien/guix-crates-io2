(define-module (crates-io wi t- wit-bindgen-gen-markdown) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-markdown-0.3.0 (c (n "wit-bindgen-gen-markdown") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)))) (h "11v26zy0azld150b03r758gwkclk049lw53ip7mc14hf6axicbjw")))

