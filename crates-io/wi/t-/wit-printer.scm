(define-module (crates-io wi t- wit-printer) #:use-module (crates-io))

(define-public crate-wit-printer-0.1.0 (c (n "wit-printer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wasmprinter") (r "^0.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)))) (h "0zzp6cb36przmiizci9p5gm7bpkqkcq2r909s5kbgzplhiry2xlc")))

(define-public crate-wit-printer-0.2.0 (c (n "wit-printer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wasmprinter") (r "^0.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)))) (h "1sgwys83pns27vdxirf6n1qjgxg4lr3n9vmcd56w38sm8nj9rwck")))

