(define-module (crates-io wi t- wit-bindgen-gen-guest-rust) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-guest-rust-0.3.0 (c (n "wit-bindgen-gen-guest-rust") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-bindgen-gen-rust-lib") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)))) (h "1m276a7qnjyh64a8qs5a1a6dp0f46imjd6zkksp0il1h3ha6vris")))

