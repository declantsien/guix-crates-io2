(define-module (crates-io wi t- wit-bindgen-gen-ts-near) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-ts-near-0.0.0 (c (n "wit-bindgen-gen-ts-near") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.0") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "1fhi6dr67ymmdn6f2vc2lgbfy4w4gxybrm8w1ngs83s48p49hv87")))

(define-public crate-wit-bindgen-gen-ts-near-0.0.1 (c (n "wit-bindgen-gen-ts-near") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.0") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "1qqjln18rb3cb4kiv80b6akif4xk16ak077pvwl0g5ax45f60piq")))

(define-public crate-wit-bindgen-gen-ts-near-0.1.0 (c (n "wit-bindgen-gen-ts-near") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.0") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "0aql7yryhwigj1i8h6705142lrp45kr3w3gx3gyb962y88gqahk5")))

(define-public crate-wit-bindgen-gen-ts-near-0.2.0 (c (n "wit-bindgen-gen-ts-near") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.1") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "1hfvcapgid6mlcghajrpxw69p2kqcbyls8js8iamh8x6qy12mxl8")))

(define-public crate-wit-bindgen-gen-ts-near-0.2.1 (c (n "wit-bindgen-gen-ts-near") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.1") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "0v24y85q0jp4irf6fdxg7jx024r625d1kggnw7vmyq3xibhlwpdy")))

(define-public crate-wit-bindgen-gen-ts-near-0.2.2 (c (n "wit-bindgen-gen-ts-near") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.1") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "0vgb165avsfl62z32nbjcyydds7c87910is1f6ab3xvy2qal870q")))

(define-public crate-wit-bindgen-gen-ts-near-0.3.0 (c (n "wit-bindgen-gen-ts-near") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-gen-core") (r "^0.0.1") (d #t) (k 0) (p "aha-wit-bindgen-gen-core")))) (h "1najxh9saz01zr1k11hpz9j1gcdkd66dbfxkyydbn63621pic6rj")))

