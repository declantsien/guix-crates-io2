(define-module (crates-io wi t- wit-bindgen-gen-rust-lib) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-rust-lib-0.3.0 (c (n "wit-bindgen-gen-rust-lib") (v "0.3.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)))) (h "1r1zkzvbfgwdzsjikz7k0knl7kh3zijxblv27hj0ixb6pqw92zjb")))

