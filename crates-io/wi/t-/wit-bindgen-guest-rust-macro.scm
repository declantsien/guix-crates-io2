(define-module (crates-io wi t- wit-bindgen-guest-rust-macro) #:use-module (crates-io))

(define-public crate-wit-bindgen-guest-rust-macro-0.3.0 (c (n "wit-bindgen-guest-rust-macro") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-bindgen-gen-guest-rust") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)))) (h "04w38zysp0id60ky6x1860mdqd1sifb2rdh22br811q6ycprywgr")))

