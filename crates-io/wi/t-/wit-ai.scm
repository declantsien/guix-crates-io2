(define-module (crates-io wi t- wit-ai) #:use-module (crates-io))

(define-public crate-wit-ai-0.1.0 (c (n "wit-ai") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0gx41xb12w83ibdwbq5p7d93qpp7np760fvysczh3gnkkzfwsvgy")))

