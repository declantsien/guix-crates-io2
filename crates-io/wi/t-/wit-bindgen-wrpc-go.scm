(define-module (crates-io wi t- wit-bindgen-wrpc-go) #:use-module (crates-io))

(define-public crate-wit-bindgen-wrpc-go-0.1.0 (c (n "wit-bindgen-wrpc-go") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (k 0)) (d (n "heck") (r "^0.5") (k 0)) (d (n "wit-bindgen-core") (r "^0.24") (k 0)))) (h "080mkws7dzkz9k0bg8kg2m9ab7jrh8v5yh0g9dai3pzxk1mr00xd")))

(define-public crate-wit-bindgen-wrpc-go-0.1.1 (c (n "wit-bindgen-wrpc-go") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (k 0)) (d (n "heck") (r "^0.5") (k 0)) (d (n "wit-bindgen-core") (r "^0.24") (k 0)))) (h "0v8q2dp3bgxyc5d00l2awgb9ghdl0yhqcnmc4y3nzpgr7wbmlw7n")))

