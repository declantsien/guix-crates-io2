(define-module (crates-io wi t- wit-bindgen-gen-guest-c) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-guest-c-0.3.0 (c (n "wit-bindgen-gen-guest-c") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)))) (h "08gv3fllvb1z9g41lcj13gfwprvskanhk8aagpzi77mfw2vawz92")))

