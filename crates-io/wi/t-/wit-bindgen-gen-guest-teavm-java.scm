(define-module (crates-io wi t- wit-bindgen-gen-guest-teavm-java) #:use-module (crates-io))

(define-public crate-wit-bindgen-gen-guest-teavm-java-0.3.0 (c (n "wit-bindgen-gen-guest-teavm-java") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)))) (h "0kh7l5nwry5qf49f5s7n31r0ifh6rfvlgila0x7qywmym77j9j3p")))

