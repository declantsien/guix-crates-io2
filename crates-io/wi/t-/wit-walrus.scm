(define-module (crates-io wi t- wit-walrus) #:use-module (crates-io))

(define-public crate-wit-walrus-0.1.0 (c (n "wit-walrus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.14") (d #t) (k 0)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.1.0") (d #t) (k 0)))) (h "02i8d71pvmgw431fhrzl1420ap6zflmpq8idb265a09j8nwzn54f")))

(define-public crate-wit-walrus-0.2.0 (c (n "wit-walrus") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.16") (d #t) (k 0)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.1.0") (d #t) (k 0)))) (h "1dahf27xy1jky4dqzjnw3svan7xs43imn7p9gw8d7k5ilw6v71qa")))

(define-public crate-wit-walrus-0.3.0 (c (n "wit-walrus") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.17") (d #t) (k 0)) (d (n "wit-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.1.0") (d #t) (k 0)))) (h "1fa0r713y32jnxpjyg6xcf9776rz5jjb491lmpjkqyxi5pw0k780")))

(define-public crate-wit-walrus-0.4.0 (c (n "wit-walrus") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.17") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.2.0") (d #t) (k 0)))) (h "00p792lx8c63ka5s07nknj1g8dsfl9b54x78ry0y2i70knxkddkm")))

(define-public crate-wit-walrus-0.5.0 (c (n "wit-walrus") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.18") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.2.0") (d #t) (k 0)))) (h "09d19yl1qpf8xml8lkr8w6qyfrxzxw0l74yw7930hanh8yydfcmm")))

(define-public crate-wit-walrus-0.6.0 (c (n "wit-walrus") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "walrus") (r "^0.19") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-schema-version") (r "^0.1.0") (d #t) (k 0)) (d (n "wit-writer") (r "^0.2.0") (d #t) (k 0)))) (h "1kap4y2ynpqwz8903faip7ikcf1asqll3m3mlskb41349hz9wmdd")))

