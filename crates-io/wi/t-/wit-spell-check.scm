(define-module (crates-io wi t- wit-spell-check) #:use-module (crates-io))

(define-public crate-wit-spell-check-0.0.0 (c (n "wit-spell-check") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "hunspell-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "wit-component") (r "^0.201.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "10qsdi7p28chm66pa9fiam8z6ch9pgjr6b8g0aa2llxyc56j8v47") (f (quote (("default"))))))

