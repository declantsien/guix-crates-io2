(define-module (crates-io wi t- wit-bindgen-rust-lib) #:use-module (crates-io))

(define-public crate-wit-bindgen-rust-lib-0.4.0 (c (n "wit-bindgen-rust-lib") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.4.0") (d #t) (k 0)))) (h "1nl3a3jwz0l7bmpbbb76izn74ps26qli5srb4ffhp0mibx5qy2mb")))

(define-public crate-wit-bindgen-rust-lib-0.5.0 (c (n "wit-bindgen-rust-lib") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.5.0") (d #t) (k 0)))) (h "0zch8s94db8qp8qgmjh0wxk6qjx79nphb1jd95lmg62l7hc1k8qg")))

(define-public crate-wit-bindgen-rust-lib-0.6.0 (c (n "wit-bindgen-rust-lib") (v "0.6.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.6.0") (d #t) (k 0)))) (h "0b1d3ak7ijaya3r62321hpiwyhf2ap8xnggdnqx3cpccfhjp7bqb")))

(define-public crate-wit-bindgen-rust-lib-0.7.0 (c (n "wit-bindgen-rust-lib") (v "0.7.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.7.0") (d #t) (k 0)))) (h "16mq9cj3p407k7zhyrwvv9cf37q5ghqy1kyfrnhz2sfjrssmhgcx")))

(define-public crate-wit-bindgen-rust-lib-0.8.0 (c (n "wit-bindgen-rust-lib") (v "0.8.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.8.0") (d #t) (k 0)))) (h "1fi0xzn956djfwb9ywl5l2r9xapndvm6wzrq8cx91c08phsg6l0w")))

(define-public crate-wit-bindgen-rust-lib-0.9.0 (c (n "wit-bindgen-rust-lib") (v "0.9.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.9.0") (d #t) (k 0)))) (h "1nid2dxgrpyn6crvah7vij1mxk8l2d6miir797yw3jkpdj6dwcaw")))

(define-public crate-wit-bindgen-rust-lib-0.10.0 (c (n "wit-bindgen-rust-lib") (v "0.10.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.10.0") (d #t) (k 0)))) (h "1p7lwyj03fwr1s9y2sv2ihpgfxlz6vdirjwmwamwspsv7iahpq48")))

(define-public crate-wit-bindgen-rust-lib-0.11.0 (c (n "wit-bindgen-rust-lib") (v "0.11.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.11.0") (d #t) (k 0)))) (h "1szf7zqlsyibpa7pd7jknwli5639ycdy0skxf85qphkx7jjbg7yd")))

(define-public crate-wit-bindgen-rust-lib-0.12.0 (c (n "wit-bindgen-rust-lib") (v "0.12.0") (d (list (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.12.0") (d #t) (k 0)))) (h "0a77h0196y7nly34yriqdwvqky33dw142n3l311icgm9q74qjg51")))

