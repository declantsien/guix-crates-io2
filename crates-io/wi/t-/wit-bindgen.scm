(define-module (crates-io wi t- wit-bindgen) #:use-module (crates-io))

(define-public crate-wit-bindgen-0.0.0 (c (n "wit-bindgen") (v "0.0.0") (h "0y5d6adj644n7r96fg4kly98xv83094dp5d4xzj1davfxag89rpb")))

(define-public crate-wit-bindgen-0.3.0 (c (n "wit-bindgen") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "wit-bindgen-guest-rust-macro") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0nji7i67kjpycf53g4gs10chf9f1n756m942cs594p9pzkmx4a65") (f (quote (("realloc") ("macros" "wit-bindgen-guest-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.4.0 (c (n "wit-bindgen") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0r3jpzifx50qcz9ykd88719gzwis40jqn8p1a9b6q8c6hxzzaz2f") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.5.0 (c (n "wit-bindgen") (v "0.5.0") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0604ysf1vn39wl7507mrl5zc4vlnwnf3kaggxlzkn5h8h1i3hb5v") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.6.0 (c (n "wit-bindgen") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1gxmv1q2f0yy34ikhwpl9rsk2lpk4rx60cfzng27m12m7wyxj8md") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.7.0 (c (n "wit-bindgen") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1gf457n6pcl6j0slqpxg35mm87mgnbimvxv2cj48wch574dmja56") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.8.0 (c (n "wit-bindgen") (v "0.8.0") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "08l577rafp41mh6kwg9jx3pnkd2dbvfqihjv2accmivcwklicb9r") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.9.0 (c (n "wit-bindgen") (v "0.9.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1h33vx7zk10gar5a00y94gp1zaq4niaafhjl5jnlz6ff0idd3hzm") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.10.0 (c (n "wit-bindgen") (v "0.10.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1hpmy3pxkzng7byadg1mymx2314fvasykw2zsq33sp9fhr6gx1ha") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.11.0 (c (n "wit-bindgen") (v "0.11.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0yc23jw91qrvbzm06f3il73av7vi40bs3n8h8kmycl6wcplyi8zq") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.12.0 (c (n "wit-bindgen") (v "0.12.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "06i1acly7cbirdx9hlbbx134m137nvm7cg019ky17q4sypbcbxxl") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.13.0 (c (n "wit-bindgen") (v "0.13.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "0wgdi8y2kv27k17qa72q82kw6l3mccd5ia8kjh2p8q3brbh2rnf7") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.13.1 (c (n "wit-bindgen") (v "0.13.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "025w2fj3740q6vwg7ppnvnbh7r6g0q8dxa5252n3rh6plma6qwiq") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.13.2 (c (n "wit-bindgen") (v "0.13.2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.13.2") (o #t) (d #t) (k 0)))) (h "0r6yj248fhw98ifmq2kp97dpv6mjqprqsi0mg31kacwpjs3lw2n2") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc")))) (y #t)))

(define-public crate-wit-bindgen-0.14.0 (c (n "wit-bindgen") (v "0.14.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.14.0") (o #t) (d #t) (k 0)))) (h "186lr35lq3989b6kc5jzpkbpq0d80zj7w3cf65bxdy9qfkkcc182") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.15.0 (c (n "wit-bindgen") (v "0.15.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.15.0") (o #t) (d #t) (k 0)))) (h "1fmykll1dpp78ifgm2pm4cmbd66kzjfp88csjgdzdfdnpnh6gqyh") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.16.0 (c (n "wit-bindgen") (v "0.16.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "085wwpdq8x0fbmbg7wy2401z46z7pf4fh8d40aagdd3qjq4isvxp") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.17.0 (c (n "wit-bindgen") (v "0.17.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.17.0") (o #t) (d #t) (k 0)))) (h "0kdsbqi2l6vrbxrqj3z1vi6dg1r7ml47n5dbrpd2cys9jf6icdv2") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.17.1 (c (n "wit-bindgen") (v "0.17.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.17.1") (o #t) (d #t) (k 0)))) (h "0wsbxiarm695gdxli0zclz9y11zaql52iajavzgc06xw9wn7d2np") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc")))) (y #t)))

(define-public crate-wit-bindgen-0.18.0 (c (n "wit-bindgen") (v "0.18.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.18.0") (o #t) (d #t) (k 0)))) (h "1h4c7rcl6sb24nanvnbw5fvi16sd1wzkkci3dxv8nhfzzi1df22l") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.19.0 (c (n "wit-bindgen") (v "0.19.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1p59cfax1ybgsy443ijay5cis7yj72dgzhrs1sphg4dp6f98xs6p") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.19.1 (c (n "wit-bindgen") (v "0.19.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.19.1") (o #t) (d #t) (k 0)))) (h "00mxyznlyvmn09in75qjv1m1fb7ksqjc8ypbwlfr7fgwkr6h1vm4") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.19.2 (c (n "wit-bindgen") (v "0.19.2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.19.2") (o #t) (d #t) (k 0)))) (h "07wkrrqvgh9kn3nnjl5i7w5a2xnxbfnk7iha96nf04j0m46jfzdk") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.20.0 (c (n "wit-bindgen") (v "0.20.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "1k5gw1rm284wyr5b00yz8yn4psbql82v32nlwbngl87k7s7hbrir") (f (quote (("realloc") ("macros" "wit-bindgen-rust-macro") ("default" "macros" "realloc"))))))

(define-public crate-wit-bindgen-0.21.0 (c (n "wit-bindgen") (v "0.21.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.21.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "172mybri0nq05mjq2f45q96gj5qwbx8m0x0ylz7w52ccsf6xppnv") (f (quote (("default" "macros" "realloc")))) (s 2) (e (quote (("realloc" "dep:wit-bindgen-rt") ("macros" "dep:wit-bindgen-rust-macro"))))))

(define-public crate-wit-bindgen-0.22.0 (c (n "wit-bindgen") (v "0.22.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.22.0") (o #t) (d #t) (k 0)))) (h "09rcm5mma5ld0mpl3sh99dalihc1pvrxbkaj3d9mqsqflcp9k3r8") (f (quote (("default" "macros" "realloc")))) (s 2) (e (quote (("realloc" "dep:wit-bindgen-rt") ("macros" "dep:wit-bindgen-rust-macro"))))))

(define-public crate-wit-bindgen-0.23.0 (c (n "wit-bindgen") (v "0.23.0") (d (list (d (n "wit-bindgen-rt") (r "^0.23.0") (f (quote ("bitflags"))) (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.23.0") (o #t) (d #t) (k 0)))) (h "17h5nj1v4j3rms25kl7ax0c7y00b96flvczm205j9kjxy9v346h4") (f (quote (("realloc") ("default" "macros" "realloc")))) (s 2) (e (quote (("macros" "dep:wit-bindgen-rust-macro"))))))

(define-public crate-wit-bindgen-0.24.0 (c (n "wit-bindgen") (v "0.24.0") (d (list (d (n "wit-bindgen-rt") (r "^0.24.0") (f (quote ("bitflags"))) (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.24.0") (o #t) (d #t) (k 0)))) (h "1xg7nyxzs9i8vj3d0bivdam6iiw3mblpkx1qgvj0ny336xjygd4z") (f (quote (("realloc") ("default" "macros" "realloc")))) (s 2) (e (quote (("macros" "dep:wit-bindgen-rust-macro"))))))

(define-public crate-wit-bindgen-0.25.0 (c (n "wit-bindgen") (v "0.25.0") (d (list (d (n "wit-bindgen-rt") (r "^0.25.0") (f (quote ("bitflags"))) (d #t) (k 0)) (d (n "wit-bindgen-rust-macro") (r "^0.25.0") (o #t) (d #t) (k 0)))) (h "1ch0gya59qw02lscbpyh37xqvn1vcfpv96q756cwprk5x5f7ljbz") (f (quote (("realloc") ("default" "macros" "realloc")))) (s 2) (e (quote (("macros" "dep:wit-bindgen-rust-macro"))))))

