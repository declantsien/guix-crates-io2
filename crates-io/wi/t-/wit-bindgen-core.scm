(define-module (crates-io wi t- wit-bindgen-core) #:use-module (crates-io))

(define-public crate-wit-bindgen-core-0.3.0 (c (n "wit-bindgen-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "wit-component") (r "^0.6.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.0") (d #t) (k 0)))) (h "0s99nrxxd9xd6y33xz02n53maizw42d5kimjxvxnvh08fmw4j7ga")))

(define-public crate-wit-bindgen-core-0.4.0 (c (n "wit-bindgen-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "wit-component") (r "^0.7.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.3") (d #t) (k 0)))) (h "1rfgz8yfih9zfhbifdjqdrvwkf0fwfkjw3hxjchcg1kx01rpn5zg")))

(define-public crate-wit-bindgen-core-0.5.0 (c (n "wit-bindgen-core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "wit-component") (r "^0.8.1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1d2nzk149fwj33pj17xblymhj72y1y489g42rg6qbdwi6xialsil")))

(define-public crate-wit-bindgen-core-0.6.0 (c (n "wit-bindgen-core") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "wit-component") (r "^0.8.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.1") (d #t) (k 0)))) (h "0rw8r5zidlx07vjyr2wcr3w59cqgvh957xwp48glj5kzx2kbbhav")))

(define-public crate-wit-bindgen-core-0.7.0 (c (n "wit-bindgen-core") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "wit-component") (r "^0.11.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "021bgslyfxdmjgay9kflhpn54kczi458hm8fq5dcncsgkjk9m89l")))

(define-public crate-wit-bindgen-core-0.8.0 (c (n "wit-bindgen-core") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "wit-component") (r "^0.11.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0n41b613dqx7bs36jss6fbp6cz408y264w9kikhs1jkqpmnd68nl")))

(define-public crate-wit-bindgen-core-0.9.0 (c (n "wit-bindgen-core") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "wit-component") (r "^0.12.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.9.0") (d #t) (k 0)))) (h "1snd72c4c22hh0pziywmnnapacwk209mmilv0wprwg2a9p2qwrf9")))

(define-public crate-wit-bindgen-core-0.10.0 (c (n "wit-bindgen-core") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.13.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.10.0") (d #t) (k 0)))) (h "022cdf2vmh5m0gl4sgq5jp1aj45x6i4qxxiylgiisyi7yh97xl9y")))

(define-public crate-wit-bindgen-core-0.11.0 (c (n "wit-bindgen-core") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "07nw8ckf1il8c6fh9g67agdd2hc0j5afhrp43nvd1ysxaq95a9bp")))

(define-public crate-wit-bindgen-core-0.12.0 (c (n "wit-bindgen-core") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.1") (d #t) (k 0)))) (h "01619w5pwrbk4r75h98p23vljnrr7r3p8bs2zfg5brw4fz2720vz")))

(define-public crate-wit-bindgen-core-0.13.0 (c (n "wit-bindgen-core") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.16.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "1q5kx01j1rjzx9dhj96nn315kffprkwdm76gxiqn123lw1d98nsn")))

(define-public crate-wit-bindgen-core-0.13.1 (c (n "wit-bindgen-core") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.17.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "07rpqbsxq848dn6wddk955apwyh2nyzxhcklsld1mwxgrkfizgy8")))

(define-public crate-wit-bindgen-core-0.13.2 (c (n "wit-bindgen-core") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.18.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1k30qv1scx85pf8izcynlswp4v9l5aala1580y0lrbqms0j11nmq") (y #t)))

(define-public crate-wit-bindgen-core-0.14.0 (c (n "wit-bindgen-core") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.18.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1vizf282vxc5h9098imh1l02ck9ajr4pa5pdxyi8vxzd77jk9z2d")))

(define-public crate-wit-bindgen-core-0.15.0 (c (n "wit-bindgen-core") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.18.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1j3i4c07drgb8n9sa2syc085hky3awriv5yw92j0vy1rvqzywyql")))

(define-public crate-wit-bindgen-core-0.16.0 (c (n "wit-bindgen-core") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.18.2") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "03bd3ffc4cyymgsdw6vsi4vaql8asbc81b7dn0griwla90d5xmbm")))

(define-public crate-wit-bindgen-core-0.17.0 (c (n "wit-bindgen-core") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.20.1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "1vc2jb4q8ng425pc0p8j93glf79fy4s7m5qv67jd79s5lr3jblm3")))

(define-public crate-wit-bindgen-core-0.17.1 (c (n "wit-bindgen-core") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-component") (r "^0.21.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.14.0") (d #t) (k 0)))) (h "1pxwacylcc18biv61r2309kxrqf4z3byfvqzg4aspazs7zl9gk23") (y #t)))

(define-public crate-wit-bindgen-core-0.18.0 (c (n "wit-bindgen-core") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.14.0") (d #t) (k 0)))) (h "14rw4yqyb4jwqzkq8vjxv62n3ygzq6kafmskdjfcz36hcda74iki")))

(define-public crate-wit-bindgen-core-0.19.0 (c (n "wit-bindgen-core") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.200.0") (d #t) (k 0)))) (h "0dx3gxzyqxlm69b7hfy5qn12p5vaykcf996d9vakfs2qm8gy6jq9")))

(define-public crate-wit-bindgen-core-0.19.1 (c (n "wit-bindgen-core") (v "0.19.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.200.0") (d #t) (k 0)))) (h "0rcp7m5djxxjxms9ylwb10am9g8n6vwzrglgrm98caxb555vgfc1")))

(define-public crate-wit-bindgen-core-0.19.2 (c (n "wit-bindgen-core") (v "0.19.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.200.0") (d #t) (k 0)))) (h "1rr07ci8iighb5izz15yg5nb94584gnckzzcyx3akpgrxc5hf8ci")))

(define-public crate-wit-bindgen-core-0.20.0 (c (n "wit-bindgen-core") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "0f7n3vaqbs23p3llzwiaz089sjmwwagn1xmf86zfkpnmzsrspa0i")))

(define-public crate-wit-bindgen-core-0.21.0 (c (n "wit-bindgen-core") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "16rn2dlbafs7kigjybyxs9gg7i3ny6zii75qx3kajs1k6q3vzg2f")))

(define-public crate-wit-bindgen-core-0.22.0 (c (n "wit-bindgen-core") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "190bnvyagn6j9vm6s412zrshdss7gr4p3l4s6mwz5k7vkxqp4pp8")))

(define-public crate-wit-bindgen-core-0.23.0 (c (n "wit-bindgen-core") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 0)))) (h "0hplz0asgvf9jhxdh4a13cb8idr4jpm4zzywxphn8bl5rm8430xh")))

(define-public crate-wit-bindgen-core-0.24.0 (c (n "wit-bindgen-core") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 0)))) (h "06zr51a5flrjb7l75bz805y0g932x807d34222d88h80jlff2rwv")))

(define-public crate-wit-bindgen-core-0.25.0 (c (n "wit-bindgen-core") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "wit-parser") (r "^0.208.0") (d #t) (k 0)))) (h "1qmigaqn7f3f0hhj7xn5sg7k0776g9yna5mxjdh1yvmgd4pa2xkh")))

