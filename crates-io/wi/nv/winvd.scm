(define-module (crates-io wi nv winvd) #:use-module (crates-io))

(define-public crate-winvd-0.0.1 (c (n "winvd") (v "0.0.1") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wtypesbase" "hstring" "guiddef"))) (d #t) (k 0)))) (h "011kgv7kg7pqzljvg1fzsg4fqzk3g61k5inz5d0i0lrvhdmmy0bd") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.2 (c (n "winvd") (v "0.0.2") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wtypesbase" "hstring" "guiddef"))) (d #t) (k 0)))) (h "0xvbqwg8z07kk0gkkbq4fcbl62dcm1n4lbrffkjqkpi1c4cvkn32") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.3 (c (n "winvd") (v "0.0.3") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wtypesbase" "hstring" "guiddef"))) (d #t) (k 0)))) (h "0fwimr5lhdzc6jjfjk7syp0f5axr9iw8pjd336kg9g6xbmi9kn8h") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.4 (c (n "winvd") (v "0.0.4") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wtypesbase" "hstring" "guiddef"))) (d #t) (k 0)))) (h "1rrv7ghqs9i3dxh4935yi4iqv9xrjxkgx4bqqq8k32iwdrj16zrz") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.5 (c (n "winvd") (v "0.0.5") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "wtypesbase" "hstring" "guiddef"))) (d #t) (k 0)))) (h "1zi3v3h802f2y1pwy5p9k7z11ka3pdxx02mipcpbnlbd0i0myhh6") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.6 (c (n "winvd") (v "0.0.6") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)))) (h "1r0hi5jxq75d469z7wp9bcdi1ws8zg8nlvyr69khl2dzickcm0lf") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.7 (c (n "winvd") (v "0.0.7") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)))) (h "0myhck25idwfqmj3p11z61n424mfz2h167sy5zpfzy07m7siimw6") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.8 (c (n "winvd") (v "0.0.8") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1vgd9l9v2x3awhw6q1mbnc0cgvza500akyarhc7miirfkkgnbaq1") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.9 (c (n "winvd") (v "0.0.9") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "058rqsf6pa57davnvc5wc41mndl7y0w9bk32dcln8liv8nf01z8f") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.10 (c (n "winvd") (v "0.0.10") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1sla9hc2m5ynv8fgj53nc00nirapl92pkdslq6x3w6pbqbanlbww") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.11 (c (n "winvd") (v "0.0.11") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1v50md28rn7k7qi9knmq7zfxgcw45niic3sbk8y5j5w6j595b4wd") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.12 (c (n "winvd") (v "0.0.12") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0m24bqj9zs6yqqb411szgwfbarnc2mjf60jv1cjd3hwlm4n7pi0s") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.13 (c (n "winvd") (v "0.0.13") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1bdiy0x8p2bz2m2ic9w0vfgy1vl8fj5a1c0y13zlmgwjjfcdmlc4") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.14 (c (n "winvd") (v "0.0.14") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0b7fms8bpj19ql8l1amm4hi9y49myw5zq7wjhfddl7ib6ca8169c") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.15 (c (n "winvd") (v "0.0.15") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0kqr11973sh71s99xasdny8x773wdh3w5iz2jwwn1cqcrvwrhgk6") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.16 (c (n "winvd") (v "0.0.16") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "1ilzgyz6j5cyn0mlgy8zk085p3qrfw6wc15vhrw3jcldg97zfbvn") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.17 (c (n "winvd") (v "0.0.17") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 2)))) (h "119nsj83z22if9bk5ibmlmjr7djcd51yrjbxqxkz45cplakh188h") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.18 (c (n "winvd") (v "0.0.18") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 2)))) (h "0nkvvczfm51lj9gf486y8ljawy6jmrvg9i9qigdgy5qwi9a9b3nx") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.19 (c (n "winvd") (v "0.0.19") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 2)))) (h "0ldssls2yxzgm916yh5l583zgjnm7jzqyj2ds3ddza01jgrrgpvy") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.20 (c (n "winvd") (v "0.0.20") (d (list (d (n "com") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 2)))) (h "0mia2dsnqkjw9djhvyk461llw932xi8028wp4z3vk0akfif2varb") (f (quote (("debug"))))))

(define-public crate-winvd-0.0.30 (c (n "winvd") (v "0.0.30") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.44") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.44") (d #t) (k 0)) (d (n "windows-interface") (r "^0.44") (d #t) (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "0qrakzry8v5jibf3yy6yx136q5p11b9yijs6pvq3z3sx5avzyv9i") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.40 (c (n "winvd") (v "0.0.40") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.44") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.44") (d #t) (k 0)) (d (n "windows-interface") (r "^0.44") (d #t) (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1gzma3n6ylywcdxnnjibijip6yx5zvn7vyb5ala35w3wrzmjb6ar") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.41 (c (n "winvd") (v "0.0.41") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.44") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.44") (d #t) (k 0)) (d (n "windows-interface") (r "^0.44") (d #t) (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "1cwsrxf8rx15ryns0jfg9f0y5ql34dn5jndbywz5zcy9wm0bkc5h") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.42 (c (n "winvd") (v "0.0.42") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.51") (d #t) (k 0)) (d (n "windows-interface") (r "^0.51") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (o #t) (d #t) (k 0)))) (h "1vivk0xpqkqrgx1ljwhf63kmxz6355sjim9v5l5bfcg1wmi9vamg") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.43 (c (n "winvd") (v "0.0.43") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.51") (d #t) (k 0)) (d (n "windows-interface") (r "^0.51") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (o #t) (d #t) (k 0)))) (h "0qrhcf12ji3acaaj25q6px8shjjg3ksarsrjyy9z8jcjf3arrkhv") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.44 (c (n "winvd") (v "0.0.44") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.51") (d #t) (k 0)) (d (n "windows-interface") (r "^0.51") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (o #t) (d #t) (k 0)))) (h "0jz226p1lvxx3m2assmy4c4rvcagw3vsl0n7fmdizyzwy6a9f7ln") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.45 (c (n "winvd") (v "0.0.45") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.51") (d #t) (k 0)) (d (n "windows-interface") (r "^0.51") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (o #t) (d #t) (k 0)))) (h "1fxq6yvz6wjf98g624wl3nkrfvqgk1003m53c91gmnl5bv08wj6v") (f (quote (("integration-tests"))))))

(define-public crate-winvd-0.0.46 (c (n "winvd") (v "0.0.46") (d (list (d (n "crossbeam-channel") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.0") (d #t) (k 2)) (d (n "windows") (r "^0.52") (f (quote ("implement" "Win32_System_Com" "Win32_UI_Shell_Common" "Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_System_Threading"))) (d #t) (k 0)) (d (n "windows-implement") (r "^0.52") (d #t) (k 0)) (d (n "windows-interface") (r "^0.52") (d #t) (k 0)) (d (n "winit") (r "^0.29.3") (o #t) (d #t) (k 0)))) (h "187iy2vv2y5gif8rrsy7qid56315svzffmcr5qi89b8x7mj9jnj5") (f (quote (("integration-tests"))))))

