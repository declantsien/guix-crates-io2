(define-module (crates-io wi nv winvec) #:use-module (crates-io))

(define-public crate-winvec-0.1.0 (c (n "winvec") (v "0.1.0") (h "1d6c3lkj63kgnjpg74bad5f608j799bvsdrpr3a9yyhmd38flfr4")))

(define-public crate-winvec-0.1.1 (c (n "winvec") (v "0.1.1") (h "0lp9iwlgsyif4dzdg0g4pf7xlbalqf9fq9sdq9vahz9nl3jsnp6p")))

(define-public crate-winvec-0.2.0 (c (n "winvec") (v "0.2.0") (h "00s2viaw4cma88d8lyp0n56y8jxxcmnzw0fn1bpi60qfmhlwdg8y")))

