(define-module (crates-io wi nv winver) #:use-module (crates-io))

(define-public crate-winver-0.1.0 (c (n "winver") (v "0.1.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_SystemInformation" "Win32_System_Com" "Win32_Security" "Win32_System_Wmi" "Win32_System_Rpc" "Win32_System_Ole"))) (k 0)))) (h "0namqvhgcj327mwfwyal9a46kildpz2nnrvwnmyrwrbxcs0xb4kg") (r "1.65.0")))

(define-public crate-winver-0.1.1 (c (n "winver") (v "0.1.1") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_SystemInformation" "Win32_System_Com" "Win32_Security" "Win32_System_Wmi" "Win32_System_Rpc" "Win32_System_Ole"))) (k 0)))) (h "1iiy5y4d6k4xn0blxn9sbxli5mzc07476b2qxhsmkczsixmr0k8h") (r "1.65.0")))

(define-public crate-winver-1.0.0 (c (n "winver") (v "1.0.0") (d (list (d (n "windows") (r "^0.48.0") (f (quote ("Win32_System_LibraryLoader" "Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_SystemInformation" "Win32_System_Com" "Win32_Security" "Win32_System_Wmi" "Win32_System_Rpc" "Win32_System_Ole"))) (k 0)))) (h "0cxbp7czv985rlca8j6q109b5gcl77lwqcm8l1szv0p2p5i723ly") (r "1.65.0")))

