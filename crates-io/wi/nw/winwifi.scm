(define-module (crates-io wi nw winwifi) #:use-module (crates-io))

(define-public crate-winwifi-0.1.0 (c (n "winwifi") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_NetworkManagement_WiFi"))) (d #t) (k 0)))) (h "05y5qc3wbmcivfs8j4jm54cbbpnra1pv6wql85y4q6n4xicc5hy6")))

(define-public crate-winwifi-0.1.0-rc1 (c (n "winwifi") (v "0.1.0-rc1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_NetworkManagement_WiFi"))) (d #t) (k 0)))) (h "151zs2yaar13d828ij8w2sp1brh2hdan8fd23cik8mnm8wv6rvka")))

