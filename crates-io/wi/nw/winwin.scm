(define-module (crates-io wi nw winwin) #:use-module (crates-io))

(define-public crate-winwin-0.0.1 (c (n "winwin") (v "0.0.1") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1l3q1gyggjrsr9chixa2mbvw7z47qcg5ms7cfybi6hsdchbqf20r")))

(define-public crate-winwin-0.0.2 (c (n "winwin") (v "0.0.2") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0crmxmkx5pqx7j199zrrwhr9d70a0rsmnf39c8g1i7v59xrav5yc")))

(define-public crate-winwin-0.0.3 (c (n "winwin") (v "0.0.3") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1h72qhvh4dhbxcbkps4f51wgwil1zn3qwmmx3789rclkv0dam3iy")))

(define-public crate-winwin-0.0.4 (c (n "winwin") (v "0.0.4") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "15n0iq0j86dci70hcp6g9nswhj5a0ncxn5abgryrfq435miz6nk3")))

(define-public crate-winwin-0.0.5 (c (n "winwin") (v "0.0.5") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Gdi" "Win32_System_LibraryLoader" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "06gwzjz93p91m2mssbnx698rlynvnxmqjczbsxmxf7zybprzwxv4")))

