(define-module (crates-io wi nw winwalk) #:use-module (crates-io))

(define-public crate-winwalk-0.1.0 (c (n "winwalk") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("fileapi" "handleapi" "timezoneapi"))) (d #t) (k 0)))) (h "0bwa70451af4imj6qpr8i4w1wim1zak8wrh3ny1l3zx401hvw6g4")))

(define-public crate-winwalk-0.2.0 (c (n "winwalk") (v "0.2.0") (d (list (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0z14h6v4l117jd8ls9hsln5w65x0hh555wpkgf6a7wsgkji5dx9s") (y #t)))

(define-public crate-winwalk-0.2.1 (c (n "winwalk") (v "0.2.1") (d (list (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0qa9cm5x4061j9wq4xvqaz7w1mbhbrqn45cd0jzg35w80n1mpvzv")))

(define-public crate-winwalk-0.2.2 (c (n "winwalk") (v "0.2.2") (d (list (d (n "divan") (r "^0.1.2") (d #t) (k 2)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0v52qsd440v16k4mrx5v80zxnm1zlsganrjpbg07m07rdn6jzq3v")))

