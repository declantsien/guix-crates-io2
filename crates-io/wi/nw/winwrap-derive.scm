(define-module (crates-io wi nw winwrap-derive) #:use-module (crates-io))

(define-public crate-winwrap-derive-0.1.0 (c (n "winwrap-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b44mk9282ya9r3h9c2if94bpydgks6fhhd4llk6iyzj5l86rsxc")))

