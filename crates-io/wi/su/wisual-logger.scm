(define-module (crates-io wi su wisual-logger) #:use-module (crates-io))

(define-public crate-wisual-logger-0.1.0 (c (n "wisual-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "13m4bvhny70nfmnf2xkx89q6kpmvwddr2pq03r53dcmiy95rzp89")))

(define-public crate-wisual-logger-0.1.1 (c (n "wisual-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0f6f85h2ykfwa49d5xi1kj77r4dhipvq9k0y416r1v0qhwgj4hwj")))

(define-public crate-wisual-logger-0.1.2 (c (n "wisual-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "01qq6lgr99fxc0l30bvk335i1skp8gk9ggm7432qwxxxn68y83ny")))

(define-public crate-wisual-logger-0.1.3 (c (n "wisual-logger") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0j6vj0kgxdzy1ivc4qd4gbhpk3md2vklk947in6ksg5z63hnj87v")))

(define-public crate-wisual-logger-0.1.4 (c (n "wisual-logger") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "02q1d7xdbasr26mahfc9i2094vzy8xdbylqwcr4y0q6zizjwdl79")))

