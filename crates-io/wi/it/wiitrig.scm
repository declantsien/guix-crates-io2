(define-module (crates-io wi it wiitrig) #:use-module (crates-io))

(define-public crate-wiitrig-0.0.1 (c (n "wiitrig") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gn3h3gkj9kvhwnm7r037ysyhbzhrgg6bcwy408k3bbws09bp4zz")))

