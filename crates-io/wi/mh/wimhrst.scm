(define-module (crates-io wi mh wimhrst) #:use-module (crates-io))

(define-public crate-wimhrst-0.1.0 (c (n "wimhrst") (v "0.1.0") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)))) (h "0yy3vj44s677ag2yp9dkf8b2qdhclp4q9xwsn451aabbda1jg7pn")))

(define-public crate-wimhrst-0.1.1 (c (n "wimhrst") (v "0.1.1") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0nmwf1xzgxdbq684bp6pkq3si99ggyizb997hj9y5zzdck1sl60a") (y #t)))

(define-public crate-wimhrst-0.1.2 (c (n "wimhrst") (v "0.1.2") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1j2rhkzc45bd3fkg0qrwd0hi6520x0mvys96dzaaby41xz3g5sy6")))

(define-public crate-wimhrst-1.0.0 (c (n "wimhrst") (v "1.0.0") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "1v4yf4nhxaljfww529849zmy9ar25qm8002jkq6mpph3ddcnksh2")))

