(define-module (crates-io wi nm winmix) #:use-module (crates-io))

(define-public crate-winmix-0.1.0 (c (n "winmix") (v "0.1.0") (d (list (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)) (d (n "windows-result") (r "^0.1.1") (d #t) (k 0)))) (h "1ndz48nqjfq4vl959bi6s6mvp6h3bn6yaavqpm8yalhypagh7xjb")))

(define-public crate-winmix-0.1.1 (c (n "winmix") (v "0.1.1") (d (list (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)) (d (n "windows-result") (r "^0.1.1") (d #t) (k 0)))) (h "0zc1pd58mj1rryglc9aj7780h6ivhqc3yxbhqpm2nbrly4vk8i05")))

(define-public crate-winmix-0.1.2 (c (n "winmix") (v "0.1.2") (d (list (d (n "windows") (r "^0.56.0") (f (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)) (d (n "windows-result") (r "^0.1.1") (d #t) (k 0)))) (h "1bczz1mp1fvr6d9c2b9rals4g0hrlpfcy4vy59smhhb3l3k3a579")))

