(define-module (crates-io wi nm winmd-macros) #:use-module (crates-io))

(define-public crate-winmd-macros-0.2.0 (c (n "winmd-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sbk5pcq3330nihmhjxn8dfffds6lxfw48a70c4x5mccl4130sf2")))

(define-public crate-winmd-macros-0.3.0 (c (n "winmd-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h7nz6d43ph853abvs80pfqj4xzcv19xgq6r9h063f7xv0fzk11k")))

(define-public crate-winmd-macros-0.4.0 (c (n "winmd-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14bp0libir8a25x81nqckp4rs2bl6wzv1m6il55qg72arjcx8c0y")))

(define-public crate-winmd-macros-0.5.0 (c (n "winmd-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "134ysjz5bqhbkb2li2x2yvpbpw2zhmf5gmbknk61q53vm620dd5k")))

(define-public crate-winmd-macros-0.6.0 (c (n "winmd-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x3rb165l39wzd5yb2bkqil55yfs0afw45cqnzsgklalhnc8iq24")))

