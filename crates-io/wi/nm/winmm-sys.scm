(define-module (crates-io wi nm winmm-sys) #:use-module (crates-io))

(define-public crate-winmm-sys-0.0.1 (c (n "winmm-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "143kb47n94vhqrk9gzvy1v438l00kp2l6jw9yb4dainswndxwlbf")))

(define-public crate-winmm-sys-0.0.2 (c (n "winmm-sys") (v "0.0.2") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "08hy1bhx7hd4rlgq4jhxac805my78hy36dmq9ip102b8rx72z6p3")))

(define-public crate-winmm-sys-0.1.0 (c (n "winmm-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0ns0w2ds2bkjaidgcs5d54kmhd9ki57cy69p5z3djccrmvygi6im")))

(define-public crate-winmm-sys-0.1.1 (c (n "winmm-sys") (v "0.1.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "1ains62pflbn7gjfyhn27jxi7538lyv89r2gq20wij2ihpa1gm9p")))

(define-public crate-winmm-sys-0.2.0 (c (n "winmm-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.4") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "0db0hqkic11mc5a5parh7avw84xy6k1v8w7c3brlpjk3df0pm990")))

