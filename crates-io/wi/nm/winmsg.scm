(define-module (crates-io wi nm winmsg) #:use-module (crates-io))

(define-public crate-winmsg-0.1.0 (c (n "winmsg") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)))) (h "02xnhm2ww6b554n4fqi0gi3q8m2b31yy38blqjac3ar8jidpkff7")))

(define-public crate-winmsg-0.1.1 (c (n "winmsg") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0b5f79xhhl89k263cxp222qj7nr372nxxcqsc6ng3634hvfiac08")))

(define-public crate-winmsg-0.1.2 (c (n "winmsg") (v "0.1.2") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1qdprpg26mjr1r7iklk0d996401b6850an4gm67qz9hazzkhh0hl")))

