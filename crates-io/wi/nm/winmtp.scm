(define-module (crates-io wi nm winmtp) #:use-module (crates-io))

(define-public crate-winmtp-0.1.0 (c (n "winmtp") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (d #t) (k 0)))) (h "0pn137wkpz0lv1lfqqz52ph85c1k9w2953dxils27ww5m15v6yzb")))

(define-public crate-winmtp-0.2.0 (c (n "winmtp") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (d #t) (k 0)))) (h "15ks8v2ldsy6kz6ajyjaqpjjrawwin64w797czmrgbl31yr2i9v2")))

(define-public crate-winmtp-0.2.1 (c (n "winmtp") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (d #t) (k 0)))) (h "1ra155k90044qjdpxx39bd5v905fx3n2wwzvzc9yj89c9912dqmx")))

(define-public crate-winmtp-0.3.0 (c (n "winmtp") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Win32_Foundation"))) (d #t) (k 0)))) (h "0i04bky13bwa7d78jjzayly84lv8gg736kj27kfa0l5ydl1nqmag")))

