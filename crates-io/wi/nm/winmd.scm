(define-module (crates-io wi nm winmd) #:use-module (crates-io))

(define-public crate-winmd-0.1.0 (c (n "winmd") (v "0.1.0") (h "0wj5cxg9x24sh6pqaw0a2290js80pgrfz590x62slplq37mxns5f") (y #t)))

(define-public crate-winmd-0.2.0 (c (n "winmd") (v "0.2.0") (d (list (d (n "winmd-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1kli4cvy4zl1cgfhdknjwq70k1hn0rhcdpcj7c2wnbx4ck892b01")))

(define-public crate-winmd-0.3.0 (c (n "winmd") (v "0.3.0") (d (list (d (n "winmd-macros") (r "^0.3.0") (d #t) (k 0)))) (h "13vgdnn05j4w7zylp68hljm78szmppmh6ly2hiyprznk57083705")))

(define-public crate-winmd-0.4.0 (c (n "winmd") (v "0.4.0") (d (list (d (n "winmd-macros") (r "^0.4.0") (d #t) (k 0)))) (h "0nacnynbkcap572kycscxqjv4ivpm1b9j6qf8vg1bhvf4zmnx5pw")))

(define-public crate-winmd-0.5.0 (c (n "winmd") (v "0.5.0") (d (list (d (n "winmd-macros") (r "^0.5.0") (d #t) (k 0)))) (h "1pzz2hvx6qv8q91445x89z8h5j4ngz88z083r4cdj9z8rxbcag26")))

(define-public crate-winmd-0.6.0 (c (n "winmd") (v "0.6.0") (d (list (d (n "winmd-macros") (r "^0.6.0") (d #t) (k 0)))) (h "00qnw4wy872syb2a6l9i9yb4z7n82gs5cx5bqf4xmzh9pz8lvdqr")))

