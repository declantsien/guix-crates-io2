(define-module (crates-io wi nx winx) #:use-module (crates-io))

(define-public crate-winx-0.1.0 (c (n "winx") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0rrk4zpih7px9ncg16zj075ifsd3vm7dvsdv5sf0vx8rbqzq9f85")))

(define-public crate-winx-0.2.0 (c (n "winx") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0l6yzmhx35lq5sj434xfvmzf6n7r64py3fal2aaqsjss5zgf3di9")))

(define-public crate-winx-0.3.0 (c (n "winx") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "16g589xa5fb310jkv1dim5s3c24f1144cgzgnqjlcr71fvmz7ib7")))

(define-public crate-winx-0.4.0 (c (n "winx") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "00nc9y9rl4jhp1as0qa3wdbd99ivdvnpw355k2zsp35kl7k4sgz9")))

(define-public crate-winx-0.5.0 (c (n "winx") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0xpwavv6vk1hrdr07c71qjlwbwjma1b6rcvl4znq3j1b594lhpxy")))

(define-public crate-winx-0.6.0 (c (n "winx") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0rdnp1csw9zzlbqgfw2vddhzvi36vqlhf1v8lmdd0s142a4c3h6l")))

(define-public crate-winx-0.7.0 (c (n "winx") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "148v8anw03bd0zg8g6sqdk0c8yp6yhlnk5a04g04d8i8vkb2alf9")))

(define-public crate-winx-0.8.0 (c (n "winx") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "1hai50j7204lw4r4lmy5g6rn58kia9b8rk8b8r609qykw7dsjrfb")))

(define-public crate-winx-0.9.0 (c (n "winx") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "1lvm7gmdzkcs4g4nyicrcwmgg4xjxhwi804y2c7q41vf7inp0iwz")))

(define-public crate-winx-0.10.0 (c (n "winx") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "1bd0mfwxq7aswqa8jbwwpyn7ahnpz31dig0hc134h9awxyi8n2g1")))

(define-public crate-winx-0.11.0 (c (n "winx") (v "0.11.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "07lshp791af90d4psysr9ch1b3yqqqhkwhngdbd9a93zmkkv643y")))

(define-public crate-winx-0.12.0 (c (n "winx") (v "0.12.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "17l0yca3hiy3fzxfr121qp1s2vga5fxpqny9ir46p64bcqvq5s95")))

(define-public crate-winx-0.13.0 (c (n "winx") (v "0.13.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "008vl96img2kb81dfbrzr7ii3wwcm4hxv6nz80lmvgl7wkwqcsib")))

(define-public crate-winx-0.14.0 (c (n "winx") (v "0.14.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "1zyvshgld19gzg1jkz2pqfx15983pn2x5qaxx8ghlqg2ljiq8ra8")))

(define-public crate-winx-0.15.0 (c (n "winx") (v "0.15.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0nzlnnll2c2rmyj3p12jqx0633gyb4kqy4di8kkxwzzs34wyjhf9")))

(define-public crate-winx-0.16.0 (c (n "winx") (v "0.16.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "12nlg1s9r9ilwkbp55f4bqac3jg1qmx9384z53l6x1pv8yf1m5mx")))

(define-public crate-winx-0.17.0 (c (n "winx") (v "0.17.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "14axfavm1cpdqzb5mfa7vhmazm4na4301f8k7ih48d2p949cic4i")))

(define-public crate-winx-0.18.0 (c (n "winx") (v "0.18.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0nw95vcwfad4m0hhzq2f51jx10bis1vsvgjpy3kwynaq0cisdidl")))

(define-public crate-winx-0.19.0 (c (n "winx") (v "0.19.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0g30gqfzdlqnbl6ilmgmwavvwlb1csn29p5y8pqygg96iq8pyzn2")))

(define-public crate-winx-0.20.0 (c (n "winx") (v "0.20.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0rn9q2rnh7zb1p42g445c10yg73i2p9lx5w7y7szgwpjfgillpmj")))

(define-public crate-winx-0.21.0 (c (n "winx") (v "0.21.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "0pvvqbdqbb6p63vmkw9d9z24850i8xrc7bg8s72rbimxgnmzzjib")))

(define-public crate-winx-0.22.0 (c (n "winx") (v "0.22.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cvt") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "errhandlingapi" "handleapi" "processthreadsapi" "profileapi" "securitybaseapi" "winbase" "winerror" "ws2def" "fileapi" "aclapi"))) (d #t) (k 0)))) (h "04m5fkwi1s2wfzmgxmmkiwsz5a41vzlvf66sv3wxmq67wiv53lzp")))

(define-public crate-winx-0.23.0 (c (n "winx") (v "0.23.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "profileapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "0gliyax4384fpl9pv4b40xls94c629w9szy368p0dkdch4k4c5m3")))

(define-public crate-winx-0.24.0 (c (n "winx") (v "0.24.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "profileapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "07i2w9grf6xlj6v6nnckvvplrxal2hifw8j83wv13nvd51g6yzym")))

(define-public crate-winx-0.25.0 (c (n "winx") (v "0.25.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0h8k90rjcvadbnq0pbj3yr0jm51gqww9rfb3x04qzjas5bhpknrb")))

(define-public crate-winx-0.26.0 (c (n "winx") (v "0.26.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1zd7r7lxr1g9ibfvxyzhv95i33c4skzy8s68k2d15n66b5yaaszg")))

(define-public crate-winx-0.27.0 (c (n "winx") (v "0.27.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "03h56k01n9fzkkvgwa1zhvfsv0vn9b7l97xif7hd3vngc6psd36c")))

(define-public crate-winx-0.28.0 (c (n "winx") (v "0.28.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.2.3") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "169qfs4yyzca6ybcrj3nb8bp5bz85gbc61x66zil8ifjavzfhnw7")))

(define-public crate-winx-0.29.0 (c (n "winx") (v "0.29.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1w3zhdmfdqz6lkhcjnzn9y32jc85zvazyzk0p2zz64glq5haarh6")))

(define-public crate-winx-0.29.1 (c (n "winx") (v "0.29.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0a49l2w1qfmng1ppg4a7652qn7b9kam39fvbpf8pl43p81digkaf")))

(define-public crate-winx-0.29.2 (c (n "winx") (v "0.29.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.3.0") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0dqicxpa5sz1w6iv0m2h4xamgpdr4khn3spw68y98l0ssj8hifmg")))

(define-public crate-winx-0.30.0 (c (n "winx") (v "0.30.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.4.0") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0aiwbcx5lam114m0lz7mpsdpd0gmn7i5jy4w2ag87rd3ab7f4kf5")))

(define-public crate-winx-0.31.0 (c (n "winx") (v "0.31.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.5.1") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1vdxmqhmxxxgwk0cx8axh0lwp2a8w6xj6zmd0dysg56dp0y9gm88")))

(define-public crate-winx-0.32.0 (c (n "winx") (v "0.32.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.6.0") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "handleapi" "ioapiset" "profileapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0bja6sfzd0dp0iqnqvhzn16ibphndqs70m4nniyr2ddivizvyfd1")))

(define-public crate-winx-0.33.0 (c (n "winx") (v "0.33.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^0.7.0") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0k85x0wlrdj7k9s0ph0i2nflcrybhszwxj0q8lx2dswh0c0ixc5p")))

(define-public crate-winx-0.34.0-rc1 (c (n "winx") (v "0.34.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.0-rc1") (k 0)) (d (n "windows-sys") (r "^0.36.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06ghi9awc61w2bvzjwqav694vd7k977mrvw1wnwhpr2clzs16v4n")))

(define-public crate-winx-0.34.0 (c (n "winx") (v "0.34.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b51ghbckckzmdnjfphs9k9l3332srravmpspf2dwh484c76kbwv")))

(define-public crate-winx-0.34.1 (c (n "winx") (v "0.34.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j5fc05kky02649ipdlhlqk68rsaj9aqj89n0bj2grrsv1pabnjv")))

(define-public crate-winx-0.35.0 (c (n "winx") (v "0.35.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "081ma5if06r538hxjrq9pia09br50nf3r7cx4cd5wlvxjgpdi70j")))

(define-public crate-winx-0.35.1 (c (n "winx") (v "0.35.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "io-lifetimes") (r "^1.0.0") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q79da3fqngkwz1pzd78lpnmfxfbys15v6iabw6k5ygvy0hs2lhw")))

(define-public crate-winx-0.36.0 (c (n "winx") (v "0.36.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1k81zzxmjfbybc7pyfkb4lb11axjhcqy7hhmmx58zyj2cm39j8b0")))

(define-public crate-winx-0.36.1 (c (n "winx") (v "0.36.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y7x2gl4b2xc7spql1myc5k1nkf5cnqf58w2cyxr1xkihggwwms8")))

(define-public crate-winx-0.36.2 (c (n "winx") (v "0.36.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1rc19ws9z56ks6r5jnpfj3gs06xqa2q688h57gw33x9djgibhyrm") (r "1.63")))

(define-public crate-winx-0.36.3 (c (n "winx") (v "0.36.3") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Ioctl" "Win32_System_LibraryLoader" "Win32_System_Performance" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ikk6cnlfiq7dm36jqwrczxa8jyh9m2zmrdbxi3d430cha1knr7r") (r "1.63")))

