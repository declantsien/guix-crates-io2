(define-module (crates-io wi tc witc-abi) #:use-module (crates-io))

(define-public crate-witc-abi-0.1.0 (c (n "witc-abi") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wasmedge-sdk") (r "^0.7.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0md8wxzncgrdgc79sg4wm0yq2qvg3vv3a1i8rp4k3gw9gl8gmdf5")))

(define-public crate-witc-abi-0.2.0 (c (n "witc-abi") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmedge-sdk") (r "^0.7.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1j3zgw8f6znx8fivkjd9jmx5r1wjd9m161j0lk932wplv8v54sak")))

(define-public crate-witc-abi-0.2.1 (c (n "witc-abi") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmedge-sdk") (r "^0.7.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0r3yn139jvsrvr31z4ldl50qrkd9djhnsj2vd7kwah2kmka8nr5s")))

(define-public crate-witc-abi-0.3.0 (c (n "witc-abi") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmedge-sdk") (r "^0.8.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0j63lrwiv5zq4pdbg02ry8w9178vvs2cb5fwnyly136591fvagyl")))

(define-public crate-witc-abi-0.3.1 (c (n "witc-abi") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmedge-sdk") (r "^0.8.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1d6m0ahvn0d6q7ss3gziyjxlw06a7am6fwd1adq369gzm3d8yz8r")))

