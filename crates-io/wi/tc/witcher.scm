(define-module (crates-io wi tc witcher) #:use-module (crates-io))

(define-public crate-witcher-0.0.2 (c (n "witcher") (v "0.0.2") (h "0qkh34dpp56hzshvxchfk2h8vf2910i251g1sr7qjzfnicbsf8ax")))

(define-public crate-witcher-0.1.2 (c (n "witcher") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "12f6wmyyrzmm8y4qndr4px824cfnilq7hvcl4rrgaml3i6p3bzc0")))

(define-public crate-witcher-0.1.3 (c (n "witcher") (v "0.1.3") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "19llqyvcfd510qiynr256p6bcrjv8sbidnjr4z6k8zqvc2rqf8z3")))

(define-public crate-witcher-0.1.4 (c (n "witcher") (v "0.1.4") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (k 0)))) (h "04wxwmagy63lad4sva73s41gvglg34kn7pvq3c01a1kapfvhh8xw")))

(define-public crate-witcher-0.1.7 (c (n "witcher") (v "0.1.7") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "0z8hpvyv1k2xmsvvfphb0d4jbq2idckmwrd0q2d29rysns96r0dh")))

(define-public crate-witcher-0.1.8 (c (n "witcher") (v "0.1.8") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "17bhw9c0v2cqmkfs6rz1wc3qmbvai4cxyqwq732kq99lia9i9icv")))

(define-public crate-witcher-0.1.9 (c (n "witcher") (v "0.1.9") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "08fzfalh4995065b656ha0ygbapnsnv4zwkirnwipz0n771600dc")))

(define-public crate-witcher-0.1.10 (c (n "witcher") (v "0.1.10") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "0946ig5380hbgj9yxy55w6h59k7cccr7qdsgfz64wa3vjjczfm4v")))

(define-public crate-witcher-0.1.11 (c (n "witcher") (v "0.1.11") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "0k3kv79zcgd33i5j5lh7zh43pnn01lhdw4dx4k09c7d5xkxk8v65")))

(define-public crate-witcher-0.1.12 (c (n "witcher") (v "0.1.12") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "05vyl1fmjdc872pypcgs2jgr7h7gg5b3jbqxx9ca5zqxfdscih4d")))

(define-public crate-witcher-0.1.19 (c (n "witcher") (v "0.1.19") (d (list (d (n "backtrace") (r "0.3.*") (d #t) (k 0)) (d (n "gory") (r "0.1.*") (d #t) (k 0)))) (h "1lkysrrjrcdsh35m23i2mizf9dqi52p35b2f36iwig3nap4wwa0w")))

