(define-module (crates-io wi nr winresult-types) #:use-module (crates-io))

(define-public crate-winresult-types-0.1.0 (c (n "winresult-types") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cy5q188jxkhy3qdpv2ag2db228gr6xfpmwxq6nxfc88zzxkz73n")))

(define-public crate-winresult-types-0.1.2 (c (n "winresult-types") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cxgqc04lyqn45m4w7nqa1sss93qjpqj2j7kmlm46qbdm4wkpk4x")))

(define-public crate-winresult-types-0.1.3 (c (n "winresult-types") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s71alcxsaypwz4qnd9d73kng6cifb8ga00l110wrdwrxpa4ycf7")))

