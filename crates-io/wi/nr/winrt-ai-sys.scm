(define-module (crates-io wi nr winrt-ai-sys) #:use-module (crates-io))

(define-public crate-winrt-ai-sys-0.0.0 (c (n "winrt-ai-sys") (v "0.0.0") (h "18vhibx6wv4k612kjq7sdl553s4pgk5pqmiad1hkk1zmxig6dh3z") (r "1.46")))

(define-public crate-winrt-ai-sys-0.0.1 (c (n "winrt-ai-sys") (v "0.0.1") (h "0jw9ffc1nyknw5qbv2jh83sakzxq4adlmhm5yrj3xryysv229d75") (r "1.46")))

(define-public crate-winrt-ai-sys-0.0.2 (c (n "winrt-ai-sys") (v "0.0.2") (h "0ysp8s4qma2q8di0iz9xhk7xki4c7vfcly0iisd0rch8ibpcin59") (r "1.46")))

(define-public crate-winrt-ai-sys-0.23.0 (c (n "winrt-ai-sys") (v "0.23.0") (h "1fcmzknvd35fascm1djxksj77bw8jcflm5jcs9lvrk6g2wv2c68l")))

