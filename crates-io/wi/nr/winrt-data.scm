(define-module (crates-io wi nr winrt-data) #:use-module (crates-io))

(define-public crate-winrt-data-0.0.0 (c (n "winrt-data") (v "0.0.0") (h "1wryqp6ch23n1d9cks9rabk5sv1gxy01cr8g3054pdg7nby30pz9") (r "1.46")))

(define-public crate-winrt-data-0.0.1 (c (n "winrt-data") (v "0.0.1") (h "0mp21gbcnxpd3h6zp2w66ljk7gp7fwqswxinwmbviqznssx14gpw") (r "1.46")))

(define-public crate-winrt-data-0.0.2 (c (n "winrt-data") (v "0.0.2") (h "0dk6mac3i6sqlav3944vbzy3s0q117m1zdk5xand39j2j2nrri18") (r "1.46")))

(define-public crate-winrt-data-0.23.0 (c (n "winrt-data") (v "0.23.0") (h "1jgmzriw9mr23k7fjq5xkf3qsfnfj073k06cab1lnwxryrx12zx5")))

