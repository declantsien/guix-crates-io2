(define-module (crates-io wi nr winr) #:use-module (crates-io))

(define-public crate-winr-0.0.0 (c (n "winr") (v "0.0.0") (d (list (d (n "eventify") (r "^0.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "windows") (r "^0.52.0") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1iczpcw5ya6vb8r558c4x50vpg4yshdc57vypb197h17dc6258xa")))

