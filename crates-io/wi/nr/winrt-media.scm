(define-module (crates-io wi nr winrt-media) #:use-module (crates-io))

(define-public crate-winrt-media-0.0.1 (c (n "winrt-media") (v "0.0.1") (h "0iaiq71m6p9png3lmy51gwy97rnjbc8lkrlv21b1bq4kb1iknvnh") (r "1.46")))

(define-public crate-winrt-media-0.0.2 (c (n "winrt-media") (v "0.0.2") (h "0k2vvlib61yfn1q41va7vq2ij5gj07f7n6zm1ssd9n9pclci4qwr") (r "1.46")))

(define-public crate-winrt-media-0.23.0 (c (n "winrt-media") (v "0.23.0") (h "1rgkrc976xmppya93fkg1jzr6hh66c5rjn66anijfwh29qcmmngy")))

