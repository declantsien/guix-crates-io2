(define-module (crates-io wi nr winrt-applicationmodel-sys) #:use-module (crates-io))

(define-public crate-winrt-applicationmodel-sys-0.0.0 (c (n "winrt-applicationmodel-sys") (v "0.0.0") (h "0x1r2kq328zk74cdsgz77cwnq284nc19kl2v0vymmh4dcpa3l8ky") (r "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.0.1 (c (n "winrt-applicationmodel-sys") (v "0.0.1") (h "082hpx8a9c1q6nmda1pfp30dqfmh1aaj5ynjml5r4v0i129xh1bg") (r "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.0.2 (c (n "winrt-applicationmodel-sys") (v "0.0.2") (h "1jddkkxrz0lv9sf9qgj0w988f32hc9sd9q719mzcgrv2ar6k9gpg") (r "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.23.0 (c (n "winrt-applicationmodel-sys") (v "0.23.0") (h "1f3yc2sz1m53vn46qggkgzkavky161jzyxgqhd305ga4af1skwgp")))

