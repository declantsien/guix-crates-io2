(define-module (crates-io wi nr winrt-graphics) #:use-module (crates-io))

(define-public crate-winrt-graphics-0.0.1 (c (n "winrt-graphics") (v "0.0.1") (h "1hzh8q6jdp1dff9sr73ci43gqbv72w386mkgdy2fxc3y83a5i76v") (r "1.46")))

(define-public crate-winrt-graphics-0.0.2 (c (n "winrt-graphics") (v "0.0.2") (h "1ws0fkj1zr0ashmm7qcbxjdy3ncb8q2bh0gxmnqyla1mvhqqv8sv") (r "1.46")))

(define-public crate-winrt-graphics-0.23.0 (c (n "winrt-graphics") (v "0.23.0") (h "1h0ll3krc2v8bn730dsrwwbx0vxskgybbbjzmkn0ml7lg1v10g5a")))

