(define-module (crates-io wi nr winrt-system-sys) #:use-module (crates-io))

(define-public crate-winrt-system-sys-0.0.1 (c (n "winrt-system-sys") (v "0.0.1") (h "02av0f30zjlnvd9fnwb3nz2lkjdhfh94k9c7yn0gjyin9b786p0y") (r "1.46")))

(define-public crate-winrt-system-sys-0.0.2 (c (n "winrt-system-sys") (v "0.0.2") (h "0snl7zi51rzkxpgwx2yrw1xqm8b5lnzwmsl34krvv0fzc56akhkx") (r "1.46")))

(define-public crate-winrt-system-sys-0.23.0 (c (n "winrt-system-sys") (v "0.23.0") (h "14ss8x0v0wj1p6qldvks5r4bmf84rbr110f5r7vx5nwk9anavfvh")))

