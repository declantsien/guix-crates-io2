(define-module (crates-io wi nr winrt-ai) #:use-module (crates-io))

(define-public crate-winrt-ai-0.0.0 (c (n "winrt-ai") (v "0.0.0") (h "05lkil5y13407baiqz6zscak1findxvjj0gjfq78n3qil8h47ybi") (r "1.46")))

(define-public crate-winrt-ai-0.0.1 (c (n "winrt-ai") (v "0.0.1") (h "0kcdkja4mg2f9jfff87ilhs7v4vqqlmb979ccwjcscygvvs9l6g5") (r "1.46")))

(define-public crate-winrt-ai-0.0.2 (c (n "winrt-ai") (v "0.0.2") (h "1v5nk5m0f72x1nvz4ckgx2p631icms0dq3qxmvmc77afg93253wi") (r "1.46")))

(define-public crate-winrt-ai-0.23.0 (c (n "winrt-ai") (v "0.23.0") (h "0b6bhsclpmafa2d202237gl1iz77pw1f7l3xw2bh1rk0pci52n57")))

