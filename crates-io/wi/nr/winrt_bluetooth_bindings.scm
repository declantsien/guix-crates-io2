(define-module (crates-io wi nr winrt_bluetooth_bindings) #:use-module (crates-io))

(define-public crate-winrt_bluetooth_bindings-0.0.1 (c (n "winrt_bluetooth_bindings") (v "0.0.1") (d (list (d (n "winrt") (r "^0.7.2") (d #t) (k 0)))) (h "1q9gzw6c226bmqmxr86pkb8ld96fzmzz37va8hdjcr5iar509p6r")))

(define-public crate-winrt_bluetooth_bindings-0.0.2 (c (n "winrt_bluetooth_bindings") (v "0.0.2") (d (list (d (n "winrt") (r "^0.7.2") (d #t) (k 0)))) (h "06axxfh1qhp8q68mr6g4r4j1zwvkw0c6cfmcp8iijs2340vdz8sc")))

(define-public crate-winrt_bluetooth_bindings-0.0.3 (c (n "winrt_bluetooth_bindings") (v "0.0.3") (d (list (d (n "winrt") (r "^0.7.2") (d #t) (k 0)))) (h "02w9j7plmx4wlkd1p68h1krvqz98bhs0irbfg9j1qin3yhb8j6mn")))

(define-public crate-winrt_bluetooth_bindings-0.0.4 (c (n "winrt_bluetooth_bindings") (v "0.0.4") (d (list (d (n "winrt") (r "^0.7.2") (d #t) (k 0)))) (h "1z3yc7nkfaxkppymrjdy9fp93f5skb82zy09d1yk9531sk3vdc2s")))

