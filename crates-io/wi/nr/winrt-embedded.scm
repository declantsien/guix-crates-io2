(define-module (crates-io wi nr winrt-embedded) #:use-module (crates-io))

(define-public crate-winrt-embedded-0.0.0 (c (n "winrt-embedded") (v "0.0.0") (h "1cqzzy4sqr3ly2g8zi2awd8nzp0gpn9p583rg42hf759w642ndb5") (r "1.46")))

(define-public crate-winrt-embedded-0.0.1 (c (n "winrt-embedded") (v "0.0.1") (h "110b8gs61nywyd4dbaz1j363f8lby1shih5w60a93x6mrimqp3zz") (r "1.46")))

(define-public crate-winrt-embedded-0.0.2 (c (n "winrt-embedded") (v "0.0.2") (h "126ydan4lkm7pvg140l0y21n1whq0wsh2wcmjc1x2d9jr9ymmlx7") (r "1.46")))

(define-public crate-winrt-embedded-0.23.0 (c (n "winrt-embedded") (v "0.23.0") (h "16nc111fmd9h5lrivzwmwfjr5bs6a56hcd3097ig2ckaxs1470zm")))

