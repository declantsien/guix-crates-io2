(define-module (crates-io wi nr winrt-web-sys) #:use-module (crates-io))

(define-public crate-winrt-web-sys-0.0.1 (c (n "winrt-web-sys") (v "0.0.1") (h "1fdvb5w2x4gs453h9il6d05n8xr5s2k9hb7kpxbqwm8f33vv952m") (r "1.46")))

(define-public crate-winrt-web-sys-0.0.2 (c (n "winrt-web-sys") (v "0.0.2") (h "0vdvl0ns21gz9s9g496s116gr8zrkcifs9bq6jh8v34rwa6ab55s") (r "1.46")))

(define-public crate-winrt-web-sys-0.23.0 (c (n "winrt-web-sys") (v "0.23.0") (h "1igp0dnrsd0yr3zvjxsi55wr2p84cbfpvmnmbyavqgq6w8ighprj")))

