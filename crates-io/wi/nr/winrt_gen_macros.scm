(define-module (crates-io wi nr winrt_gen_macros) #:use-module (crates-io))

(define-public crate-winrt_gen_macros-0.7.0 (c (n "winrt_gen_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwi1mxxcpyw9113dis7yw0hdzhh1qd1amnydyia0dbwx7cf5gkq")))

(define-public crate-winrt_gen_macros-0.7.1 (c (n "winrt_gen_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyz80zpwfqjnhp5d0yg2b23v8hf081aska084idqxahc7i82ysy")))

(define-public crate-winrt_gen_macros-0.7.2 (c (n "winrt_gen_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n5pg0ndfxsqdq56lqrz7v6086dcmyvp4ckzkcymzwzhngmpikc5")))

