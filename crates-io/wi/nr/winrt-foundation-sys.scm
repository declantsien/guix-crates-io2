(define-module (crates-io wi nr winrt-foundation-sys) #:use-module (crates-io))

(define-public crate-winrt-foundation-sys-0.0.1 (c (n "winrt-foundation-sys") (v "0.0.1") (h "0vh4c4jbagpr267f645z3w0m83agp3jqm2z5r8gry4s5xnlnwcy8") (r "1.46")))

(define-public crate-winrt-foundation-sys-0.0.2 (c (n "winrt-foundation-sys") (v "0.0.2") (h "0jv33xkf4y4l8klwd9vl36663xls8fwyxcv0zy81f7xf94vwj6aq") (r "1.46")))

(define-public crate-winrt-foundation-sys-0.23.0 (c (n "winrt-foundation-sys") (v "0.23.0") (h "0rjykpjxb9lf8rxdi9hjdzp57vi1s4l6k7b0cfh3n0sdhg96bad0")))

