(define-module (crates-io wi nr winrt-web) #:use-module (crates-io))

(define-public crate-winrt-web-0.0.1 (c (n "winrt-web") (v "0.0.1") (h "1bbyx69wrn86q1jl1g07s3ll2wvxns74xk54v4b0i8ggz6mczxbg") (r "1.46")))

(define-public crate-winrt-web-0.0.2 (c (n "winrt-web") (v "0.0.2") (h "0v1llc3f4kz7p2adg785s885hwpvwq96j695rwjciwayn24bw0kb") (r "1.46")))

(define-public crate-winrt-web-0.23.0 (c (n "winrt-web") (v "0.23.0") (h "08hp8n743rg7hy82rsy8q7zaryg8a69kw5750awg69i7qqz9lff0")))

