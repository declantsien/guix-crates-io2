(define-module (crates-io wi nr winrt-management-sys) #:use-module (crates-io))

(define-public crate-winrt-management-sys-0.0.1 (c (n "winrt-management-sys") (v "0.0.1") (h "06l9b09gkqy2g6q51fh5ql1znis72javrsxxlwj7slmxnwnvv9h9") (r "1.46")))

(define-public crate-winrt-management-sys-0.0.2 (c (n "winrt-management-sys") (v "0.0.2") (h "11l1hksd7lh91syr29ajhjqkjrcykmgf2fbi4zhd81slhq8krw6w") (r "1.46")))

(define-public crate-winrt-management-sys-0.23.0 (c (n "winrt-management-sys") (v "0.23.0") (h "1bnalv014spgn14lcvdaxyami0i2ljh3m4qbzqz2fa8i77wrwm4r")))

