(define-module (crates-io wi nr winrt-ui) #:use-module (crates-io))

(define-public crate-winrt-ui-0.0.1 (c (n "winrt-ui") (v "0.0.1") (h "1vjgpmwjv918zcniqs09356gwhn6p73992g5mxlh8g6fws5dkkbs") (r "1.46")))

(define-public crate-winrt-ui-0.0.2 (c (n "winrt-ui") (v "0.0.2") (h "19k67jnc6zi7lnxj8ba69xajzwvp1ql1lza1ygdrdb8wngxq5j5j") (r "1.46")))

(define-public crate-winrt-ui-0.23.0 (c (n "winrt-ui") (v "0.23.0") (h "19fyj62svcndc6573nr9zwg5kqpvg5gl32va7s2mmsq8ync70l1d")))

