(define-module (crates-io wi nr winrt-foundation) #:use-module (crates-io))

(define-public crate-winrt-foundation-0.0.1 (c (n "winrt-foundation") (v "0.0.1") (h "1m70sfw2p3zv39nvqnbjnj8rx9gpahkxvyyffqn38k7c58cdm4sh") (r "1.46")))

(define-public crate-winrt-foundation-0.0.2 (c (n "winrt-foundation") (v "0.0.2") (h "1997dj2001lyk2zcbdd6aqzpj372qybwfwb84wv1x1lspig6lmmr") (r "1.46")))

(define-public crate-winrt-foundation-0.23.0 (c (n "winrt-foundation") (v "0.23.0") (h "0nyjzd9vl22pjkl5kxh8yl3gy2h5rgfd93ic1dwffm1r6mk7ajms")))

