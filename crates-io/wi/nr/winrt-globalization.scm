(define-module (crates-io wi nr winrt-globalization) #:use-module (crates-io))

(define-public crate-winrt-globalization-0.0.1 (c (n "winrt-globalization") (v "0.0.1") (h "1mdi12ysxdbi8p78rci2fiqfvy89yhk58gq247jv64rdkbfihn1b") (r "1.46")))

(define-public crate-winrt-globalization-0.0.2 (c (n "winrt-globalization") (v "0.0.2") (h "04i5bncnim6z0bm91si7s5nygd390wdyrs3c7szij8z5s4cpfkri") (r "1.46")))

(define-public crate-winrt-globalization-0.23.0 (c (n "winrt-globalization") (v "0.23.0") (h "0i7vnpn3mjvwxk9cgfz2gyjzfki2h5cnhdxkw14gign59v94lbb8")))

