(define-module (crates-io wi nr winrt-networking-sys) #:use-module (crates-io))

(define-public crate-winrt-networking-sys-0.0.1 (c (n "winrt-networking-sys") (v "0.0.1") (h "0y0wmsp3nn2iwpw5csihcrblw9dfbk6bx8srnmd01l55yzs2gnc6") (r "1.46")))

(define-public crate-winrt-networking-sys-0.0.2 (c (n "winrt-networking-sys") (v "0.0.2") (h "0hswywf7wlw68c5czf9ibsh6j39akph8jzxgfmii2xhg7hyh3xrq") (r "1.46")))

(define-public crate-winrt-networking-sys-0.23.0 (c (n "winrt-networking-sys") (v "0.23.0") (h "07j3y3xa3iq7zy0bqva27kbaw64c18sgqh7ihw796yyg85h3y0i0")))

