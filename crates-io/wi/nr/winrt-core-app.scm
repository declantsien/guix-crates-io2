(define-module (crates-io wi nr winrt-core-app) #:use-module (crates-io))

(define-public crate-winrt-core-app-0.4.0 (c (n "winrt-core-app") (v "0.4.0") (d (list (d (n "atomic") (r "^0.4.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5.1") (d #t) (k 1)) (d (n "winrt") (r "^0.6.0") (f (quote ("windows-applicationmodel" "windows-ui"))) (d #t) (k 0)))) (h "1da836b80irpib2jnc2qnjzjvdasgzhh48gzqzjbggqr9i342s4x")))

