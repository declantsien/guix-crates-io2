(define-module (crates-io wi nr winrt-devices-sys) #:use-module (crates-io))

(define-public crate-winrt-devices-sys-0.0.0 (c (n "winrt-devices-sys") (v "0.0.0") (h "0pm3hxik88xcnic1xy40l14ar93b6g1bbsmcnz37dfqrp3jz65ss") (r "1.46")))

(define-public crate-winrt-devices-sys-0.0.1 (c (n "winrt-devices-sys") (v "0.0.1") (h "03d3km1raw3w1lrwp1iiiwh4n2jadd9187w95awz52l8l2lyly7z") (r "1.46")))

(define-public crate-winrt-devices-sys-0.0.2 (c (n "winrt-devices-sys") (v "0.0.2") (h "1hakfgw0pzc8d1cn722cgkkgzwc5q85vcrmvz4kxbfvx10d1s2f1") (r "1.46")))

(define-public crate-winrt-devices-sys-0.23.0 (c (n "winrt-devices-sys") (v "0.23.0") (h "0xn8vv5ls97j0nz41jvryaw478y14j593b5hkihr3dqx4vkwkvwr")))

