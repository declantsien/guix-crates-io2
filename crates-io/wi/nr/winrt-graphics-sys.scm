(define-module (crates-io wi nr winrt-graphics-sys) #:use-module (crates-io))

(define-public crate-winrt-graphics-sys-0.0.1 (c (n "winrt-graphics-sys") (v "0.0.1") (h "17d942kmrmv6kagfkjmjpig7dbqryp1f1pvim57gpd4rb3igzgs7") (r "1.46")))

(define-public crate-winrt-graphics-sys-0.0.2 (c (n "winrt-graphics-sys") (v "0.0.2") (h "1xdhij9y5r57xvzd91lq6b0zmmnsp5bk39psr44nb7isp69nyi6z") (r "1.46")))

(define-public crate-winrt-graphics-sys-0.23.0 (c (n "winrt-graphics-sys") (v "0.23.0") (h "15c5z0klrr99xgj0gw8xbi44l3jhcxiqh6g8fjmzbhn3za9z6yqf")))

