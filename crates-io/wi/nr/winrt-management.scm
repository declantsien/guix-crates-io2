(define-module (crates-io wi nr winrt-management) #:use-module (crates-io))

(define-public crate-winrt-management-0.0.1 (c (n "winrt-management") (v "0.0.1") (h "05jgafq667shxk4vcmm1pc7grxgm2gqvwncyabid0y602i7kc8l7") (r "1.46")))

(define-public crate-winrt-management-0.0.2 (c (n "winrt-management") (v "0.0.2") (h "011hv1pg932vlmcpl1qh1w5vi3d5li0z3jnxsa95m7fc6wa91m6r") (r "1.46")))

(define-public crate-winrt-management-0.23.0 (c (n "winrt-management") (v "0.23.0") (h "0r63z1i0vbw2kajsgpxkjxln40cgvhah89dbpiz7wajqv97sw40s")))

