(define-module (crates-io wi nr winrt-toast) #:use-module (crates-io))

(define-public crate-winrt-toast-0.1.0 (c (n "winrt-toast") (v "0.1.0") (d (list (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Data_Xml_Dom" "Foundation" "Globalization" "UI_Notifications" "Win32_System_Registry" "Win32_Storage_FileSystem" "Win32_Security" "Win32_Foundation"))) (d #t) (k 0)))) (h "1ba2m76w4vb7biklk4i9x0c4jd58h087rx4wd4y3d1zdhc7vhiw9")))

(define-public crate-winrt-toast-0.1.1 (c (n "winrt-toast") (v "0.1.1") (d (list (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Data_Xml_Dom" "Foundation" "Globalization" "UI_Notifications" "Win32_System_Registry" "Win32_Storage_FileSystem" "Win32_Security" "Win32_Foundation"))) (d #t) (k 0)))) (h "1i1l0h462x5xqsjlwwc402241zl261kn05sf6d0sxghccg61iqd5")))

