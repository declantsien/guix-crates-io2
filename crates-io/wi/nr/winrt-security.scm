(define-module (crates-io wi nr winrt-security) #:use-module (crates-io))

(define-public crate-winrt-security-0.0.1 (c (n "winrt-security") (v "0.0.1") (h "0j7ak3ss6kknxsfmqgk1hdcy8q0sj9p5d0ja2rvayalsg7kmh1sy") (r "1.46")))

(define-public crate-winrt-security-0.0.2 (c (n "winrt-security") (v "0.0.2") (h "1jm27cwwdcablmzvjla77qrcxbh70a7v39qwki3snly40cd4q4hk") (r "1.46")))

(define-public crate-winrt-security-0.23.0 (c (n "winrt-security") (v "0.23.0") (h "011vi8qz2cpmi8r9sqr1szvy98y2i1xhkbfyd44v3r28kqjs2xdn")))

