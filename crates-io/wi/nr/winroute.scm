(define-module (crates-io wi nr winroute) #:use-module (crates-io))

(define-public crate-winroute-0.1.0 (c (n "winroute") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("iphlpapi" "netioapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cs4gnasrx6wnx034k7flj07gc06wyg58219hk18si4sb5x6qn3x") (y #t)))

(define-public crate-winroute-0.1.1 (c (n "winroute") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("iphlpapi" "netioapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zgy291pzcinhaqqxl4jv0nqldijafh3cj0a3v412nfybggwj40z")))

(define-public crate-winroute-0.1.2 (c (n "winroute") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("iphlpapi" "netioapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b1gkadip3vhz20wr88f3wyac15wal1zvbg6a24mdb5gxsy7npcb")))

(define-public crate-winroute-0.2.0 (c (n "winroute") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("iphlpapi" "netioapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z3b16fags8sjjmjvi32f1hl0a42qzmybng493p1784lqqcj2bmn") (f (quote (("serializable" "serde") ("default" "serializable"))))))

