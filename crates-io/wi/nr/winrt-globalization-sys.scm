(define-module (crates-io wi nr winrt-globalization-sys) #:use-module (crates-io))

(define-public crate-winrt-globalization-sys-0.0.1 (c (n "winrt-globalization-sys") (v "0.0.1") (h "00pq7j7fvg8112kkiliylw48fcnv3zlz7c6kifydf373rilrldzx") (r "1.46")))

(define-public crate-winrt-globalization-sys-0.0.2 (c (n "winrt-globalization-sys") (v "0.0.2") (h "17h6l8gwm62qlpwm9h1h3ix62vdrskw4r03qxz17q4al10l13vzx") (r "1.46")))

(define-public crate-winrt-globalization-sys-0.23.0 (c (n "winrt-globalization-sys") (v "0.23.0") (h "06sh5frb8028imqlr2pw3a2kzsi0sls326plmmad1f1vk8qzy2kq")))

