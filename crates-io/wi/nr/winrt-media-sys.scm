(define-module (crates-io wi nr winrt-media-sys) #:use-module (crates-io))

(define-public crate-winrt-media-sys-0.0.1 (c (n "winrt-media-sys") (v "0.0.1") (h "0bqp6bwcsmc4ym76a7yk9dbzvj5bs7ifmrf7sz9gs5f85vzlmcpp") (r "1.46")))

(define-public crate-winrt-media-sys-0.0.2 (c (n "winrt-media-sys") (v "0.0.2") (h "07iyfag126fvyabgqhkkz6xddrkfwz2n890ikfn4dvnzvr3mcais") (r "1.46")))

(define-public crate-winrt-media-sys-0.23.0 (c (n "winrt-media-sys") (v "0.23.0") (h "1w8vw3grbmqmvfgy0gnz1sjilxvs1mxmxa1004dfwihajf5hn7v0")))

