(define-module (crates-io wi nr winrt-services-sys) #:use-module (crates-io))

(define-public crate-winrt-services-sys-0.0.1 (c (n "winrt-services-sys") (v "0.0.1") (h "02fxh2qkzj6i4gmky4wf8s0gsy86jfb59rjlm4afxyl3302f8zd3") (r "1.46")))

(define-public crate-winrt-services-sys-0.0.2 (c (n "winrt-services-sys") (v "0.0.2") (h "1hkyj1nmih584dahx05mhrb9a4dmj24l8v7wiyx0xpp86qdb4grj") (r "1.46")))

(define-public crate-winrt-services-sys-0.23.0 (c (n "winrt-services-sys") (v "0.23.0") (h "03dxx50dfljqp2j9mn32557knqpqk4s3h0wfg8phf8idkalbigsl")))

