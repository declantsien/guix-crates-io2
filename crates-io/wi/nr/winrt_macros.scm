(define-module (crates-io wi nr winrt_macros) #:use-module (crates-io))

(define-public crate-winrt_macros-0.7.0 (c (n "winrt_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "winrt_gen") (r "^0.7") (d #t) (k 0)))) (h "1a2pw9mym27ma156hyzz2kdxf3105h8az3zbkikbpl71pk3s0y7w")))

(define-public crate-winrt_macros-0.7.1 (c (n "winrt_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "winrt_gen") (r "^0.7.1") (d #t) (k 0)))) (h "154mgv55cdl5978iqm4wb4jbn43j42y9wdl0p518p4rbyxxwckp5")))

(define-public crate-winrt_macros-0.7.2 (c (n "winrt_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "winrt_gen") (r "^0.7.2") (d #t) (k 0)))) (h "0qnn90kv3m5v9yvq3kkk2jg7yn7qzs5ss36jll4lks7fg3w991fq")))

