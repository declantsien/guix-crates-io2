(define-module (crates-io wi nr winreader) #:use-module (crates-io))

(define-public crate-winreader-0.1.0 (c (n "winreader") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "memoryapi" "handleapi" "processthreadsapi" "psapi"))) (d #t) (k 0)))) (h "0ikrrwx0iihgmsngw1h27090ww4zvcjcppp7s69psav88cvybc95")))

