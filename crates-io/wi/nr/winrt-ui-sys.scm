(define-module (crates-io wi nr winrt-ui-sys) #:use-module (crates-io))

(define-public crate-winrt-ui-sys-0.0.1 (c (n "winrt-ui-sys") (v "0.0.1") (h "0af9m9b93df72l6wcfi1w5y33d1w80lp55zslmd4bhw8ha1ypjx9") (r "1.46")))

(define-public crate-winrt-ui-sys-0.0.2 (c (n "winrt-ui-sys") (v "0.0.2") (h "0j5ck2y68yakrw4b9309c21nqc7fjvdahpa47p1ccsf3kwjibfjm") (r "1.46")))

(define-public crate-winrt-ui-sys-0.23.0 (c (n "winrt-ui-sys") (v "0.23.0") (h "16nxn3ciysbaqmzw206wd88kdglqsnqy2a6ydlcvzs7hj906z8k7")))

