(define-module (crates-io wi nr winrt-embedded-sys) #:use-module (crates-io))

(define-public crate-winrt-embedded-sys-0.0.0 (c (n "winrt-embedded-sys") (v "0.0.0") (h "094y0zly65517jcakmvpsk6cxb96i4pjxj5mbszgizg4p0d06spz") (r "1.46")))

(define-public crate-winrt-embedded-sys-0.0.1 (c (n "winrt-embedded-sys") (v "0.0.1") (h "077p9a9vfnyq0z3i6lbs9hhxxwv2xic53cjrizb268sv799mcjpp") (r "1.46")))

(define-public crate-winrt-embedded-sys-0.0.2 (c (n "winrt-embedded-sys") (v "0.0.2") (h "1m927hj2pd2pzy1ipacp0rlxzfq8c6qf08844d63zl8y1ql1ij6g") (r "1.46")))

(define-public crate-winrt-embedded-sys-0.23.0 (c (n "winrt-embedded-sys") (v "0.23.0") (h "1ynhz5n0pgv7a86n71s721i8a8k86dq6lj22jnshm11492y385kz")))

