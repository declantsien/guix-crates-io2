(define-module (crates-io wi nr winrt-storage-sys) #:use-module (crates-io))

(define-public crate-winrt-storage-sys-0.0.1 (c (n "winrt-storage-sys") (v "0.0.1") (h "0bh3cby3b3dhks9a3n8nzqqkglynpxawb2s4brdmwg7gsivvd747") (r "1.46")))

(define-public crate-winrt-storage-sys-0.0.2 (c (n "winrt-storage-sys") (v "0.0.2") (h "13dn2vwkr1p3kvvx93ca1fz7v3nfpv0kfcmk6f7jcvalzbj3010c") (r "1.46")))

(define-public crate-winrt-storage-sys-0.23.0 (c (n "winrt-storage-sys") (v "0.23.0") (h "038vjsrvfp4sx07f7alkrlg5n2x5qg1j739ld5fpc4lbkj1x56cl")))

