(define-module (crates-io wi nr winrt-services) #:use-module (crates-io))

(define-public crate-winrt-services-0.0.1 (c (n "winrt-services") (v "0.0.1") (h "1vbzzipg0wfkmmjh5liw3jzzf8nzsiawnv4bpwdyr5z1m8dq5rgn") (r "1.46")))

(define-public crate-winrt-services-0.0.2 (c (n "winrt-services") (v "0.0.2") (h "0dyq9f0db0s26si3pjiab4gqvax3c78l2m8v62jqdjg3kgz46dlw") (r "1.46")))

(define-public crate-winrt-services-0.23.0 (c (n "winrt-services") (v "0.23.0") (h "0wlijjk26l2dzsdnnmvi78gb1rzvq4hl9j81zswgbc84q88ffk2b")))

