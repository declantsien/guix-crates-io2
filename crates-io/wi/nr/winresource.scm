(define-module (crates-io wi nr winresource) #:use-module (crates-io))

(define-public crate-winresource-0.1.12 (c (n "winresource") (v "0.1.12") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1zx333r2q9kp62yjpmq90n29gl2mp9m7x90khhylagzfszzc3krp")))

(define-public crate-winresource-0.1.13 (c (n "winresource") (v "0.1.13") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "11k3f0mc2a4q2rmz0m55wwihxzsyciq76d838zbva8ns3hlig6am")))

(define-public crate-winresource-0.1.14 (c (n "winresource") (v "0.1.14") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0sb9slva788hshpdh46n4pmhsa046dalkmxhsl4pir0kfzr7dp6z")))

(define-public crate-winresource-0.1.15 (c (n "winresource") (v "0.1.15") (d (list (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0c5ws4cmpwhyq91k3y39n15rqfrhp1l30rpf0fyw0137mvqqrlsw")))

(define-public crate-winresource-0.1.16 (c (n "winresource") (v "0.1.16") (d (list (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "08ww4h28b9ykixrldbmqv5rfas1hvhxs19acpc45rfnyr2lgk84b")))

(define-public crate-winresource-0.1.17 (c (n "winresource") (v "0.1.17") (d (list (d (n "toml") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0aakwh8llq2zvm7qihkrg7sz50hzccyl4x831j60g4psijpsmqkp") (f (quote (("default" "toml"))))))

