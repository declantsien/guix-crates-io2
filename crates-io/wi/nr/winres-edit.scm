(define-module (crates-io wi nr winres-edit) #:use-module (crates-io))

(define-public crate-winres-edit-0.0.0 (c (n "winres-edit") (v "0.0.0") (h "19nv37nw5kl2s8hplvyirp5m4ndr13843qlsrjb23scac9296q50")))

(define-public crate-winres-edit-0.1.0 (c (n "winres-edit") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "manual-serializer") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Diagnostics_Debug" "Win32_System" "Win32_Security" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "10v3qvazjayrjgzfs718pb305gmbfhflg3zlmmhpjm2bmqfb9lny")))

(define-public crate-winres-edit-0.2.0 (c (n "winres-edit") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "manual-serializer") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Diagnostics_Debug" "Win32_System" "Win32_Security" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1134c54g7yr72bi4559n094pfq7xykwzskbh5a9ijxphmbh70gqh")))

