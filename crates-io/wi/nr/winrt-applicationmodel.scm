(define-module (crates-io wi nr winrt-applicationmodel) #:use-module (crates-io))

(define-public crate-winrt-applicationmodel-0.0.0 (c (n "winrt-applicationmodel") (v "0.0.0") (h "018fjxh8xwkn4ajnl8k8jw0pyy2pjm3fv4s8pxknxz87kixjg4lv") (r "1.46")))

(define-public crate-winrt-applicationmodel-0.0.1 (c (n "winrt-applicationmodel") (v "0.0.1") (h "075nhy114rdarxfcy7ic3ram66w4pa47d08nbyabxqlqm6lnvixv") (r "1.46")))

(define-public crate-winrt-applicationmodel-0.0.2 (c (n "winrt-applicationmodel") (v "0.0.2") (h "0bs4k11vh4dsjqm0v3m7ahi4vgs6hp6lxk2bgmh129ajksnd5ayy") (r "1.46")))

(define-public crate-winrt-applicationmodel-0.23.0 (c (n "winrt-applicationmodel") (v "0.23.0") (h "0n54k83lzpplm5cwlc21zz7gw384ww2sx19ih06n1cal9axwbd47")))

