(define-module (crates-io wi nr winrt-notification) #:use-module (crates-io))

(define-public crate-winrt-notification-0.1.0 (c (n "winrt-notification") (v "0.1.0") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "0nyxm20qyymgc4p0mxz0q0qjaji1bhwnp3rqyfy8haxy2gdxlb72") (y #t)))

(define-public crate-winrt-notification-0.1.1 (c (n "winrt-notification") (v "0.1.1") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1av3d2wnydyyfvnz7fm5wd1hyhk0ws1i44s7nfbz6737ihx63hnx")))

(define-public crate-winrt-notification-0.1.2 (c (n "winrt-notification") (v "0.1.2") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "0hf0i8wbfn943rqv9yskap907vcmwhrd1gh1hyc2mxnsmknn3kia")))

(define-public crate-winrt-notification-0.1.3 (c (n "winrt-notification") (v "0.1.3") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1ckrghq6i6mkszdzzmf7g8mj6913mpmazd524kk7d12qkh2vrdy0")))

(define-public crate-winrt-notification-0.1.4 (c (n "winrt-notification") (v "0.1.4") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1b9ms85g9310gz3ylrn19ap7syj7mn86ikbn6gaiawmm8qaggqi9")))

(define-public crate-winrt-notification-0.1.5 (c (n "winrt-notification") (v "0.1.5") (d (list (d (n "winrt") (r "^0.3.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "0fkb6j2pcwhhn3c7lavjv7pbd2xxy3k5b5dv5gb2c6s2gxjpi284")))

(define-public crate-winrt-notification-0.2.0 (c (n "winrt-notification") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.0") (d #t) (k 0)) (d (n "winrt") (r "^0.4.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "07affgvl5lhvfjyc8l32yg24s554p549a8wmskpps4pkjcvkdbpb")))

(define-public crate-winrt-notification-0.2.1 (c (n "winrt-notification") (v "0.2.1") (d (list (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.0") (d #t) (k 0)) (d (n "winrt") (r "^0.4.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "06smq5j4zvb6ggi78w2y1pyjrxv5bx6bw32sxg948qg0h5bph1i8")))

(define-public crate-winrt-notification-0.2.2 (c (n "winrt-notification") (v "0.2.2") (d (list (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (d #t) (k 0)) (d (n "winrt") (r "^0.4.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "0hm2c8zyklk65myrnkyksaqlyv2n95i23qyjkdpjqy8dlmfsccbc")))

(define-public crate-winrt-notification-0.2.3 (c (n "winrt-notification") (v "0.2.3") (d (list (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (d #t) (k 0)) (d (n "winrt") (r "^0.4.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1cf620mcbas8035iiv1dggi14xnf845pv4fyay03adp6w4xbxdyv")))

(define-public crate-winrt-notification-0.3.0 (c (n "winrt-notification") (v "0.3.0") (d (list (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.3.1") (d #t) (k 0)) (d (n "windows") (r "^0.3.1") (d #t) (k 1)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0h3mpgphrshs2xp1jmrvgaq91gwvz9l80nicmipd90h3ydl54a0w")))

(define-public crate-winrt-notification-0.2.4 (c (n "winrt-notification") (v "0.2.4") (d (list (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.4") (d #t) (k 0)) (d (n "winrt") (r "^0.4.0") (f (quote ("windows-data" "windows-ui"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.6.1") (d #t) (k 0)))) (h "1i013b5zwx9y3hlwnwdx6cr3fy4bpahz4kdlva14d2k8h6r0wyap")))

(define-public crate-winrt-notification-0.3.1 (c (n "winrt-notification") (v "0.3.1") (d (list (d (n "strum") (r "^0.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.3.1") (d #t) (k 0)) (d (n "windows") (r "^0.3.1") (d #t) (k 1)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1jh731n9dnrvm2jk1dcildh0xb3i9znbm4rz04rm0rnsmi2rivp5")))

(define-public crate-winrt-notification-0.4.0 (c (n "winrt-notification") (v "0.4.0") (d (list (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 0)) (d (n "windows") (r "^0.21.1") (d #t) (k 1)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1nkjvfzw9s96i6r1iv1czgvky2rdzqjc29dnwqz2pi36ihmq7k7h")))

(define-public crate-winrt-notification-0.5.0 (c (n "winrt-notification") (v "0.5.0") (d (list (d (n "strum") (r "^0.22.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.24.0") (f (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Data_Xml_Dom" "UI_Notifications"))) (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "windows") (r "^0.24.0") (f (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Win32_System_LibraryLoader" "Data_Xml_Dom" "UI_Notifications"))) (d #t) (t "cfg(target_env = \"gnu\")") (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0smyrjq0s141dmzl4mxwqx7ll8sgrbbi81qasprjajh3ivxh38gd")))

(define-public crate-winrt-notification-0.5.1 (c (n "winrt-notification") (v "0.5.1") (d (list (d (n "strum") (r "^0.22.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows") (r "^0.24.0") (f (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Data_Xml_Dom" "UI_Notifications"))) (d #t) (t "cfg(target_env = \"msvc\")") (k 0)) (d (n "windows") (r "^0.24.0") (f (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Win32_System_LibraryLoader" "Data_Xml_Dom" "UI_Notifications"))) (d #t) (t "cfg(target_env = \"gnu\")") (k 0)) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "1x7djvadifkgzvkkrzyfr5mpzvaqzxibkrbkvk3f08qbhi9h6yh0")))

