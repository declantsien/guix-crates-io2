(define-module (crates-io wi nr winrt-perception-sys) #:use-module (crates-io))

(define-public crate-winrt-perception-sys-0.0.1 (c (n "winrt-perception-sys") (v "0.0.1") (h "0qh2138bzjxnbj3904y41940svfs5ggvgbjrgz8hma88yknrhanq") (r "1.46")))

(define-public crate-winrt-perception-sys-0.0.2 (c (n "winrt-perception-sys") (v "0.0.2") (h "0dys3g52ix4sdhiyv4rcy6p9fga2k3w1cxa0h2qj2zn0a8lh6lav") (r "1.46")))

(define-public crate-winrt-perception-sys-0.23.0 (c (n "winrt-perception-sys") (v "0.23.0") (h "1a2jxdmz5k25w8yhiy7jjbbpjqvs07pl7fd5cw1f438mklyfx0ck")))

