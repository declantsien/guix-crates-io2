(define-module (crates-io wi nr winrt-networking) #:use-module (crates-io))

(define-public crate-winrt-networking-0.0.1 (c (n "winrt-networking") (v "0.0.1") (h "0b51735f7k7n6qj6kkppzrpr56qkr83krbg4w7kjlganhacr44c2") (r "1.46")))

(define-public crate-winrt-networking-0.0.2 (c (n "winrt-networking") (v "0.0.2") (h "1gmbfkvydpshh02m3nlab2vdypymsfll03jd4r2j4mm3q3grpz1j") (r "1.46")))

(define-public crate-winrt-networking-0.23.0 (c (n "winrt-networking") (v "0.23.0") (h "18m1pxc0svvykzapshjkimqargh2654yamf4sa6zqylw51a7lr0y")))

