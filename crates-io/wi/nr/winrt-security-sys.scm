(define-module (crates-io wi nr winrt-security-sys) #:use-module (crates-io))

(define-public crate-winrt-security-sys-0.0.1 (c (n "winrt-security-sys") (v "0.0.1") (h "1smg2a5k2szj5hwzc2cx3iybiwk3yxscs65978wg6qsgii4pa64g") (r "1.46")))

(define-public crate-winrt-security-sys-0.0.2 (c (n "winrt-security-sys") (v "0.0.2") (h "1rb7py8j2mcsjgrvg5p59b4n1ss7c34gn87b1dlqvvx02fylysr1") (r "1.46")))

(define-public crate-winrt-security-sys-0.23.0 (c (n "winrt-security-sys") (v "0.23.0") (h "052n3m16z8cwixadhsjldjsj88kn48vfpvi468syffp8fhc1ca9x")))

