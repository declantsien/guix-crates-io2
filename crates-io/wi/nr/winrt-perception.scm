(define-module (crates-io wi nr winrt-perception) #:use-module (crates-io))

(define-public crate-winrt-perception-0.0.1 (c (n "winrt-perception") (v "0.0.1") (h "0zxidllpqml3zkq31mmn13801d6fw9wrr3lgysmajnshv6k4jn7m") (r "1.46")))

(define-public crate-winrt-perception-0.0.2 (c (n "winrt-perception") (v "0.0.2") (h "0kgh914146rp7h5a9jwlapxw4xnnhsy5d4095zbgwjd2v2q5nw1r") (r "1.46")))

(define-public crate-winrt-perception-0.23.0 (c (n "winrt-perception") (v "0.23.0") (h "0bwzjhzqsgws093nx8b9zi7m6xyydc0ys56ii573rpj3d8mjggiq")))

