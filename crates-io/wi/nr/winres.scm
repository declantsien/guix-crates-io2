(define-module (crates-io wi nr winres) #:use-module (crates-io))

(define-public crate-winres-0.1.0 (c (n "winres") (v "0.1.0") (d (list (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "0sp4ji5yb45hdkypahsv4rg8bn0wn8r9slaldmk77p3fd7257cc2")))

(define-public crate-winres-0.1.1 (c (n "winres") (v "0.1.1") (d (list (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 2)))) (h "1ppw0v2jcpd4xgvrzmni6c9d2di27l0hgig1q93gy9qwcnk2mrfq")))

(define-public crate-winres-0.1.3 (c (n "winres") (v "0.1.3") (d (list (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.6") (d #t) (k 2)))) (h "0xi4amzpwwympic7815v91kwp1gdnz6md6bhqbx136b6msyjgpzh")))

(define-public crate-winres-0.1.4 (c (n "winres") (v "0.1.4") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 2)))) (h "0fhal45h6mq2zwrw5z2fbwq52v2blmi1wm5kn9mpmgq7macd1nqq")))

(define-public crate-winres-0.1.5 (c (n "winres") (v "0.1.5") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0pgjnhilck4zydl9j15nck7l6xdn0ranrv2x83lfzmsnccnikn97")))

(define-public crate-winres-0.1.6 (c (n "winres") (v "0.1.6") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "13gji0fn3n6rij39ya4xkz25vn6k98is5javqv53whbr9vdanzgh")))

(define-public crate-winres-0.1.7 (c (n "winres") (v "0.1.7") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0p9wz1r2s24ckaq6mp0qd4hwklfg7h16zi4cld6wp280c220n6nj")))

(define-public crate-winres-0.1.8 (c (n "winres") (v "0.1.8") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1kcy3wvn5jrggb4b2pak4z1kpkinfyrdy3r5qxgikfhm9c5qv95f")))

(define-public crate-winres-0.1.9 (c (n "winres") (v "0.1.9") (d (list (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0gpxf7f2x0kqkkrwsm6xyy0gr29y1hz0pvwd2r7mm5gymcf1il93")))

(define-public crate-winres-0.1.10 (c (n "winres") (v "0.1.10") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1rv01sfq1hffbnq6kcvcfg08fzs8nfcg7xb0w0hi12qc9q8920il")))

(define-public crate-winres-0.1.11 (c (n "winres") (v "0.1.11") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "1p55gj03m5j41w6a1vghg0l0dkz6dwp7mxqmzy98jnzypc8bakzz")))

(define-public crate-winres-0.1.12 (c (n "winres") (v "0.1.12") (d (list (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt"))) (d #t) (k 2)))) (h "0v2gvqnd8iwwvb6fs69nv0mmk1z96430527n0qlfbsarxxhv53dn")))

