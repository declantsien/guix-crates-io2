(define-module (crates-io wi nr winresult) #:use-module (crates-io))

(define-public crate-winresult-0.1.0 (c (n "winresult") (v "0.1.0") (d (list (d (n "winresult-types") (r "^0.1.0") (d #t) (k 0)))) (h "0xa4ga0kpgyk5rmczlpwjkaabhcims9w3xqig9vp4p7yhcipvny2")))

(define-public crate-winresult-0.1.1 (c (n "winresult") (v "0.1.1") (d (list (d (n "winresult-types") (r "^0.1.0") (d #t) (k 0)))) (h "1rc1rbb0dwd8lqg6j4h1dpr82qhanz7j9a21mwk3aiwf5wihz43y")))

(define-public crate-winresult-0.1.2 (c (n "winresult") (v "0.1.2") (d (list (d (n "winresult-types") (r "^0.1.2") (d #t) (k 0)))) (h "1z4q7h4shf4m42qpmb8ng09hs9cc5ycvl3wh4c28r2rxlm71n3zv")))

(define-public crate-winresult-0.1.3 (c (n "winresult") (v "0.1.3") (d (list (d (n "winresult-types") (r "^0.1.3") (d #t) (k 0)))) (h "13gm27f6bpdpagah6b8a7frnc59cssawdk3k9q8xzwr1ac0x9q8z")))

