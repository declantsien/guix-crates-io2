(define-module (crates-io wi nr winrt-storage) #:use-module (crates-io))

(define-public crate-winrt-storage-0.0.1 (c (n "winrt-storage") (v "0.0.1") (h "01gph2ax8y2lhl153zkw58c0xz0da5a3302zs8q009wzc7w7nc3x") (r "1.46")))

(define-public crate-winrt-storage-0.0.2 (c (n "winrt-storage") (v "0.0.2") (h "0qx8g0nkmqcr2vj8s81x4dgnnsfn39d5qwwzhjixw2jmjlrw5s39") (r "1.46")))

(define-public crate-winrt-storage-0.23.0 (c (n "winrt-storage") (v "0.23.0") (h "1bih4fcsma3ck47idn2ywa321zdj04bs1vsw2j96i2rwsd8xlbi4")))

