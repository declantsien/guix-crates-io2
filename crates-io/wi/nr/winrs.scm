(define-module (crates-io wi nr winrs) #:use-module (crates-io))

(define-public crate-winrs-0.1.0 (c (n "winrs") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef" "errhandlingapi"))) (d #t) (k 0)))) (h "1w8b0iihh41anzljyxw9a909vjkxrxwdnc4l4xdhhdf50xgg8fhh") (f (quote (("default" "clipboard") ("console" "winapi/consoleapi" "winapi/wincon") ("clipboard" "winapi/winuser"))))))

