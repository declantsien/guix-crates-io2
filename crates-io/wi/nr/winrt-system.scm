(define-module (crates-io wi nr winrt-system) #:use-module (crates-io))

(define-public crate-winrt-system-0.0.1 (c (n "winrt-system") (v "0.0.1") (h "0j5lnjmy2gpbn9cvak907wd5lz502r2in7nzsjp6yrcy6njjvzvk") (r "1.46")))

(define-public crate-winrt-system-0.0.2 (c (n "winrt-system") (v "0.0.2") (h "0lprcjfm2zrmr70s1qw51qy2y5as5i1rdqjc74jp2jrv160j07xv") (r "1.46")))

(define-public crate-winrt-system-0.23.0 (c (n "winrt-system") (v "0.23.0") (h "0wnl330pl7zjh2cgq5qlp2zkcjs12d29ykf4ix08kv4qcwmd9y95")))

