(define-module (crates-io wi nr winrt-phone-sys) #:use-module (crates-io))

(define-public crate-winrt-phone-sys-0.0.1 (c (n "winrt-phone-sys") (v "0.0.1") (h "1w03r38d46fd6azdi8mlwdra6qh767siqyhrrpz8jj8ha6vs4033") (r "1.46")))

(define-public crate-winrt-phone-sys-0.0.2 (c (n "winrt-phone-sys") (v "0.0.2") (h "0w8kglk4lph33wy7y57pk01jbfywa744ck6g7ncs9hzzd8y96ich") (r "1.46")))

(define-public crate-winrt-phone-sys-0.23.0 (c (n "winrt-phone-sys") (v "0.23.0") (h "0kjwhcz1bdfkaq3rgdxcwfxblvaw1bp4k2502l9hizxfkdzdn3fm")))

