(define-module (crates-io wi tr witransfer) #:use-module (crates-io))

(define-public crate-witransfer-0.1.0 (c (n "witransfer") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "13v7f1jp9778jqyw0wcjmgzsr18daw1b07s5ykmwxnqh628wlcka") (y #t)))

(define-public crate-witransfer-0.1.1 (c (n "witransfer") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jipnca04h27gn5zyi94jkbzjg6ld3n10rb9cf7rdjzqmwnlsivd") (y #t)))

