(define-module (crates-io wi on wion) #:use-module (crates-io))

(define-public crate-wion-0.0.0 (c (n "wion") (v "0.0.0") (d (list (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)))) (h "1g6ncgxl1j17c4kxhi3l9j1x7hgq3g5dhji19driv4dafl4j44rn") (f (quote (("default"))))))

(define-public crate-wion-0.1.0 (c (n "wion") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (o #t) (d #t) (k 0)))) (h "0p3244gzvxbr4pnnspn3zqrys4zbp39qw3frcdm6837b957d2l5r") (f (quote (("default"))))))

