(define-module (crates-io wi al wialon_sdk) #:use-module (crates-io))

(define-public crate-wialon_sdk-0.1.0 (c (n "wialon_sdk") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "123nad843xk04fhj8wn2r1j1smama4gisfvb0cr9l49908sfsxbl") (y #t)))

(define-public crate-wialon_sdk-0.0.5 (c (n "wialon_sdk") (v "0.0.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0l2i51922cyl58dplrxp4s3j7s2kspx2dlj3b29l0lh6zwv286b4")))

