(define-module (crates-io wi al wialon-rs) #:use-module (crates-io))

(define-public crate-wialon-rs-0.1.0 (c (n "wialon-rs") (v "0.1.0") (d (list (d (n "async-trait") (r "0.1.*") (d #t) (k 0)) (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "0.1.*") (d #t) (k 0)) (d (n "serde_urlencoded") (r "0.7.*") (d #t) (k 0)))) (h "0p5bhaklich3jkjq1wyzwmb3yqg4nmaxrnimd4nw9hbrcwaj35js")))

(define-public crate-wialon-rs-0.2.0 (c (n "wialon-rs") (v "0.2.0") (d (list (d (n "async-trait") (r "0.1.*") (d #t) (k 0)) (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "0.1.*") (d #t) (k 0)) (d (n "serde_urlencoded") (r "0.7.*") (d #t) (k 0)))) (h "1r3ip38gfk52crxxbz0y298pg7ch0jjphr75izqnj5jjlfk7w8k0")))

(define-public crate-wialon-rs-0.2.1 (c (n "wialon-rs") (v "0.2.1") (d (list (d (n "async-trait") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "0.11.*") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_repr") (r "0.1.*") (d #t) (k 0)) (d (n "serde_urlencoded") (r "0.7.*") (d #t) (k 0)))) (h "17zdgfm7dl175hnw58bxi356qanfby1lx7fhaicl91w23ys2j71q")))

