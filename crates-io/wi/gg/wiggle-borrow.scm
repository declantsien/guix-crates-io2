(define-module (crates-io wi gg wiggle-borrow) #:use-module (crates-io))

(define-public crate-wiggle-borrow-0.22.0 (c (n "wiggle-borrow") (v "0.22.0") (d (list (d (n "wiggle") (r "^0.22.0") (d #t) (k 0)))) (h "09hplp5l28cbs081b0cl2slm7ws89nak8kfrc1h6p8zq6z3ywrcp")))

(define-public crate-wiggle-borrow-0.23.0 (c (n "wiggle-borrow") (v "0.23.0") (d (list (d (n "wiggle") (r "^0.23.0") (d #t) (k 0)))) (h "02ahnpp8m1n6ryxg8n86lg941llsrrfyjvh8vxq3qq955f90dypj")))

(define-public crate-wiggle-borrow-0.24.0 (c (n "wiggle-borrow") (v "0.24.0") (d (list (d (n "wiggle") (r "^0.24.0") (d #t) (k 0)))) (h "00xm3r07s5p668j4rd1bhqqbcwrcjp379i0hm3yw4pcj4jgc9a2n")))

(define-public crate-wiggle-borrow-0.25.0 (c (n "wiggle-borrow") (v "0.25.0") (d (list (d (n "wiggle") (r "^0.25.0") (d #t) (k 0)))) (h "0a22h97bcvkar4nrcwrvv46mgn4w0lijlqzznka3nkbsfld7wryz")))

(define-public crate-wiggle-borrow-0.26.0 (c (n "wiggle-borrow") (v "0.26.0") (d (list (d (n "wiggle") (r "^0.26.0") (d #t) (k 0)))) (h "13lnsg8qrgy88wdd4x1xk5bqh4n9n7yc8mpv73r9r9r25snpby4d")))

(define-public crate-wiggle-borrow-0.27.0 (c (n "wiggle-borrow") (v "0.27.0") (d (list (d (n "wiggle") (r "^0.27.0") (d #t) (k 0)))) (h "1jxqxzmyri63izamg7wj87cg3g6sbskmv2wpdfc93m9qglng060k")))

(define-public crate-wiggle-borrow-0.26.1 (c (n "wiggle-borrow") (v "0.26.1") (d (list (d (n "wiggle") (r "^0.26.1") (d #t) (k 0)))) (h "1bpsc2md17n8pg62m9511g9kha8d0vlxa1ixvzc53jy1ngkvl7qm")))

