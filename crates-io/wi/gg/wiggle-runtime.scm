(define-module (crates-io wi gg wiggle-runtime) #:use-module (crates-io))

(define-public crate-wiggle-runtime-0.1.0 (c (n "wiggle-runtime") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ziw1b0nppkghb4la1pzkqw143r1n2x7k1hakmcqay73g1rf55cg")))

(define-public crate-wiggle-runtime-0.13.0 (c (n "wiggle-runtime") (v "0.13.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (o #t) (d #t) (k 0)))) (h "0is946didmbcg2i3hgxfcg4pmc8v2lkgxia6n1mbphbsiibzyynm") (f (quote (("wiggle_metadata" "witx"))))))

