(define-module (crates-io wi gg wiggle-macro) #:use-module (crates-io))

(define-public crate-wiggle-macro-0.14.0 (c (n "wiggle-macro") (v "0.14.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.14.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (d #t) (k 0)))) (h "0j9wrlyf2px3qb1xjk96nq6mfa72w408lgi89rkf3s7w34mgyj62") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.15.0 (c (n "wiggle-macro") (v "0.15.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.15.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (d #t) (k 0)))) (h "1cwgfhqy67yxxg18l2j9jxn27p382abv3q7b6g064dxd2v6rhppk") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.16.0 (c (n "wiggle-macro") (v "0.16.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.16.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "1b14j4i28d98kj3ryq0hqniaigw54yyk0cp9hhrrrgn3qjy3pgli") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.17.0 (c (n "wiggle-macro") (v "0.17.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.17.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0sz3qbk1sxllb4rhqigiygzzfbsxhrj0yzzxbcf8af39fx9j6qcz") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.18.0 (c (n "wiggle-macro") (v "0.18.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.18.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "1abk6sz1hqnjym2h5d8zzx5vhcbg1wlxpf99f70z4j18mgjy8q5g") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.19.0 (c (n "wiggle-macro") (v "0.19.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.19.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "153ss1dqn27sq4vs66p5c8ilajsx7x00j4p7rpq0xmbhv63qdc1q") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.20.0 (c (n "wiggle-macro") (v "0.20.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.20.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "1lc2gnac0jwh6hm6fdiknn31das4li2y770b5340hfl87jcsg90v") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.21.0 (c (n "wiggle-macro") (v "0.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.21.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "09nc7finvh7jhhsdrqc670pjbgknk829aahp3plarr7k1049kpzk") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.22.0 (c (n "wiggle-macro") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.22.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "0qg37kxaf6dhgp58q14w8xww4pxamfk1kidqd0pnh2dyfkbj1vb2") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.23.0 (c (n "wiggle-macro") (v "0.23.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.23.0") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "0bnlvdvw34mfix2897d9j5nkk4n59ls2hlq899pkdaqrip520gcf") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.24.0 (c (n "wiggle-macro") (v "0.24.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.24.0") (d #t) (k 0)) (d (n "witx") (r "^0.9") (d #t) (k 0)))) (h "1rigzvsfm7qr577n11xcd4600xqaqhm2ma6h4r925q5x857gn6pq") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.25.0 (c (n "wiggle-macro") (v "0.25.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.25.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "0y6alji38pvgflfq4xamnxszw71ynrlj5d8q9a9j535c7ppdgx4i") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.26.0 (c (n "wiggle-macro") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.26.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "0879aa0kqssb0s4krly2qcmg2mk17sk3brbmns1m18h2afvw8xxb") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.27.0 (c (n "wiggle-macro") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.27.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "01vwkdkz1h2lbblg94abxzvx81dc0q7fp8qz19qndir2wrsn2gbh") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.26.1 (c (n "wiggle-macro") (v "0.26.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.26.1") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "18s3r4ipwa793kd6jsl6fn2pb37k8azijpicmfn1xk66vfhnxma9") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-0.28.0 (c (n "wiggle-macro") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.28.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.0") (d #t) (k 0)))) (h "0n2vqgpyc6vq3022x6wxxdr2spnydm5glhhkdw781jbll9vswccy") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.29.0 (c (n "wiggle-macro") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.29.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "0d46vr6n025mj0a9jjfmyzq4fw4pkhwi700irm6nm3qfrzjfv4r5") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.30.0 (c (n "wiggle-macro") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "^0.30.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "0qr6z724zgmb4k6iq7pdiwia721p94j4nm6v301qyj19fxiizfbl") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.31.0 (c (n "wiggle-macro") (v "0.31.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.31.0") (d #t) (k 0)) (d (n "witx") (r "^0.9.1") (d #t) (k 0)))) (h "12clxxfjnk554634p8cjill0brl932mbjpvc7jiypsy0vdacik1p") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.32.0 (c (n "wiggle-macro") (v "0.32.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.32.0") (d #t) (k 0)))) (h "10v2x24jg1gb822bajksc1ijg7jy4h20qx03vd06clwrx84dsi24") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.32.1 (c (n "wiggle-macro") (v "0.32.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.32.1") (d #t) (k 0)))) (h "02n6cfwh0x4z8p6gq8w017znhpm63p38s9j98j8w35h3gagsfrl8") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.33.0 (c (n "wiggle-macro") (v "0.33.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.33.0") (d #t) (k 0)))) (h "0qqn1442fhqj97jxqlpkpcayv8fdfzm6aqsx7kc4x4jpz36djgfr") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.34.0 (c (n "wiggle-macro") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.34.0") (d #t) (k 0)))) (h "09hzaq46gmw1icps2y4mzc0601mh8vcnqc20f9qxggimdhs4qbar") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.34.1 (c (n "wiggle-macro") (v "0.34.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.34.1") (d #t) (k 0)))) (h "1s2h9qwv5mi3vflbd14zh0myhsg3v18cnsdqkjjxnw0kz2xl7n5b") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.33.1 (c (n "wiggle-macro") (v "0.33.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.33.1") (d #t) (k 0)))) (h "0s9ybfhv0n7qg96cyxx7p8k0n9am279kpyq03grjpdx5725ixp5p") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.35.0 (c (n "wiggle-macro") (v "0.35.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.35.0") (d #t) (k 0)))) (h "045yw3wmymqpmci30pbn0nxsfxjwbkm7b85jjprsqyzgh1xvl6f1") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.35.1 (c (n "wiggle-macro") (v "0.35.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.35.1") (d #t) (k 0)))) (h "02y25g6m28q38vdf7zv4a0rsx9mnj4zaa0g3q2h3hzkijw0978nl") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.35.2 (c (n "wiggle-macro") (v "0.35.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.35.2") (d #t) (k 0)))) (h "09czybvjh4xxz9l6yd9kiaxha3yr89lgzvc1323nikj6ccrxwlj2") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.34.2 (c (n "wiggle-macro") (v "0.34.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.34.2") (d #t) (k 0)))) (h "1xn9pr95xcfwrsb0qvragwrz193k5drz9wwpiaf68fcbgxv7wkhl") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.35.3 (c (n "wiggle-macro") (v "0.35.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.35.3") (d #t) (k 0)))) (h "0b96b9nmq3kwwd34cdaaahvlzf1kz431zjjr3vvlpvgcjdnck9hx") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.36.0 (c (n "wiggle-macro") (v "0.36.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.36.0") (d #t) (k 0)))) (h "1spkk0y37yrl2xhfwmql4b749lybjj7md4v0cg68z42x2abrg3h5") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.37.0 (c (n "wiggle-macro") (v "0.37.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.37.0") (d #t) (k 0)))) (h "13bd2ipl4sdsr52478yg8acj030r0f69xqsx72k49vpfb0gnn8c7") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.38.0 (c (n "wiggle-macro") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.38.0") (d #t) (k 0)))) (h "1nwljw9y1s76v4v42kdm3q9nzqw8vbzk9fp6nbj1qfzjz183p63q") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.38.1 (c (n "wiggle-macro") (v "0.38.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.38.1") (d #t) (k 0)))) (h "1axa2pah6ny6apsmk0ix08zmlwnqqa0xzafqf4sdbi7jw45ilm1f") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.39.0 (c (n "wiggle-macro") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.39.0") (d #t) (k 0)))) (h "150vsnrdpgp63ib6dlrcsrbfi62fghq9jizygr2zjfs6hnp37c1v") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.38.2 (c (n "wiggle-macro") (v "0.38.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.38.2") (d #t) (k 0)))) (h "1sc59l8knb0qcs0mh99irs2xl75k29qnihsalrmj8qbdh8vlvph4") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.38.3 (c (n "wiggle-macro") (v "0.38.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.38.3") (d #t) (k 0)))) (h "1hlfni4whrn8lbjjbi86p7nnbkrwjnq0czm6x96w6q4mgpanihzi") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.39.1 (c (n "wiggle-macro") (v "0.39.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.39.1") (d #t) (k 0)))) (h "198dfj73k1szj8zbb9fv6jm3iz5x7p2bgzj93mj2zm3r5090kmcl") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.40.0 (c (n "wiggle-macro") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.40.0") (d #t) (k 0)))) (h "0by1nh0mqcjy8aqp22x6saadf46h6hdnnkb3gi3k2rjaviwr7wjk") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-0.40.1 (c (n "wiggle-macro") (v "0.40.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=0.40.1") (d #t) (k 0)))) (h "033dg6f0x1m017pb18k6r8v60cdnjag8inxsnmd3fs16fj9jblx4") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-1.0.0 (c (n "wiggle-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=1.0.0") (d #t) (k 0)))) (h "1nd8p3h1av17lahwscidhpx6s1rahncidknds15yh7bgvkanijd5") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-1.0.1 (c (n "wiggle-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=1.0.1") (d #t) (k 0)))) (h "1yjc4cqykw8si0rrxzxazbs72cll8sv7s623dbizbgkpd3p2g2iv") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-2.0.0 (c (n "wiggle-macro") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=2.0.0") (d #t) (k 0)))) (h "15kbysapvrq4mjix5k6ka03r537iddy2cpz662gzd3nnwna3fgda") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-2.0.1 (c (n "wiggle-macro") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=2.0.1") (d #t) (k 0)))) (h "0mbjg74kbpwdry0w58pkzyp4gr6zdbqzq7cgwdj1z9cjm91ib6l2") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-2.0.2 (c (n "wiggle-macro") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=2.0.2") (d #t) (k 0)))) (h "088cv21mmcxmljc4d9h09hck3jn5p9yx5qf51vxvghhqcrjc4ka9") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-1.0.2 (c (n "wiggle-macro") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=1.0.2") (d #t) (k 0)))) (h "1lzb4wc9lwxwvr8n6k1dxw4hp5s135ydw1y56xsnaj3c4j7z3lp7") (f (quote (("wiggle_metadata") ("wasmtime"))))))

(define-public crate-wiggle-macro-3.0.0 (c (n "wiggle-macro") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=3.0.0") (d #t) (k 0)))) (h "1v6lg4bfqzk4ymkjml2rrzk8ba18zy50kchhqk10ybjzz6xijhsl") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-3.0.1 (c (n "wiggle-macro") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=3.0.1") (d #t) (k 0)))) (h "1kgsn8xi80hfk6766b887b1hjzbfyw2v32xwp0pr8j1x42vhx0wb") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-4.0.0 (c (n "wiggle-macro") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=4.0.0") (d #t) (k 0)))) (h "0g3sq0bjjlhpv6r1ffr7vgg9481yx8gqlf3vmxnpyp1p0vqvi7wi") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-5.0.0 (c (n "wiggle-macro") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=5.0.0") (d #t) (k 0)))) (h "0zh6kjfkiybwgsqxkb9wl6yzygrbs25j0g9zn3dl3916g5hh22dp") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-6.0.0 (c (n "wiggle-macro") (v "6.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=6.0.0") (d #t) (k 0)))) (h "0zq69k469ac4cz6la7wxmas9ch99my8zyvyp8n6jv9ppamywkgpk") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-5.0.1 (c (n "wiggle-macro") (v "5.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=5.0.1") (d #t) (k 0)))) (h "1cm8zxg4kh3jawf4idwc2yh6liq46dc2m0v94gsglh26gn87rzgl") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-4.0.1 (c (n "wiggle-macro") (v "4.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=4.0.1") (d #t) (k 0)))) (h "0ry1p18bwk2lg5bhdwixfwh5mf8jvcw1xv1kd0xli0zg052zqv0r") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-6.0.1 (c (n "wiggle-macro") (v "6.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=6.0.1") (d #t) (k 0)))) (h "1a6psisv17f163df50ipv669073b4wm7k88f4qnkgj6xhyjk40vm") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-7.0.0 (c (n "wiggle-macro") (v "7.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=7.0.0") (d #t) (k 0)))) (h "05hvag90hqwd2h1rgj5qyds5axy2b3y5412cwbkp7w74rfjcdh3x") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-8.0.0 (c (n "wiggle-macro") (v "8.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=8.0.0") (d #t) (k 0)))) (h "182nyfi9rhjwyfn8n755klxn2srwxcpl5q8i737wifxlhm3rj8ba") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-6.0.2 (c (n "wiggle-macro") (v "6.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=6.0.2") (d #t) (k 0)))) (h "01s3c1zr1ip0i4j4xzflmpp8jspbj9hz1pqwa3xfz5jzcadd1lbx") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-8.0.1 (c (n "wiggle-macro") (v "8.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=8.0.1") (d #t) (k 0)))) (h "1k5nhd0wp1p1agjssfr2amhzqd7lkrqqng54hm639914rrzjw579") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-7.0.1 (c (n "wiggle-macro") (v "7.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=7.0.1") (d #t) (k 0)))) (h "0m9xahi89w8r4gi4q6xd4gmq40gxkalpqik4m9jpi1y8j889y64p") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-9.0.0 (c (n "wiggle-macro") (v "9.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=9.0.0") (d #t) (k 0)))) (h "031fx2mx0xnybq1cmydvp1lw7j58f0ldi4339gk9q1r6r8jq618j") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-9.0.1 (c (n "wiggle-macro") (v "9.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=9.0.1") (d #t) (k 0)))) (h "0xsbd84zx0armnxs7nq1xc6qwxmlnw5dywcms8liw6clb561pryp") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-9.0.2 (c (n "wiggle-macro") (v "9.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=9.0.2") (d #t) (k 0)))) (h "0pq1h6ckc58jgadv6l7kzfcpy2mjzllrlhqyavp5xyyv2xd003jj") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-9.0.3 (c (n "wiggle-macro") (v "9.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=9.0.3") (d #t) (k 0)))) (h "04d7vvk5fvlxz43614a8rpmmr2h8w8m9jxipxbxgvc0wzm4a8jfl") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-9.0.4 (c (n "wiggle-macro") (v "9.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=9.0.4") (d #t) (k 0)))) (h "0d6kk8cdfj8dh4xndg9n4gal6gpjg2xyayxjjhm1wy5sl12a78bc") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-10.0.0 (c (n "wiggle-macro") (v "10.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=10.0.0") (d #t) (k 0)))) (h "15cjpzyz3lm1wch66gimdf3bbd7sh35i9ls3bl0zw3hci2naf03c") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-10.0.1 (c (n "wiggle-macro") (v "10.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=10.0.1") (d #t) (k 0)))) (h "0bxa40f3p6sjb375dslm0ni2n1im0xiyxdjdghcsdk0y04d02jc6") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-11.0.0 (c (n "wiggle-macro") (v "11.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=11.0.0") (d #t) (k 0)))) (h "0hrqc08x87awj66z1pvid6wfbdc9rblwc4pq658c35ff6pc5hd32") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-11.0.1 (c (n "wiggle-macro") (v "11.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=11.0.1") (d #t) (k 0)))) (h "05wv3l1z2rm1fz9ln5an8vgj9g9dlnbn3ywvn0lfrynfzrf8z99c") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-12.0.0 (c (n "wiggle-macro") (v "12.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=12.0.0") (d #t) (k 0)))) (h "1811fzqpaa9v7m4s16k034kz4hsz15rkx0lpcjcnm2vby8gj7n2f") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-12.0.1 (c (n "wiggle-macro") (v "12.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=12.0.1") (d #t) (k 0)))) (h "0qakgnrg45rn9p5c23kfqs7amqgv0yd72sjamzvgjhdw83bzfxsn") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-10.0.2 (c (n "wiggle-macro") (v "10.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=10.0.2") (d #t) (k 0)))) (h "12a5yzk437xk3flfl1hc55qs3cd3s085kavzraz15wlhdcyswa2a") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-11.0.2 (c (n "wiggle-macro") (v "11.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=11.0.2") (d #t) (k 0)))) (h "1jj18dhsxhss2ssm5d2cz68bdaaya5fi6dx95wkqmrxkaq5xvb4k") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-12.0.2 (c (n "wiggle-macro") (v "12.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=12.0.2") (d #t) (k 0)))) (h "1rznbi36agkkpa6y0jj9mrf0ilyb1k03x63kjb6h8sv62wmb47mp") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-13.0.0 (c (n "wiggle-macro") (v "13.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=13.0.0") (d #t) (k 0)))) (h "1bvws4q4d76ra6lynvd17619iqawmzb3cky4lwn2nc69nwwdhwkx") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-14.0.0 (c (n "wiggle-macro") (v "14.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=14.0.0") (d #t) (k 0)))) (h "1b9vp3a2bf328lrzzxn32cw117zy1y8k7fxw0iqr5rcbz880d6az") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-14.0.1 (c (n "wiggle-macro") (v "14.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=14.0.1") (d #t) (k 0)))) (h "0iihgg6lhwy2h3ksk3zjfcd5izm192l0lwmyv4180cz4xj07pqka") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-13.0.1 (c (n "wiggle-macro") (v "13.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=13.0.1") (d #t) (k 0)))) (h "1vf2dcknw8dxxb6clpz0p567a8sma5jj4chanwj2ds38ph4kwvj7") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-14.0.2 (c (n "wiggle-macro") (v "14.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=14.0.2") (d #t) (k 0)))) (h "0l8glm7pad8qg8hjls717cnvhva3mjssxwwcywksalk9y9l1k62n") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-14.0.3 (c (n "wiggle-macro") (v "14.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=14.0.3") (d #t) (k 0)))) (h "0mcdqjbb8xl3gpcy8n6x2cwj4qip25lfy13iq8mgs7gmxis46rfz") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-14.0.4 (c (n "wiggle-macro") (v "14.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=14.0.4") (d #t) (k 0)))) (h "0bs5mbamsm1r5bf3lk0sg9xjqfgsg6jdp0ihbkb8jabjrvdj3wy6") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-15.0.0 (c (n "wiggle-macro") (v "15.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=15.0.0") (d #t) (k 0)))) (h "0cwfcfw4q9r9iwvsra0ak177wv7jfvwsj2ll9dnnism8qdd2y9jq") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-15.0.1 (c (n "wiggle-macro") (v "15.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=15.0.1") (d #t) (k 0)))) (h "0bdh43n6blk87b8mqlr9g5f95dgr3amspwi710rvnqbzqq0gx2f1") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-16.0.0 (c (n "wiggle-macro") (v "16.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=16.0.0") (d #t) (k 0)))) (h "02vqg19w1cml3pvznq20jw6pkjbql8cmg6pf74alishp3b0ixbii") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-17.0.0 (c (n "wiggle-macro") (v "17.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=17.0.0") (d #t) (k 0)))) (h "10yfrswjz4i7ckihqqjxr2pbznjimpg5dbpvc5fk5a2v6w9893gs") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-17.0.1 (c (n "wiggle-macro") (v "17.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=17.0.1") (d #t) (k 0)))) (h "06vgwyrkk6fpsfcdxkpx79xrhx3hj1p3m5ypkqxm39q2yhvkpfip") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-18.0.0 (c (n "wiggle-macro") (v "18.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=18.0.0") (d #t) (k 0)))) (h "1v6bchxpccdw7ynb2parflxacd1wj37jxhi7irbck69a42a5hwc9") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-18.0.1 (c (n "wiggle-macro") (v "18.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=18.0.1") (d #t) (k 0)))) (h "1b5igz7vznw4zii4fhdzvyd4xcaaq214fxvkab0vl1xf0qgjz7ly") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-17.0.2 (c (n "wiggle-macro") (v "17.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=17.0.2") (d #t) (k 0)))) (h "073rh61g1yd4wrn518n3vz70zpk2nrb0jrbz0bh97rj6gdmlypjh") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-18.0.2 (c (n "wiggle-macro") (v "18.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=18.0.2") (d #t) (k 0)))) (h "1ic660wiickj9ngs24n40a91b3dmv36gdblvvv35vwicl59yz64l") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-18.0.3 (c (n "wiggle-macro") (v "18.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=18.0.3") (d #t) (k 0)))) (h "0n2rjpb4yll4vc6hs3s51iwvl98f4fyl1gx2y517in50q78k2gyq") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-19.0.0 (c (n "wiggle-macro") (v "19.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=19.0.0") (d #t) (k 0)))) (h "1bnwv0lz49mpgpkb7982a9sz1rqqj3n0497bn81k24fhpinq2bai") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-19.0.1 (c (n "wiggle-macro") (v "19.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=19.0.1") (d #t) (k 0)))) (h "0xgg6i938bcymfw36k2rl0bcs5sfrx6syvhczjb116c5xryhi7d6") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-18.0.4 (c (n "wiggle-macro") (v "18.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=18.0.4") (d #t) (k 0)))) (h "19a81zvs0w1ydmr0xf8pqsv9aswynj5wx9c6mb1iwgbh6b1kzr4k") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-19.0.2 (c (n "wiggle-macro") (v "19.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=19.0.2") (d #t) (k 0)))) (h "1ryvpxrdyzm2p40njnqjysi01vj3ba4jy11smamrhx2h6vg15nxc") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-17.0.3 (c (n "wiggle-macro") (v "17.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=17.0.3") (d #t) (k 0)))) (h "1j2pszs6hh5fla6qbcplnxvrbrcrcrh6yair99gmk26ppwlisy2a") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-20.0.0 (c (n "wiggle-macro") (v "20.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=20.0.0") (d #t) (k 0)))) (h "150ri70pfykdhms4i33ayldn1v8zji55vxbcxlyr1wigxwb282wh") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-20.0.1 (c (n "wiggle-macro") (v "20.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=20.0.1") (d #t) (k 0)))) (h "0p1fzq16mcfz4hvkvy7vr01wfalrhdkl2sclar4i9q66nvsga5cq") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-20.0.2 (c (n "wiggle-macro") (v "20.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=20.0.2") (d #t) (k 0)))) (h "1nx3aqg1rywfgmnwbz7wcnmpvlg5bzxx9h4iqh0n9c9aawj13hxl") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-21.0.0 (c (n "wiggle-macro") (v "21.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=21.0.0") (d #t) (k 0)))) (h "021gzkymygpm9k863yb527bsqjmx35sq2pxrhzzpmhk675aj8z8i") (f (quote (("wiggle_metadata"))))))

(define-public crate-wiggle-macro-21.0.1 (c (n "wiggle-macro") (v "21.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)) (d (n "wiggle-generate") (r "=21.0.1") (d #t) (k 0)))) (h "0sxi046krjfkk02kvfvq51rvb1azkp2skld5vc333s9c8jsd4iqb") (f (quote (("wiggle_metadata"))))))

