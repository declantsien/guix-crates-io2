(define-module (crates-io wi gg wiggle-test) #:use-module (crates-io))

(define-public crate-wiggle-test-0.1.0 (c (n "wiggle-test") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "wiggle-runtime") (r "^0.1.0") (d #t) (k 0)))) (h "1vbikiiagqk8fyz1857lliy69mf89zwgb6szmlgvyqc7v2w5xjav")))

(define-public crate-wiggle-test-0.13.0 (c (n "wiggle-test") (v "0.13.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "wiggle-runtime") (r "^0.13.0") (d #t) (k 0)))) (h "0j4z52dvaq864znpnsdzwhwprb2w8afsh5mmgfqlng2vcqziwzhm")))

(define-public crate-wiggle-test-0.15.0 (c (n "wiggle-test") (v "0.15.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 0)) (d (n "wiggle") (r "^0.15.0") (d #t) (k 0)))) (h "13vwscmnaf7yqsgwqxwq8ifmwfghvd0h7hkdvl2hfqmbj7m3jlbb")))

