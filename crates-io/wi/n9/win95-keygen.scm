(define-module (crates-io wi n9 win95-keygen) #:use-module (crates-io))

(define-public crate-win95-keygen-0.1.0 (c (n "win95-keygen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v1j1llvhlvgvj0iigfb04ys9wi90n4bkv7x7rdzbdbsqsi5y9y7")))

(define-public crate-win95-keygen-0.1.1 (c (n "win95-keygen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vhdc3m6131w05jqlbqz7rcy5jsl1s8zmvrkdsyl0fpccng806cj")))

(define-public crate-win95-keygen-0.2.0 (c (n "win95-keygen") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1lldhpia7ryxmws4nj21k7jqgxnv9d8l5fx9mjfl620gwblrzjb7")))

