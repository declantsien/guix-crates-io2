(define-module (crates-io wi n9 win9x-sync) #:use-module (crates-io))

(define-public crate-win9x-sync-0.1.0 (c (n "win9x-sync") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "vc6-sys") (r "^0.1") (d #t) (k 0)))) (h "0zv0jy8qxb8q6dq7azswapy6zywrsaxd3vscnf5a8r2j6kcrclx0") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

(define-public crate-win9x-sync-0.1.1 (c (n "win9x-sync") (v "0.1.1") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "vc6-sys") (r "^0.1") (d #t) (k 0)))) (h "02nvzmia02xix4lnfancngsgq021mgidr7q2g9wd7f4y37s5ar0x") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

(define-public crate-win9x-sync-0.2.0 (c (n "win9x-sync") (v "0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "vc6-sys") (r "^0.1") (d #t) (k 0)))) (h "16xlvf43ig1f2l92sr0drz8kcra7ibm6nfs8rslvdiilb4vgrqj0") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

