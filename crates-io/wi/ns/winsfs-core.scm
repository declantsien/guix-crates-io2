(define-module (crates-io wi ns winsfs-core) #:use-module (crates-io))

(define-public crate-winsfs-core-0.1.0 (c (n "winsfs-core") (v "0.1.0") (d (list (d (n "angsd-saf") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "1ri2yhdnph9gj5ipk7fh7ljcnj0frmfmmwpn4xw73jr9nznhcsgb") (f (quote (("default" "angsd-saf/libdeflate")))) (r "1.62")))

