(define-module (crates-io wi ns winsen-co2-sensor) #:use-module (crates-io))

(define-public crate-winsen-co2-sensor-0.1.0 (c (n "winsen-co2-sensor") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0pnflyk2vhs6plsqcj576y38yx6bivz1q2z2dpmh5f4jzlc1spyg") (f (quote (("std") ("experimental"))))))

