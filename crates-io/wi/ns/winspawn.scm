(define-module (crates-io wi ns winspawn) #:use-module (crates-io))

(define-public crate-winspawn-0.1.0 (c (n "winspawn") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.7") (f (quote ("macros" "rt" "io-util"))) (d #t) (k 2)) (d (n "tokio-anon-pipe") (r "^0.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)) (d (n "winspawn-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1r6m4jylkj8z7cvi3scxfxml27d1crg03fm9gk6nq6jjkj6cyisi")))

(define-public crate-winspawn-0.1.1 (c (n "winspawn") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.7") (f (quote ("macros" "rt" "io-util"))) (d #t) (k 2)) (d (n "tokio-anon-pipe") (r "^0.1.1") (d #t) (k 2)) (d (n "windows") (r "^0.13.0") (d #t) (k 0)) (d (n "windows") (r "^0.13.0") (d #t) (k 1)) (d (n "winspawn-macro") (r "^0.1.0") (d #t) (k 0)))) (h "12qb5n4sbakgbksh6w66hvnyrgw7da16nazpzgi6wk5ad5ndgx2i")))

