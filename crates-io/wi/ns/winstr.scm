(define-module (crates-io wi ns winstr) #:use-module (crates-io))

(define-public crate-winstr-0.0.0 (c (n "winstr") (v "0.0.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("ntdef" "wtypes" "wtypesbase" "oleauto"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0mw5qp8p9bav02j9rp9khca3yw7f3hgiidrf8hnyngrg79h2500v") (f (quote (("display") ("default" "display"))))))

(define-public crate-winstr-0.0.1 (c (n "winstr") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.0") (f (quote ("ntdef" "wtypes" "wtypesbase" "oleauto"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winstr-macros") (r "^0.0.1") (d #t) (k 0)))) (h "0503a8sf0il7v7n4xd96yp6gf3xpg0g0r0ldjwaq21yr0lbvd0l6") (f (quote (("display") ("default" "bstr" "display") ("bstr" "winstr-macros/bstr"))))))

(define-public crate-winstr-0.0.2 (c (n "winstr") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.0") (f (quote ("ntdef" "wtypes" "wtypesbase" "oleauto"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winstr-macros") (r "^0.0.1") (d #t) (k 0)))) (h "0yhz5xj0322hxqqc510a2m21aw4n7xqznrdan41r9jvx55mln0lh") (f (quote (("display") ("default" "bstr" "display") ("bstr" "winstr-macros/bstr"))))))

