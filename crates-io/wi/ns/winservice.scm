(define-module (crates-io wi ns winservice) #:use-module (crates-io))

(define-public crate-winservice-0.1.0 (c (n "winservice") (v "0.1.0") (h "0mjxgzpqcbl4qzwr8nlc9cw4y5wcyxd3bah0cwl1mzppi9xpfsan")))

(define-public crate-winservice-0.1.1 (c (n "winservice") (v "0.1.1") (h "0zi733482j9zap9r0sz4h14zq6w8m66pv3m5y1q0bn8kcchiy317")))

