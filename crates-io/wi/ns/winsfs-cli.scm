(define-module (crates-io wi ns winsfs-cli) #:use-module (crates-io))

(define-public crate-winsfs-cli-0.7.0 (c (n "winsfs-cli") (v "0.7.0") (d (list (d (n "angsd-saf") (r "^0.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (f (quote ("stderr"))) (k 0)) (d (n "winsfs-core") (r "^0.1") (d #t) (k 0)))) (h "0lqn8q1jr87f7fsnpgvamj8nnrspdfacbb4mbl8p820zmpbmpyfp") (r "1.62")))

