(define-module (crates-io wi lt wilton_rusty) #:use-module (crates-io))

(define-public crate-wilton_rusty-0.3.0 (c (n "wilton_rusty") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0macmaz5w4mn58qs8aqjn4dg6l2w7bv3jk6dh4cbqh55ny9qvvkf")))

(define-public crate-wilton_rusty-0.3.1 (c (n "wilton_rusty") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16gslpav698b93rkqjawrz8lxcczsvmsxdb60y6h4x1kvy8lnr2i")))

