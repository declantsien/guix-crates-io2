(define-module (crates-io wi lt wilton_rust) #:use-module (crates-io))

(define-public crate-wilton_rust-0.1.0 (c (n "wilton_rust") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mfpkiwq5vb1f749az0vab2vjvc7q7qaj6kldycxzs1fp1g8wxqi")))

(define-public crate-wilton_rust-0.2.0 (c (n "wilton_rust") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10pq5dr45zlvq1vkphkrwbaz1skz7sgz35a41w1kadiq54dg0vwv")))

