(define-module (crates-io wi de wide-str) #:use-module (crates-io))

(define-public crate-wide-str-0.1.0 (c (n "wide-str") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "wide-str-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0ac3988fzmyrz10iqrvax9v9zzzm96a6g27wxyzjvv9bp184m29f")))

