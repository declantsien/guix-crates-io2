(define-module (crates-io wi de wide-literals) #:use-module (crates-io))

(define-public crate-wide-literals-0.1.0 (c (n "wide-literals") (v "0.1.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qr4k1i74g79g1p6pv2vdcr2fd2ab0qcbj5qll3pnym6wazf7bi3")))

(define-public crate-wide-literals-0.2.0 (c (n "wide-literals") (v "0.2.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1qq647f6nj4ml8z9l6r6y4apif3mhm68mp9k4snrzjq89g5d0r3h")))

