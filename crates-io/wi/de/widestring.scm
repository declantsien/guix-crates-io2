(define-module (crates-io wi de widestring) #:use-module (crates-io))

(define-public crate-widestring-0.1.0 (c (n "widestring") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 2)))) (h "1zg3qwhpg8k0ac72lswxzvkbb04ghgaspyyzb9ff3qz6vhzjxvcj")))

(define-public crate-widestring-0.2.0 (c (n "widestring") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 2)))) (h "11brniysddgxccgzrz8n9jh3pnj99w1jipdryczikndyhbmj8hfg")))

(define-public crate-widestring-0.2.1 (c (n "widestring") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 2)))) (h "0cf9h6qhx6mrlql1rad75a3aaf91ph0llfw0fcyp5cdjga1p7cfm")))

(define-public crate-widestring-0.2.2 (c (n "widestring") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 2)) (d (n "winapi") (r "^0.2.5") (d #t) (k 2)))) (h "1nyacxk2cbyrmn8vlra4z9ns2818qn178yshkhcd5qqj5r670mvi")))

(define-public crate-widestring-0.3.0 (c (n "widestring") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "108phf854bzmqg04cxgxy68gh7r8qr7slqrnz1251gwgllp944m2")))

(define-public crate-widestring-0.4.0 (c (n "widestring") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "1dhx6dndjsz1y7c9w06922412kdxyrrkqblvggm76mh8z17hxz7g")))

(define-public crate-widestring-0.4.1 (c (n "widestring") (v "0.4.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "06j64bv2b5g07khvzysx9x4kwazvbg2m9nz8hf4nl7zygp337ic9") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-widestring-0.4.2 (c (n "widestring") (v "0.4.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "13565qy4jhpg4x0xw8mwxzzsh0p8c93p5208lh6kpwp0q01y6qx7") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.4.3 (c (n "widestring") (v "0.4.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "0z3ba8qrxb62vpfgk7n2xs2grm9kdaj1cz4q8s0gs8fx8h0r8s61") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.5.0 (c (n "widestring") (v "0.5.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "1dkm41c8a3zszqszljr062x05imqc5smp5zfzqgf9r0v0nan7ghm") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.5.1 (c (n "widestring") (v "0.5.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "10qrilijh1qzw362mvd4nsz3vv32dxx530vk41hkcx8hah22z20p") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-1.0.0-beta.1 (c (n "widestring") (v "1.0.0-beta.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "13yfb89c84az4ggg9z2c4jfin75c6hbm194lb7wfs1x753lfzwg6") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

(define-public crate-widestring-1.0.0 (c (n "widestring") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "1a1pmaj7m9csh8c9ffhp8rf6y5c4fr1kxy9ff7wvqa6d7wnvvfmm") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-widestring-1.0.1 (c (n "widestring") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "0hz4z7ynzmgkg75y74s5ywx7vilzdj2n8j7174c63apipymv3sx5") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-widestring-1.0.2 (c (n "widestring") (v "1.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "1a11qxmqf8jhh0vbyb6cc614d9qdqsh01r5bqnivn5pc74gi8gv5") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.58")))

(define-public crate-widestring-1.1.0 (c (n "widestring") (v "1.1.0") (d (list (d (n "debugger_test") (r "^0.1") (d #t) (k 2)) (d (n "debugger_test_parser") (r "^0.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 2)))) (h "048kxd6iykzi5la9nikpc5hvpp77hmjf1sw43sl3z2dcdrmx66bj") (f (quote (("std" "alloc") ("default" "std") ("debugger_visualizer" "alloc") ("alloc")))) (r "1.58")))

