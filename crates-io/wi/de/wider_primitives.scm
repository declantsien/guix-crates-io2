(define-module (crates-io wi de wider_primitives) #:use-module (crates-io))

(define-public crate-wider_primitives-0.0.5 (c (n "wider_primitives") (v "0.0.5") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)))) (h "12z1c8pngq90fzjkcscqmh4r5ssb95b30hjypg6qhpxr25nzp6hh") (f (quote (("unstable") ("internals-doc-visible"))))))

(define-public crate-wider_primitives-0.0.6 (c (n "wider_primitives") (v "0.0.6") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)))) (h "16271zyimbrqa8ykijl74mbgfzxhgygrvg65v1ac5jyp394kdp2f") (f (quote (("unstable") ("internals-doc-visible"))))))

(define-public crate-wider_primitives-0.0.7 (c (n "wider_primitives") (v "0.0.7") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1c0b3ygn262six7p0w9y9ch1a7hfsd4gxn6kglydhww9klzwh4l4") (f (quote (("unstable") ("internals-doc-visible"))))))

