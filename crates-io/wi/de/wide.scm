(define-module (crates-io wi de wide) #:use-module (crates-io))

(define-public crate-wide-0.0.0 (c (n "wide") (v "0.0.0") (h "1w97n940ipfxd0scb9w3swdijdi95zklcx035zyg13xcrhjn06cz")))

(define-public crate-wide-0.1.0 (c (n "wide") (v "0.1.0") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lokacore") (r "^0.3") (d #t) (k 0)))) (h "1gm3nmwfd7fpk4v5cxlvgq9z078ijaycjsz7i9as2jvfknp3d4bw") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.1.1 (c (n "wide") (v "0.1.1") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lokacore") (r "^0.3") (d #t) (k 0)))) (h "14b827wy58g54g1qqc40kspfqi7faimnhg59fckdhkggxbflh808") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.1.2 (c (n "wide") (v "0.1.2") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lokacore") (r "^0.3") (d #t) (k 0)))) (h "0xdgw9jd409rnrzqdnb2v67qm5ny3lxy981l2vmapx0g3q298jc6") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.0 (c (n "wide") (v "0.2.0") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lokacore") (r "^0.3") (d #t) (k 0)))) (h "08s57bccr5h40sm533hpiwp3plfgsjbwyq6yf64ayj74lz6d56s4") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.1 (c (n "wide") (v "0.2.1") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "lokacore") (r "^0.3") (d #t) (k 0)))) (h "1yhwkpiczx4g2c2gmcja3x0scrd0883j0dv2fczjajxz9lhrbpqq") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.2 (c (n "wide") (v "0.2.2") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1lmy951gfb0xx804j43l30gyzlqw0d0g2pzi2i36p2m5kzd1rqql") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.3 (c (n "wide") (v "0.2.3") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1f7m9pqfv780ygd3iw0lli9vkpixpcg7v1kbj3l3mybrxyk7a0c5") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.4 (c (n "wide") (v "0.2.4") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "0szvadwcf2crvnf86s036czhzwl9ifnwgbk4d6fkc77fkqr7fnck") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.5 (c (n "wide") (v "0.2.5") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1w1f4kxm2dhlwb0m36c7y4dk4imdqs0jpyxi1437bgbykipqrv0r") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2.6 (c (n "wide") (v "0.2.6") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1vrnad41grx4lf2jl0bzpc5pjd90yr3aysww8m5jkyxiqc7wf9n4") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.3.0 (c (n "wide") (v "0.3.0") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "02mgdhni5cbgnzjx8iv7mwvn5rb9yzxz1v2f57vg0hyp84k4a1xy") (f (quote (("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.3.1 (c (n "wide") (v "0.3.1") (d (list (d (n "bytemuck") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "0hjmy7mggvpyl451dsmdfb3gh9rafmwljy9dgcb4aq8s5p3p4snz") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.3.2 (c (n "wide") (v "0.3.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "0qc96phigpkix50r0y5jkc4qx4n7m90dk01a6hmx6kbqa0nbvkd3") (f (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.4.0 (c (n "wide") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "1pqw4gcrcmc8anqzzyx964rxjrq3sdr50p8nbclffprnid4kb5sc") (f (quote (("extern_crate_std") ("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.4.1 (c (n "wide") (v "0.4.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "04vxgwj0kx0xjfcv2mb29gxmhd12gxbl69451pwp3nhq9s73waby") (f (quote (("extern_crate_std") ("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.4.2 (c (n "wide") (v "0.4.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "0119wjllqbh5chfq46ddv95liqrrczxc6wy3dj5vvif49cyz18xr") (f (quote (("extern_crate_std") ("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.4.3 (c (n "wide") (v "0.4.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "1s289cpc60q42d510lj3cvcfazf9c11llqw79mni2z9vjwb473sj") (f (quote (("extern_crate_std") ("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.4.4 (c (n "wide") (v "0.4.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "1wcj8197sznaam4m8985ralhv76irrcrg5r1rqaj1b2l9qfx8xr5") (f (quote (("extern_crate_std") ("default") ("always_use_stable")))) (y #t)))

(define-public crate-wide-0.4.5 (c (n "wide") (v "0.4.5") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "19f2ajp4aa2x72jscl78j5kin35yadk943jzz6zg76b30dl40d59") (f (quote (("extern_crate_std") ("default") ("always_use_stable"))))))

(define-public crate-wide-0.4.6 (c (n "wide") (v "0.4.6") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)))) (h "0ad75vnzygj8qfcl1l9n4wi93xmqzvhqlpqn4hfayrwbn6wa69aq") (f (quote (("extern_crate_std") ("default") ("always_use_stable"))))))

(define-public crate-wide-0.5.0 (c (n "wide") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "17z3b6qapd7z0ppcrcmp2cpkwsgyg55aadxh9yqfhkghm308qxcw") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.1 (c (n "wide") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0c8gn6yy0pm4k47msir74igaz0ccy5y30dfjw8lm32v74fc24yz9") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.2 (c (n "wide") (v "0.5.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1id2l5rnygg1wk1fh58gib8y8sr55lli9p3ilh6cg9dnm5gi8427") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.3 (c (n "wide") (v "0.5.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0ardwlqfg8294lmpkqm7yppph5ayg69s2wfd6nvmjdgs6hcs9gy7") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.4 (c (n "wide") (v "0.5.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1h400rcsmbhprkp637paacyc7qg8czyfh9bpkdbwqrzw9mclnpyg") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.5 (c (n "wide") (v "0.5.5") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "04cj5vlw8mwgc3j5w428f1xjs00yr3mcknc1wqgxhay2xiv7rcl0") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.5.6 (c (n "wide") (v "0.5.6") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "12qq6w9g44vgmkq4dwyjwnfxf4za4f63mhz2qqzv8x1ckihky1qh") (f (quote (("std") ("default")))) (y #t)))

(define-public crate-wide-0.6.0 (c (n "wide") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "17ksq40bhpkzivzjm0xsrfhalcn9s7my4w0zsznr08gm56n4dxhq") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.6.1 (c (n "wide") (v "0.6.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "07qsxxqxwxg0cjak1741v0mvi5nh0ivyny254ki71442p6619yc7") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.6.2 (c (n "wide") (v "0.6.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1k1nnlr8nyh1hfgh0wkxkwi64vzqn0cs0v9ibfdsx196dgm8y74k") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.6.3 (c (n "wide") (v "0.6.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0s1gj6cmkw3609ixamzmaxdwpb8g19gj4rgyw1ijyir0cmk81w42") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.6.4 (c (n "wide") (v "0.6.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1vp1w74jrkhs19z3gg163bfli4a41l0s4cbxmmpr0cv3xxhg2l9c") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.6.5 (c (n "wide") (v "0.6.5") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.5") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "16yqf8xsvns21clg3rk8nljq4qnsfqg240smbjqa0z520k3fgfs6") (f (quote (("std") ("default"))))))

(define-public crate-wide-0.7.0 (c (n "wide") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1rr23wi8p96xynywl0vgjzgrs3m21vzhsz7hhkp57ybi8i4cz2fd") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.1 (c (n "wide") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "14y8js69x0hb4pkg249ahz5a4ncyx4hd0h614vd1jrjgjn759wnj") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.2 (c (n "wide") (v "0.7.2") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1vdgmx6drry5rgm5k4g2gkhnc2k7bj3cxwzpnm68sylz2f571klz") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.3 (c (n "wide") (v "0.7.3") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0qlb9w0j8xnaxybba2lvw83kkh2bkabzyf76zwgjnqr5sbqs4va7") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.4 (c (n "wide") (v "0.7.4") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0psw31mh21cn4w7i0klsz7law8p8iddsqir8x35cf6n3vb8s5axk") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.5 (c (n "wide") (v "0.7.5") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0p2jsbvir4x0f8slm54mqyhc04xin1298j7qirf6b8c94jnyqhdf") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.6 (c (n "wide") (v "0.7.6") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0v1nv46z2irdbw6qkscm5pahdaqdikbchv3vfwip5mll510hmzzy") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.7 (c (n "wide") (v "0.7.7") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1ln04lvrifvdf752ckgg2slkmicq54n3s0h15yj9v7p8vwm1vhdk") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.8 (c (n "wide") (v "0.7.8") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "08xjlgzpyl47v3llg8dp7iln7krqjbrv1rj4z55l6jb5kp2bd2dn") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.9 (c (n "wide") (v "0.7.9") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.6") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0rgzd485mp3265k7x0497plrjl3a89317n8fpz26pk7kf5m4kl2w") (f (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7.10 (c (n "wide") (v "0.7.10") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0a27g3w3bdwdja1inml69fzxxszj6f6vmylhjwm618nvw8iqc0a0") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.11 (c (n "wide") (v "0.7.11") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0bynfx8ibdmpb0ivkbiizm9l51cvp2bkc634y6h0nzpgcpx9yima") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.12 (c (n "wide") (v "0.7.12") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1615c1qk39kzrngx5w0w0r35gbv3zaxm0xf49c5qdwcgqgpypv7b") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.13 (c (n "wide") (v "0.7.13") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0hl2974x5f34vrpibqngm6dd97kwqdgzqb21jlqkdnikgfski2f6") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.14 (c (n "wide") (v "0.7.14") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "04qn4xcn294k37g2whfwxkrbvr12qgxjfpvinsgpi8gb8kb9265k") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.15 (c (n "wide") (v "0.7.15") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "071cl37d9ni0a4r8y1hjyr5vq0w0w3wf742lqagnfvi49xafrgl9") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.16 (c (n "wide") (v "0.7.16") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1ijv1m6pxk212iyc8yid2j6dw03c5z3ia3m4zv9d24czf4d8b8c1") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.17 (c (n "wide") (v "0.7.17") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "07z16ipyiqwn0z12skrgxlvvkq1l3z7mid0j9d8c1z83qv93j3hg") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.18 (c (n "wide") (v "0.7.18") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1dhbsfc66jry48qgnfy1av5qanrnnbg1vcgdddvnx7mghnggh9ar") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.19 (c (n "wide") (v "0.7.19") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0rbhdgflm9wwah74hmkhg41qjbxn39wgmifvllcdf1nyj10mkdma") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.20 (c (n "wide") (v "0.7.20") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "0g7g2n6cbp9nipyi7wd7yqz3ailwk8pb5773m61l2y1mrjj0bq11") (f (quote (("std") ("default" "std")))) (r "1.56")))

(define-public crate-wide-0.7.21 (c (n "wide") (v "0.7.21") (d (list (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "safe_arch") (r "^0.7") (f (quote ("bytemuck"))) (d #t) (k 0)))) (h "1z6pfk6pkxribm9d9ikbgjdx0ppfq3ss8r1hb8jkqgxhl54wg3fd") (f (quote (("std") ("default" "std")))) (r "1.56")))

