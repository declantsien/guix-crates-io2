(define-module (crates-io wi de widerwlock) #:use-module (crates-io))

(define-public crate-widerwlock-0.5.0 (c (n "widerwlock") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.6") (d #t) (k 2)) (d (n "parking_lot_core") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "19ici71qc1fs4sm9f0k5v5xzwcv0ww0fvn2rcf065ipjs5fp1hck")))

