(define-module (crates-io wi nt winter-utils) #:use-module (crates-io))

(define-public crate-winter-utils-0.0.0 (c (n "winter-utils") (v "0.0.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0jhh7jmamldz97sydbnckaczgwaqrbcdz8bvhsnkarqxzjhxfzlw") (f (quote (("concurrent" "rayon"))))))

(define-public crate-winter-utils-0.1.0 (c (n "winter-utils") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11") (f (quote ("ahash" "inline-more"))) (o #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1cqg4h9p12n00rvv8gkhaw6aripchcyz3kifi06pj9fy0j6klv7d") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std") ("alloc" "hashbrown"))))))

(define-public crate-winter-utils-0.2.0 (c (n "winter-utils") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "176sqh0l8yq8xmka0r191n36dw4gaj2x9bc8i1k063askl90r288") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std"))))))

(define-public crate-winter-utils-0.3.0 (c (n "winter-utils") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "16q6v5iaamcwd0s22vidn4hpz3b7v54l8ia5li6xn1f7r3cxmgkf") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.57")))

(define-public crate-winter-utils-0.4.0 (c (n "winter-utils") (v "0.4.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "02vxcbgqlpwyfki78ljzh5kjy0sns3vd6qpf4yy55b1r9zjqdb4f") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.60")))

(define-public crate-winter-utils-0.4.1 (c (n "winter-utils") (v "0.4.1") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1abfya57qrg2d2lb6jnvidzjx2w9k9maw0qfd5dy6j3q0cna6jrp") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.60")))

(define-public crate-winter-utils-0.4.2 (c (n "winter-utils") (v "0.4.2") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "05wl9w0gbpcq916amdbpnadjgryvwzqqnw98k6sr6n5alryby3bz") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.60")))

(define-public crate-winter-utils-0.5.0 (c (n "winter-utils") (v "0.5.0") (d (list (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1210w4nnq4qc8dvb2vgc0i5ib2yhkwzr0libhg7gi4a1wh2g7pfk") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.5.1 (c (n "winter-utils") (v "0.5.1") (d (list (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)))) (h "15b5nm26xw5gsmd2vrsiz54z3jjs2vzxrr2i1awgb3vfwl50xjxc") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.6.0 (c (n "winter-utils") (v "0.6.0") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0iwhyk451l5kjd30da0jsppi3g2bsrcjdbgv4rpanfi7jakya27l") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.6.1 (c (n "winter-utils") (v "0.6.1") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0z4c0khvp3s42dpf75pla0c6jhl24122fsq6sgxny3cfkrzvan5j") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.6.2 (c (n "winter-utils") (v "0.6.2") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0z7xjg04y20vxv691dygfxmkibwn008zlxwg8aivppz18cgy7gh8") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.6.3 (c (n "winter-utils") (v "0.6.3") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1q8dl2jhxwgbnlql7krl8zkjhicq38gd7z18m16fzfqh3gcncfci") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.6.4 (c (n "winter-utils") (v "0.6.4") (d (list (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)))) (h "0wcnfx06vdvx8am1zyqay9fv428grad2dabjl6jjmkhclnkgs0xr") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.67")))

(define-public crate-winter-utils-0.7.0 (c (n "winter-utils") (v "0.7.0") (d (list (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "14lra218qc6iqjqhjjkhrgmmmxpd2r8wsspalzpv68ikg5al14cy") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.73")))

(define-public crate-winter-utils-0.7.2 (c (n "winter-utils") (v "0.7.2") (d (list (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "13vvmld9nsf5fp0815kcxxkxvri8aasbsldyq6p3d195ri20r8y8") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.73")))

(define-public crate-winter-utils-0.7.3 (c (n "winter-utils") (v "0.7.3") (d (list (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "0wbab1b21c9330xn4rwbibqjia60qz6cmlqd0f6pn8i5jxav64mq") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.73")))

(define-public crate-winter-utils-0.7.4 (c (n "winter-utils") (v "0.7.4") (d (list (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "1s926q1vlvry31m2vv190214yyp2qvd5p9kb2isxqgr2w8a2x3z5") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.73")))

(define-public crate-winter-utils-0.8.0 (c (n "winter-utils") (v "0.8.0") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "0rfn9jsrkk33qq6srzf8rzdmqaasi3426an9v1qvz2ysp0zkb4pc") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.75")))

(define-public crate-winter-utils-0.8.1 (c (n "winter-utils") (v "0.8.1") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "1gg9dr87kpjpy52c0jyjycamd126ckcang7s47z4i47q4cbl9r0n") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.75")))

(define-public crate-winter-utils-0.8.2 (c (n "winter-utils") (v "0.8.2") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "0dphik3972gm5cr764vd2470grvq5fsf05n32qjqrm4g23qxivqz") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.75")))

(define-public crate-winter-utils-0.8.3 (c (n "winter-utils") (v "0.8.3") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "06xa6hb4cnagy4dnlsai3ivvzcrwmzxawd4h5n5fla4qbp2wf7m6") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.75")))

(define-public crate-winter-utils-0.8.4 (c (n "winter-utils") (v "0.8.4") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "1rw6rv6w1m07r5zh370dqdyadbbjqd5qkww40yld0vzsdv7zqvmb") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.75")))

(define-public crate-winter-utils-0.9.0 (c (n "winter-utils") (v "0.9.0") (d (list (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (o #t) (d #t) (k 0)))) (h "174jxdr1368jihv8xp79ardvvda9fbr4ss9pslm9f5rm5j07mds2") (f (quote (("std") ("default" "std") ("concurrent" "rayon" "std")))) (r "1.78")))

