(define-module (crates-io wi nt wintheme) #:use-module (crates-io))

(define-public crate-wintheme-0.1.0 (c (n "wintheme") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.45.0") (f (quote ("Win32_UI_Controls" "Win32_Foundation" "Win32_UI_WindowsAndMessaging" "Win32_Graphics_Gdi" "Win32_System_LibraryLoader"))) (t "cfg(windows)") (k 0)) (d (n "autocfg") (r "^1.1.0") (d #t) (t "cfg(windows)") (k 1)))) (h "0fy8k7j67pkdbfbm8hlcsxs60ssgssihzwxxkwz2ch6n2fnvwkcj")))

