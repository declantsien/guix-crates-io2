(define-module (crates-io wi nt wintrap) #:use-module (crates-io))

(define-public crate-wintrap-0.1.0 (c (n "wintrap") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "0ig8lij2bmcqmrd57a94wpjh62blx5d1qnjsa6l373kzwp98piy4")))

(define-public crate-wintrap-0.1.1 (c (n "wintrap") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "05aswg3d06v0z37wr16dixzfdnyj8p9a2lwkl1grv99siigk2zgq")))

(define-public crate-wintrap-0.2.0 (c (n "wintrap") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "12vl5ww2gsc20g90iw2sg4p8dvplwf33gca6ycnb8vqndr5r79yd")))

(define-public crate-wintrap-0.2.1 (c (n "wintrap") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "06i5hhf1y2avp914j1mdhsfjilagvbygp2vham5k9m54lzrkxl2y")))

(define-public crate-wintrap-0.3.0 (c (n "wintrap") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "0453dqay4jmhp0d7b0w9m2sd4pinfx2j8yv32sbmmzh9flbcd7pq")))

(define-public crate-wintrap-0.3.1 (c (n "wintrap") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "windef" "consoleapi" "winuser" "errhandlingapi" "libloaderapi" "winbase"))) (d #t) (k 0)))) (h "1rbsf5zyyd02z5grlqz1f3picyzq37afcpl6gmbf9h0jg05anbls")))

