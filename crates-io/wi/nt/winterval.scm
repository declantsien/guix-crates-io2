(define-module (crates-io wi nt winterval) #:use-module (crates-io))

(define-public crate-winterval-0.0.0 (c (n "winterval") (v "0.0.0") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1lkvjrprmvm8skgnvx7mjhj9wydzwgs3qjpxndy5b58d47mpyhw2")))

(define-public crate-winterval-0.0.1 (c (n "winterval") (v "0.0.1") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1wfspvn2lzznqqv5hkaj83w6ja880lxcvrvh8ivg0qh56bghi42p")))

(define-public crate-winterval-0.1.0 (c (n "winterval") (v "0.1.0") (d (list (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "1dkb16lgnpsrf0l7nh0s18sawhrv3hagj4s9jndxmiw61srl31q5")))

(define-public crate-winterval-0.1.1 (c (n "winterval") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0g5vl1c8d3z59gcqx3v5qy1d8vj04srd61pvy8v8fxg0xn06w9b9")))

(define-public crate-winterval-0.1.3 (c (n "winterval") (v "0.1.3") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "11hcw5xdl9fydc2g5i35s67j74y9sn9bqgwcimddx3dkrfn0crd4")))

(define-public crate-winterval-0.1.4 (c (n "winterval") (v "0.1.4") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1ag9yb4i702xgvy3aw50ig6j8q75s37y9q1l52n40xk09khapadz") (f (quote (("use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-winterval-0.1.5 (c (n "winterval") (v "0.1.5") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1al01g2570l22mz2j8cwx3m9c61r8y08y8s1hy5ccz9pwhbpvsm5") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

(define-public crate-winterval-0.2.0 (c (n "winterval") (v "0.2.0") (d (list (d (n "interval_adapter") (r "^0.2.0") (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "1cy3f0z3zbasn4l6gcfdkdblyz2sbkhj9mvsnyyfvjaihambbmza") (f (quote (("use_alloc") ("no_std") ("full") ("default"))))))

(define-public crate-winterval-0.2.3 (c (n "winterval") (v "0.2.3") (d (list (d (n "interval_adapter") (r "^0.2.3") (f (quote ("enabled"))) (k 0)) (d (n "test_tools") (r "^0.1.5") (k 2)))) (h "12hmpc7pbjmfnspfhrrynz65fkrm2zybjc5hb8x7mw45kb2gzxrj") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "interval_adapter/enabled") ("default" "enabled"))))))

(define-public crate-winterval-0.3.0 (c (n "winterval") (v "0.3.0") (d (list (d (n "interval_adapter") (r "^0.3.0") (f (quote ("enabled"))) (k 0)) (d (n "test_tools") (r "^0.3.0") (d #t) (k 2)))) (h "1hdb41vacrd6g7f78l26sqvy3brq8gk0zfrsqcxyar5wsq4vjsrn") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled" "interval_adapter/enabled") ("default" "enabled"))))))

