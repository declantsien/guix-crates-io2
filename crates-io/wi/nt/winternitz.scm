(define-module (crates-io wi nt winternitz) #:use-module (crates-io))

(define-public crate-winternitz-0.1.1 (c (n "winternitz") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1d3xhzrgd4pba2laas1yl1bn75srvjb74gxypf4d1saxsfq6fmxd")))

(define-public crate-winternitz-0.1.2 (c (n "winternitz") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0l84ccwdx0f87rhga833laqnlh01l64fddp6drajzh9vlkjdhq0z")))

(define-public crate-winternitz-0.1.3 (c (n "winternitz") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "02q3i05sf58c63kq4xzsb4ysj70ap9w9wx1v1ywrqb8scfdgqrdl") (f (quote (("poqua-token"))))))

(define-public crate-winternitz-0.1.4 (c (n "winternitz") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0lk37r8vhzsx6p9q3wcjkxsjizdy57s0y20w8445861g8s4l8q39") (f (quote (("poqua-token"))))))

