(define-module (crates-io wi nt winterfell) #:use-module (crates-io))

(define-public crate-winterfell-0.0.0 (c (n "winterfell") (v "0.0.0") (d (list (d (n "prover") (r "^0.1") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.1") (k 0) (p "winter-verifier")))) (h "0lyj85yhxkb36xjbc23ayv11dylxp3wmypmbq1i0vx4dd09drdnx") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std") ("alloc" "prover/alloc" "verifier/alloc"))))))

(define-public crate-winterfell-0.1.0 (c (n "winterfell") (v "0.1.0") (d (list (d (n "prover") (r "^0.1") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.1") (k 0) (p "winter-verifier")))) (h "104vdnivxf5r8ns7a2ndgapw86vn3vmc1l1adhl16xmmkcrkx11g") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std") ("alloc" "prover/alloc" "verifier/alloc"))))))

(define-public crate-winterfell-0.2.0 (c (n "winterfell") (v "0.2.0") (d (list (d (n "prover") (r "^0.2") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.2") (k 0) (p "winter-verifier")))) (h "1wggidbm1kl5npqhzmk46gz318wjmsm4lsxzl3jc3z7qbf8xilvd") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std"))))))

(define-public crate-winterfell-0.3.0 (c (n "winterfell") (v "0.3.0") (d (list (d (n "prover") (r "^0.3") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.3") (k 0) (p "winter-verifier")))) (h "0gqyyj42c1bnv9fsh0hlb7x31c1dkqnvvkg2j48d23h9fw3n07fg") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.57")))

(define-public crate-winterfell-0.4.0 (c (n "winterfell") (v "0.4.0") (d (list (d (n "prover") (r "^0.4") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.4") (k 0) (p "winter-verifier")))) (h "1qd2zs9c777dmba2l33xd0mvvf4cba91bi7rs4asznk7q3vkg304") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.60")))

(define-public crate-winterfell-0.4.1 (c (n "winterfell") (v "0.4.1") (d (list (d (n "prover") (r "^0.4.1") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.4.1") (k 0) (p "winter-verifier")))) (h "1rgsa7s58x4abc6iw599ajb0jnx1jncvawv3xvfzvrdcv0pdh127") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.60")))

(define-public crate-winterfell-0.4.2 (c (n "winterfell") (v "0.4.2") (d (list (d (n "prover") (r "^0.4.2") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.4.2") (k 0) (p "winter-verifier")))) (h "1h33ir05f09g1ql3995yj331i1z4pvg6p43yj6ympdyh7jbvfa1k") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.60")))

(define-public crate-winterfell-0.5.0 (c (n "winterfell") (v "0.5.0") (d (list (d (n "prover") (r "^0.5") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.5") (k 0) (p "winter-verifier")))) (h "13kbk0d9xbn8529x8fay080rly5si9ararzw4byh77p9hjmchj4n") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.5.1 (c (n "winterfell") (v "0.5.1") (d (list (d (n "prover") (r "^0.5") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.5") (k 0) (p "winter-verifier")))) (h "1n7y6m53p072786g0b4g8fnyg3rc57kq3kb44z332nw9h1x0yzy6") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.6.0 (c (n "winterfell") (v "0.6.0") (d (list (d (n "prover") (r "^0.6") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.6") (k 0) (p "winter-verifier")))) (h "1m59rcpy841zhzq1j41f8vqgwfgs4ygg0nzphh9cavfwzhib1lzh") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.6.1 (c (n "winterfell") (v "0.6.1") (d (list (d (n "prover") (r "^0.6") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.6") (k 0) (p "winter-verifier")))) (h "1m919v1k2hxdsxdww3i4sja3a7fs8c06h5266v63c24hj27j1xvg") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.6.2 (c (n "winterfell") (v "0.6.2") (d (list (d (n "prover") (r "^0.6") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.6") (k 0) (p "winter-verifier")))) (h "1giaxvi5c2p835iv6si77fdc65g7answ80pzz92qjf5s4r7g4nbc") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.6.3 (c (n "winterfell") (v "0.6.3") (d (list (d (n "prover") (r "^0.6") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.6") (k 0) (p "winter-verifier")))) (h "06i3iazg9yf7ix4gvswck8f56sd88b1c2pwz0yypmmm70yq3w0rg") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.6.4 (c (n "winterfell") (v "0.6.4") (d (list (d (n "prover") (r "^0.6") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.6") (k 0) (p "winter-verifier")))) (h "1ddwvxrrk233nicj2mmyr54qlndmg5r822lgsibfy7ikiqcn3hbk") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.67")))

(define-public crate-winterfell-0.7.0 (c (n "winterfell") (v "0.7.0") (d (list (d (n "prover") (r "^0.7") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.7") (k 0) (p "winter-verifier")))) (h "1dv0wrd5zfnzp2y2fjx0kmi01v8q295psdr24vwisbrr1yc89hrl") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.73")))

(define-public crate-winterfell-0.7.1 (c (n "winterfell") (v "0.7.1") (d (list (d (n "prover") (r "^0.7") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.7") (k 0) (p "winter-verifier")))) (h "07fjwn6xn6scwakvi5y187ikscy59mbdc932wvglg9nyr7rs7x8v") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.73")))

(define-public crate-winterfell-0.8.0 (c (n "winterfell") (v "0.8.0") (d (list (d (n "prover") (r "^0.8") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.8") (k 0) (p "winter-verifier")))) (h "05crlivw28vzk642vvkyrqvadf60shd203ls4kpflcqdxysrg4wb") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.75")))

(define-public crate-winterfell-0.8.1 (c (n "winterfell") (v "0.8.1") (d (list (d (n "prover") (r "^0.8") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.8") (k 0) (p "winter-verifier")))) (h "1cs7m69zlr0y58k40gjvw703pnl7397kb1vvqxg9c798p0y6akvs") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.75")))

(define-public crate-winterfell-0.8.3 (c (n "winterfell") (v "0.8.3") (d (list (d (n "prover") (r "^0.8") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.8") (k 0) (p "winter-verifier")))) (h "0n4k9q2n8cb085az71djaz0lyaczznmbzrdha2lnn4pxq76nl2my") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std")))) (r "1.75")))

(define-public crate-winterfell-0.9.0 (c (n "winterfell") (v "0.9.0") (d (list (d (n "air") (r "^0.9") (k 0) (p "winter-air")) (d (n "prover") (r "^0.9") (k 0) (p "winter-prover")) (d (n "verifier") (r "^0.9") (k 0) (p "winter-verifier")))) (h "0nqyxarjq54rhs2bwk4bi17rj9ig187132iyfi83jy1dzv2il581") (f (quote (("std" "prover/std" "verifier/std") ("default" "std") ("concurrent" "prover/concurrent" "std") ("async" "prover/async")))) (r "1.78")))

