(define-module (crates-io wi nt winternitz-ots) #:use-module (crates-io))

(define-public crate-winternitz-ots-0.1.0 (c (n "winternitz-ots") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.13") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)))) (h "0z683m6gxl435bsxa1d883s63cha0l3vkb15rqq50a0hf2fxsx3l")))

(define-public crate-winternitz-ots-0.1.1 (c (n "winternitz-ots") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "043sslhf023zxrjdb1jlmxlf12qxqa4mz6yhfcxzpby2rghy5mp4")))

(define-public crate-winternitz-ots-0.2.0 (c (n "winternitz-ots") (v "0.2.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "getrandom") (r "^0.1.14") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1z4zd9ffsj8az47z7bhx1c5h5d4vzadkfppxd1w844wjin5xvyhj")))

(define-public crate-winternitz-ots-0.3.0 (c (n "winternitz-ots") (v "0.3.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06cmqg3aslxpzqkgy3qlymqcdmyari1v95c7wvhadmnq8a0b7683")))

