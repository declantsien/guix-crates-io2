(define-module (crates-io wi nt wintun-sys) #:use-module (crates-io))

(define-public crate-wintun-sys-0.1.0 (c (n "wintun-sys") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "0z7v8xvakyl7yhnnjhsaw25v6jabbsvcbsqjjq453g3874lgxi7a") (r "1.60")))

(define-public crate-wintun-sys-0.2.0 (c (n "wintun-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "libloading") (r "^0.7.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_NetworkManagement_Ndis"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1vj1pcs6vxb2r76c3fmdwwfjry73acbf7z4phas7k28fl217n6k7") (r "1.64")))

