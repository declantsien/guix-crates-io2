(define-module (crates-io wi nt winter-maybe-async) #:use-module (crates-io))

(define-public crate-winter-maybe-async-0.9.0 (c (n "winter-maybe-async") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0p478m0ddh04iyi8whj9x699x843n0f9r1mk17l0vrfx3hbg9q3w") (f (quote (("async")))) (r "1.75")))

