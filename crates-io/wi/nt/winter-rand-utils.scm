(define-module (crates-io wi nt winter-rand-utils) #:use-module (crates-io))

(define-public crate-winter-rand-utils-0.2.0 (c (n "winter-rand-utils") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.2") (d #t) (k 0) (p "winter-utils")))) (h "0yclbqh68n42vpg7l6dpxsgs89ln3w77yqp3809jq0g8insm93xz")))

(define-public crate-winter-rand-utils-0.3.0 (c (n "winter-rand-utils") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.3") (d #t) (k 0) (p "winter-utils")))) (h "0qc67k1darmiyz8xxq97apgd9mds5x16gm9xajy7xl833cfyixl4") (r "1.57")))

(define-public crate-winter-rand-utils-0.4.0 (c (n "winter-rand-utils") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.4") (d #t) (k 0) (p "winter-utils")))) (h "08w06a7xi6hwmk99yy477x4n292s54gybl2nlij910m3gs9q4bw8") (r "1.60")))

(define-public crate-winter-rand-utils-0.4.1 (c (n "winter-rand-utils") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.4.1") (d #t) (k 0) (p "winter-utils")))) (h "0i491p9913p61cnjcivf0xx1jqk1yrpw9lq0n7mmcmzdvvnaml7x") (r "1.60")))

(define-public crate-winter-rand-utils-0.4.2 (c (n "winter-rand-utils") (v "0.4.2") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.4.2") (d #t) (k 0) (p "winter-utils")))) (h "0ggndy39fyfi3h6k14jpgbd352h8rpnn61mk1djsm0l2h3k9968c") (r "1.60")))

(define-public crate-winter-rand-utils-0.5.0 (c (n "winter-rand-utils") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.5") (d #t) (k 0) (p "winter-utils")))) (h "1arzifnmii25fc3dyixmrgca42gr1g1n2qd81gia8cmz7nxjswpw") (r "1.67")))

(define-public crate-winter-rand-utils-0.5.1 (c (n "winter-rand-utils") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.5") (d #t) (k 0) (p "winter-utils")))) (h "1kz9blgi8dmkyvgjr6xh8xjg5339qp94sbm21lr6lzrqwgfigd27") (r "1.67")))

(define-public crate-winter-rand-utils-0.6.0 (c (n "winter-rand-utils") (v "0.6.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.6") (d #t) (k 0) (p "winter-utils")))) (h "10i1azx57p553jj9sj11x0vi4cp8snzfah5rvdafdhl9da9qjl5d") (r "1.67")))

(define-public crate-winter-rand-utils-0.6.1 (c (n "winter-rand-utils") (v "0.6.1") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.6") (d #t) (k 0) (p "winter-utils")))) (h "1xv8w0pl7kvcdlm56byyd0amnhgv51y3mm8j0zmjzh1hrxzsla31") (r "1.67")))

(define-public crate-winter-rand-utils-0.6.2 (c (n "winter-rand-utils") (v "0.6.2") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.6") (d #t) (k 0) (p "winter-utils")))) (h "0rv87398mr6qykh7ik7bdqhhz5d9skhmg7g5llyhy8g4xbqihg3n") (r "1.67")))

(define-public crate-winter-rand-utils-0.6.3 (c (n "winter-rand-utils") (v "0.6.3") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.6") (d #t) (k 0) (p "winter-utils")))) (h "0ns5b1246vp9z109fr2wz6cip4pl77alb16i1rv8559w505fi3rk") (r "1.67")))

(define-public crate-winter-rand-utils-0.6.4 (c (n "winter-rand-utils") (v "0.6.4") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.6") (d #t) (k 0) (p "winter-utils")))) (h "1g4z7abvb5p17fjj5rd9qs5nkgghagwnbfmdhpif21yhp9cks2h0") (r "1.67")))

(define-public crate-winter-rand-utils-0.7.0 (c (n "winter-rand-utils") (v "0.7.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.7") (d #t) (k 0) (p "winter-utils")))) (h "1ja9s1svvkd4qd8qmrpjr10gzxar8mcjhdhbjyc89rvx6hd0f2i7") (r "1.73")))

(define-public crate-winter-rand-utils-0.8.0 (c (n "winter-rand-utils") (v "0.8.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.8") (d #t) (k 0) (p "winter-utils")))) (h "1ncn4gd5vgcbm3khwfnwwsvi05n9vnrxlw2a02ixqcac2l0xnjd8") (r "1.75")))

(define-public crate-winter-rand-utils-0.8.1 (c (n "winter-rand-utils") (v "0.8.1") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.8") (d #t) (k 0) (p "winter-utils")))) (h "003krazq4ykrkgnfzliannx4yyjnqy2qhrgn843jvgbvpia5fah4") (r "1.75")))

(define-public crate-winter-rand-utils-0.8.3 (c (n "winter-rand-utils") (v "0.8.3") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.8") (d #t) (k 0) (p "winter-utils")))) (h "10r5dg3i33jkqaksx9q3iyp74z85p1r9v9jpx5920i48wr8cw6cb") (r "1.75")))

(define-public crate-winter-rand-utils-0.9.0 (c (n "winter-rand-utils") (v "0.9.0") (d (list (d (n "rand") (r "^0.8") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "utils") (r "^0.9") (d #t) (k 0) (p "winter-utils")))) (h "10a1c8l2milqn0sffyjw19f8aq0x8pzmha41i5nk235b074jgf7j") (r "1.78")))

