(define-module (crates-io wi nk wink) #:use-module (crates-io))

(define-public crate-wink-0.1.0 (c (n "wink") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)))) (h "07daijhc4yxfw2lw8znickqb8rpacrl0qhzps29qcph8riqm37ci")))

(define-public crate-wink-0.2.0 (c (n "wink") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)))) (h "03nxanwn6j6h5habf02r4gi61y35z5h9ikd5hn2b9m9ppw68m4l1")))

(define-public crate-wink-0.2.1 (c (n "wink") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)))) (h "05pn1ws9pvdr296w2i48vc6y0s6ylmb0lr08d7lrsqk82bryvr3r")))

(define-public crate-wink-0.2.2 (c (n "wink") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)))) (h "02zmrc6ypigq7l4mxj7xh9ih9q288za527dk8nl0578ywjdsl6ka")))

(define-public crate-wink-0.2.3 (c (n "wink") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)))) (h "0fpc8sqkz8plnnhaxrj5a204x3cfmwks5v1ggwz0hq8955ssspyx")))

(define-public crate-wink-0.2.4 (c (n "wink") (v "0.2.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)) (d (n "normpath") (r "^0.3.0") (d #t) (k 0)))) (h "08h6mcs1amys11cz6xpm7wf182mrqwa6kp4jy21rkx6bd8vpkgq2")))

(define-public crate-wink-0.3.0 (c (n "wink") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "wrap_help" "unicode"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)) (d (n "normpath") (r "^0.3.0") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "0s015kymqb58kd11s27h1yjv2lbqsdkrjfzbn86wshdcgjsxnf9c")))

(define-public crate-wink-0.3.1 (c (n "wink") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0") (f (quote ("cargo" "wrap_help" "unicode"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.0.0") (d #t) (k 1)) (d (n "junction") (r "^0.2.0") (d #t) (k 0)) (d (n "normpath") (r "^0.3.0") (d #t) (k 0)) (d (n "wild") (r "^2.0.4") (d #t) (k 0)))) (h "1k2z2kp54g8657mfkiq786kz1324z59b08d78dgmf4jhpmafvxsp")))

