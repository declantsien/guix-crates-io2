(define-module (crates-io wi nf winfsp-rs) #:use-module (crates-io))

(define-public crate-winfsp-rs-0.0.0 (c (n "winfsp-rs") (v "0.0.0") (d (list (d (n "registry") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "widestring") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_WindowsProgramming" "Win32_System_Console" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)) (d (n "winfsp-sys") (r "^0.0.0") (d #t) (k 0)))) (h "0hls1s044d511vvkicc20ydy5cghs99kzjklzzgq01v4ljwbnrv5") (f (quote (("system" "winfsp-sys/system") ("debug")))) (y #t)))

