(define-module (crates-io wi nf winforms-sys) #:use-module (crates-io))

(define-public crate-winforms-sys-0.0.1 (c (n "winforms-sys") (v "0.0.1") (h "0ndp9617w9prc1lzd8kc4crzmyip749pjx6pjsksn7gj575nsn9g") (y #t)))

(define-public crate-winforms-sys-0.0.2 (c (n "winforms-sys") (v "0.0.2") (h "1zfwhskvx694sbfmd94q3dpf38krrbj5m5p0f614yfh53h72rmf6")))

