(define-module (crates-io wi nf winfw) #:use-module (crates-io))

(define-public crate-winfw-0.1.1 (c (n "winfw") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s69j8iaih2zzri4jg82gc9px4wc5030klwsl39dirbaqpxnk4g5")))

(define-public crate-winfw-0.1.2 (c (n "winfw") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qxpbfkrh6n21ayzxjq2cv1lriybf9wwmnixllkcgaf908xn28zf")))

(define-public crate-winfw-0.1.3 (c (n "winfw") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "01136bwjhl0q6gb3518ib753sryyzcc2nzsdx9lv2bap4j0plakg")))

(define-public crate-winfw-0.1.4 (c (n "winfw") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mdkqh0x7s5xxl73hckvamggnz1dx0f7r44nm79ivpjrkxkkd912")))

(define-public crate-winfw-0.1.5 (c (n "winfw") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xaz4rbw225zaskykvnzn8wmwmmv01sr1l51fl7bs0s150w02h1n")))

(define-public crate-winfw-0.1.6 (c (n "winfw") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04xqyh4cap2lzbfg35p7gz3k161n7whhjszapn7pxvk0ij7fv5pd")))

(define-public crate-winfw-0.1.7 (c (n "winfw") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18zd6fz2pn9xisxgnlzrwxraxisqnkxjp1zdy0js2vkjnnl4b2qv")))

(define-public crate-winfw-0.1.8 (c (n "winfw") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ng1a8p3lcpl41y1icdvr7s0b1n2z8024mqs7ayghngihpf6phy0")))

