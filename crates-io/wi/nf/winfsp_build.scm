(define-module (crates-io wi nf winfsp_build) #:use-module (crates-io))

(define-public crate-winfsp_build-0.1.0 (c (n "winfsp_build") (v "0.1.0") (h "0pp1blkq0dqknn1q2hns8qkdvf205x7kjbz2abx3fyv42sa9415f")))

(define-public crate-winfsp_build-0.1.1 (c (n "winfsp_build") (v "0.1.1") (h "0ginmnj3076cx5174c8vf2vjmjrgw1bf9r2j26k09h23kh444dyj")))

