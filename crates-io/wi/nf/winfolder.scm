(define-module (crates-io wi nf winfolder) #:use-module (crates-io))

(define-public crate-winfolder-0.1.0 (c (n "winfolder") (v "0.1.0") (d (list (d (n "guid") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1l6qkhjzg87r1jqiqv60yhnv3dlvib860cm1h322bp0bvkrif36x")))

(define-public crate-winfolder-0.1.1 (c (n "winfolder") (v "0.1.1") (d (list (d (n "guid") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "ole32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "shell32-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "10irz87ggh2mnfylpgwbxpmwdaqqzk97fkfxaq7dqf5jhcqhfdxi")))

