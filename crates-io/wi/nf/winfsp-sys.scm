(define-module (crates-io wi nf winfsp-sys) #:use-module (crates-io))

(define-public crate-winfsp-sys-0.0.0 (c (n "winfsp-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (d #t) (k 1)))) (h "0l2049dpqmk3538lsgip51c2nr9gnw9zv2rj2niv8pbm653m508w") (f (quote (("system"))))))

(define-public crate-winfsp-sys-0.1.0 (c (n "winfsp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (d #t) (k 1)))) (h "0kcsk8rjc6grdmnj74m1mzmm41aa0yyv976j1zwsg97yfhxk0jn6") (f (quote (("system"))))))

(define-public crate-winfsp-sys-0.1.1+winfsp-1.11 (c (n "winfsp-sys") (v "0.1.1+winfsp-1.11") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (o #t) (d #t) (k 1)))) (h "0imk2wc64m1wrsw7sw9zph953kkz8hcivsjysb8zzv6j16f2ym16") (f (quote (("system" "registry"))))))

(define-public crate-winfsp-sys-0.1.2+winfsp-1.11 (c (n "winfsp-sys") (v "0.1.2+winfsp-1.11") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (o #t) (d #t) (k 1)))) (h "0vldacdmjaxvnkdyv7dn3rmhba5hj1mjixfwqiaybvhw4cf4rm6r") (f (quote (("system" "registry"))))))

(define-public crate-winfsp-sys-0.1.3+winfsp-1.11 (c (n "winfsp-sys") (v "0.1.3+winfsp-1.11") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (o #t) (d #t) (k 1)))) (h "1q166pa4xxi5mcscs1g7xnw71r4c4hwlxaahgc32m7dm0sx853wr") (f (quote (("system" "registry") ("docsrs"))))))

(define-public crate-winfsp-sys-0.1.4+winfsp-1.11 (c (n "winfsp-sys") (v "0.1.4+winfsp-1.11") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.2") (o #t) (d #t) (k 1)))) (h "0n4amddsbijb7r7n7yp80vv1z7wrsmf1k2598rn33n4zhfbicn1q") (f (quote (("system" "registry") ("docsrs"))))))

(define-public crate-winfsp-sys-0.2.0+winfsp-2.0 (c (n "winfsp-sys") (v "0.2.0+winfsp-2.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "registry") (r "^1.2.3") (o #t) (d #t) (k 1)))) (h "09zvw55r2rm0x2hncqxpsx54acyqw8pjffbbx431mn6p1jmlwwxz") (f (quote (("system" "registry") ("docsrs")))) (y #t)))

(define-public crate-winfsp-sys-0.2.1+winfsp-2.0 (c (n "winfsp-sys") (v "0.2.1+winfsp-2.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "registry") (r "^1.2.3") (o #t) (d #t) (k 1)))) (h "1myacr9ilqxvcif9wzppyygx1m8zyjsf0yw8jkgqkw2mbhrj6bir") (f (quote (("system" "registry") ("docsrs"))))))

(define-public crate-winfsp-sys-0.2.2+winfsp-2.0 (c (n "winfsp-sys") (v "0.2.2+winfsp-2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "registry") (r "^1.2.3") (o #t) (d #t) (k 1)))) (h "17ymvfac6wc9s3qf2h7rypyxbn6yw4q48xmbsf2c86hj513dkayv") (f (quote (("system" "registry") ("docsrs"))))))

