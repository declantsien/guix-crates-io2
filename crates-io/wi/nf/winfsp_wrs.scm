(define-module (crates-io wi nf winfsp_wrs) #:use-module (crates-io))

(define-public crate-winfsp_wrs-0.2.0 (c (n "winfsp_wrs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)) (d (n "windows-sys") (r "^0.52.0") (f (quote ("Wdk" "Wdk_Storage" "Wdk_Storage_FileSystem" "Win32_Foundation" "Win32_Security" "Win32_Security_Authorization" "Win32_Storage_FileSystem" "Win32_System_Console" "Win32_System_LibraryLoader" "Win32_System_Registry" "Win32_System_WindowsProgramming"))) (d #t) (k 0)) (d (n "winfsp_wrs_sys") (r "^0.2.0") (d #t) (k 0)))) (h "0r0yiphr87iyfxk96v7wyxj6vkb6w8a1bnnxn2jmmn8wpnfpwl30") (f (quote (("icon" "windows-sys/Win32_System_IO" "windows-sys/Win32_UI_Shell") ("default") ("debug"))))))

