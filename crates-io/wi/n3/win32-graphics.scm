(define-module (crates-io wi n3 win32-graphics) #:use-module (crates-io))

(define-public crate-win32-graphics-0.0.2 (c (n "win32-graphics") (v "0.0.2") (h "14bv0ncs23bzy872j5665gipzrs14r2kdbxvm7rbs09chv2ppa5y") (r "1.46")))

(define-public crate-win32-graphics-0.1.0 (c (n "win32-graphics") (v "0.1.0") (h "1bicrnq4n0nb6mnp2hgmdg8552qbg3kr2nq9618968pqm5lg8bja")))

