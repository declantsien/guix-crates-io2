(define-module (crates-io wi n3 win32-error) #:use-module (crates-io))

(define-public crate-win32-error-0.9.0 (c (n "win32-error") (v "0.9.0") (d (list (d (n "kernel32-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "17izf3zx6279hm8vkr4mrslxh6n487fdbhdv42mn4i4nri7221wv")))

