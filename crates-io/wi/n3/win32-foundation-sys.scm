(define-module (crates-io wi n3 win32-foundation-sys) #:use-module (crates-io))

(define-public crate-win32-foundation-sys-0.0.2 (c (n "win32-foundation-sys") (v "0.0.2") (h "14mmipy78r7mlhrmxxy4i5dllaygsavrhwb8sy4ln84s1a5cadsp") (r "1.46")))

(define-public crate-win32-foundation-sys-0.1.0 (c (n "win32-foundation-sys") (v "0.1.0") (h "1fnzv0mm1mdci7awm80iqrp8sxmfr5vv021yiljzzf6nq7xprc1p")))

