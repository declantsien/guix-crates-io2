(define-module (crates-io wi n3 win32-networking-sys) #:use-module (crates-io))

(define-public crate-win32-networking-sys-0.0.2 (c (n "win32-networking-sys") (v "0.0.2") (h "1mxf1pmhr1y259h8kkm7h32rlin986520lpjx46vapr1jyxwr2n9") (r "1.46")))

(define-public crate-win32-networking-sys-0.1.0 (c (n "win32-networking-sys") (v "0.1.0") (h "008cgnnnwdrziqjdpgb3lzwjrxv7ydqgx1w4i2zncbwnk1dp50hz")))

