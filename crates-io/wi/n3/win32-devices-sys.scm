(define-module (crates-io wi n3 win32-devices-sys) #:use-module (crates-io))

(define-public crate-win32-devices-sys-0.0.2 (c (n "win32-devices-sys") (v "0.0.2") (h "07m7q3rxpijpazh6jdjhv3cpzaaqqml711rv514l8p1gh1r090b2") (r "1.46")))

(define-public crate-win32-devices-sys-0.1.0 (c (n "win32-devices-sys") (v "0.1.0") (h "048166ach4ql6xf48bm7h3c8irnadz40gzr6knzxri49ardh5r65")))

