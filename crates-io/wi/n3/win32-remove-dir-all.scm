(define-module (crates-io wi n3 win32-remove-dir-all) #:use-module (crates-io))

(define-public crate-win32-remove-dir-all-0.1.0 (c (n "win32-remove-dir-all") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (t "cfg(windows)") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (t "cfg(windows)") (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("shellapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05ah7jxjsp3pkbij3b978m42gb9cpas5in6g2rc8mrj9hmwmc647") (f (quote (("symlink_tests") ("property_system_api" "winapi/libloaderapi" "winapi/objbase" "winapi/shobjidl_core" "winapi/winerror") ("default" "property_system_api"))))))

