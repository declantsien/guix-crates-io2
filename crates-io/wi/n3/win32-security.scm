(define-module (crates-io wi n3 win32-security) #:use-module (crates-io))

(define-public crate-win32-security-0.0.2 (c (n "win32-security") (v "0.0.2") (h "0vk7mv4vqbrfhs3swiaysyri67hcq19yg69fcfxq5gns591n3abs") (r "1.46")))

(define-public crate-win32-security-0.1.0 (c (n "win32-security") (v "0.1.0") (h "0wka2r8sf87cx3pbb7sypb2wjkbdx923fc0w3bif8qxhlw2i684g")))

