(define-module (crates-io wi n3 win32-gaming) #:use-module (crates-io))

(define-public crate-win32-gaming-0.0.2 (c (n "win32-gaming") (v "0.0.2") (h "05pk9ri7l9qrmdq9837464b6wflp07lmwcfiiqbyrv66yzbkijks") (r "1.46")))

(define-public crate-win32-gaming-0.1.0 (c (n "win32-gaming") (v "0.1.0") (h "1q10qdk96cnq02b1v2hsdqz8s0933k0nxh767vn3sl93lpq72j48")))

