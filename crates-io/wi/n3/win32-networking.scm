(define-module (crates-io wi n3 win32-networking) #:use-module (crates-io))

(define-public crate-win32-networking-0.0.2 (c (n "win32-networking") (v "0.0.2") (h "0h7sr8fg4lpl6gf1wsazv3ww20l3ap650a7chvgd22fz5lj6n6ir") (r "1.46")))

(define-public crate-win32-networking-0.1.0 (c (n "win32-networking") (v "0.1.0") (h "0g81gfy2sx4p7yf2v8jppxspx0s7rnbhlypp4p3wxh32lwy3dddy")))

