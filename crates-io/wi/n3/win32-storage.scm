(define-module (crates-io wi n3 win32-storage) #:use-module (crates-io))

(define-public crate-win32-storage-0.0.2 (c (n "win32-storage") (v "0.0.2") (h "0sjvqajsvazsg2kq3sa0afzifh0d5yhqzrxg7g6zbnwwkmivfvkf") (r "1.46")))

(define-public crate-win32-storage-0.1.0 (c (n "win32-storage") (v "0.1.0") (h "10q4j1zp5mrlqi2svsdaapci7gskvgn7xr89q8xylfzbjkc4qciv")))

