(define-module (crates-io wi n3 win32-ui-sys) #:use-module (crates-io))

(define-public crate-win32-ui-sys-0.0.2 (c (n "win32-ui-sys") (v "0.0.2") (h "1ss106csrsm3k68rdk6qn46anrrypnbn1j52rm4i85mnpp6ikamb") (r "1.46")))

(define-public crate-win32-ui-sys-0.1.0 (c (n "win32-ui-sys") (v "0.1.0") (h "0fqmlnca055lp2lpk049w7r8rzzdfxjxam1xrbcyp4mfh4hlahys")))

