(define-module (crates-io wi n3 win32-data) #:use-module (crates-io))

(define-public crate-win32-data-0.0.2 (c (n "win32-data") (v "0.0.2") (h "1dx9ywsysx2rfh7prz2cbcrwrbjjzxfw930qwjmsc8hz84bcc3hk") (r "1.46")))

(define-public crate-win32-data-0.1.0 (c (n "win32-data") (v "0.1.0") (h "0vzqqj9aq9x6mby2827j8cvn1hnylvf2qnh2syg08kka2ccmma3z")))

