(define-module (crates-io wi n3 win32-management) #:use-module (crates-io))

(define-public crate-win32-management-0.0.2 (c (n "win32-management") (v "0.0.2") (h "0172ac6hm81y6l0v5jv03ksw8l4r9nv76fzcxvrk97ai5ggcdclm") (r "1.46")))

(define-public crate-win32-management-0.1.0 (c (n "win32-management") (v "0.1.0") (h "0qq9d6ddpqrwj59dhrflwr56g13pm6i9vq0rsc4n9rvzk7f003dk")))

