(define-module (crates-io wi n3 win32-gaming-sys) #:use-module (crates-io))

(define-public crate-win32-gaming-sys-0.0.2 (c (n "win32-gaming-sys") (v "0.0.2") (h "0ky2xk3wy01jd4gxa27y827409nibcz710yhs5xqqn4sxmfv9w4v") (r "1.46")))

(define-public crate-win32-gaming-sys-0.1.0 (c (n "win32-gaming-sys") (v "0.1.0") (h "0dvzj5b72lj4lcbkki1iz02gaz10ily4ndqk55gvw4mqmjgacvzs")))

