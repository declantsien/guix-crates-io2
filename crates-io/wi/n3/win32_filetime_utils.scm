(define-module (crates-io wi n3 win32_filetime_utils) #:use-module (crates-io))

(define-public crate-win32_filetime_utils-0.2.0 (c (n "win32_filetime_utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.15") (d #t) (k 0)))) (h "0690bgycg77wj6x7nqcddzcf5zljkwskhiav2kbshivhlvqq0h7x")))

(define-public crate-win32_filetime_utils-0.2.1 (c (n "win32_filetime_utils") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.15") (d #t) (k 0)) (d (n "win32_filetime_utils") (r "^0.2.0") (d #t) (k 0)))) (h "168nsn1i1wscnnl5mirq27d4sh9mx1if8va5z97hkri9jm14x3yq")))

