(define-module (crates-io wi n3 win32-ai) #:use-module (crates-io))

(define-public crate-win32-ai-0.0.2 (c (n "win32-ai") (v "0.0.2") (h "1smrv2799i521qgk1i0zlkvz740z694vk43iq94cb5drrj06yssq") (r "1.46")))

(define-public crate-win32-ai-0.1.0 (c (n "win32-ai") (v "0.1.0") (h "1qs0lp6r7ngq4az4114q6p1dxapv0abjnagg0hssa1mqcrnlbij6")))

