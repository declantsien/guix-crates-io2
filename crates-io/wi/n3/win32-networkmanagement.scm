(define-module (crates-io wi n3 win32-networkmanagement) #:use-module (crates-io))

(define-public crate-win32-networkmanagement-0.0.2 (c (n "win32-networkmanagement") (v "0.0.2") (h "0nyhjxy958zzwvrd6pwicvppxwsqwsgxsrh51qkl5cwc4fpzy46q") (r "1.46")))

(define-public crate-win32-networkmanagement-0.1.0 (c (n "win32-networkmanagement") (v "0.1.0") (h "10ynycyhhdarpv99mmawbnhrs5rk1aklsxim1089fjcy8ygdk78g")))

