(define-module (crates-io wi n3 win32-management-sys) #:use-module (crates-io))

(define-public crate-win32-management-sys-0.0.2 (c (n "win32-management-sys") (v "0.0.2") (h "029i0bwzdvj1si37bq46a05g8q7ylxd0bl2px4pii0m1sssqfp64") (r "1.46")))

(define-public crate-win32-management-sys-0.1.0 (c (n "win32-management-sys") (v "0.1.0") (h "10yngf88x9g5v7nw74vlh7lcq229dv7ckv0hjip5ss8k9zyk51c0")))

