(define-module (crates-io wi n3 win32-web-sys) #:use-module (crates-io))

(define-public crate-win32-web-sys-0.0.2 (c (n "win32-web-sys") (v "0.0.2") (h "183xp60kpyikk8vwc400lwnvbpmfwl7dgwbggcisaqjqcp4dvwm1") (r "1.46")))

(define-public crate-win32-web-sys-0.1.0 (c (n "win32-web-sys") (v "0.1.0") (h "0wwndc2f3q1zph07snxwwmmnrsf6ksvvs10qb761mqwj0rval6pl")))

