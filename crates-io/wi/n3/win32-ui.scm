(define-module (crates-io wi n3 win32-ui) #:use-module (crates-io))

(define-public crate-win32-ui-0.0.2 (c (n "win32-ui") (v "0.0.2") (h "030j7znhjwhfgqz34ipia190kfdcz5wrwqrvvablvcx1c4bv7l0m") (r "1.46")))

(define-public crate-win32-ui-0.1.0 (c (n "win32-ui") (v "0.1.0") (h "0frnfab6a53clwiiqr7wcw0cdhhfgq5s0r5mm26nbc4r0h8jd573")))

