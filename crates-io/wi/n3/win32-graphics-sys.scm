(define-module (crates-io wi n3 win32-graphics-sys) #:use-module (crates-io))

(define-public crate-win32-graphics-sys-0.0.2 (c (n "win32-graphics-sys") (v "0.0.2") (h "1316z95c2x54z9qq5ldn85rg0ww63yhf47szakf68pzirqj2y52g") (r "1.46")))

(define-public crate-win32-graphics-sys-0.1.0 (c (n "win32-graphics-sys") (v "0.1.0") (h "167zijnja8yra1bx6sd97wlqvclwll7hribg1hw7hbp7ysfx8gpc")))

