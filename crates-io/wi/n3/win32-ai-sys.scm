(define-module (crates-io wi n3 win32-ai-sys) #:use-module (crates-io))

(define-public crate-win32-ai-sys-0.0.1 (c (n "win32-ai-sys") (v "0.0.1") (h "0zc71llsricj0d7vawgn7jgrkinxhiw552mgayp4lyiklfkmyx6l") (r "1.46")))

(define-public crate-win32-ai-sys-0.0.2 (c (n "win32-ai-sys") (v "0.0.2") (h "0xkq2jfc6i89hjh17d2kgcylic8srdb6pvgrf2lzw5fyjsni67ww") (r "1.46")))

(define-public crate-win32-ai-sys-0.1.0 (c (n "win32-ai-sys") (v "0.1.0") (h "0fhd7779qnxi49cm7w5hahcl2sx7v4pdr0kyyja3ckgn7mx2may2")))

