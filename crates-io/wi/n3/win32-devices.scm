(define-module (crates-io wi n3 win32-devices) #:use-module (crates-io))

(define-public crate-win32-devices-0.0.2 (c (n "win32-devices") (v "0.0.2") (h "0hdjwmlphd5xwfg2k9jp4gz2z0ncj88qm8q5h5y72r9gv1wighb0") (r "1.46")))

(define-public crate-win32-devices-0.1.0 (c (n "win32-devices") (v "0.1.0") (h "0h500g12i1x7hq4vlwxvdrgvqlcc7sdd0zd23k2qyjzrw6gnxg58")))

