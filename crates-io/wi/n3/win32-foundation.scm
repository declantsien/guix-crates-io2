(define-module (crates-io wi n3 win32-foundation) #:use-module (crates-io))

(define-public crate-win32-foundation-0.0.2 (c (n "win32-foundation") (v "0.0.2") (h "1z5k14caa05hrz1zs08jsn8cia3j1kr1mp4vr0k8rfzb44nkbzg6") (r "1.46")))

(define-public crate-win32-foundation-0.1.0 (c (n "win32-foundation") (v "0.1.0") (h "1xgi95fsh5bbgkfxp7yidzxynmgha9748l20lcn6kpfsk2bfk0vx")))

