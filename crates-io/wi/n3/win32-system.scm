(define-module (crates-io wi n3 win32-system) #:use-module (crates-io))

(define-public crate-win32-system-0.0.2 (c (n "win32-system") (v "0.0.2") (h "1qxi9irmzgznbcmp8xcbfnb06ay8sn2as1jn2yqg84xsz4w6ig71") (r "1.46")))

(define-public crate-win32-system-0.1.0 (c (n "win32-system") (v "0.1.0") (h "0afhvbd0aka01259mfc9968cwr5y2cvl8ysasyjbbk85078sd2mq")))

