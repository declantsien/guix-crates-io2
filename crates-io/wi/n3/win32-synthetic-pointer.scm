(define-module (crates-io wi n3 win32-synthetic-pointer) #:use-module (crates-io))

(define-public crate-win32-synthetic-pointer-0.1.0 (c (n "win32-synthetic-pointer") (v "0.1.0") (d (list (d (n "enumflags2") (r "^0.7.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_UI_Controls" "Win32_UI_Input_Pointer" "Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "168zn1w6rrymh65hqj6hg0gwhbjm0kzlpbwipaqk4jm008csbj4r") (y #t)))

