(define-module (crates-io wi n3 win32job) #:use-module (crates-io))

(define-public crate-win32job-1.0.0 (c (n "win32job") (v "1.0.0") (d (list (d (n "rusty-fork") (r "^0.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (d #t) (k 0)))) (h "1x4kardq4l4g4y0kwgvapjjhydgp5swcipkqh73rppis55si74if")))

(define-public crate-win32job-1.0.1 (c (n "win32job") (v "1.0.1") (d (list (d (n "rusty-fork") (r "^0.2.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (d #t) (k 0)))) (h "0da2sqxndfranjhv86akk3kr7jrjmds5fd66d8afk2vcdw9i8alv")))

(define-public crate-win32job-1.0.2 (c (n "win32job") (v "1.0.2") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (d #t) (k 0)))) (h "0hkx14gdv02vmsa58l71w8hmwf4z3g3qfi0wcg5sl0bkkkp6v6cj")))

(define-public crate-win32job-1.0.3 (c (n "win32job") (v "1.0.3") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_JobObjects" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)))) (h "172gg0inlxc4k71zlhmxg4408i5wzm7i8nsw2bi88vr2gq8dspbi") (y #t)))

(define-public crate-win32job-1.0.4 (c (n "win32job") (v "1.0.4") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (d #t) (k 0)))) (h "1pam2l6rjxgzh30fzrcv0ypcicb3w5cqscbssbx37fcg8vd1bsdi")))

(define-public crate-win32job-2.0.0 (c (n "win32job") (v "2.0.0") (d (list (d (n "rusty-fork") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_JobObjects" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (d #t) (k 0)))) (h "0g1blsb7ixrqjicykx82rvrymcydlsdgfwzb61x88iyrazsinasv")))

