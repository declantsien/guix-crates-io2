(define-module (crates-io wi n3 win32-media) #:use-module (crates-io))

(define-public crate-win32-media-0.0.2 (c (n "win32-media") (v "0.0.2") (h "1aqlamxn6mmykmi3vg4sxm17w9z13m0mx07jqmkivkh6wv378cdm") (r "1.46")))

(define-public crate-win32-media-0.1.0 (c (n "win32-media") (v "0.1.0") (h "0pjgxz48klfxza94g1gs36369i1ls2imxyym5vybk15zwvlh66vr")))

