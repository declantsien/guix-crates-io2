(define-module (crates-io wi n3 win32-data-sys) #:use-module (crates-io))

(define-public crate-win32-data-sys-0.0.2 (c (n "win32-data-sys") (v "0.0.2") (h "1msv5c1nzjm2c3sv7chprbd1lczd1asz628i739yvyksi9sv1isx") (r "1.46")))

(define-public crate-win32-data-sys-0.1.0 (c (n "win32-data-sys") (v "0.1.0") (h "1jyn6xmjvkn9qigc5nv8nvv69n2l071zpzzyli3sz2hakziilc7f")))

