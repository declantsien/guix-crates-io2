(define-module (crates-io wi n3 win32-web) #:use-module (crates-io))

(define-public crate-win32-web-0.0.2 (c (n "win32-web") (v "0.0.2") (h "182y6anamhjkq8534frja8ly872dx68lgq1i0j2v5fpng7m06pic") (r "1.46")))

(define-public crate-win32-web-0.1.0 (c (n "win32-web") (v "0.1.0") (h "0310hh4ii7r64x6z8g17sqrvkw1hpfmzhpm5542s5z6a1fqjgwk3")))

