(define-module (crates-io wi n3 win32-globalization) #:use-module (crates-io))

(define-public crate-win32-globalization-0.0.2 (c (n "win32-globalization") (v "0.0.2") (h "0vwn4qhd3yz8z5a6mv8psbpy4px6s8wj2ll6kmb2anbfki9shg3h") (r "1.46")))

(define-public crate-win32-globalization-0.1.0 (c (n "win32-globalization") (v "0.1.0") (h "1dk5rfgfk1pwgfh1w7l2r766innjnpn91wkmfgbgj3jnqc3kyqi8")))

