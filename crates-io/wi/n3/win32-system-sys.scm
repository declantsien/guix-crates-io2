(define-module (crates-io wi n3 win32-system-sys) #:use-module (crates-io))

(define-public crate-win32-system-sys-0.0.2 (c (n "win32-system-sys") (v "0.0.2") (h "0lnzqlv94j56pn3rc17q3nmc92z84xwl89fbvss3m81dbv60f3yw") (r "1.46")))

(define-public crate-win32-system-sys-0.1.0 (c (n "win32-system-sys") (v "0.1.0") (h "06yqdimas3f2gjxnf77817yalv470pmsa5hrixj2k63685gfvgvm")))

