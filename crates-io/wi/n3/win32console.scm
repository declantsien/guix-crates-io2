(define-module (crates-io wi n3 win32console) #:use-module (crates-io))

(define-public crate-win32console-0.1.0 (c (n "win32console") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "1j15p17a7apd8c2zzxvkzyah47y7dq5xan4gdif5mlr596clpd21")))

(define-public crate-win32console-0.1.1 (c (n "win32console") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "1wllfhy30gpi0s8046qnz078fkbbfbabdq907mbh8jw8fjggspxg")))

(define-public crate-win32console-0.1.2 (c (n "win32console") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "0i2w710sxrwl7anz08zrd4dvr21w88yzng4pd3y3ljh75kjdbpim")))

(define-public crate-win32console-0.1.2-pre1 (c (n "win32console") (v "0.1.2-pre1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "0a0jpc9msa99fkk3smn5q5gzl1c79mixd1y3m4v9914biz7sjy33")))

(define-public crate-win32console-0.1.2-pre1.1 (c (n "win32console") (v "0.1.2-pre1.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "091d9q6di5hp6lqj9h1gfaviicsbw9sv3vjh186ss6f0n5qlfxpg")))

(define-public crate-win32console-0.1.2-a1 (c (n "win32console") (v "0.1.2-a1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "0sfv3nd3ad06n8xgx4as3hinx9r4cndbjh58sbywf4cg9qjl5gqs")))

(define-public crate-win32console-0.1.3-a1 (c (n "win32console") (v "0.1.3-a1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "1i530xlxwk7dlv2hvq6vlms29266vfyhcl3iyk8xb86s8gichc7k")))

(define-public crate-win32console-0.1.3-a2 (c (n "win32console") (v "0.1.3-a2") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "194bn9dxgnhvcb3dlgkpxkiqm5hiyfq7ajvvm1bxdxr63ng04wb8")))

(define-public crate-win32console-0.1.3 (c (n "win32console") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "09jx8gfa3vrharb79i72jyb4c9xmli23vnz4l7pmsnixxv9h66s7")))

(define-public crate-win32console-0.1.4 (c (n "win32console") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "0fj66vap60c1wzkirq4sm4dj520sy6vmxsgvwblmjz379f9gfr3w")))

(define-public crate-win32console-0.1.5 (c (n "win32console") (v "0.1.5") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (d #t) (k 0)))) (h "0wzn0rngnkigdgkgi8haw8xwcc6szrklcz2sky5kw3cr07gvnscy")))

