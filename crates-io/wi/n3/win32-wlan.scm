(define-module (crates-io wi n3 win32-wlan) #:use-module (crates-io))

(define-public crate-win32-wlan-0.1.0 (c (n "win32-wlan") (v "0.1.0") (d (list (d (n "get-last-error") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Networking_Connectivity" "Devices" "Devices_WiFi" "Foundation" "Foundation_Collections" "Win32_Foundation" "Win32_Networking_WinHttp" "Win32_NetworkManagement_WiFi"))) (d #t) (k 0)))) (h "117n5zmffja08qlsqjr13vmi2xwql90ij4pgwp7g0jzjnp8ji2sr") (f (quote (("default"))))))

