(define-module (crates-io wi n3 win32-security-sys) #:use-module (crates-io))

(define-public crate-win32-security-sys-0.0.2 (c (n "win32-security-sys") (v "0.0.2") (h "0w5kria4xgs7gnx31gnr5g8jfaiyd57fa4pf4xj6n68804iahyfh") (r "1.46")))

(define-public crate-win32-security-sys-0.1.0 (c (n "win32-security-sys") (v "0.1.0") (h "0djwgqkrrcb8qv4hygadsccw6ixk6ifhbd6g7rqcdvrg52g7dm7k")))

