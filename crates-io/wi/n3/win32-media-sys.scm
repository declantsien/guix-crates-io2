(define-module (crates-io wi n3 win32-media-sys) #:use-module (crates-io))

(define-public crate-win32-media-sys-0.0.2 (c (n "win32-media-sys") (v "0.0.2") (h "17x4i5i6syh2w0p2zw05yr7qama87w04955fwxf5gp5l9gnwp30h") (r "1.46")))

(define-public crate-win32-media-sys-0.1.0 (c (n "win32-media-sys") (v "0.1.0") (h "04vp965l0bacc8bl30g15y4m24sfq8l9yphic8zg6b75cw6wazf6")))

