(define-module (crates-io wi n3 win32-storage-sys) #:use-module (crates-io))

(define-public crate-win32-storage-sys-0.0.2 (c (n "win32-storage-sys") (v "0.0.2") (h "01vr8zsrmm6w82mgh6qrxp5gr3zk41nafixlh0mbf26bj5s2i1pc") (r "1.46")))

(define-public crate-win32-storage-sys-0.1.0 (c (n "win32-storage-sys") (v "0.1.0") (h "1m1q7ck0rzihdfsvign6yjp0sbishhs3gwa2ni05w23hyksy11sb")))

