(define-module (crates-io wi nn winnf) #:use-module (crates-io))

(define-public crate-winnf-1.0.0 (c (n "winnf") (v "1.0.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.3.0") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "1kyn61kxrj9bz4648fr4rwy0273wd3w9mpaz42yqxf6fhqspgqk3")))

(define-public crate-winnf-1.1.0 (c (n "winnf") (v "1.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.7") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.3.0") (d #t) (k 0)) (d (n "which") (r "^6.0.1") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "00lsw4kkw7rs9qwgr9vx3zwlddy5df05hdngc4qjq3i9g6ypsqxb")))

(define-public crate-winnf-1.2.0 (c (n "winnf") (v "1.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("sysinfoapi" "winbase"))) (d #t) (k 0)) (d (n "winver") (r "^1.0.0") (d #t) (k 0)))) (h "0kp8gp96q1fdrqjlch4cxwsqfnnzl0cq4ppihabbwb041j61vv23")))

