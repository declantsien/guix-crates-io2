(define-module (crates-io wi th with-id-derive) #:use-module (crates-io))

(define-public crate-with-id-derive-1.0.0 (c (n "with-id-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0d4i1kymzvw02ig2vd38crbgmsyfqmxk59n2b6lrvx6grzp9cwkf") (y #t)))

(define-public crate-with-id-derive-1.0.1 (c (n "with-id-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0jkarilhw2ga16430gazxyyh16irylv4pzvpkf307vcmva9lv6rb") (y #t)))

(define-public crate-with-id-derive-1.0.2 (c (n "with-id-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "1g70z8mzamd6jzw5vp8ikhvbljvr6whx54jplr4bqrdjgzsmbsj6")))

(define-public crate-with-id-derive-1.0.3 (c (n "with-id-derive") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "13jvn6krm32wk5jdzhx4lq6lqw779i4fzz5g2aih6rb5krjmdy21")))

