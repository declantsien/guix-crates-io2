(define-module (crates-io wi th with-id) #:use-module (crates-io))

(define-public crate-with-id-1.0.0 (c (n "with-id") (v "1.0.0") (d (list (d (n "with-id-derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1vjqzq41zivzbhsljf5sw719svx3gsw2s4byjdf13kw7129g5vd5") (y #t) (s 2) (e (quote (("derive" "dep:with-id-derive"))))))

(define-public crate-with-id-1.0.1 (c (n "with-id") (v "1.0.1") (d (list (d (n "with-id-derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0xhmjiv1d1vqi70rjd9fqbrpvbzvf5653dnxz6dnmcw9c9m6y4rn") (y #t) (s 2) (e (quote (("derive" "dep:with-id-derive"))))))

(define-public crate-with-id-1.0.2 (c (n "with-id") (v "1.0.2") (d (list (d (n "with-id-derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0vqzvnj7xhiik6xd8hpdf5cwkzps0nn0lig5yx30phxpgrg10ngi") (s 2) (e (quote (("derive" "dep:with-id-derive"))))))

