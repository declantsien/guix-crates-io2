(define-module (crates-io wi th with_literal) #:use-module (crates-io))

(define-public crate-with_literal-1.0.0 (c (n "with_literal") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "00swdgzd818hxmjk5276i0iaklpgxmx54bpqx2v2g34papzsncy9")))

