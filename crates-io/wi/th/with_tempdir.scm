(define-module (crates-io wi th with_tempdir) #:use-module (crates-io))

(define-public crate-with_tempdir-0.1.0 (c (n "with_tempdir") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "18gfzzz6x9w0249vh0apqqixqgl4s8yn2j8k26qn6pg3sqd0g725")))

