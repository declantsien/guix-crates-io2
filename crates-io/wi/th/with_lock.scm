(define-module (crates-io wi th with_lock) #:use-module (crates-io))

(define-public crate-with_lock-0.1.0 (c (n "with_lock") (v "0.1.0") (h "0wadfhqvp3fpd0h1ib67siy5y89020pnv19m24vs6vk4m70iklfp")))

(define-public crate-with_lock-0.2.0 (c (n "with_lock") (v "0.2.0") (h "01x9wigxs6ffp54vykw8whqxhrikl2hqsqfj0ssmwdfh9qgbp6v9")))

(define-public crate-with_lock-0.2.1 (c (n "with_lock") (v "0.2.1") (h "1daypps5x747qg01z27nha4318qw2yx8xgjs44g5sk3rzhvk90f3")))

(define-public crate-with_lock-0.2.2 (c (n "with_lock") (v "0.2.2") (h "1qyg5s3hs11lilkrwr0nm92dfsh37f7w13m1mpzrh18wlabfmz5f")))

(define-public crate-with_lock-0.3.0 (c (n "with_lock") (v "0.3.0") (h "0k5khgj68z6dbswmdqlh1grsiy7s73f6zpzgh0ahnzbnnc98h6qi")))

(define-public crate-with_lock-0.3.1 (c (n "with_lock") (v "0.3.1") (h "1p0hiwl9ysyclib62gwh6z4sxvbm2pvp7ifla4jch83rvqh942gq")))

(define-public crate-with_lock-0.3.2 (c (n "with_lock") (v "0.3.2") (h "1wxavzq3465znh8whz9r2a7g16v9zrrgdjmxgdrx1zngl0phav20")))

(define-public crate-with_lock-0.3.3 (c (n "with_lock") (v "0.3.3") (h "060sp355dzb7yqzf54ah6wmg0v1r5sfz3blwgpggbqw2fss0y7la")))

(define-public crate-with_lock-0.4.0 (c (n "with_lock") (v "0.4.0") (h "0lwzhxpvl9rj2wh3dasq9ffx8rfwlyrkgaw05lrc28fa93bc71vj")))

(define-public crate-with_lock-0.4.1 (c (n "with_lock") (v "0.4.1") (h "13zk97ma2spph2ma001n5sgvnsm8m413psviphq0r6xy85ryqky2")))

(define-public crate-with_lock-0.4.2 (c (n "with_lock") (v "0.4.2") (h "08lmi82va2sim5j0df18x6srl6qml0lj6zca1anq316cq69qy2v9")))

(define-public crate-with_lock-0.5.0 (c (n "with_lock") (v "0.5.0") (h "0m0jh5r8vk20xzgcrqvf90im916z5qpfxlfggx3lqn5hxfd5v4z6")))

(define-public crate-with_lock-0.5.1 (c (n "with_lock") (v "0.5.1") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0jxb6x9ymfi5rbccarz2sdm0q2x8877bgxj6886vls95yqamf30h")))

(define-public crate-with_lock-0.5.2 (c (n "with_lock") (v "0.5.2") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0nlj8jgxb8w4hd78nv6gz5rz837kpxfyiwa15qd3b7bad9w2ka68")))

(define-public crate-with_lock-0.5.3 (c (n "with_lock") (v "0.5.3") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0hd3jr1di9yy2a38sqm7jczlrhi18lc2fmqzz5g5rv68awz712sa")))

(define-public crate-with_lock-0.5.4 (c (n "with_lock") (v "0.5.4") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0yxrrjlaa1h8jkhvzwkdvw7p3ggr4yqyw3m7yv45xg14yi0dy0y6") (r "1.54.0")))

