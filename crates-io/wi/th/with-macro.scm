(define-module (crates-io wi th with-macro) #:use-module (crates-io))

(define-public crate-with-macro-0.1.0 (c (n "with-macro") (v "0.1.0") (h "1jy9mwklfi6nfk93jri13b7x33326sja12ii249rhihkps39rkl8")))

(define-public crate-with-macro-0.1.1 (c (n "with-macro") (v "0.1.1") (h "0lzk3ipxxcxvixgyxnj2iwqrjpkwjxmavmzz4l5w1iwzcfc3nk2g")))

