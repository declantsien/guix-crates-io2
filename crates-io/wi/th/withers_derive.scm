(define-module (crates-io wi th withers_derive) #:use-module (crates-io))

(define-public crate-withers_derive-0.1.0 (c (n "withers_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1y65lils68mqq5jq3a5nn2p6dp0lsbg5w6k3dgv6y0xy49j10b6w")))

(define-public crate-withers_derive-0.2.0 (c (n "withers_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0l3pw2lqp3pz9fswy9gih87qnp77ljbm737gdm1k57c1ycz9gl82")))

