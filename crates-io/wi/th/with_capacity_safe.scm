(define-module (crates-io wi th with_capacity_safe) #:use-module (crates-io))

(define-public crate-with_capacity_safe-0.1.0 (c (n "with_capacity_safe") (v "0.1.0") (h "1fwdnp42yhhli8b3ngvq13i7xw93iqa23hpfi1whpi026b8pqia3")))

(define-public crate-with_capacity_safe-0.2.0 (c (n "with_capacity_safe") (v "0.2.0") (h "1aslacagjl9rhm56xnwsaknvqj5d2hhzgmxrq8d8f56qzdn761b0")))

(define-public crate-with_capacity_safe-0.3.0 (c (n "with_capacity_safe") (v "0.3.0") (h "07dax04h97sqv94mgwnabp9wpa7cmskkxbrhmqxfqmnafr8rlnv8")))

(define-public crate-with_capacity_safe-0.4.0 (c (n "with_capacity_safe") (v "0.4.0") (h "02qw8s2zzcxm3v1a0lh7hp5qj5p6p0wkcwvcbk2zg2zpzi9nvbm6")))

(define-public crate-with_capacity_safe-0.4.1 (c (n "with_capacity_safe") (v "0.4.1") (h "0nmzdw2vha9a3iainrjw2yhmn1i60yqvf9055p5q46gy2hc7fcab")))

(define-public crate-with_capacity_safe-0.4.2 (c (n "with_capacity_safe") (v "0.4.2") (h "07xq1w9xy6v5qbgm2csbxwn6s9w70zy6fc093vh4p4gpp77bc9ry") (r "1.57")))

