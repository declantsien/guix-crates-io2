(define-module (crates-io wi th with_builtin_macros) #:use-module (crates-io))

(define-public crate-with_builtin_macros-0.0.3 (c (n "with_builtin_macros") (v "0.0.3") (d (list (d (n "proc_macros") (r "^0.0.3") (d #t) (k 0) (p "with_builtin_macros-proc_macros")))) (h "1nl5wqyv5c56vq97lx0a7qzlsbk0r33596fnz63rnhlm4h1mb7d5")))

(define-public crate-with_builtin_macros-0.1.0-rc1 (c (n "with_builtin_macros") (v "0.1.0-rc1") (d (list (d (n "proc_macros") (r "^0.1.0-rc1") (d #t) (k 0) (p "with_builtin_macros-proc_macros")))) (h "0ib9ad2ml1zc8kxmpchxjwqspxn0cxi73b66q71bygxzn43ka96b")))

(define-public crate-with_builtin_macros-0.1.0-rc3 (c (n "with_builtin_macros") (v "0.1.0-rc3") (d (list (d (n "proc_macros") (r "^0.1.0-rc3") (d #t) (k 0) (p "with_builtin_macros-proc_macros")))) (h "0l5pwf5zk0hql52csy8ms49n0cvx3g4hk3vay8s7xb6zh433w16y")))

(define-public crate-with_builtin_macros-0.1.0 (c (n "with_builtin_macros") (v "0.1.0") (d (list (d (n "proc_macros") (r "^0.1.0") (d #t) (k 0) (p "with_builtin_macros-proc_macros")))) (h "12ir1v8x2mjivg253czyj8fsg8v0w4f0y7rbn4bpc3jkdv6v7pi4")))

