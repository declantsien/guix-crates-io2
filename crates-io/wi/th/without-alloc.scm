(define-module (crates-io wi th without-alloc) #:use-module (crates-io))

(define-public crate-without-alloc-0.2.0 (c (n "without-alloc") (v "0.2.0") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)))) (h "104li4xdlz046cxf4hax35inghdvalxqf3zlgcxp39j9j3vbrm3w")))

(define-public crate-without-alloc-0.2.1 (c (n "without-alloc") (v "0.2.1") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)))) (h "1qkm9pi8vpc0n0h83p401cgsr3sdgaa7x4l0aqz0napmxxpp6d2y")))

(define-public crate-without-alloc-0.2.2 (c (n "without-alloc") (v "0.2.2") (d (list (d (n "alloc-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "unsize") (r "^1.1.0") (d #t) (k 0)))) (h "1fxd4ggmcrpm7xkygnag8p130pxyrliww70dy479afr0id3v0p9p") (f (quote (("nightly_set_ptr_value") ("default"))))))

