(define-module (crates-io wi th with_builtin_macros-proc_macros) #:use-module (crates-io))

(define-public crate-with_builtin_macros-proc_macros-0.0.1 (c (n "with_builtin_macros-proc_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0jn3ncyprpf1iq8zwn72074drzj6szgwhgw0zg268yhx7433386b")))

(define-public crate-with_builtin_macros-proc_macros-0.0.2 (c (n "with_builtin_macros-proc_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0gf1n1iqlzsfvk2qyyis7vk90x6rzz1s12fyf26n97r4s109frfr")))

(define-public crate-with_builtin_macros-proc_macros-0.0.3 (c (n "with_builtin_macros-proc_macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0hlgkw93wnc8my4rc4mnzrsbcicc8i74vqxfad7r48jyq5wpdg8m")))

(define-public crate-with_builtin_macros-proc_macros-0.1.0-rc1 (c (n "with_builtin_macros-proc_macros") (v "0.1.0-rc1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1cidwjqh4yhfyg6f6rjasp5gpbimgzqmnin6rx13c4jlbv0a3qfp")))

(define-public crate-with_builtin_macros-proc_macros-0.1.0-rc3 (c (n "with_builtin_macros-proc_macros") (v "0.1.0-rc3") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "022h1pq1ym6pvi5nips9i01f642i8apqdj4r9m4jdfkigjbmhwgj")))

(define-public crate-with_builtin_macros-proc_macros-0.1.0 (c (n "with_builtin_macros-proc_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.4") (d #t) (k 2)) (d (n "syn") (r "1.0.*") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0jl0d63h8bgy1fqiq9vz9lyqar9w04kzds1cwlg6ync52adswn92")))

