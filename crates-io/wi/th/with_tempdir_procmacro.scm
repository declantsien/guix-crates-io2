(define-module (crates-io wi th with_tempdir_procmacro) #:use-module (crates-io))

(define-public crate-with_tempdir_procmacro-0.1.0 (c (n "with_tempdir_procmacro") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0qs47paf9rmx9dm7qbhb230w7yazsycdwcmyggm722bwaanxdb86") (y #t)))

(define-public crate-with_tempdir_procmacro-0.1.1 (c (n "with_tempdir_procmacro") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0vn6d1j4y595gz2agm4jdggirry6433cwankd1zl45q7l96rh1g4") (y #t)))

