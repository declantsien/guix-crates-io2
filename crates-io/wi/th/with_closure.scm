(define-module (crates-io wi th with_closure) #:use-module (crates-io))

(define-public crate-with_closure-0.1.0 (c (n "with_closure") (v "0.1.0") (h "094qivx5031zdv2fbgn5vgprddpapnk3xhnfi0wx6pwkckls80ls")))

(define-public crate-with_closure-0.1.1 (c (n "with_closure") (v "0.1.1") (h "0y7pns3il0gwaljvdx1zaih3y5v61z0a4mjc2ga4d6mkf80lzz44")))

(define-public crate-with_closure-0.1.2 (c (n "with_closure") (v "0.1.2") (h "1wcpavai940fh6m2hrl4xzj3sxjn6y4j8cha6w4d31zw3zczypy4")))

