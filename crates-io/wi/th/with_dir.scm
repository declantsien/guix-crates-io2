(define-module (crates-io wi th with_dir) #:use-module (crates-io))

(define-public crate-with_dir-0.1.0 (c (n "with_dir") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0yqraq52ya57jqwzp9zg6q4izfsnn8ysgcimcnm22iaxz1jcw38q")))

(define-public crate-with_dir-0.1.1 (c (n "with_dir") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1l3xf34c0gyqv0l6sy5ifz6gzx0070cspi07bbh7njvjlhhs24r9")))

(define-public crate-with_dir-0.1.2 (c (n "with_dir") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rfi84k6fm59pbgr2q7cp14gf7y3hjf5gxv3967wzqyis8axqvgq")))

(define-public crate-with_dir-0.1.3 (c (n "with_dir") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1cn4b42ihj9w01105kqsfjksrssz1f971py11qrh81ws7h5fi69d")))

(define-public crate-with_dir-0.1.4 (c (n "with_dir") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)))) (h "1fmjnv9n1yk45mmkyvdv15n390wk4iwbc54k8r50gsyh6xhddpb3")))

