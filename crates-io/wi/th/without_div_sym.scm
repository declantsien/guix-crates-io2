(define-module (crates-io wi th without_div_sym) #:use-module (crates-io))

(define-public crate-without_div_sym-0.1.0 (c (n "without_div_sym") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1ar6dr3jrvywa1nvgcripr75scsal475d77lmm1fki6mvln01r6k") (y #t)))

(define-public crate-without_div_sym-0.1.1 (c (n "without_div_sym") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0jh9pr5rq9ppzh0lnfzshy7mycwwzzblbig6b08w1clj7vkrhaaw")))

