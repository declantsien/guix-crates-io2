(define-module (crates-io wi th withings-rs) #:use-module (crates-io))

(define-public crate-withings-rs-0.1.0 (c (n "withings-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "random-string") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1bh0isf62m52n2f640bkvq3m788pspjj0gh9nx9ifjfi11mxg7q9")))

(define-public crate-withings-rs-0.1.1 (c (n "withings-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "random-string") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "100kcxz0z3hwly40slin56p5zgvgr22k3d17jcdlsjqljjy11p7p")))

