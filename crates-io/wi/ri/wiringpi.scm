(define-module (crates-io wi ri wiringpi) #:use-module (crates-io))

(define-public crate-wiringpi-0.1.0 (c (n "wiringpi") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "07591a62q8bi8kmf36d1mf2mzx7zk70w5x1m97bhqdhq5gffgzn0") (f (quote (("nightly")))) (y #t)))

(define-public crate-wiringpi-0.1.1 (c (n "wiringpi") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0h807krjbin351rqs08ymvxmfmj355077q7g12s0qn81qbpvjnsh") (f (quote (("nightly"))))))

(define-public crate-wiringpi-0.1.2 (c (n "wiringpi") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0ac1a2xg8xrzaq5vh8ckvqqy5qdnz69hdf0rf5rcqqpq94dcgadv") (f (quote (("noclib") ("nightly"))))))

(define-public crate-wiringpi-0.1.3 (c (n "wiringpi") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16p26dd88587ci1cy635g1p7d0a3cd8qbid16hp3fr098k717q3n") (f (quote (("noclib") ("nightly"))))))

(define-public crate-wiringpi-0.1.4 (c (n "wiringpi") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ib80x6qandzdvk6apgzannhl8ny6gg4bvvld2wk997mb0rgly71") (f (quote (("noclib") ("nightly")))) (y #t)))

(define-public crate-wiringpi-0.1.5 (c (n "wiringpi") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00irp64mvr075fb1wl3hrv0k9zhwmy6fxvx2dbygh48zgkvijhrz") (f (quote (("orangepi") ("noclib") ("nightly"))))))

(define-public crate-wiringpi-0.2.0 (c (n "wiringpi") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xyss5q2xl0yfl7qjd4x6410hc01v2qp0mhdlgc46s66dwa9yl68") (f (quote (("orangepi")))) (y #t)))

(define-public crate-wiringpi-0.2.1 (c (n "wiringpi") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j5fx61zzd2dfrzjc4dzkdq8ywa5mlf6xli6zr36w73rchkfasl1") (f (quote (("orangepi"))))))

(define-public crate-wiringpi-0.2.2 (c (n "wiringpi") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pq461dkw7nnm6dsmjzm7p8k0v9mx2wh6gyrzkc60w0nx5kjk4w4") (f (quote (("strict") ("orangepi") ("development"))))))

(define-public crate-wiringpi-0.2.3 (c (n "wiringpi") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18nar945qz6f4fza9z6582f3akm9wd380v5wzmkwzc9c1bm8iaac") (f (quote (("strict") ("orangepi") ("development"))))))

(define-public crate-wiringpi-0.2.4 (c (n "wiringpi") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gbg28x15zlz5s2j7f7aijia1l670pwkpsr834gaxc45i3g6bq73") (f (quote (("strict") ("orangepi") ("development"))))))

