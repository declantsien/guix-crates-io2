(define-module (crates-io wi ri wiringpi_sys) #:use-module (crates-io))

(define-public crate-wiringpi_sys-0.1.0 (c (n "wiringpi_sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.24") (d #t) (k 0)))) (h "1r4l5d89b720l8m7mr7abf15cx0bs93879ddvsbd4ky4706w69xi")))

(define-public crate-wiringpi_sys-0.1.1 (c (n "wiringpi_sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.24") (d #t) (k 0)))) (h "1qq1pqx5jdlpbvmwz0d8ls0252wkk8ragnl90lik2qdn4kgpg7l5")))

