(define-module (crates-io wi ri wiring-derive) #:use-module (crates-io))

(define-public crate-wiring-derive-0.1.0 (c (n "wiring-derive") (v "0.1.0") (h "05zg7bj4rifm9cp7l56fqzhk3hz5pqaqpl0i3xyiwy8k7d2v7z7b")))

(define-public crate-wiring-derive-0.1.1 (c (n "wiring-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0j4qaq2rw5xzscx8s35zmgbyj3gparchg8aj4gpp398smq4axzq9")))

(define-public crate-wiring-derive-0.1.2 (c (n "wiring-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "16z6p5p3xqqvxlr0snf6kdfs1lk91xsjy9c0q6g10r6hc3v7l1kp")))

(define-public crate-wiring-derive-0.2.0 (c (n "wiring-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "151dg23rmx5lwqiakmgi597y48yb2dkh3b583l0aasg7ilcxgv17")))

(define-public crate-wiring-derive-0.2.1 (c (n "wiring-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "1k33rgjcpldkx1fw3xqff8xhn9jk57cfy1v8sn1b09qyjmdrzwd2")))

