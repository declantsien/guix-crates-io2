(define-module (crates-io wi ck wick-xdg) #:use-module (crates-io))

(define-public crate-wick-xdg-0.2.0 (c (n "wick-xdg") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "16ribwlm7rb23mmgc8zh4k1kfigrxmr92d6ccacv2v43q8q5nd2z")))

(define-public crate-wick-xdg-0.3.0 (c (n "wick-xdg") (v "0.3.0") (d (list (d (n "getset") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0vql61dk7kpkq3yy7nn6l4n4fdvrv36hvbyix2ky8rh8vbw2r071") (f (quote (("default"))))))

(define-public crate-wick-xdg-0.4.0 (c (n "wick-xdg") (v "0.4.0") (d (list (d (n "getset") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1c09hjccnfkpr5cv5by38lpb0fqn2vdykc2dfp3yd9780w5lwh4c") (f (quote (("default"))))))

