(define-module (crates-io wi ck wick-interface-cli) #:use-module (crates-io))

(define-public crate-wick-interface-cli-0.3.0 (c (n "wick-interface-cli") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "wick-component") (r "^0.16.0") (k 0)) (d (n "wick-component-codegen") (r "^0.5.0") (d #t) (k 1)))) (h "0hlbz72la8h9000fbih872s43yg11m20frjwiq8k63dbk711nas5") (f (quote (("localgen") ("default"))))))

(define-public crate-wick-interface-cli-0.4.0 (c (n "wick-interface-cli") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "wick-component") (r "^0.17.0") (k 0)) (d (n "wick-component-codegen") (r "^0.6.0") (d #t) (k 1)))) (h "0kiprarxip6lbjv3fplv3kw1dwsd0z64q7bsmhsl63ir8nz2xhsw") (f (quote (("localgen") ("default"))))))

