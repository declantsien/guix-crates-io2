(define-module (crates-io wi ck wick-settings) #:use-module (crates-io))

(define-public crate-wick-settings-0.2.0 (c (n "wick-settings") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.12") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "wick-xdg") (r "^0.3.0") (d #t) (k 0)))) (h "109c2s4kbrrv6aacy3lbq7cswf0srllvgr6gwsmz949ba551gghc")))

(define-public crate-wick-settings-0.3.0 (c (n "wick-settings") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.12") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tracing") (r "^0.1") (k 0)) (d (n "wick-xdg") (r "^0.4.0") (d #t) (k 0)))) (h "18sw37jv306qm6hp60p9fg8ymc3l8lgrjsnrng66y9wqwmh3c5za")))

