(define-module (crates-io wi ck wick-wasm-engine) #:use-module (crates-io))

(define-public crate-wick-wasm-engine-0.1.0 (c (n "wick-wasm-engine") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8") (k 0)) (d (n "parking_lot") (r "^0.12") (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("fs"))) (k 0)) (d (n "wasmtime") (r "^13.0") (f (quote ("cache" "jitdump" "parallel-compilation" "cranelift" "vtune" "component-model" "async"))) (k 0)) (d (n "wasmtime-wasi") (r "^13.0") (f (quote ("sync" "tokio"))) (d #t) (k 0)))) (h "00mzh9mhslmbp6bgcimwd2fmdbap3d73vrs7si4h4g3xs937cr9s")))

