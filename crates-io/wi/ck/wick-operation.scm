(define-module (crates-io wi ck wick-operation) #:use-module (crates-io))

(define-public crate-wick-operation-0.2.0 (c (n "wick-operation") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (k 0)) (d (n "proc-macro2") (r "~1.0.63") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "derive" "proc-macro" "clone-impls" "printing" "parsing"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "wick-logger") (r "^0.3.0") (d #t) (k 2)))) (h "0qcqr5vk75b646c9wapdpyg0glxq326cqz4g0m2sxhmlz32zkcpa")))

(define-public crate-wick-operation-0.3.0 (c (n "wick-operation") (v "0.3.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (k 0)) (d (n "proc-macro2") (r "~1.0.63") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0") (f (quote ("proc-macro"))) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "derive" "proc-macro" "clone-impls" "printing" "parsing"))) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("macros" "rt-multi-thread"))) (k 2)) (d (n "wick-logger") (r "^0.4.0") (d #t) (k 2)))) (h "07mx7jc5zq185hy9j4w1fclq2ikgqqs7z642knlslpsh5hj37qij")))

