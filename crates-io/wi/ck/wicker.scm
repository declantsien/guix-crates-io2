(define-module (crates-io wi ck wicker) #:use-module (crates-io))

(define-public crate-wicker-0.1.0 (c (n "wicker") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03dxbf1arqj2kyg66f0nzi56w72rdfgv3n0vl6r35gk44lsc6inv")))

(define-public crate-wicker-0.2.0 (c (n "wicker") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06nsf93nz191j9zcmny9g6g2a04kw1jq8szpvgr7h5qxzfypfzsc")))

