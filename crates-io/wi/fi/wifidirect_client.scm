(define-module (crates-io wi fi wifidirect_client) #:use-module (crates-io))

(define-public crate-wifidirect_client-0.0.1 (c (n "wifidirect_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1lpnz0shrisw89hq1q597d8l36r56xvccs4fi9sfihallyz6w231")))

(define-public crate-wifidirect_client-0.0.2 (c (n "wifidirect_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "1ac0l2x8xy7zlmbhfdfv4nl38dsb2mlpsc9lv8gxp4annc7s0930")))

