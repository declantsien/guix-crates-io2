(define-module (crates-io wi fi wifi_viewer) #:use-module (crates-io))

(define-public crate-wifi_viewer-1.0.0 (c (n "wifi_viewer") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "0fkz4sjld0lbl3dfq970mr4rip3vdgrcilpf16wz03xs0i1bccq8")))

(define-public crate-wifi_viewer-1.0.1 (c (n "wifi_viewer") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "19gwg34xsmkqkkx00dmpj8z3nxqp6zbxrwsgj2mzrd7drw8fv3g6")))

