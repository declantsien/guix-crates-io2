(define-module (crates-io wi fi wifi-nina) #:use-module (crates-io))

(define-public crate-wifi-nina-0.1.0 (c (n "wifi-nina") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "nb") (r "^0.1.2") (k 0)) (d (n "no-std-net") (r "^0.3.0") (k 0)) (d (n "num_enum") (r "^0.4.2") (k 0)))) (h "16165gcj2csnr5hgclljxrldbly4x9gl84fq8ijhwmbnwyzcghgl")))

(define-public crate-wifi-nina-0.1.1 (c (n "wifi-nina") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "nb") (r "^0.1.2") (k 0)) (d (n "no-std-net") (r "^0.3.0") (k 0)) (d (n "num_enum") (r "^0.4.2") (k 0)))) (h "0a7jnnxc34snh79bcq8clrmbg9qinpj0iza683ygwirxmflbpcva")))

(define-public crate-wifi-nina-0.1.2 (c (n "wifi-nina") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (k 0)) (d (n "log") (r "^0.4.11") (k 0)) (d (n "nb") (r "^0.1.2") (k 0)) (d (n "no-std-net") (r "^0.4.0") (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "0vnm2dpcjncnh25da6pvl4j8hi1pfy2lqwpsjqrjawcm97igaw2f")))

