(define-module (crates-io wi fi wifi) #:use-module (crates-io))

(define-public crate-wifi-0.0.0 (c (n "wifi") (v "0.0.0") (d (list (d (n "os_type") (r "^1.0.0") (d #t) (k 0)))) (h "035yd5y4n2blxwfdyic6g9knajc44bsvm2j9vs21fqfrcxfbkwgy")))

(define-public crate-wifi-0.1.0 (c (n "wifi") (v "0.1.0") (h "03rxc37rnd50mwyr9q5pwkcv1fxawxb0jksg7i543mvbvmk1aj4c")))

