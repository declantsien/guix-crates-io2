(define-module (crates-io wi fi wifi-qr-code-generator) #:use-module (crates-io))

(define-public crate-wifi-qr-code-generator-0.1.0 (c (n "wifi-qr-code-generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)))) (h "1zwh667afcai9487crjqnxqhwk9ghpfyk82lcyy4nlz35q138gm2")))

