(define-module (crates-io wi fi wifi-rs) #:use-module (crates-io))

(define-public crate-wifi-rs-0.1.0 (c (n "wifi-rs") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "0sq763bw6by628dl9jxr01r79brdzxhalndnsk2xyxf5xbjs183c")))

(define-public crate-wifi-rs-0.2.0 (c (n "wifi-rs") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "19w788skp4z96xg4dwqvcyyp89a7y3zv7034h1f69kslxzn4as9s")))

(define-public crate-wifi-rs-0.2.1 (c (n "wifi-rs") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "1rbzra0gy036bzifppxp3aqn8570x930y1czmfbz5qhvj80x11ln")))

(define-public crate-wifi-rs-0.2.2 (c (n "wifi-rs") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "0mqavrl9hjb15m6zbzjmxqkk6iqixzamzs2b04l5s6rhq7hxgfy6")))

(define-public crate-wifi-rs-0.2.3 (c (n "wifi-rs") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "18igw7bg8730mak9bd00fri82v3m44n61s6xy5ms5zip2ck4lnnm")))

(define-public crate-wifi-rs-0.2.4 (c (n "wifi-rs") (v "0.2.4") (d (list (d (n "tempfile") (r "^3.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1z98nqnr3viy44x1iq62406dan3f1h136y66wzrvh1ax4mds8wap")))

