(define-module (crates-io wi fi wifiqr) #:use-module (crates-io))

(define-public crate-wifiqr-0.0.4 (c (n "wifiqr") (v "0.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.18.0") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.4.0") (d #t) (k 0)))) (h "0r88x09991zqdlxlwi2vsnj9ir9la1138708irdznc5kwn39amik")))

(define-public crate-wifiqr-0.0.5 (c (n "wifiqr") (v "0.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.18.0") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.4.0") (d #t) (k 0)))) (h "0b7p8hhfd5xjah5qq1p3nsxpnc1vyx6558l47hk14fs2rhcc1v10")))

(define-public crate-wifiqr-0.0.6 (c (n "wifiqr") (v "0.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "imageproc") (r "^0.18.0") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.4.0") (d #t) (k 0)))) (h "111jg2p8vy9q4r8s7fnss9y8kwykxfhyb7z4ns0jcyp8bwk0ch69")))

(define-public crate-wifiqr-0.0.7 (c (n "wifiqr") (v "0.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.4.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)))) (h "11y9s3pgms6zm0i356kjnw91zvnic7mr45537k4k5jq8g2md02sp")))

(define-public crate-wifiqr-0.0.8 (c (n "wifiqr") (v "0.0.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "qrcodegen") (r "^1.8.0") (d #t) (k 0)) (d (n "rpassword") (r "^5.0.1") (d #t) (k 0)))) (h "192b2mwy9nmip7kizlydn0a3960czfzhlvv7g729bbf32cc9mfsa")))

