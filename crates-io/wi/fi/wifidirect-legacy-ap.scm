(define-module (crates-io wi fi wifidirect-legacy-ap) #:use-module (crates-io))

(define-public crate-wifidirect-legacy-ap-0.1.0 (c (n "wifidirect-legacy-ap") (v "0.1.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0adg99b16jsmcrvczzjsv8bdf65xx49hp1shj0x3w9mfksp3qvmx")))

(define-public crate-wifidirect-legacy-ap-0.2.0 (c (n "wifidirect-legacy-ap") (v "0.2.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0xjjm74lzahl23zdwlgxbh8z3i0bgnm5mhmc95my4fsh7fk5zzgq")))

(define-public crate-wifidirect-legacy-ap-0.2.1 (c (n "wifidirect-legacy-ap") (v "0.2.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0drfqvihm0b81d7sqyx2fd8qg47ajdyqhib5qg99rv57hvmmwk4i")))

(define-public crate-wifidirect-legacy-ap-0.3.0 (c (n "wifidirect-legacy-ap") (v "0.3.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1q9q5484vhpsra137ghwpww7klf44mkmkfscwkgayk468vmvlry0")))

(define-public crate-wifidirect-legacy-ap-0.3.1 (c (n "wifidirect-legacy-ap") (v "0.3.1") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "1c64p290x7nb8nw614rrxic2fbvmkb93xz67343zg3x2jz0lv8af")))

(define-public crate-wifidirect-legacy-ap-0.4.0 (c (n "wifidirect-legacy-ap") (v "0.4.0") (d (list (d (n "windows") (r "^0.44.0") (f (quote ("Data_Xml_Dom" "Devices_Enumeration" "Devices_WiFiDirect" "Foundation" "Foundation_Collections" "Networking" "Security_Credentials" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "01zff2w1lpb4d3mrzyvblj6rp46f4lc26p8psddm77nadny6z65r")))

