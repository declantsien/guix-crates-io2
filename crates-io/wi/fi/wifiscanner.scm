(define-module (crates-io wi fi wifiscanner) #:use-module (crates-io))

(define-public crate-wifiscanner-0.1.0 (c (n "wifiscanner") (v "0.1.0") (h "05xiyz1ycqblv6288wxjz7194drjlw5qlxffw687wqk3xyzyd30j")))

(define-public crate-wifiscanner-0.2.0 (c (n "wifiscanner") (v "0.2.0") (h "189x94qyyyfcm3rsv1m1nicl875qb48dbqjjdzqbzmgw7mm0c3bv")))

(define-public crate-wifiscanner-0.2.1 (c (n "wifiscanner") (v "0.2.1") (h "00h9lbwbrfb27xz3bydyymv430rsq2a84sxply1qppg6741nr81x")))

(define-public crate-wifiscanner-0.3.0 (c (n "wifiscanner") (v "0.3.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "16bj6plxm2p782bb2yq1vc5nsg0da753qwrnd0gsr8pviwpq99fw")))

(define-public crate-wifiscanner-0.3.1 (c (n "wifiscanner") (v "0.3.1") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1ypl871gmf52ad4q7fdim3nigpcwmnwaaq4jpjy72ydxk4iqvmls")))

(define-public crate-wifiscanner-0.3.2 (c (n "wifiscanner") (v "0.3.2") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1rmxc7x40gv4z8w3fb69ih71iabhszwwf0nar1x42v6m8lah281f")))

(define-public crate-wifiscanner-0.3.3 (c (n "wifiscanner") (v "0.3.3") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0287q6vjfxpfcl7s2snni11n46cs1d550984q0piy47gzlg5j24g")))

(define-public crate-wifiscanner-0.3.4 (c (n "wifiscanner") (v "0.3.4") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "11xrdii62inhbih252v6qhwm9k9n40c16hqkg0cp388hj3dmvdac")))

(define-public crate-wifiscanner-0.3.5 (c (n "wifiscanner") (v "0.3.5") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "18jb6v4cgmrc1shpcdxxzppqr96173b64amlz784p3x52y348b20")))

(define-public crate-wifiscanner-0.3.6 (c (n "wifiscanner") (v "0.3.6") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nbmlj9x7irk3mgm6nsw8dp2a9m7r904mgy3qvfpq2lr83f5p0s3")))

(define-public crate-wifiscanner-0.4.0 (c (n "wifiscanner") (v "0.4.0") (h "1rwz8cskz0m7gwj839w9da1dqppxhfaps8lwvw2hsfhmxp0r5bmp")))

(define-public crate-wifiscanner-0.5.0 (c (n "wifiscanner") (v "0.5.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n5vl51v1424wgl7yxkr5p4bfjvmkjvfaq8na3qpnzd1m7dk5jsf")))

(define-public crate-wifiscanner-0.5.1 (c (n "wifiscanner") (v "0.5.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0mpqmrhj1bikszwn8nicp8a6bxa18v9f76jhbcg4zixb6iyncknp")))

