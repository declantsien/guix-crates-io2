(define-module (crates-io wi fi wifi-cli) #:use-module (crates-io))

(define-public crate-wifi-cli-0.1.0 (c (n "wifi-cli") (v "0.1.0") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1kmzsawkmdvwdmskkl5fggnmlxdv4wji38sgwshpfdijc03g6b5m")))

(define-public crate-wifi-cli-0.1.1 (c (n "wifi-cli") (v "0.1.1") (d (list (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "1i30dpll1qszwcvzqvs950syfxywwx7cnyyr4bfgxkxk2hdkxdwh")))

