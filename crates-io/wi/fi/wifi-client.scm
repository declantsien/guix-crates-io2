(define-module (crates-io wi fi wifi-client) #:use-module (crates-io))

(define-public crate-wifi-client-0.1.0 (c (n "wifi-client") (v "0.1.0") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "0aypmc4cm7gccjrlk95zvdk6d4mzpfivkdi4v91n3zw4w66rksaf")))

(define-public crate-wifi-client-0.2.0 (c (n "wifi-client") (v "0.2.0") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "05g3i3v4kq9721b11b2qxg2m8jf6j8mdcllqhrifmp4clsia3q3a")))

(define-public crate-wifi-client-0.2.1 (c (n "wifi-client") (v "0.2.1") (d (list (d (n "inquire") (r "^0.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "134rb2nlz80f3xijlb2zfnnnqibzgf7144930lca49qvipcslrr8")))

