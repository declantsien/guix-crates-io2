(define-module (crates-io wi fi wifilocation) #:use-module (crates-io))

(define-public crate-wifilocation-0.1.0 (c (n "wifilocation") (v "0.1.0") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "wifiscanner") (r "^0.2.1") (d #t) (k 0)))) (h "0fdxjc41z2bjcgq1xqigaqnjg1ky0zsp39v5d9f7hn0b8jkm6f6i")))

(define-public crate-wifilocation-0.2.0 (c (n "wifilocation") (v "0.2.0") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "wifiscanner") (r "^0.2.1") (d #t) (k 0)))) (h "0waf9rcly7p0d2a069kywcii7v0bsz88d395nkdxzvad7q24fvcw")))

(define-public crate-wifilocation-0.2.1 (c (n "wifilocation") (v "0.2.1") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "wifiscanner") (r "^0.3.0") (d #t) (k 0)))) (h "17srgqyr6if80l1lsk164lfah5bv0q7gkwa6c68898yci2kfixii")))

(define-public crate-wifilocation-0.2.2 (c (n "wifilocation") (v "0.2.2") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)) (d (n "wifiscanner") (r "0.3.*") (d #t) (k 0)))) (h "0swgyqm7h65w0pblcaymc4hgb4i78r86rc549d8bw625pn8cbgqp")))

(define-public crate-wifilocation-0.2.3 (c (n "wifilocation") (v "0.2.3") (d (list (d (n "curl") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "wifiscanner") (r "0.3.*") (d #t) (k 0)))) (h "128sgyhrx0laqhhns2qq730a6a5895207ldyazmgr4p21759jh1s")))

(define-public crate-wifilocation-0.3.0 (c (n "wifilocation") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "wifiscanner") (r "0.3.*") (d #t) (k 0)))) (h "016i039qgrvfqnyf1gvcpfr67w3f282awpqgihgw4h0x7lmmljmg")))

