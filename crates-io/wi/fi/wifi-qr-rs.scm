(define-module (crates-io wi fi wifi-qr-rs) #:use-module (crates-io))

(define-public crate-wifi-qr-rs-0.1.0 (c (n "wifi-qr-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "0dgw7h7gn38619lv88rg4fmdfawy2c9ly8gm3q9bl957gknlx68q")))

(define-public crate-wifi-qr-rs-0.1.1 (c (n "wifi-qr-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "1p44liws8dzid947m8bi0wgcx46s05qda0pg6gmjdvvy4dkyyb7x")))

(define-public crate-wifi-qr-rs-0.1.2 (c (n "wifi-qr-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "04dmd027xpnn7dpm7kqccc7vgkmfs4lj0ynln20p8ijv7vw7x0hh")))

(define-public crate-wifi-qr-rs-0.1.3 (c (n "wifi-qr-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "1gm3n035kil9xy5cqg285n97wcjjlxni5x3g222hc7cy6a82ky59")))

(define-public crate-wifi-qr-rs-0.1.4 (c (n "wifi-qr-rs") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "0d4xxljf4fpgyx04vz38a20f71p9j1yd8n62l44rf7dap5ksm3g9")))

(define-public crate-wifi-qr-rs-0.1.5 (c (n "wifi-qr-rs") (v "0.1.5") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 0)))) (h "0s0q76pfb2kamsqg5cad2i4r7zh9nqv086ln7vmjrbgcyfj3hzj6")))

