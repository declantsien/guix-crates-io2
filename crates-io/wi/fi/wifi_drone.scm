(define-module (crates-io wi fi wifi_drone) #:use-module (crates-io))

(define-public crate-wifi_drone-0.0.2 (c (n "wifi_drone") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "glutin") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0zv15920smp8g3c4nljrsxaczgxda14gz6i3c5jc1brsg588mj31")))

(define-public crate-wifi_drone-0.0.3 (c (n "wifi_drone") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "glutin") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0xy4d07prdqzh5p8p72mnkzl40crbb4bcakcqhi9n5jvmj5hk723")))

