(define-module (crates-io wi fi wifi-qr-code) #:use-module (crates-io))

(define-public crate-wifi-qr-code-0.1.0 (c (n "wifi-qr-code") (v "0.1.0") (d (list (d (n "qrcode-generator") (r "^1.0.5") (d #t) (k 0)) (d (n "rpassword") (r "^4.0.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.5") (d #t) (k 2)))) (h "01ilmhpcx07cb7pgkgcs5dyvf7r5gb8hwsjvlb0rsin2rx8avah5")))

