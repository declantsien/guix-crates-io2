(define-module (crates-io wi ld wild) #:use-module (crates-io))

(define-public crate-wild-0.1.0 (c (n "wild") (v "0.1.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "0zz1xja4z1i20wih5604zrzz0fqgm4yvw2i6inrgcbkdg0i8x4ml") (y #t)))

(define-public crate-wild-0.1.1 (c (n "wild") (v "0.1.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "0i306x0lwzzk0xmqfgqyn9r65x0zmnirbcvn8n3qwmy60zf8w1vx") (y #t)))

(define-public crate-wild-1.0.0 (c (n "wild") (v "1.0.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "06akmvwji76ynncglkr13wa2bsgikj6s1wqq7hdn7dz93r17n511")))

(define-public crate-wild-1.0.1 (c (n "wild") (v "1.0.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "1d956fmmgfbqn5rii3852bcx33wi0kp15ymdpxpjrdkbl06qwg0v")))

(define-public crate-wild-1.1.0 (c (n "wild") (v "1.1.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "0k4fcym7j2nmzq1swppm90gfssnycn988334s5bq6702gcm8gzqr")))

(define-public crate-wild-2.0.0 (c (n "wild") (v "2.0.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "19iy13spfky1lj9sw18yw0r3qv1bdzr9mbwmlfb4xm0cpx59anqh")))

(define-public crate-wild-1.1.1 (c (n "wild") (v "1.1.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "0sa2ajy2yilx7s5akdhw7w9rgk0ypw8m6yasfia8xb444ahql7dc")))

(define-public crate-wild-2.0.1 (c (n "wild") (v "2.0.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)))) (h "1blkm9qjccz5diyq01g5zicn9i9cx6aasxcqs0xjzbyg8syms3k9")))

(define-public crate-wild-2.0.2 (c (n "wild") (v "2.0.2") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "1j1g0p43adwbqlj4p5m5k0xx3xzp3vifm7851rf1x1r8rvn4zlwp")))

(define-public crate-wild-2.0.3 (c (n "wild") (v "2.0.3") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "1mgfmbw698kn3xxc2pgdr2b9rcj6ah3srbxiaj56kx1l50jbga2b")))

(define-public crate-wild-2.0.4 (c (n "wild") (v "2.0.4") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "0800hfmb099abwh7gqqbxhlvl7l3g5x681qsy0rm0x2lp2mr6mq3")))

(define-public crate-wild-2.1.0-alpha.1 (c (n "wild") (v "2.1.0-alpha.1") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "1nibimslkrqcki2qpj7vwnnf74nj4vinnaqs96h8qpxzxba522g3") (f (quote (("parser")))) (y #t)))

(define-public crate-wild-2.1.0-alpha.2 (c (n "wild") (v "2.1.0-alpha.2") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "0rrjxwn7v1hji08dszfpcqk295qrwsk79y7k90il30krdxyar4in") (f (quote (("parser"))))))

(define-public crate-wild-2.1.0 (c (n "wild") (v "2.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)))) (h "0x0a65qrifm3q1gp7cy74qw69nr6zz5k8cqhb8pwbq3bb9l1dc85")))

(define-public crate-wild-2.2.0 (c (n "wild") (v "2.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)))) (h "0rqblf9sbfqvhgmihmh3iav5cs1i3psr4lpradd12njdm4qikl0h") (f (quote (("glob-quoted-on-windows"))))))

(define-public crate-wild-2.2.1-rc (c (n "wild") (v "2.2.1-rc") (d (list (d (n "glob") (r "^0.3.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)))) (h "094a8p91qzn8kvsnf614kb1qkjnaimh0pg5xf9l6jxk1rf15ycjb") (f (quote (("glob-quoted-on-windows"))))))

(define-public crate-wild-2.2.1 (c (n "wild") (v "2.2.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)))) (h "1q8hnhmv3fvgx0j7bv8qig00599a15mfsdhgx3hq2ljpiky1l4x3") (f (quote (("glob-quoted-on-windows"))))))

