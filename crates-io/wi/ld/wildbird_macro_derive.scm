(define-module (crates-io wi ld wildbird_macro_derive) #:use-module (crates-io))

(define-public crate-wildbird_macro_derive-0.0.1 (c (n "wildbird_macro_derive") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0wmjg91n0i5iypf3jdk9f9dbvc7mxnxd8ad97yhca4ag3v33904q")))

(define-public crate-wildbird_macro_derive-0.0.2 (c (n "wildbird_macro_derive") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdy5l0rymagl3ilmhg1w5q893xwbxv1j7n9xk41bcjfw89y1p10")))

(define-public crate-wildbird_macro_derive-0.0.3 (c (n "wildbird_macro_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrr53gqfq46cqfp6s1xxsrbfcbvsdl1mk91mny03np70xqzqnz5") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.4 (c (n "wildbird_macro_derive") (v "0.0.4") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "050mv6wlbm9bgrqnx3d1nwirx1i10ypy06vygq4w16sy2hbyb6w1") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.5 (c (n "wildbird_macro_derive") (v "0.0.5") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0pjklsamvjvka6yvqcxvlynf00i1f7z0dziv3wfb60fx388qqc7h") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.6 (c (n "wildbird_macro_derive") (v "0.0.6") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0rzql8g12x5dyvywnjw8ydqnkmdz2jfz3qpryynfrry6qsqkcfgb") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.7 (c (n "wildbird_macro_derive") (v "0.0.7") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "11481p7h0wgzmsf14ij25kd2bk26jzn617lnz6mb9dvfw7vxzqzg") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.8 (c (n "wildbird_macro_derive") (v "0.0.8") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0798psvizwlxp810mxxk1jc0mxc6bkqykd2yxwixshi8fgyvp0h6") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.9 (c (n "wildbird_macro_derive") (v "0.0.9") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "08k5kinrfdsl9ksymscn4xwdn4wlmmjj7gpz9bqrmh4d32gs6a14") (r "1.70")))

(define-public crate-wildbird_macro_derive-0.0.10 (c (n "wildbird_macro_derive") (v "0.0.10") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0613cbmsj421hs2sdvfcc5pvncnkawbjmkcfd62k24wkr14pv8z7") (r "1.70")))

