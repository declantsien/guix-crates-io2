(define-module (crates-io wi ld wildmatch) #:use-module (crates-io))

(define-public crate-wildmatch-1.0.0 (c (n "wildmatch") (v "1.0.0") (d (list (d (n "ntest") (r "^0.4.1") (d #t) (k 2)))) (h "0zqss7h0c4g87i4gn6ci0vayn63rsi3mpzydglwmi4nbxp6nh7ra")))

(define-public crate-wildmatch-1.0.1 (c (n "wildmatch") (v "1.0.1") (d (list (d (n "ntest") (r "^0.4.1") (d #t) (k 2)))) (h "0ksriglx69pzgw34cp2lqa902glllhwqsm5i9jzzfd4ac7ipdzaz")))

(define-public crate-wildmatch-1.0.2 (c (n "wildmatch") (v "1.0.2") (d (list (d (n "ntest") (r "^0.4.1") (d #t) (k 2)))) (h "1y7bs8k0wv2fnv8qd9518sjakzfcsy4r9kz4gxjrszb0mi5jz9zy")))

(define-public crate-wildmatch-1.0.3 (c (n "wildmatch") (v "1.0.3") (d (list (d (n "ntest") (r "^0.4.1") (d #t) (k 2)))) (h "0nv5icy6ixh2bz73wic3syl8b204zydjh177qgl97m14vva0w6v1")))

(define-public crate-wildmatch-1.0.4 (c (n "wildmatch") (v "1.0.4") (d (list (d (n "ntest") (r "^0.5.1") (d #t) (k 2)))) (h "0zjlfzc8aqzk8aj8r39q15hn9qjirs9p1zprgwi5ib6aiqigv3ib")))

(define-public crate-wildmatch-1.0.5 (c (n "wildmatch") (v "1.0.5") (d (list (d (n "ntest") (r "^0.5.1") (d #t) (k 2)))) (h "01ch4fsai85rivazfjrjvgymram61jx6a9ixqidn0ax7wpbyi6pb")))

(define-public crate-wildmatch-1.0.6 (c (n "wildmatch") (v "1.0.6") (d (list (d (n "ntest") (r "^0.5.1") (d #t) (k 2)))) (h "02h8d5v100r66p06pgqp7l8wbjm9gcb1cmncqknbr3gay55dq2mz")))

(define-public crate-wildmatch-1.0.8 (c (n "wildmatch") (v "1.0.8") (d (list (d (n "ntest") (r "^0.5.1") (d #t) (k 2)))) (h "1jsrr0fw0z93yf7rxpfggdyavlisfpr3p6ikwjmjq16ylbbd2jjl")))

(define-public crate-wildmatch-1.0.9 (c (n "wildmatch") (v "1.0.9") (d (list (d (n "ntest") (r "^0.6.0") (d #t) (k 2)))) (h "1m13nqy93xhf75y6r975ka5flfngw17z6dqr83kp6n7lk33svnr1")))

(define-public crate-wildmatch-1.0.10 (c (n "wildmatch") (v "1.0.10") (d (list (d (n "ntest") (r "^0.6.0") (d #t) (k 2)))) (h "1kjkk87wj7yr1ipjlnqcfc46kqmma03cm5zw5vwcakixgmm30c7f")))

(define-public crate-wildmatch-1.0.11 (c (n "wildmatch") (v "1.0.11") (d (list (d (n "ntest") (r "^0.7.1") (d #t) (k 2)))) (h "0p31whz3jsl9dk5wamcmhqim5xzqn5cg6mzvdlr4dzf3rr8c2ydf")))

(define-public crate-wildmatch-1.0.12 (c (n "wildmatch") (v "1.0.12") (d (list (d (n "ntest") (r "^0.7.1") (d #t) (k 2)))) (h "0x7bq3sy72rj31fkhyw0w254f4aqc1paqkgf8vdp4v56bjm6sd3r")))

(define-public crate-wildmatch-1.0.13 (c (n "wildmatch") (v "1.0.13") (d (list (d (n "ntest") (r "^0.7.3") (d #t) (k 2)))) (h "02n4v27v7mbkigdhjahnxd6k6dnqc1v5r6zydfd9kmm0zla9hffj")))

(define-public crate-wildmatch-1.1.0 (c (n "wildmatch") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "02i7qwjy1rkhzp80v9i9khzf09rhr4d534wcap7i6hfkc9gvji3z")))

(define-public crate-wildmatch-2.0.0 (c (n "wildmatch") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "082yi6pxlk2vm84h4qsvaclc7cgkfi41vb0s11wsc6zq23j7rbh7")))

(define-public crate-wildmatch-2.1.0 (c (n "wildmatch") (v "2.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "1q6l67q757a71bkzx1waja24hsvwjc7mfby177awxr7p1p98pi6n")))

(define-public crate-wildmatch-2.1.1 (c (n "wildmatch") (v "2.1.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "11nv29caiay9cprayccbdf7rl263yhzvpdcx1sr9vkzibzf3nn7f")))

(define-public crate-wildmatch-2.2.0 (c (n "wildmatch") (v "2.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n1b2zc5c2q3zr75h3iscpcmj42rv5gx8kjl8sjynjfnd114m97z") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wildmatch-2.3.0 (c (n "wildmatch") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3.4") (k 2)) (d (n "glob") (r "^0.3.0") (k 2)) (d (n "ntest") (r "^0.7.3") (k 2)) (d (n "regex") (r "^1.4.3") (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1szw6lfswk6ldr9d1bj3pin5bj32jckg0907yh2m0d61ydxw8pj9") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wildmatch-2.3.1 (c (n "wildmatch") (v "2.3.1") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "glob") (r "^0.3.1") (k 2)) (d (n "ntest") (r "^0.9.0") (k 2)) (d (n "regex") (r "^1.10.2") (k 2)) (d (n "regex-lite") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1a6qyqjrgrz2jq259wh2bjk233zgxpa30k3s3gci04ikv270lzq1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wildmatch-2.3.2 (c (n "wildmatch") (v "2.3.2") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "glob") (r "^0.3.1") (k 2)) (d (n "ntest") (r "^0.9.0") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (k 2)) (d (n "regex-lite") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0if4ql8kcg2493hsh0wkhlild0k5zngjjfhgkqxpywxhhx33pmnr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wildmatch-2.3.3 (c (n "wildmatch") (v "2.3.3") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "glob") (r "^0.3.1") (k 2)) (d (n "ntest") (r "^0.9.0") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (k 2)) (d (n "regex-lite") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "09dhkskicsyi74abcav57mschy7g16r9ibfsgwsl45bkpk0mk7lk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wildmatch-2.3.4 (c (n "wildmatch") (v "2.3.4") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "glob") (r "^0.3.1") (k 2)) (d (n "ntest") (r "^0.9.0") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (k 2)) (d (n "regex-lite") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "06bcn7rrhs47yqpnjwid39nfrr0ykn06n9lks3z210lif6cr6a1r") (s 2) (e (quote (("serde" "dep:serde"))))))

