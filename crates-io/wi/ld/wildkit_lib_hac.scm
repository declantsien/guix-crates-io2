(define-module (crates-io wi ld wildkit_lib_hac) #:use-module (crates-io))

(define-public crate-wildkit_lib_hac-0.1.0 (c (n "wildkit_lib_hac") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)))) (h "052yj83wp0g6chqpq4am008bfxik0ba6yds62hjs6ch1r3m425zm")))

(define-public crate-wildkit_lib_hac-0.1.1 (c (n "wildkit_lib_hac") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)))) (h "1abcdc89bgnpc132gvjlylrnpib9da8nvd4jhl09b6v11j1zl3v3")))

