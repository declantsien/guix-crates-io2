(define-module (crates-io wi ld wild-doc-client-lib) #:use-module (crates-io))

(define-public crate-wild-doc-client-lib-0.0.0 (c (n "wild-doc-client-lib") (v "0.0.0") (h "0cjwdxi1kmpc3cfsndjg3lc29dm8pznp80f4vfwbvv5qlkgzgzqw") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.1 (c (n "wild-doc-client-lib") (v "0.0.1") (h "1p27frrfj49di8qz57ggnq9zq8ljqng8bjpb4jh60zgfa8yqvlaz") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.2 (c (n "wild-doc-client-lib") (v "0.0.2") (h "1mladf8fr6sk2wf5dk6zzwrnrygwfp60cppafc0dmr1xahal83x1") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.3 (c (n "wild-doc-client-lib") (v "0.0.3") (h "02wxp09g01rw8hyyqqy2pwix1ss40szsq3aahy59rgl04mfj3jig") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.4 (c (n "wild-doc-client-lib") (v "0.0.4") (h "1pqn8wgdnzj372wy6kb5frjj993b3q5fvln8v3rs462v2c2ycag3") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.5 (c (n "wild-doc-client-lib") (v "0.0.5") (h "0kps1ina5bm3siys3bly57aadwa3482bmlq86v17nv1wgaf4b99d") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.6 (c (n "wild-doc-client-lib") (v "0.0.6") (h "0cxlyi6mav77whx5hyi56ac9rg6p7x04zsc3lxjs3qa0x1k66wz2") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.7 (c (n "wild-doc-client-lib") (v "0.0.7") (h "0m2g99bwljfa1k9lspv95aydfgicd1fri92ghizkbv4zn573bqj5") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.8 (c (n "wild-doc-client-lib") (v "0.0.8") (h "180v6f80r6l8wc3mx5ypy9y8i6ry9srm4sib1ax2hxlhw6q9jfj0") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.9 (c (n "wild-doc-client-lib") (v "0.0.9") (h "1lmv7c4ginsf8cp1myniq9fwv6xbkg87765w91p3086ibvpsgji3") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.10 (c (n "wild-doc-client-lib") (v "0.0.10") (h "00ygx0dd17rx0ph36kbvpj7fq6fwibq2hx5wnj60hmfhdfc39nlc") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.11 (c (n "wild-doc-client-lib") (v "0.0.11") (h "18vrxpgwbyi8p6x70w43dadbagv9pz8qvcl6nwk906q1mb477ikn") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.12 (c (n "wild-doc-client-lib") (v "0.0.12") (h "1lk5slv9xvxc1vn90z38v388h29335cra7vzq28vx2bp9kv8lfhb") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.13 (c (n "wild-doc-client-lib") (v "0.0.13") (h "16zpnafgy2vlffrz6ci474pxq078lbj7y0mqvylzdmw9wcrrzz24") (y #t)))

(define-public crate-wild-doc-client-lib-0.0.14 (c (n "wild-doc-client-lib") (v "0.0.14") (h "0lr62qw944i7h0q8qhmxs9zk97phpvkfc2xyr4n898zn9yq2l9wb")))

(define-public crate-wild-doc-client-lib-0.0.15 (c (n "wild-doc-client-lib") (v "0.0.15") (h "03fjrrsm9yig587x41nrdy9cmajc9fy5q27mz67whqvl910lrs31")))

(define-public crate-wild-doc-client-lib-0.0.16 (c (n "wild-doc-client-lib") (v "0.0.16") (h "1h0fgk097sw2ga7n5c51vdmd47c5hkq96pzmcl49mzm8bfav1h0j")))

(define-public crate-wild-doc-client-lib-0.1.0 (c (n "wild-doc-client-lib") (v "0.1.0") (h "1ry84vmk6x1hfqnsr8ynfg4x6bv65n81kvhwx3qsvlblzq59qm42")))

(define-public crate-wild-doc-client-lib-0.1.1 (c (n "wild-doc-client-lib") (v "0.1.1") (h "12hn9zhzn3a31zhkbzrphx0yiwznpxdh6nhm1vxqfkp22awwsx44")))

(define-public crate-wild-doc-client-lib-0.2.0 (c (n "wild-doc-client-lib") (v "0.2.0") (h "1i4fapqswfasah690widihf8j6w4rh6d081b2yg35cfchcsvnnm4")))

(define-public crate-wild-doc-client-lib-0.3.0 (c (n "wild-doc-client-lib") (v "0.3.0") (h "0v0gpi89j0f5wdc9kg7nl6yd5yfa2fmpqfxjjn45qrpiy0m3kjbr")))

(define-public crate-wild-doc-client-lib-0.3.1 (c (n "wild-doc-client-lib") (v "0.3.1") (h "0z73da6w9fpymk5qvc3h1sgsf658ly27n21snqd3b3v596nrzwrh")))

(define-public crate-wild-doc-client-lib-0.3.3 (c (n "wild-doc-client-lib") (v "0.3.3") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)))) (h "0m0mk8xwcpk9a14vzs6whwibzg73hdfbvrb90m4hkkqdy5cp9cjl")))

(define-public crate-wild-doc-client-lib-0.3.4 (c (n "wild-doc-client-lib") (v "0.3.4") (d (list (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)))) (h "0nl8c5inhq3892hacm2j6i6n7pzyjl7yvdywmy8zg4vg4qpjx4pn")))

