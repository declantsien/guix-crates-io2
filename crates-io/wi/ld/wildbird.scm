(define-module (crates-io wi ld wildbird) #:use-module (crates-io))

(define-public crate-wildbird-0.0.1 (c (n "wildbird") (v "0.0.1") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wildbird_macro_derive") (r "^0.0.1") (d #t) (k 0)))) (h "0vnqpv9wbwmjzircfq5n70xz8afsak5f1d1iqqbg4xzy8igbnz6m") (y #t)))

(define-public crate-wildbird-0.0.2 (c (n "wildbird") (v "0.0.2") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "wildbird_macro_derive") (r "^0.0.2") (d #t) (k 0)))) (h "1lm5pi55jkr8rqfwdfc345s9wcylgaa76vcfpxv1p9pm10lcmq6v") (y #t)))

(define-public crate-wildbird-0.0.3 (c (n "wildbird") (v "0.0.3") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.3") (d #t) (k 0)))) (h "03da94l8knh4s6xgjmy47gxx72xcf6p410wfndy857si1nd741az") (r "1.70")))

(define-public crate-wildbird-0.0.4 (c (n "wildbird") (v "0.0.4") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.4") (d #t) (k 0)))) (h "1136dycr35467i8ffs5rkd8yabwg7lpp2hc5w46azxqfrjsi20l6") (r "1.70")))

(define-public crate-wildbird-0.0.5 (c (n "wildbird") (v "0.0.5") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.5") (d #t) (k 0)))) (h "1l51d2dml0nvwqjdjgd2fq9jx971v6sy2vjc8vh6bxwz24gg6imx") (y #t) (r "1.70")))

(define-public crate-wildbird-0.0.6 (c (n "wildbird") (v "0.0.6") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.6") (d #t) (k 0)))) (h "00yl6rpscl9mm96mil9kjbm10zaawrck392frdp1wg4pp63v1wb5") (r "1.70")))

(define-public crate-wildbird-0.0.7 (c (n "wildbird") (v "0.0.7") (d (list (d (n "cargo-expand") (r "^1.0.48") (d #t) (k 2)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.7") (d #t) (k 0)))) (h "16qg0b6n0w4494s0hhh51c0d51fijasgmnyikgmc78khzblgpqmz") (r "1.70")))

(define-public crate-wildbird-0.0.8 (c (n "wildbird") (v "0.0.8") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.8") (d #t) (k 0)))) (h "0ijx4f9w68a9nwz1vhqjgc3570wv371lcxvqsp3ww2bcp8yxbp4b") (s 2) (e (quote (("tokio" "dep:tokio")))) (r "1.70")))

(define-public crate-wildbird-0.0.9 (c (n "wildbird") (v "0.0.9") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("signal" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.9") (d #t) (k 0)))) (h "095xvry2mhkkk40vv5qm87r62yavpwsh8wfkpg1d00q8smdkrj8f") (s 2) (e (quote (("tokio" "dep:tokio")))) (r "1.70")))

(define-public crate-wildbird-0.0.10 (c (n "wildbird") (v "0.0.10") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("signal" "rt-multi-thread" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)) (d (n "wildbird_macro_derive") (r "^0.0.10") (d #t) (k 0)))) (h "1mbzwsdckdmyc0mdzxzjym39vc7723rwyvxqbgq0vj85v8hdbwap") (f (quote (("default" "tokio" "rayon")))) (s 2) (e (quote (("tokio" "dep:tokio") ("rayon" "dep:rayon")))) (r "1.70")))

