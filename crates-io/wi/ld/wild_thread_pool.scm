(define-module (crates-io wi ld wild_thread_pool) #:use-module (crates-io))

(define-public crate-wild_thread_pool-0.1.0 (c (n "wild_thread_pool") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0yf2mcjkl2wzc78mdb8ywicyqfd0hcgynmssrvjwjzfy2irvxqmv")))

(define-public crate-wild_thread_pool-0.2.0 (c (n "wild_thread_pool") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1sl69sj9rcfcz2hh7xd3b9ziy3sz2lf80d4j6vz7p54y9agy6yvc")))

(define-public crate-wild_thread_pool-0.3.0 (c (n "wild_thread_pool") (v "0.3.0") (d (list (d (n "closer") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_tryjoin") (r "^0.2") (d #t) (k 0)))) (h "1dk3qd8avd92xs03yl2acqgxzgbw4ibdnrm0vyzvysvc9mwj60xf")))

(define-public crate-wild_thread_pool-0.4.0 (c (n "wild_thread_pool") (v "0.4.0") (d (list (d (n "closer") (r "^0.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "thread_tryjoin") (r "^0.2") (d #t) (k 0)))) (h "167vgp06874nafwyzy8mksvbxm0ajwk2nbfaffpifvznylxl0wza")))

