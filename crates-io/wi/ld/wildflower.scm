(define-module (crates-io wi ld wildflower) #:use-module (crates-io))

(define-public crate-wildflower-0.1.0 (c (n "wildflower") (v "0.1.0") (h "0sijswclszjc915rans3vvg5bjgyckxd4pg02mhhxvdz49djgr1c") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-wildflower-0.1.1 (c (n "wildflower") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "wildmatch") (r "^2.1.1") (d #t) (k 2)))) (h "03xgvqb6rz37dasw459ykdixl6sir3s14b0jkv3f09nnc3np9b35") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-wildflower-0.2.0 (c (n "wildflower") (v "0.2.0") (h "0sznllkcwm85pdq87lmjjngjlbbkpirgnrs12k8s7krpp6dpw8r0") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-wildflower-0.3.0 (c (n "wildflower") (v "0.3.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)) (d (n "yoke") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vhmdqag2zc82fz52b7j8q9wbkz5dz9xqkrpj7v2i88ac8laqf21") (f (quote (("std") ("default" "std") ("alloc"))))))

