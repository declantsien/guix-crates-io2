(define-module (crates-io wi ld wildfire-ferris) #:use-module (crates-io))

(define-public crate-wildfire-ferris-0.0.0 (c (n "wildfire-ferris") (v "0.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ferrisgram") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)))) (h "1676aawfmlzh95wqr6r9wkbqyrcfwms2j5vsp38dgg9bsl712ji7")))

(define-public crate-wildfire-ferris-0.0.1 (c (n "wildfire-ferris") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "ferrisgram") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)) (d (n "wildfire") (r "^0.0.0") (d #t) (k 0)))) (h "04j482c59cslsv6xxy8z8baq19is9map5dqzz2lhqb4a2z0vp2hp")))

