(define-module (crates-io wi ld wildpath) #:use-module (crates-io))

(define-public crate-wildpath-0.1.0 (c (n "wildpath") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "12wd6aij475q09i7zf07s30k2mg0l0fzvcviyiq69msk96m5c6hs")))

