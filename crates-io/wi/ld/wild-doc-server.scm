(define-module (crates-io wi ld wild-doc-server) #:use-module (crates-io))

(define-public crate-wild-doc-server-0.0.0 (c (n "wild-doc-server") (v "0.0.0") (d (list (d (n "wild-doc") (r "^0.5") (d #t) (k 0)))) (h "08l03hk0m4pkcbxnhs9yw8d3v9cxcxqpi0mmkbxsny44i12r0603") (y #t)))

(define-public crate-wild-doc-server-0.0.1 (c (n "wild-doc-server") (v "0.0.1") (d (list (d (n "wild-doc") (r "^0.6") (d #t) (k 0)))) (h "06nkf0hzjr7hjk42d07cajkngf8960d7ilgf2hiyqxv792rbscrz") (y #t)))

(define-public crate-wild-doc-server-0.0.2 (c (n "wild-doc-server") (v "0.0.2") (d (list (d (n "wild-doc") (r "^0.6") (d #t) (k 0)))) (h "0gi929rcmqyl2a19mfk00vn9a6fncigssaffmz001462mkdm2cf5") (y #t)))

(define-public crate-wild-doc-server-0.0.3 (c (n "wild-doc-server") (v "0.0.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.6") (d #t) (k 0)))) (h "046szskm53l9jfd2p5y5d3ij1hpk97jqw5ck0iqlkn78mkki8ycn") (y #t)))

(define-public crate-wild-doc-server-0.0.4 (c (n "wild-doc-server") (v "0.0.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.6") (d #t) (k 0)))) (h "0kkh1npzayssjf5s1vncnh2lkf6a431gi5ycs07xscqk0k08d1yx") (y #t)))

(define-public crate-wild-doc-server-0.0.5 (c (n "wild-doc-server") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.6") (d #t) (k 0)))) (h "06lwy7p6hwmdshcacl6wx7lqm3dj33w0r417vk8hzlwk2mh3vrms") (y #t)))

(define-public crate-wild-doc-server-0.0.6 (c (n "wild-doc-server") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.7") (d #t) (k 0)))) (h "10wvcmc5xkpv4wwzsf0zyrp1mxxzyj9q6cf7iiz40cfy7d22g10b") (y #t)))

(define-public crate-wild-doc-server-0.0.7 (c (n "wild-doc-server") (v "0.0.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.8") (d #t) (k 0)))) (h "1ccnjjwkgxz2xa5nwla96y1jg059jy2qamzc2k70d8gran17cb3j") (y #t)))

(define-public crate-wild-doc-server-0.0.8 (c (n "wild-doc-server") (v "0.0.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.8") (d #t) (k 0)))) (h "0n3sl97v5ckjfd3jcayw2xcx526srhf1rnqxxp9zwpjns0nnpw8w") (y #t)))

(define-public crate-wild-doc-server-0.0.9 (c (n "wild-doc-server") (v "0.0.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.9") (d #t) (k 0)))) (h "0hwnnyxnn6x7kn6nf1fjhay27fqh1xhv2w0bfg8kc6pzlbmsygkv") (y #t)))

(define-public crate-wild-doc-server-0.0.10 (c (n "wild-doc-server") (v "0.0.10") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.9") (d #t) (k 0)))) (h "0cavs7zzp8y1lm84k58bpia0smrvjnz161rkbccla0f6sxl27rjw") (y #t)))

(define-public crate-wild-doc-server-0.0.11 (c (n "wild-doc-server") (v "0.0.11") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.10") (d #t) (k 0)))) (h "0vl161cyxg1y1x3y37m4rprkjchivdn9cdxpp195ndyc58gmd184") (y #t)))

(define-public crate-wild-doc-server-0.0.12 (c (n "wild-doc-server") (v "0.0.12") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.11") (d #t) (k 0)))) (h "1gkjhjmwgy641ns92iqxg9vfymwny3kmzbpm9z2797cgqjqlz5yw") (y #t)))

(define-public crate-wild-doc-server-0.0.13 (c (n "wild-doc-server") (v "0.0.13") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.12") (d #t) (k 0)))) (h "01iv76292fjq3brmdgjp4ari8w2w0p3psi1sfh416mhv60gzq5hw") (y #t)))

(define-public crate-wild-doc-server-0.0.14 (c (n "wild-doc-server") (v "0.0.14") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.13") (d #t) (k 0)))) (h "0bghgaibx1qymf1ladbjvja4qr7zlp7brr16s0kssrvqm1v3gj62") (y #t)))

(define-public crate-wild-doc-server-0.0.15 (c (n "wild-doc-server") (v "0.0.15") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.14") (d #t) (k 0)))) (h "0dws2fkbb4iynf1cisyhavdd3wawpdain47i9hjr1zsfab8ac86z") (y #t)))

(define-public crate-wild-doc-server-0.0.16 (c (n "wild-doc-server") (v "0.0.16") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.15") (d #t) (k 0)))) (h "1hqks7c9sl3vwvg6i9hqbkb0i0xkxqpvfzhp0f8ca9zks4p29skm") (y #t)))

(define-public crate-wild-doc-server-0.0.17 (c (n "wild-doc-server") (v "0.0.17") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.16") (d #t) (k 0)))) (h "0j8l2a12gqa4464kgcbkdp6nai4pjn67pqghf6qip8cgfc188kib") (y #t)))

(define-public crate-wild-doc-server-0.0.18 (c (n "wild-doc-server") (v "0.0.18") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.17") (d #t) (k 0)))) (h "1mm5184p0f9m07hkkdzxmdvw2cgz4lar2v5nm1l790dyiwkrx2d4") (y #t)))

(define-public crate-wild-doc-server-0.0.19 (c (n "wild-doc-server") (v "0.0.19") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.18") (d #t) (k 0)))) (h "0myd7631990p6jsnqmapxlad1whni5gfcjgnag4y29p35bargjwz") (y #t)))

(define-public crate-wild-doc-server-0.0.20 (c (n "wild-doc-server") (v "0.0.20") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.19") (d #t) (k 0)))) (h "0wqk2wbvh9sdjhx4mzfp7ivrnaj568w5q1gkf11gi7zdiyjg1w1l") (y #t)))

(define-public crate-wild-doc-server-0.0.21 (c (n "wild-doc-server") (v "0.0.21") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0.21") (d #t) (k 0)))) (h "1md18xzmhh465ycps13319nw65qydmw8gbbhybhv1bzl5zapwfqh") (y #t)))

(define-public crate-wild-doc-server-0.0.22 (c (n "wild-doc-server") (v "0.0.22") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1pkaz5jqq35c6lkwbm2sy2mgddjh3pba0ldg21kc3j4qkxrdsfl3") (y #t)))

(define-public crate-wild-doc-server-0.0.23 (c (n "wild-doc-server") (v "0.0.23") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1g486l6myd0lj4csk8y1pm2vpcmxpkiq5s0v3qga4q4yliimgxp5") (y #t)))

(define-public crate-wild-doc-server-0.0.24 (c (n "wild-doc-server") (v "0.0.24") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1bv2sckh5k5w27wbb710vnsffix1h7vm6g6i33xvvvpmfnwg7pqw") (y #t)))

(define-public crate-wild-doc-server-0.0.25 (c (n "wild-doc-server") (v "0.0.25") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "17a55kfrql3m8kdym5cl985j0zsbkl6xvs9rvgfz9g5hb3rvfy8q") (y #t)))

(define-public crate-wild-doc-server-0.0.26 (c (n "wild-doc-server") (v "0.0.26") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "0phdzivx5lqpfzas5rcgkdk1j7wan8wzy2plcmbhpm3xnysyqkd7")))

(define-public crate-wild-doc-server-0.0.27 (c (n "wild-doc-server") (v "0.0.27") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1zdghi3vkwp8i1a0zrkh9gf1qn16y2lbc3wdz3qjr70vpwm357n9")))

(define-public crate-wild-doc-server-0.0.28 (c (n "wild-doc-server") (v "0.0.28") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "136gkhx53g37x3l1c149n6i7i8wmpspdldf9m5kx8j4wplc804ap")))

(define-public crate-wild-doc-server-0.0.29 (c (n "wild-doc-server") (v "0.0.29") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1djkc2zxa2nd3ayi9iswv2mrj3dhip17hvzhavp79fv0vx4h4dd3")))

(define-public crate-wild-doc-server-0.0.30 (c (n "wild-doc-server") (v "0.0.30") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1ggwgx1x3ma51w4ksrq9lkfhaqhjbdj3px0wqvjazcnrzwv2cdkv")))

(define-public crate-wild-doc-server-0.0.31 (c (n "wild-doc-server") (v "0.0.31") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "0w440w57k3nlafwvf7h5ddspqx1k1clrkk1r0wj6ibjlw8l4961p")))

(define-public crate-wild-doc-server-0.0.32 (c (n "wild-doc-server") (v "0.0.32") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "0j6i795bxccs0r4qz4lxff2lzqxqmwgwnk5rvavrr400wrz9lfxq")))

(define-public crate-wild-doc-server-0.0.33 (c (n "wild-doc-server") (v "0.0.33") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "11rmqw3058wk3zsyqjxgsmnywdxi0ql5q43vaa34dgwqnlzh59mg")))

(define-public crate-wild-doc-server-0.0.34 (c (n "wild-doc-server") (v "0.0.34") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "162sna44rlzrvm685bp2yr6lh1ff4lmw5kgskisq8ym66gpsygfm")))

(define-public crate-wild-doc-server-0.0.35 (c (n "wild-doc-server") (v "0.0.35") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1k529fysfsx9863fscyb3yjd70nv5md41w63yg9bfg41jsivcjsf")))

(define-public crate-wild-doc-server-0.0.36 (c (n "wild-doc-server") (v "0.0.36") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1mx9za560k59cka4bnk73b0s24rpkciixx7cfnd0b3nvcl80c4rd")))

(define-public crate-wild-doc-server-0.0.37 (c (n "wild-doc-server") (v "0.0.37") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1swhmbc60par3a6vcy5vq3202918vcayggkwzj8zx0hxdydw7ipw")))

(define-public crate-wild-doc-server-0.0.38 (c (n "wild-doc-server") (v "0.0.38") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "161aagf05ami6y2sip91hm8brhvrqd8x5lm1gv3y0cbymv11pm3m")))

(define-public crate-wild-doc-server-0.0.39 (c (n "wild-doc-server") (v "0.0.39") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1wqijslrjs8cjs0v6djgmim7892g8iyx146vbhniygdjcf73wzkf")))

(define-public crate-wild-doc-server-0.0.40 (c (n "wild-doc-server") (v "0.0.40") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1jsxnp22zh6ddy2098lcc2wrjbzg215iiwgydln1pd1qf8pwbdh7")))

(define-public crate-wild-doc-server-0.0.41 (c (n "wild-doc-server") (v "0.0.41") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "168m9ny5f6iqgrkd0kxpnp8f87pyhmm6zwcwf4g1i8l2iiy65pjw")))

(define-public crate-wild-doc-server-0.1.0 (c (n "wild-doc-server") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1kjrajlylra6k427z5ms4znwmrdjcf4d4ci6vj70fyz85n4wwkmm")))

(define-public crate-wild-doc-server-0.1.1 (c (n "wild-doc-server") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1h5wpzm78y8askjklb7yc7aic1q7aipamm1lfyggjwp7j8ks410y")))

(define-public crate-wild-doc-server-0.1.2 (c (n "wild-doc-server") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "1y7x3y2c9bd16b6v9n3inwfidsb0l61fwjyvpphj747c8nq084ps")))

(define-public crate-wild-doc-server-0.2.0 (c (n "wild-doc-server") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "wild-doc") (r "^0") (d #t) (k 0)))) (h "0qfnr6dxmmrikf6rv2qzvz965djcqjyy9bpplzhmc9l75rkxgsxl")))

(define-public crate-wild-doc-server-0.3.0 (c (n "wild-doc-server") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.67") (d #t) (k 0)))) (h "1axick2lp2mkr9an6cd9h4207b02i50chl2xz7qc6c73r7sln8xr")))

(define-public crate-wild-doc-server-0.3.1 (c (n "wild-doc-server") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.68") (d #t) (k 0)))) (h "1g3pkh626djfwdyhdjw23fhmmwm6cg1cyn6y8by8zhazmzaa1ziw")))

(define-public crate-wild-doc-server-0.3.2 (c (n "wild-doc-server") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.69") (d #t) (k 0)))) (h "1d8iqympw696sda0fszc6y7axfw0lly5bsbhjbfnqgh0rkzcrg83")))

(define-public crate-wild-doc-server-0.3.3 (c (n "wild-doc-server") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.70") (d #t) (k 0)))) (h "00y0hns5b8p7y8i5qn1r8m0ghccdkg0mkq4zm27h9gxbxacciy9w")))

(define-public crate-wild-doc-server-0.3.4 (c (n "wild-doc-server") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.71") (d #t) (k 0)))) (h "1g5kkhpijy8s3dpdl8cka20xpja9di1s3i7i18bka884f4ws9qvp")))

(define-public crate-wild-doc-server-0.3.5 (c (n "wild-doc-server") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.72") (d #t) (k 0)))) (h "097hx0haiz3xjlha31z7gsywznk2iibdcr42jkn531v6my7dma6q")))

(define-public crate-wild-doc-server-0.4.0 (c (n "wild-doc-server") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.73") (d #t) (k 0)))) (h "139sgfnys1pm5a0gljvznsv3rsd7qs3k6m5klgwiqmc6v5ranja8")))

(define-public crate-wild-doc-server-0.5.0 (c (n "wild-doc-server") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.74") (d #t) (k 0)))) (h "1hrjfd4xcas2vban9hh82w3fsi1d8k3rri3b1sql0kbx3dmzi0q3")))

(define-public crate-wild-doc-server-0.5.1 (c (n "wild-doc-server") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.74") (d #t) (k 0)))) (h "0y5ifv52rs4ln13frfy0dfr2i1i61v47ca57z0maghg8jz5khbky")))

(define-public crate-wild-doc-server-0.5.2 (c (n "wild-doc-server") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.75") (d #t) (k 0)))) (h "0jdizkgc3dd1n1cvvprpnl00g27rdvyqmq49r1zl8nmz6s5nrvlj")))

(define-public crate-wild-doc-server-0.5.3 (c (n "wild-doc-server") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.76") (d #t) (k 0)))) (h "0nc0b9h82jnbq858q8wsw2n6rn4l9df60qgjfg3ya95ighy6h0zs")))

(define-public crate-wild-doc-server-0.5.4 (c (n "wild-doc-server") (v "0.5.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.77") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "037bqkc4n4mmjhzc0cn1625h14q002kld4p8qds4j7l7qfy3w94h")))

(define-public crate-wild-doc-server-0.5.5 (c (n "wild-doc-server") (v "0.5.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.78") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "1zkjkw57c0pw9plnmnf9ddnqx4ryvq182kmqak1cz4nq8kqbnxka")))

(define-public crate-wild-doc-server-0.5.6 (c (n "wild-doc-server") (v "0.5.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.79") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "1251hy5fkk27yb1gzsidjhqxgr2m677qmjjjiydgn3n861qa0s21")))

(define-public crate-wild-doc-server-0.6.0 (c (n "wild-doc-server") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.80") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.1") (d #t) (k 0)))) (h "1ra6w6db57l1n4avxkc9mjfbyilcw4vv6w9p8qb6l1g1ijxv1qv2")))

(define-public crate-wild-doc-server-0.6.1 (c (n "wild-doc-server") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.81") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.1") (d #t) (k 0)))) (h "0zinblp8mqzckd94r92i2r3qpgfs6f3n77kx5casw84dh5h1bzyx")))

(define-public crate-wild-doc-server-0.7.0 (c (n "wild-doc-server") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.82") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.1") (d #t) (k 0)))) (h "1vkrhs49p633r9jhfsx90hn4yjjyz9z0081jmv8pjlzdxqm8azh9")))

(define-public crate-wild-doc-server-0.7.1 (c (n "wild-doc-server") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.82") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "1djbgzabir22p9kfahb7l2awxyqnqnldn82ndb7wwrmvpk893aq5")))

(define-public crate-wild-doc-server-0.7.2 (c (n "wild-doc-server") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.85") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "168xnp2kjjg124w73vnv24p55shlldvkdvnb76k2a2dkh4q4qybb")))

(define-public crate-wild-doc-server-0.7.3 (c (n "wild-doc-server") (v "0.7.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.86") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "0yazaic8wdwvlmwwykyb72wcsc6qj8d1kx2cmis0vws9sxnhjigq")))

(define-public crate-wild-doc-server-0.7.4 (c (n "wild-doc-server") (v "0.7.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.87") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "14nqxz4w74cq81kq59607cfyrnklnmqxakr9q5p7f2myx5ld5qjr")))

(define-public crate-wild-doc-server-0.7.5 (c (n "wild-doc-server") (v "0.7.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.87") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "1fi82iwhga436vz5jf0qa31b17ygs26ll469azsil6qznq063kpa")))

(define-public crate-wild-doc-server-0.8.0 (c (n "wild-doc-server") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.88") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "0pzlfhwfh4vws72aig5vrkxwzs7c946y1c379xp7vppgifassxmw")))

(define-public crate-wild-doc-server-0.8.1 (c (n "wild-doc-server") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.89") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.3") (d #t) (k 0)))) (h "1dlgpwq3446f42h2fl2l6bjc3m02s769cpx4ghy56p1in3g741v1")))

(define-public crate-wild-doc-server-0.8.2 (c (n "wild-doc-server") (v "0.8.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.90") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.4") (d #t) (k 0)))) (h "09h8hq8zz3nzrjll0nda2b5carylpzgjcq43lpw4vksqlxzxzl5n")))

(define-public crate-wild-doc-server-0.8.3 (c (n "wild-doc-server") (v "0.8.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.90") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.4") (d #t) (k 0)))) (h "19s4y2q96ij1kl42vzjrz3pnd94mm54dsjg8vr2ma7ypvg414wyk")))

(define-public crate-wild-doc-server-0.9.0 (c (n "wild-doc-server") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.91") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.5") (d #t) (k 0)))) (h "1li8x8f2635qij4gdccc7jpi4fb7cnk5bhnwybk0kwqdnrn5gv0v")))

(define-public crate-wild-doc-server-0.9.1 (c (n "wild-doc-server") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.91") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.6") (d #t) (k 0)))) (h "0ffh5xs7h1clvmzbc3j1dpa8k96gkinz73mvigm520342rv0v56h")))

(define-public crate-wild-doc-server-0.9.2 (c (n "wild-doc-server") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.92") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.6") (d #t) (k 0)))) (h "04lxr17b0fmxrl2l9pp33hr7nyxl1bwavf4cqcwh6j02cqnrkjq0")))

(define-public crate-wild-doc-server-0.9.3 (c (n "wild-doc-server") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.92") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.7") (d #t) (k 0)))) (h "19bfjwxdqby9s2lvqx5382aqwynz32d5q70fcccahl79d5ng9q0r")))

(define-public crate-wild-doc-server-0.9.4 (c (n "wild-doc-server") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.93") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.8") (d #t) (k 0)))) (h "07bfzn9kidqdq0cswb4c4q8zskvdr324d1adqgfnphq7d01amm0r")))

(define-public crate-wild-doc-server-0.9.5 (c (n "wild-doc-server") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.94") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.8") (d #t) (k 0)))) (h "1697qcrir8mwqqw60qj7bysr2i3pjdsm8a99gqcrvvv3gjy5h0dp")))

(define-public crate-wild-doc-server-0.10.0 (c (n "wild-doc-server") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.95") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.9") (d #t) (k 0)))) (h "01m3mdi3ai2byy5pw5s5ig6n9859da9nb1ma0lyi1l7l5kd6lclv")))

(define-public crate-wild-doc-server-0.10.1 (c (n "wild-doc-server") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.96") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.9") (d #t) (k 0)))) (h "10g0ff7g1rpasf3sn9ajzf60k49zk2msnkiyjgyzp6k00s3yg42g")))

(define-public crate-wild-doc-server-0.10.2 (c (n "wild-doc-server") (v "0.10.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.96") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.10") (d #t) (k 0)))) (h "0frf9rnncgqb1slgmxlqbfjyj6admfimaxyr22xb8bjfp8479x67")))

(define-public crate-wild-doc-server-0.10.3 (c (n "wild-doc-server") (v "0.10.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.96") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.11") (d #t) (k 0)))) (h "1w3m3ld3a07n15yr6fpblcjiksnrnrgpvhp23dh4zfqc2x5jscgd")))

(define-public crate-wild-doc-server-0.10.4 (c (n "wild-doc-server") (v "0.10.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.96") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.11") (d #t) (k 0)))) (h "0b9x5h1b79w8vhd746ha8kf8qf4wxwkn1cg9kj2wbaq8w8p71knz")))

(define-public crate-wild-doc-server-0.10.5 (c (n "wild-doc-server") (v "0.10.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.97") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.12") (d #t) (k 0)))) (h "0l36r1418jg0pk3gqkqn4vbc2j6c3zxpgmwlfmwg4lm7nhmjg1nc")))

(define-public crate-wild-doc-server-0.10.6 (c (n "wild-doc-server") (v "0.10.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.97") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.13") (d #t) (k 0)))) (h "1c6933lbphkaia7q81zg29dnd411a6dw1rxbiinsqkapx4wsr4lp")))

(define-public crate-wild-doc-server-0.10.7 (c (n "wild-doc-server") (v "0.10.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.97") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.14") (d #t) (k 0)))) (h "1s74n08mg4nqxiwz4rh1nj2cim6krls7iv604w7578k822h5axx4")))

(define-public crate-wild-doc-server-0.10.8 (c (n "wild-doc-server") (v "0.10.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.98") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.15") (d #t) (k 0)))) (h "167hww4rn2c3vaxlhnm6cli8gbgqpr143hfky9hq1a3gi6wzkc6c")))

(define-public crate-wild-doc-server-0.10.9 (c (n "wild-doc-server") (v "0.10.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.98") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.16") (d #t) (k 0)))) (h "02v9440wynbviwbfxxrw6iwr55vls787r3hw3jmrlmn1hixjaj4m")))

(define-public crate-wild-doc-server-0.10.10 (c (n "wild-doc-server") (v "0.10.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.98") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.17") (d #t) (k 0)))) (h "1f1v6812lf9y7f1w6xi2czz9n25zb9nncljk7n2hm1y70irhg6iz")))

(define-public crate-wild-doc-server-0.10.11 (c (n "wild-doc-server") (v "0.10.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.98") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.18") (d #t) (k 0)))) (h "1b4f7y96565j4f8sg7il492z9hm8ygl1a4wcf5lfs33hi4zm55g8")))

(define-public crate-wild-doc-server-0.10.12 (c (n "wild-doc-server") (v "0.10.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.98") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.19") (d #t) (k 0)))) (h "133m6sw5f1cdfpaqf4y1vhm9gqbj5w08jhxd9sdkwj88jmhb2z8m")))

(define-public crate-wild-doc-server-0.11.0 (c (n "wild-doc-server") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.99") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.19") (d #t) (k 0)))) (h "07jafnpc0k27k9dypglqcmvlgab1fhnxxg8dwdd28m0zvl0c3csv") (y #t)))

(define-public crate-wild-doc-server-0.11.1 (c (n "wild-doc-server") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.100") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.20") (d #t) (k 0)))) (h "01qk7sv38yq5pq8xc68h03xgmgld4ysyvai7q9jp6dxc288p0l4s") (y #t)))

(define-public crate-wild-doc-server-0.11.2 (c (n "wild-doc-server") (v "0.11.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.100") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.20") (d #t) (k 0)))) (h "1f8n5id12frgz9yp4m72v3ndwz6i9sc1xjmkqcpnxg9l1fyjfnr7")))

(define-public crate-wild-doc-server-0.11.3 (c (n "wild-doc-server") (v "0.11.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.100") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.21") (d #t) (k 0)))) (h "117nqy73w96isg7gr5czlxqyri0h79rlx68p7h7in4s0wybmcgqf")))

(define-public crate-wild-doc-server-0.11.4 (c (n "wild-doc-server") (v "0.11.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.100") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.22") (d #t) (k 0)))) (h "120j9rrjf6v08lalkxsg4w6xqhxzq27w57ii5qmhidh4ywgmn962")))

(define-public crate-wild-doc-server-0.11.5 (c (n "wild-doc-server") (v "0.11.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.101") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.23") (d #t) (k 0)))) (h "1gw91iz1fk843yj8f9w9wli17n3b847iaywaspkz9b9ph8gr2cm4")))

(define-public crate-wild-doc-server-0.12.0 (c (n "wild-doc-server") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.102") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.23") (d #t) (k 0)))) (h "1ffnr52ddjnmrxmcp7pbzmg0jx358g2h8p659nn4w5p9sik7pbir")))

(define-public crate-wild-doc-server-0.13.0 (c (n "wild-doc-server") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "wild-doc") (r "^0.103") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.24") (d #t) (k 0)))) (h "0zxxwhrym06k3ks9zlxdmjr13gc4s8ahyybcl3vggs94lby8yr5d")))

(define-public crate-wild-doc-server-0.13.1 (c (n "wild-doc-server") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.104") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.25") (d #t) (k 0)))) (h "1y8gqaja7c9f3bakci9b31p2nhp2j0lbrhnlifgrs32saafi3641")))

(define-public crate-wild-doc-server-0.13.2 (c (n "wild-doc-server") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.104") (f (quote ("js" "py"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.26") (d #t) (k 0)))) (h "1i5a7x60qqq95pwfvcmyd7gq4dgnhbnynkvqd0kilf4cvp1xbdx0")))

(define-public crate-wild-doc-server-0.13.3 (c (n "wild-doc-server") (v "0.13.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.104") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.26") (d #t) (k 0)))) (h "0nxmd01b0c44zbl2j0ac34sbvz22l07cbalzcawzlxqricfqqdi1")))

(define-public crate-wild-doc-server-0.13.4 (c (n "wild-doc-server") (v "0.13.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.106") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.28") (d #t) (k 0)))) (h "139cfpi5jgp3fsxgcygzz7b3a9fw775h4vy8nxdaljyc76faxyxg")))

(define-public crate-wild-doc-server-0.13.5 (c (n "wild-doc-server") (v "0.13.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.107") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.0") (d #t) (k 0)))) (h "1pq3xiy68dg0qpw7l4vskak39kim03z0lg0zafl8c4v740g40dmx")))

(define-public crate-wild-doc-server-0.13.6 (c (n "wild-doc-server") (v "0.13.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.107") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.0") (d #t) (k 0)))) (h "0vf8h6qqrh6qsv1pha17s2csvyav4qsml1sik04m6mrfkx3bvbds")))

(define-public crate-wild-doc-server-0.13.7 (c (n "wild-doc-server") (v "0.13.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.107.2") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.2") (d #t) (k 0)))) (h "1viw8snm4fa4y7hkhibzaq78qx4kqih65ya84pnfginh83hm374r")))

(define-public crate-wild-doc-server-0.13.8 (c (n "wild-doc-server") (v "0.13.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.108.0") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.30.0") (d #t) (k 0)))) (h "1j4gdp4b0kxf49vj95b7sg1bclvfcp8flszjvb52mb7h1yqym0nb")))

(define-public crate-wild-doc-server-0.13.9 (c (n "wild-doc-server") (v "0.13.9") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.109") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.31.0") (d #t) (k 0)))) (h "0im2z84i23wq2fjcxrpmvrfy8g7lq5nyfrh2gnjaqp7zspgyw10z")))

(define-public crate-wild-doc-server-0.13.10 (c (n "wild-doc-server") (v "0.13.10") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "wild-doc") (r "^0.110") (f (quote ("js" "py" "image"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.31.0") (d #t) (k 0)))) (h "14jis1vcxar1rszfc3dgc6ijpsn00k54fjzrn89mn4w5h02vqf9d")))

