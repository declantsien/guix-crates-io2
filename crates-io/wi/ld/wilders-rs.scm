(define-module (crates-io wi ld wilders-rs) #:use-module (crates-io))

(define-public crate-wilders-rs-0.1.0 (c (n "wilders-rs") (v "0.1.0") (d (list (d (n "ta-common") (r "^0.1.0") (d #t) (k 0)))) (h "1f4l3884hw5dp7lzl2ij3dplr6mcpyj4xlm4n6cn54ycr08l45cl")))

(define-public crate-wilders-rs-0.1.1 (c (n "wilders-rs") (v "0.1.1") (d (list (d (n "ta-common") (r "^0.1.1") (d #t) (k 0)))) (h "132ll9rvv1h5ywppr1kfcb7impzipbryg28qjfcg50fkd7l72k0s")))

