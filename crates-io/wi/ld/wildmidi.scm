(define-module (crates-io wi ld wildmidi) #:use-module (crates-io))

(define-public crate-wildmidi-0.1.0 (c (n "wildmidi") (v "0.1.0") (d (list (d (n "simple-error") (r "^0.1") (d #t) (k 0)))) (h "131aif1sal6xah0zp3dybxcld1yx420rkijf8w5ys4p05f1v8rsj")))

(define-public crate-wildmidi-0.1.1 (c (n "wildmidi") (v "0.1.1") (d (list (d (n "simple-error") (r "^0.1") (d #t) (k 0)))) (h "0w2pfw6cfzqzy83hwz54yd18vgxla1bckw0hmiisfjzzd1gbsv6q") (l "WildMidi")))

