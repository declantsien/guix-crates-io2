(define-module (crates-io wi ld wildescape) #:use-module (crates-io))

(define-public crate-wildescape-0.1.0 (c (n "wildescape") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "ntest") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.3") (d #t) (k 2)))) (h "1l3lw98zwcc3rlbgyi57qq45h8zchdjiiqz00jfqhp5f48mnwxzr")))

