(define-module (crates-io wi ld wildtiger) #:use-module (crates-io))

(define-public crate-wildtiger-0.0.1 (c (n "wildtiger") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sys") (r "^0.1") (d #t) (k 0) (p "randomx-sys")))) (h "14flkkzwawlbwnysn96dv1v88a64cb0zlj5k7wx0kg5ffdpx9aym")))

(define-public crate-wildtiger-0.0.3 (c (n "wildtiger") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sys") (r "^0.1") (d #t) (k 0) (p "randomx-sys")) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1.0") (d #t) (k 0)))) (h "0gjw3yxn3f1pv2lzm6nllvq87ffr2ir8psahvjciifszmvzalr8b")))

