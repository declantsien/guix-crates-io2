(define-module (crates-io wi ld wildland-lfs) #:use-module (crates-io))

(define-public crate-wildland-lfs-0.40.0-rc.0 (c (n "wildland-lfs") (v "0.40.0-rc.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wildland-dfs") (r "^0.40.0-rc.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0lhksi8zfv3mqk18vhgkqbkga5jckplv306lhvm1kc1d9xhjdfci")))

(define-public crate-wildland-lfs-0.40.0 (c (n "wildland-lfs") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wildland-dfs") (r "^0.40.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00ncd35g8ivb17qhn34pm8h3jgy7c5lw0hb12xa49zkjh1vlgxaw")))

