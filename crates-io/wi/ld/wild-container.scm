(define-module (crates-io wi ld wild-container) #:use-module (crates-io))

(define-public crate-wild-container-0.1.0 (c (n "wild-container") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "012dqldgrhj0afqzachdn1k1a85mwqfy4z76c00174vwclgpc333") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

