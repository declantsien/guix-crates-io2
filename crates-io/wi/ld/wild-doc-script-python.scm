(define-module (crates-io wi ld wild-doc-script-python) #:use-module (crates-io))

(define-public crate-wild-doc-script-python-0.0.1 (c (n "wild-doc-script-python") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "0r1x030cisnyzw47dwdn926r4a8fw3vi3mdj9kv5yb73p69zv3rs")))

(define-public crate-wild-doc-script-python-0.0.3 (c (n "wild-doc-script-python") (v "0.0.3") (d (list (d (n "pyo3") (r "^0.19.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "17f38wilcj0rscghk7cwqsqwg2hx7laf5p2q3fp46czmwrccn746")))

(define-public crate-wild-doc-script-python-0.1.0 (c (n "wild-doc-script-python") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.19.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.0") (d #t) (k 0)))) (h "04bca83ywix35mlvgfmdypw7m99bws5nc1cqzc1bqlm96c0gsw0g")))

(define-public crate-wild-doc-script-python-0.1.1 (c (n "wild-doc-script-python") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.19.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.1") (d #t) (k 0)))) (h "1f5515wi434g7n2i06b6bhmnmdj7xynmlsw7g39329i0h4k3sxbx")))

(define-public crate-wild-doc-script-python-0.1.3 (c (n "wild-doc-script-python") (v "0.1.3") (d (list (d (n "pyo3") (r "^0.19.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "0ynzqwyxwvfcidkrpd75ridwj27g69p9dhnx65prrd3mkil7929h")))

(define-public crate-wild-doc-script-python-0.1.4 (c (n "wild-doc-script-python") (v "0.1.4") (d (list (d (n "pyo3") (r "^0.19.1") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "0vam8y8wjj6l0db7mjkiklh5xmm9v6rd08yf1c7wzzdqhxz3nlvm")))

(define-public crate-wild-doc-script-python-0.1.5 (c (n "wild-doc-script-python") (v "0.1.5") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.2") (d #t) (k 0)))) (h "008vdcmyk4hva7xkrygyy4nd1m5gl2zzc4265qh1cranpkjnrvmr")))

(define-public crate-wild-doc-script-python-0.2.0 (c (n "wild-doc-script-python") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.3") (d #t) (k 0)))) (h "0mqygpkrz0vvbbcnp2wfih6a3i5lk5y18mypk2rnp2yghbdn6ssl")))

(define-public crate-wild-doc-script-python-0.3.0 (c (n "wild-doc-script-python") (v "0.3.0") (d (list (d (n "bson") (r "^2.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize" "serde"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.4") (d #t) (k 0)))) (h "1r9lb63s5yybm3dr71j2sr6lpd9qln548i44dmfam0yik175f9ih")))

(define-public crate-wild-doc-script-python-0.3.1 (c (n "wild-doc-script-python") (v "0.3.1") (d (list (d (n "bson") (r "^2.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize" "serde"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.4") (d #t) (k 0)))) (h "1gxm0z9qfz6zyzc7ry3hl3ssx79irfl2cr1frhpz9s0wck2w99gj")))

(define-public crate-wild-doc-script-python-0.4.0 (c (n "wild-doc-script-python") (v "0.4.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.5") (d #t) (k 0)))) (h "0s6xximx4xl7nfab9na5n2n8b9ayvgbw3ja7qhmvpf6s7sinajq1")))

(define-public crate-wild-doc-script-python-0.4.1 (c (n "wild-doc-script-python") (v "0.4.1") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.6") (d #t) (k 0)))) (h "0f0nz0zb4qa8bf0iy6jcwnlv717b92b9j22izc5bgz72368jc69v")))

(define-public crate-wild-doc-script-python-0.4.2 (c (n "wild-doc-script-python") (v "0.4.2") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.7") (d #t) (k 0)))) (h "18733ahz86r44745ly118kzgsa3xvyr4h2a0921i6sfizra71sak")))

(define-public crate-wild-doc-script-python-0.4.3 (c (n "wild-doc-script-python") (v "0.4.3") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.8") (d #t) (k 0)))) (h "0jb2d7r9i7wmb2jxhvcibi2fwp49qqbvdy422zbga6ifyv09nfjr")))

(define-public crate-wild-doc-script-python-0.5.0 (c (n "wild-doc-script-python") (v "0.5.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.9") (d #t) (k 0)))) (h "158xrwfanla5d52qam1gmqp8n5g0clr04qb4iqi6pa0v9c71j6d2")))

(define-public crate-wild-doc-script-python-0.5.1 (c (n "wild-doc-script-python") (v "0.5.1") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.9") (d #t) (k 0)))) (h "16wywwipy8649i93ql530my7ky9y61pvszcla9gbmxfap92frzi1")))

(define-public crate-wild-doc-script-python-0.6.0 (c (n "wild-doc-script-python") (v "0.6.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.10") (d #t) (k 0)))) (h "0hmb1d5w29rkycxmivh7lbp2f8hmdm667fli1lg85nix5qxd86fx")))

(define-public crate-wild-doc-script-python-0.7.0 (c (n "wild-doc-script-python") (v "0.7.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.11") (d #t) (k 0)))) (h "0dhm58zksfjk62358ym2jfiyix63q6fi2sjvz0pz9j4n50hc2r8d")))

(define-public crate-wild-doc-script-python-0.7.1 (c (n "wild-doc-script-python") (v "0.7.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.11") (d #t) (k 0)))) (h "06f351plxymc4ahm4np8kyn1cgmc373jp6z3d4na3add979qbmwh")))

(define-public crate-wild-doc-script-python-0.7.2 (c (n "wild-doc-script-python") (v "0.7.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.11") (d #t) (k 0)))) (h "09sn6695z3p039mvw45mqvypmgylqm2yvi2gk6jmn6j7a0mw3brq")))

(define-public crate-wild-doc-script-python-0.8.0 (c (n "wild-doc-script-python") (v "0.8.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.12") (d #t) (k 0)))) (h "0ms74b6ms8k6bl5xxm69jaj9pj6whq27b83w12a5z73m54fqk13z")))

(define-public crate-wild-doc-script-python-0.9.0 (c (n "wild-doc-script-python") (v "0.9.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.13") (d #t) (k 0)))) (h "0vh0sqkw3hkxv1rw4fzsx6916hy00bqqmljczllpj0fw5d20qhdd")))

(define-public crate-wild-doc-script-python-0.9.1 (c (n "wild-doc-script-python") (v "0.9.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.13") (d #t) (k 0)))) (h "10nc3kpzw9xbhs18dwlvxbl2yxkk1h64vkam5ysgg06i816p1ziq")))

(define-public crate-wild-doc-script-python-0.9.2 (c (n "wild-doc-script-python") (v "0.9.2") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.13") (d #t) (k 0)))) (h "0y9mlka9iqq257lz59if987bngyfai1vhz2jzyv5sbzjxw2fpras")))

(define-public crate-wild-doc-script-python-0.9.3 (c (n "wild-doc-script-python") (v "0.9.3") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.14") (d #t) (k 0)))) (h "1bnl3v9whpkn22a5sspgsy6v8q9s9swd6f8hm0895zk5i5il7r1a")))

(define-public crate-wild-doc-script-python-0.10.0 (c (n "wild-doc-script-python") (v "0.10.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.15") (d #t) (k 0)))) (h "0rdg36ma4ph2inscss5sp61nzj8wcdg45nx6hz7ch8wbnrwhhmq8")))

(define-public crate-wild-doc-script-python-0.10.1 (c (n "wild-doc-script-python") (v "0.10.1") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.16") (d #t) (k 0)))) (h "0pm2ymivpn7dz3zwgn3b7drfgyjinfddfkj50nz6kmk0pakq6q2s")))

(define-public crate-wild-doc-script-python-0.11.0 (c (n "wild-doc-script-python") (v "0.11.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.17") (d #t) (k 0)))) (h "1fi1gjrhz4g2z6z67rnkzqj6bnki84kfjs7dypyvmq0sm400dxkj")))

(define-public crate-wild-doc-script-python-0.11.1 (c (n "wild-doc-script-python") (v "0.11.1") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.18") (d #t) (k 0)))) (h "1sgrh5c2iiv7rr9wwmc170w3iy7w6fj60npl7ndx48dcmgmcj1y9")))

(define-public crate-wild-doc-script-python-0.11.2 (c (n "wild-doc-script-python") (v "0.11.2") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.19") (d #t) (k 0)))) (h "1673bk2i8437kyyi39bv0ri9lr4lgcmpq8aykm20mjnh86cqxp80")))

(define-public crate-wild-doc-script-python-0.12.0 (c (n "wild-doc-script-python") (v "0.12.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.20") (d #t) (k 0)))) (h "0h3srxq3j516gi3ma2m038l151cl90y9j4pxydx1marvcrlh8ynw")))

(define-public crate-wild-doc-script-python-0.13.0 (c (n "wild-doc-script-python") (v "0.13.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.21") (d #t) (k 0)))) (h "098y8xgylngzl9d8sn989d3gxil2xy7xaq66gca8m8zkpy78h1l9")))

(define-public crate-wild-doc-script-python-0.14.0 (c (n "wild-doc-script-python") (v "0.14.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.22") (d #t) (k 0)))) (h "049wz33xx5qyc3hidzhxfw0iqkb8ryg9akrs7g25vwiqdr1vk2k7")))

(define-public crate-wild-doc-script-python-0.15.0 (c (n "wild-doc-script-python") (v "0.15.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.23") (d #t) (k 0)))) (h "02g6y2k382qxz1ha2ixgpw1mlvbm43j2g3hqr8kypa556b3jrar8")))

(define-public crate-wild-doc-script-python-0.16.0 (c (n "wild-doc-script-python") (v "0.16.0") (d (list (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.24") (d #t) (k 0)))) (h "03sa4239dcjj0k3aikg13hh3qcb6q3qrviqpipqvfpr7xrwv4023")))

(define-public crate-wild-doc-script-python-0.17.0 (c (n "wild-doc-script-python") (v "0.17.0") (d (list (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.25") (d #t) (k 0)))) (h "0zmhydy8vl6acybicjds9fkva8cmi16zdxsa1fmbkwkgs6n03c3y")))

(define-public crate-wild-doc-script-python-0.18.0 (c (n "wild-doc-script-python") (v "0.18.0") (d (list (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.26") (d #t) (k 0)))) (h "0b164rjbpalk8q9bbfc5i0sqb6hgg495wxypdjq3s9zd7f54vn31")))

(define-public crate-wild-doc-script-python-0.18.1 (c (n "wild-doc-script-python") (v "0.18.1") (d (list (d (n "indexmap") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.26") (d #t) (k 0)))) (h "1a8qf0frykykrl7gm3h2b5ymq9k8zq71m7splmdx4cgfbbsj9n1b")))

(define-public crate-wild-doc-script-python-0.18.2 (c (n "wild-doc-script-python") (v "0.18.2") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.28") (d #t) (k 0)))) (h "1v0f7n96vxij6fw0yhbbn87vnfg0siixafd7cqavfabx4n77h9iy")))

(define-public crate-wild-doc-script-python-0.18.3 (c (n "wild-doc-script-python") (v "0.18.3") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.0") (d #t) (k 0)))) (h "0zrx3hgbkgfhg37piayzy10zln6p46b90j7hz4jwbr0hmh9dp7s9")))

(define-public crate-wild-doc-script-python-0.18.4 (c (n "wild-doc-script-python") (v "0.18.4") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.0") (d #t) (k 0)))) (h "15950gzbas583nw4k9fl6206ppz8r1wjg59yngzmp8aw9aprh9mi")))

(define-public crate-wild-doc-script-python-0.18.5 (c (n "wild-doc-script-python") (v "0.18.5") (d (list (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.29.2") (d #t) (k 0)))) (h "0vg9klp2j44hnm34zlb7x24zdjc1n71x44149qp3imqxaks28kf1")))

(define-public crate-wild-doc-script-python-0.18.6 (c (n "wild-doc-script-python") (v "0.18.6") (d (list (d (n "indexmap") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.30.0") (d #t) (k 0)))) (h "0mx9b77zq250zx6iakkf4i60ybgg28dfl1yzfxk8sqy52ms8ab20")))

(define-public crate-wild-doc-script-python-0.18.7 (c (n "wild-doc-script-python") (v "0.18.7") (d (list (d (n "indexmap") (r "^2.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "wild-doc-script") (r "^0.31.0") (d #t) (k 0)))) (h "07350vw066dzljgzfdv9lf63lchmz8jh29sg158p0szasqizyk74")))

