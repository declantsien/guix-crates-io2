(define-module (crates-io wi tt witty) #:use-module (crates-io))

(define-public crate-witty-0.1.0 (c (n "witty") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.6") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.4.2") (d #t) (k 0)))) (h "0nm5l1lssshyk33wy5ps0xmlx2dm7zldz8535lzhvh5645xyx7k3")))

(define-public crate-witty-1.0.0 (c (n "witty") (v "1.0.0") (d (list (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.10") (d #t) (k 0)))) (h "0dhdm18dz3si2wq7agdrfmsrda87sczfnpg03dp2y801n5vxvcjz")))

