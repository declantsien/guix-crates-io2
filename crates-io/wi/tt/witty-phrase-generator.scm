(define-module (crates-io wi tt witty-phrase-generator) #:use-module (crates-io))

(define-public crate-witty-phrase-generator-0.0.1 (c (n "witty-phrase-generator") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0gki44iyx63av99g6sx6idmmymwv324gin27kx25xg95m396hg9r")))

(define-public crate-witty-phrase-generator-0.1.0 (c (n "witty-phrase-generator") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1arqahi5zimqrxg83hg5s12i6f0zbxplby3ica8v6jh19kfnh5p2")))

(define-public crate-witty-phrase-generator-0.2.0 (c (n "witty-phrase-generator") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "0ssb0r5sqwvgsmxc1g28sr272b83gflzsksk3kgfdjm4zw5wyp3p")))

