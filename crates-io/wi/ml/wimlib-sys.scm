(define-module (crates-io wi ml wimlib-sys) #:use-module (crates-io))

(define-public crate-wimlib-sys-0.1.13 (c (n "wimlib-sys") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "1avarb4zyk2g3hwqcmjykbn5f9icksdv5pvx7zqmxb378rlma2rj") (l "wim")))

(define-public crate-wimlib-sys-0.1.13-1 (c (n "wimlib-sys") (v "0.1.13-1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "08q68gcp3dsxxgf1m065hhlicldq5nj7g3945rps15xlw6mx5sbh") (y #t) (l "wim")))

(define-public crate-wimlib-sys-1.0.0 (c (n "wimlib-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "09f11l6q2vzfnbnqfmcl9gid5kasayqqxmz2psmxhhcrkbh3hicz") (l "wim")))

