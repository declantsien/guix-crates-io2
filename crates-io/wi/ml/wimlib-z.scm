(define-module (crates-io wi ml wimlib-z) #:use-module (crates-io))

(define-public crate-wimlib-z-1.0.0 (c (n "wimlib-z") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0ggn60acgi6yv07rjnvxw2pknr6x34nhwdjd70pmp2makm9a1sbj") (y #t) (l "wim")))

