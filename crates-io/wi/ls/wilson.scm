(define-module (crates-io wi ls wilson) #:use-module (crates-io))

(define-public crate-wilson-0.1.0 (c (n "wilson") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)))) (h "1naq4waz60x2yww50l90a1ihlfcrjhcfxl4wlyhq8l8n8v5g8azk") (f (quote (("f64") ("f32") ("default" "f64"))))))

(define-public crate-wilson-0.1.1 (c (n "wilson") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)))) (h "1kzyalm5c0fag6k8rsqpwa7al1d629yjv6g9kgpih8rzp7kmgd9h") (f (quote (("f64") ("f32") ("default" "f64"))))))

