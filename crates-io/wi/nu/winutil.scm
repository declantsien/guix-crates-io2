(define-module (crates-io wi nu winutil) #:use-module (crates-io))

(define-public crate-winutil-0.1.0 (c (n "winutil") (v "0.1.0") (d (list (d (n "advapi32-sys") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1igy4s5ahwywzm9nvm21wgxc1wh5cm3nyznkgvwaypwf0lfzi1ll") (y #t)))

(define-public crate-winutil-0.1.1 (c (n "winutil") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("wow64apiset" "processthreadsapi" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vkyl3fbbf05n5ph5yz8sfaccrk9x3qsr25560w6w68ldf5i7bvx")))

