(define-module (crates-io wi nu winutils-rs) #:use-module (crates-io))

(define-public crate-winutils-rs-0.1.0 (c (n "winutils-rs") (v "0.1.0") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1w9g7jx5604l1vq812k3f7yzfvgc53r7g363b7p9xwmlaqpvk3w3") (y #t)))

(define-public crate-winutils-rs-0.1.1 (c (n "winutils-rs") (v "0.1.1") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "179i75fx06h3xnp264r9kh31a55c5d01cs0dnmfbh27x30d26kp7") (y #t)))

(define-public crate-winutils-rs-0.1.2 (c (n "winutils-rs") (v "0.1.2") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1vf1mp4aryk8sl8hqm0x1srph39k5nz6a9hjwx8qq8p2gj61fjy1") (y #t)))

(define-public crate-winutils-rs-0.1.3 (c (n "winutils-rs") (v "0.1.3") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1whncm3li5p9dcbz03vvxsx3x500qhjx8az8sw73f48aqykc0mfz") (y #t)))

(define-public crate-winutils-rs-0.1.4 (c (n "winutils-rs") (v "0.1.4") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0b9pq13zkm330lhdnmn624b7kmivdccvysi3zxklnpkc02b64wwk") (y #t)))

(define-public crate-winutils-rs-0.1.5 (c (n "winutils-rs") (v "0.1.5") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1q00cqybxq319vw6mr6y4jhdysg3dyj28q9h3nqmw6jgzkicycai") (y #t)))

(define-public crate-winutils-rs-0.1.6 (c (n "winutils-rs") (v "0.1.6") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "14l6hrn36a1mrw25dwsqjkh802ws7ihxlln6kw9jzkb7dz5pvv96")))

(define-public crate-winutils-rs-0.2.0 (c (n "winutils-rs") (v "0.2.0") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "1iyz5lk3qs4whiihv6hv30vwyn05ij0s2rz8xhgs90mii23nczsa")))

(define-public crate-winutils-rs-0.2.1 (c (n "winutils-rs") (v "0.2.1") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "15nbywir2w54a5aw0d39cg86q781mp7nxmi4c1dc30a1ggcf6a9d")))

(define-public crate-winutils-rs-0.2.2 (c (n "winutils-rs") (v "0.2.2") (d (list (d (n "widestring") (r "^0.4.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("accctrl" "aclapi" "combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "sddl" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (d #t) (k 0)))) (h "0v597lr94sdbp0l2gqn2ch93l7wrqcaans12dzg33qzs0drg0arl")))

