(define-module (crates-io wi mr wimrend) #:use-module (crates-io))

(define-public crate-wimrend-0.1.0 (c (n "wimrend") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.13") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "1i87qz5099paxyyz2djh24bg6z0hpp7x9jr9d79a84xpkqw2r8h3") (y #t)))

(define-public crate-wimrend-0.1.1 (c (n "wimrend") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "wgpu") (r "^0.13") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "11959qv6hydzpfzm7x05rrd6vi1fqfnwy1f3cv3qk7pn8ipkb6bf")))

