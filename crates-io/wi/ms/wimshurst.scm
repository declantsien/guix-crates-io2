(define-module (crates-io wi ms wimshurst) #:use-module (crates-io))

(define-public crate-wimshurst-0.1.0 (c (n "wimshurst") (v "0.1.0") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)))) (h "0m5j3a2qfhpnd3rsmr9gg6ran5ncbr2g0qkd9hhfrdkcbqa1xdqv")))

(define-public crate-wimshurst-1.0.0 (c (n "wimshurst") (v "1.0.0") (d (list (d (n "topologic") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 2)))) (h "0yrpnr7a11vilbggddmdqw82lazsr6zmy2kmza2i466n27l5srnd")))

