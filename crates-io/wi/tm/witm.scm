(define-module (crates-io wi tm witm) #:use-module (crates-io))

(define-public crate-witm-0.1.0 (c (n "witm") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m7xb90hddqi95if0w3ric4mhwpqyn3cwa8sc1fwa3rj7dmjcbwk") (y #t)))

(define-public crate-witm-0.0.1 (c (n "witm") (v "0.0.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pdm769q8dpxmgm2x0bcr16vlva5z04pp03y2z2fx399jn7p62pg")))

