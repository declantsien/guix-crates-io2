(define-module (crates-io wi kt wiktionary-part-of-speech-extract) #:use-module (crates-io))

(define-public crate-wiktionary-part-of-speech-extract-0.1.0 (c (n "wiktionary-part-of-speech-extract") (v "0.1.0") (d (list (d (n "fst") (r "^0.4.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)) (d (n "ustr") (r "^0.7.0") (d #t) (k 0)))) (h "0ppipcmbjl4cyga2wyadrzkgc9bdiz355inf7h54w0fm75lm3nka") (f (quote (("raw-masking"))))))

(define-public crate-wiktionary-part-of-speech-extract-0.1.1 (c (n "wiktionary-part-of-speech-extract") (v "0.1.1") (d (list (d (n "fst") (r "^0.4.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)) (d (n "ustr") (r "^0.8.0") (d #t) (k 0)))) (h "1v5admsrl4ymg5m6h6r7x9hl3jm0nid9lym1x1j96xzzx279j30a") (f (quote (("raw-masking"))))))

(define-public crate-wiktionary-part-of-speech-extract-0.1.2 (c (n "wiktionary-part-of-speech-extract") (v "0.1.2") (d (list (d (n "fst") (r "^0.4.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (o #t) (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "ustr") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0idwk6l20vq7xzn40c163snc5rcqqblss1dl54mriff10r7fjnby") (f (quote (("raw-masking") ("bin-regenerate" "regex" "ustr" "unidecode"))))))

