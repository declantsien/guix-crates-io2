(define-module (crates-io wi kt wiktionary-entries) #:use-module (crates-io))

(define-public crate-wiktionary-entries-20221101.0.1 (c (n "wiktionary-entries") (v "20221101.0.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)))) (h "10ms2k22m5fmndprr48q0k3kk65csmy77xs0mp48ig5chp2cm093")))

(define-public crate-wiktionary-entries-2022110101.0.1 (c (n "wiktionary-entries") (v "2022110101.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrie") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "13ymgbva4azq294f0dmzrf2zy1iknaxdzjgpdsgphhi8qb1n7cql")))

(define-public crate-wiktionary-entries-2022110102.0.0 (c (n "wiktionary-entries") (v "2022110102.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrie") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "06swx9k6njb2wvqdmawkfd6j3781f89bigjnxi4yvzys194vxryn")))

(define-public crate-wiktionary-entries-2022110102.0.1 (c (n "wiktionary-entries") (v "2022110102.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrie") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1v90psqych5bhc7r1nlw2sbslzr8i63liy3nmwzakcfifw9mlrn0")))

