(define-module (crates-io wi ng wing-sqlparser) #:use-module (crates-io))

(define-public crate-wing-sqlparser-0.13.1-alpha.0 (c (n "wing-sqlparser") (v "0.13.1-alpha.0") (d (list (d (n "bigdecimal") (r "^0.3") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.9") (d #t) (k 2)))) (h "1vp01c0jqqq3l75fi91nn960pzdsks0y8m98vxi8zv7dajj9rm2k") (f (quote (("std") ("json_example" "serde_json" "serde") ("default" "std"))))))

