(define-module (crates-io wi ng wings) #:use-module (crates-io))

(define-public crate-wings-0.0.0 (c (n "wings") (v "0.0.0") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "serde") (r "^1.0.199") (k 0)) (d (n "wings_macro") (r "^0.0.0") (d #t) (k 0)))) (h "196vsb1h163052bgvdd9kl7yrxs4jndfvbdj9gwhp64ilnc8241f")))

(define-public crate-wings-0.1.0 (c (n "wings") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "wings_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "wings_marshal") (r "^0.1.0") (d #t) (k 0)))) (h "10jhj7g06038yg0v1ywvhz5zs9151qcjjmn1vyw5nc5j04dprxps")))

(define-public crate-wings-0.1.1 (c (n "wings") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "wings_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "wings_marshal") (r "^0.1.1") (d #t) (k 0)))) (h "0r6mjyl384zmk6x8vmpji4i0cpwjgrgd1rh24hp64w91hfbfrhna")))

(define-public crate-wings-0.1.2 (c (n "wings") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "const_list") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "wings_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "wings_marshal") (r "^0.1.2") (d #t) (k 0)))) (h "179r3zg7pr5paqx7f1prrc5z92mf2faj3yw4rplgx1z5zm3879n0")))

