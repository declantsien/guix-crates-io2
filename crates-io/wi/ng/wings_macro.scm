(define-module (crates-io wi ng wings_macro) #:use-module (crates-io))

(define-public crate-wings_macro-0.0.0 (c (n "wings_macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0zrsqbgwn2ji63dja8z5nrsasw3dvpf8bh50l7hz7bv84wk8kqqx")))

(define-public crate-wings_macro-0.1.0 (c (n "wings_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("derive" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ddj41pwsmrq293c1lvvgnkji4wqqp7g6iv8im61mb4xnhwazdhx")))

(define-public crate-wings_macro-0.1.1 (c (n "wings_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("clone-impls" "derive" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "01xsnv3fdi1lcrl2axpms7gglrv0psbx64s7bbgzshdkp4hjrssh")))

(define-public crate-wings_macro-0.1.2 (c (n "wings_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (k 0)) (d (n "quote") (r "^1.0.36") (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("clone-impls" "derive" "full" "parsing" "printing" "proc-macro"))) (k 0)))) (h "03sckcrr5vcy5dhncb59iq31ny4w9a0vcpm47iqrjavc321ncfg8")))

