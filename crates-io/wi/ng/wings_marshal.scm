(define-module (crates-io wi ng wings_marshal) #:use-module (crates-io))

(define-public crate-wings_marshal-0.1.0 (c (n "wings_marshal") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.59") (k 0)))) (h "1wsj41l7i48gndxj6hldncsyik2g9nxyw9f4jlldxpfyxjkmxspm")))

(define-public crate-wings_marshal-0.1.1 (c (n "wings_marshal") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.59") (k 0)))) (h "1q5m5pmvdk2qqk9s4g63grlncrymh0k76s00qcb24phggsqiqszj")))

(define-public crate-wings_marshal-0.1.2 (c (n "wings_marshal") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.59") (k 0)))) (h "1cfdm4pnzj2idqkvc93fs7wr96k544cpjlxzy2hxsfm7gapm4rwl")))

