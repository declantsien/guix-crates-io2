(define-module (crates-io wi ng wingman) #:use-module (crates-io))

(define-public crate-wingman-0.0.1 (c (n "wingman") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1d0pmr9svr0gdnkzl52lfjqkdxwzc63fq5sd727a32dvmq9iqnjf")))

(define-public crate-wingman-0.0.2 (c (n "wingman") (v "0.0.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0db0zdpnjx6jqs1w0nr1ci2c2lrx91wd1iidxrihqvhpm2fwx00i")))

(define-public crate-wingman-0.0.3 (c (n "wingman") (v "0.0.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0mwq22d7i8qk9gyqqk77wpgmqrchd0ykp9mmz20xhhics80hhp2v")))

(define-public crate-wingman-0.0.4 (c (n "wingman") (v "0.0.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0m1zcjcyh0bf2p5972520751rpvygb61nbkrf8633d05bccmxmih")))

(define-public crate-wingman-0.0.5 (c (n "wingman") (v "0.0.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "13ybc4biq23pc4is502aw169x9r2i55dqzipav06vpmajaxnd0fk")))

(define-public crate-wingman-0.0.6 (c (n "wingman") (v "0.0.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "01klmmq22jagkn5f9yifcsplb2jm24r3578iixmxgzmjlnc2943z")))

(define-public crate-wingman-0.0.7 (c (n "wingman") (v "0.0.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0m2mg58zbsjsayf5hnhkfbr4slxrxjc7092ivwavngdhl17vpqz9")))

(define-public crate-wingman-0.0.8 (c (n "wingman") (v "0.0.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0x7ckvvrmh15zgxnl48gvwcz47a3ppnb60xfwhg11frip7r738qq")))

(define-public crate-wingman-0.0.9 (c (n "wingman") (v "0.0.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "06ps167rqf6sr6hz82dvw47fi6h7hh480ygab1z2ga14zd68dz7j")))

(define-public crate-wingman-0.0.10 (c (n "wingman") (v "0.0.10") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "1rp75h14zg3lwskf2mdfhz465ns7rq8y374ifzx8qpq5cwkllsmb")))

(define-public crate-wingman-0.0.11 (c (n "wingman") (v "0.0.11") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "10xhy2siczdicgx1l4p07khvmyjv1mp1widy1h0nrw4h6w0wnb0g")))

(define-public crate-wingman-0.1.0 (c (n "wingman") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "axum") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("unicode" "derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.21.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "gray_matter") (r "^0.2.6") (d #t) (k 0)) (d (n "handlebars") (r "^5.1.0") (d #t) (k 0)) (d (n "lightningcss") (r "^1.0.0-alpha.55") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (f (quote ("fs" "trace"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1p0vwmfbbvm59zpybd18iv3zs0g6dh6rqywnnssky5zxg6py0c51")))

