(define-module (crates-io wi p- wip-s32k144) #:use-module (crates-io))

(define-public crate-wip-s32k144-0.11.0 (c (n "wip-s32k144") (v "0.11.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "02pcgsnsjy7mfn7vyw5jq4r2q44mjcz78j6dq6f846rwx4l1xakp") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-wip-s32k144-0.11.1 (c (n "wip-s32k144") (v "0.11.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0j3kh2q1rkl4n6m4bcc1360675bzyi1cdgviqvfznqb9nkmr9p3v") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-wip-s32k144-0.11.2 (c (n "wip-s32k144") (v "0.11.2") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1s2x32mfqz3fr2rpwcwigyhnh9vnhk0ng1b7w9zgq4a1algkax6r") (f (quote (("rt" "cortex-m-rt/device"))))))

