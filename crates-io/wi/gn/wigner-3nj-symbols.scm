(define-module (crates-io wi gn wigner-3nj-symbols) #:use-module (crates-io))

(define-public crate-wigner-3nj-symbols-0.1.0 (c (n "wigner-3nj-symbols") (v "0.1.0") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)))) (h "0a9d81hdh1zcr5qyzfxmmqxrqnlf92ln0vl07hv3y3cwnvx2d89p")))

(define-public crate-wigner-3nj-symbols-0.2.0 (c (n "wigner-3nj-symbols") (v "0.2.0") (d (list (d (n "factorial") (r "^0.2.1") (d #t) (k 0)) (d (n "rstest") (r "^0.10.0") (d #t) (k 2)))) (h "1bzkcxi7wac63ipsmnfsavdv6rln5dna2dp0rhh522qg08l9fkkq")))

