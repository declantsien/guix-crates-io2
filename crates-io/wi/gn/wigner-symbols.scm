(define-module (crates-io wi gn wigner-symbols) #:use-module (crates-io))

(define-public crate-wigner-symbols-0.1.0 (c (n "wigner-symbols") (v "0.1.0") (d (list (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "rug") (r "^0.7.0") (f (quote ("integer" "rational"))) (k 0)))) (h "0k4bhz2vn77gsc6gg6n8x4r5lj9s0s6j95m9k8h9kxg6vhq52x42")))

(define-public crate-wigner-symbols-0.1.1 (c (n "wigner-symbols") (v "0.1.1") (d (list (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "rug") (r "^0.7.0") (f (quote ("integer" "rational"))) (k 0)))) (h "10pgy9zhp7g4r2zqc8xkq5gwim90b4h73x167dna2426mp82bn4z")))

(define-public crate-wigner-symbols-0.2.0 (c (n "wigner-symbols") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "rug") (r "^0.7.0") (f (quote ("integer" "rational"))) (k 0)))) (h "02wrrxzhlmrfyicv0yaymrhiyz8yr87qnjvgz32jk2fyg06cky3m")))

(define-public crate-wigner-symbols-0.2.1 (c (n "wigner-symbols") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.18") (d #t) (k 2)) (d (n "rug") (r "^0.7.0") (f (quote ("integer" "rational"))) (k 0)))) (h "0a3qfq3rc02jp7gszp6d6rryd7wmfkldac0m7cnzrmdd1k4klskq")))

(define-public crate-wigner-symbols-0.3.0 (c (n "wigner-symbols") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "rug") (r "^0.9.2") (f (quote ("integer" "rational"))) (k 0)))) (h "16sc3yxwkxbd7k2y1bkaqw65d51a6fl1w6zzzvv9l0jbfw7jw0s3")))

(define-public crate-wigner-symbols-0.4.0 (c (n "wigner-symbols") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "md5") (r "^0.3.6") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.4.2") (d #t) (k 2)) (d (n "rug") (r "^1.11.0") (f (quote ("integer" "rational"))) (k 0)))) (h "1cwcy3gq8c0vszdjd518vgzswd2k23bycmdd8vksnkp90dp63lli")))

(define-public crate-wigner-symbols-0.5.0 (c (n "wigner-symbols") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 2)) (d (n "md5") (r "^0.7.0") (d #t) (k 2)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rug") (r "^1.12.0") (f (quote ("integer" "rational"))) (k 0)))) (h "0wd2bjny3chlx8jlg4fmhxrfzmslvhcf92w9qf1ycjhx8578736r")))

