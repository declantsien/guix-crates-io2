(define-module (crates-io wi gn wigners) #:use-module (crates-io))

(define-public crate-wigners-0.1.0 (c (n "wigners") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1zb47mxvhil8y3d5fcbnnb457gdl5ngsiax6v5jlmqlnilavaa07")))

(define-public crate-wigners-0.1.1 (c (n "wigners") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0p25pjhiwg8lrcfppf4gwf4ac65qglgwrprdb3xvm3cibixmfb0m")))

(define-public crate-wigners-0.2.0 (c (n "wigners") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "1rdq76kkjwrxffh9n0swfsbwyar8s488fys9xhd4ajfmc89hia9j")))

(define-public crate-wigners-0.2.1 (c (n "wigners") (v "0.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)))) (h "0cilgizpsh1sd5mm7d67fbp4frzj4h7gdhjllbgrhbk0frqivb97")))

(define-public crate-wigners-0.3.0 (c (n "wigners") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.10") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "0yqh9msnid4yk8n0wzbcyb1l6ixvkwh3f8k93h3dr6igj20y182w")))

