(define-module (crates-io wi no winos) #:use-module (crates-io))

(define-public crate-winos-0.1.0 (c (n "winos") (v "0.1.0") (h "1l66ja2jgaf0kdv71ip70bq50phsmrp7f9yr60a9wppjvk7kwgqb")))

(define-public crate-winos-0.1.1 (c (n "winos") (v "0.1.1") (h "0rvc28xbsvhbsipmzv4qxggrhlh5pkcniks7h0knwh9k6hfgdng5")))

(define-public crate-winos-0.1.2 (c (n "winos") (v "0.1.2") (h "031i16k1j6cvipwjd69ssm4sq6319mjj884a785sv4a1m2gq0pl1")))

