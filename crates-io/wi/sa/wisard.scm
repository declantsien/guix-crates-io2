(define-module (crates-io wi sa wisard) #:use-module (crates-io))

(define-public crate-wisard-0.0.1 (c (n "wisard") (v "0.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bloom") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "0akrrc9nycbbflyp6y02cn7f7q3149x84dc6nphmc7hy5yb0v32d")))

(define-public crate-wisard-0.0.2 (c (n "wisard") (v "0.0.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bloom") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yxlsy3hvvyympjj5jmg6b82rndk841cjgnrvkfaxgw2gzhy7kdp")))

(define-public crate-wisard-0.0.3 (c (n "wisard") (v "0.0.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bloom") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qpbjh98paqn4dgi9vhjq9hz8wnhhna46yql2m82aadhx59zlw6j")))

