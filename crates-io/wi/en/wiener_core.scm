(define-module (crates-io wi en wiener_core) #:use-module (crates-io))

(define-public crate-wiener_core-0.1.0 (c (n "wiener_core") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.46.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "156f3hpi61h1lkvwmmk5rhj46zn8cn1xsjyslc3h80jrja0yfmhr")))

(define-public crate-wiener_core-0.1.1 (c (n "wiener_core") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.46.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "17nkm9wvnzdha5pb5z58dcpqflha33rbgn8bqfwmpvv0piz9absy")))

