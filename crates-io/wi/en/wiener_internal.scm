(define-module (crates-io wi en wiener_internal) #:use-module (crates-io))

(define-public crate-wiener_internal-0.1.0 (c (n "wiener_internal") (v "0.1.0") (d (list (d (n "wiener_core") (r "^0.1.0") (d #t) (k 0)) (d (n "wiener_gl") (r "^0.1.0") (d #t) (k 0)) (d (n "wiener_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "wiener_vk") (r "^0.1.0") (d #t) (k 0)))) (h "1lxmi2003d4r39249g5sab3ra0rfkb1qanhhry4zc5pb4c9p9lp1")))

(define-public crate-wiener_internal-0.1.1 (c (n "wiener_internal") (v "0.1.1") (d (list (d (n "wiener_core") (r "^0.1.1") (d #t) (k 0)) (d (n "wiener_gl") (r "^0.1.1") (d #t) (k 0)) (d (n "wiener_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "wiener_vk") (r "^0.1.0") (d #t) (k 0)))) (h "0m8qrb4v789y11a206avr7ji4h78jz1akw9m7399i9k2j3yliy49")))

