(define-module (crates-io wi en wiener_gl) #:use-module (crates-io))

(define-public crate-wiener_gl-0.1.0 (c (n "wiener_gl") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.46.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "wiener_core") (r "^0.1.0") (d #t) (k 0)) (d (n "wiener_utils") (r "^0.1.0") (d #t) (k 0)))) (h "0y67k15h6s3scrf22xmkn39zgfidz3qh5iqbrgnrncdj6685ff6g")))

(define-public crate-wiener_gl-0.1.1 (c (n "wiener_gl") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.46.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "obj-rs") (r "^0.7.0") (d #t) (k 0)) (d (n "wiener_core") (r "^0.1.1") (d #t) (k 0)) (d (n "wiener_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1hhz4lkqw2x3xqi8ncbphipfsq6hpa7a5ag2vjfi1lm6kcaf4lqa")))

