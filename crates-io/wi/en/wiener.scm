(define-module (crates-io wi en wiener) #:use-module (crates-io))

(define-public crate-wiener-0.1.0 (c (n "wiener") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "wiener_internal") (r "^0.1.0") (d #t) (k 0)))) (h "133mb64rxpn4v99p39zinid4gz3q4an3spcnny1wvgr1ajr5dp1d")))

(define-public crate-wiener-0.1.1 (c (n "wiener") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "gl") (r "^0.14.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "wiener_internal") (r "^0.1.1") (d #t) (k 0)))) (h "1hsbq46gp4z98f5p8nhwaj5ylp4qb5ab168yc9bjsnfk96y0hb5g")))

