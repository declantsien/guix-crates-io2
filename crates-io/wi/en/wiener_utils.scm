(define-module (crates-io wi en wiener_utils) #:use-module (crates-io))

(define-public crate-wiener_utils-0.1.0 (c (n "wiener_utils") (v "0.1.0") (d (list (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1idydnikclz6g5q1984p9x19i9m5b1air7mlzxb6z22nq259wizf")))

(define-public crate-wiener_utils-0.1.1 (c (n "wiener_utils") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "160qr1gzy2x76accnw2gimgs2wbsyx5l2bk64956k5bpf5av9fwr")))

