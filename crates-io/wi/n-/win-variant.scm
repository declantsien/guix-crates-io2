(define-module (crates-io wi n- win-variant) #:use-module (crates-io))

(define-public crate-win-variant-0.1.0 (c (n "win-variant") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "15glvagxpxb1fdflr3xj0dk4vwy1r0azm5v0iw92fm5asy6vwbh8")))

(define-public crate-win-variant-0.1.1 (c (n "win-variant") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "058vgvmyrl69v3gp69nxw4435wk1k65mvg92rq85qck6fjn8xb4y")))

(define-public crate-win-variant-0.1.2 (c (n "win-variant") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "0xlmzvnagmqs2msk0bnh9gc8ml8km4facsqxj0dfl4nwp91pvs74")))

(define-public crate-win-variant-0.1.3 (c (n "win-variant") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "12q0sygq1hygd329kxjk3r8yiqi4d1179m4dzihkw0yary9ilpnj")))

(define-public crate-win-variant-0.2.0 (c (n "win-variant") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "0vwir2s3cq8sgvbqg9az4phij4jy5iynhmc6mzrvmp0vg3l4y60f")))

(define-public crate-win-variant-0.3.0 (c (n "win-variant") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "oaidl" "oleauto" "minwindef" "wtypes"))) (d #t) (k 0)))) (h "1qv8f6qgv9pwvm08lvqrq8mxirmyg692vx5z3zh0ig2knyhkis3k")))

