(define-module (crates-io wi n- win-memory-rs) #:use-module (crates-io))

(define-public crate-win-memory-rs-1.0.0 (c (n "win-memory-rs") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("tlhelp32" "handleapi" "minwindef" "memoryapi" "processthreadsapi" "libloaderapi" "errhandlingapi"))) (d #t) (k 0)))) (h "0miq5lvssa7j18672px8bvdnk44ms12rm7n5f1zfiqy55yqgpnhx")))

