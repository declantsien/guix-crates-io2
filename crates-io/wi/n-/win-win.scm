(define-module (crates-io wi n- win-win) #:use-module (crates-io))

(define-public crate-win-win-0.1.0 (c (n "win-win") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "0646y8acrfzyg46va492qrb384x30b1hdk7kqcwhmr6bkavglbdq")))

(define-public crate-win-win-0.1.1 (c (n "win-win") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2.2") (d #t) (k 0)))) (h "1dyz1ixzwknmmf1w5rjp07zhz4b0nsi0fqxvnw721ir3w86vw2y3")))

