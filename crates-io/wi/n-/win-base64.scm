(define-module (crates-io wi n- win-base64) #:use-module (crates-io))

(define-public crate-win-base64-0.1.0 (c (n "win-base64") (v "0.1.0") (d (list (d (n "windows") (r "^0.30.0") (f (quote ("Security_Cryptography" "Storage_Streams" "Win32_Foundation"))) (d #t) (k 0)))) (h "1djhdgw1cfg2gdfrs38yr8rwcf9slc0zbnddn0nd0vgpnv2l2pz8")))

(define-public crate-win-base64-0.1.1 (c (n "win-base64") (v "0.1.1") (d (list (d (n "windows") (r "^0.30.0") (f (quote ("Security_Cryptography" "Storage_Streams" "Win32_Foundation"))) (d #t) (k 0)))) (h "1zn9qsi0bl32lv0s5j94xlhk5clb54kb37bl1sw0l7ivm0hh1iwi")))

(define-public crate-win-base64-0.1.2 (c (n "win-base64") (v "0.1.2") (d (list (d (n "windows") (r "^0.30.0") (f (quote ("Security_Cryptography" "Storage_Streams" "Win32_Foundation"))) (d #t) (k 0)))) (h "0p412sz7gkkf7waz13wj5379d3mjl0whvlw0yj2xrw8nkaz0cwqv")))

