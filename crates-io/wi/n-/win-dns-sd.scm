(define-module (crates-io wi n- win-dns-sd) #:use-module (crates-io))

(define-public crate-win-dns-sd-0.1.0 (c (n "win-dns-sd") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.10") (d #t) (k 0)) (d (n "windows") (r "^0.10") (d #t) (k 1)))) (h "1ccxsx7xq7a8wq6zr0ybrkpx0p7hsgwl6kfk7i25k1yd1103ilny")))

(define-public crate-win-dns-sd-0.1.1 (c (n "win-dns-sd") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.10") (d #t) (k 0)) (d (n "windows") (r "^0.10") (d #t) (k 1)))) (h "0njxzjq566jscw0k6hx01nh4nlw5l5pd7vhwljryj01f9vc1kpzw")))

