(define-module (crates-io wi n- win-screenshot) #:use-module (crates-io))

(define-public crate-win-screenshot-0.1.0 (c (n "win-screenshot") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "psapi" "errhandlingapi" "impl-default" "utilapiset" "winerror"))) (d #t) (k 0)))) (h "04skinyk8wgx7ykld19b3bvi66m2zwsxfbj9mvrrydhg6iyqcqby")))

(define-public crate-win-screenshot-0.1.1 (c (n "win-screenshot") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "psapi" "errhandlingapi" "impl-default" "utilapiset" "winerror"))) (d #t) (k 0)))) (h "18g0k43f43xnay69ws6b6z5yyagvlh1sr6xi168dr6jwr78ld0np")))

(define-public crate-win-screenshot-0.1.2 (c (n "win-screenshot") (v "0.1.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "psapi" "errhandlingapi" "impl-default" "utilapiset" "winerror"))) (d #t) (k 0)))) (h "0xx0w0440r158c5c8wjlab49f2qc5zk2bjsb7q4czz2l7j36vyf2")))

(define-public crate-win-screenshot-0.1.3 (c (n "win-screenshot") (v "0.1.3") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "psapi" "errhandlingapi" "impl-default" "utilapiset" "winerror"))) (d #t) (k 0)))) (h "1kq01cypassisbgjggw6bnalfrqpspw263cc0fd2ps29ziqwdx6h")))

(define-public crate-win-screenshot-0.1.4 (c (n "win-screenshot") (v "0.1.4") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "impl-default" "winerror"))) (d #t) (k 0)))) (h "1c5kzr8fy7sfqb0dy2b0rspl83b2f194xbmwz4i5mg2qfwax07mi")))

(define-public crate-win-screenshot-0.1.5 (c (n "win-screenshot") (v "0.1.5") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "impl-default" "winerror"))) (d #t) (k 0)))) (h "0j5xb9naywxkfrfalvdh0b2hbmwy6wgn1ih0pn2jdmaqqmv8av5f")))

(define-public crate-win-screenshot-0.1.6 (c (n "win-screenshot") (v "0.1.6") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "impl-default" "winerror"))) (d #t) (k 0)))) (h "196qhlpkmnmiq4aj1n6br7df8bz67ac3grxzh5y3lyqsdwryax02")))

(define-public crate-win-screenshot-1.0.0 (c (n "win-screenshot") (v "1.0.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "impl-default" "winerror"))) (d #t) (k 0)))) (h "1c4zwsk76rj8rk4bqjiwsb1qrsrf28sbxgz6miday546d75fb6zi")))

(define-public crate-win-screenshot-1.0.1 (c (n "win-screenshot") (v "1.0.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "impl-default" "winerror"))) (d #t) (k 0)))) (h "17imjmxz6h7rz4msk0l2rv7li8p3wi9bqm3ixxqfrj58aja9d8bq")))

(define-public crate-win-screenshot-1.0.2 (c (n "win-screenshot") (v "1.0.2") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.37") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "0rxzrli5m8708w5b0ky6jnddvq4g8gp5aarm9b975pbwxrabzpf3")))

(define-public crate-win-screenshot-1.0.3 (c (n "win-screenshot") (v "1.0.3") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.37") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "1mng4p0vjbysskpy9n16cdd4sfi7wdvijk75cw35kp15q4p701g0")))

(define-public crate-win-screenshot-2.0.0 (c (n "win-screenshot") (v "2.0.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.37") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "1ia6b0k3bkfj6fhd0cnzm9v9g8bvjp4plv30rgv6wm7c84sl1qda")))

(define-public crate-win-screenshot-3.0.0 (c (n "win-screenshot") (v "3.0.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.44") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "0mxf1xiaqs5lryc59dnddrgi9a8m3jl0qyqfyvlis9sach5pmarc")))

(define-public crate-win-screenshot-3.0.1 (c (n "win-screenshot") (v "3.0.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.44") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "1zpqp9gp97yfc09mdw0kzdzm18mzn6vh51yh8m030lxzbq6ilij8")))

(define-public crate-win-screenshot-4.0.0 (c (n "win-screenshot") (v "4.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.46") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "0jnpavc53mdhld3xxziyhfmxnpnghcngb49k9wwqlsx3dv1q6p9g")))

(define-public crate-win-screenshot-4.0.1 (c (n "win-screenshot") (v "4.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.46") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps"))) (d #t) (k 0)))) (h "1bgfzy28ccwm419xqjwda4qd85sh76xdpm4m9rdl5igynp4pykyz")))

(define-public crate-win-screenshot-4.0.2 (c (n "win-screenshot") (v "4.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.46") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "1gbwk754riyqhb0n7z1xk1iq6zjyg7wdvf1r85lmd1jc3n2hk6cm")))

(define-public crate-win-screenshot-4.0.3 (c (n "win-screenshot") (v "4.0.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.46") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "12k4nvvglpn8z330gkx4fjcrlm1si4gbi74w8hi83h9rf0wjh0my")))

(define-public crate-win-screenshot-4.0.4 (c (n "win-screenshot") (v "4.0.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.48") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "1vx78p42idav67a2ifnw1bizjx914wnwp8h8imr36lvllr5wy41k")))

(define-public crate-win-screenshot-4.0.5 (c (n "win-screenshot") (v "4.0.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.51") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "0fha1540zd688mdclwk064vbk54jap42jjv74z5i12422w2z819n")))

(define-public crate-win-screenshot-4.0.6 (c (n "win-screenshot") (v "4.0.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.52") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "0d45ipxy7c0a1462jq46gx5jfy6s38alryh29mi1v5dglmdab0d4")))

(define-public crate-win-screenshot-4.0.7 (c (n "win-screenshot") (v "4.0.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.53") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "0d9mq2m354pipflgbxrnd8lxfpacv3ylc3800dlm77ilj84gl83s")))

(define-public crate-win-screenshot-4.0.8 (c (n "win-screenshot") (v "4.0.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.54") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "0xrmgnas2cakhgkrsdxqvbbnhmwfb5m5z7p6qc4izy01nkz6h49v")))

(define-public crate-win-screenshot-4.0.9 (c (n "win-screenshot") (v "4.0.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.56") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "16avm8hvzincvck82asfwlvx0h9cccnyiihlsx5wckv7fxgzxwc3")))

(define-public crate-win-screenshot-4.0.10 (c (n "win-screenshot") (v "4.0.10") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.25") (d #t) (k 2)) (d (n "qshot") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "windows") (r "^0.56") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation" "Win32_Graphics_Gdi" "Win32_Storage_Xps" "Win32_UI_HiDpi"))) (d #t) (k 0)))) (h "1z5yx45220p2py4a766afr5zjcjjis264jwlwb1n8mrdj2v004qv")))

