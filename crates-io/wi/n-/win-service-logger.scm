(define-module (crates-io wi n- win-service-logger) #:use-module (crates-io))

(define-public crate-win-service-logger-0.1.0 (c (n "win-service-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "widestring") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt"))) (d #t) (k 0)))) (h "1dbi1dr1qvwd4dklnbway7v4k865hjzkh2fm9wi7a755adip5lr4")))

(define-public crate-win-service-logger-0.1.1 (c (n "win-service-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "widestring") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt"))) (d #t) (k 0)))) (h "1w49g03dg8s1vcrs64nn4fblz5n0hjl4sq56zndscxjmgvq5j9nf")))

