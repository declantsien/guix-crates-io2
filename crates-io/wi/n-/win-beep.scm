(define-module (crates-io wi n- win-beep) #:use-module (crates-io))

(define-public crate-win-beep-1.0.0 (c (n "win-beep") (v "1.0.0") (d (list (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 2)))) (h "1havp11ij0f4x57k9kz0rgjrk17fdlbnm1h91iv90qcymcgdl1mg")))

(define-public crate-win-beep-1.0.1 (c (n "win-beep") (v "1.0.1") (d (list (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 2)))) (h "1bwx23mw1csp8xy7c1cqs2lv02ac5i89w68pzglzjkk6ki0wly79")))

(define-public crate-win-beep-1.0.2 (c (n "win-beep") (v "1.0.2") (d (list (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 2)))) (h "01mfjg25qz8mmyf9fi6fwakcm2kq38izfs8px2nqfqhi054mi0f0")))

(define-public crate-win-beep-1.0.3 (c (n "win-beep") (v "1.0.3") (d (list (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 2)))) (h "0gx9m14s6502qp5vddkjh303h10dwjn8hb451xg92pjl41am5d6i")))

