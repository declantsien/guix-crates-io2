(define-module (crates-io wi n- win-toast-notify) #:use-module (crates-io))

(define-public crate-win-toast-notify-0.1.0 (c (n "win-toast-notify") (v "0.1.0") (h "1hsp0kcxvwz5299aqczzidwi3p4pwwsnwfcbfzkcdy7ixv31aaci") (y #t) (r "1.77.1")))

(define-public crate-win-toast-notify-0.1.1 (c (n "win-toast-notify") (v "0.1.1") (h "07f105mkcicxpsz59jxl4ikj4rlkncvv1293lzjksfysq4jad3r5") (r "1.77.1")))

(define-public crate-win-toast-notify-0.1.2 (c (n "win-toast-notify") (v "0.1.2") (d (list (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "0w43lc7sal89irv3q7mn3rrm6vd61k9sxnlrjbgkrlbq8gsg8a30") (y #t)))

(define-public crate-win-toast-notify-0.1.3 (c (n "win-toast-notify") (v "0.1.3") (d (list (d (n "xml") (r "^0.8.20") (d #t) (k 0)))) (h "1mjdkk4mbjywmh8di4rpg7n7ycxqhy8b6icv9gxia8k7lcfv9hqk")))

