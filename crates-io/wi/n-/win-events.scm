(define-module (crates-io wi n- win-events) #:use-module (crates-io))

(define-public crate-win-events-0.0.1 (c (n "win-events") (v "0.0.1") (h "0srl90p0pyx5lkf81hs5krfbjdfalls0g8iqgajgvzr2840yjhls")))

(define-public crate-win-events-0.0.2 (c (n "win-events") (v "0.0.2") (h "0amxvpg0yr909vfnk959k7pk9y548kc8yd7wz5pgjp3bvjqjyq93")))

