(define-module (crates-io wi n- win-opacity) #:use-module (crates-io))

(define-public crate-win-opacity-0.1.1 (c (n "win-opacity") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(window)") (k 0)))) (h "0jpzcb0jjbm8bnlkz61i5nz25i0y8x8l6lbgj3ad4flp9jh3npbw")))

(define-public crate-win-opacity-0.1.2 (c (n "win-opacity") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(window)") (k 0)))) (h "0i0fscnbmhwq0ibycgm3n04vc0dfmgmp17pvarif8nm439ki388v")))

(define-public crate-win-opacity-1.0.0 (c (n "win-opacity") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(window)") (k 0)))) (h "17ybh125z60bz9ldgv37xkjhnsfxgl2snvvm6cjbq9g0xg7v4lz8")))

