(define-module (crates-io wi n- win-tpm) #:use-module (crates-io))

(define-public crate-win-tpm-0.1.0 (c (n "win-tpm") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "ncrypt") (r "^0.2.0") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bbmsphd5kli89fd5d7dhi2kda2ijy645kdn4r5qjm18ld87lk4y")))

(define-public crate-win-tpm-0.2.0 (c (n "win-tpm") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "ncrypt") (r "^0.2.0") (d #t) (k 0)) (d (n "rsa") (r "^0.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qgpxsjx6l1297qcxlz3axmbfnzvvmg00i2bmv6mv9rgmla7kg5g")))

