(define-module (crates-io wi n- win-font-dir) #:use-module (crates-io))

(define-public crate-win-font-dir-0.1.0 (c (n "win-font-dir") (v "0.1.0") (h "01kg1fcs3wfsqb8mg96swl416mggs2z5qdx4faik6wpzaksj0znf")))

(define-public crate-win-font-dir-0.1.1 (c (n "win-font-dir") (v "0.1.1") (h "08b9ki79lzd321i7bznr5nyhiym48cfvivplnfx9d9a2rbx4ij4a")))

