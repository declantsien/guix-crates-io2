(define-module (crates-io wi n- win-overlay) #:use-module (crates-io))

(define-public crate-win-overlay-0.1.0 (c (n "win-overlay") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "dwmapi" "d3d9" "d3d9types" "memoryapi"))) (d #t) (k 0)))) (h "1h7dvkmkblmrr77rbpx5kpki8ildcnipc4vpkv9b13rv3qz13pwk")))

