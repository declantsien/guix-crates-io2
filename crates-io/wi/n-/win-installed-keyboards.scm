(define-module (crates-io wi n- win-installed-keyboards) #:use-module (crates-io))

(define-public crate-win-installed-keyboards-0.1.0 (c (n "win-installed-keyboards") (v "0.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0qdskggdzzgy2wxc11vw984phpg17497jb883kyqjpw76b00nrbg")))

(define-public crate-win-installed-keyboards-0.1.1 (c (n "win-installed-keyboards") (v "0.1.1") (d (list (d (n "enum-primitive-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "06z8yrc48gs2w5skifbgljjmylnm63g1pnlkj2ddg8p8d39fk1r0")))

