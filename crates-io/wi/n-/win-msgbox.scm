(define-module (crates-io wi n- win-msgbox) #:use-module (crates-io))

(define-public crate-win-msgbox-0.1.0 (c (n "win-msgbox") (v "0.1.0") (d (list (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (k 0)))) (h "0ff2bgv55b7qc5lmvvcx77l5137xsyf065yqif38lkyimhh55b84")))

(define-public crate-win-msgbox-0.1.1 (c (n "win-msgbox") (v "0.1.1") (d (list (d (n "windows-sys") (r "^0.42") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (k 0)))) (h "0sbxzs30gqb999baqnryrd228za8qhvcw7k976qxalqs7p7l1vgh")))

(define-public crate-win-msgbox-0.1.2 (c (n "win-msgbox") (v "0.1.2") (d (list (d (n "windows-sys") (r "^0.45") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (k 0)))) (h "0xpfh9s611myfr8wvamfjc3dbhp5a3lxfilnv4nhvnp2d96i2xna")))

(define-public crate-win-msgbox-0.1.3 (c (n "win-msgbox") (v "0.1.3") (d (list (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (k 0)))) (h "05gzjcg9lnkkcdc0xqkbjmy2qbsv51xqdby7hwjrn7yp15hp2zd2")))

