(define-module (crates-io wi n- win-stdio-handle-dbg) #:use-module (crates-io))

(define-public crate-win-stdio-handle-dbg-0.1.0 (c (n "win-stdio-handle-dbg") (v "0.1.0") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_Console" "Win32_Storage_FileSystem"))) (d #t) (k 0)))) (h "0y65p8rg9d6d9j90psn08lfv8pvhyl6p1yx49kf9y7cf46ikc9bw")))

