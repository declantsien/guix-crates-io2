(define-module (crates-io wi n- win-idispatch) #:use-module (crates-io))

(define-public crate-win-idispatch-0.1.0 (c (n "win-idispatch") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "win-variant") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "combaseapi" "unknwnbase" "winerror" "winnls" "wtypesbase"))) (d #t) (k 0)))) (h "08iwsr4qr2r831a5p6x4wzzrlmmqbns6lp4hp3l89sq75j0a8ix3")))

(define-public crate-win-idispatch-0.2.0 (c (n "win-idispatch") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "win-variant") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "combaseapi" "unknwnbase" "winerror" "winnls" "wtypesbase"))) (d #t) (k 0)))) (h "1dhvrlfwk859ykw2101y508zly2zqlx779kg27pkgr6mc42r4k9m")))

(define-public crate-win-idispatch-0.2.1 (c (n "win-idispatch") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "win-variant") (r "^0.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "combaseapi" "unknwnbase" "winerror" "winnls" "wtypesbase"))) (d #t) (k 0)))) (h "1pmqzg4lp6hfmipabrkvvq24j2mhc3x4p9cdqgqkws5z7n4hx1dl")))

(define-public crate-win-idispatch-0.3.0 (c (n "win-idispatch") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)) (d (n "win-variant") (r "^0.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "combaseapi" "unknwnbase" "winerror" "winnls" "wtypesbase"))) (d #t) (k 0)))) (h "07wgs0xv46fl8ygmk00w8z4bzmhqy4lk44xd7clm1k4zdj2hxjfg")))

