(define-module (crates-io wi n- win-gsmtc) #:use-module (crates-io))

(define-public crate-win-gsmtc-0.1.0 (c (n "win-gsmtc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tap") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "macros" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("sync" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)) (d (n "windows") (r "^0.48") (f (quote ("Media_Control" "Foundation" "Foundation_Collections" "Storage_Streams" "Win32_Foundation"))) (d #t) (k 0)))) (h "19d7nyiqdi0d42kqzhk5rcrijy459khzwy7xr5xa70b014k4q71d") (f (quote (("default"))))))

