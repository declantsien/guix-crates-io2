(define-module (crates-io wi ni winit-modular) #:use-module (crates-io))

(define-public crate-winit-modular-0.1.0 (c (n "winit-modular") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "0708lmxxw8rb6jm4ai70khfhrphi4g3izcqw6dyr6dlr6d2a5klz")))

(define-public crate-winit-modular-0.1.1 (c (n "winit-modular") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.8.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "1khyb4g9b1894kfsa3vl30dzldyrlzq7hkwc2ar9s150cd0vi62g")))

