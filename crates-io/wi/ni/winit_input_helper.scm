(define-module (crates-io wi ni winit_input_helper) #:use-module (crates-io))

(define-public crate-winit_input_helper-0.1.0 (c (n "winit_input_helper") (v "0.1.0") (d (list (d (n "winit") (r "^0.17") (d #t) (k 0)))) (h "1z1nyqnd6qgzsk6grk8fcqayzqfnxy5khvlfjiih5fsh5pqyrvdn")))

(define-public crate-winit_input_helper-0.1.1 (c (n "winit_input_helper") (v "0.1.1") (d (list (d (n "winit") (r "^0.17") (d #t) (k 0)))) (h "0ba760xx15zd6wbs2dh1qxmzy1bzraskah80qrir7mj5g6qmcyn4")))

(define-public crate-winit_input_helper-0.1.2 (c (n "winit_input_helper") (v "0.1.2") (d (list (d (n "winit") (r "^0.17") (d #t) (k 0)))) (h "0q0867fmxq64jjw0j56ryf99bg7bysrlg2vf2hrwf7zijsp23jfh")))

(define-public crate-winit_input_helper-0.2.0 (c (n "winit_input_helper") (v "0.2.0") (d (list (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "0k5c5bp8q46rsz001mpmn1czbc4km535zh5nwp1zm3jpn2md47aa")))

(define-public crate-winit_input_helper-0.2.1 (c (n "winit_input_helper") (v "0.2.1") (d (list (d (n "winit") (r "^0.18") (d #t) (k 0)))) (h "0rracnc11qz2h70z8hh2j718vc57f7rj6dh7q47615vksp4d950h")))

(define-public crate-winit_input_helper-0.3.0 (c (n "winit_input_helper") (v "0.3.0") (d (list (d (n "winit") (r "^0.19") (d #t) (k 0)))) (h "14hbn195smjqhgprs8lccil99dn8ww4zwm1bnxbq9353nr09zv6b")))

(define-public crate-winit_input_helper-0.4.0-alpha3 (c (n "winit_input_helper") (v "0.4.0-alpha3") (d (list (d (n "winit") (r "^0.20.0-alpha3") (d #t) (k 0)))) (h "0xff3h0qqsdq7l0f0aqb0h7prfriy2aamwl6h2wcasccqrpg5mkj")))

(define-public crate-winit_input_helper-0.4.0-alpha4 (c (n "winit_input_helper") (v "0.4.0-alpha4") (d (list (d (n "winit") (r "^0.20.0-alpha4") (d #t) (k 0)))) (h "17ciq3b52dqwxi5i6ln5pni1j77j87xrl9mjmq2acbs9cjn1kps8")))

(define-public crate-winit_input_helper-0.4.0-alpha6 (c (n "winit_input_helper") (v "0.4.0-alpha6") (d (list (d (n "winit") (r "^0.20.0-alpha6") (d #t) (k 0)))) (h "0dzhgfp7lha2knl4gq0v32yl8bckzkkrp7vx6phm9lvd0a1xbql7")))

(define-public crate-winit_input_helper-0.4.0 (c (n "winit_input_helper") (v "0.4.0") (d (list (d (n "winit") (r "^0.20.0") (d #t) (k 0)))) (h "1g0rzk9rjvvzd1pi1w30iwb0dnfh17zqxy26l9vg3ynnc94q5rkh")))

(define-public crate-winit_input_helper-0.5.0 (c (n "winit_input_helper") (v "0.5.0") (d (list (d (n "winit") (r "^0.21.0") (d #t) (k 0)))) (h "1aq70j74wzbhb0af36dn8fykwjdiyzf1vwi6ysb7mlbwfa7k3i43")))

(define-public crate-winit_input_helper-0.6.0 (c (n "winit_input_helper") (v "0.6.0") (d (list (d (n "winit") (r "^0.22.0") (d #t) (k 0)))) (h "1pv3qv0w1xymp8m4gnlkdh59mhz9xwm5km80r156a540bqcr3ywj")))

(define-public crate-winit_input_helper-0.7.0 (c (n "winit_input_helper") (v "0.7.0") (d (list (d (n "winit") (r "^0.22.0") (d #t) (k 0)))) (h "1p2yfzvynycpiw9i0m4k684lrgazz6n4lz5di1ng0a3v20l1rg3g")))

(define-public crate-winit_input_helper-0.8.0 (c (n "winit_input_helper") (v "0.8.0") (d (list (d (n "winit") (r "^0.23.0") (d #t) (k 0)))) (h "180rkmirifd2n0k7d62j4xszcj47hpnijwb57xlsc053nrwcpkkv")))

(define-public crate-winit_input_helper-0.9.0 (c (n "winit_input_helper") (v "0.9.0") (d (list (d (n "winit") (r "^0.24.0") (d #t) (k 0)))) (h "0462iwx29qmwfq6i8ksnl91r5vdq2cav1590wnwzfil46ljhky95")))

(define-public crate-winit_input_helper-0.10.0 (c (n "winit_input_helper") (v "0.10.0") (d (list (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "0cg89wykw7pd1y69v4m869c3vhdj45i1g8l05s4qlvafy28zd2ld")))

(define-public crate-winit_input_helper-0.11.0 (c (n "winit_input_helper") (v "0.11.0") (d (list (d (n "winit") (r "^0.26.0") (d #t) (k 0)))) (h "029rc3vpml3lki7pyl45sl179jw0smdf3b52s2ll8gsybj5n3j3j")))

(define-public crate-winit_input_helper-0.11.1 (c (n "winit_input_helper") (v "0.11.1") (d (list (d (n "winit") (r "^0.26.0") (d #t) (k 0)))) (h "055lg40rfh9zz08s0ajz3g24gvgrmd1r7bgjd86p4bay63782cim")))

(define-public crate-winit_input_helper-0.12.0 (c (n "winit_input_helper") (v "0.12.0") (d (list (d (n "winit") (r "^0.26.0") (d #t) (k 0)))) (h "00lk3yiawahjj3lyh66dllv93yrl2rqs1f1yahkh1j1q3zzb857i")))

(define-public crate-winit_input_helper-0.13.0 (c (n "winit_input_helper") (v "0.13.0") (d (list (d (n "winit") (r "^0.27.0") (d #t) (k 0)))) (h "1103xslp2sccb52ff4m2jz64m94ir2rhk486pnkfkxjgic0xc8yy")))

(define-public crate-winit_input_helper-0.14.0 (c (n "winit_input_helper") (v "0.14.0") (d (list (d (n "winit") (r "^0.28") (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1blbv1mwqryh1xrvl3zbzbc9z9y2yygj0viil7wy5l1zg31ch5lg")))

(define-public crate-winit_input_helper-0.14.1 (c (n "winit_input_helper") (v "0.14.1") (d (list (d (n "winit") (r "^0.28") (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1my4xalqwk48zlcwwnswqrxkaj4mij83ldsc5myyhbmahrg4iq2x")))

(define-public crate-winit_input_helper-0.15.0 (c (n "winit_input_helper") (v "0.15.0") (d (list (d (n "winit") (r "^0.29") (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "1wf8c5abrcmjhkjbbyfswsj05d2ykfrrxwaakkqvchi14s0jg6iy")))

(define-public crate-winit_input_helper-0.15.1 (c (n "winit_input_helper") (v "0.15.1") (d (list (d (n "winit") (r "^0.29") (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0anlf7h4bx9qga51szfqqfbllrf2g00paq1p69cvrmqwarg156yf")))

(define-public crate-winit_input_helper-0.15.2 (c (n "winit_input_helper") (v "0.15.2") (d (list (d (n "winit") (r "^0.29") (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "0wfdil2va0bhbs67bwb2aq4dfbqrqsn0vdwymiqcz1krc3j0xcya")))

(define-public crate-winit_input_helper-0.15.3 (c (n "winit_input_helper") (v "0.15.3") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 2)) (d (n "console_log") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "web-time") (r "^1.0") (d #t) (k 0)) (d (n "winit") (r "^0.29") (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "18w3hifmphn57mzbzmhsmy0ara2i5c3hc0hy4sgyck1gfa6l737c")))

(define-public crate-winit_input_helper-0.16.0 (c (n "winit_input_helper") (v "0.16.0") (d (list (d (n "console_error_panic_hook") (r "^0.1") (d #t) (k 2)) (d (n "console_log") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "web-time") (r "^1.0") (d #t) (k 0)) (d (n "winit") (r "^0.29") (k 0)) (d (n "winit") (r "^0.29") (d #t) (k 2)))) (h "011261xjyjgfm5mszksx86kdnr86c7k5bb8akclcndam3szlrb6r")))

