(define-module (crates-io wi ni winipt) #:use-module (crates-io))

(define-public crate-winipt-0.1.0 (c (n "winipt") (v "0.1.0") (d (list (d (n "bitmask") (r "^0.5.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winsvc" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winipt-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0y4qdmhxqyp160w0wycn1md5qpq31q6dmsx8p3zshzqals86s92y")))

(define-public crate-winipt-0.2.0 (c (n "winipt") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winsvc" "winerror"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winipt-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0a83v3n99mf5n1pkn1dn22h80gw7k7jc1zvqjlqfn5ay7s7cvsws")))

