(define-module (crates-io wi ni winit-fullscreen) #:use-module (crates-io))

(define-public crate-winit-fullscreen-0.1.0 (c (n "winit-fullscreen") (v "0.1.0") (d (list (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "17gndvxm2llp71pa9vbsxwf2s6l4jcr5iay1az1vxv8xgs6481n1")))

(define-public crate-winit-fullscreen-0.1.1 (c (n "winit-fullscreen") (v "0.1.1") (d (list (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "12vh7zkd6fyf6g7prdslba229yd8rhlrzxdljlhla67qvshmxr2a")))

(define-public crate-winit-fullscreen-1.0.0 (c (n "winit-fullscreen") (v "1.0.0") (d (list (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "0imq7418l0fxggjqvgpng8k2gjl25j8mcqjz4lgfcbbc96zbgn27")))

(define-public crate-winit-fullscreen-1.0.1 (c (n "winit-fullscreen") (v "1.0.1") (d (list (d (n "pollster") (r "^0.3.0") (d #t) (k 2)) (d (n "wgpu") (r "^0.15.1") (d #t) (k 2)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "097a057qwhajj4pif8a1akkyd8b61l0i04l7k93r6c5i06c588mk")))

(define-public crate-winit-fullscreen-1.0.2 (c (n "winit-fullscreen") (v "1.0.2") (d (list (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "wgpu") (r "^0.18") (d #t) (k 2)) (d (n "winit") (r "^0.29") (d #t) (k 0)))) (h "1ayraayxwj9v7rsc1lgn688sxi81wrr0cwir7pv2rsc492pwxlmn")))

