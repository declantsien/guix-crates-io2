(define-module (crates-io wi ni winit_event_helper) #:use-module (crates-io))

(define-public crate-winit_event_helper-0.1.0 (c (n "winit_event_helper") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (d #t) (k 0)))) (h "0w9jg3vqa1bdr6sldssdgwv4pll0n0cwcjnz6nm2fsm652s4w7cg") (f (quote (("std") ("default" "std"))))))

(define-public crate-winit_event_helper-0.2.0 (c (n "winit_event_helper") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (d #t) (k 0)))) (h "18j6nklypn8ci6620120w8aprh24cn64pl7cnaxigd85mvk7kcll")))

(define-public crate-winit_event_helper-0.2.1 (c (n "winit_event_helper") (v "0.2.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (d #t) (k 0)))) (h "06s83yphi77w995cqpbk1bqv519qzfvgi1yaxq9wg0y8h5pxls4d")))

(define-public crate-winit_event_helper-0.3.0 (c (n "winit_event_helper") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "document-features") (r "^0.2.6") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (d #t) (k 0)))) (h "14jiv7lr2wq08yaj0c41r9mpyjhird3663kf9jljaf26gpccj5fb") (f (quote (("save_device_inputs"))))))

(define-public crate-winit_event_helper-0.3.1 (c (n "winit_event_helper") (v "0.3.1") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "document-features") (r "^0.2.6") (d #t) (k 0)) (d (n "winit") (r "^0.27.2") (d #t) (k 0)))) (h "1w0cls2asgpy7ma0z298ixsdb130ig3xlx3h69qw0wpamk6pr521") (f (quote (("save_device_inputs"))))))

(define-public crate-winit_event_helper-0.4.0 (c (n "winit_event_helper") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defaultmap") (r "^0.5.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "1d3bdmhx9djwqhmyrzrg3yd36xx9991x4wgb9akaj3pljvpihii7") (f (quote (("windows_with_device_ids") ("unique_windows") ("unique_devices"))))))

(define-public crate-winit_event_helper-0.4.1 (c (n "winit_event_helper") (v "0.4.1") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defaultmap") (r "^0.5.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "0yzpxn4qxmsa2fkjj0h8kmsqd0jhqwq829w3n58ps4aq6s7f0116") (f (quote (("windows_with_device_ids") ("unique_windows") ("unique_devices"))))))

(define-public crate-winit_event_helper-0.4.2 (c (n "winit_event_helper") (v "0.4.2") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defaultmap") (r "^0.5.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "06zwk1ba7hb496a904jm3pm4aqxcbv3ywzivqf5fdy34fwnwdfjq") (f (quote (("windows_with_device_ids") ("unique_windows") ("unique_devices"))))))

(define-public crate-winit_event_helper-0.5.0 (c (n "winit_event_helper") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defaultmap") (r "^0.5.0") (d #t) (k 0)) (d (n "document-features") (r "^0.2.7") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 0)))) (h "1jm0892r38422i7fx8s4mkp6mbkbj2vs0fhpywx6bvyin18na7nw") (f (quote (("windows_with_device_ids") ("unique_windows") ("unique_devices"))))))

