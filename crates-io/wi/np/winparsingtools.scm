(define-module (crates-io wi np winparsingtools) #:use-module (crates-io))

(define-public crate-winparsingtools-1.0.0 (c (n "winparsingtools") (v "1.0.0") (d (list (d (n "bitreader") (r "^0.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "16pv2kgnm5g7s5iqrdi9c1fn1jb1y2fdxh52mqzwdg70w0yh7x8k")))

(define-public crate-winparsingtools-1.1.0 (c (n "winparsingtools") (v "1.1.0") (d (list (d (n "bitreader") (r "^0.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "14qng4x064x4dqsgrfx9g8wy6b7i2b667mp8g3van8ianq14llyq")))

(define-public crate-winparsingtools-2.0.0 (c (n "winparsingtools") (v "2.0.0") (d (list (d (n "bitreader") (r "^0.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x66zhz985cbfrws29jw858cfvdyw2svw9kwr65y7q9h6ak23n9d")))

