(define-module (crates-io wi np winpty-sys) #:use-module (crates-io))

(define-public crate-winpty-sys-0.4.3 (c (n "winpty-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0s5m2vvlw7wphc466s47zfmp08zk00wzj999l1w3ajqlxbnfgb9x") (f (quote (("winpty-agent")))) (l "winpty")))

(define-public crate-winpty-sys-0.4.4 (c (n "winpty-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0rmlm5z7pmn149mq06sycpx52lsh9jz4b7yw3ckaaqa7svmcw2z3") (f (quote (("winpty-agent")))) (y #t) (l "winpty")))

(define-public crate-winpty-sys-0.4.5 (c (n "winpty-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1wimj5677gllqnxf6mbhs2zdd73wwxra0qm0cnran7lxmr1nnh52") (f (quote (("winpty-agent")))) (y #t) (l "winpty")))

(define-public crate-winpty-sys-0.5.0 (c (n "winpty-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "041j85zfzsyxcblic9igyxhq9a3dppqymrmm60fwa3bnb6d1g2pd") (f (quote (("winpty-agent")))) (l "winpty")))

