(define-module (crates-io wi np winpath) #:use-module (crates-io))

(define-public crate-winpath-0.0.1 (c (n "winpath") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1x7bcmpiyi4ggaxal1j1w7wr5kbkng4bias5csdp9ffbzdykbi6h")))

(define-public crate-winpath-0.1.0 (c (n "winpath") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0nn1mqhsj555ma1p4yc7s1s445vyjz02m4j2yf4z99k0qbxbg9pr")))

(define-public crate-winpath-0.1.1 (c (n "winpath") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fhssgwrgz8y5f4vni1l94yskvl2ygrdrblk8q0qgxmmvpgxfbkp")))

