(define-module (crates-io wi np winpipe2serial) #:use-module (crates-io))

(define-public crate-winpipe2serial-0.1.0 (c (n "winpipe2serial") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std" "commapi" "errhandlingapi" "fileapi" "namedpipeapi" "handleapi" "ioapiset" "synchapi" "winbase" "winerror" "winnt"))) (d #t) (k 0)))) (h "10mcaa8y4y1gv0flc5czjnf2jbvnkl23i8rcv24ikbl190n14vgm")))

