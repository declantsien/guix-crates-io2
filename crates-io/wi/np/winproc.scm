(define-module (crates-io wi np winproc) #:use-module (crates-io))

(define-public crate-winproc-0.1.0 (c (n "winproc") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase"))) (d #t) (k 0)))) (h "1lhdmx87aiz44jbg98fi14jazp629yyhj7izyvndqs191nljmh3j")))

(define-public crate-winproc-0.1.1 (c (n "winproc") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase"))) (d #t) (k 0)))) (h "07whplzsj26c6nwpnjaqc3dzrir16kj29wrd35k343l60ndhnaap")))

(define-public crate-winproc-0.1.2 (c (n "winproc") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi"))) (d #t) (k 0)))) (h "0nyr899lfsa8fg7f8cyyxvj7hh9zcjklp8gkyzs5h6wbjgm6l4sd")))

(define-public crate-winproc-0.2.0 (c (n "winproc") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi"))) (d #t) (k 0)))) (h "0pz1klas60wci0939h0j3l9szkizkm8zfzsxpkjah0q17bd2yzqj")))

(define-public crate-winproc-0.2.1 (c (n "winproc") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi"))) (d #t) (k 0)))) (h "1gqgw63szdsf3mqlamjkny10bjifgflbiiaxnkansqnzzfm98d9a")))

(define-public crate-winproc-0.3.0 (c (n "winproc") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi"))) (d #t) (k 0)))) (h "1rmkdz02s7yy96cka8an76kc72b9qdm6c0mdan545kbs9lf2qw2s")))

(define-public crate-winproc-0.3.1 (c (n "winproc") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi"))) (d #t) (k 0)))) (h "0ss15b95wx2wcr5wgzygvxi6nh94a25n7a1vssd0maszfin2hyb7")))

(define-public crate-winproc-0.4.0 (c (n "winproc") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "12482cipi2l81rdyf09f09dqdbgrvsj6ncp8r6ka7j6xq232wk9q")))

(define-public crate-winproc-0.5.0 (c (n "winproc") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "098ldyh913839cwq9m05axlwi1zf90y7nw8p1p7rvax3h0wmxi58")))

(define-public crate-winproc-0.5.1 (c (n "winproc") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "1cb11xq6f0m34fbd26yiay5i8bkysyfbi6x9hbkz5qdwgs4g2dsm")))

(define-public crate-winproc-0.6.0 (c (n "winproc") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "08gl4r3r59smfswamk2fsv8p8z90bmlx2mk2iazsc8gq4g4lb23j") (y #t)))

(define-public crate-winproc-0.6.1 (c (n "winproc") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "0flvrmws9xa6rpmg941sg3wl66yjzs2az9kprk5pkv3wan0d5p5h")))

(define-public crate-winproc-0.6.2 (c (n "winproc") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "0vfx0sdakzsl4chd9wpn6674sg8rnb9h5sfqvq8h27sj9mv7mncv")))

(define-public crate-winproc-0.6.3 (c (n "winproc") (v "0.6.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "1a5i4x85a2mwmkmnrjymzdcyzxzfliv4fbf2gm86fc1kzpzsdzwk")))

(define-public crate-winproc-0.6.4 (c (n "winproc") (v "0.6.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "tlhelp32" "realtimeapiset" "winbase" "psapi" "sysinfoapi"))) (d #t) (k 0)))) (h "09bv127708080bpm5bbv5qifq0xl8swx2hjf8mf7xwka7jrhg7lp")))

