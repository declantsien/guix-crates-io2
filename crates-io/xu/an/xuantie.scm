(define-module (crates-io xu an xuantie) #:use-module (crates-io))

(define-public crate-xuantie-0.0.1 (c (n "xuantie") (v "0.0.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1v18ica2nidqg93cb4sz4jngs0ab4xm7rim2jgqibcq9421nv3cq")))

(define-public crate-xuantie-0.0.2 (c (n "xuantie") (v "0.0.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "11ggnk3mfh9salbrm1zc7n113ml2fi3zjkzdznwcqkklz75nrca6")))

(define-public crate-xuantie-0.0.3 (c (n "xuantie") (v "0.0.3") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "06fnczh4lcbviiiswlqjzw7qcilz0dv0ghmvbqi8rk25ysz7z71x")))

(define-public crate-xuantie-0.0.4 (c (n "xuantie") (v "0.0.4") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "1xf4g354ljg5zydnbfbpm798b9zcdvpinxj0ia1pnr84wyi6hkq3")))

(define-public crate-xuantie-0.0.5 (c (n "xuantie") (v "0.0.5") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)))) (h "0xd9bpxp5jd84cl4y2699qb1vd5l5blgiw5z8bxwrngvbm9qhnb6")))

