(define-module (crates-io xu ex xuexi_000115) #:use-module (crates-io))

(define-public crate-xuexi_000115-0.1.0 (c (n "xuexi_000115") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m52s4bhimvmabyc7l6v0ldqpia5lw8db6k18rdbs8imv6352djg") (y #t)))

(define-public crate-xuexi_000115-0.1.1 (c (n "xuexi_000115") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s2xnm94njwligvyp33gfxyq6vqwm2wpw3r4y47pqya0lsh85cl9") (y #t)))

