(define-module (crates-io xu rl xurl) #:use-module (crates-io))

(define-public crate-xurl-0.1.0 (c (n "xurl") (v "0.1.0") (d (list (d (n "idna") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.3.3") (d #t) (k 0)))) (h "06yc63sv39ap3qnxfw8grdf33yqvsjqhsvnmsar9aj6cb92rl7yg")))

