(define-module (crates-io xu #{4-}# xu4-hal) #:use-module (crates-io))

(define-public crate-xu4-hal-0.1.1 (c (n "xu4-hal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)))) (h "0wjd52mx1i8l44njp1gz51vgri2l3yzs7dr38x4zkzgybxgacwhr") (y #t)))

(define-public crate-xu4-hal-0.1.2 (c (n "xu4-hal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)))) (h "1rsfagn1vhipzygc79p0nnazwbs39rnd3kgsp7xhhlbrgzaj08zn") (y #t)))

(define-public crate-xu4-hal-0.2.2 (c (n "xu4-hal") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)))) (h "0a94mprvwr0xw9wg4jrm59mxj3c5i8hkcs54b22gkhfz510ha778") (y #t)))

(define-public crate-xu4-hal-0.2.3 (c (n "xu4-hal") (v "0.2.3") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)))) (h "088yy6yyab8lbv39wldr783q30cmybgcxrzmibnzacxl614vpibd") (y #t)))

