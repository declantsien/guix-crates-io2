(define-module (crates-io eq ui equihash) #:use-module (crates-io))

(define-public crate-equihash-0.0.0 (c (n "equihash") (v "0.0.0") (h "1g0vn6vm6h507wivl24k06278glrd9a0pk4cl1sf0q19ly68visj")))

(define-public crate-equihash-0.1.0 (c (n "equihash") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^0.5") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1042rm46jv3gzs73pi4x9whjh4hdv77v274845szbqvpc67nh9s1")))

(define-public crate-equihash-0.2.0 (c (n "equihash") (v "0.2.0") (d (list (d (n "blake2b_simd") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1m9khpkdrp4kr9a1sw9c27bh5vq2jzwc42z80cxpfxw4yxy9smxb")))

