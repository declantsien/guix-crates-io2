(define-module (crates-io eq ui equilibrium) #:use-module (crates-io))

(define-public crate-equilibrium-0.1.0-alpha (c (n "equilibrium") (v "0.1.0-alpha") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "16sqxk4iwqk64h2pi8j3j73y54hy5hhr21lzvshz4crssqfs8hyg")))

