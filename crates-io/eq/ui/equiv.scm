(define-module (crates-io eq ui equiv) #:use-module (crates-io))

(define-public crate-equiv-0.1.1 (c (n "equiv") (v "0.1.1") (h "18w0hzp628knvky4jq333mkrrdfx3lmmqafzr5d0jzd9ga5a0vly")))

(define-public crate-equiv-0.1.2 (c (n "equiv") (v "0.1.2") (h "10swv0c3z0g945hfmdavchrpya9d7avg85q5d2bk8ky62hw163ap")))

(define-public crate-equiv-0.1.3 (c (n "equiv") (v "0.1.3") (h "032zn7s5bp186896w8s3ps4zw5d6dy513vcqzzjcy1xxy381l9dd")))

