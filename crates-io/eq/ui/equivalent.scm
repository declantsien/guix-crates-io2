(define-module (crates-io eq ui equivalent) #:use-module (crates-io))

(define-public crate-equivalent-0.1.0 (c (n "equivalent") (v "0.1.0") (h "0ckdg46jvva8hhgnyjh0ywf1kir3kr0mc8ihgawa0lziswbagzad") (r "1.6")))

(define-public crate-equivalent-1.0.0 (c (n "equivalent") (v "1.0.0") (h "18f0q7vd4awiv9bv5mda5yv8lfhpzxspiq8f2jdjqhw0bnygxgw8") (r "1.6")))

(define-public crate-equivalent-1.0.1 (c (n "equivalent") (v "1.0.1") (h "1malmx5f4lkfvqasz319lq6gb3ddg19yzf9s8cykfsgzdmyq0hsl") (r "1.6")))

