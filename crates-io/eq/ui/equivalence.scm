(define-module (crates-io eq ui equivalence) #:use-module (crates-io))

(define-public crate-equivalence-0.1.0 (c (n "equivalence") (v "0.1.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "equivalence-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0fg2w9wvgqkaqb4pqf0v931zinzsfz4idrdgzdqbhvlh2iydcr1a") (f (quote (("derive" "equivalence-derive") ("default" "derive"))))))

(define-public crate-equivalence-0.1.1 (c (n "equivalence") (v "0.1.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "equivalence-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1qddzzi4mkzqrhv19zh81kmzrbkv27cx4p949qn12igfnqh02n2k") (f (quote (("derive" "equivalence-derive") ("default" "derive"))))))

