(define-module (crates-io eq ui equivalence-derive) #:use-module (crates-io))

(define-public crate-equivalence-derive-0.1.0 (c (n "equivalence-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1pp2cbcv65zi718fw47y6p5jfs7m5vvfsd9w2xl4rddg8rdcnd6h")))

