(define-module (crates-io eq lo eqlog-eqlog) #:use-module (crates-io))

(define-public crate-eqlog-eqlog-0.3.0 (c (n "eqlog-eqlog") (v "0.3.0") (d (list (d (n "eqlog") (r "^0.2.3") (d #t) (k 1)) (d (n "eqlog-runtime") (r "^0.2.3") (d #t) (k 0)))) (h "09zd763362aqmbq9gs6rjyv3wh5fm9jaaly49a3vcfvq0lxbfsi3")))

(define-public crate-eqlog-eqlog-0.3.1 (c (n "eqlog-eqlog") (v "0.3.1") (d (list (d (n "eqlog") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "^0.3.0") (d #t) (k 0)))) (h "17sfgbjfwlrkjxxs7839xhjqqbsqck80cyfx16brxq5608h84lhv") (f (quote (("default")))) (y #t) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.3.2 (c (n "eqlog-eqlog") (v "0.3.2") (d (list (d (n "eqlog") (r "=0.3.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.3.0") (d #t) (k 0)))) (h "0jz2az6s5mjfyx5g8my91v5x2m43gmryp4bjjy1s7wqz78m3riwr") (f (quote (("default")))) (y #t) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.4.0 (c (n "eqlog-eqlog") (v "0.4.0") (d (list (d (n "eqlog") (r "=0.3.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.3.0") (d #t) (k 0)))) (h "1abahppk51dm9vapriq5acj3jzs21m7v1z2v1vzv4dnrcgbbixni") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.4.1 (c (n "eqlog-eqlog") (v "0.4.1") (d (list (d (n "eqlog") (r "=0.4.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.4.0") (d #t) (k 0)))) (h "0jlj1pp2lrj949rm8icmh8al16b846gggsqc4mh0ncgrzmg6wwxl") (f (quote (("default")))) (y #t) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.5.0 (c (n "eqlog-eqlog") (v "0.5.0") (d (list (d (n "eqlog") (r "=0.4.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.4.0") (d #t) (k 0)))) (h "1i6xa14gzck0hgsjc7v2kf1df396wk1cnk37fwxqxddjnc8ipjhq") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.5.1 (c (n "eqlog-eqlog") (v "0.5.1") (d (list (d (n "eqlog") (r "=0.4.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.4.0") (d #t) (k 0)))) (h "0px718mi24rhha7vh78nxd1ahdjg436yrssl1zmc3y1200pfp2kw") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.5.2 (c (n "eqlog-eqlog") (v "0.5.2") (d (list (d (n "eqlog") (r "=0.5.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.5.0") (d #t) (k 0)))) (h "1g3pgyb19n3a9446py3sy91rja9s0lhrms9fxz60j063vsym30ri") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.6.0 (c (n "eqlog-eqlog") (v "0.6.0") (d (list (d (n "eqlog") (r "=0.5.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.5.0") (d #t) (k 0)))) (h "0nqvsiycgnq373hqbhbgl35nns26wcgn201p3la84795m63spicq") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

(define-public crate-eqlog-eqlog-0.7.0 (c (n "eqlog-eqlog") (v "0.7.0") (d (list (d (n "eqlog") (r "=0.6.0") (o #t) (d #t) (k 1)) (d (n "eqlog-runtime") (r "=0.6.0") (d #t) (k 0)))) (h "06gv2112rnksb0dg9jlrs3i3g3nj5b1gq8ryxzlbck8m61sz00yx") (f (quote (("default")))) (s 2) (e (quote (("rebuild" "dep:eqlog"))))))

