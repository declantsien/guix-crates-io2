(define-module (crates-io eq lo eqlog-runtime) #:use-module (crates-io))

(define-public crate-eqlog-runtime-0.1.0 (c (n "eqlog-runtime") (v "0.1.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1vc0lppgz6lmd379lphb0gwf2mj1bd11jhy6fs8zxkmh7ay6m5vy")))

(define-public crate-eqlog-runtime-0.1.1 (c (n "eqlog-runtime") (v "0.1.1") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "15vk7yp9xs12gxnksv3cdaik77jx30mj65j2y010flhfnk05hjpk")))

(define-public crate-eqlog-runtime-0.1.2 (c (n "eqlog-runtime") (v "0.1.2") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1xz90w5qgmky1vn2xdx9ylyrinppbxn151dmb61vi2bb15gqbxbh")))

(define-public crate-eqlog-runtime-0.1.3 (c (n "eqlog-runtime") (v "0.1.3") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0q9af8y2lv9zjq8p041m663fq5c9mcz9qgb36p35q1i44b0maf1z")))

(define-public crate-eqlog-runtime-0.1.4 (c (n "eqlog-runtime") (v "0.1.4") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0hxzvvkdyjl2xjik0sl7pjrk2ai9d2m9pffdshb8xkj3hg69xwxf")))

(define-public crate-eqlog-runtime-0.1.5 (c (n "eqlog-runtime") (v "0.1.5") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "06bbg28h43p3mrki5ix1azzbsrzwk3ka686ip4g4ar3hlzp1li0k")))

(define-public crate-eqlog-runtime-0.2.0 (c (n "eqlog-runtime") (v "0.2.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0igky6xzlqrqw6z7khjrl1szn71rc2l3jd0ijlf42wq5wdvckmgr")))

(define-public crate-eqlog-runtime-0.2.1 (c (n "eqlog-runtime") (v "0.2.1") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0rqrd9qbnx6fjd4lyl0n0bzfm2v4vrrr3nbsiybi7047yznsa9mm")))

(define-public crate-eqlog-runtime-0.2.2 (c (n "eqlog-runtime") (v "0.2.2") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1jpxdcqwhday5cxn54xqsmpp1mnypwd6a60fl3szjadfdbvj28q7")))

(define-public crate-eqlog-runtime-0.2.3 (c (n "eqlog-runtime") (v "0.2.3") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "18l7vfzmlc2jq8lg091h933l87x412njpglkbjdh2azdla8lcvar")))

(define-public crate-eqlog-runtime-0.3.0 (c (n "eqlog-runtime") (v "0.3.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0bhc44bvs48n1adlsks5vfmdzz65pig35blpd5jzy5zp9iwa0ban")))

(define-public crate-eqlog-runtime-0.3.1 (c (n "eqlog-runtime") (v "0.3.1") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "04x7k2v12s0m7r9w7abw8x2l0q7g6kj6pbc2k5sg1p45s92k45yp") (y #t)))

(define-public crate-eqlog-runtime-0.4.0 (c (n "eqlog-runtime") (v "0.4.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "0kwrq6ar5w117y532iy59x9v3pz4j6x7n4ypxc54miw1lq861nmn")))

(define-public crate-eqlog-runtime-0.4.1 (c (n "eqlog-runtime") (v "0.4.1") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1w52aw5hvvj38x61sx2p8z7rjvy6hmalzyb5syncd8sbl716mbx3") (y #t)))

(define-public crate-eqlog-runtime-0.5.0 (c (n "eqlog-runtime") (v "0.5.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1gd4z6rxpdnh19ykkjlfdn7xycbfjpq5vzqih23r8z24psj4b6sc")))

(define-public crate-eqlog-runtime-0.5.1 (c (n "eqlog-runtime") (v "0.5.1") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1myhwf0hmycj0hbxxw4a7rd84pqn7sy44m1jf3scmxzzs0g129cx")))

(define-public crate-eqlog-runtime-0.5.2 (c (n "eqlog-runtime") (v "0.5.2") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "12hsphbgkfsdk6qzg8y5jz7az9f0909pzp7nkd8xlmsrvl2s9ymy")))

(define-public crate-eqlog-runtime-0.6.0 (c (n "eqlog-runtime") (v "0.6.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1nykfbr5xpnj6187ljfd03ql6vvs07n83a7685fzfb2sclpr87wk")))

(define-public crate-eqlog-runtime-0.2.4 (c (n "eqlog-runtime") (v "0.2.4") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1h813ac07y7nncwsbg3fcdyklw8hd9xg1kdld0sw01q3h6q91vgf")))

(define-public crate-eqlog-runtime-0.7.0 (c (n "eqlog-runtime") (v "0.7.0") (d (list (d (n "tabled") (r "^0.7") (d #t) (k 0)))) (h "1q3g5n2zm3fxi2gydzfsp3pzf6hsxnzch7dp9hb25slxipf4rh2x")))

