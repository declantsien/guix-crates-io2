(define-module (crates-io eq ut equt-md) #:use-module (crates-io))

(define-public crate-equt-md-0.0.1 (c (n "equt-md") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "unicase") (r "^2.6") (d #t) (k 0)))) (h "030v5gikicshbrg9gdjwbz2s8r5lbmqpmxhnd97v6pl8pd28ifb1") (f (quote (("simd"))))))

