(define-module (crates-io eq ut equt-md-error) #:use-module (crates-io))

(define-public crate-equt-md-error-0.1.0 (c (n "equt-md-error") (v "0.1.0") (d (list (d (n "katex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1nxwm9s6na8jaalv1dlifgzayxwqwm1hyrkdvxqwnqf1mlp8y3fn")))

(define-public crate-equt-md-error-0.1.1 (c (n "equt-md-error") (v "0.1.1") (d (list (d (n "katex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "13zwjf3b68bnrlazbf7554radk9hlrgf1g85ji0kbvmxaij6kxnc")))

