(define-module (crates-io eq ut equt-md-html) #:use-module (crates-io))

(define-public crate-equt-md-html-0.1.0 (c (n "equt-md-html") (v "0.1.0") (d (list (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-ext") (r "^0.1.0") (d #t) (k 0)) (d (n "katex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "027nb1j2m6vzr1y1xvxwqj0gyz96cawfdrda7dxhf2dha2alzipv")))

