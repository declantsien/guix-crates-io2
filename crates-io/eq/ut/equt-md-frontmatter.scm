(define-module (crates-io eq ut equt-md-frontmatter) #:use-module (crates-io))

(define-public crate-equt-md-frontmatter-0.1.0 (c (n "equt-md-frontmatter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 2)))) (h "0rpp4cpj3g0vf0fw5iy3kpgb1p5d9i43610k2xnnhaz3dzjpwdlz")))

(define-public crate-equt-md-frontmatter-0.1.1 (c (n "equt-md-frontmatter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 2)))) (h "0k7i4j2jnc7s18wazxznwzw0ccnscphv39iclfk5nnxdkdhrsf0r")))

