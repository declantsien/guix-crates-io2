(define-module (crates-io eq ut equt-md-ext) #:use-module (crates-io))

(define-public crate-equt-md-ext-0.1.0 (c (n "equt-md-ext") (v "0.1.0") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0hb7hznwgih2jz3wyy2d1xm5y42y18sxn631mmz5qkxnz1inc6hi")))

(define-public crate-equt-md-ext-0.1.1 (c (n "equt-md-ext") (v "0.1.1") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0mzigijy6jxw7mxchakb3k37s8rsji80p8d0d423ghyb8fx9n2lh")))

(define-public crate-equt-md-ext-0.2.0 (c (n "equt-md-ext") (v "0.2.0") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "1rjiig5wnh9llr3vq752cswf9nhgg1y9v5gvd4vf8ddy2d3fhij9")))

(define-public crate-equt-md-ext-0.2.1 (c (n "equt-md-ext") (v "0.2.1") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0xq8njzghln845va7k9n1jjjsmd3ngsanasc1srbihs0xwzfcy7n")))

(define-public crate-equt-md-ext-0.2.2 (c (n "equt-md-ext") (v "0.2.2") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0hd5w0xk51p86wgbm88frziybqjxzrswa88vsvlsbbcsqdl36pq5")))

(define-public crate-equt-md-ext-0.2.3 (c (n "equt-md-ext") (v "0.2.3") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "084k01kz5sg3qx7pxxpd7g66h8zzzlxc4bp1941jdpa36ll086bf")))

(define-public crate-equt-md-ext-0.2.4 (c (n "equt-md-ext") (v "0.2.4") (d (list (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "03cmp6md608q2f86pf5b2171vddx4hblf18wzbgv9mg6hsrjhp3m")))

(define-public crate-equt-md-ext-0.2.5 (c (n "equt-md-ext") (v "0.2.5") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "1ml0yjh0f2kw8r97g42c7k3zrzsqi9aq0s6fv49rl7fd2h374gh7")))

(define-public crate-equt-md-ext-0.2.6 (c (n "equt-md-ext") (v "0.2.6") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "0gfbpgixcs8vz717idi5s4ijvjl1rjksmilav850jxdd0f9qhxk5")))

(define-public crate-equt-md-ext-0.2.7 (c (n "equt-md-ext") (v "0.2.7") (d (list (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "equt-md") (r "^0.0.1") (d #t) (k 0)) (d (n "equt-md-error") (r "^0.1.0") (d #t) (k 0)) (d (n "equt-md-frontmatter") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.12") (d #t) (k 0)))) (h "1fhnssbqqw4fv82yaqzrxixmjdl0yjgza6mxi74sd1m01h2d71zh")))

