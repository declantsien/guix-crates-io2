(define-module (crates-io eq so eqsolver) #:use-module (crates-io))

(define-public crate-eqsolver-0.1.0 (c (n "eqsolver") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1lxr98z40km2rps17dm50frgr489g42hgwhml5s0zrnwfxjji8va")))

(define-public crate-eqsolver-0.1.1 (c (n "eqsolver") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1k6238mhbcmrzg6pxm3b1qrs8qm4hv7257j8h5r53ymp8789l03m")))

(define-public crate-eqsolver-0.1.2 (c (n "eqsolver") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1p10cqimmwz082qjnmwgprw6j40cicmn66a4s0z1bn43r6g43p4w")))

(define-public crate-eqsolver-0.1.3 (c (n "eqsolver") (v "0.1.3") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0djbn28rv7rzs1fi1a23w0cq9ac49lxbc87fk20lcbj5z8hkz42l")))

