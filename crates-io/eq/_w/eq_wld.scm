(define-module (crates-io eq _w eq_wld) #:use-module (crates-io))

(define-public crate-eq_wld-0.1.0 (c (n "eq_wld") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "10ylckbbwq5589q8w62q30nkbc6x8bplmdi76bq6bqndxb6im0dl")))

(define-public crate-eq_wld-0.1.1 (c (n "eq_wld") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "0kk8243ayhg639fhhjzw15i03xaxqndwyyjwbd3x5phhmaxhb1iw")))

(define-public crate-eq_wld-0.2.1 (c (n "eq_wld") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "1pfmn2lzdbimi7xjvrqm19zf3wk6jq0ay0x8m0aaf0lmji2dslp4")))

