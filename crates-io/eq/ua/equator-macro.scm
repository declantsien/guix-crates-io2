(define-module (crates-io eq ua equator-macro) #:use-module (crates-io))

(define-public crate-equator-macro-0.1.0 (c (n "equator-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1jmakba3qm779wshdp8dq9qpdb1vijy22i119g6ymqjakbc21sw5")))

(define-public crate-equator-macro-0.1.2 (c (n "equator-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "007zf37yzsjaf2ga4cwm7am0hnvn53b2agvyc0yk3q731m7vkr70")))

(define-public crate-equator-macro-0.1.3 (c (n "equator-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1i4fij9pla0689z7q5zsrdyc4dzgcxipkzwqmy8nd0dva0hpsmsw")))

(define-public crate-equator-macro-0.1.4 (c (n "equator-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1l1rdjskhj72v7nh8gj83a1dhd62crmbfw1fdx2ki7k1x8vw8a31")))

(define-public crate-equator-macro-0.1.5 (c (n "equator-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6sggxhzv3n2wjhyib73jp8ypa5zrldq99vbb33lb1pw9cncmsa")))

(define-public crate-equator-macro-0.1.9 (c (n "equator-macro") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1bpkdiw8xskx3685lr53hf8q49m5wldjaksn052gpxs9k35qml30")))

(define-public crate-equator-macro-0.2.0 (c (n "equator-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1c0qikxibxrlihpisrrq3g64dz2sc902z6nrxlqcqpfi9dsw7rgc")))

(define-public crate-equator-macro-0.2.1 (c (n "equator-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1k0n5q7d6wn9g3fva9z7hr4pr3z494dsb1zja5ima8h3diwpkxiv")))

