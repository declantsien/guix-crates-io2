(define-module (crates-io eq ua equator) #:use-module (crates-io))

(define-public crate-equator-0.1.0 (c (n "equator") (v "0.1.0") (d (list (d (n "equator-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1g8hk35dg9wanrira7gfyh94ca4w38y5g6v8d6ba83lqycgdkxgs") (y #t)))

(define-public crate-equator-0.1.1 (c (n "equator") (v "0.1.1") (d (list (d (n "equator-macro") (r "^0.1.0") (d #t) (k 0)))) (h "06gkgvi3srrai1qn5nz69ygabi3svxaprialm5v6kn51q1zw52ps") (y #t)))

(define-public crate-equator-0.1.2 (c (n "equator") (v "0.1.2") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.0") (d #t) (k 0)))) (h "178gv48jf9l1fhj37zylz0rdsfnabi106k62sspvy7sj4mi6avya") (y #t)))

(define-public crate-equator-0.1.3 (c (n "equator") (v "0.1.3") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0w0si01mfbqzpwckb9vfgf44fn79vaxlafcbgxh45zds1v0yj5s0") (y #t)))

(define-public crate-equator-0.1.4 (c (n "equator") (v "0.1.4") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.4") (d #t) (k 0)))) (h "0m2g0m08dcqs0z92mpplr4jmskh2rxf8y6kf9jwns6qi6q5jjh8p") (y #t)))

(define-public crate-equator-0.1.5 (c (n "equator") (v "0.1.5") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0j2b1rd7avrpq66fdbx7ygifmmnwxa4w3a6lg552sbjdb3jgp038") (y #t)))

(define-public crate-equator-0.1.6 (c (n "equator") (v "0.1.6") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0fm7liy9d20f18h30hcypi51r42ikbzrvfz325zc72fzfiwnfpca") (y #t)))

(define-public crate-equator-0.1.7 (c (n "equator") (v "0.1.7") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.5") (d #t) (k 0)))) (h "07y7vmd1a53v9yp8frqhppsia272dclv2mnmh3hw1lld232bmdl6") (y #t)))

(define-public crate-equator-0.1.8 (c (n "equator") (v "0.1.8") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.5") (d #t) (k 0)))) (h "1rbz0fc8xn1ndh50iac2iqfvjm8zg37y687xcpjfqh6kchj9ld0c")))

(define-public crate-equator-0.1.9 (c (n "equator") (v "0.1.9") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.9") (d #t) (k 0)))) (h "10kcv936326sp941nkdvb94lqdsk7mw6zlgkdvryhphs9xqr37dw")))

(define-public crate-equator-0.1.10 (c (n "equator") (v "0.1.10") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.1.9") (d #t) (k 0)))) (h "1s7flp2iph6j9divrnqv0pwx65qssfp7ji2fd2wx42hxm65aic53")))

(define-public crate-equator-0.2.0 (c (n "equator") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1hhsdm419h374zxfsgbjh19xgq5vj8axyr2i6n1d764xpbiv46cw")))

(define-public crate-equator-0.2.1 (c (n "equator") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.2.1") (d #t) (k 0)))) (h "06v6abvs78w79ss8pb59h0vbz8zz3ckp657kw6p75x29xxvc8xyy")))

(define-public crate-equator-0.2.2 (c (n "equator") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3.11") (d #t) (k 2)) (d (n "equator-macro") (r "^0.2.1") (d #t) (k 0)))) (h "1sk1swwd76rn091gi1m26s10z11dgynb4jfcly228782b8xsapf3")))

