(define-module (crates-io eq ua equation-solver) #:use-module (crates-io))

(define-public crate-equation-solver-0.1.0 (c (n "equation-solver") (v "0.1.0") (h "1w03y8z85qryb5xg1jlfps4w19f7p5pfh54rgmxn3n9rj3ajvhhj")))

(define-public crate-equation-solver-0.1.1 (c (n "equation-solver") (v "0.1.1") (h "0d4j6gi67s96sxcn0f9gxjw335f58k5h7h4swlgg50561crz9qxx")))

