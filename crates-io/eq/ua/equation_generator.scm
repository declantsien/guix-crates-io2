(define-module (crates-io eq ua equation_generator) #:use-module (crates-io))

(define-public crate-equation_generator-0.1.0 (c (n "equation_generator") (v "0.1.0") (d (list (d (n "random-number") (r "^0.1.4") (d #t) (k 0)))) (h "18bnpm3fiypn5l2k4j7mi1p9liqxi4jx42kcmcvn2f8hhqvb14v5")))

(define-public crate-equation_generator-0.1.1 (c (n "equation_generator") (v "0.1.1") (d (list (d (n "random-number") (r "^0.1.4") (d #t) (k 0)))) (h "02xy9gl5z7zbdna7qdxffk6g1zkdj0plivqdhawgiwzh20vmdhlx")))

(define-public crate-equation_generator-0.1.2 (c (n "equation_generator") (v "0.1.2") (d (list (d (n "random-number") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "06niv8hqlrbnnip044px8w0px7dz87c9n1v2vmpnkjpiavg8qml0")))

