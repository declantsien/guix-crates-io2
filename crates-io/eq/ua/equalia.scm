(define-module (crates-io eq ua equalia) #:use-module (crates-io))

(define-public crate-equalia-0.1.0 (c (n "equalia") (v "0.1.0") (h "0cr80ha7z54wa3zdkcpjs164g35g7qxzxa1lisjdhfypivmrbwc9")))

(define-public crate-equalia-1.0.0 (c (n "equalia") (v "1.0.0") (d (list (d (n "darling") (r "^0.12.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0d556vk4wi4340rhhyvkjhmarsarks2h7ybnqmw3569alaa8p3la")))

