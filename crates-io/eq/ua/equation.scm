(define-module (crates-io eq ua equation) #:use-module (crates-io))

(define-public crate-equation-0.0.0 (c (n "equation") (v "0.0.0") (h "1z9lij8l1w5gdz3sx801kw7bykcwqcjvadkb6ibkv53w4q8ifsxs")))

(define-public crate-equation-0.1.0 (c (n "equation") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "0pd6c499xarjgnmxn55505rvskry32zs27wx140fv3a5bdska2bf")))

(define-public crate-equation-0.1.1 (c (n "equation") (v "0.1.1") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "1ghry4ddm7s4myjayfrjnyx5c7nq0n2ksdvs74iqx23xqh0g3d1q")))

(define-public crate-equation-0.2.0 (c (n "equation") (v "0.2.0") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "0wsmxb6ai3vkanh4qhzyy60b5d0npdr6ybf1ljn17dynphjfa7pf")))

(define-public crate-equation-0.3.0 (c (n "equation") (v "0.3.0") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "0x4hja8m4755mqxf2pxv1mihj57f92839c4zaxr8iy5kzcaz8g96")))

(define-public crate-equation-1.0.0 (c (n "equation") (v "1.0.0") (d (list (d (n "pest") (r "^2.7.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.3") (d #t) (k 0)))) (h "09mqrwxjy1kjkxiqxasg3bi592n40pgrk9kx5dkgm22vcpn054ca")))

