(define-module (crates-io eq ua equationx) #:use-module (crates-io))

(define-public crate-equationx-0.1.0 (c (n "equationx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer" "regex" "unicode"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)))) (h "0ll7wgbpf0szb6bdb7p0lcbz9a11m25nvhn2driqv4r55an3h664")))

