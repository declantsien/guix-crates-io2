(define-module (crates-io eq #{3-}# eq3-max-cube_rs) #:use-module (crates-io))

(define-public crate-eq3-max-cube_rs-0.1.0 (c (n "eq3-max-cube_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1bs2kfy60adfi72scn1mp1f4vcc3qb41gn1lamwdyssmqy8c5rfb")))

(define-public crate-eq3-max-cube_rs-0.1.1 (c (n "eq3-max-cube_rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kgl2ywwx32hqvyycapvnin6llgk3m1b02f2wq8ih7r0acdanshz")))

(define-public crate-eq3-max-cube_rs-0.2.0 (c (n "eq3-max-cube_rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pmrqm7kh1jcvqsidahmkb43awal7azxvnbhgxlry8445z4zmsid")))

(define-public crate-eq3-max-cube_rs-0.2.1 (c (n "eq3-max-cube_rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mp5q5isz0k40l12n892s934lph3lnxw398isbzpqjq9z9szzbvv")))

(define-public crate-eq3-max-cube_rs-0.2.2 (c (n "eq3-max-cube_rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18vz0i6ljy2l9r60c0cin1fxxl01p83jh2iwa36xzilg1lnva122")))

(define-public crate-eq3-max-cube_rs-0.2.3 (c (n "eq3-max-cube_rs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0isi636jmkj1xvkwy30vjabk1q4vb53jfs6ksg8ywsg1lrb9pdmk")))

(define-public crate-eq3-max-cube_rs-0.2.4 (c (n "eq3-max-cube_rs") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gfsjvd1002zr1rfk04flwd3gs1k8r9qzzz69ca4a2h1v3yqgiy5")))

(define-public crate-eq3-max-cube_rs-0.2.5 (c (n "eq3-max-cube_rs") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08xsnchgagad4ad6xccb72rlyv834avz5qi7fh93mr7liy9vakgw")))

