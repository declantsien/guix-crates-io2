(define-module (crates-io eq fo eqformat_s3d) #:use-module (crates-io))

(define-public crate-eqformat_s3d-0.1.0 (c (n "eqformat_s3d") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "14fpyfhp08cs5kz77sqjdz04ndq2g1mhhvd24q3sq5k8vsdpmka9") (y #t)))

