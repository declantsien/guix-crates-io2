(define-module (crates-io eq fo eqformat_pfs) #:use-module (crates-io))

(define-public crate-eqformat_pfs-0.1.0 (c (n "eqformat_pfs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "03w03w0jvmhs4vdpdcawzfv0xllaks87smcw8njrrmz2wqh98ghz")))

(define-public crate-eqformat_pfs-0.1.1 (c (n "eqformat_pfs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "05nly30fqjsnnbs2ia2654mypps2d3h2hdihzjkqy40z3r289k3s")))

(define-public crate-eqformat_pfs-0.1.2 (c (n "eqformat_pfs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1f4djlxahgcls1phz9fzz57a0fv6izx7f5q18nhpv4lvk8bw0da7")))

(define-public crate-eqformat_pfs-0.1.3 (c (n "eqformat_pfs") (v "0.1.3") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1z1jis9wckhjl3rrd4c0vfz154nwx4hxanf5ca32xcznjgr3zfvx")))

(define-public crate-eqformat_pfs-0.1.4 (c (n "eqformat_pfs") (v "0.1.4") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1xwbgdvh5mj83i584vr24rxyfrn0pxcr0asrgd27kfbmrnjzayp0")))

(define-public crate-eqformat_pfs-0.1.5 (c (n "eqformat_pfs") (v "0.1.5") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0dl1m1x9zb5v6xhi4crklvhz89mzsvkw57pa89q0yrkvrqfp5kyr")))

(define-public crate-eqformat_pfs-0.1.6 (c (n "eqformat_pfs") (v "0.1.6") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "1c4138qzcm4cprqvnzlwy8fhxcrjjl58hzfbglxqw5x3mc74mvbx")))

(define-public crate-eqformat_pfs-0.2.0 (c (n "eqformat_pfs") (v "0.2.0") (d (list (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "036qsgmib0dfl06l1kx9abkhx83swx26ak0dmp0bj9hp32qp2wcc")))

(define-public crate-eqformat_pfs-0.2.1 (c (n "eqformat_pfs") (v "0.2.1") (d (list (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)))) (h "0vk3rvg0pixih007fb7pa2dl8y01a1bkr3bv0mb1q981ybv0dcdx")))

