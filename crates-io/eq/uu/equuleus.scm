(define-module (crates-io eq uu equuleus) #:use-module (crates-io))

(define-public crate-equuleus-0.1.0 (c (n "equuleus") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "serialport") (r "^3.1") (d #t) (k 0)))) (h "19b24rmmpjv235v1fyd52cagv4sa7pkh51nnjgnmbriwm37rxb4z")))

