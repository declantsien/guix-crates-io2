(define-module (crates-io eq eq eqeqo_server_base) #:use-module (crates-io))

(define-public crate-eqeqo_server_base-0.1.0 (c (n "eqeqo_server_base") (v "0.1.0") (h "05hbid8417cnhlsahg359if176d0bypljdlpq5l3p7w5m6yvmxy1")))

(define-public crate-eqeqo_server_base-0.1.1 (c (n "eqeqo_server_base") (v "0.1.1") (h "0vcgbq2mh5pg2wylxpkyf60jil7j4l3n40ky2j908jdg93b5rc6x")))

(define-public crate-eqeqo_server_base-0.1.2 (c (n "eqeqo_server_base") (v "0.1.2") (h "0dw7m7dbsnkklf4hq6pvgx0zs3021axzzi7lhxxc111lz148s56z")))

(define-public crate-eqeqo_server_base-0.1.3 (c (n "eqeqo_server_base") (v "0.1.3") (h "03wnl4mlg9mvdy5vaczwx4h6b79dp4v2r9zm24npf2hagagzbmj2")))

