(define-module (crates-io sk gi skgit) #:use-module (crates-io))

(define-public crate-skgit-0.1.0 (c (n "skgit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1m54zl0qbnzngbh2ziil1xqm8akbazmy8vjv121jdc5jb9l3w2lf")))

(define-public crate-skgit-0.2.0 (c (n "skgit") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "git2") (r "^0.15") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1kmk9ckw9pngm6azbssr61xbifbban6vbd96qsq5ay2m4kkl870s")))

