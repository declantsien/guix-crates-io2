(define-module (crates-io sk f- skf-rs) #:use-module (crates-io))

(define-public crate-skf-rs-0.1.0 (c (n "skf-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "skf-api") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0y7ksbddnznqxp1a51511p68s0i3d536ckvl8j2h2dqfb6qq822k")))

(define-public crate-skf-rs-0.2.0 (c (n "skf-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "172xqjlqsxbmgli3r5x99d5xjdc1qrjcz9pifzgvflggvirsy8vs")))

(define-public crate-skf-rs-0.3.0 (c (n "skf-rs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0zxiksy80f2cncckrss07h98rxkr8v9q9phjjm4j70ksdhmamfl0")))

(define-public crate-skf-rs-0.3.1 (c (n "skf-rs") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qh970ww9kqxxfh59128498dghqbf96vps49gy10fr3sndvf1hfh")))

(define-public crate-skf-rs-0.4.0 (c (n "skf-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1vy17ic2zdk691ynqys87pqicwwz1igr8cnzkv665ak28mcky2i1")))

(define-public crate-skf-rs-0.4.1 (c (n "skf-rs") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11hbwcdgn57r50d2f42acy7v0nx2r5w4c2lbr8xmbw369fpx1vl3")))

(define-public crate-skf-rs-0.5.0 (c (n "skf-rs") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0lw72fwkcs9vwbfcxf94jcyb9dyc65wk7sipmw3h68pxvdcc4pxy")))

(define-public crate-skf-rs-0.6.0 (c (n "skf-rs") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "skf-api") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1i0yav0d730wrm138ldqgqij7520mvpzw0pz57jxqx6y217r39kr")))

