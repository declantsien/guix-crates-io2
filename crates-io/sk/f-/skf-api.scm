(define-module (crates-io sk f- skf-api) #:use-module (crates-io))

(define-public crate-skf-api-0.2.0 (c (n "skf-api") (v "0.2.0") (h "0qxwx097m58wy08vv9456g4dzwpw8gq7xlavi6n0189d7ds84d3l")))

(define-public crate-skf-api-0.3.0 (c (n "skf-api") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0phqbkrp5i0fikbh2gbfk9i0smjg0mznx68xmzpx214g6gk0ix8a")))

(define-public crate-skf-api-0.4.0 (c (n "skf-api") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yjqlgbcg4dpzvkdhcbvrysskafiwah4x2iwqxx4775w39d6j8vx")))

(define-public crate-skf-api-0.5.0 (c (n "skf-api") (v "0.5.0") (h "06hdqfl3hkzs0dby59fq79vfjm1cy96xmsawabpwm593wfpyx6zg")))

