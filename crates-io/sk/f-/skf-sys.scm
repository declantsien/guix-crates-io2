(define-module (crates-io sk f- skf-sys) #:use-module (crates-io))

(define-public crate-skf-sys-0.2.0 (c (n "skf-sys") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "skf-api") (r "^0.2.0") (d #t) (k 0)))) (h "19lcg7gdbvzc3c36wa30gh9hrza2p7fa6bzl7b6zjq3bv22x98y7")))

