(define-module (crates-io sk y- sky-derive) #:use-module (crates-io))

(define-public crate-sky-derive-0.0.1 (c (n "sky-derive") (v "0.0.1") (h "19qg9imp1vvdp3f2gqsxp4sciv9lhxa732maj286dk5f1jdzjd9a")))

(define-public crate-sky-derive-0.1.0 (c (n "sky-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("derive"))) (d #t) (k 0)))) (h "11aqjhxb8ir0hl422ib6fwalbpd0l57xncxkd71bd64irihjgimf")))

(define-public crate-sky-derive-0.1.1 (c (n "sky-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("derive"))) (d #t) (k 0)))) (h "0avmilf0v6zylzqf48x234p0ychg5ddk9p4s27ywn02dsp8v4cg7")))

(define-public crate-sky-derive-0.2.0 (c (n "sky-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "083r0r14j8i6kp9pd647sbxx9f51626l8b7l859hhsnc8pfayjcm")))

(define-public crate-sky-derive-0.2.1 (c (n "sky-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16i96xdj49zhzsmh2icwdhsi7ww301gdfqvnd1xq46v8156hnvhp") (y #t)))

(define-public crate-sky-derive-0.2.2 (c (n "sky-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11r2wbsrccxhrfa0sh5y6siaiq3qgdvm39lvrwhhzyh13xm94pca")))

