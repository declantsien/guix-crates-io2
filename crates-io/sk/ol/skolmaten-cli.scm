(define-module (crates-io sk ol skolmaten-cli) #:use-module (crates-io))

(define-public crate-skolmaten-cli-0.1.0 (c (n "skolmaten-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06gff4qkarv4xmvcyh92dyfsg1wr9vsdsfwqb35kcw7qm78pddbh")))

(define-public crate-skolmaten-cli-0.1.1 (c (n "skolmaten-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.139") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9f1f9kr66g8j9sf02myx7cbvqnmik6pf6grvqqfvhzbhg7y1xc")))

