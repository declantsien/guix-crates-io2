(define-module (crates-io sk el skely) #:use-module (crates-io))

(define-public crate-skely-0.1.0 (c (n "skely") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1dqkvp96xgvshbxid89bj4g1ga5fv1fw36kilkkxbcbqxcjkgkbx")))

(define-public crate-skely-0.1.1 (c (n "skely") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0hd86f88c4j5ja4b9bqgks0i3vy7niclijsg034ffh39wx3fcmkc")))

(define-public crate-skely-0.1.2 (c (n "skely") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "12kkrcc0pwcnbimrn7vawpwnxrlnfq6sm4rhp7a85vgjxwf9dd7q")))

(define-public crate-skely-0.1.3 (c (n "skely") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "05w75sbkwjfbqga5jwbnz9nzi0l85vnzpsjryylngw63rjdlg6kj") (y #t)))

(define-public crate-skely-0.1.4 (c (n "skely") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0218n4mknx5hnpgpbcm076gfgwjyrfjkzcizw68kc268hmjlq93i")))

(define-public crate-skely-0.1.5 (c (n "skely") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "17aygavvdx37n36yv1glq1fs01gbbahlbp603la7sj5751r8a5w9")))

(define-public crate-skely-0.1.6 (c (n "skely") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "09rq2hik05lkwjqp0ybnsnv2f65jhz8csicsb9576aak5pjg6kxa")))

(define-public crate-skely-0.1.7 (c (n "skely") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)))) (h "0phzahs3y0q7dbp1qfrh8qysib0qawj67ljik1vqsamjnkmwdmsv")))

(define-public crate-skely-2.0.0 (c (n "skely") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "1bwnfw18payipncx99kzg6dkxdnf6p777y3aigl6z5b9l10bv98r")))

(define-public crate-skely-2.1.0 (c (n "skely") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)))) (h "0kmmfws1ihcbq4apz5ldshg4ffvy78iiz20s2yb9zj7y9w4fn60w")))

