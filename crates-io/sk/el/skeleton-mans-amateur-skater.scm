(define-module (crates-io sk el skeleton-mans-amateur-skater) #:use-module (crates-io))

(define-public crate-skeleton-mans-amateur-skater-0.0.0 (c (n "skeleton-mans-amateur-skater") (v "0.0.0") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "163nmmjlyhgsv171jbpsh8d8zl6l6ax9ad4scmn4p2l19hibi1rn")))

(define-public crate-skeleton-mans-amateur-skater-0.0.1 (c (n "skeleton-mans-amateur-skater") (v "0.0.1") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "0hmzgygn4dpfl7h6da9qypgx09543jb9aphn6j7hjj878r3ng7mm")))

(define-public crate-skeleton-mans-amateur-skater-0.0.2 (c (n "skeleton-mans-amateur-skater") (v "0.0.2") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "1bc4nriv4zk8ki7p4kg0kdyq2q94b2r622isl1yia2r23x8awmk2")))

(define-public crate-skeleton-mans-amateur-skater-0.0.3 (c (n "skeleton-mans-amateur-skater") (v "0.0.3") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "11y5d88kibhdxdi4q5da281q047rdsyksyb0x9l2w2656zbpyiwp")))

(define-public crate-skeleton-mans-amateur-skater-0.0.4 (c (n "skeleton-mans-amateur-skater") (v "0.0.4") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "0jclfi7wql8i6nzsxxlhx5av9lj3800y636q05vmg5f0a0f8b80g")))

(define-public crate-skeleton-mans-amateur-skater-0.0.5 (c (n "skeleton-mans-amateur-skater") (v "0.0.5") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "1bminr9r4rvb0s2gk05m22qp4vrmgiwnrcvfs9gp7f805a3402jm")))

(define-public crate-skeleton-mans-amateur-skater-0.0.6 (c (n "skeleton-mans-amateur-skater") (v "0.0.6") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "124w34y3130bz9sk8b8x759j7dr6cg4x01261v3s0cgaywlhs804")))

(define-public crate-skeleton-mans-amateur-skater-0.0.7 (c (n "skeleton-mans-amateur-skater") (v "0.0.7") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "1yrqaqsrqmln5vycg4lfzy2d5nkfhp44aa5f4570crbgc2vn6ffx")))

(define-public crate-skeleton-mans-amateur-skater-0.0.8 (c (n "skeleton-mans-amateur-skater") (v "0.0.8") (d (list (d (n "amethyst") (r "^0.10.0") (d #t) (k 0)))) (h "0b8n8rpyc3krpg8qk8zz4kjmz31i5iyhv8d5cbxpq6zm23ybfc8i")))

