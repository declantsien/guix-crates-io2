(define-module (crates-io sk el skeleton) #:use-module (crates-io))

(define-public crate-skeleton-0.1.0 (c (n "skeleton") (v "0.1.0") (d (list (d (n "clap") (r "2.22.*") (d #t) (k 0)) (d (n "hyper") (r "0.10.*") (d #t) (k 0)) (d (n "hyper-native-tls") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "0.3.*") (d #t) (k 0)))) (h "1hac072cmfy0prz0xznfxf11hnfxg0098bzwjwb9yp8avfiakn7n")))

(define-public crate-skeleton-0.2.0 (c (n "skeleton") (v "0.2.0") (d (list (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "02qj7i4faa65hffzbbz7r2asfz4ifjciv834qf3xdnjih1n7xkss")))

(define-public crate-skeleton-0.3.0 (c (n "skeleton") (v "0.3.0") (d (list (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 1)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "1cnhk4kv7fh7g95sv07jj6n6m1b0k65hmk9r3fapzmz7zsdz3yhh")))

(define-public crate-skeleton-0.3.1 (c (n "skeleton") (v "0.3.1") (d (list (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 1)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.3") (d #t) (k 0)))) (h "0whc39fx0ir7cjxb8r7ggyqm9wvsvnqwxjv06f317i7gvrmm55xq")))

(define-public crate-skeleton-0.3.3 (c (n "skeleton") (v "0.3.3") (d (list (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 1)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1xsgph0bpqldya5n8dz7qmp5gzx3gq500xmbg285lc45fdcn8g3k") (f (quote (("default"))))))

