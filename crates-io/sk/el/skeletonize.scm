(define-module (crates-io sk el skeletonize) #:use-module (crates-io))

(define-public crate-skeletonize-0.1.0 (c (n "skeletonize") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg" "png" "gif"))) (k 2)) (d (n "structopt") (r "^0.3") (k 2)))) (h "0g1ihnnc4x7mwr3p1rskdgbmvh3h5n8g6nx0xk9bzfwxpsynj53d")))

(define-public crate-skeletonize-0.2.0 (c (n "skeletonize") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png" "gif"))) (k 2)) (d (n "structopt") (r "^0.3") (k 2)))) (h "1smbs45fi0f5h64gi0grjpi6k5zd0knfqm40lhysv56538gnwh1w")))

