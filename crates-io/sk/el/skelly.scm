(define-module (crates-io sk el skelly) #:use-module (crates-io))

(define-public crate-skelly-0.1.0 (c (n "skelly") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.0-alpha.15") (d #t) (k 2)) (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "0fmf7iw1j29gz7h1x0v1pqnr7ppdffqj2mfaa2a6l408fr5ws10q") (f (quote (("ik"))))))

(define-public crate-skelly-0.1.1 (c (n "skelly") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.0-alpha.15") (d #t) (k 2)) (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "10f6xgs7wv57vjvaky0g84a5axs1yq7fjc9r0sxcxg7mcnhjssqg") (f (quote (("ik"))))))

(define-public crate-skelly-0.1.2 (c (n "skelly") (v "0.1.2") (d (list (d (n "macroquad") (r "^0.3.0-alpha.15") (d #t) (k 2)) (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "0hsfgzwbii47522r3jc178fn52zj2rqxlv3s70870khv1pvn8v5k") (f (quote (("ik"))))))

(define-public crate-skelly-0.1.3 (c (n "skelly") (v "0.1.3") (d (list (d (n "macroquad") (r "^0.3.0-alpha.15") (d #t) (k 2)) (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "1ax8djjl5kahlm9lyir6fvj0w0j2bnzh6g8dgxz713k8m02afzxh") (f (quote (("ik"))))))

(define-public crate-skelly-0.2.0 (c (n "skelly") (v "0.2.0") (d (list (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "0ndv23rcbhxjdcfm3prn1bnmgaiaaq4mgzqw38c1bcag6av14pmc") (f (quote (("ik"))))))

(define-public crate-skelly-0.3.0 (c (n "skelly") (v "0.3.0") (d (list (d (n "na") (r "^0.25") (d #t) (k 0) (p "nalgebra")))) (h "1rsz7iab7rnhc8j8bggzvab5pvd8pnfkv7ir1b4wy89frhbr3cr7") (f (quote (("ik"))))))

(define-public crate-skelly-0.4.0 (c (n "skelly") (v "0.4.0") (d (list (d (n "na") (r "^0.28") (d #t) (k 0) (p "nalgebra")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19higfjm0zc6vxyszm6bx48yz6ndij8zkz0x2mzl4d461z2i18mw") (f (quote (("serde-1" "serde" "na/serde-serialize") ("ik"))))))

