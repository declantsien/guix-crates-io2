(define-module (crates-io sk el skellige) #:use-module (crates-io))

(define-public crate-skellige-0.0.1 (c (n "skellige") (v "0.0.1") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "1d4l3ixvnb9z24ax920kxdvyvbx3gzn2540x30jzizvqsl6q515v")))

(define-public crate-skellige-0.1.1 (c (n "skellige") (v "0.1.1") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "1ly4wm67rd4fhyn428qizalicqykxzynrmbs2nsqkzga69fc291a")))

(define-public crate-skellige-0.1.2 (c (n "skellige") (v "0.1.2") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "103z8z8k7cn99181ppjinjjlyjjbch9qmyxm6hd75mb8f2d1ga03")))

(define-public crate-skellige-0.1.3 (c (n "skellige") (v "0.1.3") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "092fsnkibd2f31dbm72qzgik2y67n3g8bzi85lvjnv5w1r627gsj")))

(define-public crate-skellige-0.1.4 (c (n "skellige") (v "0.1.4") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "0yihz7wv71xp9mag4qn0qy5378w6sk13q0019ijcckslikr5c373")))

(define-public crate-skellige-0.1.5 (c (n "skellige") (v "0.1.5") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "1cl2jrcfng1s52xpxxdk38ckrpyqsx5mlcfr01i4kvrb2f5b5ddq")))

(define-public crate-skellige-0.1.6 (c (n "skellige") (v "0.1.6") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "09m2d3ygsp9mi7gy3d7d2z7i9rl7rp2qfn6pjlsxpj43nc1k12wg")))

(define-public crate-skellige-0.1.7 (c (n "skellige") (v "0.1.7") (d (list (d (n "fungus") (r "0.1.*") (d #t) (k 0)) (d (n "git2") (r "0.13.*") (d #t) (k 0)) (d (n "indicatif") (r "0.15.*") (d #t) (k 0)))) (h "0hnmldnmf8faff83gvx6qvc5dca88n6q0a3l1q1pr7s76l8vwfs7")))

