(define-module (crates-io sk s- sks-ssh2-config) #:use-module (crates-io))

(define-public crate-sks-ssh2-config-0.1.0 (c (n "sks-ssh2-config") (v "0.1.0") (h "1x4j34gb29q5mvk3j1nzd1jrj0yjb2rcl0q188b5537y6y44qnxk")))

(define-public crate-sks-ssh2-config-0.1.1 (c (n "sks-ssh2-config") (v "0.1.1") (h "0hy5f51iwbzcxdx2xfd9vvnm934wyjszdkczxzz4hx8s0bk1m2sz")))

