(define-module (crates-io sk yl skyline_macro) #:use-module (crates-io))

(define-public crate-skyline_macro-0.1.0 (c (n "skyline_macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09kpza9dxb4sgkkjqw8fcv3yn566sng58ifajvbzi78hxf5ivxd0")))

(define-public crate-skyline_macro-0.2.0 (c (n "skyline_macro") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p24i02mqkmb2v276clvayska2hnas5rpzcw57ny47bww47b554i") (f (quote (("std") ("nso"))))))

