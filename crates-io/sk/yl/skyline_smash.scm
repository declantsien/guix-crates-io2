(define-module (crates-io sk yl skyline_smash) #:use-module (crates-io))

(define-public crate-skyline_smash-0.1.0 (c (n "skyline_smash") (v "0.1.0") (d (list (d (n "compile-time-lua-bind-hash") (r "^1.1") (d #t) (k 0)) (d (n "libc-nnsdk") (r "^0.0.1") (d #t) (k 0)))) (h "0z543l7xqsz1b293aki0p1jr3nap9czf72mm3bmhhbxpx9glfrmq")))

