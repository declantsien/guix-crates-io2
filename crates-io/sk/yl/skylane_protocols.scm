(define-module (crates-io sk yl skylane_protocols) #:use-module (crates-io))

(define-public crate-skylane_protocols-0.0.1 (c (n "skylane_protocols") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "skylane") (r "^0.0.1") (d #t) (k 0)) (d (n "skylane_scanner") (r "^0.0.1") (d #t) (k 1)))) (h "07smn4d4wwh1s4mc2hyaq0qg3br72hgxqhgxfn78bi1dyc2d1b93")))

(define-public crate-skylane_protocols-0.1.0 (c (n "skylane_protocols") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "skylane") (r "^0.1.0") (d #t) (k 0)) (d (n "skylane_scanner") (r "^0.1.0") (d #t) (k 1)))) (h "03w95j86ijl93pw6a5rmwzh3vdwaqnj17mpnc4lf90sb8nqh59sq")))

(define-public crate-skylane_protocols-0.1.1 (c (n "skylane_protocols") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "skylane") (r "^0.1") (d #t) (k 0)) (d (n "skylane_scanner") (r "^0.1") (d #t) (k 1)))) (h "13xi15rns4ac3qv4d6lshg59s9fkzjh549s6zq6sdm1mvbp4q2kg")))

(define-public crate-skylane_protocols-0.1.2 (c (n "skylane_protocols") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "skylane") (r "^0.1") (d #t) (k 0)) (d (n "skylane_scanner") (r "^0.1") (d #t) (k 1)))) (h "1d6hbqgbdihq0vbd30y218rmanb70315smk63i6aa1rz436ccr42")))

