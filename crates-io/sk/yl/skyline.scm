(define-module (crates-io sk yl skyline) #:use-module (crates-io))

(define-public crate-skyline-0.1.0 (c (n "skyline") (v "0.1.0") (d (list (d (n "libc-nnsdk") (r "^0.0.1") (d #t) (k 0)) (d (n "nnsdk") (r "^0.1") (d #t) (k 0)) (d (n "skyline_macro") (r "^0.1") (d #t) (k 0)))) (h "1h04419qnmhv8bfiwzs2i5qndnldjaai873w0blpwv4qvdg1w9fr") (f (quote (("std") ("nro_internal") ("default" "std"))))))

(define-public crate-skyline-0.2.0 (c (n "skyline") (v "0.2.0") (d (list (d (n "libc-nnsdk") (r "^0.2.0") (d #t) (k 0)) (d (n "nnsdk") (r "^0.2.0") (d #t) (k 0)) (d (n "skyline_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1kldbin8rrsff43xi6y30bc5vk9kmb7isfvckm6ks4bmmas6i4zh") (f (quote (("std" "skyline_macro/std") ("nso" "skyline_macro/nso") ("nro_internal") ("default" "std")))) (y #t)))

(define-public crate-skyline-0.2.1 (c (n "skyline") (v "0.2.1") (d (list (d (n "libc-nnsdk") (r "^0.2.0") (d #t) (k 0)) (d (n "nnsdk") (r "^0.2.0") (d #t) (k 0)) (d (n "skyline_macro") (r "^0.2.0") (d #t) (k 0)))) (h "1b82b3yghvbwriczrawr0q9q1sxf2vgj79asfpsg7kwhjh9gql4r") (f (quote (("std" "skyline_macro/std") ("nso" "skyline_macro/nso") ("nro_internal") ("default" "std"))))))

