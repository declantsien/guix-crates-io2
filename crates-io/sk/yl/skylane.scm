(define-module (crates-io sk yl skylane) #:use-module (crates-io))

(define-public crate-skylane-0.0.1 (c (n "skylane") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "08539npnsbb1byvbd3lh7lvhn0k8pc0pzbjw4c4sfyys327qr9fl")))

(define-public crate-skylane-0.1.0 (c (n "skylane") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "09mxa3vqbir3an7jhw5nmw1qvj67d6gh8lcmwq3lxsg4qni8a6gq")))

(define-public crate-skylane-0.1.1 (c (n "skylane") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "1v7pplgrlr3fcdqjdya46h88x12zw33x4wkx4v0pbhxs3gmxiz6a")))

(define-public crate-skylane-0.1.2 (c (n "skylane") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.8") (d #t) (k 0)))) (h "08nx1kwsra1pvjj8yh8ab0shfir53n08xswfsl93v0im50rarw2v")))

