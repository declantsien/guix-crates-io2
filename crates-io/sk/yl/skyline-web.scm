(define-module (crates-io sk yl skyline-web) #:use-module (crates-io))

(define-public crate-skyline-web-0.1.0 (c (n "skyline-web") (v "0.1.0") (d (list (d (n "nnsdk") (r "^0.2.0") (d #t) (k 0)) (d (n "ramhorns") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "skyline") (r "^0.2.1") (d #t) (k 0)))) (h "14i8l2rpymk7d6bw7qw6m3v8llwzyda0k4p26by8nvnkkx2kmj33") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

