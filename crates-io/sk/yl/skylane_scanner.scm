(define-module (crates-io sk yl skylane_scanner) #:use-module (crates-io))

(define-public crate-skylane_scanner-0.0.1 (c (n "skylane_scanner") (v "0.0.1") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "06w4rmjlxyvyg082x0nbdp2cr38vlxr1dmcbnd9xmy43kzdrl729")))

(define-public crate-skylane_scanner-0.1.0 (c (n "skylane_scanner") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0y19izrahdahr082dkrkiy2fbr4ngrmb3cdbrqs4rqxvx5gpiil7")))

(define-public crate-skylane_scanner-0.1.1 (c (n "skylane_scanner") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "11ml4qsrcwjm5lbr67bsvz0j820rxsmcfyfpwn4pp6iadq92xwcx")))

