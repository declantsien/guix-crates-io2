(define-module (crates-io sk ua skua-voice-model) #:use-module (crates-io))

(define-public crate-skua-voice-model-0.2.0 (c (n "skua-voice-model") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0xrmmq33j3qa6vxyn4713bwrdy159q3vnxs1y2ndljh4i01rj7f5")))

