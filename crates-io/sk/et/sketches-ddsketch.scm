(define-module (crates-io sk et sketches-ddsketch) #:use-module (crates-io))

(define-public crate-sketches-ddsketch-0.1.0 (c (n "sketches-ddsketch") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "082kmv5fz4xvhbx9cbz2dscsgg10xnlv1nnz4d3hbckmkc9d16ly")))

(define-public crate-sketches-ddsketch-0.1.1 (c (n "sketches-ddsketch") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "1qdw02rc3dld6yjfiazgxfva5g2ds99dzchwc6zx2zakajpmzxvn")))

(define-public crate-sketches-ddsketch-0.1.2 (c (n "sketches-ddsketch") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "0yq1rg6brfrkq0zbx79jcmnd3mk9wlh0gsp70l7h31iqv67pm9vn")))

(define-public crate-sketches-ddsketch-0.1.3 (c (n "sketches-ddsketch") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "1vk4fa3adwcx0dqqrcz3vp955wfcaw555gg6w8ib2cygbypfrlh4")))

(define-public crate-sketches-ddsketch-0.2.0 (c (n "sketches-ddsketch") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)))) (h "1rwsvarn13w8mnibl2slzk348l5xa5w2gwg4v11rrq1887jlbfff")))

(define-public crate-sketches-ddsketch-0.2.1 (c (n "sketches-ddsketch") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "serde_derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "1q873ja2yvvls9327a7yw1mcprw0ia2cjj72snfg5mrfi30hd938") (f (quote (("use_serde" "serde" "serde/derive"))))))

(define-public crate-sketches-ddsketch-0.2.2 (c (n "sketches-ddsketch") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "serde_derive"))) (o #t) (d #t) (k 0) (p "serde")))) (h "0p6n1v0p0773d0b5qnsnw526g7hhlb08bx95wm0zb09xnwa6qqw5") (f (quote (("use_serde" "serde" "serde/derive"))))))

