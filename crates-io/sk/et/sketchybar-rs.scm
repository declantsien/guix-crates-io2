(define-module (crates-io sk et sketchybar-rs) #:use-module (crates-io))

(define-public crate-sketchybar-rs-0.1.0 (c (n "sketchybar-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "05724liksxmm0l82znf5hq85hdnis0w9i9fqnr7iawwm3a4cnv62")))

(define-public crate-sketchybar-rs-0.2.0 (c (n "sketchybar-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "0ksk3brn53gdjgk5rsdlyydmrvzdwsjw9v3xm3ym9j6kwxsbwk5k")))

