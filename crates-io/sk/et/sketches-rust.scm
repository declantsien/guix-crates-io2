(define-module (crates-io sk et sketches-rust) #:use-module (crates-io))

(define-public crate-sketches-rust-0.1.0 (c (n "sketches-rust") (v "0.1.0") (h "0i96xw31j3r31bqkfxdmzxajzc0jzh4yxzbzkibq07qmfrdd2l2f")))

(define-public crate-sketches-rust-0.1.1 (c (n "sketches-rust") (v "0.1.1") (h "0sgy954w4l43khy37csqw9jnwv9b14sdzrnyp27j4rwcxvz1a6x0")))

(define-public crate-sketches-rust-0.1.2 (c (n "sketches-rust") (v "0.1.2") (h "1sbz5vsvjx4nqzrznz0ww2n2rr7hirpxklf5gd7ifdig7zmin0vx")))

(define-public crate-sketches-rust-0.1.4 (c (n "sketches-rust") (v "0.1.4") (h "14cl5zir3fxdmvpbb31r57c6jip4j792fmlkk4w3wc9mc800vckq") (y #t)))

(define-public crate-sketches-rust-0.1.5 (c (n "sketches-rust") (v "0.1.5") (h "0pggsnqn2frggymsbymi4aknbpm0n953axzswh37zb0n778h3a8m")))

(define-public crate-sketches-rust-0.2.0 (c (n "sketches-rust") (v "0.2.0") (h "143jhhahx1wc5vpvyhsxiwvpv7475z0zsa6bsgy893nkgbcnpx3n")))

(define-public crate-sketches-rust-0.2.1 (c (n "sketches-rust") (v "0.2.1") (h "1pkf1xcksj7zgcfgcgn5bjfa83fizkmd3xiiwrp9px414ws84fjs")))

(define-public crate-sketches-rust-0.2.2 (c (n "sketches-rust") (v "0.2.2") (d (list (d (n "rust-strictmath") (r "^0.1.1") (d #t) (k 0)))) (h "0933ln5w0i0kr9qz7hk8zl6lanz0jdkrhsr2xm294f0vvm0fhxgd")))

