(define-module (crates-io sk et sketch-2d) #:use-module (crates-io))

(define-public crate-sketch-2d-0.1.0 (c (n "sketch-2d") (v "0.1.0") (d (list (d (n "fn-store") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "unsized-stack") (r "^0.2.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.16.1") (d #t) (k 0)))) (h "06l2wwr18v2k8f92fwp01h0731jhq4m0s8sb68g980xrg941qrk5")))

