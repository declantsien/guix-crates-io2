(define-module (crates-io sk et sketchy-rs) #:use-module (crates-io))

(define-public crate-sketchy-rs-0.4.0 (c (n "sketchy-rs") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)))) (h "0k68nrw98k3qk17r6m3kq1m8lnycfby3ysynsi5zlabab0grvl16")))

(define-public crate-sketchy-rs-0.4.3 (c (n "sketchy-rs") (v "0.4.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)))) (h "0cywbqx2gwmdrhxwn270j2f4pqbv640zp4xsw9jhxjvqx218z6dp")))

(define-public crate-sketchy-rs-0.4.4 (c (n "sketchy-rs") (v "0.4.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)))) (h "0vmqy74xqhrvfda74infxrhl2yzs5zrf96f8qaw7hzij23bfn36i")))

(define-public crate-sketchy-rs-0.5.0 (c (n "sketchy-rs") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jbygl3xdywc7qrw100jwy52ijqdjfy9s38mxp1n1am91035cnvx")))

(define-public crate-sketchy-rs-0.6.0 (c (n "sketchy-rs") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "finch") (r "^0.4.1") (d #t) (k 0)) (d (n "needletail") (r "^0.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jg3iq58xw0s6y6v4wv45v4c8ykj9y01jwwjsfriq1wcr7389v2m")))

