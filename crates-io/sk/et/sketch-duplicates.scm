(define-module (crates-io sk et sketch-duplicates) #:use-module (crates-io))

(define-public crate-sketch-duplicates-0.1.0 (c (n "sketch-duplicates") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "human-size") (r "^0.4.1") (d #t) (k 0)) (d (n "metrohash") (r "^1.0.6") (d #t) (k 0)) (d (n "proptest") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1gm7am3cdd1zp8zzq85h9k3ddffnsymbl5hqk0vnr3r02pq4xai3")))

