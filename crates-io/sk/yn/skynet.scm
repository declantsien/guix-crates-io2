(define-module (crates-io sk yn skynet) #:use-module (crates-io))

(define-public crate-skynet-0.1.0 (c (n "skynet") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0p66187xv599wfwc4asc6wfd6x2wzbph50h84dnrm232qchg3qpk")))

(define-public crate-skynet-0.1.1 (c (n "skynet") (v "0.1.1") (h "0gzm9a6pjpl6ccwp3l6kzglz4ngm89jfqb6yzsrc3g2n18ny515m")))

