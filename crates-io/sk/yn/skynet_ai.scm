(define-module (crates-io sk yn skynet_ai) #:use-module (crates-io))

(define-public crate-skynet_ai-0.0.1 (c (n "skynet_ai") (v "0.0.1") (h "04pr6iqvqsc8j686a02n5k5mfz8cq2wmhdlkx3h1fvmlf7rd19rk")))

(define-public crate-skynet_ai-0.1.0 (c (n "skynet_ai") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ibck7ghg55sxa3klk4x6l2sgphm3jm6pilsd8fhmd92012jh8qj")))

(define-public crate-skynet_ai-0.1.1 (c (n "skynet_ai") (v "0.1.1") (d (list (d (n "flume") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1mpm3qhx0qk0f174ksmm34zfjp8dmlnl8h2p6ff25j4j99903g88") (f (quote (("parallel" "rayon" "flume"))))))

(define-public crate-skynet_ai-0.1.2 (c (n "skynet_ai") (v "0.1.2") (d (list (d (n "flume") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1phnrl3086ivqycpky33zbg4y7ifx5p8sjfjg9yhzn9fqv58j45y") (f (quote (("parallel" "rayon" "flume"))))))

