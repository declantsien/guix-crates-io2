(define-module (crates-io sk yn skynet-rs) #:use-module (crates-io))

(define-public crate-skynet-rs-0.1.0 (c (n "skynet-rs") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "http2"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "textnonce") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("rt" "macros"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zarwzvrp705zws6h0nz34nid4ywgh9ybcw95mz5wwwxc385y6nl")))

