(define-module (crates-io sk ar skar-net-types) #:use-module (crates-io))

(define-public crate-skar-net-types-0.0.1 (c (n "skar-net-types") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.0.1") (d #t) (k 0)))) (h "0fyyy8izwf3fwijrwqdhipnp7df7n4hpwvvnvpn1vf8j2prjap13")))

(define-public crate-skar-net-types-0.1.0 (c (n "skar-net-types") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.1.0") (d #t) (k 0)))) (h "02jpj2q75an7c44l5jri1plxa764zysyhji0zapxpbq6ph974xa4")))

(define-public crate-skar-net-types-0.1.1 (c (n "skar-net-types") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.2.0") (d #t) (k 0)))) (h "06x2ig7p57wqwa8mcdcfxsypcx4kb46g8g77rsy8cw5xqfd93pip")))

(define-public crate-skar-net-types-0.1.2 (c (n "skar-net-types") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.2.0") (d #t) (k 0)))) (h "1kyvwvhnks0p1alwy2y4l72jiapa5nvb4qn747x2kh6p6w0904l1")))

(define-public crate-skar-net-types-0.1.3 (c (n "skar-net-types") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.2.0") (d #t) (k 0)))) (h "0lih9112fd6p1qz7d60mgv3ma8n45vxvh39a1g76m6hn4nf8fxzy")))

(define-public crate-skar-net-types-0.1.4 (c (n "skar-net-types") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.2.0") (d #t) (k 0)))) (h "1i5bk6ijam0qvhv2033i3n72wkzn92v6mshr1y0i9zqcx03i3wd1")))

(define-public crate-skar-net-types-0.2.0 (c (n "skar-net-types") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.2.0") (d #t) (k 0)))) (h "0a9nmgccfzl4j8klibnq0cdyjwnqabm5p7gmxkd0f1k4a1w4i30f")))

(define-public crate-skar-net-types-0.3.0 (c (n "skar-net-types") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.3") (d #t) (k 0)))) (h "0x2cgg7xms60r3ib38i3s0qvw65b094vhz12h955fab3316nlng3")))

(define-public crate-skar-net-types-0.3.1 (c (n "skar-net-types") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.19") (d #t) (k 0)) (d (n "capnpc") (r "^0.19") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.3") (d #t) (k 0)))) (h "10rcmgy364gz20a67fg0pvmcnw32081is2p9f8hcnlyfrd2vpr2b")))

