(define-module (crates-io sk ar skar-schema) #:use-module (crates-io))

(define-public crate-skar-schema-0.1.0 (c (n "skar-schema") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "1j4dhl2ryr2kxcw0syvhnrw71vhd4iwwqx1f4p91cfg1iy76dfab")))

(define-public crate-skar-schema-0.1.1 (c (n "skar-schema") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "0j7m4dvwiqyy692i3w1mzpz65qsnj72v2acxsnpd6h9gyd4cjvhf")))

(define-public crate-skar-schema-0.2.0 (c (n "skar-schema") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "polars-arrow") (r "^0.38") (d #t) (k 0)))) (h "1bb5f8apxj5rixjbk7szay1vd8fhn36w3l40q8ssj0lwm4c8jq4k")))

(define-public crate-skar-schema-0.3.0 (c (n "skar-schema") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "polars-arrow") (r "^0.38") (d #t) (k 0)))) (h "012rwsz2j8xc41apmh21dm2wl5k1vifykl09wbpf0843icgx2jkv")))

(define-public crate-skar-schema-0.3.1 (c (n "skar-schema") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "polars-arrow") (r "^0.39") (d #t) (k 0)))) (h "02fbz279qghxkdn4d5az8sp1kcl2mhhn9db0xxc7pj2qy8hhsc1w")))

