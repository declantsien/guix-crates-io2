(define-module (crates-io sk ar skar-net-types-fuel) #:use-module (crates-io))

(define-public crate-skar-net-types-fuel-0.0.1 (c (n "skar-net-types-fuel") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.0.1") (d #t) (k 0) (p "skar-format-fuel")))) (h "0kpm5rk20r4bifggkjqik6xqnnzvbf49q0bswmir6r5dpgmhhszb")))

(define-public crate-skar-net-types-fuel-0.0.2 (c (n "skar-net-types-fuel") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.0.2") (d #t) (k 0) (p "skar-format-fuel")))) (h "1vns9jakia341c16016z0kdci69skr35ijwdknlp6n0rwf30w47v")))

(define-public crate-skar-net-types-fuel-0.0.3 (c (n "skar-net-types-fuel") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "capnp") (r "^0.18") (d #t) (k 0)) (d (n "capnpc") (r "^0.18") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skar-format") (r "^0.0.4") (d #t) (k 0) (p "skar-format-fuel")))) (h "0xnzhgv8wpsa1kzshql3jffivr09khd5jizqaf6nhzvyaywlrcvd")))

