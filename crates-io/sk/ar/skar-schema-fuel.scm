(define-module (crates-io sk ar skar-schema-fuel) #:use-module (crates-io))

(define-public crate-skar-schema-fuel-0.0.1 (c (n "skar-schema-fuel") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "0yg9c6is1ari4vj55y6pdn31p3s8vhbg52bj0vqr0kmqb07xv1sw")))

(define-public crate-skar-schema-fuel-0.0.2 (c (n "skar-schema-fuel") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arrow2") (r "^0.18") (f (quote ("compute_concatenate"))) (k 0)))) (h "05rinqw2msdy7kr04likwykccy4f78fb6vil21jcx85vb7rh8pby")))

