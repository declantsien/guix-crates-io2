(define-module (crates-io sk ul skulpin-renderer-sdl2) #:use-module (crates-io))

(define-public crate-skulpin-renderer-sdl2-0.1.0 (c (n "skulpin-renderer-sdl2") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.1") (d #t) (k 0)))) (h "0zmsyn8ahlz3fvx81fwj53qmlxkj9m2x7mzmf9328kp18xdqqz5p")))

(define-public crate-skulpin-renderer-sdl2-0.2.0 (c (n "skulpin-renderer-sdl2") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.2") (d #t) (k 0)))) (h "060c5q7f222c5f303v1xdvsy7r6566k0zjd8yxw5dcgl1j0nf8jd")))

(define-public crate-skulpin-renderer-sdl2-0.3.0 (c (n "skulpin-renderer-sdl2") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "14n54b7ihlafqf9c4hbijshnds6211ylcy437ga1mxlxagn2hq3n")))

(define-public crate-skulpin-renderer-sdl2-0.3.1 (c (n "skulpin-renderer-sdl2") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "0d80wk2rnk2nvcp06n1gidpmsdw55b5yqb54lib9frqqgdw74sh3")))

(define-public crate-skulpin-renderer-sdl2-0.3.2 (c (n "skulpin-renderer-sdl2") (v "0.3.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "11vzc6bzjlsrxv3qbvg847p9d73fggcgf8yb25n7yqdm4gyw4gkd")))

(define-public crate-skulpin-renderer-sdl2-0.3.3 (c (n "skulpin-renderer-sdl2") (v "0.3.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">= 0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "0b1ny83hj1jbpghbxqks91gfb4zc4jsqd4097mxbgx1cn1dc47m5")))

(define-public crate-skulpin-renderer-sdl2-0.4.0 (c (n "skulpin-renderer-sdl2") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.33") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.4") (d #t) (k 0)))) (h "0g95y9ilb4mk2zp6c63wvr4lk6586j7ws1qnb8nfzi68nrciqybf")))

(define-public crate-skulpin-renderer-sdl2-0.5.0 (c (n "skulpin-renderer-sdl2") (v "0.5.0") (d (list (d (n "ash-window") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r ">=0.33") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.5") (d #t) (k 0)))) (h "0dprmcjrmvgijvq8pvlgsrfmdw6a5smwg82zx7cslqnb3wx53493")))

(define-public crate-skulpin-renderer-sdl2-0.4.1 (c (n "skulpin-renderer-sdl2") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r ">=0.33, <=0.34.2") (d #t) (k 0)) (d (n "sdl2-sys") (r ">=0.33, <=0.34.2") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.4") (d #t) (k 0)))) (h "0v08z54yip5wxni4zmd68a3hrdngzissi6djr3k73c5alnm277si")))

(define-public crate-skulpin-renderer-sdl2-0.5.1 (c (n "skulpin-renderer-sdl2") (v "0.5.1") (d (list (d (n "ash-window") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r ">=0.33, <=0.34.2") (f (quote ("raw-window-handle"))) (d #t) (k 0)) (d (n "sdl2-sys") (r ">=0.33, <=0.34.2") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.5.1") (d #t) (k 0)))) (h "1hax9vfmvy5ahs3vcfdigqlf39iflw9c6pdi7r456rxy18dvrmx5")))

