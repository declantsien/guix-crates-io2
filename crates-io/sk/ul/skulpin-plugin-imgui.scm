(define-module (crates-io sk ul skulpin-plugin-imgui) #:use-module (crates-io))

(define-public crate-skulpin-plugin-imgui-0.2.0 (c (n "skulpin-plugin-imgui") (v "0.2.0") (d (list (d (n "imgui") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.2") (d #t) (k 0)))) (h "0nsgjsw17mv9as70w3j8n6pgbsknq8zl54h04bzq9fwf5h2dl267")))

(define-public crate-skulpin-plugin-imgui-0.3.0 (c (n "skulpin-plugin-imgui") (v "0.3.0") (d (list (d (n "imgui") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "0mznb3dvmrqsankn8lbxhkrqqglv19hhmw9pvgvapivpq1cffdpy")))

(define-public crate-skulpin-plugin-imgui-0.4.0 (c (n "skulpin-plugin-imgui") (v "0.4.0") (d (list (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.3") (d #t) (k 0)))) (h "1mlyqyk5dxqa7ihscildhcnnx79wa90r0gjl54194f7gfvp9j81y")))

(define-public crate-skulpin-plugin-imgui-0.5.0 (c (n "skulpin-plugin-imgui") (v "0.5.0") (d (list (d (n "imgui") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.4") (d #t) (k 0)))) (h "1yyvsk3yddks23nv0vzfk7w01j9jmfcn9mxii8lsbf674ixs39r9")))

(define-public crate-skulpin-plugin-imgui-0.6.0 (c (n "skulpin-plugin-imgui") (v "0.6.0") (d (list (d (n "imgui") (r ">=0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.4") (d #t) (k 0)))) (h "0jzhsizc516k4ayr7fwl2fgx83vzmrdlyr5w7l5q2c6bds135ccp")))

(define-public crate-skulpin-plugin-imgui-0.7.0 (c (n "skulpin-plugin-imgui") (v "0.7.0") (d (list (d (n "imgui") (r ">=0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.5") (d #t) (k 0)))) (h "13y8qpij1x74zyj33sakmpw50cns6gqc8ryd9cjxqmplal8rrdda")))

(define-public crate-skulpin-plugin-imgui-0.7.1 (c (n "skulpin-plugin-imgui") (v "0.7.1") (d (list (d (n "imgui") (r ">=0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "skulpin-renderer") (r "^0.5.1") (d #t) (k 0)))) (h "01ysys2sz45rgmd9jzw01drhxgyhzfp2pcd24vn8kwmr7n7bypjf")))

