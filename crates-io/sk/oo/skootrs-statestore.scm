(define-module (crates-io sk oo skootrs-statestore) #:use-module (crates-io))

(define-public crate-skootrs-statestore-0.1.0 (c (n "skootrs-statestore") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "skootrs-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "skootrs-model") (r "^0.1.0") (d #t) (k 0)))) (h "0l4gd022ghwkvmn7jpsjchfmx0zkqi0713blnxbnl8vajmz8f0nz")))

