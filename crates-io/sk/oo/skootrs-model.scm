(define-module (crates-io sk oo skootrs-model) #:use-module (crates-io))

(define-public crate-skootrs-model-0.1.0 (c (n "skootrs-model") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regress") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (f (quote ("chrono" "url"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "strum") (r "^0.26.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "utoipa") (r "^4.1.0") (d #t) (k 0)))) (h "056y1rivqr0vmp12w4zyzhxjy54s0l2qi2553yqpxik05whsy4f7") (f (quote (("openapi"))))))

