(define-module (crates-io sk il skillratings) #:use-module (crates-io))

(define-public crate-skillratings-0.1.0 (c (n "skillratings") (v "0.1.0") (h "1fpm6k8w2d914c4pvd44fmkbzpmbnrr13can4g2s120911zaz7jl")))

(define-public crate-skillratings-0.2.0 (c (n "skillratings") (v "0.2.0") (h "0bxr3m2id6cxqxvwpf6pywvgmcnl9am3g6dv7r9jwqk46s5a0ivi")))

(define-public crate-skillratings-0.3.0 (c (n "skillratings") (v "0.3.0") (h "0ip2ggiv5lw6a09a6slwhccxzg6prz47mc1pzmkp057p8cg516lx")))

(define-public crate-skillratings-0.3.1 (c (n "skillratings") (v "0.3.1") (h "1xlqvxpnyw4l1ijg54gan378k8q3m6gv9bl9k08gp9isycjhibz5")))

(define-public crate-skillratings-0.4.0 (c (n "skillratings") (v "0.4.0") (h "1f9wwynsziy0vyam9117dz85fak61ays67fqpwj7a0fhlpmxpx39")))

(define-public crate-skillratings-0.4.1 (c (n "skillratings") (v "0.4.1") (h "1mwzpjkcgw2f6fb6da0nyhsqjbalwplmmgjlkx0pgnyf26yicz4v")))

(define-public crate-skillratings-0.5.0 (c (n "skillratings") (v "0.5.0") (h "1zikaydq634qi2lg5rjmjx0im9w25rcgmilkmp9xbmi97zpxcg8j")))

(define-public crate-skillratings-0.6.0 (c (n "skillratings") (v "0.6.0") (h "09x23m4i98k2ljaxb0ym2h4ziskxwgp02jh169mxhbbpfnr1wmxb")))

(define-public crate-skillratings-0.7.0 (c (n "skillratings") (v "0.7.0") (h "1kjkkw3vq8ag4kcrisgnv3sl7g8sazz5j6cbsi5ih1ahpii3sdyq")))

(define-public crate-skillratings-0.7.1 (c (n "skillratings") (v "0.7.1") (h "17h839pq2ks04g07bzp9aq2zmhiqq6cwv87n2s7yciks5dbd16jq")))

(define-public crate-skillratings-0.7.2 (c (n "skillratings") (v "0.7.2") (h "00kqxm69qbjq9w7csf3bljxnc3gqq878bipka20q7dmymq9zcjc7")))

(define-public crate-skillratings-0.8.0 (c (n "skillratings") (v "0.8.0") (h "131wsj2yn3ksfhqiqx9jcksyaxpiwczc729nxsck0131rsryx8q2")))

(define-public crate-skillratings-0.9.0 (c (n "skillratings") (v "0.9.0") (h "0bl36dqg711wigh8hjdpxkzdxhcp2nlm58b92fcnkzqas15mv6q2")))

(define-public crate-skillratings-0.9.1 (c (n "skillratings") (v "0.9.1") (h "0bh88ak2qydlyihbflwihvvgxpdcm14fpknbgbpkbnby99qjr6jm")))

(define-public crate-skillratings-0.9.2 (c (n "skillratings") (v "0.9.2") (h "1m9v60f5avyf3s9bcllzqxiv89sg0fja65vis01n9splx1qgy9lb")))

(define-public crate-skillratings-0.10.0 (c (n "skillratings") (v "0.10.0") (h "03i28lry76kk8zbspxr05f3255wilyyrw60cnqq49s266spmfwcy")))

(define-public crate-skillratings-0.10.1 (c (n "skillratings") (v "0.10.1") (h "1j2l9rg7ldy1gfal0lbcccnw7bsd0sllfhn1sj6zg1xc5scgiqiv")))

(define-public crate-skillratings-0.10.2 (c (n "skillratings") (v "0.10.2") (h "05wc9c5w1rsn0kc3x5czbih7yif8jyx7kk6rr5n22zgsf9ksxhki")))

(define-public crate-skillratings-0.11.0 (c (n "skillratings") (v "0.11.0") (h "0awdic0yccs9l9snirsbxhkdp30s0hb1y72vcbxkvvv4n3vssgbz")))

(define-public crate-skillratings-0.12.0 (c (n "skillratings") (v "0.12.0") (h "04jxsy98h62nswj3dzw7f4sgc8liyhmcyc9a89zr29y4lwasddih")))

(define-public crate-skillratings-0.13.0 (c (n "skillratings") (v "0.13.0") (h "05hbz7wp22ggi638lqn415vpczgl3k31lg5k6wqfcsfs7y6vd7sr")))

(define-public crate-skillratings-0.13.1 (c (n "skillratings") (v "0.13.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1ciygp1vfvqgpkw448i4x2jyn74mf3xmww40r2hs3qjgbg1fz7ck")))

(define-public crate-skillratings-0.13.2 (c (n "skillratings") (v "0.13.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1hbgxi03xka8bqy5fdk1i98bsr8z1q4nijh834jnwr1djqrjp5ya")))

(define-public crate-skillratings-0.13.3 (c (n "skillratings") (v "0.13.3") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1l3517slhw8vz93376hml76b9y1kdgg09l550bs9ib08k2rmvkq4")))

(define-public crate-skillratings-0.13.4 (c (n "skillratings") (v "0.13.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "00b2586ikdz9p88whghw6vgz60v2iy7w55k382f4xwrlsbjmggv2")))

(define-public crate-skillratings-0.14.0 (c (n "skillratings") (v "0.14.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0l6r3q9d6xix5al87z9apcyvxpcyd3q5hxa96grs69h8j41mp68s")))

(define-public crate-skillratings-0.15.0 (c (n "skillratings") (v "0.15.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0qkkgad02wwgypqiffv12lcnrc9wlc9j78c7gn8080h64f4iann0")))

(define-public crate-skillratings-0.15.1 (c (n "skillratings") (v "0.15.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1gp6pd3g4j0x1h6ishgpp8r62jlzifjb5n7paz5ivr3kpfyskvvb")))

(define-public crate-skillratings-0.15.2 (c (n "skillratings") (v "0.15.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0ncg476ks2dz2wsg2cr5l524ablfjnxi3aqcpw9l7m2ky3paw3cz")))

(define-public crate-skillratings-0.16.0 (c (n "skillratings") (v "0.16.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "03p1dr9a2cyj81ahq8ljxxahw65w77vyzq5fjcrx2sqamnhvwypz")))

(define-public crate-skillratings-0.17.0 (c (n "skillratings") (v "0.17.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0kr2wr31i9as5wbd7p1vdgqsigw5ysyz97jvcx96g4w8wpl2grrz")))

(define-public crate-skillratings-0.18.0 (c (n "skillratings") (v "0.18.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "054n8290aq71f6sm691bv9v1n5zgnsi2scadcf17p2x6fsp26sdw")))

(define-public crate-skillratings-0.19.0 (c (n "skillratings") (v "0.19.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0flcbfdmj9i1563ladc478d0szszvw88grmmp6ara4w434wii3m5")))

(define-public crate-skillratings-0.19.1 (c (n "skillratings") (v "0.19.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z4brxha7wnpkzpbimx3vbz9w6kfz9hl39hiafc3lnnsl5j1cmyg")))

(define-public crate-skillratings-0.19.2 (c (n "skillratings") (v "0.19.2") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lp6gpf8jw8207jziglpy26jbamq219vj05bnrwldrd9zm0j8m93")))

(define-public crate-skillratings-0.20.0 (c (n "skillratings") (v "0.20.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f79z771hb0yniir1cfjjzwx4hpjhf4aspsf0jdx1rbznsigvd1y")))

(define-public crate-skillratings-0.21.0 (c (n "skillratings") (v "0.21.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "041qakz4gbax9mnfffjaqqnyrkp0izpi3dmjqx7h54di8zrp62cg")))

(define-public crate-skillratings-0.21.1 (c (n "skillratings") (v "0.21.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wfsjpbln89jzy0hsb0d3j3r5d791xakag2jfa0q518nr5b2jy9d")))

(define-public crate-skillratings-0.22.0 (c (n "skillratings") (v "0.22.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zrjk3d9wvhw7cmzm30y7fx1zbn89z74sx63vg12bwq6b0k1ym1q")))

(define-public crate-skillratings-0.23.0 (c (n "skillratings") (v "0.23.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i9byw18dsydjs46bjjhx3j7z46dm79i7w0r7zq9v7faqfjsik69")))

(define-public crate-skillratings-0.24.0 (c (n "skillratings") (v "0.24.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mq5hazsb6mlb6q4i1bws1zqyyyahzjff26v69aqvmf9b4h214ya")))

(define-public crate-skillratings-0.25.0 (c (n "skillratings") (v "0.25.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1alr4m82kspjl4pvdc593qjpci92w7mpxyk6dkcgdl9gw0583ac2")))

(define-public crate-skillratings-0.25.1 (c (n "skillratings") (v "0.25.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sj0qasbzxf2r9djmdlikq3zv4m5bcnfnaz1z5yfm83smqrc79g8")))

(define-public crate-skillratings-0.26.0 (c (n "skillratings") (v "0.26.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0viz10ihx6a65j3f9h7zp2d131jif7zs6ajzdsasxyry2rp325h2")))

