(define-module (crates-io sk ep skeptic) #:use-module (crates-io))

(define-public crate-skeptic-0.1.0 (c (n "skeptic") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "1ac438n94i2845jgbw381x27849zha3vmldfs6d4mj6nrdvys0qn")))

(define-public crate-skeptic-0.2.0 (c (n "skeptic") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0sz9r86z59nvw50ad6hknxnncyvdnwslwaniak0v13ksk48017cf")))

(define-public crate-skeptic-0.3.0 (c (n "skeptic") (v "0.3.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "04qn5qb0m59dkgk3li9bh6dyy07rsr2md4d9v7xv4pbnnh20p62z")))

(define-public crate-skeptic-0.4.0 (c (n "skeptic") (v "0.4.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0si5hbzfb68ipbx30lcpmq3nkvy4mbvpmg2vmrhsx2szdyhgisr4")))

(define-public crate-skeptic-0.5.0 (c (n "skeptic") (v "0.5.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "176q6g5b8n5ccr8viyq8a9a31b0xm5x1by5sj1q0yyqi96l064h6")))

(define-public crate-skeptic-0.6.0 (c (n "skeptic") (v "0.6.0") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0zj8d1m0zarjphb4f2n0qjc5raya79dd108hp978yy0y1qf8w09x")))

(define-public crate-skeptic-0.6.1 (c (n "skeptic") (v "0.6.1") (d (list (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0kb3yalx6gqi8xhwvfhh6lakc2ny2xn44f97znms9vsmcwdz3irl")))

(define-public crate-skeptic-0.7.0 (c (n "skeptic") (v "0.7.0") (d (list (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "04l3408p3hhldmw2czy5yvs2p5s61sv0pzd7vjlvslwa4i4s7kj7")))

(define-public crate-skeptic-0.7.1 (c (n "skeptic") (v "0.7.1") (d (list (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0sxi880jah9jvhykzgl68z0ps4h4rcd0gw233qzmbpja9zp59q0q")))

(define-public crate-skeptic-0.8.0 (c (n "skeptic") (v "0.8.0") (d (list (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0srw4hgw25vxki7w9zpl6rm0qhbjh2c35vndz8rqv7i3n54997kr")))

(define-public crate-skeptic-0.8.1 (c (n "skeptic") (v "0.8.1") (d (list (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "1dbnxf42hf738ras6xvjc97arvl0pm6dbnap82fr05vykgiz1xv5")))

(define-public crate-skeptic-0.9.0 (c (n "skeptic") (v "0.9.0") (d (list (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0savk91xy74izw9z6vn6ialkaqrp81w7dayha801b52h670qszfx")))

(define-public crate-skeptic-0.10.0 (c (n "skeptic") (v "0.10.0") (d (list (d (n "pulldown-cmark") (r "^0.0.14") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0hy52b75rx4cr9686f4jxcfgrsm19cdf86af2ckjmbq7g03jma00")))

(define-public crate-skeptic-0.10.1 (c (n "skeptic") (v "0.10.1") (d (list (d (n "pulldown-cmark") (r "^0.0.15") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0yzv73wwk4qay7sims54bi1bcx34jbg1bhb62appanf6wa7r15a3")))

(define-public crate-skeptic-0.11.0 (c (n "skeptic") (v "0.11.0") (d (list (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "pulldown-cmark") (r "^0.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0kwnb0n5mb4f482mhsx12d0237h7l5cx5h42d2n2a48bhp0msdhx")))

(define-public crate-skeptic-0.12.1 (c (n "skeptic") (v "0.12.1") (d (list (d (n "bytecount") (r "^0.1.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0v4np28hxyxcw14j6py7bj5y9szgfg56gl2x0d40sgx5gdfg28ac")))

(define-public crate-skeptic-0.12.2 (c (n "skeptic") (v "0.12.2") (d (list (d (n "bytecount") (r "^0.1.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1j06hr27h02rkdj1iyn77di8y3wdkv0racrwbsblamxzlsh06x4r")))

(define-public crate-skeptic-0.12.3 (c (n "skeptic") (v "0.12.3") (d (list (d (n "bytecount") (r "^0.1.7") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0bcan07bysz9y9s9x0lxaqz3z6rhif2qbacdhc8nnfbx6i4iacmm")))

(define-public crate-skeptic-0.13.0 (c (n "skeptic") (v "0.13.0") (d (list (d (n "bytecount") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.12") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.12") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "unindent") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1idfig7q8zc9vjdqgx0ir3ghpd8zk0pskwsdw9bymagb2m9m4c8z")))

(define-public crate-skeptic-0.13.1 (c (n "skeptic") (v "0.13.1") (d (list (d (n "bytecount") (r "^0.2.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)) (d (n "unindent") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1wivb9bbka3p0ix0v8qwp7g9finp0ggv8aa9bqpnn0nlqcp9np38")))

(define-public crate-skeptic-0.13.2 (c (n "skeptic") (v "0.13.2") (d (list (d (n "bytecount") (r "^0.2.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "unindent") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0nppgnygm80g9gp4yi7ivyax0w5ckqrdhyslpss2v3hnra7iyhy8")))

(define-public crate-skeptic-0.13.3 (c (n "skeptic") (v "0.13.3") (d (list (d (n "bytecount") (r "^0.3.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.0") (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "unindent") (r "^0.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "1dplc4sy9l55z10v3r94rf1jg8d38k1hz2c6n2y72carm5nlsiy4")))

(define-public crate-skeptic-0.13.4 (c (n "skeptic") (v "0.13.4") (d (list (d (n "bytecount") (r "^0.4") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.6") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "0rai61hbs65nbvbhqlk1nap5hlav5qx3zmjjjzh9rhgxagc8xyyn")))

(define-public crate-skeptic-0.13.5 (c (n "skeptic") (v "0.13.5") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.10") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.2") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "025q3yc31jg9g1svxiw7xsyby1hqsnxvi78khkbqzbgkza7fnvbs")))

(define-public crate-skeptic-0.13.6 (c (n "skeptic") (v "0.13.6") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1258f19r032j27ml3c3hjdapn61lkwllal47pgq273yr881q32qq")))

(define-public crate-skeptic-0.13.7 (c (n "skeptic") (v "0.13.7") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.14") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1a205720pnss0alxvbx0fcn3883cg3fbz5y1047hmjbnaq0kplhn")))

