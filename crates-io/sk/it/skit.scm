(define-module (crates-io sk it skit) #:use-module (crates-io))

(define-public crate-skit-0.1.0 (c (n "skit") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0b93b3bp2xfbzwbkpl09pfmsrylhm1gy2b177sph5fcnb6dn0rmy")))

(define-public crate-skit-0.2.0 (c (n "skit") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "185ppkkhhvvsjflaan1kc6gmglpa6rjflx2wkh99j0xp91r302lg")))

(define-public crate-skit-0.2.1 (c (n "skit") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1m6xm926n5ins0jkhqsfyk95h65djriy0w8khg7pgl0fqn93wn3s")))

(define-public crate-skit-0.3.0 (c (n "skit") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1ad3xz5kwxkdpqlqm8432v9gpj24kxw9shbif6kpz7mq89nf2lgr")))

