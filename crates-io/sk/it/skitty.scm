(define-module (crates-io sk it skitty) #:use-module (crates-io))

(define-public crate-skitty-0.1.0 (c (n "skitty") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "zip") (r "^0.4.2") (d #t) (k 0)))) (h "1q82h21gh39j4bfbwgz4gp6r516fv6f6v9r9m8fgwnv44bxqqvfb")))

