(define-module (crates-io sk ys skyscanner) #:use-module (crates-io))

(define-public crate-skyscanner-0.1.0 (c (n "skyscanner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "rust_decimal") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1iq8il3lpp9ppc1c20cc4igv6ic9dpzj38r0f9mw45zz4c8hwj4a")))

