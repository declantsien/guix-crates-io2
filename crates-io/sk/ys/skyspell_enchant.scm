(define-module (crates-io sk ys skyspell_enchant) #:use-module (crates-io))

(define-public crate-skyspell_enchant-0.1.0 (c (n "skyspell_enchant") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "enchant") (r "^0.3.0") (d #t) (k 0)) (d (n "skyspell_core") (r "^0.0.0") (d #t) (k 0)))) (h "1z9z0r51mc4p1mmfk573xk47fgqabzjdmnr36kqbfbgplbfw7a8w") (y #t)))

