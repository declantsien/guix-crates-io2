(define-module (crates-io sk id skid) #:use-module (crates-io))

(define-public crate-skid-1.0.0 (c (n "skid") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "199pp1i120y1xjdsa3l10hjwafm1df9jdc4063m96bfnzi3k70wx")))

(define-public crate-skid-1.1.0 (c (n "skid") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "10r9c756fnns3ypq7ab6y5ch3ralh0pd1026ckgg93cv46aq8mn5")))

(define-public crate-skid-1.1.1 (c (n "skid") (v "1.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^9.0.0") (d #t) (k 0)))) (h "1b5v3gbv1myxgk7mihpi022nxx1g7lja8fwj57s3app8vzqb2l7h")))

