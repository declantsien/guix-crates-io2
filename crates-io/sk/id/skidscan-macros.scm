(define-module (crates-io sk id skidscan-macros) #:use-module (crates-io))

(define-public crate-skidscan-macros-0.1.0 (c (n "skidscan-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16swckfna228zr0h6w2hylkp0j2bzgd2pnpa8zj2p277i0ba1fip") (f (quote (("obfuscate"))))))

(define-public crate-skidscan-macros-0.1.1 (c (n "skidscan-macros") (v "0.1.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10wgcvm3xfwv6shvv97d2xcc3vr3rvlgkfwrkvkbzgq9wcm652n9") (f (quote (("obfuscate"))))))

(define-public crate-skidscan-macros-0.1.2 (c (n "skidscan-macros") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13by4ss3ggfi87wswd3vl1m1par1svna6qdnbf2jlkhf1waz47l8") (f (quote (("obfuscate"))))))

