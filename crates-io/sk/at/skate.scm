(define-module (crates-io sk at skate) #:use-module (crates-io))

(define-public crate-skate-0.0.0 (c (n "skate") (v "0.0.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "fs-err") (r "^2.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.7.0") (d #t) (k 2)) (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "17c6kcc7zyy2xpsji0mp3p7p4r7qs75azqwl6vsfd0xv51dqcgyk")))

