(define-module (crates-io sk ew skew-forest) #:use-module (crates-io))

(define-public crate-skew-forest-0.1.0 (c (n "skew-forest") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "debug-tag") (r "^0.1.0") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.2.0") (d #t) (k 0)))) (h "1pa5y0252fyvc4ixs424fjxg2ffl24bx0rl803xxr7a56kxsxsxl")))

