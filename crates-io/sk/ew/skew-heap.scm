(define-module (crates-io sk ew skew-heap) #:use-module (crates-io))

(define-public crate-skew-heap-0.1.0 (c (n "skew-heap") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0d060msir0jqacg5dqjy6n6akl5q0sp2mcsy3jciz7aphwcgwk3q") (f (quote (("specialization") ("placement"))))))

(define-public crate-skew-heap-0.2.0 (c (n "skew-heap") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1w096z2km01h1jlsm4lxxy61rx175yg2pw34ylzvwrmy7wwzldkc") (f (quote (("specialization") ("placement"))))))

