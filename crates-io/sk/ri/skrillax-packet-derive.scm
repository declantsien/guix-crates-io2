(define-module (crates-io sk ri skrillax-packet-derive) #:use-module (crates-io))

(define-public crate-skrillax-packet-derive-0.1.0 (c (n "skrillax-packet-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1x39absi64j5rnsj424z5gs1h2842iszvinl9zbv698bv99vjss9")))

