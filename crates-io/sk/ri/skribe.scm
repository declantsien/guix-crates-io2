(define-module (crates-io sk ri skribe) #:use-module (crates-io))

(define-public crate-skribe-0.1.0 (c (n "skribe") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gn0wqjraqx7j4qwpcan7k3idgmhyq1mb9fvk2lfmjvgj0h6mmfb")))

