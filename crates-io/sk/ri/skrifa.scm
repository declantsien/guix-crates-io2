(define-module (crates-io sk ri skrifa) #:use-module (crates-io))

(define-public crate-skrifa-0.0.1 (c (n "skrifa") (v "0.0.1") (d (list (d (n "read-fonts") (r "^0.1.4") (d #t) (k 0)) (d (n "read-fonts") (r "^0.1.4") (f (quote ("test_data"))) (d #t) (k 2)))) (h "1xivsk7w25fla4ns1afgl58h92xq8ghfk9bg2ci886hrlccvgvl5") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.1.0 (c (n "skrifa") (v "0.1.0") (d (list (d (n "read-fonts") (r "^0.2.0") (d #t) (k 0)))) (h "1zvv1iqalgrxhf73kzmfhx3vg1w52d64wl86zyv63rr4pgag61l9") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.1.1 (c (n "skrifa") (v "0.1.1") (d (list (d (n "read-fonts") (r "^0.2.1") (d #t) (k 0)))) (h "049klw8i39cksw3q1hhwmf5kzxpw2xnhy9n31j82b6834c0x7z8h") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.2.0 (c (n "skrifa") (v "0.2.0") (d (list (d (n "read-fonts") (r "^0.3.0") (d #t) (k 0)))) (h "05v3naaq4b0zb31bfmg6qzv338y7wxhz53mb7wfpjp28ysdr31g8") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.3.0 (c (n "skrifa") (v "0.3.0") (d (list (d (n "read-fonts") (r "^0.4.0") (d #t) (k 0)))) (h "1vzyfqwj9ns15kyh3nv40sh3bddm7fxr6a7bwmi9ba8nqiafq3nc") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.4.0 (c (n "skrifa") (v "0.4.0") (d (list (d (n "read-fonts") (r "^0.5.0") (d #t) (k 0)))) (h "08i4qpqfyyphiz8m4svvjhih1851mhi07p8ypkfi2ynzaw3qv76r") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.5.0 (c (n "skrifa") (v "0.5.0") (d (list (d (n "read-fonts") (r "^0.6.0") (d #t) (k 0)))) (h "0fmzlbf2w068bjhg34ljdbrvd2vk9s3vngy51lbbd3yq5l71j4j0") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.6.0 (c (n "skrifa") (v "0.6.0") (d (list (d (n "read-fonts") (r "^0.7.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.7.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "10vbi0farm4wf05b8i02jjh281qjksb5gijv0g6n5rfhfhkab31n") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.7.0 (c (n "skrifa") (v "0.7.0") (d (list (d (n "read-fonts") (r "^0.8.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.8.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "1fhcshxf6fh4s4ykifric1wi8mrh6anmxzx4hvxpwf3lc5akrdab") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.8.0 (c (n "skrifa") (v "0.8.0") (d (list (d (n "read-fonts") (r "^0.9.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.9.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "0g8bghqyh8fg1wfk5smib9nb45wifv60da8crbhrknid7y58qzjg") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.9.0 (c (n "skrifa") (v "0.9.0") (d (list (d (n "read-fonts") (r "^0.10.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.10.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "0868wf0lsszban8k1wqxzgd2xcb1q7m9mycja9mjv9frip4g6r1p") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.10.0 (c (n "skrifa") (v "0.10.0") (d (list (d (n "read-fonts") (r "^0.11.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.11.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "1bzqi0srlxkll5429hkpl4j3cqz85s5ris41w8cbq7fyqdflnf1j") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.10.1 (c (n "skrifa") (v "0.10.1") (d (list (d (n "read-fonts") (r "^0.11.2") (d #t) (k 0)) (d (n "read-fonts") (r "^0.11.2") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "0cmwq932phvrhpi6qm4qs9xrn5d74nw37qrlv039dpjc6zqg9yrs") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.11.0 (c (n "skrifa") (v "0.11.0") (d (list (d (n "read-fonts") (r "^0.12.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.12.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "01bj7dpvnpf1hax1m7nr6b2kpsamv7y8fffha3wsrpml62ffahf0") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.12.0 (c (n "skrifa") (v "0.12.0") (d (list (d (n "read-fonts") (r "^0.13.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.13.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "1ymsagqsqkpws2i4bm992n43wmamkq2j129z2r6h9ddh9m8phczw") (f (quote (("scale") ("hinting") ("default" "scale"))))))

(define-public crate-skrifa-0.13.0 (c (n "skrifa") (v "0.13.0") (d (list (d (n "read-fonts") (r "^0.13.2") (d #t) (k 0)) (d (n "read-fonts") (r "^0.13.2") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "12s677am127cgvnq9s6assjyb8mkp40n1p2p4cbs97r71hn7ncrq")))

(define-public crate-skrifa-0.14.0 (c (n "skrifa") (v "0.14.0") (d (list (d (n "read-fonts") (r "^0.14.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.14.0") (f (quote ("scaler_test"))) (d #t) (k 2)))) (h "16g6nyndkk1dzxwzlxaai1kcrirfaysaq521ir81s8bx4flbvbzv")))

(define-public crate-skrifa-0.15.0 (c (n "skrifa") (v "0.15.0") (d (list (d (n "read-fonts") (r "^0.15.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.0") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05abzaybzg5ay7rknr2lalxkksv8chbllkb4lxdy7mq8x5i38s8n")))

(define-public crate-skrifa-0.15.1 (c (n "skrifa") (v "0.15.1") (d (list (d (n "read-fonts") (r "^0.15.1") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.1") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a7581d7crzfqd36hj0wk9z7rw19wyjjc4r6izw3radqng1j1v3a")))

(define-public crate-skrifa-0.15.2 (c (n "skrifa") (v "0.15.2") (d (list (d (n "read-fonts") (r "^0.15.1") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.1") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zz05a8n7jg4drx7g89sp360jrp8yqfzz9pc4b8h1wa97rsnf1yy")))

(define-public crate-skrifa-0.15.3 (c (n "skrifa") (v "0.15.3") (d (list (d (n "read-fonts") (r "^0.15.2") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gqrali93ijmdnggi62ry07wrdwiad5h10jqqxggqdkfdhin47a6")))

(define-public crate-skrifa-0.15.4 (c (n "skrifa") (v "0.15.4") (d (list (d (n "read-fonts") (r "^0.15.2") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.2") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01afr1c4n724mdi0cd6bn4b1v58ym6bs8c1xy3xnynazj7bnhapd")))

(define-public crate-skrifa-0.15.5 (c (n "skrifa") (v "0.15.5") (d (list (d (n "read-fonts") (r "^0.15.3") (d #t) (k 0)) (d (n "read-fonts") (r "^0.15.3") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08nfm0xsqksjsjs5c08jjkqi662civqy09x3z470chvdnviqxwpg")))

(define-public crate-skrifa-0.16.0 (c (n "skrifa") (v "0.16.0") (d (list (d (n "read-fonts") (r "^0.16.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.16.0") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04l27qw2ccg6b59y0zrn4hbg2i6mqbc12xlgc0vyicpz4c28bwhv")))

(define-public crate-skrifa-0.17.0 (c (n "skrifa") (v "0.17.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "read-fonts") (r "^0.17.0") (d #t) (k 0)) (d (n "read-fonts") (r "^0.17.0") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "07mf3hf0pkap98xq6098xwkwgpcaiz92dbxk5ba6zzi4ff21xnvn")))

(define-public crate-skrifa-0.18.0 (c (n "skrifa") (v "0.18.0") (d (list (d (n "bytemuck") (r "=1.13.1") (d #t) (k 0)) (d (n "core_maths") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "read-fonts") (r "^0.18.0") (k 0)) (d (n "read-fonts") (r "^0.18.0") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1pmbbk8hk3slyn78gfbbgbnmi3vj9j88pwf97134xwbc89afb3yw") (f (quote (("traversal" "std" "read-fonts/traversal") ("std" "read-fonts/std") ("default" "traversal")))) (s 2) (e (quote (("libm" "dep:core_maths"))))))

(define-public crate-skrifa-0.19.0 (c (n "skrifa") (v "0.19.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "core_maths") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "read-fonts") (r "^0.19.0") (k 0)) (d (n "read-fonts") (r "^0.19.0") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s96ij8lgyvgj959d00c5w7jc3gy6kiqk4vpl5gbad5drdp4rxdk") (f (quote (("traversal" "std" "read-fonts/traversal") ("std" "read-fonts/std") ("default" "traversal")))) (s 2) (e (quote (("libm" "dep:core_maths"))))))

(define-public crate-skrifa-0.19.1 (c (n "skrifa") (v "0.19.1") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "core_maths") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "kurbo") (r "^0.11.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "read-fonts") (r "^0.19.1") (k 0)) (d (n "read-fonts") (r "^0.19.1") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d0gznny4viy2wcj80ia3s85p1xv0kskavznr3189nxdr6hddgld") (f (quote (("traversal" "std" "read-fonts/traversal") ("std" "read-fonts/std") ("default" "traversal")))) (s 2) (e (quote (("libm" "dep:core_maths"))))))

(define-public crate-skrifa-0.19.2 (c (n "skrifa") (v "0.19.2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "core_maths") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "kurbo") (r "^0.11.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "read-fonts") (r "^0.19.2") (k 0)) (d (n "read-fonts") (r "^0.19.2") (f (quote ("scaler_test" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lmq41czqmi1dpsai0w4qf0dybk7gga17wkjq4ffv9i1a43ymril") (f (quote (("traversal" "std" "read-fonts/traversal") ("std" "read-fonts/std") ("default" "traversal")))) (s 2) (e (quote (("libm" "dep:core_maths"))))))

