(define-module (crates-io sk ri skrillax-serde) #:use-module (crates-io))

(define-public crate-skrillax-serde-0.1.0 (c (n "skrillax-serde") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "skrillax-serde-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wyjzknx2f9im7b0sh8iaj4ixj84qr1bcc8d5kracb6f86nrswkx") (f (quote (("default" "chrono")))) (s 2) (e (quote (("derive" "dep:skrillax-serde-derive") ("chrono" "dep:chrono"))))))

(define-public crate-skrillax-serde-0.1.1 (c (n "skrillax-serde") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "skrillax-serde-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bflfa0qjmymdj77kikc27csdilx1aj3vvrm2ivfghff3lb9hpq7") (f (quote (("default" "chrono")))) (s 2) (e (quote (("derive" "dep:skrillax-serde-derive") ("chrono" "dep:chrono"))))))

