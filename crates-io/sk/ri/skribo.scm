(define-module (crates-io sk ri skribo) #:use-module (crates-io))

(define-public crate-skribo-0.0.0 (c (n "skribo") (v "0.0.0") (h "1vcrxwm2ilv152cp2frhk5gyrq36pn4b5m7f6jw0ln4ks9cvmbfh")))

(define-public crate-skribo-0.1.0 (c (n "skribo") (v "0.1.0") (d (list (d (n "font-kit") (r "^0.6") (d #t) (k 0)) (d (n "harfbuzz") (r "^0.3.1") (d #t) (k 0)) (d (n "harfbuzz-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathfinder_geometry") (r "^0.5") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.8") (d #t) (k 0)))) (h "05lxaa5gis2racjdlqd9148vsjng8sm61l18fh2d7dmlxh9ygsg6")))

