(define-module (crates-io sk ri skrillax-security) #:use-module (crates-io))

(define-public crate-skrillax-security-0.1.0 (c (n "skrillax-security") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.5") (d #t) (k 0)) (d (n "blowfish") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kd2pqp22yb8zqgrq684gpppy6xyzm3nrxnm4hpi46bycaklx832")))

