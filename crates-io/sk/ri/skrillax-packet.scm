(define-module (crates-io sk ri skrillax-packet) #:use-module (crates-io))

(define-public crate-skrillax-packet-0.1.0 (c (n "skrillax-packet") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "skrillax-codec") (r "^0.1.1") (d #t) (k 0)) (d (n "skrillax-packet-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "skrillax-security") (r "^0.1.0") (d #t) (k 0)) (d (n "skrillax-serde") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mfrxx1q118q5910hlcbsidrfvdr8780kl8skrcc3nhbdnfqjqmg") (s 2) (e (quote (("serde" "dep:skrillax-serde") ("derive" "dep:skrillax-packet-derive" "serde"))))))

