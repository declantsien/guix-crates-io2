(define-module (crates-io sk ri skribble_color) #:use-module (crates-io))

(define-public crate-skribble_color-0.0.0 (c (n "skribble_color") (v "0.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^1") (f (quote ("json"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "palette") (r "^0.6") (f (quote ("serializing"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vwpx2msg0fjsa1g0jms5hcn6m4qmgphki384q0hsn1h80n2iqhz") (r "1.68.0")))

