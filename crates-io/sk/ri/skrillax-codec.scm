(define-module (crates-io sk ri skrillax-codec) #:use-module (crates-io))

(define-public crate-skrillax-codec-0.1.0 (c (n "skrillax-codec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1nsdcja315l1zm5jkkc9qkv13h8mqf9wzpil3jzp5qd9dpsqb371") (f (quote (("default" "codec")))) (s 2) (e (quote (("codec" "dep:tokio-util"))))))

(define-public crate-skrillax-codec-0.1.1 (c (n "skrillax-codec") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "06icpxarnf08flrf6llyc3if33rp5brj3ws9mdf25b1h0n6nql4k") (f (quote (("default" "codec")))) (s 2) (e (quote (("codec" "dep:tokio-util"))))))

