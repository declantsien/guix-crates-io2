(define-module (crates-io sk #{68}# sk6812_rpi) #:use-module (crates-io))

(define-public crate-sk6812_rpi-0.1.0 (c (n "sk6812_rpi") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13") (d #t) (k 0)))) (h "1yvxmhd7jw3iv92na328zvsg1dm9r6gpzmnk1yc8s03fichbdiyn")))

(define-public crate-sk6812_rpi-0.1.1 (c (n "sk6812_rpi") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13") (d #t) (k 0)))) (h "16py1bkp45jnzmmna8zq42cfvdp4zrw0c09j0ki5m5c0d55rycfn")))

(define-public crate-sk6812_rpi-0.1.2 (c (n "sk6812_rpi") (v "0.1.2") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "palette") (r "^0.6") (d #t) (k 0)) (d (n "rppal") (r "^0.13") (d #t) (k 0)))) (h "0q3vca2ymk58bfvbkjs5vkra7901hcx1za0sga6nvkc1m746j93v")))

