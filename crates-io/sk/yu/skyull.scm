(define-module (crates-io sk yu skyull) #:use-module (crates-io))

(define-public crate-skyull-0.1.0 (c (n "skyull") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "017d228c0lnpar7w1pqyw90v737pl13dcswdji812cq6sh3xcb3s") (r "1.67.0")))

