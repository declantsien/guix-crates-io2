(define-module (crates-io sk ta sktablelayout-rs) #:use-module (crates-io))

(define-public crate-sktablelayout-rs-0.1.0 (c (n "sktablelayout-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0m4hg7aabkibncngmd57dx6kgpv45hpdq0m78iv1q0w5xx5z2fa5")))

(define-public crate-sktablelayout-rs-0.1.1 (c (n "sktablelayout-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0j1w8g1fq3x2inqhnkykzq1gnk4nvksiddhigxm96nq7l8d0mhrg")))

(define-public crate-sktablelayout-rs-0.2.0 (c (n "sktablelayout-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0sxplh8wfar2gc2m69x7s9b85xrhcngcrld8xx4qndqjaqldj98p")))

(define-public crate-sktablelayout-rs-0.2.1 (c (n "sktablelayout-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "18r6sfm8rgfxc0lcwzf2d4pjfzas76m53l4nwxfxm5h7a9dipyrq")))

