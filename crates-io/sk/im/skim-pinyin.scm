(define-module (crates-io sk im skim-pinyin) #:use-module (crates-io))

(define-public crate-skim-pinyin-0.1.0 (c (n "skim-pinyin") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pinyin") (r "^0.9") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "11r8a5z2xgyqj889w8akkhxgc815jyh4myhbgsjfy2yrww49wdd6")))

