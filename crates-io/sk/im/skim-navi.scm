(define-module (crates-io sk im skim-navi) #:use-module (crates-io))

(define-public crate-skim-navi-0.0.1 (c (n "skim-navi") (v "0.0.1") (h "135ayf8jp6k80cl4dz2j8093rpg4d45wb5r8j61x3624qsrsqybh")))

(define-public crate-skim-navi-0.0.2 (c (n "skim-navi") (v "0.0.2") (d (list (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "1b6bl0r50ws4a711mixa8p4ncq75w6kvq76xjw1n2zjpch86mk0g")))

(define-public crate-skim-navi-0.0.3 (c (n "skim-navi") (v "0.0.3") (d (list (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "1rla1nghbcybgy1d2dy1s2421cb9qklgnfi4hk05hqlz6r0yz0fn")))

(define-public crate-skim-navi-0.0.4 (c (n "skim-navi") (v "0.0.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "0c679394q7lck8ylbjbcmbyn5g2g1jj55jrhrilfnqa0kkyjr7k0")))

(define-public crate-skim-navi-0.1.0 (c (n "skim-navi") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "0x975hc1170ds44757c3176fd5dxzsy8855fjmwbhip9bzp5kvqp")))

(define-public crate-skim-navi-0.1.1 (c (n "skim-navi") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.9.4") (d #t) (k 0)))) (h "0jrivmxk1f4qn4sxyzjbiwz31qxj566an3kdk8vggg31473g224w")))

(define-public crate-skim-navi-0.1.2 (c (n "skim-navi") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 2)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.3") (d #t) (k 2)))) (h "1lw41lvv40ma2am11hmw5r53lh9sdnch30d8zbhk2ds4dcdkg5hf")))

(define-public crate-skim-navi-0.1.3 (c (n "skim-navi") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 2)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.3") (d #t) (k 2)))) (h "07sjbgn58x8lizs6x4xcx4cg1g36l5a0l4h1idwjbw5q6h2al96s")))

(define-public crate-skim-navi-0.1.4 (c (n "skim-navi") (v "0.1.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 2)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.4") (d #t) (k 2)))) (h "1br66zlziyg3i015s8k26qlwqyzim8yfzvqq6kkk9fqjk392kh5q")))

(define-public crate-skim-navi-0.1.5 (c (n "skim-navi") (v "0.1.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.4") (d #t) (k 2)))) (h "14ad803x67ra7s7w38fr3cr6qhjr862smhjw5pbl1ignxyagg30d")))

(define-public crate-skim-navi-0.1.6 (c (n "skim-navi") (v "0.1.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.24.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.5") (d #t) (k 2)))) (h "0q2hypik6nryzdlm10zlq2cmd5fd5z42g6pi7grn3aylsgww5pak")))

(define-public crate-skim-navi-0.1.7 (c (n "skim-navi") (v "0.1.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.5") (d #t) (k 2)))) (h "0i76i2rk8dbf92bwdzqfq5wyj91kwkixp53nhnqx6ic5s7lgxjs4")))

(define-public crate-skim-navi-0.1.8 (c (n "skim-navi") (v "0.1.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.5") (d #t) (k 2)))) (h "0mam8kpc0axmrl22nipdaj47a7lnqcyzrs767xv7d3bcb2d36gnq")))

(define-public crate-skim-navi-0.1.9 (c (n "skim-navi") (v "0.1.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.5") (d #t) (k 2)))) (h "1nj6hs36f4gq6czi0pmfq6kb2wmsb0r9mhknmwy2yz4hjbsj4w0y")))

(define-public crate-skim-navi-0.1.10 (c (n "skim-navi") (v "0.1.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 2)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warpy") (r "^0.3.5") (d #t) (k 2)))) (h "1bmpp06g4k709av1hcv6wl6p02nbfarp3qlp571r76nb3i9r6zsa")))

