(define-module (crates-io sk im skim-pick) #:use-module (crates-io))

(define-public crate-skim-pick-0.1.0 (c (n "skim-pick") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0hxiwbfvjxin0gn34854xv7628w0ibhmd0f6fmy23khhvj4g2xmq")))

(define-public crate-skim-pick-0.1.1 (c (n "skim-pick") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1f184hm5wj7954asxq9zpwm9akvz5yyrm3pv15xwvkbj57wkpw30")))

(define-public crate-skim-pick-0.1.2 (c (n "skim-pick") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0h659bb8rd1xsffrncwp4645crr8yv6w27abnk9cs5hq29iw9i36")))

