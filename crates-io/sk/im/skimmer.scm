(define-module (crates-io sk im skimmer) #:use-module (crates-io))

(define-public crate-skimmer-0.0.1 (c (n "skimmer") (v "0.0.1") (h "0jab5x4wvwrqqi6g6w74yhm2j7gfk91s0fm95rn381mzp89rs1y5")))

(define-public crate-skimmer-0.0.2 (c (n "skimmer") (v "0.0.2") (h "066p6xpbdqmikbadnxvjsj02bx59zccqlhzpapkn9r4gj0gzb659")))

(define-public crate-skimmer-0.0.3 (c (n "skimmer") (v "0.0.3") (h "11vs723nx1nzwwaavl1s87wkpaqr3xdymws6w4s3q9c28p90k1m0")))

