(define-module (crates-io sk ip skipchannel) #:use-module (crates-io))

(define-public crate-skipchannel-1.0.0 (c (n "skipchannel") (v "1.0.0") (h "0ym6ahf4l0sc161g6kqgf73nvx5hlvyq9847wgvgp6cqkcjrz3xv")))

(define-public crate-skipchannel-2.0.0 (c (n "skipchannel") (v "2.0.0") (h "14d9wyryj9j961j5v8xc2s0844kr2xx6gyr6ccqd4da916s6xmry")))

(define-public crate-skipchannel-2.0.1 (c (n "skipchannel") (v "2.0.1") (h "19h26i92zdj0lbc0k68b07crlgp7nn8r7i4jfc4y6psmdz0vyhka")))

