(define-module (crates-io sk ip skippy-rs) #:use-module (crates-io))

(define-public crate-skippy-rs-0.0.0-alpha.0 (c (n "skippy-rs") (v "0.0.0-alpha.0") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wrqw3bsbhx5q0crgj0apc28yvsii1qjq736179jql3m26fjq49b")))

(define-public crate-skippy-rs-0.0.0-alpha.1 (c (n "skippy-rs") (v "0.0.0-alpha.1") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a43dknjnd9xhw3j6qwwfbj3hycrkzvcvh6wqq4rciph5wqm12vs")))

(define-public crate-skippy-rs-0.0.0-alpha.2 (c (n "skippy-rs") (v "0.0.0-alpha.2") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07lw3nvlj0j1db2xa7wr1g86amwfcvc28kqnpk3r7s31djaj1052")))

(define-public crate-skippy-rs-0.0.0-alpha.3 (c (n "skippy-rs") (v "0.0.0-alpha.3") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0chxyy213w5wyfn6fn9jcfgx705fy5lcs9dfscj8yr9ldp2kck4x")))

(define-public crate-skippy-rs-0.0.0-alpha.4 (c (n "skippy-rs") (v "0.0.0-alpha.4") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 2)) (d (n "haphazard") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a9iah56acplag1anw2jdp3ngb4sbq9n5qqdla96g266fkbs63gf")))

