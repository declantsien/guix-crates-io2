(define-module (crates-io sk ip skip_error) #:use-module (crates-io))

(define-public crate-skip_error-1.0.0 (c (n "skip_error") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "173q8lnacr4saa22sxpqr2f5ilp8vrfr80g16ljyky5srfj82k5l")))

(define-public crate-skip_error-1.0.1 (c (n "skip_error") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "022a59pc5a6g5hz52k2v42b0ayhh2b45kzqil98dnki6z2jgxx7i")))

(define-public crate-skip_error-1.1.0 (c (n "skip_error") (v "1.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yajy2c9p7qihz7s1f3swngglhkah4ayak70b2blhx92v95vkvcn")))

(define-public crate-skip_error-2.0.0 (c (n "skip_error") (v "2.0.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (o #t) (d #t) (k 0)))) (h "102wjb3vckinppl5cihkzqs2z7ysab4qws7rqjagzffp7gvk3i8k")))

(define-public crate-skip_error-3.0.0 (c (n "skip_error") (v "3.0.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (o #t) (d #t) (k 0)))) (h "1jlvkpsjcka4sdwxw73xdqfsqkzg9xzrsi69zshjc0lbln5bqsk0")))

(define-public crate-skip_error-3.1.0 (c (n "skip_error") (v "3.1.0") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (o #t) (d #t) (k 0)))) (h "12avaci6873jzvpbhqgan6c0rywi4kn64702v91zynyshw1hfhv4")))

(define-public crate-skip_error-3.1.1 (c (n "skip_error") (v "3.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "testing_logger") (r "^0.1") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (o #t) (d #t) (k 0)))) (h "0wsi1mbq8pkda1h33zbbbswrll8jnjjrrxsyi302hkxr3j0bn5qa")))

