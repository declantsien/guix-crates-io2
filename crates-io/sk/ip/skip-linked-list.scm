(define-module (crates-io sk ip skip-linked-list) #:use-module (crates-io))

(define-public crate-skip-linked-list-0.1.0 (c (n "skip-linked-list") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ysmn7b4920zla835l0a9ymlrp2r075vkhv32br241sflwzgypq5")))

(define-public crate-skip-linked-list-0.1.1 (c (n "skip-linked-list") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1j29yz5gyracfidnwl152zlicz0d0yawynbv357ijfv2rhm2la93")))

