(define-module (crates-io sk ip skipjack_rs) #:use-module (crates-io))

(define-public crate-skipjack_rs-0.1.0 (c (n "skipjack_rs") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "^0.1.2") (d #t) (k 2)) (d (n "generic-array") (r "^0.14.5") (d #t) (k 0)))) (h "079xf4bdmbmzabqzwf40s89djr3vrl2qjh065kw00l09hmhklrmy")))

