(define-module (crates-io sk ip skip32) #:use-module (crates-io))

(define-public crate-skip32-0.1.0 (c (n "skip32") (v "0.1.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1v8zhdkf2nfyblwcrcdrxb3gjmdqvcwr2ji9syfxccg7rhqkpyba")))

(define-public crate-skip32-0.1.1 (c (n "skip32") (v "0.1.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0w0575pzb6ybbxfk3xz4iva2a88xz22qw9g0ijj0qmm17fx256rb")))

(define-public crate-skip32-1.0.0 (c (n "skip32") (v "1.0.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "05vjj1cd5jp7yi8dmxab5h8wwbxkx0mvkn58f5rqr8gwrs7m2aj0")))

(define-public crate-skip32-1.0.1 (c (n "skip32") (v "1.0.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "16kzhiffh2p4incrrpbc4m3niq8w04sx9akr1kx4hnr6s7yq68h9")))

(define-public crate-skip32-1.0.2 (c (n "skip32") (v "1.0.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "17v3r50m9fi8p88f7ighpsw44if260q9qjcaj5jzfv0zix1bv7xq")))

(define-public crate-skip32-1.0.3 (c (n "skip32") (v "1.0.3") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "15yy1bxy3q1k85jljc57s0nmyfsgyq470d7jmv0d9p9bcmzmap7c") (y #t)))

(define-public crate-skip32-1.0.4 (c (n "skip32") (v "1.0.4") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "1r5dzxgv98ciz54s2bn4lb90n1ivdq5d1rf0nqrm2qamj4h3xscr")))

(define-public crate-skip32-1.0.5 (c (n "skip32") (v "1.0.5") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "0xj7z2niy5qf2cl51h6c0m82w5vi9qy4qwr4z1isvibw9w4n15vg")))

