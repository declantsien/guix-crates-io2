(define-module (crates-io sk ip skipfree) #:use-module (crates-io))

(define-public crate-skipfree-0.1.0 (c (n "skipfree") (v "0.1.0") (d (list (d (n "guacamole") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1fkmjmhbw1n5q69m6afq5dp2czj9vksdmwynmz36gczf517liskh")))

(define-public crate-skipfree-0.2.0 (c (n "skipfree") (v "0.2.0") (d (list (d (n "guacamole") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0l4afy9ncwnixpcxnpr4rqrmv6kr34cr0jw8s53qr0kfnz8xdr1v")))

