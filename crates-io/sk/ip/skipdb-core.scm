(define-module (crates-io sk ip skipdb-core) #:use-module (crates-io))

(define-public crate-skipdb-core-0.1.0 (c (n "skipdb-core") (v "0.1.0") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "txn-core") (r "^0.1") (k 0)))) (h "08afnyiv4wf2czjk5mb89b6sfys3fjrfgm526w48jf8kf55v4hj4") (r "1.75.0")))

(define-public crate-skipdb-core-0.1.1 (c (n "skipdb-core") (v "0.1.1") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (d #t) (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (d #t) (k 0)) (d (n "txn-core") (r "^0.1") (k 0)))) (h "1zp4f1g19iy60mfq7xxlhmw7wxk018h5q5jhykcz23l3wavqgd1v") (r "1.75.0")))

(define-public crate-skipdb-core-0.1.2 (c (n "skipdb-core") (v "0.1.2") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (k 0)) (d (n "txn-core") (r "^0.1") (f (quote ("alloc"))) (k 0)))) (h "0sg0zzyyhjnmc9aw64q9y22l9k796jspsqi7cx7wqi3v5i3d7mfq") (f (quote (("std" "smallvec-wrapper/std" "crossbeam-skiplist/default" "txn-core/std" "either/default") ("default" "std")))) (r "1.75.0")))

(define-public crate-skipdb-core-0.2.0 (c (n "skipdb-core") (v "0.2.0") (d (list (d (n "crossbeam-skiplist") (r "^0.1") (k 0)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "smallvec-wrapper") (r "^0.1") (f (quote ("const_new"))) (k 0)) (d (n "txn-core") (r "^0.1") (f (quote ("alloc"))) (k 0)))) (h "071z98aqlx1wabfs0k54mh5b1df3skaywvjsxa23xafx59qn04df") (f (quote (("std" "smallvec-wrapper/std" "crossbeam-skiplist/default" "txn-core/std" "either/default") ("default" "std")))) (r "1.75.0")))

