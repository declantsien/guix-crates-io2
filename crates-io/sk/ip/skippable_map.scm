(define-module (crates-io sk ip skippable_map) #:use-module (crates-io))

(define-public crate-skippable_map-0.1.0 (c (n "skippable_map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "198lcbii3mh6svi63mkf0rxfq69zk5yj59yjyxa56slxg0zlxb46")))

(define-public crate-skippable_map-0.1.1 (c (n "skippable_map") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1qh03j4jnjr26arcfvwavnv2f4qhainnys251i9kwqhb848l3m9x")))

