(define-module (crates-io sk ip skip) #:use-module (crates-io))

(define-public crate-skip-0.0.0 (c (n "skip") (v "0.0.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0vq1mnqfcwrd4pwm12k3bbig11mq407bnnaj407xyfghb4x9lf53")))

(define-public crate-skip-0.2.1 (c (n "skip") (v "0.2.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c3kscvfcy3yrwwzmq5j2w5kr69cqfvm9bc6z4537z3178hxiss2") (r "1.74.1")))

