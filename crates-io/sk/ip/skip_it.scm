(define-module (crates-io sk ip skip_it) #:use-module (crates-io))

(define-public crate-skip_it-0.1.0 (c (n "skip_it") (v "0.1.0") (h "060azx4wmnkpg8yn1zai87jp8amkmsqpjcg4h907wmy7mp6ba1w4")))

(define-public crate-skip_it-0.1.1 (c (n "skip_it") (v "0.1.1") (h "0kgi7v57gnsjf2f8qfi9sd5ds345gq41gf3gjj4v5arq65bgrz47")))

