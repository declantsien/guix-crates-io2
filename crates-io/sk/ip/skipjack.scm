(define-module (crates-io sk ip skipjack) #:use-module (crates-io))

(define-public crate-skipjack-0.1.0 (c (n "skipjack") (v "0.1.0") (h "0pmw1spjgxz5jaqvp850iwcd88knpb7wm0syyr3sx38wj7byj1qj")))

(define-public crate-skipjack-0.2.0 (c (n "skipjack") (v "0.2.0") (h "0mxsf075j6ymv71bjvq8k6vl9dak4riigh23175pj9bk5gznri1i")))

