(define-module (crates-io sk ip skip_bom) #:use-module (crates-io))

(define-public crate-skip_bom-0.2.0 (c (n "skip_bom") (v "0.2.0") (h "158myg5sww0frsy113yc11mmagp4zhy4knx02qgr764h90alsczj")))

(define-public crate-skip_bom-0.2.1 (c (n "skip_bom") (v "0.2.1") (h "0jd1y37km1bni1w9sl4mx0ridfl5fr7hjl5a0i7lghfifs0by09g")))

(define-public crate-skip_bom-0.2.2 (c (n "skip_bom") (v "0.2.2") (h "0051hr7w8pn45d6vsc146vdv14jyz304vhilr1xagr3cyi9ckjby")))

(define-public crate-skip_bom-0.2.3 (c (n "skip_bom") (v "0.2.3") (h "1fzphwfiq2bmsf20z9sx48gds2lqlyksx3wb5bwh4k8llv8lfc28")))

(define-public crate-skip_bom-0.3.0 (c (n "skip_bom") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1z0c60y786cb809713fwka7c3zscdh8mhk6q45qm1ph6j0f0bz0i")))

(define-public crate-skip_bom-0.4.0 (c (n "skip_bom") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0mgya4cdiq7rg8dhj5krpgbg1jchlb4sswi82g23vf9wgrhcxhpr")))

(define-public crate-skip_bom-0.5.0 (c (n "skip_bom") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "10347x6y21hvi93d4fizy8j5dai1i33b5gcxsdn1bal5iq18c548")))

(define-public crate-skip_bom-0.5.1 (c (n "skip_bom") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "0jf5x1jfd0n2p9jql8ipsxjld9ifhz179bpgsk9g5xgmv3wssrdn")))

