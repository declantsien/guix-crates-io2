(define-module (crates-io sk ip skiplist-rs) #:use-module (crates-io))

(define-public crate-skiplist-rs-0.1.0 (c (n "skiplist-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ji8ddz7jxm631bbv5zlw03ds0awk15hih62c7b49hi3462a58aq")))

(define-public crate-skiplist-rs-0.1.1 (c (n "skiplist-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0g5mwbc3djpqkrc7m51ar0w82q4xbv4k8r7j4q9qq9a5fdyd00k1")))

(define-public crate-skiplist-rs-0.1.2 (c (n "skiplist-rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0p768j43gb8s7dj6z3cwy9589s03xhhjf3b6j3bv227c3gp7k2g7")))

(define-public crate-skiplist-rs-0.1.3 (c (n "skiplist-rs") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0r69f6l1zav6wwc4q4zi3rksmyy5pj0zarrcga7yd8gbwv4dwzdz")))

(define-public crate-skiplist-rs-0.1.4 (c (n "skiplist-rs") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17hhdnrdf1h6c9g895j86pfsaqjj2w483dqbjvpx3rc5lzwyyghf")))

(define-public crate-skiplist-rs-0.1.5 (c (n "skiplist-rs") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0xylan63x79svzb5rk5ijz4li740c2699b4fhgq0mspx4i2dcccz")))

