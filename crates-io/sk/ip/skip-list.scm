(define-module (crates-io sk ip skip-list) #:use-module (crates-io))

(define-public crate-skip-list-0.1.0 (c (n "skip-list") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0skm06z7n13w1dfbkj7xya30650pjrhi1zxdyw60ngmig99bb3al")))

(define-public crate-skip-list-0.1.1 (c (n "skip-list") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wzd9bv85xs10qgvffxrwn17m2cq1afq41a8r7578xz0gm4d1dkh")))

(define-public crate-skip-list-0.1.2 (c (n "skip-list") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02m9bn447myg1a2llzsp2xmrc3d166jh8lyjrrlas8a7fymm9vir")))

(define-public crate-skip-list-0.1.3 (c (n "skip-list") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16s6rsc0km1hv5ychvajv9pz4wcs1avfwfpwawdpy6z08xsx570v")))

