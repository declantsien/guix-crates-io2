(define-module (crates-io sk la sklauncher) #:use-module (crates-io))

(define-public crate-sklauncher-0.1.0 (c (n "sklauncher") (v "0.1.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lenient_bool") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1") (d #t) (k 0)) (d (n "skim") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "xdg") (r "^2.4") (d #t) (k 0)))) (h "13zm701kl0l27b8ahgbv8iqpj5amyw459q3nyd857z7ymps1cwdw")))

