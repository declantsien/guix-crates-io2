(define-module (crates-io sk in skinfile-gen) #:use-module (crates-io))

(define-public crate-skinfile-gen-0.1.0 (c (n "skinfile-gen") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q0lddwdkf6xv70gqb2ynj3pylmij0brhf11v1rgj3a6h4xqx5vn")))

(define-public crate-skinfile-gen-0.1.1 (c (n "skinfile-gen") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09s5iqdfvrbagh48hi7fs8bd0wgir81v0i11ng52k0v9xzkyvzmi")))

(define-public crate-skinfile-gen-0.2.0 (c (n "skinfile-gen") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09g2df3k8y9dr9k7sksa694q0d8nfgpm7a9zsbyg6q83kgcx7a86")))

