(define-module (crates-io sk in skin) #:use-module (crates-io))

(define-public crate-skin-0.1.0 (c (n "skin") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mimalloc") (r "^0.1.29") (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "1flgvb92326ibm2h69plg16zmn4aw55cgpy5ixygbk8pfa8800gn")))

