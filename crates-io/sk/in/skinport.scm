(define-module (crates-io sk in skinport) #:use-module (crates-io))

(define-public crate-skinport-0.0.0 (c (n "skinport") (v "0.0.0") (h "1dsngjd5b46v00i67i964iqpan3zw6nmdjkazkv733z2pr6317wl") (y #t)))

(define-public crate-skinport-0.1.0 (c (n "skinport") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "18zj8zmcdgjjicbc42xn26ffhi41x67jcxv1ibjykw66zj7d8nad") (f (quote (("sale-request") ("rest" "requests") ("requests" "item-request" "sale-request" "account-request") ("item-request") ("full" "rest") ("default" "full") ("account-request"))))))

(define-public crate-skinport-0.1.1 (c (n "skinport") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0way6wwphi6rggvzgrsppqamwlz51219nq0yw3as1q1fs050adr8") (f (quote (("sale-request") ("rest" "requests") ("requests" "item-request" "sale-request" "account-request") ("item-request") ("full" "rest") ("default" "full") ("account-request"))))))

