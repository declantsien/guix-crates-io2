(define-module (crates-io sk ee skeet) #:use-module (crates-io))

(define-public crate-skeet-0.1.0 (c (n "skeet") (v "0.1.0") (h "0n0b501k9fqi76mj1k6yvx4x594axymbi9jd6rdqcxvpycvm5q6l")))

(define-public crate-skeet-0.1.1 (c (n "skeet") (v "0.1.1") (d (list (d (n "bisky") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0d9786g1yda241h5rzqq48r583pfqzc1kv8mai1vdkdsxyq945r4")))

(define-public crate-skeet-0.1.2 (c (n "skeet") (v "0.1.2") (d (list (d (n "bisky") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "07ffap2wsgc4id75rinbfbx1287qzhmw1y2jgikkrrzy70mvxkc5")))

(define-public crate-skeet-0.1.4 (c (n "skeet") (v "0.1.4") (d (list (d (n "bisky") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1dm7kjh8d30ispj7n4fk85chw57drharm7jfsn4b6pdblqbsv253")))

(define-public crate-skeet-0.1.5 (c (n "skeet") (v "0.1.5") (d (list (d (n "bisky") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1amjpg1dfarjribym0vh11pqwr9rqlfwnk8w0lwshcvaz28bhfqi")))

(define-public crate-skeet-0.1.6 (c (n "skeet") (v "0.1.6") (d (list (d (n "bisky") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.7") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1i013jd5dzhrjsqvyj7ymrw5xii1ralj0prdd6p815slk07b9yd1")))

(define-public crate-skeet-0.1.8 (c (n "skeet") (v "0.1.8") (d (list (d (n "bisky") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.8") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "12ykim411ppcb5hrv8j0l0igk5fgp5847dzk1w1qv18hrh5mckir")))

(define-public crate-skeet-0.1.9 (c (n "skeet") (v "0.1.9") (d (list (d (n "bisky") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "127jyf8nmbrcd31d684z98cwj7l0s5prrh87zrb2ijc8v4bzn5z2")))

(define-public crate-skeet-0.1.10 (c (n "skeet") (v "0.1.10") (d (list (d (n "bisky") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0vqsvffd0ja2q630pwngj8kqpsis3c7n8m6paqhlbr4wni7ilr6a")))

(define-public crate-skeet-0.1.11 (c (n "skeet") (v "0.1.11") (d (list (d (n "bisky") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "grep-cli") (r "^0.1.10") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1748d7ngzg7ssasrpk0ski1jpmy3ch39grpqnivyndvys9n3i5lp")))

