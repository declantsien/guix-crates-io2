(define-module (crates-io sk ym skymd) #:use-module (crates-io))

(define-public crate-skymd-0.1.0 (c (n "skymd") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1z5r1zjghjn91pmkygj2m2wmaac46l7k2w1k4xfwim025qrsfcsl")))

(define-public crate-skymd-0.1.1 (c (n "skymd") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0gv5l5zbd2spsj6f6aadpk4sqrxi5yfb3l6xr9plkyfjmwj4jxjw")))

(define-public crate-skymd-0.1.2 (c (n "skymd") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "09nfjgdmd7f0ydpg8mwvq94vz2hmcz6930ww74pk22nm5dvxq80y")))

(define-public crate-skymd-0.1.3 (c (n "skymd") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "14a65xryl5k4xyxcf1fmnz0q3iyhxyym73982jd2kmfhdaizwrk9")))

(define-public crate-skymd-0.1.4 (c (n "skymd") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1k8q7y6y0gvil1p8k5hw44md7xnln64qp0jz0jnd2cr735lkprsz")))

(define-public crate-skymd-0.1.5 (c (n "skymd") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "1n8bijf9nfnj2mzs9y90aj2asdrrqqfnc6cxpa18lv5xhkw8j5am")))

(define-public crate-skymd-0.1.6 (c (n "skymd") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0zb9dk6iqhzg9lxpbaanwd0pi29a9hrwd1haxcabiqasz6kd4vbl")))

(define-public crate-skymd-0.1.7 (c (n "skymd") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "00v3l330a02wl4chj2ncdpsgs0bcpby40x8ijija8dax5lxsqrir")))

(define-public crate-skymd-0.1.8 (c (n "skymd") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0zbz5r7wmy6p0a9srllwg6qnhgd767sv9pgw3fkji7vk9qvvv53k")))

(define-public crate-skymd-0.1.9 (c (n "skymd") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "166ym5mhjm8klxv35nr74j1b6m40h6706zabrs7y0wqfdwvcjv2v")))

(define-public crate-skymd-0.1.10 (c (n "skymd") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.0") (d #t) (k 0)) (d (n "zeroize") (r "^1") (d #t) (k 0)))) (h "0ybhwngwphxmrgv32xdrav91nv5qykqhk2l2cffgch17k9mmhm5k")))

