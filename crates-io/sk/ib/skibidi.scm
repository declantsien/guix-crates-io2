(define-module (crates-io sk ib skibidi) #:use-module (crates-io))

(define-public crate-skibidi-0.1.0 (c (n "skibidi") (v "0.1.0") (h "00jj4fknwi9qfg568cjphljj0lvknfi9y5b7q09p8s68nk44r33q")))

(define-public crate-skibidi-0.1.1 (c (n "skibidi") (v "0.1.1") (h "0ry0v5ip90ylwfpbp19kvq8978x5ckp0mahd2zxw28rhawd02ggj")))

(define-public crate-skibidi-0.1.2 (c (n "skibidi") (v "0.1.2") (h "00jkwcmcc7indspsbz17z5gy42p3hdk5w0lyzgl5kzx2v9l3y8dm")))

