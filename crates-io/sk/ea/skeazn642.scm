(define-module (crates-io sk ea skeazn642) #:use-module (crates-io))

(define-public crate-SKEAZN642-0.1.0 (c (n "SKEAZN642") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0risdlk3hdc93b2ara3k8354lpb7j51shqq46xwk7p8x663lfzra") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-SKEAZN642-0.2.0 (c (n "SKEAZN642") (v "0.2.0") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1zs6ncig7cmg4915dr25g7g4ph9b9xpwhfpk6ya0vqhwl0qvi44j") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-SKEAZN642-0.2.1 (c (n "SKEAZN642") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.14") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "06va2dyhbhd431vy3f66rzkn44gmxx54y66nyyiicmaddvlkyihg") (f (quote (("rt" "cortex-m-rt/device"))))))

