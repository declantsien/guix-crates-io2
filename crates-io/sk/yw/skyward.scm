(define-module (crates-io sk yw skyward) #:use-module (crates-io))

(define-public crate-skyward-0.1.0 (c (n "skyward") (v "0.1.0") (h "0h2vz56iphfpcqgdkfc1hdmmk0x8n702a6gk44vydjpjpapnbjqa") (y #t)))

(define-public crate-skyward-0.1.1 (c (n "skyward") (v "0.1.1") (h "0pqq2dci87rzb4w6pj2mb481xr51dm5wb3v6qaz2jxz6mfdjxxwy") (y #t)))

