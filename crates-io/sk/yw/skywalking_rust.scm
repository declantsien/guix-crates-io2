(define-module (crates-io sk yw skywalking_rust) #:use-module (crates-io))

(define-public crate-skywalking_rust-0.1.0 (c (n "skywalking_rust") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.8.0") (d #t) (k 0)) (d (n "prost-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (f (quote ("net"))) (d #t) (k 2)) (d (n "tonic") (r "^0.5.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.2") (d #t) (k 1)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0v6b7rrvfcrjnanfif9ib8wbasf7y0r9wq3sjlcfbjx3hh4isfjm")))

