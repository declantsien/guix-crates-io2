(define-module (crates-io sk yc skycrane) #:use-module (crates-io))

(define-public crate-skycrane-0.0.0 (c (n "skycrane") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "serde_toml") (r "^0.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "wasmtime") (r "^17.0.0") (d #t) (k 0)))) (h "0i1i9caj47v2nry77y2anklq81jvfngggq01hfvcxx6rdf2iwjz1")))

