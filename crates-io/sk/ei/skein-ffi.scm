(define-module (crates-io sk ei skein-ffi) #:use-module (crates-io))

(define-public crate-skein-ffi-0.0.1 (c (n "skein-ffi") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1r6m9rn3d6y1k7d469r462hzbcyx6n7ac6ccc21jmhqkbpxak3db")))

(define-public crate-skein-ffi-0.5.0 (c (n "skein-ffi") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.26") (d #t) (k 1)))) (h "1il7ynbpsjyggzzqy8pmpdpfgh8scv478qa488j3jq96rpgkxfyp")))

