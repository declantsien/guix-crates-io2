(define-module (crates-io sk ei skein-hash) #:use-module (crates-io))

(define-public crate-skein-hash-0.3.0 (c (n "skein-hash") (v "0.3.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "block-padding") (r "^0.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "threefish-cipher") (r "^0.3.0") (d #t) (k 0)))) (h "1jnzqqmk9gh6wh75m97zrapj7m3dxq138a8v0yi1397690a9jr6d")))

(define-public crate-skein-hash-0.3.1 (c (n "skein-hash") (v "0.3.1") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "threefish-cipher") (r "^0.4") (d #t) (k 0)))) (h "0qiabpxa259g178l0dvxk8z64hakx0rg3c7kcbyg5i9s2fkgh9jp")))

