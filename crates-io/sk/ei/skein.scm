(define-module (crates-io sk ei skein) #:use-module (crates-io))

(define-public crate-skein-0.0.0 (c (n "skein") (v "0.0.0") (h "10414nxpv9jba41pm48lv5rdkbggh8wfqsj2f8yym9aki0bi7hcy")))

(define-public crate-skein-0.1.0 (c (n "skein") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "threefish") (r "^0.5.2") (k 0)))) (h "1kr5c5kb5pxzkgdfskyrhndww3nxcgg5qsxzjj8bcx7s2y92f4pl") (r "1.57")))

