(define-module (crates-io sk ei skeid) #:use-module (crates-io))

(define-public crate-skeid-0.1.0 (c (n "skeid") (v "0.1.0") (h "1h3q4wa3sqn53q11qr43wf0gbv7wswzx6i2a49h3980j8qnw5lqm")))

(define-public crate-skeid-0.2.0 (c (n "skeid") (v "0.2.0") (h "0mq39hx2ni3lzwwwba5nfsa2s3any76cz3mh41axnrr7vp9j40k5")))

(define-public crate-skeid-0.3.0 (c (n "skeid") (v "0.3.0") (h "0fwn3ff5pijjbgm2frdslfqwysf3z3cm7yamqy53h2dp12v30xyl")))

(define-public crate-skeid-0.4.0 (c (n "skeid") (v "0.4.0") (h "14z4qp9hjnks02rlg6mcks4vymp75gy1n9hd4g986kahs9rrj1cr")))

(define-public crate-skeid-0.5.0 (c (n "skeid") (v "0.5.0") (h "15fpxrmpbmrznmis7p3h2ddl1lzy0z39bizpyv74c4wlbzjr0qag")))

(define-public crate-skeid-0.6.0 (c (n "skeid") (v "0.6.0") (h "1g5qszwvhr8pk2iz4saqippbazsdkk96ljrz5v3ra92y9awwsf9x")))

(define-public crate-skeid-0.7.0 (c (n "skeid") (v "0.7.0") (h "1951r61q0nzwspwqvz2gy499sbk3vnxwg8zwy7daasmxwy47fggl")))

(define-public crate-skeid-0.8.0 (c (n "skeid") (v "0.8.0") (h "1l9r2ydbhpk0adl6y0331q2i5zcj2blkxcvljy6s80xv43gaz3hm")))

(define-public crate-skeid-0.9.0 (c (n "skeid") (v "0.9.0") (h "00gxmwi65phff0a3w8n9a66qds2blyina4dnc6hkq0354iffyfpx")))

(define-public crate-skeid-0.10.0 (c (n "skeid") (v "0.10.0") (h "01ijnghs7wv901w7ibyr9jjfbsxid35v71qs7cva710ikysaz09y")))

(define-public crate-skeid-0.11.0 (c (n "skeid") (v "0.11.0") (h "194fnka4lj37zz7pq0jb758dip0s86qbni551aadlyn3qsjw4yq0")))

(define-public crate-skeid-0.12.0 (c (n "skeid") (v "0.12.0") (h "054w5dann3zbhqhqlh7v5n9njnw8dnvv4v69b4rndcn5a1pvqzwv")))

(define-public crate-skeid-0.13.0 (c (n "skeid") (v "0.13.0") (h "1nvgz1w9506m6c53pfvnp8107m3c07f95b8y7ays7gagqv3965lp")))

(define-public crate-skeid-0.14.0 (c (n "skeid") (v "0.14.0") (h "0j4h6h0jaq23dbnq45rzqlmdbjimr11w2yhgqpg6j5zaj4b11j45")))

(define-public crate-skeid-0.15.0 (c (n "skeid") (v "0.15.0") (h "0lfp22sml3janflplqx2ch37r32vsfi5n1llrjps6pg3kzvjg9y4")))

(define-public crate-skeid-0.16.0 (c (n "skeid") (v "0.16.0") (h "1ajvkqwafcxn1pdq24f4sc4rh0hnhq2aq4vmc3250ard2qwfc25y")))

(define-public crate-skeid-0.17.0 (c (n "skeid") (v "0.17.0") (h "1kfkrkqdm7bjr3jsl731f1b16s007vsfsnn869wasfpbjalimikk")))

(define-public crate-skeid-0.18.0 (c (n "skeid") (v "0.18.0") (h "0y53hka6hln73sjbdqrpnw0d390lad8bmpvy9n7cafwbnn6aym30")))

(define-public crate-skeid-0.19.0 (c (n "skeid") (v "0.19.0") (h "12xpj630irlnvic5vs9l6lzd411khf9vwabky9lid2zmslc3dkg7")))

(define-public crate-skeid-0.20.0 (c (n "skeid") (v "0.20.0") (h "135mcqpav78n50sq1j7z6bab8jzbaz220jfpqss5jsnir1328hiv")))

(define-public crate-skeid-0.20.1 (c (n "skeid") (v "0.20.1") (h "0im59nymf07z6l30chwymzzvdbcwy5yab9992dp7z5g14k297656")))

(define-public crate-skeid-0.20.2 (c (n "skeid") (v "0.20.2") (h "0f1nx73fsmra0v2j282m0lk5b6xpli725wvmvsla7ydlnkhhhmgl")))

