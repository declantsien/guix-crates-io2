(define-module (crates-io pf v- pfv-cli) #:use-module (crates-io))

(define-public crate-pfv-cli-0.1.0 (c (n "pfv-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0mggqqnfb0s598mvqlnwa0fki30z0hirvziy5irkyzmrhrpixlsz")))

(define-public crate-pfv-cli-0.1.1 (c (n "pfv-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.4") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "102q5m3gghjmmnjzf7kxn3q5pnywp7g9ynlf4rvs77xb9f62c1dw")))

(define-public crate-pfv-cli-0.1.2 (c (n "pfv-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.5") (d #t) (k 0)))) (h "164bnykk546bqpjjwdwrb883wj15w7gmaabs9h1pqar4822jynjy")))

(define-public crate-pfv-cli-0.1.3 (c (n "pfv-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.6") (d #t) (k 0)))) (h "1yplf4ms2m3y02fl4jdgvwmayscah735hws82d2a91xpzq72jfj3")))

(define-public crate-pfv-cli-0.1.4 (c (n "pfv-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.7") (d #t) (k 0)))) (h "0baz1wr7r348z80ln02w3ikbav0awvhjbjjsvs8lm7v55a5agchj")))

(define-public crate-pfv-cli-0.1.5 (c (n "pfv-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.1.7") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "y4m") (r "^0.8.0") (d #t) (k 0)))) (h "1xjz7qz0h4g3k4yl4wvysxp7q2xs8wav9skz3rbz0bvagcm879y5")))

(define-public crate-pfv-cli-0.1.6 (c (n "pfv-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "y4m") (r "^0.8.0") (d #t) (k 0)))) (h "1f6bnjbfk69q6kxcbv2fbz39x74ibs8d54nmfbfpz6a9ik26pd6h")))

(define-public crate-pfv-cli-0.1.7 (c (n "pfv-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "pfv-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "y4m") (r "^0.8.0") (d #t) (k 0)))) (h "1dc4s27k6fv5ilmbvg4642q8788jb1798nkxiqzgfphpcxkg7j5z")))

