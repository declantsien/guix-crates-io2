(define-module (crates-io pf ta pftables-rs) #:use-module (crates-io))

(define-public crate-pftables-rs-0.1.0 (c (n "pftables-rs") (v "0.1.0") (h "06qjsfpi7v8865qkqmcrhjkdx7l3296kjfm7xnncvs8klpv0rill")))

(define-public crate-pftables-rs-0.1.1 (c (n "pftables-rs") (v "0.1.1") (h "1l5xg044rjjc8svr9z1pdbwrdbbk8fvhwr97b4kyk860ci87nmhq")))

