(define-module (crates-io pf ac pfacts) #:use-module (crates-io))

(define-public crate-pfacts-0.1.0 (c (n "pfacts") (v "0.1.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0a8ndncy7695fia6b717smvgkyxs6h0kyla9sivg2c2c6bdypaw3")))

