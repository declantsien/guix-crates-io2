(define-module (crates-io pf et pfetch-logo-parser) #:use-module (crates-io))

(define-public crate-pfetch-logo-parser-0.1.0 (c (n "pfetch-logo-parser") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "12xyqdb1m38xbjddscv00bwrll59hay2c642jkmnfjl0lflknznq") (s 2) (e (quote (("proc-macro" "dep:proc-macro2" "dep:quote"))))))

