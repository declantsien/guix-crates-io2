(define-module (crates-io pf et pfetch-extractor) #:use-module (crates-io))

(define-public crate-pfetch-extractor-0.1.0 (c (n "pfetch-extractor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1m3aafb7g44m6brv4d4iyqicii4wy7lgd35mdw7gc8q79na1qi06")))

(define-public crate-pfetch-extractor-0.1.1 (c (n "pfetch-extractor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1p0zv8qbvkv15ylvw3n0kxkn91p77c0if6qxd2ghs2rzg4hf6xlg")))

(define-public crate-pfetch-extractor-0.1.2 (c (n "pfetch-extractor") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1h8ck75n7mj5nyn952ck23544caasrq6i7bbpmccmhki78lh6l9d")))

(define-public crate-pfetch-extractor-0.1.3 (c (n "pfetch-extractor") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0nlyc7gjabqjqd4kcx74fc2421z5p2agynz7my3mlbfswb01hl2l")))

(define-public crate-pfetch-extractor-0.1.4 (c (n "pfetch-extractor") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0fgl30sdq0krghszx4xlgk3a6yxq6yjlpcargjn0p1i5yxjisizh")))

(define-public crate-pfetch-extractor-0.1.5 (c (n "pfetch-extractor") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "16wxxadshyivm39sil1wgxxmvqw4x1yqjpccnvcx72a27ml4cakw")))

(define-public crate-pfetch-extractor-0.2.0 (c (n "pfetch-extractor") (v "0.2.0") (d (list (d (n "pfetch-logo-parser") (r "^0.1.0") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "14hnr1mqawxqkwzvipi3zm9flryvpc959vpd2q6s83s1xmvz1k5x")))

(define-public crate-pfetch-extractor-0.2.1 (c (n "pfetch-extractor") (v "0.2.1") (d (list (d (n "pfetch-logo-parser") (r "^0.1.0") (f (quote ("proc-macro"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)))) (h "0sdnf15xw50akgvga6yjlvra4rwjpvbs7s6xfvhncsx786k6xh97")))

