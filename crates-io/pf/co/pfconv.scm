(define-module (crates-io pf co pfconv) #:use-module (crates-io))

(define-public crate-pfconv-0.1.0 (c (n "pfconv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "polars") (r "^0.27.2") (f (quote ("lazy" "parquet"))) (d #t) (k 0)))) (h "0jx7nl9gbrbjw85x0b5knddkvn7c84zp8y9r5wj3ms9grcgacrr2")))

(define-public crate-pfconv-0.1.1 (c (n "pfconv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "polars") (r "^0.27.2") (f (quote ("lazy" "parquet"))) (d #t) (k 0)))) (h "0ydq135p88qai6bjmh0zd355v2cis65jcq0hb3j6m2dx66bz36f7")))

