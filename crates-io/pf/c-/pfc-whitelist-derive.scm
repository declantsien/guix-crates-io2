(define-module (crates-io pf c- pfc-whitelist-derive) #:use-module (crates-io))

(define-public crate-pfc-whitelist-derive-0.1.0 (c (n "pfc-whitelist-derive") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.7") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)))) (h "18axps5gyaf23q5aqk1pf732ps4fr1gpjwnhhfix54h73fgx734y")))

(define-public crate-pfc-whitelist-derive-1.4.0 (c (n "pfc-whitelist-derive") (v "1.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)))) (h "0iqwzhwwv2qlzv51lcq6ij2gqzv8sl0f1i1l5i94m493830zany0") (r "1.73.0")))

(define-public crate-pfc-whitelist-derive-1.5.0 (c (n "pfc-whitelist-derive") (v "1.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "08d7ykcl9j5fa9cw9ia3anp9nwqm9f94qaqwnm4zvi3jb511l4p0") (r "1.73.0")))

(define-public crate-pfc-whitelist-derive-2.0.0 (c (n "pfc-whitelist-derive") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.1") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "17sps3l4329v9gkpnb3jxcmjsvmwj7by6hk3yj3m21lv2y5wi3lm") (r "1.73.0")))

(define-public crate-pfc-whitelist-derive-1.5.1 (c (n "pfc-whitelist-derive") (v "1.5.1") (d (list (d (n "cosmwasm-schema") (r "^1.5.4") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "0qz3pqzp2zyzf9c0wxxsb7a30q8jchp5qnvfcdha56443racryam") (r "1.73.0")))

