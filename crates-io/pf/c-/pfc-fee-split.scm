(define-module (crates-io pf c- pfc-fee-split) #:use-module (crates-io))

(define-public crate-pfc-fee-split-0.1.0 (c (n "pfc-fee-split") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw20") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "05amshb98rca9wwgynxvp3nzmrdaz9g81jd75ada559vrb42n2rj") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-pfc-fee-split-0.1.1 (c (n "pfc-fee-split") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw20") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "06fh7rrqazy2l8qzhz6y8ac8lgqljm62ls5hxkyknwimcapzj0p2") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-pfc-fee-split-0.2.3 (c (n "pfc-fee-split") (v "0.2.3") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw20") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ny6nphzshmblpc2hav6ixg0vxplzs6ckg4pvymnnz0vnq4ivrz5") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-pfc-fee-split-0.2.5 (c (n "pfc-fee-split") (v "0.2.5") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0mxbm8yi3n731ip181a75sazmrdzf7pagvdjdmfv8fpv0mcck6v5") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-pfc-fee-split-1.5.0 (c (n "pfc-fee-split") (v "1.5.0") (d (list (d (n "cosmwasm-std") (r "^1.5.3") (f (quote ("cosmwasm_1_2" "iterator" "stargate"))) (d #t) (k 0)) (d (n "cw20") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (k 0)))) (h "1a53jvz47rvfqd0y0mjii8adrvdamdfhg05fb26zz1gkq0536qnk") (r "1.73.0")))

