(define-module (crates-io pf c- pfc-dust-collector-derive) #:use-module (crates-io))

(define-public crate-pfc-dust-collector-derive-0.1.0 (c (n "pfc-dust-collector-derive") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.7") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)))) (h "0vdk5zjrdm0zwky8x0ppm38hhi0c0xqcrrb98m2jlmla2sc8wbyp")))

(define-public crate-pfc-dust-collector-derive-1.4.0 (c (n "pfc-dust-collector-derive") (v "1.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.5.3") (d #t) (k 2)) (d (n "cw-ownable") (r "^0.5.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.108") (d #t) (k 0)))) (h "0i55ay2kbs18swas6vdw5ma198mkpz326bgyvlnhbwl1s18da5wy") (r "1.73.0")))

