(define-module (crates-io pf c- pfc-dust-collector) #:use-module (crates-io))

(define-public crate-pfc-dust-collector-0.1.0 (c (n "pfc-dust-collector") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-ownable") (r "^0.5.0") (d #t) (k 0)) (d (n "cw-ownable-derive") (r "^0.5.0") (d #t) (k 0)) (d (n "pfc-dust-collector-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "pfc-whitelist") (r "^0.1.0") (d #t) (k 0)) (d (n "pfc-whitelist-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dhlsmlxjyr4hzdxppc7bqcy82zpfm7rhzy0a73p5lpwrs1vpswx") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

