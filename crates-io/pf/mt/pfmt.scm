(define-module (crates-io pf mt pfmt) #:use-module (crates-io))

(define-public crate-pfmt-0.1.0 (c (n "pfmt") (v "0.1.0") (d (list (d (n "galvanic-assert") (r "^0.8.0") (d #t) (k 2)) (d (n "galvanic-test") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0p7q5yw6pj2ndsblrmd1vynm0lgp19fysywjssi4262y3b8rwy68")))

(define-public crate-pfmt-0.2.0 (c (n "pfmt") (v "0.2.0") (d (list (d (n "galvanic-assert") (r "^0.8.0") (d #t) (k 2)) (d (n "galvanic-test") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "09akj5794qk69nxwq7dw0c6n9nkc9kx1iakjk90277v7lijx074m")))

(define-public crate-pfmt-0.3.0 (c (n "pfmt") (v "0.3.0") (d (list (d (n "galvanic-assert") (r "^0.8.0") (d #t) (k 2)) (d (n "galvanic-test") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "05hh10zzsyb1nbs2xmps0px56sjaw8y8h71nadsmjyxw34fgzmw8")))

(define-public crate-pfmt-0.4.0 (c (n "pfmt") (v "0.4.0") (d (list (d (n "galvanic-assert") (r "^0.8.0") (d #t) (k 2)) (d (n "galvanic-test") (r "^0.1.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0g7cg88g1bzc3gcr1a10m6n6dakl24zkzzh1rxvnwmizljq6vagq")))

