(define-module (crates-io pf du pfdump) #:use-module (crates-io))

(define-public crate-pfdump-0.1.0 (c (n "pfdump") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "procfs") (r "^0.9.1") (d #t) (k 0)))) (h "16v422qx8nkgr703p40rbi7lpm0p4acw122pqcpccchfrrds2yqm")))

