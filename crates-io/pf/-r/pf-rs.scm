(define-module (crates-io pf -r pf-rs) #:use-module (crates-io))

(define-public crate-pf-rs-13.0.1 (c (n "pf-rs") (v "13.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "15cf6l7pwk75jy1igq63rg0rf2sz89dpkik62llpn753xg0lqppk") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-13.0.2 (c (n "pf-rs") (v "13.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "1jrmp34vvbjppb2njpmyl6lshw6rwwldsdliv0mnsyfp496rq9wk") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-13.0.3 (c (n "pf-rs") (v "13.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "1arnz428biw364q8q585s1nz3lf120rx00da71z3fi17fi5vspkp") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-13.0.4 (c (n "pf-rs") (v "13.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "0f30zqww16yr27524mrdbz3f4b050hpxafbi9vlv99xhr5ajcm74") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-13.2.0 (c (n "pf-rs") (v "13.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "00wspxycg7w3w219gy2xkzdc96q4qpfl4s1vx48i58gsxrkq61ib") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-13.2.1 (c (n "pf-rs") (v "13.2.1") (d (list (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "101yhbkdpjy8m4z76zk1w3z3b8n029yd3b3my1ipjixjv9mx6bq9") (f (quote (("log_to_stdout") ("default")))) (y #t)))

(define-public crate-pf-rs-0.2.2 (c (n "pf-rs") (v "0.2.2") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (k 0)))) (h "0j3w1a2bxbyivshqn20k68clij478vkj7krfxrinkjdg7cs3ghy1") (f (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

(define-public crate-pf-rs-0.2.3 (c (n "pf-rs") (v "0.2.3") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (k 0)))) (h "0pfbc3cxcxidn3b4d5mkyqg7gcl5vs6pbz03bgc8xzs7na2208q9") (f (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

(define-public crate-pf-rs-0.2.4 (c (n "pf-rs") (v "0.2.4") (d (list (d (n "nix") (r "^0.27") (f (quote ("fs"))) (d #t) (k 0)))) (h "1920x6j45a0z498cqish033lcqryaf3ca0kn74291653zq2c5lb6") (f (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

