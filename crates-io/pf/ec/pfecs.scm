(define-module (crates-io pf ec pfecs) #:use-module (crates-io))

(define-public crate-pfecs-0.1.0 (c (n "pfecs") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "1p24r53n2cza10cbvmhqdj4q1q86yyjgkajwyzykvwdsgln880qy") (y #t)))

