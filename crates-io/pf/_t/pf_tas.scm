(define-module (crates-io pf _t pf_tas) #:use-module (crates-io))

(define-public crate-pf_tas-0.1.0 (c (n "pf_tas") (v "0.1.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "vulkano") (r "^0.3") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3") (d #t) (k 1)) (d (n "vulkano-win") (r "^0.3") (d #t) (k 0)) (d (n "vulkano_text") (r "^0.1") (d #t) (k 0)) (d (n "winit") (r "^0.5") (d #t) (k 0)))) (h "0k9l4yh2y23v7l6p82rcicg7qaas7jyygyyy5nqm9x5aqgg1sjnz") (y #t)))

(define-public crate-pf_tas-0.1.1 (c (n "pf_tas") (v "0.1.1") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "vulkano") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.5") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.3") (d #t) (k 1)) (d (n "vulkano-win") (r "^0.5") (d #t) (k 0)) (d (n "vulkano_text") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.7") (d #t) (k 0)))) (h "0kdyi91dj8xv6y9pmgjm5786sqyn447yxvhfgyqva9gdcqms39s6") (y #t)))

