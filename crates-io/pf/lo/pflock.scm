(define-module (crates-io pf lo pflock) #:use-module (crates-io))

(define-public crate-pflock-0.1.0 (c (n "pflock") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1k5prf4pipdcklpb86s043c605g01n5a63r80ldf23ncj04rwrj3")))

(define-public crate-pflock-0.1.1 (c (n "pflock") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1n4bna7yyn0q86iby5dsdfjlhjl8kn1mp4y998fzmdny4ivagx2y")))

(define-public crate-pflock-0.1.2 (c (n "pflock") (v "0.1.2") (d (list (d (n "lock_api") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0w9i5r0zyjf0ax3dkvpvv0r44fr4vy2sak7rhciibjvpyxjm33c3")))

(define-public crate-pflock-0.1.3 (c (n "pflock") (v "0.1.3") (d (list (d (n "lock_api") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0diw2a2i65jxkgcywci385gdf0675bxcfigj81bhs48i8xhrmkgc")))

(define-public crate-pflock-0.2.0 (c (n "pflock") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0zp4nbh58dldwd07wzlqi8nb7i2y2chmc0lmidz12sdlhizhvcsc")))

