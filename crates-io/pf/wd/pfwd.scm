(define-module (crates-io pf wd pfwd) #:use-module (crates-io))

(define-public crate-pfwd-0.1.0 (c (n "pfwd") (v "0.1.0") (h "1n8gf4gp5402dfsn2s203dkrzdmmla6ia85f9932xivk77bi2pf6")))

(define-public crate-pfwd-0.1.1 (c (n "pfwd") (v "0.1.1") (h "0la5mv7lm4a9zbn8ix8zlgmfmwb5bm4kppzbj4hyq3z1qnp0h6dc")))

