(define-module (crates-io pf or pformat_macro) #:use-module (crates-io))

(define-public crate-pformat_macro-0.0.1 (c (n "pformat_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1w1jxh7klizmbmn0kpz72l894n2mkrfz1a63r3yyasf93galmnzi")))

