(define-module (crates-io pf ap pfapack) #:use-module (crates-io))

(define-public crate-pfapack-0.1.0 (c (n "pfapack") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)) (d (n "pfapack-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1r9xmn137i7jqisr5jvijp1fiff8cb7i83qimmwhh389cw8h9lcy")))

(define-public crate-pfapack-0.2.0 (c (n "pfapack") (v "0.2.0") (d (list (d (n "assert") (r "^0.7.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)) (d (n "pfapack-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "13afaf39cxcps557hwgxf60cakasyr4cj3waj05wsmpriiczwpg8")))

