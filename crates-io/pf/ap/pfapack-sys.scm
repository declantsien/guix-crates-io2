(define-module (crates-io pf ap pfapack-sys) #:use-module (crates-io))

(define-public crate-pfapack-sys-0.1.0 (c (n "pfapack-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "1h72zndzjcald5rcglcbf6b1jw9yjqpvwgghz6k5mx2dkr8gx9gm") (l "pfapack")))

(define-public crate-pfapack-sys-0.1.1 (c (n "pfapack-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "1r2iw8nn0paqz1f1f6ykvqlvnj31xbb83bbx74836f8bkc7zy68s") (l "pfapack")))

