(define-module (crates-io pf he pfhe) #:use-module (crates-io))

(define-public crate-pfhe-0.0.1 (c (n "pfhe") (v "0.0.1") (d (list (d (n "crypto-bigint") (r "^0.5.3") (f (quote ("rand"))) (d #t) (k 0)) (d (n "impl_ops") (r "^0.1.1") (d #t) (k 0)))) (h "1z9q8yk5zzm3jlmbl3ycg7jar8y06g7jzzisczfmaz7r04xk329k")))

