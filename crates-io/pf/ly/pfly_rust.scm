(define-module (crates-io pf ly pfly_rust) #:use-module (crates-io))

(define-public crate-pfly_rust-0.1.0 (c (n "pfly_rust") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (f (quote ("unix"))) (d #t) (k 0)))) (h "0f84vjzdyq3xkbdc9zjcvs6lf1giig9ya9l2bpqlp2qzigbk1cg7")))

(define-public crate-pfly_rust-0.1.1 (c (n "pfly_rust") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (f (quote ("unix"))) (d #t) (k 0)))) (h "0zdhx7nv8z075yqk6pbwizsdkg77df7p3nfdy3dim6jjirj8cwg9")))

