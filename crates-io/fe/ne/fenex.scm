(define-module (crates-io fe ne fenex) #:use-module (crates-io))

(define-public crate-fenex-0.1.0 (c (n "fenex") (v "0.1.0") (h "15izr8bm95amwbbp1qmj6gymb80vv9qx4c452n1w7b2kygkmckfa")))

(define-public crate-fenex-0.1.1 (c (n "fenex") (v "0.1.1") (h "0c8vgm14388n6ibrn5fx5gdvg62phl5znb53xmmzxli4hhh4m8v8")))

(define-public crate-fenex-0.1.2 (c (n "fenex") (v "0.1.2") (h "17vmxsgyqbvisqr42lbbgkafrmvjqm8iq650z12mxv3g9ry0vign")))

(define-public crate-fenex-0.1.3 (c (n "fenex") (v "0.1.3") (h "0n35cf1ydr2js4akfk8iv11b45cagx9xa6gdygjr8ghjnrkvhd9v")))

(define-public crate-fenex-0.1.4 (c (n "fenex") (v "0.1.4") (h "0x9p7hzhkyl4z2c5wn6md1dympxhqwd6xjvd8lib6zxzmqn2wqmy")))

