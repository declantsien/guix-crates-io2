(define-module (crates-io fe n4 fen4) #:use-module (crates-io))

(define-public crate-fen4-0.1.0 (c (n "fen4") (v "0.1.0") (h "1a8m3s3plc2x1y4jy8yk5zanlr70drvq9wrlwfqir07a506ls5p7")))

(define-public crate-fen4-0.2.0 (c (n "fen4") (v "0.2.0") (h "076f9kiqwkncz9qlizd4h6lg89zwgcc9fns9b2fri04pwdppzgab")))

(define-public crate-fen4-0.3.0 (c (n "fen4") (v "0.3.0") (d (list (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0dxsj8pj765vhl4fmvfdsrbaa0bbdhscnclizji4n55r6yncm1dq")))

(define-public crate-fen4-0.4.0 (c (n "fen4") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05vghfa8dgwbr8pyna65r694am47nh96d8r3phqljmj5aljl8fdi")))

(define-public crate-fen4-0.5.0 (c (n "fen4") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0if30scz65m6fw4a4vsibvf9s853sl81phx2skm1j6ax3h1ra05l")))

(define-public crate-fen4-0.6.0 (c (n "fen4") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xh06bwhhpaj7h8nrgd157a4lk1f9xc40c1ljlaqx0qiw7jpkcrv")))

(define-public crate-fen4-0.7.0 (c (n "fen4") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02n19lsyv1dy8893qg04gizkc2bgvp82mrdlk3l57zf3977a91nw")))

