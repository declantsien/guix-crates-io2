(define-module (crates-io fe _r fe_rtos) #:use-module (crates-io))

(define-public crate-fe_rtos-0.1.0 (c (n "fe_rtos") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cortex-m") (r "^0.6") (d #t) (t "cfg(target_arch = \"arm\")") (k 0)) (d (n "crossbeam-queue") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "fe_osi") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)))) (h "1gfxp75xnrzw6ilq0a3j06hyrl1zga4gc52hlj0vzjswg9l3gcdf")))

(define-public crate-fe_rtos-0.1.1 (c (n "fe_rtos") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cortex-m") (r "^0.6") (d #t) (t "cfg(target_arch = \"arm\")") (k 0)) (d (n "crossbeam-queue") (r "^0.2") (f (quote ("alloc"))) (k 0)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "fe_osi") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)))) (h "0x4dcy419c9d4r5is4lnqsafmmqnnwwvnps59civhp1sfs8j9bdg")))

