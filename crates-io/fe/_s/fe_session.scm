(define-module (crates-io fe _s fe_session) #:use-module (crates-io))

(define-public crate-fe_session-0.4.0 (c (n "fe_session") (v "0.4.0") (d (list (d (n "cookie") (r "^0.2.2") (k 0)) (d (n "hyper") (r "^0.8.0") (k 0)) (d (n "hyper") (r "^0.8.0") (k 2)) (d (n "iron") (r "^0.2.6") (k 0)) (d (n "iron") (r "^0.2.6") (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "oven") (r "^0.2.16") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1nw4xkfss11y5646wv35bkbd9cjc6kasbqm5pxnyrs8m5ndar148")))

(define-public crate-fe_session-0.4.1 (c (n "fe_session") (v "0.4.1") (d (list (d (n "cookie") (r "^0.2.2") (k 0)) (d (n "hyper") (r "^0.8") (k 0)) (d (n "hyper") (r "^0.8") (k 2)) (d (n "iron") (r "^0.3") (k 0)) (d (n "iron") (r "^0.3") (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "oven") (r "^0.3.0") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1qmhlqmcv7i6pixvz816jgmgqd78mpq83r80xkxrj0w708xsaf2q")))

(define-public crate-fe_session-0.4.2 (c (n "fe_session") (v "0.4.2") (d (list (d (n "cookie") (r "^0.2.3") (k 0)) (d (n "hyper") (r "^0.8") (k 0)) (d (n "hyper") (r "^0.8") (k 2)) (d (n "iron") (r "^0.3") (k 0)) (d (n "iron") (r "^0.3") (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "oven") (r "^0.3.0") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "044kvb9xsqd5m1hyz5z4r7gdf3p2y6rg3s0k5lnfjn3kp32i9izq")))

(define-public crate-fe_session-0.5.0 (c (n "fe_session") (v "0.5.0") (d (list (d (n "cookie") (r "^0.2.3") (k 0)) (d (n "hyper") (r "^0.9") (k 0)) (d (n "hyper") (r "^0.9") (k 2)) (d (n "iron") (r "^0.4") (k 0)) (d (n "iron") (r "^0.4") (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "oven") (r "^0.3.0") (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0pkksqa24njdgfi28b1x273ngsd16d0ws4772jg8mgjfsas82yr0")))

