(define-module (crates-io fe nc fencer) #:use-module (crates-io))

(define-public crate-fencer-0.1.0 (c (n "fencer") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13sfivjjwybfk51k7rdlcfkphlddz1zdics3zhng2k0nv2n1jkch")))

(define-public crate-fencer-1.0.0 (c (n "fencer") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xa0g82bix3plv64rlhjndl3bfsgcz7xryh2k5f0nvjca91g2rjh")))

(define-public crate-fencer-1.1.0 (c (n "fencer") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18iggs49pd2q6dq7k5bkh6q9na347pfcb06xrw2km536hwzmnfic")))

