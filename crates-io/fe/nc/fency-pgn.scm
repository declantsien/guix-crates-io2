(define-module (crates-io fe nc fency-pgn) #:use-module (crates-io))

(define-public crate-fency-pgn-0.1.0 (c (n "fency-pgn") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0d9m0srhyilzw5na0mmw3azwrkxr7sqd8qkhkngm6p1hypzsw65d")))

(define-public crate-fency-pgn-0.1.1 (c (n "fency-pgn") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0asl5822iz7x3kk9xm848l4xs4snsp9ghgr18d26msh2qshlfmkf")))

(define-public crate-fency-pgn-0.1.2 (c (n "fency-pgn") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0nxv6nsjjkbjxrdfdjqs69kafqvll712i89iymsiymlxw5qfi60s")))

(define-public crate-fency-pgn-0.1.3 (c (n "fency-pgn") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0j5fpvh4mxzizz9fr3lfs963dr3fwv2d6f1nzphc9ja5pnlrcbcj")))

(define-public crate-fency-pgn-0.1.4 (c (n "fency-pgn") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0r6961f9d69kkazw1970yhizj54p836fs99i0qa0x1g8qqq0r6yk")))

(define-public crate-fency-pgn-0.2.0 (c (n "fency-pgn") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.19.2") (f (quote ("abi3-py311"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "09psx5653dj6py9f9vkz4i405mdwm0hc5rbgxp7v05vk6idqqbkb")))

