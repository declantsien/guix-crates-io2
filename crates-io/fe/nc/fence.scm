(define-module (crates-io fe nc fence) #:use-module (crates-io))

(define-public crate-fence-0.0.1 (c (n "fence") (v "0.0.1") (h "0xh8vy10m06kkhh8ghkfapv359z02f18as7hjpvnpbs4c25gyd14")))

(define-public crate-fence-0.0.2 (c (n "fence") (v "0.0.2") (h "1qzl11l0clq8yl5psjmml6yblc3gnsv58ripx24yam9y85vdwwa0")))

