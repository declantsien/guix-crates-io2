(define-module (crates-io fe m_ fem_2d) #:use-module (crates-io))

(define-public crate-fem_2d-0.1.0 (c (n "fem_2d") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "1xaypr4hak3zm9cp00js6247i63cz5rlr32x3rifdqj47s8yzs0h") (f (quote (("max_ortho_basis") ("json_export") ("default" "json_export"))))))

(define-public crate-fem_2d-0.2.0 (c (n "fem_2d") (v "0.2.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "02g7i823kcgclgf7xinw5zyxw8gc5mp41xdzfch4pj66aa02xkz3") (f (quote (("max_ortho_basis") ("json_export") ("default" "json_export"))))))

(define-public crate-fem_2d-0.2.1 (c (n "fem_2d") (v "0.2.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "025al3aqflc1hrs3194q9mbn385nfmxzz0pq76x50hznmli9af5z") (f (quote (("max_ortho_basis") ("json_export") ("default" "json_export"))))))

(define-public crate-fem_2d-0.2.2 (c (n "fem_2d") (v "0.2.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)))) (h "1rsfzp1q18nr6f5g0i19s3g0n07w159053zvqd9qdjb87a6n6sg8") (f (quote (("max_ortho_basis") ("json_export") ("default" "json_export"))))))

