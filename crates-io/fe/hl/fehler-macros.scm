(define-module (crates-io fe hl fehler-macros) #:use-module (crates-io))

(define-public crate-fehler-macros-0.1.0-alpha.0 (c (n "fehler-macros") (v "0.1.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("default" "fold" "full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.0") (d #t) (k 0)))) (h "0x4a38qjm2qijw7r4z2zggmakgyq1467f0c063564c09ggwdwi6x")))

(define-public crate-fehler-macros-1.0.0-alpha.1 (c (n "fehler-macros") (v "1.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("default" "fold" "full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.0") (d #t) (k 0)))) (h "1x57z1d7sbf4hixcd56l7gpy9a0d2mk5l423b86z35nraxrh7gsv")))

(define-public crate-fehler-macros-1.0.0-alpha.2 (c (n "fehler-macros") (v "1.0.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("default" "fold" "full"))) (d #t) (k 0)))) (h "118wvakricbg3nzi1lv44439q25mawr6wbxrsz7v5192jdamapcs")))

(define-public crate-fehler-macros-1.0.0 (c (n "fehler-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("default" "fold" "full" "parsing"))) (d #t) (k 0)))) (h "1y808jbwbngji40zny0b0dvxsw9a76g6fl1c5qigmfsy0jqsrdfc")))

