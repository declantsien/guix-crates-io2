(define-module (crates-io fe hl fehler) #:use-module (crates-io))

(define-public crate-fehler-1.0.0-alpha.0 (c (n "fehler") (v "1.0.0-alpha.0") (d (list (d (n "fehler-macros") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1hq01fnaxlfsd15rai40f4x0sxa622a99i43ibwaqpawi3wzh7bm")))

(define-public crate-fehler-1.0.0-alpha.1 (c (n "fehler") (v "1.0.0-alpha.1") (d (list (d (n "fehler-macros") (r "^1.0.0-alpha.1") (d #t) (k 0)))) (h "1xf3sj75n1jhq4nblis2kqil4ra0fjbdmls0lzn7y0d9bf0s3879")))

(define-public crate-fehler-1.0.0-alpha.2 (c (n "fehler") (v "1.0.0-alpha.2") (d (list (d (n "fehler-macros") (r "^1.0.0-alpha.2") (d #t) (k 0)))) (h "0kjkyyyl46ys0154vl7w71q4q5b4s372kx7l68d9c6c08acbw36i")))

(define-public crate-fehler-1.0.0 (c (n "fehler") (v "1.0.0") (d (list (d (n "fehler-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0d9nk0nimhrqhlwsm42kmg6bwhfqscnfddj70xawsa50kgj9ywnm")))

