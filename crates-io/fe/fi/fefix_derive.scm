(define-module (crates-io fe fi fefix_derive) #:use-module (crates-io))

(define-public crate-fefix_derive-0.4.0 (c (n "fefix_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dfmkk46in6l4gbnd5hkvf1lrnnyi93y50jlqn79sya3lqgqv7aw")))

(define-public crate-fefix_derive-0.5.0 (c (n "fefix_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0dv6mr8p02w7pvkxmgbv7yij57y31ndcrfmib8w9i6m5la8xp4qs")))

(define-public crate-fefix_derive-0.6.0 (c (n "fefix_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "00qhwj8vw15pkv5d2zmj6hqk7mjzcnablp7fhslb58kfi1f4qqim")))

(define-public crate-fefix_derive-0.7.0 (c (n "fefix_derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1jsvlwspdajhc8w3cs891c76i6addslxmslpb56v19q8fx7ksiii")))

