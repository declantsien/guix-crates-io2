(define-module (crates-io fe nw fenwick-bit-tree) #:use-module (crates-io))

(define-public crate-fenwick-bit-tree-0.1.0 (c (n "fenwick-bit-tree") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1a4rkfc95kbfsfrrhqvmnwl2rh2ccav0dvpqmb9w040yh3js3fmz") (f (quote (("benchmarks")))) (r "1.76.0")))

(define-public crate-fenwick-bit-tree-0.1.1 (c (n "fenwick-bit-tree") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "099z2c37mav55k1aa3iywkf320bgs3fnpb78w0m1jc1bjwc1s0k6") (f (quote (("benchmarks")))) (r "1.76.0")))

(define-public crate-fenwick-bit-tree-1.0.0 (c (n "fenwick-bit-tree") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jsvdk5xrkgz22c7484659rf46dfmap61w2f6ksb9zdc1w5rh52y") (f (quote (("benchmarks")))) (r "1.76.0")))

(define-public crate-fenwick-bit-tree-2.0.0 (c (n "fenwick-bit-tree") (v "2.0.0") (d (list (d (n "cargo-readme") (r "^3.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m3bfqlh80pnf4f37yrw4vasca9dmcp2da8ypsxc8vpgpdaqmdwm") (f (quote (("benchmarks")))) (r "1.76.0")))

(define-public crate-fenwick-bit-tree-2.0.1 (c (n "fenwick-bit-tree") (v "2.0.1") (d (list (d (n "cargo-readme") (r "^3.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zc8w2p65lzhq5r3vi3vqpz453nv89sb976a9dnrqhc8s2ixwjdb") (f (quote (("benchmarks")))) (r "1.76.0")))

(define-public crate-fenwick-bit-tree-2.0.2 (c (n "fenwick-bit-tree") (v "2.0.2") (d (list (d (n "cargo-readme") (r "^3.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l3pbjf55jfjs90inhirgf6s32w4qmmqa6l3irn65njx25qmrbhh") (f (quote (("benchmarks")))) (r "1.76.0")))

