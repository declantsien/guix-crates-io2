(define-module (crates-io fe nw fenwick) #:use-module (crates-io))

(define-public crate-fenwick-0.1.0 (c (n "fenwick") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)))) (h "0db1jy6gdnhm4a9c6igk5kvjaafrf9nf2cjwlinimqycnjrvzsy4")))

(define-public crate-fenwick-0.1.1 (c (n "fenwick") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)))) (h "0yjbzkpw41253g7fqrynz3kmxqnla22bh10q47xghj6npn6s7mk3")))

(define-public crate-fenwick-0.1.2 (c (n "fenwick") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)))) (h "0w9sf4gjb0kvi0g82w27rpajpf20m5lydzxnv5arr2y16974m8d6")))

(define-public crate-fenwick-0.2.0 (c (n "fenwick") (v "0.2.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)))) (h "1y2m12wli9cwglasdwk78m5cl34irz38cgz4bx0q3qyadp2cv920")))

(define-public crate-fenwick-1.0.0 (c (n "fenwick") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.0-pre.0") (d #t) (k 2)))) (h "12zjg6jfayb0ggw275jwg32618azrafkjh03f2gn1zanjhkcgn2s")))

(define-public crate-fenwick-2.0.0 (c (n "fenwick") (v "2.0.0") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xhmfk632ilb3rc6kfrpqx3ynfvvisqid3b9qx1spvmri14s25x4")))

(define-public crate-fenwick-2.0.1 (c (n "fenwick") (v "2.0.1") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c0h6vimbr2vx01x6w3440ksrkwg8mr3v01xshwplhgxh6g2ba4d")))

