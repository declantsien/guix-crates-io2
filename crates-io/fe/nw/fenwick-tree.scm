(define-module (crates-io fe nw fenwick-tree) #:use-module (crates-io))

(define-public crate-fenwick-tree-0.0.1 (c (n "fenwick-tree") (v "0.0.1") (h "0974jzd2jlx1phr7l38nrkmwzn5ic666mrss512dh70j8zwnlkza")))

(define-public crate-fenwick-tree-0.0.2 (c (n "fenwick-tree") (v "0.0.2") (h "0k4kq6m72gbjf30a3z45yrq7gbxc1cr9i7vknzji3cg793837giw")))

(define-public crate-fenwick-tree-0.1.0 (c (n "fenwick-tree") (v "0.1.0") (h "0pqrj3fswij67f0b9pj37ypishwql9lwbrflrn3jiy6mrjy2h13b")))

