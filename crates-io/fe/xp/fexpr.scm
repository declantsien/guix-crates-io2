(define-module (crates-io fe xp fexpr) #:use-module (crates-io))

(define-public crate-fexpr-0.1.0 (c (n "fexpr") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1yp0my7anp4hczxqd9r2lqnpvvzfsp5irjzhjlzi1p1x1hjzfwxr")))

(define-public crate-fexpr-0.1.1 (c (n "fexpr") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0f3fyw8hrwmv9sssxknn0hxzcrl1h3382dvx9rm7d2byrg0b1lsk")))

