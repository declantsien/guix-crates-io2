(define-module (crates-io fe el feel) #:use-module (crates-io))

(define-public crate-feel-0.0.1 (c (n "feel") (v "0.0.1") (d (list (d (n "filetime") (r "^0.2.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1y4vx0rl6sk9ihkh7z7n7xwjp98smahjaqlihgxlk0jdn2hnp6s4")))

(define-public crate-feel-0.0.2 (c (n "feel") (v "0.0.2") (d (list (d (n "filetime") (r "^0.2.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "0p2kxbwwmsgpqp11c56447i9qppkb71xvzky7pg5xlv2c93x3qx3")))

