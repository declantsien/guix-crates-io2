(define-module (crates-io fe el feel-parser) #:use-module (crates-io))

(define-public crate-feel-parser-0.0.5 (c (n "feel-parser") (v "0.0.5") (d (list (d (n "dmntk-common") (r "^0.0.5") (d #t) (k 0)) (d (n "dmntk-feel") (r "^0.0.5") (d #t) (k 0)) (d (n "dmntk-feel-grammar") (r "^0.0.5") (d #t) (k 1)))) (h "118bs1ff30yis51cv3bfp23nn4dvs8sgwy13wi6xgpg7sxmpw3qr") (y #t)))

