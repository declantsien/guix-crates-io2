(define-module (crates-io fe ro feroxide-gui) #:use-module (crates-io))

(define-public crate-feroxide-gui-1.0.1 (c (n "feroxide-gui") (v "1.0.1") (d (list (d (n "feroxide") (r "^1.3.2") (f (quote ("no_utf"))) (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.70.0") (d #t) (k 0)) (d (n "vecmath") (r "^0.3.0") (d #t) (k 0)))) (h "0bqyhmf5hb4z220r1893ihd0npingvxrrpn69b0k0inn5q4yqj20")))

