(define-module (crates-io fe ro feroxide) #:use-module (crates-io))

(define-public crate-feroxide-1.0.0 (c (n "feroxide") (v "1.0.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 1)) (d (n "toml") (r "^0.3.1") (d #t) (k 1)))) (h "0cnvb37gxymwqmmy9cwch0ixhq3acy4kvimjawhap9wkiri5yycm")))

(define-public crate-feroxide-1.0.1 (c (n "feroxide") (v "1.0.1") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 1)) (d (n "toml") (r "^0.3.1") (d #t) (k 1)))) (h "1hv7v37sx275bwxj9kjhykwb0s45v2c8xi5636ibh67ki1zxjyv7")))

(define-public crate-feroxide-1.1.0 (c (n "feroxide") (v "1.1.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 1)) (d (n "toml") (r "^0.3.1") (d #t) (k 1)))) (h "1cy35nhk2dvf8qxwdxhs619gfgn2mc3rivw4hpw4mp0lh3nl7cq0")))

(define-public crate-feroxide-1.2.0 (c (n "feroxide") (v "1.2.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.11") (d #t) (k 1)) (d (n "serde_derive") (r "^0.9.11") (d #t) (k 1)) (d (n "toml") (r "^0.3.1") (d #t) (k 1)))) (h "0yjq61gbl098sc84ggja56ghxxcfamdi7cig1kbln4cq1mxminy5")))

(define-public crate-feroxide-1.3.0 (c (n "feroxide") (v "1.3.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 1)) (d (n "toml") (r "^0.4.5") (d #t) (k 1)))) (h "0xk1v0np3ydclb7g907w5ndc05hg10m55dp4jiwk23l24g1d84nv")))

(define-public crate-feroxide-1.3.1 (c (n "feroxide") (v "1.3.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 1)) (d (n "toml") (r "^0.4.5") (d #t) (k 1)))) (h "1dasrrrxc01dm3zccwvz2rrcc7rsjssksiplj7y24pwpnkvnf869") (f (quote (("no_utf") ("default"))))))

(define-public crate-feroxide-1.3.2 (c (n "feroxide") (v "1.3.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.11") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 1)) (d (n "toml") (r "^0.4.5") (d #t) (k 1)))) (h "0890051pbi7xf912nc77dmqcn9fgklphp5mgcz1r5g18a6anycyl") (f (quote (("no_utf") ("default"))))))

