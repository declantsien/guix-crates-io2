(define-module (crates-io fe l4 fel4-config) #:use-module (crates-io))

(define-public crate-fel4-config-0.3.1 (c (n "fel4-config") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "multimap") (r "^0.4") (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "10qp0hyvkqp70gaxann4wvsavsvxgbk4c4rdir9nia8dvf992l28")))

(define-public crate-fel4-config-0.3.2 (c (n "fel4-config") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (k 0)) (d (n "multimap") (r "^0.4") (k 0)) (d (n "proptest") (r "^0.8.6") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0ml4dxazv2xiqf3n4w4q5khs89yahvwk959vglw4y35zcwpjify4")))

