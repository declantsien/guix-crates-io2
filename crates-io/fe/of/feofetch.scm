(define-module (crates-io fe of feofetch) #:use-module (crates-io))

(define-public crate-feofetch-0.1.0 (c (n "feofetch") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "etcetera") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "09j0pj9baklq91ii1dz24446d22hiihblycb73f8y4gbvk6v4abz")))

(define-public crate-feofetch-0.2.0 (c (n "feofetch") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.67") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "etcetera") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uptime_lib") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^4.2") (d #t) (k 0)))) (h "0wp7kvh00dng8l3b97y5zzn90xz2hjv5m5vnafri376a3yfxb6gm")))

