(define-module (crates-io fe l- fel-cli) #:use-module (crates-io))

(define-public crate-fel-cli-0.1.0 (c (n "fel-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "aw-fel") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1hwl8gmc4blxi5rdrlm943g4g744kkimq0dgr3yzk8rbdybps37y")))

(define-public crate-fel-cli-0.1.1 (c (n "fel-cli") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "aw-fel") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1yxjf817lmwjm1lg47i4zwxqkj8aafvz4vz2rsk10xf9nngsqn4x")))

(define-public crate-fel-cli-0.2.0 (c (n "fel-cli") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "aw-fel") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "1yiyznczl831sgasw4g3c39irvmxxyadzs1cdc69rjj9fjdkqzhx")))

(define-public crate-fel-cli-0.3.0 (c (n "fel-cli") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "aw-fel") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "error-chain") (r "^0.9") (d #t) (k 0)))) (h "03z7cbk3zmd2hrxswd745xpd4v6772lmyh6rfar0rq0hfs9lir89")))

(define-public crate-fel-cli-0.4.0 (c (n "fel-cli") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "aw-fel") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "1j55mzlh2rydr355pk0ich2hy5ix6wsffxx6b549hiyvdlaicqzg")))

(define-public crate-fel-cli-0.5.2 (c (n "fel-cli") (v "0.5.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "aw-fel") (r "^0.5.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)))) (h "0yw3ph79ag884bxmvma0ry77sbv7c180b5jzqj9krxn4kw53j54x")))

