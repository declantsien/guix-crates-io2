(define-module (crates-io fe a_ fea_code) #:use-module (crates-io))

(define-public crate-FEA_code-0.1.0 (c (n "FEA_code") (v "0.1.0") (d (list (d (n "cloudmap") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1cna1z8qba8lm3a7cpnryhhgsr0l59mqqx0b08nd85wmhrrfqxsg")))

(define-public crate-FEA_code-0.1.1 (c (n "FEA_code") (v "0.1.1") (d (list (d (n "cloudmap") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.18.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0il0y6jadm9jpmwpxrgh71dwn95i3251xns0mpds3h7xvlnwk92n")))

