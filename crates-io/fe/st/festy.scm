(define-module (crates-io fe st festy) #:use-module (crates-io))

(define-public crate-festy-1.0.0 (c (n "festy") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "09awnq24dzxpvbhz5gs435927mflh1rm56x7dndrcml16z7llzrb")))

(define-public crate-festy-1.0.1 (c (n "festy") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0dfx12lc7vaxz50iwvzfkcq7p4jbwzyh0p2y02kivak5qys5nwq6")))

