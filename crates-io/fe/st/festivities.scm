(define-module (crates-io fe st festivities) #:use-module (crates-io))

(define-public crate-festivities-0.2.0 (c (n "festivities") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "process_control") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "18dizsn00ki6l0rzxvxw4a51gbp0qrisl671bgzc8y3vjivjrn85") (y #t)))

