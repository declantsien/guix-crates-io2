(define-module (crates-io fe st festive-macros) #:use-module (crates-io))

(define-public crate-festive-macros-0.2.1 (c (n "festive-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cimsciwk15g7bl4s2xdy4b1xc5817iw51d8cf7qgnanvjmdjz3y")))

(define-public crate-festive-macros-0.2.2 (c (n "festive-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "052qrqjvrv630gqby0gkjlxv2jrbqkcfx51b22sizcsy4hns6lc0")))

