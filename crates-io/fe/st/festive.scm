(define-module (crates-io fe st festive) #:use-module (crates-io))

(define-public crate-festive-0.1.0 (c (n "festive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0llqy13rx3v0sk5ja8l4lrg6dhh48br465xsixd2x9s7fzbgyfnk")))

(define-public crate-festive-0.1.1 (c (n "festive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09mjwf6l3ll8sf7avd9b3235c22m9kkmabsk8hh3vgxd4lxg10qx")))

(define-public crate-festive-0.2.0 (c (n "festive") (v "0.2.0") (d (list (d (n "festivities") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lzdmxyw0injv296b55i7zk1zmz11zkknq3g946yn4p94xm0g88l") (y #t)))

(define-public crate-festive-0.2.1 (c (n "festive") (v "0.2.1") (d (list (d (n "festive-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "process_control") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "0vdmd6ys08czwvps7a0jjglnn14m4zwvp5j2yh95mc49k4m3c48a") (y #t)))

(define-public crate-festive-0.2.2 (c (n "festive") (v "0.2.2") (d (list (d (n "festive-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "process_control") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "04n8rzgq8slzdjwq5v25bmdn71wdqshyvmzycqj7088ma0jr8ks3")))

