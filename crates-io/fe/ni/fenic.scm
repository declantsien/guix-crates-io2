(define-module (crates-io fe ni fenic) #:use-module (crates-io))

(define-public crate-fenic-0.1.0 (c (n "fenic") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.0") (d #t) (k 2)) (d (n "generator") (r "^0.7") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "scoped-tls") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.27") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.8") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0cl3pymqvpip5szfn0gh559g731i6lmshm672s84pj1yhagn5qk2") (f (quote (("futures" "pin-utils") ("default") ("checkpoint" "serde" "serde_json"))))))

