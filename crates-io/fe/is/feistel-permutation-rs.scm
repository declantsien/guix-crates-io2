(define-module (crates-io fe is feistel-permutation-rs) #:use-module (crates-io))

(define-public crate-feistel-permutation-rs-0.1.0 (c (n "feistel-permutation-rs") (v "0.1.0") (d (list (d (n "xxhash-rust") (r "^0.8.7") (f (quote ("xxh64"))) (d #t) (k 2)))) (h "1ycy6m8r3qbj5a7b55r621q5nf55zfjlz9zh2cgqnwmhh825s3sa")))

