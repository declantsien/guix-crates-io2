(define-module (crates-io fe is feistel_rs) #:use-module (crates-io))

(define-public crate-feistel_rs-0.1.0 (c (n "feistel_rs") (v "0.1.0") (d (list (d (n "cipher") (r "^0.1.1") (d #t) (k 2)) (d (n "pretty-hex") (r "^0.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0mjb1mada3af6mp127byd3wi7h9gs4a17gvkx6g74y2ybhaz4xha")))

