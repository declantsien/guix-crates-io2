(define-module (crates-io fe is feishu) #:use-module (crates-io))

(define-public crate-feishu-0.1.0 (c (n "feishu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0kqv3xzg6by7pf0vpxvggb8y0slgxf69j190afw5bdr9czr8rncz")))

