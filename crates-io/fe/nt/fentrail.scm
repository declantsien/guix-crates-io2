(define-module (crates-io fe nt fentrail) #:use-module (crates-io))

(define-public crate-fentrail-0.1.0 (c (n "fentrail") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "libft") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "16s3kwsg7dqpxri8r49chyy1igc7qjwxzv82vd53bh7mg6w7y9nb")))

