(define-module (crates-io fe tc fetch-hash) #:use-module (crates-io))

(define-public crate-fetch-hash-0.1.1 (c (n "fetch-hash") (v "0.1.1") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "1qfz9lw6xyzga8jy41spkx48n2rjrf0gag221wsxqz2fa7lfkasd")))

(define-public crate-fetch-hash-0.1.2 (c (n "fetch-hash") (v "0.1.2") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0ffwn04xr92vbymr1a2cxr6r137iq1g94zkablycn2y3xvklhxri")))

(define-public crate-fetch-hash-0.1.3 (c (n "fetch-hash") (v "0.1.3") (d (list (d (n "base16ct") (r "^0.1.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ctor") (r "^0.1.22") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "temp_testdir") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0mbg40ibxkw8yv454hvmwwsgh9indl7jymb365zy7m3bzwbyn37j")))

(define-public crate-fetch-hash-0.1.4 (c (n "fetch-hash") (v "0.1.4") (h "14h8kym3rph1n4kv1301kvk4arl9ml3s3338px74wfw6lxcn2d7n")))

(define-public crate-fetch-hash-0.1.5 (c (n "fetch-hash") (v "0.1.5") (h "0z3a4n5nsdql7shqgjllyfl9liba47njq093z2c6i7r0fphbjyfp")))

