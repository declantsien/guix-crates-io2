(define-module (crates-io fe tc fetch_configuration) #:use-module (crates-io))

(define-public crate-fetch_configuration-0.1.0 (c (n "fetch_configuration") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1ww2iq3rkh3bjhg30gbgi8g266bw6zzq6q6h7yg1rvsc1i7y97vd")))

(define-public crate-fetch_configuration-0.2.0 (c (n "fetch_configuration") (v "0.2.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0kdv95kp1v73jmkr3r7ilr01cgclf8pp4nn1llb6n3cl40pcq5g1")))

