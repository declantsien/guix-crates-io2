(define-module (crates-io fe tc fetchlike) #:use-module (crates-io))

(define-public crate-fetchlike-0.1.1 (c (n "fetchlike") (v "0.1.1") (d (list (d (n "hyper") (r "^0.14.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rspjwl62bggplwilwcylif9zmcsmhxs9wympi4cpds6fglvy6fj")))

(define-public crate-fetchlike-0.1.2 (c (n "fetchlike") (v "0.1.2") (d (list (d (n "hyper") (r "^0.14.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n30f4jmdyhnmi766d29b13z8m82hqbpkh4p1ih1c69b5a00p2wd")))

