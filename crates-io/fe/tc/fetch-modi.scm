(define-module (crates-io fe tc fetch-modi) #:use-module (crates-io))

(define-public crate-fetch-modi-0.1.0 (c (n "fetch-modi") (v "0.1.0") (d (list (d (n "console") (r "^0.10.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "str-macro") (r "^0.1.4") (d #t) (k 0)))) (h "1ca1j5ilalj92w4ph9lx3q880sqil8zvyc2gjkls991lm6iq8d44")))

(define-public crate-fetch-modi-0.1.1 (c (n "fetch-modi") (v "0.1.1") (d (list (d (n "console") (r "^0.10.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "str-macro") (r "^0.1.4") (d #t) (k 0)))) (h "13dp0spx8zzlwq645zdixrvzzinc97cmb64m1ncs0smxj8fsz9l4")))

