(define-module (crates-io fe tc fetch_unroll) #:use-module (crates-io))

(define-public crate-fetch_unroll-0.1.0 (c (n "fetch_unroll") (v "0.1.0") (d (list (d (n "http_req") (r "^0.5") (f (quote ("rust-tls"))) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)))) (h "0y1klryzidxw5df4yhiyybyi2vi2lhyxczxgp4p2x2zff88p76iz")))

(define-public crate-fetch_unroll-0.2.0 (c (n "fetch_unroll") (v "0.2.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^0.11") (d #t) (k 0)))) (h "18ky4cy1w2lg2wj9v0jxprcwafqxkisi0y61a1q2l7j4bwijdmm7")))

(define-public crate-fetch_unroll-0.2.1 (c (n "fetch_unroll") (v "0.2.1") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^0.11") (d #t) (k 0)))) (h "1jpvzs8xbp4rg84wmqpzjzf2vj0dgmcvcwrayzsibgjvx42m1idm")))

(define-public crate-fetch_unroll-0.2.2 (c (n "fetch_unroll") (v "0.2.2") (d (list (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "1ya7p2vslpa9xxk13qbmhgxg5yf3l6fj1znbcgq3glb2sl3lim68")))

(define-public crate-fetch_unroll-0.3.0 (c (n "fetch_unroll") (v "0.3.0") (d (list (d (n "libflate") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1l3cf8fhcrw354hdmjf03f5v4bxgn2wkjna8n0fn8bgplh8b3666")))

