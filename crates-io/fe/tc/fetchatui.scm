(define-module (crates-io fe tc fetchatui) #:use-module (crates-io))

(define-public crate-fetchatui-0.1.0 (c (n "fetchatui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0p6a4skhg5pvwjvbk2ccd1ybqbm6bwnfvwm384jwd1bciw057lij")))

(define-public crate-fetchatui-0.1.1 (c (n "fetchatui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "query-shell") (r "^0.3.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)) (d (n "whoami") (r "^1.4.1") (d #t) (k 0)))) (h "0kwh5rrr9l708iy8whqh95dm1lg7sc1mz6ybhgvlxr9nc5amrndh")))

