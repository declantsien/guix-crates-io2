(define-module (crates-io fe tc fetch_file) #:use-module (crates-io))

(define-public crate-fetch_file-0.1.0 (c (n "fetch_file") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0yclg72vvv50p8d04rgpvz47rqv6w1f612k4f03ncz9ha8y450mg")))

(define-public crate-fetch_file-0.1.1 (c (n "fetch_file") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "17nkm6bkfwlyiwv3f87lcnfwg9zlzqcfzyx4jvi0x7rqapznx16b")))

