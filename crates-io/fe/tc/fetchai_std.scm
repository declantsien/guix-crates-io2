(define-module (crates-io fe tc fetchai_std) #:use-module (crates-io))

(define-public crate-fetchai_std-0.1.0 (c (n "fetchai_std") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6.3") (d #t) (k 0)))) (h "0c9mpcqc524y1ynm70x6m34hxpgf1sy5jwlfwiv5h6qdr67xla16")))

