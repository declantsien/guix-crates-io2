(define-module (crates-io fe tc fetch_rs) #:use-module (crates-io))

(define-public crate-fetch_rs-0.1.0 (c (n "fetch_rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.15") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwn1h0cgd8r8q03p906ki5xqi4qy3vhmhxknb445n9rf8v662hr")))

