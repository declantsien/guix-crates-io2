(define-module (crates-io fe tc fetch) #:use-module (crates-io))

(define-public crate-fetch-0.4.0 (c (n "fetch") (v "0.4.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "1s9sv9xz9ib6x4wg7j90ckin3vm82kkksh4znvwzanrdslxicw16")))

(define-public crate-fetch-0.5.0 (c (n "fetch") (v "0.5.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "1nv53afn96qwc47b2qdjdqfz0dnmmi93xx7sh18qwll7f8nblj54")))

(define-public crate-fetch-0.6.0 (c (n "fetch") (v "0.6.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "0yckq9m1df3isjjs7w93iip2f2qwj0ni955ds7wcqggd9ih163bg")))

(define-public crate-fetch-0.7.1 (c (n "fetch") (v "0.7.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "1rnw9s1p9z37dm946785ykqfsdz13wr1gyw6pary5q1apswnflpz")))

(define-public crate-fetch-0.7.2 (c (n "fetch") (v "0.7.2") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)))) (h "18acl1scgylw277757n11nh9sm0m3g5cq3jp6dbhgvzcsbb2zr2b")))

