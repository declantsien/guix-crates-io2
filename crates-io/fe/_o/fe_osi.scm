(define-module (crates-io fe _o fe_osi) #:use-module (crates-io))

(define-public crate-fe_osi-0.1.0 (c (n "fe_osi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)))) (h "09sczy390wili013g8f935j6i09bbdps62fpfr8dkysadji0ycr6")))

(define-public crate-fe_osi-0.1.1 (c (n "fe_osi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cstr_core") (r "^0.1.2") (f (quote ("alloc"))) (d #t) (k 0)))) (h "00qpmvqqx4pipiw9vrjwq3fh5cx4pm87birp49h66r5kl9v1kqi6")))

