(define-module (crates-io fe ra fera-array) #:use-module (crates-io))

(define-public crate-fera-array-0.1.0 (c (n "fera-array") (v "0.1.0") (d (list (d (n "fera-fun") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "testdrop") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0da1adkiskrp7r9jy1whn2w7z9yhfph41sdhbf98z50wd4dv7v1p") (f (quote (("nightly"))))))

