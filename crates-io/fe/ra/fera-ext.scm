(define-module (crates-io fe ra fera-ext) #:use-module (crates-io))

(define-public crate-fera-ext-0.1.0 (c (n "fera-ext") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.4") (d #t) (k 2)))) (h "067v7inbvd6jnlah7klj13aacgznm8ddf4q5959vzqgbvr6pd0v7")))

(define-public crate-fera-ext-0.2.0 (c (n "fera-ext") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "042ymv1nwzghn4lf6kdd3l8630p77cmm0s986620c43hxlq58mgf")))

