(define-module (crates-io fe ra fera-optional) #:use-module (crates-io))

(define-public crate-fera-optional-0.1.0 (c (n "fera-optional") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.4") (d #t) (k 2)))) (h "0skcxh4qf0gzyyqwm60xqwwhv7c2k36lr8bmsnbrg2yh86fs4ajy")))

(define-public crate-fera-optional-0.1.1 (c (n "fera-optional") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1dqrx4lnighzmhlqmwc5p2ql5hif8rlcriivjcf304z6rbm06xjl")))

(define-public crate-fera-optional-0.2.0 (c (n "fera-optional") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "053jnw9gr4y9d2c3silwk35fm6nrjnc7xzrd4rf02zh9km92mvlc")))

