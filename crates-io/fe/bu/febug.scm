(define-module (crates-io fe bu febug) #:use-module (crates-io))

(define-public crate-febug-0.1.0 (c (n "febug") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1ac9qz1rgy842lxdd177cnayaw2gm0jk6c3n0ij867l7gn3kl9pf")))

(define-public crate-febug-0.1.1 (c (n "febug") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1xz15sval1lzfnwmshi3b14f4y2iq4bscfmnrqi0r34h8jr2qz1k")))

(define-public crate-febug-0.1.2 (c (n "febug") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "1w24r8wadncsdiadqfpmxs1b0wg2draq51184391mhj2dnq6wimc")))

(define-public crate-febug-0.2.1 (c (n "febug") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "0kf17xyaa55dnyh5zrj3hkbav8nalvwndwa34sn6l195rlpsbqnk")))

(define-public crate-febug-1.0.0 (c (n "febug") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)))) (h "0nmsbwag28fdbzd9rbi2ka39jf0n7kkmy438qv0gfpdg0vgragm5")))

