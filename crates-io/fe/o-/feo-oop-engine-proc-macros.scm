(define-module (crates-io fe o- feo-oop-engine-proc-macros) #:use-module (crates-io))

(define-public crate-feo-oop-engine-proc-macros-0.0.3 (c (n "feo-oop-engine-proc-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zqy2gnirp7j9495fiiknhi9hrbllmdhq1q5nncwh3mj8s522rzl") (f (quote (("scriptable") ("parent") ("named") ("global") ("gameobject") ("full" "child" "drawable" "gameobject" "global" "named" "parent" "scriptable") ("drawable") ("default" "global") ("child"))))))

(define-public crate-feo-oop-engine-proc-macros-0.0.4 (c (n "feo-oop-engine-proc-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xryq1g6k6vh8kkaaz45nh7iwjglw1vqcwg0l7j57a4b1sbb3vhf") (f (quote (("scriptable") ("parent") ("named") ("global") ("gameobject") ("full" "child" "drawable" "gameobject" "global" "named" "parent" "scriptable") ("drawable") ("default" "global") ("child"))))))

(define-public crate-feo-oop-engine-proc-macros-0.0.5 (c (n "feo-oop-engine-proc-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qh3x4ikk1fap55fxsn6p4qq4r1shhrmqqlr8vzyd9q4b2dkgn89") (f (quote (("scriptable") ("parent") ("named") ("global") ("gameobject") ("full" "child" "drawable" "gameobject" "global" "named" "parent" "scriptable") ("drawable") ("default" "global") ("child"))))))

