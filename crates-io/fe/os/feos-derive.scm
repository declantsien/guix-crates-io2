(define-module (crates-io fe os feos-derive) #:use-module (crates-io))

(define-public crate-feos-derive-0.1.0 (c (n "feos-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "0gp368afp3zlnjx26hrj2pxxiz3ayf3g7z2s42axry9k659kf4xz")))

(define-public crate-feos-derive-0.1.1 (c (n "feos-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "0yhdwq3d4s6spxym0q70sccg6hp6kssshjagriwx0s39ns4r5kkc")))

(define-public crate-feos-derive-0.2.0 (c (n "feos-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "0c1aghkw1lwcr223l97z11ga2qnafd070v2n491j1gxzxmsivp2r")))

(define-public crate-feos-derive-0.3.0 (c (n "feos-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "1sci1k6vdfcnzyrn9g8wap5hfymw4hvm1rn23hj6lvkqr0jmr1kp")))

(define-public crate-feos-derive-0.4.0 (c (n "feos-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "07kspixqd8i2y08zmwi362q2lcphwkwzrwssky5adah0jzps7k2p")))

(define-public crate-feos-derive-0.5.0 (c (n "feos-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "derive"))) (d #t) (k 0)))) (h "1gg527ijmhp26amgwcl1l77jjp05kr9anq04qchy1311x5rdsn00")))

