(define-module (crates-io fe rt fert-calc) #:use-module (crates-io))

(define-public crate-fert-calc-0.1.0 (c (n "fert-calc") (v "0.1.0") (d (list (d (n "accurate") (r "^0.3.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "length") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0kglnfbdzbsr77qdqmfidhmz2gjadvr6z00iv77hlh8vn7qn12l8")))

