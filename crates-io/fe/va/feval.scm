(define-module (crates-io fe va feval) #:use-module (crates-io))

(define-public crate-feval-0.1.0 (c (n "feval") (v "0.1.0") (d (list (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)))) (h "18s7h9xkjzlldzi7hd68cdb8rl6vr8zrnal3ybazfi0n1hz7h907")))

(define-public crate-feval-0.1.1 (c (n "feval") (v "0.1.1") (d (list (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)))) (h "1ghjbfn6nk4d7glfmnp3a08s964i74hsf5ksvx1m0s8yfbyxiyjk")))

(define-public crate-feval-0.1.2 (c (n "feval") (v "0.1.2") (d (list (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)))) (h "11pbsdbckkv613gwgm8hlyg7h596kgj71r5x0c1hprdi6p30bm1s")))

(define-public crate-feval-0.2.0 (c (n "feval") (v "0.2.0") (d (list (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)))) (h "05l9fj9lrj06pvikm2i3qw91786h3kjswk8p08fpb54qarspar3v")))

(define-public crate-feval-0.3.0 (c (n "feval") (v "0.3.0") (d (list (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)))) (h "0xlm5x8sgy79g4iry5ncyjmym2rdnz16kdd3c262n0wzb5jwjsgb")))

(define-public crate-feval-0.4.0 (c (n "feval") (v "0.4.0") (d (list (d (n "evalexpr") (r "^10.0.0") (d #t) (k 0)))) (h "0dq8bd0l0bfkiq5m2g126br6p7p7xd24vw212sp62airvfa99l2h")))

(define-public crate-feval-0.4.1 (c (n "feval") (v "0.4.1") (d (list (d (n "evalexpr") (r "^10.0.0") (d #t) (k 0)))) (h "18mjzwvcrmnm4r49ssvx7nfn9j3ld7gbc6hp0m6npmjr4bqrphbi")))

(define-public crate-feval-0.4.2 (c (n "feval") (v "0.4.2") (d (list (d (n "evalexpr") (r "^10.0.0") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)))) (h "0ldgrf8sxk1l66arx56vc1cpz0rszwl1pfaj5xqbz0lcagscg2rz")))

