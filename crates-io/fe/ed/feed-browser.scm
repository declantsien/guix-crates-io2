(define-module (crates-io fe ed feed-browser) #:use-module (crates-io))

(define-public crate-feed-browser-0.1.0 (c (n "feed-browser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rss") (r "^1.6") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "webbrowser") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0xl8x4a6pd3halpz6pyjapdgs9718r7c7x24q7696b35fd2hvb6n")))

(define-public crate-feed-browser-0.1.1 (c (n "feed-browser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "rss") (r "^1.6") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1v22nbmgwp95ffbw6i7hwxici2yk8vhyay67sywn3llcx8zw2n8y")))

(define-public crate-feed-browser-0.1.2 (c (n "feed-browser") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "rss") (r "^1.6") (f (quote ("from_url"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1hawqvwgvgsidz8jcmjjs5qm0l5fw879ikdrzqs2qdbi3ks211gd")))

