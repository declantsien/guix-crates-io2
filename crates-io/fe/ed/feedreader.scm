(define-module (crates-io fe ed feedreader) #:use-module (crates-io))

(define-public crate-feedreader-0.1.0 (c (n "feedreader") (v "0.1.0") (d (list (d (n "curl") (r "^0.2.12") (d #t) (k 0)) (d (n "rss") (r "^0.2.1") (d #t) (k 0)) (d (n "url") (r "^0.2.37") (d #t) (k 0)))) (h "0403ggawqa4mj75bcf2wi00434pzvwsmsf83aaaklrn36agx9siw") (y #t)))

