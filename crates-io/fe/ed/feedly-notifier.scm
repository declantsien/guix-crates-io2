(define-module (crates-io fe ed feedly-notifier) #:use-module (crates-io))

(define-public crate-feedly-notifier-0.1.0 (c (n "feedly-notifier") (v "0.1.0") (d (list (d (n "gdk-pixbuf") (r "^0.1.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^0.8.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.9") (d #t) (k 0)))) (h "0p3m8vhfkxpwqbykrvv3ki4k46i1b48lpzqzhyb8hbr8jfinsr61")))

(define-public crate-feedly-notifier-0.1.1 (c (n "feedly-notifier") (v "0.1.1") (d (list (d (n "gdk-pixbuf") (r "^0.1.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "serde") (r "^0.8.12") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8.9") (d #t) (k 0)))) (h "1cxykiffxi25r2nfzwwklawfihmcy024cq0643g6wq49l0rqfmm5")))

