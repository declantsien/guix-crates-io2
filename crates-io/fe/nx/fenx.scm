(define-module (crates-io fe nx fenx) #:use-module (crates-io))

(define-public crate-fenx-0.1.0 (c (n "fenx") (v "0.1.0") (h "0ynj3crpd605n2svq1rvjl5anca038v0zlwilji3f6n9afq9bi4n")))

(define-public crate-fenx-0.1.1 (c (n "fenx") (v "0.1.1") (h "1i0pk6jwgs2ldbc7wir4rsfhdm6ryqc951miq6vzfs8khhyg5al7")))

