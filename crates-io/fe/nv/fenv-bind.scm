(define-module (crates-io fe nv fenv-bind) #:use-module (crates-io))

(define-public crate-fenv-bind-0.0.1 (c (n "fenv-bind") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0cpaqkgqmr5sy396p22hzg4m7qmd7bmwq9aq41h8q9ihc5gklmvk")))

(define-public crate-fenv-bind-0.0.2 (c (n "fenv-bind") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "17g4il0lbwzgfs0ngzkz14rxqiqcqk6wb4lvgjq2ay5v2j80dkds")))

(define-public crate-fenv-bind-0.0.3 (c (n "fenv-bind") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0w72r62xnr8zlyamngjdqrwxick176ylim5a8vqrvnnnk9bm818q")))

(define-public crate-fenv-bind-0.0.4 (c (n "fenv-bind") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "0vzcd9lhcq15gbfly06s04y5g9v56rld0mrm464d4mjxbnlg7d16")))

