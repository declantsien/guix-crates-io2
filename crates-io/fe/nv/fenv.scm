(define-module (crates-io fe nv fenv) #:use-module (crates-io))

(define-public crate-fenv-0.2.1 (c (n "fenv") (v "0.2.1") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.7") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0zan4wvq2fsbrikfgclbxl79gbz7wmnwahlpx1rxnvqy23yida84") (r "1.64")))

