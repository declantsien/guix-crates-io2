(define-module (crates-io fe at feature-extension-for-smol) #:use-module (crates-io))

(define-public crate-feature-extension-for-smol-0.1.0 (c (n "feature-extension-for-smol") (v "0.1.0") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "smol") (r "^2") (k 0)))) (h "1hswl6nz9x9dj43n3qgm7jwmgpiryas72nyaqncnk10jhfly3cfn") (f (quote (("time" "async-io") ("default")))) (r "1.75.0")))

(define-public crate-feature-extension-for-smol-0.2.0 (c (n "feature-extension-for-smol") (v "0.2.0") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "=0.3.29") (o #t) (d #t) (k 0)) (d (n "smol") (r "^2") (k 0)))) (h "00w4liipm7cdlglzwi4fwycf76qz5aq3lap393i5yvxksp6m6vh3") (f (quote (("time" "async-io") ("default") ("channel" "futures-channel")))) (r "1.75.0")))

(define-public crate-feature-extension-for-smol-0.2.1 (c (n "feature-extension-for-smol") (v "0.2.1") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "futures-channel") (r "=0.3.29") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "=0.3.29") (o #t) (k 0)) (d (n "smol") (r "^2") (k 0)))) (h "1jvwd3xkkakpdp38ay27i67bfyqfn5q2ag8d7r1f1c2sap3psxys") (f (quote (("time" "async-io" "futures-util/async-await-macro") ("default") ("channel" "futures-channel")))) (r "1.75.0")))

