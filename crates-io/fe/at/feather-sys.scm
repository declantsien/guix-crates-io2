(define-module (crates-io fe at feather-sys) #:use-module (crates-io))

(define-public crate-feather-sys-0.1.0 (c (n "feather-sys") (v "0.1.0") (h "0ilsphqrhw73bri6jcc36n1l9vvnpjqrqi1g0r347hkjamz8zgw8")))

(define-public crate-feather-sys-0.1.1 (c (n "feather-sys") (v "0.1.1") (h "0z265i5139na4fnzq7701hw49cbj0is2pa46vln04vsw86jm2gm9")))

