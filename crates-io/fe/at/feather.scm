(define-module (crates-io fe at feather) #:use-module (crates-io))

(define-public crate-feather-0.1.0 (c (n "feather") (v "0.1.0") (h "13wmgv00l802ydmzrsm1dinp7r49xkq1njkmg2x0hp0fqhgsyrp3")))

(define-public crate-feather-0.1.1 (c (n "feather") (v "0.1.1") (h "0y50qw9dycg030ny7314xd59af5gl65wwhysv6fxxsrfvs16qix9")))

(define-public crate-feather-0.1.2 (c (n "feather") (v "0.1.2") (h "0caqz9rq5f712k2k9458gr4337rf3lysp704yql23avpbzqiqgc0")))

