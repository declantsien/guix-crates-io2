(define-module (crates-io fe at feature-set) #:use-module (crates-io))

(define-public crate-feature-set-0.1.0 (c (n "feature-set") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "18j659f981yhmnxjx5vga13qdb17rgkd7sk0d3abcwfqvc4xvass")))

(define-public crate-feature-set-0.1.1 (c (n "feature-set") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (d #t) (k 0)))) (h "1ignfppv48r3vljjwwmgfgm5mraask2hx7qvdknmmy9rxisv2mi3")))

