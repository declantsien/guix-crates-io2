(define-module (crates-io fe at features) #:use-module (crates-io))

(define-public crate-features-0.8.0 (c (n "features") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)))) (h "1qaj5vgym2gcd0ys4kia787r398yq4r796lds0xrlfsz2m4jwjxn")))

(define-public crate-features-0.9.0 (c (n "features") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1g7m27mmcf154vik43wzndw5pwrsps5bmcw8jd8bf9cy9nzrdm25")))

(define-public crate-features-0.10.0 (c (n "features") (v "0.10.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1qvlsn4228m9ypkh8vm5whx58bpxs1slaspk1w69spz5hhy2n1w3")))

