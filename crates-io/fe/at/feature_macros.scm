(define-module (crates-io fe at feature_macros) #:use-module (crates-io))

(define-public crate-feature_macros-0.1.0 (c (n "feature_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "0dpg91hiys6k47nx2rh5wgnyk4pssqd0rrlcmpdryybcyvb8r3fq") (y #t)))

