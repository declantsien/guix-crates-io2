(define-module (crates-io fe at feature-probe) #:use-module (crates-io))

(define-public crate-feature-probe-0.1.0 (c (n "feature-probe") (v "0.1.0") (h "1k37j5lzvq155r4xazajwp52w68ccdpp4p834134vbqavar2w619")))

(define-public crate-feature-probe-0.1.1 (c (n "feature-probe") (v "0.1.1") (h "1nhif9zpr2f17gagf0qb0v914wc3jr9sfjzvnpi7b7pcs73ksnl3")))

