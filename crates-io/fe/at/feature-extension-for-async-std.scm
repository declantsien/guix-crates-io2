(define-module (crates-io fe at feature-extension-for-async-std) #:use-module (crates-io))

(define-public crate-feature-extension-for-async-std-0.1.0 (c (n "feature-extension-for-async-std") (v "0.1.0") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (k 0)))) (h "1pb0r3g97n53zf07q8002v7x4214crlhpw8pymfc2ixaczmy7f34") (f (quote (("unstable" "async-std/unstable") ("time" "async-io") ("std" "async-std/std") ("default" "async-std/default")))) (r "1.75.0")))

(define-public crate-feature-extension-for-async-std-0.2.0 (c (n "feature-extension-for-async-std") (v "0.2.0") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (k 0)) (d (n "futures-channel") (r "=0.3.29") (o #t) (d #t) (k 0)))) (h "0rq3bwsv4zmvcyj3kkxk79wp7acfh7790ymkvdff0473wdb5wlw2") (f (quote (("unstable" "async-std/unstable") ("time" "async-io") ("std" "async-std/std") ("default" "async-std/default") ("channel" "futures-channel")))) (r "1.75.0")))

(define-public crate-feature-extension-for-async-std-0.2.1 (c (n "feature-extension-for-async-std") (v "0.2.1") (d (list (d (n "async-io") (r "^2") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1") (k 0)) (d (n "futures-channel") (r "=0.3.29") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "=0.3.29") (o #t) (k 0)))) (h "0dj3cvln4sii3d2425x00814zav2vxqk9mb1s41l12ana00zksfn") (f (quote (("unstable" "async-std/unstable") ("time" "async-io" "futures-util/async-await-macro") ("std" "async-std/std") ("default" "async-std/default") ("channel" "futures-channel")))) (r "1.75.0")))

