(define-module (crates-io fe at feature-check) #:use-module (crates-io))

(define-public crate-feature-check-1.0.0 (c (n "feature-check") (v "1.0.0") (d (list (d (n "expect-exit") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16w5a43zz5kibbgs17xvkppf92r5ija8a0d3wj6rrrlbzk2wac67")))

(define-public crate-feature-check-1.0.1 (c (n "feature-check") (v "1.0.1") (d (list (d (n "expect-exit") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r3knhpf4pmbp10kgiys0kmaag8d6zgzl7ckk7lsr5r5yk2qz364")))

(define-public crate-feature-check-2.0.0 (c (n "feature-check") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0k9fjdfjik3hhhqh7r1wlf7d3ry994kvlplirlm7yk4abj2msmmy") (r "1.61")))

(define-public crate-feature-check-2.1.0 (c (n "feature-check") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0bi8p2jkqas1nj4nzpdba1m8gx251789dcx5bwxwym012gd1fqmq") (r "1.61")))

