(define-module (crates-io fe at feature) #:use-module (crates-io))

(define-public crate-feature-0.1.0 (c (n "feature") (v "0.1.0") (h "0x1zs29p2p5m3alrsmb97m62jpsavqvfypkwrvnj9j48xmx4mjq6") (y #t)))

(define-public crate-feature-0.2.0 (c (n "feature") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 0)))) (h "14w683l3ygi5pwf49qpikrlxwr9z5jb20p9pfv43alqc4mkn42i2") (y #t)))

(define-public crate-feature-0.3.0 (c (n "feature") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0bmnhp4c1v3vs3qyrz9rg3w1gllwghnnzw630mx6yq2dmmwvx7jj") (y #t)))

(define-public crate-feature-0.3.1 (c (n "feature") (v "0.3.1") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "035zd6nyjk227lr62i8i6wxlyyzzbxwp7597zl6gjm3jaixdc1g6") (y #t)))

(define-public crate-feature-0.3.2 (c (n "feature") (v "0.3.2") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "049q302dfmmryk8jv1dw3zy87jhw72z4mgfcdrmaw6kj2krbs66v") (y #t)))

(define-public crate-feature-0.4.0 (c (n "feature") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "1qdpa54adx08wxyv8p52n8jy6bxqcf0phbk5fgn714xirwmcydvi") (y #t)))

(define-public crate-feature-0.4.1 (c (n "feature") (v "0.4.1") (d (list (d (n "cargo_metadata") (r "^0.15.2") (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "0yfwkjqb38ghvj6wd1dymkafx355g6zs2hb0hncslbfpq5c0hhar") (y #t)))

(define-public crate-feature-0.5.0 (c (n "feature") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "1hlg79slwpw83f48p4awdzkqzp8ag0irds5675g5pq5x9kzihiz4") (y #t)))

(define-public crate-feature-0.5.1 (c (n "feature") (v "0.5.1") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "toml_edit") (r "^0.19.3") (d #t) (k 0)) (d (n "version") (r "^3.0.0") (d #t) (k 0)))) (h "00743hj0n4nrn4h52s31scj1pvfmb134z86mcn9gvp7gr706bd6x") (y #t)))

