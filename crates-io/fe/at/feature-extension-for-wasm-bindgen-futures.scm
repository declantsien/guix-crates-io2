(define-module (crates-io fe at feature-extension-for-wasm-bindgen-futures) #:use-module (crates-io))

(define-public crate-feature-extension-for-wasm-bindgen-futures-0.1.0 (c (n "feature-extension-for-wasm-bindgen-futures") (v "0.1.0") (d (list (d (n "futures-timer") (r "^3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1p5cylvnkw62zry6s3hwhs3yq3q2xfhfv1c2vmnsz9f6hi4cyfgw") (f (quote (("time" "futures-timer/wasm-bindgen") ("default")))) (r "1.75.0")))

(define-public crate-feature-extension-for-wasm-bindgen-futures-0.2.0 (c (n "feature-extension-for-wasm-bindgen-futures") (v "0.2.0") (d (list (d (n "futures-channel") (r "=0.3.29") (o #t) (d #t) (k 0)) (d (n "futures-timer") (r "^3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1yjyj9vxqngzzzax9qffph2s0qaldm7swnnay6lba1gc0vf7fikn") (f (quote (("time" "futures-timer/wasm-bindgen") ("default") ("channel" "futures-channel")))) (r "1.75.0")))

(define-public crate-feature-extension-for-wasm-bindgen-futures-0.2.1 (c (n "feature-extension-for-wasm-bindgen-futures") (v "0.2.1") (d (list (d (n "futures-channel") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-timer") (r "^3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0h25j1g9i4g6p30ss7f6nc7wi8vsy0hfc5bafa2yv6yjygf73xzb") (f (quote (("time" "futures-timer") ("default") ("channel" "futures-channel")))) (s 2) (e (quote (("wasm-bindgen" "futures-timer?/wasm-bindgen")))) (r "1.75.0")))

