(define-module (crates-io fe ad feaders) #:use-module (crates-io))

(define-public crate-feaders-0.2.0 (c (n "feaders") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.58") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "12bgwm7jizk1fmb64frprc4qzdgfvajhcw5vvxxbz2qjxx4jdfhz")))

