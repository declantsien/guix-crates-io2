(define-module (crates-io fe lf felfel) #:use-module (crates-io))

(define-public crate-felfel-0.1.0 (c (n "felfel") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)))) (h "0gmvczkwrv3r7cq4rqivdmnxcdg7iab852f6s9basj71mazzw6ri")))

(define-public crate-felfel-0.1.1 (c (n "felfel") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)))) (h "1xv976hq74ylk4q0gqmhhk4f0790qm1kca93jwy0px3z1qj9axpk")))

