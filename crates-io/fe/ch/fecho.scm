(define-module (crates-io fe ch fecho) #:use-module (crates-io))

(define-public crate-fecho-0.1.0 (c (n "fecho") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sv4b2mpgjagzdkn8v05jl2ca88bp4374zy750q06x4773z8bbqx")))

(define-public crate-fecho-0.2.0 (c (n "fecho") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hf2vam4sgqrz586wiipcxlgrnn0q70nxs934qgvi1nwzx5mvzdz")))

(define-public crate-fecho-0.3.0 (c (n "fecho") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jg9z28i3iwghdgjvh9nblg77q8bgck4b2bn3pcbji99v9qjfcgl") (y #t)))

(define-public crate-fecho-0.3.1 (c (n "fecho") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vl26m7x3axzywdhhqr0zx73bjkppgni335s4gb28ks67c2ppnzh") (y #t)))

(define-public crate-fecho-0.3.2 (c (n "fecho") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1df0ig24azfib6ag9sz0aaz07m8z9aza41a07s5j2wgqsz8fxqjw")))

