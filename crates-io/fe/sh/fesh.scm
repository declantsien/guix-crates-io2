(define-module (crates-io fe sh fesh) #:use-module (crates-io))

(define-public crate-fesh-0.1.0 (c (n "fesh") (v "0.1.0") (h "0z774c4jllfv4hsy4js0b43wz4si3xbxnfii5m1sb6pm5qp0mb9v")))

(define-public crate-fesh-0.1.1 (c (n "fesh") (v "0.1.1") (h "1nazvny4z0a4s13280ka0z643rbbz53hxvkcd0kx69n55yi5sbpn")))

(define-public crate-fesh-0.1.2 (c (n "fesh") (v "0.1.2") (h "1k7073sq8qssi1810javnjg1wvsx405cmw9vhd271ln86747d7z9")))

