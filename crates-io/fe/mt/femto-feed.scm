(define-module (crates-io fe mt femto-feed) #:use-module (crates-io))

(define-public crate-femto-feed-0.2.0 (c (n "femto-feed") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "feed-rs") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1jil6fjjz5mjj4sxk6dyrhdiqnr394zmb2gdbqpw1gqzkzr64h61")))

(define-public crate-femto-feed-0.3.0 (c (n "femto-feed") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "feed-rs") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1icz07xirlxxv1lzpaj2ifqwgx6spwx4zjycnpcnjm1sy0k7n99c")))

