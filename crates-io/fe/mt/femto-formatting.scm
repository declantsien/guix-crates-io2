(define-module (crates-io fe mt femto-formatting) #:use-module (crates-io))

(define-public crate-femto-formatting-0.1.0 (c (n "femto-formatting") (v "0.1.0") (d (list (d (n "femtovg") (r ">=0.1, <=0.2") (d #t) (k 0)))) (h "1jzirh6wz4slh2ics20239nmlqgar8pmpn0d2qc4y2y942h5jfmg")))

(define-public crate-femto-formatting-0.2.0 (c (n "femto-formatting") (v "0.2.0") (d (list (d (n "femtovg") (r "^0.3") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)))) (h "13mpjkrddvwhrnkni8j4vw4sv05rg5l7gfsd8zig46wrrg7wjd75")))

(define-public crate-femto-formatting-0.3.0 (c (n "femto-formatting") (v "0.3.0") (d (list (d (n "femtovg") (r "^0.3") (d #t) (k 0)) (d (n "glutin") (r "^0.28") (d #t) (k 0)))) (h "1qjpizzanchfpr2rgl19c815panjsg01nmmyqlyfx0mry8p4l91w")))

(define-public crate-femto-formatting-0.4.0 (c (n "femto-formatting") (v "0.4.0") (d (list (d (n "femtovg") (r "^0.4.0") (d #t) (k 0)))) (h "0y8pk2fq6sgi2sj511nbzaj9y8wr67cpsds3d9y048csqn7kbngq")))

(define-public crate-femto-formatting-0.4.1 (c (n "femto-formatting") (v "0.4.1") (d (list (d (n "femtovg") (r ">=0.4, <=0.6") (d #t) (k 0)))) (h "1vadwkplx6p1xbgzdx5hbahapkns56hi2rf40ykpp15f3kd11phm")))

(define-public crate-femto-formatting-0.6.0 (c (n "femto-formatting") (v "0.6.0") (d (list (d (n "femtovg") (r "^0.7") (d #t) (k 0)) (d (n "pukram-formatting") (r "^0.1") (d #t) (k 0)))) (h "1x6hppy7a8a84pnckw9h7rlgg38x6110jck6alkrjmbq3rjbw303")))

