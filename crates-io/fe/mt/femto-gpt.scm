(define-module (crates-io fe mt femto-gpt) #:use-module (crates-io))

(define-public crate-femto-gpt-0.1.0 (c (n "femto-gpt") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0xxbby4kh433r56g19pmvjrqlfm58al5ijapignxxv7zxyikb5l1")))

(define-public crate-femto-gpt-0.2.0 (c (n "femto-gpt") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ocl") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01yfkq5mmp3zrgxxj756zljaq451wn9rf56nqwa36h9jwy8dl6gi") (f (quote (("gpu" "ocl"))))))

