(define-module (crates-io fe mt femtorinth) #:use-module (crates-io))

(define-public crate-femtorinth-0.1.0 (c (n "femtorinth") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (f (quote ("json"))) (d #t) (k 0)))) (h "08vrvfd4n3wfr7f2f7swmnh8340r0dlk12r6rzxns1spzsd9nxvn")))

(define-public crate-femtorinth-0.1.1 (c (n "femtorinth") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.2.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1z68j00h56r2w9cfz9zbk7hp4l932h6hh4cv63p7098430yrq767")))

(define-public crate-femtorinth-0.1.2 (c (n "femtorinth") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (f (quote ("json"))) (d #t) (k 0)))) (h "0f7mpq0kci87s5hqqyahp5kfc523gcblgiv03xk95ixpnblz8ksy")))

(define-public crate-femtorinth-0.1.3 (c (n "femtorinth") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (f (quote ("json"))) (d #t) (k 0)))) (h "1k66d64i7j5p0fkhbvrdaxfi49j18afhydlmdl4ii0k2248ah2my")))

