(define-module (crates-io fe mt femtofemme) #:use-module (crates-io))

(define-public crate-femtofemme-0.1.0 (c (n "femtofemme") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (f (quote ("color" "regex"))) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("kv_unstable"))) (k 0)))) (h "18wjsqw6giw3xdwci56zc2zr1kgj0gi0pqasxynn0ym80zki62g6")))

(define-public crate-femtofemme-0.2.0 (c (n "femtofemme") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("clock"))) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 0)) (d (n "log") (r "^0.4.19") (k 0)))) (h "0v8h897xwdcr9rdpr4z8i4p74249fni3cia70viqpl7wbh2gizz0") (f (quote (("kv" "log/kv_unstable") ("default" "color" "kv") ("color" "env_logger/auto-color"))))))

