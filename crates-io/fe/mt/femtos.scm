(define-module (crates-io fe mt femtos) #:use-module (crates-io))

(define-public crate-femtos-0.1.0 (c (n "femtos") (v "0.1.0") (h "0zagjqjzaxdd6dnvs8h98vf6wqdpj19gcj29qcz11k6zw9lwj2mr") (r "1.60")))

(define-public crate-femtos-0.1.1 (c (n "femtos") (v "0.1.1") (h "0n8rz106fmlnsrqcy5addl6qpi1pmr7xifn58w109k9d0f02g8qq") (r "1.60")))

