(define-module (crates-io fe nr fenris-sparse) #:use-module (crates-io))

(define-public crate-fenris-sparse-0.0.1 (c (n "fenris-sparse") (v "0.0.1") (d (list (d (n "fenris-paradis") (r "^0.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.28") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "155kq0x86ycaw9fvppcx4g74pbc6q16yff4wmp8zkvcb6q93x9x3")))

(define-public crate-fenris-sparse-0.0.2 (c (n "fenris-sparse") (v "0.0.2") (d (list (d (n "fenris-paradis") (r "^0.0.1") (d #t) (k 0)) (d (n "fenris-traits") (r "^0.0.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "0qzps16y6l3wdjkkwj258q53h4q3smnivv7spjg2bma5063qmmy1")))

(define-public crate-fenris-sparse-0.0.3 (c (n "fenris-sparse") (v "0.0.3") (d (list (d (n "fenris-paradis") (r "^0.0.1") (d #t) (k 0)) (d (n "fenris-traits") (r "^0.0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)))) (h "0y8v8r3i6lm2m9j825fhai523lndv1380wxliaz52dfvwl4ahv9s")))

(define-public crate-fenris-sparse-0.0.4 (c (n "fenris-sparse") (v "0.0.4") (d (list (d (n "fenris-paradis") (r "^0.0.2") (d #t) (k 0)) (d (n "fenris-traits") (r "^0.0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "087i52l2ag4fy3rldnihl9z1qz47pi4a13f1yih3dakswdlzqm6c")))

(define-public crate-fenris-sparse-0.0.5 (c (n "fenris-sparse") (v "0.0.5") (d (list (d (n "fenris-paradis") (r "^0.0.3") (d #t) (k 0)) (d (n "fenris-traits") (r "^0.0.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (k 0)) (d (n "nalgebra-sparse") (r "^0.9.0") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)))) (h "15n9g6wzcgb2m2v8agnj7fcngi1xjc4g25c3q6wsql9fxh65djf7")))

