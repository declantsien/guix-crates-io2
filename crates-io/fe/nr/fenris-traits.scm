(define-module (crates-io fe nr fenris-traits) #:use-module (crates-io))

(define-public crate-fenris-traits-0.0.1 (c (n "fenris-traits") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.31.0") (k 0)))) (h "06lqfxmdxa17wgl82x5hrmf54320n0wpwl9dsml8r08kz87j3cng")))

(define-public crate-fenris-traits-0.0.2 (c (n "fenris-traits") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.32.1") (k 0)))) (h "1ik8jndsfxy3438hiddb8dshz5mq5li2fdrlphjg5c9fplgwqrj0")))

