(define-module (crates-io fe nr fenris-optimize) #:use-module (crates-io))

(define-public crate-fenris-optimize-0.0.1 (c (n "fenris-optimize") (v "0.0.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.28") (f (quote ("compare"))) (d #t) (k 0)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)))) (h "1q8y981bzc95xf2j8xgv19vdwm0y14m774zdj0wjhrz6fvxnacj5")))

(define-public crate-fenris-optimize-0.0.2 (c (n "fenris-optimize") (v "0.0.2") (d (list (d (n "fenris-traits") (r "^0.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.31") (f (quote ("compare"))) (d #t) (k 0)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)))) (h "0q2rxch1v50mq4cmg4g6gp8y5k6xlfc8xsi492f125gqp397n1sj")))

(define-public crate-fenris-optimize-0.0.3 (c (n "fenris-optimize") (v "0.0.3") (d (list (d (n "fenris-traits") (r "^0.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.1") (f (quote ("compare" "std"))) (k 0)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)))) (h "0gylsb0is6m83hp2hi68sv7hqak41ygj00rq7n94axvc2skr8r25")))

