(define-module (crates-io fe nr fenris-solid) #:use-module (crates-io))

(define-public crate-fenris-solid-0.0.1 (c (n "fenris-solid") (v "0.0.1") (d (list (d (n "fenris") (r "^0.0.1") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0bqqka2smllbim4dp72wai0vq0f2r44rgw5ivds048hss0ni6vms")))

(define-public crate-fenris-solid-0.0.2 (c (n "fenris-solid") (v "0.0.2") (d (list (d (n "fenris") (r "^0.0.2") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "16yv26dhl7f8i7yrrhqsrnn3qprq0adwsj92bcckj13la5x308pj")))

(define-public crate-fenris-solid-0.0.3 (c (n "fenris-solid") (v "0.0.3") (d (list (d (n "fenris") (r "^0.0.3") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "148a9mdb5yxp6fjkdc0vij66zjjl66bisnap5qpbk3f4n91hhn29")))

(define-public crate-fenris-solid-0.0.4 (c (n "fenris-solid") (v "0.0.4") (d (list (d (n "fenris") (r "^0.0.4") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1zpjr63habn30bmpy15v56wncyrmjw5cihl9qcy7fs1s7gmbsffr")))

(define-public crate-fenris-solid-0.0.5 (c (n "fenris-solid") (v "0.0.5") (d (list (d (n "fenris") (r "^0.0.5") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1i216fryacj36qhp2z3czn11bh7fpaj9311c13a1mahc6dma4wxb")))

(define-public crate-fenris-solid-0.0.6 (c (n "fenris-solid") (v "0.0.6") (d (list (d (n "fenris") (r "^0.0.6") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "042zzz24ahawgry2hdhympnb2ic6viis30xk7j41nh2bwcgs68dx")))

(define-public crate-fenris-solid-0.0.7 (c (n "fenris-solid") (v "0.0.7") (d (list (d (n "fenris") (r "^0.0.7") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "08z8fyx61wbbbzwwh0qic1p652zdw6knwjyhzjhai6xkzr00nysa")))

(define-public crate-fenris-solid-0.0.8 (c (n "fenris-solid") (v "0.0.8") (d (list (d (n "fenris") (r "^0.0.8") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1rfrrgaxdh5vw7f63161jypdbx8wqflird08za0f71gd6i0i52bx")))

(define-public crate-fenris-solid-0.0.9 (c (n "fenris-solid") (v "0.0.9") (d (list (d (n "fenris") (r "^0.0.9") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1hm1f06nyxbkzmfg5r7qkg815645f5pg9z31cyy2mwz1svliqlvv")))

(define-public crate-fenris-solid-0.0.11 (c (n "fenris-solid") (v "0.0.11") (d (list (d (n "fenris") (r "^0.0.11") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0h6qzw6nn97cyahwgks04nsjj0imsxw16vfb2m9gi46ad8rj1ji1")))

(define-public crate-fenris-solid-0.0.12 (c (n "fenris-solid") (v "0.0.12") (d (list (d (n "fenris") (r "^0.0.12") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "007hpfyyqm7fr5kq9zg522xy5z4fkmf6pblapkagn9wjc21z7n3c")))

(define-public crate-fenris-solid-0.0.13 (c (n "fenris-solid") (v "0.0.13") (d (list (d (n "fenris") (r "^0.0.13") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0w6dgjinl719slh9vah59rn4dpijmnyla566dcxrh92i5cplvfy9")))

(define-public crate-fenris-solid-0.0.14 (c (n "fenris-solid") (v "0.0.14") (d (list (d (n "fenris") (r "^0.0.14") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0q84nbkil30061jprfbkcira8zq24409d3zh34xi7pr6sbwiz6r9")))

(define-public crate-fenris-solid-0.0.15 (c (n "fenris-solid") (v "0.0.15") (d (list (d (n "fenris") (r "^0.0.15") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1whk4vd2wqrlk5cnyrwgjahmafai3rrnjj0rn6m99fy2xbn7174p")))

(define-public crate-fenris-solid-0.0.16 (c (n "fenris-solid") (v "0.0.16") (d (list (d (n "fenris") (r "^0.0.16") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "16h35fdz7a9iy4ylvp5ag54yrnq9mf9nsm20ws4vfayiq7gv9kq8")))

(define-public crate-fenris-solid-0.0.17 (c (n "fenris-solid") (v "0.0.17") (d (list (d (n "fenris") (r "^0.0.17") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0kdal68ba7rpphnlkvh565g35i3ca4qrig82l2f233x4widzk6ws")))

(define-public crate-fenris-solid-0.0.18 (c (n "fenris-solid") (v "0.0.18") (d (list (d (n "fenris") (r "^0.0.18") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0qlg3g6i4q1hr1r37j315flkbbwspqivwx36dqczldwyq06gafky")))

(define-public crate-fenris-solid-0.0.19 (c (n "fenris-solid") (v "0.0.19") (d (list (d (n "fenris") (r "^0.0.19") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.1") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "19qlwy9kbqrsx9098sl892krzvx4c2hh9xbj49x6nzg1g0hr4161")))

(define-public crate-fenris-solid-0.0.20 (c (n "fenris-solid") (v "0.0.20") (d (list (d (n "fenris") (r "^0.0.20") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.2") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "16b5gfvm1s549mrbhp5yijq9dimmbxvys1l1q09b462sxgj0kcrh")))

(define-public crate-fenris-solid-0.0.21 (c (n "fenris-solid") (v "0.0.21") (d (list (d (n "fenris") (r "^0.0.21") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.2") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1m385byxnl3d6haqvbj9icy68w45g73wgwzz2b0r2dpfwhfc27m0")))

(define-public crate-fenris-solid-0.0.22 (c (n "fenris-solid") (v "0.0.22") (d (list (d (n "fenris") (r "^0.0.22") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.2") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1l6sl154fgb6bpz7r1bcycij87synlxbxmcmrk51ijjvjviajgq6")))

(define-public crate-fenris-solid-0.0.23 (c (n "fenris-solid") (v "0.0.23") (d (list (d (n "fenris") (r "^0.0.23") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.2") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1ll1fk5waxn6p479yixp6iw7wblscxjdi1aqj9sy4wl4q3iicyb5")))

(define-public crate-fenris-solid-0.0.24 (c (n "fenris-solid") (v "0.0.24") (d (list (d (n "fenris") (r "^0.0.24") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1141j5779sshz99w0gb324xfb1x33zadfi6m7mzm2jckhps8zjlj")))

(define-public crate-fenris-solid-0.0.25 (c (n "fenris-solid") (v "0.0.25") (d (list (d (n "fenris") (r "^0.0.25") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1mldaszw3dfv711hgk5gsbaqj16ryadfm2p0zd0nb89ymfhy7k71")))

(define-public crate-fenris-solid-0.0.26 (c (n "fenris-solid") (v "0.0.26") (d (list (d (n "fenris") (r "^0.0.26") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "02hwzipgv25i5996jf6fd2m2qg3mjls95ya3qrqvkh2rznilnvlq")))

(define-public crate-fenris-solid-0.0.27 (c (n "fenris-solid") (v "0.0.27") (d (list (d (n "fenris") (r "^0.0.27") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0vj1qc724jbdcrqmrfb6s4srs9f68icxyakabs01nayvzb607m9s")))

(define-public crate-fenris-solid-0.0.28 (c (n "fenris-solid") (v "0.0.28") (d (list (d (n "fenris") (r "^0.0.28") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "06kabi72ygz2k7azp80z3z391a82h38rqr6abm2i10ahcpz4jsrw")))

(define-public crate-fenris-solid-0.0.29 (c (n "fenris-solid") (v "0.0.29") (d (list (d (n "fenris") (r "^0.0.29") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "11y7r40d1c45vmz22al7dpbq7hmcdvyjzdvrrzhb9crplhwkvhd2")))

(define-public crate-fenris-solid-0.0.30 (c (n "fenris-solid") (v "0.0.30") (d (list (d (n "fenris") (r "^0.0.30") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0gnl2j5ar0dv2g0fs2iqvmzcpnfnqp97fbal6xnwq9iws44xvsjx")))

(define-public crate-fenris-solid-0.0.31 (c (n "fenris-solid") (v "0.0.31") (d (list (d (n "fenris") (r "^0.0.30") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0wpw4074aicrq6zl1n028avrik526k8bncc6q3acpgys028yh5yd")))

(define-public crate-fenris-solid-0.0.32 (c (n "fenris-solid") (v "0.0.32") (d (list (d (n "fenris") (r "^0.0.32") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "0j8yqw77i6l356gxz9jw2k39sazraikydfhplkvk3v2yd1s93z7c")))

(define-public crate-fenris-solid-0.0.33 (c (n "fenris-solid") (v "0.0.33") (d (list (d (n "fenris") (r "^0.0.33") (d #t) (k 0)) (d (n "fenris-optimize") (r "^0.0.3") (d #t) (k 2)) (d (n "matrixcompare") (r "^0.3.0") (d #t) (k 2)) (d (n "numeric_literals") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)))) (h "1kf7iqp0c32g8ylm7an8dw9lb014ghsarj7ykfqbhz6z99klj5fm")))

