(define-module (crates-io fe nr fenris-paradis) #:use-module (crates-io))

(define-public crate-fenris-paradis-0.0.1 (c (n "fenris-paradis") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "fenris-nested-vec") (r "^0.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "070j9id6qa7475wgxkjk4pha9chvyabbfsv2r2mdc23giyps0wpn")))

(define-public crate-fenris-paradis-0.0.2 (c (n "fenris-paradis") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "fenris-nested-vec") (r "^0.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xka1c5lzj1p4pji32nlsic2nkx3b73b0kbdqpj8z2pck1jl8qyb")))

(define-public crate-fenris-paradis-0.0.3 (c (n "fenris-paradis") (v "0.0.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "fenris-nested-vec") (r "^0.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15p0ikbswq1ij1k4n78k1idvbsfhbs793gh4acivfjvmh4mywwzx")))

