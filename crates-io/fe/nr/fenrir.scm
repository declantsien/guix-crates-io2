(define-module (crates-io fe nr fenrir) #:use-module (crates-io))

(define-public crate-fenrir-0.1.0 (c (n "fenrir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "femme") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)))) (h "0208qcrph30clp8h6gyxlz53j7ljhczlvpghgw67bakn129hc84s")))

(define-public crate-fenrir-0.1.1 (c (n "fenrir") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^1.0") (d #t) (k 0)))) (h "0rh6imrj9agclyk8000m4717nslyzg7kfm71l6z3ragdzssk5s8z")))

