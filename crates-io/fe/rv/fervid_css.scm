(define-module (crates-io fe rv fervid_css) #:use-module (crates-io))

(define-public crate-fervid_css-0.0.1 (c (n "fervid_css") (v "0.0.1") (d (list (d (n "cssparser") (r "^0.29.6") (d #t) (k 0)) (d (n "lightningcss") (r "^1.0.0-alpha.41") (f (quote ("visitor"))) (d #t) (k 0)) (d (n "parcel_selectors") (r "^0.25.3") (d #t) (k 0)))) (h "11kzglsbccz11hgsdjqilh46840ihqksz2vq6xcyz583lh2hf2b3")))

(define-public crate-fervid_css-0.1.0 (c (n "fervid_css") (v "0.1.0") (d (list (d (n "fervid_core") (r "^0.1") (d #t) (k 0)) (d (n "swc_core") (r "0.86.*") (f (quote ("ecma_ast" "common"))) (d #t) (k 0)) (d (n "swc_css_ast") (r "^0.140.13") (d #t) (k 0)) (d (n "swc_css_codegen") (r "^0.151.21") (d #t) (k 0)) (d (n "swc_css_parser") (r "^0.150.20") (d #t) (k 0)))) (h "0bzid9hfvlrx4dh0zhhmhqkjpbli7k4g91208ljxw2lqlqm5msyd")))

