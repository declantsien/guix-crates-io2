(define-module (crates-io fe rv fervid_core) #:use-module (crates-io))

(define-public crate-fervid_core-0.0.1 (c (n "fervid_core") (v "0.0.1") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "swc_core") (r "0.75.*") (f (quote ("common_plugin_transform" "ecma_ast"))) (d #t) (k 0)))) (h "0gcqmd0kq8qhzvszfyrmap8pfs5n9kwnplbxdv4h2v97a4vhs61b")))

(define-public crate-fervid_core-0.1.0 (c (n "fervid_core") (v "0.1.0") (d (list (d (n "flagset") (r "^0.4.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_core") (r "0.86.*") (f (quote ("ecma_ast" "common" "ecma_ast"))) (d #t) (k 0)))) (h "00clc0l3zk54frxnfx30kglirmi3a3a043ak4jrh2awkm9mzha4w")))

(define-public crate-fervid_core-0.1.1 (c (n "fervid_core") (v "0.1.1") (d (list (d (n "flagset") (r "^0.4.3") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "swc_core") (r "0.86.*") (f (quote ("ecma_ast" "common" "ecma_ast"))) (d #t) (k 0)))) (h "0j0lwlghiyqc8b5azmmmzm30ki7clw707nxscwmi1fha7nmwkr87")))

