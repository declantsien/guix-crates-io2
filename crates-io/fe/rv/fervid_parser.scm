(define-module (crates-io fe rv fervid_parser) #:use-module (crates-io))

(define-public crate-fervid_parser-0.1.0 (c (n "fervid_parser") (v "0.1.0") (d (list (d (n "fervid_core") (r "^0.1") (d #t) (k 0)) (d (n "swc_core") (r "0.86.*") (f (quote ("ecma_ast" "common" "ecma_ast"))) (d #t) (k 0)) (d (n "swc_ecma_parser") (r "0.141.*") (d #t) (k 0)) (d (n "swc_html_ast") (r "0.33.*") (d #t) (k 0)) (d (n "swc_html_parser") (r "0.39.*") (d #t) (k 0)))) (h "1x7iy25x2nzrgq7y5dzmrn1gqfb3r8rywqgpv08lsqlzv56y89z8")))

