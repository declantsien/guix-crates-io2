(define-module (crates-io fe rm ferment-macro) #:use-module (crates-io))

(define-public crate-ferment-macro-0.1.0 (c (n "ferment-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02wy2xb65jrfzjjc083nkjb973r53rk7b9lj7f6bv28fkxl1wkkw")))

(define-public crate-ferment-macro-0.1.1 (c (n "ferment-macro") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12v07i94dg06cwyv5ah7j6fa6s85nmqj3vhxhbd9562hk5nm5lw4")))

(define-public crate-ferment-macro-0.1.4 (c (n "ferment-macro") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1b773s17nsrxayp6mamis6wwnn0b56wc34s6vimndmmh70f7bljm")))

