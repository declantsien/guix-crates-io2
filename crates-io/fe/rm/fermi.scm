(define-module (crates-io fe rm fermi) #:use-module (crates-io))

(define-public crate-fermi-0.1.0 (c (n "fermi") (v "0.1.0") (h "0fz3p196d1c5h4ch3j7jr92p4zs9dg869mrmglalda844dc7mfa3")))

(define-public crate-fermi-0.2.0 (c (n "fermi") (v "0.2.0") (d (list (d (n "closure") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1wc5mkl4xrq5xx5cq0h6wf6lhcdn38wdav4cnxzxa2jr9lz66laa")))

(define-public crate-fermi-0.2.1 (c (n "fermi") (v "0.2.1") (d (list (d (n "closure") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.2.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "15drz1rn1gpsir60491fhcwnr5wwnr39kl12csj65hvj7451fg5a")))

(define-public crate-fermi-0.3.0 (c (n "fermi") (v "0.3.0") (d (list (d (n "closure") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.3.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0clhyk9wi5n4mdml76maqpxy1bmy1iza15qf30q1gnmx8rmfzjzq")))

(define-public crate-fermi-0.4.0 (c (n "fermi") (v "0.4.0") (d (list (d (n "closure") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.4.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "13qdh06gw37bccspp6cql4mfh03dj7q5xc4dqd3p07hicaqjsb62")))

(define-public crate-fermi-0.4.3 (c (n "fermi") (v "0.4.3") (d (list (d (n "closure") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-core") (r "^0.4.2") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1hb6n79qdns0zcizn63x1w7cgaxvfb18vnpzr3d03n1sy3rvxi3b")))

