(define-module (crates-io fe rm ferment-interfaces) #:use-module (crates-io))

(define-public crate-ferment-interfaces-0.1.0 (c (n "ferment-interfaces") (v "0.1.0") (h "0d571lq924irmar0fm88qvcja5yz9npsfp6mfxv4jxvmkgync985")))

(define-public crate-ferment-interfaces-0.1.1 (c (n "ferment-interfaces") (v "0.1.1") (h "1n46ydmdps6b20y9q6bryvn6cg0nwdax3shsj7rzhgf96mzficcc")))

(define-public crate-ferment-interfaces-0.1.4 (c (n "ferment-interfaces") (v "0.1.4") (h "1ab6h3wxpzmrdjzcmbwzi43014hhq5ig1qknbb6sav1sj4bb64z3")))

