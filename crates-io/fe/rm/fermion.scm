(define-module (crates-io fe rm fermion) #:use-module (crates-io))

(define-public crate-fermion-0.1.0 (c (n "fermion") (v "0.1.0") (d (list (d (n "encode_unicode") (r "^0.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "039g86myi7gkpf0l2npk5df6bbm6xg4s36g51wzq75871mrk2ryj") (f (quote (("std") ("default" "std"))))))

(define-public crate-fermion-0.2.0 (c (n "fermion") (v "0.2.0") (d (list (d (n "encode_unicode") (r "^0.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "130s80ngg3n9d86zbhvklyi5shl5654ks2pwm0gb36b9frhs8sa9") (f (quote (("std") ("default" "std"))))))

