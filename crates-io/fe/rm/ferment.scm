(define-module (crates-io fe rm ferment) #:use-module (crates-io))

(define-public crate-ferment-0.1.0 (c (n "ferment") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1fyqypfnj263x13xsc3a66p083ldzwd85fqixjh288v0q7xsph1s")))

(define-public crate-ferment-0.1.1 (c (n "ferment") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1mx9qryhcrjs6icy8insrm8sqhgx5hds850927pqh0673nsk53g9")))

(define-public crate-ferment-0.1.4 (c (n "ferment") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1znr8syrr1jvv1mlm31r3h0ph1q18316a1big9907hz7xy97561q")))

