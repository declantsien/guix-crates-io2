(define-module (crates-io fe rn fern_macros) #:use-module (crates-io))

(define-public crate-fern_macros-0.1.0 (c (n "fern_macros") (v "0.1.0") (d (list (d (n "fern") (r "^0.1.0") (d #t) (k 0)))) (h "0mqramwrlka0ah57srs1cqjrjq50s3irx1y1zcrapx41xkfczsgj") (y #t)))

(define-public crate-fern_macros-0.1.1 (c (n "fern_macros") (v "0.1.1") (d (list (d (n "fern") (r "^0.1.3") (d #t) (k 0)))) (h "0nfhjlw62d0rbczn7zsmfmxn2pjcjdll2wm7wa1pq0r5ra6sm0ss") (y #t)))

(define-public crate-fern_macros-0.1.2 (c (n "fern_macros") (v "0.1.2") (d (list (d (n "fern") (r "^0.1.6") (d #t) (k 0)))) (h "0gw2wq3pk8jh1wd87x3xjl01v2igjlbmr31hpwiznjp7ncx3qsp2") (y #t)))

(define-public crate-fern_macros-0.1.3 (c (n "fern_macros") (v "0.1.3") (d (list (d (n "fern") (r "^0.1.6") (d #t) (k 0)))) (h "083lwscwqcr54l8zksl6whnmmzk9wd0whqg3v1jqg9v5n4zlqr5p") (y #t)))

(define-public crate-fern_macros-0.1.4 (c (n "fern_macros") (v "0.1.4") (d (list (d (n "fern") (r "^0.1.6") (d #t) (k 0)))) (h "1sri24b746j86sziy4fyvwkgizdcv503mw5cnc089la2qjmhbn7p") (y #t)))

(define-public crate-fern_macros-0.1.5 (c (n "fern_macros") (v "0.1.5") (d (list (d (n "fern") (r "^0.1.6") (d #t) (k 0)))) (h "0f505aw2fncagfjb1zcy5xpcnrmpq65p661lq6ywkjhs18jaz59w") (y #t)))

(define-public crate-fern_macros-0.1.6 (c (n "fern_macros") (v "0.1.6") (d (list (d (n "fern") (r "^0.1.6") (d #t) (k 0)))) (h "04javin69sajpnr6pqfx8a6kqn8n3rb0ljm4rsi4cz8l3mac78g5") (y #t)))

(define-public crate-fern_macros-0.1.7 (c (n "fern_macros") (v "0.1.7") (d (list (d (n "fern") (r "^0.1.7") (d #t) (k 0)))) (h "09lkvgl75ijgg7803lrm4jsgyqk8q6i8ksdznla6vmlgwnmz05dh") (y #t)))

(define-public crate-fern_macros-0.1.8 (c (n "fern_macros") (v "0.1.8") (d (list (d (n "fern") (r "^0.1.7") (d #t) (k 0)))) (h "1w5xwmxlfrw6lhrandylccf4d3a50w2s367x76xmldnj2jyfaik1")))

