(define-module (crates-io fe rn fern-proxy-interfaces) #:use-module (crates-io))

(define-public crate-fern-proxy-interfaces-0.1.0 (c (n "fern-proxy-interfaces") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "config") (r "^0.13") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1fplh33s6vkyj0kpgi8fsrs180z54gh2aab132xad6x4lh6a8ccr") (r "1.63")))

