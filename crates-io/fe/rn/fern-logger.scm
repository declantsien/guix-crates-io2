(define-module (crates-io fe rn fern-logger) #:use-module (crates-io))

(define-public crate-fern-logger-0.1.0 (c (n "fern-logger") (v "0.1.0") (d (list (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "time-helper") (r "^0.1.0") (k 0)))) (h "08lhar8lmi8lwzrli40z7qcmzlh2i8vsg81y34hpzcmldjcsrlxl")))

(define-public crate-fern-logger-0.2.0 (c (n "fern-logger") (v "0.2.0") (d (list (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "time-helper") (r "^0.1.0") (k 0)))) (h "1s6zj5yfdx64c061czsfi1m17l7bl5bh2dflgqnm2r35irjw4qq7")))

(define-public crate-fern-logger-0.3.0 (c (n "fern-logger") (v "0.3.0") (d (list (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "time-helper") (r "^0.1.0") (k 0)))) (h "1wnfdnn8agiqwq431g8v7bcprkaqv2ckcgn6swam5js6j8hzw7hd")))

(define-public crate-fern-logger-0.4.0 (c (n "fern-logger") (v "0.4.0") (d (list (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "time-helper") (r "^0.1.0") (k 0)))) (h "1g34pfznr1fi4nwmsgrnr5z2cwn8c8n9zk1y06yp3vja8grfwgy7")))

(define-public crate-fern-logger-0.5.0 (c (n "fern-logger") (v "0.5.0") (d (list (d (n "fern") (r "^0.6.0") (f (quote ("colored"))) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (k 0)) (d (n "time-helper") (r "^0.1.0") (k 0)))) (h "17rsapf7v18z4r2plf30365k708szzzgsp99kfwdrwh4z3bwnbyp")))

