(define-module (crates-io fe rn fern) #:use-module (crates-io))

(define-public crate-fern-0.1.0 (c (n "fern") (v "0.1.0") (h "0fwcjl4vljihb1jmi4n9mbly07cy2w37p2igwclz3rd9c2792gyw")))

(define-public crate-fern-0.1.1 (c (n "fern") (v "0.1.1") (h "1w6zgj1p8k02znznbclzg160ywln0ya3zw6lkcqai9gid4bhvfa9")))

(define-public crate-fern-0.1.2 (c (n "fern") (v "0.1.2") (h "1mklc6f8rrx3mzrxrv7r2va8c7z34g8rd0j9mbsy5p3pb1v6r4c2")))

(define-public crate-fern-0.1.3 (c (n "fern") (v "0.1.3") (h "0gkri9b62rblkb6i3imsl712r6nv74m3p2cyxs6s7c75161bb1wg")))

(define-public crate-fern-0.1.4 (c (n "fern") (v "0.1.4") (h "1wyc091jz86r5x82xj8kp0jwnr3hrsrgsv8zd07nwwcn6kyawa78")))

(define-public crate-fern-0.1.5 (c (n "fern") (v "0.1.5") (h "1kcma704k8pzxkgfdb73ygnhrjw07zl6hvyyn7l9vx3mid959azs")))

(define-public crate-fern-0.1.6 (c (n "fern") (v "0.1.6") (h "0r70m24djny9knx75gxw65srpf9cfxy2lcxkwgpscddb4666nb6w")))

(define-public crate-fern-0.1.7 (c (n "fern") (v "0.1.7") (h "0cjhzsl0yh9i54i2zg5hv6w4snr4d2m81kxf4m4xm1lyynisbvh9")))

(define-public crate-fern-0.1.8 (c (n "fern") (v "0.1.8") (h "0hz8872aprn2mn54k1ia0sq4nd499sz4i0bpdjx6f8wig8dkzrq4")))

(define-public crate-fern-0.1.10 (c (n "fern") (v "0.1.10") (h "1h1l38v56fjq7whqxqg3ax4gabnr7w6642l3wgb7hsfws7z31iac")))

(define-public crate-fern-0.1.11 (c (n "fern") (v "0.1.11") (h "013w8mwg9s75dxgpmrzvrjscrbi791mmnfbgmvrzcl2n9crwglc4")))

(define-public crate-fern-0.1.12 (c (n "fern") (v "0.1.12") (h "0x6xy2s29lj77654ys6vq5fg6lfcpixvzsy7plz0jqgay2n667rb")))

(define-public crate-fern-0.2.0 (c (n "fern") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2.5") (d #t) (k 2)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "1gbpsz65n3f2xc80sjssbjgq7b5gh1lx0ilbmkn5vgbchmkfnfvi")))

(define-public crate-fern-0.2.1 (c (n "fern") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2.5") (d #t) (k 2)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)))) (h "0rh7l5yf7srvaw7rnbgh1zzwjfps2rjxjl09d6qly1kr4b2vxz1c")))

(define-public crate-fern-0.3.0 (c (n "fern") (v "0.3.0") (d (list (d (n "chrono") (r "0.2.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)))) (h "0c5x5ir0lnpxnpzka17w2h81x7781sshpri244hi71z19vpjz3np")))

(define-public crate-fern-0.3.1 (c (n "fern") (v "0.3.1") (d (list (d (n "chrono") (r "0.2.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)))) (h "1iny2jq4r7mgmw0cm1n0n8x0j03g5daas4jw44py65l7zn3k4z0y")))

(define-public crate-fern-0.3.2 (c (n "fern") (v "0.3.2") (d (list (d (n "chrono") (r "0.2.*") (d #t) (k 2)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)))) (h "0w0i8kxljbxx702jgyh08b4bmknclbigzv30jjhj3p3cx6jpvfg9")))

(define-public crate-fern-0.3.3 (c (n "fern") (v "0.3.3") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)) (d (n "time") (r "0.1.*") (d #t) (k 2)))) (h "19cfq5dszk993dfdyrbb9a4qz7nzldisrir743kbbrwnyw0ham50")))

(define-public crate-fern-0.3.4 (c (n "fern") (v "0.3.4") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)) (d (n "time") (r "0.1.*") (d #t) (k 2)))) (h "0hn2vvn4dg4npff021mqdmzh9cvw64yzy22l0484qchp9iwnvr1q")))

(define-public crate-fern-0.3.5 (c (n "fern") (v "0.3.5") (d (list (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "tempdir") (r "0.3.*") (d #t) (k 2)) (d (n "time") (r "0.1.*") (d #t) (k 2)))) (h "1cjlq01a8bzrwyp85pb4qpn2lbgy84skz2nmmazr2xxdag85hbsd")))

(define-public crate-fern-0.4.0 (c (n "fern") (v "0.4.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ymlm259nkih88wirx9y3ssn0d8n1sh0k8j801h6j3r11q6kw9w9")))

(define-public crate-fern-0.4.1 (c (n "fern") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qn7vx4g3v7dka56q9mjnk348j71k8wf05zdx4pf8cj5m01ss6z8")))

(define-public crate-fern-0.4.2 (c (n "fern") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "12m44if6zffpmji2szhm1lyj932a7llm2zqlq8awp8jpp1a82an7")))

(define-public crate-fern-0.4.3 (c (n "fern") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0q7klq24h3v5l059ybifnb7y7xhrcy58vzffwiy5r42jg1irf0hz")))

(define-public crate-fern-0.4.4 (c (n "fern") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a6ih357y89hab38x17vfjx09bp3hrvz64ksgmi4v5pi8n7wfmh0")))

(define-public crate-fern-0.5.0 (c (n "fern") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1dbc7lh9xj9rrwi69jnjvvpgj8qp3p67ppvlga5gl90m0w1f2j0s")))

(define-public crate-fern-0.5.1 (c (n "fern") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0pr8lwnp3dw59d0x6k2ldd28107panf918a8c2xw51yfivbwgyyq")))

(define-public crate-fern-0.5.2 (c (n "fern") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1g7yxxv5nlir5q6bs8ma4ha0pizc3ka1h77q7hgv0a4w3cklc3sr") (f (quote (("syslog-3" "syslog"))))))

(define-public crate-fern-0.5.3 (c (n "fern") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ikkdx4d90if1lwlf66bmqi4i3v3zzgsdsa3ca09152immsfiidp") (f (quote (("syslog-3" "syslog") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.4 (c (n "fern") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17ccrhldm2imvcai6zlpi8q579wf8a4ihkbffqy38mnczi8mcish") (f (quote (("syslog-3" "syslog") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.5 (c (n "fern") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0p5whph64gphjniz42swrq6bj9d5c8pi6rg9d6w96pbqmac7h8yy") (f (quote (("syslog-3" "syslog") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.6 (c (n "fern") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^4") (o #t) (d #t) (k 0)) (d (n "syslog3") (r "^3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1j4l3d5gv65zxh3w4a0vfk21j966xjq00bgbhdcr7bw31bh5z4ap") (f (quote (("syslog-4" "syslog") ("syslog-3" "syslog3") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.7 (c (n "fern") (v "0.5.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^4") (o #t) (d #t) (k 0)) (d (n "syslog3") (r "^3") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "008az0hcy6qzqy3jpajl53wrqw3fwscmb2llxyx132wkmy5gi2ml") (f (quote (("syslog-4" "syslog") ("syslog-3" "syslog3") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.8 (c (n "fern") (v "0.5.8") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "syslog") (r "^4") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "syslog3") (r "^3") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1pf7g36gg9zfrijrklrwp74ddx3b1p0nxrj6cyax2cylyjh6zli9") (f (quote (("syslog-4" "syslog") ("syslog-3" "syslog3") ("meta-logging-in-format"))))))

(define-public crate-fern-0.5.9 (c (n "fern") (v "0.5.9") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "reopen") (r "^0.3") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "syslog3") (r "^3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog4") (r "^4") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1anslk0hx9an4ypcaxqff080hgbcxm7ji7d4qf4f6qx1mkav16p6") (f (quote (("syslog-4" "syslog4") ("syslog-3" "syslog3") ("reopen-03" "reopen" "libc") ("meta-logging-in-format"))))))

(define-public crate-fern-0.6.0 (c (n "fern") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "reopen") (r "^0.3") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "syslog3") (r "^3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog4") (r "^4") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0rghkbmpm7ckchd2fr2ifahprc7ll3qs0fbwsspsgj6cy0h4i6lc") (f (quote (("syslog-4" "syslog4") ("syslog-3" "syslog3") ("reopen-03" "reopen" "libc") ("meta-logging-in-format") ("date-based" "chrono"))))))

(define-public crate-fern-0.6.1 (c (n "fern") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (k 2)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "reopen03") (r "^0.3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "reopen")) (d (n "reopen1") (r "~1") (f (quote ("signals"))) (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "reopen")) (d (n "syslog3") (r "^3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog4") (r "^4") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog6") (r "^6") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0ahys5fmc10vcgf6yyai0jiypl8pqwidydhqkbp7jph79447pp9v") (f (quote (("syslog-6" "syslog6") ("syslog-4" "syslog4") ("syslog-3" "syslog3") ("reopen-1" "reopen1" "libc") ("reopen-03" "reopen03" "libc") ("meta-logging-in-format") ("date-based" "chrono"))))))

(define-public crate-fern-0.6.2 (c (n "fern") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std" "clock"))) (o #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 2)) (d (n "colored") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.58") (o #t) (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "reopen03") (r "^0.3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "reopen")) (d (n "reopen1") (r "~1") (f (quote ("signals"))) (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "reopen")) (d (n "syslog3") (r "^3") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog4") (r "^4") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "syslog6") (r "^6") (o #t) (d #t) (t "cfg(not(windows))") (k 0) (p "syslog")) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1vpinainw32498p0zydmxc24yd3r6479pmhdfb429mfbji3c3w6r") (f (quote (("syslog-6" "syslog6") ("syslog-4" "syslog4") ("syslog-3" "syslog3") ("reopen-1" "reopen1" "libc") ("reopen-03" "reopen03" "libc") ("meta-logging-in-format") ("date-based" "chrono")))) (r "1.31")))

