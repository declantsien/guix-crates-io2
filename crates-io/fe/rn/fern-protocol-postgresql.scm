(define-module (crates-io fe rn fern-protocol-postgresql) #:use-module (crates-io))

(define-public crate-fern-protocol-postgresql-0.1.0 (c (n "fern-protocol-postgresql") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fern-proxy-interfaces") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2") (f (quote ("log"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "02jcdqvs4lcdh2d04238ccmfsdizx1pa8sm23jzbrljcw259ax2l") (r "1.63")))

