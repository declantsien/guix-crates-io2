(define-module (crates-io fe rn fern-masking) #:use-module (crates-io))

(define-public crate-fern-masking-0.1.0 (c (n "fern-masking") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fern-protocol-postgresql") (r "^0.1") (d #t) (k 0)) (d (n "fern-proxy-interfaces") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1asfbwrksrcyhv0r1qxq3pqwi59i1bz8myghpfh3nqxfhhwxw3s9") (r "1.63")))

(define-public crate-fern-masking-0.1.1 (c (n "fern-masking") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "fern-protocol-postgresql") (r "^0.1") (d #t) (k 0)) (d (n "fern-proxy-interfaces") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ycvhd80c7q8rx9b4djds243409h6gcgmgz40iaxq6jy6gj17ag7") (r "1.63")))

