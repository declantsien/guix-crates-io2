(define-module (crates-io fe rn fern-proxy) #:use-module (crates-io))

(define-public crate-fern-proxy-0.1.0 (c (n "fern-proxy") (v "0.1.0") (d (list (d (n "config") (r "^0.13") (f (quote ("toml"))) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fern-masking") (r "^0.1") (d #t) (k 0)) (d (n "fern-protocol-postgresql") (r "^0.1") (d #t) (k 0)) (d (n "fern-proxy-interfaces") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "macros" "net" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (d #t) (k 0)))) (h "0g4cgrkhp314hvwmml3fqxw9vsa5jjfadmkh61l4s9va5b8ac5j2") (r "1.63")))

