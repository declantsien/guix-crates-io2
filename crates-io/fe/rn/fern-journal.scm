(define-module (crates-io fe rn fern-journal) #:use-module (crates-io))

(define-public crate-fern-journal-0.1.0 (c (n "fern-journal") (v "0.1.0") (d (list (d (n "fern") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "systemd") (r "0.*") (d #t) (k 0)))) (h "1q33abq8xcdir8p76vyjhxr60nf2naji43mv76xf032k1dx8dd49")))

(define-public crate-fern-journal-0.2.0 (c (n "fern-journal") (v "0.2.0") (d (list (d (n "fern") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "systemd") (r "^0.1.0") (d #t) (k 0)))) (h "1mif2mwrqdy5xx5m8n3didirxggb84cn39xwc75m8hqwcvnanyj9")))

