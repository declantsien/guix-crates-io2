(define-module (crates-io fe di fedimint-aead) #:use-module (crates-io))

(define-public crate-fedimint-aead-0.0.1 (c (n "fedimint-aead") (v "0.0.1") (h "11cqyb4k08zflbp654nqsqjnvly68rhdqdz86ymp2q8sf9124fys")))

(define-public crate-fedimint-aead-0.2.0-rc2 (c (n "fedimint-aead") (v "0.2.0-rc2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0n8d4h29dx7s3gqh67n2ak5s8y75d6vvjkadvaivqndd25zj5mrc")))

(define-public crate-fedimint-aead-0.2.0-rc3 (c (n "fedimint-aead") (v "0.2.0-rc3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1krsb0ghxqmkr71i6ld9d6jb8djrh8shd12r00m26n20fdfdml21")))

(define-public crate-fedimint-aead-0.2.0-rc4 (c (n "fedimint-aead") (v "0.2.0-rc4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0gxas5nvpxc9cz2wz0dcm93ajcjqiscv6np94zyds6zc3jsgcp94")))

(define-public crate-fedimint-aead-0.2.0-rc5 (c (n "fedimint-aead") (v "0.2.0-rc5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0gwx37b9svacpdvxgfh7lwfnqjjda1f4wvr5mk13v3k5cqmb61l5")))

(define-public crate-fedimint-aead-0.2.0-rc7 (c (n "fedimint-aead") (v "0.2.0-rc7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0k7105x8f50x2pyi4b2k9npbfxgbawxjw295ygdpbahrzbi6684r")))

(define-public crate-fedimint-aead-0.2.0-rc8 (c (n "fedimint-aead") (v "0.2.0-rc8") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "19pidxz507ih4krc4phblkrp0dkrsfrcy170c1mwr29azb65har0")))

(define-public crate-fedimint-aead-0.2.0-rc9 (c (n "fedimint-aead") (v "0.2.0-rc9") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "03ypzrc4rwwsfr0zbs7b3p8a9ch0sgmxji5wg4sxc23f151ajfcc")))

(define-public crate-fedimint-aead-0.2.0 (c (n "fedimint-aead") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1sr9p3i7yfdixf1zv59iyxmyy54k55yzjwh0nkax0a47kx2mr62x")))

(define-public crate-fedimint-aead-0.2.1-rc1 (c (n "fedimint-aead") (v "0.2.1-rc1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1sdxy1faa8ml39rcw4ly3vkj7l44by58cji8id5zykd8bjfay1vc")))

(define-public crate-fedimint-aead-0.2.1-rc2 (c (n "fedimint-aead") (v "0.2.1-rc2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1pfizk1s2752h227l8fk418k4hiz040y11n7dn277qhmidcjzp23")))

(define-public crate-fedimint-aead-0.2.1-rc3 (c (n "fedimint-aead") (v "0.2.1-rc3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0b2mc3s8v6injsf279m1hzdpckyr43q20bdr0wi7yg5c1vzrnf1h")))

(define-public crate-fedimint-aead-0.2.1-rc4 (c (n "fedimint-aead") (v "0.2.1-rc4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0d9w2p8ngzlnykkahzm1fzr5mb92v4wl8k5jy05hhwckfvppbal6")))

(define-public crate-fedimint-aead-0.2.1 (c (n "fedimint-aead") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "16idcli7672fq7w0yjqifan6vd3zx9zq6910llr8mdwg8780m26f")))

(define-public crate-fedimint-aead-0.2.2-rc1 (c (n "fedimint-aead") (v "0.2.2-rc1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "01bdjf6ix3yqxdmn932mb6qk0zl7dqy4b4n7qqr6inr03vaa8bqp")))

(define-public crate-fedimint-aead-0.2.2-rc2 (c (n "fedimint-aead") (v "0.2.2-rc2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0pv4c9a7l05sjlshrb16jdby3ws0fys6adv8nyna2yii4h2zpx9n")))

(define-public crate-fedimint-aead-0.2.2-rc3 (c (n "fedimint-aead") (v "0.2.2-rc3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1rs8fkiihfr9y719sk9sm0f3fy1bllc2v2ki9a0hr3xprgdvjfb1")))

(define-public crate-fedimint-aead-0.2.2-rc5 (c (n "fedimint-aead") (v "0.2.2-rc5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1lsk6497j5i137cbbx166jyvvvxygn52kk0zfl4508cziw774n8b")))

(define-public crate-fedimint-aead-0.2.2-rc6 (c (n "fedimint-aead") (v "0.2.2-rc6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1jr9l6z08kk5ybk1g6x20b7qzk2683jywra5wx0rhgfiz67bjblx")))

(define-public crate-fedimint-aead-0.2.2-rc8 (c (n "fedimint-aead") (v "0.2.2-rc8") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0hxwykzqwgyan9zfgvjfwyawvbhq5m600hmwn3bqri2p2yaz54h0")))

(define-public crate-fedimint-aead-0.2.2 (c (n "fedimint-aead") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "argon2") (r "^0.5.0") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0db2bppl02hyn09v3iqp9k99bi4jfva68vpjc8fxrxdpszdxcn32")))

(define-public crate-fedimint-aead-0.3.0-rc.0 (c (n "fedimint-aead") (v "0.3.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "1a5iv90ry3qfnnjsfvbn0qhmklylx082nxm212r3ppmkvv8185wg")))

(define-public crate-fedimint-aead-0.3.0-rc.1 (c (n "fedimint-aead") (v "0.3.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "1n1vvhxrs3jdc1gdw22xsqwprcl3qpkj9mzl0r5xhb77q3wqnzql")))

(define-public crate-fedimint-aead-0.3.0-rc.2 (c (n "fedimint-aead") (v "0.3.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "01ks17haqrlijvb0hnyk9c4wbqg5x598mdh13v1k1hx7p0m69m43")))

(define-public crate-fedimint-aead-0.3.0-rc.3 (c (n "fedimint-aead") (v "0.3.0-rc.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "1p4bvln7hy4xrycinm6k8xfqj732ssbcvrrgbv646b62prwfwdn3")))

(define-public crate-fedimint-aead-0.3.0 (c (n "fedimint-aead") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "1zcr8kgp5kkhhi6wzxglqpwrr28vv6ywk0gpha0lmv525b93iamn")))

(define-public crate-fedimint-aead-0.3.1-rc.0 (c (n "fedimint-aead") (v "0.3.1-rc.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "0lvs14l83n0dm8m1i4xki65nqvxkspp8j80h9g7hwyf3jng780ip")))

(define-public crate-fedimint-aead-0.3.1-rc.1 (c (n "fedimint-aead") (v "0.3.1-rc.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "0y26xv8vvmy53h7nwxpq2aazh4l12z8hqfnzibwyq4iyvxswgwwn")))

(define-public crate-fedimint-aead-0.3.1 (c (n "fedimint-aead") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "0hfv960rzzpxbcbv08sqydf68xppfhjm8ij57bpxsp5zjw8i6ips")))

(define-public crate-fedimint-aead-0.3.2-rc.0 (c (n "fedimint-aead") (v "0.3.2-rc.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "argon2") (r "^0.5.3") (f (quote ("password-hash" "alloc"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ring") (r "^0.17.8") (d #t) (k 0)))) (h "1q30nffbhd39mj61zjdxxq04kavb5kc5gfmidg5hck0hsqqmq4mi")))

