(define-module (crates-io fe di fedimint-build) #:use-module (crates-io))

(define-public crate-fedimint-build-0.0.1 (c (n "fedimint-build") (v "0.0.1") (h "1kskwpwdfbq3wxnh4kl1hw1ql6gij2pksdbsg8rdiik42j8kypcd")))

(define-public crate-fedimint-build-0.2.0-rc2 (c (n "fedimint-build") (v "0.2.0-rc2") (h "1hprv9imiwdzz5kpw7im9ql2khrz6zy9p4ad9zjrn5hcvaj0hbc8")))

(define-public crate-fedimint-build-0.2.0-rc3 (c (n "fedimint-build") (v "0.2.0-rc3") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0sqxs1jq7vvq8ppgjfw8s6kwyhyykhdm3b7gz4cgpw1qr3hps5s2")))

(define-public crate-fedimint-build-0.2.0-rc4 (c (n "fedimint-build") (v "0.2.0-rc4") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "02z28arpjqw0bmi1nhwa3gycxvzrcwghdaxj0b28ckxpll56g54m")))

(define-public crate-fedimint-build-0.2.0-rc5 (c (n "fedimint-build") (v "0.2.0-rc5") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "175wgj9v7ywpxxpwrb3l745cg27fg25ki4blz949vnpkgqll6znk")))

(define-public crate-fedimint-build-0.2.0-rc7 (c (n "fedimint-build") (v "0.2.0-rc7") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1aql7wss3a3yc8p2dgf3f5ygcf2gh8nf1jgpmiw53i4s1z0ikq58")))

(define-public crate-fedimint-build-0.2.0-rc8 (c (n "fedimint-build") (v "0.2.0-rc8") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0spk9q02dajf2q7x0nacg36cw7d48c6d9qxgva0hr0rl3yyy20hi")))

(define-public crate-fedimint-build-0.2.0-rc9 (c (n "fedimint-build") (v "0.2.0-rc9") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0qcb7kq371dn2b9wbc5c2zr2nk1r2l7x4mzbbbrjlrrx7alkq197")))

(define-public crate-fedimint-build-0.2.0 (c (n "fedimint-build") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "158z4j9zd1bwx403b41rarrrbj74gmxv3azm67f4lc9bfil8a227")))

(define-public crate-fedimint-build-0.2.1-rc1 (c (n "fedimint-build") (v "0.2.1-rc1") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "03p9l873pbjf3cwx563rw71fx2j4jg3k7s174g6k4smdmasxrfsc")))

(define-public crate-fedimint-build-0.2.1-rc2 (c (n "fedimint-build") (v "0.2.1-rc2") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "168h64ph4x579p2aj76xh8ad62s1iqs2sqsvpinx09p897iirwgh")))

(define-public crate-fedimint-build-0.2.1-rc3 (c (n "fedimint-build") (v "0.2.1-rc3") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1sgcqnz8x212smzhqf1yvxzivi9gaa9ci0ncfnk0mnahlhk7sc5j")))

(define-public crate-fedimint-build-0.2.1-rc4 (c (n "fedimint-build") (v "0.2.1-rc4") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0yhq2xs3shw90ipmdc5pi0v334h1yc9y7j6xqqlkp51fjyqdv7cq")))

(define-public crate-fedimint-build-0.2.1 (c (n "fedimint-build") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "174iy2604x4dq1rl70i11hywg8ryrnycv3h6ysj22p3d12q0vldn")))

(define-public crate-fedimint-build-0.2.2-rc1 (c (n "fedimint-build") (v "0.2.2-rc1") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0bbjhj6pxz52d1g3fgmchyanvjw8b255kjkivr5q4k5ccvcf44k8")))

(define-public crate-fedimint-build-0.2.2-rc2 (c (n "fedimint-build") (v "0.2.2-rc2") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1arp1nb09hq7j9w40zc2h3bbigspcxs7waskjb16nq73lafvrb9r")))

(define-public crate-fedimint-build-0.2.2-rc3 (c (n "fedimint-build") (v "0.2.2-rc3") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1h7sfki3rhaq9nv6pl2h4x0yvl36ihj70pkrm69jhrm7wl1rbw5d")))

(define-public crate-fedimint-build-0.2.2-rc5 (c (n "fedimint-build") (v "0.2.2-rc5") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "10dbwk8anx0vh10ibxvd74dxsg956lmri35qfblp4bby59g7adxx")))

(define-public crate-fedimint-build-0.2.2-rc6 (c (n "fedimint-build") (v "0.2.2-rc6") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "16p5skx5zw3w49iw1k057hbnbf6ghp2b2w1rr6wxy16g315il6wv")))

(define-public crate-fedimint-build-0.2.2-rc8 (c (n "fedimint-build") (v "0.2.2-rc8") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1zwqgpx0nxm0ykp1jw062g812m15372lg981gbv11jlyq4ard59g")))

(define-public crate-fedimint-build-0.2.2 (c (n "fedimint-build") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "15gr9jvj70k6j9zv852djgmp91ad2vdfcdbckyzkf6gr18vfxnj8")))

(define-public crate-fedimint-build-0.3.0-rc.0 (c (n "fedimint-build") (v "0.3.0-rc.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1sc9lx3nvzw8s8rx61bmcqi81ikly1l8bpl7a6jym141frq29w5h")))

(define-public crate-fedimint-build-0.3.0-rc.1 (c (n "fedimint-build") (v "0.3.0-rc.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0xkm2g8xn700rsdb8z2zsdbb2cjfnjhix3f1hhdyvlky3g4gspr4")))

(define-public crate-fedimint-build-0.3.0-rc.2 (c (n "fedimint-build") (v "0.3.0-rc.2") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0rf163020mkqb3imzbmm157jmylp5ngvrg4hr26qpdmid07s34ix")))

(define-public crate-fedimint-build-0.3.0-rc.3 (c (n "fedimint-build") (v "0.3.0-rc.3") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0068dvqm5xdgq0gja25w87prn7i9m84ng5acmihvxapy9qn48kwy")))

(define-public crate-fedimint-build-0.3.0 (c (n "fedimint-build") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1iifsfbv2yig2bx1zpyc37b93gx6cmm4p11ycq9llli7v4k1azd8")))

(define-public crate-fedimint-build-0.3.1-rc.0 (c (n "fedimint-build") (v "0.3.1-rc.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0rbxr92zkgds06nc4ylzmb6w1yzyi0p5mpylma44d2qcdc0xgl75")))

(define-public crate-fedimint-build-0.3.1-rc.1 (c (n "fedimint-build") (v "0.3.1-rc.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1m3dk34jrsmrxjkla8d67kygl7kb53zmpn3cqfbwpmmzwhfbxf0x")))

(define-public crate-fedimint-build-0.3.1 (c (n "fedimint-build") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1kyqf8y2k9yzc0h65inlz7xjfy5rsrdgb7xr6bqxlhj38zyg6ndi")))

(define-public crate-fedimint-build-0.3.2-rc.0 (c (n "fedimint-build") (v "0.3.2-rc.0") (d (list (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0csy9xg9ri5giznqfrw5ksdfh9lgp759g57fhbqhyfl52z2vys5z")))

