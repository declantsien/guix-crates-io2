(define-module (crates-io fe di fediverse) #:use-module (crates-io))

(define-public crate-fediverse-0.1.0 (c (n "fediverse") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "15kingl4f60y1fk5y4y064k33wkiipwbccq2sgnswj8yx0pnlwic")))

(define-public crate-fediverse-0.1.1 (c (n "fediverse") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "0mdys48m4mlqcfav0im9di5dnd789gsxndyi2cyy2mayfpzybp9v")))

(define-public crate-fediverse-0.1.2 (c (n "fediverse") (v "0.1.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "1s9bl3ibks2cr3ynapfbfhxyy8hyvb5r7nqzklww2aj9jd0834ll")))

(define-public crate-fediverse-0.1.3 (c (n "fediverse") (v "0.1.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "1yd7av55zj52xf56axcb1gd3l2j6x3bf7lswmpm2ddln4cr1w7xl")))

(define-public crate-fediverse-0.1.4 (c (n "fediverse") (v "0.1.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "13pfsjpasyyz2jbpb26fjmxqv0qmidh3qzag6y820xj9c3ap9ln6")))

(define-public crate-fediverse-0.1.5 (c (n "fediverse") (v "0.1.5") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "1jlzds5wjz6vyv7mbwsmmcn5f5lr6ky70daqn33sg481kcr9kk5q")))

(define-public crate-fediverse-0.1.6 (c (n "fediverse") (v "0.1.6") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "voca_rs") (r "^1.15") (d #t) (k 0)))) (h "0zfywrnw31d0lm4z3lgia69ql1ywvcjqg5hk46p7mn8dw304kzna")))

