(define-module (crates-io fe di fedisend) #:use-module (crates-io))

(define-public crate-fedisend-0.1.0 (c (n "fedisend") (v "0.1.0") (d (list (d (n "blake2") (r "~0.8") (d #t) (k 0)) (d (n "json") (r "~0.11") (d #t) (k 0)) (d (n "reqwest") (r "~0.9") (d #t) (k 0)) (d (n "url") (r "~1.7") (d #t) (k 0)))) (h "18xwx65dph80cvgnz3q7qq7k8mfxzwik1lxjlgmfmvack7h87lb3")))

