(define-module (crates-io fe di fedimint-derive) #:use-module (crates-io))

(define-public crate-fedimint-derive-0.0.1 (c (n "fedimint-derive") (v "0.0.1") (h "1gx2g4s9yr3yya6i9ci0yfgck35wpzfymrwk715diwk21a26qw5n")))

(define-public crate-fedimint-derive-0.2.0-rc2 (c (n "fedimint-derive") (v "0.2.0-rc2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pc9qns8g9yybzcicjyrr0hax4fvxqfs5bas7sywlcmak7v2cj62") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc3 (c (n "fedimint-derive") (v "0.2.0-rc3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fz12swgs35fdbw8fqznq3fc47xpnqd8rhvk8hk9flqjj1aa9xy2") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc4 (c (n "fedimint-derive") (v "0.2.0-rc4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wmpki7dimkwky2q6zvbsaa85nk42dh83r5fixbhk6cjvfldsajx") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc5 (c (n "fedimint-derive") (v "0.2.0-rc5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cf2k050pmwhrb7a9j0a5haff8i5iqc909dg3jzy9vpa540pxnfq") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc7 (c (n "fedimint-derive") (v "0.2.0-rc7") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rr0fskhqy07pp8z9sqnr6z55l8yz81hjvg2h9kafix9qqi77qm0") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc8 (c (n "fedimint-derive") (v "0.2.0-rc8") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "167ycakg97qkc50r784k3r9icnqkj58cwr2nxv9hz0nlk19f0bb9") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0-rc9 (c (n "fedimint-derive") (v "0.2.0-rc9") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cz80jx62gw8s1z1y9vx5zs1znwq5s16g925x2hl3vw2brl12x1b") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.0 (c (n "fedimint-derive") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lyz20jx2m5d8l2vv0wiygyfshkbzgajgyxq0wwq93f8xb8hvy0q") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.1-rc1 (c (n "fedimint-derive") (v "0.2.1-rc1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04bv8d6jlc08bz2as1nwh1pja52gsva7wr10752f9cf35nrxslv5") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.1-rc2 (c (n "fedimint-derive") (v "0.2.1-rc2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12kj4xh8qrwrbsj5ssn8z5v3ghf6nvaa78bj5igjpgi8mirmrifc") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.1-rc3 (c (n "fedimint-derive") (v "0.2.1-rc3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c9lb70iqqil4mwpax0g3951amscvpm2a7m73myf5bx4pmn03ccq") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.1-rc4 (c (n "fedimint-derive") (v "0.2.1-rc4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15sn3yjp2f98hxw279ipf82bhga3qm3vigkbyigx2nqs5naw5mf4") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.1 (c (n "fedimint-derive") (v "0.2.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17zl41bhcd5s2ajqd1d032ri3syc5r3gc4dqz4shd9rfq7nil2gg") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc1 (c (n "fedimint-derive") (v "0.2.2-rc1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18axil08my6k5aqcy7gng1y3vma42sqrp23qa87rfawsxwrv63la") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc2 (c (n "fedimint-derive") (v "0.2.2-rc2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01s0640qw2bc19fmxpxgdcpjy3swd9lnzdxpv5cnwmfif98f91rf") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc3 (c (n "fedimint-derive") (v "0.2.2-rc3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rhwyxvarlb0hrbxwfxnd9akb33gvgq73lj4sr3ab8q9j4z6771y") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc5 (c (n "fedimint-derive") (v "0.2.2-rc5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s9z634ah1iszq3653pl1258ddkbibwy7j2w85a1sq3949vjvww7") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc6 (c (n "fedimint-derive") (v "0.2.2-rc6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jcr61j1xp1x50sfsyfdlfpc5myw8j2phc1qaky932qvzx5llvrl") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2-rc8 (c (n "fedimint-derive") (v "0.2.2-rc8") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q8a7xij74nqrbkqkpaxd6fasqf4d54kxwlv18vabgs3nkgb1a7j") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.2.2 (c (n "fedimint-derive") (v "0.2.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l1nbfydbirvk9k81k7nczhznhziimbkh3afsass7n5rbj3av43l") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.0-rc.0 (c (n "fedimint-derive") (v "0.3.0-rc.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pjvg0i2ar8iyxrbki9zdvkd2bhb0wd89gs6808pmcj3xbzq45ci") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.0-rc.1 (c (n "fedimint-derive") (v "0.3.0-rc.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q2ssaskv7sa70pmpl3zcif14s4ghxb8xn92wjmp97j9shbvy01x") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.0-rc.2 (c (n "fedimint-derive") (v "0.3.0-rc.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2b3lwlvzns6ax2dk3qwwfavga62d8rqpqhvjbf5gnjdcj462a7") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.0-rc.3 (c (n "fedimint-derive") (v "0.3.0-rc.3") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19bkx53s63fw0456bdxv9l1pbgmsb3vkpvgpsf9kcaflh2l5svp5") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.0 (c (n "fedimint-derive") (v "0.3.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02hl6hc21l16d94l9as2hfzym4f5vg4mii1vp6ldgwxly2hi4clc") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.1-rc.0 (c (n "fedimint-derive") (v "0.3.1-rc.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iafpxhsfbzk9fhl0fhpiq50mm1m3ny1saw9d1ykarg4ik1ygvyr") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.1-rc.1 (c (n "fedimint-derive") (v "0.3.1-rc.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11dpn1ccn132w4ynwp2nq0pk5ansvk43a3gjqcpm699bxpp9iz1h") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.1 (c (n "fedimint-derive") (v "0.3.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qmhyhbjbs6sskx1i14lba4hyzf5jync97q9p2z58am7a53w1rym") (f (quote (("diagnostics"))))))

(define-public crate-fedimint-derive-0.3.2-rc.0 (c (n "fedimint-derive") (v "0.3.2-rc.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n3q3iaw9f4p781581z7n08z0ralc0ppvgfj1icz3ymk1aymfc1m") (f (quote (("diagnostics"))))))

