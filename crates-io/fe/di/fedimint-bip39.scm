(define-module (crates-io fe di fedimint-bip39) #:use-module (crates-io))

(define-public crate-fedimint-bip39-0.2.0-rc2 (c (n "fedimint-bip39") (v "0.2.0-rc2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0inqm8abm4apm88gxmmz7xlqmi9645p43ra712h4gw1b4p01icq0")))

(define-public crate-fedimint-bip39-0.2.0-rc3 (c (n "fedimint-bip39") (v "0.2.0-rc3") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc3") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05rcas1znq9ihsk1cg47ggimsi7qihkvihcl5zbi7mh22bvqn43v")))

(define-public crate-fedimint-bip39-0.2.0-rc4 (c (n "fedimint-bip39") (v "0.2.0-rc4") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc4") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ib4wl4l8ldbnqc5h0f57j441z7immhbkcap08ckal1c3iivd3fg")))

(define-public crate-fedimint-bip39-0.2.0-rc5 (c (n "fedimint-bip39") (v "0.2.0-rc5") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "155s6218xqx6fcfbq7rn5nvw1a2f245y70hfq73rbssn65fk8qji")))

(define-public crate-fedimint-bip39-0.2.0-rc7 (c (n "fedimint-bip39") (v "0.2.0-rc7") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wgj5q46dqgysb3pshl2xz1cfjngb8m6ryfk870008b2azdfar5h")))

(define-public crate-fedimint-bip39-0.2.0-rc8 (c (n "fedimint-bip39") (v "0.2.0-rc8") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lhmhqag83yjh22izxvzmqvq9scnn67f2p2njfa85rpxflrbkdsa")))

(define-public crate-fedimint-bip39-0.2.0-rc9 (c (n "fedimint-bip39") (v "0.2.0-rc9") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0-rc9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00qamrfhadw9m5mpqbl0v8wbqanv7v2sdqizcmgws9slidah0yml")))

(define-public crate-fedimint-bip39-0.2.0 (c (n "fedimint-bip39") (v "0.2.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.0") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "185xrbqzv70nnrn3g8q1np5ghb89vgkfr2zm8b01dsb9ggmyrl28")))

(define-public crate-fedimint-bip39-0.2.1-rc1 (c (n "fedimint-bip39") (v "0.2.1-rc1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.1-rc1") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.1-rc1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00b2i8vsf5r23irl5yj2prik1m6lrciysq614dwgggcq40269gd1")))

(define-public crate-fedimint-bip39-0.2.1-rc2 (c (n "fedimint-bip39") (v "0.2.1-rc2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.1-rc2") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.1-rc2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mvgb9jjfrd49jm4bn42kylm8q5kqf9789w7dk0c7bc32m7lwmj2")))

(define-public crate-fedimint-bip39-0.2.1-rc3 (c (n "fedimint-bip39") (v "0.2.1-rc3") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.1-rc3") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.1-rc3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0yyxh3gk9fq9hg0l8ycqswrspsyshxkhk93hqm2ffkhpi5wa3n29")))

(define-public crate-fedimint-bip39-0.2.1-rc4 (c (n "fedimint-bip39") (v "0.2.1-rc4") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.1-rc4") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.1-rc4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z7g4vixr5l0f1sigli15c7rzxys5k38iqjsy1dggf1l83w8p5ap")))

(define-public crate-fedimint-bip39-0.2.1 (c (n "fedimint-bip39") (v "0.2.1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.1") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1byznrpr612l56cw7h3zyqg6ab9xhs2ir734awdv8cy9n2msmzv9")))

(define-public crate-fedimint-bip39-0.2.2-rc1 (c (n "fedimint-bip39") (v "0.2.2-rc1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gp9a3qlvzhfvdy9cfw1w61qgz8dm4c2s1d76375f0vrx4fij6x6")))

(define-public crate-fedimint-bip39-0.2.2-rc2 (c (n "fedimint-bip39") (v "0.2.2-rc2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0brakwc3ilpcjck3bz36f2p6p6zsfnp2basm6adjaf6kb3mvf6zv")))

(define-public crate-fedimint-bip39-0.2.2-rc3 (c (n "fedimint-bip39") (v "0.2.2-rc3") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc3") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04219hkpz6878ppdr5dr5xd27s6880j52dldlmmv4vzvpzaq8sm5")))

(define-public crate-fedimint-bip39-0.2.2-rc5 (c (n "fedimint-bip39") (v "0.2.2-rc5") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc5") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gdknb8lwgm3gw0xv921q8m48anc1zrg3n91aln295rvs0girhfk")))

(define-public crate-fedimint-bip39-0.2.2-rc6 (c (n "fedimint-bip39") (v "0.2.2-rc6") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc6") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04c9ysqwsn8cz3crfi76s77p0ij4289zbwis5fpbiy37yjic0h32")))

(define-public crate-fedimint-bip39-0.2.2-rc8 (c (n "fedimint-bip39") (v "0.2.2-rc8") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2-rc8") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2-rc8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02sfk3whb72dppgg159i7npqmhkhb7alhvmh9baqx9ssa5zfh7k5")))

(define-public crate-fedimint-bip39-0.2.2 (c (n "fedimint-bip39") (v "0.2.2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.2.2") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "044yv7ry67rj2sfz9ryp1333w9df4wnpz0azhynnyzgxr2h3d78x")))

(define-public crate-fedimint-bip39-0.3.0-rc.0 (c (n "fedimint-bip39") (v "0.3.0-rc.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.3.0-rc.0") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.3.0-rc.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b7marf4nl64lm1ga6fgvxq1wkfrgnkmw0d4s47hgpdqygjwcx87")))

(define-public crate-fedimint-bip39-0.3.0-rc.1 (c (n "fedimint-bip39") (v "0.3.0-rc.1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "fedimint-core") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bwwp1rgrs2bvj0035qjjlg173nk66fqz94zw73bg7jpm0a37xzd")))

(define-public crate-fedimint-bip39-0.3.0-rc.2 (c (n "fedimint-bip39") (v "0.3.0-rc.2") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.0-rc.2") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.0-rc.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yq7pr8r1dgzsmhys5lvf7ldj8cb77xibzb80s1xj61l617b5b6m")))

(define-public crate-fedimint-bip39-0.3.0-rc.3 (c (n "fedimint-bip39") (v "0.3.0-rc.3") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.0-rc.3") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.0-rc.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nrj9xd6i03pbyfmqbnqa06s38l0bxjq2ri5lp8hmcnvdmgmi3v2")))

(define-public crate-fedimint-bip39-0.3.0 (c (n "fedimint-bip39") (v "0.3.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.0") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10pqra6zm3d1fq04350cii22cdz7zgrj570a8hfl054s83vvcl65")))

(define-public crate-fedimint-bip39-0.3.1-rc.0 (c (n "fedimint-bip39") (v "0.3.1-rc.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.1-rc.0") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.1-rc.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09w2czqprkba3dj0jnh76wazwmvnr0j89wjjhfkg202ff5nfi997")))

(define-public crate-fedimint-bip39-0.3.1-rc.1 (c (n "fedimint-bip39") (v "0.3.1-rc.1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.1-rc.1") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.1-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09x0a0ljmahlmcqif2cfsmsfnxalmsr79dv4lkmqa98l8kd262x4")))

(define-public crate-fedimint-bip39-0.3.1 (c (n "fedimint-bip39") (v "0.3.1") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.1") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1jy4m5dmhc1p49avk28nljbccdmwa9cld69ng2y6sk17wp20vh2b")))

(define-public crate-fedimint-bip39-0.3.2-rc.0 (c (n "fedimint-bip39") (v "0.3.2-rc.0") (d (list (d (n "bip39") (r "^2.0.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "fedimint-client") (r "=0.3.2-rc.0") (d #t) (k 0)) (d (n "fedimint-core") (r "=0.3.2-rc.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1i9gfaasc9gv03zqjdlqcfn0qp1r7zxybgzcg83048k0w4bwpksb")))

