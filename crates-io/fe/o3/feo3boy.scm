(define-module (crates-io fe o3 feo3boy) #:use-module (crates-io))

(define-public crate-feo3boy-0.1.0 (c (n "feo3boy") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "feo3boy-executor-generator") (r "^0.1.0") (d #t) (k 0)) (d (n "feo3boy-memdev-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "feo3boy-opcodes") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dnrqwm5q151h5srmaqm190g12k4rx8c77vwa61vbn5plc9vzb66") (f (quote (("test-roms"))))))

