(define-module (crates-io fe o3 feo3boy-microcode-generator) #:use-module (crates-io))

(define-public crate-feo3boy-microcode-generator-0.1.0 (c (n "feo3boy-microcode-generator") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00gmm9k1r1jnxz17zn8zgkhm02sjsndypbyzjmbq8zsv3l1jf89x")))

