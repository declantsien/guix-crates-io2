(define-module (crates-io fe o3 feo3boy-memdev-derive) #:use-module (crates-io))

(define-public crate-feo3boy-memdev-derive-0.1.0 (c (n "feo3boy-memdev-derive") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qd99y5kgwq95in62zvwyz5kxv35rn6zhrxrlfjkmhcaxsxh7yk5")))

