(define-module (crates-io fe o3 feo3boy-executor-generator) #:use-module (crates-io))

(define-public crate-feo3boy-executor-generator-0.1.0 (c (n "feo3boy-executor-generator") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1") (d #t) (k 0)) (d (n "feo3boy-opcodes") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "080gqfivngm23bb0xlqmlz24di4ir1yimx4kx6y2kqq81w4cjld9")))

