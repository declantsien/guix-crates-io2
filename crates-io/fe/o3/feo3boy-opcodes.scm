(define-module (crates-io fe o3 feo3boy-opcodes) #:use-module (crates-io))

(define-public crate-feo3boy-opcodes-0.1.0 (c (n "feo3boy-opcodes") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "feo3boy-microcode-generator") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1kfbc7ci8w8sxv3mfpdzaabgw59b84zn6bfb69kj9nsy74i0q6v6")))

