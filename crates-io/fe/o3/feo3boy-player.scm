(define-module (crates-io fe o3 feo3boy-player) #:use-module (crates-io))

(define-public crate-feo3boy-player-0.1.0 (c (n "feo3boy-player") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cpal") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "feo3boy") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pixels") (r "^0.12") (d #t) (k 0)) (d (n "ringbuf") (r "^0.3") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)) (d (n "winit_input_helper") (r "^0.14") (d #t) (k 0)))) (h "1ml6pavi482jifp1jxflsm9hsw7xq10f23z49rzv7b045vipyv6d")))

