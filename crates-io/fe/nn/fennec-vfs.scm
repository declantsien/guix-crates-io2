(define-module (crates-io fe nn fennec-vfs) #:use-module (crates-io))

(define-public crate-fennec-vfs-0.1.6 (c (n "fennec-vfs") (v "0.1.6") (h "1m71fijsy3as8mgk5g8f8609hx5w8p7xki88fp9bisc1pjr13x9y")))

(define-public crate-fennec-vfs-0.1.8 (c (n "fennec-vfs") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fennec-common") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "proptest-state-machine") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09ys9cn865g5xwbdfrwg1hc20phxbz9wqgnsx5hw4i0r06rrzkvg")))

