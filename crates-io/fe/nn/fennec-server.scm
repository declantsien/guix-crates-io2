(define-module (crates-io fe nn fennec-server) #:use-module (crates-io))

(define-public crate-fennec-server-0.1.6 (c (n "fennec-server") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.2") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "199lfxs2sbv66s897d9k2ykfda65dga5ykh4hrfxnpf1z40vmklb")))

(define-public crate-fennec-server-0.1.7 (c (n "fennec-server") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fennec-common") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.4") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1x3s9hafqsk90nnhgg17gbng3yfwaq0silwv8ylf586ljphilpid")))

(define-public crate-fennec-server-0.1.8 (c (n "fennec-server") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fennec-common") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.4") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zrdk2lcs7h812vhj846fncxv3dzps03kcz40478rrrqh2gsa8pw")))

