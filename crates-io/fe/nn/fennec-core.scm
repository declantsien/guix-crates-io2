(define-module (crates-io fe nn fennec-core) #:use-module (crates-io))

(define-public crate-fennec-core-0.1.5 (c (n "fennec-core") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ecow") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11ir1yhml5kl1c4gvrv4w3crd62ab1zkv8skpz11arddz9rf7w1s")))

(define-public crate-fennec-core-0.1.6 (c (n "fennec-core") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "ecow") (r "^0.1.1") (d #t) (k 0)) (d (n "fennec-common") (r "^0.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rzcviwcd13zsza926xsa8rp31n9axgybnlxwr2q68z1xparvc88")))

(define-public crate-fennec-core-0.1.7 (c (n "fennec-core") (v "0.1.7") (d (list (d (n "fennec-common") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1mn1gngqby77arrf92c35hgfxg0ngxvic8h477xcv3cdym0mwj4l")))

(define-public crate-fennec-core-0.1.8 (c (n "fennec-core") (v "0.1.8") (d (list (d (n "fennec-common") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "01fd1l3xdqdfmi0icj1bkkakvb1vdi4n5j7ns934vncd2ljfrqwj")))

