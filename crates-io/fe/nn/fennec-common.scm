(define-module (crates-io fe nn fennec-common) #:use-module (crates-io))

(define-public crate-fennec-common-0.1.6 (c (n "fennec-common") (v "0.1.6") (h "0d481rmpxdsx2v4w67r6rsrmp5lljjawpq0a1fkiygyz3la2rgwg")))

(define-public crate-fennec-common-0.1.7 (c (n "fennec-common") (v "0.1.7") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zml5p64hkvwzq1ccc9pvw7scm8x4ybmni7aacs3cjdqfza7h4cg")))

(define-public crate-fennec-common-0.1.8 (c (n "fennec-common") (v "0.1.8") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1zwvx0m32yv95rf0j5zp54g02s0yj05mgijyl20ap9mm0jyglcmd")))

