(define-module (crates-io fe nn fenny) #:use-module (crates-io))

(define-public crate-fenny-0.1.0 (c (n "fenny") (v "0.1.0") (h "060ik1gskpwafcclp7fzhfcrsqpq0smnhdp3ljvbwja4dafrfmfa")))

(define-public crate-fenny-0.1.1 (c (n "fenny") (v "0.1.1") (h "112x2ib4kvmcc6i67cvvcl4k5vzk7v5hsw9q3qzlmy2nq85mlayw")))

(define-public crate-fenny-0.2.0 (c (n "fenny") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.1") (d #t) (k 2)))) (h "044jyjjcy1s6bn9ds7dvq8ls7dlffd0n69v5l2d4q3mllmggwx9w")))

