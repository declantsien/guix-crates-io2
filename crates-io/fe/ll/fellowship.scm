(define-module (crates-io fe ll fellowship) #:use-module (crates-io))

(define-public crate-fellowship-0.1.0 (c (n "fellowship") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "sailfish") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.18") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.31") (d #t) (k 0)))) (h "0qqi4v2gzwca61v7zr9x3h703smxrcbiihjjlq1z3yvpw4nw57zm")))

