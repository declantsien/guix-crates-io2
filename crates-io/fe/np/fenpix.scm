(define-module (crates-io fe np fenpix) #:use-module (crates-io))

(define-public crate-fenpix-0.1.0 (c (n "fenpix") (v "0.1.0") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0yjx4gwx73jj20irslimsrmmhr68gxzcrgzfkc0b74is11m9dxyc")))

(define-public crate-fenpix-0.1.1 (c (n "fenpix") (v "0.1.1") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1nv59y1n00ygl6rcddpxp19fyib5mhj44xpb5gv2v6k3zx6im3xn")))

(define-public crate-fenpix-0.1.2 (c (n "fenpix") (v "0.1.2") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "03alrh2zvbv5rffzwfyc75ggabkzz0hivcj1mxfxhyh0y9nqbl45")))

(define-public crate-fenpix-0.1.3 (c (n "fenpix") (v "0.1.3") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0mgq03cnjk307rpqx1vhb1pidf7kmc6dch5kw6hyb6abfiadsjhv")))

(define-public crate-fenpix-0.1.4 (c (n "fenpix") (v "0.1.4") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1g0vblb1mzmi98x2020ikxmnw5msjxc15z2bldagwyrbxmkfqbia")))

(define-public crate-fenpix-0.1.5 (c (n "fenpix") (v "0.1.5") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0h6kgxfmfhnyaqiz3fl8k2lzlw81i4g6gsim2q4fc575rxri2vdg")))

(define-public crate-fenpix-0.1.6 (c (n "fenpix") (v "0.1.6") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1hzjh8f9cappkfwl920xm9qssnak0z7rdx1w0dkn33zh88nkg73j")))

(define-public crate-fenpix-0.1.7 (c (n "fenpix") (v "0.1.7") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1v005s5hil5k9m7g16387alsrwnr1v50hc85np2k4qx8dwqax99g")))

(define-public crate-fenpix-0.1.8 (c (n "fenpix") (v "0.1.8") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0r3643p4i7awmly5m6czh9nys0p3h50yi4x5svnqs0x431ix19yw")))

(define-public crate-fenpix-0.1.9 (c (n "fenpix") (v "0.1.9") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1wf6xkmjdkq7mr4myfqm6953601l9nfb84axl18jikw41j349xcd")))

(define-public crate-fenpix-0.2.0 (c (n "fenpix") (v "0.2.0") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1cxhhwipma0b34mhzy8mxf89a7xha743rbnyarcslxm5dh0sak7r")))

(define-public crate-fenpix-0.2.1 (c (n "fenpix") (v "0.2.1") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1h5y6di1haw9r5md4brfh4a0sx97li7ry250bbkls2xf34v5rsh5")))

(define-public crate-fenpix-0.2.2 (c (n "fenpix") (v "0.2.2") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "0zbrirnaw5drmq77lqblm0psbp3kxfc8crl5ffmw1cm5qapaff0n")))

(define-public crate-fenpix-0.2.3 (c (n "fenpix") (v "0.2.3") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "01ry2qmqrh9di83ns0vw02grhg7dgyhj13bb1rxpj8pmpcvl33ck")))

(define-public crate-fenpix-0.2.4 (c (n "fenpix") (v "0.2.4") (d (list (d (n "image") (r "^0.24.9") (d #t) (k 0)))) (h "1v7a2rcnzgzbwzg0w3bf5a6jfc1ry15x487wd0616ksy69wj1gb2")))

