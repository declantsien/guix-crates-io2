(define-module (crates-io fe lt felt) #:use-module (crates-io))

(define-public crate-felt-0.1.0 (c (n "felt") (v "0.1.0") (d (list (d (n "dirs") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1sfrzks8zdl7kkv6ldj9svv9a38jf9xq0l2yjwagh1505alsvnkd")))

(define-public crate-felt-0.1.1 (c (n "felt") (v "0.1.1") (d (list (d (n "dirs") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "188j8c712n4w5jxv2bqmrxxbkjzxmx2mrpa0kgff2qsr99pv5nzv")))

(define-public crate-felt-0.2.0 (c (n "felt") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0jichl837pynlzg7w2xcwmywnz27bgzdjq5m7jfislk3x4zjgdlz")))

