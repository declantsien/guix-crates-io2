(define-module (crates-io fe re fere-resources) #:use-module (crates-io))

(define-public crate-fere-resources-0.0.0 (c (n "fere-resources") (v "0.0.0") (d (list (d (n "fere-common") (r "^0.0.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0n5np8gm0xsnb3w5s99dqqpmp4n74xclqscbd1r4rcgbanwcf1s8")))

