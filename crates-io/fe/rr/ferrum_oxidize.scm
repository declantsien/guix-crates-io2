(define-module (crates-io fe rr ferrum_oxidize) #:use-module (crates-io))

(define-public crate-ferrum_oxidize-0.1.0 (c (n "ferrum_oxidize") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qyvxwcckjcbhzlldj3y3lbx9p08ghffknh02ql5pvc8zih9mrzc")))

(define-public crate-ferrum_oxidize-0.1.1 (c (n "ferrum_oxidize") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05831rxd65igf4fzmd1f08089nfmkkm8yzxjbwfdczcj4hsqfisg")))

(define-public crate-ferrum_oxidize-0.1.2 (c (n "ferrum_oxidize") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hfiz02wvxczd8yakfk0n6kslmlxixzqsjf0wywkz1mh48krq9zv")))

