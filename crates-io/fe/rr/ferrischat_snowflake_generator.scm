(define-module (crates-io fe rr ferrischat_snowflake_generator) #:use-module (crates-io))

(define-public crate-ferrischat_snowflake_generator-0.1.0 (c (n "ferrischat_snowflake_generator") (v "0.1.0") (h "07kmks2ha95q1bmx54ngdi7zpnhfc44kwdx8bh2zp7xx1mgkqbda") (f (quote (("time-safety-checks") ("default" "time-safety-checks"))))))

(define-public crate-ferrischat_snowflake_generator-1.0.0 (c (n "ferrischat_snowflake_generator") (v "1.0.0") (h "13rwr1clr4mzq0bdnczrisixckaafvmaxqyw2jwb7ci95jg9rp0p") (f (quote (("time-safety-checks") ("default" "time-safety-checks")))) (y #t)))

(define-public crate-ferrischat_snowflake_generator-1.0.1 (c (n "ferrischat_snowflake_generator") (v "1.0.1") (h "1n71vsxygs2qqm3k6kk6rix4r63if9vzc6sgly9yfrxpcly256is") (f (quote (("time-safety-checks") ("default" "time-safety-checks"))))))

(define-public crate-ferrischat_snowflake_generator-2.0.0 (c (n "ferrischat_snowflake_generator") (v "2.0.0") (h "0pb802y6ajq79nl736qbcpfrc0fkbz8g7098ahihxawixbqcfcnd") (f (quote (("time-safety-checks") ("default" "time-safety-checks"))))))

