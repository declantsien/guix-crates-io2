(define-module (crates-io fe rr ferrischat_common) #:use-module (crates-io))

(define-public crate-ferrischat_common-0.1.0 (c (n "ferrischat_common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y52jsy7fkiq027q23xjwdrxw8hswn8b9680aq354dn16awrc8kx")))

(define-public crate-ferrischat_common-0.1.1 (c (n "ferrischat_common") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qkbadavrjaal9zcga0qqq5wi4l2snf084ylla8rp2aasfr7c3mf")))

(define-public crate-ferrischat_common-0.1.2 (c (n "ferrischat_common") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xv8wryfq4ydqkjrygdmq4hplfyaj9y0sfkfk35vwazfabpim5ks")))

(define-public crate-ferrischat_common-0.1.3 (c (n "ferrischat_common") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14vcp571ih4790r071lkqm3c6z3k8ymidr94kkkgx1v911ic49hb")))

(define-public crate-ferrischat_common-0.1.4 (c (n "ferrischat_common") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vs5p9xvjvjbjdzgal3q1yp62mr2a8z1374msyvgvfi03b79ydsm")))

(define-public crate-ferrischat_common-0.1.5 (c (n "ferrischat_common") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08bmy9z8gvi5d6xw0v4fd6znbw4q20dr0mlzrdxacmls9y0f2aj6")))

(define-public crate-ferrischat_common-0.1.6 (c (n "ferrischat_common") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07cwfmxpzfic05nx5ldsr1a2c85n5rjvs3qvri4gknw0zpzcs9l7")))

(define-public crate-ferrischat_common-0.1.7 (c (n "ferrischat_common") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0bxnxdwxn0rvwnfrhyhq1nm4b278h0g9mq96rbckwn776z6hn25g")))

(define-public crate-ferrischat_common-0.2.0 (c (n "ferrischat_common") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0nndvdc284b6ix81lmq6apr37c9ly6rxdk948m2dn311adk5qax6")))

(define-public crate-ferrischat_common-0.3.0 (c (n "ferrischat_common") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tribool") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1cyn7ncavaaq2dsiikxdq97rwa9qi1fs7h1gjf2chrnzsjddwqdx")))

