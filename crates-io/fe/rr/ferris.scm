(define-module (crates-io fe rr ferris) #:use-module (crates-io))

(define-public crate-ferris-0.1.0 (c (n "ferris") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0a6ndrlk77ha98c50ssbvv653lr289jrkqy975q8vjvcmyp5g2fr")))

(define-public crate-ferris-0.1.1 (c (n "ferris") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y0xbw0pwzaq6xw2pzz8zarm7mzhm0zh9w3s94znkhvfwxfhcfjd")))

(define-public crate-ferris-0.2.0 (c (n "ferris") (v "0.2.0") (h "114z2c2a9h0xrm2cbw9dp13a0d68byldkbalhadwjl5g187s1mgi")))

