(define-module (crates-io fe rr ferrotype) #:use-module (crates-io))

(define-public crate-ferrotype-0.1.0 (c (n "ferrotype") (v "0.1.0") (d (list (d (n "insta") (r "^1") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (o #t) (d #t) (k 0)))) (h "1d4f50idar5dla9zh1if2bgh8waafdvrib80zps6xkm501gvmwkv") (f (quote (("dot_snapshots") ("default" "dot_snapshots" "tokenstream")))) (s 2) (e (quote (("tokenstream" "dep:proc-macro2" "dep:syn" "dep:prettyplease"))))))

