(define-module (crates-io fe rr ferris-bot) #:use-module (crates-io))

(define-public crate-ferris-bot-0.1.0 (c (n "ferris-bot") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0lv0hbaf8a6ckrfgwhx1121f4l7qbc77jjj3na59vd845nvi8a2q")))

(define-public crate-ferris-bot-0.1.2 (c (n "ferris-bot") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1yxfv93ipwf55xmp37j919n5pk91cw19wa19g8s6v0ydmxysia54")))

(define-public crate-ferris-bot-0.1.3 (c (n "ferris-bot") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "13qh6p81d1h56q1v6hypfpvrs2j4iz5ihyn3g389xgj6w6qgrrij")))

(define-public crate-ferris-bot-0.2.4 (c (n "ferris-bot") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0ywx37vpy3gh1q1m2gr971g6bphk9vplj6h789ijlpvmzw354h3h")))

(define-public crate-ferris-bot-0.2.5 (c (n "ferris-bot") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1nxnqzmpqv21z76jwq424w6vjqgf7s88kdmxj3qq50n56vn0q5nd")))

(define-public crate-ferris-bot-0.2.6 (c (n "ferris-bot") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "rustls_backend" "model"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1rqazhdxz11g83g4mkj91sm514245y68ab74h2xc6384ggphaji8")))

