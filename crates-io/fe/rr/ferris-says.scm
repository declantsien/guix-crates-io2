(define-module (crates-io fe rr ferris-says) #:use-module (crates-io))

(define-public crate-ferris-says-0.1.0 (c (n "ferris-says") (v "0.1.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "0sg21ff760p956vkafd8hkpgx4mvwhflfc2izrdp4wd3fz9ds7ab")))

(define-public crate-ferris-says-0.1.1 (c (n "ferris-says") (v "0.1.1") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "1z9j1fsyhf343g6anl29lvyrp4aqzbq6k9q0q8s7ihyxnhwi78h7") (f (quote (("clippy"))))))

(define-public crate-ferris-says-0.1.2 (c (n "ferris-says") (v "0.1.2") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "17b189cy9rdn21xccw8ij14z4rnfjcc1klmqgrpi9navlii9ly8n") (f (quote (("clippy"))))))

(define-public crate-ferris-says-0.2.0 (c (n "ferris-says") (v "0.2.0") (d (list (d (n "clap") (r "^2.25") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "11rnxajbxr7wgmhv51z27f9bz1w61gcsn66h4z0365cbk8pghd3z") (f (quote (("clippy"))))))

(define-public crate-ferris-says-0.2.1 (c (n "ferris-says") (v "0.2.1") (d (list (d (n "smallvec") (r "^0.4") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1amp0maqdrg7xzvl60b0mzra4278v5gz5ldjyqqc4vk0v4nyq5cm") (f (quote (("clippy"))))))

(define-public crate-ferris-says-0.3.1 (c (n "ferris-says") (v "0.3.1") (d (list (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0va9nzrv7mxs41r7kj1hd1fj145lg6khinadjh0bmdww646vw7l4") (f (quote (("clippy"))))))

