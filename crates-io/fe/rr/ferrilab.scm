(define-module (crates-io fe rr ferrilab) #:use-module (crates-io))

(define-public crate-ferrilab-0.1.0 (c (n "ferrilab") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1si5l1m8g4nkwknhqrjwcs3kqmy5hg85rn2xvj9s46gn1yddshh3")))

(define-public crate-ferrilab-0.1.1 (c (n "ferrilab") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("auto-initialize"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "01fzdq4ifng10firx7r4qzwjkly39c6rbdhlkc77m8l08n6pk01d")))

