(define-module (crates-io fe rr ferrishook) #:use-module (crates-io))

(define-public crate-ferrishook-1.0.0 (c (n "ferrishook") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ywikq5rajh885znzcz0qhzcd82ryg85akasml2kj0qf0lni50dp")))

(define-public crate-ferrishook-2.0.0 (c (n "ferrishook") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12ad47jwnc13vkd51mxvkpjbx2g6jhjh28w6qrjr4rg275n5gqnk")))

(define-public crate-ferrishook-2.0.1 (c (n "ferrishook") (v "2.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b54i67hc480fk8m4ymdgchi5i5xfqv8njlhcphn5zlla0hhsdic")))

