(define-module (crates-io fe rr ferrum-router) #:use-module (crates-io))

(define-public crate-ferrum-router-0.2.0 (c (n "ferrum-router") (v "0.2.0") (d (list (d (n "ferrum") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "004dr2x4jj6kqc35ja9bz254waq55sarb39rjwmwxmg3rg8nb5rf") (f (quote (("nightly"))))))

(define-public crate-ferrum-router-0.2.1 (c (n "ferrum-router") (v "0.2.1") (d (list (d (n "ferrum") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "0qdqmbwq47klhymhvxv013d373dwpv94kjb5cmvv4zyf7ms4v0h1") (f (quote (("nightly"))))))

