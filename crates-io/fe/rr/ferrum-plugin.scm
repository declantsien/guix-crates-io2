(define-module (crates-io fe rr ferrum-plugin) #:use-module (crates-io))

(define-public crate-ferrum-plugin-0.3.0 (c (n "ferrum-plugin") (v "0.3.0") (d (list (d (n "unsafe-any") (r "^0.4") (d #t) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1q7cl2hq93b89hy83cz58x73xf3qlj78blw855bhiz733r5xir0v")))

