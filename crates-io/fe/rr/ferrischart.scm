(define-module (crates-io fe rr ferrischart) #:use-module (crates-io))

(define-public crate-ferrischart-0.1.0 (c (n "ferrischart") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "19d8fa66vbscwxvc08q6y53zp71bzzw7gmg74fw1qnaas2p4yaki")))

