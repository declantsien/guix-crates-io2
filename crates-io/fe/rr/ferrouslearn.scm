(define-module (crates-io fe rr ferrouslearn) #:use-module (crates-io))

(define-public crate-FerrousLearn-0.1.0 (c (n "FerrousLearn") (v "0.1.0") (h "0j8r7w5ssa73llvgjldr4l1z72j35ywr0d2ndp5f5mcanf1bw9sm")))

(define-public crate-FerrousLearn-0.1.1 (c (n "FerrousLearn") (v "0.1.1") (h "1w8h26j0fi1nrscwy0h5nryrnvlrgwcil9s7b7zn9dqm6d33z74w")))

(define-public crate-FerrousLearn-0.1.3 (c (n "FerrousLearn") (v "0.1.3") (h "1ryrqdrzs5qhnwyg59490wwrp40ffwm9z1w5h4liyz0pdprbilbd")))

(define-public crate-FerrousLearn-0.1.4 (c (n "FerrousLearn") (v "0.1.4") (h "18marl697s59gwd0c9zf93qzx5gqfislp62kfhy2ra8k8f1wxsch")))

(define-public crate-FerrousLearn-0.1.5 (c (n "FerrousLearn") (v "0.1.5") (h "14skqmrv21pbgnwbg09hq9vpf6jy7sknwh511a1di2z3g9caradn")))

(define-public crate-FerrousLearn-0.1.6 (c (n "FerrousLearn") (v "0.1.6") (h "0jg554dm15bqb8prrimph29igbz89v7achdzpzw1xxzhhsyq9j0m")))

