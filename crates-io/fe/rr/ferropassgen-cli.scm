(define-module (crates-io fe rr ferropassgen-cli) #:use-module (crates-io))

(define-public crate-ferropassgen-cli-1.0.2 (c (n "ferropassgen-cli") (v "1.0.2") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "diceware_wordlists") (r "^1.2") (d #t) (k 0)) (d (n "ferropassgen") (r "^1") (d #t) (k 0)))) (h "04g62hrk9qlncj3m4jwf2ddc9qd233y2j0knkbxljf52ns96j21y") (r "1.74.1")))

