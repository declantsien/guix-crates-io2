(define-module (crates-io fe rr ferric) #:use-module (crates-io))

(define-public crate-ferric-0.1.0 (c (n "ferric") (v "0.1.0") (d (list (d (n "ferric-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "06mk7fchi7a7p9q7y0sad4hm9h5sq10j6i5hak5c9h2bwd8zl98b")))

(define-public crate-ferric-0.1.2 (c (n "ferric") (v "0.1.2") (d (list (d (n "ferric-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pkm11skdmm68nb903v886gdi2pgiazkjc2v8v58qhk0kimlw826")))

