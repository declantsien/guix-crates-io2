(define-module (crates-io fe rr ferric_parser) #:use-module (crates-io))

(define-public crate-ferric_parser-0.1.0 (c (n "ferric_parser") (v "0.1.0") (d (list (d (n "ferric_lexer") (r "^0.1.0") (d #t) (k 0)))) (h "1zcgkn43idghnva9lzrpa7z8jwkc8l7r4xglmyx3v2fydrs8dm0p")))

(define-public crate-ferric_parser-0.2.0 (c (n "ferric_parser") (v "0.2.0") (d (list (d (n "ferric_lexer") (r "^0.1.0") (d #t) (k 0)))) (h "03baq5vwv5hc30hdkz04wman03jm6x2bvw54p5z82h6wx1rbpy11") (f (quote (("default" "asm") ("bash") ("asm"))))))

