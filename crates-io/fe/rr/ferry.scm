(define-module (crates-io fe rr ferry) #:use-module (crates-io))

(define-public crate-ferry-0.0.1 (c (n "ferry") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "116kgq7m6vsdlfbc9s9qsh7swm68c5a7j4a0mbn63aaj5d154589")))

(define-public crate-ferry-0.0.2 (c (n "ferry") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "oauth2") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "0dlbqvsyy2vms8bqp8ab5pk2r5ap51ns60kqd1fy2x4z32mb8ii6")))

