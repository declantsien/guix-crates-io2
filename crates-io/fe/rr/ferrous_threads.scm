(define-module (crates-io fe rr ferrous_threads) #:use-module (crates-io))

(define-public crate-ferrous_threads-0.0.1 (c (n "ferrous_threads") (v "0.0.1") (d (list (d (n "bit-vec") (r "^0.4.1") (d #t) (k 2)))) (h "0n3rhd2gnh4k6p7fyn24ypalvrg30hxqak8pnvapqvbw2zlmphpk")))

(define-public crate-ferrous_threads-0.0.2 (c (n "ferrous_threads") (v "0.0.2") (d (list (d (n "bit-vec") (r "^0.4.1") (d #t) (k 2)))) (h "0ybaghqsvcc0cmv4flh7dslfsb34174kd3x1z6kz87klv21wnm4l")))

(define-public crate-ferrous_threads-0.1.0 (c (n "ferrous_threads") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4.1") (d #t) (k 2)) (d (n "canal") (r "*") (f (quote ("nightly"))) (d #t) (k 0)))) (h "0ncwpchjvqckb67nc0fry93fccinfilrxvzwwzqr2zaapzf4kvwp")))

(define-public crate-ferrous_threads-0.1.1 (c (n "ferrous_threads") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4.1") (d #t) (k 2)) (d (n "canal") (r ">= 0.1.2") (f (quote ("nightly"))) (d #t) (k 0)))) (h "16lig8zidy6ph2cwla7pwpjs6kxb48z5zl425xpvk61z3y50nybb")))

