(define-module (crates-io fe rr ferro) #:use-module (crates-io))

(define-public crate-ferro-0.5.0 (c (n "ferro") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shunting") (r "^0.1.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "termbg") (r "^0.4.3") (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1nn8z3qrp673i8rn8bbk4zias819jrvsmndy8y42927zaw6lsg1a")))

(define-public crate-ferro-0.6.0 (c (n "ferro") (v "0.6.0") (d (list (d (n "bounded-vec-deque") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "shunting") (r "^0.1.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "termbg") (r "^0.4.3") (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0x25cv683ss6vbnrxh9l09y6a4sbmkf7qbf1rwmw1hm7hgs8ilyn")))

