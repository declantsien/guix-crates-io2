(define-module (crates-io fe rr ferret) #:use-module (crates-io))

(define-public crate-ferret-1.0.0 (c (n "ferret") (v "1.0.0") (h "18x6h6gbg8rp1nlagkb9cn2wim96z29jn63qmhl5df5y2mw34y1x")))

(define-public crate-ferret-1.1.0 (c (n "ferret") (v "1.1.0") (h "0ni1887xqkksy5wsblhmqhfjd8vgfr9fvfjjpiwy38zlcqqmngvv")))

(define-public crate-ferret-1.1.1 (c (n "ferret") (v "1.1.1") (h "1s71n4hf5sfykni66n1gaqd9aczmvsqyxxbx0kp4lqbxrfps317a")))

