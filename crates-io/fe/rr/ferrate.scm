(define-module (crates-io fe rr ferrate) #:use-module (crates-io))

(define-public crate-ferrate-0.0.3 (c (n "ferrate") (v "0.0.3") (h "1nclgx5l881gr4sxvd3z5vjm5navppwhcxii7nzw6d8vpwkzb860") (y #t)))

(define-public crate-ferrate-0.0.4 (c (n "ferrate") (v "0.0.4") (h "1bn3hv5wc9zv1wgl6d9fy4pssag472xlzx2fm6iak1myhj4pc0y9")))

