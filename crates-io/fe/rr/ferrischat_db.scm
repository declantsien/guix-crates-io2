(define-module (crates-io fe rr ferrischat_db) #:use-module (crates-io))

(define-public crate-ferrischat_db-0.1.0 (c (n "ferrischat_db") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.4") (f (quote ("postgres" "macros" "offline" "runtime-actix-rustls" "bigdecimal"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0bisxaj9s8krylb2c3pfgd1hyfmi78q2vjm9v91m23j5wq8k9y7k")))

