(define-module (crates-io fe rr ferris-gc-proc-macro) #:use-module (crates-io))

(define-public crate-ferris-gc-proc-macro-0.1.1 (c (n "ferris-gc-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0vg0jqsyg09wrsxhia0ky5q3hdxbn0dmcqrfrb4jksk6xr91apam")))

(define-public crate-ferris-gc-proc-macro-0.1.2 (c (n "ferris-gc-proc-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "04y7b9z5hiyy8nng97g1khhsm2azqcvg3n3pz4j457xl45wwp0gd")))

