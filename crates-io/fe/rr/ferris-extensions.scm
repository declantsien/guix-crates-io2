(define-module (crates-io fe rr ferris-extensions) #:use-module (crates-io))

(define-public crate-ferris-extensions-0.1.0 (c (n "ferris-extensions") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1niksh4c4zdqk9xvfw5i0ga8adh3cds696qfs2mf2i8c5dy95ird")))

