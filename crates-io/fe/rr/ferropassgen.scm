(define-module (crates-io fe rr ferropassgen) #:use-module (crates-io))

(define-public crate-ferropassgen-1.0.2 (c (n "ferropassgen") (v "1.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w56iw72qa2lnmhxird6wcbjf91h10kksszgc85flcqdkq3il83z") (r "1.60.0")))

