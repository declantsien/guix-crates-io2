(define-module (crates-io fe rr ferris-finder) #:use-module (crates-io))

(define-public crate-ferris-finder-0.1.0 (c (n "ferris-finder") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "04qz5vf5wmswgcvlbmm9ficg0li7gchf20cw80z9jysy3gdhca9d") (y #t)))

(define-public crate-ferris-finder-0.1.1 (c (n "ferris-finder") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "15q8gbspfhigaq0w1qb7n80in65n2j7ri2ln3ab2s3lhqc6y8fp1")))

(define-public crate-ferris-finder-0.2.0 (c (n "ferris-finder") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1lyh9lhk96k4qy78wzdyyrsdd3251wxzs1rfli87h3gwxql67y9n")))

(define-public crate-ferris-finder-0.2.1 (c (n "ferris-finder") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "16zbgwk37n6l30vjl5fzlrckal5yn64fz1s7frr5ymybqw5xx6cy")))

