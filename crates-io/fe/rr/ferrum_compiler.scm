(define-module (crates-io fe rr ferrum_compiler) #:use-module (crates-io))

(define-public crate-ferrum_compiler-0.0.1 (c (n "ferrum_compiler") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0bvb9ymfkk47qsa808h0byn19iciswajw4nynky2nhig1krrwbyq")))

(define-public crate-ferrum_compiler-0.0.2 (c (n "ferrum_compiler") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "039kpqjwxvzskc9flmk7sac65rj88bqzm5vwm0xj6wn1mvizhhwk") (f (quote (("build-binary" "env_logger"))))))

(define-public crate-ferrum_compiler-0.0.3 (c (n "ferrum_compiler") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0z2gj8cy8cdhzyikgf2gihq8p2m1cqnxz96d2vawxghxy18abqia") (f (quote (("build-binary" "env_logger"))))))

(define-public crate-ferrum_compiler-0.0.4 (c (n "ferrum_compiler") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "16wfhfds45wq39q7cfg2mfpy165cqb9wrvfpkp9k1i16fl4bk8gn") (f (quote (("build-binary" "env_logger"))))))

