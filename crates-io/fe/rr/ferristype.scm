(define-module (crates-io fe rr ferristype) #:use-module (crates-io))

(define-public crate-ferristype-0.1.0 (c (n "ferristype") (v "0.1.0") (h "0aax4dsfvacd68d7xla92wvygm73xfp3mgcw3qdh1wk4af6m7b6g") (y #t)))

(define-public crate-ferristype-1.0.0 (c (n "ferristype") (v "1.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1lv52rhk3a8sa1jbzf2idriwz3p8z75qw4kjx53qzwi841frp6c6") (y #t)))

(define-public crate-ferristype-1.0.1 (c (n "ferristype") (v "1.0.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0y7x9ngl3ppvgqi4064fjgqs4fshzdv8vjf24gjg19nkbksm5sb7") (y #t)))

(define-public crate-ferristype-1.0.2 (c (n "ferristype") (v "1.0.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 1)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-embed") (r "^6.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1lmr3yp4xz0zh8cx6h5wq5l52dwmn90kc3s015akzl9sfxx57zp3")))

