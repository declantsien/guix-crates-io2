(define-module (crates-io fe rr ferris-gc) #:use-module (crates-io))

(define-public crate-ferris-gc-0.1.1 (c (n "ferris-gc") (v "0.1.1") (d (list (d (n "ferris-gc-proc-macro") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "ferris-gc-proc-macro") (r "^0.1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p2f3dzpg4j7xp6khqhw4nbvn362d9rgx36v1crn540wgq1z5v25") (f (quote (("proc-macro" "ferris-gc-proc-macro"))))))

(define-public crate-ferris-gc-0.1.2 (c (n "ferris-gc") (v "0.1.2") (d (list (d (n "ferris-gc-proc-macro") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "ferris-gc-proc-macro") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10rbhl806kqw7b9mfaai3g8545yqblqz0ii8waf6s899i7ydigk8") (f (quote (("proc-macro" "ferris-gc-proc-macro"))))))

(define-public crate-ferris-gc-0.1.3 (c (n "ferris-gc") (v "0.1.3") (d (list (d (n "ferris-gc-proc-macro") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "ferris-gc-proc-macro") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0071gah2q3hvwa034z6fds25vi70cmmsa0zb3024w45sn3crxbl0") (f (quote (("proc-macro" "ferris-gc-proc-macro"))))))

(define-public crate-ferris-gc-0.1.4 (c (n "ferris-gc") (v "0.1.4") (d (list (d (n "ferris-gc-proc-macro") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "ferris-gc-proc-macro") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qwb8kpvxc4x71g7zs4hwbp6acm1v9c2bvs7ispiqyd18yaps4xv") (f (quote (("proc-macro" "ferris-gc-proc-macro"))))))

(define-public crate-ferris-gc-0.1.5 (c (n "ferris-gc") (v "0.1.5") (d (list (d (n "ferris-gc-proc-macro") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "ferris-gc-proc-macro") (r "^0.1.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "08dddh19sj8b3z63pfim28rpl63fyi4kphnahz6iq18bh7w8jas9") (f (quote (("proc-macro" "ferris-gc-proc-macro"))))))

