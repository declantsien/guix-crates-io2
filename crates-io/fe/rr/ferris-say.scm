(define-module (crates-io fe rr ferris-say) #:use-module (crates-io))

(define-public crate-ferris-say-0.1.0 (c (n "ferris-say") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vj65jdiam5j3hscbf013mr0jkqfjlccg5ggdlags7i2l7r5cccz")))

