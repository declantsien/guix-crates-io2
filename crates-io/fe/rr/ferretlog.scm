(define-module (crates-io fe rr ferretlog) #:use-module (crates-io))

(define-public crate-ferretlog-0.1.0 (c (n "ferretlog") (v "0.1.0") (h "1qsmzd9zd6dyh0yc00c3abjaagf863fxx5fvsv5ls7qz0qzl70ab")))

(define-public crate-ferretlog-0.1.1 (c (n "ferretlog") (v "0.1.1") (h "1al62midwxh4mxrqjs0gx477xsj6g3q27axq6a7zsyjgq80bvavq")))

