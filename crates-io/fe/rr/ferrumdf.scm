(define-module (crates-io fe rr ferrumdf) #:use-module (crates-io))

(define-public crate-ferrumDF-0.0.9 (c (n "ferrumDF") (v "0.0.9") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0jzdq3jln45v6filinwib9dqhji6bi6z2gg6nh9gqpxnfa50sk4q")))

(define-public crate-ferrumDF-0.1.0 (c (n "ferrumDF") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "02fsbm1hl69d9x1i2jjq997i4r1bjlxq13nfd6ccypl5xpvpmlqk")))

(define-public crate-ferrumDF-0.1.1 (c (n "ferrumDF") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0lk3plh2mq4knmxsrbf9hq0qxdgfzfzwb53c9zd9g8qnp9gq5pxs")))

