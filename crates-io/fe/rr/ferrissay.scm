(define-module (crates-io fe rr ferrissay) #:use-module (crates-io))

(define-public crate-ferrissay-0.1.0 (c (n "ferrissay") (v "0.1.0") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0a16wv7iq11fxiihvxzalzwm9v4f9wygf4jdh09f86c151h78zvm")))

(define-public crate-ferrissay-0.1.1 (c (n "ferrissay") (v "0.1.1") (d (list (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)))) (h "0lm14857vyy7k3diddf815c8lmpmq5ylv905kjv30nzazmsjmabr")))

