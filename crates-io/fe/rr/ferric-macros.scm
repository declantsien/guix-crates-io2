(define-module (crates-io fe rr ferric-macros) #:use-module (crates-io))

(define-public crate-ferric-macros-0.1.0 (c (n "ferric-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z3djbhfqs1zag8gqv60kb6hbqf39j913gm3r0n48nka6yhj85f8")))

(define-public crate-ferric-macros-0.1.1 (c (n "ferric-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "01fvckd0ia3xwbb01j9lzjjak0jjkqkyawv16xqnlghgsx18cr7d") (y #t)))

(define-public crate-ferric-macros-0.1.2 (c (n "ferric-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0qjnmxxl353rif4hj3zr7mqldd5hnnbn4acb2g3aikklbbwciyf5")))

