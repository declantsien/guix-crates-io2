(define-module (crates-io fe ar fearless_simd) #:use-module (crates-io))

(define-public crate-fearless_simd-0.1.0 (c (n "fearless_simd") (v "0.1.0") (h "0rcn8l0qfsgy2qfna4x2vnvz5v60j84c9b4fanfgxw9bwz7yr89k")))

(define-public crate-fearless_simd-0.1.1 (c (n "fearless_simd") (v "0.1.1") (h "146s33kc5ficzsyz813vhb5wnayfk5asgd57jlcmpzyndclfr3hd")))

