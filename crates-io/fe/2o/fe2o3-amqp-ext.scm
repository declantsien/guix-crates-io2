(define-module (crates-io fe #{2o}# fe2o3-amqp-ext) #:use-module (crates-io))

(define-public crate-fe2o3-amqp-ext-0.0.1 (c (n "fe2o3-amqp-ext") (v "0.0.1") (d (list (d (n "fe2o3-amqp-types") (r "^0.0.30") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.0.11") (d #t) (k 0)))) (h "013nsw57bp7i0bbcd85fpplk9mkw6qd3zsrs9gi3pdr6ww6bv1gy")))

(define-public crate-fe2o3-amqp-ext-0.1.0 (c (n "fe2o3-amqp-ext") (v "0.1.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.1.0") (d #t) (k 0)))) (h "0irq3w6asnnz82kmq40br7lc6hyx0160qsjxhkmr0w1mzimmrwrz")))

(define-public crate-fe2o3-amqp-ext-0.1.1 (c (n "fe2o3-amqp-ext") (v "0.1.1") (d (list (d (n "fe2o3-amqp-types") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.1.1") (d #t) (k 0)))) (h "0pzislsp8bcyv1bp9cza74awwzigxlksv444wc3ajj541w2mcl4b")))

(define-public crate-fe2o3-amqp-ext-0.1.2 (c (n "fe2o3-amqp-ext") (v "0.1.2") (d (list (d (n "fe2o3-amqp-types") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.1.1") (d #t) (k 0)))) (h "0qws1r2xgx7w143fpm7j1rdmjp4b6l1b06gdfsl3fhq3ssj3202r")))

(define-public crate-fe2o3-amqp-ext-0.1.3 (c (n "fe2o3-amqp-ext") (v "0.1.3") (d (list (d (n "fe2o3-amqp-types") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.1.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yafz1nkzf599ia5wnx6l8vqk94y6bzk9ycssy5w61vxvzckns8a")))

(define-public crate-fe2o3-amqp-ext-0.2.0 (c (n "fe2o3-amqp-ext") (v "0.2.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nfk2wi6lkhsnf1yrjv4s56abg84ks5lirk8bvfh27rvf9s76xic")))

(define-public crate-fe2o3-amqp-ext-0.2.1 (c (n "fe2o3-amqp-ext") (v "0.2.1") (d (list (d (n "fe2o3-amqp-types") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yv5gh9acs3pahs07gylc1f4a80czn0l1vsqsavibqv4bzchslqy")))

(define-public crate-fe2o3-amqp-ext-0.3.0 (c (n "fe2o3-amqp-ext") (v "0.3.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lmsvhlrvdclfrynqvadkmz37dbawswbxmkc0gb9zy67if6xmqc6")))

(define-public crate-fe2o3-amqp-ext-0.3.1 (c (n "fe2o3-amqp-ext") (v "0.3.1") (d (list (d (n "fe2o3-amqp-types") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hcrwx5fhvnv9dmkhlllwvzw2qi8m77da6r11bw7nwifwwn66x0n")))

(define-public crate-fe2o3-amqp-ext-0.4.0 (c (n "fe2o3-amqp-ext") (v "0.4.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w0qfqr3j0bdfd1ihhnjh2zhwmi6d57p37k61yizfmjbnv84yd6f")))

(define-public crate-fe2o3-amqp-ext-0.5.0-alpha (c (n "fe2o3-amqp-ext") (v "0.5.0-alpha") (d (list (d (n "fe2o3-amqp-types") (r "^0.6.0-alpha") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.5.0-alpha") (f (quote ("derive"))) (d #t) (k 0)))) (h "1818vnffb9ff8w2vckhnksz6m2r02qjvrqgq57pfxlqcisdazid8")))

(define-public crate-fe2o3-amqp-ext-0.5.0 (c (n "fe2o3-amqp-ext") (v "0.5.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17vfr5vr0fsfm6w2jp83y88kffvfln5p4agswmccjb9g1bs30sww")))

(define-public crate-fe2o3-amqp-ext-0.6.0 (c (n "fe2o3-amqp-ext") (v "0.6.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sbcxgvgfydsn1ydax4vmk0grsi7k2y4c5vi3835fs2d6k205rrs")))

(define-public crate-fe2o3-amqp-ext-0.9.0 (c (n "fe2o3-amqp-ext") (v "0.9.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.9.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lk1sciang0yk7zp4g721sgnpfqm96s7pb4ksrs4rn66xa4dcnhc")))

(define-public crate-fe2o3-amqp-ext-0.10.0 (c (n "fe2o3-amqp-ext") (v "0.10.0") (d (list (d (n "fe2o3-amqp-types") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_amqp") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d6gcg4adgsh8sfxn7krldw31qcq4nqj5g8nzybkz3jycpmvf9ha")))

