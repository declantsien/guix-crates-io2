(define-module (crates-io fe #{2o}# fe2o3-amqp-cbs) #:use-module (crates-io))

(define-public crate-fe2o3-amqp-cbs-0.0.1 (c (n "fe2o3-amqp-cbs") (v "0.0.1") (d (list (d (n "fe2o3-amqp") (r "^0.6.7") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.2") (d #t) (k 0)))) (h "17hg8nghanwgswgjqp2a1kifzb40qn4pvf107kv924dqq6wr1780")))

(define-public crate-fe2o3-amqp-cbs-0.0.2 (c (n "fe2o3-amqp-cbs") (v "0.0.2") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "0af46l0d6pflh7xi0ygqgcvfd224qalfa5m5f3qn8849xwhsq387")))

(define-public crate-fe2o3-amqp-cbs-0.0.3 (c (n "fe2o3-amqp-cbs") (v "0.0.3") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "1igldvjqkic2cxpy17an1yf954zc9hz73xi98vv4mafzv5ssg9fw")))

(define-public crate-fe2o3-amqp-cbs-0.0.4 (c (n "fe2o3-amqp-cbs") (v "0.0.4") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "07njp32vsygi27sisqadljgnzs5q2ajz3cl1z4rfgywp0si59i7s")))

(define-public crate-fe2o3-amqp-cbs-0.0.5 (c (n "fe2o3-amqp-cbs") (v "0.0.5") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "18kshzz1a8yqjckn1mcaf2gdx0zqzi39gq01kbwwzr4xvnaac6p7")))

(define-public crate-fe2o3-amqp-cbs-0.0.6 (c (n "fe2o3-amqp-cbs") (v "0.0.6") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "0807l8yw3xahf52jggzx9pxm83nq1vygk618qdx0frjh6kflyzgh")))

(define-public crate-fe2o3-amqp-cbs-0.0.7 (c (n "fe2o3-amqp-cbs") (v "0.0.7") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "1kns11yxzbdvk3fldl7qph3vvs6q3wlpka41pslabpc4a5g3q3za")))

(define-public crate-fe2o3-amqp-cbs-0.0.8 (c (n "fe2o3-amqp-cbs") (v "0.0.8") (d (list (d (n "fe2o3-amqp") (r "^0.7.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.3") (d #t) (k 0)))) (h "1y2bvzx4j06sz936nwidc84fl8gcvhmncwz8vb7im7yp73c8zq4s")))

(define-public crate-fe2o3-amqp-cbs-0.0.9 (c (n "fe2o3-amqp-cbs") (v "0.0.9") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.4") (d #t) (k 0)))) (h "18ys1ns67kl6r2hswhip3czavcq4fm4lj77p3ajd46v12jip7148")))

(define-public crate-fe2o3-amqp-cbs-0.0.10 (c (n "fe2o3-amqp-cbs") (v "0.0.10") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.5") (d #t) (k 0)))) (h "0paah9f9j3ry8cx1wrs5k0kx7lmvkr4hpbjp04yp3z62jw8q9pvk")))

(define-public crate-fe2o3-amqp-cbs-0.0.11 (c (n "fe2o3-amqp-cbs") (v "0.0.11") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.0.5") (d #t) (k 0)))) (h "14vxa5afgddj9hnbjvvrhnr72jfa7k8h8acil7bcnzb9g6pv6cpj")))

(define-public crate-fe2o3-amqp-cbs-0.1.0 (c (n "fe2o3-amqp-cbs") (v "0.1.0") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.1.0") (d #t) (k 0)))) (h "0d8ba51qas5jzd3m7n15xy15l1jbdkkfhy85fvnpwlvrr7zbhvda")))

(define-public crate-fe2o3-amqp-cbs-0.1.1 (c (n "fe2o3-amqp-cbs") (v "0.1.1") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.1.0") (d #t) (k 0)))) (h "1z94qplliz03p0w93nzl8hgbj3dn2pk5kdp2as22kza9jrdfbmdx")))

(define-public crate-fe2o3-amqp-cbs-0.2.0 (c (n "fe2o3-amqp-cbs") (v "0.2.0") (d (list (d (n "fe2o3-amqp") (r "^0.8.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.2.0") (d #t) (k 0)))) (h "026kpaxykfbl3sdn3crak2pgg1gfrl6qrdrrnf0bfdppc610p3yw")))

(define-public crate-fe2o3-amqp-cbs-0.9.0 (c (n "fe2o3-amqp-cbs") (v "0.9.0") (d (list (d (n "fe2o3-amqp") (r "^0.9.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.9.0") (d #t) (k 0)))) (h "0bkl42wmp8kvinxpr0ypghyra5n876mxazb3a0pi5x26khz8f6g7")))

(define-public crate-fe2o3-amqp-cbs-0.10.0 (c (n "fe2o3-amqp-cbs") (v "0.10.0") (d (list (d (n "fe2o3-amqp") (r "^0.10.0") (d #t) (k 0)) (d (n "fe2o3-amqp-management") (r "^0.10.0") (d #t) (k 0)) (d (n "trait-variant") (r "^0.1.1") (d #t) (k 0)))) (h "012g1r3b56nxl7i544xpkh1z5699dbzigqz6l6malsl66mrvdscj")))

