(define-module (crates-io fe #{2o}# fe2o3_parsers) #:use-module (crates-io))

(define-public crate-fe2o3_parsers-0.1.0 (c (n "fe2o3_parsers") (v "0.1.0") (d (list (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1rmqd51czizd3kbrqqp1kfghsawf5nqk40hil561319r4cll8iiy")))

