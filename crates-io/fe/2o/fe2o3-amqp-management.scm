(define-module (crates-io fe #{2o}# fe2o3-amqp-management) #:use-module (crates-io))

(define-public crate-fe2o3-amqp-management-0.0.1 (c (n "fe2o3-amqp-management") (v "0.0.1") (d (list (d (n "fe2o3-amqp") (r "^0.6.1") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qzwg4ba26qxk4f3s6wnsg3x9ljqplw6r1dpiwcr56bbkn4nwwfy")))

(define-public crate-fe2o3-amqp-management-0.0.2 (c (n "fe2o3-amqp-management") (v "0.0.2") (d (list (d (n "fe2o3-amqp") (r "^0.6.7") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11h8zb141frf7lbmfqpyai8s16dsmif9hrzcipd5kd85p36ncsks")))

(define-public crate-fe2o3-amqp-management-0.0.3 (c (n "fe2o3-amqp-management") (v "0.0.3") (d (list (d (n "fe2o3-amqp") (r "^0.7") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04z27ljkv61gd0gig4ra4hki2biy0h6z5j0ppviw9x7lk6cd9zxq")))

(define-public crate-fe2o3-amqp-management-0.0.4 (c (n "fe2o3-amqp-management") (v "0.0.4") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dlnwxajs4a4vdh49agd21p8wd9c8x6nyaicyi9gblccmq0fhq7h")))

(define-public crate-fe2o3-amqp-management-0.0.5 (c (n "fe2o3-amqp-management") (v "0.0.5") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09r0gxksam8c5r96y1dsa29iwgz6x0z88h6k38iradaakyalpmc3")))

(define-public crate-fe2o3-amqp-management-0.1.0 (c (n "fe2o3-amqp-management") (v "0.1.0") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c7g245idimz5wbd1sirbbd2zxia6i0hmhsrjxm934rk849lzk81")))

(define-public crate-fe2o3-amqp-management-0.2.0 (c (n "fe2o3-amqp-management") (v "0.2.0") (d (list (d (n "fe2o3-amqp") (r "^0.8.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "179h6chlyb701hjdkn7v4p650px9xzgaamsczjp7dkq4gngigv7r")))

(define-public crate-fe2o3-amqp-management-0.1.1 (c (n "fe2o3-amqp-management") (v "0.1.1") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "075r0aqad7plgpyq86i22j77567ih6p2xpi2vffwlzw2xp0993gr")))

(define-public crate-fe2o3-amqp-management-0.2.1 (c (n "fe2o3-amqp-management") (v "0.2.1") (d (list (d (n "fe2o3-amqp") (r "^0.8.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b1a6dl72mgfswafnqb3j0d3jrbl5gc6jl8kamnjjpfv2zrmd598")))

(define-public crate-fe2o3-amqp-management-0.1.2 (c (n "fe2o3-amqp-management") (v "0.1.2") (d (list (d (n "fe2o3-amqp") (r "^0.7.11") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11hlk68220hvr98iqv6sicxsq4fsj3j4vqs6am3i948hlxc6931j")))

(define-public crate-fe2o3-amqp-management-0.2.2 (c (n "fe2o3-amqp-management") (v "0.2.2") (d (list (d (n "fe2o3-amqp") (r "^0.8.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lpvi357frk2kccg14zrw0ywb40qa3qkr0kkylzhz07i2nwfpk5b")))

(define-public crate-fe2o3-amqp-management-0.9.0 (c (n "fe2o3-amqp-management") (v "0.9.0") (d (list (d (n "fe2o3-amqp") (r "^0.9.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1vl4j05w79wk9xm4346n1nhkfyk5mdh8b0gd50jq16m3an68sbd9")))

(define-public crate-fe2o3-amqp-management-0.9.1 (c (n "fe2o3-amqp-management") (v "0.9.1") (d (list (d (n "fe2o3-amqp") (r "^0.9.3") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "052z3cx4c6lqh9qxdi6y9qmycl1s6mlp8qpjgv3y123lkppkp0zy")))

(define-public crate-fe2o3-amqp-management-0.2.3 (c (n "fe2o3-amqp-management") (v "0.2.3") (d (list (d (n "fe2o3-amqp") (r "^0.8.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0cihlsl0dy98n9xygw0xjsgfgg0b0kg4fyg89fdp751k0684jgq2")))

(define-public crate-fe2o3-amqp-management-0.10.0 (c (n "fe2o3-amqp-management") (v "0.10.0") (d (list (d (n "fe2o3-amqp") (r "^0.10.0") (d #t) (k 0)) (d (n "fe2o3-amqp-types") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0c477z3p8gcppgy3nrd1p66lb8ipwf5c6fxiq9d9yy78l47dk71i")))

