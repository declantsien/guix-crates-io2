(define-module (crates-io fe nd fend-core) #:use-module (crates-io))

(define-public crate-fend-core-0.1.0 (c (n "fend-core") (v "0.1.0") (h "02haqf15hfsmjpspfymshwijwqzpnd5d3jma14gn0g9n564vrcmi")))

(define-public crate-fend-core-0.1.1 (c (n "fend-core") (v "0.1.1") (h "1xanc44xdnfrd62984ghq5rgmvm6qn0635pp6wl0bsp7rrwdqcfm")))

(define-public crate-fend-core-0.1.2 (c (n "fend-core") (v "0.1.2") (h "0visy9xib04xhrjg0k4bcajxrcs21wvi8bpgq8g9bkn6l05aj2wf")))

(define-public crate-fend-core-0.1.3 (c (n "fend-core") (v "0.1.3") (h "0sx27mfzl8yv4620whrpff51jrfaasafz20apbbr6f4h3p7l7qh5")))

(define-public crate-fend-core-0.1.4 (c (n "fend-core") (v "0.1.4") (h "09xvmm473ypz5zzf0gcll5b3qcphydpvl4bmgqrx5j8zb18wd6pb") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.5 (c (n "fend-core") (v "0.1.5") (h "0phq3x51ir253g27vwc8453yfbq23azx99ag4zh1ymwv3fhj5pq0") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.6 (c (n "fend-core") (v "0.1.6") (h "1lymi91k4d50fmlwrbvmsmylxrls93k5aq0q0xvxpzqxwib1d2wm") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.7 (c (n "fend-core") (v "0.1.7") (h "0vl90fhydqpd2pxsv8iwrh1rj7m2g4dad0qw4c2bipnc5pp2sfm1") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.8 (c (n "fend-core") (v "0.1.8") (h "0n126mg5gfhrjc8cx4w86wsnnl1i9q9849k5n6dywnz9gpbr4xwc") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.9 (c (n "fend-core") (v "0.1.9") (h "1knlwfycjc0zmqrvq8w4yp3rc8n0c7paiwrqrjl3glv9nkaknvxg") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.10 (c (n "fend-core") (v "0.1.10") (h "07pwz9ipx9pqqnrjp3v1ikqqqqmirig9kwwb7lsficryhxa7ih8k") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.11 (c (n "fend-core") (v "0.1.11") (h "06r6q44ngb470hj4jwmr7bnkac9a3f631d5g7lfawnlvn1l1m8am") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.13 (c (n "fend-core") (v "0.1.13") (h "0abl5qxp557r18vkryjxpp5is9xi3zpy8ika0qp8wnqah6dim4fj") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.14 (c (n "fend-core") (v "0.1.14") (h "0xz0pl2k2nq3jgixizj7idxhqw8dzc2hfail078vygnkxgfxnyq4") (f (quote (("gpl") ("default" "gpl"))))))

(define-public crate-fend-core-0.1.15 (c (n "fend-core") (v "0.1.15") (h "1lrifsin53yvlq4w6c0gjfssgbdzj093daipvj9jr76bykm4f5g9") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.16 (c (n "fend-core") (v "0.1.16") (h "0qbw5avdfa75g5lwq18cb28580gl90ipcjay7k8yhmq2lc49nhdn") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.17 (c (n "fend-core") (v "0.1.17") (d (list (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "1gl0bqvgkazc1v53ksrsswb4x0licqnkd4w2x0myrxxi3gjxfh9l") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.18 (c (n "fend-core") (v "0.1.18") (h "1vid4zgq8cpazb90qrqwi6w72cqm3syxgvv3cx1l231m3v0fqs3q") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.19 (c (n "fend-core") (v "0.1.19") (h "0gbgnw6ri5p3y2rfssvf8f62pkr1asjixmh6baikalpvk4nk7vqv") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.20 (c (n "fend-core") (v "0.1.20") (h "000hzy23dkyqdldxp91yjfin7fi3cmsvw0iwm5scc0r0rnpr0sy8") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.21 (c (n "fend-core") (v "0.1.21") (h "09dwkaxk7sr06dijl3qmhay4vnqbjwx6sqw7lr59bqwb8hl363dl") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.22 (c (n "fend-core") (v "0.1.22") (h "1cqjvgkpapr2dndrlga4l4cqrfnfgf8ydzbqvcm20d7yb586kbnn") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.23 (c (n "fend-core") (v "0.1.23") (h "02f4zn7d69x5wvp1jn5w1h43xl24pksbfcxzg4ckbri3ikahqzxa") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.24 (c (n "fend-core") (v "0.1.24") (h "0xqpl9hagv9bzjbjy4lflhfjaqr1nvgz07kf62jzvghpicr2biil") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.25 (c (n "fend-core") (v "0.1.25") (h "1gpf95ifzhddch4xkjq4slh47c427wiggm5ibd4cr0j75iip5ppv") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.26 (c (n "fend-core") (v "0.1.26") (h "0vpv3n3sik51vlgnma7lgs5dw8mj60kha9bhj0c888bj3f80cn29") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.27 (c (n "fend-core") (v "0.1.27") (h "1m5rldhdd3c8p4lc30gck8axrbp3d6n3njiylhzmjp7gpwip1hhv") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.28 (c (n "fend-core") (v "0.1.28") (h "0grw83vkkzxr5yjdxygzs666y68382xl2949vp6zmfp39qw2xxkq") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-0.1.29 (c (n "fend-core") (v "0.1.29") (h "1h2sasxhmmzixkdg0lc4jad6m87834vbf8q0z6zgshs8yc5n1x3r") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.0 (c (n "fend-core") (v "1.0.0") (h "1s5al5a1zhg364anpi5m6fqwyvdn67ff38bp5h5v7g6qa0dsjvkn") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.1 (c (n "fend-core") (v "1.0.1") (h "1m4gn8zi5q3i4kndrjsb0dpx14rdgd395g6mhwf0zdxirzbkhfxh") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.2 (c (n "fend-core") (v "1.0.2") (h "12xjk64pivijpkxy34dsv990j408wrhzi08g4fj79mflj3jav02b") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.3 (c (n "fend-core") (v "1.0.3") (h "1yi68bq1kjaivqzxkrlnmsgxidp8dzf8injp1fzzvl35k52l4ixa") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.4 (c (n "fend-core") (v "1.0.4") (h "1yjlxsas8gcdlilk1j4rk5jkrbah4y05d6768krh7k5wx38gcgxb") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.0.5 (c (n "fend-core") (v "1.0.5") (h "0il5z6p1idq2fzzgd6i2kyrgg61qfdmkd5m6js20vzqyqh3dxmzz") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.1.0 (c (n "fend-core") (v "1.1.0") (h "1zf2k6pjk6ajmffxkw8xal5xrapm4i9yx2h27l4q32jdvnqigvp6") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.1.1 (c (n "fend-core") (v "1.1.1") (h "08j4m3nky0zw4dy5v7n4ichcf2k3bg47aa9hk41m2g5mf5ca9aaz") (f (quote (("gpl") ("default"))))))

(define-public crate-fend-core-1.1.2 (c (n "fend-core") (v "1.1.2") (h "1ldl475px6p33ssf1c406zs1mx85ckcyhckjzpbg88i8jjjd8gnc")))

(define-public crate-fend-core-1.1.3 (c (n "fend-core") (v "1.1.3") (h "1jcpvn0lnn6pg5ka1yvzgpmadmicjbvavda9gamyq92l5gbf6mjh")))

(define-public crate-fend-core-1.1.4 (c (n "fend-core") (v "1.1.4") (h "1n0kbbwbzvmyvjyfvgbhg23xav1mff110j91n5y83nvy7b6k4n04")))

(define-public crate-fend-core-1.1.5 (c (n "fend-core") (v "1.1.5") (h "04j0w93f3n8gnkzf3kf8l9f7bhrl676ab8mszavczlynixn5m46s")))

(define-public crate-fend-core-1.1.6 (c (n "fend-core") (v "1.1.6") (h "1msm59z6vq4f5xcr90fyrihyp2yl3fnv9xmqwi1y9k3dpdv1dblz")))

(define-public crate-fend-core-1.2.0 (c (n "fend-core") (v "1.2.0") (h "1n24l4vjaw554kdfq06hra67d7v2ij1ya7l6f043sx3siw4glzsv")))

(define-public crate-fend-core-1.2.1 (c (n "fend-core") (v "1.2.1") (h "0ywgas77cvkp2gwk79aq506bf5gj4k37528y0fhz2p1hnnhgak25")))

(define-public crate-fend-core-1.2.2 (c (n "fend-core") (v "1.2.2") (h "0aq962lv9b62xfy1303czaza9vvw5zh94mf0cyzilsbd44d2kp34")))

(define-public crate-fend-core-1.3.0 (c (n "fend-core") (v "1.3.0") (h "1hdxnihfz804vhj3xd4zc598fzqqgyvqjc50l3q77y0fj1ymdql6")))

(define-public crate-fend-core-1.3.1 (c (n "fend-core") (v "1.3.1") (h "0jbxwznlzb42d7917v3l79q29dyqdh8hpn9xz823lrmrmgdmzwhh")))

(define-public crate-fend-core-1.3.2 (c (n "fend-core") (v "1.3.2") (h "05zz3jb7gzp0wlrmr8pbcprlndp2bjhwvpkfj1vwm8a1s1d99m4s")))

(define-public crate-fend-core-1.3.3 (c (n "fend-core") (v "1.3.3") (h "1qaxsfqqyk9d7w91zsrf0lmmywg7hih8mh3cgx6hz66fwqxylw74")))

(define-public crate-fend-core-1.4.0 (c (n "fend-core") (v "1.4.0") (h "0sl9snimpxhj80pzp90r0fxq4qhgk5xhk2xa6pdxajiw74d6l53h")))

(define-public crate-fend-core-1.4.1 (c (n "fend-core") (v "1.4.1") (h "0lysk1vls1h78vh6ms5x9d7dv4ig5416cr901dzjhycm8s87lniz")))

(define-public crate-fend-core-1.4.2 (c (n "fend-core") (v "1.4.2") (h "1ryl7lwhlx1sv2w7mi0rwmxymxh4gzs9hv1hx445s6y8vdn229nf")))

(define-public crate-fend-core-1.4.3 (c (n "fend-core") (v "1.4.3") (h "0f0n0aa12ch7czkd6h7h8n4gdhp37sfw51qjg3n73brjmyg8498b")))

(define-public crate-fend-core-1.4.4 (c (n "fend-core") (v "1.4.4") (h "1s5v4g783y0bj4v13v983aw33k8kbqcimfq85491p5lg3kp81255")))

(define-public crate-fend-core-1.4.5 (c (n "fend-core") (v "1.4.5") (h "1g05fs7injrh4xbx16bg0b3kn4ccmcvingkwgljgzk14dqb1bywc")))

(define-public crate-fend-core-1.4.6 (c (n "fend-core") (v "1.4.6") (h "0cpr307ya0lb5sww1ivx4qdgbc1c07dm8iwrmi752fi3044dhhr6")))

(define-public crate-fend-core-1.4.7 (c (n "fend-core") (v "1.4.7") (h "0a2z6cp3bf629h1cb0b0q8xi4b104y6aiq81zvxknm5fjw2rvlhx")))

(define-public crate-fend-core-1.4.8 (c (n "fend-core") (v "1.4.8") (h "1m48af3ah28pvaafrhdqbd16jbz8b8m5d4d1c386ygyw6m6m55js")))

