(define-module (crates-io fe ig feignhttp-codegen) #:use-module (crates-io))

(define-public crate-feignhttp-codegen-0.0.0 (c (n "feignhttp-codegen") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1167iz3c4a75wv29pgphw9nzzahk6ims0ya7pqpcrqbk9f8y13n3")))

(define-public crate-feignhttp-codegen-0.1.0 (c (n "feignhttp-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yd5ws02zfxspms1xnq7ghqfrn2gjxs810038lr4zb4qyk6982zd")))

(define-public crate-feignhttp-codegen-0.1.1 (c (n "feignhttp-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7d3am35s801l2wdvh9c796615r2ha83j5bn973qfrm9lgq6si0")))

(define-public crate-feignhttp-codegen-0.1.2 (c (n "feignhttp-codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1njmf284kbxz5fabd36mkgs8lhc101kpzppmwgymld0idbsm6g4i")))

(define-public crate-feignhttp-codegen-0.2.0 (c (n "feignhttp-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dhrn1g64viycpjzy97zjkw64phiz48qpik4banzw2fqg0m812sn")))

(define-public crate-feignhttp-codegen-0.2.3 (c (n "feignhttp-codegen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15m46b424l5wy8g72bplh1waqnlhq7lhnwwlnmiczn3lp31s4wa2")))

(define-public crate-feignhttp-codegen-0.3.0 (c (n "feignhttp-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bs2084h73mhf5ycb3qbbsqwywv5f4rc9mj2zq98sksiryzz3sd5")))

(define-public crate-feignhttp-codegen-0.3.3 (c (n "feignhttp-codegen") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15pf2flv9h8qww46fsiad0q8i6wwlb125hhd55zc2cpa6afjc1n0")))

(define-public crate-feignhttp-codegen-0.4.0 (c (n "feignhttp-codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04193ssnn3j3v045i1lbxafh8d2n9xk9542gc10xdv0xv4kf9wxr")))

(define-public crate-feignhttp-codegen-0.4.1 (c (n "feignhttp-codegen") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "167zyxnnrbc35lyhp29x9lv2a1n6s26yi3lv5bmbr4mfhs4sp7ci")))

(define-public crate-feignhttp-codegen-0.4.2 (c (n "feignhttp-codegen") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "0kcagpdng4l18b2082rk4lg9fi4vwmhmy7bnbxk42ryprazcsp11")))

(define-public crate-feignhttp-codegen-0.4.3 (c (n "feignhttp-codegen") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "13div4adi3sj422d7lzpz90g0hq2bk908d5ly3ybzkgs53vj2hbz")))

(define-public crate-feignhttp-codegen-0.4.4 (c (n "feignhttp-codegen") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "10iiqgc339qq9alnfb1rcx411kplfscmb6ff4q52kjdkn9xqlqz2")))

(define-public crate-feignhttp-codegen-0.4.5 (c (n "feignhttp-codegen") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "0pzsj1k5v0mdh74qsjnfh2bkqcqj14kxa4ds3xz7plhrk6vrlim3")))

(define-public crate-feignhttp-codegen-0.5.0 (c (n "feignhttp-codegen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "0g3ymcz1848lgxz3jp88ax6yrjpk4aqpcnsgigwrz6ddpmw8yjp2")))

(define-public crate-feignhttp-codegen-0.5.1 (c (n "feignhttp-codegen") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "034wvncw9lqh8zjp905qbl5lx4w9hwybkj7va2xcsmddkra7n7rh")))

(define-public crate-feignhttp-codegen-0.5.2 (c (n "feignhttp-codegen") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.62") (d #t) (k 2)))) (h "13l3adxf0qhhnv4dbidiqk9qslm6dkb6w8jci0fvx9kn4hmp9inc")))

