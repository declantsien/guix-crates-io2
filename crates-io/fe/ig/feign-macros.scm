(define-module (crates-io fe ig feign-macros) #:use-module (crates-io))

(define-public crate-feign-macros-0.1.0 (c (n "feign-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (d #t) (k 0)))) (h "0g0b3z30fvq6hgffal74w9ls4idfzbczc1d9kayfzq02kqjwjl49")))

(define-public crate-feign-macros-0.1.1 (c (n "feign-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "179s763mmwmrhrs33h32ar7xps8w8airzdzgxysafxm27ym5frzq")))

(define-public crate-feign-macros-0.1.2 (c (n "feign-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0myw9gv82qpy4zz8i7mw5gdq16scnc717f29w3mg5cf7r91hsn1l")))

(define-public crate-feign-macros-0.1.3 (c (n "feign-macros") (v "0.1.3") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0pcppz1pa2f4w7kijrrpimrpjzs1kfcfz9xfr5jawgwq4406a0h4")))

(define-public crate-feign-macros-0.1.4 (c (n "feign-macros") (v "0.1.4") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w3jnyn417i473sizn596459j29301q4vljknks9x8gjrgddwm7w")))

(define-public crate-feign-macros-0.1.5 (c (n "feign-macros") (v "0.1.5") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06haglxknldqv77kd2hwzn6hn01c79j95g1967i5a0w4qahb60xb")))

(define-public crate-feign-macros-0.1.6 (c (n "feign-macros") (v "0.1.6") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jfbhbfmzv34sm8jar9aj6iz7i6rwiq9cxqxm9v593khz33v3w25")))

