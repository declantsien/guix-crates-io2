(define-module (crates-io fe ig feign) #:use-module (crates-io))

(define-public crate-feign-0.1.0 (c (n "feign") (v "0.1.0") (d (list (d (n "feign-macros") (r "^0.1.0") (d #t) (k 0)))) (h "11f9wfn2f65n51wri8fmcjs8ibsmb04fkgg219gqjmhkwbva9zgy")))

(define-public crate-feign-0.1.1 (c (n "feign") (v "0.1.1") (d (list (d (n "feign-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0acq7yd6jzmcjlcpr27i36q7r1gl23hxikaifdqx6cpy09cfxsvv")))

(define-public crate-feign-0.1.2 (c (n "feign") (v "0.1.2") (d (list (d (n "feign-macros") (r "^0.1.2") (d #t) (k 0)))) (h "0qlfrd58h75pzl3dx7jihv98qinm9kjm4v294mv9wj22ir4m2n56")))

(define-public crate-feign-0.1.3 (c (n "feign") (v "0.1.3") (d (list (d (n "feign-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1r4z32bc39hrzjdsnc1q1fi0ajqjj4h1qw44i0b4y38kikb7xddi")))

(define-public crate-feign-0.1.4 (c (n "feign") (v "0.1.4") (d (list (d (n "feign-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pf7sf4wn4airngwiymzjb8sq4df7addfdjbblyxh8zl9xyndyw1")))

(define-public crate-feign-0.1.5 (c (n "feign") (v "0.1.5") (d (list (d (n "feign-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0l0rif1rs7zc0n4f934z5rqbann7n1jmh43qg0dfqa0qv4p826qr")))

(define-public crate-feign-0.1.6 (c (n "feign") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "feign-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gsrqyjmgjaa7hayxx3n44kggi8qn2xciw2h51413hrr2m19wxg8")))

