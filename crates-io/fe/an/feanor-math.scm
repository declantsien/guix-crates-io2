(define-module (crates-io fe an feanor-math) #:use-module (crates-io))

(define-public crate-feanor-math-1.0.0 (c (n "feanor-math") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "10vcx4mm0rv8f8ffwcbqjvsfr5plwzj730d2zmsn5ff544n47d90") (f (quote (("mpir") ("log_memory") ("generic_tests"))))))

(define-public crate-feanor-math-1.0.1 (c (n "feanor-math") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1c96m518lw9lknsqc7fab14579q1qlc0rlqxi7snxvlzvnvrhb8x") (f (quote (("mpir") ("log_memory") ("generic_tests"))))))

(define-public crate-feanor-math-1.1.0 (c (n "feanor-math") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1v5p55fc10hp7hq0ljqwqajgyi7hz02vc5spwmy2n348vx7m54zh") (f (quote (("mpir") ("log_memory") ("generic_tests"))))))

(define-public crate-feanor-math-1.2.0 (c (n "feanor-math") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "0p129hc7nfckcpp7pgd40x6y44n1wi281a26i8c6xrrp4mwlkb9z") (f (quote (("mpir") ("log_memory") ("generic_tests"))))))

(define-public crate-feanor-math-1.3.0 (c (n "feanor-math") (v "1.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1pqgdg7j8kjrw0zz08bq5ksgs59mwp8fllfvkf251bv1gkx4qyka") (f (quote (("mpir") ("log_memory") ("generic_tests"))))))

(define-public crate-feanor-math-1.4.0 (c (n "feanor-math") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "121ahaprrfn16zl7yx04ily7pq0mnm5l2zayk01w8dxp28m0w49j") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-feanor-math-1.4.1 (c (n "feanor-math") (v "1.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1x6cap26w4hkiabd50da79y8cz3li8v56cbn32xzmbdabz2rj0bx") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-feanor-math-1.5.0 (c (n "feanor-math") (v "1.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0isn3zpsgqvnql9wakv9v3gwg0ngxfi3jnj7877xa3pac5nz5vbp") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-feanor-math-1.5.1 (c (n "feanor-math") (v "1.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0kryjyw6s933z7y4jlw57yxh3n7d9mynxj1l0avjh2d45ds7i4m7") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-feanor-math-1.5.2 (c (n "feanor-math") (v "1.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1s3swspaavb6pzxvmlvis4yjp39my6c98pmvx8lspijggda34yjm") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon"))))))

(define-public crate-feanor-math-1.5.3 (c (n "feanor-math") (v "1.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1k2k5z4x69j77sp74x4h5pwhzphd380gcjyilghsb7mg06azhdpd") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon") ("ndarray" "dep:ndarray"))))))

(define-public crate-feanor-math-1.6.0 (c (n "feanor-math") (v "1.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1zkwv9h1jjcv9d1qsmwp8jp1n111m1j8bx24blc55d6r8j6zni68") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon") ("ndarray" "dep:ndarray"))))))

(define-public crate-feanor-math-1.7.0 (c (n "feanor-math") (v "1.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "1q1d1zkmmkxabk7pwncw9sfcn6xgzz2chlngvqy7bavsmmw6rhl2") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon") ("ndarray" "dep:ndarray"))))))

(define-public crate-feanor-math-1.7.1 (c (n "feanor-math") (v "1.7.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "0bm6gp8n7488sbbn46fp0vy2mp280pxvi4kf7jnc3hfkfv4649i3") (f (quote (("mpir") ("log_memory") ("generic_tests")))) (s 2) (e (quote (("parallel" "dep:rayon") ("ndarray" "dep:ndarray"))))))

