(define-module (crates-io fe bi febits) #:use-module (crates-io))

(define-public crate-febits-0.1.0 (c (n "febits") (v "0.1.0") (h "1g5vv9ppg1jsar5z04ygxb4k7q8z5qkkkis4g1a6q90yncw6x7iw") (y #t)))

(define-public crate-febits-0.1.1 (c (n "febits") (v "0.1.1") (h "0wp4wk1rb08m5rls5lf8dq346psw549law4nrim385qmxmyjf73x") (y #t)))

(define-public crate-febits-0.1.2 (c (n "febits") (v "0.1.2") (h "02dl57vf59hnl5jg953l0fzwrphzgakwwgkpi2wlbsn0d8sfzz3n")))

(define-public crate-febits-0.1.3 (c (n "febits") (v "0.1.3") (h "0wf73fnxr21xsv1asgf0fxar750jcfxc7v7avlg1532bmkxy02n3")))

