(define-module (crates-io fe do fedora-coreos-metrics-client) #:use-module (crates-io))

(define-public crate-fedora-coreos-metrics-client-0.0.1 (c (n "fedora-coreos-metrics-client") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "liboverdrop") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0k0y4arjqvw24v9bqvrh6fcszgikcksnd9jcjk9xhd38zfqzw9di")))

(define-public crate-fedora-coreos-metrics-client-0.0.2 (c (n "fedora-coreos-metrics-client") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "liboverdrop") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0zrs70xjdddxaccca4qyw9cik8y000bks738scdhb49r1ivfq6jl")))

