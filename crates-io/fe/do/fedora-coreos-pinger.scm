(define-module (crates-io fe do fedora-coreos-pinger) #:use-module (crates-io))

(define-public crate-fedora-coreos-pinger-0.0.3 (c (n "fedora-coreos-pinger") (v "0.0.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "liboverdrop") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0an0myacgxi8c4hpabiccsi1r53m2xsd0ahkl7f37zxk99d3frn6")))

(define-public crate-fedora-coreos-pinger-0.0.4 (c (n "fedora-coreos-pinger") (v "0.0.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "liboverdrop") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "19dmxzpjh2rkc5jj65j6jm3vizjs801fykcm1krvr871l1y86c0k")))

