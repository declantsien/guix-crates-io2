(define-module (crates-io fe do fedora-prime) #:use-module (crates-io))

(define-public crate-fedora-prime-0.1.0 (c (n "fedora-prime") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "03x6q8g3ryb61mfd009553fgq54cpyjaxpcpq3vkl6w5hwx4na5j")))

(define-public crate-fedora-prime-0.1.1 (c (n "fedora-prime") (v "0.1.1") (h "1f0zp7xgbb2gcmhs9gqgwqjwrg42136bssajzfv3x9ywr28jsg93")))

