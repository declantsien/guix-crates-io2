(define-module (crates-io nn -r nn-rs) #:use-module (crates-io))

(define-public crate-nn-rs-0.1.0 (c (n "nn-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("rand" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kkj85sjwnf3ibsqrd9q0njizx84d17jbjjyf8birl3mm3m8dhbg")))

(define-public crate-nn-rs-0.1.1 (c (n "nn-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("rand" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s25hhnv2qqbjjf6fp0239g9vbafdy0m547pamvcci00mic5qgzj")))

(define-public crate-nn-rs-0.1.2 (c (n "nn-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29.0") (f (quote ("rand" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "173x9q7zb2as47f1dncp34d1r4l04k402v39jck1k18vrgmrqq3f")))

