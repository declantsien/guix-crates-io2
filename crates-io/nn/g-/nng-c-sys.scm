(define-module (crates-io nn g- nng-c-sys) #:use-module (crates-io))

(define-public crate-nng-c-sys-1.8.0-beta.1 (c (n "nng-c-sys") (v "1.8.0-beta.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0z3h8arjmshwhqwlywxq8mp53q6hfp4s5di0rc9vhlvihm9wnmjm") (f (quote (("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.2 (c (n "nng-c-sys") (v "1.8.0-beta.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "14mimg4ns8m5g12fy8dw0blxgi3lafhaws8sznlr461fmxnlqmp0") (f (quote (("tls") ("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.3 (c (n "nng-c-sys") (v "1.8.0-beta.3") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "09ykrah3fyx599bpcqz820mj32gnccn90i18ciqi2a61lxdmqpwq") (f (quote (("tls") ("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.4 (c (n "nng-c-sys") (v "1.8.0-beta.4") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0x02i3546yh6lan73ikpqpsd1cck2hmdl11qdcf4j7rq68b8l1my") (f (quote (("tls") ("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.5 (c (n "nng-c-sys") (v "1.8.0-beta.5") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0679j4nk5iqazzjljijy0dwvmxnqadp3ybxvbv4ab6dzzbpgsdv6") (f (quote (("tls") ("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.6 (c (n "nng-c-sys") (v "1.8.0-beta.6") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "12v3yj7ch11p4w3kj31fs5cf93bcbnczp9v20pixl417cwix2xba") (f (quote (("websocket" "http") ("tls") ("stats") ("http") ("build-bindgen" "bindgen"))))))

(define-public crate-nng-c-sys-1.8.0-beta.7 (c (n "nng-c-sys") (v "1.8.0-beta.7") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1h80ckpni7vbaifrlvh1knmng2kfsfjixn5296wnk31p37ym0r84") (f (quote (("websocket" "http") ("tls") ("stats") ("http") ("build-bindgen" "bindgen"))))))

