(define-module (crates-io nn ls nnls) #:use-module (crates-io))

(define-public crate-nnls-0.1.0 (c (n "nnls") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)))) (h "0dy8pp1l924d2wsyh08wjwkd0l4aly2a9p3xv83ssy5wgyqnk29y")))

(define-public crate-nnls-0.2.0 (c (n "nnls") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)))) (h "1pm6yvc3ybcc8ii83874kqva7ldi6biywpmja5bwmwbrszvsiy36")))

(define-public crate-nnls-0.3.0 (c (n "nnls") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx-0_5"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)))) (h "0ys3026iimqfnh2ab67x5dafwvb3n5p5sgx128jhzbfrxmcz0jfw")))

