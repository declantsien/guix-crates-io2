(define-module (crates-io nn ap nnapi) #:use-module (crates-io))

(define-public crate-nnapi-0.1.0 (c (n "nnapi") (v "0.1.0") (d (list (d (n "nnapi-sys") (r "^0.1") (d #t) (k 0)))) (h "0dyb4k5ns3xwp1viyr2vxy19lh22z4cixhpqdv85vnf1ha94dbkq")))

(define-public crate-nnapi-0.2.0 (c (n "nnapi") (v "0.2.0") (d (list (d (n "nnapi-sys") (r "^0.1") (d #t) (k 0)))) (h "1g9x50y01lrmg01v0javfcx1sfhp41h1kh90alvjdp64zbkfibhb")))

