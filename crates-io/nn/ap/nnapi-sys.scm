(define-module (crates-io nn ap nnapi-sys) #:use-module (crates-io))

(define-public crate-nnapi-sys-0.1.0 (c (n "nnapi-sys") (v "0.1.0") (h "1pn45hf29bgbngxv0qm53hvg6bn1lc4446c31w0x42qjlchiqyfl")))

(define-public crate-nnapi-sys-0.1.1 (c (n "nnapi-sys") (v "0.1.1") (h "107ra22y9xbahq7npgdj17z36p0bqm48b5q1lr2fn1lwm0y2f9s3")))

