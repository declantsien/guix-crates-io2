(define-module (crates-io nn sd nnsdk) #:use-module (crates-io))

(define-public crate-nnsdk-0.1.0 (c (n "nnsdk") (v "0.1.0") (d (list (d (n "libc-nnsdk") (r "^0.0.1") (d #t) (k 0)))) (h "1qz47ljnfy67q467pc2bvkqbz08mqy2wm87g9plcs5nnq1qgifv6") (f (quote (("rustc-dep-of-std"))))))

(define-public crate-nnsdk-0.2.0 (c (n "nnsdk") (v "0.2.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc-nnsdk") (r "^0.2.0") (d #t) (k 0)))) (h "0v49arcd5cxwqqayf4xnyjd0jr3gwazqqb6l0vs2bfkhfiy57x70") (f (quote (("rustc-dep-of-std" "libc-nnsdk/rustc-dep-of-std" "core" "alloc" "compiler_builtins"))))))

(define-public crate-nnsdk-0.3.0 (c (n "nnsdk") (v "0.3.0") (d (list (d (n "alloc") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-alloc")) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "libc-nnsdk") (r "^0.3.0") (d #t) (k 0)))) (h "1qqgjb1lqp5dshai9p2c0ymsdz4l9vvq4wmqlvlm50lf5q2qahyz") (f (quote (("rustc-dep-of-std" "libc-nnsdk/rustc-dep-of-std" "core" "alloc" "compiler_builtins"))))))

