(define-module (crates-io nn er nnera) #:use-module (crates-io))

(define-public crate-nnera-23.214.42 (c (n "nnera") (v "23.214.42") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clia-tracing-config") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ri8h1r61ing2iaci2v01jp5qfrlla3dy04xb9askg6mzqm1yqw4")))

