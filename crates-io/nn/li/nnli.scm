(define-module (crates-io nn li nnli) #:use-module (crates-io))

(define-public crate-nnli-0.0.3 (c (n "nnli") (v "0.0.3") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1nns6ppm6axqql6ncii5m6kzd7ihw4rks47ijjkfwvhqs5vzgfb1")))

(define-public crate-nnli-0.0.4 (c (n "nnli") (v "0.0.4") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1njgal3say142vyk6nl8v3wz9j7zcgj1nkgjvdy5csiy4l24zlfz")))

(define-public crate-nnli-0.0.5 (c (n "nnli") (v "0.0.5") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "13z4qicjqy594wy690fp772zc49d7alrkcj6mwwn835m9z7jhaqy")))

(define-public crate-nnli-0.0.6 (c (n "nnli") (v "0.0.6") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0syrgav9qhri2dp2dfipn67kci96i8sqm65h61lhfav8hl7rbgap")))

(define-public crate-nnli-0.0.7 (c (n "nnli") (v "0.0.7") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0fcfzdcanyfsvdbdy8mnlzb2hwpjqkpagqr8fzj8lir5dj7m35v9")))

(define-public crate-nnli-0.0.8 (c (n "nnli") (v "0.0.8") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1dc1vg6l54xpli0nsslw721p287jhpaay2zfswb0ksdz6v9ksbci")))

(define-public crate-nnli-0.0.11 (c (n "nnli") (v "0.0.11") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "10vzlyxgq5sn7ziwl5qxiqwi3mzb3qrrql7w6w07bc1fmf7z4w9l")))

(define-public crate-nnli-0.0.12 (c (n "nnli") (v "0.0.12") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1qc1z1j4psns0l7wd2mbkhz99s7sqfpqwr981swj7h5sd4xlfn0l")))

(define-public crate-nnli-0.0.13 (c (n "nnli") (v "0.0.13") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "02w99bkq3ra5ddf9g738wylfaxj9wh6mpy2r5k14jz1l06n5aid7")))

(define-public crate-nnli-0.0.14 (c (n "nnli") (v "0.0.14") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0dpclx3ky6jln4dikqpaava5icmg0w83ysx2cxcd5x3x6sb5ps1r")))

(define-public crate-nnli-0.0.15 (c (n "nnli") (v "0.0.15") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1qczhac7a2p5yf6rbhqcnif0qgy7lyi2bsqhfk392qbgqqfgmpkc")))

(define-public crate-nnli-0.0.17 (c (n "nnli") (v "0.0.17") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1wjx35rsgdh09m5mhs98mq02jkcq33kll0afxf7kmc70gckv2gv2")))

(define-public crate-nnli-0.0.18 (c (n "nnli") (v "0.0.18") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0d90nxc7r0in9k686ikw3f95bmsrz9wqm5s18chsprmb5l4syx4s")))

(define-public crate-nnli-0.0.19 (c (n "nnli") (v "0.0.19") (d (list (d (n "candle-onnx") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0gdgl3vzxm3fys2f8n3rcmr8l60h6dwfdyc4l8xnhnbl5z9699sc")))

