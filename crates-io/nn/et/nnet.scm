(define-module (crates-io nn et nnet) #:use-module (crates-io))

(define-public crate-nnet-0.0.1 (c (n "nnet") (v "0.0.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0fnwyyyd7px8ka1lp8kva6l9kgs3nb9xmjp3zd39bqywsw0h9r4i")))

(define-public crate-nnet-0.0.2 (c (n "nnet") (v "0.0.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1pjlw2144bjmk7pnqw1x8wv367dkig9lhwk173ql48wh66cghyp5")))

