(define-module (crates-io nn rs nnrs) #:use-module (crates-io))

(define-public crate-nnrs-0.1.0 (c (n "nnrs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("std" "backtrace"))) (d #t) (k 0)))) (h "10yqa24b8pw6icmk30vark5h61azgz202qgbdyjcyib134hnk0jd")))

(define-public crate-nnrs-0.2.0 (c (n "nnrs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "1qd800anig90xxhppp9hhnr61zjyp1v6k4maa0vwbzqjv8zxw1dh")))

(define-public crate-nnrs-0.2.1 (c (n "nnrs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0kri3dbnf1wg8rjwv6mr4g04gf6a38myfpr26vlvx6cg4g3cm35y")))

(define-public crate-nnrs-0.2.2 (c (n "nnrs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "078bbyndq8609l2qk59zb0k2lkb8mpjq49nvpvk8f0grakjfx20x")))

(define-public crate-nnrs-0.2.3 (c (n "nnrs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.69") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "07wlcxr38g5gv9gl6dm4nljpnsrd8fxm2zx9p3nl2awhglb53chw")))

