(define-module (crates-io nn ye nnye_solana_communication_layer) #:use-module (crates-io))

(define-public crate-nNye_solana_communication_layer-0.1.0 (c (n "nNye_solana_communication_layer") (v "0.1.0") (h "1wq0h3az206ahb0dyz78dki5svgd6c65f26bvka8b13l1510ys4z")))

(define-public crate-nNye_solana_communication_layer-0.1.1 (c (n "nNye_solana_communication_layer") (v "0.1.1") (h "1xaj65wmbq2ciwjqyagc3kvgk3h7dpbypajnf77dahrsqmv80wsr")))

(define-public crate-nNye_solana_communication_layer-0.1.2 (c (n "nNye_solana_communication_layer") (v "0.1.2") (h "0lp22zqbxpx9vy5kdpb3p3jzq9ycfa0cjswjvlyp1qw1j2ibmadz")))

