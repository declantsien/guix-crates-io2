(define-module (crates-io nn ye nnye_user_persistence) #:use-module (crates-io))

(define-public crate-nNye_user_persistence-0.1.1 (c (n "nNye_user_persistence") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.4") (d #t) (k 0)))) (h "1rikyha7rscdb2ql6z6vcl231yv04qw8m9m46y5rda1dkfpkpxg6")))

(define-public crate-nNye_user_persistence-0.1.2 (c (n "nNye_user_persistence") (v "0.1.2") (d (list (d (n "diesel") (r "^1.4.4") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.4") (d #t) (k 0)))) (h "002w66sxlrxbab05i61qv55dpz81pj16vv66l50bjk0vvxmv915k")))

