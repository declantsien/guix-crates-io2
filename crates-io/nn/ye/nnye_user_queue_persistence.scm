(define-module (crates-io nn ye nnye_user_queue_persistence) #:use-module (crates-io))

(define-public crate-nNye_user_queue_persistence-0.1.0 (c (n "nNye_user_queue_persistence") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1il1bygz65j03rx8zady0xzawwm97s8b9c3n7rflirx2sbfb7i5c")))

(define-public crate-nNye_user_queue_persistence-0.1.1 (c (n "nNye_user_queue_persistence") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1fyh06wnc7g8adxs1knbp4vyqpyi0pbn46bplcsy16d8wi6dpdwl")))

(define-public crate-nNye_user_queue_persistence-0.1.2 (c (n "nNye_user_queue_persistence") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "039sbczwd4dr0rvzy0h18j5baj79gh8x82jn2q2a94n7616c9f8p")))

(define-public crate-nNye_user_queue_persistence-0.1.3 (c (n "nNye_user_queue_persistence") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1k07mp4yz5vwqayplq5x7wjilmixpyayhcpc3xbvbxx4w8a4x7rk")))

(define-public crate-nNye_user_queue_persistence-0.1.4 (c (n "nNye_user_queue_persistence") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_solana_communication_layer") (r "^0.1.1") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0cvrxya3izdd105b46dv71rpfwx55ny468h8dy6w0psap4hgzngv")))

(define-public crate-nNye_user_queue_persistence-0.1.5 (c (n "nNye_user_queue_persistence") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "nNye_solana_communication_layer") (r "^0.1.1") (d #t) (k 0)) (d (n "nNye_user_business") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1vprv7f9vrkfsri9wg19cqw8biagb69lv120cd4lldjlrrh1xq20")))

