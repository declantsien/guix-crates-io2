(define-module (crates-io nn rt nnrt) #:use-module (crates-io))

(define-public crate-nnrt-1.0.0 (c (n "nnrt") (v "1.0.0") (d (list (d (n "k_board") (r "^1.1.10") (d #t) (k 0)))) (h "1l3y1zv4639k5w6h845aky6wgvbni9x9hzz2inskg05d41rmbw8g")))

(define-public crate-nnrt-1.0.1 (c (n "nnrt") (v "1.0.1") (d (list (d (n "k_board") (r "^1.1.10") (d #t) (k 0)))) (h "1h3rq2mbgffcg4sivhm0b1sg2xp4xcfw1qa6js8qiv620gwh1d6k")))

(define-public crate-nnrt-1.0.2 (c (n "nnrt") (v "1.0.2") (d (list (d (n "k_board") (r "^1.2.1") (f (quote ("lower_letter" "upper_letter" "ctrl_lower_letter" "f" "standar"))) (d #t) (k 0)))) (h "1g35k4104wzqa9n8h8q5nzpkdqqpcaqdhn3arnxxj5a5amprg94q")))

(define-public crate-nnrt-1.0.3 (c (n "nnrt") (v "1.0.3") (d (list (d (n "k_board") (r "^1.2.2") (f (quote ("lower_letter" "upper_letter" "ctrl_lower_letter" "f" "standar"))) (d #t) (k 0)))) (h "0mmbaadaigyj5sw7irj0q3q0r3y9xphqvgdyj13126d923npg2mk")))

