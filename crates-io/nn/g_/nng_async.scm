(define-module (crates-io nn g_ nng_async) #:use-module (crates-io))

(define-public crate-nng_async-0.2.0 (c (n "nng_async") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.0-alpha") (d #t) (k 0) (p "futures-preview")) (d (n "futures-timer") (r "^0.3") (d #t) (k 2)) (d (n "futures_util") (r "^0.3.0-alpha") (d #t) (k 0) (p "futures-util-preview")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "runng-sys") (r "^1.1.2-rc") (d #t) (k 0)) (d (n "runng_derive") (r "^0.2") (d #t) (k 0)))) (h "0y9jwg8fnf885sibnw3k90cx327rxp58ybag3xfvwkdmcp72z97s") (f (quote (("stats") ("pipes") ("default" "pipes" "stats"))))))

