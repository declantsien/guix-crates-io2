(define-module (crates-io nn tp nntp) #:use-module (crates-io))

(define-public crate-nntp-0.0.1 (c (n "nntp") (v "0.0.1") (h "0p6l0dv7rb93v3rlxdnjs5ziidxb6safiqr5b2js3dbgplqyyhjw")))

(define-public crate-nntp-0.0.2 (c (n "nntp") (v "0.0.2") (h "0viqb28ny21bxkkjrpqxj86yzm781wr5vsnix4wrzg0vzs6v6lmv")))

(define-public crate-nntp-0.0.3 (c (n "nntp") (v "0.0.3") (h "1zc4jxmmn5i8a44bfmg8lhqmaiyxvkh0rj36rpsbkjmfh72br18y")))

(define-public crate-nntp-0.0.4 (c (n "nntp") (v "0.0.4") (h "1g15nq9f1bb3jcy9f3c7rbnwikc7ad9xc0ddcjl9c1s7c1a1v6js")))

(define-public crate-nntp-0.0.5 (c (n "nntp") (v "0.0.5") (h "1fc217yzjz1291ddv7rb3vdhxikw073a016npmk2zq4mk1nynvrv")))

