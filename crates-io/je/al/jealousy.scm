(define-module (crates-io je al jealousy) #:use-module (crates-io))

(define-public crate-jealousy-0.1.0 (c (n "jealousy") (v "0.1.0") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "jealousy_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qgc3svrli19cxk5iqcy1bw824j8v8ax9mqjb8h2hrj8wyym4g0p") (f (quote (("derive")))) (y #t)))

(define-public crate-jealousy-0.1.1 (c (n "jealousy") (v "0.1.1") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "jealousy_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "1wzk4k4v1llcrrf5ldir83smv299vhy5w3d2jx9gzsn8b6mip47v") (f (quote (("derive")))) (y #t)))

(define-public crate-jealousy-0.1.2 (c (n "jealousy") (v "0.1.2") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "jealousy_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "0vx17iw59a41707isiaaji9bxab61z77rsn3jq2m5zc2ff510sdb") (f (quote (("derive")))) (y #t)))

(define-public crate-jealousy-0.1.3 (c (n "jealousy") (v "0.1.3") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "jealousy_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "1qzrnic312hwp25hgpy755yq4lnkwsbwkd0m6aqfyh4wjny2rbi2") (f (quote (("derive"))))))

(define-public crate-jealousy-0.1.5 (c (n "jealousy") (v "0.1.5") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "jealousy_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 2)))) (h "1gqghbcm1qrb548in0ww883ay1fkjmwahj303wy51fvicn7nwmjc") (f (quote (("derive"))))))

