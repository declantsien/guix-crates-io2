(define-module (crates-io je al jealousy_derive) #:use-module (crates-io))

(define-public crate-jealousy_derive-0.1.0 (c (n "jealousy_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0079rkji5ff7qwzi163ih8hawn1jz47n8fa4549fszqgny3qc6yh")))

(define-public crate-jealousy_derive-0.1.1 (c (n "jealousy_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrxi18dn645ajda05ig4381wdgsyavlip249y822jjh52674a4w")))

