(define-module (crates-io je la jelastic-rs) #:use-module (crates-io))

(define-public crate-jelastic-rs-0.1.0 (c (n "jelastic-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "01yl74p90lm3vb1lbh98a5h8zcp0cmhynp1xv8yirlqh7djvyab3")))

