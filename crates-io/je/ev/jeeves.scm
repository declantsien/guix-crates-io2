(define-module (crates-io je ev jeeves) #:use-module (crates-io))

(define-public crate-jeeves-0.1.0 (c (n "jeeves") (v "0.1.0") (d (list (d (n "peg") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "06v2b2yrrdwkx8mnk777h2nk8i8908ncwwz3rlagwmwxyh2b18y9")))

