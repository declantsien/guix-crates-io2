(define-module (crates-io je nq jenq) #:use-module (crates-io))

(define-public crate-jenq-0.1.1 (c (n "jenq") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "jenkins_api") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "0a9783rnbcv185pqllfwdm5ycasyppmjsvis5ii1p62j68jzhv8h")))

(define-public crate-jenq-0.1.2 (c (n "jenq") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "jenkins_api") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "loggerv") (r "^0.7.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "031rl9xly2p0pfyvqdqsfza0dpvg4xvh96dkixmmz1vm496n3h5s")))

