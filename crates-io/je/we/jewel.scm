(define-module (crates-io je we jewel) #:use-module (crates-io))

(define-public crate-jewel-0.1.0 (c (n "jewel") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (f (quote ("defmt"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 0)))) (h "05k0p3rgxpv7w0vclc895fv3ppd8ha072bvblm5nl6jd9k0k6fqd") (y #t)))

(define-public crate-jewel-0.1.1 (c (n "jewel") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (f (quote ("defmt"))) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (k 0)))) (h "0h4zjiv6jn82i6b7xg1bahxf47isalia1xfq3hc2xc6wnkcd46m4")))

