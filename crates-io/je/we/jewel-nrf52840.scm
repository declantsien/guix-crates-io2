(define-module (crates-io je we jewel-nrf52840) #:use-module (crates-io))

(define-public crate-jewel-nrf52840-0.1.0 (c (n "jewel-nrf52840") (v "0.1.0") (d (list (d (n "embassy-nrf") (r "^0.1.0") (f (quote ("defmt" "nrf52840" "time-driver-rtc1" "time"))) (d #t) (k 0)))) (h "1rvcik8p2y27fw0bifkwbvhm9bnkh4x1qjfg65zq0nx6bnzns0kx")))

