(define-module (crates-io je sp jesper) #:use-module (crates-io))

(define-public crate-jesper-0.1.0 (c (n "jesper") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "keccak-hash") (r "^0.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1psyijrh0ya0hy7651vzcs8rm9jp5v5plpqiacgvv7ksi3pay75j")))

