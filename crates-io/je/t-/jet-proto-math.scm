(define-module (crates-io je t- jet-proto-math) #:use-module (crates-io))

(define-public crate-jet-proto-math-1.0.0 (c (n "jet-proto-math") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "113x7p45jh6vz02j3pmaymmbig2ik5ggwqpjhxwj7ypzafmh49b8")))

(define-public crate-jet-proto-math-1.0.1 (c (n "jet-proto-math") (v "1.0.1") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "0chj9fhliz0bcydsx7iwxyhhr0kbi945fw9wx6cmj4g4kmk6qdh2")))

(define-public crate-jet-proto-math-1.0.2 (c (n "jet-proto-math") (v "1.0.2") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "170lw94bwp5811g2fz026pmjckzx88wgqv3p28npbp53lg4d7mrd")))

(define-public crate-jet-proto-math-1.0.4 (c (n "jet-proto-math") (v "1.0.4") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "1x71ac3cdayw65nydffya2l1xccxs2rnpbpvsi3n3m09gsyanl86")))

(define-public crate-jet-proto-math-1.0.6 (c (n "jet-proto-math") (v "1.0.6") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "uint") (r "^0.8") (d #t) (k 0)))) (h "1zgnnz70vr2zw4if1vbk17wl2gf5p00h0jvjd0lj0z4gy4b4hlcl")))

