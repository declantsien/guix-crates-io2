(define-module (crates-io je t- jet-proto-proc-macros) #:use-module (crates-io))

(define-public crate-jet-proto-proc-macros-1.0.0 (c (n "jet-proto-proc-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0npysh7vly665cgknh3x1i961qqbrjqs7llrnivkh7jnv4v1ckzl")))

(define-public crate-jet-proto-proc-macros-1.0.1 (c (n "jet-proto-proc-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1whhcf8n0pw8r15wdf91zi537h21ah286n0fjwcw7ddnxc9wv4j1")))

(define-public crate-jet-proto-proc-macros-1.0.3 (c (n "jet-proto-proc-macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.57") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vggwgkw38m5nlnzicnhlm1p79vswhzkcagkwv2sk7shv5plravy")))

