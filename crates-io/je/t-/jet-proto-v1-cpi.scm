(define-module (crates-io je t- jet-proto-v1-cpi) #:use-module (crates-io))

(define-public crate-jet-proto-v1-cpi-0.21.0 (c (n "jet-proto-v1-cpi") (v "0.21.0") (d (list (d (n "anchor-lang") (r "^0.21.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "jet-math") (r "^1") (d #t) (k 0) (p "jet-proto-math")) (d (n "jet-proc-macros") (r "^1") (d #t) (k 0) (p "jet-proto-proc-macros")) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ddx7h2rjwzii5d1abnqa83c5m185dhnn0fhq26zl6q3waf6798n")))

(define-public crate-jet-proto-v1-cpi-0.22.0 (c (n "jet-proto-v1-cpi") (v "0.22.0") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "jet-math") (r "^1") (d #t) (k 0) (p "jet-proto-math")) (d (n "jet-proc-macros") (r "^1") (d #t) (k 0) (p "jet-proto-proc-macros")) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1pnkvm98zvdlfqz72xvv8iygg6sz0y7cr56fx1l73fc9varqv4b9")))

(define-public crate-jet-proto-v1-cpi-0.23.0 (c (n "jet-proto-v1-cpi") (v "0.23.0") (d (list (d (n "anchor-lang") (r "^0.23.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "jet-math") (r "^1") (d #t) (k 0) (p "jet-proto-math")) (d (n "jet-proc-macros") (r "^1") (d #t) (k 0) (p "jet-proto-proc-macros")) (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rn7d27851069irl9l5xbmvjf97ygi1syd2cnj4lv22mfpgi9rwk")))

