(define-module (crates-io je ts jetson_gpio) #:use-module (crates-io))

(define-public crate-jetson_gpio-0.1.0 (c (n "jetson_gpio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "0m9n8pv9kadazhw76k5iq16f0qfk0zzfvwhsdvign504m5fz746w")))

(define-public crate-jetson_gpio-0.1.1 (c (n "jetson_gpio") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)))) (h "1p2nqlx3h7sp84lsd1dq1475wifnk82vivr4ysy5awqjz04f7aj9")))

