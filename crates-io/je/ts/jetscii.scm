(define-module (crates-io je ts jetscii) #:use-module (crates-io))

(define-public crate-jetscii-0.1.0 (c (n "jetscii") (v "0.1.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1f73b90x88iid6kd9sz3515f24aa0xpsscq82lbqxlrr3pmfl345")))

(define-public crate-jetscii-0.2.0 (c (n "jetscii") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1lm7hz4z7m7ks5yv5mr9j9w3ad04882vhvky932b68h3i3cfpp69") (f (quote (("unstable"))))))

(define-public crate-jetscii-0.3.0 (c (n "jetscii") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0c8lx0sq5hgil6axgxgxazpj1wc2akffypvlfbshw3lh3f46x2n7") (f (quote (("unstable"))))))

(define-public crate-jetscii-0.3.1 (c (n "jetscii") (v "0.3.1") (d (list (d (n "libc") (r "*") (d #t) (k 2)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1sq6d6c9vi44gkr566w2f1d4n6mmrjx8gjdwgnhkgcsg051j391j") (f (quote (("unstable"))))))

(define-public crate-jetscii-0.4.0 (c (n "jetscii") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0mj87md5yg2scw5ihrd6qjnccvc3sj42fqizl0b2pblnr74hd2br") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.4.1 (c (n "jetscii") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1lgr6mm15xpa7im0kfy45bmkp1b8c8q5r3xfacjml5v462jvlyj5") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.4.2 (c (n "jetscii") (v "0.4.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "0knc03nixh2jk3d14l3k9v1hrvda6lvzmn4c14ibwvk3pipp74gp") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.4.3 (c (n "jetscii") (v "0.4.3") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.8.3") (d #t) (k 2)))) (h "0hg2qz1n4cs4n4xdz4dg76fy8awbc8fvwpbmh8g59za9fsa3kj3y") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.4.4 (c (n "jetscii") (v "0.4.4") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.0") (d #t) (k 2)) (d (n "region") (r "^2.0.0") (d #t) (k 2)))) (h "01cv5gcqdr1dw309ynjsrkhlqplbib9kpsv122xrvc9w8sicq9az") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.5.0 (c (n "jetscii") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "region") (r "^2.0.0") (d #t) (k 2)))) (h "1x4z5ixlxv1wa70d3d6ad3n8qxz6z00i13zwnv219v45qcwybbnb") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.5.1 (c (n "jetscii") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "region") (r "^3.0.0") (d #t) (k 2)))) (h "05ca13gcprg0xpr9id18yxfjf933dbwwsx883df2v2ksqlipji69") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.5.2 (c (n "jetscii") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "region") (r "^3.0.0") (d #t) (k 2)))) (h "0wz3358p1vimnhk1s43xlry0fci0whw7m5zj36j7h6lcdsvraa29") (f (quote (("pattern") ("benchmarks"))))))

(define-public crate-jetscii-0.5.3 (c (n "jetscii") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "memmap") (r "^0.7.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "region") (r "^3.0.0") (d #t) (k 2)))) (h "0pppbawc1v6lshz6zi3d4bkz7xbalph9sd78a5299jd94kz45wa7") (f (quote (("pattern") ("benchmarks"))))))

