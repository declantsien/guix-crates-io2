(define-module (crates-io je ts jetstream_p9_wire_format_derive) #:use-module (crates-io))

(define-public crate-jetstream_p9_wire_format_derive-0.1.1 (c (n "jetstream_p9_wire_format_derive") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1wy5a4pkbfqxhi8y41zfr9785rmlvq94mmd9fc5ijfhqbj1jdjic")))

(define-public crate-jetstream_p9_wire_format_derive-0.1.2 (c (n "jetstream_p9_wire_format_derive") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dhs9ssdvlh3qcj053l9h3xm5cgpbxfbrfq04msr8rhq2hpg3151")))

(define-public crate-jetstream_p9_wire_format_derive-0.1.3 (c (n "jetstream_p9_wire_format_derive") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0ansjh56m9fiyzgchbsysnyfwjnnimi338j35drgrg6kzyqdg0gb")))

(define-public crate-jetstream_p9_wire_format_derive-0.1.4 (c (n "jetstream_p9_wire_format_derive") (v "0.1.4") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1yp34fxdfwbiqdyli8li314mmxd7p6688n27pfa2sb1297mnrddf")))

(define-public crate-jetstream_p9_wire_format_derive-0.1.5 (c (n "jetstream_p9_wire_format_derive") (v "0.1.5") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1nqvi8kyvzj7amc5399sg14g61dh6w08cpxq9bkvsq6vqn1dp329")))

(define-public crate-jetstream_p9_wire_format_derive-0.3.2 (c (n "jetstream_p9_wire_format_derive") (v "0.3.2") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0y4mnwg459dclynh94y22xng5nhrm7b43pw754swrsarbw83nw1h")))

(define-public crate-jetstream_p9_wire_format_derive-0.4.0 (c (n "jetstream_p9_wire_format_derive") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05qmk0rz0ph8cv3v2cc77a9d1dimfvqmxra68w7z5h1vx46zxzzi")))

(define-public crate-jetstream_p9_wire_format_derive-0.5.0 (c (n "jetstream_p9_wire_format_derive") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "13id84iy29gq55nj9q1ld1s4rvb0k51f0fk3pxn6mvbjspi0b4ss")))

(define-public crate-jetstream_p9_wire_format_derive-0.5.1 (c (n "jetstream_p9_wire_format_derive") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qwjziv662z94zghngkd1jb6wnfi8996grf4xfxwqg9rz9ncmiff")))

(define-public crate-jetstream_p9_wire_format_derive-0.6.0 (c (n "jetstream_p9_wire_format_derive") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "06sm0qwimpv3dlziykrv64vw53ij4rvvw7gqhn1pdfh3d227fiwf")))

