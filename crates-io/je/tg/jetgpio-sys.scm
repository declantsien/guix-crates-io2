(define-module (crates-io je tg jetgpio-sys) #:use-module (crates-io))

(define-public crate-jetgpio-sys-0.1.0 (c (n "jetgpio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ajncfb76chiw6hz8gf3x5ji4kvsag83j1ksi4g73zgbvkal7v39") (l "jetgpio")))

(define-public crate-jetgpio-sys-0.1.1 (c (n "jetgpio-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hi726q1rj0bxj23r0yb7d9z3bnj29iv2xrd8zrxzdazjcxdcs00") (l "jetgpio")))

(define-public crate-jetgpio-sys-0.2.0-beta.0 (c (n "jetgpio-sys") (v "0.2.0-beta.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m93xl0lxd05gv0z1123c1nxs940ny3rwgqzylhb95m9y6mh5if9") (f (quote (("orin")))) (l "jetgpio")))

(define-public crate-jetgpio-sys-0.2.0 (c (n "jetgpio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sxl4k5bz3w71q7v1v00r79fvxfbyyd2bidsjwnhnazf2z9i1b7c") (f (quote (("orin")))) (l "jetgpio")))

(define-public crate-jetgpio-sys-0.2.1 (c (n "jetgpio-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "110gzk6rx71jij1zm2d760spxlp4sysw55861h13dn88hz372cyy") (f (quote (("orin")))) (l "jetgpio")))

