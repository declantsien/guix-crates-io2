(define-module (crates-io je rr jerry) #:use-module (crates-io))

(define-public crate-jerry-0.1.0 (c (n "jerry") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)))) (h "090zrk4c6m4zj17fbkb6zsj9qq5fl4q5jps3hlfig83hlql4wlmb")))

