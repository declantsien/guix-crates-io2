(define-module (crates-io je ng jenga) #:use-module (crates-io))

(define-public crate-jenga-0.1.0 (c (n "jenga") (v "0.1.0") (h "07z91ympw4r0gkwvx709smxcrz96vvz84x6sf4xjhs704va4jg91")))

(define-public crate-jenga-0.1.1 (c (n "jenga") (v "0.1.1") (h "016qncwwzxyl6v7jv98dfwva3cy31v97x718vm4bx2jz59nvvp21")))

(define-public crate-jenga-0.1.2 (c (n "jenga") (v "0.1.2") (h "1jfs16mn5bxbcp810gjj1d31f8kv1hr2wm2kfsfpwrax8d69n22s") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-jenga-0.1.3 (c (n "jenga") (v "0.1.3") (h "10ggk0v6pykpzywf38inpkdwz63199slblmkzsw8yizc6hhj2dnh") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (y #t)))

(define-public crate-jenga-0.1.4 (c (n "jenga") (v "0.1.4") (h "14c30w5aakalry4ilqkbf2xsyda7v82dmswh5l2ncw40sqsjaf6b") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (y #t)))

(define-public crate-jenga-0.1.5 (c (n "jenga") (v "0.1.5") (h "1kf2mj7f3s3nvsaq3ycbj6mw32cfnh0g57zw51zai4zqm6wxwzfa") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (y #t)))

(define-public crate-jenga-0.1.6 (c (n "jenga") (v "0.1.6") (h "1409phvqjjzxabszd0hgcvix8gyclpd4qcv3y423xlcws5vxkybb") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1.7 (c (n "jenga") (v "0.1.7") (h "1d2srl75dgrnzplmqvjmfk3xpyfcqb01nqss9vhlpm0xlgnxx0fb") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1.8 (c (n "jenga") (v "0.1.8") (h "0n56vykw816fmyivfyp3va592wh7riplmdzqs2w18xq91wgw9xqv") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1.9 (c (n "jenga") (v "0.1.9") (h "06lfy3494a2i71nfygbphliw5xxknalx7mifycg92av0kq1ani50") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1.10 (c (n "jenga") (v "0.1.10") (h "149pnb83788br3mzl05ljzn051ddywxnvrhpcrmkhcap0afxazm8") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

