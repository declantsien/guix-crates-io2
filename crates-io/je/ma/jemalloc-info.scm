(define-module (crates-io je ma jemalloc-info) #:use-module (crates-io))

(define-public crate-jemalloc-info-0.1.0 (c (n "jemalloc-info") (v "0.1.0") (d (list (d (n "jemalloc-ctl") (r "^0.3") (d #t) (k 0)) (d (n "rustler") (r "^0.21.0") (d #t) (k 0)))) (h "1p26c9m370l61rpbps4ka7sl1n7s88hvs7x1ah9myzf7gx0vv4xl")))

(define-public crate-jemalloc-info-0.2.0 (c (n "jemalloc-info") (v "0.2.0") (d (list (d (n "jemalloc-ctl") (r "^0.3") (d #t) (k 0)) (d (n "rustler") (r "^0.22") (d #t) (k 0)))) (h "0cj5b6nj3hn5dxdp5vxhgalr1v2v34bzig079g9ki29wm36qp96c")))

(define-public crate-jemalloc-info-0.2.1 (c (n "jemalloc-info") (v "0.2.1") (d (list (d (n "jemalloc-ctl") (r "^0.3") (d #t) (k 0)) (d (n "rustler") (r "^0.22") (d #t) (k 0)))) (h "06b2lmlbyry79z7832m64ci0xvwaxwic6y8niwjgkzw97fqbkjq6")))

(define-public crate-jemalloc-info-0.3.0 (c (n "jemalloc-info") (v "0.3.0") (d (list (d (n "jemalloc-ctl") (r "^0.3") (d #t) (k 0)) (d (n "rustler") (r "^0.22") (d #t) (k 0)))) (h "1b8jzx0xnq27n56h1022gfinaqf7wksqrffpy2dhlxfn19j1vy6x")))

(define-public crate-jemalloc-info-0.5.0 (c (n "jemalloc-info") (v "0.5.0") (d (list (d (n "jemalloc-ctl") (r "^0.5.0") (d #t) (k 0)) (d (n "rustler") (r "^0.25.0") (d #t) (k 0)))) (h "0qj44x5h3q9xkvlyjd83prbzhki0ah75v2rrcfai2jgnl23m15z3")))

