(define-module (crates-io je ma jemalloc-ctl) #:use-module (crates-io))

(define-public crate-jemalloc-ctl-0.1.0 (c (n "jemalloc-ctl") (v "0.1.0") (h "1lhms8hn29i4h1lcdmfvfhhvxfg99gb8y14w19jy7pqi0hj1h8j3")))

(define-public crate-jemalloc-ctl-0.1.1 (c (n "jemalloc-ctl") (v "0.1.1") (h "18jmxbmskiy3kz6xgdrcd88706381xlv1f7qznbfnimj951rpmaf")))

(define-public crate-jemalloc-ctl-0.1.2 (c (n "jemalloc-ctl") (v "0.1.2") (h "0ffrihjx2m15yl8papng9lsd1hzbvknb4icz0a9pmf90kmgbjyvk")))

(define-public crate-jemalloc-ctl-0.1.3 (c (n "jemalloc-ctl") (v "0.1.3") (h "0j9ac9bkvc04lcv40dakxx6qb9mckr08qv1kwdr90lyqzmq5qhim")))

(define-public crate-jemalloc-ctl-0.1.4 (c (n "jemalloc-ctl") (v "0.1.4") (h "1xxqv8hk3ivg7j2h8sfdw152889fdj3fmd0w9kz23yydpa85p69c")))

(define-public crate-jemalloc-ctl-0.2.0 (c (n "jemalloc-ctl") (v "0.2.0") (d (list (d (n "jemalloc-sys") (r "^0.1.7") (k 0)) (d (n "jemallocator") (r "^0.1.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d8i05ypykpsrlisrxznycgn5qg8pnqxaxh1c5mmqwvxgvrv14sf")))

(define-public crate-jemalloc-ctl-0.3.2 (c (n "jemalloc-ctl") (v "0.3.2") (d (list (d (n "jemalloc-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0a5ddnd7r7yfy19dx60m64i6l5wlvcw0fq47l9yyn00nians6l2v") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-jemalloc-ctl-0.3.3 (c (n "jemalloc-ctl") (v "0.3.3") (d (list (d (n "jemalloc-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1dx4mik2ww5ic4qqv5zx9dxq6pbkclxnxa9bscg4z4njkpzsa0n5") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-jemalloc-ctl-0.5.0 (c (n "jemalloc-ctl") (v "0.5.0") (d (list (d (n "jemalloc-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "jemallocator") (r "^0.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1sgxgdx1hdjs7sh287ci85q9gya7n4xd8pajm275vf1x3xkir2f1") (f (quote (("use_std" "libc/use_std") ("default"))))))

(define-public crate-jemalloc-ctl-0.5.4 (c (n "jemalloc-ctl") (v "0.5.4") (d (list (d (n "jemalloc-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "jemallocator") (r "^0.5.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0g5nb2aam7kc9vbbps25j99z80hlb7p14p8k9q2lqd2a882wgzvw") (f (quote (("use_std" "libc/use_std") ("disable_initial_exec_tls" "jemalloc-sys/disable_initial_exec_tls") ("default"))))))

