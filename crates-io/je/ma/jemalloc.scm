(define-module (crates-io je ma jemalloc) #:use-module (crates-io))

(define-public crate-jemalloc-0.1.0 (c (n "jemalloc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "loca") (r "^0.4") (d #t) (k 0)))) (h "1d778x0swdk60zcgqa96j5plzmqrbjjy4i3fhhzl8jhs4zckf3y3") (y #t)))

(define-public crate-jemalloc-0.1.1 (c (n "jemalloc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "loca") (r "^0.4") (d #t) (k 0)))) (h "0mrlfpynhp6yyx92wyzdwk7s6pa2dwl3ij02im0ffhi472pq0qjw") (y #t)))

(define-public crate-jemalloc-0.2.0 (c (n "jemalloc") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "loca") (r "^0.5.4") (d #t) (k 0)))) (h "0nxq9lbkb370aq917qmrpalmj3jnw8plagw8gjv0r9zr4ngrp7l8") (f (quote (("stable-rust" "loca/stable-rust")))) (y #t)))

(define-public crate-jemalloc-0.2.1 (c (n "jemalloc") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "loca") (r "^0.5.4") (d #t) (k 0)))) (h "0y4hlla5kc8hjnfnj6z2rrg0wxn7zjmx5dqglhfw4qfdz0yzvmav") (f (quote (("stable-rust" "loca/stable-rust"))))))

(define-public crate-jemalloc-0.2.2 (c (n "jemalloc") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "loca") (r "^0.5.4") (d #t) (k 0)))) (h "0m59jpg0nmjzvri6155f7wf4vqqbv9sj2cnkayi2mpq4jc0fb5fd") (f (quote (("stable-rust" "loca/stable-rust"))))))

(define-public crate-jemalloc-0.3.0 (c (n "jemalloc") (v "0.3.0") (d (list (d (n "loca") (r "^0.7") (d #t) (k 0)))) (h "1rpqfvvs09h47zsd15k06c8ga60m5pnmf5rcd0rr749h4vf2fnfx")))

