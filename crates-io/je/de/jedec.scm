(define-module (crates-io je de jedec) #:use-module (crates-io))

(define-public crate-jedec-0.0.0 (c (n "jedec") (v "0.0.0") (h "07hvhgjswa4lsn0m6l4pb1a298714j7k2mj050ysywdlshsd5kcq")))

(define-public crate-jedec-0.0.1 (c (n "jedec") (v "0.0.1") (h "0rkg5cnwn6k97mwx09y3qs3kf9nr5zvdghhi06xvfm6p05hm7ahd")))

(define-public crate-jedec-0.0.2 (c (n "jedec") (v "0.0.2") (h "14dwzbgxz39cpwldg9aks2hhdpgpdyh9ixk10lp35kbyhqlncil6")))

(define-public crate-jedec-0.0.3 (c (n "jedec") (v "0.0.3") (h "04hkgq70f6sxgxml2aknw8cqjvry1xd1b8znw8l2fr90nngch1h3")))

(define-public crate-jedec-0.1.0 (c (n "jedec") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)))) (h "0rydlc75md5yygs5nw26b8ymrb5nmyi2kzljsq2nwsg50gikklbl") (f (quote (("std") ("default" "std"))))))

