(define-module (crates-io je nt jentry) #:use-module (crates-io))

(define-public crate-jentry-0.1.0 (c (n "jentry") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0li7ij2ra3zy3hnbh8vdq3ws3jlpizlx7rhj2005y6cc5lghc8aq")))

