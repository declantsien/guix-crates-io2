(define-module (crates-io je xl jexl-eval) #:use-module (crates-io))

(define-public crate-jexl-eval-0.1.1 (c (n "jexl-eval") (v "0.1.1") (d (list (d (n "jexl-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r7q55w3dgnnxy1p3139ykh59fd0ic69zfafyzc93dsjfkckqkz5")))

(define-public crate-jexl-eval-0.1.3 (c (n "jexl-eval") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0076f3ihz7gaphx6yz804khbij0ncs28irfr3rv10980hx8k5xan")))

(define-public crate-jexl-eval-0.1.4 (c (n "jexl-eval") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.1.3-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "093hi2czcpnqrf6f55dsklycxfkg69sg7slq2l38c6pyda0g06c1")))

(define-public crate-jexl-eval-0.1.5 (c (n "jexl-eval") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.1.4-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rfd1sk0fjc13nkkk65xwk89ynmbsx1yaz4n6szhxxlnjxjf0zmd")))

(define-public crate-jexl-eval-0.1.6 (c (n "jexl-eval") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.1.6-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gyld9m52fblhxw7ggj58ghkb4izr8l7phav66qfidks2mwanhxi")))

(define-public crate-jexl-eval-0.1.7 (c (n "jexl-eval") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.1.7-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19718r0bhvv59g82gwzkmjimn1shv1n7bfwlb3z3cj10pzlqs81w")))

(define-public crate-jexl-eval-0.2.0 (c (n "jexl-eval") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10r06q07l8zvp2pmpm7v48mydlf269vc4z0bsfy4cfqacch3s472")))

(define-public crate-jexl-eval-0.2.1 (c (n "jexl-eval") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.2.1-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1aqgp1pymky7ms6igs8n907m066dzy336j2lc21z9xcka041bh3h")))

(define-public crate-jexl-eval-0.2.2 (c (n "jexl-eval") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "jexl-parser") (r "^0.2.2-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ihh9svd6ag80ka4x42p8n2sgal8kfkg2b4whrc9vqr0iyshrfs1")))

