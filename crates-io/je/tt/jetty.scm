(define-module (crates-io je tt jetty) #:use-module (crates-io))

(define-public crate-jetty-0.1.0 (c (n "jetty") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1") (d #t) (k 0)))) (h "023xknmlfpywspbab5g8jgfwb931zknmic5gk1jlj6skk7asawx9")))

(define-public crate-jetty-0.1.1 (c (n "jetty") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.1") (d #t) (k 0)))) (h "1ml7196ck1v7vfh5bag32rds1zj3fr2xij6pxs6nbg068rx03251")))

(define-public crate-jetty-0.2.0 (c (n "jetty") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)))) (h "0dy6rr2wga5z2h9m69igcsfchlxwnkqm0q9cw0wbm8zaqsspm2rd")))

(define-public crate-jetty-0.2.1 (c (n "jetty") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)))) (h "15ldmib6b57q7la0wls32i06abckamwghhhqqnnys87h1lvjmfw6")))

(define-public crate-jetty-0.3.0 (c (n "jetty") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpu-time") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "1zdsv1prab5ql4f1in79zg3qzpdy1242qvrcqsshprlmqnpsv3bc")))

(define-public crate-jetty-0.3.1 (c (n "jetty") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpu-time") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "1z604kbiby66jssbcj17yswqspni59k96i2qc8266hkc0pfddssg")))

(define-public crate-jetty-0.3.2 (c (n "jetty") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpu-time") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "1h1hqmhi3vbmvmmaxxsxnblh1hrpgan2k45d9rqx8vg9ndjgzfnw")))

(define-public crate-jetty-0.4.0 (c (n "jetty") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpu-time") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "1zkmya74n763dinxi6a230x73skbx8zp36hglmwzlgiwa19315j0")))

(define-public crate-jetty-0.4.1 (c (n "jetty") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cpu-time") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1") (d #t) (k 2)) (d (n "zstd") (r "^0.12") (d #t) (k 2)))) (h "01niqvy88cr6wjgkl5wp6xd49cm60g7nv0c9ib682jqicmw2r5nb")))

