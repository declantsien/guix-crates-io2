(define-module (crates-io je tr jetro) #:use-module (crates-io))

(define-public crate-jetro-0.1.0 (c (n "jetro") (v "0.1.0") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "19n13z3l33v9cjf5rvzj81ymmmdz0j189rh1awgxgh0ag4561q70")))

(define-public crate-jetro-0.2.0 (c (n "jetro") (v "0.2.0") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0vy5dvp9hjp6j0gm9ak7pf8qpm4i38l0h480p2r3nmhxc9k19zdw")))

(define-public crate-jetro-0.2.1 (c (n "jetro") (v "0.2.1") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0f9pdxjrdykyfmjjkfp5dzsz7035j1317ziv8qmpvrai2jqicmc2")))

(define-public crate-jetro-0.2.2 (c (n "jetro") (v "0.2.2") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "12dwvxpryp48xbrzak77wwqhdlv0icjadgwsjphxyibj6fykf87i")))

(define-public crate-jetro-0.2.3 (c (n "jetro") (v "0.2.3") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1slsrxbvm22x647sf47jnl4lwx67dbrxg9ddn1krqk41a3wqj6yq")))

(define-public crate-jetro-0.2.4 (c (n "jetro") (v "0.2.4") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0pbqvkda1wl4a31bv71sra6ch9v2hi2qvv6vxgvkkd02sxrwrpfm")))

(define-public crate-jetro-0.2.5 (c (n "jetro") (v "0.2.5") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "1dr6i5li3kvfxg2v6h9r9lm5na3zzr5pw77lh26k4mslbxkxwxz6")))

