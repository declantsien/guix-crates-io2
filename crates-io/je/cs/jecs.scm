(define-module (crates-io je cs jecs) #:use-module (crates-io))

(define-public crate-jecs-0.1.0 (c (n "jecs") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "00r0qryy1pwiizz0n6i37zbwvdpc34v3rjwdhhgmm686smbqzmbv")))

(define-public crate-jecs-0.1.1 (c (n "jecs") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1mamvhdgalr6lvv6zvldgfk5kagr98y1nb0dbld1652v1swymdc0")))

