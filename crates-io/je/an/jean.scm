(define-module (crates-io je an jean) #:use-module (crates-io))

(define-public crate-jean-0.1.0 (c (n "jean") (v "0.1.0") (d (list (d (n "jean_alignment") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jean_alignment") (r "^0.1.0") (d #t) (k 2)) (d (n "jean_blosum") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jean_blosum") (r "^0.1.0") (d #t) (k 2)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 0)) (d (n "jean_cut") (r "^0.1.0") (d #t) (k 0)) (d (n "jean_io") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "jean_io") (r "^0.1.0") (d #t) (k 2)))) (h "02xfpgdfc1yzmc3ygif17n37lzzj230i0p2816rw6c25d4gg3vlk") (f (quote (("io" "jean_io") ("blosum" "jean_blosum") ("alignment" "jean_alignment"))))))

