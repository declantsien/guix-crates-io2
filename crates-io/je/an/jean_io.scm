(define-module (crates-io je an jean_io) #:use-module (crates-io))

(define-public crate-jean_io-0.1.0 (c (n "jean_io") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 0)))) (h "0nnkwsfyls7p2hmiiclcj0waakvrhpfpg4z2mmb6rd5c2m5ygrq0")))

