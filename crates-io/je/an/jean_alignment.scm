(define-module (crates-io je an jean_alignment) #:use-module (crates-io))

(define-public crate-jean_alignment-0.1.0 (c (n "jean_alignment") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1xiipdisxssk1fadsmm2kzxdsi1wl49xy4hv988bq5lpcbyd65nm")))

