(define-module (crates-io je an jean_blosum) #:use-module (crates-io))

(define-public crate-jean_blosum-0.1.0 (c (n "jean_blosum") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 2)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.0") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.5.0") (d #t) (k 2)) (d (n "pest_derive") (r "^2.5.0") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 2)))) (h "0rldw28wdm53p66y6qsp26rzrx5xsz0w3z1bfhkj1inrdpmzw81a") (f (quote (("codegen" "clap" "pest" "pest_derive" "anyhow"))))))

