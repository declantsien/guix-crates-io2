(define-module (crates-io je an jean_core) #:use-module (crates-io))

(define-public crate-jean_core-0.1.0 (c (n "jean_core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1f9siyi7xf94ykv6fmfysv5g6j3lr6nxlig95nywx4lplhdipvsd")))

