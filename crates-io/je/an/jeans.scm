(define-module (crates-io je an jeans) #:use-module (crates-io))

(define-public crate-jeans-0.1.0 (c (n "jeans") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "15v7vz8i8f7aiajngcgrv6qmvlvgniby6kdwhdpf32279ln9a7kw")))

(define-public crate-jeans-0.1.1 (c (n "jeans") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1lh60sx601j397i134vxzx6fyvs17vk1rhqhylr6qbvd245722pf")))

(define-public crate-jeans-0.1.2 (c (n "jeans") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1qv2gyjv8a3f74ynad3l966pig8fchpbl58c4qa1qlv7k8llfz2v")))

(define-public crate-jeans-0.1.3 (c (n "jeans") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1by0fm08faxdzn0dg9ypbkdd8ggh3nc28hhv577hg737pkiial77")))

(define-public crate-jeans-0.1.4 (c (n "jeans") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zspjkj1shdd90y993862r0g2jwjnnl29zhkdgcy84clfl92qfvn")))

