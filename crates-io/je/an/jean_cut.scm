(define-module (crates-io je an jean_cut) #:use-module (crates-io))

(define-public crate-jean_cut-0.1.0 (c (n "jean_cut") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 0)) (d (n "jean_core") (r "^0.1.0") (d #t) (k 2)) (d (n "jean_io") (r "^0.1.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.5.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.0") (d #t) (k 0)))) (h "10gsf7mzaqjmrldzazic7dkfxbmghk7lx33j5yaz7klsl46ssjka")))

