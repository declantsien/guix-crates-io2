(define-module (crates-io je ll jellybean-tree-sitter) #:use-module (crates-io))

(define-public crate-jellybean-tree-sitter-0.20.12 (c (n "jellybean-tree-sitter") (v "0.20.12") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1acj7msllj42j0r1gsylnkjy31js5gl184wa6aikfx1vcw0spplj") (s 2) (e (quote (("serde" "dep:serde" "serde_regex")))) (r "1.65")))

(define-public crate-jellybean-tree-sitter-0.20.13 (c (n "jellybean-tree-sitter") (v "0.20.13") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "143b6kcg1f2riy1p1pmg7d9cj1dxwqzpgzf7yk0ibk1wggimajx5") (s 2) (e (quote (("serde" "dep:serde" "serde_regex")))) (r "1.65")))

(define-public crate-jellybean-tree-sitter-0.20.14 (c (n "jellybean-tree-sitter") (v "0.20.14") (d (list (d (n "bindgen") (r "^0.66.1") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0kf8g1p57mg2d5c6yj8ryksyn1h19sgx36d7j0vq5qa64yx5bx5q") (s 2) (e (quote (("serde" "dep:serde" "serde_regex")))) (r "1.65")))

