(define-module (crates-io je ll jellyfish) #:use-module (crates-io))

(define-public crate-jellyfish-0.1.0 (c (n "jellyfish") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "10p1z6yck9vv817x5sr20c78jf89dmybh2dj2mn4rc7hlv510p2m")))

(define-public crate-jellyfish-0.2.0 (c (n "jellyfish") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1lzkva8qi7nprq5gg1h4wnim4q1kiw77lazba1jpjlwlz402cfil")))

(define-public crate-jellyfish-0.3.0 (c (n "jellyfish") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "00qs1r39f73ikp6b9s81crm77kwljp245bh4dz2dxzpq5n68xwnk")))

(define-public crate-jellyfish-0.4.0 (c (n "jellyfish") (v "0.4.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1acn2wcv433nszbs6s342nxg4gb2kc4j6i153jswil1fsmm48qa7")))

(define-public crate-jellyfish-0.4.1 (c (n "jellyfish") (v "0.4.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "07an3h0dfrw3wwjhcf18mzn8p0kswpzfdwl108j31i8ijsrhvh0j")))

(define-public crate-jellyfish-0.11.0-alpha.4 (c (n "jellyfish") (v "0.11.0-alpha.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.18.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "07cirzhcawx53bkw2mf5cs6y1xhrahv9h6ksd3r07rf1902l88qm") (f (quote (("python"))))))

(define-public crate-jellyfish-1.0.0 (c (n "jellyfish") (v "1.0.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.18.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0g8yvjrzfxr64453b64y7n116mwqa386bj0h16cylbsbd3vzih7n") (f (quote (("python"))))))

