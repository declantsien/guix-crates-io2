(define-module (crates-io je ll jellyfin-sdk-rust) #:use-module (crates-io))

(define-public crate-jellyfin-sdk-rust-0.1.0 (c (n "jellyfin-sdk-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("native-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.68") (k 0)) (d (n "thiserror") (r "^1.0.29") (k 0)))) (h "1j2n71cgylqj2l5lw5gf2k6mncjd02ryblzrzgbj7sj37x0zch5d") (f (quote (("sync" "reqwest/blocking") ("default" "async") ("async"))))))

(define-public crate-jellyfin-sdk-rust-0.1.1 (c (n "jellyfin-sdk-rust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("native-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.68") (k 0)) (d (n "thiserror") (r "^1.0.29") (k 0)) (d (n "url") (r "^2.2.2") (k 0)))) (h "01pph319np0797xdi2yyaw06dr9ypms230d0zvfhm2gm7j20ig26") (f (quote (("sync" "reqwest/blocking") ("headers") ("default" "async") ("async"))))))

(define-public crate-jellyfin-sdk-rust-0.1.2 (c (n "jellyfin-sdk-rust") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("native-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.68") (k 0)) (d (n "thiserror") (r "^1.0.29") (k 0)) (d (n "url") (r "^2.2.2") (k 0)))) (h "1rssl7m1hrvfc4j3hy8750grg508vrpb0gvcvmrn4d03m419yjs3") (f (quote (("sync" "reqwest/blocking") ("headers") ("default" "async") ("async"))))))

