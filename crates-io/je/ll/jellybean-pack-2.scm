(define-module (crates-io je ll jellybean-pack-2) #:use-module (crates-io))

(define-public crate-jellybean-pack-2-0.0.2 (c (n "jellybean-pack-2") (v "0.0.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "rayon") (r "^1.7") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (d #t) (k 1)))) (h "1dbhwi3vbg6sn24h59dw18jyvn7rm9r9n56q5x8zc2avxm90c0c7") (f (quote (("zig") ("yuck") ("yang") ("xml") ("wgsl") ("vim") ("vhs") ("vala") ("default" "vim" "yuck" "yang" "vala" "xml" "vhs" "wgsl" "zig"))))))

