(define-module (crates-io je rk jerk-build) #:use-module (crates-io))

(define-public crate-jerk-build-0.1.0 (c (n "jerk-build") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "1pa6a8ri1hiign59n4rh7djxramfm7mw9860av0ms010dg8x0h3d") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-build-0.1.1 (c (n "jerk-build") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "10wsiizkxd8pcmxahk2g5iqyg5qz8nv6dr5sbg558y48k15mwh2n") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-build-0.1.2 (c (n "jerk-build") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)))) (h "1l9fld2zgb0gybfwbai5xc9n1f62q0d9khy4kmpn7pxcr9pmxhkc") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-build-0.1.3 (c (n "jerk-build") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "jerk") (r "^0.1.3") (d #t) (k 0)))) (h "0vfb7p57r3d2b8fwavh2rhb7db2qhlbwq12hh1hn4wm6ll1ckh6d") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-build-0.1.4 (c (n "jerk-build") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "jerk") (r "^0.1.4") (d #t) (k 0)))) (h "041zsb94dnahdda2lzvqsdm5vf89wbvaxcjf2afkqm9q8ckw53cm") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-build-0.1.5 (c (n "jerk-build") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "jerk") (r "^0.1.5") (d #t) (k 0)))) (h "1cx994yqgnkwggbvw8sbr5a7kq9d7b2v0v50n1679xs4pf1sdlrs") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.1.6 (c (n "jerk-build") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "jerk") (r "^0.1.6") (d #t) (k 0)))) (h "0qqybc8g8z7yj3x4l3cfkf8y35k43f0cg1wryqcw1bmcvb9yymfk") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.2.0 (c (n "jerk-build") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "jerk") (r "^0.2.0") (d #t) (k 0)))) (h "0jq2mayqnbl1n97zyycl46pq6nrgbkn5vj6slfvw7nzasa0phks5") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.2.1 (c (n "jerk-build") (v "0.2.1") (d (list (d (n "jerk") (r "^0.2.1") (d #t) (k 0)))) (h "090rjndgacranck6h5r1anfhmbx95b490k01iplqhczg9z6jhd85") (f (quote (("nightly" "jerk/nightly") ("default"))))))

