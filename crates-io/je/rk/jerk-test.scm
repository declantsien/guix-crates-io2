(define-module (crates-io je rk jerk-test) #:use-module (crates-io))

(define-public crate-jerk-test-0.1.0 (c (n "jerk-test") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0j33dsm40zqnq1v8c7s86ywqdc7hs6x1dqg66mm099rdkr80ijjf") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-test-0.1.1 (c (n "jerk-test") (v "0.1.1") (d (list (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "09g8hqdb588n20zarh6bbsc2dr8a5d067dd26yk2ab44w217srig") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-test-0.1.2 (c (n "jerk-test") (v "0.1.2") (d (list (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "06jnjdxb2x7i8wjxgwr7nvliyi3i75fj5r9jaxib7kjzlqkhq1g1") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-test-0.1.3 (c (n "jerk-test") (v "0.1.3") (d (list (d (n "jerk") (r "^0.1.3") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "05vf2r4ma8dxmc0whrlawbl656g7m0w8lf4di3i14y0h244nicr3") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-test-0.1.4 (c (n "jerk-test") (v "0.1.4") (d (list (d (n "jerk") (r "^0.1.4") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0n9w9z7synqzdklwk0kzylmfh3sqy4k2gm32ihm8hrah7dhshpsp") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-test-0.1.5 (c (n "jerk-test") (v "0.1.5") (d (list (d (n "jerk") (r "^0.1.5") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "06p9bc5j905zp9vwf3gkrbf0i9bsygr8iprviwnz74frr7rp3i4i") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.1.6 (c (n "jerk-test") (v "0.1.6") (d (list (d (n "jerk") (r "^0.1.6") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0zxa12sq2raj15spgxkb1rrjgy51nhzz1fr31zksffb9pswy5jsd") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.2.0 (c (n "jerk-test") (v "0.2.0") (d (list (d (n "jerk") (r "^0.2.0") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0j605i31hhsbx2a016mkacr7cdqabcg7zibx0724x8b23pahy8bd") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.2.1 (c (n "jerk-test") (v "0.2.1") (d (list (d (n "jerk") (r "^0.2.1") (d #t) (k 0)))) (h "1v2g3gyyadrjn2rfph16kpv2wv0hfy8p2wr2hhkpn9pfvn80q711") (f (quote (("nightly" "jerk/nightly") ("default"))))))

