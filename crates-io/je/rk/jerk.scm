(define-module (crates-io je rk jerk) #:use-module (crates-io))

(define-public crate-jerk-0.0.0 (c (n "jerk") (v "0.0.0") (h "1p0pqswpaa9fg1mrhbqzm0x779mf51wyq3d5vyjypd6l79v356ip") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-0.0.1 (c (n "jerk") (v "0.0.1") (h "0vlcpmj2na0nr9l3vsf0mrn63viv2m9rf2dnnsjqkm31f966caib") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-0.1.3 (c (n "jerk") (v "0.1.3") (d (list (d (n "dlopen") (r "^0.1") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)))) (h "0wjgv514ljwfsx5jsdq9bkgda3hbmvmw0l25w1g2xdq4hpkfapcy") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-0.1.4 (c (n "jerk") (v "0.1.4") (d (list (d (n "dlopen") (r "^0.1") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)))) (h "11lfr3a0alga9gm8w74mbxbb3375rsxhvl3xmdr31bi0i9cib88c") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-jerk-0.1.5 (c (n "jerk") (v "0.1.5") (d (list (d (n "dlopen") (r "^0.1") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)))) (h "19vq8s48b8f47pjpzh0g1x3h13bvas0jdn5ywz510gk603kfvbgd") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.1.6 (c (n "jerk") (v "0.1.6") (d (list (d (n "dlopen") (r "^0.1") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winerror"))) (d #t) (k 0)))) (h "002v32bb2dd9dkmrxc0083gphi3f6v2klcljazr3qfq4d528l725") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2.0 (c (n "jerk") (v "0.2.0") (d (list (d (n "dlopen") (r "^0.1") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winerror"))) (d #t) (k 0)))) (h "0zzjy55cakwlcvy6rjvgn1c2m2fb1xvsc5hyaz6qjxikk27glsaf") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2.1 (c (n "jerk") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "1pqy6mdfh8nhfjpyx3bcs37lv54s43qzm484sbxzdfbhmkdv5shc") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2.2 (c (n "jerk") (v "0.2.2") (d (list (d (n "bitflags") (r "=1.3") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "042spfwrdcidq5h7xmb9966rrr4dcmnl70zmxsg1gan4khzyk4dg") (f (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2.3 (c (n "jerk") (v "0.2.3") (d (list (d (n "bitflags") (r "=1.3") (d #t) (k 0)) (d (n "jni-sys") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (t "cfg(unix)") (k 0)))) (h "1ibmzpiv7cjzipk0sjhycx12cv30c3i3wl96wwj9z78rghplm2l8") (f (quote (("nightly") ("default"))))))

