(define-module (crates-io je ky jekyll-to-gatsby) #:use-module (crates-io))

(define-public crate-jekyll-to-gatsby-0.1.0 (c (n "jekyll-to-gatsby") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "07p5m92mz1yklk2w0wdc9f06dl1yh64saip8m1zy5biqyvkhwiir")))

(define-public crate-jekyll-to-gatsby-0.2.0 (c (n "jekyll-to-gatsby") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "164sym28dk3c2ihmi8ishns4zqm09m3xkxqsgyd6h9ghmckp79c9")))

(define-public crate-jekyll-to-gatsby-0.3.0 (c (n "jekyll-to-gatsby") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1zs50v8b2wf52m6n48yjcbz47l3j1jf5snf38iz5xfgfbz9kfyrm")))

(define-public crate-jekyll-to-gatsby-0.4.0 (c (n "jekyll-to-gatsby") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1mipc1n67pzzbxlwvl0nl2q32lyh9q0kb69ch9q6qhxm0d3ps0j3")))

