(define-module (crates-io je nk jenkins_hash) #:use-module (crates-io))

(define-public crate-jenkins_hash-0.1.0 (c (n "jenkins_hash") (v "0.1.0") (h "1x2gmhfvbbff79lm0gdkx0kc3w9n0ykdgpk0wdwhv1f314f5mzkh")))

(define-public crate-jenkins_hash-0.2.0 (c (n "jenkins_hash") (v "0.2.0") (h "17s4xdqvp3cbrb8k1rnd55x5z4v630ddnymfl49ax4mdvgr6ywsi")))

