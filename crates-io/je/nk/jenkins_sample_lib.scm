(define-module (crates-io je nk jenkins_sample_lib) #:use-module (crates-io))

(define-public crate-jenkins_sample_lib-0.0.1 (c (n "jenkins_sample_lib") (v "0.0.1") (h "1g01ja31shh35ncpcmri3rs1fcww7ip0nx3sb7gjks1l1aiwni25")))

(define-public crate-jenkins_sample_lib-0.0.2 (c (n "jenkins_sample_lib") (v "0.0.2") (h "0xaz15izqyfdc7f2wlpc7lkk1b1qrk7ba3azzliwlbhdimp4011c")))

(define-public crate-jenkins_sample_lib-0.0.3 (c (n "jenkins_sample_lib") (v "0.0.3") (h "1q01j48lxjbvcksphh3g2l75x1l6mr1z2dz3270xc13zh89nsy0g")))

