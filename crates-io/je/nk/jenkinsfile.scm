(define-module (crates-io je nk jenkinsfile) #:use-module (crates-io))

(define-public crate-jenkinsfile-0.1.0 (c (n "jenkinsfile") (v "0.1.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xq8nc7gb12i5qpnviz0dlcbf0llsx5kblnxr8svb76r757xrd0c")))

(define-public crate-jenkinsfile-0.1.1 (c (n "jenkinsfile") (v "0.1.1") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "173maq46sx7wiw43lr55d5jg6nr3l7pf01m3yb8ql1cphsx0yn9z")))

(define-public crate-jenkinsfile-0.2.0 (c (n "jenkinsfile") (v "0.2.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11m3n1pi7iy0gpjhipgvsviy1kddnnp4yhc47n83w7paw44bvsv5")))

(define-public crate-jenkinsfile-0.2.1 (c (n "jenkinsfile") (v "0.2.1") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "00hb997dwbsm8m96vkmqpdrfvpg6kijnymdmdj28657pq9d3c4qh")))

