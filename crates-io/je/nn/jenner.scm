(define-module (crates-io je nn jenner) #:use-module (crates-io))

(define-public crate-jenner-0.1.0 (c (n "jenner") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "16yp92zldwaqjlqw3nrpscjh7rczwvsr1ch36bnwhzi71h0vc5zw")))

(define-public crate-jenner-0.1.1 (c (n "jenner") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0w812qggfl2rfv137xg99ha54hii3knj8504vc3bnxbvvjk5bp2c")))

(define-public crate-jenner-0.1.2 (c (n "jenner") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.2") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wjshs9qdjn6bxnas2hng0xlal2yhsx4iiyflzfpyrx4g3cbm09p")))

(define-public crate-jenner-0.1.3 (c (n "jenner") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11jb4mf27ghcmqyiyscg6kkn40zpl34asdkwhncja5qc90dg46pb")))

(define-public crate-jenner-0.1.4 (c (n "jenner") (v "0.1.4") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.4") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1qlgsvfzgy1bdgjk26csv2b9sqr265myw7idajdc7x9kvnrhmvni")))

(define-public crate-jenner-0.1.5 (c (n "jenner") (v "0.1.5") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "jenner-macro") (r "=0.1.5") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0pl21bkk19pzi4pppcsh6jl63ggzn0ifaj59sd5mr6p7h1c7f2hf")))

(define-public crate-jenner-0.2.0 (c (n "jenner") (v "0.2.0") (d (list (d (n "jenner-macro") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11vyydslb3ipzjq57pgqzalcah58diq620iwayczmcnxfcz9prry")))

