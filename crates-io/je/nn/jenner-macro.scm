(define-module (crates-io je nn jenner-macro) #:use-module (crates-io))

(define-public crate-jenner-macro-0.1.0 (c (n "jenner-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing"))) (k 0)))) (h "107px0ffdvx9nx3dsy54ka0z4gid4mspdsqribxfvn7kalj80jca")))

(define-public crate-jenner-macro-0.1.1 (c (n "jenner-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls"))) (k 0)))) (h "0c5p5lvc9ba4m5s0amkx6wpzhrr85v72w679nmahkiwc4y3i7h0w")))

(define-public crate-jenner-macro-0.1.2 (c (n "jenner-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls"))) (k 0)))) (h "1x9ni5nh8y6mr7r6p83snyvsmm8k3k18xi6jzy9lx7zq1aj5xgfp")))

(define-public crate-jenner-macro-0.1.3 (c (n "jenner-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls"))) (k 0)))) (h "118z8vbdpzfanbax1wi62dqkr3hgv1zm68f887xzk1wrj33lm9l5")))

(define-public crate-jenner-macro-0.1.4 (c (n "jenner-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls"))) (k 0)))) (h "019zgxsikcff6kw6rj183fx4srjcvjr5qmjwax8g5qv3zinhjr22")))

(define-public crate-jenner-macro-0.1.5 (c (n "jenner-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls" "proc-macro"))) (k 0)))) (h "10i8v09dl0vharchykaj86awpymcxfmhynznhl1bm8k3fmii05d8")))

(define-public crate-jenner-macro-0.2.0 (c (n "jenner-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "visit-mut" "printing" "clone-impls" "proc-macro"))) (k 0)))) (h "0jr8f7bh29fgi9y7mha2i6jnbfjjhx6bmghhig1y34blzcmgxm6k")))

