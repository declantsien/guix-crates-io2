(define-module (crates-io je st jest_lint) #:use-module (crates-io))

(define-public crate-jest_lint-0.1.0 (c (n "jest_lint") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "12l30lqiplr1wfpy2ckcxsw72qlny6y7fqnfbdakbq2rqzr4w6r0")))

(define-public crate-jest_lint-0.1.1 (c (n "jest_lint") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1j1r4xp1zbw0iz46fmcn7i970wyk592j8z3i2r95xnlk4lmlqq7k")))

(define-public crate-jest_lint-0.1.2 (c (n "jest_lint") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "049i6zp3jn8lh1ga54arsig318pplnhvbbd2jp8nng5aw1p31ajh")))

(define-public crate-jest_lint-0.1.3 (c (n "jest_lint") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "1n2nmsgq729qgw0l7k03nqsabah3j0i75762qhxhmh3sdxrmmcfd")))

