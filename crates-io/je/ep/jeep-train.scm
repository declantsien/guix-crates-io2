(define-module (crates-io je ep jeep-train) #:use-module (crates-io))

(define-public crate-jeep-train-0.1.0 (c (n "jeep-train") (v "0.1.0") (d (list (d (n "jeep-train-macro") (r "^0.1") (d #t) (k 0)) (d (n "jeep-train-prelude") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "16jcmvf7hlwhzyzjhj9wf23il5gxj8lsakl9wfydrky7xqxjnzyy")))

