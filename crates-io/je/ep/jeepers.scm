(define-module (crates-io je ep jeepers) #:use-module (crates-io))

(define-public crate-jeepers-0.1.0 (c (n "jeepers") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1v3zqldmcp3p4dsa9xnqpngav9vrky0yas1z5fpyi14kpn1hnijf") (y #t)))

(define-public crate-jeepers-0.1.1 (c (n "jeepers") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.112") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0fv9j7qh1dbkd2qbb6jwdyr941c54yxn18jrz7rddpr3khcrp94q") (f (quote (("dev" "clippy") ("default")))) (y #t)))

