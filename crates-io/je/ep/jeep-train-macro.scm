(define-module (crates-io je ep jeep-train-macro) #:use-module (crates-io))

(define-public crate-jeep-train-macro-0.1.0 (c (n "jeep-train-macro") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "jeep-train-prelude") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jfcqm9d1af8shvqdxb3m4hk6ci7cryk2rvl3w7n0jbzx7zkw0qc")))

