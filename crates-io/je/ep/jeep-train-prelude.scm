(define-module (crates-io je ep jeep-train-prelude) #:use-module (crates-io))

(define-public crate-jeep-train-prelude-0.1.0 (c (n "jeep-train-prelude") (v "0.1.0") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.6.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0") (d #t) (k 0)))) (h "1yi09lcbadjssxlwjg13bqmc2q1vvbn880dyk8rm6n5fq3s292k1")))

