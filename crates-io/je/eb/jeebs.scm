(define-module (crates-io je eb jeebs) #:use-module (crates-io))

(define-public crate-Jeebs-0.1.0 (c (n "Jeebs") (v "0.1.0") (h "0a4vqv3ksiglg7q2a3mabrnd2qlnj23rpgy1vl1hhgbv12gxy2vm") (y #t)))

(define-public crate-Jeebs-0.1.0-beta.23120801 (c (n "Jeebs") (v "0.1.0-beta.23120801") (h "1gvkpj7f4knvwx48sac6zlandgiii4hx22c627zhb67k3y8x9lzm") (y #t)))

