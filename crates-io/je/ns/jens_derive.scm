(define-module (crates-io je ns jens_derive) #:use-module (crates-io))

(define-public crate-jens_derive-0.6.0 (c (n "jens_derive") (v "0.6.0") (d (list (d (n "jens") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "1s71hh4qff9y2infl1mn7gqxkn5bansp1gm33qq6494bzvcfb15k")))

