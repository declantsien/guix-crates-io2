(define-module (crates-io na sl naslint) #:use-module (crates-io))

(define-public crate-naslint-0.0.1 (c (n "naslint") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1vxy66chvhhgc2rslhmqkhlj2isqiia2d6xi1j3kzj1s202c1b0l")))

(define-public crate-naslint-0.0.11 (c (n "naslint") (v "0.0.11") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0k4h47jrvzqkyn1skkg0jn2iiylh8lxvqq5v49s9dpdhm9xnbnwg")))

(define-public crate-naslint-0.0.12 (c (n "naslint") (v "0.0.12") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1cs7ykxmnlv9wacvf3r0cgdqjkl64p9z86laa7spy7wnqdirkr93")))

(define-public crate-naslint-0.0.13 (c (n "naslint") (v "0.0.13") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0k5bcrvzswgqzd6vrlzqc4lg9x4dapkzis675ghhw5rzrcs1zr46")))

(define-public crate-naslint-0.0.2 (c (n "naslint") (v "0.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0wqrarrnwhh991z1rqzspv9pxs8844fam4h31m30bxhk3adassfc")))

(define-public crate-naslint-0.0.21 (c (n "naslint") (v "0.0.21") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0gz080y0jlfr98aygxam05p0p9fw6hsnz5zn5kv8xx7shf50kylb")))

(define-public crate-naslint-0.0.22 (c (n "naslint") (v "0.0.22") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "promptly") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.123") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1yw4r1sqdr210g0a5kjxwjgbniks1ifjs321jcp0ckawx8609v6n")))

