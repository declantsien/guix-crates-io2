(define-module (crates-io na nb nanbox) #:use-module (crates-io))

(define-public crate-nanbox-0.1.0 (c (n "nanbox") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "0dayavngpmz4y6cwkphfgiqxjxlln1zcpnb4b9w0pmzmknmlq4mx")))

(define-public crate-nanbox-0.2.0 (c (n "nanbox") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)) (d (n "unreachable") (r "^0.1.1") (d #t) (k 0)))) (h "0kc6vzcb55v2bdhb9maxs8r9wngfrxvw3s8a74chlbb1v660xwm3")))

