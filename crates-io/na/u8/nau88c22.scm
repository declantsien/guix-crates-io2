(define-module (crates-io na u8 nau88c22) #:use-module (crates-io))

(define-public crate-nau88c22-0.9.0 (c (n "nau88c22") (v "0.9.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "1lf6y9arcddbzlmibbg3k2rvb673m7y424qn6f6rj3sygw9mkp1v") (s 2) (e (quote (("defmt" "dep:defmt"))))))

