(define-module (crates-io na cl nacl) #:use-module (crates-io))

(define-public crate-nacl-0.1.0 (c (n "nacl") (v "0.1.0") (h "081i076i6vj74m7kkz45gbrk86nymfm7izhr2lf6yxx3lc49jrsq")))

(define-public crate-nacl-0.2.0 (c (n "nacl") (v "0.2.0") (h "1q3c42nww01am0jjq1fwkai4zn8104dab7m0z6b1l9m308c66qb1")))

(define-public crate-nacl-0.3.0 (c (n "nacl") (v "0.3.0") (h "12axbhlrwmdsp0sw9rvsj18isx9vm9sypm9bmqpr0p4k7yf8a40n")))

(define-public crate-nacl-0.4.0 (c (n "nacl") (v "0.4.0") (h "1f2la5ic370dhsbg9jakfr62d5bw5lvfayb6xqpv3wxp3z1w1hrv")))

(define-public crate-nacl-0.5.0 (c (n "nacl") (v "0.5.0") (h "1b0gfcr6pi3lyp6yb826sahcaicf3vf801mych20gh9ipz3877n9")))

(define-public crate-nacl-0.5.1 (c (n "nacl") (v "0.5.1") (h "1av0z35i00gvppwnlwgz4cgr1m9z9yd7vz58d7fl32kyjilgs51d")))

(define-public crate-nacl-0.5.2 (c (n "nacl") (v "0.5.2") (h "0v5d4hdsbx4rzsyq5pkahb29hqmsv0zp0l4wbq4jmfkydzsgf341")))

(define-public crate-nacl-0.5.3 (c (n "nacl") (v "0.5.3") (h "0j6m4d9y00d7h613rmkl68d0wbkzq63hx5ajg5g1pi8kv12grbih")))

