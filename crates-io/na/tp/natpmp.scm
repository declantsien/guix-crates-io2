(define-module (crates-io na tp natpmp) #:use-module (crates-io))

(define-public crate-natpmp-0.1.5 (c (n "natpmp") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15s38bhliycc1xsf2za5c997864g084nn457nnik6ns8yvqy83dy")))

(define-public crate-natpmp-0.1.10 (c (n "natpmp") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12hglx4zs859spciyzpqpipkdakc2z6w7mp6r5k3a5zrnkz71xlg")))

(define-public crate-natpmp-0.1.11 (c (n "natpmp") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "128gbsdxzgpwdd7xpqgl2q40n8sdb845m3yfhs2pnfn7aa1f3m8l")))

(define-public crate-natpmp-0.1.12 (c (n "natpmp") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "087pni8lnvf7shprzfkppfwg6j5sqvvzbndmyirnl22rhqxy9ili")))

(define-public crate-natpmp-0.1.13 (c (n "natpmp") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vcc2xv7cis2zjw0jyvl0g9bivqbvs3r9qfmy4556c89lkj3vklj")))

(define-public crate-natpmp-0.1.14 (c (n "natpmp") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11yn49r7iylciaaapgrlb6nyzv222182fr5ari3d31n7mfp0rxcj")))

(define-public crate-natpmp-0.1.15 (c (n "natpmp") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "059ycq1b0bj548zqdzkgzabv0brld9rx7jlxfr3drz40nydmms90")))

(define-public crate-natpmp-0.2.0 (c (n "natpmp") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04kaz1dw0r8jn39s87212jkf7v9z3pi2izmndarbislmgn8p8nyq")))

(define-public crate-natpmp-0.3.0 (c (n "natpmp") (v "0.3.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "1d7i4fip6346lx7lr9zq80rc5wq1zq0lfyswzcf24badjnb9mg0l") (f (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3.1 (c (n "natpmp") (v "0.3.1") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)))) (h "175nmwlcmbjccrs6aa9bmdd5k198jpl4bvvk4frgw2m0f6l3avcz") (f (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3.2 (c (n "natpmp") (v "0.3.2") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1bbn56k57x2lc0scjxy2dca48m5lim5pcpp8qn23k87r6fxyzshr") (f (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3.3 (c (n "natpmp") (v "0.3.3") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0rd3x5aj2vvbg9sak1ghrr633q6iap8kh19z8w45f6hqjw8qsdz1") (f (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.4.0 (c (n "natpmp") (v "0.4.0") (d (list (d (n "async-std") (r "^1") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11x7bx6j61zf8dzkag5gnrlm4g28klwlvik0h20grvg1qmfb6r5g") (f (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

