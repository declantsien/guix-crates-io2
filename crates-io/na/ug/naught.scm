(define-module (crates-io na ug naught) #:use-module (crates-io))

(define-public crate-naught-0.1.0-dev (c (n "naught") (v "0.1.0-dev") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "hyper") (r "^0.12.24") (d #t) (k 0)) (d (n "jch") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio-sync") (r "^0.1.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1.2") (d #t) (k 0)))) (h "0jk155lw67nz1d8y12r4w235kr97rm2c99q7aqj54n6965kx5968")))

