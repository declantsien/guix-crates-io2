(define-module (crates-io na ug naughty-strings) #:use-module (crates-io))

(define-public crate-naughty-strings-0.1.0 (c (n "naughty-strings") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "17qfsrwr3ga8l0s9x3xcsya1vimlq9610x11jl9bvjkdxkqll00l") (y #t)))

(define-public crate-naughty-strings-0.2.0 (c (n "naughty-strings") (v "0.2.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wh0kwl4jwpzycx6sjkmrfqddanandxin6x0nabbaxj2cr568kwh")))

(define-public crate-naughty-strings-0.2.1 (c (n "naughty-strings") (v "0.2.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "12cxljf0gwjd9j3n99i6iycz3djxmi62ikvayslv2zwqdm84plpg")))

(define-public crate-naughty-strings-0.2.2 (c (n "naughty-strings") (v "0.2.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "079i5h43236j61qgiwmfiw6ml9z7zm9l6azxfac6wd3ihrcmviqb")))

(define-public crate-naughty-strings-0.2.3 (c (n "naughty-strings") (v "0.2.3") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "131w22gnzigq06pc2saf5mpgffdphpxapf9xfldqh22qvwg1kb3d")))

(define-public crate-naughty-strings-0.2.4 (c (n "naughty-strings") (v "0.2.4") (d (list (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03srmajmasqzcg2cz0n1bbvvsjdiv776lab9lm9l8d4i3w10pxkp")))

