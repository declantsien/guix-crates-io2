(define-module (crates-io na ug naughtyfy) #:use-module (crates-io))

(define-public crate-naughtyfy-0.1.0 (c (n "naughtyfy") (v "0.1.0") (h "1idhfdq5bxzyy64fhab3i03jcipwzimwq7jv3xviaphjyal6xjp9") (y #t)))

(define-public crate-naughtyfy-0.0.1 (c (n "naughtyfy") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0nspsaflyn3icccwqd4qsf1a53afallyy0fhj5w42dqc1cfiw14p")))

(define-public crate-naughtyfy-0.0.2 (c (n "naughtyfy") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "03ipshi2khh6qp523n4lx1j5qfywh9rz1z0i59hq8dd7j8c4f412")))

(define-public crate-naughtyfy-0.0.3 (c (n "naughtyfy") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1hrkbxf51byvxnz04c6v6d36i1k9ykwpwlbbr259wxwkwzkq248v")))

(define-public crate-naughtyfy-0.0.4 (c (n "naughtyfy") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1gi27lqi1bh4ci370y13kmj3syvk6zk5frmjsq3hdpzwg9sw7fz5")))

(define-public crate-naughtyfy-0.0.5 (c (n "naughtyfy") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0yj9r0n3iqs9rpca8s3ijsssk13v3958x6ny53wqx8c2b3z55hgc")))

(define-public crate-naughtyfy-0.0.6 (c (n "naughtyfy") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0kyjgrr0988y517s7j75dc9875x25vcmvwpjz34x74di6grf27n8")))

(define-public crate-naughtyfy-0.0.7 (c (n "naughtyfy") (v "0.0.7") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1ya09mr3cb04zycssnjsc0ra1zjzbsmbdc8rzl6yzdxpa3y9sqk5")))

(define-public crate-naughtyfy-0.0.8 (c (n "naughtyfy") (v "0.0.8") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "11361nvmpwwr1qgnfaxks82jm58g91wi57m2a1mf1pv1x6avqbyv")))

(define-public crate-naughtyfy-0.0.9 (c (n "naughtyfy") (v "0.0.9") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "071dd9awxglzn27dmg84flks735mp2ih9ga0ww9vzcgrvsa96fvl")))

(define-public crate-naughtyfy-0.0.10 (c (n "naughtyfy") (v "0.0.10") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "12wjg2gcv6nbz7p6qj4mdwa3cjll73j8x7q45xik9pwq20xhdap0")))

(define-public crate-naughtyfy-0.1.1 (c (n "naughtyfy") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1izjwf8fhzxrqizc42sgqf0paph7qvfn16x8s8b942fmy4q5gcsd")))

(define-public crate-naughtyfy-0.2.0 (c (n "naughtyfy") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1b3rcga8xky4vlbh92h76q9sa10cng5dcj54jqx41skfz5yn99iz")))

(define-public crate-naughtyfy-0.2.1 (c (n "naughtyfy") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)))) (h "0w91dvsy9mwnwwhb032v0rajs5kiprgjj9fggcmdlkkxdqbhll6k")))

