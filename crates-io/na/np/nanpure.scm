(define-module (crates-io na np nanpure) #:use-module (crates-io))

(define-public crate-nanpure-0.1.2 (c (n "nanpure") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "legion") (r "^0.4") (d #t) (k 0)) (d (n "rscenes") (r "^1.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1jzgz6h939xzw0sxs2s45jxrxxsp84kqxq8v4d4fav0sh1x1p26m")))

(define-public crate-nanpure-0.1.3 (c (n "nanpure") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "legion") (r "^0.4") (d #t) (k 0)) (d (n "rscenes") (r "^1.1") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1ff0sarwgrfb6lp5gv3508fy99j81b58f3azhk3cg6rlnxshsmi1")))

(define-public crate-nanpure-0.1.4 (c (n "nanpure") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "legion") (r "^0.4") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1ymq312g0fkkwhslb274swb8jymv8sg51ygzm9d02zbzpgn18r51")))

(define-public crate-nanpure-0.1.5 (c (n "nanpure") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0ff105b7wqsnnih292mhsmqiphp0p37icflhsynm2ygwsl88hznd")))

(define-public crate-nanpure-0.1.6 (c (n "nanpure") (v "0.1.6") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("ecs" "eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "145qnnl25wixjlhbbwh62ax8pp89v791vb4bkia046bki2n05pms")))

(define-public crate-nanpure-0.1.7 (c (n "nanpure") (v "0.1.7") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("ecs" "eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "00vi85kzyj679pmqdgh5wzm6xxj9p4i294ww55s1p13fnrs2ascm")))

(define-public crate-nanpure-0.2.0 (c (n "nanpure") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "raylib-ffi") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "0id0n2ps6z8p6ykjyy1pynzfpyx7mi7h7s2l0s955bsfid7kghxk")))

(define-public crate-nanpure-0.2.1 (c (n "nanpure") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "raylib-ffi") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "02k7h66hh97rddyjfbynql6z53xkjpqjb8iv1l1y7hb96ca1hps9")))

(define-public crate-nanpure-0.2.2 (c (n "nanpure") (v "0.2.2") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "raylib-ffi") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5") (d #t) (k 0)))) (h "1pk23drsdyvjg2g90dr2n1kzcyn8w0swmwvjrrfa1pdzsjm3g94i")))

