(define-module (crates-io na ry nary_tree) #:use-module (crates-io))

(define-public crate-nary_tree-0.4.1 (c (n "nary_tree") (v "0.4.1") (d (list (d (n "slab") (r "~0.4") (d #t) (k 0)) (d (n "snowflake") (r "~1.3") (d #t) (k 0)))) (h "1n8gzwbm6h9qwixswgvc0vdms0yfw1g58gvv17fhqsb02hkpjyxw") (f (quote (("experimental")))) (y #t)))

(define-public crate-nary_tree-0.4.2 (c (n "nary_tree") (v "0.4.2") (d (list (d (n "slab") (r "~0.4") (d #t) (k 0)) (d (n "snowflake") (r "~1.3") (d #t) (k 0)))) (h "1xm9hz8mizfhp5xnyh647jk96ysyi56lxzm82nh7p4adww93jr33") (f (quote (("experimental"))))))

(define-public crate-nary_tree-0.4.3 (c (n "nary_tree") (v "0.4.3") (d (list (d (n "slab") (r "~0.4") (d #t) (k 0)) (d (n "snowflake") (r "~1.3") (d #t) (k 0)))) (h "1iqray1a716995l9mmvz5sfqrwg9a235bvrkpcn8bcqwjnwfv1pv") (f (quote (("experimental"))))))

