(define-module (crates-io na ca nacafoil) #:use-module (crates-io))

(define-public crate-nacafoil-0.0.1 (c (n "nacafoil") (v "0.0.1") (h "09bkp0x07l8algjgw6m6gjp87zrd3g4x7gydz0215p4zl7g2sv6v")))

(define-public crate-nacafoil-0.0.2 (c (n "nacafoil") (v "0.0.2") (h "012dwbagfcsyxh3ibd9pjx6bp7ymlx0s11v0n1afdc3jgbndkv8z")))

(define-public crate-nacafoil-0.0.3 (c (n "nacafoil") (v "0.0.3") (h "1qzxlf00jxhb370ms0nh9xygrj43n0gnw9nxj3hksqhpihz7m4nb")))

(define-public crate-nacafoil-0.0.4 (c (n "nacafoil") (v "0.0.4") (h "05005c82rm6asg26i44k9q8z9f5x4mfjy10fj7fxjiy59awrs50f")))

(define-public crate-nacafoil-0.0.5 (c (n "nacafoil") (v "0.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0rvka0xrykba40kr9hf5bw86fza41n6xnavkdyrbdmb5x08vjh0p")))

