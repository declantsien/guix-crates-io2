(define-module (crates-io na to natom-macros) #:use-module (crates-io))

(define-public crate-natom-macros-0.1.0 (c (n "natom-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "072ni5v12bvy8i43s1fsxxfjrnp5iqhk17d6zpfyhm8f305vr995") (r "1.69")))

(define-public crate-natom-macros-0.1.1-rc.1 (c (n "natom-macros") (v "0.1.1-rc.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17pfs19p9b9gh2mwcn1w9fnssl2lkk7k52bvk234g8pmi1dzckri") (r "1.69")))

(define-public crate-natom-macros-0.1.1-rc.2 (c (n "natom-macros") (v "0.1.1-rc.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15pqyqrpl57y8rlvdd4sinh3pyigd4257svxcj3gjk5f4syac424") (r "1.69")))

