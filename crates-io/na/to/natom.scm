(define-module (crates-io na to natom) #:use-module (crates-io))

(define-public crate-natom-0.1.0 (c (n "natom") (v "0.1.0") (d (list (d (n "ctor") (r "^0.2.2") (d #t) (k 0)) (d (n "natom-macros") (r "^0.1.0") (d #t) (k 0)))) (h "18wjmj4xp9wax9sxbyx5px9ahsrin494adyaljkv4nh3vl8jd894") (r "1.69")))

(define-public crate-natom-0.1.1-rc.1 (c (n "natom") (v "0.1.1-rc.1") (d (list (d (n "ctor") (r "^0.2.2") (d #t) (k 0)) (d (n "natom-macros") (r "^0.1.1-rc.1") (d #t) (k 0)))) (h "1bxqmvk788d1vvz58183xiz53wx4yv6qa6qh6b7hw9vw933xbll9") (r "1.69")))

(define-public crate-natom-0.1.1-rc.2 (c (n "natom") (v "0.1.1-rc.2") (d (list (d (n "ctor") (r "^0.2.2") (d #t) (k 0)) (d (n "natom-macros") (r "^0.1.1-rc.2") (d #t) (k 0)))) (h "0d1z69hvma351bx4spnb4ji3m2rf1kaxyasd32l1s1d3mkwikqds") (r "1.69")))

