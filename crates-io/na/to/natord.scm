(define-module (crates-io na to natord) #:use-module (crates-io))

(define-public crate-natord-1.0.0 (c (n "natord") (v "1.0.0") (h "1qg6rqkxfcy7d2r7l5slk7jh0l581vhnvwpjs8wbnawsx7825gf4")))

(define-public crate-natord-1.0.1 (c (n "natord") (v "1.0.1") (h "117srinzjf0p9k96zyh84v8axf19c6zc16v6xs156pyrv0nssd9m")))

(define-public crate-natord-1.0.2 (c (n "natord") (v "1.0.2") (h "0qc17827dn4psnqjczz26ls990fv1p2y01sxjlyvs8nddj8mcrpm")))

(define-public crate-natord-1.0.3 (c (n "natord") (v "1.0.3") (h "0yhs3lkh5ziqpz296f1ay5bm8525yhlkbrw8jh1pd7fi9hx7sdj6")))

(define-public crate-natord-1.0.4 (c (n "natord") (v "1.0.4") (h "1lm2pmdapy2ijhhicr26pykwsisnp9gnximik56mfdbrai5z3gq4")))

(define-public crate-natord-1.0.5 (c (n "natord") (v "1.0.5") (h "0m69g9gdfi72zssn37pxj6v4n8r0pplbfl7l5qp4f7lsb6wzxavc")))

(define-public crate-natord-1.0.6 (c (n "natord") (v "1.0.6") (h "0mssbl9ynscqv9fvi9s0wvs2kas2k9igx7ixy4qb46r6i9qj90yk")))

(define-public crate-natord-1.0.7 (c (n "natord") (v "1.0.7") (h "1wl8mfv1hfjh4sdsng5s35ih7q74avwk28hwk325iym79z2dkzz0")))

(define-public crate-natord-1.0.8 (c (n "natord") (v "1.0.8") (h "0vp5cdax4qb3x7jlx430rkjmw7gm4y3dlzm9hsk5yz3fb1hyfixz")))

(define-public crate-natord-1.0.9 (c (n "natord") (v "1.0.9") (h "0z75spwag3ch20841pvfwhh3892i2z2sli4pzp1jgizbipdrd39h")))

