(define-module (crates-io na to nato) #:use-module (crates-io))

(define-public crate-nato-0.1.0 (c (n "nato") (v "0.1.0") (h "0nwwnn0f807kadka0ls135127gkw3fiy1j91r7b1wdxidlv200x8")))

(define-public crate-nato-0.2.0 (c (n "nato") (v "0.2.0") (h "1ax891i74bhrcaga4mx52y04ajy5pvxjkavb3cffyigq36626jlx")))

