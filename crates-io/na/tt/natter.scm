(define-module (crates-io na tt natter) #:use-module (crates-io))

(define-public crate-natter-0.0.1 (c (n "natter") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("std" "color" "help" "derive"))) (k 0)) (d (n "duster") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "organic") (r "^0.1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("std" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("rt" "rt-multi-thread" "fs" "io-util"))) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1nm4zpvsg3jixcvfp3bc7m6gs4zc5vgq8lpijkrc08j6zarbiiw8")))

