(define-module (crates-io na um naumi) #:use-module (crates-io))

(define-public crate-naumi-0.1.0 (c (n "naumi") (v "0.1.0") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0d1v7n6iq1a4r8ackxw1s01xpgr5xlybz5hvyrdn1dd8490srj8k") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.1.1 (c (n "naumi") (v "0.1.1") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1rn3wwfig1gzgkjxfk5hj6rwc35qsbf7dd53i6ilby9l8hn955ka") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.1.2 (c (n "naumi") (v "0.1.2") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "13hcfhzzs1rn37hxya3fgmfchxkh2g27nwyslhjyfjphwhi1xyca") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.1.3 (c (n "naumi") (v "0.1.3") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "03ihbyaycx2fp0lxqhqsjy6v6dham2mqibdjnxb40w0l3iav4lzw") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.1.4 (c (n "naumi") (v "0.1.4") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "19pamzz642a06wh04am30mgkzb84ik9nmhndbrli2vv1ydf5dzi4") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.1.5 (c (n "naumi") (v "0.1.5") (d (list (d (n "nmacro") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0pcj5qwqbbb65fb9mjd9a8jhfmxc7x3mipib7lmgyyymwhybcgpc") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.0 (c (n "naumi") (v "0.2.0") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0mgdkzv2m075l3n1nzvkaws1dfrd3px1gl3lkarbg6xjjik9i4v5") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.1 (c (n "naumi") (v "0.2.1") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "02qigihv24lar1js7b9jl0w32rr6h68lr7gsslms46kng4v26w8i") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.2 (c (n "naumi") (v "0.2.2") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0fkb0560jz7d0h3khspzvxvvk32fgp6262lci3kffbgsxyl995fm") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.3 (c (n "naumi") (v "0.2.3") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0x1mk4dzv0y7v0fbrffzj25rmxlcw8z28aglcir45406yypdhhf4") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.4 (c (n "naumi") (v "0.2.4") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1vy5nj5rfm21kfdr3pvbi02abrfr1ys7q7wjgnrbagz4j5yrzh7b") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.5 (c (n "naumi") (v "0.2.5") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0dfmgk7c25hk65dxvbr9fp4wfp6wylbd8fymrim0wijigjq76nlp") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.6 (c (n "naumi") (v "0.2.6") (d (list (d (n "nmacro") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1gzyqh78b1p38aj0kaaw6n7r11hby7wf93ka9ypbhlymd4xmmvxz") (f (quote (("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.2.7 (c (n "naumi") (v "0.2.7") (d (list (d (n "nmacro") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "04wfp5i1lbyinz7v6djrdh0lkcz78qsz7vqa7nc4d3gy2h6lz9w7") (f (quote (("partial_eq") ("default_") ("debug") ("clone") ("async" "tokio")))) (y #t)))

(define-public crate-naumi-0.3.0 (c (n "naumi") (v "0.3.0") (d (list (d (n "nmacro") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1a5mqzvxivgizp730cj6vjw5sl3fqmfg991r4nl6si72c4aw5030") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone")))) (y #t)))

(define-public crate-naumi-0.3.1 (c (n "naumi") (v "0.3.1") (d (list (d (n "nmacro") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0y8yqdyjdkp53h8rbis671snpqgyl1sncfnhq6cdn7jchl5pj011") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone"))))))

(define-public crate-naumi-0.3.2 (c (n "naumi") (v "0.3.2") (d (list (d (n "nmacro") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1kbgcwqwq2f4j5ff1w18i1a891b7szd3s4wqgbir20has08137h5") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone"))))))

(define-public crate-naumi-0.3.3 (c (n "naumi") (v "0.3.3") (d (list (d (n "nmacro") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "0dwlyrwa0cqyfyvhybdwlpi1zj86z595k7hzjlx3kkvl8iqsa5r5") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone"))))))

(define-public crate-naumi-0.3.4 (c (n "naumi") (v "0.3.4") (d (list (d (n "nmacro") (r "^0.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "1iq0a13ib0h24y0929zmwl8yp13nzq1fyj4i3gndv27mak6zw0g8") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone"))))))

(define-public crate-naumi-0.3.5 (c (n "naumi") (v "0.3.5") (d (list (d (n "nmacro") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "io-util"))) (o #t) (d #t) (k 0)) (d (n "varint-simd") (r "^0") (d #t) (k 0)))) (h "040pdgg97dlghhxhwyqidk70pn6zghppnfxk4g6kbzhwhj866wh8") (f (quote (("partial_eq") ("net_async" "tokio") ("net") ("default_") ("debug") ("clone"))))))

