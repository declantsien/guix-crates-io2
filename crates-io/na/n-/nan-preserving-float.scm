(define-module (crates-io na n- nan-preserving-float) #:use-module (crates-io))

(define-public crate-nan-preserving-float-0.1.0 (c (n "nan-preserving-float") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 2)))) (h "07zk1zqiwsa8brc4gbwpivi910p2m46xnwf9ikx9wk1grh7z1m1l")))

