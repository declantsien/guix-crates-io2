(define-module (crates-io na ti native-file-tests) #:use-module (crates-io))

(define-public crate-native-file-tests-0.1.0 (c (n "native-file-tests") (v "0.1.0") (d (list (d (n "goblin") (r "^0.0.14") (d #t) (k 0)) (d (n "pdb") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cdwf2pvjaw5818w6nw7svsr9g92fpj53n15i1jy9j29r203w8q9")))

