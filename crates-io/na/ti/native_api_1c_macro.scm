(define-module (crates-io na ti native_api_1c_macro) #:use-module (crates-io))

(define-public crate-native_api_1c_macro-0.9.0 (c (n "native_api_1c_macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "057657vd1y5sbpv55mdizhvmgkcn5r097r73avc6zkqzd1dd3ccf")))

(define-public crate-native_api_1c_macro-0.9.1 (c (n "native_api_1c_macro") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0cvg9kmg8c6mfdbmzv21myg4vi7p8bsiqnplk1va3v5yx4yxv5q6")))

(define-public crate-native_api_1c_macro-0.10.1 (c (n "native_api_1c_macro") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 2)))) (h "1j81lvz7rxvzvyci2943ckp418bc9bfafqv1qzszwyni16z24q0g")))

(define-public crate-native_api_1c_macro-0.10.2 (c (n "native_api_1c_macro") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 2)))) (h "01kff080jgnpbxnbgildgk6ixid5n3g69qd4g8sy513iqszflzgj")))

(define-public crate-native_api_1c_macro-0.10.3 (c (n "native_api_1c_macro") (v "0.10.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 2)))) (h "0sg21pwmvxm0isckni5p4579h22kjaxf4jp4b62xalzf0rw8irvj")))

(define-public crate-native_api_1c_macro-0.10.4 (c (n "native_api_1c_macro") (v "0.10.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 2)))) (h "1wj7phhbcnirsy7v09y0d0caw38s6fdn0h6j0lmzfw7bsikdgb49")))

(define-public crate-native_api_1c_macro-0.10.5 (c (n "native_api_1c_macro") (v "0.10.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 2)))) (h "1dhibqc20ri900xxnr53891kgkzkdmjann7fy48kl3jalrdc6ayp")))

