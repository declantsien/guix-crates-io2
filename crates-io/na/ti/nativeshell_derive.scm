(define-module (crates-io na ti nativeshell_derive) #:use-module (crates-io))

(define-public crate-nativeshell_derive-0.1.1 (c (n "nativeshell_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06ix7ib1qm97a6b27sagnh978xc5ayw844g1bylpg2hb39vb2zgz")))

