(define-module (crates-io na ti native-windows-derive) #:use-module (crates-io))

(define-public crate-native-windows-derive-1.0.0 (c (n "native-windows-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1iiabqnzzglvjhcfkrhxhkjg2q4p9anrrc7k1hg5kjw6w9zwm6fq")))

(define-public crate-native-windows-derive-1.0.1 (c (n "native-windows-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ijs2i4c0qcxaq0d8jcm27fxh4w697qf15hmzf3whcm556h5g4y3")))

(define-public crate-native-windows-derive-1.0.2 (c (n "native-windows-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xwl1jh6phfyp8fslpm646z7j5ly4iwrzw84inlh9rhkc7abs4jy")))

(define-public crate-native-windows-derive-1.0.3 (c (n "native-windows-derive") (v "1.0.3") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jjrfj9qcchm4996l30p1xa77xw1sbhn258gla96ns0hyk45q8cd")))

(define-public crate-native-windows-derive-1.0.4 (c (n "native-windows-derive") (v "1.0.4") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "01smwbdq4x4032bg9n8p9rh5qjyriyz7l86sniy3zdlgn1vb0r41")))

(define-public crate-native-windows-derive-1.0.5 (c (n "native-windows-derive") (v "1.0.5") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1payjz8f8mab2whxrcbn88havv1cb94x57v19warvn1023l4l4vn")))

