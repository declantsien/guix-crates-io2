(define-module (crates-io na ti native-timer) #:use-module (crates-io))

(define-public crate-native-timer-0.1.0 (c (n "native-timer") (v "0.1.0") (d (list (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xc11gwdj4vy62ymwwhqkfgn8wfi5n6pxazj1gwvg1v089mi9q06")))

(define-public crate-native-timer-0.2.0 (c (n "native-timer") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i1p7k476cwbamin0kn0qq96wh77bcxl8zrlp7g87j74ph0k33ac")))

(define-public crate-native-timer-0.3.0 (c (n "native-timer") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0faj5z1i50w97pf6qc6z2c1c921dz2bzpc7dg68m4nwp709w8j4z")))

(define-public crate-native-timer-0.3.1 (c (n "native-timer") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1r77w7jdgnwaiwycyp4dirl1ddk71zrm67crjin5322nww90bn20")))

(define-public crate-native-timer-0.3.2 (c (n "native-timer") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qkrmagwpzf3bgxch6rz5zzcqabhyqld26kmx4a0l2i5d3fg7ypy")))

(define-public crate-native-timer-0.3.3 (c (n "native-timer") (v "0.3.3") (d (list (d (n "sync-wait-object") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0l3cd3xww4yn6dyck4wajlyf078gc2an6pal6v43y0s4j135qrx8")))

(define-public crate-native-timer-0.4.0 (c (n "native-timer") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sync-wait-object") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0xk3001hdsrd79x1gjar55pn085sdf4xl5gz2dr4a06c0p3511rc")))

(define-public crate-native-timer-0.5.0 (c (n "native-timer") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sync-wait-object") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ffw9kds6msfglqwx3jwnk5ljx0aqipd91cv28cj4sxkkfpdkzcs")))

(define-public crate-native-timer-0.5.1 (c (n "native-timer") (v "0.5.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sync-wait-object") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b9pq5yzl45s3b558hasf2j7b00jxfqjgdq1lz5d4vam5pcshm90") (f (quote (("tracker") ("default" "tracker"))))))

(define-public crate-native-timer-0.5.2 (c (n "native-timer") (v "0.5.2") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "sync-wait-object") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_Security" "Win32_System_WindowsProgramming"))) (d #t) (t "cfg(windows)") (k 0)))) (h "137cqvmcl2v5wa92sfh08xapy1wk7idl338x5kyasn94gs7whffg") (f (quote (("tracker") ("default" "tracker"))))))

