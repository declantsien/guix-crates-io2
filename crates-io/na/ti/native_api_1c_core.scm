(define-module (crates-io na ti native_api_1c_core) #:use-module (crates-io))

(define-public crate-native_api_1c_core-0.9.0 (c (n "native_api_1c_core") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1q9d3mqn6byb0gkffji3s701z0x36s0ialy91v5iamaj0asil2xp")))

(define-public crate-native_api_1c_core-0.9.1 (c (n "native_api_1c_core") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "13nb68r9y9gasjmqwnvbgkr1b92bi7vxy6rs6fj0m8m7g6vbayi4")))

(define-public crate-native_api_1c_core-0.9.2 (c (n "native_api_1c_core") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1f43abpjdmljnrcw7bncqxbg5qa01pj7prnp295rlah1bvfmprif")))

(define-public crate-native_api_1c_core-0.9.3 (c (n "native_api_1c_core") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "00yb52m9khqkmk0wwkwznnzwfg636ggz0zbwh6y4jvl9bzhs8kla")))

(define-public crate-native_api_1c_core-0.9.4 (c (n "native_api_1c_core") (v "0.9.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "utf16_lit") (r "^2.0") (d #t) (k 0)))) (h "1qi3phrfi5170bn1fvpp343cggifi7sdxm5d1cdrrj1yvvmas1v2")))

