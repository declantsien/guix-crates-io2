(define-module (crates-io na ti native-json-macro) #:use-module (crates-io))

(define-public crate-native-json-macro-1.0.0 (c (n "native-json-macro") (v "1.0.0") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "065l57g5fn0szlksz73ia42c8dvmi8rxi6ah4a9jf5b8g0fkvjbk")))

(define-public crate-native-json-macro-1.0.1 (c (n "native-json-macro") (v "1.0.1") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0v5bm935mmn9mvjspbmcy0saqdrf7pnrwsz7p34id8jhp4r9aq0q")))

(define-public crate-native-json-macro-1.0.2 (c (n "native-json-macro") (v "1.0.2") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "00h671hs8r4637mvnb91fgja31zp04b4lg7fz9z08c4xx3qkbdda")))

(define-public crate-native-json-macro-1.0.3 (c (n "native-json-macro") (v "1.0.3") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0zxhcrcy9wvqx77m7y02nz8gxva5al9k0fkdhi5xs74vdwsfv9b4")))

(define-public crate-native-json-macro-1.0.4 (c (n "native-json-macro") (v "1.0.4") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1g34h1g1zimkxaf5c0p9zmvivsg2ldyziahf4lpzsyz6z0la6if7")))

(define-public crate-native-json-macro-1.0.5 (c (n "native-json-macro") (v "1.0.5") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1radn5srjlvfm8paqzplspscq5nhv5m94mdj3ky90dh7hsw7l79y")))

(define-public crate-native-json-macro-1.0.6 (c (n "native-json-macro") (v "1.0.6") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0ykvfsvd9bfdinkwn758a84rhdpxn8ampsbn1xkqyqjzj8chcza4")))

(define-public crate-native-json-macro-1.0.7 (c (n "native-json-macro") (v "1.0.7") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03z0wkcpx5qpp0cf7lfv0xknhyzhajcrz037y6b0zdpcfm4vfpjm")))

(define-public crate-native-json-macro-1.0.8 (c (n "native-json-macro") (v "1.0.8") (d (list (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1lw414v8n8j1qp2wpbbzwj126mb9ipwav2cm9vbyrir0av7n0miw")))

