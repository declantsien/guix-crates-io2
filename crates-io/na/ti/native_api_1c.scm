(define-module (crates-io na ti native_api_1c) #:use-module (crates-io))

(define-public crate-native_api_1c-0.9.0 (c (n "native_api_1c") (v "0.9.0") (d (list (d (n "native_api_1c_core") (r "=0.9.0") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.9.0") (d #t) (k 0)))) (h "0j9hgi1sj906i9rylmh4h2w995hzk5r6vxsj47511ygn1mkz9rr1") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.9.1 (c (n "native_api_1c") (v "0.9.1") (d (list (d (n "native_api_1c_core") (r "=0.9.0") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.9.0") (d #t) (k 0)))) (h "046bp3wljcl31f54a9znq8k7wwrx9q98sizdd1wdqmq860z8i637") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.9.2 (c (n "native_api_1c") (v "0.9.2") (d (list (d (n "native_api_1c_core") (r "=0.9.0") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.9.0") (d #t) (k 0)))) (h "1gr08903cpyqldfhmsdd852yzk3v2pcjln5f0qmz71k9k0gjs11y") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.9.3 (c (n "native_api_1c") (v "0.9.3") (d (list (d (n "native_api_1c_core") (r "=0.9.0") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.9.1") (d #t) (k 0)))) (h "09dw94wklklkm0xw18qi96gndv8jfbalvd9vb8a47gi0vfhshki7") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.10.1 (c (n "native_api_1c") (v "0.10.1") (d (list (d (n "native_api_1c_core") (r "=0.9.0") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.1") (d #t) (k 0)))) (h "0z9jxcac7jas5cnvkzyzlxpk62j0qrkhgsc5wcq4c0m6k6z7cl3c") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.10.2 (c (n "native_api_1c") (v "0.10.2") (d (list (d (n "native_api_1c_core") (r "=0.9.1") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.2") (d #t) (k 0)))) (h "03zckcq0wzxp1x5siyynk7famyg2b1bkhnbnyldrwax9akkcivnw") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.10.3 (c (n "native_api_1c") (v "0.10.3") (d (list (d (n "native_api_1c_core") (r "=0.9.3") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.3") (d #t) (k 0)))) (h "0bxk5rf3zpxc2zb3bz9rdy6d0v7mvwaihq82mxb78139v7hf7zg4") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.10.4 (c (n "native_api_1c") (v "0.10.4") (d (list (d (n "native_api_1c_core") (r "=0.9.3") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.3") (d #t) (k 0)))) (h "0bqa3wwlr6r7mwxzwg6k0q1fm5yax4m3d1b4jib580vpklx6r7zp") (f (quote (("macro") ("default" "macro")))) (y #t)))

(define-public crate-native_api_1c-0.10.5 (c (n "native_api_1c") (v "0.10.5") (d (list (d (n "native_api_1c_core") (r "=0.9.3") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.4") (d #t) (k 0)))) (h "0d3jcdiaakvrcl89adgmf7fdndxxxfk6742q5hhiky4v88a0cqhy") (f (quote (("macro") ("default" "macro"))))))

(define-public crate-native_api_1c-0.10.6 (c (n "native_api_1c") (v "0.10.6") (d (list (d (n "native_api_1c_core") (r "=0.9.3") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "=0.10.4") (d #t) (k 0)))) (h "0x9haqgn6846libqviwbky2mg5644wfxs22adhmasihp7fn2yzk3") (f (quote (("macro") ("default" "macro")))) (y #t)))

(define-public crate-native_api_1c-0.10.7 (c (n "native_api_1c") (v "0.10.7") (d (list (d (n "native_api_1c_core") (r "^0.9.4") (d #t) (k 0)) (d (n "native_api_1c_macro") (r "^0.10.5") (d #t) (k 0)))) (h "0b4bxfv59pa7h1zqiaq5q4mf2l483747fpn9scsl8hh4yzzdpv20") (f (quote (("macro") ("default" "macro"))))))

