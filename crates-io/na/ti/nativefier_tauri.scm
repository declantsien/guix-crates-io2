(define-module (crates-io na ti nativefier_tauri) #:use-module (crates-io))

(define-public crate-nativefier_tauri-0.1.0 (c (n "nativefier_tauri") (v "0.1.0") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0xvd4mmq65fg1y4h1759jxjk2891fa4wka2rwmrjccxsjid7a07n")))

(define-public crate-nativefier_tauri-0.1.1 (c (n "nativefier_tauri") (v "0.1.1") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1h5bj2672mll1aba03jxnk72glc42867gh7b55nk97d4hxm8s1hk")))

(define-public crate-nativefier_tauri-0.1.2 (c (n "nativefier_tauri") (v "0.1.2") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1891j7nzmd2yg3g1w5ign5vhidvjzpcp1yxghnbrp7bp3fz2cfv1")))

(define-public crate-nativefier_tauri-0.1.3 (c (n "nativefier_tauri") (v "0.1.3") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1hvhya9id5majbwx5gfqgs20c2zqviar8ysb6km0hk3pb3d0ymi3")))

(define-public crate-nativefier_tauri-0.1.4 (c (n "nativefier_tauri") (v "0.1.4") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1vysagl7yh06rqvaf4xi58pdysd0ldqag6586mr40spd3mxdvb0r")))

(define-public crate-nativefier_tauri-0.1.5 (c (n "nativefier_tauri") (v "0.1.5") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "116vsaj6ngzbhlyfmxz4r6wbyl6dh937q8fcrivfcdbymgyjbkrx")))

(define-public crate-nativefier_tauri-0.1.6 (c (n "nativefier_tauri") (v "0.1.6") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0rbyz7h195h448fdg5ms28zz1fdkn5sw9gzn6xx5xz2y72nrflfx")))

(define-public crate-nativefier_tauri-0.1.7 (c (n "nativefier_tauri") (v "0.1.7") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0dcp9mdvpcpdq58ggy5bgij9bnvri8k0xj8gsabmddr52a2sx5yc")))

(define-public crate-nativefier_tauri-0.1.8 (c (n "nativefier_tauri") (v "0.1.8") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0adk11fax2yjzchhpb4w2nams7dk7in40qgyna1hslm8h6zxpx1v")))

(define-public crate-nativefier_tauri-0.1.9 (c (n "nativefier_tauri") (v "0.1.9") (d (list (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "15jpw60dx48bsyd62i6zy7fg183cipljjaphghsmdds8q1yzw22l")))

(define-public crate-nativefier_tauri-0.2.0 (c (n "nativefier_tauri") (v "0.2.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1dvhdmb1w82h6jqhlvzf5q37bxsjqkzhwl6n2ybhi0q5b4ddmflw")))

(define-public crate-nativefier_tauri-0.2.1 (c (n "nativefier_tauri") (v "0.2.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1f06m3bjhf4nv1wjadgcg5dil2gijx2hyr1khjibi2ks12vv9iic")))

(define-public crate-nativefier_tauri-0.2.2 (c (n "nativefier_tauri") (v "0.2.2") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1r6nh1rxa7558qnqpm0wak2p2ndrzbyqp073r66xj3lq67rflyf1")))

(define-public crate-nativefier_tauri-0.2.3 (c (n "nativefier_tauri") (v "0.2.3") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "0i84kdqghas1w68wyrzsj0g560zg273ifspf5vlqls6bvagvhzii")))

(define-public crate-nativefier_tauri-0.2.5 (c (n "nativefier_tauri") (v "0.2.5") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)))) (h "1n1pc161chp33bc09lg78crzkxcxh8awaid7phai9xy4ivbzg37n")))

