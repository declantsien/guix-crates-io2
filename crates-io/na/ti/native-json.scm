(define-module (crates-io na ti native-json) #:use-module (crates-io))

(define-public crate-native-json-0.0.0 (c (n "native-json") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pzfm1mzifmhbwavkf7hyvnlvss50y9km4r1gnniabb6jf7fsjbr")))

(define-public crate-native-json-0.0.1 (c (n "native-json") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14k8bl2g99dc9vh9lf9krm9m8wa5jgfmnjaazb0p033m9sdrspc2")))

(define-public crate-native-json-1.0.0 (c (n "native-json") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1b7v0i2bf53yiwgsx83wpyqs14rqawcgqv5a1lvy6fy5nbxd0wiq")))

(define-public crate-native-json-1.0.1 (c (n "native-json") (v "1.0.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1s80fikwn6h7xfx82pqq6ihfmnmssh07j98b3vi4zdsh72151sx9")))

(define-public crate-native-json-1.0.2 (c (n "native-json") (v "1.0.2") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1vzf6srwfnqxsdbm0237pwbqf6zgb6kir5qgdv3cmj4mbbpi5061")))

(define-public crate-native-json-1.1.0 (c (n "native-json") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10cz99ix05a1hyiwsgrc2qycg94740mk2srs23xydsdw6pyacgsd")))

(define-public crate-native-json-1.1.1 (c (n "native-json") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "03dqaznim0w90g4h8366mw84cwiwf4nr0nhgfg69svdkfdn59q76")))

(define-public crate-native-json-1.1.2 (c (n "native-json") (v "1.1.2") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("parsing"))) (d #t) (k 0)))) (h "099w7m680zmmscinn760q8y8gw8i0r8c5xz65yzvxs9a766lajad")))

(define-public crate-native-json-1.1.3 (c (n "native-json") (v "1.1.3") (d (list (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1981jshilka8dqckq6g316wjpyd5vnds8a4ijsa5j6rnj9wqmfla")))

(define-public crate-native-json-1.1.4 (c (n "native-json") (v "1.1.4") (d (list (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "03f7qgwjcjf4wvs53cxgh7ll16hybsfd6hd747i8kcwidzxdy70a")))

(define-public crate-native-json-1.1.5 (c (n "native-json") (v "1.1.5") (d (list (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1jw2jknslxsd1p1azfgr06wfcnrp72bmr7qr9fxrwpvsc26lc2ir")))

(define-public crate-native-json-1.1.6 (c (n "native-json") (v "1.1.6") (d (list (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "08brnrkhg79dw82cc3ar6hij2fr4mv82v5p8fix25llk40c1lr6j")))

(define-public crate-native-json-1.2.0 (c (n "native-json") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pfkvq46z3wb3dcxvmn0hkh94xg3n3x8mhw8abhv2qjv5xc4r2wh")))

(define-public crate-native-json-1.2.1 (c (n "native-json") (v "1.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07y1xvq8hrfh4qw6mndkzw15cnyf20ddvmgmb79l7zhl40bs9rzg")))

(define-public crate-native-json-1.2.2 (c (n "native-json") (v "1.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gia1ffa9zv0zc3z70g2rhxb7yisda1zb7ni4zgjvlw92f5n52vl")))

(define-public crate-native-json-1.2.3 (c (n "native-json") (v "1.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k9fq9msf6ibivadaskrjrsgbjmk9j4la70vqfy8mfwp5dckc48p")))

(define-public crate-native-json-1.2.4 (c (n "native-json") (v "1.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g69yqfwlq5irlssbc061v5nzidf9ws9mxmkj9f9pxqh77rn6vpk")))

(define-public crate-native-json-1.2.5 (c (n "native-json") (v "1.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "010di20n2h3ldc4dxsnr0f0fqhan8yl2dbs5y5s1iarn05h7hr92")))

(define-public crate-native-json-1.2.6 (c (n "native-json") (v "1.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08kpyir9wc1h5n8dkgiszqmxx1x6xb47w2fxfl2bbik6znf9qwsr")))

(define-public crate-native-json-1.2.7 (c (n "native-json") (v "1.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l3ldg4i2vwgj471g5b7nvv8f500awmki3xsvviz2rihcnhibib9")))

(define-public crate-native-json-1.2.8 (c (n "native-json") (v "1.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yq7432wmc9z1bj03g2iylpyxdg8jm7rkb8gai63ji2dxs4vpr5w")))

(define-public crate-native-json-1.2.9 (c (n "native-json") (v "1.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19y2mq0qj3k05hbszg4asb8qljqb368c1j4ndmn5k8c01kxh80ks")))

(define-public crate-native-json-1.2.10 (c (n "native-json") (v "1.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "native-json-macro") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kwwz5kk18ia74y8qb40m9ss557shyncn95xjbp45pbc7j4146yb")))

