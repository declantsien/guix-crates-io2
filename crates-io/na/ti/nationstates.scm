(define-module (crates-io na ti nationstates) #:use-module (crates-io))

(define-public crate-nationstates-0.1.0 (c (n "nationstates") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)))) (h "17i88rzy0c8mbz92svjwky88pvyl0k40x1mc297a6xgm23kl9gc1")))

(define-public crate-nationstates-0.1.1 (c (n "nationstates") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "tokio") (r "^1.7.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yw9v5maad2y7mxca15v1cmnfmm1fnyds9rql7nz5w65hci5qy91")))

