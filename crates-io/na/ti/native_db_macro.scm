(define-module (crates-io na ti native_db_macro) #:use-module (crates-io))

(define-public crate-native_db_macro-0.5.0 (c (n "native_db_macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ig7dniripsi5h131zwvz4kg11fkmrc0vl1cjjfw0g9d3jl1yvdg") (f (quote (("default"))))))

(define-public crate-native_db_macro-0.5.1 (c (n "native_db_macro") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12fhl2160p4xjj29rpmfv2hs32awds067galsk7sqbnm5i37914a") (f (quote (("default"))))))

(define-public crate-native_db_macro-0.5.2 (c (n "native_db_macro") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f4cax278zk3q08w2xgwwpnhpa8pgkf92m5lvf6nkrqhpf53h76m") (f (quote (("default"))))))

(define-public crate-native_db_macro-0.5.3 (c (n "native_db_macro") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z9scghysfyrjngiypc5f0sr9xp2wkylykyqzmfhspps9nmz3b7x") (f (quote (("default"))))))

(define-public crate-native_db_macro-0.6.0 (c (n "native_db_macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sgg9f2lp7zm3865r6p9i217xd1n9m6qf5f8c218idhzc10bl9xz") (f (quote (("default"))))))

(define-public crate-native_db_macro-0.6.1 (c (n "native_db_macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17irf83xh8dwpz0w0w63fpcbcngirn1lhqfylm741c4fkrhd302f") (f (quote (("default"))))))

