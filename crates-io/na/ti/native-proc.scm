(define-module (crates-io na ti native-proc) #:use-module (crates-io))

(define-public crate-native-proc-0.1.0 (c (n "native-proc") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cfg5x7mmbrnddhrr041wybrrxcd74drjp48i37rvgiszy95w43d")))

(define-public crate-native-proc-0.2.0 (c (n "native-proc") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03fds3af2l8kcxjp174y7vqgyvg0zk2xpf3zx4ny1k7zgcq7hg70")))

(define-public crate-native-proc-0.3.0 (c (n "native-proc") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10h9qcdylj0xm1qb6p8qzgi95zbfbfjzz16vf962085w8wk0hd0z")))

