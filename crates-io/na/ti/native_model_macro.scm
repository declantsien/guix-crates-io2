(define-module (crates-io na ti native_model_macro) #:use-module (crates-io))

(define-public crate-native_model_macro-1.0.0 (c (n "native_model_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16nb7y2jlc4rqxwbg2b52cf2bj71cbsjk9m4rcnx6gda73snx04a")))

(define-public crate-native_model_macro-0.2.0 (c (n "native_model_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0qiv1ls6zvjdqw6kn5fyrzplc6ciqjg3457rxbb0m8qjbi39my")))

(define-public crate-native_model_macro-0.2.1 (c (n "native_model_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1idclzj1jpf6nmbp07nyh5ckyal0p42pavrkbxkkzzqhvb9d780m")))

(define-public crate-native_model_macro-0.2.2 (c (n "native_model_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q2k5bi6vz62rx09wfw8bs0amr6a7x92b1z2pgk8gszndjm8yhl8")))

(define-public crate-native_model_macro-0.2.3 (c (n "native_model_macro") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vxxhp9kmy6g9334rlv7v8djgzd4nx3ypm5lkw3fpzvhrgmpnizq")))

(define-public crate-native_model_macro-0.2.4 (c (n "native_model_macro") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wdf5vbmgmyi60g13cp3qh2w33gii80fxj4dawv0vjmp3zbc18xs")))

(define-public crate-native_model_macro-0.3.0 (c (n "native_model_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p2p8m7mhifs9r6sisy3w44d3daabax883pnij0yycmbrrjfxl2s")))

(define-public crate-native_model_macro-0.3.1 (c (n "native_model_macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vwrrbnx6f5i0ifg366aglqlarf9495lr1jxjrcrh0d0g2c70548")))

(define-public crate-native_model_macro-0.3.2 (c (n "native_model_macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c98c91v75cxvv1przflzi53l6c7a9z70r1dvrqazwj4s9vfvh2c")))

(define-public crate-native_model_macro-0.3.3 (c (n "native_model_macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gpb023f2fdb4iw1g7bzxfz9gys3g2qndm0sjlmrlxphdaxd57p1")))

(define-public crate-native_model_macro-0.3.4 (c (n "native_model_macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02a4p1i8hdwgyl8m98pax53dwjwszv9xhcarsd8wbwmgw6dmqgl0")))

(define-public crate-native_model_macro-0.3.5 (c (n "native_model_macro") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14kd0i5kqrr8w7jbhjxv0w7wyb0wfi790v0111f8s50jsv4hsm9i")))

(define-public crate-native_model_macro-0.3.6 (c (n "native_model_macro") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dc644sjw8bj2al28hzxs3w6lrs49vy1fxdra196br0b8hch4mrb")))

(define-public crate-native_model_macro-0.3.7 (c (n "native_model_macro") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ifkd1yrb9dfzzmwa8bs88hwpyb5vk8nlaip9kqqwpa7w9j7c1wa")))

(define-public crate-native_model_macro-0.3.8 (c (n "native_model_macro") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lck75ckcicf9zxd7lsv0kif0wjy6bdwmzcys6dy465ga4a2jlnh")))

(define-public crate-native_model_macro-0.3.9 (c (n "native_model_macro") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13fzzldvr523cyspymf99r35gj12jw3vhfg3z5h5abv9rf4wj9xc")))

(define-public crate-native_model_macro-0.3.10 (c (n "native_model_macro") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18q5b64siqmxv38i4bfzkzisr9b0hazppnfl0b825ia67mwyyfmp")))

(define-public crate-native_model_macro-0.3.11 (c (n "native_model_macro") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qz0hwl2pv2fplkn9yc2krfqwbp9sckq9jdmzd0spmycz0bl81rx")))

(define-public crate-native_model_macro-0.3.12 (c (n "native_model_macro") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xb12518k6z3by0pkinh2iq4zsna6r5bj7z37s0hq6x35xxc1fkl")))

(define-public crate-native_model_macro-0.3.13 (c (n "native_model_macro") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04g7j2g297hcrhxd4n6mkr23rbh7p7x0829c7bifdfr64rpnfv8n")))

(define-public crate-native_model_macro-0.3.14 (c (n "native_model_macro") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxpdz47pcgph6wbqpnb32agchnmlbr79fg0hlhjx6hg36kg65qh")))

(define-public crate-native_model_macro-0.3.15 (c (n "native_model_macro") (v "0.3.15") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1643avdhrjdqgsd41nmjpf7p1dqinp7iyyi903rnnyiz2wy2l4wl")))

(define-public crate-native_model_macro-0.3.16 (c (n "native_model_macro") (v "0.3.16") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l750322vdnd9ps12m9fhlfgap2c6m3v28jrxi1qm852m5d3ygrw")))

(define-public crate-native_model_macro-0.3.17 (c (n "native_model_macro") (v "0.3.17") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ak5xxwy873cjps8xiwrjcxkayn1q4m4xfvskp1bf64g57ip89d2")))

(define-public crate-native_model_macro-0.3.18 (c (n "native_model_macro") (v "0.3.18") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08prbh8j6fpp4y9a946cq244xx0wrg4q8c1wnw2vfjzgdqfwsrwx")))

(define-public crate-native_model_macro-0.3.19 (c (n "native_model_macro") (v "0.3.19") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mz3y6ixfqrq0fs54c92mshj1lk0srapi5d66c5vcxy7vm4qkpa0")))

(define-public crate-native_model_macro-0.3.20 (c (n "native_model_macro") (v "0.3.20") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sas3x7lbrrcw87yh1mnpcm73zb0g8v83m0am0qc199w3f8kz7dv")))

(define-public crate-native_model_macro-0.3.21 (c (n "native_model_macro") (v "0.3.21") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gngjmg5k1zgp05ligwlqaicw0yimpxsh5sklsmb7mvhzky77xnc")))

(define-public crate-native_model_macro-0.3.22 (c (n "native_model_macro") (v "0.3.22") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c3rfndcc79kikd8jq5bbpr3fi8nlqblifpcnpr1vizkg2fi290i")))

(define-public crate-native_model_macro-0.3.23 (c (n "native_model_macro") (v "0.3.23") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m77fbx3ha2hii9i7rywnj9dbxjdnqzp0y78m9frs1qff5zikfnz")))

(define-public crate-native_model_macro-0.3.24 (c (n "native_model_macro") (v "0.3.24") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09ilnvpav730hn1q4jgn8x7g7gmif4ha3nph3fkbabzz88zr6qdk")))

(define-public crate-native_model_macro-0.3.25 (c (n "native_model_macro") (v "0.3.25") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "167ypiswv6sdac2dgc8wcwv8mpld23k7hmd1yk5l26pk0vc9c744")))

(define-public crate-native_model_macro-0.3.26 (c (n "native_model_macro") (v "0.3.26") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0svrknfsn39gkjpx2gfcjiqc2kv4fyzankfjiiqaw49gmvgjy2ah")))

(define-public crate-native_model_macro-0.3.27 (c (n "native_model_macro") (v "0.3.27") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rfyzvy15w13dq6s1w3d4gshivvg3mnbmbap2w4h9p96iqsid93z")))

(define-public crate-native_model_macro-0.3.28 (c (n "native_model_macro") (v "0.3.28") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zqc259adwkr6ddlwgxw6gaczp568ibrysjp30yhpih4n6mxl43i")))

(define-public crate-native_model_macro-0.3.29 (c (n "native_model_macro") (v "0.3.29") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "135f04j2jdwqyq39spw6p10475kv0dxca72jrzh1j2d0973pzxcv")))

(define-public crate-native_model_macro-0.3.30 (c (n "native_model_macro") (v "0.3.30") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17cddzj8gbqaaqx3f8kz93zrffrj4vixifvffx318chbl2z93vay")))

(define-public crate-native_model_macro-0.4.0 (c (n "native_model_macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qgd54v65q5cyp35g83mwb2xl0hr8axvqbv044iqhbjn2qf8yxg5")))

(define-public crate-native_model_macro-0.4.1 (c (n "native_model_macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i7pmcaag6b9xmdgzfd5cfjz7g8wjpirqivrzagm87lwq11g3lsx")))

(define-public crate-native_model_macro-0.4.2 (c (n "native_model_macro") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dwf9gy849c51vd0f1d1rzp3zv8zaxmanw91mp75smgdkj3n2l8g")))

(define-public crate-native_model_macro-0.4.3 (c (n "native_model_macro") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "081hrzdzj99s1c66wrxnqdvz5bzvkck5n082zdbijnhg61df9afg")))

(define-public crate-native_model_macro-0.4.4 (c (n "native_model_macro") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd1nn67ihx62fan17vwigmnc2jd1z8x3mkbr12fjhwi9pyv1ii4")))

(define-public crate-native_model_macro-0.4.5 (c (n "native_model_macro") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j7asn0if5m03mmcav6l46dl0ijljcnxy9qwzqwz1aj9wdwkp7wd")))

(define-public crate-native_model_macro-0.4.6 (c (n "native_model_macro") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1li7m8d2s77n9wggdrsirshvahr4gmmqg0k0kqhvpsdxhhvkl50l")))

(define-public crate-native_model_macro-0.4.7 (c (n "native_model_macro") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0.73") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kdvnlcq534fslx1y807rpdxzyzx5cyyswyn73gw8n2m0iql18m3")))

(define-public crate-native_model_macro-0.4.8 (c (n "native_model_macro") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6fdvlrbdmj6sw6qn21svw5fw9zqxlzvk955fnpl3b5c8k78vyd")))

(define-public crate-native_model_macro-0.4.9 (c (n "native_model_macro") (v "0.4.9") (d (list (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksl9fwyhzjbj3hg67jpl3xh2a0r733dbw1xdwyj7ixznrgvf8l7")))

(define-public crate-native_model_macro-0.4.10 (c (n "native_model_macro") (v "0.4.10") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bb6zi5p5y8dxqqw693idmba3jpa67hp41c8vznwhccaqhf2ybj4")))

(define-public crate-native_model_macro-0.4.11 (c (n "native_model_macro") (v "0.4.11") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18hsz1lf32a4xzsw416kq5h9mrr6w3gsrqzkh665ib4m46fcq63f")))

(define-public crate-native_model_macro-0.4.12 (c (n "native_model_macro") (v "0.4.12") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dg2j5z44qsjm16ghdf57mh3achg8c8m4wpfwcw95fmzw2gl5arw")))

(define-public crate-native_model_macro-0.4.13 (c (n "native_model_macro") (v "0.4.13") (d (list (d (n "proc-macro2") (r "^1.0.80") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hwrh8jbj9vqjqxl4v2c6974q8mh0lhdlvpnspazvgpxain8vvl4")))

(define-public crate-native_model_macro-0.4.14 (c (n "native_model_macro") (v "0.4.14") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y6qndp2xnlq5pzkf0nmg5dvc2riwzvma35x0p0v2vqfz1aqynwa")))

(define-public crate-native_model_macro-0.4.15 (c (n "native_model_macro") (v "0.4.15") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09xnbqb0n5bd4gwv857bpngdb1xk5m71vpmj1xgvxxrwjziq0kkp")))

(define-public crate-native_model_macro-0.4.16 (c (n "native_model_macro") (v "0.4.16") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12c11lv5dchak34wyf31132hkb7hm7lrc50mmfjg2n8418wg1idp")))

(define-public crate-native_model_macro-0.4.17 (c (n "native_model_macro") (v "0.4.17") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16y048vhg0vnifyybndfv4wpjsc4xc79cqwg5rj0k59crczw2h6y")))

(define-public crate-native_model_macro-0.4.18 (c (n "native_model_macro") (v "0.4.18") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1jzm2sm2asm79wy7rqjismac9flqg2phhn3xki4qlj1x68zxmg85")))

(define-public crate-native_model_macro-0.4.19 (c (n "native_model_macro") (v "0.4.19") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0834qjlyxwx215kszshrxmkris3mbglw1cv4a11pd1wc25aykb9m")))

