(define-module (crates-io na nd nand2tetris-hdl-parser) #:use-module (crates-io))

(define-public crate-nand2tetris-hdl-parser-0.1.0 (c (n "nand2tetris-hdl-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1n2p6pn930j3nfkrh8219zdj6hhxvqajidkvk4qnacij5k38ki6p")))

