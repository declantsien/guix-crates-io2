(define-module (crates-io na ka nakamoto-net-mio) #:use-module (crates-io))

(define-public crate-nakamoto-net-mio-0.1.0 (c (n "nakamoto-net-mio") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.8.5") (f (quote ("os-poll" "net"))) (d #t) (k 0)) (d (n "nakamoto-net") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "socket2") (r "^0.4") (d #t) (k 0)))) (h "01rcp33fwp049lnw9i8nyplr76w7vmgvgq68i1kllhrpkcj2xq4y")))

