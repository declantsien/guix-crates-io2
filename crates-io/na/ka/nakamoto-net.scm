(define-module (crates-io na ka nakamoto-net) #:use-module (crates-io))

(define-public crate-nakamoto-net-0.3.0 (c (n "nakamoto-net") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a021if6vnk2dym7rslm7ilmddhxm0xw2v1inj11av0jhpsxzrm5") (f (quote (("default"))))))

(define-public crate-nakamoto-net-0.4.0 (c (n "nakamoto-net") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rvixpcbpjqi1s5n4n2bigjpjby7pchc56248r1sdn34kckfd12w") (f (quote (("default"))))))

