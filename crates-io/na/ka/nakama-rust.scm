(define-module (crates-io na ka nakama-rust) #:use-module (crates-io))

(define-public crate-nakama-rust-0.1.0 (c (n "nakama-rust") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (f (quote ("prost"))) (d #t) (k 1)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "1df88b8x41lqql0317nw02g8aa3zppwvs3gz0lsklkcisahi9qkd")))

