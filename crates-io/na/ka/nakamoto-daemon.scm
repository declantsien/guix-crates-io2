(define-module (crates-io na ka nakamoto-daemon) #:use-module (crates-io))

(define-public crate-nakamoto-daemon-0.1.0 (c (n "nakamoto-daemon") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.3") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nakamoto-node") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "019r5ip5634x30hp7h7nh0qdnd0r8v6b2g4qw3y1plnrnmq29i3h") (y #t)))

