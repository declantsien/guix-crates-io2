(define-module (crates-io na ka nakama-rs) #:use-module (crates-io))

(define-public crate-nakama-rs-0.1.0 (c (n "nakama-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "quad-net") (r "^0.1.1") (d #t) (k 0)))) (h "1v4jlwxxr8lks7kbiq8k3rg7f9zyg7bmsx68v748jpn26qgb7m2x")))

(define-public crate-nakama-rs-0.1.1 (c (n "nakama-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "quad-net") (r "^0.1.1") (d #t) (k 0)))) (h "0vmj0bvigxcxk5dy5cvxnlsdasahlng7pq8wkphrda165npknxky")))

