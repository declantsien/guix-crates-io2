(define-module (crates-io na ka nakamoto-common) #:use-module (crates-io))

(define-public crate-nakamoto-common-0.1.0 (c (n "nakamoto-common") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ynfm8b7mmka4m3qr6848alwaj12x2hb5mwrlfljrxdyd2rv72xm") (y #t)))

(define-public crate-nakamoto-common-0.2.0 (c (n "nakamoto-common") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.25.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.9.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "nonempty") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08gd9a0r17530akgcfc4rm1j7n74a5l56bciczjsjrg0szi8p3zq")))

(define-public crate-nakamoto-common-0.3.0 (c (n "nakamoto-common") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.28.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "nakamoto-net") (r "^0.3.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vnv80m41lcxx6m5b86axwpa5a2a9myij1vz1bg5fc48lv68wgjh")))

(define-public crate-nakamoto-common-0.4.0 (c (n "nakamoto-common") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "microserde") (r "^0.1") (d #t) (k 0)) (d (n "nakamoto-net") (r "^0.4.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "184cjm9dmhmpf8chgir50b5d00zinmclmhj03qw607hpv872drjc")))

