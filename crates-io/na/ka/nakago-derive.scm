(define-module (crates-io na ka nakago-derive) #:use-module (crates-io))

(define-public crate-nakago-derive-0.8.0 (c (n "nakago-derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08r3n82vm7jyfyx35l138s4agwlp1rmdgika0hnrcsa49gbs4jp3")))

(define-public crate-nakago-derive-0.18.0 (c (n "nakago-derive") (v "0.18.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ljy8j99qk67lklxdb1rbzafiwgf95vpf2xgag0ab4036n836l4v")))

(define-public crate-nakago-derive-0.19.0 (c (n "nakago-derive") (v "0.19.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut" "visit"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qw30d877bzp6dgiy8d33fz7al91fcyz3bbjdc3piakarzk6d6jm")))

