(define-module (crates-io na ka nakamoto-test) #:use-module (crates-io))

(define-public crate-nakamoto-test-0.1.0 (c (n "nakamoto-test") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.23") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nakamoto-common") (r "^0.1.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.5") (d #t) (k 0)))) (h "04c4f23kc1i7zixid6s6rv373hhn01m4vmwj3rsw9n101yqjnbi6")))

(define-public crate-nakamoto-test-0.2.0 (c (n "nakamoto-test") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.25.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nakamoto-common") (r "^0.2.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.5") (d #t) (k 0)))) (h "0801k1dm9rqs5pjkh94wav5yk0p9pp9l2hi9ym7f93i2za1zahqn")))

(define-public crate-nakamoto-test-0.3.0 (c (n "nakamoto-test") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nakamoto-common") (r "^0.3.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (k 0)))) (h "1axfpf9sly2s51k8mcz4mnqcv2q546gwxsb1gfqyxlkx4681wj9m")))

(define-public crate-nakamoto-test-0.4.0 (c (n "nakamoto-test") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("std"))) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "fastrand") (r "^1.3.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "nakamoto-common") (r "^0.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (k 0)))) (h "1cm5d5ibj1y2w13s7wh3fg63fxnjj0vvs9zrxzwarlxr0sw3423v")))

