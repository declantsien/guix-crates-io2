(define-module (crates-io na tn natnet-decode) #:use-module (crates-io))

(define-public crate-natnet-decode-0.1.0 (c (n "natnet-decode") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0.0.95") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "semver") (r "^0.4") (d #t) (k 0)))) (h "0jfpsbz2s4rgfi7dv0zhfszkiy7si59j9yi3pivn7vwql5v31psf") (f (quote (("default"))))))

