(define-module (crates-io na st nastran) #:use-module (crates-io))

(define-public crate-nastran-0.0.1 (c (n "nastran") (v "0.0.1") (d (list (d (n "ascii") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^2.24") (d #t) (k 0)) (d (n "dtoa") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "19fii19xz1rq3fp9pgv0rr3198sk7nak25gwk0viz9v6ns63iy4c")))

