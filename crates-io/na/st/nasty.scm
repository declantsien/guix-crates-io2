(define-module (crates-io na st nasty) #:use-module (crates-io))

(define-public crate-nasty-0.1.0 (c (n "nasty") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.0") (f (quote ("termination"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "zbus") (r "^3.2.0") (d #t) (k 0)))) (h "1jzawxb8nkjscg6ad9f1p88gvhxc0g63nc5lakvf27rh5pd3fcns")))

