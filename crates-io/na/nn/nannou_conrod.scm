(define-module (crates-io na nn nannou_conrod) #:use-module (crates-io))

(define-public crate-nannou_conrod-0.18.0 (c (n "nannou_conrod") (v "0.18.0") (d (list (d (n "conrod_core") (r "^0.76") (d #t) (k 0)) (d (n "conrod_wgpu") (r "^0.76") (d #t) (k 0)) (d (n "conrod_winit") (r "^0.76") (d #t) (k 0)) (d (n "nannou") (r "^0.18.0") (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "19xmgnprwvpgvdamrrysf5dzxn35zydlqwkkr1c0y4jklc6k040a") (f (quote (("notosans" "nannou/notosans") ("default" "notosans"))))))

