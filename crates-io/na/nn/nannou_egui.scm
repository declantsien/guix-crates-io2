(define-module (crates-io na nn nannou_egui) #:use-module (crates-io))

(define-public crate-nannou_egui-0.1.0 (c (n "nannou_egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.6") (d #t) (k 0)) (d (n "nannou") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "0mcg17r33ah664zrgc99ij1hjdb4l6fwmshg20igmhwkp1q9m30z") (y #t)))

(define-public crate-nannou_egui-0.2.0 (c (n "nannou_egui") (v "0.2.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.6") (d #t) (k 0)) (d (n "nannou") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "0qdwhyx7b4srlgmbxhnx7gw20ils2g058g7j7mw6x8bj7cnafaqf")))

(define-public crate-nannou_egui-0.2.1 (c (n "nannou_egui") (v "0.2.1") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.6") (d #t) (k 0)) (d (n "nannou") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1jpkbp1gvbc69algl12yddi1lfr3kiyb508j77hy6wzf7mbx01i3")))

(define-public crate-nannou_egui-0.3.0 (c (n "nannou_egui") (v "0.3.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.6") (d #t) (k 0)) (d (n "nannou") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "0yrjwjg70k5va1mydn18rrqc24d49lz7vm95aq8b2z62bh90s6jq")))

(define-public crate-nannou_egui-0.3.1 (c (n "nannou_egui") (v "0.3.1") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.6") (d #t) (k 0)) (d (n "nannou") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 0)))) (h "1pi7kh192nrcj04sb76jfrfcjizvqkhg81b2jg9gssr0drhv86n8")))

(define-public crate-nannou_egui-0.4.0 (c (n "nannou_egui") (v "0.4.0") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.10") (d #t) (k 0)) (d (n "nannou") (r "^0.17.1") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "1mdyrcvy152mps1dcs3c36qa1dgq015j1r3mx9k45xybxf242w53")))

(define-public crate-nannou_egui-0.5.0 (c (n "nannou_egui") (v "0.5.0") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "egui_wgpu_backend") (r "^0.14") (d #t) (k 0)) (d (n "nannou") (r "^0.18.1") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 0)))) (h "0lmabg898hli27rqwwmkqjvg31hybk9n0rkhlgxnxlz01d5a44lr")))

(define-public crate-nannou_egui-0.19.0 (c (n "nannou_egui") (v "0.19.0") (d (list (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "egui-wgpu") (r "^0.23") (d #t) (k 0)) (d (n "nannou") (r "^0.19.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "0xlqfkcid21iq1sgsshl2r1nsvaf9rydsq9csfv74lvz6qlfm6f0") (f (quote (("wayland"))))))

