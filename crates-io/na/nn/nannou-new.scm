(define-module (crates-io na nn nannou-new) #:use-module (crates-io))

(define-public crate-nannou-new-0.1.0 (c (n "nannou-new") (v "0.1.0") (d (list (d (n "cargo") (r "^0.28") (d #t) (k 0)) (d (n "names") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1jg4yiw1cf667wl5nzdw44zywr5igr8srp6fgrhb31sk19rs5x5b")))

(define-public crate-nannou-new-0.1.1 (c (n "nannou-new") (v "0.1.1") (d (list (d (n "cargo") (r "^0.28") (d #t) (k 0)) (d (n "names") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0yaqn20xgsvq7503zrp5f1slzcq91jm0sv3q6m50wlj40hxqavvy")))

