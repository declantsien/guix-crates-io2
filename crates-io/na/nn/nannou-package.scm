(define-module (crates-io na nn nannou-package) #:use-module (crates-io))

(define-public crate-nannou-package-0.1.0 (c (n "nannou-package") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "copy_dir") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "zip") (r "^0.3") (d #t) (k 0)))) (h "179ywlda724ij4ng3m4lr4s1vmzzgj5kbqgdz8f67dkaq343fh1v")))

