(define-module (crates-io na nn nannou_laser) #:use-module (crates-io))

(define-public crate-nannou_laser-0.3.0 (c (n "nannou_laser") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "0v6l0cva077ffv7hnjclv8978qaw566ixkr73a38apgkvb6hhc7b")))

(define-public crate-nannou_laser-0.14.0 (c (n "nannou_laser") (v "0.14.0") (d (list (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "017smr4yk4qvmixn2546ilikphinjvrwff18nffx02s7g3gw06bp") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.14.1 (c (n "nannou_laser") (v "0.14.1") (d (list (d (n "ether-dream") (r "^0.2") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jqdy4x2ajd4f948zy45rwkrznk9hh01r8s8h96sf7i7f68ji2vg") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.14.2 (c (n "nannou_laser") (v "0.14.2") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0p60fi8kyfld34l1wf7dy64b5xbdhvgk00s1dmn7r5wnfv5an0l1") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.14.3 (c (n "nannou_laser") (v "0.14.3") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17f7y8h0qxm7kys4pgn8mgrwrw5j906chsfzws4r87wz8pqnmicd") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.15.0 (c (n "nannou_laser") (v "0.15.0") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hkgj1i92d2pdw619cpj099rjh6541shi38g7c8wds9lky8bdfvy") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.16.0 (c (n "nannou_laser") (v "0.16.0") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rssl3vlp60sc12yzf5jhvjg6df3hjilf272x3ssak0hmrw9x2cp") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.17.0 (c (n "nannou_laser") (v "0.17.0") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0nymrmp01n86zmlifwq8v1vszxprhhpd9jjny8mvsgmgz0j3qr67") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.18.0 (c (n "nannou_laser") (v "0.18.0") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "031h5hml60r60pc8rgxqvp4pjfb9vfk18lvk9g12mplp3n0mmx9k") (f (quote (("ffi"))))))

(define-public crate-nannou_laser-0.19.0 (c (n "nannou_laser") (v "0.19.0") (d (list (d (n "ether-dream") (r "~0.2.5") (d #t) (k 0)) (d (n "ilda-idtf") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lasy") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cd3aax4x1fj4jsw3pw13wk1g1vlk4iqivflj4mhnjyvhgcvca6a") (f (quote (("ffi"))))))

