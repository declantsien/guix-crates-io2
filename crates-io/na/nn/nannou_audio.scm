(define-module (crates-io na nn nannou_audio) #:use-module (crates-io))

(define-public crate-nannou_audio-0.1.0 (c (n "nannou_audio") (v "0.1.0") (d (list (d (n "cpal") (r "^0.8") (d #t) (k 0)) (d (n "sample") (r "^0.10") (d #t) (k 0)))) (h "16sv5f4nnrdxw8c21lspnafjpykq7chc2d650sx40zbv2im0jy0w")))

(define-public crate-nannou_audio-0.2.0 (c (n "nannou_audio") (v "0.2.0") (d (list (d (n "cpal") (r "^0.10") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "sample") (r "^0.10") (d #t) (k 0)))) (h "1v44j8in6m0c7xqgl28xbc7v5zni72gwlarhilh8lr73v9zr6g1w") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.14.0 (c (n "nannou_audio") (v "0.14.0") (d (list (d (n "cpal") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "sample") (r "^0.10") (d #t) (k 0)))) (h "1zycignc6rsp690p12bc0nzwica95jqhmapsv0wibnhvicxri74x") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.15.0 (c (n "nannou_audio") (v "0.15.0") (d (list (d (n "cpal") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "sample") (r "^0.10") (d #t) (k 0)))) (h "1drslm106qd5vw68w18h21qbzc2qzjwqg938lrly37f1fcb3w061") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.16.0 (c (n "nannou_audio") (v "0.16.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lxx6p8hbpxjplz4pfpa09fd4438d03z52hr6zsbdk92f3lil36z") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.17.0 (c (n "nannou_audio") (v "0.17.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0c9arh0nwlkqhs1ig7hig3678c6w6ncnq7a90pr4cfmmwr3inb5n") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.18.0 (c (n "nannou_audio") (v "0.18.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "191fvnhs9anmhncznshnrxpdnqi98ihvnc1qpz0zp85w4cklqqb5") (f (quote (("asio" "cpal/asio"))))))

(define-public crate-nannou_audio-0.19.0 (c (n "nannou_audio") (v "0.19.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 0)) (d (n "dasp_sample") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0g1nm36wgy6n5bdgk14irv38waqhk699rgdan7ph2s76imvssd7y") (f (quote (("asio" "cpal/asio"))))))

