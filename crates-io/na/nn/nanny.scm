(define-module (crates-io na nn nanny) #:use-module (crates-io))

(define-public crate-nanny-0.0.1 (c (n "nanny") (v "0.0.1") (d (list (d (n "nanny-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0xik0d6ly3nv90dva7wsnsrv4gqvbmfns3iry2mffddibf4hqvx1")))

(define-public crate-nanny-0.0.2 (c (n "nanny") (v "0.0.2") (d (list (d (n "nanny-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0g6a297kdz864wd1s9g7ldn8q0bv3xg6rynxf24vghgvd93jpcn4")))

(define-public crate-nanny-0.0.3 (c (n "nanny") (v "0.0.3") (d (list (d (n "nanny-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1yblr95w4ig9d4cgp8fhqjixw62h1513pn98lgcmi54lh04sd3fk")))

(define-public crate-nanny-0.0.4 (c (n "nanny") (v "0.0.4") (d (list (d (n "nanny-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0mbxqvm3nn0ldi6al7bc0hwjsl8rvlixrr11p0zk2l2j7d3zkmnn")))

(define-public crate-nanny-0.0.5 (c (n "nanny") (v "0.0.5") (d (list (d (n "nanny-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1ahj35ydr6jwri759mi9l5hhy7y8rnk2cdk34jiwyjchzsx7bij5")))

(define-public crate-nanny-0.0.6 (c (n "nanny") (v "0.0.6") (d (list (d (n "nanny-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0gxg0c1pl26dazldpm1zl6shrmgc1cman22pvdwyb65y19vas45n")))

(define-public crate-nanny-0.0.7 (c (n "nanny") (v "0.0.7") (d (list (d (n "nanny-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0sphwa48z884fkxn5r6lr876nzm7lgddqm7x6638jx8zqa2d5ndv")))

(define-public crate-nanny-0.0.8 (c (n "nanny") (v "0.0.8") (d (list (d (n "nanny-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1pdyf2mlhnhzx4sqxcq3jnwvpx8fb4hpp8x5p701aa0hizf77r30")))

(define-public crate-nanny-0.0.9 (c (n "nanny") (v "0.0.9") (d (list (d (n "nanny-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1813cpibydcnqwkjy5pbmzx81yackmz5njb61yibq59ycpl6l5kd")))

