(define-module (crates-io na nn nanny-sys) #:use-module (crates-io))

(define-public crate-nanny-sys-0.0.1 (c (n "nanny-sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1c6fxmq3x8g6pv02fpj0z05ffmcrsacifys002clp6clfddsggnf")))

(define-public crate-nanny-sys-0.0.2 (c (n "nanny-sys") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0qpz0pl1m4j2sj2wi847bgj6i8y8sn23a7n39bx296yjlcdamm9b")))

(define-public crate-nanny-sys-0.0.3 (c (n "nanny-sys") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "06q9w81n879qmqdzi22x8vw8i88kg5g4rj70h0ymfnp56bynnh5x")))

(define-public crate-nanny-sys-0.0.4 (c (n "nanny-sys") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0a7n0z3nhjdpsbbk3fp6pzmqidpmyq544bqmmg6hi0am429l46c9")))

(define-public crate-nanny-sys-0.0.5 (c (n "nanny-sys") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0sa7hvby2vfy2m6xlcp6gv8xsdrj7qa63ml6fzrbdn4cvj9hhpga")))

(define-public crate-nanny-sys-0.0.6 (c (n "nanny-sys") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1s70y707ny5kizrdlbw60gjfq6cjngjlp7mif2i3hy1gc72md770")))

