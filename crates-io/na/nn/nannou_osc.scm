(define-module (crates-io na nn nannou_osc) #:use-module (crates-io))

(define-public crate-nannou_osc-0.1.0 (c (n "nannou_osc") (v "0.1.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "1mkvxg8sffd15h69dq1yi9a4h2h7nj4r5vxhyq13lizkq3cia463")))

(define-public crate-nannou_osc-0.14.0 (c (n "nannou_osc") (v "0.14.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "1b6nkfg0hr6m98plna3750m50pdpmijgy2yw6s1wf3alskaxsvzr")))

(define-public crate-nannou_osc-0.15.0 (c (n "nannou_osc") (v "0.15.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "0mzzr4ij053hjbylkndslc6wq43lbcfp8z6zfmnrmbw0zwhms759")))

(define-public crate-nannou_osc-0.16.0 (c (n "nannou_osc") (v "0.16.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "13jmaiqrgz7xnsqxw2z74szyzqbik9m0kycy5m3dx7rf9z7l22fw")))

(define-public crate-nannou_osc-0.17.0 (c (n "nannou_osc") (v "0.17.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "1n925azwjs58brw5s5kr6mna4wj202p5cdkmw8g24x8irn3chri0")))

(define-public crate-nannou_osc-0.18.0 (c (n "nannou_osc") (v "0.18.0") (d (list (d (n "rosc") (r "^0.1") (d #t) (k 0)))) (h "0p3qfisjg8m036r3pzzi15jb89xv3qg2cvq5m82hmrgrbag5x6gr")))

(define-public crate-nannou_osc-0.19.0 (c (n "nannou_osc") (v "0.19.0") (d (list (d (n "rosc") (r "^0.10") (d #t) (k 0)))) (h "109vic2n9sgy1vwah1cy2n54vqr494fqx0z92zg63xbws5fr4ki3")))

