(define-module (crates-io na mu namui-panic-hook) #:use-module (crates-io))

(define-public crate-namui-panic-hook-0.1.0 (c (n "namui-panic-hook") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "099mqlixr1b3hpvrlsawarnr1mfcqsarpwpvsvbn7c8qsnbxiwxh")))

(define-public crate-namui-panic-hook-0.1.1 (c (n "namui-panic-hook") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "0n5xcdijf1537kcw0smvz4n0gawl1yh6m7n2gyi0l5c0apfab994")))

(define-public crate-namui-panic-hook-0.1.2 (c (n "namui-panic-hook") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "094n414rzdm8pc4403k0z7vndm4y4bxpzsf13rc4alsw97g5p9ar")))

