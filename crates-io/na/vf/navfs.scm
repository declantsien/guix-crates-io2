(define-module (crates-io na vf navfs) #:use-module (crates-io))

(define-public crate-navfs-0.1.0 (c (n "navfs") (v "0.1.0") (h "0rca3ik4v32lwkxlgxas1v5rpidlsry7n9rg3i0aigyknw80544g")))

(define-public crate-navfs-0.1.1 (c (n "navfs") (v "0.1.1") (h "0vxkd0a9gb1fq83r653rs04mhb6csj8c5kplppbdk98j6rnppdlp")))

(define-public crate-navfs-0.1.2 (c (n "navfs") (v "0.1.2") (d (list (d (n "text-colorizer") (r "^1.0.0") (d #t) (k 0)))) (h "121rbjj07nagqymdr7vmwq9h9r2hrg0zq5rmc8bh8pgn539dz5s5")))

(define-public crate-navfs-0.1.3 (c (n "navfs") (v "0.1.3") (h "1ybypwxrp6zydb9f192b6nf5vwyhx1pkfk5ay927ssm9ywh46a5d")))

