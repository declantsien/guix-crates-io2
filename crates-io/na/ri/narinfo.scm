(define-module (crates-io na ri narinfo) #:use-module (crates-io))

(define-public crate-narinfo-1.0.0 (c (n "narinfo") (v "1.0.0") (d (list (d (n "derive_builder") (r "^0.11.2") (k 0)))) (h "19jj2fq0inziyawrb45n4g9k24993ns3ir6zqzg272qmlwdkc2f4")))

(define-public crate-narinfo-1.0.1 (c (n "narinfo") (v "1.0.1") (d (list (d (n "derive_builder") (r "^0.11.2") (k 0)))) (h "1qm1nk8dzn8adsidhhg6v50v9b6mkmzjp8nr0y9mlgvf6w7jr75p")))

