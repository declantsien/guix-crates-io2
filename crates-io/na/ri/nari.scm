(define-module (crates-io na ri nari) #:use-module (crates-io))

(define-public crate-nari-0.2.1 (c (n "nari") (v "0.2.1") (d (list (d (n "file-lock") (r "^2.1") (d #t) (k 0)) (d (n "notify") (r "^5.1") (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("rt" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0cqds8xz5zz4dyq1z2q2w0l708kq0zdz0qj392nr5czrw2hd74pv")))

