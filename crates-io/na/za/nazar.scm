(define-module (crates-io na za nazar) #:use-module (crates-io))

(define-public crate-nazar-0.1.0 (c (n "nazar") (v "0.1.0") (d (list (d (n "redis") (r "^0.5.3") (d #t) (k 0)))) (h "10mf8lxiihypf279nadgfhbwsny06p5pjjsl6nq1jkwps749sr8h")))

(define-public crate-nazar-1.0.0 (c (n "nazar") (v "1.0.0") (d (list (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "00qszhbn66ryshcx5vnkx37lryrwvjf6f5by1p18qcv3nddzhp67")))

(define-public crate-nazar-1.0.1 (c (n "nazar") (v "1.0.1") (d (list (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "0nycnc5g1sm6f579lrwq3z4fwgp7li8xzxbg753h788674axalm9")))

(define-public crate-nazar-1.0.2 (c (n "nazar") (v "1.0.2") (d (list (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "12d9cvkw8cd8xwsl03b7mfwjbxsx693nf9swpb9ivsmqxdzc9csm")))

(define-public crate-nazar-1.0.3 (c (n "nazar") (v "1.0.3") (d (list (d (n "geojson") (r "^0.8.1") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "0b183z4x60dpyrlpdr41fa6czjwra4j11a7lkqvidk84s9axb6ka")))

(define-public crate-nazar-1.0.4 (c (n "nazar") (v "1.0.4") (d (list (d (n "geojson") (r "^0.8.1") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "06i1xg62gh3s0zafajz1y21ijsymk3mg4d9jpzibzzpjr11n9sp2")))

(define-public crate-nazar-1.0.5 (c (n "nazar") (v "1.0.5") (d (list (d (n "geojson") (r "^0.8.1") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "08w53hhn0pqwwp24p5x0vlkms3n71syrc08w147azz3kzyb4f7si")))

(define-public crate-nazar-1.0.6 (c (n "nazar") (v "1.0.6") (d (list (d (n "geojson") (r "^0.8.1") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "0ygi1qv74i9ilslpmqgvyia2sbaalw99zfdybxpr3pr8j3wyw02y")))

(define-public crate-nazar-1.0.7 (c (n "nazar") (v "1.0.7") (d (list (d (n "geojson") (r "^0.8.1") (d #t) (k 0)) (d (n "redis") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "148ml62dc4ak52l1arpscjymznvaw6r2s879rig9g5lq099dqfar")))

