(define-module (crates-io na ve naver) #:use-module (crates-io))

(define-public crate-naver-0.1.0 (c (n "naver") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1d267pybw4cdc4j48fcprw9md6pgh3anjrqk901v28mqaz86zwrv")))

(define-public crate-naver-0.1.1 (c (n "naver") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "0m07r7ix44cbpckjgx2239f8b8vf43nm8d7wdvs0l2cc4b9m7ya5")))

(define-public crate-naver-0.1.2 (c (n "naver") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "select") (r "^0.6.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)))) (h "1mhq89wxlkhifya0nagl30jcgh8ncpn2hrdfscr3j71zphc9a0y6")))

