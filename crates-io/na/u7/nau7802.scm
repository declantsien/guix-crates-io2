(define-module (crates-io na u7 nau7802) #:use-module (crates-io))

(define-public crate-nau7802-0.1.0-alpha (c (n "nau7802") (v "0.1.0-alpha") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0s3aww5fqj2lfipkwp5i75x246arvkgjrdzbnfcr746npyr33nb0") (f (quote (("embedded-hal-adc" "embedded-hal/unproven") ("default"))))))

