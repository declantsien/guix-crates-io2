(define-module (crates-io na rt narthex_web_app) #:use-module (crates-io))

(define-public crate-narthex_web_app-0.1.0 (c (n "narthex_web_app") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "narthex_engine_trait") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "08l3zplnq3rjp7vlrq332jdsr58197s2hn5bb5p9g2wccqgrcs0i")))

(define-public crate-narthex_web_app-0.1.1 (c (n "narthex_web_app") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "narthex_engine_trait") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "web-view") (r "^0.7.3") (d #t) (k 0)))) (h "0i16bf61zjfk8x1kvgw1ib6fyv4isi48bnzm7yjifq7481qm2mwx")))

