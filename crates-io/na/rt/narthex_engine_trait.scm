(define-module (crates-io na rt narthex_engine_trait) #:use-module (crates-io))

(define-public crate-narthex_engine_trait-0.1.0 (c (n "narthex_engine_trait") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g9zks74q1762mb31lyanyz1qgqx5645y0krb0yhyp6afrspibip")))

(define-public crate-narthex_engine_trait-0.1.1 (c (n "narthex_engine_trait") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rb5r6irqlfr1wxwim93rlp5x78cdvvrk6ahpkdhm3fqdhll8rz1")))

(define-public crate-narthex_engine_trait-0.1.2 (c (n "narthex_engine_trait") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "strum") (r "^0.22.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06fvg7yi70ihc1ja96frijdw4h4cdzja29i7k04a6l68wvkr5h87")))

