(define-module (crates-io na lg nalgebra-py) #:use-module (crates-io))

(define-public crate-nalgebra-py-0.3.1 (c (n "nalgebra-py") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.6") (d #t) (k 0)))) (h "078s1pj57sjsb71j1dk5iw1rd5y1n15f3h37nrvg063m8qpprcm6")))

