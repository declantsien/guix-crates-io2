(define-module (crates-io na lg nalgebra-numpy) #:use-module (crates-io))

(define-public crate-nalgebra-numpy-0.1.0 (c (n "nalgebra-numpy") (v "0.1.0") (d (list (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "1wdm14j9nn154j9fdlgyqq6cdi5g98a3rggdihkhi4c15k1ak4jz")))

(define-public crate-nalgebra-numpy-0.1.1 (c (n "nalgebra-numpy") (v "0.1.1") (d (list (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "04sswx4sbnqh8g07v86d1c8rz3kas46nfh81g2ixczjds8psd2wc")))

(define-public crate-nalgebra-numpy-0.1.2 (c (n "nalgebra-numpy") (v "0.1.2") (d (list (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "1gma0qw17f54plbpfq2s0zy5cn6x65qafcsgv88921kwb0vd034i")))

(define-public crate-nalgebra-numpy-0.1.3 (c (n "nalgebra-numpy") (v "0.1.3") (d (list (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "1v2yk74rnk3nvbwhsjczfaid6739p172j44j232xv5qfhrv0xjvd")))

(define-public crate-nalgebra-numpy-0.2.0 (c (n "nalgebra-numpy") (v "0.2.0") (d (list (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "0k6zdlw55cvmdid6g7m3j29yqb0djj0gqa94a5jvihdxli8y4p6i")))

(define-public crate-nalgebra-numpy-0.2.1 (c (n "nalgebra-numpy") (v "0.2.1") (d (list (d (n "assert2") (r "^0.1.1") (d #t) (k 2)) (d (n "inline-python") (r "^0.3.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "numpy") (r "^0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.2") (d #t) (k 0)))) (h "082v0ndqrdqcv845fc5k5scx33xpc5ds3fwdhy6lx3a9v2jwjnnr")))

(define-public crate-nalgebra-numpy-0.3.0 (c (n "nalgebra-numpy") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.4") (d #t) (k 2)) (d (n "inline-python") (r "^0.6.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "numpy") (r "^0.11") (d #t) (k 0)) (d (n "pyo3") (r "^0.11") (d #t) (k 0)))) (h "01a0ka80azraq0qc500fsc4waldzx4a454ys7w7p9yfn02ph1zs9")))

