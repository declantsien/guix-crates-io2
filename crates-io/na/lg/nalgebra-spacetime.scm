(define-module (crates-io na lg nalgebra-spacetime) #:use-module (crates-io))

(define-public crate-nalgebra-spacetime-0.1.0 (c (n "nalgebra-spacetime") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0ha1r3khj9qvfkci2wrrqbs3hyn1czi3z5lxrpxygc40c7b4a0sq")))

(define-public crate-nalgebra-spacetime-0.1.1 (c (n "nalgebra-spacetime") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "12c272akfhrdfs2fixxy89bijs0pm92nk2hxcmcrhd89kfhdbicj")))

(define-public crate-nalgebra-spacetime-0.1.2 (c (n "nalgebra-spacetime") (v "0.1.2") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1almjvx9g131sqfmbbkmjdm9ap4bg8p2kpj65qm63izjc7k6mmni")))

(define-public crate-nalgebra-spacetime-0.2.0 (c (n "nalgebra-spacetime") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0h3gmayjibdsha1c14y5ydzdny3w0f9h3m2vk1bixw095hlralnc")))

(define-public crate-nalgebra-spacetime-0.2.1 (c (n "nalgebra-spacetime") (v "0.2.1") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0bxw46244g0sw4ybgqh0j1sk0fivk2yggl5572f61i598a2lxbdk")))

(define-public crate-nalgebra-spacetime-0.2.2 (c (n "nalgebra-spacetime") (v "0.2.2") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14xgzy16d87zk774g3iaym27j2c17y27r414avchd9jbch5zbvg7")))

(define-public crate-nalgebra-spacetime-0.2.3 (c (n "nalgebra-spacetime") (v "0.2.3") (d (list (d (n "approx") (r "^0.4") (k 0)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1v2hny6hb876s125593vfpvl6jm92pm761z7nf886av3z4ij40q5")))

(define-public crate-nalgebra-spacetime-0.2.4 (c (n "nalgebra-spacetime") (v "0.2.4") (d (list (d (n "approx") (r "^0.4") (k 0)) (d (n "nalgebra") (r "^0.25") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0axqskjrm9nfc6m6i6mfshp5w4rakpln940ls3gf61xvm3v2cjl7")))

(define-public crate-nalgebra-spacetime-0.3.0 (c (n "nalgebra-spacetime") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "nalgebra") (r "^0.32.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1w7jj9iwc5qj5195zhr2gny0s4ryx5vhk2f36jgg80bnbh8cl3yp")))

(define-public crate-nalgebra-spacetime-0.4.0 (c (n "nalgebra-spacetime") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.1") (k 0)) (d (n "nalgebra") (r "^0.32.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0jl0lv0jpgcmrvrapgj8azw2ardzz1h5yix0mvz3ahaqj96x2874")))

