(define-module (crates-io na lg nalgebra-macros) #:use-module (crates-io))

(define-public crate-nalgebra-macros-0.1.0 (c (n "nalgebra-macros") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.26.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "063jvvvlwmzzxfr4wyiil2cn1yqj3arvghwsr2nk4ilv2jwc1z01")))

(define-public crate-nalgebra-macros-0.2.0 (c (n "nalgebra-macros") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0ri747snn7jfwp4yz1fw1vyrqs27gvpk7lx4ll89ijf0hj4cccnj")))

(define-public crate-nalgebra-macros-0.2.1 (c (n "nalgebra-macros") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.32.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "166rzbzi1hcyjfvwxmrdimrcmflvxxifjfkqxxkdjfnhcznilxli")))

