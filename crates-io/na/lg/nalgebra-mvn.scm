(define-module (crates-io na lg nalgebra-mvn) #:use-module (crates-io))

(define-public crate-nalgebra-mvn-0.1.0 (c (n "nalgebra-mvn") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0lm57kdfwrrpgl1mjz7qixa45dgr7gaxrvi57glavrwmv0d4vb5b")))

(define-public crate-nalgebra-mvn-0.1.1 (c (n "nalgebra-mvn") (v "0.1.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1q61sk6q5dfk333fs7vk07rjs50prva3v4h9bspny7i152shglfw")))

(define-public crate-nalgebra-mvn-0.2.0 (c (n "nalgebra-mvn") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)))) (h "0i4w4mh02hf7kl1w8xp05xry7bvi6cv4dy938zqcrl8vf2c9s9qq")))

(define-public crate-nalgebra-mvn-0.3.0 (c (n "nalgebra-mvn") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)))) (h "013m4dh90skra5xxx0dfr649r94jw78xs2lcd2x638rn90xbpiaj")))

(define-public crate-nalgebra-mvn-0.4.0 (c (n "nalgebra-mvn") (v "0.4.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)))) (h "16agpxa4apyk5mlzjm7vvqpl4cz80p7p0xg26nbysr5vxb90v37d")))

(define-public crate-nalgebra-mvn-0.5.0 (c (n "nalgebra-mvn") (v "0.5.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.23") (d #t) (k 0)))) (h "14vlydnfc4jb5xx15ywdi5c8lpm049d76xhq1f8x84liavx0fdzw")))

(define-public crate-nalgebra-mvn-0.6.0 (c (n "nalgebra-mvn") (v "0.6.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.24") (d #t) (k 0)))) (h "0qq98ff0xizafjxp6ha0x1xr3myndwg9c1g6w6n430g92f15k50r")))

(define-public crate-nalgebra-mvn-0.7.0 (c (n "nalgebra-mvn") (v "0.7.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)))) (h "0b6li6jl4jjclcf4kg0dsgzzvxv3rf5p5mvgabkrnfn63wcbcldc")))

(define-public crate-nalgebra-mvn-0.8.0 (c (n "nalgebra-mvn") (v "0.8.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)))) (h "1a325gcllsqjjdbawszn47i9plvybnrld26ldbjvh80ixd1k40fm")))

(define-public crate-nalgebra-mvn-0.9.0 (c (n "nalgebra-mvn") (v "0.9.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.27") (d #t) (k 0)))) (h "16pf58g2xfy73hsg6by385ifm1lghyhljc4i4k3496ym27ami0hw")))

(define-public crate-nalgebra-mvn-0.10.0 (c (n "nalgebra-mvn") (v "0.10.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.28") (d #t) (k 0)))) (h "1z1al7fz7dyvkr57f33kygf6l2yadz9r94g67wdpr500a7c9pcw6")))

(define-public crate-nalgebra-mvn-0.11.0 (c (n "nalgebra-mvn") (v "0.11.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29") (d #t) (k 0)))) (h "0n9s51mysipq6v9jpjxzmk3hc1zpyjq16y9w9kvr4k1izb2d5ndl")))

(define-public crate-nalgebra-mvn-0.12.0 (c (n "nalgebra-mvn") (v "0.12.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)))) (h "14szap87273gx0dvl8in42rgnfpa1fmh2vlfd89dvxn1qwqj3n0h")))

(define-public crate-nalgebra-mvn-0.13.0 (c (n "nalgebra-mvn") (v "0.13.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.31") (d #t) (k 0)))) (h "0qfvj20vhyfqf32km6m6vi1c9mg543xz9v8rbwllx75x9qr8qfbw")))

(define-public crate-nalgebra-mvn-0.14.0 (c (n "nalgebra-mvn") (v "0.14.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1z1cxy5d14g232ss2qz7cpqsc3m2q7d0dljnshx4nr5krpbsshkc")))

