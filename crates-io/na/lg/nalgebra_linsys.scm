(define-module (crates-io na lg nalgebra_linsys) #:use-module (crates-io))

(define-public crate-nalgebra_linsys-0.1.0 (c (n "nalgebra_linsys") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08cb7iq3v65qmg04jvljvv22vci2zcxywn4482q5xd270zcbahi2") (r "1.56.1")))

(define-public crate-nalgebra_linsys-0.1.1 (c (n "nalgebra_linsys") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1g68yfrqhr4nbp4mcvf9xj30yvj8qz0pmc4363dk0fxcyqc6hym5") (r "1.56.1")))

