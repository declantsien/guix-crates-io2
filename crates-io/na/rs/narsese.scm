(define-module (crates-io na rs narsese) #:use-module (crates-io))

(define-public crate-narsese-0.13.0 (c (n "narsese") (v "0.13.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "054bwslmnv5dysqzd4953vriwfdpsmzhk7mcy9v9snn6gzilifrc") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.14.0 (c (n "narsese") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0rb8wmj6bx1b4whkifc0p7lxn8gwdjcn5z3xyx5jg9mihiw8a2a5") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.15.0 (c (n "narsese") (v "0.15.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0axc846c18q11l5lk1vcvrgidq5yb1daiack92wx2iswihr2jayx") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.16.0 (c (n "narsese") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1zzh6msdifb82ji6q2r8zl4bzzr7dg8raflqdnmmpih7rp3izrh7") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.17.0 (c (n "narsese") (v "0.17.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "13svg96y8dqfbznxzdn6qigpcy9jnh4l7jj3c3n31p3nic9f7q7s") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.18.0 (c (n "narsese") (v "0.18.0") (d (list (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "nar_dev_utils") (r "^0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "0mznvq0awx369kv8cd3gb9b0vpf3wm4zjdrxvm7351lmf0sawyys") (f (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

