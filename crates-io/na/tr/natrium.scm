(define-module (crates-io na tr natrium) #:use-module (crates-io))

(define-public crate-natrium-0.1.0 (c (n "natrium") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)))) (h "0r5cyjcvw5bv0imigijbdwl3705sb3p09k3ckpjldi9ra654zmkj")))

(define-public crate-natrium-0.2.0 (c (n "natrium") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0ns14xpdj815sx4v5kf3z0m0xnix7cfgzggd60qfyflapwfs0gqq")))

