(define-module (crates-io na te nate-engine-core) #:use-module (crates-io))

(define-public crate-nate-engine-core-0.1.0 (c (n "nate-engine-core") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1iw3b3sw90x7hwi63a8f5r7mmm0yj78a65xip5v0zwv3js18d9zn")))

(define-public crate-nate-engine-core-0.1.1 (c (n "nate-engine-core") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "11cjqvq540cz2ahhp11g8if16qx28kxhiad0dnhbd60ybfvjksmx")))

(define-public crate-nate-engine-core-0.1.2 (c (n "nate-engine-core") (v "0.1.2") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "18dxdz25hrq3rmbvk7ig3qygqq25ggrjc7zlz4a0850gbary1nfl")))

(define-public crate-nate-engine-core-0.1.3 (c (n "nate-engine-core") (v "0.1.3") (d (list (d (n "ctrlc") (r "^3.4.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "12f5n4gks2b283arwrfv8g4p440mfdk677dy99z92fcw9rz9lk6w")))

