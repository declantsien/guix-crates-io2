(define-module (crates-io na te nate) #:use-module (crates-io))

(define-public crate-nate-0.1.0 (c (n "nate") (v "0.1.0") (d (list (d (n "nate-common") (r "^0.1.0") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.0") (d #t) (k 0)))) (h "01wynadrdpvxx0cynqrynzds7fry1a3vhz6yzqhdbvpapkvx8jxr")))

(define-public crate-nate-0.1.1 (c (n "nate") (v "0.1.1") (d (list (d (n "nate-common") (r "^0.1.0") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0zgy5jc3934jj1has7qy3nri99wq3kijx8y932hz4nh36l03kcsf")))

(define-public crate-nate-0.1.2 (c (n "nate") (v "0.1.2") (d (list (d (n "nate-common") (r "^0.1.0") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.1") (d #t) (k 0)))) (h "192mr6m8zgi2fncplb3f90388gs7dsiiyr5vsnvlr02yjifsqz8x")))

(define-public crate-nate-0.1.3 (c (n "nate") (v "0.1.3") (d (list (d (n "nate-common") (r "^0.1.1") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.3") (d #t) (k 0)))) (h "1p1262d6svh28l2yr1r49jqc53ly9qmy22755zwj497amrzav160")))

(define-public crate-nate-0.1.4 (c (n "nate") (v "0.1.4") (d (list (d (n "nate-common") (r "^0.1.4") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.4") (d #t) (k 0)))) (h "0i8cqj50g8jf30p1b00rv79c5hlcirprg4i2bjns961hw4cchg55")))

(define-public crate-nate-0.1.5 (c (n "nate") (v "0.1.5") (d (list (d (n "nate-common") (r "^0.1.5") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.5") (d #t) (k 0)))) (h "14zxwz82q8sdhia3s6f2wlcqh9xvyyj44ziadg055zm7yp9i969y")))

(define-public crate-nate-0.1.6 (c (n "nate") (v "0.1.6") (d (list (d (n "nate-common") (r "^0.1.6") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.6") (d #t) (k 0)))) (h "0gyigqpy47flx36af8sr3i30qhiilq30si7b9piffzxm6z0mg9w7") (y #t)))

(define-public crate-nate-0.1.7 (c (n "nate") (v "0.1.7") (d (list (d (n "nate-common") (r "^0.1.7") (d #t) (k 0)) (d (n "nate-derive") (r "^0.1.6") (d #t) (k 0)))) (h "16d9bimyn8ychak4r7rsv5n0iix9hbshirvimwpilyrh6dp5s8bj")))

(define-public crate-nate-0.1.8 (c (n "nate") (v "0.1.8") (d (list (d (n "nate-common") (r "^0.1.8") (k 0)) (d (n "nate-derive") (r "^0.1.8") (k 0)))) (h "0v877dcbdjzxz9jfqzaw7ni8jyhj72jfilx76fij5dfig18n113z") (f (quote (("std" "alloc" "nate-common/std") ("default") ("alloc" "nate-common/alloc"))))))

(define-public crate-nate-0.1.9 (c (n "nate") (v "0.1.9") (d (list (d (n "document-features") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "nate-common") (r "^0.1.9") (k 0)) (d (n "nate-derive") (r "^0.1.9") (k 0)))) (h "008nq9i95sa19kv44fnp0c21ck5n67f1qrwi0gwszszg99wnfzsv") (f (quote (("std" "alloc" "nate-common/std") ("docsrs" "nate-common/docsrs" "document-features") ("default" "std" "alloc") ("alloc" "nate-common/alloc"))))))

(define-public crate-nate-0.1.10 (c (n "nate") (v "0.1.10") (d (list (d (n "document-features") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "nate-common") (r "^0.1.10") (k 0)) (d (n "nate-derive") (r "^0.1.10") (k 0)))) (h "0jywf32i67h0ab4ydx5csq7ms6qngx77whvp440z48sn35zjsg31") (f (quote (("std" "alloc" "nate-common/std") ("docsrs" "nate-common/docsrs" "document-features") ("default" "std" "alloc") ("alloc" "nate-common/alloc"))))))

(define-public crate-nate-0.1.11 (c (n "nate") (v "0.1.11") (d (list (d (n "document-features") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "nate-common") (r "^0.1.10") (k 0)) (d (n "nate-derive") (r "^0.1.11") (k 0)))) (h "1v03wgjy6sp300wb89pchqci723cgcyyx34104fq59l4369ai525") (f (quote (("std" "alloc" "nate-common/std") ("docsrs" "nate-common/docsrs" "document-features" "default") ("default" "std" "alloc") ("alloc" "nate-common/alloc"))))))

(define-public crate-nate-0.2.0 (c (n "nate") (v "0.2.0") (d (list (d (n "nate-derive") (r "^0.2.0") (k 0)))) (h "1g8s1dzq9b5w3xqdb7g5yirbxawf2w876gi1iyv1k7nppgy45fi2") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-nate-0.2.1 (c (n "nate") (v "0.2.1") (d (list (d (n "nate-derive") (r "^0.2.1") (k 0)))) (h "1v96rljpc3j8qlm3h5gj8mp20v28rcidz83ay4yf2q7bsrqxwcnl") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-nate-0.2.2 (c (n "nate") (v "0.2.2") (d (list (d (n "nate-derive") (r "^0.2.1") (k 0)))) (h "14n7rq1w6yjwyhy52jr2v0cgaridbkvzs84f467bdad5jswzmich") (f (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-nate-0.3.0 (c (n "nate") (v "0.3.0") (d (list (d (n "itoa_") (r "=1") (o #t) (d #t) (k 0) (p "itoa")) (d (n "nate-derive") (r "^0.3.0") (k 0)) (d (n "ryu_") (r "=1") (o #t) (d #t) (k 0) (p "ryu")) (d (n "ryu_js_") (r "=0.2") (o #t) (d #t) (k 0) (p "ryu-js")))) (h "0dqcrclqdd71i3qpshfvhihqfms2gd5sia54h7igphkf9kx8lmaq") (f (quote (("std" "alloc") ("ryu-js" "ryu_js_") ("ryu" "ryu_") ("itoa" "itoa_") ("faster" "itoa" "ryu") ("docsrs" "default") ("default" "std" "faster") ("alloc"))))))

(define-public crate-nate-0.2.3 (c (n "nate") (v "0.2.3") (d (list (d (n "nate_") (r "=0.3") (k 0) (p "nate")))) (h "0dsw0wj2j132pns6g9abggz5rrv61g5jrwmafz4symsbs27m3dia") (f (quote (("std" "alloc" "nate_/std") ("default" "std" "alloc") ("alloc" "nate_/alloc"))))))

(define-public crate-nate-0.3.1 (c (n "nate") (v "0.3.1") (d (list (d (n "itoa_") (r "=1") (o #t) (d #t) (k 0) (p "itoa")) (d (n "nate-derive") (r "=0.3.1") (d #t) (k 0)) (d (n "ryu_") (r "=1") (o #t) (d #t) (k 0) (p "ryu")) (d (n "ryu_js_") (r "=0.2") (o #t) (d #t) (k 0) (p "ryu-js")))) (h "0hkrczh7nlnv7zasn1ggsd3r1ra7m5g96fr0bgrr5rhlf3x2q31z") (f (quote (("std" "alloc") ("ryu-js" "ryu_js_") ("ryu" "ryu_") ("itoa" "itoa_") ("faster" "itoa" "ryu") ("docsrs" "default") ("default" "std" "faster") ("alloc"))))))

(define-public crate-nate-0.3.2 (c (n "nate") (v "0.3.2") (d (list (d (n "itoa_") (r "=1") (o #t) (d #t) (k 0) (p "itoa")) (d (n "nate-derive") (r "=0.3.1") (d #t) (k 0)) (d (n "ryu_") (r "=1") (o #t) (d #t) (k 0) (p "ryu")) (d (n "ryu_js_") (r "=0.2") (o #t) (d #t) (k 0) (p "ryu-js")))) (h "0mclh8vcgvryy44lx9fxv47415h0li6fr6lc5w074aw3i2q80zax") (f (quote (("std" "alloc") ("ryu-js" "ryu_js_") ("ryu" "ryu_") ("itoa" "itoa_") ("faster" "itoa" "ryu") ("docsrs" "default") ("default" "std" "faster") ("alloc"))))))

(define-public crate-nate-0.3.3 (c (n "nate") (v "0.3.3") (d (list (d (n "itoa_") (r "=1") (o #t) (d #t) (k 0) (p "itoa")) (d (n "nate-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "ryu_") (r "=1") (o #t) (d #t) (k 0) (p "ryu")) (d (n "ryu_js_") (r "=0.2") (o #t) (d #t) (k 0) (p "ryu-js")))) (h "1rf1zyd6hrc38g54vv4lzrz047gn07vlr2dwrwyz5zb3bwcfcdmy") (f (quote (("std" "alloc") ("ryu-js" "ryu_js_") ("ryu" "ryu_") ("itoa" "itoa_") ("faster" "itoa" "ryu") ("docsrs" "default") ("default" "std" "faster") ("alloc"))))))

(define-public crate-nate-0.4.0 (c (n "nate") (v "0.4.0") (d (list (d (n "itoa") (r "=1") (d #t) (k 0)) (d (n "nate-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "ryu") (r "=1.0") (d #t) (k 0)))) (h "03lpd4df5pb81mhzx786xchpvh3x9sscv6m8b9y1227xidpb6axg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.56")))

