(define-module (crates-io na te nate-engine-macros) #:use-module (crates-io))

(define-public crate-nate-engine-macros-0.1.0 (c (n "nate-engine-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0frbp8rj2hpm5c45ambl0nvcy97qgfp1kj9hmf7nwa0q563dd90n")))

(define-public crate-nate-engine-macros-0.1.1 (c (n "nate-engine-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11dwvwmc96zkdm8cp3rcz9pg0mjr292p6lpzl2ibgpl9zwwbgwpv")))

(define-public crate-nate-engine-macros-0.1.2 (c (n "nate-engine-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jpzrh10r5zypnvsb632x5ni8qhj5syvc6wc7vl3268ag25di19x")))

(define-public crate-nate-engine-macros-0.1.3 (c (n "nate-engine-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "099brpc3prlwmg8jqlzz4bfd4h79d0p24aprawgfiik6ckrbd4ag")))

(define-public crate-nate-engine-macros-0.1.4 (c (n "nate-engine-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bncmimy9iyl91k8vj61z31qwd4wgpynq72xlzbq90x2dy1kaljx")))

