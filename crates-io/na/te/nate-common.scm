(define-module (crates-io na te nate-common) #:use-module (crates-io))

(define-public crate-nate-common-0.1.0 (c (n "nate-common") (v "0.1.0") (h "1314fj9gf6a7r4ysi8gyzps2n401va7r8jy9krlpqddc7mc170gn")))

(define-public crate-nate-common-0.1.1 (c (n "nate-common") (v "0.1.1") (h "0d8jpb4mbh274s8rl6266mrrzfs0swrm9zjmjql0ic47gdx7qsfm")))

(define-public crate-nate-common-0.1.4 (c (n "nate-common") (v "0.1.4") (h "0hhl7jam3j9cb1hyladhwd7b9spqilnq2lycp2xfb31ih5jqhmic")))

(define-public crate-nate-common-0.1.5 (c (n "nate-common") (v "0.1.5") (h "0bnbbvg1wy0cg8lrbazc4171959ih3gpy7l29hyr9y9nc8zqhabi")))

(define-public crate-nate-common-0.1.6 (c (n "nate-common") (v "0.1.6") (h "11acr7qq0i7q6c4vbs34wry7xzv1rpwqszrc3ihkhc7b02jwfgdf") (y #t)))

(define-public crate-nate-common-0.1.7 (c (n "nate-common") (v "0.1.7") (h "1r774f6rla8fp35djxfjixv1y3szjygirzi74blgxvqvpxi1wc0f")))

(define-public crate-nate-common-0.1.8 (c (n "nate-common") (v "0.1.8") (h "1c0vid4lwjn0h55y61g8kkvx13sf78n7zs7s1j22r5vibg7fj5k7") (f (quote (("std" "alloc") ("default") ("alloc"))))))

(define-public crate-nate-common-0.1.9 (c (n "nate-common") (v "0.1.9") (d (list (d (n "document-features") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "00xl2illg0mf1g2d00pj5n3yqcsgnjsnb7ww755mgccls4yr7zf8") (f (quote (("std" "alloc") ("docsrs" "document-features") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-nate-common-0.1.10 (c (n "nate-common") (v "0.1.10") (d (list (d (n "document-features") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "0b8jag27ns9hzc64yx3hq2w7jzg8n4hvf6wddz7c81lrwyr3f6gq") (f (quote (("std" "alloc") ("docsrs" "document-features") ("default" "std" "alloc") ("alloc"))))))

