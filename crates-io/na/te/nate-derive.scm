(define-module (crates-io na te nate-derive) #:use-module (crates-io))

(define-public crate-nate-derive-0.1.0 (c (n "nate-derive") (v "0.1.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xsq83v05mrjx4ias73k61690zvxw9f12ya8phkh7piwb3n1ahix")))

(define-public crate-nate-derive-0.1.1 (c (n "nate-derive") (v "0.1.1") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15fd8hbz3nr9c9h8kv742xwbb7b13j11m04wzrja22ycjww9x6wb")))

(define-public crate-nate-derive-0.1.2 (c (n "nate-derive") (v "0.1.2") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0i86ibxgzl712cbhynpfskzjbd9zszzbm9yx71wg8lmykcbcj549")))

(define-public crate-nate-derive-0.1.3 (c (n "nate-derive") (v "0.1.3") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgcmc7n05a1mp0kscbwsdhsac5vijywa50v8gb8r8g9xl7zzfcc")))

(define-public crate-nate-derive-0.1.4 (c (n "nate-derive") (v "0.1.4") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r ">=6, <8") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11l93f8pzgzwyhm6yz7wd7852h9hf7qfgp4kbi0pfd92g22qw84l")))

(define-public crate-nate-derive-0.1.5 (c (n "nate-derive") (v "0.1.5") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r ">=6, <8") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0cvapaimmv1j0pldvzps84rs0qhn9rx6p0wkr65j0ssiqal718wj")))

(define-public crate-nate-derive-0.1.6 (c (n "nate-derive") (v "0.1.6") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r ">=6, <8") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10x9xa16kndcphnlpwvrz120r2ih30knvfirzy8vaa0j6vwv8ci6")))

(define-public crate-nate-derive-0.1.8 (c (n "nate-derive") (v "0.1.8") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r ">=6, <8") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yd67k91msikvaq1bkw2zzxb6ql8kfj17b7ryv5k3fzvhbp34dpb")))

(define-public crate-nate-derive-0.1.9 (c (n "nate-derive") (v "0.1.9") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r ">=6, <8") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0apmm9g91drmmhvvbqwf14gqb79icsigmfqbkwxlddc7x8lxgyps")))

(define-public crate-nate-derive-0.1.10 (c (n "nate-derive") (v "0.1.10") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("proc-macro"))) (k 0)))) (h "1kr923wr6i5agz6ic4cdl0pxxswwis1qlfhyd8b77clsydganjbp") (f (quote (("docsrs") ("default"))))))

(define-public crate-nate-derive-0.1.11 (c (n "nate-derive") (v "0.1.11") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (k 0)) (d (n "syn") (r "^1.0.86") (k 0)))) (h "09vnhhxcsnn5dvicpr6l7avh72ycq3b9npq3lw4wqsjm7rm1dbhg") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.2.0 (c (n "nate-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.16") (k 0)) (d (n "syn") (r "^1.0.89") (k 0)))) (h "057xq4nnb254ivl8qmar0234mgbad0incmwhzsxrzffwwsgmg33c") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.2.1 (c (n "nate-derive") (v "0.2.1") (d (list (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "09hh85h2wr0czb07i8bfj15mp4l7xx25ghyq66lhcqaizpc5cslb") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.2.2 (c (n "nate-derive") (v "0.2.2") (d (list (d (n "blake2") (r "=0.10") (d #t) (k 0)) (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "hex") (r "=0.4") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "0yih9pia4v1vx6cchsf88d98igwim6inc1q76fvxncnrgjcq11vx") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.3.0 (c (n "nate-derive") (v "0.3.0") (d (list (d (n "blake2") (r "=0.10") (d #t) (k 0)) (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "hex") (r "=0.4") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "0ww43miswxbp9sa8yp6zl4qb5h4djj6lj7xdrgvyxsjcwdql83h7") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.3.1 (c (n "nate-derive") (v "0.3.1") (d (list (d (n "blake2") (r "=0.10") (d #t) (k 0)) (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "hex") (r "=0.4") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "1h1w0x739q16bzkmxx377j83vkcillbxypfbcha9g0rkps69jnrc") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.3.3 (c (n "nate-derive") (v "0.3.3") (d (list (d (n "blake2") (r "=0.10") (d #t) (k 0)) (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "hex") (r "=0.4") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "1xxwfpwd6n0zd81wxr9kmapa6cc2c9yr91rgkgmzsljqdkfkcg97") (f (quote (("docsrs" "default") ("default"))))))

(define-public crate-nate-derive-0.4.0 (c (n "nate-derive") (v "0.4.0") (d (list (d (n "blake2") (r "=0.10") (d #t) (k 0)) (d (n "darling") (r ">=0.12, <=0.14") (d #t) (k 0)) (d (n "hex") (r "=0.4") (d #t) (k 0)) (d (n "nom") (r "=7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "=1") (k 0)) (d (n "syn") (r "=1") (k 0)))) (h "1apf1989h0368yk68mnij89dya7x0zxalpnzh60i24q42gb8dih3") (r "1.56")))

(define-public crate-nate-derive-0.4.1 (c (n "nate-derive") (v "0.4.1") (d (list (d (n "blake2") (r "^0.10.5") (d #t) (k 0)) (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "1ajyjzy3i616xfljr44wrphsqs9izzwhv7rh8hv8ykk2z7pwmqzg") (r "1.56")))

(define-public crate-nate-derive-0.4.2 (c (n "nate-derive") (v "0.4.2") (d (list (d (n "blake2") (r "^0.10.5") (d #t) (k 0)) (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (k 0)) (d (n "syn") (r "^2.0.15") (k 0)))) (h "0p12668xmnki6xyh92r6ifipnscbaz4r9ahbwj9sl9anvqsq8036") (r "1.56")))

