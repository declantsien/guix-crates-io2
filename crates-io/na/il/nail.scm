(define-module (crates-io na il nail) #:use-module (crates-io))

(define-public crate-nail-0.1.0-pre.0 (c (n "nail") (v "0.1.0-pre.0") (d (list (d (n "clap") (r "~2.5.1") (d #t) (k 0)) (d (n "clippy") (r "~0.0.69") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "linenoise-rust") (r "~0.2.0") (d #t) (k 0)) (d (n "regex") (r "~0.1.71") (d #t) (k 0)) (d (n "repl") (r "~0.7.1") (d #t) (k 0)))) (h "11mhsbich86gp116m1wzdpzqr9nyd7xwvk7g462jfh1v6wlly3ja") (f (quote (("lint" "clippy"))))))

(define-public crate-nail-0.1.0 (c (n "nail") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libnail") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "thread_local") (r "^1.1.7") (d #t) (k 0)))) (h "108p5gd71mnyjhlmqk9wr43k1ig5p3mm7k9523q864qwdfj3gin4")))

