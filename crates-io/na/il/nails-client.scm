(define-module (crates-io na il nails-client) #:use-module (crates-io))

(define-public crate-nails-client-0.1.0 (c (n "nails-client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "06rvqll981klmlrswkx4az0k9hjfz57ga625ly5v1w9cbqfd3z7q")))

(define-public crate-nails-client-0.2.0 (c (n "nails-client") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.2.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1qziqjrf5ib1adbid2w4nfxbw8msnk5xx6azzd12mrq0l8wyx4xa")))

(define-public crate-nails-client-0.3.0 (c (n "nails-client") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "10shjin1ia8nnklnb3j0j6nay51xmqi26wdzdny2jbfh6j1p0cvf")))

(define-public crate-nails-client-0.4.0 (c (n "nails-client") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)))) (h "13q71afrszr6wixicyzck7jin4p90m1ccl71sms40qwa16klmzrl")))

(define-public crate-nails-client-0.4.1 (c (n "nails-client") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)))) (h "1wsl15f4cvj9drnprbh3886sdac92hbmb1s5nimvy8z3gs38mp3g")))

(define-public crate-nails-client-0.5.0 (c (n "nails-client") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)))) (h "1krgai4ka9k02a2d735s5gq11f9j1pw1xhvlbm2076imcxl7x57q")))

(define-public crate-nails-client-0.5.1 (c (n "nails-client") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "tcp"))) (d #t) (k 0)))) (h "1440ckxg1jpknyd26viad0i6zg2bqryhwzz56alv7g45h1r5ira5")))

(define-public crate-nails-client-0.5.2 (c (n "nails-client") (v "0.5.2") (d (list (d (n "env_logger") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "= 0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "1axymshgw976fidc6ibnpx0zx4mqsi2nby9qign6hvzgwlnxwkcp")))

(define-public crate-nails-client-0.6.0 (c (n "nails-client") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "1mzdcw7aaqz4fz88f03yrklkmph99w5p9qmzvnqm9hhz5s5bw088")))

(define-public crate-nails-client-0.7.0 (c (n "nails-client") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "0p8n17zyj30dmpvr75d2ws79v0jgq6iqqn6v9w09cilzi7gk0mc5")))

(define-public crate-nails-client-0.8.0 (c (n "nails-client") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "02ll39b0imfc6qq2hrx1wc08vqrdgvq50k6ry273ymzl50c4mj97")))

(define-public crate-nails-client-0.9.0 (c (n "nails-client") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "18pr8jzii7907q66gw8ja6cp1aha5dvr1d7hk51z37f39c9c9nvg")))

(define-public crate-nails-client-0.11.0 (c (n "nails-client") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-std" "io-util" "macros" "rt-threaded" "tcp"))) (d #t) (k 0)))) (h "1d1dafqci37imxiac5w1x1hdv6arm51agc8wbafxwqsns6pa5wan")))

(define-public crate-nails-client-0.12.0 (c (n "nails-client") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "io-std" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "05y7dnp3ph183xg1alnp8a26v5d87831w58r56xlq4xxk2ldbfnr")))

(define-public crate-nails-client-0.13.0 (c (n "nails-client") (v "0.13.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nails") (r "=0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "net" "io-std" "io-util" "rt-multi-thread"))) (d #t) (k 0)))) (h "011ib9lz7hh2mgdhsq8n64iaxz9hrmr4vvrd0wg19fgg3m1v68bj")))

