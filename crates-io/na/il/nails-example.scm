(define-module (crates-io na il nails-example) #:use-module (crates-io))

(define-public crate-nails-example-0.1.0 (c (n "nails-example") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nails") (r "= 0.1.0") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "15n651jfxx2gjrdvkm0avij9r822fi2yws4py5i3rfabhsc8skbx")))

(define-public crate-nails-example-0.2.0 (c (n "nails-example") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nails") (r "= 0.2.0") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.2.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1d7kzl8vf9rmvmhsrnrb4jm4jm0j8f5zwv4j42bcipgq0s7mq48n")))

(define-public crate-nails-example-0.3.0 (c (n "nails-example") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "nails") (r "= 0.3.0") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "02vly8kdbbhbjy2h50ylprzxixp5ddzxx1vsnj7yj3mkwgkmn9ip")))

(define-public crate-nails-example-0.4.0 (c (n "nails-example") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "= 0.4.0") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "stream" "tcp"))) (d #t) (k 0)))) (h "06fjlg5bjkz0b0vqq7ci59n94aalmg5d0czqv61lzpfb23sbzaf4")))

(define-public crate-nails-example-0.4.1 (c (n "nails-example") (v "0.4.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "= 0.4.1") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core" "stream" "tcp"))) (d #t) (k 0)))) (h "0ffl4inki8dx23fg85akngjasa2723iw0w742grqhwjs1v30hmgw")))

(define-public crate-nails-example-0.5.0 (c (n "nails-example") (v "0.5.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "= 0.5.0") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "0klgk9gg10fndfkmglqkzjpfph7ia4jrpgfgd2n64jwy639z0ndz")))

(define-public crate-nails-example-0.5.1 (c (n "nails-example") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "= 0.5.1") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "13cjvd69bi75lg7wrs1ihw8jiqgf8wh309lw7vcv2vji8lacaz0s")))

(define-public crate-nails-example-0.5.2 (c (n "nails-example") (v "0.5.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "= 0.5.2") (d #t) (k 0)) (d (n "nails-fork") (r "= 0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "0cbvpds9zljqrjvvzlc9vircs2jf169ggjnvj4vkf04z8k9givhn")))

(define-public crate-nails-example-0.6.0 (c (n "nails-example") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.6.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "0lxg4rii29rz90295v839yn902xwafy63rf7ygs2000hbm6vr4g5")))

(define-public crate-nails-example-0.7.0 (c (n "nails-example") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.7.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "04jjcgdq31mxm876wwdjbzk25wsdi42mm8p1s9w431cp7wipg291")))

(define-public crate-nails-example-0.8.0 (c (n "nails-example") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.8.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "0gdcccr6syc9v5cxrcl6i442kssb7mqxlsg7n3kyi0jyh7qacagk")))

(define-public crate-nails-example-0.9.0 (c (n "nails-example") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.9.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "18apkignzjzm9qmnfpdf5pvk7vil4hmwr5f21a1b7cz9b6q9y3mc")))

(define-public crate-nails-example-0.11.0 (c (n "nails-example") (v "0.11.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.11.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("dns" "macros" "rt-threaded" "stream" "tcp"))) (d #t) (k 0)))) (h "04ma6vn2r0wv6kpnjakilfqvlmld87gd040wy3w6j1n4zh06vmnf")))

(define-public crate-nails-example-0.12.0 (c (n "nails-example") (v "0.12.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.12.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "0z8kc1a3xcx9i8gcvyiq4ashrlijrxn4z64i2ynyyq5iz28x835z")))

(define-public crate-nails-example-0.13.0 (c (n "nails-example") (v "0.13.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nails") (r "=0.13.0") (d #t) (k 0)) (d (n "nails-fork") (r "=0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("macros" "net" "rt-multi-thread"))) (d #t) (k 0)))) (h "1s1b08h80m2pgda0019n159nhrr77mchad4l9ia7jr9q1nwlmz08")))

