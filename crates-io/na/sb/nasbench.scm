(define-module (crates-io na sb nasbench) #:use-module (crates-io))

(define-public crate-nasbench-0.1.0 (c (n "nasbench") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1b99cig79vy6cy2lsgah6swa86127lhvn90s3c6rvqd2zirg061m")))

(define-public crate-nasbench-0.1.1 (c (n "nasbench") (v "0.1.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.6") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1hfsn477ldmxhkq3wgsryddi7nim4pswhdf96ggcycl5akkdc15i")))

(define-public crate-nasbench-0.1.2 (c (n "nasbench") (v "0.1.2") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crc") (r "^1") (d #t) (k 0)) (d (n "md5") (r "^0.7") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0lvycq6y8yi3lk5bqai3px6y7j3h4y6pdxmnq5c7kvfjn9n6dfqz")))

