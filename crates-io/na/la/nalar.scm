(define-module (crates-io na la nalar) #:use-module (crates-io))

(define-public crate-nalar-0.0.1 (c (n "nalar") (v "0.0.1") (d (list (d (n "regex") (r "^1.8.3") (d #t) (k 0)))) (h "1ng5a25dlzrlngf9aw134fdpmajv2ff18hrmb3lya3widvnggrca") (f (quote (("utils" "errors") ("errors") ("default" "utils")))) (y #t)))

(define-public crate-nalar-0.0.0 (c (n "nalar") (v "0.0.0") (h "1vy5l98sdv7qp8ad7fa2l3crxr83ablff38qjsqwcc7jdk66cg6a") (y #t)))

