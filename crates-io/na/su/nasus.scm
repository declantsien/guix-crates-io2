(define-module (crates-io na su nasus) #:use-module (crates-io))

(define-public crate-nasus-0.1.0 (c (n "nasus") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "peace-performance") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "17xb3si2cyl5hx564xk2pcy7kr3jc4fra9xn2sbclgwc1m3m00bg")))

(define-public crate-nasus-0.1.1 (c (n "nasus") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "peace-performance") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1ssg4lr63kxpcf6lxgjyazxlyjandws8w3pkfbvdl5hwj2rpyxif")))

(define-public crate-nasus-0.1.2 (c (n "nasus") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "peace-performance") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1l07qrgxdqcr97dzz612kmipbrr90rqz98fs5n2rs87nzzglycky")))

(define-public crate-nasus-0.1.3 (c (n "nasus") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "peace-performance") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0zyg32zjzb2399israz6zn7i515ycmsfyrpb29gpm7gyycyh461r")))

(define-public crate-nasus-0.2.0 (c (n "nasus") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qd0481nvrwxgz7c07p56swp3274r7h0a2xj86jaym0wcazzpwmi")))

(define-public crate-nasus-0.2.1 (c (n "nasus") (v "0.2.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mir0vh3yv2qdajyjg032kv00isgnfj548a7dwh8q7ilja4n37jm")))

