(define-module (crates-io na su nasu) #:use-module (crates-io))

(define-public crate-nasu-0.0.0 (c (n "nasu") (v "0.0.0") (h "1ph67czr23xad3qxy4k28g9hv79bgvpvnyg1hppdldhcjjhyvr0f")))

(define-public crate-nasu-0.0.1 (c (n "nasu") (v "0.0.1") (h "1y2wipp7pnq358r6k9yika1ln2pw4v6dqr1v3hkpcfk7yb1q3gvh")))

(define-public crate-nasu-0.0.2 (c (n "nasu") (v "0.0.2") (h "089x7f8qh00s55rphch0n60daziglidyzqr9sqmryr9vwriyiq6s")))

(define-public crate-nasu-0.1.0 (c (n "nasu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "cron") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "http2" "tcp"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "08vhf4sqzyhlis9c25nrwmybh9zmadx96jmk06ap78wbpwy19v7p")))

