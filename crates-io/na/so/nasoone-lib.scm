(define-module (crates-io na so nasoone-lib) #:use-module (crates-io))

(define-public crate-nasoone-lib-0.1.0 (c (n "nasoone-lib") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "04ajcq8mypjvf0s5zr9sljfay51n4bwxy686lmm329w0rwy51mx6") (y #t)))

(define-public crate-nasoone-lib-0.2.0 (c (n "nasoone-lib") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "1m08xnl14i752vmi72i42zkix5l1rwf5zbqrmiv8q57bfg4fi4p1")))

(define-public crate-nasoone-lib-0.2.1 (c (n "nasoone-lib") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "0v9a4ri0g9b2wgn4wmypf3aklddmqcq2pq3zr1i9bf3vpp0m25f5")))

(define-public crate-nasoone-lib-0.2.2 (c (n "nasoone-lib") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "0qgcjs2rwgfajayi1s8dc5658s77b2pkr824cwaa19hppajw1gxf")))

(define-public crate-nasoone-lib-0.2.3 (c (n "nasoone-lib") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "00qn1fbakwg7n2jdpc8539zqhhbjkwbyqfx4f3l6z69dfj4rjlb8")))

(define-public crate-nasoone-lib-0.2.4 (c (n "nasoone-lib") (v "0.2.4") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "1kpf4m5shigvx3nxz11mxbp2jzn49d3b270hw6x51s16m309wbyr")))

(define-public crate-nasoone-lib-0.2.5 (c (n "nasoone-lib") (v "0.2.5") (d (list (d (n "crossbeam-channel") (r "^0.5.5") (d #t) (k 0)) (d (n "etherparse") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "pcap") (r "^0.9.2") (d #t) (k 0)))) (h "1swk46608l83z9a7wcyk0q89kp2vc9varakb7pg8rdxydq79dy1n")))

