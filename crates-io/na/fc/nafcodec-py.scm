(define-module (crates-io na fc nafcodec-py) #:use-module (crates-io))

(define-public crate-nafcodec-py-0.1.1 (c (n "nafcodec-py") (v "0.1.1") (d (list (d (n "nafcodec") (r "^0.1.1") (f (quote ("arc"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "09s7mkkrkfc35rb4wsiag5978ph4az9mb0gvb3kdj4midr5hrfbb") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default"))))))

(define-public crate-nafcodec-py-0.2.0 (c (n "nafcodec-py") (v "0.2.0") (d (list (d (n "nafcodec") (r "^0.2.0") (f (quote ("arc"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (d #t) (k 0)))) (h "03axzrdk06vvbxzv7j8q98yqjz5mnxgsjxyidcaib2wh7gm280dj") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default"))))))

