(define-module (crates-io na fc nafcodec) #:use-module (crates-io))

(define-public crate-nafcodec-0.1.0 (c (n "nafcodec") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (f (quote ("experimental"))) (d #t) (k 0)))) (h "1izz7xlrvjyavk5q4ldaxap6hb4i4h03yqiw9n8kidq9nwa567pw") (f (quote (("default") ("arc"))))))

(define-public crate-nafcodec-0.1.1 (c (n "nafcodec") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (f (quote ("experimental"))) (d #t) (k 0)))) (h "0xm35f96zfi917bj6s1bpgp6c7dyqd55vbk785rj9zgagdfaafc2") (f (quote (("default") ("arc"))))))

(define-public crate-nafcodec-0.2.0 (c (n "nafcodec") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.13.1") (f (quote ("experimental"))) (d #t) (k 0)))) (h "09d2lakipny74jlcck4swqzzdgyw8y08dvj5xw5qcgw5kigpsazl") (f (quote (("nightly") ("default" "tempfile") ("arc"))))))

