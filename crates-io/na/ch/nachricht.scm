(define-module (crates-io na ch nachricht) #:use-module (crates-io))

(define-public crate-nachricht-0.1.0 (c (n "nachricht") (v "0.1.0") (h "0g7fxc6zgb1jzq58wdqkpy2vaqg2nrq4m3w53bkq0pcy162a9lan")))

(define-public crate-nachricht-0.2.0 (c (n "nachricht") (v "0.2.0") (h "0a4j343qplcnhal1dfsdzg12ps6f2nzf121lz985lnfcwz8zbn3f")))

(define-public crate-nachricht-0.2.1 (c (n "nachricht") (v "0.2.1") (h "1v2xdbfcvdj3icbbcb0zfc1n5c73mvjc4m7r5177haic9zjz9s79")))

(define-public crate-nachricht-0.4.0 (c (n "nachricht") (v "0.4.0") (h "18vgbi059p6vl242frfglywvmz6i6k4ssf3skcnxbdb6wlh6wr8n")))

