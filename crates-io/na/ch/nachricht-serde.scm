(define-module (crates-io na ch nachricht-serde) #:use-module (crates-io))

(define-public crate-nachricht-serde-0.1.0 (c (n "nachricht-serde") (v "0.1.0") (d (list (d (n "nachricht") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)))) (h "1l6l9q2shrz5c0rzvhy8ngfc1j2x5lr189lxjvlaj9n1cva57y8a")))

(define-public crate-nachricht-serde-0.2.0 (c (n "nachricht-serde") (v "0.2.0") (d (list (d (n "nachricht") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)))) (h "1s99wrdmjzk4ap91rhzsm5vixkwlmdyfjb6j9s3jycf5sxlzncln")))

(define-public crate-nachricht-serde-0.3.0 (c (n "nachricht-serde") (v "0.3.0") (d (list (d (n "nachricht") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "18d3ax29rdsa1n40239ppgjm8vhfkyn50223aqnnv9bk60mr41gw") (y #t)))

(define-public crate-nachricht-serde-0.3.1 (c (n "nachricht-serde") (v "0.3.1") (d (list (d (n "nachricht") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1n0lvk4y6mijpchphz22nxhv1zvlahp9pm111ia9xqmkf7klzm4x")))

(define-public crate-nachricht-serde-0.3.2 (c (n "nachricht-serde") (v "0.3.2") (d (list (d (n "nachricht") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1rf94a9p5m2vi2gmj6vq6nif3vzdqfhcnf6sqvc79h51hwv8zvn7")))

(define-public crate-nachricht-serde-0.4.0 (c (n "nachricht-serde") (v "0.4.0") (d (list (d (n "nachricht") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0n676pgdbzn367yih2znsalkamy428kfz1bkxnhfg0gpjxkvqa59")))

