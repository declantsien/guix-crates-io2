(define-module (crates-io na ch nachricht-nq) #:use-module (crates-io))

(define-public crate-nachricht-nq-0.1.0 (c (n "nachricht-nq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "nachricht") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hfm7x9h5bianmxm63fzwp6pyjd096v0pkjim6s2vr8qaicm9x92")))

(define-public crate-nachricht-nq-0.2.0 (c (n "nachricht-nq") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "nachricht") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1yvjnlhi5ba17n8y8n3lkhl0gn221aa0bch1gnwwfka6ilmrg0vb")))

(define-public crate-nachricht-nq-0.2.1 (c (n "nachricht-nq") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "edit") (r "^0.1.2") (d #t) (k 0)) (d (n "nachricht") (r "^0.2.1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qjamn6mwbq3v8da1da16mpzc98j3cqqxh4qb4mgz1gk02qfcbri")))

(define-public crate-nachricht-nq-0.4.0 (c (n "nachricht-nq") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "edit") (r "^0.1.3") (d #t) (k 0)) (d (n "nachricht") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "159v0alvg6s9nk02pbvdnrhhkf5rs0lj3r3qcphksplfp2ry67wz")))

