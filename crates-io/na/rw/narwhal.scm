(define-module (crates-io na rw narwhal) #:use-module (crates-io))

(define-public crate-narwhal-0.2.0 (c (n "narwhal") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 2)))) (h "0i6r4np6sbrqcr50xlhhs4rrcx410agjgpidnl1pik4xlgiay2iy")))

(define-public crate-narwhal-0.2.1 (c (n "narwhal") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 2)))) (h "1dyhh2nnbynlb05g9rylkzygdwphjdsxsgparmslvp1r7p6397z1")))

(define-public crate-narwhal-0.3.0 (c (n "narwhal") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.13") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.13") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 2)))) (h "1bp3n3dm9x87hdy0gjizj1j31snhzgqixnyyq6w51l40sx4hfani")))

