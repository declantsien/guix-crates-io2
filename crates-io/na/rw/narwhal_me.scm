(define-module (crates-io na rw narwhal_me) #:use-module (crates-io))

(define-public crate-narwhal_me-0.1.0 (c (n "narwhal_me") (v "0.1.0") (h "185v44kdrzv81b5n03dalm1mr72kfwf9b1frknjv63brk9jwd0gf")))

(define-public crate-narwhal_me-0.2.0 (c (n "narwhal_me") (v "0.2.0") (h "1j4wnmaqwhp5qmkfg7y685yq6llhln5crdldkc7hxwmcdklghn20")))

(define-public crate-narwhal_me-0.2.1 (c (n "narwhal_me") (v "0.2.1") (h "1kqq7dyq4gdl24wdvmr0djx4p70dgivacakpnr2izdfcyfs1hkds")))

