(define-module (crates-io na rw narwhal-tooth) #:use-module (crates-io))

(define-public crate-narwhal-tooth-0.1.0 (c (n "narwhal-tooth") (v "0.1.0") (d (list (d (n "async_once") (r "^0.2.6") (d #t) (k 0)) (d (n "btleplug") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdpbn21kps6j80hwrxlqn9k6y1lh1nf35bhzhfwv729nyd7nfyb")))

(define-public crate-narwhal-tooth-0.1.1 (c (n "narwhal-tooth") (v "0.1.1") (d (list (d (n "async_once") (r "^0.2.6") (d #t) (k 0)) (d (n "btleplug") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15vixsmx2sv9mj2fjk7wk6lrphvlvdq7j7k7i4qb1sghvfk1a0q6")))

(define-public crate-narwhal-tooth-0.2.0 (c (n "narwhal-tooth") (v "0.2.0") (d (list (d (n "async_once") (r "^0.2.6") (d #t) (k 0)) (d (n "btleplug") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q12s15dsf103h49hfyrsj8mrsfmq8n6zxg8yn5zbw2aw771i3bq")))

