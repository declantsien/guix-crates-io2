(define-module (crates-io na t- nat-poc) #:use-module (crates-io))

(define-public crate-nat-poc-0.1.0 (c (n "nat-poc") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "filetime") (r "^0.2.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "14kn4p241732w7m7xnk7znjs6lq9zn1dcps3bin2afs38md5zfbf")))

(define-public crate-nat-poc-0.1.1 (c (n "nat-poc") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "filetime") (r "^0.2.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "192clzpxjmqfaigpiia5zvlblayrxsv79bnpnn0dspvarfa7190i")))

(define-public crate-nat-poc-0.1.2 (c (n "nat-poc") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1v4sbc52350rdqipxm0rq5s67ybkn7wb7wfw8d3irnha4ifxpgin")))

(define-public crate-nat-poc-0.1.3 (c (n "nat-poc") (v "0.1.3") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1mj0cw84dys7jv2kck2dcra31naidbgqm9g8bc24m80wk1r94rxh")))

(define-public crate-nat-poc-0.1.4 (c (n "nat-poc") (v "0.1.4") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "002bjrnblrhbkx2iy09p6icqnwawg132y2lh8f0mg159k83889sl")))

(define-public crate-nat-poc-0.1.5 (c (n "nat-poc") (v "0.1.5") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "11w2n9wsays48kbglhab4bpyijnl3zzsf2jxq04qg7w1b2yvs64d")))

(define-public crate-nat-poc-0.1.6 (c (n "nat-poc") (v "0.1.6") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "02y03nmbbxgg34yf9h8647brxidfj0c2b5ard7mkaysccyw4w4b6")))

(define-public crate-nat-poc-0.1.7 (c (n "nat-poc") (v "0.1.7") (d (list (d (n "structopt") (r "^0.3.20") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0874ikzb7mqzhac9pj4cyg8scmc0fc885zwp0bv6nzl8ncvvsmmv")))

(define-public crate-nat-poc-0.1.8 (c (n "nat-poc") (v "0.1.8") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)))) (h "0x2800w20m4w5np4yfg51rga2k24azvwy84qy54fqr76p159n26h")))

(define-public crate-nat-poc-0.1.9 (c (n "nat-poc") (v "0.1.9") (d (list (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "users") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "1mizpjgckkx7ph7hs62g6p8s6bi0aaah5allwgh5vim8cwza042x")))

(define-public crate-nat-poc-0.2.0 (c (n "nat-poc") (v "0.2.0") (d (list (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "filetime") (r ">=0.2.13, <0.3.0") (d #t) (k 0)) (d (n "humansize") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "pretty-bytes") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "users") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "03447c7fbwsvgq7dpgqn47m3l693bc7nwvwymz61w48rdi31w6i4")))

(define-public crate-nat-poc-0.2.1 (c (n "nat-poc") (v "0.2.1") (d (list (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "filetime") (r ">=0.2.13, <0.3.0") (d #t) (k 0)) (d (n "humansize") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "pretty-bytes") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "users") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "1k3zk8p88zphd93g4slri62csymzr4kpq3n7c4zjnycbbx2qj5qm")))

(define-public crate-nat-poc-0.2.2 (c (n "nat-poc") (v "0.2.2") (d (list (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "filetime") (r ">=0.2.13, <0.3.0") (d #t) (k 0)) (d (n "humansize") (r ">=1.1.0, <2.0.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "pretty-bytes") (r ">=0.2.2, <0.3.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "termion") (r ">=1.5.5, <2.0.0") (d #t) (k 0)) (d (n "users") (r ">=0.11.0, <0.12.0") (d #t) (k 0)))) (h "064z8lnrnalhpq6a1xqsia9jrxggkwizqsapjc177v6hhv55bm2s")))

