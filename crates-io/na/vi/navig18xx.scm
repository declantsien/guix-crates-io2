(define-module (crates-io na vi navig18xx) #:use-module (crates-io))

(define-public crate-navig18xx-0.1.0 (c (n "navig18xx") (v "0.1.0") (d (list (d (n "n18brush") (r "^0.1.0") (d #t) (k 0)) (d (n "n18catalogue") (r "^0.1.0") (d #t) (k 0)) (d (n "n18example") (r "^0.1.0") (d #t) (k 0)) (d (n "n18game") (r "^0.1.0") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18io") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18route") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)) (d (n "n18ui") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "13p43in59gh0f8kx890rppsq5mwgk1ja1kjnzik4hm0xq14jcmc9") (f (quote (("ui" "n18ui") ("default" "ui"))))))

