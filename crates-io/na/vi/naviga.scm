(define-module (crates-io na vi naviga) #:use-module (crates-io))

(define-public crate-naviga-0.1.0 (c (n "naviga") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "1ls69dg1rp7gabrwkmh0hrx17vd4yss1d00vs5kg50qypdfn32k0")))

(define-public crate-naviga-0.1.1 (c (n "naviga") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0c4ms2q74gxzclyq1915lvj8ibnj5j4ww1n76bw8vj9gbvl9rr9d")))

(define-public crate-naviga-0.1.2 (c (n "naviga") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0c5x2bw218x5nllr2m2dbrwxxqnljng3xlwhhbx5vvi9gjsx8mqn")))

(define-public crate-naviga-0.1.3 (c (n "naviga") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0lngnndmpl37j9a4f7bf5jhx130iiy9aky2173kjpasr2ri70af8")))

(define-public crate-naviga-0.1.4 (c (n "naviga") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "03dwb2vv8y2mlzis0r8y3pwslxr1jp8dxwfzh9q1zz7vh82qk73z")))

