(define-module (crates-io na vi navigation) #:use-module (crates-io))

(define-public crate-navigation-0.1.0 (c (n "navigation") (v "0.1.0") (h "0vfgs1lzshjp4d4gl4i22i7lagszmh02m505vs2f6ziax8yy7jpr")))

(define-public crate-navigation-0.1.1 (c (n "navigation") (v "0.1.1") (h "0hbbr9nwz331fdxrjkk4g0r894800z554j1chhr269r6lc1j5dkm")))

(define-public crate-navigation-0.1.2 (c (n "navigation") (v "0.1.2") (h "10ixldzyg5z0ahgsiy9yi34w5n9dchjdgwign2iv9vv4j7ycx21w")))

(define-public crate-navigation-0.1.3 (c (n "navigation") (v "0.1.3") (h "1k7pdk6jzpf0p6xcrd5w5arjhyvmjsv9adrrc7gryc0b65dsxc8x")))

(define-public crate-navigation-0.1.4 (c (n "navigation") (v "0.1.4") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0b4ga22400vk8g1p77gvanqdcrmvnlrqg7qa42s4mgxp8gn3diiw")))

(define-public crate-navigation-0.1.5 (c (n "navigation") (v "0.1.5") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0z5yr9rzsvn5lfd5c4x2vkzbj3hma0cgnxfhk6j91m6cmwxvnlm6")))

(define-public crate-navigation-0.1.6 (c (n "navigation") (v "0.1.6") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "0imkf4bdlkv8gryn0vcqiyjpwmy16z0ishk8giqpdzi3vigbsn3r")))

(define-public crate-navigation-0.1.7 (c (n "navigation") (v "0.1.7") (d (list (d (n "rand") (r "^0.3.13") (d #t) (k 0)))) (h "1h9k40djd11z23q86ps4rxz9bzba305rl4gyrp6lkl4lbwh6sxlj")))

