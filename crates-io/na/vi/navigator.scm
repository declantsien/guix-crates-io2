(define-module (crates-io na vi navigator) #:use-module (crates-io))

(define-public crate-navigator-0.1.0 (c (n "navigator") (v "0.1.0") (h "1d8jwyzy2dzc995j7j1g72j2rnh01cwfrplwb9s27yphbfrb92bv")))

(define-public crate-navigator-0.2.0 (c (n "navigator") (v "0.2.0") (h "14gjfqwqdb9cx6cqk962bs4b6yxfq8b2dp5b7zcqyk5piasci9i8")))

(define-public crate-navigator-0.3.0 (c (n "navigator") (v "0.3.0") (h "1mn4gfk6839fdm926nb5d1vraknaf4d7ibk8lp9gd27370y5dmxc")))

