(define-module (crates-io na vi navier) #:use-module (crates-io))

(define-public crate-navier-0.1.0 (c (n "navier") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "netcdf") (r "^0.8.3") (d #t) (k 0)))) (h "0x13rmjf185rdx74cxivpb96a0fckl7wljwpyj786942can5cbba")))

