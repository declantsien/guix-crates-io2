(define-module (crates-io na gi nagios-range) #:use-module (crates-io))

(define-public crate-nagios-range-0.1.0 (c (n "nagios-range") (v "0.1.0") (h "1q2z6cxnw00p727d7wnsdbbv9ql50k5xr1clqjgvglqi99w0amig")))

(define-public crate-nagios-range-0.2.0 (c (n "nagios-range") (v "0.2.0") (h "1b5vjipbj428bgnr6c3rajhk3rqh3bzzcz4zk7p850rwk85ypvxx")))

(define-public crate-nagios-range-0.2.1 (c (n "nagios-range") (v "0.2.1") (h "1bjyv3c962jsd4l5gh8m96acic03lqi8jc0nvca1srjsrmw8mnf5")))

(define-public crate-nagios-range-0.2.2 (c (n "nagios-range") (v "0.2.2") (h "1jx8spdgkjdyhry5w4lg0vrnhqgi8mdjq11dx02fpc8yz6pqzamm")))

(define-public crate-nagios-range-0.2.3 (c (n "nagios-range") (v "0.2.3") (h "0zjxr9s897x8bby55imr89xrkgp970jf8ay6aksqii8hg72l2n8i")))

(define-public crate-nagios-range-0.2.4 (c (n "nagios-range") (v "0.2.4") (h "086zssd4gl74wjdvck0qy2ai4dghh87b7qw8k83mj8dzka40ka47")))

(define-public crate-nagios-range-0.2.5 (c (n "nagios-range") (v "0.2.5") (h "0g44046mny1g7dxw3d1vbsic1j835asisxwl4k010qjisdf9lwqf")))

