(define-module (crates-io na ts nats-jwt) #:use-module (crates-io))

(define-public crate-nats-jwt-0.1.0 (c (n "nats-jwt") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "nkeys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "00hh3wcrlqg8ws9691s071nc4j91al0ckym457vfms24i4cl8py9")))

(define-public crate-nats-jwt-0.2.0 (c (n "nats-jwt") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.3") (d #t) (k 0)) (d (n "nkeys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "07l43f76xb1hzp802kgmvsjq1bq7b0mhixq4narkbdk1r9kz8js5")))

