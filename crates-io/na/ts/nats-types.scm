(define-module (crates-io na ts nats-types) #:use-module (crates-io))

(define-public crate-nats-types-0.1.0 (c (n "nats-types") (v "0.1.0") (d (list (d (n "nom") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "029f0xjpz707hb509s5bcw8m7bf21x8pkh64ngn44bcjlc2z6wp2")))

(define-public crate-nats-types-0.1.1 (c (n "nats-types") (v "0.1.1") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xfjf13skpbrmgks42fy4fn19c3b6p5lzgaikbcghv81qd4fmvkn") (y #t)))

(define-public crate-nats-types-0.1.2 (c (n "nats-types") (v "0.1.2") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04n0cnxm4289hwr6psi22lpzfawm6ml59vmh47snc817qmd3snk3") (y #t)))

(define-public crate-nats-types-0.1.3 (c (n "nats-types") (v "0.1.3") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wasrfdj0a12kiarx8zsx57wn6g2r1qhy1v4qghji009hfs8b649") (y #t)))

(define-public crate-nats-types-0.1.4 (c (n "nats-types") (v "0.1.4") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qxscrp2xq1x7d3x69bi9dy1ib8dmhl5hma0428ns2zg3wd00c1k") (y #t)))

(define-public crate-nats-types-0.1.5 (c (n "nats-types") (v "0.1.5") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yg54sckdy05xv68pizfcj3jrvk1gxycip8nhq9ya4g3am75ii03") (y #t)))

(define-public crate-nats-types-0.1.6 (c (n "nats-types") (v "0.1.6") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0psdxmsy0wddl6f7c53yk56h7bm30a609nxi13mp5hrppph7awvs")))

(define-public crate-nats-types-0.1.7 (c (n "nats-types") (v "0.1.7") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05a2j93mz4y680kkd3f11f86zjs9dv09wj4xlsn9b7mbg5a86l1a")))

(define-public crate-nats-types-0.1.8 (c (n "nats-types") (v "0.1.8") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qvig7igqazwhlqndsp3an07lhd8n9d9syx6za10c51klasf6grm")))

