(define-module (crates-io na ts nats_client) #:use-module (crates-io))

(define-public crate-nats_client-0.1.0 (c (n "nats_client") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7.12") (f (quote ("tlsv1_2"))) (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.1.0") (d #t) (k 0)))) (h "1g88crc7xw09lvcp1sdb5yadnr893ddlnmc62j182699v10aniai")))

