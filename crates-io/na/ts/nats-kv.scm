(define-module (crates-io na ts nats-kv) #:use-module (crates-io))

(define-public crate-nats-kv-0.1.0 (c (n "nats-kv") (v "0.1.0") (d (list (d (n "async-graphql") (r "^2") (o #t) (k 0)) (d (n "nats") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wsdbfvz7amzy4q7769vk7j6fscz9n46cqwwdq6hmhxpgxqhaly0")))

(define-public crate-nats-kv-0.1.1 (c (n "nats-kv") (v "0.1.1") (d (list (d (n "async-graphql") (r "^2") (o #t) (k 0)) (d (n "nats") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10nb49ajh8qvlm18hcv0f9v3zzhdj36i512iyn6c7aghhmbdjhy3")))

