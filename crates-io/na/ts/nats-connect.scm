(define-module (crates-io na ts nats-connect) #:use-module (crates-io))

(define-public crate-nats-connect-0.1.0 (c (n "nats-connect") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (f (quote ("std"))) (k 0)) (d (n "async-nats") (r "^0.33") (k 0)) (d (n "bytes") (r "^1") (k 0)) (d (n "clap") (r "^4") (f (quote ("color" "derive" "error-context" "help" "std" "suggestions" "usage"))) (o #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "fmt"))) (o #t) (k 0)))) (h "18c3w4sdsrvp7gsmshc74i6nhh4rwvrhylpi0npvl1fgki8a2wr2") (f (quote (("default" "bin") ("bin" "clap" "tokio/io-std" "tokio/macros" "tokio/rt-multi-thread" "tracing" "tracing-subscriber"))))))

