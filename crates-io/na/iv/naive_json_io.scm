(define-module (crates-io na iv naive_json_io) #:use-module (crates-io))

(define-public crate-naive_json_io-0.1.0 (c (n "naive_json_io") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fb0z6i0grmkaz8fjlbb9n2w57nh41l5ggkn3vxvph47kvzanyd4")))

