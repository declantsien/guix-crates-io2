(define-module (crates-io na iv naiveuring) #:use-module (crates-io))

(define-public crate-naiveuring-0.1.0 (c (n "naiveuring") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1r6hiaq6nz18xi4l955jn5kar8kg4ck1b4r8sd4hkaprr7h3msh5") (y #t)))

(define-public crate-naiveuring-0.1.1 (c (n "naiveuring") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "03fybjj6i0jq26bzlpjxpzxnsb1mbx2ag8721pzvpr0isfgk8k1h") (y #t)))

(define-public crate-naiveuring-0.2.0 (c (n "naiveuring") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1wv1bmvkpb91qq5zv0skqjrdzlvmwc6m416dncq286f6krv2cc4x") (y #t)))

(define-public crate-naiveuring-0.3.0 (c (n "naiveuring") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1f4baih96hklbql1qa3yjb468jlwdsb7i9wwrgxkj40bgb9l4frf") (y #t)))

(define-public crate-naiveuring-0.4.0 (c (n "naiveuring") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0sdbxrhdzbajavqlv7k3vhyz1b40hcqk8hgq38mk0qbq2a4xgzw8") (y #t)))

