(define-module (crates-io na iv naive-cityhash) #:use-module (crates-io))

(define-public crate-naive-cityhash-0.1.0 (c (n "naive-cityhash") (v "0.1.0") (h "1llcsfha7ik04lifjx6zxbs2k5vlvvkavlq8l1bprkv4ynh42mhy")))

(define-public crate-naive-cityhash-0.2.0 (c (n "naive-cityhash") (v "0.2.0") (d (list (d (n "clickhouse-rs-cityhash-sys") (r "^0.1.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "fake") (r "^2.4.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0hkzbq1kkjxs81fgddidxdb8gx8adj9lq77g69rad2kd3sgb9rzw")))

