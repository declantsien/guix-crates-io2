(define-module (crates-io na iv naive_opt) #:use-module (crates-io))

(define-public crate-naive_opt-0.1.1 (c (n "naive_opt") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "195609zq8vn0cpc9y61644wk6c8x78jc6m0i0cgb3qv6h3l1d8pq")))

(define-public crate-naive_opt-0.1.2 (c (n "naive_opt") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "18nw7h7jkfkvjas3054nhi6gvpm7bjjyc9hq9dslp16lb5cpywr0")))

(define-public crate-naive_opt-0.1.3 (c (n "naive_opt") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "0nwm6fma93f9svr5p7i3fz76qhaq5mcrgf9gz0nxfamky7c4wbgp")))

(define-public crate-naive_opt-0.1.4 (c (n "naive_opt") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "08803xryskagn59yffmf8xri7pw8g0p88r91gbcgq7z7xjwgfylm")))

(define-public crate-naive_opt-0.1.5 (c (n "naive_opt") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "09l6zrdjnrsmanrm07v2zdj7g8jaghpz8rymv0dgrwrykz5is138")))

(define-public crate-naive_opt-0.1.6 (c (n "naive_opt") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "1wbfb5gcc931awv339gk35liyfs6fhg89d6b5r27afi0ynwcmywz")))

(define-public crate-naive_opt-0.1.7 (c (n "naive_opt") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"x86_64\"))") (k 0)) (d (n "memchr") (r "^2.3") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "0laxiv92msv4s7jpa6x4820dqp66rhlfwkdii0dzw17yf6a09n8j") (f (quote (("default"))))))

(define-public crate-naive_opt-0.1.8 (c (n "naive_opt") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"x86_64\"))") (k 0)) (d (n "memchr") (r "^2.3") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "04br9f2744dhpqabd1zr9d2dpiqzm9d4q6952cgczrhlkwnlkdxf") (f (quote (("default"))))))

(define-public crate-naive_opt-0.1.9 (c (n "naive_opt") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "=0.2") (d #t) (t "cfg(not(any(target_arch = \"x86_64\", target_arch = \"x86\")))") (k 0)) (d (n "memchr") (r "=2.4") (d #t) (t "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (k 0)))) (h "0yx4swahrx1a59wyzb6hr250956dabkn865k1s2rsj6s6lzyccq4") (f (quote (("default"))))))

(define-public crate-naive_opt-0.1.10 (c (n "naive_opt") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "03bypx81wpxm2nij6r560hq71hibc1xbmdl2r41071c4fcmdyd4w") (f (quote (("std" "memx/std") ("default" "std"))))))

(define-public crate-naive_opt-0.1.11 (c (n "naive_opt") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "05m55d4ikk22cdl9n07m088ar4mhskpyryvnyyvk7ma6l2nkx4ch") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.12 (c (n "naive_opt") (v "0.1.12") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "17an37dfvyv75rmwyv0lm9g9ggddnrrawq1q6m3x5ac82lmys3lf") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.13 (c (n "naive_opt") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "0rvwba54r4gy4s82zyhpsi7qcdj8vijxxracdq0i1sg64p8k51df") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.14 (c (n "naive_opt") (v "0.1.14") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "0ww8iq8knlnq7l2n0b2k1vgz7cydwv07k64v0bkdd8z1482d0rcd") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std")))) (y #t)))

(define-public crate-naive_opt-0.1.15 (c (n "naive_opt") (v "0.1.15") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "04mzyh7dr0f36c4pag4cxp5pwk1rzjsd6087rmybhnfqxrvz13lz") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.16 (c (n "naive_opt") (v "0.1.16") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "0z0fcmmwp5zcjimwn8fpkqv67jcji3c4fd0v8r0lhdl627k985yl") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.17 (c (n "naive_opt") (v "0.1.17") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "0xqcgmbimbm1952x6fmmig71kfn6jwva9h11v7fam7jr1sv6d4a8") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.18 (c (n "naive_opt") (v "0.1.18") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "10qz0fawpda7bmaqgy7s7r5690qbxis8v147dbdbx462r0rmyymq") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1.19 (c (n "naive_opt") (v "0.1.19") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "0pqy1vlavifxylbmid7x42adxfwspbfw3hzvn8w0s02rn2rv1xxc") (f (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std")))) (y #t)))

(define-public crate-naive_opt-0.1.20 (c (n "naive_opt") (v "0.1.20") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "memx") (r "^0.1") (k 0)))) (h "1zwlrmzl2ms5bd3k1h7jrsv2gcd14yh68b9znw5bdpfbm6nk02yy") (f (quote (("only_mc_last") ("only_mc_1st") ("default"))))))

(define-public crate-naive_opt-0.1.21 (c (n "naive_opt") (v "0.1.21") (d (list (d (n "memx") (r "^0.1") (k 0)))) (h "1z7s5d17333w8ybbrikslpx0s1hfiz97mvkrisy3dpb7w0ilcf1m") (f (quote (("only_mc_last") ("only_mc_1st") ("default"))))))

(define-public crate-naive_opt-0.1.22 (c (n "naive_opt") (v "0.1.22") (d (list (d (n "memx") (r "^0.1") (k 0)))) (h "13vgj4c7k86v5ipkaa8cbimrrjbxvnaazaaadm13dn5cgmmfbcb5") (f (quote (("only_mc_last") ("only_mc_1st") ("default")))) (r "1.56.0")))

(define-public crate-naive_opt-0.1.23 (c (n "naive_opt") (v "0.1.23") (d (list (d (n "memx") (r "^0.1") (k 0)))) (h "0p5spv59bs4ddx1sdfciqilmpmy17vjp94y1a9g9s0vwx412r83v") (f (quote (("only_mc_last") ("only_mc_1st") ("default")))) (r "1.56.0")))

(define-public crate-naive_opt-0.1.24 (c (n "naive_opt") (v "0.1.24") (d (list (d (n "memx") (r "^0.1") (k 0)))) (h "03c9z11f3829j9zm819swhqiy0l5hdry45jmaczah5rlk77zd7n0") (f (quote (("only_mc_last") ("only_mc_1st") ("default")))) (r "1.56.0")))

