(define-module (crates-io na iv naivebayes) #:use-module (crates-io))

(define-public crate-naivebayes-0.1.0 (c (n "naivebayes") (v "0.1.0") (h "1nab1frib341qvr46yajfp4yaivv77lyw6y8pqh2hhr24lbgi6xm")))

(define-public crate-naivebayes-0.1.1 (c (n "naivebayes") (v "0.1.1") (h "1qdd8g4482fl3pkrgnfpw94i6h1chnr04p1d8j78mpjpsz9j2p7b")))

(define-public crate-naivebayes-0.1.2 (c (n "naivebayes") (v "0.1.2") (h "1r8nf2yfqfjycbbc63rw97diq469cv657pxb87nx74mgnvig5fd4")))

