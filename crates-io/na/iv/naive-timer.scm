(define-module (crates-io na iv naive-timer) #:use-module (crates-io))

(define-public crate-naive-timer-0.1.0 (c (n "naive-timer") (v "0.1.0") (h "1yhq1x339cv2wvafqhcg5ysmv8bdsrxhpi8gib70wkzd94llwgsj")))

(define-public crate-naive-timer-0.2.0 (c (n "naive-timer") (v "0.2.0") (h "1v9zhizqmylfyk0d1ynqm5gc3hv6cq59ad94rymw5w7bvvbhljh3")))

