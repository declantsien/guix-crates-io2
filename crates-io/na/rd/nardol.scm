(define-module (crates-io na rd nardol) #:use-module (crates-io))

(define-public crate-nardol-0.0.1 (c (n "nardol") (v "0.0.1") (d (list (d (n "backtrace") (r "^0.3.61") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "ron") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04l3s5yz1snjv30a4v93mqqn2d69a3jj087nydc3la77dvwczfxm")))

(define-public crate-nardol-0.0.2 (c (n "nardol") (v "0.0.2") (d (list (d (n "backtrace") (r "^0.3.61") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "ron") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yi9g3ias14ylqmikgc2di9c8lc2xr51fcbpz889ji7ziy6g2ml8")))

(define-public crate-nardol-0.0.3 (c (n "nardol") (v "0.0.3") (d (list (d (n "backtrace") (r "^0.3.61") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ron") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hk8lqnv4c0hxkz64qjhv5c22vqyc4pk9b04bh8hczyis86dip67")))

