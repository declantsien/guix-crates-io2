(define-module (crates-io na ro naromat) #:use-module (crates-io))

(define-public crate-naromat-0.0.1 (c (n "naromat") (v "0.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0k452bav2lrvy0igqmdhy9pgfii3x9m0am3kggxbl0480n0as7zc")))

(define-public crate-naromat-0.2.0 (c (n "naromat") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "01bswzpwxcr8im23cr5g4qn4hbdvghmcpa71c8820k44mhmxclql")))

(define-public crate-naromat-0.2.1 (c (n "naromat") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0ii45z18ma7q6x6lzpx18gqa8di3gbxy3ghw047903sc34z4bzqm")))

(define-public crate-naromat-0.3.0 (c (n "naromat") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0sp6hx4ixvls577ny41ls712q9pmq4n86rq77vzs56pccyckmy9r")))

(define-public crate-naromat-0.3.1 (c (n "naromat") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "1c62mb7iaps7knh6mimmhzi6qxrn0yyykmjjrbhqjifp8v5vnd3l")))

