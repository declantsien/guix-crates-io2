(define-module (crates-io na ro naro-derive) #:use-module (crates-io))

(define-public crate-naro-derive-0.0.1 (c (n "naro-derive") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rocksdb") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1gfq8snhsi6f2pdrk8mn9jif5awghrwf1a6sc7s00ahhq1wavhnq")))

