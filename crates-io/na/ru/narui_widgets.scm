(define-module (crates-io na ru narui_widgets) #:use-module (crates-io))

(define-public crate-narui_widgets-0.1.0 (c (n "narui_widgets") (v "0.1.0") (d (list (d (n "narui_core") (r "^0.1.0") (d #t) (k 0)) (d (n "narui_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.26.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.26.0") (d #t) (k 0)))) (h "0l343lnbf31ppvvfapi5wsagx35zc0qdvail0qz8184hm7vxi8cx")))

(define-public crate-narui_widgets-0.1.1 (c (n "narui_widgets") (v "0.1.1") (d (list (d (n "narui_core") (r "^0.1.1") (d #t) (k 0)) (d (n "narui_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.27.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.27.0") (d #t) (k 0)))) (h "1mf2l9lhp7paihml37imldz0k8xy52spxd4zicn8m1adk4553yb3") (y #t)))

(define-public crate-narui_widgets-0.1.2 (c (n "narui_widgets") (v "0.1.2") (d (list (d (n "narui_core") (r "^0.1.2") (d #t) (k 0)) (d (n "narui_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "vulkano") (r "^0.27.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.27.0") (d #t) (k 0)))) (h "00yrd34iwy0yyih9i360f3qa1jrj8k637lqblkl3sn51wscldcp3")))

