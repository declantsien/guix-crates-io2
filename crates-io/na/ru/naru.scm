(define-module (crates-io na ru naru) #:use-module (crates-io))

(define-public crate-naru-0.2.0 (c (n "naru") (v "0.2.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0l4frblsqdgjz3h62vxg3krndb00ibyzi5zvflk905chr7p91kjq")))

(define-public crate-naru-0.2.1 (c (n "naru") (v "0.2.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0c8kh9f3p40dpb14xckjmx8w0msa700k9kw65737pjcmb06qzna1")))

(define-public crate-naru-0.2.2 (c (n "naru") (v "0.2.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1azphzn55v93l19isfdpjicgh680rjbqi0ljdv8wr4dq96jpq178")))

(define-public crate-naru-0.2.3 (c (n "naru") (v "0.2.3") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1rrmz7z2rbai64vdbiyw3yzmnv0kn1ca7n2lplwg325q3251gs4i")))

(define-public crate-naru-0.3.0 (c (n "naru") (v "0.3.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "1nkb46mwh5pr0i2qx4yh2k2j5c2y21v6w6hlm111wi8aky4hq41h")))

(define-public crate-naru-0.4.0 (c (n "naru") (v "0.4.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sublime_fuzzy") (r "^0.6") (d #t) (k 0)) (d (n "termios") (r "~0.3.1") (d #t) (k 0)) (d (n "toml") (r "~0.5.5") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "15h4jzzxm8v2fk806f7ig90b5j5g4z2l9348ns3l22ac8wffacfx")))

