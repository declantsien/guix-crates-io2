(define-module (crates-io na ru narui_macros) #:use-module (crates-io))

(define-public crate-narui_macros-0.1.0 (c (n "narui_macros") (v "0.1.0") (d (list (d (n "bind_match") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0") (d #t) (k 0)))) (h "1gpscnr6fwdzwrq3fwii17x03djp0l5y2lk0qx66br13cq8nbvi9")))

