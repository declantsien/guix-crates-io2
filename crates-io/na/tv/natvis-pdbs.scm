(define-module (crates-io na tv natvis-pdbs) #:use-module (crates-io))

(define-public crate-natvis-pdbs-0.0.0 (c (n "natvis-pdbs") (v "0.0.0") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)))) (h "1p64y037iyf77yp4qiw9gb476wifvj3y7azipbd2p8qw8hbdy98b") (y #t)))

(define-public crate-natvis-pdbs-0.0.1 (c (n "natvis-pdbs") (v "0.0.1") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)))) (h "05qag2vf1hjxqj5jf4ci8mvgal2a5c6mafixwqk7hzmh9ry949hy") (y #t)))

(define-public crate-natvis-pdbs-1.0.0 (c (n "natvis-pdbs") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)))) (h "0cki4v7sql2997178349mvh7xvda00rz6mbxwnj9xg2s73ln280j")))

(define-public crate-natvis-pdbs-1.0.1 (c (n "natvis-pdbs") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.8.2") (d #t) (k 0)))) (h "1pk97wsvc9h8bxm09a5xhiza2mrss8dn60lfk26vph9h9ixkay1v")))

(define-public crate-natvis-pdbs-1.0.2 (c (n "natvis-pdbs") (v "1.0.2") (d (list (d (n "cargo_metadata") (r "^0.9") (d #t) (k 0)))) (h "132k78vqqjidkqr6iviip1d63zb68rsr7xdzda7nmxicfblaqf1p")))

(define-public crate-natvis-pdbs-1.0.3 (c (n "natvis-pdbs") (v "1.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vll8lrvxxbxj9ih2iwxl1n3ynypadpjy3z98jmj1aaiays3ajjz")))

