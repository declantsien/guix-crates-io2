(define-module (crates-io na v- nav-types) #:use-module (crates-io))

(define-public crate-nav-types-0.1.0 (c (n "nav-types") (v "0.1.0") (d (list (d (n "assert") (r "^0.7.1") (d #t) (k 2)) (d (n "clippy") (r "^0.0.80") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)))) (h "1i4x162iwv8g4g6832vgfgm0nj9xsk7qm02a86qgbfwgr458xk1s") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.2.0 (c (n "nav-types") (v "0.2.0") (d (list (d (n "assert") (r "^0.7.1") (d #t) (k 2)) (d (n "clippy") (r "^0.0.82") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.8.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.34") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3.1") (d #t) (k 2)))) (h "0qcmkfi303h7v8j0wp14vqpf8303ppffb31q4yxn52h1g1jz9f8c") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.3.0 (c (n "nav-types") (v "0.3.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.112") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "09z73c11m0ba3fg4y6c2n3xq75j9z7z5im0p87ss0jhrr58z3xld") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.4.0 (c (n "nav-types") (v "0.4.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.112") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "12dywzy2n8xdszcsy1pn6y2nqnkg7vnlh06l9by4svw38qpb7cfb") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.4.1 (c (n "nav-types") (v "0.4.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.112") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0gv96ckh32ld4qhz6gyvnwl1ngkz8vi7mby4vl9nh0is5ixj7cwd") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.4.2 (c (n "nav-types") (v "0.4.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1zp88r6jh0i021mw5saq00ayxfncr3jcnw9svlbqd4n82h5qlc8s") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.4.3 (c (n "nav-types") (v "0.4.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.19") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1xz12gjbq50rpc8rhb5xyfq4rynizr5s5xfxcajvxip9f08sgz0q") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.4.4 (c (n "nav-types") (v "0.4.4") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "clippy") (r "^0.0.302") (o #t) (d #t) (k 0)) (d (n "nalgebra") (r "^0.20") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "035nlsfr4nd1djwl7qllkn55ndk6m7qm9n48cvka5l9q5nxlq6z2") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nav-types-0.5.0 (c (n "nav-types") (v "0.5.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1c81yg5iipfi5xflilrw4hih15az1bwwx0vkd0nja8id0w3gpd9y") (f (quote (("dev") ("default"))))))

(define-public crate-nav-types-0.5.1 (c (n "nav-types") (v "0.5.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "nalgebra") (r "^0.22") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.116") (d #t) (k 2)))) (h "0dwah9wmq2alvyxyqncngzsc1npjmqhav0lfbyaa9ar75rmvcqd7") (f (quote (("dev" "serde") ("default"))))))

(define-public crate-nav-types-0.5.2 (c (n "nav-types") (v "0.5.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.164") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.164") (d #t) (k 2)))) (h "08zxskrl40xcik1170y28axx811l1779l0x3vz6wrj9pqm4vggkr") (f (quote (("dev" "serde") ("default"))))))

