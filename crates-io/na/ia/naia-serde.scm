(define-module (crates-io na ia naia-serde) #:use-module (crates-io))

(define-public crate-naia-serde-0.10.0 (c (n "naia-serde") (v "0.10.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.10") (d #t) (k 0)))) (h "0569vm4k2z3h8yb4mmz47n17jn5n4v921wsdzy36rn4r7sd4f5c2")))

(define-public crate-naia-serde-0.13.0 (c (n "naia-serde") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.13") (d #t) (k 0)))) (h "1m74d2vg9fz9zq37hvbpswrkaq64g47dwc3i8kx7qypmj8kqn2cf")))

(define-public crate-naia-serde-0.15.0 (c (n "naia-serde") (v "0.15.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.13") (d #t) (k 0)))) (h "02zs31bdkqybwzfd1ip3yjkb4ip43ivl2sz2zlsnm0zmazq9a07i")))

(define-public crate-naia-serde-0.16.0 (c (n "naia-serde") (v "0.16.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.16") (d #t) (k 0)))) (h "04zncvkw5r6nfvr5yrqg4c8v7g1klpa46rl8907amnwjd362fd9y")))

(define-public crate-naia-serde-0.18.0 (c (n "naia-serde") (v "0.18.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)))) (h "1xrns6fbf5b0jdnhjx5y2g2r9fnzlr445xiw1ii3v60aiawcn8k4")))

(define-public crate-naia-serde-0.22.0 (c (n "naia-serde") (v "0.22.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.22") (d #t) (k 0)))) (h "0hj7vbj0v0ah7by9053hyjbqfwz0fjm8gc3c0n91g165g2xmv4hb")))

