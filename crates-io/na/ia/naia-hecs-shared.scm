(define-module (crates-io na ia naia-hecs-shared) #:use-module (crates-io))

(define-public crate-naia-hecs-shared-0.7.0-alpha.1 (c (n "naia-hecs-shared") (v "0.7.0-alpha.1") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-shared") (r "^0.7.0-alpha.1") (d #t) (k 0)))) (h "1ppr80zsb7wncxkkw35a5mjh6iammlf0bs6jmb9drys6l2cmi95s") (f (quote (("wbindgen" "naia-shared/wbindgen") ("multithread" "naia-shared/multithread") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.7.0 (c (n "naia-hecs-shared") (v "0.7.0") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-shared") (r "^0.7.0") (d #t) (k 0)))) (h "08jhj08wsb5lzym8hp2py61h4cixdaycpwjvm07c6rxw0p8l5pbd") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.7.3 (c (n "naia-hecs-shared") (v "0.7.3") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-shared") (r "=0.7.3") (d #t) (k 0)))) (h "14vxj5p2ysyc183cjvi0xxpl46y2v7q98jsjrzk5waqcqmgvbfq5") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.8.0 (c (n "naia-hecs-shared") (v "0.8.0") (d (list (d (n "hecs") (r "^0.5.2") (d #t) (k 0)) (d (n "naia-shared") (r "^0.8") (d #t) (k 0)))) (h "0x3sq365l35ff4ad37r3y011i0rhsmxq1p61ck46jzp92nm10c0v") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.10.0 (c (n "naia-hecs-shared") (v "0.10.0") (d (list (d (n "hecs") (r "^0.7") (d #t) (k 0)) (d (n "naia-shared") (r "^0.10") (d #t) (k 0)))) (h "1fyf39cjarnyqhb0bbiw4kwxrjzq678d9mzwsq6zpfjv3mjhjwh1") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.10.1 (c (n "naia-hecs-shared") (v "0.10.1") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-shared") (r "^0.10") (d #t) (k 0)))) (h "065cgz2agnnbsanyi0dmgah0kc569sgxas9b0pbvf92jk4ssfkav") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.12.0 (c (n "naia-hecs-shared") (v "0.12.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-shared") (r "^0.12.0") (d #t) (k 0)))) (h "0d2dijidh0l65mcgsi8z2cx6abp9px091gikw0i28jsg68idxam1") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.13.0 (c (n "naia-hecs-shared") (v "0.13.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-shared") (r "^0.13.0") (d #t) (k 0)))) (h "1cq6kh9gq6g0m775a2div9mdm0w16lml91v5sn9wlfzzd3g42myw") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.14.0 (c (n "naia-hecs-shared") (v "0.14.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-shared") (r "^0.14") (d #t) (k 0)))) (h "0j2nygj5cj44y1w3mr1x0byzx6gb5rwgklfmdvx6bv13apq9ipdl") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.16.0 (c (n "naia-hecs-shared") (v "0.16.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-shared") (r "^0.16") (d #t) (k 0)))) (h "0igqwjhl665120lq5bx6mhyrzkz1q9b2sdazbc17fm2rkk0ca2r1") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.20.0 (c (n "naia-hecs-shared") (v "0.20.0") (d (list (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "naia-shared") (r "^0.20") (d #t) (k 0)))) (h "0qk0jsppa95p4g0kd0scs2z3bfm2byvhxwy49sc7hmyz1b6diyr6") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

(define-public crate-naia-hecs-shared-0.21.0 (c (n "naia-hecs-shared") (v "0.21.0") (d (list (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "naia-shared") (r "^0.21") (d #t) (k 0)))) (h "1p5sp73a7vzim18f84qc7pqkvy3bryzxavb9zx3d4l1h3mkpiirr") (f (quote (("wbindgen" "naia-shared/wbindgen") ("mquad" "naia-shared/mquad"))))))

