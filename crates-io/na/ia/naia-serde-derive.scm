(define-module (crates-io na ia naia-serde-derive) #:use-module (crates-io))

(define-public crate-naia-serde-derive-0.10.0 (c (n "naia-serde-derive") (v "0.10.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-parse") (r "^0.10") (d #t) (k 0)))) (h "12033hv6rz9m8r5sav57jpjw0s2ypyn0r6qm0v408qx9v6vq0j5m")))

(define-public crate-naia-serde-derive-0.13.0 (c (n "naia-serde-derive") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-parse") (r "^0.13") (d #t) (k 0)))) (h "1kcrps33l0d632f0b8basn3y85jwjckj6ap6y25aw8alk81q7zxl")))

(define-public crate-naia-serde-derive-0.16.0 (c (n "naia-serde-derive") (v "0.16.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0vwvln6wsf1f0swgqxh3xdahw0qfvvmb445bcq77lq6bdcdq6idq")))

(define-public crate-naia-serde-derive-0.18.0 (c (n "naia-serde-derive") (v "0.18.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "0f8hx6cg6lp8pqqj2i6z74kbcbmahyy9xhkb1iwpisvdp0ljrh7r")))

(define-public crate-naia-serde-derive-0.18.1 (c (n "naia-serde-derive") (v "0.18.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "0gzq5a5swvc5z1fjcg6k2c9is36qlfxb7dj1490s3npf8i29myz6")))

(define-public crate-naia-serde-derive-0.22.0 (c (n "naia-serde-derive") (v "0.22.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1f4zp2qvyzhjxgdmmar5957y64cr5w87spk8qnr1nz1v6yqfd825")))

