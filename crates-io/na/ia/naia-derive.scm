(define-module (crates-io na ia naia-derive) #:use-module (crates-io))

(define-public crate-naia-derive-0.1.0 (c (n "naia-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09wsnar4qk47xcaz2b0wvrzmy5iwmhzdydp3nz4c26sln49n1ab3")))

(define-public crate-naia-derive-0.1.1 (c (n "naia-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0viz8gn11kd5sma30fgxh0l5cvxi3ncx9jj3ld32randds0d5qks")))

(define-public crate-naia-derive-0.3.0 (c (n "naia-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kv41qyg40z10b1ysz8gavfhhnz411chsabmzqrggd3rhpv2dnq3")))

(define-public crate-naia-derive-0.7.0-alpha.1 (c (n "naia-derive") (v "0.7.0-alpha.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qsc9hz0q9q3wadf4ksa0aalk6pckaml9agi1mvkn2iy63x121bv") (f (quote (("multithread"))))))

(define-public crate-naia-derive-0.7.0 (c (n "naia-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.65") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "05ma25hd2k40fzh8xxdvdzpxx2hnx58v226b8kdcf2fp6l1357jp")))

(define-public crate-naia-derive-0.7.3 (c (n "naia-derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0d64wna3zqwhhmm822pbqlriwhqr27bny93kklxs34d5s73ma308")))

(define-public crate-naia-derive-0.7.4 (c (n "naia-derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "00xrvs1a1mllfxz59vd67xqpc4bk9knkx2j3mxa5nl1amk91src4")))

(define-public crate-naia-derive-0.10.0 (c (n "naia-derive") (v "0.10.0") (d (list (d (n "naia-parse") (r "^0.10") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1ndrhpgy684jkc9kc4qy3l5kvzipmzb09k74kry0r921jpizs2mc")))

(define-public crate-naia-derive-0.13.0 (c (n "naia-derive") (v "0.13.0") (d (list (d (n "naia-parse") (r "^0.13") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1gmx7jr5s85fmxczl1y2rffpj6q7rpangxiyl791cl26yw4sx4si")))

(define-public crate-naia-derive-0.15.0 (c (n "naia-derive") (v "0.15.0") (d (list (d (n "naia-parse") (r "^0.13") (d #t) (k 0)) (d (n "naia-serde-derive") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1xwkngajhs1cc9czr7jcpzp5m3zk18nrnvvzqr2gfc3nb3f6pcp4")))

(define-public crate-naia-derive-0.16.0 (c (n "naia-derive") (v "0.16.0") (d (list (d (n "naia-serde-derive") (r "^0.16") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0b4d90avj8nh21qnxi1zb47012vkapbjwbfsd1r2j1i3arpnxd48")))

(define-public crate-naia-derive-0.18.0 (c (n "naia-derive") (v "0.18.0") (d (list (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0y9zyzby7v4a7a70g7yskfzmbbqc2hyxia1n0s14db7nxqw8bxmx")))

(define-public crate-naia-derive-0.19.0 (c (n "naia-derive") (v "0.19.0") (d (list (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1d3flx8ja8ibwdgsv3hxa65gyj5q1zlcp2b7ca3xzcc1cbwy2dgw")))

(define-public crate-naia-derive-0.19.1 (c (n "naia-derive") (v "0.19.1") (d (list (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1m5pyx0c2fw52kiq9a84mv4l4lavaf74yk9hrfci338rid3wfcjb")))

(define-public crate-naia-derive-0.21.0 (c (n "naia-derive") (v "0.21.0") (d (list (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0rszxpccnx08skg99jc420p57zdgqz4g5vc6b7g7ncqjnjgs52q0")))

(define-public crate-naia-derive-0.21.1 (c (n "naia-derive") (v "0.21.1") (d (list (d (n "naia-serde-derive") (r "^0.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "1xrjicga5hjzk9v9h7rax0iz70ismsn4hc5347bql842c2pfqkpj")))

(define-public crate-naia-derive-0.22.0 (c (n "naia-derive") (v "0.22.0") (d (list (d (n "naia-serde-derive") (r "^0.22") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("clone-impls"))) (d #t) (k 0)))) (h "0gmjfl5ssdijw0h5rsc0x230lwzq19haz4008c1v4g7vznm3jv5s")))

