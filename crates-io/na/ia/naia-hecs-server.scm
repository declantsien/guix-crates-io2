(define-module (crates-io na ia naia-hecs-server) #:use-module (crates-io))

(define-public crate-naia-hecs-server-0.7.0-alpha.1 (c (n "naia-hecs-server") (v "0.7.0-alpha.1") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.7.0-alpha.1") (d #t) (k 0)) (d (n "naia-server") (r "^0.7.0-alpha.1") (d #t) (k 0)))) (h "1jnxi7hiydcwxp15020vxd2yn22i332x9qlgk81w7m3kcjlzjs0s") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.7.0 (c (n "naia-hecs-server") (v "0.7.0") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.7.0") (d #t) (k 0)))) (h "1qrxf0nh2rnazgacsai2wrbd84wgd6x01czbnhjk7435p65bwjf9") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.7.3 (c (n "naia-hecs-server") (v "0.7.3") (d (list (d (n "hecs") (r "=0.5.2") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "=0.7.3") (d #t) (k 0)) (d (n "naia-server") (r "=0.7.3") (d #t) (k 0)))) (h "0fk606cf5v71f0qq9hdg2ksslcvy6h5il2xn5321vam1rj98rv0w") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.8.0 (c (n "naia-hecs-server") (v "0.8.0") (d (list (d (n "hecs") (r "^0.5.2") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.8") (d #t) (k 0)) (d (n "naia-server") (r "^0.8.0") (d #t) (k 0)))) (h "0mrn0rhgw3fix3fjgvxijn66bija6zdzia113mxvvsld1iclsprv") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.10.0 (c (n "naia-hecs-server") (v "0.10.0") (d (list (d (n "hecs") (r "^0.7") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (d #t) (k 0)))) (h "1fcfj6agrqg7c7r1cap19dswwvvcm6sdjbsh0xm2cnamwglpjm3s") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.10.1 (c (n "naia-hecs-server") (v "0.10.1") (d (list (d (n "hecs") (r "^0.7") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (d #t) (k 0)))) (h "1gi2c56j8j5b3gs8a225qszjaxcwsfib4b1mjcfspw5pji830k99") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.10.2 (c (n "naia-hecs-server") (v "0.10.2") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (d #t) (k 0)))) (h "057143y0ha7cn7f6bxdwr5yz7v6bxmmpxvm2lxaya64qd6iikd7j") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-hecs-server-0.12.0 (c (n "naia-hecs-server") (v "0.12.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.12.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.12.0") (d #t) (k 0)))) (h "0n0q3knsqzcpl1mq723n86rdvv6k5rg1dyvnkawlfxkv9m7cf7w0")))

(define-public crate-naia-hecs-server-0.13.0 (c (n "naia-hecs-server") (v "0.13.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.13.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.13.0") (d #t) (k 0)))) (h "1wkpp884as2j9wqgxqpz7br3y3inw0b2mm2raws72vdk5lkm2kpc")))

(define-public crate-naia-hecs-server-0.14.0 (c (n "naia-hecs-server") (v "0.14.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.14") (d #t) (k 0)) (d (n "naia-server") (r "^0.14") (d #t) (k 0)))) (h "0b6ryfi6vvirzw24q34sg2milarchmll14bwxzfjylqsqfh4h4fq")))

(define-public crate-naia-hecs-server-0.16.0 (c (n "naia-hecs-server") (v "0.16.0") (d (list (d (n "hecs") (r "^0.9") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.16") (d #t) (k 0)) (d (n "naia-server") (r "^0.16") (d #t) (k 0)))) (h "06h1n1qp3dn2nk9jqv0a648f4bwzidlmck3jbwxv8km8lzhphykm")))

(define-public crate-naia-hecs-server-0.20.0 (c (n "naia-hecs-server") (v "0.20.0") (d (list (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.20") (d #t) (k 0)) (d (n "naia-server") (r "^0.20") (d #t) (k 0)))) (h "0585lhclzgw211rykdxbq2map1hmqlgqynnf4fnglc6cib1q7xks") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

(define-public crate-naia-hecs-server-0.21.0 (c (n "naia-hecs-server") (v "0.21.0") (d (list (d (n "hecs") (r "^0.10") (d #t) (k 0)) (d (n "naia-hecs-shared") (r "^0.21") (d #t) (k 0)) (d (n "naia-server") (r "^0.21") (d #t) (k 0)))) (h "0kf5cvvlnznakbyvfg91sbbvkdsifplfi99q2793xjkw65ai6d5c") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

