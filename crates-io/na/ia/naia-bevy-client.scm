(define-module (crates-io na ia naia-bevy-client) #:use-module (crates-io))

(define-public crate-naia-bevy-client-0.7.0-alpha.1 (c (n "naia-bevy-client") (v "0.7.0-alpha.1") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.7.0-alpha.1") (d #t) (k 0)) (d (n "naia-client") (r "^0.7.0-alpha.1") (f (quote ("wbindgen" "multithread"))) (d #t) (k 0)))) (h "1apq766pqmikgs6g11g67l32c0qzl29cbmgqr0bb6v8dyl2zaij9")))

(define-public crate-naia-bevy-client-0.7.0 (c (n "naia-bevy-client") (v "0.7.0") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "naia-client") (r "^0.7.0") (f (quote ("wbindgen"))) (d #t) (k 0)))) (h "1rpl6r5j08iw3dljv11539i6g0w8d0fsjz9rcsijb1b7i8qgciqy")))

(define-public crate-naia-bevy-client-0.7.3 (c (n "naia-bevy-client") (v "0.7.3") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "=0.7.3") (d #t) (k 0)) (d (n "naia-client") (r "=0.7.3") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0g1536smfx6r2kp6bf1h4af04z4lkg0id99h6jcx1skvsqlfwvqp")))

(define-public crate-naia-bevy-client-0.8.0 (c (n "naia-bevy-client") (v "0.8.0") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "=0.7.3") (d #t) (k 0)) (d (n "naia-client") (r "=0.7.4") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "066rpszw54c7lbykh3r1jiww20zi21cyic3sy88as948q38yzfiv")))

(define-public crate-naia-bevy-client-0.9.0 (c (n "naia-bevy-client") (v "0.9.0") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.9") (d #t) (k 0)) (d (n "naia-client") (r "^0.8") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1ig2714r6lmmiyk0vwfgrq18dri5id6irzc0nvb2f592mfqpr2zj")))

(define-public crate-naia-bevy-client-0.9.1 (c (n "naia-bevy-client") (v "0.9.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "naia-bevy-shared") (r "^0.9") (d #t) (k 0)) (d (n "naia-client") (r "^0.8") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0y4mfkyn3c27sj2fi1csyyqmc9pwhhrxqpfqc3lm2r99lh874f37")))

(define-public crate-naia-bevy-client-0.10.0 (c (n "naia-bevy-client") (v "0.10.0") (d (list (d (n "bevy_app") (r "^0.7") (k 0)) (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-bevy-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-client") (r "^0.10") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "19b5lqda7rp0hzxb6g0dfnf8af35z1lgrq035d8m6mvb1n0m074b")))

(define-public crate-naia-bevy-client-0.10.1 (c (n "naia-bevy-client") (v "0.10.1") (d (list (d (n "bevy_app") (r "^0.7") (k 0)) (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-bevy-shared") (r "^0.10.1") (d #t) (k 0)) (d (n "naia-client") (r "^0.10") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "14lkz0cb0a063kgxipx74scq5z4halsfc68rizpcij04dv99wchc")))

(define-public crate-naia-bevy-client-0.10.2 (c (n "naia-bevy-client") (v "0.10.2") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-client") (r "^0.10.1") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0nhpc7rpi43n1q0i57j4gs3wbd82ngb5jn06h5q7y6fy1hxgs8ni")))

(define-public crate-naia-bevy-client-0.12.0 (c (n "naia-bevy-client") (v "0.12.0") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.12.0") (d #t) (k 0)) (d (n "naia-client") (r "^0.12.0") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "003zmmzx8qcl27gq9jzwxpjygbss8wq84ml7szy47k9w0dwx908g")))

(define-public crate-naia-bevy-client-0.13.0 (c (n "naia-bevy-client") (v "0.13.0") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.13.0") (d #t) (k 0)) (d (n "naia-client") (r "^0.13.0") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0sh5qyb7iqsdihzn4arfcdynilhr880ps732vm3hb5zqw1bzybjw")))

(define-public crate-naia-bevy-client-0.14.0 (c (n "naia-bevy-client") (v "0.14.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.14") (d #t) (k 0)) (d (n "naia-client") (r "^0.14") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0r7wagv7iips412bss3fx8cnl7gpzx45m95bfd9710b87frqd5qy")))

(define-public crate-naia-bevy-client-0.15.0 (c (n "naia-bevy-client") (v "0.15.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.15") (d #t) (k 0)) (d (n "naia-client") (r "^0.15") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0mlzx1v3w2ngcr3hwimq1gl908nlpymkxk772pppn98lwvhnyvi6")))

(define-public crate-naia-bevy-client-0.16.0 (c (n "naia-bevy-client") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.16") (d #t) (k 0)) (d (n "naia-client") (r "^0.16") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0wvymvkf6kpgjqy5bcghmqq0zclsgxi8h6nvbwn8ampg5072zpgl")))

(define-public crate-naia-bevy-client-0.17.0 (c (n "naia-bevy-client") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.17") (d #t) (k 0)) (d (n "naia-client") (r "^0.17") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0wl0wksmkj2xvvz302xby6vzrsd1w14h93z7yy34dx1c7y160q90")))

(define-public crate-naia-bevy-client-0.18.0 (c (n "naia-bevy-client") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.18") (d #t) (k 0)) (d (n "naia-client") (r "^0.18") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0ls23jdksawy5fy74njmx4fqz0v187yjl968ra1bknvm20s57gnv")))

(define-public crate-naia-bevy-client-0.19.0 (c (n "naia-bevy-client") (v "0.19.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.19") (d #t) (k 0)) (d (n "naia-client") (r "^0.19") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1rf2b9m3vw26hvw5n2krxh11d74153b5vvlz8d6hr8zrwgvqcpd9")))

(define-public crate-naia-bevy-client-0.20.0 (c (n "naia-bevy-client") (v "0.20.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.20") (d #t) (k 0)) (d (n "naia-client") (r "^0.20") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "13ay2z3yb9qrzfaa30cr3lwx8y0wqs9cf2vbc56llr5kyz1q5ljp") (f (quote (("transport_webrtc" "naia-client/transport_webrtc") ("transport_udp" "naia-client/transport_udp"))))))

(define-public crate-naia-bevy-client-0.20.1 (c (n "naia-bevy-client") (v "0.20.1") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.20") (d #t) (k 0)) (d (n "naia-client") (r "^0.20.1") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1ld4qnjgy8yb6rwadwwm2f57xc8ls2n156qvh7ylwm1snnv6i2a1") (f (quote (("transport_webrtc" "naia-client/transport_webrtc") ("transport_udp" "naia-client/transport_udp"))))))

(define-public crate-naia-bevy-client-0.21.0 (c (n "naia-bevy-client") (v "0.21.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.21") (d #t) (k 0)) (d (n "naia-client") (r "^0.21.0") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0iyrqvbm6av300xy5ipv7agk420rgjw1k0i7rqf2j9chjk9jm592") (f (quote (("transport_webrtc" "naia-client/transport_webrtc") ("transport_udp" "naia-client/transport_udp"))))))

(define-public crate-naia-bevy-client-0.21.1 (c (n "naia-bevy-client") (v "0.21.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "naia-bevy-shared") (r "^0.21") (d #t) (k 0)) (d (n "naia-client") (r "^0.21") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0rbj265nahsskg25x6xvxzgbh4vndg7jvh3kv66lgb5jplj7wfyj") (f (quote (("transport_webrtc" "naia-client/transport_webrtc") ("transport_udp" "naia-client/transport_udp"))))))

(define-public crate-naia-bevy-client-0.22.0 (c (n "naia-bevy-client") (v "0.22.0") (d (list (d (n "bevy_app") (r "^0.12.1") (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-bevy-shared") (r "^0.22") (d #t) (k 0)) (d (n "naia-client") (r "^0.22") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0xw0000m61n8f89gyikik281v3ldslpvl2qswvhk5y5mlvhfjf0j") (f (quote (("transport_webrtc" "naia-client/transport_webrtc") ("transport_udp" "naia-client/transport_udp"))))))

