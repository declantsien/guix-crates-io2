(define-module (crates-io na ia naia-bevy-shared) #:use-module (crates-io))

(define-public crate-naia-bevy-shared-0.7.0-alpha.1 (c (n "naia-bevy-shared") (v "0.7.0-alpha.1") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-shared") (r "^0.7.0-alpha.1") (f (quote ("wbindgen" "multithread"))) (d #t) (k 0)))) (h "1smgvjki8da0xi40pnvj6rqc5cc94r37jxxb781m83x9i5a32zmc")))

(define-public crate-naia-bevy-shared-0.7.0 (c (n "naia-bevy-shared") (v "0.7.0") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-shared") (r "^0.7.0") (f (quote ("wbindgen"))) (d #t) (k 0)))) (h "0pysvw6g4n0z8vndv6x05b8fr8v1yh1c711xkgg6c6caa9a7k69l")))

(define-public crate-naia-bevy-shared-0.7.3 (c (n "naia-bevy-shared") (v "0.7.3") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-shared") (r "=0.7.3") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1bfrczxp8p0nmnz7b7yfxw86xvznjgw2mk8rgc9qbvbvhyrn72fm")))

(define-public crate-naia-bevy-shared-0.9.0 (c (n "naia-bevy-shared") (v "0.9.0") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-shared") (r "^0.8") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0vk233wgysz5h61qnnh2n8hb71l8ral2cg6bva75469kyw0wazrh")))

(define-public crate-naia-bevy-shared-0.9.1 (c (n "naia-bevy-shared") (v "0.9.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "naia-shared") (r "^0.8") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0j37msm3yi5kxzqm8cpi9ix9ckmzjiis578zlakfz2mc0l7mvc9q")))

(define-public crate-naia-bevy-shared-0.10.0 (c (n "naia-bevy-shared") (v "0.10.0") (d (list (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-shared") (r "^0.10") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "08164zb4pxqjdapi3g9rb4s5mkqvqfq9im4xlrpm77k1f9hlk709")))

(define-public crate-naia-bevy-shared-0.10.1 (c (n "naia-bevy-shared") (v "0.10.1") (d (list (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-shared") (r "^0.10") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1ys6cczamqr9ajcpcqcw9ckixwbs7w47hspg2dqk1qyxwigqbbw0")))

(define-public crate-naia-bevy-shared-0.10.2 (c (n "naia-bevy-shared") (v "0.10.2") (d (list (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-shared") (r "^0.10") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1rb0k7ipxad7d929v29rjma98xdcwy2qj0p8qp077ldnfd5fmxym")))

(define-public crate-naia-bevy-shared-0.12.0 (c (n "naia-bevy-shared") (v "0.12.0") (d (list (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-shared") (r "^0.12.0") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0sq5xj1ngrblgy0gwm8iik3kkzzwwsd20gncgfh3siz2q5h1ifz6")))

(define-public crate-naia-bevy-shared-0.13.0 (c (n "naia-bevy-shared") (v "0.13.0") (d (list (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-shared") (r "^0.13.0") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1y110rwgs48l2p2lgl4jvlyzkj2zyd5vpp821msdmadicc36m68c")))

(define-public crate-naia-bevy-shared-0.14.0 (c (n "naia-bevy-shared") (v "0.14.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-shared") (r "^0.14") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "077qzxnq400hm98xncvy9gkcdmqsw4rmplv79h9asnab19ij9ah7")))

(define-public crate-naia-bevy-shared-0.15.0 (c (n "naia-bevy-shared") (v "0.15.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-shared") (r "^0.15") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0w2dj2g5zbalzlszis4fr763l471sk944awyand22aiww1ls3ndk")))

(define-public crate-naia-bevy-shared-0.16.0 (c (n "naia-bevy-shared") (v "0.16.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-shared") (r "^0.16") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0wnhfsh2qjnvv2qsjs3a8xvj7dnl3b1yh10d6vhalh6d3rkjbd8c")))

(define-public crate-naia-bevy-shared-0.17.0 (c (n "naia-bevy-shared") (v "0.17.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-shared") (r "^0.17") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0w52bc9h3fayja6zi877sp4jbc9pp99jqjgj6q63ikrd2hq0xzl8")))

(define-public crate-naia-bevy-shared-0.18.0 (c (n "naia-bevy-shared") (v "0.18.0") (d (list (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-shared") (r "^0.18") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0glfkpcjs64fxq5x311sxzqx1qa1l6k22q48cir9yqf35jp0n1m6")))

(define-public crate-naia-bevy-shared-0.19.0 (c (n "naia-bevy-shared") (v "0.19.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-shared") (r "^0.19") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "0xnmc2963xp3qfn96sn5gxr230fnvmlmq1g8p7as63icjhv7rbw2")))

(define-public crate-naia-bevy-shared-0.20.0 (c (n "naia-bevy-shared") (v "0.20.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-shared") (r "^0.20") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "1ysclcrqzi05x3c2pf6615a0l2ihbrq8fwwi20fkvdnhkp9l9nnz")))

(define-public crate-naia-bevy-shared-0.21.0 (c (n "naia-bevy-shared") (v "0.21.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-shared") (r "^0.21") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "10cdl6j3xlcm5w0g9rsn7a5gkmdxxpiqidl48gwsfy934zx34x83")))

(define-public crate-naia-bevy-shared-0.21.1 (c (n "naia-bevy-shared") (v "0.21.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "naia-shared") (r "^0.21") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "18rm3yf348qjbllz9ifhw9jyyz71l0hffq7f9b9ir5pxp2kz4vbp")))

(define-public crate-naia-bevy-shared-0.22.0 (c (n "naia-bevy-shared") (v "0.22.0") (d (list (d (n "bevy_app") (r "^0.12.1") (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-shared") (r "^0.22") (f (quote ("bevy_support" "wbindgen"))) (d #t) (k 0)))) (h "12y62iz5a8mmip9i027jwpxwcrj6r0yka29ha96jhigv4zaxip04")))

