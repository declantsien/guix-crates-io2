(define-module (crates-io na ia naia-parse) #:use-module (crates-io))

(define-public crate-naia-parse-0.10.0 (c (n "naia-parse") (v "0.10.0") (h "0xxgd05b7mg4fakqiayn768xq5bsnlgvd2fvycqcwhf5ikfsp2l6")))

(define-public crate-naia-parse-0.13.0 (c (n "naia-parse") (v "0.13.0") (h "0r3rkh7ac4zv91w90gjw2inyzd0gr5k5cy9gkzfvyssh3p3zayrp")))

