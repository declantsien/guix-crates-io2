(define-module (crates-io na ia naia-bevy-server) #:use-module (crates-io))

(define-public crate-naia-bevy-server-0.7.0-alpha.1 (c (n "naia-bevy-server") (v "0.7.0-alpha.1") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.7.0-alpha.1") (d #t) (k 0)) (d (n "naia-server") (r "^0.7.0-alpha.1") (f (quote ("multithread"))) (d #t) (k 0)))) (h "0m9593wbn8k8chwkhq7zrs2q9fzfzfc0dyxbn08p59ma6fb2fl4s") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.7.0 (c (n "naia-bevy-server") (v "0.7.0") (d (list (d (n "bevy") (r "=0.5.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.7.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.7.0") (d #t) (k 0)))) (h "19f2fppmswldn2i4c7p2nwym4m8par9jm76al67nvm58744g2n02") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.7.3 (c (n "naia-bevy-server") (v "0.7.3") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "=0.7.3") (d #t) (k 0)) (d (n "naia-server") (r "=0.7.3") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1a1708lc2mm4xy5r9j3cj0gd9d29mmca02yyxcdz1a9m66fyx66x") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.8.0 (c (n "naia-bevy-server") (v "0.8.0") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "=0.7.3") (d #t) (k 0)) (d (n "naia-server") (r "=0.7.4") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0856kdlgn9vi19xi2zcllbhi08jj4syzm88yyb3gv9ccvwy81ch5") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.9.0 (c (n "naia-bevy-server") (v "0.9.0") (d (list (d (n "bevy") (r "=0.6.0") (k 0)) (d (n "naia-bevy-shared") (r "^0.9") (d #t) (k 0)) (d (n "naia-server") (r "^0.8") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0y5ajzz1a7zzszclvlh7awzr7mxnhzc3ay0m14zka2lw8sv9fny2") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.9.1 (c (n "naia-bevy-server") (v "0.9.1") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "naia-bevy-shared") (r "^0.9") (d #t) (k 0)) (d (n "naia-server") (r "^0.8") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0z625s097wry9fhczh1i1nksgy00cgpxk75swq8aln26amnvjwnv") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.10.0 (c (n "naia-bevy-server") (v "0.10.0") (d (list (d (n "bevy_app") (r "^0.7") (k 0)) (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-bevy-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1l2r0wjavjbm5p74gk8isy7dljfd9wp9iz89zxx9dwf8ihqs7yb1") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.10.1 (c (n "naia-bevy-server") (v "0.10.1") (d (list (d (n "bevy_app") (r "^0.7") (k 0)) (d (n "bevy_ecs") (r "^0.7") (k 0)) (d (n "naia-bevy-shared") (r "^0.10.1") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "11mhw7v97acl7ay6g5kqif4zsab1wi2fmfg7rx36rnsyvyi0476f") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.10.2 (c (n "naia-bevy-server") (v "0.10.2") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.10") (d #t) (k 0)) (d (n "naia-server") (r "^0.10") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1w59zgzb83mflwq5ji1n69z2awdklwrj2i8piv2prs3gksrw6sqv") (f (quote (("use-webrtc" "naia-server/use-webrtc") ("use-udp" "naia-server/use-udp"))))))

(define-public crate-naia-bevy-server-0.12.0 (c (n "naia-bevy-server") (v "0.12.0") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.12.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.12.0") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0kzqcg539fz6dv74y0rsr1pwv0zanln0n4kd5bw7598rj0dx3dzx")))

(define-public crate-naia-bevy-server-0.13.0 (c (n "naia-bevy-server") (v "0.13.0") (d (list (d (n "bevy_app") (r "^0.8") (k 0)) (d (n "bevy_ecs") (r "^0.8") (k 0)) (d (n "naia-bevy-shared") (r "^0.13.0") (d #t) (k 0)) (d (n "naia-server") (r "^0.13.0") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1yrkqkm4h2ar2kqmw4ls66dhznz4802s6k7m6w7nqzhv9gpznqar")))

(define-public crate-naia-bevy-server-0.14.0 (c (n "naia-bevy-server") (v "0.14.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.14") (d #t) (k 0)) (d (n "naia-server") (r "^0.14") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1y7xfjbwr8jh2yfi5j1i8m41ynq58kryha58k2l4sv5mpwi01g9z")))

(define-public crate-naia-bevy-server-0.15.0 (c (n "naia-bevy-server") (v "0.15.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.15") (d #t) (k 0)) (d (n "naia-server") (r "^0.15") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0langbxc9kdcnaqvfabnimxrj3q9zw3fz4a72l18rxpxf1yjb5cb")))

(define-public crate-naia-bevy-server-0.16.0 (c (n "naia-bevy-server") (v "0.16.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.16") (d #t) (k 0)) (d (n "naia-server") (r "^0.16") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1w771rfvjr6jdsw6gcxxjywjd30lnp2a274sdsy1rrk9576v9axl")))

(define-public crate-naia-bevy-server-0.17.0 (c (n "naia-bevy-server") (v "0.17.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.17") (d #t) (k 0)) (d (n "naia-server") (r "^0.17") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1psnhagfs37ndpacqd1ajr3gwra484inqx0l92kmf6l0grg8dlcf")))

(define-public crate-naia-bevy-server-0.18.0 (c (n "naia-bevy-server") (v "0.18.0") (d (list (d (n "bevy_app") (r "^0.9") (k 0)) (d (n "bevy_ecs") (r "^0.9") (k 0)) (d (n "naia-bevy-shared") (r "^0.18") (d #t) (k 0)) (d (n "naia-server") (r "^0.18") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0vijpm2q43ac5xvl18xv4rxszri8hzz6194snbsw3sbf2bxl8lal")))

(define-public crate-naia-bevy-server-0.19.0 (c (n "naia-bevy-server") (v "0.19.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.19") (d #t) (k 0)) (d (n "naia-server") (r "^0.19") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "08jp9s24x0y98nv285vfj6fxyndzry4c8796nca0nzcg34vrj97d")))

(define-public crate-naia-bevy-server-0.20.0 (c (n "naia-bevy-server") (v "0.20.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.20") (d #t) (k 0)) (d (n "naia-server") (r "^0.20") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "03lzyhjndc0027rfs3lmw889b2v2l7bcnbbz01b7xg5pvp7f9a7i") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

(define-public crate-naia-bevy-server-0.21.0 (c (n "naia-bevy-server") (v "0.21.0") (d (list (d (n "bevy_app") (r "^0.10") (k 0)) (d (n "bevy_ecs") (r "^0.10") (k 0)) (d (n "naia-bevy-shared") (r "^0.21") (d #t) (k 0)) (d (n "naia-server") (r "^0.21") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "1dzm02z12hpaj8180x21li3kkzf01j5gy8q573dxjn1p52q5s70i") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

(define-public crate-naia-bevy-server-0.21.1 (c (n "naia-bevy-server") (v "0.21.1") (d (list (d (n "bevy_app") (r "^0.11") (k 0)) (d (n "bevy_ecs") (r "^0.11") (k 0)) (d (n "naia-bevy-shared") (r "^0.21") (d #t) (k 0)) (d (n "naia-server") (r "^0.21") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0arb1r92a5x1w27mfb3ikjn2cflc6ih98165q94lg4kpizbrsbpk") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

(define-public crate-naia-bevy-server-0.22.0 (c (n "naia-bevy-server") (v "0.22.0") (d (list (d (n "bevy_app") (r "^0.12.1") (k 0)) (d (n "bevy_ecs") (r "^0.12.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "naia-bevy-shared") (r "^0.22") (d #t) (k 0)) (d (n "naia-server") (r "^0.22") (f (quote ("bevy_support"))) (d #t) (k 0)))) (h "0yvrlp2mgmi93vq2jpqkbhh63910mpap8iah0dwnliq94v8acwla") (f (quote (("transport_webrtc" "naia-server/transport_webrtc") ("transport_udp" "naia-server/transport_udp"))))))

