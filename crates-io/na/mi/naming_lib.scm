(define-module (crates-io na mi naming_lib) #:use-module (crates-io))

(define-public crate-naming_lib-0.1.0 (c (n "naming_lib") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1yyzjc21sv1ww1k4v21sld819sax33k932flcqz6p1zwc6jvaras")))

(define-public crate-naming_lib-0.1.1 (c (n "naming_lib") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1v9p6nv2wamwb0qs296ihfqv92bvics3j0xwraf4yhzv1cjhfvhq")))

(define-public crate-naming_lib-0.1.2 (c (n "naming_lib") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1cqjjiy252my5hrvw3x9lrf24w1f4f5q0y13dvn49pbj4qmdvc4h")))

(define-public crate-naming_lib-0.1.3 (c (n "naming_lib") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0hkgm4gysnvl9yxgv6rkav54r7fqizggv7rwnjh6nqhl0vr70rwy")))

(define-public crate-naming_lib-0.1.4 (c (n "naming_lib") (v "0.1.4") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "quickcheck") (r "~1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "~0.8.4") (d #t) (k 2)) (d (n "regex") (r "~1.5.4") (d #t) (k 0)))) (h "1li9q9ryi8dgyh6ch04jz8h9d5k8h93wrgdkj2sx7crbxr7qhs7l")))

