(define-module (crates-io na mi naming-conventions) #:use-module (crates-io))

(define-public crate-naming-conventions-1.0.0 (c (n "naming-conventions") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "0q6kyzky5fvqgdk8azqfm1h0dfgicnkc2bwl0154wna199m5wl71") (r "1.68")))

(define-public crate-naming-conventions-1.0.1 (c (n "naming-conventions") (v "1.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)))) (h "14rbkm7i2cacji2x6jvk9h80620bjr5rv7k4zvppz3z63miqm0kf") (r "1.68")))

