(define-module (crates-io na mi naming_clt) #:use-module (crates-io))

(define-public crate-naming_clt-1.0.0 (c (n "naming_clt") (v "1.0.0") (d (list (d (n "assert_cmd") (r "~2.0.2") (d #t) (k 2)) (d (n "atty") (r "~0.2.14") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("suggestions" "color" "vec_map" "wrap_help"))) (k 0)) (d (n "naming_clt_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "predicates") (r "~2.0.3") (d #t) (k 2)))) (h "0wq22vni8lxq2qw7m9lwd0nimsxjcicj9g67vmg2a7a5i0b9n105")))

(define-public crate-naming_clt-1.1.0 (c (n "naming_clt") (v "1.1.0") (d (list (d (n "assert_cmd") (r "~2.0.2") (d #t) (k 2)) (d (n "atty") (r "~0.2.14") (d #t) (k 0)) (d (n "clap") (r "~2.33") (f (quote ("suggestions" "color" "vec_map" "wrap_help"))) (k 0)) (d (n "naming_clt_lib") (r "^0.2.0") (d #t) (k 0)) (d (n "predicates") (r "~2.0.3") (d #t) (k 2)))) (h "0w8jcjd431c4n80n86p536cwynirlhjz2qq8ip4vggw3gfv24f2c")))

