(define-module (crates-io na mi naming_clt_lib) #:use-module (crates-io))

(define-public crate-naming_clt_lib-0.1.0 (c (n "naming_clt_lib") (v "0.1.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "naming_lib") (r "~0.1.4") (d #t) (k 0)) (d (n "regex") (r "~1.5.4") (d #t) (k 0)))) (h "0asslbc0f1y03fkgh8qlhpcfllc5zfsas1dvc4vgnryaigk4vxir")))

(define-public crate-naming_clt_lib-0.2.0 (c (n "naming_clt_lib") (v "0.2.0") (d (list (d (n "fancy-regex") (r "~0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "naming_lib") (r "~0.1.4") (d #t) (k 0)))) (h "1w6c68vpzdik7bw4n0lg846br1wdkx29af1mjd3aiaccv1s1wpzy")))

