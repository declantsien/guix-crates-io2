(define-module (crates-io na rr narrative-macros) #:use-module (crates-io))

(define-public crate-narrative-macros-0.1.0 (c (n "narrative-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0rmndzqhp63sgp09zfcydrjyrvl6aq8ghqzrlq9z00fyi9mx7hjh")))

(define-public crate-narrative-macros-0.2.0 (c (n "narrative-macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1b3npw20xz8aw30jhxyk36mvfn341m8ii76r3jxkia8j0pfi0raw")))

(define-public crate-narrative-macros-0.3.0 (c (n "narrative-macros") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0js3777ych370di5vs0kaldh43qipwsrhh2cg136fnb1aqhhc4jx")))

(define-public crate-narrative-macros-0.4.0 (c (n "narrative-macros") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "00yrn05ybjv6mlkcj4n13mpjp92sa7cgzrihwv4pxbzi1c9rzvnd")))

(define-public crate-narrative-macros-0.5.0 (c (n "narrative-macros") (v "0.5.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0r3hfpz8divs3gfrd2g7s7bxgvg3fg7appkpxyk7vfgdxiivrnpk")))

