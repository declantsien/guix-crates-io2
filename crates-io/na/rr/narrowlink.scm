(define-module (crates-io na rr narrowlink) #:use-module (crates-io))

(define-public crate-narrowlink-0.0.1 (c (n "narrowlink") (v "0.0.1") (h "1s3nv177v7aylbjk7szn6wvki1vyb2yf1z72j9ih0hf6zxlmm3q4") (y #t)))

(define-public crate-narrowlink-0.0.0 (c (n "narrowlink") (v "0.0.0") (h "0akicadkc2r6nd83v5ipz3nkdf00myszp61mckdzn0m2r1w3ipdm")))

