(define-module (crates-io na rr narrative) #:use-module (crates-io))

(define-public crate-narrative-0.1.0 (c (n "narrative") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "narrative-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1jcnlx8qv1fdzmvrdh5b5z0zck070kvsn3fjri2a1iif59rjpifm")))

(define-public crate-narrative-0.2.0 (c (n "narrative") (v "0.2.0") (d (list (d (n "narrative-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1nms9w0f1236q4mrq1dn2v2a20q8nld6zma44db9v278qd2301lz")))

(define-public crate-narrative-0.3.0 (c (n "narrative") (v "0.3.0") (d (list (d (n "narrative-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "136rnr94fqxn7iw6ciwj96haqk5dvmjbm2lk0wss2v53sgbs8ppx")))

(define-public crate-narrative-0.4.0 (c (n "narrative") (v "0.4.0") (d (list (d (n "narrative-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "074fgy103wn0an4ac2jc6qv5a5xdq34yr3h5ksjgrzsrmdfjpnax")))

(define-public crate-narrative-0.5.0 (c (n "narrative") (v "0.5.0") (d (list (d (n "narrative-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0yzihjgas4by1gg9iqs6nmp6h5zr1ywhaw2bhdn22wanzx016x55")))

