(define-module (crates-io na rr narrowlink-message) #:use-module (crates-io))

(define-public crate-narrowlink-message-0.1.0 (c (n "narrowlink-message") (v "0.1.0") (h "0nj04mq6dqrq60ghcyx80f0myssvwqy7wsk3ak82ayrivnpx3bah") (y #t)))

(define-public crate-narrowlink-message-0.0.0 (c (n "narrowlink-message") (v "0.0.0") (h "0n50ysprpf2bi3pfhp4wznswnvnlwfq21vyvliip4y9srjdpb8fj") (y #t)))

