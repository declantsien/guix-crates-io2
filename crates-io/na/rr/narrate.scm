(define-module (crates-io na rr narrate) #:use-module (crates-io))

(define-public crate-narrate-0.1.0 (c (n "narrate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1xk224v30xxc2r44ql7bm2lnlg0psb22jnfbfmq58km8m50kc5v1")))

(define-public crate-narrate-0.1.1 (c (n "narrate") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1vhfgxd842z3chqa1hi0n89gw85hj7nzkzrcy4zpjq9pm461zrwg")))

(define-public crate-narrate-0.1.2 (c (n "narrate") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1cw5ryq1020xxyshz40lnpcfapv7ri68i4sglcgkgy72vlw5iimq")))

(define-public crate-narrate-0.1.3 (c (n "narrate") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1n8wa1fhfmw0m1c65a9k32rv4vbi8hl6gcfiic0w3hc91k768glp")))

(define-public crate-narrate-0.2.0 (c (n "narrate") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0g84rwxkhz4rqjdyd345vqzd79nadbf1l798skqsinnvp64c1n3p")))

(define-public crate-narrate-0.3.0 (c (n "narrate") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "03k47v39n7b46pin9rvc1rn38507xw5c0dafmb01kypdfmq2a97p")))

(define-public crate-narrate-0.4.0 (c (n "narrate") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.63") (o #t) (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "14n8xgairilwb89w6dn6vccvif5l38qan8szn3l3d1snwaav12cw") (f (quote (("error" "anyhow") ("default" "cli-error" "error" "report")))) (s 2) (e (quote (("report" "anyhow" "dep:atty" "dep:colored") ("cli-error" "dep:exitcode") ("anyhow" "dep:anyhow")))) (r "1.61.0")))

(define-public crate-narrate-0.4.1 (c (n "narrate") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.63") (o #t) (d #t) (k 0)) (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "0bcn3ybw7b46f9m93xnl7aqnbp4hnz1n07y3hv25212gb2436zin") (f (quote (("error" "anyhow") ("default" "cli-error" "error" "report")))) (s 2) (e (quote (("report" "anyhow" "dep:atty" "dep:colored") ("cli-error" "dep:exitcode") ("anyhow" "dep:anyhow")))) (r "1.61.0")))

