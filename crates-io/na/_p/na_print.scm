(define-module (crates-io na _p na_print) #:use-module (crates-io))

(define-public crate-na_print-0.1.0 (c (n "na_print") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0mmryg3kai4axgp7zicl8yjyj28lsj72x8lkxdzryigi3fnnvjv5")))

(define-public crate-na_print-1.0.0 (c (n "na_print") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0vag2f0nhm72xgj2zjzwb7mi2921k7smrb0pg9bl2ibvgp2vd38c")))

