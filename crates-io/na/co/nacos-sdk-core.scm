(define-module (crates-io na co nacos-sdk-core) #:use-module (crates-io))

(define-public crate-nacos-sdk-core-0.1.0 (c (n "nacos-sdk-core") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (d #t) (k 0)))) (h "1kfiy5vv0s545nhdhfq0hdkjflxc744d7d56mxd6882asvyxfnzh")))

