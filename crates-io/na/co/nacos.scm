(define-module (crates-io na co nacos) #:use-module (crates-io))

(define-public crate-nacos-0.0.1 (c (n "nacos") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.39") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.39") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j3bj7xw3pqv00gvb9d2c1adsajd3k5gf8xqlrd2wp34w2ml34mn")))

