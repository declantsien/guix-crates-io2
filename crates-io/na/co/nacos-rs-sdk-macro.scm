(define-module (crates-io na co nacos-rs-sdk-macro) #:use-module (crates-io))

(define-public crate-nacos-rs-sdk-macro-0.1.0 (c (n "nacos-rs-sdk-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "0a1jjwnv91w4pvyywfv0n0l5ghnqiyx6jn8ajqb76d1qkyprvnyw") (y #t)))

(define-public crate-nacos-rs-sdk-macro-0.1.1 (c (n "nacos-rs-sdk-macro") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "02g14bf1cw5y12v87wiwpi5acyqsflfjvbxqs6q5ihb40jp6wgap")))

(define-public crate-nacos-rs-sdk-macro-0.2.0 (c (n "nacos-rs-sdk-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03rippl44l66v682c1xpacjl6i3ddaafc8s42z80kpq61i5ds37c")))

(define-public crate-nacos-rs-sdk-macro-0.2.1 (c (n "nacos-rs-sdk-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xxzsh7jfjh4kjbqlq7j3xivp6s5vkx0dsxb2g1dnss6qwvb0rib")))

(define-public crate-nacos-rs-sdk-macro-0.2.2 (c (n "nacos-rs-sdk-macro") (v "0.2.2") (d (list (d (n "heck") (r "^0.3.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1s3qsh5mnlf7zw72n1l37hpx4ncs4rrh3137nbvf1ginazby859k")))

