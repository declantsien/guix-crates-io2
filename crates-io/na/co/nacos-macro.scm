(define-module (crates-io na co nacos-macro) #:use-module (crates-io))

(define-public crate-nacos-macro-0.1.0 (c (n "nacos-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0vp9mb6hv6lp24lbjwg8i0q3fmjwir0maf3qvhh9gwhpzzqpfyw3")))

