(define-module (crates-io na rv narvie-processor) #:use-module (crates-io))

(define-public crate-narvie-processor-0.1.0 (c (n "narvie-processor") (v "0.1.0") (h "0bhjh83vwl7xhg1lvhjywjvnrwd9d14q4xgg9lj182w56lka7a2v")))

(define-public crate-narvie-processor-0.2.0 (c (n "narvie-processor") (v "0.2.0") (h "1v9wrh335756lib4dyxqvliqsni9n4ccbwsg9w76c8csgl9fybgq")))

(define-public crate-narvie-processor-0.3.0 (c (n "narvie-processor") (v "0.3.0") (h "1r7jj90izhlhcygj0di73wppq3lwgx5lh2ymvcld6b5w2l6n4zp2")))

(define-public crate-narvie-processor-0.3.1 (c (n "narvie-processor") (v "0.3.1") (h "1cv9ha48z6rjhhdpazhbx96p28w7vgc5hhvs345i3x0agc18km80")))

(define-public crate-narvie-processor-0.3.2 (c (n "narvie-processor") (v "0.3.2") (h "0qm91bwzgpia7fs4mg2ja1rp8yhs59yah6i0apmvnhh8p0ci47x9")))

(define-public crate-narvie-processor-0.3.3 (c (n "narvie-processor") (v "0.3.3") (h "00yw3ji7pp86ya5gp63zj5hnpxw9492axpz5va41ncr86wnpcbka")))

