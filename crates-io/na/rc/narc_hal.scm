(define-module (crates-io na rc narc_hal) #:use-module (crates-io))

(define-public crate-narc_hal-0.1.0 (c (n "narc_hal") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.4.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.2.3") (f (quote ("stm32l0x1"))) (d #t) (k 0)))) (h "047fc2xfzxygwmnb6g10w2dvsa3x3r6898hk177lgmciz12ks9lp")))

(define-public crate-narc_hal-0.1.1 (c (n "narc_hal") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.4.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.2.3") (f (quote ("stm32l0x1"))) (d #t) (k 0)))) (h "10dmy7jdmwczywv4gfxk2sw83pzwiw2zmq7p8b29crn7630wg8fn")))

(define-public crate-narc_hal-0.1.2 (c (n "narc_hal") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.4.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.2.3") (f (quote ("stm32l0x1"))) (d #t) (k 0)))) (h "11pjz0z73m1igp3fkag8lqxw12hny3ylc8x98g74asnbfi03lsb6")))

(define-public crate-narc_hal-0.1.3 (c (n "narc_hal") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "cortex-m") (r "^0.5.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.4.0") (d #t) (k 0)) (d (n "stm32l0") (r "^0.2.3") (f (quote ("stm32l0x1" "rt"))) (d #t) (k 0)))) (h "1np7hjz6w54d6iprpyl1qig7rs1ifkhdanpn4m1p1xiiqmqmc3xa")))

