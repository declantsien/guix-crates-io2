(define-module (crates-io na rc narcissistic) #:use-module (crates-io))

(define-public crate-narcissistic-0.1.0 (c (n "narcissistic") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0dv7q5xwakvk4h2bmfg41c86cb09qknlj1yk09p1kxnsmmv936d1")))

(define-public crate-narcissistic-0.2.0 (c (n "narcissistic") (v "0.2.0") (d (list (d (n "convert-base") (r "^1.1.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "05vbvw63l174522wli180dib8qxx6k57c3qcrpgb0pri8p9crzcb")))

(define-public crate-narcissistic-0.2.1 (c (n "narcissistic") (v "0.2.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1bc3ykc0iv06mskpqxlhvhk27c0nm8rwpa29xqphn6rmsl8mq96a")))

(define-public crate-narcissistic-0.3.0 (c (n "narcissistic") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1x6k5rgi19k3mv8qlvl1dvm4f0im5yb7nzxgnayvmajbd8y440c4")))

