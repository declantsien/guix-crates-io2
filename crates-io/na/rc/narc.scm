(define-module (crates-io na rc narc) #:use-module (crates-io))

(define-public crate-narc-0.1.0 (c (n "narc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "nitro_fs") (r "^0.1") (d #t) (k 0)))) (h "0390i5q9b76w2f9xdncpav429mgz4i98v1r1h4wfbw87qs78xdwh")))

(define-public crate-narc-0.1.1 (c (n "narc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nitro_fs") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1vd5gnf1fnmj6jq5nz8bqrxynl1g5jxargjny7lbd4hvk37j96dx")))

(define-public crate-narc-0.2.0 (c (n "narc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nitro_fs") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0ch6hhvbz4ggfhv8fa45arl25zhkl9hmv0x8s3sf4dbya7wx1ych")))

