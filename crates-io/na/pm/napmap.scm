(define-module (crates-io na pm napmap) #:use-module (crates-io))

(define-public crate-napmap-0.1.1 (c (n "napmap") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("sync" "time" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("time" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "0qas4jnw8c34dzd3snh9gihlrlxvr898p8055yvk9lsqp62yaf6j")))

