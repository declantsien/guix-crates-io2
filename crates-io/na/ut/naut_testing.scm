(define-module (crates-io na ut naut_testing) #:use-module (crates-io))

(define-public crate-naut_testing-0.14.3 (c (n "naut_testing") (v "0.14.3") (d (list (d (n "naut_core") (r "=0.14.3") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "07q0w6mskgzmifvhnqvjxvcj7i7m3qp8lzq7vyg6ipzyhiy082y5")))

(define-public crate-naut_testing-0.14.4 (c (n "naut_testing") (v "0.14.4") (d (list (d (n "naut_core") (r "=0.14.4") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "11qhyliygk8nqp2zspcz7xf5aw180qnm971v568b9lhi6f5k0hc6")))

(define-public crate-naut_testing-0.14.5 (c (n "naut_testing") (v "0.14.5") (d (list (d (n "naut_core") (r "=0.14.5") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "0fk93m103xn3fy3f4q9s0v6jdvabvlbz6224sib3abli1rrxdsmx")))

(define-public crate-naut_testing-0.14.6 (c (n "naut_testing") (v "0.14.6") (d (list (d (n "naut_core") (r "=0.14.6") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "14xqjjdscs7csm5rrdl67dnv64y61a7wsy810msc1wf5ix50fq96")))

(define-public crate-naut_testing-0.14.8 (c (n "naut_testing") (v "0.14.8") (d (list (d (n "naut_core") (r "^0.14.8") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "03cxjdwsxycgq5m2ilsq6pwv782ghdq4qyw2jvnrxp74d8i408jv")))

(define-public crate-naut_testing-0.14.9 (c (n "naut_testing") (v "0.14.9") (d (list (d (n "naut_core") (r "^0.14.9") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "03qqzax2x2vh0xvfirlciffswwf57xrnl0laz4g5y86nbdv6fy0y")))

(define-public crate-naut_testing-0.14.10 (c (n "naut_testing") (v "0.14.10") (d (list (d (n "naut_core") (r "^0.14.10") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "14bik9vfh14a1q4qqy1clixla2jslahzp25p08ir6gp6scz0vf5a")))

(define-public crate-naut_testing-0.14.11 (c (n "naut_testing") (v "0.14.11") (d (list (d (n "naut_core") (r "^0.14.11") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "0dyzmac12s3iad3hlkd0rhy40sb4b7d1mag36lrwzb1rnqkfsilz")))

(define-public crate-naut_testing-0.14.12 (c (n "naut_testing") (v "0.14.12") (d (list (d (n "naut_core") (r "^0.14.12") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "1ql10zfr101xkfk8y9m2k7p2rjgaa6hchrj6i6parf3v9fkd1qlx")))

(define-public crate-naut_testing-0.14.13 (c (n "naut_testing") (v "0.14.13") (d (list (d (n "naut_core") (r "^0.14.13") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "01m57jkzrs2s1k9f6khjv9agz22ankip8yyay7l8npjydwbqrkq4")))

(define-public crate-naut_testing-0.14.14 (c (n "naut_testing") (v "0.14.14") (d (list (d (n "naut_core") (r "^0.14.14") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "022n1d18val508zadj0h6i2wnjgjwpwq67d66j410fn3ax61mh4q")))

(define-public crate-naut_testing-0.14.15 (c (n "naut_testing") (v "0.14.15") (d (list (d (n "naut_core") (r "^0.14.15") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "07f2hpl23qz0zhzfr2jrmzzqzs5zm2zxzj83r92whj9gxn0rbsja")))

(define-public crate-naut_testing-0.14.18 (c (n "naut_testing") (v "0.14.18") (d (list (d (n "naut_core") (r "^0.14.18") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "11fv7242gkl04jyigvw97635qj593c52jrdwfzc34ryarcp4s6hi")))

(define-public crate-naut_testing-0.14.19 (c (n "naut_testing") (v "0.14.19") (d (list (d (n "naut_core") (r "^0.14.19") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "0qr4ksbqi5ma784anyzbcf2y7jfbd4nwwvjajvadysi6mzmpvm84")))

(define-public crate-naut_testing-0.14.20 (c (n "naut_testing") (v "0.14.20") (d (list (d (n "naut_core") (r "^0.14.20") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "0p1qla28zlcbrk49bwq1dfxd6hkz42jwp2pywmavgh00acfbl9nx")))

(define-public crate-naut_testing-0.14.21 (c (n "naut_testing") (v "0.14.21") (d (list (d (n "naut_core") (r "^0.14.21") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "1n4njsvw594dwfjx2642bjf0154v2ycji9hp43w0j83z95l4m1x8")))

(define-public crate-naut_testing-0.14.22 (c (n "naut_testing") (v "0.14.22") (d (list (d (n "naut_core") (r "^0.14.22") (d #t) (k 0)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 0)))) (h "0vysb4iq9cc6dfffcfq0fb93agskigw21c9gbhsrrlil7sj1dfi2")))

