(define-module (crates-io na ut naut_io) #:use-module (crates-io))

(define-public crate-naut_io-0.14.3 (c (n "naut_io") (v "0.14.3") (d (list (d (n "naut_core") (r "=0.14.3") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.0") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0gi70mlm9hq4ac0lialpzd2bs107x9cq5b8cdz5aiyjms3f7injq")))

(define-public crate-naut_io-0.14.4 (c (n "naut_io") (v "0.14.4") (d (list (d (n "naut_core") (r "=0.14.4") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.0") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0hr6w2h6rd3rqdax1xpm340cz3dfin139f9p3na73kz47fa5x28y")))

(define-public crate-naut_io-0.14.5 (c (n "naut_io") (v "0.14.5") (d (list (d (n "naut_core") (r "=0.14.5") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.0") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0gy3vwcdr7rvzn1gigcr2nnriad66rigkjpcw8ncxlp57ps6c6hm")))

(define-public crate-naut_io-0.14.6 (c (n "naut_io") (v "0.14.6") (d (list (d (n "naut_core") (r "=0.14.6") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.0") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "08i44afnpysv0q5rqsy3jjiwp9vqn52a2g7rmsilnib97jkv4l93")))

(define-public crate-naut_io-0.14.8 (c (n "naut_io") (v "0.14.8") (d (list (d (n "naut_core") (r "^0.14.8") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.8") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0ymd21vcbcxky3vf51vsfmqg7y9kvwvml6vk0acxq0p0l7mhksh5")))

(define-public crate-naut_io-0.14.9 (c (n "naut_io") (v "0.14.9") (d (list (d (n "naut_core") (r "^0.14.9") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.9") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1dyh4b51qmg8jq2ppnsy9qhdghy7rgcbk276l2kzjah5wmlh26v2")))

(define-public crate-naut_io-0.14.10 (c (n "naut_io") (v "0.14.10") (d (list (d (n "naut_core") (r "^0.14.10") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.10") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1ik2m1mq1y0jzy4lcv9s92i11gr4j7kp24rgglw5ngvxan2g3cqm")))

(define-public crate-naut_io-0.14.11 (c (n "naut_io") (v "0.14.11") (d (list (d (n "naut_core") (r "^0.14.11") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.11") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1h8jvzp8xy4rsc58gm763q5m17250v2clic26j8sgi9zz7g48jv9")))

(define-public crate-naut_io-0.14.13 (c (n "naut_io") (v "0.14.13") (d (list (d (n "naut_core") (r "^0.14.13") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.13") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "19g4n53k2ccgg3mhya2yjmnc2bsgnnbzg6cvw7s7zzx4gxvvnawz")))

(define-public crate-naut_io-0.14.14 (c (n "naut_io") (v "0.14.14") (d (list (d (n "naut_core") (r "^0.14.14") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.14") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "19l3lkw9fqg68wfx6jddvibp3vl88jg8ancr94qh6i205y3nb6j3")))

(define-public crate-naut_io-0.14.15 (c (n "naut_io") (v "0.14.15") (d (list (d (n "naut_core") (r "^0.14.15") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.15") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "153impj3va6k9k44b149czd909bj5n9ni12fpssj35dvscpvg9lr")))

(define-public crate-naut_io-0.14.18 (c (n "naut_io") (v "0.14.18") (d (list (d (n "naut_core") (r "^0.14.18") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.18") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "17z8jnvv11jlgzwm9ka04az2wvr7pk33357d9vgljy6n4pxzhhf7")))

(define-public crate-naut_io-0.14.19 (c (n "naut_io") (v "0.14.19") (d (list (d (n "naut_core") (r "^0.14.19") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.19") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0adxm3id8kzww5z6d7n7w35g49wr5ivr9id7nxilscggbf3qk1yh")))

(define-public crate-naut_io-0.14.20 (c (n "naut_io") (v "0.14.20") (d (list (d (n "naut_core") (r "^0.14.20") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.20") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1m25iz17kx6z0hq9fl6chzqlr5hni9654vngq2mymmrbyl1ar8hm")))

(define-public crate-naut_io-0.14.21 (c (n "naut_io") (v "0.14.21") (d (list (d (n "naut_core") (r "^0.14.21") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.21") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0asxljyd943scjqdnmirr1ahi80ka085krjj1zpka5iwwfvvzq86")))

(define-public crate-naut_io-0.14.22 (c (n "naut_io") (v "0.14.22") (d (list (d (n "naut_core") (r "^0.14.22") (d #t) (k 0)) (d (n "naut_testing") (r "^0.14.22") (d #t) (k 2)) (d (n "parameterized") (r "^0.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1ik3jyph01rf8hzxwi7x1lakvsg69zxz8pwkjxl57j7cqld3gmmg")))

