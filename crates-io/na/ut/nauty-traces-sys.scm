(define-module (crates-io na ut nauty-traces-sys) #:use-module (crates-io))

(define-public crate-nauty-Traces-sys-0.1.0 (c (n "nauty-Traces-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0fdjimsxsbxhbjg3wlqhf74kh0285v102k5s4whq5n1flxz2xgpj")))

(define-public crate-nauty-Traces-sys-0.1.1 (c (n "nauty-Traces-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1dn0g3pahvi8mc96sbxd9nn3hs4vb0nvxybcffhsgh1q69cwx15i")))

(define-public crate-nauty-Traces-sys-0.1.2 (c (n "nauty-Traces-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1fllsjzq4kk0v16zgjbvq0sphav0f78mcfisjab2vza95r9lajzx")))

(define-public crate-nauty-Traces-sys-0.2.0 (c (n "nauty-Traces-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "11cscvicbiwkqj9qmlj3yf6hdzghyhx5n74g3k816q78xmia8s1h")))

(define-public crate-nauty-Traces-sys-0.3.0 (c (n "nauty-Traces-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "050mfg3h4j12gh9zcrn6da5j65f9r70s4yif4cb7khx8sjfibjsv") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

(define-public crate-nauty-Traces-sys-0.4.0 (c (n "nauty-Traces-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "16s6np25wp3awbvij4bhgagbvj6rbsrh75vswj7iqw8wbmcyx6zp") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

(define-public crate-nauty-Traces-sys-0.5.0 (c (n "nauty-Traces-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0slqpvcyzc9na9nw7xfmsa3yq9zcp5f8sj0g3h3x9khn46gwyf84") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

(define-public crate-nauty-Traces-sys-0.6.0 (c (n "nauty-Traces-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "11w8cn18sxd8wfbj6cc0qby3brln4254qg3nmr6908scn5nzxp3m") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

(define-public crate-nauty-Traces-sys-0.6.1 (c (n "nauty-Traces-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1m2rgmj2j3q472sjz19kwy9xdlw55vm4qi0i5a4d2w7xb9cp20h7") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

(define-public crate-nauty-Traces-sys-0.7.0 (c (n "nauty-Traces-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0vwxjnd633ygzrdmk569fxnnsfz4fhnra2frkzwxygs8nars4h0v") (f (quote (("tls" "bundled") ("popcnt" "bundled") ("native" "bundled" "clz" "popcnt") ("default" "bundled" "tls") ("clz" "bundled") ("bundled" "cc" "libc"))))))

