(define-module (crates-io na ut nautilus) #:use-module (crates-io))

(define-public crate-nautilus-0.1.0 (c (n "nautilus") (v "0.1.0") (h "1kn3jp47j357xivyckww13x8hklig42p9i5srzmdihpfxly38wqv")))

(define-public crate-nautilus-0.1.1 (c (n "nautilus") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "17hk2z5gcpzqygcy6q5h6q1542khwck7kw0lq0ci3c6bi98xmh1c")))

