(define-module (crates-io na ut nautic_dns) #:use-module (crates-io))

(define-public crate-nautic_dns-0.0.1-alpha (c (n "nautic_dns") (v "0.0.1-alpha") (d (list (d (n "bitter") (r "^0.6") (d #t) (k 0)) (d (n "bytes") (r "^1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0") (d #t) (k 0)) (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.30") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1p6r3lj55y0ghb9j2hpf78pz7v960b8pndr7b37midq0k4bpidgz")))

