(define-module (crates-io na ut naut_core) #:use-module (crates-io))

(define-public crate-naut_core-0.14.2 (c (n "naut_core") (v "0.14.2") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0rx6n52pz9j1znvl5d35l8sia5y4fhy1lylwpzf3xidnm2cn8caw")))

(define-public crate-naut_core-0.14.3 (c (n "naut_core") (v "0.14.3") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0i56jzz8pd46pkdc3rcgsi17kpz33v24621lirrr3l58330zfd6a")))

(define-public crate-naut_core-0.14.4 (c (n "naut_core") (v "0.14.4") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1w3hp8m287ynvgjgq8i4hzf6l1nxwwzadwcl9rhhyzrgy9xxz1ih")))

(define-public crate-naut_core-0.14.5 (c (n "naut_core") (v "0.14.5") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1wl90fxadn14zc61sr0k64p0hr19cf5fl3xz0asfbmmx4kricy2d")))

(define-public crate-naut_core-0.14.6 (c (n "naut_core") (v "0.14.6") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1avjkn1b9p7hihhkljxbas1r6l5drg57634r3p8fq4a0qfiq5mjd")))

(define-public crate-naut_core-0.14.7 (c (n "naut_core") (v "0.14.7") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1x005a9bl28xc5kg5r3fqvlxmmz92417sq96svmq9c9jqw0k883i")))

(define-public crate-naut_core-0.14.8 (c (n "naut_core") (v "0.14.8") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1dgyja3fyfp98brsxp19011y8pbmirxacqsgzkn7g7wr4dq5pxs0")))

(define-public crate-naut_core-0.14.9 (c (n "naut_core") (v "0.14.9") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0pyksds4na5fiz316pj7ajirpcwqp595n1jsjklxs4da3p4hg2hk")))

(define-public crate-naut_core-0.14.10 (c (n "naut_core") (v "0.14.10") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1ldw9iyvfpdn7vv3mc58kr5y8wirmcx120rc9hvc3iqda2z8d3dp")))

(define-public crate-naut_core-0.14.11 (c (n "naut_core") (v "0.14.11") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "12kb1yi183knl35skvcxgaj3msgc68ap4mzgh64l0q8fp243gl1x")))

(define-public crate-naut_core-0.14.12 (c (n "naut_core") (v "0.14.12") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0y7lx7rcdhzpymf86jjij44ib5vgh6xnc6xs4586nj59d62yzzai")))

(define-public crate-naut_core-0.14.13 (c (n "naut_core") (v "0.14.13") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0p8s8pkgqfxl1df7fbwic3dbv10i0lprf7dvsl8jdvfznjd8yq54")))

(define-public crate-naut_core-0.14.14 (c (n "naut_core") (v "0.14.14") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "02kyg8vga5wpvz3ddh17sp763qpgi27yvya7xrjbpm0x69vbrsns")))

(define-public crate-naut_core-0.14.15 (c (n "naut_core") (v "0.14.15") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "04iyanfdns8ai7hxkgjk1h4vdx59ylpd32vfwri0g529p7wzyrp9")))

(define-public crate-naut_core-0.14.16 (c (n "naut_core") (v "0.14.16") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "04lybfcks66n0sm6w5807pj3wy3ni3dbhbnl62dn4mvs8hnv3lgp")))

(define-public crate-naut_core-0.14.17 (c (n "naut_core") (v "0.14.17") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0gg4gkydwza6hi870y6vgrb5idj6mx0qkb68gadmpjwnlgfi0z6c")))

(define-public crate-naut_core-0.14.18 (c (n "naut_core") (v "0.14.18") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "053n739f28jwi43rv3cma507xzz91xkcpy9j30z720jzd204hl5l")))

(define-public crate-naut_core-0.14.19 (c (n "naut_core") (v "0.14.19") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "01havwmmd77pxjs066zk34dj68gf7795n38r0kwrpc4m1fb8yhdl")))

(define-public crate-naut_core-0.14.20 (c (n "naut_core") (v "0.14.20") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "1dp80g4sp9kngkjk97c8wz2kc1mnnx7gwcacdfd1zq7iwbm88c2n")))

(define-public crate-naut_core-0.14.21 (c (n "naut_core") (v "0.14.21") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "0qqn8amkvydd59hij2lgqdykj7mj9da7j3q3inmmcsqyvjr54480")))

(define-public crate-naut_core-0.14.22 (c (n "naut_core") (v "0.14.22") (d (list (d (n "image") (r "^0.23.11") (f (quote ("avif"))) (d #t) (k 0)))) (h "174bqpnic7rqdpd11qsqgbaha702fmm5cpbpr68hvkn7jzhav36h")))

