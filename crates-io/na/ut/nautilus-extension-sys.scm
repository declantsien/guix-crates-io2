(define-module (crates-io na ut nautilus-extension-sys) #:use-module (crates-io))

(define-public crate-nautilus-extension-sys-0.1.0 (c (n "nautilus-extension-sys") (v "0.1.0") (d (list (d (n "gio-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.1") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dfr7pnj2hy9idd0j07zyi39m7ij6rs504ymjy7w7lxyh200xyk6")))

(define-public crate-nautilus-extension-sys-0.2.0 (c (n "nautilus-extension-sys") (v "0.2.0") (d (list (d (n "gio-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.1") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qqzjx2nza4pyfq3rngyk03wg6rvgs33xddva4wnxf15h75hmkgr")))

(define-public crate-nautilus-extension-sys-0.2.2 (c (n "nautilus-extension-sys") (v "0.2.2") (d (list (d (n "gio-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3.1") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ni06pcfcwdmfs0iqm2w25nhbvhbgpl8al52sxgjwrm0bbwx210r")))

(define-public crate-nautilus-extension-sys-0.3.0 (c (n "nautilus-extension-sys") (v "0.3.0") (d (list (d (n "gio-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.5.0") (f (quote ("v3_18"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18cdfkxidc9ncwf0sdinn3zq11kr0d6hbbiq60a30addklwisj55")))

(define-public crate-nautilus-extension-sys-0.4.0 (c (n "nautilus-extension-sys") (v "0.4.0") (d (list (d (n "gio-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.5.0") (f (quote ("v3_18"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0injb867zf2a1kkqnp3pswgw0s9kdidrsa9rqiyn28ngj9q0rnb2")))

(define-public crate-nautilus-extension-sys-0.5.0 (c (n "nautilus-extension-sys") (v "0.5.0") (d (list (d (n "gio-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (f (quote ("v3_18"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sffyirizcfs8ydqmmf932sw3fkpjz4zpl133v0ia8l3g8hr00d5")))

(define-public crate-nautilus-extension-sys-0.7.0 (c (n "nautilus-extension-sys") (v "0.7.0") (d (list (d (n "gio-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15.3") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iyrlzr93bgqvjsa67z0afg8626yv0921ddxjmc3j1mx86nd1iw6")))

(define-public crate-nautilus-extension-sys-0.8.0 (c (n "nautilus-extension-sys") (v "0.8.0") (d (list (d (n "gio-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "glib-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15.3") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l9syq8v3gnj65prhncyp2wk7cli34gny33ygckwsc239g7jpb3i")))

