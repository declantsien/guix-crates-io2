(define-module (crates-io na nv nanvm) #:use-module (crates-io))

(define-public crate-nanvm-0.0.0 (c (n "nanvm") (v "0.0.0") (h "1x9jikvl0609iv57gjkyig5sif90k7jv0xriqdry5pla2fa0pz5v")))

(define-public crate-nanvm-0.0.1 (c (n "nanvm") (v "0.0.1") (d (list (d (n "io-impl") (r "^0.8.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.1") (d #t) (k 0)))) (h "07mpbgxj10b9nwc0acpp31asfr671mbrj429cwwcz68xc464w5q8")))

(define-public crate-nanvm-0.0.2 (c (n "nanvm") (v "0.0.2") (d (list (d (n "io-impl") (r "^0.9.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.2") (d #t) (k 0)))) (h "0xqndprqkllmjvzlfxvp71ghh391zy07d5abvgvqr69qjid8hvrx")))

(define-public crate-nanvm-0.0.3 (c (n "nanvm") (v "0.0.3") (d (list (d (n "io-impl") (r "^0.10.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.3") (d #t) (k 0)))) (h "0an5iw50dria1wsswxwjzl3czhng9z4ilz8bnr7gslnwnmybiv3f")))

(define-public crate-nanvm-0.0.4 (c (n "nanvm") (v "0.0.4") (d (list (d (n "io-impl") (r "^0.10.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.4") (d #t) (k 0)))) (h "002fwq4mvdsz4vqrn793ck8wl5czd3hmg1p849fb80vbw9iabxzs")))

(define-public crate-nanvm-0.0.5 (c (n "nanvm") (v "0.0.5") (d (list (d (n "io-impl") (r "^0.11.0") (d #t) (k 0)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "nanvm-lib") (r "^0.0.5") (d #t) (k 0)))) (h "1y9jchpiwyvamv0gksayhvilabayjqxn9ydiipfmqgxbq8wiflqm")))

