(define-module (crates-io na nv nanval) #:use-module (crates-io))

(define-public crate-nanval-0.1.0 (c (n "nanval") (v "0.1.0") (h "1bnpp4fm1xpxdf6ym76lgnnbdx6b69kyfbwnccrjlral2gzywzlj")))

(define-public crate-nanval-0.1.1 (c (n "nanval") (v "0.1.1") (h "1jib52krn36my57skrdchpd6anh554iiapw1wmaidn2zv89k3gcx")))

(define-public crate-nanval-0.2.0 (c (n "nanval") (v "0.2.0") (h "1vpl6z4j13zgkk3rxy5h9c65c9d993d402p9rds418sgai9lh78w") (f (quote (("std") ("default" "std" "cell") ("cell"))))))

(define-public crate-nanval-0.2.1 (c (n "nanval") (v "0.2.1") (h "1hxsnp7dclh65irpdnnb15fmn9xchkabn3lab9rwv9cq8yv3q2il") (f (quote (("std") ("default" "std" "cell") ("cell"))))))

