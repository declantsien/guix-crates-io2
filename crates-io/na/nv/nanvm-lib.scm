(define-module (crates-io na nv nanvm-lib) #:use-module (crates-io))

(define-public crate-nanvm-lib-0.0.0 (c (n "nanvm-lib") (v "0.0.0") (d (list (d (n "wasm-bindgen-test") (r "^0.3.39") (d #t) (k 2)))) (h "1fdimcmchyqp38by2j9iymzbv28g3m61s5dnzx7h40x6dd1qpkas")))

(define-public crate-nanvm-lib-0.0.1 (c (n "nanvm-lib") (v "0.0.1") (d (list (d (n "io-test") (r "^0.8.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.39") (d #t) (k 2)))) (h "0qxzaj6z6g5q5wzz6wsk5bqfg2fah4z3zs4cc8vixqgirg59dkip")))

(define-public crate-nanvm-lib-0.0.2 (c (n "nanvm-lib") (v "0.0.2") (d (list (d (n "io-test") (r "^0.9.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "0xvj2qvxybp83ddiq54d61sjl526ywxms30yalr5w7cw9fy656fy")))

(define-public crate-nanvm-lib-0.0.3 (c (n "nanvm-lib") (v "0.0.3") (d (list (d (n "io-test") (r "^0.10.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "0503r79hqj3i3rjbw27b9rachf68addsw7icyw4k9ina0w4r978r")))

(define-public crate-nanvm-lib-0.0.4 (c (n "nanvm-lib") (v "0.0.4") (d (list (d (n "io-test") (r "^0.10.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "0dnp3dk0sz4c33dasak8g3x4lapfriq36gv42qzc41p7f8hb90ms")))

(define-public crate-nanvm-lib-0.0.5 (c (n "nanvm-lib") (v "0.0.5") (d (list (d (n "io-test") (r "^0.11.0") (d #t) (k 2)) (d (n "io-trait") (r "^0.11.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.42") (d #t) (k 2)))) (h "1ps04md5z6mcszna1nl4gi9izmcgamsqyc3pb71rx8abgjxm0x4q")))

