(define-module (crates-io na de nade) #:use-module (crates-io))

(define-public crate-nade-0.1.0 (c (n "nade") (v "0.1.0") (d (list (d (n "macro-v") (r "^0.1.2") (k 0)) (d (n "nade-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "0mk21qnpnrnls4w4j3am8wf140lcx83fqyfc7859jmliign35adv")))

(define-public crate-nade-0.1.1 (c (n "nade") (v "0.1.1") (d (list (d (n "macro-v") (r "^0.1.2") (k 0)) (d (n "nade-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "0vmg6avmqvjk97qk3jsb09bij3jwfbzlsp1p1b8fzm2c8r3ibwwz")))

(define-public crate-nade-0.2.0 (c (n "nade") (v "0.2.0") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "1p8nw2sxl1wvszkyj11gkf1bwib1b2985ppyhj4a44l7h83ylq9l")))

(define-public crate-nade-0.2.1 (c (n "nade") (v "0.2.1") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.2.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "1z8fz9qglhsibmhcfmfncq02b0a9yjshwsf2wydg4f8bqfchsy3p")))

(define-public crate-nade-0.2.2 (c (n "nade") (v "0.2.2") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.2.2") (d #t) (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "0hjz4vaa7brax1bpcpysxm8lf112n5yzc3j9n091409ywa9ncbxm")))

(define-public crate-nade-0.3.0 (c (n "nade") (v "0.3.0") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.3.0") (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "11nkvb5ll342axfkyfic695kpjyddjf1hdj37ra3gv5m6n0vd989")))

(define-public crate-nade-0.3.1 (c (n "nade") (v "0.3.1") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.3.1") (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "1qsn64bcj1i0vvm4w8wks1dywy6gvqn0an19ckvj03jk0nkjmdsc")))

(define-public crate-nade-0.3.2 (c (n "nade") (v "0.3.2") (d (list (d (n "macro-v") (r "^0.1.3") (k 0)) (d (n "nade-macro") (r "^0.3.2") (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "13vp9b4hfn7f32axbj0lmhgv6hq7cgr76kkb5p62ip4vinc2799s")))

(define-public crate-nade-0.3.3 (c (n "nade") (v "0.3.3") (d (list (d (n "macro-v") (r "^0.1") (k 0)) (d (n "nade-macro") (r "^0.3.3") (k 0)) (d (n "trybuild") (r "^1") (k 2)))) (h "167z4bib2pwy6xk8d7f0vmy3zwybj8d30hakfw44ialjzsi53kg0")))

