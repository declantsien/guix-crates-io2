(define-module (crates-io na de nadesiko3) #:use-module (crates-io))

(define-public crate-nadesiko3-0.1.0 (c (n "nadesiko3") (v "0.1.0") (h "00kwa2f1x2xffy96wrf5bhbb1w9rfmhy88ndjpcm490y9mwv58kd")))

(define-public crate-nadesiko3-0.1.1 (c (n "nadesiko3") (v "0.1.1") (h "1m65bi2yzcj8ga2vc8whgzadrjbcp8zl6f9xa42drq1lngbma58j")))

(define-public crate-nadesiko3-0.1.2 (c (n "nadesiko3") (v "0.1.2") (h "1nj4j5ax3qizdcxbs78kchn611yyvj64f114lc2agmxjjs56k48w")))

(define-public crate-nadesiko3-0.1.3 (c (n "nadesiko3") (v "0.1.3") (h "0db1izfxbbn3hddi8w8h63bgknkilq5rf2rr687p6zz7y88f981x")))

