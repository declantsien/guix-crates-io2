(define-module (crates-io na de nade-macro) #:use-module (crates-io))

(define-public crate-nade-macro-0.1.0 (c (n "nade-macro") (v "0.1.0") (d (list (d (n "prettyplease") (r "^0.1.25") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1nbk3h2slianwhyf7jl7s00yizvwrcxvl8df1mdwid2kc9zmdgcj")))

(define-public crate-nade-macro-0.1.1 (c (n "nade-macro") (v "0.1.1") (d (list (d (n "prettyplease") (r "^0.1.25") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0hlf0f4p6ic5bl9q78j7jnv354nczambnmwj28nw1qz9w126b44x")))

(define-public crate-nade-macro-0.2.0 (c (n "nade-macro") (v "0.2.0") (d (list (d (n "prettyplease") (r "^0.1.25") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1yscwjxa1fqfylp52ia43lb617vr8mxknydk04ffwagz2fj5rz6m")))

(define-public crate-nade-macro-0.2.1 (c (n "nade-macro") (v "0.2.1") (d (list (d (n "prettyplease") (r "^0.1.25") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^1") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "11cmbl94chp81akwdbg40w2ffbcvb8nndxj68dmg07jhpd1fmp56")))

(define-public crate-nade-macro-0.2.2 (c (n "nade-macro") (v "0.2.2") (d (list (d (n "prettyplease") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0bias8pxdyqxhbffrypx9fcfwicdjmzxqnh7vayz3bym9sz7np9l")))

(define-public crate-nade-macro-0.3.0 (c (n "nade-macro") (v "0.3.0") (d (list (d (n "prettyplease") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0cdckzffsr68baymlc5zwq6cxanmanh03931c69n1l0dqwfvlgky")))

(define-public crate-nade-macro-0.3.1 (c (n "nade-macro") (v "0.3.1") (d (list (d (n "prettyplease") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1jcfvbxibpbcaxf32bdmgwm23zpdz7wd03kn7k6g55wy407hwfxp")))

(define-public crate-nade-macro-0.3.2 (c (n "nade-macro") (v "0.3.2") (d (list (d (n "prettyplease") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing"))) (k 0)))) (h "1j3imb039gfdszws2sa9qchm61r5vc7dgfcn5rqzxacanb5i7mkk")))

(define-public crate-nade-macro-0.3.3 (c (n "nade-macro") (v "0.3.3") (d (list (d (n "prettyplease") (r "^0.2") (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("clone-impls" "extra-traits" "parsing" "proc-macro" "printing"))) (k 0)))) (h "0983mw69wk1rikj1is1ha1z9sjs4x5ig9nnsi6bh3m2jz55447p1")))

