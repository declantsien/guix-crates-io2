(define-module (crates-io na bo nabo-pbc) #:use-module (crates-io))

(define-public crate-nabo-pbc-0.2.1 (c (n "nabo-pbc") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.7.0") (d #t) (k 0)) (d (n "partition") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "0sfilsfnndf9z1zs6qxndhqh82khmi2sgwqgxpj6pkp7finrigm6") (f (quote (("dummy_point" "rand") ("default" "dummy_point"))))))

