(define-module (crates-io na sm nasm-rs) #:use-module (crates-io))

(define-public crate-nasm-rs-0.0.2 (c (n "nasm-rs") (v "0.0.2") (h "17k3q3zviwjzl3nq6qw465iwi7kw9ds07amgryhbz7986b889p82") (y #t)))

(define-public crate-nasm-rs-0.0.3 (c (n "nasm-rs") (v "0.0.3") (h "0mypd6105lk818nxfn3db1qmr09bk310b6j6gl2v5swciyjlk10l")))

(define-public crate-nasm-rs-0.0.4 (c (n "nasm-rs") (v "0.0.4") (h "1mi6g307di7fz5py5rjmlizg5asmcfwgh4ggwhli86ppj2v7jwar")))

(define-public crate-nasm-rs-0.0.5 (c (n "nasm-rs") (v "0.0.5") (h "1imc3q0cswjlmvh1mzj1rm2mqxalvl53h07wbrfjvkzdq19s6i03")))

(define-public crate-nasm-rs-0.0.6 (c (n "nasm-rs") (v "0.0.6") (h "0rdqh64xivj0jfdw5hfxcpjzfjkk8kfp6phxvdvpy2yybkhm866s")))

(define-public crate-nasm-rs-0.0.7 (c (n "nasm-rs") (v "0.0.7") (h "0xmkz70s3b776fy1w99fqx6fdpzcak451ldwxfmkyy0k7p8kibw9")))

(define-public crate-nasm-rs-0.1.0 (c (n "nasm-rs") (v "0.1.0") (d (list (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0db8vvq3s5wksfld1831c6hkhh1bl93lynb6m5cbhqq2d3sf7ssk") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.1 (c (n "nasm-rs") (v "0.1.1") (d (list (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "1yxlakw88ip52h14zy6758m56n0dp9i6518c7c747ccahw6x4zd3") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.2 (c (n "nasm-rs") (v "0.1.2") (d (list (d (n "rayon") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1n4nz397nzf2fxd03yk2ywzpdly1da91lgx4irrf89j7wnwkda0m") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.3 (c (n "nasm-rs") (v "0.1.3") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1266k2bwhh3z2c27g06n4inywf6b4iwp87hvz7dl06rsykijq2q5") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.4 (c (n "nasm-rs") (v "0.1.4") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "044yc3z8j4vls2spf7msph0vxh4vlh0l9854k058kh9zjw0gficn") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.5 (c (n "nasm-rs") (v "0.1.5") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mvyvkr9m5ablbmr43rc9qh8jk7vp2gimq1igvjr05zlkn66saw8") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.6 (c (n "nasm-rs") (v "0.1.6") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vsvj3l6v1bb6d6p1mn9llpr3byqs6r2p6308jlsmj459i4s9k5j") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.7 (c (n "nasm-rs") (v "0.1.7") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r34hiy1pc0aksrfc02zsl0zyw33i9yi7kyx8l214l7nm0mzm97y") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1.8 (c (n "nasm-rs") (v "0.1.8") (d (list (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0d5mr6fjhs59gfxb5hyhah3sgp6jlii1q35g8dlxgkrj58bpfbnk") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.0 (c (n "nasm-rs") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lgc3gg32hj4pcbfp07vzwy013smdm27469fyy4rqgyil3x46vx7") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.1 (c (n "nasm-rs") (v "0.2.1") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1aghasvrs6vwrzjzfvi80ry9caj10hsjr3k0x03v937fs9mzigwx") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.2 (c (n "nasm-rs") (v "0.2.2") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "056nrnwjxhl5bxfjflb8szki0cdd1jaw6dpsjbwdmp2q7g980qx0") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.3 (c (n "nasm-rs") (v "0.2.3") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0whf6wwyc3zgh45rwxj9b0yccsd9hd9077bz3pzvhidhhi894cgh") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.4 (c (n "nasm-rs") (v "0.2.4") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "10zl67i9gr7qarmnnnd8538mydw0yr6jlpbsvb5kxap9mr15h2ff") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2.5 (c (n "nasm-rs") (v "0.2.5") (d (list (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0lfs2xfbpl1j7zq6qfg2wmi4djbl36qsygjb2spisjsz0v89hkgy") (f (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.3.0 (c (n "nasm-rs") (v "0.3.0") (d (list (d (n "jobserver") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nfrmnfvc1rcpghi14zbrdx3x5jr7gl2pv873pn440wyshdzmz0j") (f (quote (("parallel" "jobserver"))))))

