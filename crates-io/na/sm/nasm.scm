(define-module (crates-io na sm nasm) #:use-module (crates-io))

(define-public crate-nasm-0.0.0 (c (n "nasm") (v "0.0.0") (h "011wchl78h9zy0idhngf5d22h75qf8sprp7n3cqbdixa9042hk07")))

(define-public crate-nasm-0.0.1 (c (n "nasm") (v "0.0.1") (h "0dl5g0ss54nnx4xvyyvjk8bjxixjmpganiy44znvcfvsyi6hlk79")))

