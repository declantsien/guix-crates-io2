(define-module (crates-io na v_ nav_msgs) #:use-module (crates-io))

(define-public crate-nav_msgs-4.2.3 (c (n "nav_msgs") (v "4.2.3") (d (list (d (n "builtin_interfaces") (r "^1.2.1") (d #t) (k 0)) (d (n "geometry_msgs") (r "^4.2.3") (d #t) (k 0)) (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "std_msgs") (r "^4.2.3") (d #t) (k 0)))) (h "1c92manmz56jsgjv8rzmn1mpizljfkhzkyf3ic3kxhf1j91ahdps") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "geometry_msgs/serde" "std_msgs/serde" "builtin_interfaces/serde"))))))

