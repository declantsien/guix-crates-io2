(define-module (crates-io na ft nafta) #:use-module (crates-io))

(define-public crate-nafta-0.1.0 (c (n "nafta") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4") (f (quote ("r2d2" "sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "08nai48v6dr1jvkbhfiz317wj51lp6h8jym1p8nd99mnzzcpggdf")))

(define-public crate-nafta-0.1.1 (c (n "nafta") (v "0.1.1") (d (list (d (n "diesel") (r "^1.4") (f (quote ("r2d2" "sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "07mvh4dsqq4d4561rk6602iwy84f267sc64r59zbraaprymxkjbp")))

(define-public crate-nafta-0.1.2 (c (n "nafta") (v "0.1.2") (d (list (d (n "diesel") (r "^1.4") (f (quote ("r2d2" "sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0gmzr7wkz0cqcqskgbxwlcdjfdm2j41dxhkfwr6nlx9fa9ygz690")))

(define-public crate-nafta-0.1.3 (c (n "nafta") (v "0.1.3") (d (list (d (n "diesel") (r "^1.4") (f (quote ("r2d2" "sqlite"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "02qhdhx0xi2qvnzfgxz6pcjcgninpzlmay5jxjd2mbl8lbxi0cp1")))

