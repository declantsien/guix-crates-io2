(define-module (crates-io na ps naps) #:use-module (crates-io))

(define-public crate-naps-0.1.0 (c (n "naps") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "nats") (r "^0.17.0") (d #t) (k 0)))) (h "1hwxjrlfgi5ldbv0xl7zzx53khdxxm0x7n3jn3rcw14impnkaiwz")))

(define-public crate-naps-0.1.1 (c (n "naps") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "nats") (r "^0.17.0") (d #t) (k 0)))) (h "01j911scacaxwb8vjb6f6x8hf37p3fiqqhx1yvzns2d9i7gr5fwd")))

(define-public crate-naps-0.2.1 (c (n "naps") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.12") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "data-url") (r "^0.1.1") (d #t) (k 0)) (d (n "deno_ast") (r "^0.11.0") (f (quote ("transpiling"))) (d #t) (k 0)) (d (n "deno_core") (r "^0.118.0") (d #t) (k 0)) (d (n "deno_runtime") (r "^0.44.0") (d #t) (k 0)) (d (n "nats") (r "^0.17.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.8") (d #t) (k 0)) (d (n "rusty_v8") (r "^0.32.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_v8") (r "^0.29.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("fs"))) (d #t) (k 0)))) (h "05g90vhygynjm5jlbhj9zlyzcl9vh2bgc8i3lwghifcszq3g8lv0")))

