(define-module (crates-io na ke naked-function-macro) #:use-module (crates-io))

(define-public crate-naked-function-macro-0.1.0 (c (n "naked-function-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0sr6ph9affjxcav7hpwl6366vr3cmd0aapy7nz866cyixv5wz1gw") (r "1.59.0")))

(define-public crate-naked-function-macro-0.1.1 (c (n "naked-function-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "13g5is4nq0dcc2q98yzvxk1pj9k5kngchrs5wgv0i52d0a6i2b32") (r "1.59.0")))

(define-public crate-naked-function-macro-0.1.2 (c (n "naked-function-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1vl31v8gdxb643csygh928s2r9mlks4dhlspn8xin0hjdh9kfqgf") (r "1.59.0")))

(define-public crate-naked-function-macro-0.1.3 (c (n "naked-function-macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwzkhic3lc6i1hiwycrnkrwsj1rzyc83i9bxkc9gc3icz1l12b3") (r "1.59.0")))

(define-public crate-naked-function-macro-0.1.4 (c (n "naked-function-macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1dw67m5jm2vjjwp6y1m4nrvqmdkbi8d0pvvzb29fimm8a7sid3cx") (r "1.59.0")))

(define-public crate-naked-function-macro-0.1.5 (c (n "naked-function-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0x84g8a9hm75yz035jfgl8j5771v8np6dwfgf2rhpzpm1pkj6hav") (r "1.59.0")))

