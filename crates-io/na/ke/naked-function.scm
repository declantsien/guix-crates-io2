(define-module (crates-io na ke naked-function) #:use-module (crates-io))

(define-public crate-naked-function-0.1.0 (c (n "naked-function") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.0") (d #t) (k 0)))) (h "0a3bs0mbk8w13f7pglp9z8y2c3p1k37p78byx06dhbwlk7zbd2h8") (r "1.59.0")))

(define-public crate-naked-function-0.1.1 (c (n "naked-function") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.1") (d #t) (k 0)))) (h "17gbs4d0wag7hsyr8sn9336scyw1k24c8fhmwvl1cjghcsibb6r9") (r "1.59.0")))

(define-public crate-naked-function-0.1.2 (c (n "naked-function") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.2") (d #t) (k 0)))) (h "0vaa4kpg99l1zr6jvjsa46rj23f57p5m47fhmg83s4nnfp4d0pmm") (r "1.59.0")))

(define-public crate-naked-function-0.1.3 (c (n "naked-function") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.3") (d #t) (k 0)))) (h "0bklwig893k0nqzsqaaz606ykj9nh7890g5k7cxdbrjmi7m3z956") (r "1.59.0")))

(define-public crate-naked-function-0.1.4 (c (n "naked-function") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.4") (d #t) (k 0)))) (h "1q42m4k495lbj15fijrbakjq3ikmg66rg03w18kwfvfq89w6r2jv") (r "1.59.0")))

(define-public crate-naked-function-0.1.5 (c (n "naked-function") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "naked-function-macro") (r "=0.1.5") (d #t) (k 0)))) (h "1b0ryz4nqaagyfks7bhgmsfkdbjsmjwx7vqa05dj3rmidb55z39v") (r "1.59.0")))

