(define-module (crates-io na na nanachi) #:use-module (crates-io))

(define-public crate-nanachi-0.0.1 (c (n "nanachi") (v "0.0.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)))) (h "0jav9h31nrpwlpw076pq2j5qh25cmfdfp4l8vpg9i22qpxci2v73")))

(define-public crate-nanachi-0.0.2 (c (n "nanachi") (v "0.0.2") (d (list (d (n "image") (r "^0.23") (o #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (d #t) (k 2)) (d (n "lyon_geom") (r "^0.16") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)))) (h "05n602d8xk9j7rygc65dvvnyb5hwlmdbbd97qfvszr6la7iwf0cn") (f (quote (("path-data-notation") ("image-crate" "image") ("default" "image-crate" "path-data-notation"))))))

(define-public crate-nanachi-0.0.3 (c (n "nanachi") (v "0.0.3") (d (list (d (n "image") (r "^0.23") (o #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (d #t) (k 2)) (d (n "lyon_geom") (r "^0.16") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "02m36gvf6dp05lch5k85n1k3yraifxh4h61r7w1asqnackzf19qq") (f (quote (("path-data-notation") ("image-crate" "image") ("default" "image-crate" "path-data-notation"))))))

(define-public crate-nanachi-0.0.4 (c (n "nanachi") (v "0.0.4") (d (list (d (n "image") (r "^0.23") (o #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("png"))) (d #t) (k 2)) (d (n "lyon_geom") (r "^0.16") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)))) (h "1qln9xhhndzl7y7pw2kqdq3nxw8vlwisa3wxai9s6ny1d7yrsdr5") (f (quote (("path-data-notation") ("image-crate" "image") ("default" "image-crate" "path-data-notation"))))))

