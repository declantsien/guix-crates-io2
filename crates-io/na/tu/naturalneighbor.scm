(define-module (crates-io na tu naturalneighbor) #:use-module (crates-io))

(define-public crate-naturalneighbor-1.0.0 (c (n "naturalneighbor") (v "1.0.0") (d (list (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)))) (h "0pzwdj56dm7ddirfg33m9b23gchp1cri25lypdabj52h35f0adgw")))

(define-public crate-naturalneighbor-1.0.1 (c (n "naturalneighbor") (v "1.0.1") (d (list (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0syrjwy3rhyxizpz01kw58pnyxm4b2c1hn62n3p3faajrv10jggy")))

(define-public crate-naturalneighbor-1.1.0 (c (n "naturalneighbor") (v "1.1.0") (d (list (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)))) (h "184dad3d3wi5qcbb7dv0dhqc21wzh38d4lbkz7xnllzmm9zwwl5g")))

(define-public crate-naturalneighbor-1.1.2 (c (n "naturalneighbor") (v "1.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)))) (h "0k46pck4nm9n328v9xvh1g0xddz2dsz8mzzb029g7c7lswm04jg1")))

(define-public crate-naturalneighbor-1.1.3 (c (n "naturalneighbor") (v "1.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)))) (h "1r95gkxn9id4cz95yjjfwq05b32qgq7z1snnqr1dm7qrdv73hj0v")))

(define-public crate-naturalneighbor-1.2.0 (c (n "naturalneighbor") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h6rxysj468fmsdcfdnwazwid1hxhdfm0cdgkn9sy830mml7qpn3")))

(define-public crate-naturalneighbor-1.2.1 (c (n "naturalneighbor") (v "1.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12ygl8h0bks2zd68qvgghgc366psh08n4w6bsqnkkfahiad2gizq")))

(define-public crate-naturalneighbor-1.2.2 (c (n "naturalneighbor") (v "1.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "delaunator") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rstar") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1asdjnzw7df8y3qlbvmw8z4cvjx85m42d2fpwnwynwldk0q8ngxf")))

