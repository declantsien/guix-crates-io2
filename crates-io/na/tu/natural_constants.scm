(define-module (crates-io na tu natural_constants) #:use-module (crates-io))

(define-public crate-natural_constants-0.1.0 (c (n "natural_constants") (v "0.1.0") (h "0ykmwl7jjwwhrq2bfmw8vnskqq95rd21c36zv3mha8nzz83nwadz")))

(define-public crate-natural_constants-0.2.0 (c (n "natural_constants") (v "0.2.0") (h "0vczzbip6qjnbp1h10ia2i0i46xyg44pr8cfk0qh9154hpldix2g")))

