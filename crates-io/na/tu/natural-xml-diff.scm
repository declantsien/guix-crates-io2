(define-module (crates-io na tu natural-xml-diff) #:use-module (crates-io))

(define-public crate-natural-xml-diff-0.1.0 (c (n "natural-xml-diff") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.4") (d #t) (k 0)) (d (n "insta") (r "^1.21.2") (f (quote ("yaml" "glob"))) (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "xot") (r "^0.5.1") (d #t) (k 0)))) (h "07akjdxxnk10kp4cpzkgd255cvrdnx0pbv7931q2fhrljaq2i5wj") (y #t)))

(define-public crate-natural-xml-diff-0.2.0 (c (n "natural-xml-diff") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.2") (d #t) (k 0)) (d (n "dissimilar") (r "^1.0.4") (d #t) (k 0)) (d (n "insta") (r "^1.21.2") (f (quote ("yaml" "glob"))) (d #t) (k 2)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 0)) (d (n "xot") (r "^0.8.0") (d #t) (k 0)))) (h "0nfqkflqjs34rq63z440y97zx1y1hblb9720da8ajyh7fn218rxw") (y #t)))

