(define-module (crates-io na tu nature_demo_executor) #:use-module (crates-io))

(define-public crate-nature_demo_executor-0.2.0 (c (n "nature_demo_executor") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.2.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12rmww6mlydvmj8hjq4dqh8q99vx7wbg60ab19r397f2589ixr3n")))

(define-public crate-nature_demo_executor-0.3.0 (c (n "nature_demo_executor") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.3.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17wsagchhzd437di82jspr9c04y791qib54kh292yn9jg20kn6wj")))

(define-public crate-nature_demo_executor-0.4.0 (c (n "nature_demo_executor") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.4.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ivjvmhbkj7ika1qg70947v13rgrny8z4zq2sc89j39dh5d6lvk4")))

(define-public crate-nature_demo_executor-0.5.0 (c (n "nature_demo_executor") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.5.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hp9dgwzc2qnwl37pdjcxadxih49d1fc65khqpsmijhhmqbdp7k9")))

(define-public crate-nature_demo_executor-0.6.0 (c (n "nature_demo_executor") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.6.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cb1b5fw0kz511qadfvnvm2z155xk07d38a4qrhzii0p3gl8s3l9")))

(define-public crate-nature_demo_executor-0.7.0 (c (n "nature_demo_executor") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.7.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rsxz1v2abx0rbpnq6nzqj68s2gypj4hd2h0z8qzkvirgvfakz90")))

(define-public crate-nature_demo_executor-0.8.0 (c (n "nature_demo_executor") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.8.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hr15rppigksc2m6wmpqsdkd2a6kimqalcd64n1pdr8zd4572s0a")))

(define-public crate-nature_demo_executor-0.9.0 (c (n "nature_demo_executor") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.9.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xq4g90x2ibwhk4a9k5k1minb1lzm14m96zhf01vdp22hl5xkc94")))

(define-public crate-nature_demo_executor-0.10.0 (c (n "nature_demo_executor") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.10.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aa1hqxhn4g13gjlgdv4xjv9aqrbwly8xdibh21ivccaam3simml")))

(define-public crate-nature_demo_executor-0.11.0 (c (n "nature_demo_executor") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.11.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0584cdijd72d4140k3kkayha44aib1k8vy76flqw6fi4yhl7rkq8")))

(define-public crate-nature_demo_executor-0.12.0 (c (n "nature_demo_executor") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.12.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z9l98yg2q2pjf6grswwv5iaa3rw8bf968b4vn0wi0gjkg6ywk9b")))

(define-public crate-nature_demo_executor-0.13.0 (c (n "nature_demo_executor") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.13.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ii62kafvj7l8nziw8yxrrnhwc132fnzf874nba3661bvh19b889")))

(define-public crate-nature_demo_executor-0.14.0 (c (n "nature_demo_executor") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.14.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m36yyz1i899v0rq6368b2jzgbs60zf69mimxr07rjswy66l9ass")))

(define-public crate-nature_demo_executor-0.14.1 (c (n "nature_demo_executor") (v "0.14.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.14.1") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19mry84l2wb9fw196k2cdd79gc88rhainggg3bklmlv9q65k2h8i")))

(define-public crate-nature_demo_executor-0.15.0 (c (n "nature_demo_executor") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.15.0") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vy835bbirssk5lpdmcq2hj68kf0bnx7ds26sciipajv833rqgxf")))

