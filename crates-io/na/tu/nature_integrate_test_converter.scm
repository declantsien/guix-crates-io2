(define-module (crates-io na tu nature_integrate_test_converter) #:use-module (crates-io))

(define-public crate-nature_integrate_test_converter-0.1.0 (c (n "nature_integrate_test_converter") (v "0.1.0") (d (list (d (n "nature_common") (r "^0.0.1") (d #t) (k 0)))) (h "06fz6bzk08mw1zp8llw1ndy8h17h180v34hhl41ziwwl54qxsp29") (y #t)))

(define-public crate-nature_integrate_test_converter-0.0.2 (c (n "nature_integrate_test_converter") (v "0.0.2") (d (list (d (n "nature_common") (r "^0.0.2") (d #t) (k 0)))) (h "18rzyjggy4z6x2nvz6avqvllyyq4ak9claipkyz7f8cjrzpxks5c")))

