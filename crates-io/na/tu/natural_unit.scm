(define-module (crates-io na tu natural_unit) #:use-module (crates-io))

(define-public crate-natural_unit-0.1.0 (c (n "natural_unit") (v "0.1.0") (h "14lr25ws79b7f7q7k3j65sg534ifhrqzahdlxr5x2f5lvn7z6syw")))

(define-public crate-natural_unit-0.1.1 (c (n "natural_unit") (v "0.1.1") (h "1qwjmww9gvqzlcdizknp3p5ry4ylawww3ci9qryxmiva62c2zi4n")))

(define-public crate-natural_unit-0.1.2 (c (n "natural_unit") (v "0.1.2") (h "0w4g1m3mvvfpj623iilb69pf6gj8srfr90l7q573k8ql24y9b9x5")))

(define-public crate-natural_unit-0.1.3 (c (n "natural_unit") (v "0.1.3") (h "0a8866jl6fmvgwsgffzm8h7lql9j0b7rhkfzh5pfd75caz4nknap")))

(define-public crate-natural_unit-0.1.4 (c (n "natural_unit") (v "0.1.4") (h "1ywv8m19325vpgvillghfkl4y7nm022hg982qa9yzhcpd8zwkdps")))

