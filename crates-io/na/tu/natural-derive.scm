(define-module (crates-io na tu natural-derive) #:use-module (crates-io))

(define-public crate-natural-derive-0.1.0 (c (n "natural-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zmn7fn2c760av235rcgap221yp87nw32lj09dpj8l8xf2gwcbhl")))

(define-public crate-natural-derive-0.3.0 (c (n "natural-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0alxjxf6dm5g05pp1v98wx4xl67cdsax46s55pnzpy7x7nw2qfsr")))

(define-public crate-natural-derive-0.3.1 (c (n "natural-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xx2kgssipn1fbwxwrwz7rr2cpyzv1p3l4mmpzzfrjk57b5kdipl")))

(define-public crate-natural-derive-0.4.0 (c (n "natural-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14wkzl51zm08mpb3c95pl06h40jss8lphljz034mixanxkbsmd9d")))

