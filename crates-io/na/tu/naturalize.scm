(define-module (crates-io na tu naturalize) #:use-module (crates-io))

(define-public crate-naturalize-0.1.0 (c (n "naturalize") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0mvs86a9ykk1qxdmkjhrcyq8as761xgaklgfijz3yjppd9dfk8vd")))

(define-public crate-naturalize-0.1.1 (c (n "naturalize") (v "0.1.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1kbbdahf0vhgn6g4zgj8s6s9yywj1xp6qxajllsn3jp5md2w1x27")))

(define-public crate-naturalize-0.1.2 (c (n "naturalize") (v "0.1.2") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1j4x3ycsagklfpi2jb8c55j3lh0bq0xs19l038bf5kbc103sjx9y")))

