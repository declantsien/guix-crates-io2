(define-module (crates-io na tu nature_demo_common) #:use-module (crates-io))

(define-public crate-nature_demo_common-0.0.2 (c (n "nature_demo_common") (v "0.0.2") (d (list (d (n "nature_common") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0sa58v51cz2d5wspwrry23x1qa8rbpzg66nlp1i6pgmiwqknbhs5")))

(define-public crate-nature_demo_common-0.2.0 (c (n "nature_demo_common") (v "0.2.0") (d (list (d (n "nature_common") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1zqj10mkz06hng7ansmfi4hxvf5ckmyna4s3c8zi7xkyhb8jyd1s")))

(define-public crate-nature_demo_common-0.3.0 (c (n "nature_demo_common") (v "0.3.0") (d (list (d (n "nature_common") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11xb2cn5q3wxaw2vifv2376qh4h2cm7mhb5113cchh6pnyl6gwkv")))

(define-public crate-nature_demo_common-0.4.0 (c (n "nature_demo_common") (v "0.4.0") (d (list (d (n "nature_common") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1fx6wkbq3j52bsb72mm941r8g82l8i9h4dqx9v9zpa8q0m6bqbim")))

(define-public crate-nature_demo_common-0.5.0 (c (n "nature_demo_common") (v "0.5.0") (d (list (d (n "nature_common") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00bc428vwvz5kpmaqj6x38ad3q8047gcy7z83lcs9nc4kh23gigm")))

(define-public crate-nature_demo_common-0.6.0 (c (n "nature_demo_common") (v "0.6.0") (d (list (d (n "nature_common") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0frl3skrgd79zzqs6a8fw6k4nj7m2q37yg11k5vcf968ghrr8s99")))

(define-public crate-nature_demo_common-0.7.0 (c (n "nature_demo_common") (v "0.7.0") (d (list (d (n "nature_common") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "09m7rxy5d3ry3ndbqxrnfl81iwcpigypy5prjx2sxnlrrsryh131")))

(define-public crate-nature_demo_common-0.8.0 (c (n "nature_demo_common") (v "0.8.0") (d (list (d (n "nature_common") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0isjs8ahzx4b708zqmbhhk2ma2dn71l7742dl6nkbfgsqrjnrnfv")))

(define-public crate-nature_demo_common-0.9.0 (c (n "nature_demo_common") (v "0.9.0") (d (list (d (n "nature_common") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g63wcvkdf4x0gghx94rz2rknqnfalvxacrzzf22pzhsccnhc73q")))

(define-public crate-nature_demo_common-0.10.0 (c (n "nature_demo_common") (v "0.10.0") (d (list (d (n "nature_common") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0gcjps0c1qrvi5yxz5c36ss51q4dk2wii2nl9scz5zaa3z7v3ds1")))

(define-public crate-nature_demo_common-0.11.0 (c (n "nature_demo_common") (v "0.11.0") (d (list (d (n "nature_common") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "10d5ygc5sgrphvxrbml7kycnwc67vih6i19j30ffwc1fcz74gl61")))

(define-public crate-nature_demo_common-0.12.0 (c (n "nature_demo_common") (v "0.12.0") (d (list (d (n "nature_common") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1k9m8c4p1pa6s577wgkyrb3xvwd8nj9z8b7dy4k809pfa553acch")))

(define-public crate-nature_demo_common-0.13.0 (c (n "nature_demo_common") (v "0.13.0") (d (list (d (n "nature_common") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sd6s0sxyyyrk3xnk60sc485qzfiyilzcagkwx3xln9dy2accg1h")))

(define-public crate-nature_demo_common-0.14.0 (c (n "nature_demo_common") (v "0.14.0") (d (list (d (n "nature_common") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0fvm06h7lkg1b2f0kcdv8mqkhvgagx7k3cqkjkpq7by7ygkmwsbn")))

(define-public crate-nature_demo_common-0.14.1 (c (n "nature_demo_common") (v "0.14.1") (d (list (d (n "nature_common") (r "^0.14.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "175f5silrf95r25d30jf9zdqxswiag3bfvdabs6pzzwr0wypkc65")))

(define-public crate-nature_demo_common-0.15.0 (c (n "nature_demo_common") (v "0.15.0") (d (list (d (n "nature_common") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08qwmrkq1d8liw8gwsc20fw3kyw1sjgxvfkz3c5j0vhncwqm5w19")))

