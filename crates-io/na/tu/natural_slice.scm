(define-module (crates-io na tu natural_slice) #:use-module (crates-io))

(define-public crate-natural_slice-0.1.0 (c (n "natural_slice") (v "0.1.0") (d (list (d (n "factorial") (r "^0.2.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xz3lgvx4gcx6nwx48h08a9mlskf3c9wbk36kw69z1xyvwrknmxx")))

(define-public crate-natural_slice-0.1.1 (c (n "natural_slice") (v "0.1.1") (d (list (d (n "factorial") (r "^0.2.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1h4jkng30kngz2s8p0vlvgwzdk2b0hc59k406xl2ph7ic98lvbs7")))

(define-public crate-natural_slice-0.1.2 (c (n "natural_slice") (v "0.1.2") (d (list (d (n "factorial") (r "^0.2.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nbjinxr3yap1mzxlx3vv879lwjbybx2rv3cs9lz9f6dpl89299v")))

