(define-module (crates-io na tu natural) #:use-module (crates-io))

(define-public crate-natural-0.2.0 (c (n "natural") (v "0.2.0") (h "1xdgf6wdz5r46i8m7kzv0sn5kzv48p9a9kbc8v9c8sp6ph51ic6h")))

(define-public crate-natural-0.2.1 (c (n "natural") (v "0.2.1") (h "1j18baafhab2x7c162adqarigmcp5gdmc92568lgycjhasrk2iwl")))

(define-public crate-natural-0.2.2 (c (n "natural") (v "0.2.2") (h "0nk4ydfi24kmy32fgnzkvan5kpw1x35c9bdvfr890p7lx74b0arv")))

(define-public crate-natural-0.3.0 (c (n "natural") (v "0.3.0") (h "19hh23z4w3r77fsqmj5sw3hgqkdja9cnsj3s1qndlm25ddyrsrgx")))

(define-public crate-natural-0.4.0 (c (n "natural") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07cibvs99r7dm23y2b1m1nqnpmp3h8cswwqjjwrrdapfadvy61fq") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-natural-0.5.0 (c (n "natural") (v "0.5.0") (d (list (d (n "rust-stemmers") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rqqgh4ik6hzmxs0fhd10g5jdba16h6i9q2m6pg9qrfwim7v9rk5") (f (quote (("serde_support" "serde") ("default"))))))

