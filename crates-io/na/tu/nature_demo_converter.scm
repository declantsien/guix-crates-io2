(define-module (crates-io na tu nature_demo_converter) #:use-module (crates-io))

(define-public crate-nature_demo_converter-0.0.2 (c (n "nature_demo_converter") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nature_common") (r "^0.0.2") (d #t) (k 0)) (d (n "nature_demo_common") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y297rzxynsn5afh3kdq9pdc3r8dlvxqsma69hk533x764im5g1n")))

