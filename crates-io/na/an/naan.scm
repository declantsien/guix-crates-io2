(define-module (crates-io na an naan) #:use-module (crates-io))

(define-public crate-naan-0.1.0 (c (n "naan") (v "0.1.0") (h "196d0z8iygnx7bawvmz7dn6glwsyn3gx9hb76fqiw4jmg6w6i952")))

(define-public crate-naan-0.1.1 (c (n "naan") (v "0.1.1") (h "0pc6li9ql000lp5lwi2d8pcl17dkc9akmhyzc1cnpv0a47v981mw") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.2 (c (n "naan") (v "0.1.2") (h "1nf1bxjm494k3cfc7y6xz5n1vd47czvi37ag210g89v24afscgyd") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.3 (c (n "naan") (v "0.1.3") (h "0b4sxbqampi309vaavz0z13z9xj2pandiiblf6655bv9a0qqfkfa") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.4 (c (n "naan") (v "0.1.4") (h "0wck3ikk96inr2p870gkd3x9hhzhqibbvyp34wlw095c7y9ijm9a") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.5 (c (n "naan") (v "0.1.5") (h "1lh6jwmncg9i8gjph9ry5kkdynwy224v93s2kfcf0syj4msgg3dk") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.6 (c (n "naan") (v "0.1.6") (h "087spn7y0qwxp83kq4f91618lhsbf99xd83kryjz8xvdd6767lzj") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.7 (c (n "naan") (v "0.1.7") (h "1dyk1kv30pnnv4vimd89qvwdbsb9kw0g2sll3r6qd6yklfkynvln") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.8 (c (n "naan") (v "0.1.8") (h "0zigwp731z7lpq4di9z3w8vsmxjzn12751g8i6skrpblb1bykbj3") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.9 (c (n "naan") (v "0.1.9") (h "1vcicyh0cnrkcwjg343yjv35w05kqizywrpvb1kbmr214scvc0m4") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.10 (c (n "naan") (v "0.1.10") (h "03vh89dwn1fkcngg8yyhc8qsv5k795v2gpz2y45pp62pdjf2d3g8") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.11 (c (n "naan") (v "0.1.11") (h "1klpc77faa0lr18jrgmjkazsii82z9r6qw5zl8dy79c5y3xxkwb1") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.12 (c (n "naan") (v "0.1.12") (h "0911lssgai7kbqgp9mqbl0p482ni808g3w9qdxp1y344dh9bah8i") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.13 (c (n "naan") (v "0.1.13") (h "0lxg9il4813546y0zdr2nmxzx7gdvr0vdfngg8bk56k6kgg1js5s") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.14 (c (n "naan") (v "0.1.14") (h "16frwmnsbmwrqpkp7hwxl9bxga7xrgls4hi0v0d1si21fhghxr65") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.15 (c (n "naan") (v "0.1.15") (h "07qz68chx9mmlscwm1sgwzvj02d2l72yd5adihlcc5y1pbvb7lnh") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.16 (c (n "naan") (v "0.1.16") (h "01ddqyx4ayccmb45k1fcvy4161h9lkychrpdm4ign68rvgyhlczy") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.17 (c (n "naan") (v "0.1.17") (h "14fg36h1rwjldlb42qn4sggjkhvc0jw50jjn2c8v7nrsvk4h48gr") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.18 (c (n "naan") (v "0.1.18") (h "1b58mg4qg56fjdf40xgw1dfzyqld11ia7c2idxmbr8j0x5h8xpih") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.19 (c (n "naan") (v "0.1.19") (h "1y7mw0y9cyav2kzmskyz8hnyk2ccfajcplhsdqxd04a2d2sj525l") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.20 (c (n "naan") (v "0.1.20") (h "1s425zj5kg5rs0y20p6dvzf9kgvyl5zgmgmc0r6jj25pmir64fy2") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.21 (c (n "naan") (v "0.1.21") (h "1cazk69q39igna5c9rzbz3k2hysh2s0g7knwns2kn925acizkj7i") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.22 (c (n "naan") (v "0.1.22") (h "1ca061nz1z07wfhvp3x2nb0pwx5paklbdi1j9ll6vvklxvb2fdib") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1.23 (c (n "naan") (v "0.1.23") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "1h806wr64b6m9vjabmld99cv0vgwf6b09qpxkq8gjz4r73n9g3z4") (f (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.24 (c (n "naan") (v "0.1.24") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "193wgaaq2fqfj6cvc7bb5gda3scz14ink6rld4qh0m9x6iyzqwx3") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.25 (c (n "naan") (v "0.1.25") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "11w14y3fx32r5k17mwzyjn3bhzklahspjj4g3k48pai9jfawxlf1") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.26 (c (n "naan") (v "0.1.26") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "1jr0csrl9yzajyirzx7rf9pmfkxx161bs3c6b4m5zrcvm5rs03l0") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.27 (c (n "naan") (v "0.1.27") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "05qq1vp23gwdqfpabpi91ii2msshwn6jqdm8vbnbddil43wz7dh4") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.28 (c (n "naan") (v "0.1.28") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "1iqk4f963jigv8jdcjj94qha2s10c8wcklvvwqp955q5gcc71lxn") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.29 (c (n "naan") (v "0.1.29") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "0zl06qbn5c4036zrcjgj170gy1dwm9lh4im2z6x5k2z4x3zi6ghx") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.30 (c (n "naan") (v "0.1.30") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "0122acgxkk16cm0pnvkgqrh6qiczkmd7xlf06yglh8ylvp3gcvhg") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.31 (c (n "naan") (v "0.1.31") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "07gqsihafdxc0g03ihaw9vllnp8g83n98gdvmw917fczrwf1qqf7") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1.32 (c (n "naan") (v "0.1.32") (d (list (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57"))) (o #t) (d #t) (k 0)))) (h "04n6xg0n2w712k0n301zv3k1sdavkmhmv70nmyprhi5z42cv2m31") (f (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (s 2) (e (quote (("tinyvec" "dep:tinyvec"))))))

