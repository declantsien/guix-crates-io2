(define-module (crates-io na me namedlock) #:use-module (crates-io))

(define-public crate-namedlock-0.1.0 (c (n "namedlock") (v "0.1.0") (h "05wd1f9n664bzys0s9wa9zyg2i3g4br9wh5pv325gyc7vjnhhjdr")))

(define-public crate-namedlock-0.2.0 (c (n "namedlock") (v "0.2.0") (h "1br8i14832qm5yz0b7irplh70m8pdxkaj4bfd8pfkamar44mkvwd")))

(define-public crate-namedlock-0.2.1 (c (n "namedlock") (v "0.2.1") (h "1ykaq64nj1b1d88vfl679bfxlz5hy7wzfsy7bdq2qghqxlzjw545") (y #t)))

(define-public crate-namedlock-0.2.2 (c (n "namedlock") (v "0.2.2") (h "1azrbmnz0pz90r6mhw80vms3kk3bsmbb99q7zzj86ambs0n0h0sq")))

(define-public crate-namedlock-0.3.0 (c (n "namedlock") (v "0.3.0") (h "1l963k011s4m85hs4z889z0pvgs470mv7nd02drapikiqfbgy5wk")))

(define-public crate-namedlock-0.4.0 (c (n "namedlock") (v "0.4.0") (h "1fvlfgnbjwi0fsikylvyivkwf8ydz3drn09gryxwy7y5aqy48r1i")))

(define-public crate-namedlock-0.5.0 (c (n "namedlock") (v "0.5.0") (d (list (d (n "core_collections") (r "^0.3") (f (quote ("rand"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1rzvq5ap3kpnd99hr9v4swx8svp9bllmzg8y0hvdgv7m2s4m1zq6") (f (quote (("std") ("default" "std"))))))

(define-public crate-namedlock-0.5.1 (c (n "namedlock") (v "0.5.1") (d (list (d (n "core_collections") (r "^0.3") (f (quote ("rand"))) (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1q0mbhlcdcfrwx4nvw4r73aa76xscrzkav4swa32p45srrbjcbgg") (f (quote (("std") ("default" "std"))))))

(define-public crate-namedlock-0.6.0 (c (n "namedlock") (v "0.6.0") (d (list (d (n "core_collections") (r "^0.3") (f (quote ("rand"))) (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0w4lr7a8j9j2wqx1hibml60rrdka8xwzgr3fyqqzgq8sdrswm73q") (f (quote (("std") ("default" "std"))))))

(define-public crate-namedlock-0.7.0 (c (n "namedlock") (v "0.7.0") (d (list (d (n "core_collections") (r "^0.3") (f (quote ("rand"))) (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0awsw61dz5vjpqsq6zy4nkrxmswwwzzhfy3k0m8y67fy6vnhn200") (f (quote (("std") ("default" "std"))))))

