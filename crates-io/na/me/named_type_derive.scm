(define-module (crates-io na me named_type_derive) #:use-module (crates-io))

(define-public crate-named_type_derive-0.1.0 (c (n "named_type_derive") (v "0.1.0") (d (list (d (n "named_type") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1pkh17v41bbh2pmkzji1qbxb6b09xk7cgymaz6rmr6ylb9xy5f3n")))

(define-public crate-named_type_derive-0.1.1 (c (n "named_type_derive") (v "0.1.1") (d (list (d (n "named_type") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1za5zw48dx7ybqq24hxlbq9lil6w61sr9407hc72w7faznmrb508")))

(define-public crate-named_type_derive-0.1.2 (c (n "named_type_derive") (v "0.1.2") (d (list (d (n "named_type") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "1zqlaq9vhjfwiwjfw1fvvi5rk8graypkz495ipxni9f36fcjvdfz")))

(define-public crate-named_type_derive-0.1.3 (c (n "named_type_derive") (v "0.1.3") (d (list (d (n "named_type") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0l8svqi8pdczybr8swnwpnl7plc4dlr1bwdb01547b9zwpb6xdm3")))

(define-public crate-named_type_derive-0.1.4 (c (n "named_type_derive") (v "0.1.4") (d (list (d (n "named_type") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0lqwc0dj9yvk6y9zjs5l9yslcyj2jln16ci05cqc9bivyk5l2lk9")))

(define-public crate-named_type_derive-0.1.5 (c (n "named_type_derive") (v "0.1.5") (d (list (d (n "named_type") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0azxg0j8xfzhkxc0psmafhk9k0i4mrmm0xl7s3s4y7y2b6m8zflj")))

(define-public crate-named_type_derive-0.1.6 (c (n "named_type_derive") (v "0.1.6") (d (list (d (n "named_type") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0jp1hdx2qxng9crymndd4nywgnsnc42dr1l9fikfv3bjim07w7b5")))

(define-public crate-named_type_derive-0.2.0 (c (n "named_type_derive") (v "0.2.0") (d (list (d (n "named_type") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0ai1zq9qf2pcnyylxp4bq37dnmz9pzn2w3xjlrx13gq7silvpx0a")))

(define-public crate-named_type_derive-0.2.1 (c (n "named_type_derive") (v "0.2.1") (d (list (d (n "named_type") (r "^0.1.4") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.9") (d #t) (k 0)))) (h "0fr4s5x5qws8jjhfl0gzxjhwb3x20qzfd4z9y822ll7v96p92g61")))

(define-public crate-named_type_derive-0.2.2 (c (n "named_type_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1hn01y8ip4jsk1bv4csbk8056bcnfxmajc3isbdb5p4j706f4f35")))

