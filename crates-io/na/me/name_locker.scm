(define-module (crates-io na me name_locker) #:use-module (crates-io))

(define-public crate-name_locker-0.1.0 (c (n "name_locker") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0544081is9pmqxvy390fngapary1f5zjdwnzvi50x332lg0479qx") (s 2) (e (quote (("inmem" "dep:dashmap" "dep:tokio" "dep:async-stream"))))))

(define-public crate-name_locker-0.1.1 (c (n "name_locker") (v "0.1.1") (d (list (d (n "async-stream") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "dashmap") (r "^5.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync"))) (o #t) (d #t) (k 0)))) (h "0z866fk5b9w0ysrsslq6fsq6brahimyfjnxkhj08awx5mc45knyd") (s 2) (e (quote (("inmem" "dep:dashmap" "dep:tokio" "dep:async-stream"))))))

