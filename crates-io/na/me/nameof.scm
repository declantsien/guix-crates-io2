(define-module (crates-io na me nameof) #:use-module (crates-io))

(define-public crate-nameof-0.1.0 (c (n "nameof") (v "0.1.0") (h "0yc2xd3786q4bqc7avbq76q5gr4b28m2kdm1rizdzyr8vziss8cd")))

(define-public crate-nameof-0.1.1 (c (n "nameof") (v "0.1.1") (h "0ni2gc3krdcd7fvbq9r9s7cajnj5pmkhkbpwdffwx5y13qhcavzr")))

(define-public crate-nameof-1.0.0 (c (n "nameof") (v "1.0.0") (h "1xqddgjblnw0ka2swcf80x3481h8frfigj627sa9dxwphn2dm2rz")))

(define-public crate-nameof-1.0.1 (c (n "nameof") (v "1.0.1") (h "0s1frb9ql62bp7s1mrfacycpv94q3ngdaxnraa8rf05fqz4gp41f")))

(define-public crate-nameof-1.0.2 (c (n "nameof") (v "1.0.2") (h "0s8lyjf4h7s8b96wydrylfzgbr32hwzfpxg0ddrf8dhrplh54l6y") (y #t)))

(define-public crate-nameof-1.0.3 (c (n "nameof") (v "1.0.3") (h "1girliarqi1lqz2scmzmlxi7xajycxhmkw4893pdvq49yhvaxasj")))

(define-public crate-nameof-1.1.0 (c (n "nameof") (v "1.1.0") (h "1zbyb03v0qwngkj7y6vbxnm9vp0kh8yxzjfb6ddawnl3dgv0s1vd")))

(define-public crate-nameof-1.2.0 (c (n "nameof") (v "1.2.0") (h "1bfqkcyb1dsi7khav1il5n1955mvbmd0979304nn6dbm4nb6vcbv")))

(define-public crate-nameof-1.2.1 (c (n "nameof") (v "1.2.1") (h "1lbidpky082jhlipggx1a3258bi0v4mm737ip8z471pysys6z2ds")))

(define-public crate-nameof-1.2.2 (c (n "nameof") (v "1.2.2") (h "0i4wblxiy4b99qkzwhj3j4wdxd6bydh1x9ikic6ypavcm24v7s4w")))

