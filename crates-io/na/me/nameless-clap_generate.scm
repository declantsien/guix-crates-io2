(define-module (crates-io na me nameless-clap_generate) #:use-module (crates-io))

(define-public crate-nameless-clap_generate-3.0.0-beta.2 (c (n "nameless-clap_generate") (v "3.0.0-beta.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0) (p "nameless-clap")) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "08sc0pr61k8baihphqfby5wabhxjpm6k6pw5wxcmj37pk2gic9wz") (f (quote (("unstable" "clap/unstable") ("doc") ("default") ("debug" "clap/debug"))))))

(define-public crate-nameless-clap_generate-3.0.0-beta.2.1 (c (n "nameless-clap_generate") (v "3.0.0-beta.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2.1") (d #t) (k 0) (p "nameless-clap")) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11z57plnm2mxbpa44lka7k4vcv93d5dx6q1q0pcb2nlhq7ql7wpm") (f (quote (("doc") ("default") ("debug" "clap/debug"))))))

(define-public crate-nameless-clap_generate-3.0.0-beta.2.2 (c (n "nameless-clap_generate") (v "3.0.0-beta.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.2.2") (d #t) (k 0) (p "nameless-clap")) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1jkyms7qcrfwq765j9qn0alj6pisyhyh2wbrxg8b0klc5mblnnap") (f (quote (("doc") ("default") ("debug" "clap/debug"))))))

