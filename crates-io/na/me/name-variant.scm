(define-module (crates-io na me name-variant) #:use-module (crates-io))

(define-public crate-name-variant-0.1.0 (c (n "name-variant") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1xj7sl800hfrz3w6v2wir513py2d0vai8ipki42gqwi3vksm7h66")))

