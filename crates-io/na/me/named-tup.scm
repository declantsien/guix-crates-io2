(define-module (crates-io na me named-tup) #:use-module (crates-io))

(define-public crate-named-tup-0.1.0 (c (n "named-tup") (v "0.1.0") (d (list (d (n "named-tup-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 1)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 1)))) (h "01g2d8ymma50ss2s57w084473hag6glb8i24rkfs99c1kqp0g12r") (f (quote (("dev-test") ("default"))))))

(define-public crate-named-tup-0.1.1 (c (n "named-tup") (v "0.1.1") (d (list (d (n "named-tup-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 1)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 1)))) (h "1x30kfr843j7xl4g3wwrgfs6ddflxlpy8fch5b3ympqyyprchysb") (f (quote (("dev-test") ("default"))))))

(define-public crate-named-tup-0.2.0 (c (n "named-tup") (v "0.2.0") (d (list (d (n "named-tup-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "named-tup-derive") (r "^0.2.0") (f (quote ("add_dev_idents"))) (d #t) (k 2)))) (h "0ylclwhm2j9v66p7jz55zfq2kb013z8ql80hqmwbqa6n91vfc794")))

(define-public crate-named-tup-0.3.0 (c (n "named-tup") (v "0.3.0") (d (list (d (n "named-tup-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "named-tup-derive") (r "^0.3.0") (f (quote ("add_dev_idents"))) (d #t) (k 2)))) (h "0snmzpxw82khv0sx7xki07h4b0dy95bc0rm055by7476di03k4yy")))

(define-public crate-named-tup-0.3.1 (c (n "named-tup") (v "0.3.1") (d (list (d (n "named-tup-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "named-tup-derive") (r "^0.3.1") (f (quote ("add_dev_idents"))) (d #t) (k 2)))) (h "14ccl2z9qvd8pdkajx02gfk67ypsypqqsygdza5rmfychnvs7ip2")))

