(define-module (crates-io na me name_maker) #:use-module (crates-io))

(define-public crate-name_maker-0.1.0 (c (n "name_maker") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wwrrmdazwcaz1r97ib707hsy8c4n9b0l9ffb67rddadi9g6pgbq")))

(define-public crate-name_maker-0.1.1 (c (n "name_maker") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0qgrzkxvl5vi4n4fhiyqz3wkyr7px0iyw5430a7np31z8fmrw4p1")))

