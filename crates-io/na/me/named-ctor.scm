(define-module (crates-io na me named-ctor) #:use-module (crates-io))

(define-public crate-named-ctor-0.1.0 (c (n "named-ctor") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2d8p0v725s9khy8lasdlx2fm5b20p09xy18ffjyw4744azp1by") (y #t)))

(define-public crate-named-ctor-0.1.1 (c (n "named-ctor") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8mw6liw9n4f8yzps1gyx0gnkan06yx4mak9irj7gzikc715flw")))

