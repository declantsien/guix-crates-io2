(define-module (crates-io na me named-sem) #:use-module (crates-io))

(define-public crate-named-sem-0.1.0 (c (n "named-sem") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ab5mbdk7jh1g5ybrx4sv640pg95yd5wjqdb5gxs98y9w5farpm4") (f (quote (("commandline" "clap" "anyhow"))))))

(define-public crate-named-sem-0.2.0 (c (n "named-sem") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1b5n316jafyf6v89xkqvavi83hfgsq89lfd4fhn73y9krhqs4bpl") (f (quote (("commandline" "clap" "anyhow"))))))

