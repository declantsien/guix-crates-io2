(define-module (crates-io na me named-binary-tag) #:use-module (crates-io))

(define-public crate-named-binary-tag-0.1.0 (c (n "named-binary-tag") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0ay3mscb7rqfgmaf8dv20gvzcsmaba96054af16y048f1miw7772")))

(define-public crate-named-binary-tag-0.2.0 (c (n "named-binary-tag") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0q5jr7pra5wmgb70b5v2lvqqymrpr280aqdws1fvhpl3wq32jr71")))

(define-public crate-named-binary-tag-0.2.1 (c (n "named-binary-tag") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "02mwnb2p9vvyyhbd4ilb04qjdkc8m118d61l41992w870s5vjfbi")))

(define-public crate-named-binary-tag-0.2.2 (c (n "named-binary-tag") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0p2zi2qasihbx18kf42c0965fvv0l3ah3pq4pykz8wnm25b2xjwz")))

(define-public crate-named-binary-tag-0.2.3 (c (n "named-binary-tag") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1s01n7zn7g84i4s5ilnhp7whdhw4cka9wxli2izxcdrxjh14frad")))

(define-public crate-named-binary-tag-0.3.0 (c (n "named-binary-tag") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0q5s0c3r0c6cs1wlc75mn68czlm21nnbqcmdk610djpi4z1crs3s")))

(define-public crate-named-binary-tag-0.4.0 (c (n "named-binary-tag") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "0s45mh69m2i2l0p8j0j8715xcghrh0xmpp3r1lvf2dya8chi1y1p")))

(define-public crate-named-binary-tag-0.5.0 (c (n "named-binary-tag") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1k1ylgrxw4njrqv9a11zjvcvfz1r4n0p3rmy3xla8x8m3rb4zyy3")))

(define-public crate-named-binary-tag-0.6.0 (c (n "named-binary-tag") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1hyfzq7ncj3h1lzgmcni188ra3jai8mrcfz0lbwm9n9vqvx9hcjj")))

