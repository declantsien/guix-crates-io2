(define-module (crates-io na me named-array) #:use-module (crates-io))

(define-public crate-named-array-0.1.0 (c (n "named-array") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full" "parsing" "proc-macro" "printing" "extra-traits"))) (k 0)))) (h "0mdkkia0ia4h0pr91gxcjxka9qy9gkanckl9yphhgzp280hr3x4i")))

(define-public crate-named-array-0.1.1 (c (n "named-array") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("derive" "parsing" "proc-macro" "printing" "extra-traits"))) (k 0)))) (h "17yms4rsfgsr8f1wvi54h3xwifx8wwn8v075jy2a09hzij102z5f")))

