(define-module (crates-io na me named_return) #:use-module (crates-io))

(define-public crate-named_return-0.1.0 (c (n "named_return") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "13y12spjf8aypq364nd4j4cxjh1bi3j7jlb2q144nx5d6awycs0z")))

(define-public crate-named_return-0.1.1 (c (n "named_return") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.4") (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "1cirqalisjnywsfx27gcbqmqcp06qkpmh7kr3k1qd9i8f3pz3d00")))

