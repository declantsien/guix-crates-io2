(define-module (crates-io na me nametag) #:use-module (crates-io))

(define-public crate-nametag-0.0.1 (c (n "nametag") (v "0.0.1") (d (list (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1gfhwb4lhpiwds7jsx4j6znl8a33drgnn5v3gpcsk5h875bk6bax") (f (quote (("std") ("default" "std"))))))

