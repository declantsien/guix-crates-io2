(define-module (crates-io na me named-future-proc-macros) #:use-module (crates-io))

(define-public crate-named-future-proc-macros-0.0.1 (c (n "named-future-proc-macros") (v "0.0.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19fj67vnn8kw43fqhxdynb4sh6cv6ll07ddj48g86ar4di6hczdg") (r "1.65")))

(define-public crate-named-future-proc-macros-0.0.2 (c (n "named-future-proc-macros") (v "0.0.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4nlb4jh39krpiqhwszasij1rirydwc5acw96vz40fdrg90p86b") (r "1.65")))

(define-public crate-named-future-proc-macros-0.1.0-pre.1 (c (n "named-future-proc-macros") (v "0.1.0-pre.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3h9zyzj0vkc8fz8ryjkc86h598i002x57x8pc1hcf04676r0ha") (r "1.65")))

