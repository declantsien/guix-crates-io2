(define-module (crates-io na me namewise) #:use-module (crates-io))

(define-public crate-namewise-0.1.0 (c (n "namewise") (v "0.1.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lp5zcmaqzmvjkfngkl9bjv219icgamks8xnrgsrpc1y8clng388")))

(define-public crate-namewise-0.1.1 (c (n "namewise") (v "0.1.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0k9kvvdkl5cynd54i34x4lkl5g7m2bq31na3rv07s25z11hq0isq")))

(define-public crate-namewise-0.2.0 (c (n "namewise") (v "0.2.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1g2kcyfjhlrc44395l82sm4hkrlvi8ykv7mp9dwschd7qykabm2a")))

(define-public crate-namewise-0.3.0 (c (n "namewise") (v "0.3.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1g1rg24c0qajamsy0kfyj9alr4595bflwh4r14w0xfcvwvp52sdw")))

(define-public crate-namewise-1.0.0 (c (n "namewise") (v "1.0.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0izh1kqgrr2vipa6syffd7giznmlzql0sskahj1l2q0aznv35jsq")))

(define-public crate-namewise-1.0.1 (c (n "namewise") (v "1.0.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1daq2w9g2rn6r17j9ih4yfvd7hkynmvdrznn7am55slnknbvhrb8")))

(define-public crate-namewise-1.2.0 (c (n "namewise") (v "1.2.0") (d (list (d (n "namewise-common") (r "^1.0.1") (d #t) (k 0)) (d (n "namewise-derive") (r "^1.0.1") (d #t) (k 0)))) (h "1m5xsy9ya6j41sjqhrw69pn5r82s32kzyxbfcaqnxw4zw22h648d")))

(define-public crate-namewise-1.2.1 (c (n "namewise") (v "1.2.1") (d (list (d (n "namewise-common") (r "^1.0.1") (d #t) (k 0)) (d (n "namewise-derive") (r "^1.0.1") (d #t) (k 0)))) (h "09pywn7pc1ra1xmmrmfkr4kn02p3mxi75xy2mf6i8g2f1603piad")))

(define-public crate-namewise-2.0.0 (c (n "namewise") (v "2.0.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "0iyd771jp50hhrhcallqb9ixrvz3agd5caxmawwrmys0q0ds8cjh")))

(define-public crate-namewise-2.1.0 (c (n "namewise") (v "2.1.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "1bqx0nkzpg49aqx7wyk0zqj7g8ydb1r9hwxingh52m7kf3x6j830")))

(define-public crate-namewise-2.2.0 (c (n "namewise") (v "2.2.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "1w5h0fh003rsp2kmzzp35lw1ydlfmabs8qzc22b90p1109x5i7wg")))

(define-public crate-namewise-2.2.1 (c (n "namewise") (v "2.2.1") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "1rs76af61kq6bk1nizzvhq384lj14d8xllnygfpwp6nz8a72glik")))

(define-public crate-namewise-2.2.2 (c (n "namewise") (v "2.2.2") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "1jjx55gp8kari7q5rs6n5hrlsjb82mja41iiqgy9wasika07q1k6")))

(define-public crate-namewise-2.3.0 (c (n "namewise") (v "2.3.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "0bzv0mvidqnm5sz6ndj3adn47q3fqh28lwckys1q1qwdl3wqm2cf")))

(define-public crate-namewise-2.3.1 (c (n "namewise") (v "2.3.1") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "0pvy25rg6akdri4v0ppq26kqv0bv4vr4dbdqr3a1vzfflmbyk5c9")))

(define-public crate-namewise-2.3.2 (c (n "namewise") (v "2.3.2") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "0mjp7d4laaij8bbq4nl4dmj8xiv2fj5bc9xp7f3f0af5nsmf5cbl")))

(define-public crate-namewise-2.4.0 (c (n "namewise") (v "2.4.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "0rgd0csc5s5k1is0dmc0yw8vqmhhrjjiql26qan0bidpz780ab0f")))

(define-public crate-namewise-2.5.0 (c (n "namewise") (v "2.5.0") (d (list (d (n "namewise-common") (r "^2.0.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.0.0") (d #t) (k 0)))) (h "14jhwxjqfka1vmxq1cj2q1apw3lzk7rj5hc20wn59jbh5ab060q7")))

(define-public crate-namewise-2.6.0 (c (n "namewise") (v "2.6.0") (d (list (d (n "namewise-common") (r "^2.6.0") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.6.0") (d #t) (k 0)))) (h "097riws7g1jjl868p8l388jhv8rwchmlbgzxr85610b2ic79z63f")))

(define-public crate-namewise-2.6.2 (c (n "namewise") (v "2.6.2") (d (list (d (n "namewise-common") (r "^2.6.2") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.6.2") (d #t) (k 0)))) (h "02xyczch8q1n3y4f5hxq2bqd6m1bdf77cgbkqjv40xs1v3wsz003")))

(define-public crate-namewise-2.6.3 (c (n "namewise") (v "2.6.3") (d (list (d (n "namewise-common") (r "^2.6.3") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.6.3") (d #t) (k 0)))) (h "1vajf7gr2zqmgyg33l435f2kwsgy5cazwg5fcffbb0f2ic1n2n7a")))

(define-public crate-namewise-2.6.6 (c (n "namewise") (v "2.6.6") (d (list (d (n "namewise-common") (r "^2.6.6") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.6.6") (d #t) (k 0)))) (h "0y89zs6nrx2id2nndvfpc4xj6bw0qc9704wl334171jym0ch7g95")))

(define-public crate-namewise-2.6.8 (c (n "namewise") (v "2.6.8") (d (list (d (n "namewise-common") (r "^2.6.8") (d #t) (k 0)) (d (n "namewise-derive") (r "^2.6.8") (d #t) (k 0)))) (h "0d7mk4q7dvwn9p1jwywbl943gh6vk2qwbkjppx644yl03v72jzwa")))

