(define-module (crates-io na me name_of) #:use-module (crates-io))

(define-public crate-name_of-0.0.0 (c (n "name_of") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1zk3x1akcrkh9am859ky6bx9sl3fxmj1iaim3vf1r72wi7vb13vi") (f (quote (("default"))))))

(define-public crate-name_of-0.1.0 (c (n "name_of") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "040ds592c06s5vs3fiamyk6fcgsxlwg1h0kp1j3fgr69c3sv5pvj") (f (quote (("default"))))))

(define-public crate-name_of-0.1.1 (c (n "name_of") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "19xb5nlvbq8cpdc44bij3xi69wz0adg9n5czv7x72r9qvr3mzcly") (f (quote (("default"))))))

