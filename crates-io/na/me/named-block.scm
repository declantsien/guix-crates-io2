(define-module (crates-io na me named-block) #:use-module (crates-io))

(define-public crate-named-block-0.1.0 (c (n "named-block") (v "0.1.0") (d (list (d (n "static-cond") (r "^0.1") (d #t) (k 0)))) (h "1yvh26pxbfjfdhdzx15lya3idk3bl8cxq24da2s6b4cq3jx83s65") (f (quote (("nightly"))))))

(define-public crate-named-block-0.1.1 (c (n "named-block") (v "0.1.1") (d (list (d (n "static-cond") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "static-cond") (r "^0.1") (d #t) (k 2)))) (h "0jkqrhqmz9lxsqp401d51x86rvrhal5zw8g7qapbbv9m91mx8m69") (f (quote (("nightly" "static-cond"))))))

(define-public crate-named-block-0.2.0 (c (n "named-block") (v "0.2.0") (d (list (d (n "static-cond") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "static-cond") (r "^0.1") (d #t) (k 2)))) (h "0j7v4ghm87f084pyp9mm2qvj8ghrrw4si8hk6bq2aldsn0b0j5b7") (f (quote (("nightly" "static-cond"))))))

(define-public crate-named-block-0.3.0 (c (n "named-block") (v "0.3.0") (d (list (d (n "static-cond") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "static-cond") (r "^0.1") (d #t) (k 2)))) (h "0vfk8pv57naqva94q8wp45d9mw7y6sfspzs8l5f84rz0wxjdxb3j") (f (quote (("nightly" "static-cond"))))))

(define-public crate-named-block-0.3.1 (c (n "named-block") (v "0.3.1") (d (list (d (n "static-cond") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "static-cond") (r "^0.1") (d #t) (k 2)))) (h "1ny6l95j798sl7h0fcb28ibd7wsrrrs2ad2g2xlqn05pzs87ic2i") (f (quote (("nightly" "static-cond"))))))

