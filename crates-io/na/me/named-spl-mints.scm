(define-module (crates-io na me named-spl-mints) #:use-module (crates-io))

(define-public crate-named-spl-mints-0.1.0 (c (n "named-spl-mints") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)))) (h "0k23lsl6s75sgb72kjbs27l0fd97psh57ashmrzzjbihlmkz94ng") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-named-spl-mints-0.1.1 (c (n "named-spl-mints") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.19.0") (d #t) (k 0)))) (h "0axj3s4grs4vzqy0b6hqwjwcx9ncvxfn4a0cgn8qzvl923r43a2b") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

