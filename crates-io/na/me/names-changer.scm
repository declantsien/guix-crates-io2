(define-module (crates-io na me names-changer) #:use-module (crates-io))

(define-public crate-names-changer-0.1.0 (c (n "names-changer") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1v62b4ig0qqrpgzqmxb5l460kflxqi2wdh1j4wf2qn448s7cp6dh") (y #t)))

(define-public crate-names-changer-0.1.1 (c (n "names-changer") (v "0.1.1") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zcxibc129y4jvy3jsl55kifhppi6q10q51wcz1hpla6vz2w3xd5") (y #t)))

(define-public crate-names-changer-0.2.0 (c (n "names-changer") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sigmsh53ynlkmci670hpjh0chdk3vj1id6f4nizivjvw8xkk04a")))

(define-public crate-names-changer-0.2.1 (c (n "names-changer") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02prsszw4ckzccj6whd0c4s7hlpgj68kqdbzd0kdw566my3dzr8f")))

