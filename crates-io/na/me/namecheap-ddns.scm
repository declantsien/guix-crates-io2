(define-module (crates-io na me namecheap-ddns) #:use-module (crates-io))

(define-public crate-namecheap-ddns-0.1.0 (c (n "namecheap-ddns") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cagg2aq9x9ybpb050495a3bj24i6205b5xsbz9gdd14hqhrn7p6")))

(define-public crate-namecheap-ddns-0.1.1 (c (n "namecheap-ddns") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.17") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "08g45mq72h7c42xjj673cnz72j9ysy6sq0brpzzs3cnw54ycmw9s")))

(define-public crate-namecheap-ddns-0.2.0 (c (n "namecheap-ddns") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0qgqi5kj311c70b441028j4395gw3nhzf16l8nplaasmfklrg4hn")))

(define-public crate-namecheap-ddns-0.2.1 (c (n "namecheap-ddns") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0cd0r5fprn1zgyfjlx0al44724gxj02swsipk3609r80sq7dv62b")))

(define-public crate-namecheap-ddns-0.2.2 (c (n "namecheap-ddns") (v "0.2.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0sxsysz7c83n8ydnvzdkjp8bfpqsnysff0r1i5flps89wzjnvfjr")))

(define-public crate-namecheap-ddns-0.2.3 (c (n "namecheap-ddns") (v "0.2.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18nbk2wc4gf76xnis6mc5mk6kcn3va4q4xfdbci8y6gjmdrxnnb0")))

(define-public crate-namecheap-ddns-0.2.4 (c (n "namecheap-ddns") (v "0.2.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1z19amahi5ds7p67kz1l9jzwqnnnfg52bvpbrj15g3jcjmj5p5x9")))

(define-public crate-namecheap-ddns-0.3.0 (c (n "namecheap-ddns") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0dniyq24286mc563w4dlccv6n8knf579bgqshrmdyahcwla7f801")))

(define-public crate-namecheap-ddns-0.3.1 (c (n "namecheap-ddns") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0hs9rvd4cfrmyzwx0gqd914xfyyac6n99kr3lg0g8lkk5bwd88lf")))

(define-public crate-namecheap-ddns-0.3.2 (c (n "namecheap-ddns") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0x4lln8g0c7cn4mkkkb50ncncmfvxv1ay6l213vpnbvdc160qnh0")))

(define-public crate-namecheap-ddns-0.3.3 (c (n "namecheap-ddns") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "minreq") (r "^2") (f (quote ("https"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1lqwzd3f892m50znw63r7m6b6lr2357kp4ww0wlszmg62wc732bg")))

