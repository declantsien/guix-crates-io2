(define-module (crates-io na me nametable) #:use-module (crates-io))

(define-public crate-nametable-0.1.0 (c (n "nametable") (v "0.1.0") (h "0rppv6r048lis0gy9qpadlgnw1v50fb9xfavn53m6h81ivhr3idv")))

(define-public crate-nametable-0.1.1 (c (n "nametable") (v "0.1.1") (h "1gydx07aa2sccy7nfylr3c979b589slwxgv2h0h0ygg6lzd61r49")))

(define-public crate-nametable-0.1.2 (c (n "nametable") (v "0.1.2") (h "170a2kzgjbcrcp74jrf5wpinf129v9a09qkpdd8z6vy0my5czh52")))

