(define-module (crates-io na me namedarg_rustc_macro) #:use-module (crates-io))

(define-public crate-namedarg_rustc_macro-0.1.1 (c (n "namedarg_rustc_macro") (v "0.1.1") (d (list (d (n "namedarg_hack") (r "= 0.1.1") (d #t) (k 0)))) (h "1vr91g3xh88aahijwxs9jk06pgnl7qixh7zd0jl1v1ps6sjyp102") (f (quote (("println_spam" "derive_debug") ("force_macros11_mode" "namedarg_hack/force_macros11_mode") ("derive_debug"))))))

(define-public crate-namedarg_rustc_macro-0.1.2 (c (n "namedarg_rustc_macro") (v "0.1.2") (d (list (d (n "namedarg_hack") (r "= 0.1.2") (d #t) (k 0)))) (h "13x2lgdsskiw9xzaz03xlgcp6fkbif73i6i9d3wkdqiyp8m9mxsv") (f (quote (("println_spam" "derive_debug") ("force_macros11_mode" "namedarg_hack/force_macros11_mode") ("derive_debug"))))))

(define-public crate-namedarg_rustc_macro-0.1.3 (c (n "namedarg_rustc_macro") (v "0.1.3") (d (list (d (n "namedarg_hack") (r "= 0.1.3") (d #t) (k 0)))) (h "1y6svsi5silbhf1h1mdc1127ns4c81niq5pj48vas656771bhwwh") (f (quote (("println_spam" "derive_debug") ("force_macros11_mode" "namedarg_hack/force_macros11_mode") ("derive_debug"))))))

(define-public crate-namedarg_rustc_macro-0.1.5 (c (n "namedarg_rustc_macro") (v "0.1.5") (d (list (d (n "namedarg_hack") (r "= 0.1.5") (d #t) (k 0)))) (h "1q04gyq2ikc1crsigxvf39ni7n8q1iwn8jcvwgbb5aidayxnaxri") (f (quote (("println_spam" "derive_debug") ("force_macros11_mode" "namedarg_hack/force_macros11_mode") ("derive_debug"))))))

(define-public crate-namedarg_rustc_macro-0.1.1474613452 (c (n "namedarg_rustc_macro") (v "0.1.1474613452") (d (list (d (n "namedarg_hack") (r "= 0.1.1474613452") (d #t) (k 0)))) (h "090x74hfj1z5wdmnj42544shrrpa6hqvxz46q7n0s44rcvpjvb2f") (f (quote (("println_spam" "derive_debug") ("force_macros11_mode" "namedarg_hack/force_macros11_mode") ("derive_debug"))))))

