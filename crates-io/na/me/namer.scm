(define-module (crates-io na me namer) #:use-module (crates-io))

(define-public crate-namer-0.1.0 (c (n "namer") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1rbg5x4jh1qhcwyg6i4rds1291cjlqps3w13i54spc5rzc71bhj4")))

(define-public crate-namer-0.1.1 (c (n "namer") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0vkvl8jpr3chbjylpg8blr5silr42px6lavw0xd0fx1wmqlpgjv7")))

