(define-module (crates-io na me named_tuple) #:use-module (crates-io))

(define-public crate-named_tuple-0.1.0 (c (n "named_tuple") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1w5v1m4vhdlinjaaj14dhplybwq670ghi3ys8s6fyffsgj2bawlr")))

(define-public crate-named_tuple-0.1.1 (c (n "named_tuple") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0c9kqsl76lvrwdnfsyywdcv7c33jgcqc26n5q8bgfwj0nkxb3i30")))

(define-public crate-named_tuple-0.1.2 (c (n "named_tuple") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0b8w8zb0gdrv7lmmaa9iargxpcwrx40cp0a3k7ir3k6vgv9hlxyx")))

(define-public crate-named_tuple-0.1.3 (c (n "named_tuple") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustfmt-nightly") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("fold"))) (d #t) (k 0)) (d (n "term") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0c1455pzk6hdwldpl51608kchdyag1935alpgjv677slmsa7f304") (f (quote (("dump" "rustfmt-nightly" "term") ("default"))))))

