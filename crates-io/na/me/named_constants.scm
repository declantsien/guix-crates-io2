(define-module (crates-io na me named_constants) #:use-module (crates-io))

(define-public crate-named_constants-0.1.0 (c (n "named_constants") (v "0.1.0") (h "1lvwjfgmrnapq26na0cmn2xmax6kwcs4k1jxqdwj7wz724lmyc5c")))

(define-public crate-named_constants-0.2.0 (c (n "named_constants") (v "0.2.0") (h "1mg8xjasvj7kfdjafv51zyw1xgq3xm0avp1qid7xvfqk94kqy041")))

