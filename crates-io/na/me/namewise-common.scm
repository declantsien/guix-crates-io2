(define-module (crates-io na me namewise-common) #:use-module (crates-io))

(define-public crate-namewise-common-1.2.0 (c (n "namewise-common") (v "1.2.0") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0afywa3h6c018dqy5zinf38n2z3iqc3jvsi6hkfw225jxyvjvnng")))

(define-public crate-namewise-common-1.2.1 (c (n "namewise-common") (v "1.2.1") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wp0fsyl2897qglwyyv3micp94x4mqqcwf7jygys6i2xn7z7b52l")))

(define-public crate-namewise-common-2.0.0 (c (n "namewise-common") (v "2.0.0") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06q05fynma5c0yiyrl93j142ddbn981gvxyj8v69mg4kfxp1k6vz")))

(define-public crate-namewise-common-2.1.0 (c (n "namewise-common") (v "2.1.0") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rzaicwgfrw3b6lfl22rjsg2wh04w9wykp5l29m81ph9ra8nz0sv")))

(define-public crate-namewise-common-2.2.0 (c (n "namewise-common") (v "2.2.0") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bg9fblf9zfy9m11kcmyba8vbgwfdrx06pwxjid2dxy6q7wk5yyx")))

(define-public crate-namewise-common-2.2.1 (c (n "namewise-common") (v "2.2.1") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b2x164f1bva0ghrcfg8s8fmisjlc1xcfz1cq05d58q2z46ml79a")))

(define-public crate-namewise-common-2.2.2 (c (n "namewise-common") (v "2.2.2") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f4r1iqxph2c5cx24nk03lzmzw7mv3j30m86fmr3yqcc5xsghr14")))

(define-public crate-namewise-common-2.3.0 (c (n "namewise-common") (v "2.3.0") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "010hpmh5bh99spfn4yr4xhq4k0m93blvdca5640nv21kr5fzig7w")))

(define-public crate-namewise-common-2.3.1 (c (n "namewise-common") (v "2.3.1") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vv1h0lilmizdysxma0nrzzmp1zhfwifpzy87kfppadyw0kvjvbz")))

(define-public crate-namewise-common-2.3.2 (c (n "namewise-common") (v "2.3.2") (d (list (d (n "strum") (r "^0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hg5sm4xq3f2mhs9rdipn7a7v4i9863y6v2n7afwkjw0n4gw394f")))

(define-public crate-namewise-common-2.4.0 (c (n "namewise-common") (v "2.4.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0519nb2y6d52ph3jah1mj0fxycs9hvb3cnv8qmxslinmnnfsi5lx")))

(define-public crate-namewise-common-2.5.0 (c (n "namewise-common") (v "2.5.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kkrd220b0lqjxxgcd3aw87ch86vw9f2bb9jxnwvs2avg4wjkn9v")))

(define-public crate-namewise-common-2.6.0 (c (n "namewise-common") (v "2.6.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b8qiibhgk83vzq7a0sxfjg6nwgc59pyinihdp3g1in85l4nwcy4")))

(define-public crate-namewise-common-2.6.1 (c (n "namewise-common") (v "2.6.1") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02xh4649k541asj45z8v5w33qk19rq4iky9qkpw8cxs33465iy52")))

(define-public crate-namewise-common-2.6.2 (c (n "namewise-common") (v "2.6.2") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sshm5njan70f9kgvb4k9pjkkhy7r0pblff4qmzk430i7i74iivy")))

(define-public crate-namewise-common-2.6.3 (c (n "namewise-common") (v "2.6.3") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06r1zlhs9l59ha30d9824fwd37zyrpxvs5krrwpypls9pdwsdywc")))

(define-public crate-namewise-common-2.6.4 (c (n "namewise-common") (v "2.6.4") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p0zxb3wygisw1mcssl9rya43a6aik1k297zrj8rpcipifm2ika1")))

(define-public crate-namewise-common-2.6.5 (c (n "namewise-common") (v "2.6.5") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zmp88hqqci0vqfwycm4hfg7kkpamj2465hlcpnqwxmw6im158js")))

(define-public crate-namewise-common-2.6.6 (c (n "namewise-common") (v "2.6.6") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lh3djzh17mqbmskpdvnnsh8p3hz1wv1h618k14pyk8831s5d30g")))

(define-public crate-namewise-common-2.6.8 (c (n "namewise-common") (v "2.6.8") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bk0p9i5m9hfqgh45kkjm50a6yjvb3yz72p2npdisvs7213c9ppp")))

