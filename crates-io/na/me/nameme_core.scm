(define-module (crates-io na me nameme_core) #:use-module (crates-io))

(define-public crate-nameme_core-0.2.3 (c (n "nameme_core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 1)))) (h "1anp77k53aff8jx47rvq9xxghqsnjbp68zszxbbn1i3cck82dr2l")))

