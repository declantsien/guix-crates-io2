(define-module (crates-io na me named-retry) #:use-module (crates-io))

(define-public crate-named-retry-0.1.0 (c (n "named-retry") (v "0.1.0") (d (list (d (n "tokio") (r "^1.17.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1c2v0nc2fbardfrlifh80givhzjr7iw2gyjyw63i7rphhm9zff03")))

(define-public crate-named-retry-0.2.0 (c (n "named-retry") (v "0.2.0") (d (list (d (n "tokio") (r "^1.17.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("test-util"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)))) (h "1j869cx0c7ahzfkjjwqaxxb412cjgyfl32gvvik44k2cdki5z465")))

(define-public crate-named-retry-0.2.1 (c (n "named-retry") (v "0.2.1") (d (list (d (n "tokio") (r "^1.24.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.32") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("test-util"))) (d #t) (k 2)))) (h "1rg3yvacrlm9dsrz75s3qczzhn3c6nz7xwdb27n3rkzqg7kz6s3i")))

