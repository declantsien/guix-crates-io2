(define-module (crates-io na me namewise-derive) #:use-module (crates-io))

(define-public crate-namewise-derive-1.2.0 (c (n "namewise-derive") (v "1.2.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1i6zwca389rbhdhdppmcq66jf7al6f28w8crxw4h02zw1lqkh261")))

(define-public crate-namewise-derive-1.2.1 (c (n "namewise-derive") (v "1.2.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ffii2cfxyvh8wl1rzs9p7n7q9mca5rayvnydd4554ckj747c4ld")))

(define-public crate-namewise-derive-2.0.0 (c (n "namewise-derive") (v "2.0.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17608bba4g3fnap9cgdxlmyy8l2ra2d2lwd3fafids2ibd3cllzd")))

(define-public crate-namewise-derive-2.1.0 (c (n "namewise-derive") (v "2.1.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19q01hrh0r1m6rfqsrf2nlaw62s2xxwls3hx5kbhgs92ci4p0d3g")))

(define-public crate-namewise-derive-2.2.0 (c (n "namewise-derive") (v "2.2.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lwplcbvdxhqmcvsad2qg7j9d0hj5dhawbp86fj4nybyfx6ip8zq")))

(define-public crate-namewise-derive-2.2.1 (c (n "namewise-derive") (v "2.2.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12067c4rcllppj37gn0vkr7bp0swx0qsqnwgvh7492bw3vkvsrzf")))

(define-public crate-namewise-derive-2.2.2 (c (n "namewise-derive") (v "2.2.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ir2w00zspr31n3agg8j6a2l8bsls002jqrsg3ky2c4qnhmsibyn")))

(define-public crate-namewise-derive-2.3.0 (c (n "namewise-derive") (v "2.3.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "04cmi6k5mysywaxgb43r6fwrhkcd2fj048z5i1ayb4jrbw6bslkb")))

(define-public crate-namewise-derive-2.3.1 (c (n "namewise-derive") (v "2.3.1") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08f3dwfb17fl65hvj5z7zirkfdbf5v54d18clmbcvpz6rph197jl")))

(define-public crate-namewise-derive-2.3.2 (c (n "namewise-derive") (v "2.3.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1a6ir0aidcw45fvb2ri9cpgkimi89dmqnhwk90y7wclb61gyvrw9")))

(define-public crate-namewise-derive-2.4.0 (c (n "namewise-derive") (v "2.4.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0aaqhgr59f7jgrl5rabc0nymvc445hvza6vrbnhmvpjg532nnb50")))

(define-public crate-namewise-derive-2.5.0 (c (n "namewise-derive") (v "2.5.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1w4mi1pd427iihlpn7lz5rqvlh0xdx5vscpc7r7484xg5dimqwp5")))

(define-public crate-namewise-derive-2.6.0 (c (n "namewise-derive") (v "2.6.0") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0f6s0hiy26004d1zhq929n8aaslmh5k1rw8wgchz8lqb2d8x79xg")))

(define-public crate-namewise-derive-2.6.2 (c (n "namewise-derive") (v "2.6.2") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0qqq5v208b6swv74blfaws89hzf2wa02mvhszgv8w14jdwiki2h9")))

(define-public crate-namewise-derive-2.6.3 (c (n "namewise-derive") (v "2.6.3") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "072ndcf9kppz6zzm3185glkh5hq9dqxzv2yg7jma4bspwd60ak04")))

(define-public crate-namewise-derive-2.6.4 (c (n "namewise-derive") (v "2.6.4") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "00cl9mz39lygggzw88hrv3fkfv4qm4kz6xm5r49ab75a0bh7zca9")))

(define-public crate-namewise-derive-2.6.6 (c (n "namewise-derive") (v "2.6.6") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0x13bkmw8yg4k7v2qb235kxilyy559b8jm1aqwsd2db9891grpz3")))

(define-public crate-namewise-derive-2.6.8 (c (n "namewise-derive") (v "2.6.8") (d (list (d (n "darling") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0vb78vw7f2ikhx35znb3mrv8c8bylk2d4mbxyaj9w5cy94wbdz13")))

