(define-module (crates-io na me name-it-macros) #:use-module (crates-io))

(define-public crate-name-it-macros-0.0.0 (c (n "name-it-macros") (v "0.0.0") (h "0h8rbv8pd3hhicpiynf9ycgzlwq61h6hdnmk66hk2nlj9pyy986g") (r "1.61")))

(define-public crate-name-it-macros-0.1.0 (c (n "name-it-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0vdw9577r2xq7sfq5yqz57wg23w25xicy5v8l73mmp1qabnwl4jq")))

(define-public crate-name-it-macros-0.1.3 (c (n "name-it-macros") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0p4c29cq1np6ah85pr4w33qa6q5941yc8n6mgy6490dr2hafvhbv")))

