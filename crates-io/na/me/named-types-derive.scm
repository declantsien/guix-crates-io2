(define-module (crates-io na me named-types-derive) #:use-module (crates-io))

(define-public crate-named-types-derive-0.1.0 (c (n "named-types-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wkw9hbx9iawk6ng47adxkvld40sn5klzcs62yamfza7kc3p7ymi")))

