(define-module (crates-io na me nameme) #:use-module (crates-io))

(define-public crate-nameme-0.1.0 (c (n "nameme") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0n8cpz2na2qqfgvs44cjpikbcrcssaij2djzblx1lihjni0g7in1")))

(define-public crate-nameme-0.2.3 (c (n "nameme") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nameme_core") (r "^0.2.0") (d #t) (k 0)) (d (n "tabled") (r "^0.7.0") (f (quote ("color"))) (d #t) (k 0)))) (h "1haxnpsb87sybf273qpkvmqxymfa0zxpn7mlwb0kxyryn0ash9v7")))

