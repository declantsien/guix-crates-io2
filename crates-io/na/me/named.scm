(define-module (crates-io na me named) #:use-module (crates-io))

(define-public crate-named-0.1.0 (c (n "named") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0hibb3xfh6s4d3hji61d90cl46g9qw9r608icridrgj28lzra1n8")))

