(define-module (crates-io na me named-semaphore) #:use-module (crates-io))

(define-public crate-named-semaphore-0.1.0 (c (n "named-semaphore") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.7") (d #t) (k 0)) (d (n "function_name") (r "^0.2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0f2rsrqdj25ikyvds306li36argn4ivrzky1dcbarj2f6rj735gh")))

