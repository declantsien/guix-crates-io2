(define-module (crates-io na me namefn) #:use-module (crates-io))

(define-public crate-namefn-0.1.0 (c (n "namefn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "1hsz52a6pa6zrq4lcx69z5rx3igs88jp9q3ic29ji32xyjbymbb7")))

(define-public crate-namefn-0.1.1 (c (n "namefn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "05p1grp3z5q3qwbra3r5qb0xcy24pp6mfvc7hg4prr3v25afypyr")))

(define-public crate-namefn-0.1.2 (c (n "namefn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0f2hjmcz44ykgvrj4df2mzkgcgx35x1bbigfy0dbryfwy8yil3s1")))

(define-public crate-namefn-0.1.3 (c (n "namefn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "05kn99ccwwqy5b16cp5kxpib4yw2wibw3dybmdcfsg15sims771w")))

