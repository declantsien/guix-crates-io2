(define-module (crates-io na me name_generator) #:use-module (crates-io))

(define-public crate-name_generator-0.1.0 (c (n "name_generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "07sbfxdjd9a60la0pvavv6m25s3cg7v5b266gak3ycxqwr3ng7jg")))

