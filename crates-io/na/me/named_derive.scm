(define-module (crates-io na me named_derive) #:use-module (crates-io))

(define-public crate-named_derive-0.1.0 (c (n "named_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pdx0a4zigxcdjp73sbmzzhd4x1886nka06qhg5i8l87kmfp9ryp")))

(define-public crate-named_derive-0.1.1 (c (n "named_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00yn8gnkkhdg8faypwzn5bkavla23iv1rj4vm9wmpir4vd8gv8a3")))

(define-public crate-named_derive-0.1.2 (c (n "named_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ldwv4hnjv6hk72a5g4i1nmy1sf3vsb3vsfn0igs13g2xi5gk1rq")))

