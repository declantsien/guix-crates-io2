(define-module (crates-io na me namedarg_hack) #:use-module (crates-io))

(define-public crate-namedarg_hack-0.1.0 (c (n "namedarg_hack") (v "0.1.0") (h "1al37q5ja0g1fh0rlcvw4rjzmi6325klbdakq3k5w191xa6plwn2") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.1 (c (n "namedarg_hack") (v "0.1.1") (h "1dbp9dy2mcj3m8rv58jq0krby9vvv4vz87h0nmpz1f1kkbh7c0i3") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.2 (c (n "namedarg_hack") (v "0.1.2") (h "07j5rsylgh23sy6nas7vir8qspdlaawaj16mjl3q0bwm2bspqjlh") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.3 (c (n "namedarg_hack") (v "0.1.3") (h "0a6h4z27khmxd9bv4zv65vcr1x0aj577bhric1q7f58kffqv92cr") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.4 (c (n "namedarg_hack") (v "0.1.4") (h "0q92bw0mxwxiq5agwmmjncadq0iy7kfyh33b2kgjr4hymkrj1aqw") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.5 (c (n "namedarg_hack") (v "0.1.5") (h "0b5n8xyc4m922nikbfwr9drcxway4c09pg18dlhzf4qiiviznbyw") (f (quote (("force_macros11_mode"))))))

(define-public crate-namedarg_hack-0.1.1474613452 (c (n "namedarg_hack") (v "0.1.1474613452") (h "0p1lqz6i6hcf7rh42w3z5jvlyf2h8chq5zi2am2q811xsb04lryi") (f (quote (("force_macros11_mode"))))))

