(define-module (crates-io na me namedarg) #:use-module (crates-io))

(define-public crate-namedarg-0.1.1 (c (n "namedarg") (v "0.1.1") (d (list (d (n "namedarg_rustc_macro") (r "= 0.1.1") (d #t) (k 0)))) (h "1cb90gkz67blc07by6zvv7fsaaqmjbihrkr09bq23qqyvnb4lprh") (f (quote (("force_macros11_mode" "namedarg_rustc_macro/force_macros11_mode"))))))

(define-public crate-namedarg-0.1.2 (c (n "namedarg") (v "0.1.2") (d (list (d (n "namedarg_rustc_macro") (r "= 0.1.2") (d #t) (k 0)))) (h "0vwr09chz7bqq1j0n1zf8i5s5v00a489qagzzrnr8lzwabjp3ffj") (f (quote (("force_macros11_mode" "namedarg_rustc_macro/force_macros11_mode"))))))

(define-public crate-namedarg-0.1.3 (c (n "namedarg") (v "0.1.3") (d (list (d (n "namedarg_rustc_macro") (r "= 0.1.3") (d #t) (k 0)))) (h "0q8k5027jh6sllb81mxx8n84avm1k8akk0ivmbgss5qakw78yagp") (f (quote (("force_macros11_mode" "namedarg_rustc_macro/force_macros11_mode"))))))

(define-public crate-namedarg-0.1.5 (c (n "namedarg") (v "0.1.5") (d (list (d (n "namedarg_rustc_macro") (r "= 0.1.5") (d #t) (k 0)))) (h "1dy9zqrp0cwbi4vmcwb4fdfyvvy1yvlzzai7qfsbwvygmarx3ir8") (f (quote (("force_macros11_mode" "namedarg_rustc_macro/force_macros11_mode"))))))

(define-public crate-namedarg-0.1.1474613452 (c (n "namedarg") (v "0.1.1474613452") (d (list (d (n "namedarg_rustc_macro") (r "= 0.1.1474613452") (d #t) (k 0)))) (h "12b72853x6xaxvrpa829r5rd08yvvjz5bwf64c7yj3vqrsgs0a5d") (f (quote (("force_macros11_mode" "namedarg_rustc_macro/force_macros11_mode"))))))

