(define-module (crates-io na me names) #:use-module (crates-io))

(define-public crate-names-0.9.0 (c (n "names") (v "0.9.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "06wgnmswnjpk2mv39q3w220pdd6l0iw6n62n1mygkf04vfk7zppg")))

(define-public crate-names-0.10.0 (c (n "names") (v "0.10.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0njrkx11jsarq4mkkrp3xwc8kadyd7lsj2barpwhfl2ha3w6vxj0")))

(define-public crate-names-0.11.0 (c (n "names") (v "0.11.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1npjav2wa995ldf5dawqk6l74skr3kv27nndnl7vb1ij6amhscpg")))

(define-public crate-names-0.12.0 (c (n "names") (v "0.12.0") (d (list (d (n "clap") (r "=3.0.0-beta.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0xapf5qs9yrc6fg7mgf20ypwcynkqdkdd375a6c6bgwsy05nka0h") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-names-0.13.0 (c (n "names") (v "0.13.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0ii17393shhiir5cvp57anrh8ly2346d269nnb66qjjxn91n1mp7") (f (quote (("default" "application") ("application" "clap"))))))

(define-public crate-names-0.14.0 (c (n "names") (v "0.14.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1g1rxifcsvj9zj2nmwbdix8b5ynpghs4rq40vs966jqlylxwvpbv") (f (quote (("default" "application") ("application" "clap"))))))

