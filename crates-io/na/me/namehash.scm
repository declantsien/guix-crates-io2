(define-module (crates-io na me namehash) #:use-module (crates-io))

(define-public crate-namehash-0.1.0 (c (n "namehash") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1iqzv0f88k62ncikgkc9pxpcvv5cprcnpixcmckmb5kw9b8gcmk9")))

(define-public crate-namehash-0.1.1 (c (n "namehash") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "02kyryn8lj2qjzyqyxlgpm67cc2y41ijig3qyb0bljr2riq2w2h4")))

(define-public crate-namehash-0.1.2 (c (n "namehash") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0lyw6wi3sj3233jw6n94d0l0nhi3nrpsvw3cdg710ryabalmxh8j")))

