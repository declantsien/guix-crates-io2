(define-module (crates-io na me namespaces-rs) #:use-module (crates-io))

(define-public crate-namespaces-rs-0.1.0 (c (n "namespaces-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.14.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zafc5fshficc6s1hbcy481rn570g1ac4vdwkxcdxavazvg5zx2y")))

(define-public crate-namespaces-rs-0.1.1 (c (n "namespaces-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.14.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07nl87azv6vzfjxsw2qdq7kf2iyjaalv83wb15rjainmwmzxn1db")))

