(define-module (crates-io na me named-pipe-rs) #:use-module (crates-io))

(define-public crate-named-pipe-rs-0.0.1 (c (n "named-pipe-rs") (v "0.0.1") (d (list (d (n "windows") (r "^0.28.0") (f (quote ("std" "Win32_System_Pipes" "Win32_Foundation" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO"))) (d #t) (k 0)))) (h "1w2xy1zpwhdcyjnma0d7sclf5fhfcia18hav9hz9mvn5qyrqqfkx")))

