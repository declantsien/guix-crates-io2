(define-module (crates-io na me named_pipe) #:use-module (crates-io))

(define-public crate-named_pipe-0.1.0 (c (n "named_pipe") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "~0.1.4") (d #t) (k 0)) (d (n "winapi") (r "~0.2.2") (d #t) (k 0)))) (h "1liwfdiamzp4lwzm70nc3wrzha6n8xqh22shbb71ffapz93xiglf") (y #t)))

(define-public crate-named_pipe-0.1.1 (c (n "named_pipe") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "~0.1.4") (d #t) (k 0)) (d (n "winapi") (r "~0.2.2") (d #t) (k 0)))) (h "02izwp6cs9i9i5wxi9f314jhxd5mlbgn7xkiwdx2sywr1jzm1ak9") (y #t)))

(define-public crate-named_pipe-0.1.2 (c (n "named_pipe") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "~0.1.4") (d #t) (k 0)) (d (n "winapi") (r "~0.2.2") (d #t) (k 0)))) (h "1qv933wysczajar6wlw3mr126nnrbm1728siyv0fz687yc62ljbh") (y #t)))

(define-public crate-named_pipe-0.2.0 (c (n "named_pipe") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "~0.1.4") (d #t) (k 0)) (d (n "winapi") (r "~0.2.2") (d #t) (k 0)))) (h "0fbszz3188r3inpfy2i5x33jywwcy6fn14lkz4wn8bk99ca482s7") (y #t)))

(define-public crate-named_pipe-0.2.1 (c (n "named_pipe") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "~0.1.4") (d #t) (k 0)) (d (n "winapi") (r "~0.2.2") (d #t) (k 0)))) (h "102j9zd0l7yn97y4w09w8whfl6v5jzpkwf204amgndibddx8aksl") (y #t)))

(define-public crate-named_pipe-0.2.2 (c (n "named_pipe") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "~0.2.2") (d #t) (k 0)) (d (n "winapi") (r "~0.2.8") (d #t) (k 0)))) (h "1f59ihr8qa6cnmhzyr4n7vk07f0fl3wld4ndx01lrx1cmq05sjap") (y #t)))

(define-public crate-named_pipe-0.2.3 (c (n "named_pipe") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "~0.2.2") (d #t) (k 0)) (d (n "winapi") (r "~0.2.8") (d #t) (k 0)))) (h "16a0llkzfj61wd65fjfrb7l2a7fzby6b7xhk5rjzczcy4lpyl4b7")))

(define-public crate-named_pipe-0.2.4 (c (n "named_pipe") (v "0.2.4") (d (list (d (n "kernel32-sys") (r "~0.2.2") (d #t) (k 0)) (d (n "winapi") (r "~0.2.8") (d #t) (k 0)))) (h "0mdaz9vc63zghrfw2xyli5gsdacw6vnby32f7lm927qkn6sy4vsi")))

(define-public crate-named_pipe-0.3.0 (c (n "named_pipe") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "~0.2.2") (d #t) (k 0)) (d (n "winapi") (r "~0.2.8") (d #t) (k 0)))) (h "0vgsdvg9zhg4s6fjfwz5h75fnbalblfjrcajapbybxzmqid0mlcf")))

(define-public crate-named_pipe-0.4.0 (c (n "named_pipe") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "synchapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "1442cgg1xh31wcb9kbyik9dmfj8w4bdnb6001khsb97ky2pnsd1h")))

(define-public crate-named_pipe-0.4.1 (c (n "named_pipe") (v "0.4.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "ioapiset" "minwindef" "namedpipeapi" "synchapi" "winbase" "winerror"))) (d #t) (k 0)))) (h "0azby10wzmsrf66m1bysbil0sjfybnvhsa8py093xz4irqy4975d")))

