(define-module (crates-io na me named_type) #:use-module (crates-io))

(define-public crate-named_type-0.1.0 (c (n "named_type") (v "0.1.0") (h "1psqzkp5pwc6c9k88ys861jva6m7z4d70qq3xv5484n4w1c6myic")))

(define-public crate-named_type-0.1.1 (c (n "named_type") (v "0.1.1") (h "17n3nmssbimmx8dv08d6sacg5cgwi7b0z1x9mkm8dr6gw0ivg9sf")))

(define-public crate-named_type-0.1.2 (c (n "named_type") (v "0.1.2") (d (list (d (n "named_type_derive") (r "^0.1.2") (d #t) (k 2)))) (h "19g8jlpkxmq7v7bazlnsk0ayddk6s1r6x81nqvz2c7gdqdz8mvr3")))

(define-public crate-named_type-0.1.3 (c (n "named_type") (v "0.1.3") (d (list (d (n "named_type_derive") (r "^0.1.3") (d #t) (k 2)))) (h "086gckjnb6cav5i0afcnqd6s2wx1ib1j7k4a0m1bw1p89ssgc6dh")))

(define-public crate-named_type-0.1.4 (c (n "named_type") (v "0.1.4") (h "13zlsvrm8nhnxvb2vdlzfs0723zaf28r07wvsmka9byrnrx17d94")))

(define-public crate-named_type-0.1.5 (c (n "named_type") (v "0.1.5") (h "0vfks8fp7mqpgxl4fg4ajfzhqspgkryp9p19fyghs4m9h3vn1775")))

(define-public crate-named_type-0.1.6 (c (n "named_type") (v "0.1.6") (h "0rly1jjv2gnpaqmmkdvz7z7qjjg8myq9h8d8g645bgqkxv2wd6n3")))

(define-public crate-named_type-0.2.0 (c (n "named_type") (v "0.2.0") (h "188g7l0h01q3r6agwk87rvw9dz9ghmq1i1f2356805d8dy7zvpfy")))

(define-public crate-named_type-0.2.1 (c (n "named_type") (v "0.2.1") (h "1mqk81qw3k19xcp6dh2icjx04p5nqlafk4879b6i90vykx34b6rs")))

(define-public crate-named_type-0.2.2 (c (n "named_type") (v "0.2.2") (d (list (d (n "named_type_derive") (r "^0.2.2") (d #t) (k 2)))) (h "1fcn0gyx693j2hbf6fph7n21ka200z5zwvf8d30j74nrm3mxz98m")))

