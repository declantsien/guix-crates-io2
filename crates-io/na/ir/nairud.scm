(define-module (crates-io na ir nairud) #:use-module (crates-io))

(define-public crate-nairud-0.1.1 (c (n "nairud") (v "0.1.1") (h "03vq4nlff5gxl7s97r9c3kmzbd55qwmr4ih8bn7d6prndk014z6d") (f (quote (("std"))))))

(define-public crate-nairud-0.1.2 (c (n "nairud") (v "0.1.2") (h "1ag3wrrknnjw0sqknf9hja429gg7if0r9zr9qvilfywr6x6shi33") (f (quote (("std"))))))

(define-public crate-nairud-0.1.3 (c (n "nairud") (v "0.1.3") (h "1s50mxacqr3c675j60fb0kyyfpc1zm6103rl3xkl31nbbvml7m00") (f (quote (("std"))))))

(define-public crate-nairud-0.1.4 (c (n "nairud") (v "0.1.4") (h "0j6mvmiz8wkgsxq8xdx6lwrbs644sm0rr2mqfq1q5kz98jw672v5") (f (quote (("std"))))))

(define-public crate-nairud-0.1.5 (c (n "nairud") (v "0.1.5") (h "1qhi8i937iy1ngkdj3y7pzq96x2k6xgyl87bvf4ir6fg4mj539c2") (f (quote (("std"))))))

(define-public crate-nairud-0.2.0 (c (n "nairud") (v "0.2.0") (h "096dwwbv2jnqpbyfy7slva3q4rj686kv8xicai2br0kx2i0g1dnf") (f (quote (("std"))))))

(define-public crate-nairud-0.3.0 (c (n "nairud") (v "0.3.0") (h "1xqr3vsdag637zqqp82rvacpvgfjrf9mzqk4lgcyx8a4263qffk4") (f (quote (("std"))))))

(define-public crate-nairud-0.3.1 (c (n "nairud") (v "0.3.1") (h "1gzbc3mwr47f8x7r84bj8bvb171gqpfj3q66283g2cq8cygsw99i") (f (quote (("std"))))))

(define-public crate-nairud-0.3.2 (c (n "nairud") (v "0.3.2") (h "04kd1rqllaj3cjf1kawqkg5sic8dlybxq9hliqy8bhzdwqdzw7hv") (f (quote (("std"))))))

(define-public crate-nairud-0.3.3 (c (n "nairud") (v "0.3.3") (h "1dj6ga8a8zmj6bzb2lnhrnks8w0rpq1gfq1sasw18b84195f72wa") (f (quote (("std"))))))

(define-public crate-nairud-0.3.4 (c (n "nairud") (v "0.3.4") (h "1yp6mpq61k2l2g1lk1l9agq5g6qvd4lrggy1xaxkbl70yvyha582") (f (quote (("std"))))))

(define-public crate-nairud-0.4.0 (c (n "nairud") (v "0.4.0") (h "0ncssymczhlmpgwwyid5gz74g0i9gn0hwm68g3mbx23fvqbawsas") (f (quote (("std"))))))

(define-public crate-nairud-0.4.1 (c (n "nairud") (v "0.4.1") (h "1za31sq42y9di9hk3ib9cfvcm6f5bc04cmbl3zhkrvg7q9jijzwr") (f (quote (("std"))))))

(define-public crate-nairud-0.5.0 (c (n "nairud") (v "0.5.0") (h "0hgsif8ls3ill00wvdzhl4a16scki700mqbpk62anwq9ff8av1d2") (f (quote (("std"))))))

(define-public crate-nairud-0.5.1 (c (n "nairud") (v "0.5.1") (h "0580776pdg2sbkkwf2sjmyswd9f5m97w15z8bn4vwq427xljvmsv") (f (quote (("std"))))))

(define-public crate-nairud-0.5.2 (c (n "nairud") (v "0.5.2") (h "0cg7gjcrj9mdffm39crlan9w92hx9hfankyl31rj6h5vk70g3b16") (f (quote (("std"))))))

(define-public crate-nairud-0.5.3 (c (n "nairud") (v "0.5.3") (h "13jp2hym6rpa9fz7rqgzv86cwn7ng49cm678b5w26qp11h0k3vxc") (f (quote (("std"))))))

(define-public crate-nairud-0.5.4 (c (n "nairud") (v "0.5.4") (h "1czccrsdg4gjhfajrqp9vlx20bk3sd069002y0pm2ldlyjwijv0b") (f (quote (("std"))))))

(define-public crate-nairud-0.5.5 (c (n "nairud") (v "0.5.5") (h "19qckj0jq2qa8lndwaz2jda4i2zh10l81as9rwr5jp44ffla2hif") (f (quote (("std"))))))

(define-public crate-nairud-0.5.6 (c (n "nairud") (v "0.5.6") (h "0xlfwkf7bch8y2vr5av9ympr96cmkd522z3adpv0qsmd0c35gd6k") (f (quote (("std"))))))

(define-public crate-nairud-0.5.7 (c (n "nairud") (v "0.5.7") (h "1zpqh99pmqi5nivcw60f2mgc7l9ndnzxpjhzikipc9cywn769pkm") (f (quote (("std"))))))

(define-public crate-nairud-0.5.8 (c (n "nairud") (v "0.5.8") (h "0gxm4yljlnn9bwzcj80lhs75xcdadx7jja3mzs5aspjndmnykwxf") (f (quote (("std"))))))

(define-public crate-nairud-0.5.9 (c (n "nairud") (v "0.5.9") (h "0y3q52id2acm8z0087bb4w0ksqvlqipgf36y6g1hcgck1dq4rsjg") (f (quote (("std"))))))

(define-public crate-nairud-0.5.10 (c (n "nairud") (v "0.5.10") (h "0bd87kic0krxqfnrp5sclspv6q0ljc1ly6p7zxps1iq7qysciyd2") (f (quote (("std"))))))

(define-public crate-nairud-0.5.11 (c (n "nairud") (v "0.5.11") (h "0jjkla9rc43nxsvxdgy45grw5dp56ajfax5ibbamif65kiyjd6i2") (f (quote (("std"))))))

(define-public crate-nairud-0.5.12 (c (n "nairud") (v "0.5.12") (h "178ch5nz55h8cq7l8b6nwl8kmxmyax3cpqv8myrmr2yknxbh2mf7") (f (quote (("std"))))))

(define-public crate-nairud-0.5.13 (c (n "nairud") (v "0.5.13") (h "0gyxsil410iykvjs7ibl2xp96qhi6zydpal8ka06dc1smdsvc85s") (f (quote (("std"))))))

(define-public crate-nairud-0.5.14 (c (n "nairud") (v "0.5.14") (h "0392lqw0s0x2h6g8l40nzybpydp6qnqkplwnrrg92dfaal966q37") (f (quote (("std"))))))

(define-public crate-nairud-0.5.15 (c (n "nairud") (v "0.5.15") (h "0bpsbg98nq6m1nd4vq064p17qm8f6i1lndfh3c4s64lyhbnc7vnr") (f (quote (("std"))))))

