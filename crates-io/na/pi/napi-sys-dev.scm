(define-module (crates-io na pi napi-sys-dev) #:use-module (crates-io))

(define-public crate-napi-sys-dev-0.0.1 (c (n "napi-sys-dev") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jzxmqdk0hkdz5z319dbgp1zazmm427fvyjsr92azra5l7jby8zv")))

(define-public crate-napi-sys-dev-0.0.2 (c (n "napi-sys-dev") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b6d7ipjgxfdk4z2qdnd2vsc83abqkfrcmk3q9ijp8j4m0mzf1jh")))

(define-public crate-napi-sys-dev-0.0.3 (c (n "napi-sys-dev") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jia0hqpxwikr9yr73a6wnpi9bk852h6x5k6pfff3rqg6pa3kfs2")))

