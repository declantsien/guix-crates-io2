(define-module (crates-io na pi napi-build-ohos) #:use-module (crates-io))

(define-public crate-napi-build-ohos-0.0.1 (c (n "napi-build-ohos") (v "0.0.1") (h "1wfkjmhcbgf28jamkngfs5k6qdx0crqcb3wr4k5ffydgpwj2w8fm") (r "1.65")))

(define-public crate-napi-build-ohos-0.0.2 (c (n "napi-build-ohos") (v "0.0.2") (h "0aqmi9f5cgmi69vv9nkavd86sapgdm2jiylk2yy23nhr1x6rrdih") (r "1.65")))

(define-public crate-napi-build-ohos-0.0.3 (c (n "napi-build-ohos") (v "0.0.3") (h "18mc2i1jp8pbb5y8l8yjrp1k3kmf59y86dd7j9868pdd3yp7j8yx") (r "1.65")))

(define-public crate-napi-build-ohos-1.0.0-beta.0 (c (n "napi-build-ohos") (v "1.0.0-beta.0") (h "029j2mwkb8gk3li57d0113j4bissgjklj3jmk657l4smdp39fij9") (r "1.65")))

(define-public crate-napi-build-ohos-1.0.0-beta.1 (c (n "napi-build-ohos") (v "1.0.0-beta.1") (h "1rcmmqmgrlszk3y3p8a2za5n23j6pcd7map45gkcyr3iy28fgfwp") (r "1.65")))

(define-public crate-napi-build-ohos-1.0.0-beta.2 (c (n "napi-build-ohos") (v "1.0.0-beta.2") (h "1q1dn0a63x5yrxkyzzrxb99cr7cjyr2bfmb8cixrdlcdld94d5qr") (r "1.65")))

