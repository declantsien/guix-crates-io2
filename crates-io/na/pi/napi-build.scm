(define-module (crates-io na pi napi-build) #:use-module (crates-io))

(define-public crate-napi-build-0.1.0 (c (n "napi-build") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "14bqf12p3w2g51rxcqr11plqc5scg1xcz2pkh9ch1ic628g9rsrb")))

(define-public crate-napi-build-0.1.1 (c (n "napi-build") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04vpxa48cb0lv7l69fgmh6nk7y78i49bswacpjh73q3nz5dd0131")))

(define-public crate-napi-build-0.2.0 (c (n "napi-build") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1d6kas3b2qwvkqqml1y9d6zyjjpypv4n91kxx0rs7ydic06k1n9g")))

(define-public crate-napi-build-0.2.1 (c (n "napi-build") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("native-tls" "blocking"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1z02mlw1wa01fjpjnqns3f3vxacbg1jnk98hcg3pgwp5xy3zdyqq")))

(define-public crate-napi-build-1.0.0-alpha.0 (c (n "napi-build") (v "1.0.0-alpha.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.0") (f (quote ("native-tls"))) (t "cfg(windows)") (k 0)))) (h "079pszd4bwzkh6wm79c531krd7ywpsfqdrycqs9q25vzga970qc1")))

(define-public crate-napi-build-1.0.0 (c (n "napi-build") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.5.0") (f (quote ("native-tls"))) (t "cfg(windows)") (k 0)))) (h "0yh8sj97nbf9b0ian9q6vkg2hrn9a10kbqsscw7h1dd2yg98j1z9")))

(define-public crate-napi-build-1.0.1 (c (n "napi-build") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (t "cfg(windows)") (k 0)))) (h "0yp2mgb494icx2l1wphlm4brg1khjbi93mpwxfsb1vmy62ryfapi")))

(define-public crate-napi-build-1.0.2 (c (n "napi-build") (v "1.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (t "cfg(not(target_env = \"musl\"))") (k 0)))) (h "1f19kgadx1rdjm09xi27jn4yv83ic2dap713c033c0330qaysf9h")))

(define-public crate-napi-build-1.1.0-beta.0 (c (n "napi-build") (v "1.1.0-beta.0") (h "0kxjp1j079mhwcwd3pv0wwyfcgd23w4sc6330pq605nk29pf7czr")))

(define-public crate-napi-build-1.1.0 (c (n "napi-build") (v "1.1.0") (h "1k2xb8xhnp3fqb53cs3crraic9cy5q1smqm02am4h6k05jbi3glz")))

(define-public crate-napi-build-1.1.1 (c (n "napi-build") (v "1.1.1") (h "12my92frckz4garygyas4kij3zbfjfp0x1ygdihdss07zyn5ndw7")))

(define-public crate-napi-build-1.1.2 (c (n "napi-build") (v "1.1.2") (h "1207f54rf28ka4x9cf6v9mr3iyjbzj5shkbhr1a26igg2g6z6snl")))

(define-public crate-napi-build-1.2.0 (c (n "napi-build") (v "1.2.0") (h "1vrvl3x8knfjh4qblvzjr1p0y635pdavczsjclx87zfgk72lqn6r")))

(define-public crate-napi-build-1.2.1 (c (n "napi-build") (v "1.2.1") (h "0asjspv0gkg6lgdz3d6gl9ab7hbc9z2hc51m637j6x3jfa8l3m7b")))

(define-public crate-napi-build-2.0.0 (c (n "napi-build") (v "2.0.0") (h "16yl5l7d1vq2gy79d0ccn3hln6a4fsd88qh003k6b61yk5ys1fyn")))

(define-public crate-napi-build-2.0.1 (c (n "napi-build") (v "2.0.1") (h "03nz85fliiarrkj121y1ibqw0rzl5spaddpzpcpdrs13xzcp6al8") (r "1.57")))

(define-public crate-napi-build-2.1.0 (c (n "napi-build") (v "2.1.0") (h "1pqkdwrgavrssjkcqrkm1sgqf8ri3rb5xila6ibgbzkbz0n57d6l") (r "1.65")))

(define-public crate-napi-build-2.1.1 (c (n "napi-build") (v "2.1.1") (h "06xii3hbr1ygpw6y0ydwajscnv6d7g91a0bz9jnccz7asy5nkak0") (r "1.65")))

(define-public crate-napi-build-2.1.2 (c (n "napi-build") (v "2.1.2") (h "0hrwh8yilm5d5wgvikkaqs1hh3cgl64s0d4v0vr3qxjzrky3149g") (r "1.65")))

(define-public crate-napi-build-2.1.3 (c (n "napi-build") (v "2.1.3") (h "02jnsl9zy16q4w0ry65lybiqlp30b1zapxb1bdla8274gvbgbh71") (r "1.65")))

