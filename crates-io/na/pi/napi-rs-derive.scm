(define-module (crates-io na pi napi-rs-derive) #:use-module (crates-io))

(define-public crate-napi-rs-derive-0.1.0 (c (n "napi-rs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1hnw92s3lc4s48walca54ji13hilgdy0nlxr9z15k69vmngfb9dh")))

(define-public crate-napi-rs-derive-0.2.0 (c (n "napi-rs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1067vrg26sb19vqq45jyp9b2ijm76ch4403lbcxdymk0ak69wf69")))

