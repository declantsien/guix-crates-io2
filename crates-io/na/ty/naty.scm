(define-module (crates-io na ty naty) #:use-module (crates-io))

(define-public crate-naty-0.1.0 (c (n "naty") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "0hx5b1ncsw16zvs06098jfhr27lvh07dhrxk90jmsm8bmvsz0q9p")))

(define-public crate-naty-0.1.4 (c (n "naty") (v "0.1.4") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "downloader") (r "^0.2.6") (f (quote ("tui"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "0a4w4jslwnc7v9ggangmdsa8fpjwdqnp8aaxs96c0zn54241j1kn")))

(define-public crate-naty-0.1.6 (c (n "naty") (v "0.1.6") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "downloader") (r "^0.2.6") (f (quote ("tui"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "0bvgnp4a9mqw2qjv399bkj2pd3v6pysqpsl5a0l4vq1hp95aaiak")))

(define-public crate-naty-0.1.7 (c (n "naty") (v "0.1.7") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "downloader") (r "^0.2.6") (f (quote ("tui"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "0lwjnlkxn3ac8mim9gvzc9dbcpv9yv8p6q998igm7bmjbb49v6x0")))

(define-public crate-naty-0.2.0 (c (n "naty") (v "0.2.0") (d (list (d (n "naty_app") (r "^0.2.0") (d #t) (k 0)) (d (n "naty_common") (r "^0.2.0") (d #t) (k 0)) (d (n "naty_nativefy") (r "^0.2.0") (d #t) (k 0)))) (h "13f9023drsr4j2d1ra55984jbvywvpkzjzkvhzrnacmjvkpqk2xn")))

(define-public crate-naty-0.2.1 (c (n "naty") (v "0.2.1") (d (list (d (n "naty_app") (r "^0.2.1") (d #t) (k 0)) (d (n "naty_common") (r "^0.2.1") (d #t) (k 0)) (d (n "naty_nativefy") (r "^0.2.1") (d #t) (k 0)))) (h "1rdcjq0vaycd72k4lnrcr955gg3xdqfb9rwda0a7g30nmsiysb1j")))

(define-public crate-naty-0.3.1 (c (n "naty") (v "0.3.1") (d (list (d (n "naty_app") (r "^0.3.1") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.1") (d #t) (k 0)) (d (n "naty_nativefy") (r "^0.3.1") (d #t) (k 0)))) (h "17lgfj38jmwqkxq9l2r5mx4pmcrwlf190snc575j2n9040x2n0vs")))

(define-public crate-naty-0.3.8 (c (n "naty") (v "0.3.8") (d (list (d (n "naty_app") (r "^0.3.8") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.8") (d #t) (k 0)) (d (n "naty_nativefy") (r "^0.3.8") (d #t) (k 0)))) (h "166wy2cx5dfajc9bqiqq6p9qg9ql4jy40agn6xp314cbma6h6r78")))

(define-public crate-naty-0.3.9 (c (n "naty") (v "0.3.9") (d (list (d (n "naty_app") (r "^0.3.8") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.8") (d #t) (k 0)) (d (n "naty_nativefy") (r "^0.3.8") (d #t) (k 0)))) (h "1rs431ysmk9q42z5ac48i7bicpy05s9zqk3lpy6zi3ppsxj5sqy6")))

