(define-module (crates-io na ty naty_common) #:use-module (crates-io))

(define-public crate-naty_common-0.2.0 (c (n "naty_common") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)))) (h "04d3scilm2k01niqa1dks5hqpm1v6v8wkq2ny45ym3qvyhdqjwsk")))

(define-public crate-naty_common-0.2.1 (c (n "naty_common") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l4cvrbhkmmf01npj6j25irqha7ywdpdlfsxh109mkdpax00nbb9")))

(define-public crate-naty_common-0.3.1 (c (n "naty_common") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0a4mx3kid491v1nx4vnzvw0zpfdm16993fyavq84k6fzdiwnx40g")))

(define-public crate-naty_common-0.3.2 (c (n "naty_common") (v "0.3.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1g4gryrih14xnh7l6g9rp642sjlr3qx6h6d0xibw9fr32nc9jq18")))

(define-public crate-naty_common-0.3.6 (c (n "naty_common") (v "0.3.6") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "16wr08bqz01ic7v24rbl1lqhhd5fj9azyl6jxalcdprg46pmknjy")))

(define-public crate-naty_common-0.3.8 (c (n "naty_common") (v "0.3.8") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "19lkqv3vdn76rigrk89hfn7zik1c52knsnqn3p7qnf9rkxpz4024")))

(define-public crate-naty_common-0.3.9 (c (n "naty_common") (v "0.3.9") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1qqg4rajrga4sipb53jwq6hd9gyrjrqk00wi0xym8nxk0s7mpaw8")))

