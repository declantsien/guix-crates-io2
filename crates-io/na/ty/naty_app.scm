(define-module (crates-io na ty naty_app) #:use-module (crates-io))

(define-public crate-naty_app-0.2.0 (c (n "naty_app") (v "0.2.0") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "naty_common") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "0zqyvi6nszll0z5r9cfbd0vhs7dg0wbwh6yjg00vv5y1r20f2f00")))

(define-public crate-naty_app-0.2.1 (c (n "naty_app") (v "0.2.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "naty_common") (r "^0.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.0") (d #t) (k 0)))) (h "026baffsfrzg8hx9fv36cwd64wdr3xlja4c8ji9a8gy0fhmrziyb")))

(define-public crate-naty_app-0.3.1 (c (n "naty_app") (v "0.3.1") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.2") (d #t) (k 0)))) (h "072xvqpnm0vxac0djl6cn1lwl0rkdkfk4rqvkqnzvp4i9mw86n83")))

(define-public crate-naty_app-0.3.8 (c (n "naty_app") (v "0.3.8") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.2") (d #t) (k 0)))) (h "03xlpirdax7fa48yi16mqm4sbrhpbs7k9f2hzym0vcwm3gs3acyj")))

(define-public crate-naty_app-0.3.9 (c (n "naty_app") (v "0.3.9") (d (list (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "naty_common") (r "^0.3.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "wry") (r "^0.20.2") (d #t) (k 0)))) (h "0d8q08lnas9jzggc418qhbwkxs787f2wk05kq4q2apbs2mc4fxgc")))

