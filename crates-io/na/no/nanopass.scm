(define-module (crates-io na no nanopass) #:use-module (crates-io))

(define-public crate-nanopass-0.1.0 (c (n "nanopass") (v "0.1.0") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0qn4irrw05zmp1gz4njqw1x9si2wkjiimhkkxnb87gsqmgxjm946")))

(define-public crate-nanopass-0.1.1 (c (n "nanopass") (v "0.1.1") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0gjmgxn8m9xm8qdnc0y459f7ahli46msranqq3j5575lwpg2h8dg")))

(define-public crate-nanopass-0.1.2 (c (n "nanopass") (v "0.1.2") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0y9sphdq56xkg7fm34g0ac0fxlglaxlsmh6ak183qjmhnn7mr326")))

(define-public crate-nanopass-0.1.3 (c (n "nanopass") (v "0.1.3") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1zwzfla95wrwmrgfdi1m0vhn2z7hnf15l0011944y0jlm7i1g1lq")))

(define-public crate-nanopass-0.1.4 (c (n "nanopass") (v "0.1.4") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1nsbl1znfscdkyw3ffm0xca2xvrmnsdxhh15ibqh3z3bn5xh9sv7")))

(define-public crate-nanopass-0.1.5 (c (n "nanopass") (v "0.1.5") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "07hnb5d4k0rfdpfdxmxf2jmb4b5r5a6zzqlpp1ixvdknyrkjks5g")))

(define-public crate-nanopass-0.1.6 (c (n "nanopass") (v "0.1.6") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0f1bmk8s5ww57sg62l05c395c4hz8pcb1z6wdd03pv8cb4f1dssv")))

(define-public crate-nanopass-0.1.7 (c (n "nanopass") (v "0.1.7") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1g95ngwasb1y0c12rm9zd8hpbprhi9shhj19gcrxw9n4i8mcpi50")))

(define-public crate-nanopass-0.1.8 (c (n "nanopass") (v "0.1.8") (d (list (d (n "base-encode") (r "^0.3.1") (d #t) (k 0)) (d (n "blake3") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1h52gwbb71cm3p7bsnxhqaq261k2xxl1306m6pziyzyxzs15wv5b")))

