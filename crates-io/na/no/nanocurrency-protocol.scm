(define-module (crates-io na no nanocurrency-protocol) #:use-module (crates-io))

(define-public crate-nanocurrency-protocol-0.1.0 (c (n "nanocurrency-protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "00r7ry51xh8n96m45zjgf2r5lgbcmwhzfl6w499lfag30wnrh4kb")))

(define-public crate-nanocurrency-protocol-0.2.0 (c (n "nanocurrency-protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.2") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0b09n5dyxsg2yagzkdc7kfv9pj3irxb8qikipbjf0cam2bycdrx7")))

(define-public crate-nanocurrency-protocol-0.3.0 (c (n "nanocurrency-protocol") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.2") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "15hj6ry9h0zh6vk8x12z3zr07kx45cyng2b1pvix6lvm7alg3waj")))

(define-public crate-nanocurrency-protocol-0.4.1 (c (n "nanocurrency-protocol") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "04f41g4wi4mip421y47aj8520f2bvzmfsdgvldfy5jjvanrlk5n8")))

(define-public crate-nanocurrency-protocol-0.4.2 (c (n "nanocurrency-protocol") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1525hdgw790b9g97kic9nsvbbvvpx3i7r6r33afgdpw0a7545iln")))

(define-public crate-nanocurrency-protocol-0.5.0 (c (n "nanocurrency-protocol") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0fb4zwwwbz3r52flvdwr9xcjb256g7maxrxbzcn13b4gcyfy3w99")))

(define-public crate-nanocurrency-protocol-0.5.1 (c (n "nanocurrency-protocol") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.3.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1lhpr59nivqrpq6v6k0ih8my59dkb5dp33czyxc02xkl4gqzl29l")))

(define-public crate-nanocurrency-protocol-0.6.0 (c (n "nanocurrency-protocol") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.3") (d #t) (k 0)) (d (n "tokio-codec") (r "^0.2.0-alpha.5") (d #t) (k 0)))) (h "11ljn0j0yxn3wivn92qsm5kv58v7idxsfpxmkmphlk8dpk740ydp")))

(define-public crate-nanocurrency-protocol-0.7.0 (c (n "nanocurrency-protocol") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "nanocurrency-types") (r "^0.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.3.1") (f (quote ("codec"))) (d #t) (k 0)))) (h "0gxqzpkfgzsrxn7ldk0dj03x7mmcgmppcm30ry4przwb8ryskkq5")))

