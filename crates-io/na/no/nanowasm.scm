(define-module (crates-io na no nanowasm) #:use-module (crates-io))

(define-public crate-nanowasm-0.0.1 (c (n "nanowasm") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.20") (d #t) (k 0)))) (h "0hxl1yd0ap4rd9mqiclb8aijgp5j48idzz9fq78ihvw8xkq3f154")))

(define-public crate-nanowasm-0.0.2 (c (n "nanowasm") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "wabt") (r "^0.2") (d #t) (k 0)))) (h "17pzvd1aj9sxfk1l23rmfgzrn6p7amy9y41flm2s8cxh6xb85qnq")))

