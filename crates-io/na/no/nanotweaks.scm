(define-module (crates-io na no nanotweaks) #:use-module (crates-io))

(define-public crate-nanotweaks-0.1.0 (c (n "nanotweaks") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "10f39vs5ga5mj4033wz1ga29idc2vm6h3idzjhllb7bpi6xyirs7")))

(define-public crate-nanotweaks-0.1.1 (c (n "nanotweaks") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "108rmpf6yrrjp5qzqy18h4n8h0yw3py8v5k5w52j32rk9a1b5bsn")))

(define-public crate-nanotweaks-0.2.0 (c (n "nanotweaks") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "similar-asserts") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "083w68p37gdqhxj7afqgk6q250j3vkk0h5qzgr2l60j5r7y3hx4w")))

(define-public crate-nanotweaks-0.3.0 (c (n "nanotweaks") (v "0.3.0") (d (list (d (n "nanotweaks-proc") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "00r16018isl5h9zlchsazwrddaxq13hcrgnjxi00mld40ihq9h8k") (f (quote (("serde_derive" "nanotweaks-proc/serde_derive"))))))

(define-public crate-nanotweaks-0.4.0+moved (c (n "nanotweaks") (v "0.4.0+moved") (h "0627j1abfv2nwprsb88qwm1nhib8i8a321za0ky6y3v7cya09bd0")))

(define-public crate-nanotweaks-0.4.1+moved (c (n "nanotweaks") (v "0.4.1+moved") (h "16a6gffak55whsbkczkygg06h29qw3gfl4arnr9b2ddciqqflag8")))

