(define-module (crates-io na no nanomsg) #:use-module (crates-io))

(define-public crate-nanomsg-0.2.0 (c (n "nanomsg") (v "0.2.0") (h "1x8p2yf2mrkxhzhlns9x79yx87wjj2hidpn1fg14akajbdx0yi8v")))

(define-public crate-nanomsg-0.3.0 (c (n "nanomsg") (v "0.3.0") (d (list (d (n "libnanomsg") (r "^0.1.0") (d #t) (k 0)))) (h "1paz9i9q3k7v8hfyypx9knxxfrhsifndw5jk66im2wr0s2n52w5q")))

(define-public crate-nanomsg-0.3.1 (c (n "nanomsg") (v "0.3.1") (d (list (d (n "nanomsg-sys") (r "^0.1.1") (d #t) (k 0)))) (h "179pn0nj6sjdd59gwflldfdrpjjimkgmilldwix35hhmvdz85c6r")))

(define-public crate-nanomsg-0.3.2 (c (n "nanomsg") (v "0.3.2") (d (list (d (n "nanomsg-sys") (r "^0.1.2") (d #t) (k 0)))) (h "11qa2g2y3jasjk5rpfqq3g1apk3bk459xvfrj0p5k3l12hvwzc2d")))

(define-public crate-nanomsg-0.3.3 (c (n "nanomsg") (v "0.3.3") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1aqsafry9hxvazn6i9ln0ynqdfn688h5bzjgr3ic7z9a1f7z1ias")))

(define-public crate-nanomsg-0.3.4 (c (n "nanomsg") (v "0.3.4") (d (list (d (n "libc") (r "^0.1.6") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0qghfd489gyqgb2d0c51pkp2yzx3gkj90rnzspiz9a65lypd7073")))

(define-public crate-nanomsg-0.4.0 (c (n "nanomsg") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.2.0") (d #t) (k 0)))) (h "011d6spd5yyd5amll6b140ad47876wn4jpziqi1f2b7scrwrwbm0")))

(define-public crate-nanomsg-0.4.1 (c (n "nanomsg") (v "0.4.1") (d (list (d (n "libc") (r "^0.1.12") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1rj8f65v7fg46pd6fsx2g7a8g820cnsplbicgxjn450icbk3b701")))

(define-public crate-nanomsg-0.4.2 (c (n "nanomsg") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0464ykbk1pzyxlp6yf15a1si9844y64irsmwch5k2f73jg13marc")))

(define-public crate-nanomsg-0.5.0 (c (n "nanomsg") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1047shdq5bhmmlpxcrr8vl08r6iad8nnbjzgkwmzci6f4rlfrpvc")))

(define-public crate-nanomsg-0.6.0 (c (n "nanomsg") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1s146yz4ns46syr38lk4qzhhnr4kwi6hyxq5jgddpk7fwllwgb4j")))

(define-public crate-nanomsg-0.6.1 (c (n "nanomsg") (v "0.6.1") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1k4llm2ygxms85w91b7zgbwjzdh9fa2m10pjmzhkr15vzvd5lv64") (f (quote (("bundled" "nanomsg-sys/bundled"))))))

(define-public crate-nanomsg-0.6.2 (c (n "nanomsg") (v "0.6.2") (d (list (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.6.2") (d #t) (k 0)))) (h "04vp2hs72zwy4kwh7bmff6sbg3q5z5vnh18m3ar0gzjj2gjzkk3n") (f (quote (("no_anl" "nanomsg-sys/no_anl") ("bundled" "nanomsg-sys/bundled"))))))

(define-public crate-nanomsg-0.7.0 (c (n "nanomsg") (v "0.7.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0splg442wg2jqazg4pmdb1w1643g8vsnss0ckqqsyls0zh8j6z0w") (f (quote (("no_anl" "nanomsg-sys/no_anl") ("bundled" "nanomsg-sys/bundled"))))))

(define-public crate-nanomsg-0.7.1 (c (n "nanomsg") (v "0.7.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "nanomsg-sys") (r "^0.7.1") (d #t) (k 0)))) (h "0phlm1rx8s1lvvxaf77zk3393z2rid89y22dr5l6p4rrcfqk760b") (f (quote (("no_anl" "nanomsg-sys/no_anl") ("bundled" "nanomsg-sys/bundled"))))))

(define-public crate-nanomsg-0.7.2 (c (n "nanomsg") (v "0.7.2") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "nanomsg-sys") (r "0.7.*") (d #t) (k 0)))) (h "1b6k7dgplwffngmj706hd0whwjm7nkrpkg7pgmkgh8m5zdh02zk1") (f (quote (("no_anl" "nanomsg-sys/no_anl") ("bundled" "nanomsg-sys/bundled"))))))

