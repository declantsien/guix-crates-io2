(define-module (crates-io na no nano-rs-core) #:use-module (crates-io))

(define-public crate-nano-rs-core-0.1.0 (c (n "nano-rs-core") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter" "time"))) (d #t) (k 0)))) (h "1a7238cipajjr5xj4hr9f5gcjs0gn62k2yspsmw19qqavhd24wz7") (r "1.66")))

