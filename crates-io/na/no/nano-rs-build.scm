(define-module (crates-io na no nano-rs-build) #:use-module (crates-io))

(define-public crate-nano-rs-build-0.1.0 (c (n "nano-rs-build") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1y72dd0cxwn75svf2dhgsc3inc6b6xccnpjy938vml4b71g5jiza") (r "1.66")))

(define-public crate-nano-rs-build-0.1.1 (c (n "nano-rs-build") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0wkd061bqkhczx70s9d4gp1828m8f1vl61y7idinqmkwy0jhjwzp") (r "1.66")))

