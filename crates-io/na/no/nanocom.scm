(define-module (crates-io na no nanocom) #:use-module (crates-io))

(define-public crate-nanocom-0.1.0 (c (n "nanocom") (v "0.1.0") (h "0k9xk5y3dbnqh99vh6ai0m3n0fhl1lj9fwzkpsxziwn8amsy7psy")))

(define-public crate-nanocom-0.1.1 (c (n "nanocom") (v "0.1.1") (h "0jyh2a6sy2f6azyj593yhrj7mxv4fp5plf1dxpi582x1pxgjv19w")))

(define-public crate-nanocom-0.1.2 (c (n "nanocom") (v "0.1.2") (h "1dm4qjvkgb8w8msr5dmagx0ld7vd9jiiyfvr0xkyfimwr6n06hzr")))

(define-public crate-nanocom-0.2.0 (c (n "nanocom") (v "0.2.0") (h "0c1glld7jg7cc7gkbr7dgxbhq05rb5cnj4vnbgf68awcbf5whl3f")))

(define-public crate-nanocom-0.2.1 (c (n "nanocom") (v "0.2.1") (h "03szgxki5l45gkvzxvnvif775hsvnz6v8g58ag5sandsb5n9ra5p")))

