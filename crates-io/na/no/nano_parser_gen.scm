(define-module (crates-io na no nano_parser_gen) #:use-module (crates-io))

(define-public crate-nano_parser_gen-0.2.0 (c (n "nano_parser_gen") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0db4nazv3g747zjcpz9scmg6gh5qxj94d3i2lwjn8d9k2qi7g2la")))

(define-public crate-nano_parser_gen-0.2.1 (c (n "nano_parser_gen") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1r8m2fdfbnw2p8nsc7ma2cs2vp5112zgag53hj8hr3bc7v84i9y7")))

