(define-module (crates-io na no nanoem) #:use-module (crates-io))

(define-public crate-nanoem-0.1.0 (c (n "nanoem") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)))) (h "09nixzhrkgi2z377s2id80g9lnj9sfj4i6kvwnjxgl2kizxa7sgz")))

(define-public crate-nanoem-0.1.1 (c (n "nanoem") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)))) (h "1x7kb2yk30fvq6xzb8d18fz681x12j414hxshanbrcxwxq5l8pbq")))

(define-public crate-nanoem-0.1.2 (c (n "nanoem") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)))) (h "13v3q8npj79szd48zl2rv631ps0xhbgr59qkr9pqraz74y46vlkl")))

