(define-module (crates-io na no nanohttp) #:use-module (crates-io))

(define-public crate-nanohttp-0.1.0 (c (n "nanohttp") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1c438imy8gjvk8a1x4s4sxdg703bj41fbv7ws8wg5fglby7gv175")))

(define-public crate-nanohttp-0.2.0 (c (n "nanohttp") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)))) (h "1khs9fi06frpq1h4z4jzrndagbbmhzav8x1r3vnyr52vzn6v71x0")))

