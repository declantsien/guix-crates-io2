(define-module (crates-io na no nanohat-oled) #:use-module (crates-io))

(define-public crate-nanohat-oled-0.1.0 (c (n "nanohat-oled") (v "0.1.0") (d (list (d (n "i2c-linux") (r "^0.1") (d #t) (k 0)))) (h "03ws89bgi235z8xk4jh2d48vrdn5r38nad159n8rrfwlihwsvl79")))

(define-public crate-nanohat-oled-0.2.0 (c (n "nanohat-oled") (v "0.2.0") (d (list (d (n "i2c-linux") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qsqbnzp11l6nkwqbf6w8z9xx08j6ymlr9na4gx0q5c6mfm6c09n")))

