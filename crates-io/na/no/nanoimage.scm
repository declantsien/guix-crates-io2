(define-module (crates-io na no nanoimage) #:use-module (crates-io))

(define-public crate-nanoimage-0.1.0 (c (n "nanoimage") (v "0.1.0") (h "02grx4fszfpp7cdzj7w7yk957c28gyx16k5glicnbi7wgnxsm2sq")))

(define-public crate-nanoimage-0.1.1 (c (n "nanoimage") (v "0.1.1") (h "0ilfp6hv9fszrjzr8c0chnj2sys62hbc3xwdhyc2y32ymbph8lw3")))

(define-public crate-nanoimage-0.1.2 (c (n "nanoimage") (v "0.1.2") (h "076c4p5n327rapba9k0n69ynf01zf1nkkc31v8hg8vc3ams04jcy")))

(define-public crate-nanoimage-0.1.3 (c (n "nanoimage") (v "0.1.3") (h "0wjwkq3va3wrnkf1iaylsxwdrpd01dr0sp64b21mksqgsw4yyk8y")))

