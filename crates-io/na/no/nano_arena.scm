(define-module (crates-io na no nano_arena) #:use-module (crates-io))

(define-public crate-nano_arena-0.1.0 (c (n "nano_arena") (v "0.1.0") (h "1m902b3fms2pfg9z8fx8xdyw5g0xh527b8hn3l741h11ygki7863")))

(define-public crate-nano_arena-0.1.1 (c (n "nano_arena") (v "0.1.1") (h "1iydj9g91vsjcc6kc67i8412i3av0yh5vxx9gdk871v8cxhmwav3")))

(define-public crate-nano_arena-0.2.0 (c (n "nano_arena") (v "0.2.0") (h "1jq80dxrm0yvqbbbbwyhhn2vapn7w6936askab8rkwkh36r7wgf8")))

(define-public crate-nano_arena-0.3.0 (c (n "nano_arena") (v "0.3.0") (h "19ap4rk09w1p2vvl2qyh4fcarb7dli29qm1817k257nqqjq9crih")))

(define-public crate-nano_arena-0.4.0 (c (n "nano_arena") (v "0.4.0") (h "1ffqkwvjy9fxc0i73vaalla3as3c71djxklxk15jx5m6sq3g9a61")))

(define-public crate-nano_arena-0.5.0 (c (n "nano_arena") (v "0.5.0") (h "1g003hdyrwiid4p2fcvdvdaapfx9cxk7561jyz3yg4zw6vmsw70w")))

(define-public crate-nano_arena-0.5.1 (c (n "nano_arena") (v "0.5.1") (h "1nc743qci4v0h77c0pvh55s78vr3gyjhbnqz7dizxqs67inpxmdg")))

(define-public crate-nano_arena-0.5.2 (c (n "nano_arena") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)))) (h "170w82rl1qarc5azik77vhhvqd4i0h9gpbzfa2ywp0nidabqqrar")))

