(define-module (crates-io na no nanos_sdk) #:use-module (crates-io))

(define-public crate-nanos_sdk-0.2.1 (c (n "nanos_sdk") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "rand_core") (r "^0.6.3") (k 0)))) (h "18k9is5fy2z9yrv8sv67s4na3dncahj7c5sazlfdnxwa03jqwv73") (f (quote (("speculos") ("pre1_54") ("pending_review_screen") ("lib_bagl") ("ccid")))) (y #t)))

