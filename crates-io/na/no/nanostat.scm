(define-module (crates-io na no nanostat) #:use-module (crates-io))

(define-public crate-nanostat-0.1.0 (c (n "nanostat") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argh") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "plotlib") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.14") (d #t) (k 0)))) (h "0kdb9mvzpfw5x2bdgv67z25zh55valg1c4lxxwwjq9xnqirhnm9x") (f (quote (("default") ("cli" "argh" "plotlib"))))))

(define-public crate-nanostat-0.2.0 (c (n "nanostat") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "plotlib") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.15") (d #t) (k 0)))) (h "157nhw4356kk9x632lda12a5kcl1yqw29bs6glqsahl1830id8wm") (f (quote (("default") ("cli" "clap" "plotlib"))))))

