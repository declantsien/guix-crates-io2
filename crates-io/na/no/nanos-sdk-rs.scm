(define-module (crates-io na no nanos-sdk-rs) #:use-module (crates-io))

(define-public crate-nanos-sdk-rs-0.1.0 (c (n "nanos-sdk-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "0dh0k9232pv6a21xja94ki8q88v95ygv4qrv67h8prxxbrs4zk5v")))

(define-public crate-nanos-sdk-rs-0.2.0 (c (n "nanos-sdk-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1akbh1hq287da5qf07ldzzv88g94y9cvv0an3n4lzsbywv1mv7a9")))

