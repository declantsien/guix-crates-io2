(define-module (crates-io na no nanogrep) #:use-module (crates-io))

(define-public crate-nanogrep-1.0.0 (c (n "nanogrep") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1l8y8xgjjlk7bpl8b3pm8pxbcicy9vdfv1wksivnf1ibw2fzr4nj")))

(define-public crate-nanogrep-1.0.1 (c (n "nanogrep") (v "1.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qq7zh3qgd5zdfmgw3vsmhiqglxf84aaaixq2g1lsp47hi9y5zri")))

(define-public crate-nanogrep-1.0.2 (c (n "nanogrep") (v "1.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0yy46y8bpfz1hyck5nas2f3wfylv2aqcarrn2jgq382b6q6q0aik")))

(define-public crate-nanogrep-1.0.3 (c (n "nanogrep") (v "1.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1223iz7j1a01zj03drcp6z32w5yniagcnjkyyjih900286ix9sab")))

(define-public crate-nanogrep-1.0.5 (c (n "nanogrep") (v "1.0.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0asd76hhdczznsfwzan121vfchknlqw29fbaqmkz3a910nv45sck")))

(define-public crate-nanogrep-1.0.6 (c (n "nanogrep") (v "1.0.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0np5zh7b8zpnw1xw1g0kf0shsp9xsf655ak64hqa6b2mckdic8gg")))

(define-public crate-nanogrep-1.0.7 (c (n "nanogrep") (v "1.0.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0d58fga1sigv072vrzccl77l71ckg343lij28gh3l9xbfcg2w5jw")))

(define-public crate-nanogrep-1.0.8 (c (n "nanogrep") (v "1.0.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1644sfyvw7nk61ii7fn3vsygh37mv92aq1q4w5jasmc02kzvsfc1")))

(define-public crate-nanogrep-1.0.9 (c (n "nanogrep") (v "1.0.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1fk7p6qm7jccjf8mqwlz1n2y8zzx94ccj1q82prk90jfd1kc4ivx")))

(define-public crate-nanogrep-1.0.10 (c (n "nanogrep") (v "1.0.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gj75iy63ig0xv10ds4fcwxsrvilfxx76yv8vq9jp5qd3bsf98vh")))

(define-public crate-nanogrep-1.1.0 (c (n "nanogrep") (v "1.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "15wj99wqb0pf0xb0h5gzib2xwrnxyj88yzjz993vwj6jiz9041cp")))

(define-public crate-nanogrep-1.2.0 (c (n "nanogrep") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0v7f010h5cigc6b38cn804z0hvkmy31rv39wywdamyj3am9wmajd")))

(define-public crate-nanogrep-1.2.1 (c (n "nanogrep") (v "1.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0lyljnm67jxn3yx7f2kr3svn8n9zid0cq621s6f4hmdvxk31vs64")))

(define-public crate-nanogrep-2.0.0 (c (n "nanogrep") (v "2.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0lawflgb5k2ll56qnj9irxbx8p0vns146vv1kvljqvrk0l574fbs")))

(define-public crate-nanogrep-2.1.0 (c (n "nanogrep") (v "2.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1j0bn4ynlm6hf2szc8c8nv9fnda4d6axzv832j2w0xbpi29jxz03")))

(define-public crate-nanogrep-2.2.0 (c (n "nanogrep") (v "2.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0939mbibk3kksflzf1ldk03ahbgzcn8b7mcahk8ywjghff18ws2a")))

(define-public crate-nanogrep-2.2.1 (c (n "nanogrep") (v "2.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0wxcis4pixdpv0dfrvdff83hi8bv73q47dlpw9f4qvw61s4awffl")))

