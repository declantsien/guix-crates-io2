(define-module (crates-io na no nanovg) #:use-module (crates-io))

(define-public crate-nanovg-0.2.0 (c (n "nanovg") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)))) (h "1szgmqiajvwdvz311fvlcarq1bm63rnv7gmd0blmhg4lvjdhswpb")))

(define-public crate-nanovg-0.3.0 (c (n "nanovg") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.2") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "08c4sv5ln60drrq64mimrqwq95m6wdnp5w9lwz9jif3mik0lma6b") (f (quote (("gles3") ("gles2") ("gl3") ("gl2"))))))

(define-public crate-nanovg-0.4.0 (c (n "nanovg") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "09kg38hsrmfrcdnpy4jxl2ljj9pixca0l6shk8cff6nvil9jxn4w") (f (quote (("gles3") ("gles2") ("gl3") ("gl2") ("default" "gl3"))))))

(define-public crate-nanovg-0.4.1 (c (n "nanovg") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0h8xy7rwg888d49xrjwfjv1cw0irwnvnndlg3l0x74xg60ryjylg") (f (quote (("gles3") ("gles2") ("gl3") ("gl2") ("default" "gl3"))))))

(define-public crate-nanovg-0.4.2 (c (n "nanovg") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1v3f6xbc8d5zpmy1ks2mkfmmsd8wgr046j4nka0idabkg593b6cr") (f (quote (("gles3" "nanovg-sys/gles3") ("gles2" "nanovg-sys/gles2") ("gl3" "nanovg-sys/gl3") ("gl2" "nanovg-sys/gl2") ("default" "gl3"))))))

(define-public crate-nanovg-1.0.0 (c (n "nanovg") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1gc2gwvzafcd1yr1c31wamx62q5i3jp7cdhnp0ks0ap8l43zmwhs") (f (quote (("gles3" "nanovg-sys/gles3") ("gles2" "nanovg-sys/gles2") ("gl3" "nanovg-sys/gl3") ("gl2" "nanovg-sys/gl2")))) (y #t)))

(define-public crate-nanovg-1.0.1 (c (n "nanovg") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0q6ah5cq6kqiggnfpbnvcl35bygi97mv8s3hh0hngfjb0grnnzl4") (f (quote (("gles3" "nanovg-sys/gles3") ("gles2" "nanovg-sys/gles2") ("gl3" "nanovg-sys/gl3") ("gl2" "nanovg-sys/gl2")))) (y #t)))

(define-public crate-nanovg-1.0.2 (c (n "nanovg") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "gl") (r "^0.10.0") (d #t) (k 2)) (d (n "glutin") (r "^0.13.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "nanovg-sys") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1m05i6qm6w3zy6va6p5jhv6danp64isaqq21c4v3zjrij3dfdngi") (f (quote (("gles3" "nanovg-sys/gles3") ("gles2" "nanovg-sys/gles2") ("gl3" "nanovg-sys/gl3") ("gl2" "nanovg-sys/gl2"))))))

