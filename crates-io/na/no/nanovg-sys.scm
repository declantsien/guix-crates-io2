(define-module (crates-io na no nanovg-sys) #:use-module (crates-io))

(define-public crate-nanovg-sys-0.1.0 (c (n "nanovg-sys") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zr5f6lwl09n49hn7lcz8w6k5sfbvx0r2552pnrv56pyslxp9lnq") (f (quote (("gles3") ("gles2") ("gl3") ("gl2") ("default" "gl3"))))))

(define-public crate-nanovg-sys-0.1.1 (c (n "nanovg-sys") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "08mbj3vl036gns948fjpq48zbr2y18s4lrgjcvpy5gzvvf7s0m39") (f (quote (("gles3") ("gles2") ("gl3") ("gl2") ("default" "gl3"))))))

(define-public crate-nanovg-sys-1.0.0 (c (n "nanovg-sys") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1brrqn82z4xnagns3sgrriicqwlsg4p0v10rw7669lsyb9265lwv") (f (quote (("gles3") ("gles2") ("gl3") ("gl2")))) (y #t) (l "nanovg")))

(define-public crate-nanovg-sys-1.0.1 (c (n "nanovg-sys") (v "1.0.1") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01hv0aarrpbj8lzkfh16s9bnz8ji1ryykavw0ifalx3gg6dmv2ww") (f (quote (("gles3") ("gles2") ("gl3") ("gl2")))) (y #t) (l "nanovg")))

(define-public crate-nanovg-sys-1.0.2 (c (n "nanovg-sys") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vnpkaw64963ky8xnkvnkkqk98yxjmlyq1dh52vmcfcgwrr0k4xj") (f (quote (("gles3") ("gles2") ("gl3") ("gl2")))) (l "nanovg")))

