(define-module (crates-io na no nano-gemm-f32) #:use-module (crates-io))

(define-public crate-nano-gemm-f32-0.1.0 (c (n "nano-gemm-f32") (v "0.1.0") (d (list (d (n "nano-gemm-codegen") (r "^0.1") (d #t) (k 1)) (d (n "nano-gemm-core") (r "^0.1") (d #t) (k 0)))) (h "1fx88r2zcfqf0zni2w9w6yff41d0c0ngddwlv9wqyr9mrsvq2djf") (f (quote (("nightly") ("default"))))))

