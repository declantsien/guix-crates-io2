(define-module (crates-io na no nanoid-dictionary) #:use-module (crates-io))

(define-public crate-nanoid-dictionary-0.1.0 (c (n "nanoid-dictionary") (v "0.1.0") (h "1xzd7hp02569yimgbkaj4495njds4by0vfs05jab1fjr9fsj99xs")))

(define-public crate-nanoid-dictionary-0.1.1 (c (n "nanoid-dictionary") (v "0.1.1") (h "09k4kfg9j1pch05c9prbfcm7173jpc9f0x4r41l5yl5lmh3mg8wx")))

(define-public crate-nanoid-dictionary-0.4.1 (c (n "nanoid-dictionary") (v "0.4.1") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 2)))) (h "09f70dxn46d9i8v73ixyydim2paq5p28m46y8ff97dp0rff97vi4")))

(define-public crate-nanoid-dictionary-0.4.2 (c (n "nanoid-dictionary") (v "0.4.2") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 2)))) (h "1k6ml002zfyb03v6f86qf2gkgbhwwb5nnwjwbyszlv98d3bc9q0d")))

(define-public crate-nanoid-dictionary-0.4.3 (c (n "nanoid-dictionary") (v "0.4.3") (d (list (d (n "nanoid") (r "^0.4") (d #t) (k 2)))) (h "13jxbmchdiafcrngnz2bqlr97k0z7lrgfmf2f5i85xhqr0qr2viw")))

