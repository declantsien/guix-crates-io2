(define-module (crates-io na no nano-http) #:use-module (crates-io))

(define-public crate-nano-http-0.1.0 (c (n "nano-http") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "0qsiin51w87q4hc5yxh8xxsr085y65zhw0w389hg5fyqc7pzkdgx")))

(define-public crate-nano-http-0.1.1 (c (n "nano-http") (v "0.1.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "0zschyq1kscwrxclazmls1li8h874sf62498y0lxgg79r9sahfnh")))

(define-public crate-nano-http-0.1.2 (c (n "nano-http") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "04ij3p4bq321hanaav75j7qxgza9a84wvl67wk3phlx2w3755rx7")))

(define-public crate-nano-http-0.1.3 (c (n "nano-http") (v "0.1.3") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "18i2m9jhq8584q8mvr7dyqcaybm689n8gpj7psdkd0cqhyd0qkk2")))

(define-public crate-nano-http-0.1.4 (c (n "nano-http") (v "0.1.4") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "0ypg8wf0zqja9nv3g89aifg89i53g9xljmldks4yrv90dmqrhsww")))

(define-public crate-nano-http-0.1.5 (c (n "nano-http") (v "0.1.5") (d (list (d (n "async-std") (r "^1.6.2") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "femme") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tide") (r "^0.11.0") (d #t) (k 0)))) (h "11wzwfq5iasgispdq40g9b8dl53a6dwzdclz5irn7vb36irsyx20")))

