(define-module (crates-io na no nanokit) #:use-module (crates-io))

(define-public crate-nanokit-0.1.0 (c (n "nanokit") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pprof") (r "^0.12") (f (quote ("flamegraph" "criterion"))) (d #t) (t "cfg(unix)") (k 2)))) (h "17srcr6hqq9iq2j5x2657a6laviv5489ys033lnz0y2wrwj7wncw") (f (quote (("std") ("no-inline-concat") ("default" "std") ("c-exports"))))))

