(define-module (crates-io na no nanoserde-derive) #:use-module (crates-io))

(define-public crate-nanoserde-derive-0.1.0 (c (n "nanoserde-derive") (v "0.1.0") (h "1xrpl3bfjzd1vh3yzzspxpsabw0bz6dwzhmrax4izgnm6pny2xsg")))

(define-public crate-nanoserde-derive-0.1.1 (c (n "nanoserde-derive") (v "0.1.1") (h "0q96fwr6npgws3vjj69q73b3p53gxp7dacbda9175wnramp84z2c")))

(define-public crate-nanoserde-derive-0.1.2 (c (n "nanoserde-derive") (v "0.1.2") (h "0q72h7jlz4sqzhn5j9vd046j4axr3xf4xl7sycmxni9b5kqafijj")))

(define-public crate-nanoserde-derive-0.1.3 (c (n "nanoserde-derive") (v "0.1.3") (h "1vb9l3g274b7wywnmll8ghpd9mngnr1nvbfhkazmxydd7s42k1fb")))

(define-public crate-nanoserde-derive-0.1.4 (c (n "nanoserde-derive") (v "0.1.4") (h "17875jj6wbsl11ra37prqggjb7az474njdj0l9v0gjy9ssv1a4cw")))

(define-public crate-nanoserde-derive-0.1.5 (c (n "nanoserde-derive") (v "0.1.5") (h "0bh6m33a7i169zlmjgbjncq7ingpc56qpgpq2j4r4612d5wylaif")))

(define-public crate-nanoserde-derive-0.1.6 (c (n "nanoserde-derive") (v "0.1.6") (h "0r5h9c4h8h30r6jmiwwvfdigcidjdyjgndbvgnyawldklzqm3vly")))

(define-public crate-nanoserde-derive-0.1.7 (c (n "nanoserde-derive") (v "0.1.7") (h "07n3x5kxg2zabk1fzm836pgp0mcgb0sk6fxzbnrk8s3d04vv49ky")))

(define-public crate-nanoserde-derive-0.1.8 (c (n "nanoserde-derive") (v "0.1.8") (h "09kk0xhln5gb0b7s72346v9arrhswmq3d7xngfyb7jbxnqpxvlr1")))

(define-public crate-nanoserde-derive-0.1.9 (c (n "nanoserde-derive") (v "0.1.9") (h "05a0swrljlgi2b1nxjsqcq96ix6hladgj0bngf93ilw09ynwkdjr")))

(define-public crate-nanoserde-derive-0.1.10 (c (n "nanoserde-derive") (v "0.1.10") (h "1wwrvrl853z6cb76057hvjcicxpbv3r9l1h0f4piiwxnpcnz3rrl")))

(define-public crate-nanoserde-derive-0.1.11 (c (n "nanoserde-derive") (v "0.1.11") (h "0iyxhlinl9pxqf7r345912ffq8gnsx6ik7ah525wzd3wlpyrk84h")))

(define-public crate-nanoserde-derive-0.1.12 (c (n "nanoserde-derive") (v "0.1.12") (h "1cf3068c89m9i69kh9iznpqv8fgvfllvb1d94gfd2mdm9xvl438f")))

(define-public crate-nanoserde-derive-0.1.13 (c (n "nanoserde-derive") (v "0.1.13") (h "0jqrlwipmffh2gzmfrbp6z7bx5vy7z1irqh4b1g56kyfs6yd6r6b")))

(define-public crate-nanoserde-derive-0.1.14 (c (n "nanoserde-derive") (v "0.1.14") (h "0s6x8509zbfp4nsm35jx8bsi00lij3kcbrbyn073rf4bg219ij0l")))

(define-public crate-nanoserde-derive-0.1.15 (c (n "nanoserde-derive") (v "0.1.15") (h "1bk9pmsjyhj9l6dzln5mn5wi8lmfb59s21c47jhpzcpv4xb1x7sm")))

(define-public crate-nanoserde-derive-0.1.16 (c (n "nanoserde-derive") (v "0.1.16") (h "03zjfhnbllk8hpbchgbadipbny76rx7a79acapf3p95j1ca9rhki")))

(define-public crate-nanoserde-derive-0.1.17 (c (n "nanoserde-derive") (v "0.1.17") (h "12kkjmkfvq4p1n94w52r536x2088pxnmvfhpxf3xjd8kfjabjaxi")))

(define-public crate-nanoserde-derive-0.1.18 (c (n "nanoserde-derive") (v "0.1.18") (h "0fgvi19nrk2c3ij1gz4ihz7ss8b4mpw9brm0bhgyz2nn35kyq3i9")))

(define-public crate-nanoserde-derive-0.1.19 (c (n "nanoserde-derive") (v "0.1.19") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1ayn0has82v5fxzh5f7777bsblwnmi1iriiz0ifw70b1dkd98ypd") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-derive-0.1.20 (c (n "nanoserde-derive") (v "0.1.20") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "024y87nb4wz7brznwxp60qs0my1ihqdz99hzyw6wcwx715d3rdc6") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-derive-0.1.21 (c (n "nanoserde-derive") (v "0.1.21") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "0nchd8h1xj03j83plki5larckhcwmyarzkzxfaajg9372xa9dp6l") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-derive-0.1.22 (c (n "nanoserde-derive") (v "0.1.22") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "011211f0flikd4sg4mylk9ivfhf7rn3hcl3qcvnf7drp2g1b4hz9") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

