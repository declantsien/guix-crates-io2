(define-module (crates-io na no nanotweaks-proc) #:use-module (crates-io))

(define-public crate-nanotweaks-proc-0.3.0 (c (n "nanotweaks-proc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "1m3spw681dqmidy0wpgzwqpdv5j00jqdd981n1c3y9im2s30wbs4") (f (quote (("serde_derive"))))))

(define-public crate-nanotweaks-proc-0.4.1+moved (c (n "nanotweaks-proc") (v "0.4.1+moved") (h "1dky0zwyarn78b56nsham34ik4mig1x01cgj8msjxyfv5n2h6wl7")))

