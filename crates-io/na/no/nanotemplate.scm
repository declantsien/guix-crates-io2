(define-module (crates-io na no nanotemplate) #:use-module (crates-io))

(define-public crate-nanotemplate-0.1.0 (c (n "nanotemplate") (v "0.1.0") (h "0va2cwcis8rgx5bmzdkmfvy5azh0ss27yq011igy3yn1pp7lpm6g")))

(define-public crate-nanotemplate-0.2.0 (c (n "nanotemplate") (v "0.2.0") (h "0q61cnsgmdiijy8vp1ma3160r8nn5ss3y941w3882f9w4mwnima0")))

(define-public crate-nanotemplate-0.2.1 (c (n "nanotemplate") (v "0.2.1") (h "0qfxkr8y10hy44n6fj2rkk8mc5wzssqfws83hqwbvjls18gi9ppw")))

(define-public crate-nanotemplate-0.3.0 (c (n "nanotemplate") (v "0.3.0") (h "06w3n9yfhw1ny3iq7ks7v9nlyhmpvzcyxd8yic6lx87qjklbyizj")))

