(define-module (crates-io na no nanoid_cli) #:use-module (crates-io))

(define-public crate-nanoid_cli-1.0.0 (c (n "nanoid_cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "easy-error") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nanoid") (r "^0.4.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "00zm80gl7g9nh1xs4igpnfcnxlzd35halakdilgc58281khi4hw6")))

