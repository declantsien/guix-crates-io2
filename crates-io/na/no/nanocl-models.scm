(define-module (crates-io na no nanocl-models) #:use-module (crates-io))

(define-public crate-nanocl-models-0.0.1 (c (n "nanocl-models") (v "0.0.1") (d (list (d (n "bollard") (r "^0.14") (d #t) (k 0)) (d (n "diesel") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "diesel-derive-enum") (r "^2.0.0-rc.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (d #t) (k 0)))) (h "0khk8clkdqknvb1jd8glzzshnhj3v46y7yxjiq777gdbakyihs5g") (y #t) (s 2) (e (quote (("serde" "dep:serde" "uuid/serde") ("diesel" "dep:diesel" "dep:diesel-derive-enum"))))))

