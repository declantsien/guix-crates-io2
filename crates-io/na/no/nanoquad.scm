(define-module (crates-io na no nanoquad) #:use-module (crates-io))

(define-public crate-nanoquad-0.1.2 (c (n "nanoquad") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.11") (d #t) (k 0)) (d (n "nona") (r "^0.1.2") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "054v8r730fb1mj5bn6pj6z0zyqyjya9kndv2iypa48034diqcgx2") (y #t)))

(define-public crate-nanoquad-0.1.3 (c (n "nanoquad") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.11") (d #t) (k 0)) (d (n "nona") (r "^0.1.2") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "038gxqsq1a0qavr0yamlzq94zd326j387b4z1fxi9i9vh037p5pg") (y #t)))

(define-public crate-nanoquad-0.1.4 (c (n "nanoquad") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "miniquad") (r "^0.3.11") (d #t) (k 0)) (d (n "nona") (r "^0.1.2") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0i5f2x5f92cjg34plqa1dfr5xndnnf39rqnhy2jcvcih6zy9njn3") (y #t)))

(define-public crate-nanoquad-0.1.5 (c (n "nanoquad") (v "0.1.5") (h "0a4710w6g44jvw77gis1ggmmmb64znximgii53gj8y4xni9lb20l") (y #t)))

(define-public crate-nanoquad-0.1.6 (c (n "nanoquad") (v "0.1.6") (h "1fp8j5gr3v0lpkby8ryljra5d3w0i2rbcfbbnwi1gasx9394zsvp")))

