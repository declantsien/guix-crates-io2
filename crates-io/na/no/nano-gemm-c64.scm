(define-module (crates-io na no nano-gemm-c64) #:use-module (crates-io))

(define-public crate-nano-gemm-c64-0.1.0 (c (n "nano-gemm-c64") (v "0.1.0") (d (list (d (n "nano-gemm-codegen") (r "^0.1") (d #t) (k 1)) (d (n "nano-gemm-core") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (k 0)))) (h "1gxk4mfg51ffhkk28ny9yj3dl1s13rsid5h0s62vm3rm25i6wfkl") (f (quote (("nightly") ("default"))))))

