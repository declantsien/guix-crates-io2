(define-module (crates-io na no nano-ogmo) #:use-module (crates-io))

(define-public crate-nano-ogmo-0.1.0 (c (n "nano-ogmo") (v "0.1.0") (d (list (d (n "nanoserde") (r "^0.1.20") (d #t) (k 0)))) (h "0vlxr6gnysv1rg0kdck80hz25yb5y9blkmw2rikavjm303r52cby")))

(define-public crate-nano-ogmo-0.1.1 (c (n "nano-ogmo") (v "0.1.1") (d (list (d (n "nanoserde") (r "^0.1.25") (d #t) (k 0)))) (h "1alc7cg9fa4mhqpcy9ip6fmilbs85mckcx44p9ks2y4w2vbzhfhb")))

(define-public crate-nano-ogmo-0.1.2 (c (n "nano-ogmo") (v "0.1.2") (d (list (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)))) (h "17fhsg7si7y3nd61lm6qyhgzgx7dp75w7p9kmckwrkkx6viqgp7j")))

(define-public crate-nano-ogmo-0.1.3 (c (n "nano-ogmo") (v "0.1.3") (d (list (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)))) (h "0p7xa9nzl1yddwkh86qlgfnzpp5g4v6bv1m0p9x1sbgh0zril1f9")))

(define-public crate-nano-ogmo-0.1.4 (c (n "nano-ogmo") (v "0.1.4") (d (list (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)))) (h "0lg7hgb9qh7nl4sh2782ilm3p4klcl2fb66w3k92sl3j7g96ljg1")))

