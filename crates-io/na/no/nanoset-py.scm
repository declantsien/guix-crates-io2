(define-module (crates-io na no nanoset-py) #:use-module (crates-io))

(define-public crate-nanoset-py-0.1.0 (c (n "nanoset-py") (v "0.1.0") (d (list (d (n "built") (r "^0.3.1") (d #t) (k 1)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.3.0") (d #t) (k 0)))) (h "0jnnmjxlgsg5wp4jx532pb1972wc880xbqywhh3kzy20kwn21dpf") (y #t)))

(define-public crate-nanoset-py-0.1.1 (c (n "nanoset-py") (v "0.1.1") (d (list (d (n "built") (r "^0.3.1") (d #t) (k 1)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1j67hz382dyi61wji3llly48aw1vy0hn6n206lj86skjnpjypkyh") (f (quote (("extension-module" "pyo3/extension-module" "pyo3-built") ("default"))))))

(define-public crate-nanoset-py-0.1.2 (c (n "nanoset-py") (v "0.1.2") (d (list (d (n "built") (r "^0.3.1") (d #t) (k 1)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "07al2p68wxkfhgsgdr9kjfk672r3lh4m212kmdj5gaip2jwvpd5v") (f (quote (("extension-module" "pyo3/extension-module" "pyo3-built") ("default"))))))

(define-public crate-nanoset-py-0.1.3 (c (n "nanoset-py") (v "0.1.3") (d (list (d (n "built") (r "^0.3.1") (d #t) (k 1)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "04l4ibc5blhmqg0rk68hlhbnpr8bz5frd7cq84hx3dk0khgl3fjx") (f (quote (("extension-module" "pyo3/extension-module" "pyo3-built") ("default"))))))

(define-public crate-nanoset-py-0.1.4 (c (n "nanoset-py") (v "0.1.4") (d (list (d (n "built") (r "^0.3.1") (d #t) (k 1)) (d (n "pyo3") (r "^0.8.0") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "17a378q8cmbbr7f4i4icgqfyiwn2913mfcrpi8scrcjk2g5wrqk9") (f (quote (("extension-module" "pyo3/extension-module" "pyo3-built") ("default"))))))

