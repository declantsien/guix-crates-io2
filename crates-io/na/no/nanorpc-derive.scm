(define-module (crates-io na no nanorpc-derive) #:use-module (crates-io))

(define-public crate-nanorpc-derive-0.1.0 (c (n "nanorpc-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dh3694lv46yznbvw3ilsibkj3dhv5kk3sywp9s5vag22f1hj4xi") (y #t)))

(define-public crate-nanorpc-derive-0.1.1 (c (n "nanorpc-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sff2pvalfilrbirznwqf145x4dk0hbyms2zp6gnny4v3ychgcb9") (y #t)))

(define-public crate-nanorpc-derive-0.1.2 (c (n "nanorpc-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pw22dipx62wxcsq8kas6grgh58q68x6j3qvjkhxxjh2yy2cgcqy") (y #t)))

(define-public crate-nanorpc-derive-0.1.3 (c (n "nanorpc-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxxpb5pkizn4ys56v8fbp41w3s6fgiyah3q7am8rwjlm08l898l")))

(define-public crate-nanorpc-derive-0.1.4 (c (n "nanorpc-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0xkb4li2mwzx0c6qhlb875rjkbqx8259dpk148v3bnwmdk7f0hlr")))

(define-public crate-nanorpc-derive-0.1.5 (c (n "nanorpc-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0c5h6g8va3d9lq96277ky6a7hirpr2jha1xw1f6v52xq0lgxs62h")))

