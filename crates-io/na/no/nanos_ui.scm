(define-module (crates-io na no nanos_ui) #:use-module (crates-io))

(define-public crate-nanos_ui-0.2.0 (c (n "nanos_ui") (v "0.2.0") (d (list (d (n "include_gif") (r "^0.1.0") (d #t) (k 0)) (d (n "nanos_sdk") (r "^0.2.1") (d #t) (k 0)))) (h "169czd81x08jhwkm6f9h1a09ynizp30dvx4pi1w6dgz5ab4vdqhj") (f (quote (("speculos" "nanos_sdk/speculos")))) (y #t)))

