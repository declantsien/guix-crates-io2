(define-module (crates-io na no nanoid) #:use-module (crates-io))

(define-public crate-nanoid-0.1.0 (c (n "nanoid") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "0js28ccgb97zy11nxisbj5lv7kf12pj1ivrify6dg4drwal51490")))

(define-public crate-nanoid-0.1.1 (c (n "nanoid") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "1plw4dcvkwyrsc334mv0qvhykvqy7nlzagpviggfpy3hnq49q4jg")))

(define-public crate-nanoid-0.1.2 (c (n "nanoid") (v "0.1.2") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "1cki6niw9drjzsbva1kh28c483mzsbj06da4g11j005g84zn440l")))

(define-public crate-nanoid-0.1.3 (c (n "nanoid") (v "0.1.3") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "1b8z2pmphch370qd51l4nrz7i5m4gj0jyxa82y200npcr63xds3b")))

(define-public crate-nanoid-0.2.0 (c (n "nanoid") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.1") (d #t) (k 0)))) (h "03zy9d81dd0xfd132hby83vxvjprkv7q6p8c3xrmr9570w7klmgg")))

(define-public crate-nanoid-0.3.0 (c (n "nanoid") (v "0.3.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("prepush-hook" "precommit-hook" "postmerge-hook" "run-cargo-test" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0pk3iy018fqx8iajhr6q83ki1fyr9jh3g6ih9ss4q4j2w726n8m6")))

(define-public crate-nanoid-0.4.0 (c (n "nanoid") (v "0.4.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("prepush-hook" "precommit-hook" "postmerge-hook" "run-cargo-test" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1n5l5xig5zcinh41bb4p0rzam8gxic02qpngnylb3d8pq3g01yiz")))

