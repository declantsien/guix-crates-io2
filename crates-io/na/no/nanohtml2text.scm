(define-module (crates-io na no nanohtml2text) #:use-module (crates-io))

(define-public crate-nanohtml2text-0.1.0 (c (n "nanohtml2text") (v "0.1.0") (h "10w129g9w4brgwbpiy9l8ayg62mc9ws58c31z5y12yrp768rmyms")))

(define-public crate-nanohtml2text-0.1.1 (c (n "nanohtml2text") (v "0.1.1") (h "1b8cchmn8hx84yl021win5in9c77gvkj052304aw4r36dxx2waj8")))

(define-public crate-nanohtml2text-0.1.2 (c (n "nanohtml2text") (v "0.1.2") (h "10ab7xjykv9wgxyqcmh781q1bwqjg7wjcd3s1p3z5lhylwhrjr5v")))

(define-public crate-nanohtml2text-0.1.3 (c (n "nanohtml2text") (v "0.1.3") (h "0x641wvcad8ln99ynnq7v392xr7v3dnnij5ks0xkn8vqlpwr0khz")))

(define-public crate-nanohtml2text-0.1.4 (c (n "nanohtml2text") (v "0.1.4") (h "0b6waszwh0cj1ynn3qy1bkr0g1i7wigc5qgck1p369057kz835lr")))

