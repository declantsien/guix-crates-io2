(define-module (crates-io na no nano-gemm-f64) #:use-module (crates-io))

(define-public crate-nano-gemm-f64-0.1.0 (c (n "nano-gemm-f64") (v "0.1.0") (d (list (d (n "nano-gemm-codegen") (r "^0.1") (d #t) (k 1)) (d (n "nano-gemm-core") (r "^0.1") (d #t) (k 0)))) (h "1n26y08v3il4s5pds34bkq8w906kd6v62gpnw44ii02ds2g627mw") (f (quote (("nightly") ("default"))))))

