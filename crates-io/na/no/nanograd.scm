(define-module (crates-io na no nanograd) #:use-module (crates-io))

(define-public crate-nanograd-0.1.0 (c (n "nanograd") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jrvarsmb2ffwajgmyqxj5f1rzq185yacxmz9y6n8wnhb099sn10")))

(define-public crate-nanograd-0.1.1 (c (n "nanograd") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0sy3ran7dhk7hiidmd9yjapshmhgnwn5k131hdi1indp6767xdvv")))

(define-public crate-nanograd-0.1.2 (c (n "nanograd") (v "0.1.2") (d (list (d (n "getrandom") (r "^0.2.3") (f (quote ("js"))) (d #t) (k 0)))) (h "1zsy7yxdkg6zyvzcws48fafp2cih86za0k0yb2ci45ndhgm3k66w")))

