(define-module (crates-io na no nano-id) #:use-module (crates-io))

(define-public crate-nano-id-0.0.1 (c (n "nano-id") (v "0.0.1") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)))) (h "179lmkvkzw69xi3lq7aqczw0zl642mvv5cn1mdhk9zy3rcnly52p") (f (quote (("step") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.1.0 (c (n "nano-id") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.2") (d #t) (k 0)))) (h "0g4z645z6zxi8sfz88gi042rzg31dmnz7jm7jsh805v1m2xdsf2q") (f (quote (("step") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.1.1 (c (n "nano-id") (v "0.1.1") (d (list (d (n "getrandom") (r "^0.2.3") (d #t) (k 0)))) (h "0v6402zklsaxwknzknmp329i663rld7qkj756c874mqxrn2z12xa") (f (quote (("step") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.2.0 (c (n "nano-id") (v "0.2.0") (d (list (d (n "getrandom") (r "^0.2.3") (d #t) (k 0)))) (h "03r90qipgbq7cwd47vqmpqm70widrkdg0gjf6sifz6jkp3lxas43") (f (quote (("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.2.1 (c (n "nano-id") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 0)))) (h "1i58griqcvnrw78xxwf4fljr3w6yvjhqa1apf9350495pfp4v1sh") (f (quote (("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.3.0 (c (n "nano-id") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.6") (d #t) (k 0)))) (h "1k69ri6m33mdd1iakp3w1v4ixc78if309nvkch0kzz87hj1cdx30") (f (quote (("wasm" "getrandom/js") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.3.1 (c (n "nano-id") (v "0.3.1") (d (list (d (n "getrandom") (r "^0.2.8") (d #t) (k 0)))) (h "17ng95799kdyil0smp3nf49h6w39n4wiax4ks87077v8fillckbl") (f (quote (("wasm" "getrandom/js") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.3.2 (c (n "nano-id") (v "0.3.2") (d (list (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)))) (h "1gcy4dgjdni4zdhd3vgnm0wl6hl9087ymwmqz70kgkzxybzwrrsr") (f (quote (("wasm" "getrandom/js") ("default" "base64") ("base64") ("base62") ("base58"))))))

(define-public crate-nano-id-0.3.3 (c (n "nano-id") (v "0.3.3") (d (list (d (n "getrandom") (r "^0.2.11") (d #t) (k 0)))) (h "0m5xzs4y0pqycilifnf18rvdmv8kxmld56l36agpdw9jihhgv7bf") (f (quote (("wasm" "getrandom/js") ("default" "base64") ("base64") ("base62") ("base58"))))))

