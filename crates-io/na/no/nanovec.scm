(define-module (crates-io na no nanovec) #:use-module (crates-io))

(define-public crate-nanovec-0.1.0 (c (n "nanovec") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1gjbv7p27vdmgimd9zbvp5lgcjvhar7d9wqm6m5vyhcx7vhxhw8v") (y #t)))

(define-public crate-nanovec-0.1.1 (c (n "nanovec") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0x76lbi1dz0nldy8gid1zr9im8zfli1p34jb1ycis8b2d44wqz4k") (y #t)))

(define-public crate-nanovec-0.2.0 (c (n "nanovec") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "replace_with") (r "^0.1.7") (k 0)))) (h "14l3yhd8lkc6ckyrww6cw5m2q8731dyjkl0snv4ic9fc8dzdpcz3")))

(define-public crate-nanovec-0.2.1 (c (n "nanovec") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "replace_with") (r "^0.1.7") (k 0)))) (h "1mk96dy5bqrw0biwnr4mzcil6a38cs9i8xgmwagyyrz4rdawwka5")))

