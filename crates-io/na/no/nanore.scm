(define-module (crates-io na no nanore) #:use-module (crates-io))

(define-public crate-nanore-0.1.0 (c (n "nanore") (v "0.1.0") (h "07z1bxkpcxa22z3d3pskify3rbkv6f7xbms9mk2n70sx5ygbdqnc")))

(define-public crate-nanore-0.2.0 (c (n "nanore") (v "0.2.0") (h "0yvzpda61w6bcnhg4vwgdkzgdr6xm47l4mfjdv5l7dyy3mc8wdcb")))

