(define-module (crates-io na no nano-leb128) #:use-module (crates-io))

(define-public crate-nano-leb128-0.1.0 (c (n "nano-leb128") (v "0.1.0") (d (list (d (n "byteio") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "13kj2ls8fqxvzbwm5vfmlmzalp5vgaxyirqzambwj3zf7i1sfy7p") (f (quote (("std_io_ext" "std") ("std" "byteio/std") ("default" "std") ("byteio_ext"))))))

