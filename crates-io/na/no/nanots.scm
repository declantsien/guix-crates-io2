(define-module (crates-io na no nanots) #:use-module (crates-io))

(define-public crate-nanots-0.1.0 (c (n "nanots") (v "0.1.0") (d (list (d (n "swc_common") (r "^0.11.6") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.49.4") (d #t) (k 0)))) (h "1pjcnmv5i765xmk63l3fnzl5phk9w3zax4m02fm105dj55v2pkam")))

