(define-module (crates-io na no nanopre) #:use-module (crates-io))

(define-public crate-nanopre-0.1.0 (c (n "nanopre") (v "0.1.0") (h "1wyl5bhzsk1mizrbzm7g3bnzl0crgmnn0cjgzwwaafbncdlmi0v1")))

(define-public crate-nanopre-0.1.1 (c (n "nanopre") (v "0.1.1") (h "00bav20qsx1qw5ylmnjdp3a7a158az4cs45rcn55sv30m711i24s")))

(define-public crate-nanopre-0.1.2 (c (n "nanopre") (v "0.1.2") (h "0gs44i4kl1l98dxlwx0v83yl0h1xvxw01k00w1g6ajmd7xnx6lpr")))

