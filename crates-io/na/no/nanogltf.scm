(define-module (crates-io na no nanogltf) #:use-module (crates-io))

(define-public crate-nanogltf-0.1.0 (c (n "nanogltf") (v "0.1.0") (d (list (d (n "dolly") (r "^0.4") (d #t) (k 2)) (d (n "glam") (r "^0.24") (d #t) (k 2)) (d (n "miniquad") (r "^0.4.0-alpha") (f (quote ("log-impl"))) (d #t) (k 2)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "shadermagic") (r "^0.1") (d #t) (k 2)) (d (n "zune-core") (r "^0.2") (d #t) (k 2)) (d (n "zune-jpeg") (r "^0.3") (d #t) (k 2)) (d (n "zune-png") (r "^0.2") (d #t) (k 2)))) (h "1j9l24mn8ckhv8q9fhszhslfdp16m4clwr6pn5x6x6kgq3g0r8q6")))

