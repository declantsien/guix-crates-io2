(define-module (crates-io na no nano-rs) #:use-module (crates-io))

(define-public crate-nano-rs-0.1.0 (c (n "nano-rs") (v "0.1.0") (d (list (d (n "nano-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-extra") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-proc-macro-route") (r "^0.1.0") (d #t) (k 0)))) (h "0jpf6rlxlqn8dgnd7498v7bh63k8bff4sralfllpcjm9kalprwhv") (r "1.66")))

(define-public crate-nano-rs-0.1.1 (c (n "nano-rs") (v "0.1.1") (d (list (d (n "nano-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-extra") (r "^0.1.1") (d #t) (k 0)) (d (n "nano-rs-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-proc-macro-route") (r "^0.1.0") (d #t) (k 0)))) (h "1xcpd1zhh6y46qrsfbqsk320smi946ikprk4bzwhz53knrqpb6c6") (r "1.66")))

(define-public crate-nano-rs-0.1.2 (c (n "nano-rs") (v "0.1.2") (d (list (d (n "nano-rs-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-extra") (r "^0.1.2") (d #t) (k 0)) (d (n "nano-rs-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "nano-rs-proc-macro-route") (r "^0.1.0") (d #t) (k 0)))) (h "1nxpi46bslgb9l7j85qagnrm037090ipvviywhrp40a4hb4z8h0i") (r "1.66")))

