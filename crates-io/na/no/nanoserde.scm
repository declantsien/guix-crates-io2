(define-module (crates-io na no nanoserde) #:use-module (crates-io))

(define-public crate-nanoserde-0.1.0 (c (n "nanoserde") (v "0.1.0") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "1c494352gv6g6arj0knicprhcf398ck70gajha5wc1a88wiqkid3")))

(define-public crate-nanoserde-0.1.1 (c (n "nanoserde") (v "0.1.1") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "1r066hmj66f1bywa08zm87i1gxp9pkwa1rry2gqyzpxh8z5g6qr1")))

(define-public crate-nanoserde-0.1.2 (c (n "nanoserde") (v "0.1.2") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "1hggssr4v8sfw6xk9lbirdv26dq82f7p90qwgdizy9ip0sla3rg5")))

(define-public crate-nanoserde-0.1.3 (c (n "nanoserde") (v "0.1.3") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "0pv1mlpqvhg3466mcdimp0dbr0mjyfz5w95jkdw3f306w5ycwb6c")))

(define-public crate-nanoserde-0.1.4 (c (n "nanoserde") (v "0.1.4") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "01bvza2dg65mf5nnn91n87kfg9m36a19518ykijphhmx1qsbq6gi")))

(define-public crate-nanoserde-0.1.5 (c (n "nanoserde") (v "0.1.5") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "1clka1s2pi4dq49alqhq10qb10hbkwq437aqaagz95lzcnkrs9c7")))

(define-public crate-nanoserde-0.1.6 (c (n "nanoserde") (v "0.1.6") (d (list (d (n "nanoserde-derive") (r "^0.1") (d #t) (k 0)))) (h "0qkvphjzifh7wgb3zqvc3ryxvmy90d7265waiajrlwz2z915sh9h")))

(define-public crate-nanoserde-0.1.7 (c (n "nanoserde") (v "0.1.7") (d (list (d (n "nanoserde-derive") (r "=0.1.4") (d #t) (k 0)))) (h "1av1wd1f93ys72kxajbp1l1i46cpv0nsb0n3iwdwbmbdj3r0sgl1")))

(define-public crate-nanoserde-0.1.8 (c (n "nanoserde") (v "0.1.8") (d (list (d (n "nanoserde-derive") (r "=0.1.5") (d #t) (k 0)))) (h "1dx3x5g9sdzz2z4bmvhbjcm5k9fygx590l18d168gqvmmwpbbh9d")))

(define-public crate-nanoserde-0.1.9 (c (n "nanoserde") (v "0.1.9") (d (list (d (n "nanoserde-derive") (r "=0.1.6") (d #t) (k 0)))) (h "0qyk5vad542824jnnlvnrjbz06ay2948frfdqxa1z9ik4dv0n22b")))

(define-public crate-nanoserde-0.1.10 (c (n "nanoserde") (v "0.1.10") (d (list (d (n "nanoserde-derive") (r "=0.1.7") (d #t) (k 0)))) (h "0wm057zlyn71j4pm0ksiakkcmxzwhx8gwvi5nzc5bxapfkfpzm7y")))

(define-public crate-nanoserde-0.1.11 (c (n "nanoserde") (v "0.1.11") (d (list (d (n "nanoserde-derive") (r "=0.1.8") (d #t) (k 0)))) (h "1dnrz9dlf8bky26qnak05qavifb82r1l8jm12m8yq48h2sbsxx1q")))

(define-public crate-nanoserde-0.1.12 (c (n "nanoserde") (v "0.1.12") (d (list (d (n "nanoserde-derive") (r "=0.1.9") (d #t) (k 0)))) (h "0rq8858lj24pa4l0zv5lfb6aqkw6qf52ln1yl3l3a5y9bpvikvkj")))

(define-public crate-nanoserde-0.1.13 (c (n "nanoserde") (v "0.1.13") (d (list (d (n "nanoserde-derive") (r "=0.1.9") (d #t) (k 0)))) (h "0r40kargwgzn8x7xagspqkwc50ycv3qfbzispznq3by2mrmjj0g7")))

(define-public crate-nanoserde-0.1.14 (c (n "nanoserde") (v "0.1.14") (d (list (d (n "nanoserde-derive") (r "=0.1.10") (d #t) (k 0)))) (h "1aw9ydnhih44g0k1fapyrbhllm9ylfc59vb39srfir6l4rpnci0d")))

(define-public crate-nanoserde-0.1.15 (c (n "nanoserde") (v "0.1.15") (d (list (d (n "nanoserde-derive") (r "=0.1.10") (d #t) (k 0)))) (h "0if6yyw8m0m48km21gk232bwhy5mpzc12cy3fjy3p6by4pcahavr") (y #t)))

(define-public crate-nanoserde-0.1.16 (c (n "nanoserde") (v "0.1.16") (d (list (d (n "nanoserde-derive") (r "=0.1.10") (d #t) (k 0)))) (h "0jpl5zfqq1bzg2df3g6cjyfrqn0m0cfv9pba91imkwwa3k0cj7k6")))

(define-public crate-nanoserde-0.1.17 (c (n "nanoserde") (v "0.1.17") (d (list (d (n "nanoserde-derive") (r "=0.1.11") (d #t) (k 0)))) (h "1csj4n9g7rxsw8pn5w86nqd82cfr41israydm03m3ikvxkhkzm2i")))

(define-public crate-nanoserde-0.1.18 (c (n "nanoserde") (v "0.1.18") (d (list (d (n "nanoserde-derive") (r "=0.1.12") (d #t) (k 0)))) (h "0y3r3syj0p7a5ax4mlqfwdm08b15v90blbkm154kmnkhbk6939pg")))

(define-public crate-nanoserde-0.1.19 (c (n "nanoserde") (v "0.1.19") (d (list (d (n "nanoserde-derive") (r "=0.1.12") (d #t) (k 0)))) (h "0p6j00mv4wicmwavg7fw3h299l8rzyx4vja2n68xk7mkxqxzsa4g")))

(define-public crate-nanoserde-0.1.20 (c (n "nanoserde") (v "0.1.20") (d (list (d (n "nanoserde-derive") (r "=0.1.12") (d #t) (k 0)))) (h "1mvi7kc5l7nq9x819z1nbvgzcdrzd98h4bvympiwf5lapzl2qxip")))

(define-public crate-nanoserde-0.1.21 (c (n "nanoserde") (v "0.1.21") (d (list (d (n "nanoserde-derive") (r "=0.1.13") (d #t) (k 0)))) (h "0qjy0la1q7cwf8cahg5vjphchisqyjil6dg4ags8m4538pqx9z5n")))

(define-public crate-nanoserde-0.1.22 (c (n "nanoserde") (v "0.1.22") (d (list (d (n "nanoserde-derive") (r "=0.1.14") (d #t) (k 0)))) (h "1if75y0pqlhdwjwsc7q20p7ki4sxna2qi3aa9fxl9qff2mpvzpya")))

(define-public crate-nanoserde-0.1.23 (c (n "nanoserde") (v "0.1.23") (d (list (d (n "nanoserde-derive") (r "=0.1.15") (d #t) (k 0)))) (h "0aw4pqvyjsykzn8lv1512f9pj8lgcqmqrl05gk7dxaa16cclzlc5")))

(define-public crate-nanoserde-0.1.24 (c (n "nanoserde") (v "0.1.24") (d (list (d (n "nanoserde-derive") (r "=0.1.16") (d #t) (k 0)))) (h "0lg9s34l4xm7a1dwszd5af4jxlrlvghn9bkg1flcwcrg7pqkkzmj")))

(define-public crate-nanoserde-0.1.25 (c (n "nanoserde") (v "0.1.25") (d (list (d (n "nanoserde-derive") (r "=0.1.16") (d #t) (k 0)))) (h "1sfpambmf3ifs8h2k7y9w6va8sjak5yads2wywb4pml1vi58kr8s")))

(define-public crate-nanoserde-0.1.26 (c (n "nanoserde") (v "0.1.26") (d (list (d (n "nanoserde-derive") (r "=0.1.17") (d #t) (k 0)))) (h "0f1cy7kqk3zr67dl4mfqbgsgppdypi740x4cbi6cd80yzklhjw0r")))

(define-public crate-nanoserde-0.1.27 (c (n "nanoserde") (v "0.1.27") (d (list (d (n "nanoserde-derive") (r "=0.1.17") (d #t) (k 0)))) (h "0mkdml5zh276fiblk30imbb048lfmngfa9a0riga4kriwzawz5j7") (y #t)))

(define-public crate-nanoserde-0.1.28 (c (n "nanoserde") (v "0.1.28") (d (list (d (n "nanoserde-derive") (r "=0.1.18") (d #t) (k 0)))) (h "1c8gm9qgg73bm2avcy1h6x834872frvgqlskrbdrix8k2i1wwfns")))

(define-public crate-nanoserde-0.1.29 (c (n "nanoserde") (v "0.1.29") (d (list (d (n "nanoserde-derive") (r "=0.1.18") (d #t) (k 0)))) (h "0s184sfrqf81jwbb96fzgf8njqv8cs9gnhsnb7nz1vhx6gkf71i7")))

(define-public crate-nanoserde-0.1.30 (c (n "nanoserde") (v "0.1.30") (d (list (d (n "nanoserde-derive") (r "=0.1.18") (d #t) (k 0)))) (h "0k8w5bj8zzggdfsvpcjb1qgn61yk7qkjgm520xjjbn34fv383nka")))

(define-public crate-nanoserde-0.1.31 (c (n "nanoserde") (v "0.1.31") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.18") (d #t) (k 0)))) (h "1x6ps3fhr7cdd1bp7c4yw149z92n1m009qwi88v3ysc3ya5mmjds") (f (quote (("default")))) (y #t) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-0.1.32 (c (n "nanoserde") (v "0.1.32") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.19") (d #t) (k 0)))) (h "1lrr30q9l6gsac53yqgx8c25p0xzlnnzabdskxy4vibbadjpjpkm") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-0.1.33 (c (n "nanoserde") (v "0.1.33") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.20") (d #t) (k 0)))) (h "0rl6yf4hmpzc6rmk6j38iiz4dsp7sj62fdipvz0lr3wjys1h7mzg") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-0.1.34 (c (n "nanoserde") (v "0.1.34") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.20") (d #t) (k 0)))) (h "087v4gy1qrfmdpaynqg0nn44kqs6bigcvawwzfy4lgm2qpnbbykz") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-0.1.35 (c (n "nanoserde") (v "0.1.35") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.21") (d #t) (k 0)))) (h "1g5iv92sr18959s9hkfl0rzd2vay1nrgj12g7j0cs3zd345kv62a") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

(define-public crate-nanoserde-0.1.37 (c (n "nanoserde") (v "0.1.37") (d (list (d (n "hashbrown") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "nanoserde-derive") (r "=0.1.22") (d #t) (k 0)))) (h "168vga2z8inldwahsjps2mi3m149p16dfnsjac1mmqmi9a2czsax") (f (quote (("default")))) (s 2) (e (quote (("no_std" "dep:hashbrown"))))))

