(define-module (crates-io na no nano-gemm-c32) #:use-module (crates-io))

(define-public crate-nano-gemm-c32-0.1.0 (c (n "nano-gemm-c32") (v "0.1.0") (d (list (d (n "nano-gemm-codegen") (r "^0.1") (d #t) (k 1)) (d (n "nano-gemm-core") (r "^0.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (k 0)))) (h "06x9fc9wjaq2rwi1hwa42gp2k38w631w820j7964c4spgbjlj154") (f (quote (("nightly") ("default"))))))

