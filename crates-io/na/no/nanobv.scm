(define-module (crates-io na no nanobv) #:use-module (crates-io))

(define-public crate-nanobv-0.1.0 (c (n "nanobv") (v "0.1.0") (h "1dvlx05na8wlbxlczmhk1ddvqxg2g8cscj3gy3xi8flzkkd6lqa8")))

(define-public crate-nanobv-0.1.1 (c (n "nanobv") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 2)) (d (n "picorand") (r "^0.1.1") (d #t) (k 2)))) (h "0xr02hm5dswx4nh7nib9d5ws250mqy6w2ch7nwjma26f20hg89g2")))

