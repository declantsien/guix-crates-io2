(define-module (crates-io ra nd random-wall) #:use-module (crates-io))

(define-public crate-random-wall-1.0.0 (c (n "random-wall") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.2") (f (quote ("http2" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0ggc591q5hlsq8vl8xkkklkd9qim5r3j8qivakb9fhixycxbbl0z")))

