(define-module (crates-io ra nd randomstringb) #:use-module (crates-io))

(define-public crate-randomstringb-1.0.0 (c (n "randomstringb") (v "1.0.0") (h "0bqnglw1syrx25pvk0824sicfdilwq5nswl480sdga0zxhih2gkw")))

(define-public crate-randomstringb-1.0.1 (c (n "randomstringb") (v "1.0.1") (h "00qxyag5a2x77r4q8lc7jm1ry5kaffhx4v039cpk550bpxqzcxsn")))

