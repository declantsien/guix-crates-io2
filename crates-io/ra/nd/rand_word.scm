(define-module (crates-io ra nd rand_word) #:use-module (crates-io))

(define-public crate-rand_word-0.1.0 (c (n "rand_word") (v "0.1.0") (h "0li7cfvr5h8laly2bmbm0di07kcrz3zk69fyggka2214xpa8f991")))

(define-public crate-rand_word-0.1.1 (c (n "rand_word") (v "0.1.1") (h "18jjlxszin89nqxnn0ibsldvb6kj1n18ifvs45v6j4gwn6443vr1")))

(define-public crate-rand_word-0.1.2 (c (n "rand_word") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.17") (d #t) (k 0)))) (h "091pjs0qs3x89r45wxnrwymm1j6d5gdqjnmxbw81i908y8dgydyb")))

(define-public crate-rand_word-0.1.3 (c (n "rand_word") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.17") (d #t) (k 0)))) (h "1iblxsn4sa1lj1cic36kjdkaphf2spmr8qfizmi7ldk8vbgmlzz6")))

(define-public crate-rand_word-0.1.4 (c (n "rand_word") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.17") (d #t) (k 0)))) (h "1x6maq6y0xxl03sqd29029gyy3ywix1fi47rq1ic9792kqiwl395")))

(define-public crate-rand_word-0.1.5 (c (n "rand_word") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.17") (d #t) (k 0)))) (h "07xgrcjndgnlp47kpkwnn75m2xb53n3issgn9sc2aiy1fa5dasvm")))

(define-public crate-rand_word-0.1.6 (c (n "rand_word") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jb3k5v0vxrnz0m2l8kipbr5f0mj2b716lamnb97qbjq44lmcyk0")))

(define-public crate-rand_word-0.1.8 (c (n "rand_word") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rzqzr2i7lxl9hdk1gvwk3ld9a25y89021xppa0ik4gp9gr1vbvc")))

(define-public crate-rand_word-0.1.9 (c (n "rand_word") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1b0rzy65v0yh81sm6vh0fd616m5k31864j2arjib0ndqmjr8vwpp")))

(define-public crate-rand_word-0.1.10 (c (n "rand_word") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q4sakqzsp0g9dy7wdjm7hiinpk76zyjxp2x995isn3aqq02byam")))

(define-public crate-rand_word-0.1.11 (c (n "rand_word") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1kjia5arahz1davc0bf4h6ivzrsmv3iph0w4mlq6s46axvccg75y")))

(define-public crate-rand_word-0.1.12 (c (n "rand_word") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05yxvj00w3z5hqdb7046gndzzmfzgm1c65qmsnk05a50nx9dyr5b")))

(define-public crate-rand_word-0.1.13 (c (n "rand_word") (v "0.1.13") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01g7hjr7rr4ag481jfdphljhcfsf9g4prxgrfsdjzn62fbxhdsag")))

(define-public crate-rand_word-0.1.14 (c (n "rand_word") (v "0.1.14") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05dl0740bjfq546397i3am75n2wv2ky7si7wcdm0rqvm0l5a0bv4")))

(define-public crate-rand_word-0.1.15 (c (n "rand_word") (v "0.1.15") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a1hjwzx39rnm8q20b9skl85fz1dd8la42ml3csmwwjgqszw04rx")))

(define-public crate-rand_word-0.1.16 (c (n "rand_word") (v "0.1.16") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hkd1c7fbxhajm0g1fj51npab5qbprx9645a2rfnkgj56hddww57")))

(define-public crate-rand_word-0.1.17 (c (n "rand_word") (v "0.1.17") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ya0rqqv2i3qr0sh1qqdhpbnncc0y11v6g0c2hcinwazjr12h59a")))

(define-public crate-rand_word-0.1.18 (c (n "rand_word") (v "0.1.18") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11djy5qgvp9x1zp2l97q4p529kr07firbzm8hidk27947l4hzp9a")))

