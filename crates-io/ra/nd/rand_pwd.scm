(define-module (crates-io ra nd rand_pwd) #:use-module (crates-io))

(define-public crate-rand_pwd-1.0.0 (c (n "rand_pwd") (v "1.0.0") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "161r1rli0n6cm493xj9ng78r7i007py4lam0k3vy8msy5qy4w2ml")))

(define-public crate-rand_pwd-1.0.1 (c (n "rand_pwd") (v "1.0.1") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "00v9blbdj2jaa3psncmafwbgh6sl1nfz75f7svb0z7h35m2v6rbk")))

(define-public crate-rand_pwd-1.0.2 (c (n "rand_pwd") (v "1.0.2") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1rsfmh924616v0l8f1sq5vxn2bhjr4d8r3i77kq96cxwpf00wqn7")))

(define-public crate-rand_pwd-1.0.3 (c (n "rand_pwd") (v "1.0.3") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0yvwy09z6vi1d6z7nqmirh0s4m1ckhqniwdp5hw3a4czngp4i8c7")))

(define-public crate-rand_pwd-1.1.0 (c (n "rand_pwd") (v "1.1.0") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1is709hnx07g1s0zqxyd6wprm0351hvvd2mcy08s8s8273hdszcz")))

(define-public crate-rand_pwd-1.1.1 (c (n "rand_pwd") (v "1.1.1") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1iihxy47z7ypxjnqf0ql40yy9v9dlbgawrz8pa33pp8ngns4p4d4")))

(define-public crate-rand_pwd-1.1.2 (c (n "rand_pwd") (v "1.1.2") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "0x8flm2qsdzrgbhm9c1pdzhghyx735cdk88cny75j3dbrbwrhij2")))

(define-public crate-rand_pwd-1.1.3 (c (n "rand_pwd") (v "1.1.3") (d (list (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)))) (h "1a9lpqfs820zz537hvzm05s3zq9aysxmnwn6xw8wk387ikdng77n")))

