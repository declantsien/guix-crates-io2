(define-module (crates-io ra nd randomforest) #:use-module (crates-io))

(define-public crate-randomforest-0.1.0 (c (n "randomforest") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hxy0vgmpq8d9qk218ln7paccd2fw0a1qmsgkw05lvjd7lyydibm")))

(define-public crate-randomforest-0.1.1 (c (n "randomforest") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01rsgs0bklinlk2kxyspj3givg72jh08wjmpvx2zgy4w46f13q35")))

(define-public crate-randomforest-0.1.2 (c (n "randomforest") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1l5j7ysx0fmv1n9p71ywn8lc5xv2kkkk4z19db2x77l1n0wl6vj7")))

(define-public crate-randomforest-0.1.3 (c (n "randomforest") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03vv4mnp0y37imqw3ypjrfxkckrxbx02x8m2qpk02mvhm3z15f88")))

(define-public crate-randomforest-0.1.4 (c (n "randomforest") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1b2l29zfd8c7ywpjrcbajssp7762l28q18f5ym3mg2jfhq7qci3z")))

(define-public crate-randomforest-0.1.5 (c (n "randomforest") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0awdv66zg1zp7i10pf2vj58l8gvzzw4909fvlpzasx01kcicifi5")))

(define-public crate-randomforest-0.1.6 (c (n "randomforest") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12kbci4gwmcz0w46qb1msy5gj35809aq9bg74vjdq34pf3pzhil3")))

