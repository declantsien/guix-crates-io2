(define-module (crates-io ra nd random_number_generators) #:use-module (crates-io))

(define-public crate-random_number_generators-0.2.0 (c (n "random_number_generators") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (d #t) (k 0)) (d (n "itertools") (r "~0.8.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.0") (d #t) (k 0)) (d (n "rand_core") (r "~0.5.0") (d #t) (k 0)) (d (n "rand_lcg") (r "^0.1") (d #t) (k 0)) (d (n "rand_pcg") (r "~0.2.0") (d #t) (k 0)) (d (n "rand_xorshift") (r "~0.2.0") (d #t) (k 0)) (d (n "rand_xoshiro") (r "~0.3.1") (d #t) (k 0)) (d (n "sfmt") (r "^0.6.0") (k 0)))) (h "02rfp6nv1085jr81frmhnhrx2827xm2crq4sc15fhhdsvzj6v06h")))

