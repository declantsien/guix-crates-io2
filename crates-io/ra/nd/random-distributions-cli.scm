(define-module (crates-io ra nd random-distributions-cli) #:use-module (crates-io))

(define-public crate-random-distributions-cli-0.1.0 (c (n "random-distributions-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "1n1vrhdhmnzkddr5saaiki0glfxbqhkfa45z4bg9kqfsl2hgjwjl")))

(define-public crate-random-distributions-cli-0.2.0 (c (n "random-distributions-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "0p9gx6bmb9h5n8vsrrh34pf6lcgv3w7q162jhrlcrvmczcsx7vn2")))

