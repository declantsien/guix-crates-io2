(define-module (crates-io ra nd random-unicode-emoji) #:use-module (crates-io))

(define-public crate-random-unicode-emoji-0.1.0 (c (n "random-unicode-emoji") (v "0.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "118sajqj7b8mg2d1d6pa306q7zwcj01r9ry9y6jfiakzfaqx8hzx")))

(define-public crate-random-unicode-emoji-1.0.0 (c (n "random-unicode-emoji") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xjxmkrw37lwrw01pkhmp7vx992jmcs729q66ah99nxy5jkqwhv8")))

(define-public crate-random-unicode-emoji-1.0.1 (c (n "random-unicode-emoji") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lj5rc7rbw4p6dk4b4jvq2c6m76g9x79wfiwaklj46fdxgqk95ni")))

