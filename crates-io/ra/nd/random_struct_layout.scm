(define-module (crates-io ra nd random_struct_layout) #:use-module (crates-io))

(define-public crate-random_struct_layout-0.1.0 (c (n "random_struct_layout") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "07zm2kjvzni3vrk5g9q66fpnkk97ybs4kpv7ggmjg0jn7qdkglav")))

(define-public crate-random_struct_layout-0.1.1 (c (n "random_struct_layout") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7hzi2l69bk4086bhhnm076qdgscl8k0yrlxyvyf3rrc5hp0n0k")))

(define-public crate-random_struct_layout-0.2.1 (c (n "random_struct_layout") (v "0.2.1") (d (list (d (n "offset") (r "^0.1.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "07fixyijxbz1srh95rcv7hx042b4kdxarml5pkdp1adyj35071jx")))

(define-public crate-random_struct_layout-0.3.1 (c (n "random_struct_layout") (v "0.3.1") (d (list (d (n "offset") (r "^0.1.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "1ilajqv34mb1lxh8v50faww7ajw68myarfrvnhg5h242qwx49nd7")))

(define-public crate-random_struct_layout-0.3.2 (c (n "random_struct_layout") (v "0.3.2") (d (list (d (n "offset") (r "^0.1.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0zmydgk5rdvpx02mvbgadhphdw5ssbbak2kjq5x6w2irdpm8vwk3")))

