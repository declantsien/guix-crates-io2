(define-module (crates-io ra nd randrust) #:use-module (crates-io))

(define-public crate-randrust-0.1.1 (c (n "randrust") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1yiigralj5wnrjclgkn0fv1l16bbvdl7v6a1aihljy04cxnb5ipc")))

(define-public crate-randrust-0.1.3 (c (n "randrust") (v "0.1.3") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1jkibfaza7l9q10clxzcx8rfj48gqb7idrgrjd7p8b6b3hq3yz3z")))

(define-public crate-randrust-0.1.14 (c (n "randrust") (v "0.1.14") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "05vgr1azz64dff35zh4hqflqa7nqrxh2f6w9ld7cjamqwah84yzg")))

(define-public crate-randrust-0.1.16 (c (n "randrust") (v "0.1.16") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0bpkkcyqpwzyzw5i4k1i4434x2dsprdpj0jisvcj8dl6p8lyh3qz")))

(define-public crate-randrust-0.1.17 (c (n "randrust") (v "0.1.17") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "hyper") (r "^0.12.35") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1jvi7w2c8846aj3l4ww40mshpwyvdjk6bx7dwwysszl5mvzbh2z4")))

