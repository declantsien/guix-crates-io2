(define-module (crates-io ra nd rand-functors) #:use-module (crates-io))

(define-public crate-rand-functors-0.1.0 (c (n "rand-functors") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1i5ma1zrzzpd0lfcc3jk5ylfqk9b942vrwi5xiby435xmaw4lcj2") (r "1.76.0")))

(define-public crate-rand-functors-0.1.1 (c (n "rand-functors") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0askwjib22qpr2k02a0zpaiv9rzddh32h3qma9wnyz1z89ikrs6k") (r "1.76.0")))

(define-public crate-rand-functors-0.1.2 (c (n "rand-functors") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "037mkc1mziwj1hgk2qcpbghv7bjvjll2a4m2nkqd2avpkgnlr30l") (r "1.76.0")))

(define-public crate-rand-functors-0.2.0 (c (n "rand-functors") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0hfjpx90l2zsvaqbz5iidi2dd1n4xy1ripnvqip4b4c6jdragmaf") (r "1.76.0")))

(define-public crate-rand-functors-0.3.0 (c (n "rand-functors") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0h5ylz9y7fh9h1zl026120pa2qmm98bjncx6picfryb7mi9crpx7") (r "1.76.0")))

(define-public crate-rand-functors-0.4.0 (c (n "rand-functors") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0lbmrarl6r7b4pjpaxv9llsh6lfbh85vz34r19z1q2jf6shrwvfl") (r "1.76.0")))

(define-public crate-rand-functors-0.5.0 (c (n "rand-functors") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1sky1lq6dv1vgq93ksr6ggd0vfa75v0hcjq4752hy6cgy3s69623") (r "1.76.0")))

(define-public crate-rand-functors-0.6.0 (c (n "rand-functors") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "04i3nbs3xyih0c3yghs7jm04hfjkpw5pymyg76lswl1lvpqadx9r") (f (quote (("std" "alloc" "rand/std") ("default" "std") ("alloc" "rand/alloc")))) (r "1.75.0")))

(define-public crate-rand-functors-0.7.0 (c (n "rand-functors") (v "0.7.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1khc2chgyw2j65qxk8320l0mccn4rmlbwykg0zhqciihn3qg6iga") (f (quote (("std" "alloc" "rand/std") ("default" "std") ("alloc" "rand/alloc")))) (r "1.75.0")))

(define-public crate-rand-functors-0.8.0 (c (n "rand-functors") (v "0.8.0") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.18") (k 0)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0qb4qqzr5xknxgc4lg48s6zms712zx49xmsgjl3yv7856a291scj") (f (quote (("std" "alloc" "rand/std") ("default" "std") ("alloc" "rand/alloc")))) (r "1.75.0")))

