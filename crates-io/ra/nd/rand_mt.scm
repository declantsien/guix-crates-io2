(define-module (crates-io ra nd rand_mt) #:use-module (crates-io))

(define-public crate-rand_mt-2.0.0 (c (n "rand_mt") (v "2.0.0") (d (list (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0vq16y5qm0m6rhragrg2h7hy4m39ga6bi1zg5iacnidwn2i8y2mp")))

(define-public crate-rand_mt-3.0.0 (c (n "rand_mt") (v "3.0.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1ihxprdkzgpnjisiinkwhrp1r7iisbs3mfvf3qz2acc63v2i9pr7") (f (quote (("std") ("default" "std"))))))

(define-public crate-rand_mt-4.0.0 (c (n "rand_mt") (v "4.0.0") (d (list (d (n "getrandom") (r "^0.2") (k 2)) (d (n "rand_core") (r "^0.6") (o #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11vgxwda6rf2mzm25clmd6mjx9h66rbdnapm5vj7ggknyxp83z1r") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std")))) (y #t)))

(define-public crate-rand_mt-4.0.1 (c (n "rand_mt") (v "4.0.1") (d (list (d (n "getrandom") (r "^0.2") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1z5rppkn4801jariy52r6s7jkfxa2m6ilxdz6dgmnbqjnx6k0l4q") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std"))))))

(define-public crate-rand_mt-4.1.0 (c (n "rand_mt") (v "4.1.0") (d (list (d (n "getrandom") (r "^0.2") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1ik40b03zz0jccgns7ij4l4vcqpcyyr34kpwwqrfzjl226w1mxdl") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std")))) (y #t)))

(define-public crate-rand_mt-4.1.1 (c (n "rand_mt") (v "4.1.1") (d (list (d (n "getrandom") (r "^0.2") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1bbsbkckcbidp2520bc8zs43yp11lxpkxfmiqrl00h3kxri5q0c8") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std"))))))

(define-public crate-rand_mt-4.1.2 (c (n "rand_mt") (v "4.1.2") (d (list (d (n "getrandom") (r "^0.2.0") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1y7jgn5vl5f57jyy1nf3hlp0kpm224pvpzqjbzzg6h7zn85hiw55") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std"))))))

(define-public crate-rand_mt-4.1.3 (c (n "rand_mt") (v "4.1.3") (d (list (d (n "getrandom") (r "^0.2.0") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1h6p5hi15fclw9rzsfhshc8a660lia3qzphjx3ziqwhhh79mwq1h") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std"))))))

(define-public crate-rand_mt-4.2.0 (c (n "rand_mt") (v "4.2.0") (d (list (d (n "getrandom") (r "^0.2.0") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1q287kajs51da1ad8yczh51jfgh9qd3bgv7m2ahj45gvrnzdpxkc") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std"))))))

(define-public crate-rand_mt-4.2.1 (c (n "rand_mt") (v "4.2.1") (d (list (d (n "getrandom") (r "^0.2.0") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1a8hzfy50jk8czq0xzibh2yp8hz5q3mmqqarbf32ff5f6467ywbp") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std")))) (r "1.47.0")))

(define-public crate-rand_mt-4.2.2 (c (n "rand_mt") (v "4.2.2") (d (list (d (n "getrandom") (r "^0.2.0") (k 2)) (d (n "rand_core") (r "^0.6.2") (o #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1pz2l6kwhx9rvfkr8n4c4klr49fancpc31wqc19543nnvv31iq29") (f (quote (("std") ("rand-traits" "rand_core") ("default" "rand-traits" "std")))) (r "1.47.0")))

