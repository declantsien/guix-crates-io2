(define-module (crates-io ra nd randomx-bindings) #:use-module (crates-io))

(define-public crate-randomx-bindings-0.1.0 (c (n "randomx-bindings") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx-bindings-sys") (r "^0.1.3") (d #t) (k 0)))) (h "08cyv73snlyjvjp6hwifzjwi7lqyfk5hkd1dgz85bgppv1gh3cik")))

(define-public crate-randomx-bindings-0.1.1 (c (n "randomx-bindings") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx-bindings-sys") (r "^0.1.3") (d #t) (k 0)))) (h "181sm38bh2gd2rmy23g8hbrnpgnwg41y1mmsllj0180663hqyfd6")))

(define-public crate-randomx-bindings-0.1.2 (c (n "randomx-bindings") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx-bindings-sys") (r "^0.1.4") (d #t) (k 0)))) (h "06nq3mmnsadvc7grrb1gipzih1bw5912l2266hvkcnzqi4lnxfmb")))

(define-public crate-randomx-bindings-0.1.3 (c (n "randomx-bindings") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx-bindings-sys") (r "^0.1") (d #t) (k 0)))) (h "0inmkn997kbww268755q4jl4grxllbrjfp1h03wx88afbjnkdszw")))

(define-public crate-randomx-bindings-0.1.4 (c (n "randomx-bindings") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx-bindings-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0a5ypvsg1bvm1lgca8c1vbs85qpy74v21l8jx5vb2mwnn9pxqxqx")))

