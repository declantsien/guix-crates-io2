(define-module (crates-io ra nd randerive) #:use-module (crates-io))

(define-public crate-randerive-1.0.0 (c (n "randerive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "16v0mbyxmigbm3yqlwvqarwb94vx9rplf1kd1apc3m3fqi1ymplg")))

(define-public crate-randerive-1.1.0 (c (n "randerive") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1797h52ifm30yqfdilnfm4wfx732j903l7dfkwl6xyhd10jmcas2")))

(define-public crate-randerive-1.2.0 (c (n "randerive") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1z826wkprfn6k99hlx00pqdmahxgafzka7by2y037n326cfmsdxj")))

