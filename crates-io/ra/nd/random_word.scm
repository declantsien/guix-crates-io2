(define-module (crates-io ra nd random_word) #:use-module (crates-io))

(define-public crate-random_word-0.1.0 (c (n "random_word") (v "0.1.0") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "05bcwn7nhr4q4rbx362axsfsfbm6r2mpndjhqd90bqni2ms8dj0y")))

(define-public crate-random_word-0.1.1 (c (n "random_word") (v "0.1.1") (d (list (d (n "reqwest") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1lv9hz1ljbm4cl4pl5nl3wv6rhmd8dfkhccw3441sbml33rcqp3j")))

(define-public crate-random_word-0.2.0 (c (n "random_word") (v "0.2.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1jx389hxy4lq02dn45gjhqkf0b7261v75q4ym94vb89p1afijkwq")))

(define-public crate-random_word-0.2.2 (c (n "random_word") (v "0.2.2") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "17y9nwzxlmizgxdy40ffphw821j67fg0949m3m3lbgb365z4hd4c")))

(define-public crate-random_word-0.2.3 (c (n "random_word") (v "0.2.3") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "01px93yvjpvxy0vvcr60z2xymbx39cc2h4y97jqnyw73dval5jqd")))

(define-public crate-random_word-0.2.4 (c (n "random_word") (v "0.2.4") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "0q5vyan7ll08m5ijs950l4l1a67gp2g87012mgir0bzrp88d6ppa")))

(define-public crate-random_word-0.2.5 (c (n "random_word") (v "0.2.5") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1d4cp5dvp99swi4i7840sfv764ww07gfrxh1bgc77pn4rrrx4k84")))

(define-public crate-random_word-0.2.6 (c (n "random_word") (v "0.2.6") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "10vzis8qn0r6fyacxdldnb6z5pmvjk7l17avnw09741k88zl3xh6")))

(define-public crate-random_word-0.3.0 (c (n "random_word") (v "0.3.0") (d (list (d (n "rand") (r "^0") (d #t) (k 0)))) (h "182gnab2q4npp56w68adaxsdpfai4iysa8v7zn16bcq31n1vpclp")))

(define-public crate-random_word-0.3.1 (c (n "random_word") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wvgh4z4jfkqdpja3c4a71jwcr2p18j50xchmc9fk42m2mqp23qx")))

(define-public crate-random_word-0.4.0 (c (n "random_word") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 1)))) (h "1yri6rzlwh1c87ipsvhgpj9sm50q24g92yyhycpcrqvcxw58mpq6") (f (quote (("fr") ("es") ("en") ("de"))))))

(define-public crate-random_word-0.4.1 (c (n "random_word") (v "0.4.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 1)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 1)))) (h "1dfqd81ldws04jg9q8w4wv6bcgjc3a52fhxm6swmbqi9b7q2qfp8") (f (quote (("zh") ("fr") ("es") ("en") ("de"))))))

(define-public crate-random_word-0.4.2 (c (n "random_word") (v "0.4.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "brotli") (r "^3.4.0") (d #t) (k 0)) (d (n "brotli") (r "^3.4.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 1)))) (h "0b3bjx3x6rn1n4yqvq5rkwqcg485r44qj234f8cgfr805i2xyd97") (f (quote (("zh") ("ja") ("fr") ("es") ("en") ("de"))))))

(define-public crate-random_word-0.4.3 (c (n "random_word") (v "0.4.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "brotli") (r "^3.4.0") (d #t) (k 0)) (d (n "brotli") (r "^3.4.0") (d #t) (k 1)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 1)))) (h "0a8zcp3m3f0d69y0d5db98yybcnmv2n75h35gwycrqnx2rxddvh7") (f (quote (("zh") ("ja") ("fr") ("es") ("en") ("de"))))))

