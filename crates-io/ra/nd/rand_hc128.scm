(define-module (crates-io ra nd rand_hc128) #:use-module (crates-io))

(define-public crate-rand_hc128-0.1.0 (c (n "rand_hc128") (v "0.1.0") (d (list (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)))) (h "14k6nr17cld2lf8gxyj9w07lciiy25scr1qaj7563m1azrq75wna") (y #t)))

