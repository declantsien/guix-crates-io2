(define-module (crates-io ra nd randog) #:use-module (crates-io))

(define-public crate-randog-0.1.0 (c (n "randog") (v "0.1.0") (h "0s3kvh9xgrxnp4yqr0ffpwbz5mi1kk64cpcyr4b2ziiqx30fbpv3")))

(define-public crate-randog-0.2.0 (c (n "randog") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bzfzrda6s81yrd6i009wbxx13mb7421d8ix9a7vkglnzjp5dd8s")))

