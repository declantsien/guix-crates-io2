(define-module (crates-io ra nd random_num) #:use-module (crates-io))

(define-public crate-random_num-0.1.0 (c (n "random_num") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "02yg1hag79v852qwzkp6xkj9riz214dllbbj6iwzf9l7i5vc4cdn")))

(define-public crate-random_num-0.2.0 (c (n "random_num") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "029a33ayynyqg439hcgbv51abdzbkb5h34zi0s6pwxj4bv5q4q9g")))

