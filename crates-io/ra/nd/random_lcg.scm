(define-module (crates-io ra nd random_lcg) #:use-module (crates-io))

(define-public crate-random_lcg-0.1.0 (c (n "random_lcg") (v "0.1.0") (h "090y04yf60jm6d3407bciw4ah1x4796dmixjbhzdawdrrnfixdf0") (y #t)))

(define-public crate-random_lcg-0.1.1 (c (n "random_lcg") (v "0.1.1") (h "01b2h91i1pl0pja3vawl17y0zx2ymq75mhkphav2yhy30sibqacm") (y #t)))

(define-public crate-random_lcg-0.2.0 (c (n "random_lcg") (v "0.2.0") (h "14vdrj4pdbydvzz7chfwhiyiwirivqpjrvfmgfvspqcqn7q7c7q9")))

(define-public crate-random_lcg-0.2.1 (c (n "random_lcg") (v "0.2.1") (h "14ixqrziwbnybfwl9n95xsc7sbn14cylxhl7g47a7m20845h5vlm")))

(define-public crate-random_lcg-0.2.2 (c (n "random_lcg") (v "0.2.2") (h "0jyhfzlvz9x5pmg2vg8zmls5rx15krx6qj4ijb2nxp22vbl1f79i")))

