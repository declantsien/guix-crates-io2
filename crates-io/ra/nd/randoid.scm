(define-module (crates-io ra nd randoid) #:use-module (crates-io))

(define-public crate-randoid-0.1.0 (c (n "randoid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "smartstring") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1y53fzc47hz9gza7sa81n3miy2kqdxn9zv1ra9iggkd3xwcm45rd") (f (quote (("std-rand" "std" "rand" "rand/std" "rand/std_rng") ("std") ("default" "std-rand") ("alloc"))))))

(define-public crate-randoid-0.2.0 (c (n "randoid") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (k 0)) (d (n "smartstring") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "1wcbfvapapiybsglq7hgmgsd6nqjvx8cpsg84bbbgq734njh8gvw") (f (quote (("std-rand" "std" "rand/std" "rand/std_rng") ("std") ("default" "std-rand") ("alloc")))) (s 2) (e (quote (("smartstring" "dep:smartstring"))))))

(define-public crate-randoid-0.3.0 (c (n "randoid") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (k 0)) (d (n "smartstring") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "0d62aswy8mljm88axnfclzkq4bcj5jdg2kyfzh1fbbpyd4s1m09h") (f (quote (("std-rand" "std" "rand/std" "rand/std_rng") ("std") ("default" "std-rand") ("alloc")))) (s 2) (e (quote (("smartstring" "dep:smartstring"))))))

