(define-module (crates-io ra nd random-names) #:use-module (crates-io))

(define-public crate-random-names-0.1.0 (c (n "random-names") (v "0.1.0") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1jp7abd5gnl920ji9g8nf143vsqzzy4z55ff68v0hi6rfrw8pkv8")))

(define-public crate-random-names-0.1.1 (c (n "random-names") (v "0.1.1") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1zwn2m32xp47cwqsry3nb8i2cy06cxrgjg1r9byibvl2yy1ns3f1")))

(define-public crate-random-names-0.1.2 (c (n "random-names") (v "0.1.2") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "17zd8k1z1zh2cn48b3igmpfnqfmsnscrpvc717kr1vmxs6mk4ibm")))

(define-public crate-random-names-0.1.3 (c (n "random-names") (v "0.1.3") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1igv0j7s8vmd4wv6av6qnd9ijjaqpckakkr8hapm81245mjgvkra")))

