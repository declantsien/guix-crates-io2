(define-module (crates-io ra nd rand-wyrand) #:use-module (crates-io))

(define-public crate-rand-wyrand-0.1.0 (c (n "rand-wyrand") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0xrfqpbz34nxihzbxxrhnvvca61gcc6rnkcabcczr9v39jzv4bws")))

