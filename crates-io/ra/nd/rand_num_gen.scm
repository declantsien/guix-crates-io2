(define-module (crates-io ra nd rand_num_gen) #:use-module (crates-io))

(define-public crate-rand_num_gen-0.1.0 (c (n "rand_num_gen") (v "0.1.0") (d (list (d (n "imagefmt") (r "^4.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "01vcri06p7bjpbgmj9cfal182v50wrxqw03rapxhf5p2cg90j2wj") (f (quote (("use_xorshift") ("default" "use_xorshift"))))))

