(define-module (crates-io ra nd random_str) #:use-module (crates-io))

(define-public crate-random_str-0.1.0 (c (n "random_str") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "057n6malk2h6pz5wwh8r4i6nxjbv3nnxxrkvpkp5njzq6s37crg6") (y #t)))

(define-public crate-random_str-0.1.1 (c (n "random_str") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fnx2nbcvfqd023wz1j113961j4zka7drnwbfxwfh5nvyssv3nzd")))

(define-public crate-random_str-0.1.2 (c (n "random_str") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bxk1gyr6fh41y8gvs100caacnvpha7r304197dx3jwsja10cs2g")))

