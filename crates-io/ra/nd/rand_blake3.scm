(define-module (crates-io ra nd rand_blake3) #:use-module (crates-io))

(define-public crate-rand_blake3-0.1.0 (c (n "rand_blake3") (v "0.1.0") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "0laz7ck0cicaas3l1kd6hn53z91y5vgnpj7vgfnwwn23i1sqa2hp")))

(define-public crate-rand_blake3-0.1.1 (c (n "rand_blake3") (v "0.1.1") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "0c2xn02xnnqyy075nfyg7zn9hf49zz38yfzs1w4bd7rr4kjad4g3")))

(define-public crate-rand_blake3-0.1.2 (c (n "rand_blake3") (v "0.1.2") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "1cl7q1p7gpdx2zpa3sijxinpps4npzkvrb2dywwbf4j80bcx0q9i")))

(define-public crate-rand_blake3-0.1.3 (c (n "rand_blake3") (v "0.1.3") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "1z19833dnk9k1rpbln5pg63vwj8my68x95gjk0pwng56v0mzykcy")))

(define-public crate-rand_blake3-0.1.4 (c (n "rand_blake3") (v "0.1.4") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)))) (h "0mfkc3gqx3jc960zjhbkz1cdyk3dvjnr202xf2s0zvi56bi809ca")))

(define-public crate-rand_blake3-1.0.0 (c (n "rand_blake3") (v "1.0.0") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "load-buffer") (r "^0.1.2") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "039yhn7xzlavqcj2zadcnhmsc3dk960rx7pgfpngf5i2dg0prgn2")))

(define-public crate-rand_blake3-1.0.1 (c (n "rand_blake3") (v "1.0.1") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "load-buffer") (r "^1.0.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "1g4lsl3h7y137y176ivdz2f4v6bgz0p4cz9p3xcldippnvmx3sf5")))

(define-public crate-rand_blake3-1.0.2 (c (n "rand_blake3") (v "1.0.2") (d (list (d (n "blake3") (r "^1.5.0") (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "load-buffer") (r "^1.0.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std" "small_rng"))) (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)))) (h "0rrdnx5ybny4iw37bj0asgiqg0qk0kys7yk4jfb46sqi5628xm25")))

