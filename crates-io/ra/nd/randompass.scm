(define-module (crates-io ra nd randompass) #:use-module (crates-io))

(define-public crate-randompass-0.0.1 (c (n "randompass") (v "0.0.1") (h "0nllvil9fjqslys79ygzwax6ck152rx6aizpy44brjws8xdqxq6f") (y #t)))

(define-public crate-randompass-0.0.2 (c (n "randompass") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0rxq1rdnx96r4zfkq9jkz4a661vmn8rgfbvv1xd6vzpp00w8lxj3") (y #t)))

(define-public crate-randompass-0.0.3 (c (n "randompass") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0l4sybhbzkfcplhhh5ixzwz46d646qxyazmcsr890kbb4564pd1k") (y #t)))

(define-public crate-randompass-0.0.4 (c (n "randompass") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ylpvmqj7s7781w6skgvs8ikj6bxkxdr4irrnbbcfn8xdfng4pmf") (y #t)))

(define-public crate-randompass-0.1.0 (c (n "randompass") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "09rs3qvlmlnprpivbm6akkq446xjrn35jcjnl8skaag7q4bc6fzg") (y #t)))

(define-public crate-randompass-0.1.1 (c (n "randompass") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1rgkq6vw69p7ld1q17pyvyy1d6zcwgq8k0d1b29v8czy1j9fbpkc") (y #t)))

(define-public crate-randompass-0.1.2 (c (n "randompass") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0109yg6358yfmck2xi1dijvr8r49xfcr3zk3vqq3x6z61mj8v7h5")))

(define-public crate-randompass-0.2.0 (c (n "randompass") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "191kzv8m8pnyvzfxbxs544zwd72la45h9nra5c0763y6040n7am3")))

(define-public crate-randompass-0.2.1 (c (n "randompass") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0frl9wz8df4vz2xx5cvi9x2wan3rvizk29wv21id6809ca6zkdbf")))

(define-public crate-randompass-0.2.2 (c (n "randompass") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02n76swkrp8wyj9fxhx3m419g48ya5gbmn8w2w3ncxwj94j0gm9h")))

(define-public crate-randompass-0.2.3 (c (n "randompass") (v "0.2.3") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "179pa5cq6bnsxh32j0r2r4imdp22s97ijbpcczfmj012m1afvwhw")))

(define-public crate-randompass-0.2.4 (c (n "randompass") (v "0.2.4") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1j5rp5mahksma7wxsqgpsaj7ci3hgvj5xanag6frc15iff8ijxs7")))

(define-public crate-randompass-0.2.5 (c (n "randompass") (v "0.2.5") (d (list (d (n "autoclap") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0l8ylv67543jjidq8nljmn4jx5nz1fzqpfzhc1n0bbxakllsswsz")))

(define-public crate-randompass-1.0.0 (c (n "randompass") (v "1.0.0") (d (list (d (n "autoclap") (r "^0.3.11") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("cargo" "string"))) (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1x845sw68wg7z4ij5kp39q0rb2wr9f2hyaj7iji1yxkjcmj3sv0s")))

