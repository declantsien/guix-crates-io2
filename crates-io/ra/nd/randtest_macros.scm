(define-module (crates-io ra nd randtest_macros) #:use-module (crates-io))

(define-public crate-randtest_macros-0.1.0 (c (n "randtest_macros") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "04s3ldlxcxnm29bp69lrnawf01i0khr64l5fm0wy3771m3098ng6") (y #t)))

(define-public crate-randtest_macros-0.1.1 (c (n "randtest_macros") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1pafmcl30sbf9j0gyh941spqk4rcsdap60fis9vpws5s9596378s") (y #t)))

