(define-module (crates-io ra nd random-number) #:use-module (crates-io))

(define-public crate-random-number-0.1.0 (c (n "random-number") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1n0hzk0xsga9wc4waxd2idc7v74i8lz4snfqwfaxb7ygakg7m4nw")))

(define-public crate-random-number-0.1.1 (c (n "random-number") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "10isli0k86vqq3knhkwhyikh9ggc78536p69sq1z40f2rh4g1y9a")))

(define-public crate-random-number-0.1.2 (c (n "random-number") (v "0.1.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1c8nfhrg1hqz2ljz86scvryj7s75zfm0w0zfq307hn6r01jq3nw8")))

(define-public crate-random-number-0.1.3 (c (n "random-number") (v "0.1.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1ahvsdkbmqlgf441aycnwz94457yvmxjbjm5whlgk2pj3r9w2b29")))

(define-public crate-random-number-0.1.4 (c (n "random-number") (v "0.1.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "17c6zma9a0f3iq95bnvqbbg2xwdc8vjdza61791shqj2qnmx04dz")))

(define-public crate-random-number-0.1.5 (c (n "random-number") (v "0.1.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0cd8qlkb8rg7svwnxzb40qf1p9fwwg5pp461gc2vpxk7ddvkvinx")))

(define-public crate-random-number-0.1.6 (c (n "random-number") (v "0.1.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0mrdc6xcm2jg80nfp3v2m8g85i0gprrh2c60pmz56lzps8d7jv0b")))

(define-public crate-random-number-0.1.7 (c (n "random-number") (v "0.1.7") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "0ybk8y48b50rrpxdm78mrg170j4yr1iifxwz37hbvmibfm54776b")))

(define-public crate-random-number-0.1.8 (c (n "random-number") (v "0.1.8") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "random-number-macro-impl") (r "^0.1") (d #t) (k 0)))) (h "1gmp9ca9m20bgijswag3z68csdslfkjafm1sq1852z62nk5sag9s")))

