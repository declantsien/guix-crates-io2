(define-module (crates-io ra nd randcli) #:use-module (crates-io))

(define-public crate-randcli-0.1.0 (c (n "randcli") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07sksc4f2nrkf10z9m0068wzqyd4yj8clkjlfihy60l95g89rswz")))

(define-public crate-randcli-0.1.1 (c (n "randcli") (v "0.1.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ywwdc1qrd10g27gdvr24kfac0yz7jj5g416l5vbfkqfprifzh86")))

(define-public crate-randcli-0.1.2 (c (n "randcli") (v "0.1.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0r6sndmlq1sdd78gg8p8ah2il7faw5br1gyh29ifl5xqniprh2p0")))

(define-public crate-randcli-0.1.3 (c (n "randcli") (v "0.1.3") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1f5lp23ph1alki33nir3fk0cjzyawfw1km2hzcj0j0963zfjd43b")))

(define-public crate-randcli-0.1.4 (c (n "randcli") (v "0.1.4") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xy0vh9kwr463ikmrs725fz8ilb7g4yyy7n6r4bw83mfqkxzvyxs")))

(define-public crate-randcli-0.1.5 (c (n "randcli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02lyfhsmfmd6dldf9nlfinbqqf909hjdhkqn3v3mamigfk74anv7")))

(define-public crate-randcli-0.1.6 (c (n "randcli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jfxnal0nfzw7psjdy23a7sb4y1sxkalaqmia5806zpsna2lql1w")))

