(define-module (crates-io ra nd rand_fruits) #:use-module (crates-io))

(define-public crate-rand_fruits-0.1.0 (c (n "rand_fruits") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0k29wmxbh3mh4ygyv564cx0130r2mc39zhwm7v2wn29k38byyf0v") (y #t)))

(define-public crate-rand_fruits-0.1.1 (c (n "rand_fruits") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yrzlc9v3v7dgcfiw0l1cqwbsqirpwbdpy7qmyb8wj59ihr2xiha")))

