(define-module (crates-io ra nd randomoracle) #:use-module (crates-io))

(define-public crate-randomoracle-4.0.1 (c (n "randomoracle") (v "4.0.1") (d (list (d (n "ink") (r "^4.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (o #t) (k 0)))) (h "12fyqcrpj1097a7d1hdysr67z43dqkjxby58j18r7vwmif6kqr0p") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-randomoracle-4.0.2 (c (n "randomoracle") (v "4.0.2") (d (list (d (n "ink") (r "^4.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (o #t) (k 0)))) (h "097k2y4zcp46830g0n4n6129sxdq065fxn4qhmrdlyaj734igyw8") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-randomoracle-4.0.3 (c (n "randomoracle") (v "4.0.3") (d (list (d (n "ink") (r "^4.0") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2.3") (f (quote ("derive"))) (o #t) (k 0)))) (h "1gpaja1ar8l4ln81m5c10p1ls16czgf87zfwb110np666qgfpxdj") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

