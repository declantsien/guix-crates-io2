(define-module (crates-io ra nd rand_chacha) #:use-module (crates-io))

(define-public crate-rand_chacha-0.1.0 (c (n "rand_chacha") (v "0.1.0") (d (list (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0jnk1m281sfkfzxjjs61q5xqqdm5m9aa9p93i1zbd32h7ag006vp")))

(define-public crate-rand_chacha-0.1.1 (c (n "rand_chacha") (v "0.1.1") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)))) (h "1vxwyzs4fy1ffjc8l00fsyygpiss135irjf7nyxgq2v0lqf3lvam")))

(define-public crate-rand_chacha-0.2.0 (c (n "rand_chacha") (v "0.2.0") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "c2-chacha") (r "^0.2.2") (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "178d36jfkc4v95s25scc2vibj2hd2hlk64cs6id4hvzg89whd4z1") (f (quote (("std" "c2-chacha/std") ("simd" "c2-chacha/simd") ("default" "std" "simd"))))))

(define-public crate-rand_chacha-0.2.1 (c (n "rand_chacha") (v "0.2.1") (d (list (d (n "c2-chacha") (r "^0.2.2") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0lv8imzzl4h2glm6sjj8mkvasgi8jym23ya48dakyln7m06sk8h3") (f (quote (("std" "c2-chacha/std") ("simd") ("default" "std" "simd"))))))

(define-public crate-rand_chacha-0.2.2 (c (n "rand_chacha") (v "0.2.2") (d (list (d (n "ppv-lite86") (r "^0.2.6") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "00il36fkdbsmpr99p9ksmmp6dn1md7rmnwmz0rr77jbrca2yvj7l") (f (quote (("std" "ppv-lite86/std") ("simd") ("default" "std" "simd"))))))

(define-public crate-rand_chacha-0.3.0 (c (n "rand_chacha") (v "0.3.0") (d (list (d (n "ppv-lite86") (r "^0.2.8") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)))) (h "03df2xh5nbdvwr17qm3sviaxa95r8yhm1nil2pr0pqf90p7ka9z1") (f (quote (("std" "ppv-lite86/std") ("simd") ("default" "std"))))))

(define-public crate-rand_chacha-0.3.1 (c (n "rand_chacha") (v "0.3.1") (d (list (d (n "ppv-lite86") (r "^0.2.8") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "123x2adin558xbhvqb8w4f6syjsdkmqff8cxwhmjacpsl1ihmhg6") (f (quote (("std" "ppv-lite86/std") ("simd") ("serde1" "serde") ("default" "std"))))))

(define-public crate-rand_chacha-0.9.0-alpha.0 (c (n "rand_chacha") (v "0.9.0-alpha.0") (d (list (d (n "ppv-lite86") (r "^0.2.14") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "=0.9.0-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1y3xz975g3gw2h4px18dcxmv6s8c3s9a0xwfwhjw4fnb79r42p0v") (f (quote (("std" "ppv-lite86/std" "rand_core/std") ("simd") ("serde1" "serde") ("default" "std")))) (r "1.60")))

(define-public crate-rand_chacha-0.9.0-alpha.1 (c (n "rand_chacha") (v "0.9.0-alpha.1") (d (list (d (n "ppv-lite86") (r "^0.2.14") (f (quote ("simd"))) (k 0)) (d (n "rand_core") (r "=0.9.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0insyvqs1n4gnanz79ls9kv99d0rhqgj1y2hsbdm356133wlwrvq") (f (quote (("std" "ppv-lite86/std" "rand_core/std") ("simd") ("serde1" "serde") ("default" "std")))) (r "1.60")))

