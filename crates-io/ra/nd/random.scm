(define-module (crates-io ra nd random) #:use-module (crates-io))

(define-public crate-random-0.0.1 (c (n "random") (v "0.0.1") (h "12hbl2p48fw0d6mw3c8fsxxj797220s50xirxkkzzm3505lawhbn")))

(define-public crate-random-0.1.0 (c (n "random") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0633xqwkw6ccf9dscf80ia20sfg6hsyc33h5w6mv90wbzsvx9968")))

(define-public crate-random-0.2.0 (c (n "random") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1l14f4xwq543xxif5mgvrvnijmys7zz066y6sgqv6lxr4n026d9q")))

(define-public crate-random-0.3.0 (c (n "random") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0av0m40yjvl29sh0wizgkvr42dfyajzzr6g0hn7m06jlc9asnckr")))

(define-public crate-random-0.4.0 (c (n "random") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "0wbsx0m0wwr8qa2qphjpyx2zga1v7b057hslmayd8532ndzn611i")))

(define-public crate-random-0.5.0 (c (n "random") (v "0.5.0") (h "0qkfwlpjjnmx30la3i9wcka703b8jsy2s9v14lb8r8md4mgrwdxh")))

(define-public crate-random-0.6.0 (c (n "random") (v "0.6.0") (h "01crinrri4gkq819w02hryazj6czfclv7avg5fd1kxszg7iych1f")))

(define-public crate-random-0.6.1 (c (n "random") (v "0.6.1") (h "1qwc5kghawpdh8l5m4vfhsh8hwi64nz1rjx5gw2a276fc0qv2ih1")))

(define-public crate-random-0.6.2 (c (n "random") (v "0.6.2") (h "0m96qsvky3y46h6riqp9z555g04fvsrddnfsxagqnzfi1qi45b10")))

(define-public crate-random-0.7.0 (c (n "random") (v "0.7.0") (h "0wjxh0cl2l9laz7b1bwwxxhm30hi36kvhj3n4zcxippi10drz8sb")))

(define-public crate-random-0.8.0 (c (n "random") (v "0.8.0") (h "1zr7pi48wyiq3hcg8sl4szkj49wg62za0fs2ysiqsflwhzaznml8")))

(define-public crate-random-0.9.0 (c (n "random") (v "0.9.0") (h "04j95zvppqjcc8r9maac0ksl650q05h5d30xzj5whdmpiqsymndv")))

(define-public crate-random-0.9.1 (c (n "random") (v "0.9.1") (h "0dwqjzv9hzh6acbrhglcqnap7igl6ylm7vyyy07akm3kvbav3ypb")))

(define-public crate-random-0.9.2 (c (n "random") (v "0.9.2") (h "04zn0d1ihsyk9bnmpyr8v6w3w54xfc5ncxr8gafsac998ii0jgrd")))

(define-public crate-random-0.10.0 (c (n "random") (v "0.10.0") (h "0b8ki9lxasf69x1fpkdsi59543qcigjrybjlw41icyd5c7f2ldpl")))

(define-public crate-random-0.10.1 (c (n "random") (v "0.10.1") (h "0k5cik14idj8kwa486w8gwdf3bfnhvrjw26npb5yfmcpkh23ygnc")))

(define-public crate-random-0.11.0 (c (n "random") (v "0.11.0") (h "0rb1139gcm9alz6avz5q4wlchrfakx38kwq5xmivb49z94fyq6p6")))

(define-public crate-random-0.12.0 (c (n "random") (v "0.12.0") (h "1j3x4hxx1kqs73kgxwzlsdsi2hx0xc79bg1rfk3zyy9cdb4gq8cn") (y #t)))

(define-public crate-random-0.12.1 (c (n "random") (v "0.12.1") (h "0k8v5nxnjrsxjhz266jm8xaqdzwk5jdg0fq6xikq4fg0hxf58sms")))

(define-public crate-random-0.12.2 (c (n "random") (v "0.12.2") (h "03s2c59vzcr5fmxbhlhxvrsnwgic488jl4br1k4q369lhls3mlcp")))

(define-public crate-random-0.13.0 (c (n "random") (v "0.13.0") (h "1lr256rmdp9spx4cd81h96p7xjyqd460g3h6qk9fc92qj7lyk7lq") (y #t)))

(define-public crate-random-0.13.1 (c (n "random") (v "0.13.1") (h "0nppjra2mjzfxkpsc2k18br750721rm4g8vxq3qqrf49iddk9i5i") (y #t)))

(define-public crate-random-0.13.2 (c (n "random") (v "0.13.2") (h "016milxvjh11q2jcj73nnpzr4phf38ggf0jsb4mgwkgh0k4l4k27")))

(define-public crate-random-0.14.0 (c (n "random") (v "0.14.0") (h "125xmlmpp1byaf2hv1rsss6gbsfvh9j5q5jb1pqlhnvhh8x0kiv1")))

