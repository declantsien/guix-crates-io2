(define-module (crates-io ra nd rand_server) #:use-module (crates-io))

(define-public crate-rand_server-0.1.0 (c (n "rand_server") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1nhii4dh1md0vgbpizz7ib90yjj2178zl4j37dbqc5lm1igwxw05")))

(define-public crate-rand_server-0.1.1 (c (n "rand_server") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1gkcb5kgnfz2091azh907vv2cvwsi4gnnsg4yvz2d66zni8g5qvm")))

