(define-module (crates-io ra nd random-pick) #:use-module (crates-io))

(define-public crate-random-pick-1.0.0 (c (n "random-pick") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0nn0yznqqlh89ffk2n78br9fx8cdqdcc1hfzci83jmp892mjwccc")))

(define-public crate-random-pick-1.0.1 (c (n "random-pick") (v "1.0.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "036qcvnzx71rf31wfc7vsl0i2mc2ak9lipwpwxhi4gvsqhcl7n78")))

(define-public crate-random-pick-1.0.2 (c (n "random-pick") (v "1.0.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1p1z2237ly3cad5n0ick3gl8h2b4ijg9z3ffbzsn2nnqb52dl70b")))

(define-public crate-random-pick-1.0.3 (c (n "random-pick") (v "1.0.3") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "00ycdvjxmjbzyiz6sd40mpfb64vn1j6f4kqfznh4kkz19idqbc3q")))

(define-public crate-random-pick-1.1.0 (c (n "random-pick") (v "1.1.0") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "0k06x2mn5210ynyh5vfbh39bqk1pci289m3b8rvs2j7h6ikxbv3s")))

(define-public crate-random-pick-1.1.1 (c (n "random-pick") (v "1.1.1") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "0x3h819bnjvcyvics7dhci1c98cmycrm7wzinj4yhvn3f3rqkg1b")))

(define-public crate-random-pick-1.2.0 (c (n "random-pick") (v "1.2.0") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "1c359hskyhb20l06v7phd4lg45wqlbw3x8psf0jcc9d1a84892xf")))

(define-public crate-random-pick-1.2.1 (c (n "random-pick") (v "1.2.1") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "12n289rd6rfnbndg0mchar3s7ssncmai6x7fn2aj51g8i56ga1ws")))

(define-public crate-random-pick-1.2.2 (c (n "random-pick") (v "1.2.2") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "13ykg5nmf6lklwfwda7m1iyqib4q5k4mvqf84hrqmgvzqdjm4bkd")))

(define-public crate-random-pick-1.2.3 (c (n "random-pick") (v "1.2.3") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "0qk8zxayyr0cna4f2ywc2zpb3d2d3mpbyc163p1bd24nn04m32pm")))

(define-public crate-random-pick-1.2.4 (c (n "random-pick") (v "1.2.4") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "0xidvm43h3w06vg98551c5br004jf04kqkmrhhg7xnaw2rz78fc0")))

(define-public crate-random-pick-1.2.5 (c (n "random-pick") (v "1.2.5") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "1m33qdgwqvm82vg0673xbx3ajjnj0ynimjqy144m3fzxh0h1akfn")))

(define-public crate-random-pick-1.2.7 (c (n "random-pick") (v "1.2.7") (d (list (d (n "random-integer") (r "^1.0.0") (d #t) (k 0)))) (h "1wjzb45a990d3g89jfrv3wbrl9943xp5249lnv13giz2qhw585h7")))

(define-public crate-random-pick-1.2.8 (c (n "random-pick") (v "1.2.8") (d (list (d (n "random-integer") (r "^1.1.1") (d #t) (k 0)))) (h "0rfbsbkgs4j6sn2fdcwk4iyarl5b3dg7zpn0hzalzcs1fin0hvr9")))

(define-public crate-random-pick-1.2.9 (c (n "random-pick") (v "1.2.9") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "14k7m2inrnjr94pvgxpwq7v4x419f4p19h5xvv64vb7kpm931w13")))

(define-public crate-random-pick-1.2.10 (c (n "random-pick") (v "1.2.10") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "0l2bgdfbsmnzlid7qsvrqr1qgi708jbxx9d0cpmnmr28qy942sbh")))

(define-public crate-random-pick-1.2.11 (c (n "random-pick") (v "1.2.11") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "0s6fgklf3ga95ky0099lifkmdak8phyqkw2mhl7fa0sxfkpvrglf")))

(define-public crate-random-pick-1.2.12 (c (n "random-pick") (v "1.2.12") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "12p5q5iavcwi5lmaa8nhqpp55v9ix5f9ry4krk7msyafhy62i7nm")))

(define-public crate-random-pick-1.2.13 (c (n "random-pick") (v "1.2.13") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "1kznhc8dn3893m30ysc6vqza9fjvvx8c852csq40bwjxz0vzzfk0")))

(define-public crate-random-pick-1.2.14 (c (n "random-pick") (v "1.2.14") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "1yjfgv3aj1v0pp9hwwa1v5myxb5zrm9k91b9k3ak7az64q84f3j2")))

(define-public crate-random-pick-1.2.15 (c (n "random-pick") (v "1.2.15") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "1li5p9qz4fd8khc21rq86il940n1gm6w0zn9y7wsa6am3yks4fb2")))

(define-public crate-random-pick-1.2.16 (c (n "random-pick") (v "1.2.16") (d (list (d (n "random-number") (r "^0.1") (d #t) (k 0)))) (h "1y50la3p7cwn06cmkxvfz71f4b81lr55yz8j8kz9ly6sfa84jyf1")))

