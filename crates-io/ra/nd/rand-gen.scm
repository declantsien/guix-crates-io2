(define-module (crates-io ra nd rand-gen) #:use-module (crates-io))

(define-public crate-rand-gen-0.1.0 (c (n "rand-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-gen-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "091m126xvjprb7and7pymh5c23212inbf7ars53irsixsfd7yh89")))

(define-public crate-rand-gen-0.1.1 (c (n "rand-gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand-gen-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "19j337f0i9pfkpdkfhiq9kn6zchanzgqv1c2pi5235k5qcfyjzqq")))

