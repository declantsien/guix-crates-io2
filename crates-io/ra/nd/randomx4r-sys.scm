(define-module (crates-io ra nd randomx4r-sys) #:use-module (crates-io))

(define-public crate-randomx4r-sys-0.1.0 (c (n "randomx4r-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1ib3lbmpwhr7chfanb2pyy11c1yqzainzm6vd320ri0ksplzpbqz")))

(define-public crate-randomx4r-sys-0.1.1 (c (n "randomx4r-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0x7f9drxc5p5d9a0gw901ssqj6rf8xd2q4zdg3lvc62v9mq8kfwi")))

