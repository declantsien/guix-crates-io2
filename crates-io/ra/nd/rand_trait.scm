(define-module (crates-io ra nd rand_trait) #:use-module (crates-io))

(define-public crate-rand_trait-0.0.1 (c (n "rand_trait") (v "0.0.1") (h "0xhgi5d464jahmcn53qq8s3nlnya6nv5xzrv9jk743sai09b4j3d") (y #t)))

(define-public crate-rand_trait-0.0.2 (c (n "rand_trait") (v "0.0.2") (h "0s4msrfsfr94fygn9vfwf1dis4848y5jiz273w5mkc4jm8l8lg9k") (y #t)))

(define-public crate-rand_trait-0.0.3 (c (n "rand_trait") (v "0.0.3") (h "048qb51yqp16yjsikz2bds3qhi0dg5ayp6yy6qj3v2x7ipqixn50") (y #t)))

(define-public crate-rand_trait-0.0.4 (c (n "rand_trait") (v "0.0.4") (h "0ynh1322bvxqjdj106qcds10iqf07g0i8c00qxk894yyxn4faxb0") (y #t)))

(define-public crate-rand_trait-0.0.5 (c (n "rand_trait") (v "0.0.5") (h "0nzg7frhpa44mjbk668a094zx1i2nvxk43yp9lndz9jmw4fq6v8s") (y #t)))

(define-public crate-rand_trait-0.0.6 (c (n "rand_trait") (v "0.0.6") (h "0y7vcpjyy0sr38zl5ngwnsqvwk88ckhnri61lz9y6n7v53hbm3r1") (y #t)))

