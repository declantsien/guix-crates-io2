(define-module (crates-io ra nd randomx-bindings-sys) #:use-module (crates-io))

(define-public crate-randomx-bindings-sys-0.1.2 (c (n "randomx-bindings-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "19552j50q927mchz32lbi3hnjsx7309xrm0wxzlysq3zv18jzzlr")))

(define-public crate-randomx-bindings-sys-0.1.3 (c (n "randomx-bindings-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0qkv4bx0k2gzl97m3066xncbd9j4plr9ly4a1aq47vcpwi3p5968")))

(define-public crate-randomx-bindings-sys-0.1.4 (c (n "randomx-bindings-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "07fckz1s143hvys9kv59yyr1vg5z714yqgfva8mppx5r4a1s4qmf")))

(define-public crate-randomx-bindings-sys-0.1.5 (c (n "randomx-bindings-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0h3i9vkkmz3pnwawpvdwwsvd9sp45n1dzrg6lmigv7d82ssw90rk")))

