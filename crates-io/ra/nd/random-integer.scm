(define-module (crates-io ra nd random-integer) #:use-module (crates-io))

(define-public crate-random-integer-1.0.0 (c (n "random-integer") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "18dz7yrvjx3m5nizx1100kblc6gh23p881lvyw3prv3lylh39ax1")))

(define-public crate-random-integer-1.0.1 (c (n "random-integer") (v "1.0.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0398qkqfs13qsj95l96wialqfiv95s57bi9nbsrpl5ljfah4paby")))

(define-public crate-random-integer-1.0.2 (c (n "random-integer") (v "1.0.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0f9mfjvpm1nax2rilciah9h17vkip22vixlx9qhb2gl524wbxww4")))

(define-public crate-random-integer-1.0.3 (c (n "random-integer") (v "1.0.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1bi7iq1b337n0kajm71080jinak5ws79rs9s58fh19ipbjsx9r8q")))

(define-public crate-random-integer-1.0.4 (c (n "random-integer") (v "1.0.4") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1zgs11c8g4vggqvsmzszk3ilvi789iq4rd4w891lnd77jj12rhb4")))

(define-public crate-random-integer-1.0.5 (c (n "random-integer") (v "1.0.5") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1pmqcych7mj0yj459q7hzv4k79gp3qrlhij5grbhp41vbfa7gab5")))

(define-public crate-random-integer-1.1.0 (c (n "random-integer") (v "1.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1b649d3pnxbf4mzwqf209vilja0j5mr4yz64gpksqdhkzill0lwq")))

(define-public crate-random-integer-1.1.1 (c (n "random-integer") (v "1.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1yg8p21dwzw9q5qdsigrzjckahgwp0wwkd8xl9agyrsc6cc5pcwb")))

(define-public crate-random-integer-1.2.0 (c (n "random-integer") (v "1.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0282gq0l20p3d98lb3y6jhs0rsdmyl2zlfdwbx7v81yr1s6zz0b0") (y #t)))

(define-public crate-random-integer-1.2.1 (c (n "random-integer") (v "1.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1mw1spih7zjz1qw02av11kdnbph53h04c3r60mgrixb84hv2lz18")))

