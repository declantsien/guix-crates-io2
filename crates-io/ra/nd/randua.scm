(define-module (crates-io ra nd randua) #:use-module (crates-io))

(define-public crate-randua-0.1.0 (c (n "randua") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "05fcb7xs28siffv517xb938lsgcsic0yi6m0fks1rcikh5g3zcrx")))

(define-public crate-randua-0.1.2 (c (n "randua") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "04xfx5p566rd4m4fzdsyij3s13yki40d83aq6g1iq9xbx53faiaz")))

(define-public crate-randua-0.1.3 (c (n "randua") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0g9h49q3cs6a66aphk82ia2a8h0ssdh220ph64z2cfd2hwdcyypa")))

(define-public crate-randua-0.1.5 (c (n "randua") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "030x2i2gibxbpcfy0p3p2aj2k1215fqc9841zqxdp32xn8lhdhr8")))

(define-public crate-randua-0.1.6 (c (n "randua") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0zm47sw6lhriy573zfi3bnmjbb4d9cyxn7s23d709c2rm1cdf16v")))

