(define-module (crates-io ra nd rand_funcs) #:use-module (crates-io))

(define-public crate-rand_funcs-0.1.0 (c (n "rand_funcs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hcwcjgda2m3kpbr92ahckn1pz7g26vx3i9qnjjxdj9s6rycr6h5")))

(define-public crate-rand_funcs-0.1.1 (c (n "rand_funcs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0yjpxxhykszxzkr53y2q6wfn50w17pq7vbpl4cqrbp5gq0bb0k2h")))

(define-public crate-rand_funcs-0.1.2 (c (n "rand_funcs") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1j35599j38bsjbyxkviy6ijn46869gznn7bg0qlg71awh06v424n")))

(define-public crate-rand_funcs-0.1.3 (c (n "rand_funcs") (v "0.1.3") (d (list (d (n "rand") (r "0.*.*") (d #t) (k 0)))) (h "1mwy229gy3ydym9g402zh9hfdi0hwx4rdxm6qnyy8p5faagbwdhh")))

