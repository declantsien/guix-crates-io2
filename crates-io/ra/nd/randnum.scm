(define-module (crates-io ra nd randnum) #:use-module (crates-io))

(define-public crate-randnum-0.1.0 (c (n "randnum") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1c7ijcff4ijr673b419w75857nx2hjxbc26m81lnn3adcd3nfvxb")))

(define-public crate-randnum-0.2.0 (c (n "randnum") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0fv5syvsgsda07cs6vvwn5hxkzxx1qkdfns56wj2ifig03cb8vc5")))

