(define-module (crates-io ra nd random-branch) #:use-module (crates-io))

(define-public crate-random-branch-0.1.0-alpha.1 (c (n "random-branch") (v "0.1.0-alpha.1") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "11lzvcsiccgq97g24vkw8p904msrbca8rj3cssg1b5xbznll6cms") (f (quote (("std" "rand/std" "rand/std_rng") ("doc_cfg") ("default" "std"))))))

(define-public crate-random-branch-0.1.0 (c (n "random-branch") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "0a15mnylnqixf5fvwnhwwjwf0dwhhrr1qlk4rnryb677z8wfgzq8") (f (quote (("std" "rand/std" "rand/std_rng") ("doc_cfg") ("default" "std"))))))

(define-public crate-random-branch-0.1.1 (c (n "random-branch") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)))) (h "1sb1s10cmy9b5ngyxwksl4w3rr20ifxxgjs6cd4631ma1pzpf7y0") (f (quote (("std" "rand/std" "rand/std_rng") ("doc_cfg") ("default" "std"))))))

