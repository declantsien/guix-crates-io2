(define-module (crates-io ra nd random_ayah) #:use-module (crates-io))

(define-public crate-random_ayah-0.1.0 (c (n "random_ayah") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wa8v2dwkyxrkh0yfh0liq9jb46ngl8wyj7dsgk1wpizwp79za8v")))

(define-public crate-random_ayah-0.1.1 (c (n "random_ayah") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0rkxsmwd9c153rpnlc6r57x0zsqkckzndmx4jaqqb1w5a026kn61")))

