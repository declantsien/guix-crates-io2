(define-module (crates-io ra nd randbats-winrates) #:use-module (crates-io))

(define-public crate-randbats-winrates-0.1.0 (c (n "randbats-winrates") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1l0gpfs8nppz6xl28kvq27c3sfjrxqzp2iy3a59fbwq7azqx0dya")))

(define-public crate-randbats-winrates-0.1.1 (c (n "randbats-winrates") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1vffy1znavl47z7blsf7p75flbwlp69133lx3nc6cs3si5c1nknd")))

(define-public crate-randbats-winrates-0.1.2 (c (n "randbats-winrates") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "06zp492axzz4fr3m2jy63h6z4gyz0vx4q7vlp6s4wiaihwr72kqx")))

(define-public crate-randbats-winrates-0.2.0 (c (n "randbats-winrates") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0lqnzyhac3vbmdwxr42a896dvzqyh6qgwnhqdyn3z1ah6vs3hy4c")))

(define-public crate-randbats-winrates-0.2.1 (c (n "randbats-winrates") (v "0.2.1") (d (list (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "pikkr-annika") (r "^0.16.1") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "04qzj50c9l1am86ifxiwh8f67wri33ml3j6l7m3qzkfpb1z2bw4g")))

(define-public crate-randbats-winrates-0.3.0 (c (n "randbats-winrates") (v "0.3.0") (d (list (d (n "gjson") (r "^0.8.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1ykyfjn8i5j7xwk9pmpnmip5vix8m68w5m1bbzy1biyrmyrak3x1")))

