(define-module (crates-io ra nd rand_core) #:use-module (crates-io))

(define-public crate-rand_core-0.0.1 (c (n "rand_core") (v "0.0.1") (h "0wjdr2qdwq2dvp6jgscib7zwcaf9m6y0q3x1w9gdpgaiqf7p9isz") (f (quote (("std") ("nightly" "i128_support") ("i128_support") ("default" "std")))) (y #t)))

(define-public crate-rand_core-0.1.0-pre.0 (c (n "rand_core") (v "0.1.0-pre.0") (h "0fpjam1yl158pd2d7nm9gdzi6ws59fvxn4mzrx9mj2ir6hbs8qi3") (f (quote (("std") ("alloc"))))))

(define-public crate-rand_core-0.1.0 (c (n "rand_core") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0pfapcwf07v8riham0cnyr49pyc8q8v97maqgcw1if544i22h902") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("alloc"))))))

(define-public crate-rand_core-0.2.0-pre.0 (c (n "rand_core") (v "0.2.0-pre.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "16ny41mqlibnbgnpk7hb8c3l5pl7y4y9yvwvlvhmp38qvgxmy9f7") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-rand_core-0.2.0 (c (n "rand_core") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "00ik43dq3ndraccb504phk7r7zc26wzxp1m5z36fajbwahkmyyhv") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-rand_core-0.2.1 (c (n "rand_core") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1wpfh9pgdlpr47i0x55pgmjr3a21n0q3w2cjnk4iymd59pwz1v7d") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-rand_core-0.3.0 (c (n "rand_core") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1ny668mv568q7ym8qrkwv6gvf7lkyq0qfx2c9lqkpiwy0yvvc189") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("default" "std") ("alloc"))))))

(define-public crate-rand_core-0.2.2 (c (n "rand_core") (v "0.2.2") (d (list (d (n "rand_core") (r "^0.3") (k 0)))) (h "0wikbw2a36bz8ywjyycjrd7db6ra3yzj14zs1ysxz2fiqhia8q8r") (f (quote (("std" "rand_core/std") ("serde1" "rand_core/serde1") ("alloc" "rand_core/alloc"))))))

(define-public crate-rand_core-0.4.0 (c (n "rand_core") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1h3dbrhi5qgflqnzzd86s48v1dn1l17bmdssi5q170whsm4sbryh") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("alloc"))))))

(define-public crate-rand_core-0.3.1 (c (n "rand_core") (v "0.3.1") (d (list (d (n "rand_core") (r "^0.4") (d #t) (k 0)))) (h "0jzdgszfa4bliigiy4hi66k7fs3gfwi2qxn8vik84ph77fwdwvvs") (f (quote (("std" "rand_core/std") ("serde1" "rand_core/serde1") ("default" "std") ("alloc" "rand_core/alloc"))))))

(define-public crate-rand_core-0.5.0 (c (n "rand_core") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1jis94x9ri8xlxki2w2w5k29sjpfwgzkjylg7paganp74hrnhpk1") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde" "serde_derive") ("alloc"))))))

(define-public crate-rand_core-0.4.1 (c (n "rand_core") (v "0.4.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "02gy63n2shvxgyxbn1qlkkbrmsqfdb61qw3794c8sprc6sr85q9l") (f (quote (("std" "rand_core/std") ("serde1" "rand_core/serde1") ("alloc" "rand_core/alloc")))) (y #t)))

(define-public crate-rand_core-0.4.2 (c (n "rand_core") (v "0.4.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1p09ynysrq1vcdlmcqnapq4qakl2yd1ng3kxh3qscpx09k2a6cww") (f (quote (("std" "alloc") ("serde1" "serde" "serde_derive") ("alloc"))))))

(define-public crate-rand_core-0.5.1 (c (n "rand_core") (v "0.5.1") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06bdvx08v3rkz451cm7z59xwwqn1rkfh6v9ay77b14f8dwlybgch") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc"))))))

(define-public crate-rand_core-0.6.0 (c (n "rand_core") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "066skb5l89jc8gafp0ifzxgph3gz6345867rinyl64mjryl4pcx8") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc")))) (y #t)))

(define-public crate-rand_core-0.6.1 (c (n "rand_core") (v "0.6.1") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rfjrcyaj7blz2nawv2pypm5kqc59p80n6f5pg691399iggxf9n0") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc")))) (y #t)))

(define-public crate-rand_core-0.6.2 (c (n "rand_core") (v "0.6.2") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rvas1afjvd2827b8mf2ilg78h3ksl9npkrdds3wbw9x33mndkrl") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc"))))))

(define-public crate-rand_core-0.6.3 (c (n "rand_core") (v "0.6.3") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rxlxc3bpzgwphcg9c9yasvv9idipcg2z2y4j0vlb52jyl418kyk") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc"))))))

(define-public crate-rand_core-0.6.4 (c (n "rand_core") (v "0.6.4") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b4j2v4cb5krak1pv6kakv4sz6xcwbrmy2zckc32hsigbrwy82zc") (f (quote (("std" "alloc" "getrandom" "getrandom/std") ("serde1" "serde") ("alloc"))))))

(define-public crate-rand_core-0.9.0-alpha.0 (c (n "rand_core") (v "0.9.0-alpha.0") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "=0.8.0-alpha.5") (k 0)))) (h "0vh9vkx60v1fx5jyj16pxyxic8g5s2ssq5qwaz4byarrq5a255ib") (f (quote (("serde1" "serde") ("alloc")))) (s 2) (e (quote (("std" "alloc" "getrandom?/std")))) (r "1.60")))

(define-public crate-rand_core-0.9.0-alpha.1 (c (n "rand_core") (v "0.9.0-alpha.1") (d (list (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "=0.8.0-alpha.6") (k 0)))) (h "1jdqkrcfc3ydjnvfh98slgdkb8mxj8ab8ayi8z45wz1pm3xxz2fc") (f (quote (("serde1" "serde") ("alloc")))) (s 2) (e (quote (("std" "alloc" "getrandom?/std")))) (r "1.60")))

