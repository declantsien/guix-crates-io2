(define-module (crates-io ra nd randomprefixb) #:use-module (crates-io))

(define-public crate-randomprefixb-0.0.1 (c (n "randomprefixb") (v "0.0.1") (h "1qsgn3w1mvnha27ikfwmmbrxl5h3csx8q27dmaq900ab3ss1z9rp")))

(define-public crate-randomprefixb-0.0.2 (c (n "randomprefixb") (v "0.0.2") (h "1j6pfn6q6rzswgblx2gmxy84s03idlisgxi5x1rz80vc0x4nlj77")))

