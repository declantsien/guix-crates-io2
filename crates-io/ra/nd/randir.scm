(define-module (crates-io ra nd randir) #:use-module (crates-io))

(define-public crate-randir-0.1.0 (c (n "randir") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1gj6sfzrcrx9d2n4jilyfj0zkix3w2qsihf20ab2a9xhvrqwr9xa")))

(define-public crate-randir-0.1.1 (c (n "randir") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gxndciss0x2bcj851rrvqdjjf6kklnz09ks17ws3vndh35j1bf9")))

(define-public crate-randir-0.1.2 (c (n "randir") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xrlm843q721f1b17acalkgcn7j2sjm33lcywiq656fpwms3rc11")))

(define-public crate-randir-0.1.3 (c (n "randir") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gmy0az9z1h1v2h3xdmbxwl4zlv7r012ms2ki3ycdp8jkky2gqdp")))

(define-public crate-randir-0.1.4 (c (n "randir") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ml66f6r0cf24knf4c44nwi7avifbv58vxxqp9pclvdr5z6jdxkb")))

(define-public crate-randir-0.2.0 (c (n "randir") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gcfasnab4yfvcd2kh0fvg9vx19hw6aqa6qsbyyk0gfzv1hh42qg")))

