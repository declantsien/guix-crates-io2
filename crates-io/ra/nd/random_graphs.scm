(define-module (crates-io ra nd random_graphs) #:use-module (crates-io))

(define-public crate-random_graphs-0.1.0 (c (n "random_graphs") (v "0.1.0") (h "09zyj10cjcw51mj2fbmzns4j060scsndmb6qwra3201051npj9yz")))

(define-public crate-random_graphs-0.1.1 (c (n "random_graphs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zhkkqnp1n60cd8r3pj641qy8k17bwsbza42b8ck5jivyz9fbb77")))

