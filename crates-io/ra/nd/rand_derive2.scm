(define-module (crates-io ra nd rand_derive2) #:use-module (crates-io))

(define-public crate-rand_derive2-0.1.0 (c (n "rand_derive2") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q2x7nzxz6fa0dza39a637xsafcdk4xqmvg11dlz816dvf027g4m")))

(define-public crate-rand_derive2-0.1.1 (c (n "rand_derive2") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fx1whz5b1mzm2jbzrmkxcf8l2m5wdl9qj8d518ha20lcg2z3cqi")))

(define-public crate-rand_derive2-0.1.2 (c (n "rand_derive2") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hk03hlfv9dwrz1vqwiklfs045d7dwk7vcy1nw4xsaxjwcinbgfx")))

(define-public crate-rand_derive2-0.1.3 (c (n "rand_derive2") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bjbq9754wcgw9rxjnm105ngdq4m3dznss3xknzr0rww28v4ds3n")))

(define-public crate-rand_derive2-0.1.5 (c (n "rand_derive2") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wp9z40887qrq62rwscxw0prqyyv5gw4pvzlcidghnpw5rhpvx4n")))

(define-public crate-rand_derive2-0.1.6 (c (n "rand_derive2") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04g8pjvr9hvpy42yk2xgxl4a8riakzl0h682yr1ja532rl6rzi5q")))

(define-public crate-rand_derive2-0.1.7 (c (n "rand_derive2") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0frq3nas3dn8vzpwlwxgmffa428rdiwz6rffk32361y52cl19080")))

(define-public crate-rand_derive2-0.1.8 (c (n "rand_derive2") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xhaj34g0n7ism1ijgbksjihncf1smpngkrwhi4g9hqx366m7180")))

(define-public crate-rand_derive2-0.1.9 (c (n "rand_derive2") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y5a7pbzxnwc0ywpnm6pi6x3p71vdnh1ira30a6l1ljmq4w9amj2")))

(define-public crate-rand_derive2-0.1.10 (c (n "rand_derive2") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "035204i3nb3kac271824vavbk2aa6nmhhmxl7ynb2ngakan4vp2i")))

(define-public crate-rand_derive2-0.1.11 (c (n "rand_derive2") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16sriwsf2qbak0cr7q47livw6n9l5wxjlhjfqvdbp6z9lz71vdhv")))

(define-public crate-rand_derive2-0.1.12 (c (n "rand_derive2") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p2dd8in0hk2dfm71lv4kqbi446yqd5xn7m2q2p3cm07mfqwh2ri")))

(define-public crate-rand_derive2-0.1.13 (c (n "rand_derive2") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ba2k7ai5r6sqd7xfy4bb0yv60zz3vkcwkdq0m2sgbdf2ay06g5s")))

(define-public crate-rand_derive2-0.1.14 (c (n "rand_derive2") (v "0.1.14") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mpgv9hgrrhpldrgibfxr199p826kssxvi3j27gpgnda209ynljx")))

(define-public crate-rand_derive2-0.1.15 (c (n "rand_derive2") (v "0.1.15") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j90f5iafy7j3ggxdz6xdy5r1wdc935qjirdj6zbv44y93yiy5kc")))

(define-public crate-rand_derive2-0.1.16 (c (n "rand_derive2") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ni0rzflzaizcibvpg0854sh0wcvdj5gl2vpv524r9w4df55q5wz")))

(define-public crate-rand_derive2-0.1.17 (c (n "rand_derive2") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14j1n7sby31mglljwn2mghj65pjwd56vsfplz11da6zl8yqlxi2x")))

(define-public crate-rand_derive2-0.1.18 (c (n "rand_derive2") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "009dynpaq172w04q1jhgdpwksgwjaygc2mifxs79f3cwrai331sj")))

(define-public crate-rand_derive2-0.1.19 (c (n "rand_derive2") (v "0.1.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dkh32mrd9vfhrymir1f7ih8fwvcrd6w3r91xgb5n90fccyp9vki")))

(define-public crate-rand_derive2-0.1.20 (c (n "rand_derive2") (v "0.1.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dxibqvr2zz12az7w2d0ydm5q8rq812cf5z08d64hi9qdiq0gipi")))

(define-public crate-rand_derive2-0.1.21 (c (n "rand_derive2") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cxzdi15rvb4gdqhcgmhbhsfbig7dc5ax1xvbiy01qhw0l6dh1vy")))

