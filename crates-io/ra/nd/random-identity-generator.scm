(define-module (crates-io ra nd random-identity-generator) #:use-module (crates-io))

(define-public crate-random-identity-generator-1.0.0 (c (n "random-identity-generator") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)))) (h "0fp0rnpg2czvig6fm2y6rlp6kqls6691z466pj2jywnp8hwq7gb7")))

