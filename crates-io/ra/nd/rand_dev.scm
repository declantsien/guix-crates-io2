(define-module (crates-io ra nd rand_dev) #:use-module (crates-io))

(define-public crate-rand_dev-0.1.0 (c (n "rand_dev") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)))) (h "1llgm3rz0vq4lnbpmy2m0pqr5m8skrfk6cidhs5x33w252jwph6m") (f (quote (("default") ("__docs" "rand"))))))

(define-public crate-rand_dev-0.1.1 (c (n "rand_dev") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0p0lvwd0ag6bw2f5ylpxw0kckxkjd18ydzs9vc1mz85dgp19gvnb") (f (quote (("default"))))))

