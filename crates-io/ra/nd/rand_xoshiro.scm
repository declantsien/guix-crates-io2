(define-module (crates-io ra nd rand_xoshiro) #:use-module (crates-io))

(define-public crate-rand_xoshiro-0.1.0 (c (n "rand_xoshiro") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "rand") (r "^0.6") (k 2)) (d (n "rand_core") (r "^0.3") (k 0)))) (h "0ac9ha6ll8b6l1930bd99k29jrjpsbpddvr6ycrnbi5rkwb1id03")))

(define-public crate-rand_xoshiro-0.2.0 (c (n "rand_xoshiro") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "rand") (r "^0.6") (k 2)) (d (n "rand_core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1xy2n45k6bxdd3y0yvjhy7nldbxq1s9xaldiwymkcy95mrf5424i") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-rand_xoshiro-0.2.1 (c (n "rand_xoshiro") (v "0.2.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1c72j2xvr9dlbkv678k184bwvpxlkpj4gz1a8k3r2j0paj7rphb2") (f (quote (("serde1" "serde" "serde_derive")))) (y #t)))

(define-public crate-rand_xoshiro-0.3.0 (c (n "rand_xoshiro") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07w3qgrac8r356lz5vqff42rly6yd9vs3g5lx5pbn13rcmb05rqb") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_xoshiro-0.3.1 (c (n "rand_xoshiro") (v "0.3.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zvv90caawng82c82wsxinwsy4sz81cwfr1767qgcw7nfqbcj60f") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_xoshiro-0.4.0 (c (n "rand_xoshiro") (v "0.4.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "013h45rikipv5bda2ixmwx5rwsk9wpc7mr0a77cz20hxi0pdvz59") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_xoshiro-0.6.0 (c (n "rand_xoshiro") (v "0.6.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ajsic84rzwz5qr0mzlay8vi17swqi684bqvwqyiim3flfrcv5vg") (f (quote (("serde1" "serde"))))))

