(define-module (crates-io ra nd random-access-memory) #:use-module (crates-io))

(define-public crate-random-access-memory-0.0.1 (c (n "random-access-memory") (v "0.0.1") (h "17hqc5b3q8vl3ccggig07ic8rcv6f7lylmf6wqzf3fb14hq31l0a")))

(define-public crate-random-access-memory-0.1.0 (c (n "random-access-memory") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "random-access-storage") (r "^0.3.0") (d #t) (k 0)))) (h "0ckwc00azqlwhg3v72wl5dgpyvan6dd8nh3al3djmlar4rzxs6bi")))

(define-public crate-random-access-memory-0.1.1 (c (n "random-access-memory") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "random-access-storage") (r "^0.3.0") (d #t) (k 0)))) (h "0qb05xn9gviqpb33s38rmmdab7bw23rb8wl2kckf4cynwbbq02vi")))

(define-public crate-random-access-memory-0.2.0 (c (n "random-access-memory") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.3.0") (d #t) (k 0)))) (h "197sxqxwlnm9mbhz8mavmrdnqks23xihs0m28h65qy8ks2pynrsa")))

(define-public crate-random-access-memory-0.2.1 (c (n "random-access-memory") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.3.0") (d #t) (k 0)))) (h "01kpsxqxdw5rj0sasm8jvn8cxwrd2sb6h1xy7lvx0psy5la2m0y9")))

(define-public crate-random-access-memory-0.2.2 (c (n "random-access-memory") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.3.0") (d #t) (k 0)))) (h "06n9rkkjb4abbl4242j74b39qid4rw6c6y709ypfj16xm4brp8as")))

(define-public crate-random-access-memory-0.3.0 (c (n "random-access-memory") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.4.0") (d #t) (k 0)))) (h "1pd89l4y5328phbdiwcjy0b7dlwvj41f3vh7065hv7zgv8iqvda2")))

(define-public crate-random-access-memory-0.4.0 (c (n "random-access-memory") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.5.0") (d #t) (k 0)))) (h "1sjyg3saj9b2njnf6524sm6hlpq0cq0w1snzbql5c40ic03hgjjn")))

(define-public crate-random-access-memory-0.5.0 (c (n "random-access-memory") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)) (d (n "random-access-storage") (r "^0.6.0") (d #t) (k 0)))) (h "1qg0k2br3i3v3cgfpwadj429vdjh91g8fznhmnhgxfid4zw5n3r8")))

(define-public crate-random-access-memory-1.0.0 (c (n "random-access-memory") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)) (d (n "random-access-storage") (r "^2.0.0") (d #t) (k 0)))) (h "0i9b4606c14a9sh5lx22gf2f9h3wsyqy6p2m6pzbryii27wqdg9k")))

(define-public crate-random-access-memory-1.1.0 (c (n "random-access-memory") (v "1.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "random-access-storage") (r "^3.0.0") (d #t) (k 0)))) (h "02aqs0ainxj8j62rhnl8n4sxvscjw3hxk7bzgf3mn979v6rr9ldm")))

(define-public crate-random-access-memory-1.2.0 (c (n "random-access-memory") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "random-access-storage") (r "^3.0.0") (d #t) (k 0)))) (h "0jjyvbskqcjdyfn8a455awnrsc7dcnr2zi3m882c8qqz8hwx2dmb")))

(define-public crate-random-access-memory-2.0.0 (c (n "random-access-memory") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "random-access-storage") (r "^4.0.0") (d #t) (k 0)))) (h "01w1bn50pmqn7p1qbszviy9agh6wd5r3jpqz86drbjpcbjyzx54i")))

(define-public crate-random-access-memory-3.0.0 (c (n "random-access-memory") (v "3.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "intmap") (r "^2.0.0") (d #t) (k 0)) (d (n "random-access-storage") (r "^5.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)))) (h "0hf43bmsclmgkhbsnbpgv4ghim303w2cbahbi361iz0r45miq8zp")))

