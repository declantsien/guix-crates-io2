(define-module (crates-io ra nd random_stuff) #:use-module (crates-io))

(define-public crate-random_stuff-0.1.0 (c (n "random_stuff") (v "0.1.0") (d (list (d (n "log") (r "^0.4.13") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2.7.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0gmkykh0b68hfwh4ph15iyg9fp2hbb8icpx2lywm3zx60mpjdw04")))

