(define-module (crates-io ra nd random-fast-rng) #:use-module (crates-io))

(define-public crate-random-fast-rng-0.1.0 (c (n "random-fast-rng") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "random-trait") (r "^0.1") (d #t) (k 0)))) (h "0zfasgb3l347f9nprnpfvm11fgkcqyjxy2a67ydm64rwy9byr22v") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-random-fast-rng-0.1.1 (c (n "random-fast-rng") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "random-trait") (r "^0.1") (d #t) (k 0)))) (h "18q577c8j2j9j044b5fnj1xw1lwkyjrkl3agzp3lvx3iln24wy4m") (f (quote (("std") ("default" "std"))))))

