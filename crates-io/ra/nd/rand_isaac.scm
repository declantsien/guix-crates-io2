(define-module (crates-io ra nd rand_isaac) #:use-module (crates-io))

(define-public crate-rand_isaac-0.1.0 (c (n "rand_isaac") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0yvw170mrzxp2hjfwdkj1bdzg9a7n280j5cxli3wsspkxglwyvid") (f (quote (("serde1" "serde" "serde_derive" "rand_core/serde1"))))))

(define-public crate-rand_isaac-0.1.1 (c (n "rand_isaac") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "027flpjr4znx2csxk7gxb7vrf9c7y5mydmvg5az2afgisp4rgnfy") (f (quote (("serde1" "serde" "serde_derive" "rand_core/serde1"))))))

(define-public crate-rand_isaac-0.1.2 (c (n "rand_isaac") (v "0.1.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0zinsqypvm0vwdiwdabvq74d0nnrzshb0kyzsrwi4l5mlp4pkkx9") (f (quote (("serde1" "serde" "serde_derive" "rand_core/serde1")))) (y #t)))

(define-public crate-rand_isaac-0.2.0 (c (n "rand_isaac") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xlb9415x518ffkazxhvk8b04i9i548nva4i5l5s34crvjrv1xld") (f (quote (("serde1" "serde" "rand_core/serde1"))))))

(define-public crate-rand_isaac-0.3.0 (c (n "rand_isaac") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (o #t) (d #t) (k 0)))) (h "0a0b188s960qknwwgvpn7zpq3fzdhvrb0gsm5ir5akqvv4y3gi7s") (f (quote (("serde1" "serde" "rand_core/serde1"))))))

