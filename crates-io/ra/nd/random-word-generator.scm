(define-module (crates-io ra nd random-word-generator) #:use-module (crates-io))

(define-public crate-random-word-generator-0.1.0 (c (n "random-word-generator") (v "0.1.0") (d (list (d (n "random_word") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)))) (h "1k11av07rv56pcrigz7g3jjk6ijwm33y0b3iy6p2acg6kf30s7mn")))

