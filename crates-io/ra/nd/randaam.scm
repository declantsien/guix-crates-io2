(define-module (crates-io ra nd randaam) #:use-module (crates-io))

(define-public crate-randaam-0.1.0 (c (n "randaam") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s0bqswjgqg1yqnnklr754pzb333sdxf7ch0w2bs9gp4mrjl1n6g")))

(define-public crate-randaam-0.1.1 (c (n "randaam") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12mgh9ppb0vif2hkd65i76gp7i1njrkgpm58gg0fr5hf0rwsbn64")))

(define-public crate-randaam-0.1.2 (c (n "randaam") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14im04s59ijanzc8bsqpka5jfyglyp0x3j5bzakc1mdhjsi4qfp4")))

(define-public crate-randaam-0.1.3 (c (n "randaam") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1424wby14553kbchlyxj6n9lv0lc299iidbyax3zx7fg6z7x17lh")))

(define-public crate-randaam-0.1.4 (c (n "randaam") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01s5vd3rnb530zspq1grdf11mf185pvn9k5dy7j8b00sbwflbcyg")))

