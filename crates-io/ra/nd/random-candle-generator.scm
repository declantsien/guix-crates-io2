(define-module (crates-io ra nd random-candle-generator) #:use-module (crates-io))

(define-public crate-random-candle-generator-0.1.0 (c (n "random-candle-generator") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)))) (h "17izzakwysgfy8gl66siapj9bp8jhx51pkglvzvgyhw4kpsj463l")))

(define-public crate-random-candle-generator-0.1.1 (c (n "random-candle-generator") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)))) (h "0bl4nhwyd0w3j3ghsysh753bkjs3jk3y6iyji0n1586aj1q7cgwj") (f (quote (("use-rand" "rand"))))))

(define-public crate-random-candle-generator-0.1.2 (c (n "random-candle-generator") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)))) (h "0j38dla7l0wbgmi2ima3lgiwf403pnhq59n3lj1kwwd8y7zphd14") (f (quote (("use-rand" "rand"))))))

