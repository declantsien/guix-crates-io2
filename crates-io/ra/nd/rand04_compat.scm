(define-module (crates-io ra nd rand04_compat) #:use-module (crates-io))

(define-public crate-rand04_compat-0.1.0 (c (n "rand04_compat") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (k 0)) (d (n "rand04") (r "^0.1.0") (k 0)))) (h "1sq1w3qxbf5vl4jvhx0y5dwpkjf8iznw1hq80fq1d81hj6fgvldq") (f (quote (("std" "rand04/std" "rand/std") ("default" "std"))))))

(define-public crate-rand04_compat-0.1.1 (c (n "rand04_compat") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (k 0)) (d (n "rand04") (r "^0.1.0") (k 0)))) (h "0jr83gqy7dzpcpw4qizakqv2159v608sm0c059nbq35v9gmw0k0f") (f (quote (("std" "rand04/std" "rand/std") ("default" "std"))))))

