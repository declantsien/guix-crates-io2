(define-module (crates-io ra nd random_lfsr_256_galois) #:use-module (crates-io))

(define-public crate-random_lfsr_256_galois-22.10.0 (c (n "random_lfsr_256_galois") (v "22.10.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0854rlpl6xh62sdd35m7hp4s73vhl6nbydlypcm9sxvxlzm4kq06")))

