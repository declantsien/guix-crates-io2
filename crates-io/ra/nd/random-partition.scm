(define-module (crates-io ra nd random-partition) #:use-module (crates-io))

(define-public crate-random-partition-0.1.0 (c (n "random-partition") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (f (quote ("integer" "num-traits" "rand"))) (d #t) (k 0)))) (h "17zv3w8bz0daxfh8m1pfhkk7sir6z3c7lvgk97cfwpr5583i483b")))

