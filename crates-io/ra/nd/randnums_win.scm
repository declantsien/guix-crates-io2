(define-module (crates-io ra nd randnums_win) #:use-module (crates-io))

(define-public crate-randnums_win-0.1.0 (c (n "randnums_win") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1zj6n7b6lljqnk9fh7l5v5v65km92jp707ia99z3fp54vyqfqfn4")))

(define-public crate-randnums_win-0.1.1 (c (n "randnums_win") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0hn9ki4fxvv2jyz555d6pnryicrw87c2jcrcr4lvda6yyx7r95cf")))

