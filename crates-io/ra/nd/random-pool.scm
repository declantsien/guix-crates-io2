(define-module (crates-io ra nd random-pool) #:use-module (crates-io))

(define-public crate-random-pool-0.1.0 (c (n "random-pool") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mvsifyfix5cbk933zwprs59wx4nll25mqlb077vraw73pq74af4")))

(define-public crate-random-pool-0.1.1 (c (n "random-pool") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1dyighh1jp8dvq6avrgnxw7xldskg65i0bpcxw0zab06xxjm1zhx")))

