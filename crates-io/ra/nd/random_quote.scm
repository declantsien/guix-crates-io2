(define-module (crates-io ra nd random_quote) #:use-module (crates-io))

(define-public crate-random_quote-0.1.0 (c (n "random_quote") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)))) (h "1h399r58k4r4y0a2b0zlxi0g823i0hwsmb403ddy41hmzbf7wk5d")))

(define-public crate-random_quote-0.1.1 (c (n "random_quote") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)))) (h "1ds5i07qxkp5aq8cjw3wqfa0y1184dxqi3csq4gy00h8635wfifd")))

(define-public crate-random_quote-0.1.2 (c (n "random_quote") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)))) (h "180hvqm71024kfl5hsn9z5d8gvflcaamc0irg83fm4z4ghwiasfk")))

(define-public crate-random_quote-0.1.3 (c (n "random_quote") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)))) (h "1vf77xlx1c66bw6clbhmw15mynyp8a5iw7l0vwinp3rav1qzr6i0")))

