(define-module (crates-io ra nd random-pairings) #:use-module (crates-io))

(define-public crate-random-pairings-0.1.0 (c (n "random-pairings") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vhd0rz2qpmwqylhhfknrsvjbrv7ykb6nc0fcv6bhx3gc0csaxla") (f (quote (("js" "getrandom/js"))))))

(define-public crate-random-pairings-0.1.1 (c (n "random-pairings") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (k 0)))) (h "170ivrzfiqcjhfcpkpvw8g5qvd9f9zyqf4xbmirp208ck5ygs538")))

