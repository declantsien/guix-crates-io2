(define-module (crates-io ra nd random-access-storage) #:use-module (crates-io))

(define-public crate-random-access-storage-0.1.0 (c (n "random-access-storage") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "0xcbcwjrsq5wz50g7xcpwgfdsxdw1f1qiyb5dlaiw7ya3dmwksib")))

(define-public crate-random-access-storage-0.2.0 (c (n "random-access-storage") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "16rbbjvwm00dg8l7lr9zgzirz49gcxlkq4ba4x6smb7ihiym7c0b")))

(define-public crate-random-access-storage-0.3.0 (c (n "random-access-storage") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "04l1d5nwpfynz29rvm1gyclxazzwawmcmszdbyqikimypxgmywid")))

(define-public crate-random-access-storage-0.3.2 (c (n "random-access-storage") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1w8ybxm8kwcclck5kmzaagyw705ngq8dv5vnnw6xi6ndqawn92fh")))

(define-public crate-random-access-storage-0.4.0 (c (n "random-access-storage") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "118fk3m1jdx45ldbx2mld34b59pad844zs5n7vgwflmfnrx9agkg")))

(define-public crate-random-access-storage-0.5.0 (c (n "random-access-storage") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1admlk5946346gpg95d9h151lkyvsizw8yld3lh7z1xq4a6xbmf7")))

(define-public crate-random-access-storage-0.6.0 (c (n "random-access-storage") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1j4lnw8s2ahy37yw4c85l1d8kf9mxfr8plxa2ggc26v84q42vr67")))

(define-public crate-random-access-storage-1.0.0 (c (n "random-access-storage") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "191x2sjhgi44ci39h2hkgnrmdlmj3v4lrv0m7kvsqhshxmkfb3c1")))

(define-public crate-random-access-storage-2.0.0 (c (n "random-access-storage") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "1i3ai9k8v0gx8n2pvw9hx7mxjlqpjbiizd62vnpdkr23c29picvj")))

(define-public crate-random-access-storage-3.0.0 (c (n "random-access-storage") (v "3.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "08wc9n5xjk4d6nnldnddrs5g243w0gg71r30jvk8yvb9k7v5l1qw")))

(define-public crate-random-access-storage-4.0.0 (c (n "random-access-storage") (v "4.0.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "futures-io") (r "^0.3.4") (d #t) (k 0)))) (h "1b7yngyjh3s791g4rdjmmpn9l6p15vzz0sn3c7ri2vvy334bl9wx")))

(define-public crate-random-access-storage-5.0.0 (c (n "random-access-storage") (v "5.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14s310hif23xmqh93fya48l2smcbmv0dvrjy7nssvi6yklxqbcy5")))

