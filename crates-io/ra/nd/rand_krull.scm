(define-module (crates-io ra nd rand_krull) #:use-module (crates-io))

(define-public crate-rand_krull-0.1.0 (c (n "rand_krull") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wrapping_arithmetic") (r "^0.1.0") (d #t) (k 0)))) (h "19gndvm19b8y0gz71yf1frxbq5a2zalywp1r82rmlifw2nsn18an") (f (quote (("default"))))))

(define-public crate-rand_krull-0.2.0 (c (n "rand_krull") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wrapping_arithmetic") (r "^0.1.0") (d #t) (k 0)))) (h "17zryg66r6hshkjv72s03azp5i65wpax7rdbnfyvp8aji07sc2nj") (f (quote (("default"))))))

(define-public crate-rand_krull-1.0.0 (c (n "rand_krull") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wrapping_arithmetic") (r "^0.1.0") (d #t) (k 0)))) (h "06vsyvrq7vl37lii85ji77rgaqpvl3ff3wcq8fh9xcln2lnlm7mg") (f (quote (("default"))))))

(define-public crate-rand_krull-1.1.0 (c (n "rand_krull") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wrapping_arithmetic") (r "^0.1.0") (d #t) (k 0)))) (h "1vhzp4hs2yjrrvmhrpcsw53qz2vpp1cwhqfjdl41q9mxxwnc83vd") (f (quote (("default"))))))

