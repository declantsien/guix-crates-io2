(define-module (crates-io ra nd rand-facade) #:use-module (crates-io))

(define-public crate-rand-facade-0.1.0 (c (n "rand-facade") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "rand") (r "^0.7.3") (k 0)) (d (n "rand_chacha") (r "^0.2.2") (k 0)))) (h "170kpn6sfahk9h2z4nllw8rghbyfj5rzj3pcgpqgraz2v0bnx6j2") (f (quote (("std" "rand/std") ("default" "std") ("cortex_m" "cortex-m" "lazy_static/spin_no_std"))))))

(define-public crate-rand-facade-0.1.1 (c (n "rand-facade") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "rand") (r "^0.7.3") (k 0)) (d (n "rand_chacha") (r "^0.2.2") (k 0)))) (h "1a78xm8drbsx7har8dzg6n07ri1f2140y733hwqgvcvdifb8cxl5") (f (quote (("std" "rand/std") ("os_rng" "rand/std") ("default" "os_rng") ("cortex_m" "cortex-m" "lazy_static/spin_no_std"))))))

(define-public crate-rand-facade-0.1.2 (c (n "rand-facade") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "rand") (r "^0.7.3") (k 0)) (d (n "rand_chacha") (r "^0.2.2") (k 0)))) (h "1p23w34dg30djzibyn8v4cnkp3mv4l022jpqixp9kpa7pq2vbk9r") (f (quote (("std" "rand/std") ("os_rng" "rand/std") ("default" "os_rng") ("cortex_m" "cortex-m" "lazy_static/spin_no_std"))))))

(define-public crate-rand-facade-0.2.0 (c (n "rand-facade") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (k 2)) (d (n "rand_core") (r "^0.5.1") (k 0)))) (h "022ldf41246kfbpz6gs5ld6z77246an34ijcwcv2jl4rqd0mjh90") (f (quote (("std" "rand") ("os_rng" "rand" "rand/std") ("default") ("cortex_m" "cortex-m" "lazy_static/spin_no_std"))))))

