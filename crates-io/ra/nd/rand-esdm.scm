(define-module (crates-io ra nd rand-esdm) #:use-module (crates-io))

(define-public crate-rand-esdm-0.0.1 (c (n "rand-esdm") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "1qpk6kkjswds0pagqi7sshqij13fijpxa5paqvdfgl1nwkb9yd7b")))

(define-public crate-rand-esdm-0.0.2 (c (n "rand-esdm") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)))) (h "01ib1aqc59qmblvrzw6y7kcrbzcj45pr151zyqb47l4rcpnvrq4k")))

(define-public crate-rand-esdm-0.0.3 (c (n "rand-esdm") (v "0.0.3") (d (list (d (n "named-sem") (r "^0.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0fiz09v7hxxg33w0j346sh7m2bdz8alrqrdgy6rb77sm6bdbh5bn") (f (quote (("privileged_tests") ("botan"))))))

(define-public crate-rand-esdm-0.0.4 (c (n "rand-esdm") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0rjjvd9yp2441kbazrry4pj3y6g9hy735g593hl6hj1v08gs7a9q") (f (quote (("privileged_tests") ("default")))) (s 2) (e (quote (("build-binary" "dep:clap"))))))

(define-public crate-rand-esdm-0.0.5 (c (n "rand-esdm") (v "0.0.5") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "esdm-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0a48dqiqiys56yp3lf7nzdr1icxnx8wiv8sw1qvfs7aswmrsxc5f") (f (quote (("privileged_tests") ("default")))) (s 2) (e (quote (("build-binary" "dep:clap"))))))

(define-public crate-rand-esdm-0.0.6 (c (n "rand-esdm") (v "0.0.6") (d (list (d (n "esdm-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1ffnkxv8qw7ir89x5w8f01n8ir0adwpaw57di6ykpfx7x5jfyzaq") (f (quote (("privileged_tests") ("default"))))))

(define-public crate-rand-esdm-0.1.0 (c (n "rand-esdm") (v "0.1.0") (d (list (d (n "esdm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "15fw8qgf5v6sziq0gbmhyrazbycrl5pm1f0j6nl8mh5ifbnisgzl") (f (quote (("privileged_tests") ("default"))))))

(define-public crate-rand-esdm-0.1.1 (c (n "rand-esdm") (v "0.1.1") (d (list (d (n "esdm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0y7ma2sccq307c436xbqcs3v5lh61hv2hsd51k45jsk5ly1kayc6") (f (quote (("privileged_tests") ("default"))))))

