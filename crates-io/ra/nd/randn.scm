(define-module (crates-io ra nd randn) #:use-module (crates-io))

(define-public crate-randn-0.1.0 (c (n "randn") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1cl7aijq2immip5dgl4x9rzkfn8iw9ql3jjj07201vysqzpd2h64")))

(define-public crate-randn-0.1.1 (c (n "randn") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "0391plh453hybf0cddlhhzkgfvird96y4q6d3jac4l4qzvw5xk3r")))

(define-public crate-randn-0.1.11 (c (n "randn") (v "0.1.11") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "15g5cbq9g91ii29lfpjmf1524x2j3im4029ln124vf3gadnl9yl5")))

(define-public crate-randn-0.1.12 (c (n "randn") (v "0.1.12") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1m25bifx7r15gsxjd9bd6mijswvyg0pk0k9g3kzxq6afm2qrw6sd")))

