(define-module (crates-io ra nd random-show-themes) #:use-module (crates-io))

(define-public crate-random-show-themes-0.1.0 (c (n "random-show-themes") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4.3") (d #t) (k 0)) (d (n "term-table") (r "^1.2.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.10") (d #t) (k 0)))) (h "00b9559j59jdffgz7ranyc3p7pf95bsc7kgwx2pqr0hfxrh8xwxk")))

