(define-module (crates-io ra nd randsym) #:use-module (crates-io))

(define-public crate-randsym-0.1.0 (c (n "randsym") (v "0.1.0") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0nqpckkmqd1milx7cil0j2gfyq8wlzsf8vay985pgzgr48szkz4x")))

(define-public crate-randsym-0.1.1 (c (n "randsym") (v "0.1.1") (d (list (d (n "uuid") (r "^1.2.2") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1c7ldcr9pk99a7zkx0p5jcmlkscy31zqh8jnbqhqdvd44f4mxr7g")))

