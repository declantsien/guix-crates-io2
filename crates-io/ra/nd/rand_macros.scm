(define-module (crates-io ra nd rand_macros) #:use-module (crates-io))

(define-public crate-rand_macros-0.1.1 (c (n "rand_macros") (v "0.1.1") (h "0yqhvqws81hnrrygg9v19npcb2ihy13n3cdgbywxa2wvw3d0pbpw")))

(define-public crate-rand_macros-0.1.2 (c (n "rand_macros") (v "0.1.2") (h "03qgvqz4vjxqqpsgrs76006pj0v0mgrff251gwzmlf0592qq21xi")))

(define-public crate-rand_macros-0.1.3 (c (n "rand_macros") (v "0.1.3") (h "07ax6pv9jjqissx2yrnqv4xj0grl4fk6na37vd9ya416r4khm0ms")))

(define-public crate-rand_macros-0.1.4 (c (n "rand_macros") (v "0.1.4") (h "05abxh6p4ch768052fydy2ph9x182jmjjqf16x6ls7aiqzxwr6ba")))

(define-public crate-rand_macros-0.1.5 (c (n "rand_macros") (v "0.1.5") (h "0ynbj0fnijb9miv98i6rq2phn3sz91r00xfbf3jskwabgd3x1ygn")))

(define-public crate-rand_macros-0.1.6 (c (n "rand_macros") (v "0.1.6") (h "0yk2r40hg2z1jfi3k02dallribf5pw3mm0djfx8hrxfxkv8c3afb")))

(define-public crate-rand_macros-0.1.7 (c (n "rand_macros") (v "0.1.7") (h "0g73w229h2iy5b9qhx73a8hddnps6pyh9b7s4548cws1b3fimzw5")))

(define-public crate-rand_macros-0.1.8 (c (n "rand_macros") (v "0.1.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1rcs9ng9fq0k5g1kc8fbifhvk513wg4lqmsnwf7gf52lfmk5zqab")))

(define-public crate-rand_macros-0.1.9 (c (n "rand_macros") (v "0.1.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0k1pn57dd2f3x1i5nzw2n8rlxi23k0p9a6pgssflg4b8fh9rkhg7")))

(define-public crate-rand_macros-0.1.10 (c (n "rand_macros") (v "0.1.10") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1cgnl9bw4ip6mbmlciwfz3dgpi33mx0gsgnh8flrz5ayxs7hp0kz")))

