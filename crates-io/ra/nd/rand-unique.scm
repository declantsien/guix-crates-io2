(define-module (crates-io ra nd rand-unique) #:use-module (crates-io))

(define-public crate-rand-unique-0.2.0 (c (n "rand-unique") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "is_prime") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.16") (d #t) (k 2)))) (h "1lxwijdgamznppcq91jr3l7n9v6gpx0qzv9mj5q1nsb6srhvwbwq") (f (quote (("default" "rand"))))))

(define-public crate-rand-unique-0.2.1 (c (n "rand-unique") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "is_prime") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.16") (d #t) (k 2)))) (h "1fz7pfvf30alqhdkpxan886z0gx851fh62wqvzgkxqb12cswzc4n") (f (quote (("default" "rand"))))))

(define-public crate-rand-unique-0.2.2 (c (n "rand-unique") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "is_prime") (r "^2.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "statrs") (r "^0.16") (d #t) (k 2)))) (h "1zcd6r017vrm33p5qdc8p232a5y8wvah4r2xwsns2m69q68iygm8") (f (quote (("default" "rand"))))))

