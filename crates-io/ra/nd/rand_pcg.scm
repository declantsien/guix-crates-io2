(define-module (crates-io ra nd rand_pcg) #:use-module (crates-io))

(define-public crate-rand_pcg-0.1.0 (c (n "rand_pcg") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "13byh6cf1fmmw8lrzaxmgcaw3hff5f20vmaxv4x3kv7bvdihbp33") (f (quote (("serde1" "serde" "serde_derive" "bincode/i128"))))))

(define-public crate-rand_pcg-0.1.1 (c (n "rand_pcg") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.3") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "01gy6ad7bpj5ymshfnz9m4cz3rv0bbgbvma4pdb4w1676fdd0sq8") (f (quote (("serde1" "serde" "serde_derive" "bincode/i128"))))))

(define-public crate-rand_pcg-0.1.2 (c (n "rand_pcg") (v "0.1.2") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "bincode") (r "^1.1.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0i0bdla18a8x4jn1w0fxsbs3jg7ajllz6azmch1zw33r06dv1ydb") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-rand_pcg-0.2.0 (c (n "rand_pcg") (v "0.2.0") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)) (d (n "bincode") (r "^1.1.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dljwilv3640l1c5vlg4isiq7qz8gqa2cjbvgv3p0p5wrd36669y") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_pcg-0.2.1 (c (n "rand_pcg") (v "0.2.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ab4h6s6x3py833jk61lwadq83qd1c8bih2hgi6yps9rnv0x1aqn") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_pcg-0.3.0 (c (n "rand_pcg") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1w47awndfhgcc31zbji66pwndqmc6lsyairqi9b17f82f19riqbx") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_pcg-0.3.1 (c (n "rand_pcg") (v "0.3.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gn79wzs5b19iivybwa09wv4lhi4kbcqciasiqqynggnr8cd1jjr") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_pcg-0.9.0-alpha.0 (c (n "rand_pcg") (v "0.9.0-alpha.0") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "=0.9.0-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19q3vaixvv2smcb6dw81izcn6znjdlbs37bjzbhv29m66rl6rn3y") (f (quote (("serde1" "serde")))) (r "1.60")))

(define-public crate-rand_pcg-0.9.0-alpha.1 (c (n "rand_pcg") (v "0.9.0-alpha.1") (d (list (d (n "bincode") (r "^1.1.4") (d #t) (k 2)) (d (n "rand_core") (r "=0.9.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qj8xffanvpjqvybz17cmk7i191dxi0pq6v1m5iq2gz9wqh4c1sh") (f (quote (("serde1" "serde")))) (r "1.60")))

