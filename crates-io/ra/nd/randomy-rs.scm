(define-module (crates-io ra nd randomy-rs) #:use-module (crates-io))

(define-public crate-randomy-rs-1.1.14 (c (n "randomy-rs") (v "1.1.14") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zi7biwcryyk8wig21547wqz4dlkrrkznpgpa1ppciyssf1bbgaz")))

