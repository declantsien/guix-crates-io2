(define-module (crates-io ra nd rand-bytes) #:use-module (crates-io))

(define-public crate-rand-bytes-0.1.0 (c (n "rand-bytes") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.7.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "18s01b445jaaf314pd7ywfq4ar122s6pxkv3a39yy3vr4lm9jk4v")))

(define-public crate-rand-bytes-0.2.0 (c (n "rand-bytes") (v "0.2.0") (d (list (d (n "clap") (r "^2.23.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.13.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "1f62p9hf4az69pbijcgxr7wdpmc8b4bddxz6hfz6avvpy1p0xza9")))

