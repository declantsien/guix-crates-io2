(define-module (crates-io ra nd randomizer) #:use-module (crates-io))

(define-public crate-randomizer-0.1.0 (c (n "randomizer") (v "0.1.0") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "0aixf3n7wkcn2cwm7k51wwvw8zg4v2j0va7zd5kfpfd7xnmg9070")))

(define-public crate-randomizer-0.1.1 (c (n "randomizer") (v "0.1.1") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "1qirl120dw1sz7nm3circ5m188q2pz2ynymp4wjrnqiixa9z0jl4")))

(define-public crate-randomizer-0.1.2 (c (n "randomizer") (v "0.1.2") (d (list (d (n "fastrand") (r "^1.8.0") (d #t) (k 0)))) (h "081yfswhsb20y0942nlzbxnz4g2x88jkrc5nawi1wjvykavpnzrm")))

