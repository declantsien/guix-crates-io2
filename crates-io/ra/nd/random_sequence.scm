(define-module (crates-io ra nd random_sequence) #:use-module (crates-io))

(define-public crate-random_sequence-0.1.0 (c (n "random_sequence") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1zkjarxdkspqgvsrvhlv5mx05z454zb6awqv6gsij1igv2430shc")))

(define-public crate-random_sequence-0.1.1 (c (n "random_sequence") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1sacj2pi63shjz34az5ik06ckij9k7vgf331kzzcvjr1d9c30q7x")))

