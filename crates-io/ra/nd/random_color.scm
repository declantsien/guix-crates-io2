(define-module (crates-io ra nd random_color) #:use-module (crates-io))

(define-public crate-random_color-0.4.0 (c (n "random_color") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "03fjabgzjljr2pw8vwvq3swnzvy1yg761j6w4vh789g01g7qhr7h")))

(define-public crate-random_color-0.4.1 (c (n "random_color") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1hn0hmi4yzfc3qj4gx99chb983ddsa9agg6frf9ca9349in275hs")))

(define-public crate-random_color-0.4.2 (c (n "random_color") (v "0.4.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0c93jmqg717z30n2wzv0pwj49kab49sx2j67ndjapr0f83n16pn1")))

(define-public crate-random_color-0.4.3 (c (n "random_color") (v "0.4.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0411jqfm6whd2skdi20c9zv8y5x4wxwjk3zzid8ivkbsg1q6s5ip")))

(define-public crate-random_color-0.4.4 (c (n "random_color") (v "0.4.4") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0jc5s40v4189whw2cp83sm1qb5q5n3ib6sb0x7sa7g8i74qj1wik")))

(define-public crate-random_color-0.5.0 (c (n "random_color") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1ka50fjd8l3jh7lpqfsnp4lcz285bzbvvk8m5xb4jxn2949k1215")))

(define-public crate-random_color-0.5.1 (c (n "random_color") (v "0.5.1") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0h27rqmk69hyr27qxgy60c7h0fqbdb5cjjl4ppm3jn2rzyzmq935") (f (quote (("wasm-bindgen" "rand/wasm-bindgen"))))))

(define-public crate-random_color-0.6.1 (c (n "random_color") (v "0.6.1") (d (list (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "10bixxy2lj051mdmnnblmvms2pab12j3gza1a77b51k7abb4pwzm") (f (quote (("wasm-bindgen" "rand/wasm-bindgen"))))))

(define-public crate-random_color-0.7.0 (c (n "random_color") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0kyhxyr5hnwbbhifq3qin08q5j9hafj94lzgzsf9db9jlg6sijhx")))

(define-public crate-random_color-0.8.0 (c (n "random_color") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "15jiv3x43r3qbq5dh0xygb266ikk505aqsylsrzgmvr7qldl5180")))

