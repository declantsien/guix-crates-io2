(define-module (crates-io ra nd randomx_rust) #:use-module (crates-io))

(define-public crate-randomx_rust-0.1.1 (c (n "randomx_rust") (v "0.1.1") (d (list (d (n "bigint") (r "^4.4.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "cmake") (r "^0.1.40") (d #t) (k 1)) (d (n "filetime") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "0rwiwcvlk1c4zjvx0mx3i7fs6mrn24a4nnjnl6w6md3vw81c6ikz")))

