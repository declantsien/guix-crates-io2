(define-module (crates-io ra nd rand-distributions) #:use-module (crates-io))

(define-public crate-rand-distributions-0.1.2 (c (n "rand-distributions") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "140w49ivmrxbdbwpasgmdcza01m3lwp3cg5kl4d85dyp9h6gdysx")))

