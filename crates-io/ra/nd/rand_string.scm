(define-module (crates-io ra nd rand_string) #:use-module (crates-io))

(define-public crate-rand_string-0.1.0 (c (n "rand_string") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00473ny852rr197wl9w5kngkigqhk14df1pwy091bdfgglxp04b6")))

(define-public crate-rand_string-0.1.1 (c (n "rand_string") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00d3mp8289d4bvvvyr9k4pvzhnx4w0smj90jhyf45gd8bqs940zd")))

