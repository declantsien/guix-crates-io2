(define-module (crates-io ra nd rand-bits) #:use-module (crates-io))

(define-public crate-rand-bits-0.0.0 (c (n "rand-bits") (v "0.0.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0npq943rnpk3fgxhqcwqa7dp3dk1i309pw5g91i3392mm1v33396") (y #t) (r "1.61.0")))

(define-public crate-rand-bits-0.1.0 (c (n "rand-bits") (v "0.1.0") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0r6q9jpm7sccwrrynkqp5v3bfk656kd7pidxivfh0csmp48ljvh2") (y #t) (r "1.61.0")))

(define-public crate-rand-bits-0.1.1 (c (n "rand-bits") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bvap5vdlgfhix264li7mc2nb90srkld7qgv6a23zcznkh7zhx1m") (r "1.61.0")))

