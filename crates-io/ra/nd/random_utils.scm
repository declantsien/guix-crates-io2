(define-module (crates-io ra nd random_utils) #:use-module (crates-io))

(define-public crate-random_utils-0.2.0 (c (n "random_utils") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03j12j6kbxias6v49bk7vxg4d2klw6jy2mb4x59bhv8ksy6mgjmw")))

(define-public crate-random_utils-0.2.1 (c (n "random_utils") (v "0.2.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "12qlbvzndxmb6nj2ic9x4r2yyxqan673zj8dzh4drappfq3mbyw6")))

