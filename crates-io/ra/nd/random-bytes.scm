(define-module (crates-io ra nd random-bytes) #:use-module (crates-io))

(define-public crate-random-bytes-0.1.0 (c (n "random-bytes") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^0.4.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "013kncvc2whkpyk5iaclr0i27ja7db9rqyzhbchjjp7bp5sbldjm")))

(define-public crate-random-bytes-1.0.0 (c (n "random-bytes") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "darth-rust") (r "^3.0.7") (f (quote ("build"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0k7nmjr82wj3ry2cq6kah80s1m7n2d8fzzl0r6m5sxb9kamz19a8")))

(define-public crate-random-bytes-1.0.1 (c (n "random-bytes") (v "1.0.1") (d (list (d (n "darth-rust") (r "^3.0.7") (f (quote ("build"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pah8ld3803rp3xlp74ya5w81j4044xshdsi0w6hs8d4g0fas8mq")))

(define-public crate-random-bytes-1.0.2 (c (n "random-bytes") (v "1.0.2") (d (list (d (n "darth-rust") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00h9r2z4jqkdkaisd7wrhn71mz11b2942291pxncd5y3w2g6q9cb")))

