(define-module (crates-io ra nd random-geojson-points) #:use-module (crates-io))

(define-public crate-random-geojson-points-0.1.0 (c (n "random-geojson-points") (v "0.1.0") (d (list (d (n "geojson") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "124jsl1sb7cl5xq1yaysgz8rllgg85sn5bd3qp2aiq8ba6864zi2")))

(define-public crate-random-geojson-points-0.1.1 (c (n "random-geojson-points") (v "0.1.1") (d (list (d (n "geojson") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0s90ir0rijg7vkxaykmjyhk1xrda976agfwcpd2fmmjvsz2d7mb3")))

(define-public crate-random-geojson-points-0.1.2 (c (n "random-geojson-points") (v "0.1.2") (d (list (d (n "geojson") (r "^0.16") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1hjkqdghf45g8z2crsjzdlalc2kvn4lp6nl6s5xgc8kkrgh5z3kp")))

