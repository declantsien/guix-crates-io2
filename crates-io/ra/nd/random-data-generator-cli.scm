(define-module (crates-io ra nd random-data-generator-cli) #:use-module (crates-io))

(define-public crate-random-data-generator-cli-0.1.3 (c (n "random-data-generator-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pd327bxbas5akiyw9aiarkg0kvv8x4ckb6nxqr98clkj258qmqm")))

(define-public crate-random-data-generator-cli-0.1.4 (c (n "random-data-generator-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07smza7nzg112ij56nh1gmyw78hhs6svbj7bh6sbqph0n5v4dgvv")))

