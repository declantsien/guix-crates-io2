(define-module (crates-io ra nd randlib) #:use-module (crates-io))

(define-public crate-randlib-0.1.0 (c (n "randlib") (v "0.1.0") (h "0aiwvszf11mw6ljk7y4znwjsa5z4qa7kaqb8kbsm6rwb0ksrc95v")))

(define-public crate-randlib-0.1.1 (c (n "randlib") (v "0.1.1") (h "08imqn2kz8zw30rxdzf413jb4b1s7y3zpfnzc8brx6yqz6mak0vf")))

(define-public crate-randlib-0.1.2 (c (n "randlib") (v "0.1.2") (h "180br85nff2yklmrmwn2qcq3lrp2lxkwmnwzs6fb3mkh5n994sj4")))

(define-public crate-randlib-0.1.3 (c (n "randlib") (v "0.1.3") (h "097pfx4n7kjbm2y5q2l27n0mbfqdnxy2s6bmd98a9kdlvkxyqdvq")))

(define-public crate-randlib-0.1.4 (c (n "randlib") (v "0.1.4") (h "1rld5dvj1qars1fpvwrrsmc1wi8p2s5dgk46gz2k2h5wgk85w7y1")))

(define-public crate-randlib-0.1.5 (c (n "randlib") (v "0.1.5") (h "03nbj9fzxa2dp5rnmn0lfh7f6qivb03yrc4n7wj4zamxasby7pqb")))

(define-public crate-randlib-0.1.6 (c (n "randlib") (v "0.1.6") (h "0q0cwivp5sfgy07wrd0k70sj1rhcps3cyqvv6bd9akvl64lyl88l")))

(define-public crate-randlib-0.1.7 (c (n "randlib") (v "0.1.7") (h "1cpda2qdjd6g6pqaw64xmiybg6h0i807k2g26q80i0cmnmcxlkw3")))

(define-public crate-randlib-0.1.8 (c (n "randlib") (v "0.1.8") (h "1h76nwm9p7pdnfdd9n78kghlsi0wz3dhf9acfrjwx5prsnh5bfi6")))

(define-public crate-randlib-0.1.9 (c (n "randlib") (v "0.1.9") (h "089y8rkjcw7pj9605csfi5chzij7n1p6wswz26vsq7hkwya9zbk8")))

(define-public crate-randlib-0.2.0 (c (n "randlib") (v "0.2.0") (h "0hfijsaxr43jmm4sljglmwl6rzbqglcg36v96kwx2pap1cffzwa6")))

