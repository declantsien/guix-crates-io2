(define-module (crates-io ra nd random_rust) #:use-module (crates-io))

(define-public crate-random_rust-0.1.0 (c (n "random_rust") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pgzwzqmc2lwpjnlbymyq3hhh9iqdqh6xbavw2n1qka7mz64ww57")))

