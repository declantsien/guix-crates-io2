(define-module (crates-io ra nd rand_romu) #:use-module (crates-io))

(define-public crate-rand_romu-0.1.0 (c (n "rand_romu") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)))) (h "0ymhhlcgk83d785p359nrnwvnhi9xzijl34h32hwwcwf6dp6zmcq")))

