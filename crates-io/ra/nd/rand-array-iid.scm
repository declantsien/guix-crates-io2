(define-module (crates-io ra nd rand-array-iid) #:use-module (crates-io))

(define-public crate-rand-array-iid-0.1.0 (c (n "rand-array-iid") (v "0.1.0") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (k 0)))) (h "1kjh3p7r12wkf7l9an5jwfd4ij8d49v3h9nqbrzr3aqj2czc9ma7") (f (quote (("more_array_sizes"))))))

(define-public crate-rand-array-iid-0.1.1 (c (n "rand-array-iid") (v "0.1.1") (d (list (d (n "array-init") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (k 0)))) (h "1zg3w91k61vg0ih51j82zmlzgyxjssaydwyzdh5zg6hiw2bwzk1s") (f (quote (("more_array_sizes"))))))

(define-public crate-rand-array-iid-0.2.0 (c (n "rand-array-iid") (v "0.2.0") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (k 0)))) (h "14bdp68iwszc8sbflb2wvygqyllghgfhhmiqy6jldgcpdrlaapax") (f (quote (("more-array-sizes") ("const-generics" "array-init/const-generics"))))))

