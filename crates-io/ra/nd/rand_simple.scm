(define-module (crates-io ra nd rand_simple) #:use-module (crates-io))

(define-public crate-rand_simple-0.0.1 (c (n "rand_simple") (v "0.0.1") (h "03b9y9ckhbra9hafh6gx8fx1g7zc0nhvspr0vccqak1z28in5wnw")))

(define-public crate-rand_simple-0.0.2 (c (n "rand_simple") (v "0.0.2") (h "1bbsnh5mbpmq179nslf4rw5v50zg94ww2lk8659a6j0cdqjkpgnl")))

(define-public crate-rand_simple-0.0.3 (c (n "rand_simple") (v "0.0.3") (h "02hj1qb28sisifsj8va91kw8hr9dhj1z1h0mlaviax8y6cdg9h0x")))

(define-public crate-rand_simple-0.0.4 (c (n "rand_simple") (v "0.0.4") (h "17sry9c921i4n0pzcqnxh9jkr722sjygqslirk4k22j5dbzwpb56")))

(define-public crate-rand_simple-0.0.5 (c (n "rand_simple") (v "0.0.5") (h "0wsc5bjfvnjmvjqz9f6v04m9a19sy3qpy63i4cm5v8c03imphi0x")))

(define-public crate-rand_simple-0.0.6 (c (n "rand_simple") (v "0.0.6") (h "14nwbj5xmdppcpgh7racs2qxk7c0mcr784rj4af3hhbghppj59an")))

(define-public crate-rand_simple-0.0.7 (c (n "rand_simple") (v "0.0.7") (h "15mg307nwsy3p366i7f0x8v6873b6g1sl485dla20dbifpawbilh")))

(define-public crate-rand_simple-0.0.8 (c (n "rand_simple") (v "0.0.8") (h "0nmypj5nz5scld5dwagwn9pd3dmghl6xj7f4671800kvfv51shmw")))

(define-public crate-rand_simple-0.0.9 (c (n "rand_simple") (v "0.0.9") (h "1lyccsl298pj04ah7rq01z54f76k5znqd6yv06s0l5qaj1vx33n6")))

(define-public crate-rand_simple-0.0.10 (c (n "rand_simple") (v "0.0.10") (h "0zggcz5fw1s2d4drvl7gn9y7g7f0lwwnd1pil1v2qnfqq1xkav89")))

(define-public crate-rand_simple-0.0.11 (c (n "rand_simple") (v "0.0.11") (h "1hj713axwdcxbc5zi3api5c5xzx4niz4ll5rw00amn5p2h446h3w")))

(define-public crate-rand_simple-0.0.12 (c (n "rand_simple") (v "0.0.12") (h "1s4251pfxf5kfhl4j5xr38dyv3cpzxg6bj9i5gnvvycygg83jza8")))

(define-public crate-rand_simple-0.0.13 (c (n "rand_simple") (v "0.0.13") (h "1h2fmfdm3m6alqc2i471y9h1gmbdf1ga5q53vvi9ahnf3y7ws1q3")))

(define-public crate-rand_simple-0.0.14 (c (n "rand_simple") (v "0.0.14") (h "1nf8h42hmmljlgr0shxvbf9rax4xby6ishszfl89jlnhwd5lndkr")))

(define-public crate-rand_simple-0.0.15 (c (n "rand_simple") (v "0.0.15") (h "1dffbfc2bl4w66yk70x6pfwrw0aj1gvb99ix545mj87va1x0cqdv")))

(define-public crate-rand_simple-0.0.16 (c (n "rand_simple") (v "0.0.16") (h "17bs1x9cgnnxlxsblyhdvsmm8w5ncykljh1gkmh8ri5jc14qxbcd")))

(define-public crate-rand_simple-0.0.17 (c (n "rand_simple") (v "0.0.17") (h "0wq25nkiia6d851i59xgf8wlgn4h641y5fnchdxya3aqqsm9sl7m")))

(define-public crate-rand_simple-0.0.18 (c (n "rand_simple") (v "0.0.18") (h "0v6l1g35pzq537rz4adl3g4jvzyar2xxb9j44wmpf6r3gbhdxq9j")))

(define-public crate-rand_simple-0.0.19 (c (n "rand_simple") (v "0.0.19") (h "12d7ywm62lidxhd540pi9rsvhkq2y0d88blfnf3j2pykdsd7x5n8")))

(define-public crate-rand_simple-0.0.20 (c (n "rand_simple") (v "0.0.20") (h "1a36bk16sx8l7ws6ikc0yhvwwhsm94g6aqkm5kpf5xjaxgk6aphz")))

(define-public crate-rand_simple-0.0.21 (c (n "rand_simple") (v "0.0.21") (h "1nzjl68zwf6py6m112fwm32xnxwx24php9cyi4dxp4ssj064x66w")))

(define-public crate-rand_simple-0.0.22 (c (n "rand_simple") (v "0.0.22") (h "1p3lz91iadj4s76qpyxh031wvlxmhg7lk0qcp9740f2s7a0f27bx")))

(define-public crate-rand_simple-0.0.23 (c (n "rand_simple") (v "0.0.23") (h "13p5sbzvxv4nfb8sj8l3d6zhyag8dfsykq20nlj005fd75pg3bbs")))

(define-public crate-rand_simple-0.0.24 (c (n "rand_simple") (v "0.0.24") (h "0023g8zz9vzjc21fyfrm4gkkc774xdcp6wk901parza2iz38kr1c")))

(define-public crate-rand_simple-0.0.25 (c (n "rand_simple") (v "0.0.25") (h "1b0w9iqnr61s9sd775lzbgwi9296wdrl5dxgspmbky79d8kmx98w")))

(define-public crate-rand_simple-0.0.26 (c (n "rand_simple") (v "0.0.26") (h "1x4mc4d2rm9qag5lsfc5059nx2z52jdlgsgw62mlvxcabmx4xsfj")))

(define-public crate-rand_simple-0.0.27 (c (n "rand_simple") (v "0.0.27") (h "0y84wjzwq7lgv52syn0r26pnqfji5az3q128vgbsz804i7cnyn06")))

(define-public crate-rand_simple-0.0.28 (c (n "rand_simple") (v "0.0.28") (h "0ry01538m1bd7spcgw8fdvy2i9sz3zs8y8gs57gwskmfl4nir5mh")))

(define-public crate-rand_simple-0.0.29 (c (n "rand_simple") (v "0.0.29") (h "09yqhn5awk12ycdzh9lf44rj829rzj8i4xb4g5lf6ywpvm0vd5sq")))

(define-public crate-rand_simple-0.0.30 (c (n "rand_simple") (v "0.0.30") (h "17iiipbby1b1c9c63545qspwd9bqj75lf9nkbbnmwvgdjf0wzi0f")))

(define-public crate-rand_simple-0.0.31 (c (n "rand_simple") (v "0.0.31") (h "1bdq422sxq6jkjnvw6j86whr66hiz9s7hvy9hsg0ivxfh7llxkgj")))

(define-public crate-rand_simple-0.0.32 (c (n "rand_simple") (v "0.0.32") (h "1f9wi98zpd2fmy93432b5yw7phcsr335ynhlaxj6ff5qkl8x1936")))

(define-public crate-rand_simple-0.0.33 (c (n "rand_simple") (v "0.0.33") (h "079hl57xfw6gbxjlqd7q24bgi5pc78s5ic2kbsl7z536sn3dik0q")))

(define-public crate-rand_simple-0.0.34 (c (n "rand_simple") (v "0.0.34") (h "0ks8c0a0f5s5319cp14yjr6gcbrmimzky4zz2agkz243ygna4b7x")))

(define-public crate-rand_simple-0.0.35 (c (n "rand_simple") (v "0.0.35") (h "1bcz930nnag2mv8qcrf654yi92gr0884snchyb500nl99nnl3v8x")))

(define-public crate-rand_simple-0.0.36 (c (n "rand_simple") (v "0.0.36") (h "1xjbfhjxs0sv8fr4859d8a9nlcspnzk0nj9lqws6ld1hy7d8lw6w")))

(define-public crate-rand_simple-0.1.0 (c (n "rand_simple") (v "0.1.0") (h "14bil0jj3flpw0j9c13ywy50ckc5fqpbncip1snwgasyhychik63")))

(define-public crate-rand_simple-0.1.1 (c (n "rand_simple") (v "0.1.1") (h "1gvjdz4cvhhx6l6in7lgw69l3a0agslkr71qr6pdgqnbz3nr2l59")))

(define-public crate-rand_simple-0.1.2 (c (n "rand_simple") (v "0.1.2") (h "0yvy1qima59w1h7zsqpf9rqwnq39b8fh7nh8ryl3cnrmm0jnflpg")))

(define-public crate-rand_simple-0.1.3 (c (n "rand_simple") (v "0.1.3") (h "14zg76z2xrz5y65sis2ksr2fr1cbps13y7iww0nf2xzl2nq379bs")))

(define-public crate-rand_simple-0.1.4 (c (n "rand_simple") (v "0.1.4") (h "06l67bjibli137a2bfh6iym32hz7jl8j4xnwpi3zyyhssv62s1qr")))

(define-public crate-rand_simple-0.1.5 (c (n "rand_simple") (v "0.1.5") (h "02m2m5kd4m2kfqq1k3q4ra2xgl462ywvr5n6ry74a5g8j2rvj82q")))

(define-public crate-rand_simple-0.1.6 (c (n "rand_simple") (v "0.1.6") (h "1r47zq1rcyrklbiv0c82msfamn59xavwjiz1ln6r9iqc0744b94l")))

(define-public crate-rand_simple-0.1.7 (c (n "rand_simple") (v "0.1.7") (h "06czca9fd0jah8vypcdszx9qv6pzyq78bsinfipga8wx4x54wx3i")))

(define-public crate-rand_simple-0.1.8 (c (n "rand_simple") (v "0.1.8") (h "0k0whql7q4n61lq6igbzkypzfbzyz420y9rniyq1x35s4kss65na")))

(define-public crate-rand_simple-0.1.9 (c (n "rand_simple") (v "0.1.9") (h "1x9cp0wig0dy6fgn7dh3mmm1kf4a26fjnq02a9pa6l3r1jsk5a6g")))

(define-public crate-rand_simple-0.1.10 (c (n "rand_simple") (v "0.1.10") (h "03vq2cr7fn0wf6xynvhs6m5hibd1zlrmddf229j015dz3cyz6066")))

(define-public crate-rand_simple-0.1.11 (c (n "rand_simple") (v "0.1.11") (h "0r92xgnk35rz3f5x38w11qy9qf061kvkycawkpsa15af8g38rh3k")))

(define-public crate-rand_simple-0.2.0 (c (n "rand_simple") (v "0.2.0") (h "0rz6xj856v2qar2xw8avbpjhbbbq2d9n8liz438rg3qqwjjbwig7")))

(define-public crate-rand_simple-0.2.1 (c (n "rand_simple") (v "0.2.1") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "0jlzg40cy8qkw2n4wwfk7l3q3in0sd559kpxmbxlf9kzh5rvyphq")))

(define-public crate-rand_simple-0.2.2 (c (n "rand_simple") (v "0.2.2") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "0xb9gg2nwz5spwb6fxw1qh4q48m88w4fi0cwlagmxl13xw284sqw")))

(define-public crate-rand_simple-0.2.3 (c (n "rand_simple") (v "0.2.3") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "030d5ladmrw9aa6wfv1la63bzr90p70acjx5gikwi45b0fhznxn2") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-rand_simple-0.2.4 (c (n "rand_simple") (v "0.2.4") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "0q0yf4r0lgwqcvy6g3if1wgvafrhzf565icqrsdd4gf6r14psq5q")))

(define-public crate-rand_simple-0.2.5 (c (n "rand_simple") (v "0.2.5") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 2)))) (h "1yidgbhc92vkkphs6r8q3hhz4nlq8qrjbaym3wn6nmcigj88pn6d")))

(define-public crate-rand_simple-0.2.6 (c (n "rand_simple") (v "0.2.6") (h "0k4fsmq16syl1v3najyxckn664a1ik2125fyf484np3sd5hysss4")))

(define-public crate-rand_simple-0.2.7 (c (n "rand_simple") (v "0.2.7") (h "1wpzwn0nnpa8419ac9grhgmayf3cixczif1c12cs74i9j6v947cd")))

(define-public crate-rand_simple-0.2.8 (c (n "rand_simple") (v "0.2.8") (h "030k34jm113vx6gbd6bvq23rdgj2gcw3ybqs742f9rb6hllp59ha")))

(define-public crate-rand_simple-0.2.9 (c (n "rand_simple") (v "0.2.9") (h "04ldqynclxivzs39llh0gr2b61d8gswd511s3yfmhv1mnx7xigxa")))

(define-public crate-rand_simple-0.2.10 (c (n "rand_simple") (v "0.2.10") (h "0d6cqgs7wkn7s04nv3y5r7idh8fzchiqwa0lb7s26fxfi8xip9y1")))

(define-public crate-rand_simple-0.2.11 (c (n "rand_simple") (v "0.2.11") (h "0n75pgxs9m3j0w8i0v8icjp0505v65l0hvvl6rdgxmcra09hvqix")))

(define-public crate-rand_simple-0.2.12 (c (n "rand_simple") (v "0.2.12") (h "0ny77g0hy0mcw4n0xa8zfp049rdr7bmw7rywgh6lr8c23sxvc5wg")))

(define-public crate-rand_simple-0.2.13 (c (n "rand_simple") (v "0.2.13") (h "0p7lxzv4k60qs4r45hscqqdk4zyrpx2jzzqrcsd02z4scblh1n32")))

(define-public crate-rand_simple-0.2.14 (c (n "rand_simple") (v "0.2.14") (h "064cg604vph3mk099zw0dd9qfafg54gvsj4vz4q5jm5mha2l2d79")))

(define-public crate-rand_simple-0.2.15 (c (n "rand_simple") (v "0.2.15") (h "136y7j2gmnssmbyvm6ll73zxc7fnw8smgdx26a7pkvrrbh6v622z")))

(define-public crate-rand_simple-0.2.16 (c (n "rand_simple") (v "0.2.16") (h "0bpjjdc1v35gvy6sn8wv6fax92pq06kl4zgr31n16cihzywrx7b9")))

(define-public crate-rand_simple-0.2.17 (c (n "rand_simple") (v "0.2.17") (h "19lgw7qg8d4dri3mc9rqbg53xgnpznzki0wb0995bkm628wgwcia")))

(define-public crate-rand_simple-0.2.18 (c (n "rand_simple") (v "0.2.18") (h "1ydiayhgjyk52ii0amj9skgjh7s1fryz7mrwrdpc5akb5dq3nwgv")))

(define-public crate-rand_simple-0.2.19 (c (n "rand_simple") (v "0.2.19") (h "1qz2x4mrpv7snsd38jwa58vixj8y85j2gzc1k6b8dy9cfjbbk284")))

(define-public crate-rand_simple-0.2.20 (c (n "rand_simple") (v "0.2.20") (h "1vcdq745qn56g4ilraaf5h1syymayb6k97pc6civnx70awcw0jn6")))

(define-public crate-rand_simple-0.2.21 (c (n "rand_simple") (v "0.2.21") (h "14imnhi80q2s3133pgwqcnlgqi2cwdajv7bgz019z22b54qmppb5")))

(define-public crate-rand_simple-0.2.22 (c (n "rand_simple") (v "0.2.22") (h "05qcmi1lnbb8kkcznlm505mly55v9xvdz07pd4v1vlmmjqffjfc6")))

(define-public crate-rand_simple-0.2.23 (c (n "rand_simple") (v "0.2.23") (h "0hnigqxd54my6zhp6kfsrvb04nrf16nq7pb3n03pyjs232iq3c56")))

(define-public crate-rand_simple-0.2.24 (c (n "rand_simple") (v "0.2.24") (h "1ihn5hca91l84a0zdafx3q65if92hw5rn6q09j27v32qjj3vr39k")))

(define-public crate-rand_simple-0.2.25 (c (n "rand_simple") (v "0.2.25") (h "1ijp3b5kx46zs3i2fq3102b9hhbm2vp294xnvxj452gzws1hby8q")))

(define-public crate-rand_simple-0.2.26 (c (n "rand_simple") (v "0.2.26") (h "1agwg8nn008ss3dnb2s4nsvcnw8d703cg1z80426vy5kyj4hkwch")))

(define-public crate-rand_simple-0.2.27 (c (n "rand_simple") (v "0.2.27") (h "0ym42khq0xn21kg67lxp28rwbyifqzyimbndd0qr39bach22c81a")))

(define-public crate-rand_simple-0.2.28 (c (n "rand_simple") (v "0.2.28") (h "1jkv4hxh8v8q7i6hwzlym3f6h07f732rgqzmv16g457x52prgn05")))

