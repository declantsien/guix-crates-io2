(define-module (crates-io ra nd rand-extra) #:use-module (crates-io))

(define-public crate-rand-extra-0.1.0 (c (n "rand-extra") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0psbk9qnn4iygyp6h7rj4crfcwda0g3xfh93migg3yy18haydsdn")))

