(define-module (crates-io ra nd random_access_unicode) #:use-module (crates-io))

(define-public crate-random_access_unicode-0.1.0 (c (n "random_access_unicode") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1ln7px7hn7v53i82qzj2xzygc0c99vjym1yhjpwjbdqxvr2ddjns")))

(define-public crate-random_access_unicode-0.2.0 (c (n "random_access_unicode") (v "0.2.0") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1vr6lcnhkf2c45ldvc0x1zl0scsbjxch05miyf8zvzzdvn186za9")))

(define-public crate-random_access_unicode-0.2.1 (c (n "random_access_unicode") (v "0.2.1") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1xq05jvza6my01lg6y9gjg0iz1wxpx1bjspz4y55rrv8b5xx1ix9")))

(define-public crate-random_access_unicode-0.2.2 (c (n "random_access_unicode") (v "0.2.2") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0z9fcvvqjf90sy19fjc01dj0f5s9sdpvr3l2cv0l95dqv1i0p24b")))

(define-public crate-random_access_unicode-0.2.3 (c (n "random_access_unicode") (v "0.2.3") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1ws582gp5z165mr4fhfydaijcwp8q9ml7cdpc680lqrqivp8m8mf")))

(define-public crate-random_access_unicode-0.2.4 (c (n "random_access_unicode") (v "0.2.4") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1bk1h6pr098frvfxz6lpbc49dliv2bsg0ybziyszzb5hg8agnd5l")))

(define-public crate-random_access_unicode-0.2.5 (c (n "random_access_unicode") (v "0.2.5") (d (list (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0p3z3pxpmnsca8hkh4pcsw6r7yv5p18m8qmpd3p1amnhlc2xz9zm")))

