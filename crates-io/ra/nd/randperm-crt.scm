(define-module (crates-io ra nd randperm-crt) #:use-module (crates-io))

(define-public crate-randperm-crt-0.1.0 (c (n "randperm-crt") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "1jnpfqqp34gk9knn8hqiy2iyz9s5x9v9vkyk4s0vpykz4swkdc36")))

(define-public crate-randperm-crt-0.1.1 (c (n "randperm-crt") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)))) (h "056vn37cza96hsc6xrz7syscqkhxva1yyi6ph21bsnbhnj8s7jy5")))

