(define-module (crates-io ra nd rand_seeder) #:use-module (crates-io))

(define-public crate-rand_seeder-0.1.0 (c (n "rand_seeder") (v "0.1.0") (d (list (d (n "rand_core") (r ">= 0.1, <= 0.5") (k 0)))) (h "0zq53iy0b81p4mfr81dkwwqdicpli4fqjs4rfawvqsd1rvvidz9j")))

(define-public crate-rand_seeder-0.2.0 (c (n "rand_seeder") (v "0.2.0") (d (list (d (n "rand_core") (r ">= 0.1, <= 0.5") (k 0)))) (h "088ilfsp78snz31zp554y5qka335rlfqj39abn7i1815vci0q3kx")))

(define-public crate-rand_seeder-0.2.1 (c (n "rand_seeder") (v "0.2.1") (d (list (d (n "rand_core") (r ">=0.1, <=0.5") (k 0)))) (h "19r5q5ly0az8l7657qx36yr4hg9mjjv35z6n4n0qcm7lqg40rgy4")))

(define-public crate-rand_seeder-0.2.2 (c (n "rand_seeder") (v "0.2.2") (d (list (d (n "rand_core") (r ">=0.1, <=0.6") (k 0)))) (h "1l6pcqp98y4546y8mh6gvsc2g50izdj1r7f2nhsi6lwxjjcdcbb1")))

(define-public crate-rand_seeder-0.2.3 (c (n "rand_seeder") (v "0.2.3") (d (list (d (n "rand_core") (r ">=0.1, <=0.6") (k 0)))) (h "1nqbqhx28f7fwnha7qa2nis4nj7rcki8v00fllcjga0axym90a6g")))

