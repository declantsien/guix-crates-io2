(define-module (crates-io ra nd rand_map) #:use-module (crates-io))

(define-public crate-rand_map-0.1.0 (c (n "rand_map") (v "0.1.0") (d (list (d (n "hashers") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c10c7f20k3rhhav4mdqr4l1w575gq25m5vvyyxj0liidpv68cfa") (f (quote (("serialize" "serde"))))))

