(define-module (crates-io ra nd rand_derive) #:use-module (crates-io))

(define-public crate-rand_derive-0.1.0 (c (n "rand_derive") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.5") (d #t) (k 2)) (d (n "parse-generics-shim") (r "^0.1") (d #t) (k 0)) (d (n "parse-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "121sq20qfrix67qfhghbzyawm0dxnvmbazv4vs7cmqx7iskf17i8")))

(define-public crate-rand_derive-0.2.0 (c (n "rand_derive") (v "0.2.0") (d (list (d (n "macro-attr") (r "^0.2.0") (d #t) (k 2)) (d (n "parse-generics-shim") (r "^0.1") (d #t) (k 0)) (d (n "parse-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1a7asvay01z7hj2s2y03a2jaz9c0wyc69pgwici8m8hnpq8alm81")))

(define-public crate-rand_derive-0.3.0 (c (n "rand_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0f3kr1c4k2k9bwrlcwa6cnldsdacbdyg69qh0v0b2klh6q54zspl")))

(define-public crate-rand_derive-0.3.1 (c (n "rand_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0skcgm9pys12r2xhfva21yx4pf3a53ma71i3rfjl5fhvpcz3fd2x")))

(define-public crate-rand_derive-0.5.0 (c (n "rand_derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1bz1skmpjfm0xv96ac9xvyqn3rm7c2qk7l3xjiwjvk5wsl1h7w0j")))

