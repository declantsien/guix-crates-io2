(define-module (crates-io ra nd random_derive) #:use-module (crates-io))

(define-public crate-random_derive-0.1.0 (c (n "random_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1x7mm84pd1xi42l4as746x58cdy3r2hq6cwzgz881h7ibv0x88cg") (y #t)))

