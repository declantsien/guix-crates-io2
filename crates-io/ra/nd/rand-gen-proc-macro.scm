(define-module (crates-io ra nd rand-gen-proc-macro) #:use-module (crates-io))

(define-public crate-rand-gen-proc-macro-0.1.0 (c (n "rand-gen-proc-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "1pg57pndilhkkhxn9ybda9ggkasjff7jcw1arcxyk71zm12d7sim")))

(define-public crate-rand-gen-proc-macro-0.1.1 (c (n "rand-gen-proc-macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.73") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0hiraiy699y18zgxadbf2j921wmjsfqs4gkxq1x7wq0gp2lxvkng")))

