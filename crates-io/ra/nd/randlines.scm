(define-module (crates-io ra nd randlines) #:use-module (crates-io))

(define-public crate-randlines-0.1.0 (c (n "randlines") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "00967f2gk13iww3x3g3gx0w3d5z2b74q50xmfxr7ppf5af1f0461")))

(define-public crate-randlines-0.1.1 (c (n "randlines") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "07izclv7wj0656a2svi350xll921fvzinv93vaj10wvwhgc7jc1h")))

(define-public crate-randlines-0.1.2 (c (n "randlines") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "14zass2q9siys1bfx5xrs0fcpzrgpv3xv5a5b07d7lzmqhq5iik8")))

(define-public crate-randlines-0.1.3 (c (n "randlines") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0il45n40gyhwsf2xrvvjbb25yjfn0yz9nlajdhqscqwk57msqfj2")))

