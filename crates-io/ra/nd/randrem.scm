(define-module (crates-io ra nd randrem) #:use-module (crates-io))

(define-public crate-randrem-1.0.0 (c (n "randrem") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "human-panic") (r "^1.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "111py1ji9w6zqkfx6jcw03m6fwh6blz5mymxlnmchdkkgm6f3xvw")))

