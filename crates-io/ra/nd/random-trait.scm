(define-module (crates-io ra nd random-trait) #:use-module (crates-io))

(define-public crate-random-trait-0.1.0 (c (n "random-trait") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1wcg61qnmrsmkcv05sd30amx08wdjk0i1x93r2avlj0dvpmxhqv4") (f (quote (("u128"))))))

(define-public crate-random-trait-0.1.1 (c (n "random-trait") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0iw4laa9i97x1m1mc72rx0km0j6pjdrb75b0c93fdaq45spqcc8d") (f (quote (("u128"))))))

