(define-module (crates-io ra nd rand_xorshift) #:use-module (crates-io))

(define-public crate-rand_xorshift-0.1.0 (c (n "rand_xorshift") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1qvv33cqli3kx0q1qiyvv37zklmn8h4hdrnx5c0dn63ylk53zypg") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-rand_xorshift-0.1.1 (c (n "rand_xorshift") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "0p2x8nr00hricpi2m6ca5vysiha7ybnghz79yqhhx6sl4gkfkxyb") (f (quote (("serde1" "serde" "serde_derive"))))))

(define-public crate-rand_xorshift-0.1.2 (c (n "rand_xorshift") (v "0.1.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.38") (o #t) (d #t) (k 0)))) (h "1vwyvly2bgni246j8xls83axvcjz9p563vynx5rqqll8hizsza97") (f (quote (("serde1" "serde" "serde_derive")))) (y #t)))

(define-public crate-rand_xorshift-0.2.0 (c (n "rand_xorshift") (v "0.2.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a6wy76lc5fimm1n9n8fzhp4cfjwfwxh4hx63bg3vlh1d2w1dm3p") (f (quote (("serde1" "serde"))))))

(define-public crate-rand_xorshift-0.3.0 (c (n "rand_xorshift") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (o #t) (k 0)))) (h "13vcag7gmqspzyabfl1gr9ykvxd2142q2agrj8dkyjmfqmgg4nyj") (f (quote (("serde1" "serde"))))))

