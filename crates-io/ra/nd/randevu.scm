(define-module (crates-io ra nd randevu) #:use-module (crates-io))

(define-public crate-randevu-1.0.0 (c (n "randevu") (v "1.0.0") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "11jnf3mbnhq3bachsm652bny6p3adxl8zb0kd6jbsnzhma1gb16x")))

(define-public crate-randevu-1.0.1 (c (n "randevu") (v "1.0.1") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)))) (h "04didvygz8h36azdlrd02r29an28gg4lzkvd1b3wxp10170wb9fm")))

(define-public crate-randevu-1.0.2 (c (n "randevu") (v "1.0.2") (d (list (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)))) (h "0qifx9f41maggw2hqfxkils54lha461dlscjvzqvlykj0hf24yn8")))

(define-public crate-randevu-2.0.0 (c (n "randevu") (v "2.0.0") (d (list (d (n "blake3") (r "^1.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "itoa") (r "^1.0.11") (d #t) (k 0)))) (h "0b316mc7y1bbnjjcqpafwzwx7ymyql9g2m4ciawhsn0l3zlvxwlq")))

