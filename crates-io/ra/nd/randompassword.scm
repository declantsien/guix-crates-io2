(define-module (crates-io ra nd randompassword) #:use-module (crates-io))

(define-public crate-randompassword-0.1.0 (c (n "randompassword") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02wjhzh80nyqvv8w02vwb29b1n151c6qrrb33pkmga88gh2nf2hr") (y #t)))

(define-public crate-randompassword-1.0.0 (c (n "randompassword") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "09nc17wxy3w9nicssgr1dsz53a4yif4rghv1qdcplk0a4m8288vr")))

