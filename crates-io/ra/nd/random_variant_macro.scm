(define-module (crates-io ra nd random_variant_macro) #:use-module (crates-io))

(define-public crate-random_variant_macro-0.1.0 (c (n "random_variant_macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07f12i6bwczmmlpb4hqafwpgnlhg4k4vmkgzcskgypf2mx1z29r5")))

(define-public crate-random_variant_macro-0.2.0 (c (n "random_variant_macro") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07l72i9s3xd20bgj1n0s96419nw72jd8g7z8a2gjl4qycksf48mm")))

