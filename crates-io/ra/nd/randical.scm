(define-module (crates-io ra nd randical) #:use-module (crates-io))

(define-public crate-randical-0.1.0 (c (n "randical") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "rand") (r "~0.5.5") (d #t) (k 0)))) (h "0dy2f24znzf06yyv9l4vhn84qf9kzdw48ax1wrxfbnd8s9lly18k")))

(define-public crate-randical-0.1.1 (c (n "randical") (v "0.1.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "rand") (r "~0.5.5") (d #t) (k 0)))) (h "0vrcw8c5skbn1wlbzj9v3r419p6j689viif4jryvgz0z954l27kv")))

(define-public crate-randical-1.6.1 (c (n "randical") (v "1.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1y56y6il7w4m7sivqkr8m9qq1fh22cwlswa7pygv6mlamynl5kgy")))

(define-public crate-randical-1.6.18 (c (n "randical") (v "1.6.18") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1ydjanm5yvpkf3yfqarybm1p4za7rxwiir4yfqy7r317sjb1a4y4") (y #t)))

(define-public crate-randical-1.6.180 (c (n "randical") (v "1.6.180") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "1b8k1r5j1jrl087dghjzg60l1aa2wa89lan1lxlpb1md5787mm5z")))

(define-public crate-randical-1.6.1803 (c (n "randical") (v "1.6.1803") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "0lwy0fpcx0z07zgab7zq5xj0399rjjqmg9n8vymrg26vvanyq47b")))

(define-public crate-randical-1.6.18033 (c (n "randical") (v "1.6.18033") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "svix-ksuid") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (k 0)))) (h "0zc6h8psxjnlf3clilbvd913z0rba7n5j9rhwv3dx86k0k02r75k")))

