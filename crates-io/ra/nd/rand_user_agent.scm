(define-module (crates-io ra nd rand_user_agent) #:use-module (crates-io))

(define-public crate-rand_user_agent-0.0.1 (c (n "rand_user_agent") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1n84cndhxm0l28ii1hs9zzk755lv9w7y9pk24s6n21abfnagjqzx") (y #t)))

(define-public crate-rand_user_agent-0.0.2 (c (n "rand_user_agent") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1y4fmar2ykb7m87s43mi39hxnm3r2gf7smyqhry5zdrk8jlnz32x") (y #t)))

(define-public crate-rand_user_agent-0.0.3 (c (n "rand_user_agent") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "085g4s5ccmd850sbrw19hwldb7fgb5d5va17sf9473w5i213xxxx")))

(define-public crate-rand_user_agent-0.1.0 (c (n "rand_user_agent") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ymmgw42idnhcgxywbm1m1ih04ppfwzcmc51b1mmkbkgkl5ksdvj")))

