(define-module (crates-io ra nd randomize) #:use-module (crates-io))

(define-public crate-randomize-0.789.0 (c (n "randomize") (v "0.789.0") (h "0is2y1hn0l4838crnvx9zc469ml3aq75fq9ipn2wrk0l1nqrmz75")))

(define-public crate-randomize-0.890.0 (c (n "randomize") (v "0.890.0") (h "0ac8v7m9kib36lh5rqfxn79vnbks1nmbdbwk94jm1i4w5nl4y68q")))

(define-public crate-randomize-0.901.0 (c (n "randomize") (v "0.901.0") (d (list (d (n "rand_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01jhzdhycpx3pz996ssm34ds8panx82w2h18qa68y6j9rlxqq5r0") (f (quote (("std") ("serde1" "serde" "serde_derive") ("randcore" "rand_core") ("default" "std"))))))

(define-public crate-randomize-1.0.0 (c (n "randomize") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1v6p0vmk3i9yx88yq1xn8khh5h1rlmx52q8a6pbxibssvg3n287m")))

(define-public crate-randomize-2.0.0 (c (n "randomize") (v "2.0.0") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("i128"))) (d #t) (k 0)))) (h "1700m55hklmfpar21qm8k3gav0i0s92n1krx0x29zpfqydd9yrfv") (f (quote (("std_stuff") ("serde_support" "serde" "serde_derive") ("default"))))))

(define-public crate-randomize-2.1.0 (c (n "randomize") (v "2.1.0") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("i128"))) (d #t) (k 0)))) (h "1df7x400k551r31vmdxkbswa859bhsp572dihy3dlmbrfkxg2r0v") (f (quote (("serde_support" "serde" "serde_derive") ("default"))))))

(define-public crate-randomize-2.2.0 (c (n "randomize") (v "2.2.0") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("i128"))) (d #t) (k 0)))) (h "1ibp0j61fxzvflzpvzymbcs0j9wgc8pkdjxj6nd5xnkk05w8l2jb") (f (quote (("serde_support" "serde" "serde_derive") ("default"))))))

(define-public crate-randomize-2.2.1 (c (n "randomize") (v "2.2.1") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("i128"))) (d #t) (k 0)))) (h "00qcbiawgb03nlsb76jlh60b5a9f3hicnxf1rj99z9vdbnw9mjrx") (f (quote (("serde_support" "serde" "serde_derive") ("default"))))))

(define-public crate-randomize-2.2.2 (c (n "randomize") (v "2.2.2") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (k 0)) (d (n "typenum") (r "^1.10") (f (quote ("i128"))) (d #t) (k 0)))) (h "11rricfkcg5w1w05gqsnzy0cak27x5zr1h9w3vlrr538131aplzi") (f (quote (("serde_support" "serde" "serde_derive") ("default"))))))

(define-public crate-randomize-3.0.0-alpha.1 (c (n "randomize") (v "3.0.0-alpha.1") (d (list (d (n "lokacore") (r "^0.0.2") (d #t) (k 0)))) (h "1dnhbjd77iq79sy6fh6srg6w236qg0ihl3p74jjk60sw2hkcq3fk")))

(define-public crate-randomize-3.0.0-alpha.2 (c (n "randomize") (v "3.0.0-alpha.2") (h "1fh8yaxd0w8qf7xc8i56039lc809aibaa5gacckpq2gmblglhk2q")))

(define-public crate-randomize-3.0.0-alpha.3 (c (n "randomize") (v "3.0.0-alpha.3") (h "0hdsh9ybq9b0vzll5lbdp6na1fq2wfq2zhqa116sxhalc7lalbvp")))

(define-public crate-randomize-3.0.0-rc.1 (c (n "randomize") (v "3.0.0-rc.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09wqs290k900sz2v7y88wlgjpqabw1yl6zkzlkn1xnqkvvx9d6d2")))

(define-public crate-randomize-3.0.0-rc.2 (c (n "randomize") (v "3.0.0-rc.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qa0v4l05vlispmgsj2nfba0d2jpygybdd2iwl4vbfkk0sl1hcnj")))

(define-public crate-randomize-3.0.0-rc.3 (c (n "randomize") (v "3.0.0-rc.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sxfh1h7j72n24sn81gp61parpb75flsvb9d1lqfyy125jq8pkvn")))

(define-public crate-randomize-3.0.0 (c (n "randomize") (v "3.0.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0prazwdly09wfcrf9pash7mw0x6wdqwavsrdn3hizqcixrj0yvxx")))

(define-public crate-randomize-3.0.1 (c (n "randomize") (v "3.0.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02ll7r3rrpmhjx34w91m1yvqw1685bq2n9amqvycjcqznncqrhw8")))

(define-public crate-randomize-4.0.0-alpha.1 (c (n "randomize") (v "4.0.0-alpha.1") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0j417d309lzid770wpqhkm4w135l7hv5nki9nrz20ijjd7gzg0ak")))

(define-public crate-randomize-4.0.0-alpha.2 (c (n "randomize") (v "4.0.0-alpha.2") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1baxnazpaw30zn8dfgg22g7vmma7kdzr6ngqir17c2gsyqanmb4m")))

(define-public crate-randomize-4.0.0-alpha.3 (c (n "randomize") (v "4.0.0-alpha.3") (d (list (d (n "getrandom") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0m4vkgm161q51ww9bvf0kram9cxg8j3p80rl9w1fzpgkwcwbqhpm")))

(define-public crate-randomize-5.0.0 (c (n "randomize") (v "5.0.0") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (o #t) (d #t) (k 0)))) (h "1cs88fkc6ar90gm828dnqj5bp6vkr6gqph1y20l6bysq41c6c245")))

