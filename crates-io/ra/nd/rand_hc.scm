(define-module (crates-io ra nd rand_hc) #:use-module (crates-io))

(define-public crate-rand_hc-0.1.0 (c (n "rand_hc") (v "0.1.0") (d (list (d (n "rand_core") (r ">= 0.2, < 0.4") (k 0)))) (h "1i0vl8q5ddvvy0x8hf1zxny393miyzxkwqnw31ifg6p0gdy6fh3v")))

(define-public crate-rand_hc-0.1.1 (c (n "rand_hc") (v "0.1.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0g33l381hwfvxdcv502d5c88fh7gw78fkfmlp27jp8hhlfxzxam5") (y #t)))

(define-public crate-rand_hc-0.2.0 (c (n "rand_hc") (v "0.2.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0g31sqwpmsirdlwr0svnacr4dbqyz339im4ssl9738cjgfpjjcfa")))

(define-public crate-rand_hc-0.3.0 (c (n "rand_hc") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)))) (h "0wra6ar22zdjkry9dsq1mg620m4h3qb9s8rfykkz4im4crqfz41i")))

(define-public crate-rand_hc-0.3.1 (c (n "rand_hc") (v "0.3.1") (d (list (d (n "rand_core") (r "^0.6.0") (d #t) (k 0)))) (h "1rwpykyvhkxs4jvqdja3mzp9dqaqamzn113cxaigs9z2dmcry7nm")))

(define-public crate-rand_hc-0.3.2 (c (n "rand_hc") (v "0.3.2") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0m2grc3bjndds93mv55qw7qf1gapar080v2qpxi8vy3hcd7ksdkv")))

