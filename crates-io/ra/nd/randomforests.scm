(define-module (crates-io ra nd randomforests) #:use-module (crates-io))

(define-public crate-randomforests-0.1.0 (c (n "randomforests") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "00sngxnz014xy15223cn4girr4swnwi930zzcszcsbnlb2xlnbfw")))

(define-public crate-randomforests-0.1.1 (c (n "randomforests") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "03gcvk52n0xgnb56j67c4b9anddkkla45vdblicy03lhdzqr5vjb")))

