(define-module (crates-io ra nd rand-utf8) #:use-module (crates-io))

(define-public crate-rand-utf8-0.0.1 (c (n "rand-utf8") (v "0.0.1") (d (list (d (n "libc-print") (r "^0.1.19") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc" "small_rng"))) (k 2)))) (h "03z88qjf9s10ykkkdg4mbzx06d608xl06p1qq2gz9w12viy03wnh")))

