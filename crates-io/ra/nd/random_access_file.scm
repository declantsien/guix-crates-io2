(define-module (crates-io ra nd random_access_file) #:use-module (crates-io))

(define-public crate-random_access_file-0.1.0 (c (n "random_access_file") (v "0.1.0") (d (list (d (n "cfile-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0pd1d866gpcnm901h1v1z066q78fivmf8xwdkwk8gi8rv9sjcix8")))

(define-public crate-random_access_file-0.2.0 (c (n "random_access_file") (v "0.2.0") (d (list (d (n "cfile-rs") (r "^0.2.0") (d #t) (k 0)))) (h "0hb3zpzypp6zw5pb7vaps7pld6x7my4wdzci25jsrrwavpfk7qig")))

(define-public crate-random_access_file-0.3.0 (c (n "random_access_file") (v "0.3.0") (d (list (d (n "cfile-rs") (r "^0.2.0") (d #t) (k 0)))) (h "06qdfpdlg3zwmvqvvd2dmrwzkp2g2zjjj86g5j38qcii03qvqcph")))

(define-public crate-random_access_file-0.3.1 (c (n "random_access_file") (v "0.3.1") (d (list (d (n "cfile-rs") (r "^0.3.1") (d #t) (k 0)))) (h "0j52qkxc6yccgynn74q6ii4dr2ayhp4y6y6jlkarnzfnq6n21pn1")))

(define-public crate-random_access_file-0.3.2 (c (n "random_access_file") (v "0.3.2") (d (list (d (n "cfile-rs") (r "^0.3.3") (d #t) (k 0)))) (h "0fzbjdwm9hk0i46syr39vcnrsjak593a2ij413jph724nk7lpa6b")))

(define-public crate-random_access_file-0.3.3 (c (n "random_access_file") (v "0.3.3") (d (list (d (n "cfile-rs") (r "^0.3.3") (d #t) (k 0)))) (h "1vjh5nl4218fhqpzd6rp6gv9mdganszg1jnknbbsz3k21gaw44xm")))

(define-public crate-random_access_file-0.4.0 (c (n "random_access_file") (v "0.4.0") (d (list (d (n "cfile-rs") (r "^0.3.3") (d #t) (k 0)))) (h "0q2fimibmgjaw7mm3np7im2k6drx8w7gyh90bq2m66isyjxpsjmc")))

