(define-module (crates-io ra nd randomx4r) #:use-module (crates-io))

(define-public crate-randomx4r-0.1.0 (c (n "randomx4r") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx4r-sys") (r "^0.1.0") (d #t) (k 0)))) (h "03n48mnz0gz583g73ndxmn8sh4w3zb3bq21icxq031pi73k28hmh")))

(define-public crate-randomx4r-0.2.0 (c (n "randomx4r") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx4r-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1rifgxw2a8q2y68716i5gq1wxmngy08921ka27axsqjmah7m7w9b")))

(define-public crate-randomx4r-0.2.1 (c (n "randomx4r") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "randomx4r-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1nvdwc2vm4ljghff18gbr9jhrfqxsi7rxx6csvxylz43kgyyn8i1")))

