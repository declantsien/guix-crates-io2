(define-module (crates-io ra nd randata) #:use-module (crates-io))

(define-public crate-randata-0.1.0 (c (n "randata") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "172331pnwvx7b8frd1bzv07al73wylrmw3rhlrqwmyfmfxvb5q8s")))

(define-public crate-randata-0.2.0 (c (n "randata") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qq4qn6s39j2nwshq4mn56avlxx2qddbw11vjl4plm1w88h03npb")))

(define-public crate-randata-0.3.0 (c (n "randata") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fzhai18s2w42ma4l3r02q86j0gzjvl5dpm6i0frdahmra39y4i1")))

(define-public crate-randata-0.3.1 (c (n "randata") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fkpjxs2wg4glybk277h3y1p2jkc6s2w6psgbgmz0kccsqb2miyi")))

(define-public crate-randata-0.4.0 (c (n "randata") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18hm84n1gxcycsq413129qm1hjd8w9fwra5yy7zj46d93c8cgipn")))

(define-public crate-randata-0.4.1 (c (n "randata") (v "0.4.1") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0czjr2x18pjc84pnmz8snfv5nnh8gbm9jd3akcrnzn4smzagx7dd")))

(define-public crate-randata-0.4.2 (c (n "randata") (v "0.4.2") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "141zi57b192rk2la1653bhlj33jw2dgrzzb4mdlynq2ik21wv4z4")))

