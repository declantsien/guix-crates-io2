(define-module (crates-io ra nd random_grouping) #:use-module (crates-io))

(define-public crate-random_grouping-0.1.0 (c (n "random_grouping") (v "0.1.0") (d (list (d (n "nameof") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "0125xsh8kvh2ry72dq324gvh95704a7r0qbf3i6i18apxc4j537j")))

(define-public crate-random_grouping-0.2.0 (c (n "random_grouping") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "1v9hwh9csam4flrf24if667aa5cjq3ffil71zj8gd5a9ndsc94dl")))

(define-public crate-random_grouping-0.2.1 (c (n "random_grouping") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "081ywndzps8rgh34gba7ji3pwp9wmkbfv7kb5zdprzcvpnaz1kk2")))

(define-public crate-random_grouping-0.2.2 (c (n "random_grouping") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "191zjsx7bx3zrn5xmzxmhl9mdjzgx4qy92j2pndffhf2ka33psqp")))

(define-public crate-random_grouping-0.2.3 (c (n "random_grouping") (v "0.2.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "1cpv4ls785nwq565wf55y5b5n95k5czfxa0xplvb9i1bvnswqg6r")))

(define-public crate-random_grouping-0.3.0 (c (n "random_grouping") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "0ch9b803i6ybyslspi0bfki133k9cfg6ch4x6wnsl0x01azcm9yp")))

(define-public crate-random_grouping-0.3.1 (c (n "random_grouping") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 0)) (d (n "simple_scan") (r "^0.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "test_panic") (r "^0.1") (d #t) (k 2)))) (h "18vi57bwx3fghd0rgvvigy38kzmaxq69h00yiplgpps0qkic8zb7")))

