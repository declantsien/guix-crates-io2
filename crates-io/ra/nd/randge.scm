(define-module (crates-io ra nd randge) #:use-module (crates-io))

(define-public crate-randge-1.0.0 (c (n "randge") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1msar7kbv29bi84nq3g4xnlyqyp4w7302p6gka3sscwjkg329lg7") (f (quote (("default" "rand"))))))

(define-public crate-randge-1.1.0 (c (n "randge") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0a7d57srnfhlvj7c0n65kn7688a7hgq7ys0p1fib1ly1lh9zj3c5") (f (quote (("default" "rand"))))))

