(define-module (crates-io ra nd random-tag) #:use-module (crates-io))

(define-public crate-random-tag-0.1.5 (c (n "random-tag") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0s7rzygjij4la9vhkdfd20qrc30r60kshxpypmdwngmv4inc2sbp")))

(define-public crate-random-tag-0.1.6 (c (n "random-tag") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0symivc4cv1r57i7pwv4ydsbrhcrip2fg3syah10d04jvwclx1lc")))

(define-public crate-random-tag-0.1.7 (c (n "random-tag") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12rwn6ay973f5jhcq4crz7djx65mc1a80fm6m5igkblj3nizyqn5")))

(define-public crate-random-tag-0.1.8 (c (n "random-tag") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1cn2843dbw11326iq4xnn8pq97yhlnba5b2k2wbn8llsn5ng9mp7")))

(define-public crate-random-tag-0.1.10 (c (n "random-tag") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1yrygq658qvvpbzc3bdzap1v5ycpvckqxa68qji5bhkjdag8dg4r")))

(define-public crate-random-tag-0.1.11 (c (n "random-tag") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "085a736iw9rgjw8id0kj75k1paki5ahnlsrk2yqvir58r4wkajyi") (r "1.59")))

(define-public crate-random-tag-1.0.0-beta.0 (c (n "random-tag") (v "1.0.0-beta.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("default" "derive" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4") (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "redb") (r "^1.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "local-offset"))) (d #t) (k 0)))) (h "1cninwvrmrf3njk1hzhnf0rcdr4yl96ijq3rs3waxsh6as331r7m") (r "1.73")))

