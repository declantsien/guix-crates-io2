(define-module (crates-io ra nd random-string) #:use-module (crates-io))

(define-public crate-random-string-0.1.1 (c (n "random-string") (v "0.1.1") (d (list (d (n "random-number") (r "^0.1.3") (d #t) (k 0)))) (h "1mrdgv2q0rjv9r47pp8g5kgxaizvr44lq2mxw7d8j21rin02xv57")))

(define-public crate-random-string-0.1.2 (c (n "random-string") (v "0.1.2") (d (list (d (n "random-number") (r "^0.1.3") (d #t) (k 0)))) (h "1s4gmq1ns0v5bfhl49qlg3ijbw8xx05f2smp6alixk4n5nhr985q")))

(define-public crate-random-string-0.2.0 (c (n "random-string") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.4.0") (d #t) (k 0)))) (h "0541dbanfqaqdnbm4gpw0zvv5g5jkns0xws4mhm4pzldxxc5lq1i")))

(define-public crate-random-string-1.0.0 (c (n "random-string") (v "1.0.0") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "05q8lswfc6hx55z1yvwbqwf9k1xvwdpz0862z652saf53q8n6kng")))

(define-public crate-random-string-1.0.1 (c (n "random-string") (v "1.0.1") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "0f4clzfp3k3hxlx4myd8ayzjwz87qq9n9ff33xaj2q2r5scagbg3") (f (quote (("default" "charsets") ("charsets"))))))

(define-public crate-random-string-1.1.0 (c (n "random-string") (v "1.1.0") (d (list (d (n "fastrand") (r "^1.4") (d #t) (k 0)))) (h "1by4lfz0b8xb8whzl22wi6k9xp6617ac9d8v70bkzbi460yd23zp") (f (quote (("default" "charsets") ("charsets"))))))

