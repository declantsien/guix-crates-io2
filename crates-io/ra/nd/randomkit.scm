(define-module (crates-io ra nd randomkit) #:use-module (crates-io))

(define-public crate-randomkit-0.1.0 (c (n "randomkit") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1b64vmh399pljrnw8arrrkaj9g2ip3ngr0rg126gz13gna50zx2l")))

(define-public crate-randomkit-0.1.1 (c (n "randomkit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p61i4b9pgbv8kkkkdn5n3fmrgb6v5020amz0vqmkzwf7bhr9017")))

