(define-module (crates-io ra nd random_choice) #:use-module (crates-io))

(define-public crate-random_choice-0.1.0 (c (n "random_choice") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ywyffi0visy0ph46nn4dpwkbjkws86i64zk3cl8fcdsr5f8dwg1")))

(define-public crate-random_choice-0.1.1 (c (n "random_choice") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0pv1glr2sl5v13a5714b39yqly91hr78zly80m2yvbrgb4qk5cyh")))

(define-public crate-random_choice-0.2.0 (c (n "random_choice") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0n6h7n9dhkxqvilh5dgmm0is4kyjskfkf8xfkj56kapzs0h1dr3d")))

(define-public crate-random_choice-0.3.0 (c (n "random_choice") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01wh3q7lnlmldm62bymia4365ksfaj4xjz7fdicd51jpkd3r5lyx")))

(define-public crate-random_choice-0.3.1 (c (n "random_choice") (v "0.3.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1nmjmj12hgh7qbfzl7aksc1189hviia1airmd8mhkc5bsj0c5f25")))

(define-public crate-random_choice-0.3.2 (c (n "random_choice") (v "0.3.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gvxqahf73pnfj175471pdnrqixh22ldzjc4dib847csw0zx5j09")))

