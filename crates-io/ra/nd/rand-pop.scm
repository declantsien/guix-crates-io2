(define-module (crates-io ra nd rand-pop) #:use-module (crates-io))

(define-public crate-rand-pop-0.1.0 (c (n "rand-pop") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1w80i58glbjw70k5sdix57rr702w04ww2zla120an33jyrl4hhyz")))

(define-public crate-rand-pop-0.1.1 (c (n "rand-pop") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1rm581bfnh34hvxkcf8csjh0m0cx427761zisywy59v2v0k6k50m")))

