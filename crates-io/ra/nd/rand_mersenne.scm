(define-module (crates-io ra nd rand_mersenne) #:use-module (crates-io))

(define-public crate-rand_mersenne-0.0.1 (c (n "rand_mersenne") (v "0.0.1") (h "0md2qz77zqdfggqz4rbjy971ydpjd79apj9ly6zli831pgaljh0s") (y #t)))

(define-public crate-rand_mersenne-0.0.2 (c (n "rand_mersenne") (v "0.0.2") (h "14nvma92prcjpwbxrc93p7mpy3dwp1709q9yjsnbrg9gb27n6adl") (y #t)))

(define-public crate-rand_mersenne-0.0.3 (c (n "rand_mersenne") (v "0.0.3") (h "1qjbxx3ivb5ch5pgcscl8bj9wxxhd8rz820rlcxllxinkvz64zxm") (y #t)))

(define-public crate-rand_mersenne-0.0.4 (c (n "rand_mersenne") (v "0.0.4") (h "1i7b3kilfx6hm8ndkwfcjyp6vkpljznkdr4hi89nh511l63gi0q6") (y #t)))

(define-public crate-rand_mersenne-0.0.5 (c (n "rand_mersenne") (v "0.0.5") (h "0d8x551bdzzna3928fs32gzz3h9jzzky6359h3fhkqnlv6pwfvs8") (y #t)))

(define-public crate-rand_mersenne-0.0.6 (c (n "rand_mersenne") (v "0.0.6") (h "16r9hmz8lh38zbk57bh4z83a28iv9pyd01f1iv8my65z1wpzjvay") (y #t)))

(define-public crate-rand_mersenne-0.0.7 (c (n "rand_mersenne") (v "0.0.7") (h "1sk7vsvamgs51mxz64v6175z6d8sqcvzrjpfhdm7qnnrzqm06my0") (y #t)))

(define-public crate-rand_mersenne-0.0.8 (c (n "rand_mersenne") (v "0.0.8") (h "1b033b3pa2hdkdsayhw9g795bmlmb21ydj0iw9j6y7hm1qzzhn8f") (y #t)))

(define-public crate-rand_mersenne-0.0.9 (c (n "rand_mersenne") (v "0.0.9") (h "0jjiqq4vri1jcwabawkdfa2vrn72xz86i0daymc39hy2s04va8r4") (y #t)))

(define-public crate-rand_mersenne-0.0.10 (c (n "rand_mersenne") (v "0.0.10") (h "1qqcw63nvbw123znjknr6p9j58xpncn8zrczk79hg3nm7y5lv3gq") (y #t)))

