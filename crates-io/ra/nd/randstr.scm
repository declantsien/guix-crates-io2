(define-module (crates-io ra nd randstr) #:use-module (crates-io))

(define-public crate-randstr-0.1.0 (c (n "randstr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "108y05qzv8l19zbfln2vpg7xwyql3h68qdjdrqgcfhp2xhd7pb79")))

(define-public crate-randstr-0.2.0 (c (n "randstr") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14dr89mka8zzvq0v0kiqw0agzfzkqjljv7h96cdp4k3wjkahd7vy")))

(define-public crate-randstr-0.2.1 (c (n "randstr") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "135zdv1d9llhqxs0acgpfk96r2wyw6x13d1hpxyrkcpnzzzrlq06")))

