(define-module (crates-io ra nd random-output) #:use-module (crates-io))

(define-public crate-random-output-0.2.0 (c (n "random-output") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "0cphax4js237gqbmq5zpwqi2qhc3wa474dcnq98x0dxcnqmrh6r2")))

(define-public crate-random-output-0.3.0 (c (n "random-output") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)))) (h "075x6yyhnm7j915v61q6wgyzfl37gd0fq8r8cjza59cc8i559a2v")))

