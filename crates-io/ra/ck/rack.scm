(define-module (crates-io ra ck rack) #:use-module (crates-io))

(define-public crate-rack-0.0.0 (c (n "rack") (v "0.0.0") (h "09msv4lbqp6g39iqsxpnpsz3b50gyfvayqy4n5drvg3fjiyxg3md")))

(define-public crate-rack-0.0.1 (c (n "rack") (v "0.0.1") (h "1h7kcjf7yfarci009g2vhb9bk4z1slfxswj3350c0v4yd5h10gyp")))

