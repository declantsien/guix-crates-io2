(define-module (crates-io ra #{2e}# ra2e2) #:use-module (crates-io))

(define-public crate-ra2e2-0.2.0 (c (n "ra2e2") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1ckwbh9zj0bkmqrxgq6xyhigxjizp4fzbr32j2r4gwwpb4114r9i") (f (quote (("rt" "cortex-m-rt/device"))))))

