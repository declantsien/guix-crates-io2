(define-module (crates-io ra #{2e}# ra2e1) #:use-module (crates-io))

(define-public crate-ra2e1-0.0.1 (c (n "ra2e1") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1v2iv4vp01nfljr7khdqfcmh6wqk4b5vjis3vkchpwxpcasgvbc8") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ra2e1-0.2.0 (c (n "ra2e1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "17mp2iwn1s9lgjj6mrgxl8d6ck3gyq69n0sp7564gb6h9kqw0px2") (f (quote (("rt" "cortex-m-rt/device"))))))

