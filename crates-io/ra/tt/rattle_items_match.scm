(define-module (crates-io ra tt rattle_items_match) #:use-module (crates-io))

(define-public crate-rattle_items_match-0.1.0 (c (n "rattle_items_match") (v "0.1.0") (d (list (d (n "look_ahead_items") (r "^0.1.1") (d #t) (k 0)))) (h "1klxv98wxjgzqg0m8jr9b98h459w2fp41np9xckcrc7pqgxhwxyf")))

(define-public crate-rattle_items_match-0.1.1 (c (n "rattle_items_match") (v "0.1.1") (h "18x39vydapfl7h31ylj1xvy20lhxhvsxsm3hhsdl56dhpcz1f8da")))

(define-public crate-rattle_items_match-0.1.2 (c (n "rattle_items_match") (v "0.1.2") (h "0ssf29adcpz816gy15f4fq4sksgh60pszxf2gxr1hbb915m7l73b")))

(define-public crate-rattle_items_match-0.1.3 (c (n "rattle_items_match") (v "0.1.3") (h "009rflqc6bq9rpsvidp03bp8qfxz5n3x79aln0r5ar0bv2j4byd8")))

(define-public crate-rattle_items_match-0.1.4 (c (n "rattle_items_match") (v "0.1.4") (h "1nxc37a8hzv2ww9nc9phj7s3kqgpf6xhsjab5za08wh6k5gznxly")))

(define-public crate-rattle_items_match-0.1.5 (c (n "rattle_items_match") (v "0.1.5") (h "0462gag734q7zh5pppndnx877iwqrjb1mfy94mhhrcdlvs4qp8n4")))

(define-public crate-rattle_items_match-0.1.6 (c (n "rattle_items_match") (v "0.1.6") (h "0riqjc9s5f6x0270ph9v982paar0kq004iq35348kjl577f372kz")))

(define-public crate-rattle_items_match-0.1.7 (c (n "rattle_items_match") (v "0.1.7") (h "0ql92jlxv5pf0h2f6sgn1n68jn2zj19dqqygshxw2sdgdk1013p3")))

(define-public crate-rattle_items_match-0.1.8 (c (n "rattle_items_match") (v "0.1.8") (h "16pfdsjl0hvnkzlcma0wha0x0nizis9n27xklx7pp5151zvm6cv6")))

(define-public crate-rattle_items_match-0.1.9 (c (n "rattle_items_match") (v "0.1.9") (h "1964c3nrqr4yr3df69bi12r9p9pk7aa22wijh1dh3fbcyfpjbdr4")))

(define-public crate-rattle_items_match-0.1.10 (c (n "rattle_items_match") (v "0.1.10") (h "1bhjfc6yq2blq8br3ww2spz9v69kap40pva2dld6qfgfqm566xxp")))

(define-public crate-rattle_items_match-0.1.11 (c (n "rattle_items_match") (v "0.1.11") (h "020kmryd0xiansxvbah3iy3v0xyy7jyd71ksmlsgpdaxalxj336n")))

(define-public crate-rattle_items_match-0.1.12 (c (n "rattle_items_match") (v "0.1.12") (h "1jg7h08dwf0fjck6rriiyc07p5vgvzszhzrphahwjxi1n4dra3nx")))

