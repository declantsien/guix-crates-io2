(define-module (crates-io ra tt rattish) #:use-module (crates-io))

(define-public crate-rattish-0.0.0 (c (n "rattish") (v "0.0.0") (h "0swmq3rd7g7kg3bhi0n8f02fydm00137kxgyv185vxbld0gp50mx") (f (quote (("std"))))))

(define-public crate-rattish-0.1.0 (c (n "rattish") (v "0.1.0") (h "0c4xprs160lfjvj4fkrk9brk2aj4qv3gw47isxc6jh5c2m5652dc") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-rattish-0.2.0 (c (n "rattish") (v "0.2.0") (h "19syqmwn63dkrj3q5swbnnpk7jhxwn8ladprc1781sd55k6n162d") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.2.1 (c (n "rattish") (v "0.2.1") (h "0js2hkwc5rhdjwayiwzrqv9nh2hf072gb445xw39b384wxkd8apr") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.2.2 (c (n "rattish") (v "0.2.2") (h "0v1h3s0fsxrsmp6ss20g7fabdb27zwv1s3zpk8zy0wirzi47mdh3") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.2.3 (c (n "rattish") (v "0.2.3") (h "0slbm9v5samfi113nk6nvykssqj98d6hjzi7b4hi0935wdygb8hy") (f (quote (("std" "alloc") ("static" "std") ("alloc")))) (y #t)))

(define-public crate-rattish-0.3.0 (c (n "rattish") (v "0.3.0") (h "1gvd7c4cxqskywhxd2b5sixjvn9w1f4qk19jknjzp8xg1vi0587r") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.3.1 (c (n "rattish") (v "0.3.1") (h "1r5hk9lsgm6hwwj9d1v3s1g3w0yciaj65dvgy9h2q81p3biq5hrp") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.3.2 (c (n "rattish") (v "0.3.2") (h "0igkfljni3fn32cc764hq21b8jqjkl3qjmfjc6zp4kbn4103l7kd") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.3.3 (c (n "rattish") (v "0.3.3") (h "05clysiwfi9xqhl52pgrnmfda55xzlh39ibi6lsmirksw7j055vx") (f (quote (("std" "alloc") ("static" "std") ("alloc")))) (y #t)))

(define-public crate-rattish-0.4.0 (c (n "rattish") (v "0.4.0") (h "05xx2652pb6y5i6hdf30zxgymp6pbpn0gh4vz44wrnvnx481nidy") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.4.1 (c (n "rattish") (v "0.4.1") (h "1qvx8rv1w2jgwpg7bd6aar5v8g0zix2y1w8zs1i8744qv1s8880i") (f (quote (("std" "alloc") ("static" "std") ("alloc"))))))

(define-public crate-rattish-0.5.0 (c (n "rattish") (v "0.5.0") (h "00niwmcxsnm11i6smkl483argiii80zwfzkpam5gx6cdlc3655r6") (f (quote (("std" "alloc") ("global" "std") ("alloc"))))))

(define-public crate-rattish-0.6.0 (c (n "rattish") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0.29") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.28") (o #t) (d #t) (k 0)))) (h "05i3qwj70apasy3mhn0l8hh6npi7lasja4s7jjlc3yzz3nd91djc") (f (quote (("trace" "tracing") ("std" "alloc" "thiserror") ("global" "std") ("alloc"))))))

