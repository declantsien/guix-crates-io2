(define-module (crates-io ra tt rattler_macros) #:use-module (crates-io))

(define-public crate-rattler_macros-0.2.0 (c (n "rattler_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hf9rghz6xygqdr7mybghsay3n8mp3ndxjqmbhl39p61488nb1n9")))

(define-public crate-rattler_macros-0.3.0 (c (n "rattler_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pjgjg6iiyy4nbjm0cazv89hdpn67qqg39aarl5fkaiknjhlxixy")))

(define-public crate-rattler_macros-0.4.0 (c (n "rattler_macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "12rih07cg6sbakkbadc75r43bnws5ky1dxh6app87vpyn2fcdd7c")))

(define-public crate-rattler_macros-0.5.0 (c (n "rattler_macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lbr8pdz7l8q426iyr1l6jw7s0wvmkf2p0j33l9a1qip0krms00v")))

(define-public crate-rattler_macros-0.6.0 (c (n "rattler_macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "0yaixs6awiz27rsg7zaz0pam75r1c65afsrryzff2zj2yj5w2gx4")))

(define-public crate-rattler_macros-0.7.0 (c (n "rattler_macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0kmqv9smijlhxpw3n80b1hkbbwh65jbk8i8vmlzpgrxbrwqm3p9m")))

(define-public crate-rattler_macros-0.8.0 (c (n "rattler_macros") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "10y90dnan64zpp8gjqk500q2svqnixbngl6islyd9nkb9zhrv7yz")))

(define-public crate-rattler_macros-0.9.0 (c (n "rattler_macros") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.83") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fg8zqvnc7m80zh2c7yckqbvf45vgsj65jxfygyimgs5fs6n3kc5")))

(define-public crate-rattler_macros-0.10.0 (c (n "rattler_macros") (v "0.10.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.83") (f (quote ("diff"))) (d #t) (k 2)))) (h "15w84iy5wnn09br670cyb8ghlwg8f2z9pm7lgnv3pjyp9ixxd5il")))

(define-public crate-rattler_macros-0.11.0 (c (n "rattler_macros") (v "0.11.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0kyswdd0ns7qqf25jbl2gv2h2wi742c06na1dmm4q3prx749rq01")))

(define-public crate-rattler_macros-0.12.0 (c (n "rattler_macros") (v "0.12.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hsxn1q6bn05p51mhxwqqani1r1yy2gylrqh5ay84p74rlr82i14")))

(define-public crate-rattler_macros-0.12.1 (c (n "rattler_macros") (v "0.12.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "18npbd7chxzwa07vwpsg62jbha50y1f2gjdn5iypl33x2wg43xw8")))

(define-public crate-rattler_macros-0.12.2 (c (n "rattler_macros") (v "0.12.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "04av4a638wr3588gylwcgiyryxh1dimisj08pf0ya5pcyfv6vs5v")))

(define-public crate-rattler_macros-0.12.3 (c (n "rattler_macros") (v "0.12.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "12nbfydxqg8z1m20i8radh47j4ifvnvz2mky85m7wjphxrcrd2qk")))

(define-public crate-rattler_macros-0.13.0 (c (n "rattler_macros") (v "0.13.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1x15912ssy245jp3gyihiyrag5p3b93c1l641kqa8b4h178qkwzs")))

(define-public crate-rattler_macros-0.14.0 (c (n "rattler_macros") (v "0.14.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0m8nbb0grdy0hgf5d6javrpchx9g1w2x132apsgw1zz0bhmdq8f8")))

(define-public crate-rattler_macros-0.15.0 (c (n "rattler_macros") (v "0.15.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1k730gy252ryn7gqdc2xc3yzbbqykm3xja5zspa0lbpkdp7fjb4a")))

(define-public crate-rattler_macros-0.16.0 (c (n "rattler_macros") (v "0.16.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "0y8g5870lx05v0kz6gcs4zbs3vj7fxk9nqqqkab56rmkjp8scrbd")))

(define-public crate-rattler_macros-0.16.1 (c (n "rattler_macros") (v "0.16.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "01b554i0j6a5qqr2wl931c987c62vvyr9d3ksdcb11nxalsgc0jg")))

(define-public crate-rattler_macros-0.16.2 (c (n "rattler_macros") (v "0.16.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "08vm520495rxym87mmd2qc6964sq6xfm5fk5zmv1bcr4g1gbf7f7")))

(define-public crate-rattler_macros-0.17.0 (c (n "rattler_macros") (v "0.17.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gyrdg1f8vkayzhm3vxj554a1rh000yybrqm0b2mifp900ccfv10")))

(define-public crate-rattler_macros-0.18.0 (c (n "rattler_macros") (v "0.18.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0cyiirxri5d122si7qn5474dzm56fajqzq64n19vcc5g002yk5ka")))

(define-public crate-rattler_macros-0.19.0 (c (n "rattler_macros") (v "0.19.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0akv2y3c92h7r4xvd7hqx79iilavzid610viz2y6v281mhnbbchi")))

(define-public crate-rattler_macros-0.19.1 (c (n "rattler_macros") (v "0.19.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "13q44znlipgsrjlaa8qabm773rmwc76npiwwgysb1677azyglca5")))

(define-public crate-rattler_macros-0.19.2 (c (n "rattler_macros") (v "0.19.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vcdxjh5y9r2nw5ds2hv9zi0l6f7mg43g5dy845sb9jgvnpc7xn7")))

(define-public crate-rattler_macros-0.19.3 (c (n "rattler_macros") (v "0.19.3") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.91") (f (quote ("diff"))) (d #t) (k 2)))) (h "10r4b8hrj3rgchbxaabzp6qhwfv6m3358msf5506isjnhc7g5khh")))

