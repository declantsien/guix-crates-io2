(define-module (crates-io ra tt rattler_libsolv_rs) #:use-module (crates-io))

(define-public crate-rattler_libsolv_rs-0.6.0 (c (n "rattler_libsolv_rs") (v "0.6.0") (d (list (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rattler_conda_types") (r "^0.6.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "03f5g8838bsy97vxj5w52igmll143gmh5y0fzh4x8hqji5v51r2j")))

(define-public crate-rattler_libsolv_rs-0.7.0 (c (n "rattler_libsolv_rs") (v "0.7.0") (d (list (d (n "insta") (r "^1.30.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rattler_conda_types") (r "^0.7.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0bh6y3k0h3q525dmw4cza36fk8jai4zjc325p09x0d06xmi806qh")))

(define-public crate-rattler_libsolv_rs-0.8.0 (c (n "rattler_libsolv_rs") (v "0.8.0") (d (list (d (n "insta") (r "^1.30.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "rattler_conda_types") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0w2fmydvn0xxr30chk6ljx7fdy7vr42kkhfjjsgx9n5wbbx1lp20")))

(define-public crate-rattler_libsolv_rs-0.9.0 (c (n "rattler_libsolv_rs") (v "0.9.0") (d (list (d (n "elsa") (r "^1.9.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 2)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.4") (d #t) (k 2)))) (h "175am8azk1g7v5hrcmzagcmr94yq6hlfrf5bx3n28w2q5p32lh66")))

