(define-module (crates-io ra nk ranked_voting) #:use-module (crates-io))

(define-public crate-ranked_voting-0.3.0 (c (n "ranked_voting") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sha256") (r "^1.0") (d #t) (k 0)))) (h "0ixgadjsazmfj7cimxh3167h4qrygiq7nx9mjjkih2gly61vl0ww")))

