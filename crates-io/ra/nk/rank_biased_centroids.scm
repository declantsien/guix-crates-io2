(define-module (crates-io ra nk rank_biased_centroids) #:use-module (crates-io))

(define-public crate-rank_biased_centroids-0.1.0 (c (n "rank_biased_centroids") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06wg8x02igd8b027v94xpgwsmfbinfh23460pah1hjg6jx15jnwp")))

(define-public crate-rank_biased_centroids-0.2.0 (c (n "rank_biased_centroids") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1gn8l7j69gcf3m9gillanxlab4rvwk9pag4brkvisdyynk4d9snd")))

(define-public crate-rank_biased_centroids-0.2.2 (c (n "rank_biased_centroids") (v "0.2.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m0vcg2ippsril0jrfpdgv458jsciav7dv7azqnckkwsjb5wsjim")))

(define-public crate-rank_biased_centroids-0.3.1 (c (n "rank_biased_centroids") (v "0.3.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c4bnxvyi75rd19417zrgma7dh5s7vhllwz8i9mh7a0c3ah64rkg")))

