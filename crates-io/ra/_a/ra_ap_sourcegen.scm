(define-module (crates-io ra _a ra_ap_sourcegen) #:use-module (crates-io))

(define-public crate-ra_ap_sourcegen-0.0.63 (c (n "ra_ap_sourcegen") (v "0.0.63") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0y8rfp4fjyrrhlg5zcnx5ncc2gsqmdi9qim05n4fqbsvjylk68f4")))

(define-public crate-ra_ap_sourcegen-0.0.64 (c (n "ra_ap_sourcegen") (v "0.0.64") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0pwl0ahbklfz8j9q4xpvncg0pdi2zyfc4zf827hm7fmaxp40xmc0")))

(define-public crate-ra_ap_sourcegen-0.0.65 (c (n "ra_ap_sourcegen") (v "0.0.65") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0vlmwqydxl8ncqaf1wh4rl8cp9fazjswn49xg60dngzjw4kr2a2r")))

(define-public crate-ra_ap_sourcegen-0.0.66 (c (n "ra_ap_sourcegen") (v "0.0.66") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "14r60b78r2rj361i7q380w56scdrkyc77yhj2aihxb4dkdk7l6yj")))

(define-public crate-ra_ap_sourcegen-0.0.67 (c (n "ra_ap_sourcegen") (v "0.0.67") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0mijm6rsgrd887nnw7gw57lkgk1vdcqp7jxmaj2rrcffajlgm3vc")))

(define-public crate-ra_ap_sourcegen-0.0.68 (c (n "ra_ap_sourcegen") (v "0.0.68") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0ihy80bwj0ymgr8ghnpg0pbvmd8qnf6sydaav3nbqd6iwir79071")))

(define-public crate-ra_ap_sourcegen-0.0.69 (c (n "ra_ap_sourcegen") (v "0.0.69") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1dl56mpg471s18qxd0bclzfxqrd6xla0yw2hgw66pyxl8l1liaw4")))

(define-public crate-ra_ap_sourcegen-0.0.70 (c (n "ra_ap_sourcegen") (v "0.0.70") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1m9f8ks76mmp4i4wnfcjhh2wi8gf163l2hlc3lim2zhzka0mbcnm")))

(define-public crate-ra_ap_sourcegen-0.0.71 (c (n "ra_ap_sourcegen") (v "0.0.71") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "16yjj19vxkrcaig4qgp1kxxn3xypwyzrvypaizzx6i9pwzwlpw5f")))

(define-public crate-ra_ap_sourcegen-0.0.72 (c (n "ra_ap_sourcegen") (v "0.0.72") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0rwpn00ia4751yibdkrnnrzyf6zbg6z2n8cxfn50vl7q8jl1x8nk")))

(define-public crate-ra_ap_sourcegen-0.0.73 (c (n "ra_ap_sourcegen") (v "0.0.73") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1q27y26pv37dwjygckl1inc76b6ixlfsf12wi2amkgdi25r4ipd3")))

(define-public crate-ra_ap_sourcegen-0.0.74 (c (n "ra_ap_sourcegen") (v "0.0.74") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "11mf6fvk30hwp7xl1jfjzy138s6g762hmhcyd9b0z75qpgyycsd4")))

(define-public crate-ra_ap_sourcegen-0.0.75 (c (n "ra_ap_sourcegen") (v "0.0.75") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0dxxsva6wwjxzhi2px8bbfbxp8sgnqq4l63b7jlysb4lcs9f258c")))

(define-public crate-ra_ap_sourcegen-0.0.76 (c (n "ra_ap_sourcegen") (v "0.0.76") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1f18jrnv1h1r2f358g7l15rlc6yw4ddzpm3pjk2cmq3wkwsa2mz4")))

(define-public crate-ra_ap_sourcegen-0.0.77 (c (n "ra_ap_sourcegen") (v "0.0.77") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "04rnhxj70ljq0qb3sdc57k6dd74s8i9ca6lr88r9da8g39687mx5")))

(define-public crate-ra_ap_sourcegen-0.0.78 (c (n "ra_ap_sourcegen") (v "0.0.78") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1c15w47a8y3h2ncgva26gvi5rbn7pmrnl7yc4x8xsry7pikx1qzh")))

(define-public crate-ra_ap_sourcegen-0.0.79 (c (n "ra_ap_sourcegen") (v "0.0.79") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0q6b4yqwn4hg7943j890lzn24imcmiw89pq1c3mh0s87ivzbiwbl") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.80 (c (n "ra_ap_sourcegen") (v "0.0.80") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1qrbrnjjiapp0aiwjwzv962d3vx8p0c8igdzizgmcans9zdrgv83") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.81 (c (n "ra_ap_sourcegen") (v "0.0.81") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1q4yg2pq7c4g7pciq5r2j4mi91917yllygzgh4rlk7mwb72w67ic") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.82 (c (n "ra_ap_sourcegen") (v "0.0.82") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "05ag1d7lacjh3i5kkx5d6iazy7m0prbxwl8q6lb712i9rxw6p0kc") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.83 (c (n "ra_ap_sourcegen") (v "0.0.83") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1bvbq13bxn07yr0i99cxdbz9yvhiwdn99d2w736pzv14a6mxrg3q") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.84 (c (n "ra_ap_sourcegen") (v "0.0.84") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0ckyd7rn32lyi99madq14mq5kk7ljqg2qnsxfm2454yss9g67gax") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.85 (c (n "ra_ap_sourcegen") (v "0.0.85") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0iiilb7lg3si0g54k2ccjiak6c57lxqv4zdgb7v5rclayb15zgb6") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.86 (c (n "ra_ap_sourcegen") (v "0.0.86") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1szyppr1wwyqy9h2vij2snicrlkd79hjnq4q5z6ik7b7w1raf8c3") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.87 (c (n "ra_ap_sourcegen") (v "0.0.87") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "13a4p629hp8lanajzkw1py44akrqpfbm2d2bs39plzmfjqg1v2rr") (r "1.56")))

(define-public crate-ra_ap_sourcegen-0.0.89 (c (n "ra_ap_sourcegen") (v "0.0.89") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "10n5xkcjl812mv4fhx2dnhsm7nhrv1rfz4sv2f4aq5c1l9psp0g0") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.90 (c (n "ra_ap_sourcegen") (v "0.0.90") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "07j4a31nj85wrm5g0zwynbgzi2rskpqbd78g5d78pqr5ng89lqv6") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.91 (c (n "ra_ap_sourcegen") (v "0.0.91") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1hyx7cb4vlm04z2nxsh62x3rsd0az0va74sd83768v0g1698xwvi") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.92 (c (n "ra_ap_sourcegen") (v "0.0.92") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0g6ddnv55axw2hl1rd1mac2668459vg3v5q0pjz9skvvz55lfz9m") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.93 (c (n "ra_ap_sourcegen") (v "0.0.93") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "1h0b06i5wi4lgh64nyzb7p88x4qvsw4bf2r0jg2pbja6m42h4jxv") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.94 (c (n "ra_ap_sourcegen") (v "0.0.94") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0ws53w63gbs9vs09hsfmja4s7f0fv57lzwgszj7fvfp1cf5nhbld") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.95 (c (n "ra_ap_sourcegen") (v "0.0.95") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0qkhlbzdsyk75g9wnmibz4wgmpa1b4k1mxzcyldsfidddijhxi8f") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.96 (c (n "ra_ap_sourcegen") (v "0.0.96") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "04dws0xxa19cqiklfli1m434fd43sxsnirxzy9636bydh0vi7q7s") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.97 (c (n "ra_ap_sourcegen") (v "0.0.97") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "0i2nlbkq1zrnxwk9xpnmz1y5f2pq7gz8g36kf159fh6i2dva07qy") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.98 (c (n "ra_ap_sourcegen") (v "0.0.98") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "04sqn0jl5121jf8sv8zq6xkz09l6vrmqwqiar0ykjshqnyhdkb60") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.99 (c (n "ra_ap_sourcegen") (v "0.0.99") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "00vp1qycyggz99h6s1kaxb86bq5jakd9xaah57lb7cgrzsbk5s67") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.100 (c (n "ra_ap_sourcegen") (v "0.0.100") (d (list (d (n "xshell") (r "^0.1") (d #t) (k 0)))) (h "01ah4bmnnvy7h1gfb2kh6cj41qiwa2dcp2vb44yzw0r4n0z1ggbw") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.101 (c (n "ra_ap_sourcegen") (v "0.0.101") (d (list (d (n "xshell") (r "^0.2.1") (d #t) (k 0)))) (h "0ac3c4p147vcla8xwdshxc6fw96hcpwdi241vsc1j5jvwrd0ghng") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.102 (c (n "ra_ap_sourcegen") (v "0.0.102") (d (list (d (n "xshell") (r "^0.2.1") (d #t) (k 0)))) (h "1j40srn4sr19spnvlmlrph9bn4x01mhxwvwsbgz2vs5j0r7zcn42") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.103 (c (n "ra_ap_sourcegen") (v "0.0.103") (d (list (d (n "xshell") (r "^0.2.1") (d #t) (k 0)))) (h "08z9yabsg2sb7vzvylwncbwbrn34b1zvm6hjrxkra1x5nnjhyfyw") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.104 (c (n "ra_ap_sourcegen") (v "0.0.104") (d (list (d (n "xshell") (r "^0.2.1") (d #t) (k 0)))) (h "1a08j2jrnmmxnil2nzp77m0403qpdaghcq14cfcz1cscvk4fasrr") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.120 (c (n "ra_ap_sourcegen") (v "0.0.120") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0x7cg0wvi6sdcsk5hy2yp4xc7fmkp9dgqbhlp3fb8rjia663pnal") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.121 (c (n "ra_ap_sourcegen") (v "0.0.121") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "19dnqjx1ki1kllsxxwc34i8ii0qpcllcp01vfz9hxy6mxs9w25aq") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.122 (c (n "ra_ap_sourcegen") (v "0.0.122") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1ag40i5hynjf7nvsj69n1kr02lx2pliaj2cccjzjjfzhh8xll7yp") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.132 (c (n "ra_ap_sourcegen") (v "0.0.132") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0mkrbxi99lqik06irbwj331zn5bajscrcmbr1k4k0fzi5m7swn1n") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.133 (c (n "ra_ap_sourcegen") (v "0.0.133") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "02vsymrgd5q79gjw5yzqmxmnvvsckdvmm7r8flzrxsy49pm3ipip") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.134 (c (n "ra_ap_sourcegen") (v "0.0.134") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1saysn2wl367sc9jxrnqv0pylgdjaq57gsp6cm822gdn981ipab8") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.137 (c (n "ra_ap_sourcegen") (v "0.0.137") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0ad6m4vm6fbmhbw3gdljd2axkw1cymwhl489z54xajk3mycpw2i6") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.138 (c (n "ra_ap_sourcegen") (v "0.0.138") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "15msf1r4iv1ykrbm8myachxq8kp2z4k4nn3ymsjmymc1gc7gbkk6") (r "1.57")))

(define-public crate-ra_ap_sourcegen-0.0.139 (c (n "ra_ap_sourcegen") (v "0.0.139") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1h8cdfi65yvg2vf6ajdy76hhbygy264hsw9bi94xc6d8nk33wjwr") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.140 (c (n "ra_ap_sourcegen") (v "0.0.140") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "089nvlqr33lnrphc6n3vr97yrg0dkdriqzz165n09jph7irpjikr") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.141 (c (n "ra_ap_sourcegen") (v "0.0.141") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0wccqx0d0cq0mlyfaq566gs2vb9l3y2lv5vr5vygksdzqxwjw7q2") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.142 (c (n "ra_ap_sourcegen") (v "0.0.142") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1jyr0ljb1i31nrsy41ll18jmd0y0a8nk8k5p5777kll6qs0gmnln") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.143 (c (n "ra_ap_sourcegen") (v "0.0.143") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1kqkr0yk9gdsdl1d5amcqz5jg76bbk5ix0bwrll5cfxgd70sw6k3") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.144 (c (n "ra_ap_sourcegen") (v "0.0.144") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0llpas8dvs6gg0c228lv82n05hd0kf3w2gs1mbk89g4bhgbcrccs") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.145 (c (n "ra_ap_sourcegen") (v "0.0.145") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "18i7hfgvfdbilkarirl9rjlbrizcvwjyb2rdsx6wh6p96f76nkj3") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.146 (c (n "ra_ap_sourcegen") (v "0.0.146") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1d2zyxqazwwhr52gag86d77gshllfbr9q5zmpfs4il03sbbbgs7s") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.148 (c (n "ra_ap_sourcegen") (v "0.0.148") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1k0sbcgm293glv5fa0zyrgdhaj7nnak09plmp3j3waihjx0iadqf") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.149 (c (n "ra_ap_sourcegen") (v "0.0.149") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0h8gs5f4dqwyx7sjpnh3h0wfqmf6a2i9marggx4kkml9x6snkd0j") (r "1.65")))

(define-public crate-ra_ap_sourcegen-0.0.159 (c (n "ra_ap_sourcegen") (v "0.0.159") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1a3ijfs1ncascjkfskki30x2a7clmlw94r3rc4yga6xd8m98vwy0") (r "1.66")))

(define-public crate-ra_ap_sourcegen-0.0.160 (c (n "ra_ap_sourcegen") (v "0.0.160") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0ray0dgl3jjzxq0vbh1i5ff52fywisprj4mvi3gpdh6bkidvg8d2") (r "1.66")))

(define-public crate-ra_ap_sourcegen-0.0.163 (c (n "ra_ap_sourcegen") (v "0.0.163") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "096qqcdspsh3rxsjk6q78vjhh033jzqd3z3kjqbr55a38rwcrlk7") (r "1.66")))

(define-public crate-ra_ap_sourcegen-0.0.164 (c (n "ra_ap_sourcegen") (v "0.0.164") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "018r8wirn9a23bww4fzhgykrpjjnm14js6dz1848q7h1raqa2fh3") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.165 (c (n "ra_ap_sourcegen") (v "0.0.165") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "05nap5xj2wffnj4mmxffifsd2pdmqr3yqbd8x70s76pj4qw61mcp") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.166 (c (n "ra_ap_sourcegen") (v "0.0.166") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0craynayzi8c7ra518pbwjlhz1bcsn3q51g3spm6zj5gmq4cmv9j") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.167 (c (n "ra_ap_sourcegen") (v "0.0.167") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "12ac6mg4xm81i9l1b83x0lyjah37m2fik6dcdx5l3zasdsx5lkza") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.168 (c (n "ra_ap_sourcegen") (v "0.0.168") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "17kyx61csyw2jkjg6sx5i3j047bfckrn79yk217gz4k4ick90vsl") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.169 (c (n "ra_ap_sourcegen") (v "0.0.169") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "18iyz6p09cl8f5zw91g61h27yflzbc5swa9qi9spmf8w2qp2c4ps") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.171 (c (n "ra_ap_sourcegen") (v "0.0.171") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "1l0d2gpi6d5jh9l9lryk7brywmjrfn9q3lchcybjbr393vdvwnqc") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.172 (c (n "ra_ap_sourcegen") (v "0.0.172") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0mb32gch6y9r5av57cgsy22kkz9k46idypigslwnbvkqsbb0rba1") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.173 (c (n "ra_ap_sourcegen") (v "0.0.173") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0nscxa86ivadlqvgafwkifig658m6191hjv3vmqi3xv3ra7bm0mh") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.174 (c (n "ra_ap_sourcegen") (v "0.0.174") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0gdmz10dly23c6zmfrvpaqwhr87alqpza9c60l1fy2ag8qy8hkgr") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.177 (c (n "ra_ap_sourcegen") (v "0.0.177") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0c9npph5zbq2abld78nr9vvmvsfwrcj3yl2xbc5jc1aar8aibsvg") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.178 (c (n "ra_ap_sourcegen") (v "0.0.178") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0nndf8v8579xsdj6rgxyjl9b2fxja6rszrrmdbdkmbbbhkbh9zln") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.181 (c (n "ra_ap_sourcegen") (v "0.0.181") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "02l71clrxykh95zdrrann5i3945zhdidmkmras8mvik1kly5498z") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.182 (c (n "ra_ap_sourcegen") (v "0.0.182") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0ipx4hxr6d30lhhgi3rmz1a761myn3p9sbh4pzavw1kaw6fp9bvl") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.183 (c (n "ra_ap_sourcegen") (v "0.0.183") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "0x0z52pwgxwcz6jhcfbh0z469cwkw0brcs7cnx0i5sm5bsqpijcl") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.184 (c (n "ra_ap_sourcegen") (v "0.0.184") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "133ggaz82lv76mzav664nc0iamq5r8j9wlwa1v0ij376lh3bzill") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.185 (c (n "ra_ap_sourcegen") (v "0.0.185") (d (list (d (n "xshell") (r "^0.2.2") (d #t) (k 0)))) (h "049ydgvfxfislcxc1jdkc51rc7wannappgimv79dj2c8gmkdm54z") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.186 (c (n "ra_ap_sourcegen") (v "0.0.186") (d (list (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "0vzlfcjhc148vrfwgxxkp0xq7sk7b1jh94k16q73yzwsb4s9b1a7") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.187 (c (n "ra_ap_sourcegen") (v "0.0.187") (d (list (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1g173hc9ccix24ca3hbkf5zhwygifxbhm3zys6vvzfy7s3kiipxx") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.188 (c (n "ra_ap_sourcegen") (v "0.0.188") (d (list (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1ppmdsddiz2bn1vzb27naj9fkympzgc6qinyhcc4gq6nfz9krcxf") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.189 (c (n "ra_ap_sourcegen") (v "0.0.189") (d (list (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "1bn3r55bi7vq5bfjxmgvs3cyfcc1wn2jxbprxi6c1lg1q7f9i6nc") (r "1.70")))

(define-public crate-ra_ap_sourcegen-0.0.190 (c (n "ra_ap_sourcegen") (v "0.0.190") (d (list (d (n "xshell") (r "^0.2.5") (d #t) (k 0)))) (h "146kh8p4i96hfvkf5qymgnx3zqd5inx0ng7lpwmnrrqn32ya7p67") (r "1.70")))

