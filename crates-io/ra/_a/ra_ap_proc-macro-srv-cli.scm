(define-module (crates-io ra _a ra_ap_proc-macro-srv-cli) #:use-module (crates-io))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.123 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.123") (d (list (d (n "proc-macro-srv") (r "=0.0.123") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "018j3qcil4cqrqlbyhxk9hz7r4gn5wpamr1ninln2nrgwk569mnr") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.124 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.124") (d (list (d (n "proc-macro-srv") (r "=0.0.124") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1nmc6qlj733dk3hak3rw965qh4ajjb8nraxplap8y255qzrq8dlj") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.125 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.125") (d (list (d (n "proc-macro-srv") (r "=0.0.125") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "09992iiaafwz0fs040rjxw8apkhz8w9qbh54szh8lr9q29fl5l76") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.126 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.126") (d (list (d (n "proc-macro-srv") (r "=0.0.126") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "13mgl688cd8rnyyv2i56cpq4jnfgl14c43v65q9k3857zv1mk8mr") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.127 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.127") (d (list (d (n "proc-macro-srv") (r "=0.0.127") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "12nqg8hk1x6zxrva8ms5nz0rvg9w2ck0wqd6p1ciqivdih0agin8") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.128 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.128") (d (list (d (n "proc-macro-srv") (r "=0.0.128") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1r7i5ibxk2n505xrgv7mhzai98jg3izsb0gns4q2vjnzzgxn82g0") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.129 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.129") (d (list (d (n "proc-macro-srv") (r "=0.0.129") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1iw14m7k20zg0b06amr67hci91hmkqm6n6g2l0ndzjh02pxfbfwp") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.131 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.131") (d (list (d (n "proc-macro-srv") (r "=0.0.131") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "046hdp35wm01kzyf8ph62p08slz906bqmlzpsnl2mwmy08x8sc5c") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.132 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.132") (d (list (d (n "proc-macro-srv") (r "=0.0.132") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0lm9h4yy2yqg20gcwr3xq5f2mdzdkmd4w8la7ifc1j1x774649a4") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.133 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.133") (d (list (d (n "proc-macro-srv") (r "=0.0.133") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0zk0pgb9maj8rf0pqmxb3lbvrlaq2gncfnl54g5mfqa5il97kamw") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.134 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.134") (d (list (d (n "proc-macro-srv") (r "=0.0.134") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0rhhwbj65gbq9x9yxsg2mq6cknsxgpbmk4fy280g67g4g8716ya0") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.136 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.136") (d (list (d (n "proc-macro-srv") (r "=0.0.136") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0cawlbr5mn06kxq2d23df7rcp9nmv8f0am2zd037vnm4sm2w2srr") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.137 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.137") (d (list (d (n "proc-macro-srv") (r "=0.0.137") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0hyhpk9hj5vn3rnqcq08drccjzz7cd6n4yrsm10svflyw191cnkb") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.138 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.138") (d (list (d (n "proc-macro-srv") (r "=0.0.138") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0p2mmmabw0r08670hic8rnvplqz0q8i33jf10x7z92vyz24isrsf") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.57")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.139 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.139") (d (list (d (n "proc-macro-srv") (r "=0.0.139") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1cbplv1gbm9vx0kvfk8qrf0kbr9g2w19z2jhdlv24kild30yr5if") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.140 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.140") (d (list (d (n "proc-macro-srv") (r "=0.0.140") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "175qci2lxb1862d2sq13aa67bmsv8jl45ykzbxi3f7mw55vriqc2") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.141 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.141") (d (list (d (n "proc-macro-srv") (r "=0.0.141") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "06ak6lfmjmhakvzbha6mfp80vf2p4pf2y08zq9rc1qn83h8mnhnk") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.142 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.142") (d (list (d (n "proc-macro-srv") (r "=0.0.142") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0p0zsbgayr4g2py8n0n5qji6vmqc4r170jqri0595g85iw4h4fhm") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.143 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.143") (d (list (d (n "proc-macro-srv") (r "=0.0.143") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "13vz165lfab0x0j13r1kf2n542fwhmdqs32bxrqbp6w1s24sa3q7") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.144 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.144") (d (list (d (n "proc-macro-srv") (r "=0.0.144") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0r4kfvk4aqscarcbhjj7jvzcfvvam2yjp5jc1wmli22qyz715nxa") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.145 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.145") (d (list (d (n "proc-macro-srv") (r "=0.0.145") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1faxdahjnplb643gcfsdzsvjigdcxv6rx1hbrdm77qdjg5aagrcx") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.146 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.146") (d (list (d (n "proc-macro-srv") (r "=0.0.146") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0cbbjlggv0dcsw62qb4kk2af1vk1lm891i3krrcshfc9vqkc2q8i") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.148 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.148") (d (list (d (n "proc-macro-srv") (r "=0.0.148") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1s9qym7gsab9d6vsagr0wainf1vhy1zw0xrwzazswhqjphl57njl") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.149 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.149") (d (list (d (n "proc-macro-srv") (r "=0.0.149") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "11h516grz7smlhn4wm7kvpl91wygi53bqv0ifwbmz22gkrna3yb5") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.65")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.159 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.159") (d (list (d (n "proc-macro-api") (r "=0.0.159") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.159") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "14sdi4a8nf3vmgmdq18c2wkjf9q51nb9ahsmchb4nwzd1gv0n9hr") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.66")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.160 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.160") (d (list (d (n "proc-macro-api") (r "=0.0.160") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.160") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1i80jhary437in2vzsz56arq9jbhg703xz6cjihnc443vfzdpgr1") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.66")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.163 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.163") (d (list (d (n "proc-macro-api") (r "=0.0.163") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.163") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "10zrllrv9z7magxlxsx4cv1jqhaj7ibdl3h4pzgaf279gni49b7l") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.66")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.164 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.164") (d (list (d (n "proc-macro-api") (r "=0.0.164") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.164") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "11mvyp452j73bghcn920r52fs4mf107s2ad0m7s2m7zny8q4d3d6") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.165 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.165") (d (list (d (n "proc-macro-api") (r "=0.0.165") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.165") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1nh33ji3p9q1z1m4v7zf6kqdjhd6rl9v2gdp22qh8ka3j5q41ig8") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.166 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.166") (d (list (d (n "proc-macro-api") (r "=0.0.166") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.166") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1lcmhz79ybp6qv5x7w8dcr5nijyj5qs9lxz389m6a0z1m85f0awn") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.167 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.167") (d (list (d (n "proc-macro-api") (r "=0.0.167") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.167") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "09434l3r5n6vvsr9r3aclbarnivhkqnn1l1yh560nhfvngyjw5xf") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.168 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.168") (d (list (d (n "proc-macro-api") (r "=0.0.168") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.168") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1n7fsszmybyqmykd1hmyapi8pl7hzwvs554b2nnzfx866pa70q2b") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.169 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.169") (d (list (d (n "proc-macro-api") (r "=0.0.169") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.169") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0f2h16pyhli7bj8p1wc06j9hwhqvw21a4ii6xp6i74rk9dpc8h61") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.171 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.171") (d (list (d (n "proc-macro-api") (r "=0.0.171") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.171") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0zgkssakn6ifgrgjp2v0jcgw4zv2rka2g815cf8bd41qgkk94pal") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.172 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.172") (d (list (d (n "proc-macro-api") (r "=0.0.172") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.172") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0nkvdbd8jpjipxsf0g0ly0ss9vdbxa32mcxgs0cilvhs37dfxb3c") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.173 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.173") (d (list (d (n "proc-macro-api") (r "=0.0.173") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.173") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0pr47lv26qdcwy4wjs5j7xxjf4d5wiwa3kggz5hmany9lamy7ik0") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.174 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.174") (d (list (d (n "proc-macro-api") (r "=0.0.174") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.174") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0cps5hdirkna8s4pqh5wxrsy1x60x3vw5x84jkj0ahlrv2h02n54") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.177 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.177") (d (list (d (n "proc-macro-api") (r "=0.0.177") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.177") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0glpw93i062k02lq4qkgh079461xklspfbpa7spnfmab5q7aqbwc") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.178 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.178") (d (list (d (n "proc-macro-api") (r "=0.0.178") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.178") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0nz77bk6ppirricrx8xk36nr6b7x0pf8qd3rb5kvd6qvdr8gyiri") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.181 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.181") (d (list (d (n "proc-macro-api") (r "=0.0.181") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.181") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0mg9pv2ihfhgf2qqxmqv80wy9li1byj68ypgfy6q89j52im1v9ws") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.182 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.182") (d (list (d (n "proc-macro-api") (r "=0.0.182") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.182") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0gazy7hq4544al53scsa3kcvbl5b1p57qnvrr3y24ysfqy6s8whv") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.183 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.183") (d (list (d (n "proc-macro-api") (r "=0.0.183") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.183") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1ic9hci9h8pkpizjpc2hdxpzl2a1hr2kl6z9q9w9l5kbdp8ih7jg") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.184 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.184") (d (list (d (n "proc-macro-api") (r "=0.0.184") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.184") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0gn5x1s8fc2ilqfix96hjiv916g4gaz49133b5ml78f754rgpz1f") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.185 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.185") (d (list (d (n "proc-macro-api") (r "=0.0.185") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.185") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0qrwar2xgys50qx0l09kzrswypc3869pmkgxpanzvp8mqb4h3r3f") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.186 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.186") (d (list (d (n "proc-macro-api") (r "=0.0.186") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.186") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0xj84aswlw1s00iqm9lw7slffnv0alkgi0r4ihzgbrrzljdgribv") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.187 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.187") (d (list (d (n "proc-macro-api") (r "=0.0.187") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.187") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1kip4bwdgijjjfsnk52cqnilpwjc2qyw42vwnpi4b8fdgxwc62sb") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.188 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.188") (d (list (d (n "proc-macro-api") (r "=0.0.188") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.188") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "01aycs0gs6m86wbc5h7wvlkpfwvf4c6a2jfyafki9fzb40fnnn3f") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.189 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.189") (d (list (d (n "proc-macro-api") (r "=0.0.189") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.189") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "11jps7qpgrk6z8jxggyisa6qi0bkablz4z30qgzfd1r16piraxg5") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.190 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.190") (d (list (d (n "proc-macro-api") (r "=0.0.190") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.190") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0fsplxaxvcb0m6x2c891rchwjwshb0s6s7s39c03nrnvyfymdcgi") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi")))) (r "1.70")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.196 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.196") (d (list (d (n "proc-macro-api") (r "=0.0.196") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.196") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1gyr0rvh1f0wm4fl8vjkgamhf8jlwc466w32h6hzg88n58fkph65") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.197 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.197") (d (list (d (n "proc-macro-api") (r "=0.0.197") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.197") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0bzhfvapz91vpr1x75z9znbl3bygijx5h6imjw91hps34vff2pp2") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.198 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.198") (d (list (d (n "proc-macro-api") (r "=0.0.198") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.198") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1gwh6qj56qawgm45lsbqhmkxyrsl4dlk84amxm1sjpvcfr6da9n3") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.199 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.199") (d (list (d (n "proc-macro-api") (r "=0.0.199") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.199") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0hyjj6j8nwph41kjl2gzy0qf6fi9kqjxb5h9k87ydb13ka2y2kj1") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.200 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.200") (d (list (d (n "proc-macro-api") (r "=0.0.200") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.200") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1k3px2hb9kx26vbdfxh5q9jsanl6s0nzgw4niwwzxbb2mchm5ffi") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.201 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.201") (d (list (d (n "proc-macro-api") (r "=0.0.201") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.201") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1ckjpg2j4c47yalym1hkiykzqysly4rhayywifzxsvmqm5md03hb") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.202 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.202") (d (list (d (n "proc-macro-api") (r "=0.0.202") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.202") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1i77p2nppv0wxrkwz39ipl20q5rms37612wrhx2vfdyndk1dha6v") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.203 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.203") (d (list (d (n "proc-macro-api") (r "=0.0.203") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.203") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1lzx25lg4hbpcdjsgax4ddsg8yjpk5qrfdad324pixarim8j5i0d") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.74")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.204 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.204") (d (list (d (n "proc-macro-api") (r "=0.0.204") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.204") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1jgn2vck43jzffr05xg1pifzl2v2p83ndrxgfixikj6npmzp9bgh") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.205 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.205") (d (list (d (n "proc-macro-api") (r "=0.0.205") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.205") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1rj76y7v67iyl288pwapb8hhgvbrw7gvgnfxhhy5swnd589slh0s") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.206 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.206") (d (list (d (n "proc-macro-api") (r "=0.0.206") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.206") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1z3xap86ppj8pwfxagfww9xg8l96i4r31ijycpmhsyn1535lw62c") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.207 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.207") (d (list (d (n "proc-macro-api") (r "=0.0.207") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.207") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "01pf09g5z3sxvxpbqg1qg6vj2czsv9i2na264kz7c01wh3xj5hq2") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.208 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.208") (d (list (d (n "proc-macro-api") (r "=0.0.208") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.208") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1cm3aynn693j94sfj34jdj86b3sgvr25a3kzcqgiw2dc8pa1c8m8") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.209 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.209") (d (list (d (n "proc-macro-api") (r "=0.0.209") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.209") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "160gh1clglq54k9hjm9a2jpbcicn07pdx2c9c5qwdy92xgqqgqq2") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.210 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.210") (d (list (d (n "proc-macro-api") (r "=0.0.210") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.210") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1f52dvilmhkv70sh6rgzvmln3pavgfgayl428n7iwgl9gd2k4wb3") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.211 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.211") (d (list (d (n "proc-macro-api") (r "=0.0.211") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.211") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1md99javkhcc35mfkpnkgifbsj4bpwlnnfdcvxcaxjx9nxbzlm8g") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.212 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.212") (d (list (d (n "proc-macro-api") (r "=0.0.212") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.212") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0q3cdarp8v4n19rygylml61awbdmiagfxq2zzrszvydn3fg7q79s") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.213 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.213") (d (list (d (n "proc-macro-api") (r "=0.0.213") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.213") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "08322qvg00805x4gfnlsshqxaskb1ql1flvc4hvpzcvjs1hzka4h") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.214 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.214") (d (list (d (n "proc-macro-api") (r "=0.0.214") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.214") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0b6qa1sfcrkvj9npxlplskrqyn2w7v3zfd69w6d525v115mb3sri") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.215 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.215") (d (list (d (n "proc-macro-api") (r "=0.0.215") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.215") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "0agryli5arhjjk1rhn1wgkv5y9k4s2jqwk1l0v1jlc4gy9319fdz") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.76")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.216 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.216") (d (list (d (n "proc-macro-api") (r "=0.0.216") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.216") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1i2kkavcbp05i9qa7i06plcvj2q4fmsjg6cjfmk65k21qidbcy73") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.78")))

(define-public crate-ra_ap_proc-macro-srv-cli-0.0.217 (c (n "ra_ap_proc-macro-srv-cli") (v "0.0.217") (d (list (d (n "proc-macro-api") (r "=0.0.217") (d #t) (k 0) (p "ra_ap_proc_macro_api")) (d (n "proc-macro-srv") (r "=0.0.217") (d #t) (k 0) (p "ra_ap_proc_macro_srv")))) (h "1a5wb7r4ldj1y6fmzmhfi29xwf20ssglhwfwhcxp0qwkyncw2rk9") (f (quote (("sysroot-abi" "proc-macro-srv/sysroot-abi") ("in-rust-tree" "proc-macro-srv/in-rust-tree" "sysroot-abi")))) (r "1.78")))

