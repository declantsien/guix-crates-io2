(define-module (crates-io ra _a ra_ap_salsa-macros) #:use-module (crates-io))

(define-public crate-ra_ap_salsa-macros-0.0.201 (c (n "ra_ap_salsa-macros") (v "0.0.201") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "173yik8l1v25gi3bxxj73p9lci37bih2wdb049nf3245535yz6m5") (r "1.74")))

(define-public crate-ra_ap_salsa-macros-0.0.202 (c (n "ra_ap_salsa-macros") (v "0.0.202") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19cm92wx1kjcn7l4pmbfixl4n22gdy056qfg4vn3xzkhcfmjszan") (r "1.74")))

(define-public crate-ra_ap_salsa-macros-0.0.203 (c (n "ra_ap_salsa-macros") (v "0.0.203") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zrhj9dq8dbqaknq2hiw04f3q2c7irs80zqwii82995xvwa47g17") (r "1.74")))

(define-public crate-ra_ap_salsa-macros-0.0.204 (c (n "ra_ap_salsa-macros") (v "0.0.204") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1i53y3j4rgian0qwymqcljy2vgsp5jbn76qrqxhwrwhmfhcqizs0") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.205 (c (n "ra_ap_salsa-macros") (v "0.0.205") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kh6gap6f42y2zgxbxad0rq7q3ivy09fnfl2nk0qms84hnbcwnd1") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.206 (c (n "ra_ap_salsa-macros") (v "0.0.206") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03k5vw86lb7b7a995grpl408gcwh19apwzybq31qgci331i8rq8s") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.207 (c (n "ra_ap_salsa-macros") (v "0.0.207") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jfd8p6awq2y547a4xn90f3kvil8fh85wh3qv0qhqi1glln9917n") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.208 (c (n "ra_ap_salsa-macros") (v "0.0.208") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y32gwnhm59zkslkpw8ai11hmkblblvzagnaanmcdcidaqnrw3xy") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.209 (c (n "ra_ap_salsa-macros") (v "0.0.209") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1iym73yp6wwzw1vccnjr383v3z8s3281i7xa1yb9vcyvz8zis9va") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.210 (c (n "ra_ap_salsa-macros") (v "0.0.210") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mrxdy9a7af5d4xqq9x6k2i4xd19bqzdmdvs2nqz9ww65raryvdf") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.211 (c (n "ra_ap_salsa-macros") (v "0.0.211") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dkrhbk8hj6wphyk8x3hg7w2vhr8z26ddyzilp06difh4gynarpg") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.212 (c (n "ra_ap_salsa-macros") (v "0.0.212") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a9h4hy4s8yr8688cm19bbxhr8gdlha3058799bdlkn3hcy7lg00") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.213 (c (n "ra_ap_salsa-macros") (v "0.0.213") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mp1ggiyahnl2x1q6783kvk1yqvr10gz2jb2r1h4bwm34rrgrc24") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.214 (c (n "ra_ap_salsa-macros") (v "0.0.214") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0iwflqaln5myzkl57r6j92iydip4l1cbd7w49hc9cj5hin8bnsa1") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.215 (c (n "ra_ap_salsa-macros") (v "0.0.215") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12djf34rm6r3zsxkllnpywlijbk8lip9xykz3r1gk4q4ya783sz1") (r "1.76")))

(define-public crate-ra_ap_salsa-macros-0.0.216 (c (n "ra_ap_salsa-macros") (v "0.0.216") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rwh7s1skv99wx5cg04hvbd7fn859rsfck8kld0qis0n0zlxi9yr") (r "1.78")))

(define-public crate-ra_ap_salsa-macros-0.0.217 (c (n "ra_ap_salsa-macros") (v "0.0.217") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a0cikgv32agnfg7iiyvlc1wshm0dkndhincn6cp6alswdcfl6n7") (r "1.78")))

