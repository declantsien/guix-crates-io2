(define-module (crates-io ra _a ra_ap_text_edit) #:use-module (crates-io))

(define-public crate-ra_ap_text_edit-0.0.2 (c (n "ra_ap_text_edit") (v "0.0.2") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1al149iarkfszlfffc7vf70avkkm1z97zm8wxycsak1faba75gam")))

(define-public crate-ra_ap_text_edit-0.0.3 (c (n "ra_ap_text_edit") (v "0.0.3") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1z40qpamrzlmdy3z1nm9whmla24vdjzq0icdcq0qx9mmf230w1cx")))

(define-public crate-ra_ap_text_edit-0.0.4 (c (n "ra_ap_text_edit") (v "0.0.4") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1k4qmglwc865spzdnm9pgradc5psxh6a8y1fjsgbhqckjfmc1k4i")))

(define-public crate-ra_ap_text_edit-0.0.5 (c (n "ra_ap_text_edit") (v "0.0.5") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1rgpidykcrdqj6c4s4jkrjifnfkcfry8amw8v6qf6fhanfpfslsg")))

(define-public crate-ra_ap_text_edit-0.0.6 (c (n "ra_ap_text_edit") (v "0.0.6") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1jwqpf1dn25arn8047w0zi7q1vnmfpk19b45im2g9rjpbyxcnwa6")))

(define-public crate-ra_ap_text_edit-0.0.7 (c (n "ra_ap_text_edit") (v "0.0.7") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1qr6ahbsl0ajg5qsw491wf3gnj9m3zxwx4dhb3qky55rmrirzp3g")))

(define-public crate-ra_ap_text_edit-0.0.8 (c (n "ra_ap_text_edit") (v "0.0.8") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "144ksg6yarqh97l8jjpas6c97g6n9h1inl6fj3f513584i82ymk0")))

(define-public crate-ra_ap_text_edit-0.0.9 (c (n "ra_ap_text_edit") (v "0.0.9") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0pf5l82i6zicn8h0525gn850wwni4zsqssbj75w2xsdck58krrbb")))

(define-public crate-ra_ap_text_edit-0.0.10 (c (n "ra_ap_text_edit") (v "0.0.10") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0kgah8kz07rivh95krnk7z6v9ia9ngqh47wwz5ql2mybhi7nvzwi")))

(define-public crate-ra_ap_text_edit-0.0.11 (c (n "ra_ap_text_edit") (v "0.0.11") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0w3fnmnh98hp7s6zqlj8wbjv42a0cy76x59kxbbm00ig1dfvdsqa")))

(define-public crate-ra_ap_text_edit-0.0.12 (c (n "ra_ap_text_edit") (v "0.0.12") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "188bpjkqj39rhwg1spk5d455c08bmdpna6hzc4sldfpaphi1719y")))

(define-public crate-ra_ap_text_edit-0.0.13 (c (n "ra_ap_text_edit") (v "0.0.13") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1ph9zfi28qzdc2dqg90lhwbwnk8i6257wkhsv7vmhzvwjxz9a6gl")))

(define-public crate-ra_ap_text_edit-0.0.14 (c (n "ra_ap_text_edit") (v "0.0.14") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0876r56ah9wzzb94kxcck8lrgr872q88nah2yn045isw643xpdlw")))

(define-public crate-ra_ap_text_edit-0.0.15 (c (n "ra_ap_text_edit") (v "0.0.15") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "08ddb8ig93nrzs3rf73h7p89d9m19lz6i5mjgnacwa2dmpk0lf69")))

(define-public crate-ra_ap_text_edit-0.0.16 (c (n "ra_ap_text_edit") (v "0.0.16") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "09cgxc8gjdmdmvw13drdvnmsl436mnljsrj3csclgky8gymgggnl")))

(define-public crate-ra_ap_text_edit-0.0.17 (c (n "ra_ap_text_edit") (v "0.0.17") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0ia1i348fha7m6a38vm7rh03zh2gcbjcw8prhyv755jp954pbg3m")))

(define-public crate-ra_ap_text_edit-0.0.18 (c (n "ra_ap_text_edit") (v "0.0.18") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1qdmvl7wj1gyfpl5yb5h0dhcixqhjvzz5x9aipn3v230m11kkc0j")))

(define-public crate-ra_ap_text_edit-0.0.19 (c (n "ra_ap_text_edit") (v "0.0.19") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1qd4mfgj1nhhs4hgc78spb408wmffw24hkj5jrk8x4rdaf01d6yg")))

(define-public crate-ra_ap_text_edit-0.0.21 (c (n "ra_ap_text_edit") (v "0.0.21") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0yaysn01z6bl750h0whfpwkj8sn69p391z06knihlnqyz6y5libs")))

(define-public crate-ra_ap_text_edit-0.0.22 (c (n "ra_ap_text_edit") (v "0.0.22") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "11bvk736p72yh4psnhd3hi5myb2hqsxlr9sl4f9dgdw5qjqa43y2")))

(define-public crate-ra_ap_text_edit-0.0.23 (c (n "ra_ap_text_edit") (v "0.0.23") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0mk4y96994byfpdnzdxn8ajqq17d9w0x366k5cpcrx2v16g2ln35")))

(define-public crate-ra_ap_text_edit-0.0.24 (c (n "ra_ap_text_edit") (v "0.0.24") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1dmsd3lsvi8df11dc9981l2clp2jldkxzbk19p7xvc6gj7ynfqrp")))

(define-public crate-ra_ap_text_edit-0.0.25 (c (n "ra_ap_text_edit") (v "0.0.25") (d (list (d (n "text-size") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1qp7x0xnfj6fjh2vl8kbyzz9z3gg9980hcv7sgwrc8z3rzqrif4z")))

(define-public crate-ra_ap_text_edit-0.0.26 (c (n "ra_ap_text_edit") (v "0.0.26") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0y2jg6q9b0zxfz8c99nn2454ns8v5pdfr16pczlfwgrlq5m53hs8")))

(define-public crate-ra_ap_text_edit-0.0.27 (c (n "ra_ap_text_edit") (v "0.0.27") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0rfrbqb048aka0szvh2mb1ssn14s87hw022ggvlkldzxlxb9zc7n")))

(define-public crate-ra_ap_text_edit-0.0.28 (c (n "ra_ap_text_edit") (v "0.0.28") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "196bknmzz1dd198f3r2mp7n5c893qi6v3wrxd5d0r5f3c1w7ksg1")))

(define-public crate-ra_ap_text_edit-0.0.29 (c (n "ra_ap_text_edit") (v "0.0.29") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "16rflckhh7p00wqrflkq99h4pf7hghdz6g82w0rp83wq1f6zh78i")))

(define-public crate-ra_ap_text_edit-0.0.30 (c (n "ra_ap_text_edit") (v "0.0.30") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0sis32z6pvkyminwj5846vwjlrhy0ncmm62n32fqrq9pnfq9yfbr")))

(define-public crate-ra_ap_text_edit-0.0.32 (c (n "ra_ap_text_edit") (v "0.0.32") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1my42m0clxsv256g5w9vhhncwnnbzfq2j6f0lyrgy0dlnfn62sq3")))

(define-public crate-ra_ap_text_edit-0.0.33 (c (n "ra_ap_text_edit") (v "0.0.33") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0g40x3f9qx2gzk9d7qck01fnd574k36hkws2pn3v8z5d5sdn0wvl")))

(define-public crate-ra_ap_text_edit-0.0.34 (c (n "ra_ap_text_edit") (v "0.0.34") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0psd9224qb88a35d56al36h3fsgggxjqf0cgf6k6m1a6ryyr33af")))

(define-public crate-ra_ap_text_edit-0.0.35 (c (n "ra_ap_text_edit") (v "0.0.35") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "18cax046znbn5vil8wf3zhnjx9xzkm3b204f36i1k4mk4cjr3kq2")))

(define-public crate-ra_ap_text_edit-0.0.36 (c (n "ra_ap_text_edit") (v "0.0.36") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1ids0jn13755qxas0lal8zjbgnqsgcc5hikmk88am2df3d5v9s8q")))

(define-public crate-ra_ap_text_edit-0.0.37 (c (n "ra_ap_text_edit") (v "0.0.37") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1w1afdiyxrxh2hw72jy20fiic9s8x09p2r7y7xypcvmjp6p488r3")))

(define-public crate-ra_ap_text_edit-0.0.38 (c (n "ra_ap_text_edit") (v "0.0.38") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1rkqjwiylyhzm75n0mjil0i72304wkhw8mgb0902c9r1ipwh7hgs")))

(define-public crate-ra_ap_text_edit-0.0.39 (c (n "ra_ap_text_edit") (v "0.0.39") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1nj578iid23gkyw6x7ci8wjj6ppv7bkh052ycjnn5yyhdml91xg6")))

(define-public crate-ra_ap_text_edit-0.0.41 (c (n "ra_ap_text_edit") (v "0.0.41") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1hvc0zmhwrq41sj3m8j603b53zp1al8g3i9gbh4cgh6llvgbrnqh")))

(define-public crate-ra_ap_text_edit-0.0.42 (c (n "ra_ap_text_edit") (v "0.0.42") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0xvyw5yf863adam96wm4g0fccajhdmpf47p9jwiz5xlpsgj8cbxk")))

(define-public crate-ra_ap_text_edit-0.0.43 (c (n "ra_ap_text_edit") (v "0.0.43") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0s9kr1bjmyv6m0rpyyvdjf2mxiscc2gdxfjlacvxwdlf14qgimg6")))

(define-public crate-ra_ap_text_edit-0.0.44 (c (n "ra_ap_text_edit") (v "0.0.44") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0a0nkglbjfprhnsak32bpd7nxbacjdhm9q6b8bxv9dfs8k5n1fbb")))

(define-public crate-ra_ap_text_edit-0.0.45 (c (n "ra_ap_text_edit") (v "0.0.45") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0vkjygkaz6933d8wl5967icki5k07ji8h565f3990lwsawi2lnm7")))

(define-public crate-ra_ap_text_edit-0.0.46 (c (n "ra_ap_text_edit") (v "0.0.46") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1ybcm6mg5d36nlaiailxfyfpzaanlzy7c329zsvyk43jjs8ilkch")))

(define-public crate-ra_ap_text_edit-0.0.47 (c (n "ra_ap_text_edit") (v "0.0.47") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1g79cagl5qa23j4wjk5nlvh035rc922rpblyjy2hvkqxnqyr80cs")))

(define-public crate-ra_ap_text_edit-0.0.48 (c (n "ra_ap_text_edit") (v "0.0.48") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0nzvq8xx1vs40wnwxbnx5p5qanxv5jdf5fhnsyd46zmxpjmh4mwa")))

(define-public crate-ra_ap_text_edit-0.0.49 (c (n "ra_ap_text_edit") (v "0.0.49") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1wvm3vfnnvj5mgvn6rvl9ak2c1h2k804dn178kvhmlxcjvrkhh9i")))

(define-public crate-ra_ap_text_edit-0.0.57 (c (n "ra_ap_text_edit") (v "0.0.57") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1cin62f16122cg335f49gvmyj5zn3421jzha59aa3xj89sirc4a5")))

(define-public crate-ra_ap_text_edit-0.0.58 (c (n "ra_ap_text_edit") (v "0.0.58") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0lq9z15h053lnn43ara044s309h751rjzklg8p0b5npzzi715nm1")))

(define-public crate-ra_ap_text_edit-0.0.59 (c (n "ra_ap_text_edit") (v "0.0.59") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "15lqy1hx8xcg86in6c7l0wxa9k1xr06i1c5w3wxyq9wiqxdy48r0")))

(define-public crate-ra_ap_text_edit-0.0.60 (c (n "ra_ap_text_edit") (v "0.0.60") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1aalmdgibk2d49dfs799mxqdqs6pkc8ixwb33gm6ipbxlcyjy8kn")))

(define-public crate-ra_ap_text_edit-0.0.61 (c (n "ra_ap_text_edit") (v "0.0.61") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0qxi0xdg2qv2y7h5rw9mipwfyk8dfl2213rwwj0miz30wd339q8g")))

(define-public crate-ra_ap_text_edit-0.0.62 (c (n "ra_ap_text_edit") (v "0.0.62") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0mj6dggqwqccgl6lwsxhx29ibv210fdg4r2h6b3krkdg25vd8wr5")))

(define-public crate-ra_ap_text_edit-0.0.63 (c (n "ra_ap_text_edit") (v "0.0.63") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0ql32fz46ialcy3bw2g2lzc70psqcfq687q18a8gzjka00lhdjcd")))

(define-public crate-ra_ap_text_edit-0.0.64 (c (n "ra_ap_text_edit") (v "0.0.64") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0i0028zwbrl25x0d7kzq6zjh2wc7ln7vn7pcgqja40jly7iqqfav")))

(define-public crate-ra_ap_text_edit-0.0.65 (c (n "ra_ap_text_edit") (v "0.0.65") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0bwk0v02nk77hpica644jysanmyhm046zzd9q2g75fp35j0cwryr")))

(define-public crate-ra_ap_text_edit-0.0.66 (c (n "ra_ap_text_edit") (v "0.0.66") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1agqdhsi8czs92jk929za18pbr9k624869kj200y78kqmhv5fmvf")))

(define-public crate-ra_ap_text_edit-0.0.67 (c (n "ra_ap_text_edit") (v "0.0.67") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1z54079jrmmxvc01pyid96mzl90q1zk9nx34agw9yg8345xvvpbr")))

(define-public crate-ra_ap_text_edit-0.0.68 (c (n "ra_ap_text_edit") (v "0.0.68") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1zbjckwcv5ywxsiagpqf82hvplhbh5bdglkwg1njbf25nn8ndnms")))

(define-public crate-ra_ap_text_edit-0.0.69 (c (n "ra_ap_text_edit") (v "0.0.69") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "09f0cxyg2yb4imgcc5hwi7aaqma6f75hn6gj6iw7jzg8kxd3c87v")))

(define-public crate-ra_ap_text_edit-0.0.70 (c (n "ra_ap_text_edit") (v "0.0.70") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "05hkhpf2m0w6796gnr18cnpiy9bqfbzcxqrpsqh7kg5jilm95wsr")))

(define-public crate-ra_ap_text_edit-0.0.71 (c (n "ra_ap_text_edit") (v "0.0.71") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1a8y0k34l68kg815g2k3dxwr3qfikrnirljrpbwz1c0rp4jf3h4k")))

(define-public crate-ra_ap_text_edit-0.0.72 (c (n "ra_ap_text_edit") (v "0.0.72") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "18k87k4379hxv8qrqdwxvbfp1w8kcy6d1knhhzaw1i38qn05jcv9")))

(define-public crate-ra_ap_text_edit-0.0.73 (c (n "ra_ap_text_edit") (v "0.0.73") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1rlap0y52i8rx7pcpqv4dn574s0sc48ly44wf9dfph6jaa1g98dw")))

(define-public crate-ra_ap_text_edit-0.0.74 (c (n "ra_ap_text_edit") (v "0.0.74") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1i2ncfb54mygd3i0m1sa3i458pwn3inmq8ks808rf6da0i5mvjkp")))

(define-public crate-ra_ap_text_edit-0.0.75 (c (n "ra_ap_text_edit") (v "0.0.75") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "12r1lbqmvjc299350r5x92mpdwwwcvc5ikyy8vsz5463k66l67n8")))

(define-public crate-ra_ap_text_edit-0.0.76 (c (n "ra_ap_text_edit") (v "0.0.76") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1n460p45yhibmwykg1acyh4mf1kd6jv3jiqvnnwrsrwp0ynsmvbc")))

(define-public crate-ra_ap_text_edit-0.0.77 (c (n "ra_ap_text_edit") (v "0.0.77") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0144wa56balmiwfkghzdmr5wkhwkql0lcyg2l8hjbmkij4ifyk3j")))

(define-public crate-ra_ap_text_edit-0.0.78 (c (n "ra_ap_text_edit") (v "0.0.78") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0bv5b935fkb4ya5slmr9gp9gakk2fnvz75pa6jr4i4pqjy9xj546")))

(define-public crate-ra_ap_text_edit-0.0.79 (c (n "ra_ap_text_edit") (v "0.0.79") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0zx68iqjizbqsl2jddal3gl049806k840angnv0n73alfjlg189j") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.80 (c (n "ra_ap_text_edit") (v "0.0.80") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0ppqj0cir6rjblrlrmhxr4bf0na2ilc54p7645sb73jjcaf0ykv6") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.81 (c (n "ra_ap_text_edit") (v "0.0.81") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "11743s9cdbr1bqjxflbxknd1jcnychb4k3hr77rblhiv3kd0b8m7") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.82 (c (n "ra_ap_text_edit") (v "0.0.82") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0apzkcp8ykx6l7rcxndab37hrvik1p1fragr58dr1a3wwlw4y544") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.83 (c (n "ra_ap_text_edit") (v "0.0.83") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1lajxqpfcx3x53ycw7l9k55vpv0i18sh6jjyr111a7751d2gscnf") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.84 (c (n "ra_ap_text_edit") (v "0.0.84") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "07w93i9v67f4rmly6ki3d4nnk8hkipn7rlp6g1qz92nnhcxj6cp8") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.85 (c (n "ra_ap_text_edit") (v "0.0.85") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0029cvvc12w56yp0r0fvgms9y8j0lvb06xxi9f50ssb0ji4xznmd") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.86 (c (n "ra_ap_text_edit") (v "0.0.86") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "173bx4dhfv3wc3k5x7ypfx9m2c24f8xs32gdr229535abh8nw5za") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.87 (c (n "ra_ap_text_edit") (v "0.0.87") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0h78f7hnyr5zry5yghzk2yaz54c4v2n3pf1dbgyi7k29h2ax5fw7") (r "1.56")))

(define-public crate-ra_ap_text_edit-0.0.89 (c (n "ra_ap_text_edit") (v "0.0.89") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1d9asqjimqvzn06wiiwdzk3vcc8axpmqx5jk00a414iq7y5fh1hd") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.90 (c (n "ra_ap_text_edit") (v "0.0.90") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "14nrlnbaarjj0y4vbvs4nmlx5l0y61zr9i6gj6s6sjcacn0jn7ym") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.91 (c (n "ra_ap_text_edit") (v "0.0.91") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "18l4b95q9lrkikd4c4jycgycn00cxfdz2qdgkhvz2iv0kbzx2ynq") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.92 (c (n "ra_ap_text_edit") (v "0.0.92") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1zjj2g47zv888xwmkhxysm853x7gchjwjzjb52i0db6wl3050sn7") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.93 (c (n "ra_ap_text_edit") (v "0.0.93") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1hjchs4qb4a2f9sg6x626kmxy20gv7a6zk5llkyh4v1vvb3nwih0") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.94 (c (n "ra_ap_text_edit") (v "0.0.94") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0krfdyjwlqmmdks70xnjc4k6b0m0w5gqg48pjsadszbivrd4dr3p") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.95 (c (n "ra_ap_text_edit") (v "0.0.95") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0l3axq9gzv3ycgf791kdic0hhnzxmx16r1zq9j7a6z536m51f680") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.96 (c (n "ra_ap_text_edit") (v "0.0.96") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0y8ggfa87rncwj5v5ps412d922mfw1dzrnbbzs8bhg132hz8bfny") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.97 (c (n "ra_ap_text_edit") (v "0.0.97") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1nm7c48961y2d7gsb3himk85kcm48gzjf3369n2s6qxa2rjcxppr") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.98 (c (n "ra_ap_text_edit") (v "0.0.98") (d (list (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "0cfdi1sj6lrx8fnnsy7r0wh84r4599wv1f9cb77dipn73wlsar3l") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.99 (c (n "ra_ap_text_edit") (v "0.0.99") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1sml4zwid1sr6wkb3ajl6q56mlypa2vj418gblx1v7ixvdwn3z4x") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.100 (c (n "ra_ap_text_edit") (v "0.0.100") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "04jsjwc3v3i9gqqm2pf7mj9qj0m3ii1xhf50wycvnrd6ashvv34s") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.101 (c (n "ra_ap_text_edit") (v "0.0.101") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "text-size") (r "^1.0.0") (d #t) (k 0)))) (h "1aqyjpdwmnhq06mbysmw59v4jy5s256m5vb9vvddhg23y66wx6j8") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.102 (c (n "ra_ap_text_edit") (v "0.0.102") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0d368haksmmiz8pcp0vgmlamh8qs4n132ff75nk992a9z9cx3ib2") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.103 (c (n "ra_ap_text_edit") (v "0.0.103") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0lh2hsy7rq5fpi9nnkx0jylzpag168wfr48mza0zyfq6pd276f0l") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.104 (c (n "ra_ap_text_edit") (v "0.0.104") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0ikqmq6vh3q307i02cafh1p8gzg0zbw3nan6g88z9brp711gcdsh") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.120 (c (n "ra_ap_text_edit") (v "0.0.120") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1azl0pn2nsfdykwxxhag7yj24914qczi6r38a8hxlikyk2gj2rax") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.121 (c (n "ra_ap_text_edit") (v "0.0.121") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0708w7hiww8salhy0wsr3fr1fwsy9sv5lzzp9p21iz64dy8y6j9n") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.122 (c (n "ra_ap_text_edit") (v "0.0.122") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1rwnycl7vwxsw3y9lrjr73q7n3lpkk9i0d5d979hbva5ss3a5qi4") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.123 (c (n "ra_ap_text_edit") (v "0.0.123") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "14g1a2j6rdcl85nsg23x3y9m88kmspgw924ss7hy24rn015dxibw") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.124 (c (n "ra_ap_text_edit") (v "0.0.124") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "16795jqrjgfdh48smmf5yy61jh33cxbzp48x40ryg64279f1ijav") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.125 (c (n "ra_ap_text_edit") (v "0.0.125") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0qbp51wwpi4isalkc1i887qwkgjmfg9dvy58mwxn82pxhpzwcxf2") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.126 (c (n "ra_ap_text_edit") (v "0.0.126") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0431za7mn34s3r50xb2wkm44vgjwz29q71v5jb89vn5vcbz51nzs") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.127 (c (n "ra_ap_text_edit") (v "0.0.127") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1bbn5vjnmmzxwfkxmblrsv118fxx7s05gjc8bf38jfn1g9k6w33i") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.128 (c (n "ra_ap_text_edit") (v "0.0.128") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1w8vzn755nm2aanafvczwsr40qib563x1sf6nj1fzx98dp81smdc") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.129 (c (n "ra_ap_text_edit") (v "0.0.129") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1wx7axbf12g33jk4ai950djq5aav3p5jhnrbmjiga83h5xj4ncky") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.131 (c (n "ra_ap_text_edit") (v "0.0.131") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "13wcy8z1mahd90668lmnbg6xnhgm2ycrxr9n2wi8yms6phdizwkn") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.132 (c (n "ra_ap_text_edit") (v "0.0.132") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1rmlm7y2j9b9lx9xnyb0k95nj5klps099jmfmnh4wfypihgl2p8d") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.133 (c (n "ra_ap_text_edit") (v "0.0.133") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0a2n93r333mkrzh96jhannzgxrdvik60qkps1904y6hksg00h3rx") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.134 (c (n "ra_ap_text_edit") (v "0.0.134") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0cb95c1ql65bqqazkw751q0rwfrjfh2bifmzqhi79dycsplqbvq5") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.136 (c (n "ra_ap_text_edit") (v "0.0.136") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1r2l6r13zbf612w2fykx0cdhldfiabqhvwihnwkb16cjk6a5r348") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.137 (c (n "ra_ap_text_edit") (v "0.0.137") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1rs250ik7cy04laccql89lw8jvynffqqfd5bqchx1xzsa9n4pdjl") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.138 (c (n "ra_ap_text_edit") (v "0.0.138") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "163wp1lp32pbs8sv5qd73syhrwnpq14lzsgvlv1xk3d129jqvlq3") (r "1.57")))

(define-public crate-ra_ap_text_edit-0.0.139 (c (n "ra_ap_text_edit") (v "0.0.139") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "15lyngg2gxpvspv89sm1hlasa549r64bi05bdxsfj37zryygsm65") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.140 (c (n "ra_ap_text_edit") (v "0.0.140") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "09yw360b9lrf94k5xbsz6nhwm515w9mbz4w62h21r9q2x1xs706v") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.141 (c (n "ra_ap_text_edit") (v "0.0.141") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0fqnxz7dfypd76z6n5dfgd8s3xk770nl9d1cd59x60rk2g2x0ja4") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.142 (c (n "ra_ap_text_edit") (v "0.0.142") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1wispdjmygfqxswnwj5rcl7d421hjl2ail2x7cykd4jp1rjjrykk") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.143 (c (n "ra_ap_text_edit") (v "0.0.143") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1bcwbn4p814fy2kpcgipca3wg9s5n119l3caw19ylr0mqp7d8zgi") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.144 (c (n "ra_ap_text_edit") (v "0.0.144") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0318ziapwm66cxr7s6hhx7rn7yniplsnzjrff2ybkglbfw6wyvgc") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.145 (c (n "ra_ap_text_edit") (v "0.0.145") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1xvh0xinwdyswdgcm4fdmsj0ng9k7l05dccp0b60mmggiiwha9dn") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.146 (c (n "ra_ap_text_edit") (v "0.0.146") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0plki9v637ms7rb5ihlayfmmbk1dmrcjnazryr2vnz6183iipnq3") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.148 (c (n "ra_ap_text_edit") (v "0.0.148") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0kb557xkk7c1inmv27by76alkf9cjkg10zgv8b0pjpr7s3aym2na") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.149 (c (n "ra_ap_text_edit") (v "0.0.149") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "08a7v8qdzr6awfmybin7wwy0ca46hfwqsw0ygnfhnb8fd73mi0fi") (r "1.65")))

(define-public crate-ra_ap_text_edit-0.0.157 (c (n "ra_ap_text_edit") (v "0.0.157") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0il2r9bl6i2aliw0cfgn56h2hmvl33h134f4vl3kkyryxdpxm78j") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.158 (c (n "ra_ap_text_edit") (v "0.0.158") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "037g6fj768r0bkmaszlmk01rgyz5fqzr15j3l27p8cbdq0rv5l8s") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.159 (c (n "ra_ap_text_edit") (v "0.0.159") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0kpbpjg6a1dwxq1sgcl4apcmx12fhcg709za5k77cp6dc4i4n8av") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.160 (c (n "ra_ap_text_edit") (v "0.0.160") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0mya2mr5f73n8s35mv5dc9xak922nipmnb147rczhf1pjpx4h5da") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.161 (c (n "ra_ap_text_edit") (v "0.0.161") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1cdvr0awfb7jv377c93bxlvyp4mbhc23zzw4m15cv025c5bb4adj") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.162 (c (n "ra_ap_text_edit") (v "0.0.162") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "015ahplp647bwhxbviahyd8h873rvlbw0ghmjcic6chqhbbhja7s") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.163 (c (n "ra_ap_text_edit") (v "0.0.163") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1rdzfpbgdmidlpyg3g18fyps1a26gqv1x0xkilk7mhhs6w8s0cds") (r "1.66")))

(define-public crate-ra_ap_text_edit-0.0.164 (c (n "ra_ap_text_edit") (v "0.0.164") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0kmdvi60b7iy61n6af8p1in1qyajq2lxx7j88xz05xbn6v5zdzlz") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.165 (c (n "ra_ap_text_edit") (v "0.0.165") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "14ikmwd2a6qhcmmrip32nkycm9s6m23pj9z241ay8ndf4mspcz8m") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.166 (c (n "ra_ap_text_edit") (v "0.0.166") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0amkpn90ayx5wclrv9j1sv4isghwvxbsvi6aky6flk981fnmlp0b") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.167 (c (n "ra_ap_text_edit") (v "0.0.167") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "13lf2hpb8iba2lkfail27745z7c1l7yhamcvsn568hklj7ybj4bz") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.168 (c (n "ra_ap_text_edit") (v "0.0.168") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1vqj7j4a40ambqw6ph7sbav4lj3yi81dg00xhmjv5xms68z1xy6n") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.169 (c (n "ra_ap_text_edit") (v "0.0.169") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "13c02k9z5ggprqv4vjnrys4yfg38g4hip9lyh17hyzgl8p93943r") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.171 (c (n "ra_ap_text_edit") (v "0.0.171") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1w4b0b1mgmgr44c9zyy5xjna1lxpzfxd05cp9anz2qsxklgy48lx") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.172 (c (n "ra_ap_text_edit") (v "0.0.172") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1v75vxc4iaszvflgk29p3afdk6p1giw3fxi1sgaklrhdcbzn09mp") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.173 (c (n "ra_ap_text_edit") (v "0.0.173") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1x50r7bzsdmpyxr0dlyzm3ff9smmf80gf7z7nff5hxwsi6qys6cz") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.174 (c (n "ra_ap_text_edit") (v "0.0.174") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1kkpksjcxpldwjrcvhgjvghbczrvqkfv5m0iwqwxc9v8ay9s6569") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.177 (c (n "ra_ap_text_edit") (v "0.0.177") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0a0gv9qxjd5xs3zsamm8prghy2da1dw6c461f25vbl79nx3gv1bp") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.178 (c (n "ra_ap_text_edit") (v "0.0.178") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0hgpgfka8a7n272zmlc22rnj3jai35df8razw84yzrnjaxmm1ib9") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.179 (c (n "ra_ap_text_edit") (v "0.0.179") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1al9aln7yjicapa126lg2l44j3vb4jml6qm0pv24cg1p776d57ns") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.180 (c (n "ra_ap_text_edit") (v "0.0.180") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "19f0c1hzmqfwbhhxcw70z8339rwd2c855admg090wcnxnbzbdk7w") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.181 (c (n "ra_ap_text_edit") (v "0.0.181") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "048kf4hyx6xrcgpr09g2gzax5fvg9vqmps1plcwxd1x4aj978sy7") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.182 (c (n "ra_ap_text_edit") (v "0.0.182") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0r5kpax00f9mpzln061khbfa3yzccdjzs1jdbfrwprpawvscwgdp") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.183 (c (n "ra_ap_text_edit") (v "0.0.183") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "13jl4wvyqlw8fcx0jm309l4c6fvqgpc377bhrry80c8cjhhs5k19") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.184 (c (n "ra_ap_text_edit") (v "0.0.184") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1pyq7p79j60k81c413zj5qiyby0fhbz2rvw388rmldh94z19djf2") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.185 (c (n "ra_ap_text_edit") (v "0.0.185") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "0fg10dj127kyz2jrz1qbi5hgnd6gj5ljwqyy5csppcvsbb8n0si2") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.186 (c (n "ra_ap_text_edit") (v "0.0.186") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0dmzig80w2zmlanhsvkaxx57bm7axgwca0rxvya7fy4w5fvhyng2") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.187 (c (n "ra_ap_text_edit") (v "0.0.187") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1chh7mh075rdc8iwnpykn8ff3qw9db46vjsx3xhmfwc7cl9sv5xj") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.188 (c (n "ra_ap_text_edit") (v "0.0.188") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0rns7h48562g96gqn1jjm4fwwhc7zfcih60hi1ilwk1g9n3jqjrf") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.189 (c (n "ra_ap_text_edit") (v "0.0.189") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "08a5ki33sf8iprs96l4xn9b6vg6fzpmwndbj52kkwvz2vc2air2n") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.190 (c (n "ra_ap_text_edit") (v "0.0.190") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1n47vmak1ajii629pi4nkjvijqdnp4bnn3q9a9sdwmk2yj8l0rcj") (r "1.70")))

(define-public crate-ra_ap_text_edit-0.0.193 (c (n "ra_ap_text_edit") (v "0.0.193") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0jy626xawk14gpdjcgcm0shwqg9b3az7r0bx6bww3ax7qjk6rcp5") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.194 (c (n "ra_ap_text_edit") (v "0.0.194") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1snm1kj80z3gqykmc25s287r15l7acrzyalxh0cgl512g01lfkfi") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.195 (c (n "ra_ap_text_edit") (v "0.0.195") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0d52wc6z3llccfifw2fyshhsab5rr7qlf54x96djldb95cq0fcw6") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.196 (c (n "ra_ap_text_edit") (v "0.0.196") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "02qkhh1bn20xlcy47s047l51c4fil54wqv4p701kg81qprb0wgq8") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.197 (c (n "ra_ap_text_edit") (v "0.0.197") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0sjgdqf13il2dj9xkpkws42nv7i2zk6pyhzq42706qxc78flm05g") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.198 (c (n "ra_ap_text_edit") (v "0.0.198") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1zq8xxsy6ii67rmgq2998ipms273h7xb9fqghwxg4f5k5gsnbp7i") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.199 (c (n "ra_ap_text_edit") (v "0.0.199") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0j17d3a8dvi7rywfmlgizz12a46smph9ri7pgmfq3gfkwm8magh8") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.200 (c (n "ra_ap_text_edit") (v "0.0.200") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0fz0mf02i4h0ij9a8wb9zf2f644mi9ac0lvc5skbpbnqd794rha2") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.201 (c (n "ra_ap_text_edit") (v "0.0.201") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "00mf1jkcry586ph109ay8hl31vxzrc0biz11kvz6b1ph957qydr6") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.202 (c (n "ra_ap_text_edit") (v "0.0.202") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0vfw4papmspb7xbdzx827karic90s080ygfn4xzvqdadc13mjqwn") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.203 (c (n "ra_ap_text_edit") (v "0.0.203") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0gvlf123p9y6vyhdnq9nj4fxq77177yvdlxkaqns3rlggfi0fwnb") (r "1.74")))

(define-public crate-ra_ap_text_edit-0.0.204 (c (n "ra_ap_text_edit") (v "0.0.204") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1p9hig4gszizkdqbzxqcysynjfrg7dz7q67zgjaw6hm9y6x1smfn") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.205 (c (n "ra_ap_text_edit") (v "0.0.205") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "00dbn9py7xzdbfs6ljjsjics3j10hi3kbba8gh7c79xds337qjvz") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.206 (c (n "ra_ap_text_edit") (v "0.0.206") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1b74rd82p3xr7mqfgzbswy2aynr68l8dxryci723gj3d5vk89p7b") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.207 (c (n "ra_ap_text_edit") (v "0.0.207") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1izkwnj9fg0cyfbgiyk7adg9dch9iah6w6y1i2i9qzscbf70sb47") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.208 (c (n "ra_ap_text_edit") (v "0.0.208") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1mrjisjbn2yxvczhymv035qx76xvnhm7vpbrrpcrpp9rwymj05g8") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.209 (c (n "ra_ap_text_edit") (v "0.0.209") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1xig2viqyijz7acn45wf0ghwn1zx0dw6b0l96w1y5ki041y6p1ac") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.210 (c (n "ra_ap_text_edit") (v "0.0.210") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "14avcnhhl22cbd3cnllsjdm101ls538rffbpsjrvcxz8mppf1lc3") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.211 (c (n "ra_ap_text_edit") (v "0.0.211") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0gd2fl6wfjs831alnc77c81hfcqifildcg768i8mk84qj21yrxw3") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.212 (c (n "ra_ap_text_edit") (v "0.0.212") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0g5bpj0wx1pcpfriv1ly67wdc115rqqzy4z8vy1b1b37s4h5vr7x") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.213 (c (n "ra_ap_text_edit") (v "0.0.213") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1bdh4k7c3yf8wfwmk6ha3apjsmxzp6clijw05c6hd93mjss8jm0b") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.214 (c (n "ra_ap_text_edit") (v "0.0.214") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0ic460v6ah1wpfx2zd0c4j30dh4i11kb8jfz32c87mmncmgl62zj") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.215 (c (n "ra_ap_text_edit") (v "0.0.215") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "1bd7kdsxyrykr0dhfxicq154d2a6aq91d917n2nbxxvdfa7z8l51") (r "1.76")))

(define-public crate-ra_ap_text_edit-0.0.216 (c (n "ra_ap_text_edit") (v "0.0.216") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0jwpflpvkak8ffg42dj52gs6mdc3j27bfh4f641dirlbd2xysgpx") (r "1.78")))

(define-public crate-ra_ap_text_edit-0.0.217 (c (n "ra_ap_text_edit") (v "0.0.217") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0pc138wnlav4clyg76109sqw38phr43fwh5ki5zjirwv5hxk1rsg") (r "1.78")))

