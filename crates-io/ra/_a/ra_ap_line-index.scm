(define-module (crates-io ra _a ra_ap_line-index) #:use-module (crates-io))

(define-public crate-ra_ap_line-index-0.0.16 (c (n "ra_ap_line-index") (v "0.0.16") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1ix7yaclzqls63w2a6wsarj8jgxmjg9ndqmrmqik14mnccj2w6hl")))

(define-public crate-ra_ap_line-index-0.0.17 (c (n "ra_ap_line-index") (v "0.0.17") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1dxf26xb7gvibmys8vjjidzdy4l8kvv40h1qdsdlyl5ppn693vbv")))

