(define-module (crates-io ra no rano) #:use-module (crates-io))

(define-public crate-rano-0.2.0 (c (n "rano") (v "0.2.0") (d (list (d (n "console") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09j1snbp0p46vmv4qifs7x94sk613ssqyjwq7khfd2zb8xysvay3")))

