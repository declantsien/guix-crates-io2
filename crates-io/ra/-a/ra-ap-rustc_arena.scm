(define-module (crates-io ra -a ra-ap-rustc_arena) #:use-module (crates-io))

(define-public crate-ra-ap-rustc_arena-0.9.0 (c (n "ra-ap-rustc_arena") (v "0.9.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0zii8fm5jxap8xbxffb0i5vvy34753cpz9rg9xbqwvs0ygbk53a9")))

(define-public crate-ra-ap-rustc_arena-0.10.0 (c (n "ra-ap-rustc_arena") (v "0.10.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0k94ig830i8p7lfz1460aixxjwnfbvzbghv7c0w2m5rmnrd9wkjv")))

(define-public crate-ra-ap-rustc_arena-0.11.0 (c (n "ra-ap-rustc_arena") (v "0.11.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0l7fz9j8yc5nqdxfxy2bim0csrl3w6sa40i9216wj732c8sxqsic")))

(define-public crate-ra-ap-rustc_arena-0.12.0 (c (n "ra-ap-rustc_arena") (v "0.12.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13qjpinssaq69zhk2n1h0i3yi8fcprynn5rk50gd8dr8qahczhj4")))

(define-public crate-ra-ap-rustc_arena-0.13.0 (c (n "ra-ap-rustc_arena") (v "0.13.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0gzcgrgmh586bmpxjmv6593yvnxilz75dv7gkmkk5kqssl7jygpj")))

(define-public crate-ra-ap-rustc_arena-0.14.0 (c (n "ra-ap-rustc_arena") (v "0.14.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1i8m3l9n7bhpnl6xqprh7z816jc2cq4i2ym6n0bc6xndlqb38vh4")))

(define-public crate-ra-ap-rustc_arena-0.15.0 (c (n "ra-ap-rustc_arena") (v "0.15.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08b4kw14ifyncfxr1xaxw1gxiwv3q7b4f8p6bqs89wmrdi3q3mjy")))

(define-public crate-ra-ap-rustc_arena-0.16.0 (c (n "ra-ap-rustc_arena") (v "0.16.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1qrmclmmih7zm6i1i8rnbxay3dp1i5d5bi675syll4cis9zv4wch")))

(define-public crate-ra-ap-rustc_arena-0.17.0 (c (n "ra-ap-rustc_arena") (v "0.17.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0ifrgrb6hbr337j4ak36k8dpc475l2ps45chfqhkqggi3dz0yq07")))

(define-public crate-ra-ap-rustc_arena-0.18.0 (c (n "ra-ap-rustc_arena") (v "0.18.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1397zj37v2gq8gafaj3spn2d9dq2s2qzzwaj6xkjih0x9lzmrg78")))

(define-public crate-ra-ap-rustc_arena-0.19.0 (c (n "ra-ap-rustc_arena") (v "0.19.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0qm17wdkif2ijnq4mhsz20wpyvd7af1hd62gsjfrgrpnifmc4arr")))

(define-public crate-ra-ap-rustc_arena-0.20.0 (c (n "ra-ap-rustc_arena") (v "0.20.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "08drl8hbq6jsjcdisg30a92f8gbgc0bq0ssb3ymvbyv7y043n9wi")))

(define-public crate-ra-ap-rustc_arena-0.21.0 (c (n "ra-ap-rustc_arena") (v "0.21.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "05814d41xw9gxkqq03himms39q20g9jzrnbk3r6dqc21v4n1kdyz")))

(define-public crate-ra-ap-rustc_arena-0.22.0 (c (n "ra-ap-rustc_arena") (v "0.22.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "13c32p7436rkah2zzmlf37ca0c99ck7z7h6wicqzd99k0fcyl6gv")))

(define-public crate-ra-ap-rustc_arena-0.23.0 (c (n "ra-ap-rustc_arena") (v "0.23.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1wrg3sfbr4wn59wl536rj8f1259ddfd5fmkaixr2dfc4zax5v6hl")))

(define-public crate-ra-ap-rustc_arena-0.24.0 (c (n "ra-ap-rustc_arena") (v "0.24.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "1z1cg1yxcdxmqk48a72q7169k74jrv17n0g4z0jahfn3daak5lmw")))

(define-public crate-ra-ap-rustc_arena-0.25.0 (c (n "ra-ap-rustc_arena") (v "0.25.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0mfqw9qq2xmr3hixs2klg012dz00inbbw26nyxr8fzrxcf6i9g7h")))

(define-public crate-ra-ap-rustc_arena-0.26.0 (c (n "ra-ap-rustc_arena") (v "0.26.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "04d3i1ifi9yvwjf9b17dsqz19xkmvsmsra1bzw8659al4n31b7ak")))

(define-public crate-ra-ap-rustc_arena-0.27.0 (c (n "ra-ap-rustc_arena") (v "0.27.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "03292fm0vp03q2ajnc8dawj4pdgc2p22qskavr9pwdm517cfy0yz")))

(define-public crate-ra-ap-rustc_arena-0.28.0 (c (n "ra-ap-rustc_arena") (v "0.28.0") (d (list (d (n "smallvec") (r "^1.8.1") (f (quote ("union" "may_dangle"))) (d #t) (k 0)))) (h "0i1qz9jing837f6442qfbwjz792n25gkn0k66hmwai3bswwq6qj8")))

