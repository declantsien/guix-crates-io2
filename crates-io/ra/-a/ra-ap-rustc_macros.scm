(define-module (crates-io ra -a ra-ap-rustc_macros) #:use-module (crates-io))

(define-public crate-ra-ap-rustc_macros-0.8.0 (c (n "ra-ap-rustc_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1npahd8yw07ricb5jg6xlvlhh345cmrcj6l8nvci7fgzf7hddw31")))

(define-public crate-ra-ap-rustc_macros-0.9.0 (c (n "ra-ap-rustc_macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "02jvls11b31l8fldqd3plmxp6bf6k86nir26bqv61ipq5fdwlvr1")))

(define-public crate-ra-ap-rustc_macros-0.10.0 (c (n "ra-ap-rustc_macros") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "139gnn5fgx21vk8h0v11zssc1rqm9jn6r8fkg41dcdz9hbpkf3zl")))

(define-public crate-ra-ap-rustc_macros-0.11.0 (c (n "ra-ap-rustc_macros") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "17bkhv826p5syk7xfw4s2laljgij0vq891w7c0nrz22x4kxd3fqx")))

(define-public crate-ra-ap-rustc_macros-0.12.0 (c (n "ra-ap-rustc_macros") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "10653h69masifil2arlyzbgdfj2wcc031m7w5cy8a46ijm3xm9zs")))

(define-public crate-ra-ap-rustc_macros-0.13.0 (c (n "ra-ap-rustc_macros") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1lkr0kr998i8da5hrx4gzyl2kdb9pis1j2ffpqz5pav304mjvljh")))

(define-public crate-ra-ap-rustc_macros-0.14.0 (c (n "ra-ap-rustc_macros") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "19bz58pvzy55s7b8289250ljrrihn3l7k1hiaakmgv49ns6ga9fz")))

(define-public crate-ra-ap-rustc_macros-0.15.0 (c (n "ra-ap-rustc_macros") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0q81d3a5danh0r8m4l011888qjzqh45ljf19bph67nxfg545v29i")))

(define-public crate-ra-ap-rustc_macros-0.16.0 (c (n "ra-ap-rustc_macros") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1dw39liy99vqjlj20vapr2bdii0krjxajiwmxb4yki6mpfvk1pyr")))

(define-public crate-ra-ap-rustc_macros-0.17.0 (c (n "ra-ap-rustc_macros") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "17945c6a146xhgqml9h4wi2hm6g2sz3081f1g3nj0ffva0wh7cq8")))

(define-public crate-ra-ap-rustc_macros-0.18.0 (c (n "ra-ap-rustc_macros") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1063lrhr1phc80w9anrl2zfhaa17il2qlgf9cfb82mq0ng3gkv0s")))

(define-public crate-ra-ap-rustc_macros-0.19.0 (c (n "ra-ap-rustc_macros") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "02zzjzg2x0qk5zj8dzz3ykbj3pwjafj9zsahkb2smlyhx2glnljl")))

(define-public crate-ra-ap-rustc_macros-0.20.0 (c (n "ra-ap-rustc_macros") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0yc1a1sbzyq8aywg57zb8nzngbv99gmff5x6py07vl684kavh9cd")))

(define-public crate-ra-ap-rustc_macros-0.21.0 (c (n "ra-ap-rustc_macros") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0h4d5apvvrap4gfidwlsi4rb9phxzpgpxm0xz4dadqyncvqylnbp")))

(define-public crate-ra-ap-rustc_macros-0.22.0 (c (n "ra-ap-rustc_macros") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1h3h5zyfbv0m9r3b51sx3r4r5cgc3fh2mbalp2fgany2449a0cg1")))

(define-public crate-ra-ap-rustc_macros-0.23.0 (c (n "ra-ap-rustc_macros") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "02xx72x53lmp092yhxvfv76pxvn72cnnzijav8n43g0rm3nqy9wx")))

(define-public crate-ra-ap-rustc_macros-0.24.0 (c (n "ra-ap-rustc_macros") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1xwrlpm2gaypq8f6lrjq9gavkspd4s1vc9980j10fkml5dfx2rlh")))

(define-public crate-ra-ap-rustc_macros-0.25.0 (c (n "ra-ap-rustc_macros") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0vpbfkz2jplyi4ry8a1vnb8m6p0b1jrdvp5ifyij486dmn9xjqr4")))

(define-public crate-ra-ap-rustc_macros-0.26.0 (c (n "ra-ap-rustc_macros") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1cmd44q057g2g8z482ghm4490ga00zjy2i5mmm9wr24x9f0lwm72")))

(define-public crate-ra-ap-rustc_macros-0.28.0 (c (n "ra-ap-rustc_macros") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1j5vb3i9nzc6yz6qx1zx4441i0vwg815kc0x6qh1m67jh3i3fdf8")))

(define-public crate-ra-ap-rustc_macros-0.30.0 (c (n "ra-ap-rustc_macros") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1yypga1nnpjng1xbx8d42kxrs4x6safh9aq06dxrpxp6p707grg2")))

(define-public crate-ra-ap-rustc_macros-0.31.0 (c (n "ra-ap-rustc_macros") (v "0.31.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0iqzb222w21vrk6aa6bcaq2wf88p0rm8yqyx5swr61s9cpr19c78")))

(define-public crate-ra-ap-rustc_macros-0.32.0 (c (n "ra-ap-rustc_macros") (v "0.32.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1c5xi9vp0z3z7l7h3wkz9awgyvr888kkj63dl30jfl4i0fk6ma5b")))

(define-public crate-ra-ap-rustc_macros-0.33.0 (c (n "ra-ap-rustc_macros") (v "0.33.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0jfardshs0ax4sv4w6wnqsjb5fa9dmvi0ivm5hhqxzqwy03xzvyg")))

(define-public crate-ra-ap-rustc_macros-0.34.0 (c (n "ra-ap-rustc_macros") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1qhdjpyvab35m1xn882n11jl5rblzrapbawia7hn0nzw6wwkrnh1")))

(define-public crate-ra-ap-rustc_macros-0.35.0 (c (n "ra-ap-rustc_macros") (v "0.35.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1a1b7h9iql9wfa9czgachc5sxwfdj2ss1gm48w06wzm963yp81bg")))

(define-public crate-ra-ap-rustc_macros-0.36.0 (c (n "ra-ap-rustc_macros") (v "0.36.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0cz6a6v69jf7h57bh9i2g429abi4v51vvrnz6nvxn46vz19jjb19")))

(define-public crate-ra-ap-rustc_macros-0.37.0 (c (n "ra-ap-rustc_macros") (v "0.37.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "19hwa3770flplr47sf3qx8kdhn3q08nx0i6lq0fiix050mxln2is")))

(define-public crate-ra-ap-rustc_macros-0.38.0 (c (n "ra-ap-rustc_macros") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "11p50nid9lhhzpc96s3py1jlzpxbim6q659npzf2wgl0pmp9alic")))

(define-public crate-ra-ap-rustc_macros-0.39.0 (c (n "ra-ap-rustc_macros") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1h1fd0f0c7wz9rvibyflj9inf69w25mxdygl6j9a62yrf7l33j93")))

(define-public crate-ra-ap-rustc_macros-0.40.0 (c (n "ra-ap-rustc_macros") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0izlfb1fpi0xx0cl6918jw4axmpcd2lnbvd0pd8hj05hqzar1b22")))

(define-public crate-ra-ap-rustc_macros-0.41.0 (c (n "ra-ap-rustc_macros") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0fhlvmmy6hj9vqhk6222n5r9mqgis9a3gwvgw18zc9qfmzyiwrnx")))

(define-public crate-ra-ap-rustc_macros-0.42.0 (c (n "ra-ap-rustc_macros") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0vh4b1mzd4l6gympnzwnk32n86ifl367fjzcsd1hik7vcjqwb6my")))

(define-public crate-ra-ap-rustc_macros-0.43.0 (c (n "ra-ap-rustc_macros") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1rc6zpj7q1l5yvsl5flj935dg7v8rnbxwfc1fwh7hx28m5y6l880")))

(define-public crate-ra-ap-rustc_macros-0.44.0 (c (n "ra-ap-rustc_macros") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1l0wvqfwy7fiqhnmxvzrxp3za88jsfz5mn9nb4n1d9fvpc7bazlx")))

(define-public crate-ra-ap-rustc_macros-0.45.0 (c (n "ra-ap-rustc_macros") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0jbgdgv014452fw0193p5l64afq5k6dk9f72l0z4w1yfm351wsrg")))

(define-public crate-ra-ap-rustc_macros-0.46.0 (c (n "ra-ap-rustc_macros") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1nzlx5ign0ravbqchbfksdyhm8ld7gxc5w322ip5y8a510bwa3ah")))

(define-public crate-ra-ap-rustc_macros-0.47.0 (c (n "ra-ap-rustc_macros") (v "0.47.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0ykkij1rkfw5kgspq5mhypcbpz6shww9a89l5njllywa8vb3ydlf")))

(define-public crate-ra-ap-rustc_macros-0.48.0 (c (n "ra-ap-rustc_macros") (v "0.48.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0sqbawgyniyxyqak05lq8ald1s3x7lgdfnf0y5w16996pyclvqxk")))

(define-public crate-ra-ap-rustc_macros-0.49.0 (c (n "ra-ap-rustc_macros") (v "0.49.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1rp3ibc41asl1km6gv14dxllg04k4mz3bh3xch359ka3bwzgzkyl")))

(define-public crate-ra-ap-rustc_macros-0.50.0 (c (n "ra-ap-rustc_macros") (v "0.50.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0c1iimfpahyh3gwrashc72hnk1hy72rav56fx15hjwchdq6abnxf")))

(define-public crate-ra-ap-rustc_macros-0.51.0 (c (n "ra-ap-rustc_macros") (v "0.51.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "18c2g3yq43rkwqf1md3q4haw0vmlfpim9kisxap2gbpb55d7bn9q")))

(define-public crate-ra-ap-rustc_macros-0.52.0 (c (n "ra-ap-rustc_macros") (v "0.52.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "048gksr0baf99vqv7ydvvx48hj9qkag74b4n4k2j5l1hzdjhv901")))

(define-public crate-ra-ap-rustc_macros-0.53.0 (c (n "ra-ap-rustc_macros") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0bcf4v9fn6vcah6rp056qxcfn0j7q5k3b0r4naav4axjf60xgypr")))

(define-public crate-ra-ap-rustc_macros-0.54.0 (c (n "ra-ap-rustc_macros") (v "0.54.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "13gf6sr50jccbvngp9az5adili3crsap8d8h6rsp3yrq78zfak6q")))

