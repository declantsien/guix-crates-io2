(define-module (crates-io ra -a ra-ap-rustc_lexer) #:use-module (crates-io))

(define-public crate-ra-ap-rustc_lexer-0.1.0 (c (n "ra-ap-rustc_lexer") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1cs5kfxdhq83s9inqcm5qgnx1968blc8a9jiis8kgwnk5rq4bhg1")))

(define-public crate-ra-ap-rustc_lexer-0.2.0 (c (n "ra-ap-rustc_lexer") (v "0.2.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0x92v10zmzg2agclkdh7bzckj1p1z06j6ck9pbq74dhf3rmmvcj0")))

(define-public crate-ra-ap-rustc_lexer-0.3.0 (c (n "ra-ap-rustc_lexer") (v "0.3.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1klbzf1r5v4dw14mk3ml8p2almwg99m0nrsjlsvq5hfxr8df47i5")))

(define-public crate-ra-ap-rustc_lexer-0.4.0 (c (n "ra-ap-rustc_lexer") (v "0.4.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "037wgdqsr6gcxqnmyws4a5nqmw57b1rb7k6v4xlw5vgrw6jgs8qq")))

(define-public crate-ra-ap-rustc_lexer-0.5.0 (c (n "ra-ap-rustc_lexer") (v "0.5.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1pr5zn31vzy4nh10arfn0v7b4y4lcp19my2a01h9incsyalpmmar")))

(define-public crate-ra-ap-rustc_lexer-0.6.0 (c (n "ra-ap-rustc_lexer") (v "0.6.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "197xf2h4ny8y16v1g9spjnd1wnk7n6bcg7pxafjrl95bdxfly16f")))

(define-public crate-ra-ap-rustc_lexer-0.7.0 (c (n "ra-ap-rustc_lexer") (v "0.7.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1ivy20pbd6r5xbyg9d0zg512s8z59kpy6b3320rf3yzghg1sprpi")))

(define-public crate-ra-ap-rustc_lexer-0.8.0 (c (n "ra-ap-rustc_lexer") (v "0.8.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0pfkrq7c0hwgbrfhxf9dwvljnx5k5bvk018yrhpc5lrin8lbb9a0")))

(define-public crate-ra-ap-rustc_lexer-0.9.0 (c (n "ra-ap-rustc_lexer") (v "0.9.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1q3v7my1f5g05271j05hhlxyl77bnz3f8mzn7jsm5yd04rr5zvwv")))

(define-public crate-ra-ap-rustc_lexer-0.10.0 (c (n "ra-ap-rustc_lexer") (v "0.10.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "15f19pzcm829dbrz8rriizfn0gkkr5yanxw247ayxr12hjsgdqpj")))

(define-public crate-ra-ap-rustc_lexer-0.11.0 (c (n "ra-ap-rustc_lexer") (v "0.11.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0l2v257cg6aj5i7bixj7kda3cl1lm46hnmzc52mx6cw6amly0rkx")))

(define-public crate-ra-ap-rustc_lexer-0.12.0 (c (n "ra-ap-rustc_lexer") (v "0.12.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1wv0h0yh4j3y41j9rpzxwmk45q59893kcyjmx3w3affrh7fpsyhz")))

(define-public crate-ra-ap-rustc_lexer-0.13.0 (c (n "ra-ap-rustc_lexer") (v "0.13.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "03yddxfqw4qdbyp85amx9a29gcbi45mw3r5fcyly2iar1lrf1l3c")))

(define-public crate-ra-ap-rustc_lexer-0.14.0 (c (n "ra-ap-rustc_lexer") (v "0.14.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0sysllgi4qybpz44yr6g2bxvv8r2indld8bwy9gx5wd4kd7x5zrh")))

(define-public crate-ra-ap-rustc_lexer-0.15.0 (c (n "ra-ap-rustc_lexer") (v "0.15.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1qqqihxmjidc7x8pfn2c825c8b0lcli3if0gmf0zr9xz45jcnj7m")))

(define-public crate-ra-ap-rustc_lexer-0.16.0 (c (n "ra-ap-rustc_lexer") (v "0.16.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1s61p7llay7285ipq516733d3lnd266403c0baqsdb5vy3vi7hd6")))

(define-public crate-ra-ap-rustc_lexer-0.17.0 (c (n "ra-ap-rustc_lexer") (v "0.17.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "05wpmnvgn11fr4hcq2a6fm6fivanaxcy54jx9mb7frwdnfvcg4ii")))

(define-public crate-ra-ap-rustc_lexer-0.18.0 (c (n "ra-ap-rustc_lexer") (v "0.18.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1k9dipvw45lm9h4dn613h5qdig0vhah5sazn4cc7rqxibsranqhp")))

(define-public crate-ra-ap-rustc_lexer-0.19.0 (c (n "ra-ap-rustc_lexer") (v "0.19.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1z4q0nj5hgkf9qqw1vdh9lkh88018bnxxpnv84lgw0byf2zp7rch")))

(define-public crate-ra-ap-rustc_lexer-0.20.0 (c (n "ra-ap-rustc_lexer") (v "0.20.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0h5sr1gpmcdkq28qmjspqidrfjlrcrj888c37l1c95ap3588cplx")))

(define-public crate-ra-ap-rustc_lexer-0.21.0 (c (n "ra-ap-rustc_lexer") (v "0.21.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0adz7j6jy1n6hq602xc58xklk39wsxdy6qmm2ssglghhg1x1qx6w")))

(define-public crate-ra-ap-rustc_lexer-0.22.0 (c (n "ra-ap-rustc_lexer") (v "0.22.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1ffg46cnc9lsdsl13mslhy1z6hr59p4b7b55xnwlsv01wwb6b8n5")))

(define-public crate-ra-ap-rustc_lexer-0.23.0 (c (n "ra-ap-rustc_lexer") (v "0.23.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0jjknw1hwaq4f2fipwyd3rjkz6mggzyidfb2gm7m672aky2213p5")))

(define-public crate-ra-ap-rustc_lexer-0.24.0 (c (n "ra-ap-rustc_lexer") (v "0.24.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0rybn8wypjcsl5adqnwj28xgr0hr23r97g2ri0j7ifm0swd691xx")))

(define-public crate-ra-ap-rustc_lexer-0.25.0 (c (n "ra-ap-rustc_lexer") (v "0.25.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "15ay89na2p4bdhd1h6n53ks38swf6iakln3hy4dqjg8hwmkb3vh4")))

(define-public crate-ra-ap-rustc_lexer-0.26.0 (c (n "ra-ap-rustc_lexer") (v "0.26.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1h2l00a2pkhawjrld8k3hmcq3vxqjxm365ni1qjiqfziqqgy4vx5")))

(define-public crate-ra-ap-rustc_lexer-0.28.0 (c (n "ra-ap-rustc_lexer") (v "0.28.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0q2jgan713v47nkzq4ksg0cc24yl9iby9lbypgg5sxkmbqgblhsa")))

(define-public crate-ra-ap-rustc_lexer-0.30.0 (c (n "ra-ap-rustc_lexer") (v "0.30.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0khwqpzyba7gvq9xna3lakcc2bs7sx3hja6rk77jk5v4rr0ym6ha")))

(define-public crate-ra-ap-rustc_lexer-0.31.0 (c (n "ra-ap-rustc_lexer") (v "0.31.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "17fp3pcsg94255qlbny2idc2ybamxkgl0dgv7s1vpd9z7igcz5jl")))

(define-public crate-ra-ap-rustc_lexer-0.32.0 (c (n "ra-ap-rustc_lexer") (v "0.32.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "02bgmhnjvsg47xs0zmhfz8g1glbnii7fkr2x9l4kgx10fhivwxfy")))

(define-public crate-ra-ap-rustc_lexer-0.33.0 (c (n "ra-ap-rustc_lexer") (v "0.33.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0rpb7x72iizxk16xr15p581v9p102sx5zyhai9g5j5spdqsj3lnj")))

(define-public crate-ra-ap-rustc_lexer-0.34.0 (c (n "ra-ap-rustc_lexer") (v "0.34.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "126h4i1y2q7cylr0lrsc2skn75azw97f82y4x39i2vanknr9bdxh")))

(define-public crate-ra-ap-rustc_lexer-0.35.0 (c (n "ra-ap-rustc_lexer") (v "0.35.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "05g0b6nh42ca6fv4c99f8wxianl1ajj8zix1jr1bm5qs3ajhznn8")))

(define-public crate-ra-ap-rustc_lexer-0.36.0 (c (n "ra-ap-rustc_lexer") (v "0.36.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1bx3vb75lah7zayqhffn97f5h8yna902r59qnihy6b8bghqjql0s")))

(define-public crate-ra-ap-rustc_lexer-0.37.0 (c (n "ra-ap-rustc_lexer") (v "0.37.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "02h5cd0480f3fay39klsia31dm4c0q42lm2gq1wyi2fghi9f9hln")))

(define-public crate-ra-ap-rustc_lexer-0.38.0 (c (n "ra-ap-rustc_lexer") (v "0.38.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0llif2f4s0cgkgh2ljnsib7vi945qlnpxi0n6aan6fm5giw8067z")))

(define-public crate-ra-ap-rustc_lexer-0.39.0 (c (n "ra-ap-rustc_lexer") (v "0.39.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0cx84rj6bywzl6ydcmqic4si9dgl724y7s7xq73b9ayy93w7c2nb")))

(define-public crate-ra-ap-rustc_lexer-0.40.0 (c (n "ra-ap-rustc_lexer") (v "0.40.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0j3b46j0rvfmfv8b2cd63mvm0iiwh0lzjwq3514gsh9hsyw6wd2g")))

(define-public crate-ra-ap-rustc_lexer-0.41.0 (c (n "ra-ap-rustc_lexer") (v "0.41.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0qpqrrjc7l6nbyfxpym053xalp6nlqm3kilfv7xljxmichjcvjf8")))

(define-public crate-ra-ap-rustc_lexer-0.42.0 (c (n "ra-ap-rustc_lexer") (v "0.42.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "101d92dcdi1bw7z4avi63s5ddjkmypbwx5nwpciwgd2plxrj71gs")))

(define-public crate-ra-ap-rustc_lexer-0.43.0 (c (n "ra-ap-rustc_lexer") (v "0.43.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "16igxf3kzad32wm6d11yavmax9zi73flx3r7qs8aq8rl6hnx9kij")))

(define-public crate-ra-ap-rustc_lexer-0.44.0 (c (n "ra-ap-rustc_lexer") (v "0.44.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1wyzhh8zg6pigq4fspsvcjlilw3dpb4mvg9k42vrxl3rhpy87dma")))

(define-public crate-ra-ap-rustc_lexer-0.45.0 (c (n "ra-ap-rustc_lexer") (v "0.45.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1a9apwj2fdavyd8377k2qrssaxsaj2f6rw14mz9nc908hjmw3l2m")))

(define-public crate-ra-ap-rustc_lexer-0.46.0 (c (n "ra-ap-rustc_lexer") (v "0.46.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1jsp8gh3whh7rmafn5mk2c13p7d2zxawf4877840cjpdgavf6b7s")))

(define-public crate-ra-ap-rustc_lexer-0.47.0 (c (n "ra-ap-rustc_lexer") (v "0.47.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "09r2l7by95ys1mvcxnmj9ibrnnsdk8iksbchsxh08nxviafqd0cz")))

(define-public crate-ra-ap-rustc_lexer-0.48.0 (c (n "ra-ap-rustc_lexer") (v "0.48.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0dih675wz3k5vrssafs27mp3b1l102593w22p07fsnm8cqfdrifn")))

(define-public crate-ra-ap-rustc_lexer-0.49.0 (c (n "ra-ap-rustc_lexer") (v "0.49.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0k537jjh4vgjpid79f468pzhlqhdazy5x5k22w7hsindzpzwxdzf")))

(define-public crate-ra-ap-rustc_lexer-0.50.0 (c (n "ra-ap-rustc_lexer") (v "0.50.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "03fzymjfp4bf3rcqxzaspgaabnhb6w2glcjp8p0v0ha2s7b8634c")))

(define-public crate-ra-ap-rustc_lexer-0.51.0 (c (n "ra-ap-rustc_lexer") (v "0.51.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1k71kdkmcg5lfm7g3imnvbz2cn9gf79hyc4x0sjz038lv9vkf29g")))

(define-public crate-ra-ap-rustc_lexer-0.52.0 (c (n "ra-ap-rustc_lexer") (v "0.52.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "0nx4285ybfj4b2n412ffg21csb6zdp7y6fdiswa1ck98fmmh32r3")))

(define-public crate-ra-ap-rustc_lexer-0.53.0 (c (n "ra-ap-rustc_lexer") (v "0.53.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1n1slbjzr118d7y20d0zpkzb0c9xjmn5zcp0phn8kafbpnqa5n6v")))

(define-public crate-ra-ap-rustc_lexer-0.54.0 (c (n "ra-ap-rustc_lexer") (v "0.54.0") (d (list (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "unicode-properties") (r "^0.1.0") (f (quote ("emoji"))) (k 0)) (d (n "unicode-xid") (r "^0.2.0") (d #t) (k 0)))) (h "1frl4fhwr31imffsys5v30z7vf4j1z9rlb1fpg99wlm6h3jpi5ck")))

