(define-module (crates-io ra qi raqiya-bible-reference) #:use-module (crates-io))

(define-public crate-raqiya-bible-reference-0.1.0 (c (n "raqiya-bible-reference") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "06xdljxfb0kz8m4n4mdw6s64rrb1c9kr1d8rsk8kg01xcn2bhbi6")))

(define-public crate-raqiya-bible-reference-0.1.2 (c (n "raqiya-bible-reference") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "17lh9pl2c6ni9zz4vj93s9y8k86l1x89h05xwa3dxf39g55n863b")))

(define-public crate-raqiya-bible-reference-0.1.3 (c (n "raqiya-bible-reference") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "0bjvzgy7422f25hxd4n1zmc88v8aqbrnz46sq7n0ik0sbq761ir5")))

