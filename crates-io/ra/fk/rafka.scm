(define-module (crates-io ra fk rafka) #:use-module (crates-io))

(define-public crate-rafka-0.1.0 (c (n "rafka") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rafka_codegen") (r "^0.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "tokio-tower") (r "^0.4") (d #t) (k 0)) (d (n "tokio-util") (r "^0.3.1") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tower") (r "^0.3") (d #t) (k 0)))) (h "079nmdw9wc9qnamypn6bp07z3mdffmhnq4xk47lkaykf8fnqhnf2") (f (quote (("tls" "tokio-rustls") ("default" "tls"))))))

