(define-module (crates-io ra fk rafka_codegen) #:use-module (crates-io))

(define-public crate-rafka_codegen-0.0.0 (c (n "rafka_codegen") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11s8qhj34km4swnd1r18wa300i0akpw63cr14x0c3fp05ra74x8y")))

