(define-module (crates-io ra np ranpha) #:use-module (crates-io))

(define-public crate-ranpha-0.1.0 (c (n "ranpha") (v "0.1.0") (d (list (d (n "bpaf") (r "^0.4") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.6") (d #t) (k 0)))) (h "1ffnp56cc33ipqcy5nmn1fp9wlgf9k1zw70c9cihvsdnbyp9xpab")))

(define-public crate-ranpha-0.1.1 (c (n "ranpha") (v "0.1.1") (d (list (d (n "bpaf") (r "^0.4") (d #t) (k 0)) (d (n "qrcode-generator") (r "^4.1.6") (d #t) (k 0)))) (h "1yw613vsp3w45gi3ihqvx61s6big6m6467ymvi1y9gbm05mrg7a0")))

