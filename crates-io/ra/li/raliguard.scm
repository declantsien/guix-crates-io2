(define-module (crates-io ra li raliguard) #:use-module (crates-io))

(define-public crate-raliguard-0.1.0 (c (n "raliguard") (v "0.1.0") (h "1sajgjm4ixqvniigmbh572msfjmcrdlwkqafr9wq4zx8drcydry8")))

(define-public crate-raliguard-0.1.1 (c (n "raliguard") (v "0.1.1") (h "134y983ndkby718jyw4zgksbcijf6v8lyyzq9m1w6bl0malxwkic")))

(define-public crate-raliguard-0.1.2 (c (n "raliguard") (v "0.1.2") (h "0dmk28ir5ipwghq31l74dr7sznim0692wibfkjxla9q6vagv6prx")))

(define-public crate-raliguard-0.1.3 (c (n "raliguard") (v "0.1.3") (h "01jw83kggdip3zjqn445rv1z128ihfa4a228rfwjs0h48qkc2drm")))

