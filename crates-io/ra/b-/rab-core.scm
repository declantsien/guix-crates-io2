(define-module (crates-io ra b- rab-core) #:use-module (crates-io))

(define-public crate-rab-core-0.3.3 (c (n "rab-core") (v "0.3.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y4446y49kfgic5l56sq7zhmmd9jpzl9qrybqrkcfbrbvmqzsydp")))

(define-public crate-rab-core-0.4.0 (c (n "rab-core") (v "0.4.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gj2c8is18fws6pm1iy1j3cmph58nmxyr25qmd9ach8jdamylyrl")))

(define-public crate-rab-core-0.4.1 (c (n "rab-core") (v "0.4.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z4wz7ajaprymqx5mznilk5rqssvks5288dzscrxwxnc8h0g7zv6")))

