(define-module (crates-io ra it raitech2020_guessing_game) #:use-module (crates-io))

(define-public crate-raitech2020_guessing_game-0.1.0 (c (n "raitech2020_guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1306gq78lfx2cly5i4d56arljvxzm706q2vvnbxar6bl95di6ay0")))

(define-public crate-raitech2020_guessing_game-0.1.1 (c (n "raitech2020_guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1n79ii0avbk8vzja7wciihqi9s4y7z3p344mgli0044aghly1gjg")))

