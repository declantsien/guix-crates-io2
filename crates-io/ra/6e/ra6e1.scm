(define-module (crates-io ra #{6e}# ra6e1) #:use-module (crates-io))

(define-public crate-ra6e1-0.2.0 (c (n "ra6e1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1h37givng01kkzfy3indblbdhva0r4fhjgilagjbz0aa8ysv0jsl") (f (quote (("rt" "cortex-m-rt/device"))))))

