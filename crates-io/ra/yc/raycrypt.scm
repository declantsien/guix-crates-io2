(define-module (crates-io ra yc raycrypt) #:use-module (crates-io))

(define-public crate-raycrypt-0.1.0 (c (n "raycrypt") (v "0.1.0") (d (list (d (n "benchmark-simple") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1f2ah0ri68jbhfn8c0ambbgw88y5i5mc8w31jy4agfkcz7aq07jp")))

(define-public crate-raycrypt-0.3.0 (c (n "raycrypt") (v "0.3.0") (d (list (d (n "benchmark-simple") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0nyirs1fp522qsr4p9lhxmzq0b0xqk0mxlb39mrh4c3q2zlrm4sn")))

(define-public crate-raycrypt-0.3.1 (c (n "raycrypt") (v "0.3.1") (d (list (d (n "benchmark-simple") (r "^0.1") (d #t) (k 2)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "1di0ayp1k2yny71agxs8j05i48yqywprbl3prfq3fv9wviabfsg4")))

