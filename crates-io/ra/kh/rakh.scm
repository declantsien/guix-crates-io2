(define-module (crates-io ra kh rakh) #:use-module (crates-io))

(define-public crate-rakh-0.1.0 (c (n "rakh") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1rc7741k9g07ilpy88yqkpyyliyyy9fp131f9vq1fxwr8p73zd62")))

(define-public crate-rakh-1.0.0 (c (n "rakh") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0f5397dlvqgb4yvc0hggag2lnisvah343bxl61cb6faqs1s3a0q0")))

(define-public crate-rakh-1.0.1 (c (n "rakh") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1jy3nlyk1zi92wbnzn2hmyq0qpb9hz4wdvn02qy4q49dkfr9qnws")))

(define-public crate-rakh-1.1.0 (c (n "rakh") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "05balssx3sbfsrk3a6syqhhw25p6cs151m0r9fvy4r0ynr8j0p9f")))

(define-public crate-rakh-1.1.1 (c (n "rakh") (v "1.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mfac88j99qbhaw7akpvmgv39rfbzjsmd8dsdmj0v4dw6r28pymh")))

(define-public crate-rakh-1.1.2 (c (n "rakh") (v "1.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nraf7h6jy3922f1bd6hwvdkps7j5xslyyabix8znb2n7b630zc2")))

