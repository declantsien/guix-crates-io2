(define-module (crates-io ra ph raph) #:use-module (crates-io))

(define-public crate-raph-0.1.1 (c (n "raph") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vf6mrzp0wypjsgmdwiqs6xqw53fb822nn3xn329wmpxag5k7n15")))

(define-public crate-raph-0.1.2 (c (n "raph") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "06f79c3ad3zj6wy2j9ff2lggl18pz8139p157ma9w409vzcrbsmz")))

