(define-module (crates-io ra ph raphtory-pymodule) #:use-module (crates-io))

(define-public crate-raphtory-pymodule-0.3.2 (c (n "raphtory-pymodule") (v "0.3.2") (d (list (d (n "py-raphtory") (r "^0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.1") (f (quote ("multiple-pymethods" "chrono"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18.1") (d #t) (k 1)))) (h "1l48djzflgk2c7mn0ah8icr30v3xrsfhyl6gcm66xz7sxp46ddg8") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module")))) (r "1.67.1")))

(define-public crate-raphtory-pymodule-0.4.0 (c (n "raphtory-pymodule") (v "0.4.0") (d (list (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "py-raphtory") (r "^0.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.1") (f (quote ("multiple-pymethods" "chrono"))) (d #t) (k 0)) (d (n "pyo3-build-config") (r "^0.18.1") (d #t) (k 1)))) (h "09fd923zl8c7dwnm6kykfa1lp1yh24wh3xix8c49d184i9zmbrh7") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module")))) (r "1.67.1")))

