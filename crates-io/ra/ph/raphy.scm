(define-module (crates-io ra ph raphy) #:use-module (crates-io))

(define-public crate-raphy-0.1.0 (c (n "raphy") (v "0.1.0") (h "1bfyzy7a2qx3rfi6ksrg026zh0szzn7cx0qnqrcbp4sn7pv407db")))

(define-public crate-raphy-0.1.1 (c (n "raphy") (v "0.1.1") (h "170d1qc54lqnqqcfbkpf5qkgylkaxqbn1mdwjjysjcmxm8xkjmga")))

(define-public crate-raphy-0.1.2 (c (n "raphy") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "00f4p0grjgjdpj0n62vl70zkjllz8schkdlb2j5yqipbpz2fsn60")))

(define-public crate-raphy-0.1.3 (c (n "raphy") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1cqd9jz1c4l73lyicbdndxrnx2hidnnhdf2757wkjwwnshzrrxi9")))

(define-public crate-raphy-0.1.4 (c (n "raphy") (v "0.1.4") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0ja3fy72skansvlfim2arv7g55ix6zj9v3nirb7cz96aqsm2a44y")))

(define-public crate-raphy-0.1.5 (c (n "raphy") (v "0.1.5") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1g2yy6qr3sn8p0jcp2f83zgh271vniwivam6ca5snxakmpb6xzws")))

(define-public crate-raphy-0.1.6 (c (n "raphy") (v "0.1.6") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1kw6p89blvc04hxcxdrcw6vzqxkbdvcswpwx0h0wp8v0sgx51kyw")))

(define-public crate-raphy-0.1.7 (c (n "raphy") (v "0.1.7") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1farinpvdpiqfcqxjqc2gy1aqg4b4ydngdgvfsh3bqkk7nk1pvyz")))

(define-public crate-raphy-0.1.8 (c (n "raphy") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "0xpmva0hhp78kycs2m3ymf8nrjamx6rvp2l5w6pxa2nni47bm9cj")))

(define-public crate-raphy-0.1.9 (c (n "raphy") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)))) (h "1wvxx8ap3bvnc71b098asr2jn30riq3sfxp2qapg9rlsfn0awx9j")))

(define-public crate-raphy-0.1.10 (c (n "raphy") (v "0.1.10") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0qd0iz38d4y83y36fik361f56q6p1x79ar7y6rrzqqjp9lps4app")))

(define-public crate-raphy-0.1.11 (c (n "raphy") (v "0.1.11") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0b4xblsaq3i507f6h40qwh8vjqdwwc0m21nnix5qapwx7pp6l91y")))

(define-public crate-raphy-0.1.12 (c (n "raphy") (v "0.1.12") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1wsysfsdqvazd8i6lyv8278hsv22180xdq3kfy0b7mzxbpr2l8kk")))

(define-public crate-raphy-1.0.0 (c (n "raphy") (v "1.0.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "byte-slice-cast") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "08nbxcq490q9wdfb6rlgs4h0hpd8956r4lapy93wbm5f1snj16hk")))

(define-public crate-raphy-1.0.1 (c (n "raphy") (v "1.0.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "byte-slice-cast") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0gkf9jqddn7fgl4xawmw3radmkz2jvkvgvicrf9wxyh0zvfq74cd")))

