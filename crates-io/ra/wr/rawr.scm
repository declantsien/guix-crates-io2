(define-module (crates-io ra wr rawr) #:use-module (crates-io))

(define-public crate-rawr-0.1.0 (c (n "rawr") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.7.14") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "0rzqrv8a5p63gc1iaawjymp2vm266v83f9srj8xcsgbq4xnhx3id")))

(define-public crate-rawr-0.1.1 (c (n "rawr") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "serde") (r "^0.7.14") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.4") (d #t) (k 0)))) (h "1bfvh46kk71yxckqgwv44g7blb7wdnvx6lizp6y32i4plcyz5r1m")))

