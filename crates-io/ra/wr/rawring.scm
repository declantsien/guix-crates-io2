(define-module (crates-io ra wr rawring) #:use-module (crates-io))

(define-public crate-rawring-1.0.0 (c (n "rawring") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1i7mfaf9x77cisaqq75s79kvg1npj3j5jcnakm1y4sa707v6z4sr") (y #t)))

(define-public crate-rawring-2.0.0 (c (n "rawring") (v "2.0.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "0b2givs07v77jz19wd82rgxp8gwinkcxgan007davn402cdm1ib0") (y #t)))

(define-public crate-rawring-2.0.1 (c (n "rawring") (v "2.0.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)))) (h "1f235ai3n0wagp2bzfm64hnp3zbfjlp9is8lnzsc8i3plqjjnlv3") (y #t)))

