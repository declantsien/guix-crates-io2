(define-module (crates-io ra wr rawrrr) #:use-module (crates-io))

(define-public crate-rawrrr-0.1.0 (c (n "rawrrr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g605wwbk6lf5gbcxyar5pdll7ap1hml97zc2i1xgnbyicx9iz5w")))

(define-public crate-rawrrr-0.2.0 (c (n "rawrrr") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rbi6a5bpwyj445g15fwhpz6dadjzyjbjgv5lvm17ygmbhwijs2i")))

(define-public crate-rawrrr-0.2.1 (c (n "rawrrr") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sk35k4ahkxh4yx7z1k1dr9g9x0lsc7b2l2x59fni0rh9n4m2y3p")))

