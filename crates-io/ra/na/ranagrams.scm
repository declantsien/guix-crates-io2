(define-module (crates-io ra na ranagrams) #:use-module (crates-io))

(define-public crate-ranagrams-0.1.0 (c (n "ranagrams") (v "0.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dranqy1jn06wlyfvmrncg1p4cx95z5q8ar6qajprsg1n208jq1y")))

(define-public crate-ranagrams-0.1.1 (c (n "ranagrams") (v "0.1.1") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1djrqzihmdvngbxbj47bah995ivikjh9cyi67mcgzb4s514q8s5p")))

(define-public crate-ranagrams-0.1.2 (c (n "ranagrams") (v "0.1.2") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0v88q1wr9k5xfghxhvxbjwb33r9bb0zqa8r4p09d4qzf3jnl6828")))

(define-public crate-ranagrams-0.1.4 (c (n "ranagrams") (v "0.1.4") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1l7pc378963p2i6320dz1lgffhvh6wsvvwq2dy4yxbmms92df602")))

(define-public crate-ranagrams-0.1.5 (c (n "ranagrams") (v "0.1.5") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0mizqsmr9047xpy3403fa0c13mpza9qriiv62g21a73gkva770pj")))

(define-public crate-ranagrams-0.1.6 (c (n "ranagrams") (v "0.1.6") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17jflbc2p3swly3xgp10gw88dxsmir8ldnwi0bhyah34llf8995w")))

(define-public crate-ranagrams-0.1.7 (c (n "ranagrams") (v "0.1.7") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hzchx94llckv0hah4ap28r91pq3lhnyi3qwmb7v4zaq5qgk2q95")))

(define-public crate-ranagrams-0.2.1 (c (n "ranagrams") (v "0.2.1") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "08ygrn5vinvdahfsq4qsj558b1vzgrp2qlhmzqgh0qq70klj4gld")))

(define-public crate-ranagrams-0.2.2 (c (n "ranagrams") (v "0.2.2") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1g4zf617b4mzbakdjdnn50xbc3z9kgrlxl8x8dss004fk1nlm4xk")))

(define-public crate-ranagrams-0.2.3 (c (n "ranagrams") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "mpmc") (r "^0.1.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.5") (d #t) (k 0)))) (h "0ffwsgd1qn5zk7xka02yjns8qf3isxwszmg1sv2lvl6kqjkd1vdx")))

(define-public crate-ranagrams-0.2.4 (c (n "ranagrams") (v "0.2.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "mpmc") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0hi0apky3qqimpaq03r20cvh279v7zch23y7qkqrph0n3n3vpp62")))

(define-public crate-ranagrams-1.0.0 (c (n "ranagrams") (v "1.0.0") (d (list (d (n "clap") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "dirs") (r ">=2.0.0, <3.0.0") (d #t) (k 0)) (d (n "mpmc") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.3, <0.8.0") (d #t) (k 0)))) (h "0g9002sahawpdx2b1xj0x4cmbj5cwrdh4acnsv6c4pvkqlqlr0l7")))

