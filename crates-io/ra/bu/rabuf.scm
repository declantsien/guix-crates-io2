(define-module (crates-io ra bu rabuf) #:use-module (crates-io))

(define-public crate-rabuf-0.1.1 (c (n "rabuf") (v "0.1.1") (h "0ibb38rblj7n403qbp9ij7m99kabravjcapsmpqcxgnfac1mm67b") (f (quote (("default"))))))

(define-public crate-rabuf-0.1.2 (c (n "rabuf") (v "0.1.2") (h "13sz8z0axxg48ba0lq5ngdv666lbfb5ipascr52r5kkwbcam0m5k") (f (quote (("default") ("buf_stats") ("buf_lru"))))))

(define-public crate-rabuf-0.1.3 (c (n "rabuf") (v "0.1.3") (h "04xay6lzksf8bvxrbhsi1v7h3d0jcq15p8w1xbwvlsrz1frkcf87") (f (quote (("default" "buf_overf_rem_all") ("buf_stats") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru"))))))

(define-public crate-rabuf-0.1.4 (c (n "rabuf") (v "0.1.4") (h "0as64slf4jjpf899l01srd660qy40ya6hnzawmj0szk7vqwrsm0q") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero") ("buf_stats") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_idx_btreemap") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.5 (c (n "rabuf") (v "0.1.5") (h "100jj342wmhwl600782fcgcmjpwcs59d7bj1ld74wbkyfndf7r69") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero") ("buf_stats") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_idx_btreemap") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.6 (c (n "rabuf") (v "0.1.6") (h "0yq1c9ac0pvv94l6laark80zx5b6i31jfpwrsj7b5dk4vclwy7y2") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.7 (c (n "rabuf") (v "0.1.7") (h "0kvsh9dqzzxmx5ry85ssvfd0zfq4s8vlb803zpw4fn7xb8szay7d") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.8 (c (n "rabuf") (v "0.1.8") (h "09ilh6jxcmbpnrj8ilnzib2r247psii6wmbyz1yjxwmqlys7flrp") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_hash_turbo") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.9 (c (n "rabuf") (v "0.1.9") (h "0igxk1q762549a5vy7m9f0jdmycw9xccjrb57zgmzcnk1q16q561") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_hash_turbo") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.10 (c (n "rabuf") (v "0.1.10") (h "0lzq1f6ppx76sycwdk23mbcqz0g653d5q39blhrnr1q3l1fr4xlv") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_lru") ("buf_hash_turbo") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.11 (c (n "rabuf") (v "0.1.11") (h "0xf2mg9xkarra567rhpl6ss942vc9h7n7gawgy7939vi3ivf68a5") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.12 (c (n "rabuf") (v "0.1.12") (h "0k1p6f9z6mvhp0nh3wg2sl6szhl2zbbcsbc0a3ilsc1q1lwphcil") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.13 (c (n "rabuf") (v "0.1.13") (h "0r5351gjq102q8jzc1i5w97vc7g8dzibdjjqdris4a9yxk6018xj") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.14 (c (n "rabuf") (v "0.1.14") (h "0f3glqv8rqcj4axj3si91x956sqmbr4gblhpzr786zw73r8z8asn") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.15 (c (n "rabuf") (v "0.1.15") (h "1c8g1i6ygkknqlbbzvvbrdg8axa6wsac0hg8h1r55xpjf9fm9f6c") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.16 (c (n "rabuf") (v "0.1.16") (h "195y75cajccxa42lrvs51a7pfpajy2g20vx2g8yslck4pvr26kzn") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size"))))))

(define-public crate-rabuf-0.1.17 (c (n "rabuf") (v "0.1.17") (h "08dlz7k73p7al5m7mz01gmwckmybmr7fv77g14vbldjgap5s7fp8") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size")))) (r "1.56.0")))

(define-public crate-rabuf-0.1.18 (c (n "rabuf") (v "0.1.18") (h "1z81hrcrfp5h9752a22ypfwydqiz6i0cirfc88x580gbly9vmh8p") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size")))) (r "1.56.0")))

(define-public crate-rabuf-0.1.19 (c (n "rabuf") (v "0.1.19") (h "0a6aqixnjljv1p5dil61m4scmdfw9jy744aydh67w1ixv20px6ks") (f (quote (("default" "buf_auto_buf_size" "buf_overf_rem_all" "buf_pin_zero" "buf_hash_turbo") ("buf_stats") ("buf_print_hits") ("buf_pin_zero") ("buf_overf_rem_half" "buf_overf_rem" "buf_lru") ("buf_overf_rem_all" "buf_overf_rem") ("buf_overf_rem") ("buf_myhash") ("buf_lru") ("buf_hash_turbo" "buf_myhash") ("buf_debug") ("buf_auto_buf_size")))) (r "1.56.0")))

