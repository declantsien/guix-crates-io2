(define-module (crates-io ra bu rabu) #:use-module (crates-io))

(define-public crate-rabu-0.2.2 (c (n "rabu") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1w69arji2gxh7j21sd00m2f3jf96xgrmc7snxdgxwhxglacm40y8")))

(define-public crate-rabu-0.2.3 (c (n "rabu") (v "0.2.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1fk95rhyhvpjxkjcblbkq4gkw2b73shhflfz69qmwa70gnnil3zg") (f (quote (("default"))))))

(define-public crate-rabu-0.2.4 (c (n "rabu") (v "0.2.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0ybikk3ar8hrk5siglhzc8jfngxi23932z6064dy8saaa6i7gxk7") (f (quote (("default"))))))

(define-public crate-rabu-0.3.0 (c (n "rabu") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1piv7ih7i2wq2a5d5sw22yw9kmr448fif1nm5b3fdqybckl3a12c") (f (quote (("default"))))))

(define-public crate-rabu-0.3.1 (c (n "rabu") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "partial-min-max") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0npxhnhvr5kx8prc9pjrqj7cxxcnx71bv3lwh78slfajlbd58gdn") (f (quote (("default"))))))

