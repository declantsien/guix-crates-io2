(define-module (crates-io ra ss rassert-rs) #:use-module (crates-io))

(define-public crate-rassert-rs-1.0.0 (c (n "rassert-rs") (v "1.0.0") (h "1bsnrvspvdss51zz82ziic1ikgg644z5517vi24aghhsvaayzc0z")))

(define-public crate-rassert-rs-2.0.0 (c (n "rassert-rs") (v "2.0.0") (h "1m2gya8pnb6i372mk295iv5pk9hs5r4cx4pdvhh9jz768v5fzb3p")))

(define-public crate-rassert-rs-3.0.0 (c (n "rassert-rs") (v "3.0.0") (h "167h6jql163m80hlh4bdnbqyw6zvhv79kv2akwj46hjfrq7qy9lv")))

