(define-module (crates-io ra pl raplay) #:use-module (crates-io))

(define-public crate-raplay-0.1.0 (c (n "raplay") (v "0.1.0") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)))) (h "0sx7lvri69gs8dvkibx67dwx6lv36bv1n84b4wqd6kx9ysyxq888")))

(define-public crate-raplay-0.1.1 (c (n "raplay") (v "0.1.1") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)))) (h "0x9dh86g1kzps1f9fhm8fnfq0q4vv4plvrq7qr2dqy7vf4c67zq0")))

(define-public crate-raplay-0.1.2 (c (n "raplay") (v "0.1.2") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)))) (h "004ch07jk5rac79agrwcsxwbvm9lxnnxz4xpmbshp02gi75pcw9k")))

(define-public crate-raplay-0.2.0 (c (n "raplay") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1aw9kxjs2k9h6xnsp6zw9i858x2fsjhys4h7rpji6hvii51kfp4l")))

(define-public crate-raplay-0.2.1 (c (n "raplay") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0g2sznfvi7zcqmcvn1kcr8p3w8060lrz90jxfb541hwsgs4k2dcx")))

(define-public crate-raplay-0.2.2 (c (n "raplay") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "12waqiixpig1l7p4cmrr6ihrglv70bqv7kz228gky2qskjh58axj")))

(define-public crate-raplay-0.3.0 (c (n "raplay") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0fvn63nf6gr92qy1hasrkgqrj75w83w1b74dssc40wxrlpcb7j0k")))

(define-public crate-raplay-0.3.1 (c (n "raplay") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0x3mlx4lja24yfgf3vfxsby2r616izxwydjxwj3irggssw1mjhgr")))

(define-public crate-raplay-0.3.2 (c (n "raplay") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0q8f26i4v7vg3ljjbhvj6p4rr0jbh792v1illv8r57a9vhn7bsrm")))

(define-public crate-raplay-0.3.3 (c (n "raplay") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1vq9gpvmyfi33za0zdlc0z387mrwi97j8kky42pyqbf0vschm0j8")))

(define-public crate-raplay-0.3.4 (c (n "raplay") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1py1kaisdg14l03j2dyy77wfs5jpgx3a6m74iigr1g1l3kbb2j7g")))

