(define-module (crates-io ra pl rapl) #:use-module (crates-io))

(define-public crate-rapl-0.1.0 (c (n "rapl") (v "0.1.0") (h "13smq25ds321l1mwprd2hwi279f950lzd7kzk1334p3icb1h5s2c")))

(define-public crate-rapl-0.1.1 (c (n "rapl") (v "0.1.1") (h "1174aqkn4bm73nv677c3c100qpnl91d707nhsyxfhgm4rkxivc4a")))

(define-public crate-rapl-0.2.0 (c (n "rapl") (v "0.2.0") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1jiw3w7bf03zxqxsfd9a3k7ipmwnfhazp0y08l6pm8cw53ssvlzi") (f (quote (("default" "complex" "rapl_image") ("complex")))) (s 2) (e (quote (("rapl_image" "dep:image"))))))

(define-public crate-rapl-0.2.1 (c (n "rapl") (v "0.2.1") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "01sxcamw35kgsqcx73qrnkbwic5janncl8wmh2qcsx49wzkn7q1r") (f (quote (("default" "complex" "rapl_image") ("complex")))) (s 2) (e (quote (("rapl_image" "dep:image"))))))

(define-public crate-rapl-0.3.0 (c (n "rapl") (v "0.3.0") (d (list (d (n "image") (r "^0.24.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.1.0") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.16.0") (d #t) (k 0)))) (h "1i916ppnh9vmqkc24gbbzhmayiy5dcklgyabc3ncdisnqb8x79bg") (f (quote (("default" "complex" "rapl_image" "fft") ("complex")))) (s 2) (e (quote (("rapl_image" "dep:image") ("fft" "complex" "dep:rustfft"))))))

