(define-module (crates-io ra _t ra_traits) #:use-module (crates-io))

(define-public crate-ra_traits-0.0.1 (c (n "ra_traits") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0xq7ix2svif1lvmcb5zdcamhnkl4i976x2hv9v9vg5qbj75j3xcc")))

