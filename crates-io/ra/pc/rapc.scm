(define-module (crates-io ra pc rapc) #:use-module (crates-io))

(define-public crate-rapc-0.1.0 (c (n "rapc") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "text_io") (r "^0.1.9") (d #t) (k 0)))) (h "13gf0s5y3d628rp8frbw82wv449f3kyw42cprmf4mxayr4ph5hc5")))

