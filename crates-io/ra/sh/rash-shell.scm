(define-module (crates-io ra sh rash-shell) #:use-module (crates-io))

(define-public crate-rash-shell-0.1.0 (c (n "rash-shell") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.16.1") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.5") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^2.1.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0vmyx36b9bam6kfahzys5046rcnbcr1fyk8jlw4wnqvcfkvxsga0")))

(define-public crate-rash-shell-0.2.0 (c (n "rash-shell") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.5") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rustyline") (r "^8.0.0") (d #t) (k 0)))) (h "1x10f1zbrn5g9za6dhbjhbbnhhlcdryp1mlay81vwyrmsdrhqihh")))

