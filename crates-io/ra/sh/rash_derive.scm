(define-module (crates-io ra sh rash_derive) #:use-module (crates-io))

(define-public crate-rash_derive-0.1.0 (c (n "rash_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wsswdxh0ncrxnx5sw744163hbghq6vvvpwccppqwc3kmflq2d1m") (f (quote (("default" "syn/full"))))))

(define-public crate-rash_derive-1.0.0 (c (n "rash_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a8xmy5mriyrl2dxy3b18vmkwc4f3zkg988hmw3l0j5xawsvncgj") (f (quote (("default" "syn/full"))))))

(define-public crate-rash_derive-1.0.1 (c (n "rash_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12qvns4abagpfg9702l8yqdm8mqjn8whwg74d1wbdwcl6swf87yy") (f (quote (("default" "syn/full"))))))

(define-public crate-rash_derive-1.0.2 (c (n "rash_derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c4941jx60m47bn59iwskjvvxx8v9vfl5sxg9fm87zg7di6z0zya") (f (quote (("default" "syn/full"))))))

(define-public crate-rash_derive-1.1.0 (c (n "rash_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1crq5n663gc99lrcddvws399is9icjh0bp016zab0r5xgvazbhgh") (f (quote (("default" "syn/full"))))))

(define-public crate-rash_derive-1.2.0 (c (n "rash_derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18i7qj7mdx2fj0mz8jmpfp5qd7bdgqlh4rj1kn2sapfj2ss02bfq") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.3.1 (c (n "rash_derive") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fidr2d6alr1bsvk6gvf13vj3n9k7kzy60qm8g6sqvfbh4ba1iha") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.4.1 (c (n "rash_derive") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11lzqyr4l3hz4xhj462gcx3m2a54lpigippl7lc20995abp5sawi") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.5.0 (c (n "rash_derive") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12za0nxi7mb557skxajg8dx7v42cr2v2y5pm7aj3p1vl3qgx9nzb") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.6.0 (c (n "rash_derive") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ay60bnlvasqqbvd8k5qr0vki1dic9d61874n68v434lgajaxdvy") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.6.1 (c (n "rash_derive") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xxzvrwm8w50gsig7mrgj59z5752ap5yvbi7m849kssrqxhxn85g") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.7.0 (c (n "rash_derive") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a9b04464zang5m1v9md94fgd2qgzjk67cyrziagwgqj43gibsv8") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.7.1 (c (n "rash_derive") (v "1.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10spkj7g16v3vf0y3glzrbhw0jv933wgw6by9mphljcr4ywbc9zg") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.8.0 (c (n "rash_derive") (v "1.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzlgszggz81l7d9p0qkc3f9rb4pvjf78bhvqzl5pprvip2mivl3") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.8.1 (c (n "rash_derive") (v "1.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzbas6cy0nk3ns7ry03y6rmisa2lpzh8s9hmlr8ql6dhkcwvryg") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.8.2 (c (n "rash_derive") (v "1.8.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fq2d7zgi609k07mnwpcci6j4mhd0g12wdks69ighlvf6a83i1i8") (f (quote (("docs" "schemars"))))))

(define-public crate-rash_derive-1.8.5 (c (n "rash_derive") (v "1.8.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0422wj502flz0pqa0p59yqpysilfmv0fz7wzicc0kg4m70h6148h") (f (quote (("docs" "schemars")))) (r "1.66")))

(define-public crate-rash_derive-1.8.6 (c (n "rash_derive") (v "1.8.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hn88h5qvqr0jb8fxwaasxjbqqh3zjha2d1gaxmxdwfbfnjpn1zr") (f (quote (("docs" "schemars")))) (r "1.67")))

(define-public crate-rash_derive-1.9.0 (c (n "rash_derive") (v "1.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4cai99ssnm3hfxfw3chp6d7cp481j9r9q2wy659n29inx963jk") (f (quote (("docs" "schemars")))) (r "1.72")))

(define-public crate-rash_derive-1.10.0 (c (n "rash_derive") (v "1.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a4cy3n0sdzr3xhswv1l05pjjqdaxdylqn85fl2aajjjrmvvahga") (f (quote (("docs" "schemars")))) (r "1.72")))

(define-public crate-rash_derive-1.10.1 (c (n "rash_derive") (v "1.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0057nnz3cag6qx6rigi5x0vclj6gyqfribqcgsdd32d6n7s25kia") (f (quote (("docs" "schemars")))) (r "1.74")))

