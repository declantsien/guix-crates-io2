(define-module (crates-io ra yf rayfork-sys) #:use-module (crates-io))

(define-public crate-rayfork-sys-0.0.1 (c (n "rayfork-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fsii0k4wgpglxq2bmfzz3y6saz7f4avqlz2hfadscf3si5bj4di")))

