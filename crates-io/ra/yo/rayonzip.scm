(define-module (crates-io ra yo rayonzip) #:use-module (crates-io))

(define-public crate-rayonzip-0.1.0 (c (n "rayonzip") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "0dycv9i6xbqk05sqks4sdr70j6iv3cxb62364pw42wba9ix14yv3")))

(define-public crate-rayonzip-0.2.0 (c (n "rayonzip") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "0ia9sdxl2w45x2my65lgsv9zn9r8q0dg9jxfgw033f7zpml3nj82") (y #t)))

(define-public crate-rayonzip-0.2.1 (c (n "rayonzip") (v "0.2.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "031ijm324b7iclzny1b7wss4jsdcs79bl5kxc17szp8pik8c2ra9")))

