(define-module (crates-io ra yo rayon-join-macro) #:use-module (crates-io))

(define-public crate-rayon-join-macro-0.1.0 (c (n "rayon-join-macro") (v "0.1.0") (d (list (d (n "rayon") (r "^1.2") (d #t) (k 0)))) (h "0f5fm18qxl2m55zw3vr78vwdsx4ni36z35cgnk42xi7ckj1sy61n")))

(define-public crate-rayon-join-macro-0.1.1 (c (n "rayon-join-macro") (v "0.1.1") (d (list (d (n "rayon") (r "^1.2") (d #t) (k 0)))) (h "18arrdkzm4w89gjvw27ply7a1y9cf13dcjjala3dqgfmvifi8kfn")))

