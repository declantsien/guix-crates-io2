(define-module (crates-io ra yo rayon-scan) #:use-module (crates-io))

(define-public crate-rayon-scan-0.1.0 (c (n "rayon-scan") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0w6b8w0rx337sw1djny2n7vcsgyk30ayi61iaapzki8nkhibln5d")))

(define-public crate-rayon-scan-0.1.1 (c (n "rayon-scan") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "057sg3q0sb0bj84ak01dcw9ipik0ay48kj7zl06ln2qll08wr1rz") (f (quote (("default") ("bench"))))))

