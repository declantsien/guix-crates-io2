(define-module (crates-io ra yo rayon_iter_concurrent_limit) #:use-module (crates-io))

(define-public crate-rayon_iter_concurrent_limit-0.1.0-alpha (c (n "rayon_iter_concurrent_limit") (v "0.1.0-alpha") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "12ygy66zwyhhqb1iiw0jalanzj93pfgqfnsdy856a41fiyzasw1l") (f (quote (("ci")))) (r "1.63")))

(define-public crate-rayon_iter_concurrent_limit-0.1.0-alpha2 (c (n "rayon_iter_concurrent_limit") (v "0.1.0-alpha2") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1akllqajx8cfm6p62440vmnlh8l6bz4aynnj7m83l9s4ishgbh19") (f (quote (("ci")))) (r "1.63")))

(define-public crate-rayon_iter_concurrent_limit-0.1.0-alpha3 (c (n "rayon_iter_concurrent_limit") (v "0.1.0-alpha3") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "0mzcgyxvylkm2zj0j5wcvc72ngjfxpb92pkjdcngz2qh93ar8q6w") (f (quote (("ci")))) (r "1.63")))

(define-public crate-rayon_iter_concurrent_limit-0.1.0-alpha4 (c (n "rayon_iter_concurrent_limit") (v "0.1.0-alpha4") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1i03qav6ix5sz2skaxjam7q069lg4073fs3qmxl9vb1wnzqj5lwy") (f (quote (("ci")))) (r "1.63")))

(define-public crate-rayon_iter_concurrent_limit-0.1.0 (c (n "rayon_iter_concurrent_limit") (v "0.1.0") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "02yj9i4mg1firy6jd55jia2kig2djw8sginqcpc7d44c1r4fasfj") (f (quote (("ci")))) (r "1.63")))

(define-public crate-rayon_iter_concurrent_limit-0.2.0 (c (n "rayon_iter_concurrent_limit") (v "0.2.0") (d (list (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "1mzyafalvl7n9b7jj2mnn339kq59w35kfk717h3zl1yy4c8f17nh") (f (quote (("ci")))) (r "1.63")))

