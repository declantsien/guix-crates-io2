(define-module (crates-io ra yo rayoff) #:use-module (crates-io))

(define-public crate-rayoff-0.0.1 (c (n "rayoff") (v "0.0.1") (h "02if9i1c96bf8njw1mi2r4qfwgmypmf2zcyild4arpi429gk0d5d")))

(define-public crate-rayoff-0.0.2 (c (n "rayoff") (v "0.0.2") (h "1n8mxfqvqwf86yy3d4gjklhd5c9f1akss2mbvyq57dy0c71had65")))

