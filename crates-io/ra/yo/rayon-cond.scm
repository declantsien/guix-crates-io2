(define-module (crates-io ra yo rayon-cond) #:use-module (crates-io))

(define-public crate-rayon-cond-0.1.0 (c (n "rayon-cond") (v "0.1.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1dvwbqxrsqn67amvbdxyji697zg3n50fz2d777mfarch5hv5j4px")))

(define-public crate-rayon-cond-0.2.0 (c (n "rayon-cond") (v "0.2.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1y760kgszyhww90417vk3pnr09inxg02k1ksr1md5rhpaf6a5hls")))

(define-public crate-rayon-cond-0.3.0 (c (n "rayon-cond") (v "0.3.0") (d (list (d (n "either") (r "^1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1ybxppq84p3q60h9rng9j3dm79f6970hn4wljyf31lpgan5m77q5") (r "1.59.0")))

