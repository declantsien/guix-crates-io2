(define-module (crates-io ra yo rayon-macro-hack) #:use-module (crates-io))

(define-public crate-rayon-macro-hack-0.1.0 (c (n "rayon-macro-hack") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "19f1ji2bgqbjd1z8zmgdkb8qwwnvzbm293w964wbsmqykm5yrpi1")))

