(define-module (crates-io ra yo rayon-wasm) #:use-module (crates-io))

(define-public crate-rayon-wasm-1.6.1 (c (n "rayon-wasm") (v "1.6.1") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "rayon-core") (r "^1.10.0") (d #t) (k 0)))) (h "064k6fqpkg02bfdr53jjhrk8n0y1pc6qld47kigljxp95bx927nb") (y #t) (r "1.56")))

(define-public crate-rayon-wasm-1.6.2 (c (n "rayon-wasm") (v "1.6.2") (d (list (d (n "either") (r "^1.0") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "rayon-core") (r "^1.10.0") (d #t) (k 0) (p "rayon-core-wasm")))) (h "0hsa7hhrhs7pyv6ml4m1q2676pxlhgk1yi4jmhim3xl6c5hsgydy") (r "1.56")))

