(define-module (crates-io ra yo rayon-par-bridge) #:use-module (crates-io))

(define-public crate-rayon-par-bridge-0.1.0 (c (n "rayon-par-bridge") (v "0.1.0") (d (list (d (n "rayon") (r "^1.10.0") (k 0)))) (h "1s0xk7qnh8wlx4ns7wmixz9rqqvylbbvnk7yn2kaqd2qyvc18snb") (f (quote (("web_spin_lock" "rayon/web_spin_lock") ("default"))))))

