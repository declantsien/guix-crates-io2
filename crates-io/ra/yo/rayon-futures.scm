(define-module (crates-io ra yo rayon-futures) #:use-module (crates-io))

(define-public crate-rayon-futures-0.0.1 (c (n "rayon-futures") (v "0.0.1") (h "0q6hwijs6irpvv8c83yr8sbf335v9wcg4xlk9bjgql8j02g3ainc")))

(define-public crate-rayon-futures-0.1.0 (c (n "rayon-futures") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "rayon-core") (r "^1.3") (d #t) (k 0)))) (h "0jkwm1j28mg70p0mzqhjkijpakd1hjhdr6vydi9qihrk43jdlw7a")))

(define-public crate-rayon-futures-0.1.1 (c (n "rayon-futures") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "rayon-core") (r "~1.6") (d #t) (k 0)))) (h "0744xk4qbq1fh03v7fnf1ib2d94clbyhbww5axzqq01akb65b0i8")))

