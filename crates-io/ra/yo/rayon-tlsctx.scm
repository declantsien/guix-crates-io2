(define-module (crates-io ra yo rayon-tlsctx) #:use-module (crates-io))

(define-public crate-rayon-tlsctx-0.1.0 (c (n "rayon-tlsctx") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1lnmfl8705v5axl3s98dw6s53icggpn6zbyvm82shbhn8lgmppys")))

(define-public crate-rayon-tlsctx-0.1.1 (c (n "rayon-tlsctx") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "14inmqx94akwj8xlc19npa2vdr86rjv9j2r2r2hmjxqsw93s5cy1")))

(define-public crate-rayon-tlsctx-0.1.2 (c (n "rayon-tlsctx") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1kb86ylybqp4jzk3nvf1va5cav7i67xdd86s1xdr66vhzma9vqxf")))

(define-public crate-rayon-tlsctx-0.2.0 (c (n "rayon-tlsctx") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1ggz27n5qfhly552sxbylpdxkv2i65wa94jpqcwywwynldnfp8pa")))

