(define-module (crates-io ra yo rayon-core-wasm) #:use-module (crates-io))

(define-public crate-rayon-core-wasm-1.10.2 (c (n "rayon-core-wasm") (v "1.10.2") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 2)) (d (n "num_cpus") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)) (d (n "scoped-tls") (r "^1.0") (d #t) (k 2)))) (h "0wfq16xpj78kyyswv8yyq5nbfgzq3p6rmph4ba94lnay8r7wqq0p") (l "rayon-core") (r "1.56")))

