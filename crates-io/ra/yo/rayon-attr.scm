(define-module (crates-io ra yo rayon-attr) #:use-module (crates-io))

(define-public crate-rayon-attr-0.1.0 (c (n "rayon-attr") (v "0.1.0") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h89q9jj0b14f42fax5mhadim1zvkkxsidsps8nzg6vwi8j29xdi")))

(define-public crate-rayon-attr-0.1.1 (c (n "rayon-attr") (v "0.1.1") (d (list (d (n "quote") (r "^0.5.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "037irr00yjgk7wyazb3s0jvhb0ny57h612ygl9fpijh8s5fk09kl")))

