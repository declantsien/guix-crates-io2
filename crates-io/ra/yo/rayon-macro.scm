(define-module (crates-io ra yo rayon-macro) #:use-module (crates-io))

(define-public crate-rayon-macro-0.1.0 (c (n "rayon-macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.9") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "rayon-macro-hack") (r "= 0.1.0") (d #t) (k 0)))) (h "0gl61wrs5x8mfkzfhim5hdij5h3x9qcfpza0jwyfkfp0fhxhpmgh")))

(define-public crate-rayon-macro-0.2.0 (c (n "rayon-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1bga5qb03hpxykzk35ynxccnizgah3495rmmhg4qgn7zlykkdk40")))

