(define-module (crates-io ra yo rayon-hash) #:use-module (crates-io))

(define-public crate-rayon-hash-0.1.0 (c (n "rayon-hash") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "07092vz0qydalhc6kmc6nc2dn6s6rhrmi3qf2xz2r35ypdqi31h9")))

(define-public crate-rayon-hash-0.2.0 (c (n "rayon-hash") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "0y4215r2bpnpj9db30nglw3l378fb44wvxqcz5hbd6viwv0ap04q")))

(define-public crate-rayon-hash-0.3.0 (c (n "rayon-hash") (v "0.3.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "078wscsg5vzvnnlbfnkrvgk51b2p6gvpxzxks1f2s9qf1a5ql6y6")))

(define-public crate-rayon-hash-0.4.0 (c (n "rayon-hash") (v "0.4.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0apf7xc7qwn6ki2ii752vlzm4rmh8iy6y6hk91rlwk6nm1hsasll")))

(define-public crate-rayon-hash-0.4.1 (c (n "rayon-hash") (v "0.4.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "10j09i5s4jcnfp9hb5x61yfqbmfv4lsab1ajc7a8s6gqgrplylfz")))

(define-public crate-rayon-hash-0.5.0 (c (n "rayon-hash") (v "0.5.0") (d (list (d (n "hashbrown") (r "^0.3.0") (f (quote ("rayon"))) (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1qprl1ybzyyqgcypinhw1x0l9gv8q5l3qlb77g1s03ph2mskgm4m")))

