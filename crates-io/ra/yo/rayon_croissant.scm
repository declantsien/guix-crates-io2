(define-module (crates-io ra yo rayon_croissant) #:use-module (crates-io))

(define-public crate-rayon_croissant-0.1.0 (c (n "rayon_croissant") (v "0.1.0") (d (list (d (n "moite_moite") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1b28wv14b2b6zxbrkgygh0vb53apqb28ngix4jg517cir8fa6c36")))

(define-public crate-rayon_croissant-0.1.1 (c (n "rayon_croissant") (v "0.1.1") (d (list (d (n "moite_moite") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1yjfvhymd4dygyk8ybzypbg8js1k0d8ag0rq0qc8g81sby0mwwjv")))

(define-public crate-rayon_croissant-0.2.0 (c (n "rayon_croissant") (v "0.2.0") (d (list (d (n "moite_moite") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "09wfgryp5yn839vg63g6n62bgl0ksyqy4n4qd3n0zlab8gdayjiy")))

