(define-module (crates-io ra #{6m}# ra6m4) #:use-module (crates-io))

(define-public crate-ra6m4-0.2.0 (c (n "ra6m4") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gsn2yxws8mnim648npmjm5bxj52xxpnw35jaw13ky06hvnrrq09") (f (quote (("rt" "cortex-m-rt/device"))))))

