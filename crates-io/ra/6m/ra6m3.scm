(define-module (crates-io ra #{6m}# ra6m3) #:use-module (crates-io))

(define-public crate-ra6m3-0.2.0 (c (n "ra6m3") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0fg1kkw9hk70nqnj67ynlf5ds277vk31lw8mcbqdywx7w5vglyh6") (f (quote (("rt" "cortex-m-rt/device"))))))

