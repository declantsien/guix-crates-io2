(define-module (crates-io ra ya rayauth-session) #:use-module (crates-io))

(define-public crate-rayauth-session-0.1.0 (c (n "rayauth-session") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)))) (h "0hrpzn3x1iw0f6gx814zx1vg0dcaqry150lczm6sx6mpc9y20qm6") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

