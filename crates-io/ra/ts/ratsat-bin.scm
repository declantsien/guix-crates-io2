(define-module (crates-io ra ts ratsat-bin) #:use-module (crates-io))

(define-public crate-ratsat-bin-0.0.1 (c (n "ratsat-bin") (v "0.0.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "ratsat") (r "= 0.0.1") (d #t) (k 0)))) (h "1nm9bx8dmpl2sbvaqgwd8g88145yqx9kybgjnsy06kgnl0p7q1dq")))

