(define-module (crates-io ra is raise) #:use-module (crates-io))

(define-public crate-raise-1.0.0 (c (n "raise") (v "1.0.0") (h "197w559zzs3x77g2jqr4lv2kwkskgfkqbjzkym3f6r0q8jfbn8v4")))

(define-public crate-raise-2.0.0 (c (n "raise") (v "2.0.0") (h "12g55hjvif7jmp9w519na4b76381xhvngc0pj5ahgl0drh7dw9si")))

