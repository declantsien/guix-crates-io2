(define-module (crates-io ra is raisensu) #:use-module (crates-io))

(define-public crate-raisensu-0.1.1 (c (n "raisensu") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0msjjxblghmlq7g41nskj6k5wg2rcgp1icmii39hn2aymp6imwsb")))

(define-public crate-raisensu-0.1.2 (c (n "raisensu") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0srhh1za84k4p8y2yb48m1zb6y28dykdr9hm6gwalqyf71dl54s7")))

