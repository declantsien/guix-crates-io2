(define-module (crates-io ra ri rarity) #:use-module (crates-io))

(define-public crate-rarity-0.1.0 (c (n "rarity") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0ic7xr2xvfslpnavjrqwxg3v3fa38qxf9fwmwhb64347syvazk5b") (y #t)))

(define-public crate-rarity-0.1.1 (c (n "rarity") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1yhifh46yi3rqj0zixwv4r6v554w214sv4cdvznrs47z3z1s2hh0") (y #t)))

(define-public crate-rarity-0.1.2 (c (n "rarity") (v "0.1.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "0bimviyqzlqn4639gvbyfrjgh82rm9wz539k0ddcibscmrvdm6w5") (y #t)))

(define-public crate-rarity-0.1.3 (c (n "rarity") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "03wpr14vvr6niv17qghx402cdz6sh2g68xqwswz3xk6qkgj61rs5") (y #t)))

(define-public crate-rarity-0.1.4 (c (n "rarity") (v "0.1.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "1hdlm9w5yllf1hycnnqh2fifkh7p9av9j2gj9ddvi630vvax1scw") (y #t)))

(define-public crate-rarity-0.1.5 (c (n "rarity") (v "0.1.5") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "0nqvxfvp9kc0mb51wlkb1ciaizgzd3ndmm39hvz596jjiffxvyk9") (y #t)))

(define-public crate-rarity-0.1.6 (c (n "rarity") (v "0.1.6") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "0sgdxaaixwazf5b4fm3ccz3pbcirp87rjirdksklb1k2v404d8cb") (y #t)))

(define-public crate-rarity-0.2.0 (c (n "rarity") (v "0.2.0") (d (list (d (n "golden-oak-library") (r "^0.1.0") (d #t) (k 0)))) (h "12112zj3hd81yqlifcw5fjwn0pk0kmgmfbxmmm2rxri7q3fmpv4i") (y #t)))

(define-public crate-rarity-0.3.0 (c (n "rarity") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.17") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "terminal_color_builder") (r "^0.1.1") (d #t) (k 0)))) (h "118fz1gia7v2s1mb6rm2w9mhnsccm3kjw2n2rvxx2msn4n7ywr1c")))

