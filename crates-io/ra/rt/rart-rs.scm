(define-module (crates-io ra rt rart-rs) #:use-module (crates-io))

(define-public crate-rart-rs-0.0.0 (c (n "rart-rs") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.23") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.0") (d #t) (k 0)))) (h "1vqp9aa1pny8vwpjy0vkhbfx2vh6qh5wssgmysjvbkvkklxgwapb") (f (quote (("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.1 (c (n "rart-rs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.23") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.1") (d #t) (k 0)))) (h "12lxs2n5qhbir7aipq3fj0l8rg1l3blpzhvi9gzr28z7d4khay0g") (f (quote (("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.2 (c (n "rart-rs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.1") (d #t) (k 0)))) (h "00kwvzpx2rghd1z4iha7c1ri23srk1wpyhblfnj96pcahsgriszd") (f (quote (("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.3 (c (n "rart-rs") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.1") (d #t) (k 0)))) (h "14g5knl716jpvjn2q0gkrkzr8zrc16qy3h9z1fq5zqi2yngxj98n") (f (quote (("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.4 (c (n "rart-rs") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.2") (d #t) (k 0)))) (h "043fd3cb5v1j67jpcp8c0kgcfhh9p4m83xx1r4g7jiqpr18i2q7v") (f (quote (("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.6 (c (n "rart-rs") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.3") (d #t) (k 0)))) (h "07b4a5dw87fga1racicig2085zfr0hggm1ipmw4lwr9xvcjpcrp3") (f (quote (("zbus") ("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.7 (c (n "rart-rs") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.4") (d #t) (k 0)))) (h "005nh3xw3fxpjm30yimqy1s172my0rq9f6qjzvjgzpk3w7ykj6jj") (f (quote (("zbus") ("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

(define-public crate-rart-rs-0.0.8 (c (n "rart-rs") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "const_format") (r "^0.2.26") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 0)) (d (n "rart-macros") (r "^0.0.6") (d #t) (k 0)))) (h "1k1za496lk5456fkysr52lcn87r9cx34xvlqfaf7k077ls5n0g54") (f (quote (("zbus") ("std" "chrono/default") ("peripherals") ("alloc_rust") ("alloc_rtos"))))))

