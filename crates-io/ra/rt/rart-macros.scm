(define-module (crates-io ra rt rart-macros) #:use-module (crates-io))

(define-public crate-rart-macros-0.0.0 (c (n "rart-macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m5l285mmr39dnhszlry72qic6541zd4cxgpl13l2l1d9cgmnxaj")))

(define-public crate-rart-macros-0.0.1 (c (n "rart-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhia8c3qff76w74cx3rh950qvgaizqhh34fls39i0macdmlg67b")))

(define-public crate-rart-macros-0.0.2 (c (n "rart-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mrpflra5d5ps27z34zkl1m34dg57sbzs2n8w8n2msh2m7xyqkg6")))

(define-public crate-rart-macros-0.0.3 (c (n "rart-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0chzm1dahmld4030m337z91l6qfz89bvjsvpd8562m7yy065m2dm")))

(define-public crate-rart-macros-0.0.4 (c (n "rart-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12n2nynvw3fjrb09cf25bi99jsscaw6asdfnl6qm0c12y4ir0dgd")))

(define-public crate-rart-macros-0.0.6 (c (n "rart-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4bwwf0wf59jnjdbrkz7g6nn3811lfwzaacd9nzaphwdfj5xqid")))

