(define-module (crates-io ra ge rager) #:use-module (crates-io))

(define-public crate-rager-0.2.0 (c (n "rager") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "ransid") (r "^0.4.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0whsmz042b59lgqxnaqf8vglgkld82sgv65n1qf4xaag48ry84l0")))

(define-public crate-rager-0.2.1 (c (n "rager") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "ransid") (r "^0.4.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0gdxkassqy5ygm3zzlvki237fbzg3pxh5pp6n0p0vlp8gy346021")))

(define-public crate-rager-0.3.0 (c (n "rager") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "ransid") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "12mqi2krxpzgmyjc5cqpszdhpmppvjwj70np5f6f51dpc9yldsf0")))

(define-public crate-rager-0.3.1 (c (n "rager") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "ransid") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.8") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0l3wb49wkvr0y06wwcjfigv17hh45vvy83nwhkzw8lgz53cv7lk5")))

