(define-module (crates-io ra te ratelimit_meter) #:use-module (crates-io))

(define-public crate-ratelimit_meter-0.0.2 (c (n "ratelimit_meter") (v "0.0.2") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0plgg0b7km01d5vv9c23ryiijmgs82kmf41cspi0vznrw4iffkzd")))

(define-public crate-ratelimit_meter-0.0.3 (c (n "ratelimit_meter") (v "0.0.3") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "08946ib0md1kfnxri3j5ym4s06p82awryr170v9iy1wab0jp1h6d")))

(define-public crate-ratelimit_meter-0.1.0 (c (n "ratelimit_meter") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0kjwkwbiv1q339lmbd0wr5dg8qv2q76ijl91ikbp2mwpl00k3lrj")))

(define-public crate-ratelimit_meter-0.2.0 (c (n "ratelimit_meter") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0wl0ip7k59xm1c5sn9p93iqm9zifv02wcq45sv4ca9c0gklziaal")))

(define-public crate-ratelimit_meter-0.3.0 (c (n "ratelimit_meter") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0kx0gxxvhfvyg40qkbb5jmqx72axnz26ghxcbla7ahmnpx1jjvqb")))

(define-public crate-ratelimit_meter-0.3.1 (c (n "ratelimit_meter") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "1xbs7c2w3252pnbh40xl1dljbwwlg93abka0wm7cbl97wgj4kvn1")))

(define-public crate-ratelimit_meter-0.4.0 (c (n "ratelimit_meter") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0gg6a8xgx06pfm2348vjf9wb6is15n3k5bhz3jcxikzn8k92l0lq")))

(define-public crate-ratelimit_meter-0.4.1 (c (n "ratelimit_meter") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "044kr53fmd0g65fnm38q4vilwg7wyj84a444cqvahnbpvc5vbriq")))

(define-public crate-ratelimit_meter-0.5.0 (c (n "ratelimit_meter") (v "0.5.0") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "1zlr8g1c56bmxiv6ymhbf6l0wprdkw2p753a248ckrg66mbr9jik")))

(define-public crate-ratelimit_meter-1.0.0 (c (n "ratelimit_meter") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1lazkwzklw8fy2bc5rp24b4f1qip6l6h6wjypcmmwvsk0xvzv8xs")))

(define-public crate-ratelimit_meter-2.0.0 (c (n "ratelimit_meter") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)))) (h "11c356w9pzyy1mqq5ysik44gskk4lcnb35g00lc1pq71pjk9hwdx")))

(define-public crate-ratelimit_meter-3.0.0 (c (n "ratelimit_meter") (v "3.0.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)))) (h "0ac39vmvvl21r26pzd7lv8bwizl05jvp0rhxk33vyaxrkh0yj2qz")))

(define-public crate-ratelimit_meter-4.0.0 (c (n "ratelimit_meter") (v "4.0.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "evmap") (r "^4.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "nonzero_ext") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)))) (h "0qa947drqx8wc3vn67hsdbzpcd9psx5q8a97g2k0mbmvnw2gj55q")))

(define-public crate-ratelimit_meter-4.0.1 (c (n "ratelimit_meter") (v "4.0.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "evmap") (r "^4.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "nonzero_ext") (r "^0.1.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)))) (h "1baivqxaqkj7rcsqz5vsrnqym4walb5x2apj3nrflypvzcdypqa5")))

(define-public crate-ratelimit_meter-4.1.0 (c (n "ratelimit_meter") (v "4.1.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "evmap") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "nonzero_ext") (r "^0.1.5") (k 0)) (d (n "parking_lot") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "09bh36jxs9xlhm20v02dr3v7z6l0vrdq3xbvn468zhjcc4wn4fdn") (f (quote (("std" "parking_lot" "evmap" "nonzero_ext/std") ("no_std" "spin") ("default" "std"))))))

(define-public crate-ratelimit_meter-4.1.1 (c (n "ratelimit_meter") (v "4.1.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "evmap") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "nonzero_ext") (r "^0.1.5") (k 0)) (d (n "parking_lot") (r "^0.6.4") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1l80p0yr6z69l0as4v4y9v4p5y48q8d7l9q0jbwzhs57x7z0xy5h") (f (quote (("std" "parking_lot" "evmap" "nonzero_ext/std") ("no_std" "spin") ("default" "std"))))))

(define-public crate-ratelimit_meter-5.0.0 (c (n "ratelimit_meter") (v "5.0.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "evmap") (r "^6.0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 2)) (d (n "nonzero_ext") (r "^0.1.5") (k 0)) (d (n "parking_lot") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "093py8kbnxd52mqdi61n3yrf6x7r5vcgiga82k80ky4y6salyzd3") (f (quote (("std" "parking_lot" "evmap" "nonzero_ext/std") ("no_std" "spin") ("default" "std"))))))

