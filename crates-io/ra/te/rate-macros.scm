(define-module (crates-io ra te rate-macros) #:use-module (crates-io))

(define-public crate-rate-macros-0.1.0 (c (n "rate-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zfqnjvqjnh0ms4l0b64gq6ifs757rl9wclfi4gpkip5gwc3c3pf")))

(define-public crate-rate-macros-0.2.0 (c (n "rate-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bpahril9dph650x7bh0bkplxp0qy1shh37jv8z46ngqazffwljl") (y #t)))

(define-public crate-rate-macros-0.2.1 (c (n "rate-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12a5rgisdvc988v6ljvp99in9kl6by9fj00g1ap6rkwl9rm8r5vj")))

(define-public crate-rate-macros-0.2.2 (c (n "rate-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vd6zixbsdplj0hzqzy6vzhk9w4ssqncjnlp84jm7dz8p1bkl54p")))

(define-public crate-rate-macros-0.2.3 (c (n "rate-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ansh1x7kj6g8a1j7j104nf7xsa0ncp92jwsmzi7applkgv710pv")))

(define-public crate-rate-macros-0.3.0 (c (n "rate-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cag5gmpwi8q4zw6gd9gpv0ys6fy2hp2iws2qnz2bhr3mp78mgqd")))

