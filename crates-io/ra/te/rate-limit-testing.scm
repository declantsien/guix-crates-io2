(define-module (crates-io ra te rate-limit-testing) #:use-module (crates-io))

(define-public crate-rate-limit-testing-0.1.1 (c (n "rate-limit-testing") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ivd29hra2rx4g611hir8rj3r6dgikzzb603r38xck06b0d1ww2l") (y #t)))

(define-public crate-rate-limit-testing-0.1.2 (c (n "rate-limit-testing") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfch10gb0lis9l69ky6h4k7fqg2k3pmlwqf0sz9076pgj8qdqp0") (y #t)))

(define-public crate-rate-limit-testing-0.1.3 (c (n "rate-limit-testing") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1qbarld145792wcsld988yhgy2xa64m6q3i4q4lfc3nj97ayvz") (y #t)))

(define-public crate-rate-limit-testing-0.1.4 (c (n "rate-limit-testing") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "133ndjd6dz0q1hvqxyckhili14vz4w4i8rdfrari7fgyn3phqln6") (y #t)))

(define-public crate-rate-limit-testing-0.1.5 (c (n "rate-limit-testing") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x2lr76a9fyjvd542wsrma4hy9bzb4953crx9s415krf8ks3sc7l")))

(define-public crate-rate-limit-testing-0.1.6 (c (n "rate-limit-testing") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19nj2m07g56y23nbfgs3x66cil1ja663ywjgm5g5pxfpgwq3ixqy")))

