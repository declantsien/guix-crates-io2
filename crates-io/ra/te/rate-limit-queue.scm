(define-module (crates-io ra te rate-limit-queue) #:use-module (crates-io))

(define-public crate-rate-limit-queue-0.1.0 (c (n "rate-limit-queue") (v "0.1.0") (d (list (d (n "delegate") (r "^0.1.3") (d #t) (k 0)))) (h "1k3sv5251zjphprm3g2wp1k4q50jx2jmn6bkkqhldj1dwf44gdln")))

(define-public crate-rate-limit-queue-0.1.1 (c (n "rate-limit-queue") (v "0.1.1") (d (list (d (n "delegate") (r "^0.1.3") (d #t) (k 0)))) (h "1ybqd6j9v9bi1gl1h5lrirpkjb1yxsajk3m5x2jd8my1mp9plfnr")))

