(define-module (crates-io ra te ratel) #:use-module (crates-io))

(define-public crate-ratel-0.2.3 (c (n "ratel") (v "0.2.3") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "itoa") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0jnw3af8lbrx7gf5yg6153pqq3ab5aa9fvv604xf4vmyjn50f5yi")))

(define-public crate-ratel-0.3.0 (c (n "ratel") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6.78") (d #t) (k 0)) (d (n "itoa") (r "^0.1.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1q39xi1252gr5d3mqzpndnrv1jjy633n7pcbga60bf9ysqg0y9i7")))

(define-public crate-ratel-0.4.0 (c (n "ratel") (v "0.4.0") (h "1sfjgs4vb17x1zxfcd00jyzk3jqi7hprh5h9kmbz5q7lc5lr5qg3")))

(define-public crate-ratel-0.4.1 (c (n "ratel") (v "0.4.1") (h "06zlicfrj4lha8wdvlicn4h56zhiwdx0lylqqdsvd9b5qzys6bgy")))

(define-public crate-ratel-0.5.0 (c (n "ratel") (v "0.5.0") (h "0r6iqbgdfz1vhdiajz64x7qmj14vbglzbwdrcj0sxwgqg108k1hr")))

(define-public crate-ratel-0.5.1 (c (n "ratel") (v "0.5.1") (h "0l8272s1nmd6vns8rmd5f2g5dc2ksy4kb7s8p55482ff1lbsycw5")))

(define-public crate-ratel-0.5.2 (c (n "ratel") (v "0.5.2") (h "13d98d88ph4vl2kw5i6hz14cddk7rmx29656g3dvgh03mqbsja06")))

(define-public crate-ratel-0.5.3 (c (n "ratel") (v "0.5.3") (h "1vsrs4vshyzfnv5yigappcnnynhfqbq9mcy500b1k3q0r07972sk")))

(define-public crate-ratel-0.5.4 (c (n "ratel") (v "0.5.4") (h "0n5n742c35pslysgqfws838lgk94j734hmkkya3zs4j5zbvd0ny8")))

(define-public crate-ratel-0.5.5 (c (n "ratel") (v "0.5.5") (h "1982a7wi68qhjji3ar20516i426amvi7g5hlkxdhhhx5a3qvhm1r")))

(define-public crate-ratel-0.6.0 (c (n "ratel") (v "0.6.0") (h "1kfn5mwlhhhl5027j045rzp713m95hik5icp54lnxfgz7sijpbdw")))

(define-public crate-ratel-0.6.1 (c (n "ratel") (v "0.6.1") (h "1p41qkx03x2gw0824xjsq6mw5fj1a2ibskgwf5dw8d7agfm597vd")))

(define-public crate-ratel-0.6.2 (c (n "ratel") (v "0.6.2") (h "131j0h67xg0a2c3pdww2x5dilsha5bsj8m3lsbqsyakqc0ka6y31")))

(define-public crate-ratel-0.7.0 (c (n "ratel") (v "0.7.0") (h "186s5arypz1gbpqj1rnqsgb10n68syd0iy5z10k59qd0bzdjpaql")))

