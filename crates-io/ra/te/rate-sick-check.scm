(define-module (crates-io ra te rate-sick-check) #:use-module (crates-io))

(define-public crate-rate-sick-check-0.1.0 (c (n "rate-sick-check") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sldkixf58rzz5k7bmggi6v0xv3mvk1krnqz37i6n16nhffgk99k")))

(define-public crate-rate-sick-check-0.2.1 (c (n "rate-sick-check") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "04k567s2p6xxxsykghhhsvgnlig5v8kahai77p83afd3mqddp1ki")))

(define-public crate-rate-sick-check-0.2.2 (c (n "rate-sick-check") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0b90gw46qms4nrr6vhmzb93079rmg5yrx31fv0xy792ra6s3hz36")))

(define-public crate-rate-sick-check-0.2.3 (c (n "rate-sick-check") (v "0.2.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wl594yigzipfymh9pgzwwncq0vnxmdpdgvlw5laafq2iwafgilr")))

(define-public crate-rate-sick-check-0.3.0 (c (n "rate-sick-check") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1n8vpykp3jfinv0x1dbpf0nw1bg5hcv81jzh59x0jsm6wwd9kzqv")))

