(define-module (crates-io ra te rate) #:use-module (crates-io))

(define-public crate-rate-0.1.0 (c (n "rate") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.1.0") (d #t) (k 0)) (d (n "rate-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "1s1qf3bgl1rivs1skc393sb772ljxc88nnna6nw04kdkq1mzgdhz")))

(define-public crate-rate-0.2.1 (c (n "rate") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.1") (d #t) (k 0)) (d (n "rate-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "0lppls39z5syp79wr1g835c3q34jw3hbhj1hz1i32zm2m1zyq2jn")))

(define-public crate-rate-0.2.2 (c (n "rate") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.2") (d #t) (k 0)) (d (n "rate-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "07b3fgjcd6gb4qhcs55cdac1582n49q0zgfzwp11cql742dvi9va")))

(define-public crate-rate-0.2.3 (c (n "rate") (v "0.2.3") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.2.3") (d #t) (k 0)) (d (n "rate-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "1c87sq0l81dcl6qca2ickpgpjq72zw4abz6ywb056mjghpaaalwl")))

(define-public crate-rate-0.3.0 (c (n "rate") (v "0.3.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "rate-common") (r "^0.3.0") (d #t) (k 0)) (d (n "rate-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "1apdqmpklmla3d4mnjzid5hsfmnd5hr69z7z07m5xlln0ivz8awa")))

