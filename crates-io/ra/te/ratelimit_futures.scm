(define-module (crates-io ra te ratelimit_futures) #:use-module (crates-io))

(define-public crate-ratelimit_futures-0.0.1 (c (n "ratelimit_futures") (v "0.0.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "futures-timer") (r "^0.1.1") (d #t) (k 0)) (d (n "nonzero_ext") (r "^0.1.2") (d #t) (k 0)) (d (n "ratelimit_meter") (r "^4.0.1") (d #t) (k 0)))) (h "0a600hl4z4qlqf5wi9jfqyyi4g97vnsxlfq9xqfz5dck1lhnvdg1")))

