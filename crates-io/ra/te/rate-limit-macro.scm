(define-module (crates-io ra te rate-limit-macro) #:use-module (crates-io))

(define-public crate-rate-limit-macro-1.0.0 (c (n "rate-limit-macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ayhrxgr3b4lbzi3nm1yy1kgn3529mlp5w5igawk4qlp037h19sy") (f (quote (("integration_tests"))))))

(define-public crate-rate-limit-macro-1.0.1 (c (n "rate-limit-macro") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kf9b05x743sdmkp3isd80rm120fld6vfkdkq20viwh2ch01myg6") (f (quote (("integration_tests"))))))

(define-public crate-rate-limit-macro-1.0.2 (c (n "rate-limit-macro") (v "1.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rsmwpnwlmdvikq9nzimall34m06fqc500lb49rfkvbv4h6s6zq2") (f (quote (("integration_tests"))))))

(define-public crate-rate-limit-macro-1.0.3 (c (n "rate-limit-macro") (v "1.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qj3jrnz380lm53ihi9bn9si0fcs3nifrq0gy9c9rpad9n0psm3f") (f (quote (("integration_tests"))))))

