(define-module (crates-io ra te rate_limit) #:use-module (crates-io))

(define-public crate-rate_limit-0.1.0 (c (n "rate_limit") (v "0.1.0") (h "1d86r5ndhldy9anz0cpqx7hjxn1ynwbbj7waah6fjzhmjciz2kk9")))

(define-public crate-rate_limit-0.1.1 (c (n "rate_limit") (v "0.1.1") (h "1jhzdjp7c25ngsna79vbsg0qpv4zkid9prb732lx8ibb9w86kcr2")))

