(define-module (crates-io ra te ratelimiter-rs) #:use-module (crates-io))

(define-public crate-ratelimiter-rs-0.1.0 (c (n "ratelimiter-rs") (v "0.1.0") (h "095rb94jik14mqanah7vmyk7h27p8ilwh60hcrlkj2b89wczwzsq")))

(define-public crate-ratelimiter-rs-0.1.1 (c (n "ratelimiter-rs") (v "0.1.1") (h "0bgn8mx78diwq3f1mrm6l8lif1xaqy4vz6nqvvdg0qjkjrzzxik5")))

(define-public crate-ratelimiter-rs-0.1.2 (c (n "ratelimiter-rs") (v "0.1.2") (h "14psssykia6sbi0hliwfrczpbryr98f6azs6dqqhsgdw9q8vgxqs")))

(define-public crate-ratelimiter-rs-0.1.3 (c (n "ratelimiter-rs") (v "0.1.3") (h "0mpvqhw014a3ria3hkjylapa13inrkpl0nn1fqx1wiaiiwnjqbgp")))

(define-public crate-ratelimiter-rs-0.1.4 (c (n "ratelimiter-rs") (v "0.1.4") (h "19vh98vffpzm6jmy204rinzrjnckdkgy4gan53wp2n8pzgdbbpnk")))

(define-public crate-ratelimiter-rs-0.1.5 (c (n "ratelimiter-rs") (v "0.1.5") (h "17cszkw26l47qqhmc6xvz82jq1qr0l6qsmpchpa1vha2iz82clvy")))

