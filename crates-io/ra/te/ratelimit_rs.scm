(define-module (crates-io ra te ratelimit_rs) #:use-module (crates-io))

(define-public crate-ratelimit_rs-0.1.0 (c (n "ratelimit_rs") (v "0.1.0") (h "06byx7q8f345rrgrji2awl9np4jlvbfx3k8j3vknlkmnsmzaa3b2") (y #t)))

(define-public crate-ratelimit_rs-0.1.1 (c (n "ratelimit_rs") (v "0.1.1") (h "1c79z1l1jps0gg162gwypyifj8jh986f1x4l2nzv4qv0yw256y8z") (y #t)))

(define-public crate-ratelimit_rs-0.1.2 (c (n "ratelimit_rs") (v "0.1.2") (h "1ibgj306zjrjk59fm544yg219px8769j7w5m61rx5y0sn86jcpsg") (y #t)))

(define-public crate-ratelimit_rs-0.1.3 (c (n "ratelimit_rs") (v "0.1.3") (h "01c8dqbgy6q8sdz6ivcy45w7aqx8g8wh1f064ljsfhpw9c4wv3fb") (y #t)))

(define-public crate-ratelimit_rs-0.1.4 (c (n "ratelimit_rs") (v "0.1.4") (h "1qphyf6v3ajvk55yphb2in8dxxkzanap77k39ipq97s190ygigy5")))

