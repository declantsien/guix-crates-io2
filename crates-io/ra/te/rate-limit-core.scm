(define-module (crates-io ra te rate-limit-core) #:use-module (crates-io))

(define-public crate-rate-limit-core-0.1.0 (c (n "rate-limit-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rate-limit-macro") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kd8lak44zr7d05is50srrn2ic84ywx2sidkvxahmac6g0swjgy7")))

(define-public crate-rate-limit-core-1.0.1 (c (n "rate-limit-core") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rate-limit-macro") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j114xgc8wi32494pg828rq66mr5la57y088d7aixwcqkcphqj1d")))

(define-public crate-rate-limit-core-1.0.2 (c (n "rate-limit-core") (v "1.0.2") (d (list (d (n "rate-limit-macro") (r "^1") (d #t) (k 0)))) (h "1ayna262d6if63zk0acxxz8wmi32rj9g48651ahxsgx83la26b87")))

(define-public crate-rate-limit-core-1.0.3 (c (n "rate-limit-core") (v "1.0.3") (d (list (d (n "rate-limit-macro") (r "^1") (d #t) (k 0)))) (h "0d6dddr52vrfmpib85gpld7qays03vigxplql2c033w2ii7ghis0")))

