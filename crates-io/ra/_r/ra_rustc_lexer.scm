(define-module (crates-io ra _r ra_rustc_lexer) #:use-module (crates-io))

(define-public crate-ra_rustc_lexer-0.1.0-pre.1 (c (n "ra_rustc_lexer") (v "0.1.0-pre.1") (d (list (d (n "unicode-xid") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "07lzfhxji6q4ahrqnql0a5ilx7j3mxhslms6kin8m5r2z1r2gng8")))

(define-public crate-ra_rustc_lexer-0.1.0-pre.2 (c (n "ra_rustc_lexer") (v "0.1.0-pre.2") (d (list (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "1fwpbl6lzgrxdm9v52a291mlp1qg26gqzg50z3bsvpvl2nlwvb3b")))

(define-public crate-ra_rustc_lexer-0.1.0-pre.3 (c (n "ra_rustc_lexer") (v "0.1.0-pre.3") (d (list (d (n "unicode-xid") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0jisz9a8qg9ylgs9n9j6s8drxkq9xc1va4kxbk9zh3w2h7s1ldq4")))

