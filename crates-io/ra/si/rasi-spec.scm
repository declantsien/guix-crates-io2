(define-module (crates-io ra si rasi-spec) #:use-module (crates-io))

(define-public crate-rasi-spec-0.1.4 (c (n "rasi-spec") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "0xbdw75ziqjrsvi10s4mpaczshz3if6lyp50qyaz23k00d025nyb")))

(define-public crate-rasi-spec-0.1.5 (c (n "rasi-spec") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "1hxj9ic1rh9mcifvbby169cl8ywjm1klszy82h7lvawcpibdn2z7")))

(define-public crate-rasi-spec-0.1.6 (c (n "rasi-spec") (v "0.1.6") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "012rm3lkzw797kpi3lkwmadniy6bp33740hwlmd8chzphw1y5867")))

(define-public crate-rasi-spec-0.1.8 (c (n "rasi-spec") (v "0.1.8") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (o #t) (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "12xyag99hzm83v8nxjpkmfwjma0hqib2n38731j2d949h7hfan8q")))

(define-public crate-rasi-spec-0.1.9 (c (n "rasi-spec") (v "0.1.9") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (o #t) (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "1rqwjgqidlyydnsmd8a7k0g7zrmch6g632yyxy1vq99dhy4jidhy")))

(define-public crate-rasi-spec-0.1.10 (c (n "rasi-spec") (v "0.1.10") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (o #t) (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor"))) (k 0)))) (h "1s1y69jyqjmmas1cs279jnq77869v70v8hdrqhq93md3hrnvyn3q")))

(define-public crate-rasi-spec-0.1.11 (c (n "rasi-spec") (v "0.1.11") (d (list (d (n "futures") (r "^0.3") (f (quote ("std" "executor" "thread-pool"))) (k 0)) (d (n "futures-test") (r "^0.3.30") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("getrandom"))) (o #t) (d #t) (k 0)) (d (n "rasi") (r "^0.1") (d #t) (k 0)) (d (n "rasi-syscall") (r "^0.1") (f (quote ("net" "time" "fs" "executor" "unix_socket"))) (k 0)))) (h "1yyl1cagkp2h3c84070ligikb1nb8mynp78c44s1ac80abxc1215")))

