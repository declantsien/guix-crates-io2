(define-module (crates-io ra si rasi-syscall) #:use-module (crates-io))

(define-public crate-rasi-syscall-0.1.0 (c (n "rasi-syscall") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "189lwp3lvd1gf6b36jx6ynygka59kbs3nxkc7pr6b2k5a82yifsn")))

(define-public crate-rasi-syscall-0.1.1 (c (n "rasi-syscall") (v "0.1.1") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "029dkcfifxlc845v4cvaz97qannsyzwym83q11s3ximqs8jqd58w") (f (quote (("timer") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.2 (c (n "rasi-syscall") (v "0.1.2") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1g4gdik3smrcxvqa8yfivg7znyqzqgv7v7ifrlk186gsk9k6km0w") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.3 (c (n "rasi-syscall") (v "0.1.3") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1vsd01kijzzjk2kjz22gzk6d26lj08zmc7n5afld4aj0f35dqfj4") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.4 (c (n "rasi-syscall") (v "0.1.4") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1ifi574hiqaknqpy6k19lq6ra5x7lwbihd8qvk82fccg3bpwj5ca") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.5 (c (n "rasi-syscall") (v "0.1.5") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1sq6a3fkz6ka97c88f61d7xykjv1jgs4xpr22hnfiagvsxmwyna2") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.6 (c (n "rasi-syscall") (v "0.1.6") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "11sfwllbw076kz8p5avjg8ml4nrjdbqj8c9s11s3rrc0jyf0vy8i") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.8 (c (n "rasi-syscall") (v "0.1.8") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "0qvx9ljznhdpm21by9qr6cgdqx6f6jdffzbb5y6yfpbfn2ww9xav") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.9 (c (n "rasi-syscall") (v "0.1.9") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1c4l8ralx8i7bfxd8gq6mvpj8vajjmpiv935jpp1mdfhxbqwslz0") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.10 (c (n "rasi-syscall") (v "0.1.10") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "1khkxi9vy8bdrwma0i4cnizbazj1r51b0s83m458xln7zkx8imha") (f (quote (("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

(define-public crate-rasi-syscall-0.1.11 (c (n "rasi-syscall") (v "0.1.11") (d (list (d (n "bitmask-enum") (r "^2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (k 0)))) (h "0wazydah9bnj51f3zl4gavjms4ynbvfqv82wyknfq4f3w75n8lmy") (f (quote (("windows_named_pipe") ("unix_socket") ("time") ("std" "futures/std" "futures/executor") ("net") ("fs") ("executor") ("default" "alloc") ("alloc" "futures/alloc" "futures/executor"))))))

