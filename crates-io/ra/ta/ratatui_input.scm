(define-module (crates-io ra ta ratatui_input) #:use-module (crates-io))

(define-public crate-ratatui_input-0.1.0 (c (n "ratatui_input") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^5.3.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "1jgp00yrn38js40j1mbc6w3x7c094hwkga3s0x29rlra7mqs03r7") (r "1.76.0")))

(define-public crate-ratatui_input-0.1.1 (c (n "ratatui_input") (v "0.1.1") (d (list (d (n "clipboard-win") (r "^5.3.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "00pfd6sq22cx6rm6qsc1mzajpdq4gi7hfjvpjxaxzkfmifx0jb7a") (r "1.76.0")))

(define-public crate-ratatui_input-0.1.2 (c (n "ratatui_input") (v "0.1.2") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "031d8jmxqxva6lbpcb1pcjwxha9r85g6xk04spk76703f94bqxdv") (r "1.76.0")))

(define-public crate-ratatui_input-0.1.3 (c (n "ratatui_input") (v "0.1.3") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "0awiwfcvsw3rcvnzf0g2331vs4iw7063js9pli6igkw9601k92hf") (r "1.76.0")))

