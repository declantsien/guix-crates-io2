(define-module (crates-io ra ta ratatat) #:use-module (crates-io))

(define-public crate-ratatat-0.1.0 (c (n "ratatat") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "elsa") (r "^1.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (f (quote ("ron" "glob"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)))) (h "1b6v6gp7c1jham682rpvyck6grf506cqasydz6bshkjwh8b9dmb8") (f (quote (("traces"))))))

(define-public crate-ratatat-0.1.1 (c (n "ratatat") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.0") (d #t) (k 0)) (d (n "bstr") (r "^0.2.16") (d #t) (k 0)) (d (n "elsa") (r "^1.4.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (f (quote ("serde-1"))) (d #t) (k 2)) (d (n "insta") (r "^1.7.1") (f (quote ("ron" "glob"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.126") (d #t) (k 2)))) (h "13dsfcbcz36v4pj28yprf7k5nxhq3n37anym4i2rxnsr5sp2vmjm") (f (quote (("traces"))))))

