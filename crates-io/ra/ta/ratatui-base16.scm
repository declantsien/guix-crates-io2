(define-module (crates-io ra ta ratatui-base16) #:use-module (crates-io))

(define-public crate-ratatui-base16-0.1.0 (c (n "ratatui-base16") (v "0.1.0") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0wnjfwkdiy36ndcichsj2mv43dyq6z3hv2nah6x11afdfn6pdbar")))

(define-public crate-ratatui-base16-0.1.1 (c (n "ratatui-base16") (v "0.1.1") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "077yx25b04fxd1kh175vjra5d0y3gsl6si5gbbdsl447nz0i5296")))

(define-public crate-ratatui-base16-0.1.2 (c (n "ratatui-base16") (v "0.1.2") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1ardk79yqjn8jlwa6s1w3vrxkgx9lycknqlaz29mi2nl401n0ml4")))

(define-public crate-ratatui-base16-0.2.0 (c (n "ratatui-base16") (v "0.2.0") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1ixr8ql4ghb25f7arkjk6bcjxmb20yawp1yfrx53r1bhyxx1dp57")))

(define-public crate-ratatui-base16-0.3.0 (c (n "ratatui-base16") (v "0.3.0") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "03wdys2kp4m8fb7w1d78m5fl0nxh7ipgqw9kj4g3xqfx6nh0ckvc")))

(define-public crate-ratatui-base16-0.3.1 (c (n "ratatui-base16") (v "0.3.1") (d (list (d (n "figment") (r "^0.10.18") (f (quote ("env" "toml" "yaml"))) (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_with") (r "^3.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1jdc7iybz8dz2fmsfxlyq2dd1bpqlxqr9h85pna8shccy72r6a8h")))

