(define-module (crates-io ra ta ratatui-statusbar) #:use-module (crates-io))

(define-public crate-ratatui-statusbar-0.1.0 (c (n "ratatui-statusbar") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.2") (d #t) (k 0)))) (h "1cwy1mnda1j6gvkl7k5ay4d31b7gdki8a9ygq0rq9hzhzsg62ri7")))

(define-public crate-ratatui-statusbar-0.2.0 (c (n "ratatui-statusbar") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("unstable-widget-ref"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0faz3hxhki8g3asipxfrxq37xazkk3m18ns92had0kg2vp4b0bm2")))

(define-public crate-ratatui-statusbar-0.2.1 (c (n "ratatui-statusbar") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.6.3") (d #t) (k 2)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.2") (f (quote ("unstable-widget-ref"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "18gxha39dfay88babx7yarzzmkg8s7wbca3l5v1cxabg40hmbcrj")))

