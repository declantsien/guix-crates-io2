(define-module (crates-io ra ta ratatosk) #:use-module (crates-io))

(define-public crate-ratatosk-0.1.0 (c (n "ratatosk") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "unleash-types") (r "^0.10.5") (d #t) (k 0)) (d (n "unleash-yggdrasil") (r "^0.7.0") (d #t) (k 0)))) (h "198cd2sl45l30sxv0kbz3r0wb14grvssby35nb4xa1riynvyk37g")))

