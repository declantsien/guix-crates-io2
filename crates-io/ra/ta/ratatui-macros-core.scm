(define-module (crates-io ra ta ratatui-macros-core) #:use-module (crates-io))

(define-public crate-ratatui-macros-core-0.1.0 (c (n "ratatui-macros-core") (v "0.1.0") (d (list (d (n "colored-diff") (r "^0.2.3") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "13lbs65zdhmf892jfpsrxisyi4dk99bcnilqgi6a7pwdn0hm3l61")))

