(define-module (crates-io ra ta ratata) #:use-module (crates-io))

(define-public crate-ratata-0.1.0 (c (n "ratata") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "11fd3x85marlvvvc11ax34qb04z88v42h4vcw9zqzmyi3fpzmljc") (f (quote (("paste" "crossterm/bracketed-paste") ("default" "paste"))))))

(define-public crate-ratata-0.1.1 (c (n "ratata") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "02sgiybbi70xfs804aq0y1g97qy2dbwvvvcjcvqniwv9liaszzsl") (f (quote (("paste" "crossterm/bracketed-paste") ("default" "paste"))))))

