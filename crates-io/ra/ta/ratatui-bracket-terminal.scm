(define-module (crates-io ra ta ratatui-bracket-terminal) #:use-module (crates-io))

(define-public crate-ratatui-bracket-terminal-0.1.0 (c (n "ratatui-bracket-terminal") (v "0.1.0") (d (list (d (n "bracket-terminal") (r "^0.8") (d #t) (k 0)) (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "1k1bxnlkmrjp8apcabcn9yaq9s2jl2yq87jrxpmnjlw1mxf4s9b6") (y #t)))

(define-public crate-ratatui-bracket-terminal-0.1.1 (c (n "ratatui-bracket-terminal") (v "0.1.1") (d (list (d (n "bracket-terminal") (r "^0.8") (d #t) (k 0)) (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (d #t) (k 0)))) (h "14iasbxp2jii8i9p7a7pqnqh3zhm6lgqya13l18knarx211m2s4r")))

(define-public crate-ratatui-bracket-terminal-0.1.2 (c (n "ratatui-bracket-terminal") (v "0.1.2") (d (list (d (n "bracket-terminal") (r "^0.8") (d #t) (k 0)) (d (n "object-pool") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (k 0)))) (h "1i4b1rmwcz79bn5mf52zdrvgqwb625p0dfcjgssw9zlpwsbb5c9j")))

