(define-module (crates-io ra ta ratatui-macros-derive) #:use-module (crates-io))

(define-public crate-ratatui-macros-derive-0.1.0 (c (n "ratatui-macros-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "ratatui-macros-core") (r "^0.1.0") (d #t) (k 0)))) (h "017gibgvkn96hbvzjwjzf6h2y86dkpls766wxrqk0glbj3lm60vw")))

