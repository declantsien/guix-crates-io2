(define-module (crates-io ra ta ratatui-template) #:use-module (crates-io))

(define-public crate-ratatui-template-0.1.1 (c (n "ratatui-template") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "1kkxs2mcb7lzkifnzvw5w41qndwcr2klh8ls34vqdd0v1if1xbd2") (r "1.73")))

