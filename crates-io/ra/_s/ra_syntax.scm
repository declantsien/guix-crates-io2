(define-module (crates-io ra _s ra_syntax) #:use-module (crates-io))

(define-public crate-ra_syntax-0.1.0 (c (n "ra_syntax") (v "0.1.0") (d (list (d (n "drop_bomb") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.0") (d #t) (k 0)) (d (n "rowan") (r "^0.1.1") (d #t) (k 0)) (d (n "text_unit") (r "^0.1.5") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.0") (d #t) (k 2)))) (h "1sbc4xjfcr35la7n60gj77248352nr12fjsahyy1agv20wcqwc5y")))

