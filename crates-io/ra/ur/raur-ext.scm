(define-module (crates-io ra ur raur-ext) #:use-module (crates-io))

(define-public crate-raur-ext-0.1.0 (c (n "raur-ext") (v "0.1.0") (d (list (d (n "raur") (r "^1.0.2") (d #t) (k 0)))) (h "0rsv82bacdq2jns4c76rshmknrhgbk8x18i5a173pxwkf6gf8n72")))

(define-public crate-raur-ext-0.2.0 (c (n "raur-ext") (v "0.2.0") (d (list (d (n "raur") (r "^1.0.2") (d #t) (k 0)))) (h "1yqgwq3j6r3jr9vl629dcgmxc4j5nwyqpnflbgwrdq8jpjpnzzdf")))

(define-public crate-raur-ext-0.2.1 (c (n "raur-ext") (v "0.2.1") (d (list (d (n "raur") (r "^1.0.2") (d #t) (k 0)))) (h "0za00jnikr7jpj35qrjah2sx4db24y6hgdxffrz79ggwpy0sh62n")))

(define-public crate-raur-ext-1.0.0 (c (n "raur-ext") (v "1.0.0") (d (list (d (n "raur") (r "^2.0.0") (d #t) (k 0)))) (h "0hl2pcxi5rxi7c59hlar1k20629aww977w0yy49lymkpp98qd7wq")))

(define-public crate-raur-ext-1.0.1 (c (n "raur-ext") (v "1.0.1") (d (list (d (n "raur") (r "^2.0.0") (d #t) (k 0)))) (h "1klzc8ywjazwxdkq1cq7q12fwrg5xxq3pp7f9g30kmnsq30jcwyp") (f (quote (("rustls" "raur/rustls"))))))

(define-public crate-raur-ext-1.1.0 (c (n "raur-ext") (v "1.1.0") (d (list (d (n "raur") (r "^2.0.0") (d #t) (k 0)))) (h "0mq137li7a1i9m1zk6r3ykxhyd2f7x994w9w9dmawfq9pi3hlv0c") (f (quote (("rustls" "raur/rustls"))))))

(define-public crate-raur-ext-1.1.1 (c (n "raur-ext") (v "1.1.1") (d (list (d (n "raur") (r "^2.0.0") (d #t) (k 0)))) (h "0fc14wsab9bjqw6jjppmkgafx91nlp84pd46cnc5kajbz3smb72y") (f (quote (("rustls" "raur/rustls"))))))

(define-public crate-raur-ext-1.1.2 (c (n "raur-ext") (v "1.1.2") (d (list (d (n "raur") (r "^2.0.0") (d #t) (k 0)))) (h "0y91j581533xl8601ih48jf93cb8nxr4zp4pz7c33767wqd284gq") (f (quote (("rustls" "raur/rustls"))))))

(define-public crate-raur-ext-1.1.3 (c (n "raur-ext") (v "1.1.3") (d (list (d (n "raur") (r "^2.0.1") (k 0)))) (h "0vjjyaq13j25l2j53jyi9crhyr476rvkjj3g5jjjqfyf5grhcy14") (f (quote (("rustls" "raur/rustls") ("default" "raur/default"))))))

(define-public crate-raur-ext-2.0.0 (c (n "raur-ext") (v "2.0.0") (d (list (d (n "raur") (r "^3.0.1") (k 0)))) (h "01b87b60djv1gc5rkj58596s6wd4snkz4src9i8nj71s2pbxqa8j") (f (quote (("rustls" "raur/rustls") ("default" "raur/default"))))))

(define-public crate-raur-ext-2.0.1 (c (n "raur-ext") (v "2.0.1") (d (list (d (n "raur") (r "^3.0.1") (k 0)))) (h "0hanaa4q61sqiazp9jd5xrrbqlsnmmpzfb6mhc5dgxy02cr0ihm5") (f (quote (("rustls" "raur/rustls") ("default" "raur/default"))))))

(define-public crate-raur-ext-2.0.2 (c (n "raur-ext") (v "2.0.2") (d (list (d (n "raur") (r "^3.0.1") (k 0)))) (h "151kpkk2vgll1f1mcm7fqq20p9433m9psryknb0nwjdb27f8rqma") (f (quote (("rustls" "raur/rustls") ("default" "raur/default"))))))

