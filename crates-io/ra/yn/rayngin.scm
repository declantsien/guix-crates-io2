(define-module (crates-io ra yn rayngin) #:use-module (crates-io))

(define-public crate-rayngin-0.3.0 (c (n "rayngin") (v "0.3.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "mixer" "unsafe_textures"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09l5ylijmi3kbgz6mszcns138drqnji2bjynbmkdjcw96q3drx4d")))

(define-public crate-rayngin-0.3.2 (c (n "rayngin") (v "0.3.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "mixer" "unsafe_textures"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1if4cr22pjfy47l3bsih68s30ivxylzsvi8pfgi09467q5s350qj")))

