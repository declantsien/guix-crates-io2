(define-module (crates-io ra l- ral-registers) #:use-module (crates-io))

(define-public crate-ral-registers-0.1.0 (c (n "ral-registers") (v "0.1.0") (d (list (d (n "stm32ral") (r "^0.5.0") (f (quote ("stm32f0x0"))) (d #t) (k 2)))) (h "00lq89w5gkq3v0dsd0la36zf0hr2fm1ycaps1yzgr1qwgm4xz4i9")))

(define-public crate-ral-registers-0.1.1 (c (n "ral-registers") (v "0.1.1") (d (list (d (n "stm32ral") (r "^0.5.0") (f (quote ("stm32f0x0"))) (d #t) (k 2)))) (h "0xnp5kq8z3bps4qvgn3cmmfyjxpi3w080kd03jh75spxfvjb3yr3")))

(define-public crate-ral-registers-0.1.2 (c (n "ral-registers") (v "0.1.2") (d (list (d (n "stm32ral") (r "^0.5.0") (f (quote ("stm32f0x0"))) (d #t) (k 2)))) (h "1242009wjg8sghz99iadnlh77np6n9hkxk8n5vhcs6y7d79h680j")))

(define-public crate-ral-registers-0.1.3 (c (n "ral-registers") (v "0.1.3") (d (list (d (n "stm32ral") (r "^0.5.0") (f (quote ("stm32f0x0"))) (d #t) (k 2)))) (h "06n8bzvw6wsh2rb8878c6ph132zaranmahn72ikv9s06jafimds6")))

