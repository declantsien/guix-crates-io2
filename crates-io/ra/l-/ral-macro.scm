(define-module (crates-io ra l- ral-macro) #:use-module (crates-io))

(define-public crate-ral-macro-0.2.0 (c (n "ral-macro") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08fh6v0cgaxjfzkmxj7fbvxp5r9aapm99ikfzvcp4849bkiv8k48")))

