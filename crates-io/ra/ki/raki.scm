(define-module (crates-io ra ki raki) #:use-module (crates-io))

(define-public crate-raki-0.1.0 (c (n "raki") (v "0.1.0") (h "0nh6m62bdqwayi2mizrwxhxvxk8lzlpvyvarqy9341nrzy8vjz1x")))

(define-public crate-raki-0.1.1 (c (n "raki") (v "0.1.1") (h "0mqpjilvp71vd4abnrpj47wmj3hv5dqwnd0x7jyxxa2ysjabmpzy")))

(define-public crate-raki-0.1.2 (c (n "raki") (v "0.1.2") (h "09advc9i6w70b6av4pd7qqyb0rsi6vqhxygzm7pd2n5lxf5xxdza")))

(define-public crate-raki-0.1.3 (c (n "raki") (v "0.1.3") (h "0ln046y1rr18q4ka9wh6lfflf4x5z3pn1vwz9cy4sn64m1h4nn20")))

