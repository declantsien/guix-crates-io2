(define-module (crates-io ra ws rawslice) #:use-module (crates-io))

(define-public crate-rawslice-0.1.0 (c (n "rawslice") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rawpointer") (r "^0.1") (d #t) (k 0)))) (h "09bympww1rpsd422da3w444q5w1znjbjh7mjninhq9gaaygkpci2")))

(define-public crate-rawslice-0.1.1 (c (n "rawslice") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.4") (k 2)) (d (n "rawpointer") (r "^0.2.1") (d #t) (k 0)))) (h "1kfidydpw770wfzp2c4y7jfq1vr5jbql5sk86xg2wx3an84cj8wf")))

