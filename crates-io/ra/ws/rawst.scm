(define-module (crates-io ra ws rawst) #:use-module (crates-io))

(define-public crate-rawst-0.1.0 (c (n "rawst") (v "0.1.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json_flex") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusoto") (r "^0.20") (f (quote ("kinesis"))) (d #t) (k 0)) (d (n "rustbox") (r "^0.9") (d #t) (k 0)))) (h "00ljf5bd3mc5cxivhn8yi2cgg661ww4wn1r7la29dimp52s6w6h8")))

