(define-module (crates-io ra ws rawstdraw) #:use-module (crates-io))

(define-public crate-rawstdraw-0.1.0 (c (n "rawstdraw") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.65") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.1") (d #t) (k 1)))) (h "08djdr6lb2jk6nnsfnpjvyy00jivgqb6ay04gx9nxgwi0j4cljs2") (l "rawdraw")))

(define-public crate-rawstdraw-0.1.1 (c (n "rawstdraw") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.65") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mut_static") (r "^5.0.0") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.1") (d #t) (k 1)))) (h "0qw91ygcpsvzac1x47gaxrrrgdmn4w856npqvnq210m7hwh9v7ck") (l "rawdraw")))

