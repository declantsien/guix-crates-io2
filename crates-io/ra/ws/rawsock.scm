(define-module (crates-io ra ws rawsock) #:use-module (crates-io))

(define-public crate-rawsock-0.1.0 (c (n "rawsock") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.7") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16pyqvjd2jj6bkgmgaqrh2d6mxmm1gpkra36akajhq9nzczv6idc")))

(define-public crate-rawsock-0.1.1 (c (n "rawsock") (v "0.1.1") (d (list (d (n "dlopen") (r "^0.1.7") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nfnksxd834dnya5pfy9v99qa0dvijp99961r6dj4psb88kjxy3p")))

(define-public crate-rawsock-0.2.0 (c (n "rawsock") (v "0.2.0") (d (list (d (n "dlopen") (r "^0.1.7") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1l6wwvjw6ixqrpv5ckv5w046wws3b0hiikncx83b2sck2x15s59m")))

(define-public crate-rawsock-0.2.1 (c (n "rawsock") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "dlopen") (r "^0.1.7") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1krxnmi2nh7l6qmzs7d2w94c2jdn4z0775z37849y32yxyglpvxz")))

(define-public crate-rawsock-0.3.0 (c (n "rawsock") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.6") (d #t) (k 2)) (d (n "dlopen") (r "^0.1.7") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rljbbv6wdj29jnrvn46yqb3znlplyzh6h984dgdi1w5jj1xngmv")))

