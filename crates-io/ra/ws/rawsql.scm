(define-module (crates-io ra ws rawsql) #:use-module (crates-io))

(define-public crate-rawsql-0.1.0 (c (n "rawsql") (v "0.1.0") (d (list (d (n "postgres") (r "^0.10") (d #t) (k 2)))) (h "0nsmnghrd7z5wnl6llqxl9yj1imvh7ax594ivxlcpgi2h1yhp9q5")))

(define-public crate-rawsql-0.1.1 (c (n "rawsql") (v "0.1.1") (d (list (d (n "postgres") (r "^0.19.2") (d #t) (k 2)))) (h "1a1gh7frsmbrgk94cncvj27md3n1bgs99x5dj16x6qak8743c2zp")))

