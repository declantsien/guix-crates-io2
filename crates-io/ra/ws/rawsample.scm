(define-module (crates-io ra ws rawsample) #:use-module (crates-io))

(define-public crate-rawsample-0.1.0 (c (n "rawsample") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1p8hz0pzydrwnakmp820nlh4x14qj3jfwhjv9ny6vdk10599lvsv")))

(define-public crate-rawsample-0.1.1 (c (n "rawsample") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0jlpxp1kzr7lsk3y4iy9iglcgmdf6yyvvcbv3q0csypcfaqrpbr7")))

(define-public crate-rawsample-0.2.0 (c (n "rawsample") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nc0af62kqi4dfszy9sjpp9pv0lzv5afjk24pfs21143iwmcddpz")))

