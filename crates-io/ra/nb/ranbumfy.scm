(define-module (crates-io ra nb ranbumfy) #:use-module (crates-io))

(define-public crate-ranbumfy-0.1.0 (c (n "ranbumfy") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockers") (r "^0.13.0") (d #t) (k 2)) (d (n "mockers_derive") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rspotify") (r "^0.2.1") (d #t) (k 0)))) (h "0fnlfz5prx4kdsz0rr23s1yk3n6d09ajgwxg55sf0ljxidbrxq9p")))

(define-public crate-ranbumfy-0.1.1 (c (n "ranbumfy") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockers") (r "^0.13.1") (d #t) (k 2)) (d (n "mockers_derive") (r "^0.13.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rspotify") (r "^0.2.1") (d #t) (k 0)))) (h "0mc7p8n56q2mx25fpm7v7fsl34sdkhcbsfwjzliz5v3rdphl3ywl")))

