(define-module (crates-io ra mn ramn-currency) #:use-module (crates-io))

(define-public crate-ramn-currency-0.4.0 (c (n "ramn-currency") (v "0.4.0") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "0gdzd16vaij2b3n8y3m0aha6shacc6ix6j083b75d5jpmh7bw853") (y #t)))

(define-public crate-ramn-currency-0.4.1 (c (n "ramn-currency") (v "0.4.1") (d (list (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "1xj432bwnm9fd2bgng4d5pcvp4nnzr31kbqjic7d7frain7p2yqs") (y #t)))

