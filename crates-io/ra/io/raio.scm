(define-module (crates-io ra io raio) #:use-module (crates-io))

(define-public crate-raio-0.1.0 (c (n "raio") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)))) (h "1j4zwmcrviv1rxva87zgd47al6y7x566r1w4gkjyjfl0nl61akld")))

(define-public crate-raio-0.2.0 (c (n "raio") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "deadpool") (r "^0.5.2") (d #t) (k 0)) (d (n "packs") (r "^0.2.0") (d #t) (k 0)) (d (n "packs") (r "^0.2.0") (d #t) (k 2)) (d (n "packs-proc") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0a4iyxkfasxx8061dkkimcxxcks05j0vrnf9yxpkhagphdhkk0l1")))

