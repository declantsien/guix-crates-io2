(define-module (crates-io ra nu ranum) #:use-module (crates-io))

(define-public crate-ranum-0.0.1 (c (n "ranum") (v "0.0.1") (h "0487p9fkwgric8yji7y7913qvb195rknxw2fhsh4p92r2nwvpji2")))

(define-public crate-ranum-0.0.2 (c (n "ranum") (v "0.0.2") (h "113b82c3x1bcahsm8drb7j3wgbsschi7j1c9q0qjzzvs25fra6vb")))

