(define-module (crates-io ra th rathe-storage-program) #:use-module (crates-io))

(define-public crate-rathe-storage-program-1.0.0 (c (n "rathe-storage-program") (v "1.0.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "solana-program") (r "^1.11.4") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.11.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.11.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0z0i9iplr09j71v4k2k4rh308crz55r90h6lhjaszqj85mdxacgs") (f (quote (("no-entrypoint"))))))

