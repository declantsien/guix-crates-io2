(define-module (crates-io ra th rathe-storage-cli) #:use-module (crates-io))

(define-public crate-rathe-storage-cli-1.0.0 (c (n "rathe-storage-cli") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-client") (r "^1.11.4") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.11.4") (d #t) (k 0)) (d (n "storage-program") (r "^1.0.0") (f (quote ("no-entrypoint"))) (d #t) (k 0) (p "rathe-storage-program")) (d (n "tokio") (r "^1.14.1") (d #t) (k 0)))) (h "1xwjh3yvrygcmfi93pjn4iv5qbqhrf8l6dqfkbsibmnglw6y1p86")))

