(define-module (crates-io ra me ramenon) #:use-module (crates-io))

(define-public crate-ramenon-0.1.0 (c (n "ramenon") (v "0.1.0") (h "0yb8gj97x5bxy0l2qs4fv7x0f5qfp0glfkjyay8pfyvcrijpbaf8") (y #t)))

(define-public crate-ramenon-0.1.1 (c (n "ramenon") (v "0.1.1") (h "0w05fn1lrvnnyp97wz1nh37x0hnziciwr1b633j8bvikdv3bixhy") (y #t)))

(define-public crate-ramenon-0.1.2 (c (n "ramenon") (v "0.1.2") (h "1gj9hzkz43wrcbbdn00745l5n3h5l7xs6nk3xz60gni0yxx9jk2l") (y #t)))

(define-public crate-ramenon-0.1.3 (c (n "ramenon") (v "0.1.3") (h "1i7c90pimhdf3kf4zd3ms7a8vsx7r0mc7vfjxww92jxz671z7vg1")))

