(define-module (crates-io ra me ramen) #:use-module (crates-io))

(define-public crate-ramen-0.0.0 (c (n "ramen") (v "0.0.0") (h "1486sqypp3996s52vjgpskrymm3mvk0prw4rnjr57f78jqb0a5hl")))

(define-public crate-ramen-0.0.1 (c (n "ramen") (v "0.0.1") (h "0915306pw3qa7x6a97alpdr7vks1l490h0647r3ml3pqc3yqqhki")))

(define-public crate-ramen-0.0.2 (c (n "ramen") (v "0.0.2") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1bj0j3vgqk3inc828ds7vpjvdddji6dbb4lkhjk0f8y7ivmgk2x3") (f (quote (("parking-lot" "parking_lot") ("default"))))))

