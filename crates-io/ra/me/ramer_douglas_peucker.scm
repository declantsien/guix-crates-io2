(define-module (crates-io ra me ramer_douglas_peucker) #:use-module (crates-io))

(define-public crate-ramer_douglas_peucker-0.1.0 (c (n "ramer_douglas_peucker") (v "0.1.0") (d (list (d (n "legasea_line") (r "^0.2.0") (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0byfim1n3sn66ad5qnj5rhh96dfiy7ihxd2af6rcnds4hnd9xci2")))

(define-public crate-ramer_douglas_peucker-0.2.0 (c (n "ramer_douglas_peucker") (v "0.2.0") (d (list (d (n "legasea_line") (r "^0.2.0") (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0d6xrkfxfca2dngl6lg224clipi61nqymdm0h7pc92gzf9xx929i")))

(define-public crate-ramer_douglas_peucker-0.2.1 (c (n "ramer_douglas_peucker") (v "0.2.1") (d (list (d (n "legasea_line") (r "^0.2.0") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19h0gzmcf9j28rslm9qc5wn9lha7hxfxikma62dpspi9xpf051ha")))

(define-public crate-ramer_douglas_peucker-0.2.2 (c (n "ramer_douglas_peucker") (v "0.2.2") (d (list (d (n "legasea_line") (r "^0.2.1") (d #t) (k 0)) (d (n "mint") (r "^0.5.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "10z7bjwsiyi94zxiii8wqnr5k6ywxccva4gfms3w4x8f6s6jmvrz")))

