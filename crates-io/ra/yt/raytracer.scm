(define-module (crates-io ra yt raytracer) #:use-module (crates-io))

(define-public crate-raytracer-0.1.0 (c (n "raytracer") (v "0.1.0") (h "1xg1vhscdzbss8hzm4ncmmm7w0apdmq2y1g5ys817lqijyy6q6qs")))

(define-public crate-raytracer-0.1.1 (c (n "raytracer") (v "0.1.1") (h "1fqify53adxml6q52y9c2r3frkmq1fg9vxdrbwfj8gr4isjmfnxl")))

(define-public crate-raytracer-0.2.0 (c (n "raytracer") (v "0.2.0") (d (list (d (n "ice-threads") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1plys9gzql06f3a06pwn4yshm6nrs1gz4frjw3a3yns2wbl9gjnx")))

(define-public crate-raytracer-0.2.1 (c (n "raytracer") (v "0.2.1") (d (list (d (n "ice-threads") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0b62qwd0ln2na3rmqjh5rmhcyqj9fmhz5v4l85xcqx3pcnyy5j6k")))

(define-public crate-raytracer-0.2.2 (c (n "raytracer") (v "0.2.2") (d (list (d (n "ice-threads") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1d40xrrg10axycxf755ivn6gcg3m1wj7zzi21wgllkbm3czskvqg")))

