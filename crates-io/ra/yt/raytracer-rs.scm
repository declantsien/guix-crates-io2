(define-module (crates-io ra yt raytracer-rs) #:use-module (crates-io))

(define-public crate-raytracer-rs-0.1.0 (c (n "raytracer-rs") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("wgpu"))) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "type-uuid") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.0") (d #t) (k 0)))) (h "1difrr2bdmv73f5pj1mb0yz59lkxn6930i0icdsqbdxp2c6qhp7j")))

(define-public crate-raytracer-rs-0.1.1 (c (n "raytracer-rs") (v "0.1.1") (d (list (d (n "eframe") (r "^0.19.0") (f (quote ("wgpu"))) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "type-uuid") (r "^0.1.2") (d #t) (k 0)) (d (n "uuid") (r "^1.2.0") (d #t) (k 0)))) (h "0clbn82ndpjqrzd09wc8bmgyywwy643wng711vw9nc5wqklvh0x4")))

(define-public crate-raytracer-rs-0.1.2 (c (n "raytracer-rs") (v "0.1.2") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05xs0vi6bm28xxkjj66skgznbqncwnffcczpy7rbn9k9h1pl5dsm") (f (quote (("wgpu" "eframe/wgpu"))))))

(define-public crate-raytracer-rs-0.1.3 (c (n "raytracer-rs") (v "0.1.3") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y86n1msfk0w5lvfywj041l0w9js3wrgl6nklk44m75g5xa9lxwk") (f (quote (("wgpu" "eframe/wgpu"))))))

