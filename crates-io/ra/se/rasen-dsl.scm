(define-module (crates-io ra se rasen-dsl) #:use-module (crates-io))

(define-public crate-rasen-dsl-0.1.0 (c (n "rasen-dsl") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 1)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 1)) (d (n "rasen") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 1)))) (h "1bnlawrdfk9rcrz0cnv7gp7l4bbmvvrfy7sr5n272di6k1rj0lv5") (f (quote (("functions") ("default"))))))

(define-public crate-rasen-dsl-0.2.0 (c (n "rasen-dsl") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 0)) (d (n "clippy") (r "^0.0.134") (o #t) (d #t) (k 1)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^0.3.15") (d #t) (k 1)) (d (n "rasen") (r "^0.12.0") (d #t) (k 0)) (d (n "rspirv") (r "^0.5.2") (d #t) (k 2)) (d (n "syn") (r "^0.11.11") (d #t) (k 1)))) (h "12haiqnah892x70ssy3knrpgda39xnz064rasghxgam3mp83fd2j") (f (quote (("functions") ("default"))))))

