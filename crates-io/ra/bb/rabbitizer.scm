(define-module (crates-io ra bb rabbitizer) #:use-module (crates-io))

(define-public crate-rabbitizer-1.5.4 (c (n "rabbitizer") (v "1.5.4") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1rvszi3i6986laqf380kd63bba27df9lsv9ax8x2ajn0chay44x3")))

(define-public crate-rabbitizer-1.5.5 (c (n "rabbitizer") (v "1.5.5") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "16g94ywkzans1krq572a6f40wibniv8kw5aw1yqq77zgk52pi8v6")))

(define-public crate-rabbitizer-1.5.6 (c (n "rabbitizer") (v "1.5.6") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0p4hjxbjkncakvq5wld51pqdacmfy5vn95ycjn3f0scwdh67v220")))

(define-public crate-rabbitizer-1.5.7 (c (n "rabbitizer") (v "1.5.7") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0q35x8qzq53h0b18qz5p2k4nsah29aa1bd6darm285gja3yrds9p")))

(define-public crate-rabbitizer-1.5.8 (c (n "rabbitizer") (v "1.5.8") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1nibnr1qinqsh9v5m1m6gn7lz9w8q0147hlxd9l4fcsxikraxi7f")))

(define-public crate-rabbitizer-1.5.10 (c (n "rabbitizer") (v "1.5.10") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "07phpc2bl2pxzz8rn1q4akpsx3l3cikmgm7f56p7psc8libbvfkx")))

(define-public crate-rabbitizer-1.5.11 (c (n "rabbitizer") (v "1.5.11") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0sxa9q7xvbv9nyzs8p5p7msjksp8by9a48d5x1fpdb64lp54a8d0")))

(define-public crate-rabbitizer-1.6.0 (c (n "rabbitizer") (v "1.6.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1pdcpwqj75q71cgklhjbdbq95wwijjmqlg1zqjmf882r60rg3p6l")))

(define-public crate-rabbitizer-1.6.1 (c (n "rabbitizer") (v "1.6.1") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1rya85hlb9qdapfn26kfdp35ka7b1k6202spy0wj9pg24mcy5dyl")))

(define-public crate-rabbitizer-1.6.2 (c (n "rabbitizer") (v "1.6.2") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0kqc6sapp1l6gbm9367frsypcn5yzvzyc9z6f63hw0h9d2plbs8l")))

(define-public crate-rabbitizer-1.7.0 (c (n "rabbitizer") (v "1.7.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1pldf430ghi21qkjfkarw20xvqzj8vmyr3i792zi71mpxcnyibfa")))

(define-public crate-rabbitizer-1.7.1 (c (n "rabbitizer") (v "1.7.1") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0s5gw9xg4qp82qgbj1yaqs0wfyqh857xyzyccvb4dkra1jgg15jj")))

(define-public crate-rabbitizer-1.7.3 (c (n "rabbitizer") (v "1.7.3") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "04x4m6vg9ckdfvg7m29x5ggwg8hp1gz1fdirny349hwjx1zws40p")))

(define-public crate-rabbitizer-1.7.4 (c (n "rabbitizer") (v "1.7.4") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0fix1rga45nqcb8bcjhfmq6h2bhgm51hr6ifs94fi7nkvja7hr9g")))

(define-public crate-rabbitizer-1.7.5 (c (n "rabbitizer") (v "1.7.5") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0rxbn13jcpv9cj5br5gccx4di3nmkc54rdwwp5mf9cmvdrv65dyc")))

(define-public crate-rabbitizer-1.7.6 (c (n "rabbitizer") (v "1.7.6") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0w4wzqd7q6b26rc3w8m9sdra6cp337b9ypg6gv043v5zcawiw780")))

(define-public crate-rabbitizer-1.7.7 (c (n "rabbitizer") (v "1.7.7") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0cc0llx30lx7h25rpx76d39fmmw6s9gc7mypw63wqqbfkjbbgdhk")))

(define-public crate-rabbitizer-1.7.8 (c (n "rabbitizer") (v "1.7.8") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0v49gdqlcs1q2bp4icnikgj4qn7fffakcqys810jrin34qfv0rh6")))

(define-public crate-rabbitizer-1.7.9 (c (n "rabbitizer") (v "1.7.9") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1k7friil9nfy4mx8i2zs0190df5xs8y6kjv4j8068azb7v92416v")))

(define-public crate-rabbitizer-1.7.10 (c (n "rabbitizer") (v "1.7.10") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1zvfpbqxdivx9niqs8fr2m1lr4sdmcjwmk74vmdjcgadbjmwr2m5")))

(define-public crate-rabbitizer-1.8.0 (c (n "rabbitizer") (v "1.8.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0ykl99rn2kh92wx52kh7653lq8drrcrvc9syibnmw8hyhymak4q7")))

(define-public crate-rabbitizer-1.8.1 (c (n "rabbitizer") (v "1.8.1") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1piskcqcahsnz4w9ibmwmy5ymrn8dga7hzlg7ic240k67nj53n2x")))

(define-public crate-rabbitizer-1.8.2 (c (n "rabbitizer") (v "1.8.2") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "15ndmk3fmqp6fsimdi07x3cjng7vzk884vg1sk4ywi4hxg0rjm07")))

(define-public crate-rabbitizer-1.8.3 (c (n "rabbitizer") (v "1.8.3") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "16j8x597qph7c0siv1licasyw6051v59nvylk7ihgwciw7pkgwh0")))

(define-public crate-rabbitizer-1.9.0 (c (n "rabbitizer") (v "1.9.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0gj05jrjjsav2xq40h0qbpzbfh51y81a1vksvl96gcmj5bqx2d6q")))

(define-public crate-rabbitizer-1.9.1 (c (n "rabbitizer") (v "1.9.1") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1yi9jpggrsxyh9lx32li3ai3gbb3iyhgrcgfbk2378q4ag26ix2i")))

(define-public crate-rabbitizer-1.9.2 (c (n "rabbitizer") (v "1.9.2") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0lbmljnhl56fgafz3g7y3cfa8n9qbxifnrkjrbi6y7a25p99fc2g")))

(define-public crate-rabbitizer-1.9.3 (c (n "rabbitizer") (v "1.9.3") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "07dy1jkb5f4vmnqrisrz4lmgxlg6111a0bbpwqnsg2w8877knwsx")))

(define-public crate-rabbitizer-1.9.4 (c (n "rabbitizer") (v "1.9.4") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "06r8kn4hzhh7kl7iw45iidgalzjglzgk4mww3fcpxarcf2528cfn")))

(define-public crate-rabbitizer-1.9.5 (c (n "rabbitizer") (v "1.9.5") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "0zbcca6swfirqrd3i2vxl4hqraia72rdpfx5dxb0wys9zx1qx103")))

(define-public crate-rabbitizer-1.10.0 (c (n "rabbitizer") (v "1.10.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "09syf94xkd41g9bw2vjc04l8g7n4f4kdjlk6bvwfvdnw2xvs0w7m")))

(define-public crate-rabbitizer-1.11.0 (c (n "rabbitizer") (v "1.11.0") (d (list (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "08gwl05amh0g1yf6pn7wpg849vmhsyqyzi3ak6x89a5892wnypih")))

