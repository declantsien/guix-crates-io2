(define-module (crates-io ra bb rabbot) #:use-module (crates-io))

(define-public crate-rabbot-0.1.0 (c (n "rabbot") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "0v16xxfy4vcii5xck5zh0a4j74bhwh4hdc8ch2fx6ic41945syk6")))

(define-public crate-rabbot-0.1.1 (c (n "rabbot") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "1p2rninfk3di31jj1202qm14lxb5kkv3ni4gw3lydnmrhykz0aq5")))

(define-public crate-rabbot-0.1.2 (c (n "rabbot") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "1mdsxjny7zgypcf6ppf6q3cj97j6i0cfz7sxbsq0g6q53pz4js2f")))

(define-public crate-rabbot-0.1.3 (c (n "rabbot") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "0l9adfv1mi67vbd97s90i3pgz1a00rqjnhmafc82daacbjn5l91d")))

(define-public crate-rabbot-0.2.0 (c (n "rabbot") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.12.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.12.0") (d #t) (k 0)))) (h "080phfw8yybagr2ky9sds9srdq6hz0cmzw5h4jw3vxzq781bidc8")))

