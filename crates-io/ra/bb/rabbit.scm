(define-module (crates-io ra bb rabbit) #:use-module (crates-io))

(define-public crate-rabbit-0.1.0 (c (n "rabbit") (v "0.1.0") (h "1f04rpzx3gfbmb745jvwvlyx9j3cyhp7an1ghnjid3qxg2r53qix") (f (quote (("nostd") ("default"))))))

(define-public crate-rabbit-0.2.0 (c (n "rabbit") (v "0.2.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "18kaggr6yfq3igdlgh87vgf5hiw9ljadzcw2aw2j4s2jsddlblik") (f (quote (("default"))))))

(define-public crate-rabbit-0.3.0 (c (n "rabbit") (v "0.3.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (o #t) (k 0)))) (h "15j3j3cwp1ip5qabsnbc6l527i74cd4333ds1mlcm40bndkl8lck") (f (quote (("default"))))))

(define-public crate-rabbit-0.3.1 (c (n "rabbit") (v "0.3.1") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "zeroize") (r "=1.3") (f (quote ("zeroize_derive"))) (o #t) (k 0)))) (h "1p06cdyjbhsb2x0pwm0kybab813yw9g7pmc6vljgaawdqgpjf2cv") (f (quote (("default"))))))

(define-public crate-rabbit-0.4.0 (c (n "rabbit") (v "0.4.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0j4kzmj9j9n7wwk18h3cfh6l8wfmib2syr3r2vk5qjg4afi5xh6y") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

(define-public crate-rabbit-0.4.1 (c (n "rabbit") (v "0.4.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "1m8538i048q2p65jgfq4s333yhcj8bqf04ipfkv61lkkip9226lk") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (r "1.56")))

