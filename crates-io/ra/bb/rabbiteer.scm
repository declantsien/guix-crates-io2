(define-module (crates-io ra bb rabbiteer) #:use-module (crates-io))

(define-public crate-rabbiteer-0.1.0 (c (n "rabbiteer") (v "0.1.0") (d (list (d (n "amqp") (r "^0.0.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)))) (h "1grg4c7yzvyg42bbasr4hgk8mzrls11w13bm84c3wcfbld6zdccj")))

(define-public crate-rabbiteer-0.1.1 (c (n "rabbiteer") (v "0.1.1") (d (list (d (n "amqp") (r "^0.0.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)))) (h "0dj3x7zc54rlb3sxiv5ngwwybjsznichrzhhij0rl3ihlmq4kw8n")))

(define-public crate-rabbiteer-0.1.2 (c (n "rabbiteer") (v "0.1.2") (d (list (d (n "amqp") (r "^0.0.19") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)))) (h "09a3vj435c1ng4w66yzraf3jqp31qrh7bf5awylw9bh9k4a64qmk")))

(define-public crate-rabbiteer-0.2.0 (c (n "rabbiteer") (v "0.2.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2.3") (d #t) (k 0)))) (h "0mywfkq38ligf65l34mgazqqagvhsbnyjdsx869la0xjzfaxw1lc")))

(define-public crate-rabbiteer-1.0.0 (c (n "rabbiteer") (v "1.0.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "02yq4cj1gih4zb0cvb5qk91mhckrbdqmwsr91lblv84dqjc1ggzm")))

(define-public crate-rabbiteer-1.1.0 (c (n "rabbiteer") (v "1.1.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1picdbb01d8i636zlpjmv84gw8ayd9pxwk20jlapwwbs8023ddrh")))

(define-public crate-rabbiteer-1.1.1 (c (n "rabbiteer") (v "1.1.1") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0iz6vai0yfx27rh1gnig31prg9dm9zfyw3csjw524d2fbqss7af7")))

(define-public crate-rabbiteer-1.2.0 (c (n "rabbiteer") (v "1.2.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1dqa3z4psxvg9gm85bjmigv71nglkgj65176ddjpcl94r2agaicz")))

(define-public crate-rabbiteer-1.2.1 (c (n "rabbiteer") (v "1.2.1") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "19f46i54r6xbp842fpbn9rw0z6zprf0a4w4w43q7xxsmz625r9jy")))

(define-public crate-rabbiteer-1.2.2 (c (n "rabbiteer") (v "1.2.2") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0sbji151n984mxfanrb8x28qhjn812b4q3g00j980aavgdqgq3gl")))

(define-public crate-rabbiteer-1.3.0 (c (n "rabbiteer") (v "1.3.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0svhfmkhrj5zz1mw9b5sfgxrcl2gbkd9mr0f5091ih6p6x2y0sh7")))

(define-public crate-rabbiteer-1.3.1 (c (n "rabbiteer") (v "1.3.1") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1wixgn47222az0sz2sngcxd7in9c0nclj49p45qfvrwlwr5kdlc1")))

(define-public crate-rabbiteer-1.4.0 (c (n "rabbiteer") (v "1.4.0") (d (list (d (n "amqp") (r "^0.0.20") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1lgma811l9i6mxi1359vl3fcm2zchy06hwpf4i4k32yv5ka045vc")))

(define-public crate-rabbiteer-1.4.1 (c (n "rabbiteer") (v "1.4.1") (d (list (d (n "amqp") (r "^0.1") (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "conduit-mime-types") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "12zx9sj16ciwpjfx9ghbh2q4di3px5vl2ybg679alvlgwjipmx05")))

