(define-module (crates-io ra wt rawtx-rs) #:use-module (crates-io))

(define-public crate-rawtx-rs-0.1.0 (c (n "rawtx-rs") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.26") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "1x223hlr3njsah759n6kmd184r37bzgz16vcvrpz0il7i10gfghi")))

(define-public crate-rawtx-rs-0.1.1 (c (n "rawtx-rs") (v "0.1.1") (d (list (d (n "bitcoin") (r "^0.29") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "1gi4hs2m1rznadg5gszjvh6hbrsbckq4i3rwk5qx4wnk3fh6c2jv") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.2 (c (n "rawtx-rs") (v "0.1.2") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "0c72yaz6bd4l6ypjzk3d8g1ifzdr0lz1f3k3hmjihxhi6lf7x79r") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.3 (c (n "rawtx-rs") (v "0.1.3") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "1icb251k2xg5d623qx6m6nxn0fjrx28f253v75hyqkq6w7aqib5b") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.4 (c (n "rawtx-rs") (v "0.1.4") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "0fsws35raqgm9xq33cc4dpxf457szchqhhh12gn84cwqfqf1g9bl") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.5 (c (n "rawtx-rs") (v "0.1.5") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "05j5jsf08wsv7mx8pcdwd540jkkbc0dnyr91xmv9iay2cb3k1dmj") (f (quote (("default") ("counterparty" "rc4")))) (y #t)))

(define-public crate-rawtx-rs-0.1.6 (c (n "rawtx-rs") (v "0.1.6") (d (list (d (n "bitcoin") (r "^0.30") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "0bq3rwjh44phyrq4n0dq8hf4bysqyfg53jgj77llxh9ixivnr7wj") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.7 (c (n "rawtx-rs") (v "0.1.7") (d (list (d (n "bitcoin") (r "^0.31") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "0j1mlrs4cngw165bxyiq51a3d3prgba0x7rq93d59m36xqcbr583") (f (quote (("default") ("counterparty" "rc4"))))))

(define-public crate-rawtx-rs-0.1.8 (c (n "rawtx-rs") (v "0.1.8") (d (list (d (n "bitcoin") (r "^0.31") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rc4") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "secp256k1") (r "^0.19.0") (d #t) (k 0)))) (h "0brzbzf8vr40639rrj4vg2ncw317fk8nahy7jz57qhv1vnl393zp") (f (quote (("default") ("counterparty" "rc4"))))))

