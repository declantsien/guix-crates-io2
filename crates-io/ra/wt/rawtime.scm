(define-module (crates-io ra wt rawtime) #:use-module (crates-io))

(define-public crate-rawtime-0.0.1 (c (n "rawtime") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.3") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "x86") (r "^0.19") (d #t) (k 0)))) (h "1jwcmq0g8bmyy56i2cvpynmmb98pxpqz4xp96hf9bwjfq8nr14mi")))

(define-public crate-rawtime-0.0.2 (c (n "rawtime") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.3") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "x86") (r "^0.32") (d #t) (k 0)))) (h "1pkc6cv6iad0z8j8iggyy5ma4d11zlirhn28lg68xkv5fr3id6y8")))

(define-public crate-rawtime-0.0.3 (c (n "rawtime") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.37") (d #t) (k 0)))) (h "018a3q1i91zk0zl23565rkmz2la254gpf8357pvks9j7pgdyhlgf")))

(define-public crate-rawtime-0.0.4 (c (n "rawtime") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.37") (d #t) (k 0)))) (h "1rn4mmml0xw1hy3f6fxplr136d3186lrlfx3dbsb5ak40w5zbj75")))

(define-public crate-rawtime-0.0.5 (c (n "rawtime") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.47") (d #t) (k 0)))) (h "00xfm245v07rk41ix9l592q9fkib1r25ylp7jgihkj03gs21gm0w")))

(define-public crate-rawtime-0.0.6 (c (n "rawtime") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.47") (d #t) (k 0)))) (h "11awqv8lmxxmlk698gl1xxg9ryw27a4gmy5l7r9k79jhns5rw4a1")))

(define-public crate-rawtime-0.0.7 (c (n "rawtime") (v "0.0.7") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.48") (d #t) (k 0)))) (h "1idlvxj88zlki3vi2nazi201872b6hvcfm06jv98pm40xzp85fcz")))

(define-public crate-rawtime-0.0.8 (c (n "rawtime") (v "0.0.8") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.50") (d #t) (k 0)))) (h "1610ziw61y3y234736l7r4wqvqhsx27pjg1pq5a2h8p3i1jrq5p9")))

(define-public crate-rawtime-0.0.9 (c (n "rawtime") (v "0.0.9") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.51") (f (quote ("unstable"))) (d #t) (k 0)))) (h "16xb52v8m1bs8xjvdffq7qqhjwazgd7p07b3xncc8i0aykrc2d3x")))

(define-public crate-rawtime-0.0.10 (c (n "rawtime") (v "0.0.10") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "lazy_static") (r "^1.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"none\"))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "x86") (r "^0.52") (f (quote ("unstable"))) (d #t) (k 0)))) (h "1ghvhc5qg8sc18jv0hrc57m1pi4rh8sjj0isxfg2qhpf8ixa44jd")))

