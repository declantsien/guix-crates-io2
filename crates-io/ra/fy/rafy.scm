(define-module (crates-io ra fy rafy) #:use-module (crates-io))

(define-public crate-rafy-0.1.0 (c (n "rafy") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0x8zkj8n6xvvxrsjn8mszflwp0xz940m213fysx2qyl7fgrgir4x")))

(define-public crate-rafy-0.1.1 (c (n "rafy") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1q4p6kldg0pm1z3dldryaqn7p73zpxidhqw3hh35irnfw0l568hv")))

(define-public crate-rafy-0.1.2 (c (n "rafy") (v "0.1.2") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1yhv5ql8rajcvv5j7k0yhcm5mn4shws43ca5v339qx78rlahsy7i")))

(define-public crate-rafy-0.1.3 (c (n "rafy") (v "0.1.3") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0dhvr4lcc53q3y9dxm3kq7b0k1axbhihv5kd1x9y2nnmd5as36ps")))

(define-public crate-rafy-0.1.4 (c (n "rafy") (v "0.1.4") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0qj3zd27dmq7ixqq2cqivqnwl0r0l29m4gh5k94p5qhp20jdrgxg")))

(define-public crate-rafy-0.1.5 (c (n "rafy") (v "0.1.5") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ppszdagiwzhz6hxgpaq5k3pnld29kq33m9xm2r0iwamcsxkgrk2")))

(define-public crate-rafy-0.1.6 (c (n "rafy") (v "0.1.6") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0fd7zk659kvg6x7n69517cn8h0irk04i9ilw43axxhf78hlpfm6d")))

(define-public crate-rafy-0.1.7 (c (n "rafy") (v "0.1.7") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0c6klmbbgkczp6fgypz9awbcq8g27rxf2ydwnfdixv5w5cgg7hm5")))

(define-public crate-rafy-0.1.8 (c (n "rafy") (v "0.1.8") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0sy7rqv5k22zjcj5lz8j9ia1cflsjapavd8n8nayz3a988wvfaa1")))

(define-public crate-rafy-0.1.9 (c (n "rafy") (v "0.1.9") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0idvijf35xfzzr2sswmsxfn3irnkhkviac33yynn9d7g71mjmgvl")))

(define-public crate-rafy-0.2.0 (c (n "rafy") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1pj17crsy4ydz2d4qmzh4vll4w25baj1vk3jmh8bjgcj4kcz25ag")))

(define-public crate-rafy-0.2.1 (c (n "rafy") (v "0.2.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "03lifjixczvnvydll3q6j868mgqca63yrh576qhq6raz59v0jzrr")))

