(define-module (crates-io ra yi rayimg) #:use-module (crates-io))

(define-public crate-rayimg-0.0.1 (c (n "rayimg") (v "0.0.1") (h "0dpaqsd163yjcra59dqz92j9c5y1pxkcs656458gczygsyj6s2cm")))

(define-public crate-rayimg-0.0.11 (c (n "rayimg") (v "0.0.11") (h "171nbvmawgq8rak66a87acjmf607ddmjwqnc66vn8d9kcikc0msx") (y #t)))

(define-public crate-rayimg-0.0.2 (c (n "rayimg") (v "0.0.2") (h "0ycszvkl8s51c8grnnpskjw2xp4mah5mrrd4gkj6w9ngdz9zi1zv")))

(define-public crate-rayimg-0.0.3 (c (n "rayimg") (v "0.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09r974y5nihsql2s7h38idgnlh66q4rc1jl8s7jlv0mjnqi8h2xv")))

(define-public crate-rayimg-0.0.4 (c (n "rayimg") (v "0.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "018xhw7gqw2a7yvdfss096w4451zdlm2j1620f52yfw74a2scb3g")))

(define-public crate-rayimg-0.0.5 (c (n "rayimg") (v "0.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1irvwxigxzqvalnwni4lvhkihizh27n3zsficdn2klsblvbw2h8y")))

(define-public crate-rayimg-0.0.6 (c (n "rayimg") (v "0.0.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10n01apvzcygc4zpqzkn7p80r602qzypkpdkrqsdz9yad3ga1mwb")))

(define-public crate-rayimg-0.0.7 (c (n "rayimg") (v "0.0.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gm21y8zz3hwgwig3k0a49gc8231j2hj1dxnq68cda2gqg0c6vl3")))

(define-public crate-rayimg-0.0.8 (c (n "rayimg") (v "0.0.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zf23cd8168141jy43jnq3nk7i7zn8smhwp5wnz9bwc43gh4p3sl")))

(define-public crate-rayimg-0.0.9 (c (n "rayimg") (v "0.0.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0di90ic495klcg7vaj815hv01vlgg2zydqkg6hgcwhhi31ig5krd")))

(define-public crate-rayimg-0.0.10 (c (n "rayimg") (v "0.0.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lihy35vwr1g6xad0idscdbjwxg45d824sqpc965r1jfha1y7qii")))

(define-public crate-rayimg-0.1.0 (c (n "rayimg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "152vkvqfqjqmc9hqng3l1qzlg1x3f1s1yqr3qi0jpflkvq2jki7b")))

(define-public crate-rayimg-0.1.1 (c (n "rayimg") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03ax8h4lz28my0ldpx33zlr486d558xffshjc2jfcvrnmh4xcbhg")))

(define-public crate-rayimg-0.1.2 (c (n "rayimg") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qmmakgc35x1a624pnpn5xjbpyn32raapvyizm95fp1b683jvpg4")))

