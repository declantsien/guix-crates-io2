(define-module (crates-io ra gs rags-rs) #:use-module (crates-io))

(define-public crate-rags-rs-0.1.0 (c (n "rags-rs") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)))) (h "128iyj2wv1zv1z7461maja84wyc5i6rx1q4hxa5s3li548jc7jjs")))

(define-public crate-rags-rs-0.1.1 (c (n "rags-rs") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)))) (h "0h7fpb5hs4f3i3cibx4mp3czkicp2ykzqh7p76n7qpr33x6d5cmy")))

(define-public crate-rags-rs-0.1.2 (c (n "rags-rs") (v "0.1.2") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)))) (h "1ajj3ygd5is56xgr60yaxns5xvgnxhc3v0hv5qxkf8g6h0fdj7sn")))

(define-public crate-rags-rs-0.1.3 (c (n "rags-rs") (v "0.1.3") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)))) (h "006b64ds1v1zy25mfblbzxrig7w8xhmw16fdk3vk7crb7qzfwbq1")))

(define-public crate-rags-rs-0.1.4 (c (n "rags-rs") (v "0.1.4") (d (list (d (n "bit-set") (r "^0.5.1") (d #t) (k 0)))) (h "0sa5in2cg9iifxdr48v9a2dxwx150jzzclcvplij9xhq95m1f1vb")))

