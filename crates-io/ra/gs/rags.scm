(define-module (crates-io ra gs rags) #:use-module (crates-io))

(define-public crate-rags-0.1.0 (c (n "rags") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "min-max-heap") (r "^1.2.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1jpwl8j490icrn5pcqfv4fvvaaiapf7aigvlc2sivw88idlkfsn4")))

