(define-module (crates-io ra ad raad) #:use-module (crates-io))

(define-public crate-raad-0.1.0 (c (n "raad") (v "0.1.0") (h "0xglsgwpszmj4xg16wnz6q8qpg6iaqz8lap59hc6kfn4wlbgi3gl") (r "1.79")))

(define-public crate-raad-0.1.1 (c (n "raad") (v "0.1.1") (h "1ya85kydxamiqjijk1vkmwbg4l4qpas87xyh72344w4fqabj6mj0") (r "1.79")))

(define-public crate-raad-0.1.2 (c (n "raad") (v "0.1.2") (h "0qpdfc1yhsrnzmn617lyx23w5wxyxrh3950qagadvyyh3v66xa1k") (r "1.78")))

