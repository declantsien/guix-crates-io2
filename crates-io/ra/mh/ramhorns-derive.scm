(define-module (crates-io ra mh ramhorns-derive) #:use-module (crates-io))

(define-public crate-ramhorns-derive-0.1.0 (c (n "ramhorns-derive") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0bsy9h4dw4d9f0hahqcwrlx4zhvxcvw1gbnrl9xvsyfsf82k6r05")))

(define-public crate-ramhorns-derive-0.2.0 (c (n "ramhorns-derive") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1rg55dzssxk56wzzw2rhihazyhicchdnaasdskgj5300y3nxll33")))

(define-public crate-ramhorns-derive-0.3.0 (c (n "ramhorns-derive") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "06zlnjxcpqgzppkridaf2vphllbmbvn3ljhfz4sli5wz02lp3x84")))

(define-public crate-ramhorns-derive-0.4.0 (c (n "ramhorns-derive") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9r9nb6adn0ammvxsm7wm95hlw2csdw7in8q5h463947sms1k66")))

(define-public crate-ramhorns-derive-0.5.0 (c (n "ramhorns-derive") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv5crnk9w1dha5x5zf2m9ym54515vvs6zcpgpcmgm3rkl1sdcv4")))

(define-public crate-ramhorns-derive-0.7.0 (c (n "ramhorns-derive") (v "0.7.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bvi2awlb9b7j86pqqzwsr37iih74b78bx75biq261yff97vqa7j")))

(define-public crate-ramhorns-derive-0.8.0 (c (n "ramhorns-derive") (v "0.8.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nr84vwlapn4l76xqk8x04y3nvizjc1hbc7ypl9mmh7czj9x7569")))

(define-public crate-ramhorns-derive-0.9.2 (c (n "ramhorns-derive") (v "0.9.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16dbq2fa64zxw2kv7zz3kbx9ik9yvjd5r61ic95ay6l92dgw8nd4")))

(define-public crate-ramhorns-derive-0.9.3 (c (n "ramhorns-derive") (v "0.9.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b5yi4m8fqllsd8rj7dj0gvgyr2gnfr6p7xaxcp2r888vs3g8jyc")))

(define-public crate-ramhorns-derive-0.9.4 (c (n "ramhorns-derive") (v "0.9.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vj44ylpcp0a373w3hcnlffcnpykxcy2dzxr6qw16x16gpsh6hlj")))

(define-public crate-ramhorns-derive-0.10.0 (c (n "ramhorns-derive") (v "0.10.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yljs5gsf7qf3pp9h9ivaw98pwkzjspdlypwlxaj1zfhzf6qbfx1")))

(define-public crate-ramhorns-derive-0.10.1 (c (n "ramhorns-derive") (v "0.10.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywcl5g2nj2wc1ajwvw0dm6pddbdfsgr139pjjbjgsgz7dr2cwzj")))

(define-public crate-ramhorns-derive-0.10.2 (c (n "ramhorns-derive") (v "0.10.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l6fgaj4yp1vplvm2pjdpvgv3lskjhywz6ggkhpvygw7npxyjidc")))

(define-public crate-ramhorns-derive-0.11.0 (c (n "ramhorns-derive") (v "0.11.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zkgh5khd1h0mgarar41a734hyxa7a8pb85irccvwklbviyj5249")))

(define-public crate-ramhorns-derive-0.12.0 (c (n "ramhorns-derive") (v "0.12.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d46klm55ryshnkns0l31fh08bm39jiy624grgk42c8vr6rxi200")))

(define-public crate-ramhorns-derive-0.13.0 (c (n "ramhorns-derive") (v "0.13.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16zmz6acmm03kizm6jz1yzyn1gn9cc6dsrrg23hdp5j102b2nvr2")))

(define-public crate-ramhorns-derive-0.14.0 (c (n "ramhorns-derive") (v "0.14.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0djmlw823kp2ikxxbbqnzkgkhdamgpdv6xmz6bwjdx5d47fvpadd")))

(define-public crate-ramhorns-derive-1.0.0 (c (n "ramhorns-derive") (v "1.0.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mf3k3jj8hafwpjyjj54f82dgvwan1h796wyg2i89l81sdrdbskl")))

