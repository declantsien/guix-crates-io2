(define-module (crates-io ra mh ramhorns-derive-ext) #:use-module (crates-io))

(define-public crate-ramhorns-derive-ext-0.14.0 (c (n "ramhorns-derive-ext") (v "0.14.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14bidcz1zcyyfab1nz0ja2pnkm19l703nw6b30fx85j0hsbsi3gy")))

(define-public crate-ramhorns-derive-ext-0.15.0 (c (n "ramhorns-derive-ext") (v "0.15.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n22j9mbh57yigy3ask4kvyzl70d7hbfl24r27d48f08y6fbzs19")))

(define-public crate-ramhorns-derive-ext-0.16.0 (c (n "ramhorns-derive-ext") (v "0.16.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jd56q6bkpnh7sbs6f0rinksg94jc6vp5b1v2b9lz5mda672i24s")))

(define-public crate-ramhorns-derive-ext-0.17.0 (c (n "ramhorns-derive-ext") (v "0.17.0") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1migchvs4ycgmm6qy2l466cnarz38zl277s8g6p7ba9lv4hw9w")))

(define-public crate-ramhorns-derive-ext-0.17.1 (c (n "ramhorns-derive-ext") (v "0.17.1") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gpf9gw7a53rxqlhsxrs68xn16arfmdsmgh664gn52axsk7923bp")))

(define-public crate-ramhorns-derive-ext-0.17.2 (c (n "ramhorns-derive-ext") (v "0.17.2") (d (list (d (n "bae") (r "^0.1.7") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1na32x9imy735838yldmykh0v9npqfjzi7c0icizzp0l7kzfaick")))

