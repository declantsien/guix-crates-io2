(define-module (crates-io ra st rastronomy) #:use-module (crates-io))

(define-public crate-rastronomy-0.1.0 (c (n "rastronomy") (v "0.1.0") (h "14m6n94k81znm2d4pjh8bwah04p8inaxrqnbaw4px90ydnn9zrc0") (y #t)))

(define-public crate-rastronomy-0.1.1 (c (n "rastronomy") (v "0.1.1") (h "05riw5k76qz8vyjlqdhhwpsp4pc2wimq38w9kpd9jfg47zqzcxgn") (y #t)))

(define-public crate-rastronomy-0.1.2 (c (n "rastronomy") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0i7x3zdpwdyg0gcqqm2m6fmq0q2lsd705r8zhd3syxn9h1vvj5rl") (y #t)))

