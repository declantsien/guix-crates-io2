(define-module (crates-io ra st rast) #:use-module (crates-io))

(define-public crate-rast-0.1.0-alpha.0 (c (n "rast") (v "0.1.0-alpha.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "extreme") (r "^666.666.666666") (d #t) (k 2)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "03gpar3msaz72lndr276gdxj8qyl44pg0lypg3cg9kww21c7ghzd") (f (quote (("runtime"))))))

