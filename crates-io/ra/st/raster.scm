(define-module (crates-io ra st raster) #:use-module (crates-io))

(define-public crate-raster-0.0.1 (c (n "raster") (v "0.0.1") (d (list (d (n "image") (r "^0.10.3") (d #t) (k 0)))) (h "0kcr2h2aap10vbmsi81gadmvfjwzc8i0zk360fc9x946h4sbm3i0")))

(define-public crate-raster-0.0.2 (c (n "raster") (v "0.0.2") (d (list (d (n "image") (r "^0.10.3") (d #t) (k 0)))) (h "1lxllvg0hfp1c4j87p8pkr404kxfnd2isvzg92nkmnqxm7mcbn6a")))

(define-public crate-raster-0.0.3 (c (n "raster") (v "0.0.3") (d (list (d (n "image") (r "^0.10.3") (d #t) (k 0)))) (h "19xi4sipiskcy6irdc19c7fz3in42s4c9yxcfd984y46r654llfd")))

(define-public crate-raster-0.0.4 (c (n "raster") (v "0.0.4") (d (list (d (n "image") (r "^0.10.3") (d #t) (k 0)))) (h "0s1dya4x6zmz74lfqabd0pk14246cdbiy5yamgk4h7g0p8rpmh2j")))

(define-public crate-raster-0.0.5 (c (n "raster") (v "0.0.5") (d (list (d (n "image") (r "^0.10.3") (d #t) (k 0)))) (h "1wqm557rsi54d865jp52q4j1fam6ggx2syky3vwcfkf537lbgf94")))

(define-public crate-raster-0.0.6 (c (n "raster") (v "0.0.6") (d (list (d (n "image") (r "^0.10.3") (f (quote ("gif_codec" "jpeg" "ico" "png_codec"))) (k 0)))) (h "1wsavfk9hijv3bpmffz4m8waashis3z489lfwf0gc9qlxhprn9kw")))

(define-public crate-raster-0.0.7 (c (n "raster") (v "0.0.7") (d (list (d (n "image") (r "^0.10.3") (f (quote ("gif_codec" "jpeg" "ico" "png_codec"))) (k 0)))) (h "17calpswg00ji2zcq52mpxq5knbyqhg62b361gqv1x96cs03nylz")))

(define-public crate-raster-0.0.8 (c (n "raster") (v "0.0.8") (d (list (d (n "image") (r "^0.10.3") (f (quote ("gif_codec" "jpeg" "ico" "png_codec"))) (k 0)))) (h "0wrcdldhyw0gjhqb4piy0xv45mma0grh9cyq8b5zi7i6n1jxxzfq")))

(define-public crate-raster-0.1.0 (c (n "raster") (v "0.1.0") (d (list (d (n "image") (r "^0.10.3") (f (quote ("gif_codec" "jpeg" "ico" "png_codec"))) (k 0)))) (h "169wihxnfhlr90k65cmpb7d94gkfipbb6dlc89dasz5v9x4jbjnl")))

(define-public crate-raster-0.2.0 (c (n "raster") (v "0.2.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "image") (r "^0.10.4") (f (quote ("jpeg"))) (k 0)) (d (n "png") (r "^0.5.2") (d #t) (k 0)))) (h "1vypkhqfak8xik9v930zlrjypysafbrp5g6kl1gf4q22zaf9wgsc")))

