(define-module (crates-io ra st rastro) #:use-module (crates-io))

(define-public crate-rastro-0.1.0 (c (n "rastro") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "1nyskd6ndccicmyfp1hy2i5n4g7cy4pqlxn6nl0i1ps2asq1pdn1")))

(define-public crate-rastro-0.1.1 (c (n "rastro") (v "0.1.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "0lvmh3qcbz37bs88rwh497nk5860jj46fmq5xiymjd693r8a5az8")))

(define-public crate-rastro-0.1.2 (c (n "rastro") (v "0.1.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "03wsdv3648qyj71dzh16q9bkckhak45s1k2m2bijdm8yzdayd3kq")))

(define-public crate-rastro-0.1.3 (c (n "rastro") (v "0.1.3") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "0will528j0hrgcybkv6vmx1dyb4z9b8dzn7dh90fzq02zdfjjwxq")))

(define-public crate-rastro-0.1.4 (c (n "rastro") (v "0.1.4") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "19nd9fh2yypqxbb67rqp0k2h0swhcyb4gbris8lblq1z1gmlzwya")))

(define-public crate-rastro-0.1.5 (c (n "rastro") (v "0.1.5") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.25.3") (d #t) (k 0)) (d (n "rsofa") (r "^0.3.0") (d #t) (k 0)))) (h "1vyb7j8lr5m2cjzr8164w3p7lync27xb82f09h4dpmnrzbn6h1cx")))

(define-public crate-rastro-0.1.6 (c (n "rastro") (v "0.1.6") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "rsofa") (r "^0.4.5") (d #t) (k 0)))) (h "103liwqchlyj0xrplzd5w3srvhnmww4rhdfsywspfbwn70ry0xvz")))

(define-public crate-rastro-0.1.8 (c (n "rastro") (v "0.1.8") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "rsofa") (r "^0.4.5") (d #t) (k 0)))) (h "042wihvgy3wdddb41xrl95sq00acchf1gxgnjqjrwp1vwprlkjpv")))

