(define-module (crates-io ra st raster-retrace) #:use-module (crates-io))

(define-public crate-raster-retrace-0.1.0 (c (n "raster-retrace") (v "0.1.0") (h "0sva6s6dpn2fgqyjb900w49wc5n71dwj55mbvkzs6h9a32by1z2z")))

(define-public crate-raster-retrace-0.1.1 (c (n "raster-retrace") (v "0.1.1") (h "0m6nr9a46l7kvz2l5kkd3if6a0d6p5ll7nyrwnxdc1m8qjyw92d8")))

(define-public crate-raster-retrace-0.1.2 (c (n "raster-retrace") (v "0.1.2") (h "1152vcgz5izalch82i4w2ksiqw7ifxavnfcqlq795h0iw3jcpld6")))

