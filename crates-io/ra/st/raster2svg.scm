(define-module (crates-io ra st raster2svg) #:use-module (crates-io))

(define-public crate-raster2svg-1.0.0 (c (n "raster2svg") (v "1.0.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)))) (h "1jskfcfa25khi5yvq5jlwqsn2wsqia74zs7l8gxlkwc022y2mk47") (y #t)))

(define-public crate-raster2svg-1.0.1 (c (n "raster2svg") (v "1.0.1") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)))) (h "036mqzadbv0g8sqjpz52fr9952sdhrczcjscyc4k0q5kc0pf1xms") (y #t)))

(define-public crate-raster2svg-1.0.2 (c (n "raster2svg") (v "1.0.2") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)))) (h "0gjb1d0m4a7d4a7zy8hc2kg1ihzirccza2d3hf0n1liivy2p3i37") (y #t)))

(define-public crate-raster2svg-1.0.3 (c (n "raster2svg") (v "1.0.3") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.2") (d #t) (k 0)))) (h "11gx3awjww76ffy7wgcgjyf7wm65vmw2mvvkpi0bf85kbfcms75l") (y #t)))

(define-public crate-raster2svg-1.0.4 (c (n "raster2svg") (v "1.0.4") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.3") (d #t) (k 0)))) (h "1ajn9372hjv6jykm74lpvv30cy4bckh2xcsy2glcprr5pfy89v2s") (y #t)))

(define-public crate-raster2svg-1.0.5 (c (n "raster2svg") (v "1.0.5") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "00gxmp37zqi8a8gs0115lb2db0f0sr11sjjsxjn5yxdhii0vf7x0") (y #t)))

(define-public crate-raster2svg-1.0.6 (c (n "raster2svg") (v "1.0.6") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)))) (h "0652w3wsdd6xqyyk7l6afg9h26rd4m0ivrma5bmflnd4w7allqjn") (y #t)))

(define-public crate-raster2svg-1.0.7 (c (n "raster2svg") (v "1.0.7") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.5") (d #t) (k 0)))) (h "025qbi0s6vgmzsjf8x0qrpnmyj3x0872qpmlk9z1z82zyj33ghvx") (y #t)))

(define-public crate-raster2svg-1.0.8 (c (n "raster2svg") (v "1.0.8") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.7") (d #t) (k 0)))) (h "1nv96690lc5n66h0zlm4l2061fwx9yncdwnblj4nchhqmwz6s3gl") (y #t)))

(define-public crate-raster2svg-1.0.9 (c (n "raster2svg") (v "1.0.9") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)))) (h "0ig12d9hckj8v2z16yh7sn24srhrl390xril53i4pca14q6bxcf6") (y #t)))

(define-public crate-raster2svg-1.0.10 (c (n "raster2svg") (v "1.0.10") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)))) (h "018ik7i4v3nrnkgkh6acw282wbqndax2b9xacipssjwdl5nli1gl") (y #t)))

(define-public crate-raster2svg-1.0.11 (c (n "raster2svg") (v "1.0.11") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.2") (d #t) (k 0)) (d (n "image") (r "^0.23.13") (d #t) (k 0)))) (h "0wc79f75r5vqbhx1kss52gjahlax6b1d8wclyc79i53m3b2ky9r9") (y #t)))

(define-public crate-raster2svg-1.0.12 (c (n "raster2svg") (v "1.0.12") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "17wb2hs5qycn2wx46n50mz8la9vqjifdarwkw95r9kdlbqxl7gg3") (y #t)))

(define-public crate-raster2svg-1.0.13 (c (n "raster2svg") (v "1.0.13") (d (list (d (n "clap") (r "^2.34.0") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.11") (f (quote ("array"))) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)))) (h "1kypzwf75i6wz2hbsr5jkysndy8bkyrkd1xgkv9w5r00jyyklj7z") (y #t) (r "1.58.1")))

(define-public crate-raster2svg-1.0.14 (c (n "raster2svg") (v "1.0.14") (d (list (d (n "clap") (r "^2.34.0") (f (quote ("yaml"))) (k 0)) (d (n "contour_tracing") (r "^1.0.12") (f (quote ("array"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)))) (h "0cy0cbgi0agg6dfd3avmx18ryqx1ri8j6n52mlwd1ngfkh83a7ni") (r "1.58.1")))

