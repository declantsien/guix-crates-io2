(define-module (crates-io ra #{6t}# ra6t1) #:use-module (crates-io))

(define-public crate-ra6t1-0.2.0 (c (n "ra6t1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1f3hyhrskbj4gk13bx27b4vkdi5hw08zdav25w7qjj2nx8mm8x97") (f (quote (("rt" "cortex-m-rt/device"))))))

