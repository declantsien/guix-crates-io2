(define-module (crates-io ra m- ram-machine) #:use-module (crates-io))

(define-public crate-ram-machine-0.1.0 (c (n "ram-machine") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0g85jd0jacid2vw49wm904cqvr0d562m6xc6a6fcvfx3mwb173xm")))

(define-public crate-ram-machine-0.2.0 (c (n "ram-machine") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0rd2x4h6pzykxvr22v4vdqlz0z3n7d9hzkm09ci3h1i05dy7l262")))

(define-public crate-ram-machine-1.0.0 (c (n "ram-machine") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1yfkwnda8d0y97j7ccszxy69mg21sywx5b3r6kszm30a41xkdbq9")))

(define-public crate-ram-machine-1.1.0 (c (n "ram-machine") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0f6d4hicj4y0pkcha41hbiq9aymnkmjw17nsakbdknc975fjfqi0")))

(define-public crate-ram-machine-1.2.0 (c (n "ram-machine") (v "1.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "049n0kg2hg3yi8hlmr6jc50n1z3vldf1r53mxf4dxv35mn7zsx9z")))

(define-public crate-ram-machine-2.0.0 (c (n "ram-machine") (v "2.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "06pwcmprw49di0ppmam9qkc8y8iiqxzsbndbw0vhxzn6d4k7vzwj")))

(define-public crate-ram-machine-2.0.1 (c (n "ram-machine") (v "2.0.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "0i7y8nnbwfgc7fdsqdr7y09ixgj6cspq4as2dxbdx9n35pwwd6ii")))

