(define-module (crates-io ra rg rargs) #:use-module (crates-io))

(define-public crate-rargs-0.2.0 (c (n "rargs") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "1lwx7x52nkszf2jkllak45lbphgjkxry4kr7r0sbsgwpbbq5dv76")))

(define-public crate-rargs-0.2.2 (c (n "rargs") (v "0.2.2") (d (list (d (n "assert_cli") (r "^0.5.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0a7llvym9286bfmiiqb5cyam95ps0j57ds2gjwj5x5hxpm21grz1")))

(define-public crate-rargs-0.3.0 (c (n "rargs") (v "0.3.0") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.0") (d #t) (k 0)))) (h "0inqvlys44las6nzpifw7hw1fci1rz4wwz5v81sl4d6rvz2v2abj")))

