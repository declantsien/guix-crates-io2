(define-module (crates-io ra gk ragkit) #:use-module (crates-io))

(define-public crate-ragkit-0.0.0 (c (n "ragkit") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0hifpxy90q3bjf947prmb4flk0qlj525zhmjsv46xsbrl265sxj9")))

