(define-module (crates-io ra gk ragkit-ai) #:use-module (crates-io))

(define-public crate-ragkit-ai-0.0.0 (c (n "ragkit-ai") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1854a6qk5rm2nhcply0hgh3dp3k9dbik0sc3n5hbmc9dpqw12ksr")))

(define-public crate-ragkit-ai-0.0.1 (c (n "ragkit-ai") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zdjzcs440k5wpifpj82rvp5hjkq98b7c60nb8nrlq8vjx5qdmww")))

(define-public crate-ragkit-ai-0.0.2 (c (n "ragkit-ai") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hp56fk2zr1v8n3qzmbnl1p4k6p5xmkk8nw8p85gbf9bw0ymxk4z")))

