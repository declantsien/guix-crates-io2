(define-module (crates-io ra fx rafx-base) #:use-module (crates-io))

(define-public crate-rafx-base-0.0.2 (c (n "rafx-base") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06xcmlz71sr6h6vwjlkrljadvhcl130knrr1463v1wb1gnwzv6hg")))

(define-public crate-rafx-base-0.0.3 (c (n "rafx-base") (v "0.0.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01al8b481vbzagqmmw93fbj4d48cdfzc9h69j5v66zl0yhs76p4a")))

(define-public crate-rafx-base-0.0.4 (c (n "rafx-base") (v "0.0.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0kq7hw2jhbg5mxd64ny9r6232d03ipivydkwpmmyp61gi1argmwr")))

(define-public crate-rafx-base-0.0.5 (c (n "rafx-base") (v "0.0.5") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "068iigvkhlbn8hvprp2ricl5niv2nxgkpp7i63f1h3nmr69l3w1x")))

(define-public crate-rafx-base-0.0.6 (c (n "rafx-base") (v "0.0.6") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "095jmx4iyknynh84md3igw2y3k45c53jqam7gpg5y6kkx1d0iclk")))

(define-public crate-rafx-base-0.0.7 (c (n "rafx-base") (v "0.0.7") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03rq6anhpnlh3f229228z1rp0v82rm3l9fwvx8xi43dsz1ijjcgb")))

(define-public crate-rafx-base-0.0.8 (c (n "rafx-base") (v "0.0.8") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05c7ilpmf5mb4qgfma2kar8l09cdmag8da6lpb584l6iv5j0smbi")))

(define-public crate-rafx-base-0.0.9 (c (n "rafx-base") (v "0.0.9") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08ms0msi89n62zvvrqp17a7i59x4sz4554cz402nicz981lb17i3")))

(define-public crate-rafx-base-0.0.10 (c (n "rafx-base") (v "0.0.10") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02p67y5s7fyf84w9vmzi7bpmjryl6p27y786jydsdz76jc1564n8")))

(define-public crate-rafx-base-0.0.11 (c (n "rafx-base") (v "0.0.11") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11p8vqaacd5l21zd7nz4b5sw2hq731d7x1gd819q6r8gkk0lawgp")))

(define-public crate-rafx-base-0.0.12 (c (n "rafx-base") (v "0.0.12") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mv595xm8a1dvd0ca23p50vxlsfn59xnkq7nws75mfykfq7mhz7g")))

(define-public crate-rafx-base-0.0.13 (c (n "rafx-base") (v "0.0.13") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gjkadx14ixpgcbnzcb2bvx7v5pg6ziz06i333sps1fgrlx8wcsr")))

(define-public crate-rafx-base-0.0.14 (c (n "rafx-base") (v "0.0.14") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r01k7sh4h4l6cgi885ijdxww9aacxhgpki7r123xnk9sary5hx0")))

(define-public crate-rafx-base-0.0.15 (c (n "rafx-base") (v "0.0.15") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1lcj5vvlzn5za1v1bmz2v97w34fhgfaqp8lzx5v9xdb660hy7xwm")))

(define-public crate-rafx-base-0.0.16 (c (n "rafx-base") (v "0.0.16") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0a2xddphx4pfflm6hawwnqkfzm4rd529pyzqhli6y5vdfbpkdw0l")))

