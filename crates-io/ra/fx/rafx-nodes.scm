(define-module (crates-io ra fx rafx-nodes) #:use-module (crates-io))

(define-public crate-rafx-nodes-0.0.2 (c (n "rafx-nodes") (v "0.0.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "^0.0.2") (d #t) (k 0)) (d (n "rafx-base") (r "^0.0.2") (d #t) (k 0)))) (h "1920381zsqib2zn0pblrib5nikqmrzpry5vwjsz2d7n1n7fi3zxk")))

(define-public crate-rafx-nodes-0.0.3 (c (n "rafx-nodes") (v "0.0.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "^0.0.3") (d #t) (k 0)) (d (n "rafx-base") (r "^0.0.3") (d #t) (k 0)))) (h "06lmgzr84r109p6w2axk5mg6p1yv0gdd5lxqn5zr7v8rdvwcb6l5")))

(define-public crate-rafx-nodes-0.0.4 (c (n "rafx-nodes") (v "0.0.4") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "^0.0.4") (d #t) (k 0)) (d (n "rafx-base") (r "^0.0.4") (d #t) (k 0)))) (h "0grmh2zxlanfdvdmd8gg45g1q42vv39gfr2j7hldqprrihr4rddk")))

(define-public crate-rafx-nodes-0.0.5 (c (n "rafx-nodes") (v "0.0.5") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "^0.0.5") (d #t) (k 0)) (d (n "rafx-base") (r "^0.0.5") (d #t) (k 0)))) (h "0lcv83a3870q3rsqsd458fr39bi6hhfnkq3733spzmzj5nrghlc3")))

(define-public crate-rafx-nodes-0.0.6 (c (n "rafx-nodes") (v "0.0.6") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "^0.0.6") (d #t) (k 0)) (d (n "rafx-base") (r "^0.0.6") (d #t) (k 0)))) (h "1n6s3g4g7jahn45mslqsskkp0a60xgsfb60c6chfw2dfci3yy66v")))

(define-public crate-rafx-nodes-0.0.7 (c (n "rafx-nodes") (v "0.0.7") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rafx-api") (r "=0.0.7") (d #t) (k 0)) (d (n "rafx-base") (r "=0.0.7") (d #t) (k 0)))) (h "0ychklnfp34c1xqsxm717h05nwllgbbmmfr0zdvypm54rff1h27p")))

