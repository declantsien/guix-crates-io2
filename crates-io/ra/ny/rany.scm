(define-module (crates-io ra ny rany) #:use-module (crates-io))

(define-public crate-rany-0.1.1 (c (n "rany") (v "0.1.1") (h "1d2k31kyfgdy7qaprz39qwn42k21rcabadrb62ibhh0jbwsg9qd9")))

(define-public crate-rany-0.1.2 (c (n "rany") (v "0.1.2") (h "0akavrz6hhqhlx2473jmbcwfiv6br98x355jcmzilnn1i9f44jsk")))

(define-public crate-rany-0.1.3 (c (n "rany") (v "0.1.3") (h "0jbf2bpinz9jm1qqcai2saib7n11xfcl7x4zgd1hd80fxlswxlc9")))

(define-public crate-rany-0.1.4 (c (n "rany") (v "0.1.4") (h "0rplicalp6vgb6x8blscnaf56l7amz8ymhfpscd6p00v4qb82980")))

(define-public crate-rany-0.1.5 (c (n "rany") (v "0.1.5") (h "0dy5q558xpq428nny7vjbg9lz29257xl4cq3kk5d7cfrwhsicdk5")))

(define-public crate-rany-0.1.6 (c (n "rany") (v "0.1.6") (h "1dwndqd2pfd4a2rmh82wg3nhy8s6s5g888k4i20mqw9byvxxvwpi") (f (quote (("url") ("default" "url") ("b255"))))))

(define-public crate-rany-0.1.7 (c (n "rany") (v "0.1.7") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0pwnb87vwkwcr3f265cxjdxxxdvsxlan6pjlzxslnzd608fkb1z1") (f (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1.8 (c (n "rany") (v "0.1.8") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1swmkxz6zd41sxjcqii5cx3rp0b8nn4fhw163r044rbpjb75pxzc") (f (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1.9 (c (n "rany") (v "0.1.9") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0bnpfj5ic5nsx8x76gzy5zafwj19mdd8b9k6dby23h2dycjmfk18") (f (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1.10 (c (n "rany") (v "0.1.10") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0gr2ilrm2nbkifqn5470f3ysicdmx5578zxbr19arqzqvdsnpbjg") (f (quote (("url" "strany") ("strany") ("default") ("b255"))))))

