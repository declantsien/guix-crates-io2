(define-module (crates-io ra cr racros) #:use-module (crates-io))

(define-public crate-racros-0.1.0 (c (n "racros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a99ggxlpxr43vphdcrmhwp17zsqfa7lp6sxih515v4gxadv28s7")))

(define-public crate-racros-0.2.0 (c (n "racros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h35kk6wafagjzjj4h47kwvfpv7swd7f1zg92b5589vbprsa6qw2")))

