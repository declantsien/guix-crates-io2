(define-module (crates-io ra tm ratman-configure) #:use-module (crates-io))

(define-public crate-ratman-configure-0.1.0 (c (n "ratman-configure") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "netmod-mem") (r "^0.4.0") (d #t) (k 0)) (d (n "netmod-tcp") (r "^0.4.0") (d #t) (k 0)) (d (n "netmod-udp") (r "^0.2.0") (d #t) (k 0)) (d (n "ratman") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07g5542g3xk6w26p7k6v7s1lw1s4n94axwfw6xvhc9xc8jx9slmi") (f (quote (("android"))))))

