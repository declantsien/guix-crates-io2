(define-module (crates-io ra tm ratman-client) #:use-module (crates-io))

(define-public crate-ratman-client-0.3.0 (c (n "ratman-client") (v "0.3.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "types") (r "^0.3.0") (d #t) (k 0) (p "ratman-types")))) (h "1qj7cp71j6879q26xkl8wfg5dmrjrh2qys49324j7s6k5lg183b6")))

(define-public crate-ratman-client-0.3.1 (c (n "ratman-client") (v "0.3.1") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "types") (r "^0.3.0") (d #t) (k 0) (p "ratman-types")))) (h "0k5kdhnwb3cmjilw8nkak6vszx0k1pzdrirc72bd7nk0hagw5qmn")))

(define-public crate-ratman-client-0.3.2 (c (n "ratman-client") (v "0.3.2") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "types") (r "^0.3.0") (d #t) (k 0) (p "ratman-types")))) (h "0lszs161g60k0dwy61j3w9rhf12r66sribqmbhbr5fbpvpxdwp73")))

(define-public crate-ratman-client-0.4.0 (c (n "ratman-client") (v "0.4.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures-lite") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "types") (r "^0.4.0") (d #t) (k 0) (p "ratman-types")))) (h "0dkxjrkqmr8bhiv3klb131zlrk5kx294cvgsj5k1c89zkcmvm2fk")))

