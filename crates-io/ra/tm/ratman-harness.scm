(define-module (crates-io ra tm ratman-harness) #:use-module (crates-io))

(define-public crate-ratman-harness-0.1.0 (c (n "ratman-harness") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.0") (d #t) (k 0)) (d (n "netmod-mem") (r "^0.4") (d #t) (k 0)) (d (n "ratman") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0cif2ppg9i259633s7hmsb7icn17j9lnvmsvrkki1x0lhkgbqimp")))

