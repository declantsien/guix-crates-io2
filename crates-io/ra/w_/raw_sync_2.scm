(define-module (crates-io ra w_ raw_sync_2) #:use-module (crates-io))

(define-public crate-raw_sync_2-0.1.5 (c (n "raw_sync_2") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.23") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winnt" "winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jzxwkvpipwm0k7pd229dm4sabanplw2d73q6rbd2cbyl5gv8rzh")))

