(define-module (crates-io ra w_ raw_vulkan_handle) #:use-module (crates-io))

(define-public crate-raw_vulkan_handle-0.1.0 (c (n "raw_vulkan_handle") (v "0.1.0") (h "1nyywz5k4nc8b9b28z8spx8alr2f0mm5dvvvfyq1074447niipal")))

(define-public crate-raw_vulkan_handle-0.1.1 (c (n "raw_vulkan_handle") (v "0.1.1") (h "1cp4bq1m5rnqkyxdj1rnzcvgin49yd2narmvvzxwqv9py12zdnr3")))

