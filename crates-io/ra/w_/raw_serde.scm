(define-module (crates-io ra w_ raw_serde) #:use-module (crates-io))

(define-public crate-raw_serde-0.1.0 (c (n "raw_serde") (v "0.1.0") (d (list (d (n "raw_serde_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1n2fxf3ylcg20sm4bg9wpj9agal1r8n5ygkfmxfd7wv51j6sm1vx")))

(define-public crate-raw_serde-0.1.1 (c (n "raw_serde") (v "0.1.1") (d (list (d (n "raw_serde_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1a410cb1a57sxswq0lpdx0gl2hl3c4y8nffng3rzn3jsyn4052k2")))

(define-public crate-raw_serde-0.1.2 (c (n "raw_serde") (v "0.1.2") (d (list (d (n "raw_serde_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1hl1wfy8mizwx5d87x2amzpn2l3czrbfah0aq4p91qgw3vz7r28l")))

(define-public crate-raw_serde-0.1.3 (c (n "raw_serde") (v "0.1.3") (d (list (d (n "raw_serde_derive") (r "^0.1.2") (d #t) (k 0)))) (h "147gghywxfypgbfc2nl5lwk8y7f478h74wrazhfwys51x3ki28xw")))

(define-public crate-raw_serde-0.1.4 (c (n "raw_serde") (v "0.1.4") (d (list (d (n "raw_serde_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1rcd2pa5q8nmg7a40k8bjfipjhk9r24hwk8gbf3rnnx3pkzhs91k")))

