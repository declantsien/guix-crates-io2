(define-module (crates-io ra w_ raw_tty) #:use-module (crates-io))

(define-public crate-raw_tty-0.1.0 (c (n "raw_tty") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.55") (d #t) (k 0)))) (h "1cgqjh9i0q4v1i85rbygzjjaxgi9a6mdhj2x7w6yyja0a3bi5xai")))

