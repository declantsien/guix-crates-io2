(define-module (crates-io ra w_ raw_pointer) #:use-module (crates-io))

(define-public crate-raw_pointer-0.1.0 (c (n "raw_pointer") (v "0.1.0") (h "0107vcls5ppyfgcb0c0k7dqm133g3ppwm1hbcxc9yqvzpnp125ri") (y #t)))

(define-public crate-raw_pointer-0.1.1 (c (n "raw_pointer") (v "0.1.1") (h "1gh3angckvv8lc8jf17k66jc6hz4qgljaf1n82ygc7b4m7f0w1wd")))

(define-public crate-raw_pointer-0.1.2 (c (n "raw_pointer") (v "0.1.2") (h "0s5myqls07j4b0j9p9jw04rn0c0v7j41bq2krbn16c9cgm4pi4pm")))

(define-public crate-raw_pointer-0.1.3 (c (n "raw_pointer") (v "0.1.3") (h "1qdvhph7hpz6bn6rx8zbamq7z2bs145hzqhzzr7brkbk6lz8f7k1")))

(define-public crate-raw_pointer-0.1.4 (c (n "raw_pointer") (v "0.1.4") (h "1k0x84hr5ls93akdqg8j2n34b6qlzsdbg39f99z34ddp852qqf81")))

