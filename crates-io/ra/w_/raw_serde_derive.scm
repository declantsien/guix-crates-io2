(define-module (crates-io ra w_ raw_serde_derive) #:use-module (crates-io))

(define-public crate-raw_serde_derive-0.1.0 (c (n "raw_serde_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "0g7p1xgl4x7jjrhxpqlry7m1lk9n84kfciwsg3k7g6jk4gk39vfr")))

(define-public crate-raw_serde_derive-0.1.1 (c (n "raw_serde_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "1p0qvcmr1l70lznhk1r6l70a6qkm91qi4pldp09hv2an110jf8cc")))

(define-public crate-raw_serde_derive-0.1.2 (c (n "raw_serde_derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.10") (d #t) (k 0)) (d (n "syn") (r "^0.10.5") (d #t) (k 0)))) (h "1bl7rljqx01x4r4w1dy37hij6h0168xr4s5c6gh75b9c67g8g6ik")))

