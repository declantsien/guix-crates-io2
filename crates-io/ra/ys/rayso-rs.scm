(define-module (crates-io ra ys rayso-rs) #:use-module (crates-io))

(define-public crate-rayso-rs-0.1.0 (c (n "rayso-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "0nnnpwq8l6v7y52na6hfmkqag4k8gs517wiya19walqx3zw68wig")))

(define-public crate-rayso-rs-0.1.1 (c (n "rayso-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "00xvadca5drh2nlypnr0jymkmg8knm6j8y5m5n3x9v9kq407v35p")))

(define-public crate-rayso-rs-0.1.2 (c (n "rayso-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "thirtyfour") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "00bnd7qjjbjk0prfkn1chk90bfx13gyzx7si8babx49wwf1hlqp3")))

