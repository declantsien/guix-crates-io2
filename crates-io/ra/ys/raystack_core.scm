(define-module (crates-io ra ys raystack_core) #:use-module (crates-io))

(define-public crate-raystack_core-0.1.0 (c (n "raystack_core") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lisdpysyxb95gaarrb5d4n8vbpmgv644bliv9y1zlz0d3sixr38")))

(define-public crate-raystack_core-0.2.0 (c (n "raystack_core") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j9g9n258mi3sgancxczb190l3jmd8v85wy4ais1h7wnv2hyxpvd")))

(define-public crate-raystack_core-0.2.1 (c (n "raystack_core") (v "0.2.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qmapy73x72897i89vwv30kzsvixw4izps64vryp995z3q928v7f")))

(define-public crate-raystack_core-0.2.2 (c (n "raystack_core") (v "0.2.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0njmsm5wrqj7ylgj6rznscaw8hwlcxrxphrmp01pp87a5vwvaaah")))

(define-public crate-raystack_core-0.2.3 (c (n "raystack_core") (v "0.2.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "044igwr6fxnizaxcr5hsali2lk0cajrir4wi0s2b8sbyqan2ld6a")))

(define-public crate-raystack_core-0.3.0 (c (n "raystack_core") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0594b8hix4hc8xmb1f5xgmqxj00769p3xarimhz4lav5b8nfrgi9")))

(define-public crate-raystack_core-0.3.1 (c (n "raystack_core") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0729pfhbwjcryjz4c3vvy0ji4gw5zwch036k9a7ql7zbh2q56pdl")))

(define-public crate-raystack_core-0.4.0 (c (n "raystack_core") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17hx2h193nd27fgfl7jag6hzvjh8ajy29pba4lh9004m6j3cvlwj") (f (quote (("json" "serde_json"))))))

(define-public crate-raystack_core-0.4.1 (c (n "raystack_core") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18l58p53ddrnyylwb33f1xiz1rcjyfyqkdhkps2jwzz5ms41ng0p") (f (quote (("json" "serde_json"))))))

(define-public crate-raystack_core-0.5.0 (c (n "raystack_core") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bfbf68a66jdf8rknxnf43xqzplj494z4icrv83dqjfbv3h58awq") (f (quote (("json" "serde_json"))))))

