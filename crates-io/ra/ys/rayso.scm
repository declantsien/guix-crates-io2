(define-module (crates-io ra ys rayso) #:use-module (crates-io))

(define-public crate-rayso-0.1.0 (c (n "rayso") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rbase64") (r "^2.0.3") (d #t) (k 0)))) (h "0mr3c1fwf9prd1rix6l7s4a0qsmgxrrmx51rdivcyfnkckazrmi6")))

(define-public crate-rayso-0.1.1 (c (n "rayso") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "rbase64") (r "^2.0.3") (d #t) (k 0)))) (h "0y51frph7z2vabw8r7ijyk575322xmqymngl4bylnjvddblmn0r3")))

