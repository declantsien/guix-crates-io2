(define-module (crates-io ra ml ramlink) #:use-module (crates-io))

(define-public crate-ramlink-0.1.0 (c (n "ramlink") (v "0.1.0") (d (list (d (n "const-assert") (r "^1.0.1") (d #t) (k 0)))) (h "0vgpf4mxxviq86m9j9n0ziqvhqpwyr5j1a9zysg2a9c5772g6wrl") (f (quote (("producer") ("consumer"))))))

