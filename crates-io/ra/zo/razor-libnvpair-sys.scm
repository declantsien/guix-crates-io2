(define-module (crates-io ra zo razor-libnvpair-sys) #:use-module (crates-io))

(define-public crate-razor-libnvpair-sys-0.3.0 (c (n "razor-libnvpair-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "059b3dwml0211yxab3d5zx7xz8pwl6xbvwdbk6hafpz3gpds0iwz") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.3.1 (c (n "razor-libnvpair-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0q5x5raghpbqxdqh1x6imnlylhmv03c6qbqmrpmabjax4b1dba5p") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.4.0 (c (n "razor-libnvpair-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0k7vj9l8vhf0v430c3m5d756fjvkac0nhj21cym49l5yhgy0kj21") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.4.1 (c (n "razor-libnvpair-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "115nahjsrmgmqx71kmzv8dlcpan5r6hi5bciz92a5dj1vzpyngib") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.5.0 (c (n "razor-libnvpair-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c5mic10s9mcnl47isq583g3h6y2bpvicndhlcyj5cj9byhl1jjd") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.6.0 (c (n "razor-libnvpair-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "192wxvi68g0b5il2zsw6vnws8slh15kc9kxgrmdzibfxhj69bkla") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.6.1 (c (n "razor-libnvpair-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p8jiam2xdbl8xkn0lk7pyncy7yhssyr7b56v1r7x4wryh3fjjlg") (l "nvpair")))

(define-public crate-razor-libnvpair-sys-0.7.0 (c (n "razor-libnvpair-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j5igpzgsw5584c6pspb21a6bzlvxh110j8cch55pi89bnjlw739") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.8.0 (c (n "razor-libnvpair-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dh30i8rakhj4krb3dvps2n4yn8q6pqkc63bgz9h3har5wl1474z") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.9.0 (c (n "razor-libnvpair-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0n7q97d6hj58061zjdj9nablrjgw0b74qgn1mb5d7xzh4y4wbkdy") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.10.0 (c (n "razor-libnvpair-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1jjvjr1dfr54awxvi6ka2k9ijcxkls52b3gjg57pqx4l5yazbasa") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.10.1 (c (n "razor-libnvpair-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1d5gbrpvamlvd54vm30ghwqr666manxixwlqg7win8lyvmybmpby") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.11.0 (c (n "razor-libnvpair-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "068k6yaqx3xz7bc3a6iypbrkqmy1q4w1hqh4vmydrz4jf7p6k0ys") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.12.0 (c (n "razor-libnvpair-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lwl3irbxp8ay261dff2ika3bqn1jh4326ra5jnq97aalh1dvgn1") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.13.0 (c (n "razor-libnvpair-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cmpwj8a2hhh76g15jakm9pb6ss8y5xk53yfpbakw12wpb3q83cs") (l "nvpair") (r "1.62")))

(define-public crate-razor-libnvpair-sys-0.13.1 (c (n "razor-libnvpair-sys") (v "0.13.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ak2qcpg98g7rq3zv6hk4lchkl6j2rpfzsa24psb1kn9hc1xy66p") (l "nvpair") (r "1.62")))

