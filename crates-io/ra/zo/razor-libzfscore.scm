(define-module (crates-io ra zo razor-libzfscore) #:use-module (crates-io))

(define-public crate-razor-libzfscore-0.6.1 (c (n "razor-libzfscore") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.6") (d #t) (k 0)))) (h "1an8yshkq0b9i422md3n23vshad8ccb0brwx3vsnr919p7hrqybf")))

(define-public crate-razor-libzfscore-0.7.0 (c (n "razor-libzfscore") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.7") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.7") (d #t) (k 0)))) (h "0lq09zjwjsy06m2flg3bhcccbhac2dwcbqdf2ajx24mfzdyjwch6") (r "1.62")))

(define-public crate-razor-libzfscore-0.8.0 (c (n "razor-libzfscore") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.8") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.8") (d #t) (k 0)))) (h "0ma99pbcgdbmycc7z5wkkak11wfqkq5f1jw1f642bzxv5ii07zdc") (r "1.62")))

(define-public crate-razor-libzfscore-0.9.0 (c (n "razor-libzfscore") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.9") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.9") (d #t) (k 0)))) (h "1xkz65lw62crf2by5mwjw0qawwvwj4jy5ajw0fjk8a5q0fpfvyic") (r "1.62")))

(define-public crate-razor-libzfscore-0.10.0 (c (n "razor-libzfscore") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.10") (d #t) (k 0)))) (h "0fz6d5kn635bz8yz19yzbg41y9q3wqhmd59fzb4kjv2kmjjr9mi2") (r "1.62")))

(define-public crate-razor-libzfscore-0.10.1 (c (n "razor-libzfscore") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.10") (d #t) (k 0)))) (h "1vj0hs09yk1fzh6gliyl239nyvv7nvc8snnyjrq1bmrp7x7rri54") (r "1.62")))

(define-public crate-razor-libzfscore-0.11.0 (c (n "razor-libzfscore") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.11") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.11") (d #t) (k 0)))) (h "1rqcjfv0j9dgnnxjj4mjy27gnmx60cjh2949i8an0l932nq5k7nn") (f (quote (("wait")))) (r "1.62")))

(define-public crate-razor-libzfscore-0.12.0 (c (n "razor-libzfscore") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.12") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.12") (d #t) (k 0)))) (h "1klz4v5b7aqs4s5yk5krxcwkrzjg7jgzsvg7kmd28ympg9f2q1b3") (f (quote (("wait")))) (r "1.62")))

(define-public crate-razor-libzfscore-0.13.0 (c (n "razor-libzfscore") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.13") (d #t) (k 0)))) (h "0s3k1bpj6ngs708wsk2qiq0a6my3w04zhx11llxz4hw6k4akgm33") (f (quote (("wait")))) (r "1.62")))

(define-public crate-razor-libzfscore-0.13.1 (c (n "razor-libzfscore") (v "0.13.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)) (d (n "razor-libzfscore-sys") (r "^0.13") (d #t) (k 0)))) (h "0lmbb5vr7wyvnwv931ifx4rayv2xwkzndprmaqapj90zyk8xmsr6") (f (quote (("wait")))) (r "1.62")))

