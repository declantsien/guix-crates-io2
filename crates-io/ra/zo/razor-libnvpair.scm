(define-module (crates-io ra zo razor-libnvpair) #:use-module (crates-io))

(define-public crate-razor-libnvpair-0.3.1 (c (n "razor-libnvpair") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wwyfajghl1672h9xaldzyg0ks9j2fwyjb1702a65wv4fxd5r407")))

(define-public crate-razor-libnvpair-0.4.0 (c (n "razor-libnvpair") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rv5pm8wvg6vq6flmmq002989436k9ax9vrryjnxfjfj1s9fi5w9")))

(define-public crate-razor-libnvpair-0.4.1 (c (n "razor-libnvpair") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xrkk43j7lar8hhax9z582h3f7arwc7pm2mdlzsjj77v1vanq7m4")))

(define-public crate-razor-libnvpair-0.5.0 (c (n "razor-libnvpair") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "085s9gp2lwkwn225chqsgkbpkjbiaz187lrk1zkwgk58034pgwqq")))

(define-public crate-razor-libnvpair-0.6.0 (c (n "razor-libnvpair") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mscdc4gy7jmv9i70qjfg13rybh7bi81yj3b6zdksawr6f3i8sdk")))

(define-public crate-razor-libnvpair-0.6.1 (c (n "razor-libnvpair") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cfw2mq23sm2zf55zv5i3fzc6i52ygs5pdqmsvqzw1bilq7xflqa")))

(define-public crate-razor-libnvpair-0.7.0 (c (n "razor-libnvpair") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nxpps19bak71q7wvv4r56xxwi0qrpqhajjv9sjn54nrfaj16ns0") (r "1.62")))

(define-public crate-razor-libnvpair-0.8.0 (c (n "razor-libnvpair") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zyggjf1jjd0ym3kagm9lyjijgzxyfl1gn51nc56ignp2sq4wc68") (r "1.62")))

(define-public crate-razor-libnvpair-0.9.0 (c (n "razor-libnvpair") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07z5jz4dvwf72mba7ds8wbz9lwrrhlz04nrnalpqcps9y34s5gvy") (r "1.62")))

(define-public crate-razor-libnvpair-0.10.0 (c (n "razor-libnvpair") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19h5qrxwngqsyqh29w17nnd03wpw1gbmhqcaiji743xdxdv7pwzr") (r "1.62")))

(define-public crate-razor-libnvpair-0.10.1 (c (n "razor-libnvpair") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s4m0pvawd40p7ik8saawi0k0k5zff6ahyvh1j2nlz1w3l5jaj8n") (r "1.62")))

(define-public crate-razor-libnvpair-0.11.0 (c (n "razor-libnvpair") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d6winzqq3m77gnv1xlk5i4rbfpmgk0j0jmmv7kml0ajcgxr5hrs") (r "1.62")))

(define-public crate-razor-libnvpair-0.12.0 (c (n "razor-libnvpair") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mdgmy3qnhqkv0byd005ns4yv15c3xjm3yq5y6b66yqwzsw2ryrq") (r "1.62")))

(define-public crate-razor-libnvpair-0.13.0 (c (n "razor-libnvpair") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nfa0xlfb34sz9cavnbrhb82x60manir53lky9mnlpimfc5cbh83") (r "1.62")))

(define-public crate-razor-libnvpair-0.13.1 (c (n "razor-libnvpair") (v "0.13.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair-sys") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02la1x159x9pb5828jwby7w1s2l5s847bg3g8l24lidwv49capqg") (r "1.62")))

