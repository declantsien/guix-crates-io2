(define-module (crates-io ra zo razor-libzfscore-sys) #:use-module (crates-io))

(define-public crate-razor-libzfscore-sys-0.4.1 (c (n "razor-libzfscore-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.4") (d #t) (k 0)))) (h "1h1nvkgfs4rh8ajwihg8kqqgf8hyr6gmpcidxqsxv5b6d1n52jzw") (l "zfs_core")))

(define-public crate-razor-libzfscore-sys-0.5.0 (c (n "razor-libzfscore-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.5") (d #t) (k 0)))) (h "1ynl5pqi3snaicdslifsw9g57hflvw2flch4c0hcvpv6diq2kkfx") (l "zfs_core")))

(define-public crate-razor-libzfscore-sys-0.6.0 (c (n "razor-libzfscore-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)))) (h "1lw7amap4qax47w3ls96ppifyv0hwcrxk9r367hnf3z6yhlqvxnk") (l "zfs_core")))

(define-public crate-razor-libzfscore-sys-0.6.1 (c (n "razor-libzfscore-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)))) (h "0w2gddvc7l8vs56xjq5r7mnljr7ca1207w16pgm1n4p6b1h1dkz0") (l "zfs_core")))

(define-public crate-razor-libzfscore-sys-0.7.0 (c (n "razor-libzfscore-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.7") (d #t) (k 0)))) (h "06id66q6frq43vjsq165pfps2rbh18jn0sc7cf0934l4v76lwb1y") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.8.0 (c (n "razor-libzfscore-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.8") (d #t) (k 0)))) (h "0l13qhwxy2f1r8cppv1pbz6nim4p3kviw4pjx7mkh9d1v5ip9cv0") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.9.0 (c (n "razor-libzfscore-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.9") (d #t) (k 0)))) (h "14hlh0yhy9gci7ib5z7hmr46x38cnjs6n7bawav3rc50ql6adlzr") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.10.0 (c (n "razor-libzfscore-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)))) (h "1gqi5n1lj9s8b2xg6qwy995g058qs7djbwsxlmpxnywvjlzk4vz9") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.10.1 (c (n "razor-libzfscore-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)))) (h "1bq1wv6z9y4f5vahakzbm6lp0sqhx57dykbqlba0yc9k8b9j1fmx") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.11.0 (c (n "razor-libzfscore-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.11") (d #t) (k 0)))) (h "190fhcf4kgla9vxzwr1zvxzlcyl1cagcr8lcskv7pqdbp7l10nh0") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.12.0 (c (n "razor-libzfscore-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.12") (d #t) (k 0)))) (h "13j3rqcdk0lf96qaw20hgnx1mj13w2ic4njb8r0lh4qyyikawhk8") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.13.0 (c (n "razor-libzfscore-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)))) (h "03fh68yzlx30rgfc1vrwzpjn2pj0gsm5n033gq5sx7s3s1g2rpm3") (l "zfs_core") (r "1.62")))

(define-public crate-razor-libzfscore-sys-0.13.1 (c (n "razor-libzfscore-sys") (v "0.13.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)))) (h "0jvsz43rhy7jqjgj38ajqj7ksafk1jhajaqwi8s4i8hn5asxb5ms") (l "zfs_core") (r "1.62")))

