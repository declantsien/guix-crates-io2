(define-module (crates-io ra zo razor-libzfs-sys) #:use-module (crates-io))

(define-public crate-razor-libzfs-sys-0.6.1 (c (n "razor-libzfs-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)) (d (n "razor-libzfscore") (r "^0.6") (d #t) (k 0)))) (h "0y8sr4dwbsz0z18gcj763g21730r5ayk7xbhrl84ykwqx8hbq2dk") (l "zfs")))

(define-public crate-razor-libzfs-sys-0.7.0 (c (n "razor-libzfs-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.7") (d #t) (k 0)) (d (n "razor-libzfscore") (r "^0.7") (d #t) (k 0)))) (h "1i6l27f8kqmz5h5zwb9mq6v63crbc4nw3z1mbqdcf6yc4yr2qwmv") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.8.0 (c (n "razor-libzfs-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.8") (d #t) (k 0)) (d (n "razor-libzfscore") (r "^0.8") (d #t) (k 0)))) (h "1vicp295ipglvywhjkjb3zqjzwig3qa5rxzzfyi6gnnx0svifgvs") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.9.0 (c (n "razor-libzfs-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.9") (d #t) (k 0)))) (h "13y8i2zz7babq8w2niwgip0c81a8wq9agbrzk9c52k3qrm6sbfj6") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.10.0 (c (n "razor-libzfs-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)))) (h "1myd40qj7sxw1gz0gz4k14dzvwjinxqg5m18zprl26g6hsb88fgx") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.10.1 (c (n "razor-libzfs-sys") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)))) (h "1dkl9q6i88hsi2hvfp80lss1jrg40hfmx9d2jw7519hmzfcksirw") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.11.0 (c (n "razor-libzfs-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.11") (d #t) (k 0)))) (h "164nwy9sr5mi9b6cbfaixa9xppp2qjpdlk6iz367cq11c4j5ylsf") (l "zfs") (r "1.62")))

(define-public crate-razor-libzfs-sys-0.12.0 (c (n "razor-libzfs-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "razor-libnvpair") (r "^0.12") (d #t) (k 0)))) (h "01b9f7ds2igc85p27y3rhxd6ypy7z10mjqqypy54zrk9w6da7rrs") (l "zfs") (r "1.62")))

