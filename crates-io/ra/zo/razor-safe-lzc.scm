(define-module (crates-io ra zo razor-safe-lzc) #:use-module (crates-io))

(define-public crate-razor-safe-lzc-0.11.0 (c (n "razor-safe-lzc") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libzfscore") (r "^0.11") (d #t) (k 0)) (d (n "razor-nvpair") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mp3mm22827bhpxd5grvn77xqq9g71c03xw6kiwn0nyg6jkp1z77") (r "1.62")))

(define-public crate-razor-safe-lzc-0.12.0 (c (n "razor-safe-lzc") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libzfscore") (r "^0.12") (d #t) (k 0)) (d (n "razor-nvpair") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hikzrh1gk7zf3w33lkwrapw0pnf9vwjq66znf3kla1vwqmz9g0y") (r "1.62")))

