(define-module (crates-io ra zo razor-nvpair) #:use-module (crates-io))

(define-public crate-razor-nvpair-0.4.0 (c (n "razor-nvpair") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mfda28y8q02byys48xkqpbwxs3n3m5b4ak7655z6gg8ymcl5aj9")))

(define-public crate-razor-nvpair-0.4.1 (c (n "razor-nvpair") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wk1ffmkzcnwn81fyzfwispqzbkkrk35ap8xmzpy568xwx3625x5")))

(define-public crate-razor-nvpair-0.5.0 (c (n "razor-nvpair") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nwg9mj71yzz3w693bihdizh4kz5ljssg1lq75j4lpi1r6ja5pva")))

(define-public crate-razor-nvpair-0.6.0 (c (n "razor-nvpair") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m877vxdg8jfryxs50wdny5qpbfndmkra07k7i1zazjpiznsb7r1")))

(define-public crate-razor-nvpair-0.6.1 (c (n "razor-nvpair") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mmdvcr1ngwjlbgj6igzs2wgn43zmid3klikmwssdvz1f2vhfgp1")))

(define-public crate-razor-nvpair-0.7.0 (c (n "razor-nvpair") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01sixhn94w6xkayzy480w9ayjfya31gfs881444xl3mfzz0ipw2d") (r "1.62")))

(define-public crate-razor-nvpair-0.8.0 (c (n "razor-nvpair") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gzwcfvf4bhh551whn689h9xvwyfivfkli2lm3qgn0xv3avsdcxp") (r "1.62")))

(define-public crate-razor-nvpair-0.9.0 (c (n "razor-nvpair") (v "0.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fp6mxbnbfp4bj4p275w7p09gzqskjxgcbamhs7mwpll4agrkpmf") (r "1.62")))

(define-public crate-razor-nvpair-0.10.0 (c (n "razor-nvpair") (v "0.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jbj18xg6f1j6fyg29asr6371vhc1q1al9fcddsiji6myr9vj4aq") (r "1.62")))

(define-public crate-razor-nvpair-0.10.1 (c (n "razor-nvpair") (v "0.10.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15ijcv0d4b8khyn9p4mvcwn7k4rvd8703hh3dw7czqjhmzivkncr") (r "1.62")))

(define-public crate-razor-nvpair-0.11.0 (c (n "razor-nvpair") (v "0.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gd447726xm08ck14kxdndh3f4bx26jn29494yn8295gk9isahw3") (r "1.62")))

(define-public crate-razor-nvpair-0.12.0 (c (n "razor-nvpair") (v "0.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cqg4z8g522ih72vmgpi92jm2c9zhcxld2ihlhl1psl3vr8nxzjq") (r "1.62")))

(define-public crate-razor-nvpair-0.13.0 (c (n "razor-nvpair") (v "0.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05wp1q4h2sz8hfsz2rs9zgsr5b7z899jn84yz65xycmpr68wpkk0") (r "1.62")))

(define-public crate-razor-nvpair-0.13.1 (c (n "razor-nvpair") (v "0.13.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "razor-libnvpair") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06cs2ly0gc73fxjv7lm17c3i10wpvpv2k9m0y5anf3pmbmbhyhyn") (r "1.62")))

