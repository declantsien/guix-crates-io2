(define-module (crates-io ra zo razorback) #:use-module (crates-io))

(define-public crate-razorback-0.1.0 (c (n "razorback") (v "0.1.0") (d (list (d (n "cortex-m-rtic") (r "^0.6.0-alpha.4") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.6") (d #t) (k 0)) (d (n "stm32f4") (r "^0.13.0") (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.9.0") (f (quote ("stm32f407"))) (d #t) (k 0)))) (h "190dyhyc19lg21avcaglqlpgakbr4cjsmjnljqgd3krs1yashk9b")))

