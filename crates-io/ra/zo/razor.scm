(define-module (crates-io ra zo razor) #:use-module (crates-io))

(define-public crate-razor-0.1.0 (c (n "razor") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "razor-chase") (r "^0.1.0") (d #t) (k 0)) (d (n "razor-fol") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1j3ryv4vlnq2677525ykd5z9p2i681mpgr4zpwyjqs04zb6rj3m3")))

