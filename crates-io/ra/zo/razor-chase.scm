(define-module (crates-io ra zo razor-chase) #:use-module (crates-io))

(define-public crate-razor-chase-0.1.0 (c (n "razor-chase") (v "0.1.0") (d (list (d (n "criterion") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "razor-fol") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1y9zpm1ylwkikwmq3kznwl5i0b9ja1mz4cyw90axiznh16f7l4wa")))

