(define-module (crates-io ra zo razor-fol) #:use-module (crates-io))

(define-public crate-razor-fol-0.1.0 (c (n "razor-fol") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "nom_locate") (r "^0.3.1") (d #t) (k 0)))) (h "0pizvvhsq7jdq9ncgyaw0z18mqaznf6k20n9pz956sdz7c8mx7lp")))

