(define-module (crates-io ra yd raydium-contract-instructions-tp) #:use-module (crates-io))

(define-public crate-raydium-contract-instructions-tp-0.1.0 (c (n "raydium-contract-instructions-tp") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.10.31") (d #t) (k 0)) (d (n "spl-token") (r "^3.5.0") (d #t) (k 0)))) (h "0dc6a3l5anwl27kcbnf9ixrlks7s1skppqdy0n5js7931d3zgvnl")))

