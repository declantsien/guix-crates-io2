(define-module (crates-io ra ce racer-testutils) #:use-module (crates-io))

(define-public crate-racer-testutils-0.1.0 (c (n "racer-testutils") (v "0.1.0") (d (list (d (n "racer") (r "^2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1q91hva2m6v6l3i572bf91xgvsrxic7lvj2j0yigya1r4jpn3cmd")))

