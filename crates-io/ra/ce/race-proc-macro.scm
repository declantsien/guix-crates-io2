(define-module (crates-io ra ce race-proc-macro) #:use-module (crates-io))

(define-public crate-race-proc-macro-0.0.1 (c (n "race-proc-macro") (v "0.0.1") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0377wbyza49z76zrfkkyjq5m60jq1kc856p5qqhzrl8w572shg35") (r "1.69.0")))

(define-public crate-race-proc-macro-0.0.2 (c (n "race-proc-macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0pcc067lzjdiqmrskz9afcjd17l0d4z42dpcc407lk0am2gk4q97") (r "1.69.0")))

(define-public crate-race-proc-macro-0.0.3 (c (n "race-proc-macro") (v "0.0.3") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r "^0.0.2") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0bnh6j0vqi6hhyxh19shv0yc8gfbva71imxwfgkxjyfjjqlh27in") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.4 (c (n "race-proc-macro") (v "0.0.4") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "09lmjjfqfmzfy519bv17m83w8gyqb6xm7s857f9pw9sc7y1l5lj2") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.5 (c (n "race-proc-macro") (v "0.0.5") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "17nd002pv35jhm9lpacc8cm8yzzhsa3r2mrvm5jmik54mya0c0h1") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.7 (c (n "race-proc-macro") (v "0.0.7") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.7") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "11s1sn10vpff3gkbxjn4dwcb8gmx2x3lxycrgn6c0cswnybj5is5") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.8 (c (n "race-proc-macro") (v "0.0.8") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.8") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "09w8s37vcd45qany1fm2bpyjm1m7pds1qf3cgyzgaxvyz14g4rcr") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.9 (c (n "race-proc-macro") (v "0.0.9") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.9") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "18zyj8vyg66x9f3142c7gwxj5lglhq1xrk3qnc0jyl5x3kh0sqa4") (r "1.65.0")))

(define-public crate-race-proc-macro-0.0.10 (c (n "race-proc-macro") (v "0.0.10") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1s684sql9d9q0ymmkr8ai0ijgc3c7bj24hiwnyx5ghm7bqs80jf1") (r "1.65.0")))

(define-public crate-race-proc-macro-0.1.0 (c (n "race-proc-macro") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-core") (r ">=0.1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "010c4g67fm3zlaxf4wi29d96jhs3ffrny1141ax9awya1jx2x45s") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.0 (c (n "race-proc-macro") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5h9kjwvggn7aazp16iy928nkgi4ilgl15q46h38mr0ms775fvr") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.1 (c (n "race-proc-macro") (v "0.2.1") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.1") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1jqixw92s1vzpjbxs9sliivp1a4ncfl0kyf0a7gnf5vsb5v9q74q") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.2 (c (n "race-proc-macro") (v "0.2.2") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.2") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "165yb012m39y29y2737g2midic7xzdw5dqg0rjkc69yq5brspwlv") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.3 (c (n "race-proc-macro") (v "0.2.3") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0drf2bp82jc73mx3cp9ppawhsldbr0p6i50lbg13if540nymsvll") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.4 (c (n "race-proc-macro") (v "0.2.4") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.4") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "11yjdq7qcd8x68grhia03ddq5aqn1nmfpgpg8di7ln5xlmk9kcw1") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.5 (c (n "race-proc-macro") (v "0.2.5") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.5") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "096yyvpk8a35jifl01ilawl42bb45xm479x76w5yxa7zvcr8k7fd") (r "1.65.0")))

(define-public crate-race-proc-macro-0.2.6 (c (n "race-proc-macro") (v "0.2.6") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "race-api") (r ">=0.2.6") (d #t) (k 2)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "15pi4jwwg127k7awf134cpib9azfmklg5385grc32ccc4m60mdgx") (r "1.65.0")))

