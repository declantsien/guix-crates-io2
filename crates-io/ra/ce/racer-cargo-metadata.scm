(define-module (crates-io ra ce racer-cargo-metadata) #:use-module (crates-io))

(define-public crate-racer-cargo-metadata-0.1.0 (c (n "racer-cargo-metadata") (v "0.1.0") (d (list (d (n "racer-interner") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yxha6as3hlmvgqp1lch96z14q2gi9lxvrd384fix75xivh7nv0v")))

(define-public crate-racer-cargo-metadata-0.1.1 (c (n "racer-cargo-metadata") (v "0.1.1") (d (list (d (n "racer-interner") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vvwbfi991gjbk2k9a7yl7fqc8amvwlf7sa9lsx1sr0s55rcsq1b")))

(define-public crate-racer-cargo-metadata-0.1.2 (c (n "racer-cargo-metadata") (v "0.1.2") (d (list (d (n "racer-interner") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ay3l0fkv70n8y6rpafpwcfyz08mg4ariaxk4kghdcvs103nsvlr")))

