(define-module (crates-io ra ce racetrack-proc-macro) #:use-module (crates-io))

(define-public crate-racetrack-proc-macro-0.0.1 (c (n "racetrack-proc-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rz4bi58a2wwy8h3pkjzwkimqh0gcwc5nn0rqm8bdhgkbq7n8mjq") (f (quote (("nightly"))))))

