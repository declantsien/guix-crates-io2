(define-module (crates-io ra ce racetrack) #:use-module (crates-io))

(define-public crate-racetrack-0.0.1 (c (n "racetrack") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "racetrack-proc-macro") (r "^0.0.1") (d #t) (k 0)))) (h "0vmxjlfcz5m7dsys04clra5mwd8yhxc3z6scq9h2qvlfi4ay7dx8") (y #t)))

(define-public crate-racetrack-0.0.2 (c (n "racetrack") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "racetrack-proc-macro") (r "^0.0.1") (d #t) (k 0)))) (h "0nwwnaqmwyicsfsvqxj44nw0rbkf31b5hv908dl3a93gy7xr25mh")))

