(define-module (crates-io ra ce race-client) #:use-module (crates-io))

(define-public crate-race-client-0.0.3 (c (n "race-client") (v "0.0.3") (d (list (d (n "race-core") (r "^0.0.2") (d #t) (k 0)))) (h "18jzzgd2yir6h00rh90bd62h3ihby0kkxbkqhzs02kagl5bfhd0v") (r "1.65.0")))

(define-public crate-race-client-0.0.4 (c (n "race-client") (v "0.0.4") (d (list (d (n "race-core") (r ">=0.0.3") (d #t) (k 0)))) (h "1hpmjr3bk43r0pwbvl9cfc8y4rnpxcsm7phsyl7fc3xvg2kb9vn1") (r "1.65.0")))

(define-public crate-race-client-0.0.5 (c (n "race-client") (v "0.0.5") (d (list (d (n "race-core") (r ">=0.0.4") (d #t) (k 0)))) (h "1fin5vzgsm91ngbyvl5crd9a57cmkfjpki72w9gm8l847hiplini") (r "1.65.0")))

(define-public crate-race-client-0.0.6 (c (n "race-client") (v "0.0.6") (d (list (d (n "race-core") (r ">=0.0.4") (d #t) (k 0)))) (h "1fkys598zkmmg07579yk8kkvxx3wd0sa9d27bz89i5q86ssm94f9") (r "1.65.0")))

(define-public crate-race-client-0.0.7 (c (n "race-client") (v "0.0.7") (d (list (d (n "race-core") (r ">=0.0.7") (d #t) (k 0)))) (h "1xvzcz5dj24xjhh1ha49q60rp8shnw9ylnhxfbf9pbj5z7b3m803") (r "1.65.0")))

(define-public crate-race-client-0.0.8 (c (n "race-client") (v "0.0.8") (d (list (d (n "race-core") (r ">=0.0.8") (d #t) (k 0)))) (h "1yswm5byw2l49pijg5j1jl2v8p27wq9dshysm3z3mlihcc11a0cr") (r "1.65.0")))

(define-public crate-race-client-0.0.9 (c (n "race-client") (v "0.0.9") (d (list (d (n "race-core") (r ">=0.0.9") (d #t) (k 0)))) (h "1n8yqxnmbr78dwzlj3c85rwwrj6ld6l8l3hdyx7sdivqgs14dlr1") (r "1.65.0")))

(define-public crate-race-client-0.2.0 (c (n "race-client") (v "0.2.0") (d (list (d (n "race-api") (r ">=0.2.0") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.0") (d #t) (k 0)))) (h "01bg2ikcbyx49qcn7ypggmn45m5z29khv1bfx990j500fgjq5pv0") (r "1.65.0")))

(define-public crate-race-client-0.2.1 (c (n "race-client") (v "0.2.1") (d (list (d (n "race-api") (r ">=0.2.1") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.1") (d #t) (k 0)))) (h "1y20894lkpgrhv987pl5mnhg6g36aq9818ddf30frlaqj3aff775") (r "1.65.0")))

(define-public crate-race-client-0.2.2 (c (n "race-client") (v "0.2.2") (d (list (d (n "race-api") (r ">=0.2.2") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.2") (d #t) (k 0)))) (h "1dsyy22l7dxc4793drvklykqrwar3sihb4kd5qcsj87qfa9g2vb5") (r "1.65.0")))

(define-public crate-race-client-0.2.3 (c (n "race-client") (v "0.2.3") (d (list (d (n "race-api") (r ">=0.2.3") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.3") (d #t) (k 0)))) (h "0w208qzfqzv6iij5f9bdjkmrxmbsriqz24mfxz0rq4czv3ikzk9z") (r "1.65.0")))

(define-public crate-race-client-0.2.4 (c (n "race-client") (v "0.2.4") (d (list (d (n "race-api") (r ">=0.2.4") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.4") (d #t) (k 0)))) (h "1jfpq4sv67y51ql03khw2698dsx4dpcsibbh0hy7psd871jq3jsq") (r "1.65.0")))

(define-public crate-race-client-0.2.5 (c (n "race-client") (v "0.2.5") (d (list (d (n "race-api") (r ">=0.2.5") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.5") (d #t) (k 0)))) (h "0psg6gx6r30mkd18d9iqgv9nff6d1vqaaawdd2cp50h058wywc40") (r "1.65.0")))

(define-public crate-race-client-0.2.6 (c (n "race-client") (v "0.2.6") (d (list (d (n "race-api") (r ">=0.2.6") (d #t) (k 0)) (d (n "race-core") (r ">=0.2.6") (d #t) (k 0)))) (h "0mxl10gmyvm3f71pv4r5007qd24xl11dnsqmmg8h9nzx781id2mr") (r "1.65.0")))

