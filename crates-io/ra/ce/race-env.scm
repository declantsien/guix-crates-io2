(define-module (crates-io ra ce race-env) #:use-module (crates-io))

(define-public crate-race-env-0.0.1 (c (n "race-env") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0ipd03rj67miwaf5gn02lfvks37j20kf6359phyxn77jby4g03v9") (r "1.69.0")))

(define-public crate-race-env-0.0.2 (c (n "race-env") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0rmjmcgxydyf64053z16ycx6s4cqac9n59ikdikwfjys9yy7qcvd") (r "1.69.0")))

(define-public crate-race-env-0.0.3 (c (n "race-env") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1c1vq1gr1gdshf48qjnk8jhbhmjbj6ls2bgwy77nw6hzi5gyp2yi") (r "1.65.0")))

(define-public crate-race-env-0.0.4 (c (n "race-env") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "097423q0k2596vhbyrlfnq8b2v71g752i4hkz7d5ag3yqlsxzwd6") (r "1.65.0")))

(define-public crate-race-env-0.0.5 (c (n "race-env") (v "0.0.5") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "12mgbhnjish64rf7y851mh31frr1mwnzyicqlrn7ri49rb1hv6n1") (r "1.65.0")))

(define-public crate-race-env-0.0.6 (c (n "race-env") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "040252z5ca4sagxv9rrfi5j971q2ssmvlcq58vkwprr77a8r10w7") (r "1.65.0")))

(define-public crate-race-env-0.0.7 (c (n "race-env") (v "0.0.7") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "01qlqxf5xx208q3dgg3zllscwbcwzq5cxalzd7zvb5s6m2d3pkh7") (r "1.65.0")))

(define-public crate-race-env-0.0.8 (c (n "race-env") (v "0.0.8") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "03g5ir4nwhmsihwaf4gwlrmvlwrccd2zrbf67vbrsq64vpas7zbc") (r "1.65.0")))

(define-public crate-race-env-0.0.9 (c (n "race-env") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "0c4z4c8l0qfv7gjz8znvpjjzcf63bj3ri72vbjwvc66lkz8l0dfh") (r "1.65.0")))

(define-public crate-race-env-0.0.10 (c (n "race-env") (v "0.0.10") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "00adbjz50xc1iwascblrh3w3cax2m48zwywaklnv7yf6zhlxilbr") (r "1.65.0")))

(define-public crate-race-env-0.1.0 (c (n "race-env") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)))) (h "1zmw3nadrm1fpncv7y96j4a77inkdprjn99xqaxirj5kdjxfl917") (r "1.65.0")))

