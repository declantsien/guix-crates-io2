(define-module (crates-io ra ii raii_flock) #:use-module (crates-io))

(define-public crate-raii_flock-0.1.0 (c (n "raii_flock") (v "0.1.0") (d (list (d (n "fs2") (r "^0") (d #t) (k 0)))) (h "0lq2ppqswvi0afrn7nf59k11815bk3cbviiwavvi0vaz09i6mf4l")))

(define-public crate-raii_flock-0.2.0 (c (n "raii_flock") (v "0.2.0") (d (list (d (n "fs2") (r "^0") (d #t) (k 0)))) (h "1zqhm7vz69nkzsg8a4fqajiirjjnmdjkcsbxh92wgk4z1qgj6vv4")))

