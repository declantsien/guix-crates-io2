(define-module (crates-io ra ii raii-counter-futures) #:use-module (crates-io))

(define-public crate-raii-counter-futures-0.1.0 (c (n "raii-counter-futures") (v "0.1.0") (d (list (d (n "futures-intrusive") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "10571d6pc8rgc4fa6smnfy2s52jflrmmnm1hmqw7d45q2x6gjbsh")))

