(define-module (crates-io ra ii raii-change-tracker) #:use-module (crates-io))

(define-public crate-raii-change-tracker-0.1.0 (c (n "raii-change-tracker") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "111l2aggzq2hs5h6p8zgddhj7fvlyhkm488pqg1kgybkaswskxxj")))

(define-public crate-raii-change-tracker-0.1.1 (c (n "raii-change-tracker") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "1h1x3925piyynszf3r03dvhzzirz1mzv0pc2b0k5l38kjwli7bfx")))

