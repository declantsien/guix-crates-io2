(define-module (crates-io ra ii raii-counter) #:use-module (crates-io))

(define-public crate-raii-counter-0.1.0 (c (n "raii-counter") (v "0.1.0") (h "0hn6gn1nyp0xa73q8bkspp0ry115sd23ckhg47zpclwwsbcw3hd2")))

(define-public crate-raii-counter-0.1.1 (c (n "raii-counter") (v "0.1.1") (h "0cqfhx6c6vgxv81l05k29rbpwcrsywypg9j9z5vpgk2dz9ig9nql")))

(define-public crate-raii-counter-0.2.0 (c (n "raii-counter") (v "0.2.0") (h "1g92clvzdl45999agvqbpahx1wydygmaf0k35vn3w24hpdpz3108")))

(define-public crate-raii-counter-0.2.1 (c (n "raii-counter") (v "0.2.1") (h "1r6kz717fdmz6jylg1iqzh4g1kpdi0q0rh2ybfjzmhfmizmwjb6a")))

(define-public crate-raii-counter-0.3.1 (c (n "raii-counter") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gmbgy9nhhcv5l8266vd27zqsgvjapwclp89002gnw6nrbkzq4iw")))

(define-public crate-raii-counter-0.4.1 (c (n "raii-counter") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xgs4yy79kpac3jjhgj1ymy2ky3b1xplviy4pb42p0il5l4l7ghq")))

