(define-module (crates-io ra gg ragged-buffer) #:use-module (crates-io))

(define-public crate-ragged-buffer-0.3.6 (c (n "ragged-buffer") (v "0.3.6") (d (list (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "16ivp2fnj0bm5w1sv6m7h8rq4mjymlr5xrgpk260k6rwv4yi5zqb")))

(define-public crate-ragged-buffer-0.3.7 (c (n "ragged-buffer") (v "0.3.7") (d (list (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "17yq56qvl3kawl3sxg0q5czsvd5x9g42nq2bnypzzr3sxhrl5wap")))

(define-public crate-ragged-buffer-0.3.8 (c (n "ragged-buffer") (v "0.3.8") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.15") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1wsagm9rb66y39ndfymrjbv92r1v0apbzafcsidjgy84bwbrr9s9") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.0 (c (n "ragged-buffer") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0ffa6i961im13rh0h80lwyycjfxhv51b041ixr1zzv7cwgynjdb3") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.1 (c (n "ragged-buffer") (v "0.4.1") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0lbzjxq9694ms3dcf8jygmhxq0lws4h59n6h0b6n5giajizcgvbg") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.2 (c (n "ragged-buffer") (v "0.4.2") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0acnibm9sk2fl3dz4mcgyq186zzb7m82d49a52lpdf0ys4s7j4js") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.3 (c (n "ragged-buffer") (v "0.4.3") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1kizhbgczl7mwcjdmqvlccsxc51msmlcq7svs3pmmspwq8ibspfn") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.7 (c (n "ragged-buffer") (v "0.4.7") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "072kkfrbcck2ikk555vdib03n4ka22213v2gnj23a19j0bw821hr") (f (quote (("python" "pyo3" "numpy"))))))

(define-public crate-ragged-buffer-0.4.8 (c (n "ragged-buffer") (v "0.4.8") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.16.2") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.16.5") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0zq68xm02b62g09kdmr31y9yzqqqq053wmqadw4r361iy9kwc9g1") (f (quote (("python" "pyo3" "numpy"))))))

