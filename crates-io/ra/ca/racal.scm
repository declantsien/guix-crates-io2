(define-module (crates-io ra ca racal) #:use-module (crates-io))

(define-public crate-racal-0.0.0-alpha.0 (c (n "racal") (v "0.0.0-alpha.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ak0s6mkxvmv5ixy20qhwh5paxwqx1w9mzd5hflp5dvy9jmjnmiy")))

(define-public crate-racal-0.1.0 (c (n "racal") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1khvg401cpmf5lh23naw410kfavx98gmzvahish5pz0yl5687kzv")))

(define-public crate-racal-0.2.0 (c (n "racal") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1lrma9rsc0glja6rsqxl2rs83x4dbf6ysjkmw521zy4ia22h5q5v") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.2.1 (c (n "racal") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bfm54qp7kkrc7nwcrw4i11jd39i1d2hgs8dz7y05y3b18rn687i") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.3.0 (c (n "racal") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0p9p3zxmcwn6llml5i0qjz7a9i5002qqq2r1cxs0w5956ms2rg3x") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.3.1 (c (n "racal") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1b182lak95p816av1xc69x5ndcvbjx3g9gzxd13jpjdrvkfwyp95") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.3.2 (c (n "racal") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xxkgc3585jk2m8c7q5c8wjr8qnkh6vkg4n9hkmdqp0fiawfx619") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.3.3 (c (n "racal") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1mjz5ixyqlcrjsp18panmgfz675qrasadayg5hhvycajjpgl6c1l") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

(define-public crate-racal-0.3.4 (c (n "racal") (v "0.3.4") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (o #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xk8119qyhwwazf7grhvldhnmdr8ppjckqgj71pp2hkp37s1w9na") (f (quote (("default")))) (s 2) (e (quote (("reqwest" "dep:reqwest" "async-trait"))))))

