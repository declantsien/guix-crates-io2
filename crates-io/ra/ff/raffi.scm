(define-module (crates-io ra ff raffi) #:use-module (crates-io))

(define-public crate-raffi-0.0.3 (c (n "raffi") (v "0.0.3") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1n915nvc3di8vd2a01mxb32jav5wwy4qxjpvgl0mk0dnhih415lv")))

(define-public crate-raffi-0.0.5 (c (n "raffi") (v "0.0.5") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1lyqif1jlgyn9f661g5sqg92ln01aih0xvqkp9ndq7icmq7p7a66")))

(define-public crate-raffi-0.0.6 (c (n "raffi") (v "0.0.6") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0nnaml3vdzny7if2mpyl5lxjs861w6c2ipwczvxpfsrqb2fkvvxz")))

(define-public crate-raffi-0.1.0 (c (n "raffi") (v "0.1.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0xzakfkwzb957mwp2bd7vc5fvia4f5gvav828z6604sdh6v622r8")))

(define-public crate-raffi-0.2.0 (c (n "raffi") (v "0.2.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "01whk9gi72wy0pfrz9dnir7j55hs7ld5cs6almbxrmm7fprmp7rc")))

(define-public crate-raffi-0.3.0 (c (n "raffi") (v "0.3.0") (d (list (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.27") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1aqp38svhs6xkxv80fyfrgmwnh2kfpqzmra6hm8w4iq5c6acmpa0")))

(define-public crate-raffi-0.4.0 (c (n "raffi") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "09z1y1g7pfw42crm77n5jgmqbqd67vvq5ilxwz1ll0hf5674yvga")))

