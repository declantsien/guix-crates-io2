(define-module (crates-io ra ff raffia_macro) #:use-module (crates-io))

(define-public crate-raffia_macro-0.1.0 (c (n "raffia_macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "147996w6a2dkcq87264h3c7szdxzw3pmaha408kdi8pxwk9pk5kk")))

