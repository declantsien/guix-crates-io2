(define-module (crates-io ra ff raffle) #:use-module (crates-io))

(define-public crate-raffle-0.0.1 (c (n "raffle") (v "0.0.1") (d (list (d (n "blake3") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0r9nw5bb3kbnwcf0nrc2s853w93lgnikak7hsjnsrcrfana59ab4") (f (quote (("default_features")))) (s 2) (e (quote (("serde" "dep:serde") ("prost" "dep:prost"))))))

