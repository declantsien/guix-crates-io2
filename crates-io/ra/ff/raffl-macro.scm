(define-module (crates-io ra ff raffl-macro) #:use-module (crates-io))

(define-public crate-raffl-macro-0.1.0 (c (n "raffl-macro") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1zzrsia7s0h0x7x212scf3jj8kp415gv0m5v1k5q6f35zj2k8v4w")))

(define-public crate-raffl-macro-0.1.1 (c (n "raffl-macro") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "1ika2kcwnhl67f87a4vjl085a5p78zq8zwj2hwayfmjxmk28qxx5")))

