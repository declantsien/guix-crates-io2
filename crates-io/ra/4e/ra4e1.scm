(define-module (crates-io ra #{4e}# ra4e1) #:use-module (crates-io))

(define-public crate-ra4e1-0.2.0 (c (n "ra4e1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "02ggn3bwy5hphz7s2ijl09yzp5s8ychkjx92m4s4l6qlw1f9xvvg") (f (quote (("rt" "cortex-m-rt/device"))))))

