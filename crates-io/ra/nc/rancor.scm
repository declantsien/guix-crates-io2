(define-module (crates-io ra nc rancor) #:use-module (crates-io))

(define-public crate-rancor-0.1.0-pre1 (c (n "rancor") (v "0.1.0-pre1") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "079zg1ainn6qrjlircg8zghc4n463z8mnv0v48z1rm2hja5p6l7r") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre2 (c (n "rancor") (v "0.1.0-pre2") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "0dvjmq2kjgrqrx7wblnjdj8dvpb9dwk6805a1nsbk10vz163z77j") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre3 (c (n "rancor") (v "0.1.0-pre3") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "1bdbmdabfvqsjpg8f1nm2dra593vjnz4kr8mnv7hfq08bgjd9mzf") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre4 (c (n "rancor") (v "0.1.0-pre4") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "08q35amp3yvzzjzv1y8gkb7g9jqcm68af1vz5wpvlamd4kh0wpm3") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre5 (c (n "rancor") (v "0.1.0-pre5") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "1z51l4karfxsx7rfv3df7jjv74038nyk23h566y8yy685rxwcfhb") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre6 (c (n "rancor") (v "0.1.0-pre6") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "1mqhhjq5myjrp47kzliiwgx5axhj4v07kpy8wbwg1pb7b3mv4zaq") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre7 (c (n "rancor") (v "0.1.0-pre7") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "1pccm3vkl24wyfp8p8i2pqcqmxmypb61ddci61bxb3qhys54j3b2") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-pre8 (c (n "rancor") (v "0.1.0-pre8") (d (list (d (n "ptr_meta") (r "^0.3.0-pre1") (k 0)))) (h "048y3gqw3pvy0s2602wxmgq8yajjsx89xdk5323v03v286ils6dp") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

(define-public crate-rancor-0.1.0-alpha.9 (c (n "rancor") (v "0.1.0-alpha.9") (d (list (d (n "ptr_meta") (r "^0.3.0-alpha.2") (k 0)))) (h "0ljvlah67dky1yxahpz9791anfqxk393mnmlamcd1y7xgqm7xbia") (f (quote (("std" "alloc" "ptr_meta/std") ("default" "std") ("alloc"))))))

