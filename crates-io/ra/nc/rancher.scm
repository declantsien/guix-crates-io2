(define-module (crates-io ra nc rancher) #:use-module (crates-io))

(define-public crate-rancher-0.1.0 (c (n "rancher") (v "0.1.0") (h "1j4zcd7plwlrlr5j0bqblbp6ddqdcx1g9fcf7fhy04dbvcdsf8gp")))

(define-public crate-rancher-0.1.1 (c (n "rancher") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06gbx9jfybws9spjyc4hvxj3hkhasixzc2kazjvykvw8pzn6a0mc")))

(define-public crate-rancher-0.1.2 (c (n "rancher") (v "0.1.2") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wi5h037h45crjr9y6nk70gbi7b407xj2x65zn59knhi5vk66cri")))

(define-public crate-rancher-0.1.3 (c (n "rancher") (v "0.1.3") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11bc2w7lfabx5g6jpz9xdhvdgkk3r0wrvdii7w40wma554vjzc63")))

(define-public crate-rancher-0.1.4 (c (n "rancher") (v "0.1.4") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06la9md8506mqpc7s88kmxxgyzdpi53ii4kn74yg1wczxfrypszg")))

(define-public crate-rancher-0.1.5 (c (n "rancher") (v "0.1.5") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1knp66hxr9ax7b6iw21dircxhbb91hgscbpzs5xx2346q823fih7")))

(define-public crate-rancher-0.1.6 (c (n "rancher") (v "0.1.6") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16y9i36y5y8kfcbcvjpnyrkfq39jc63m9w3cggrgh416fm3rx1jb")))

(define-public crate-rancher-0.2.0 (c (n "rancher") (v "0.2.0") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h02y1gif3hzg7qgp6dmwwg0zy1rqd53y5ibgwjq99s9w3h4c37b") (f (quote (("rustls" "reqwest/rustls") ("native-tls" "reqwest/native-tls") ("default" "native-tls"))))))

(define-public crate-rancher-0.2.1 (c (n "rancher") (v "0.2.1") (d (list (d (n "dirs-next") (r "^2") (d #t) (k 0)) (d (n "eyre") (r "^0") (d #t) (k 0)) (d (n "reqwest") (r "^0") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lfgzy93a9dlhhagx92mfsjdpg3py9an1h7gbv1iwplbdsaqjqli") (f (quote (("rustls" "reqwest/rustls") ("native-tls" "reqwest/native-tls") ("default" "native-tls"))))))

