(define-module (crates-io ra sn rasn-smi) #:use-module (crates-io))

(define-public crate-rasn-smi-0.3.0 (c (n "rasn-smi") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.3.0") (d #t) (k 0)))) (h "0f5dn5pgjyjcl4m46yj6i5ygsar026xnf8hharckkbn50m0ihvr2")))

(define-public crate-rasn-smi-0.3.1 (c (n "rasn-smi") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.3.0") (d #t) (k 0)))) (h "1x9fjr8djbdrzgfmvxz725a3jymndh47v7hca4rqipcrh25lqdz3")))

(define-public crate-rasn-smi-0.4.0 (c (n "rasn-smi") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.4.0") (d #t) (k 0)))) (h "1j0411xnfrqggfj5yv4qdma76hxfj6540ipdma0p5sj1kw9w96f9")))

(define-public crate-rasn-smi-0.5.0 (c (n "rasn-smi") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.5.0") (d #t) (k 0)))) (h "1sf7hxa3d0npsh066y8isyrapvkhalxzsw9i19f98nknl36b7gfl")))

(define-public crate-rasn-smi-0.6.0 (c (n "rasn-smi") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.6.0") (d #t) (k 0)))) (h "0dzayrw1f4ywx8sv3gzwgfj89x39whfny1b180k0cd2am62y6zrn")))

(define-public crate-rasn-smi-0.7.0 (c (n "rasn-smi") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rasn") (r "^0.7.0") (d #t) (k 0)))) (h "0azvs8lb64p9bpsb80cdbz7035sfbrnfq1hyzd7fidr4jw9qd580")))

(define-public crate-rasn-smi-0.8.2 (c (n "rasn-smi") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.8.1") (d #t) (k 0)))) (h "03x1mcx0n1v8x1pvmf2b9n1pr46capmf9z474vfnlrf5lvgrh33f")))

(define-public crate-rasn-smi-0.9.0 (c (n "rasn-smi") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.0") (d #t) (k 0)))) (h "0x3220f1sq0mm55am8qp3acqgv7hsj7fscfnnr2f8wzwm64bj7si")))

(define-public crate-rasn-smi-0.9.1 (c (n "rasn-smi") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.0") (d #t) (k 0)))) (h "0zyic2x74dac4f4qyj7mv3m47khwysr4xa9nmfn12y9qszilpgx5")))

(define-public crate-rasn-smi-0.9.2 (c (n "rasn-smi") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.2") (d #t) (k 0)))) (h "0wrpn05avng1v82027n8i4sgdlcfi2a08y6gmv9di8dpyiwxnw16")))

(define-public crate-rasn-smi-0.9.3 (c (n "rasn-smi") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.3") (d #t) (k 0)))) (h "0jpv62r3vvkw1rwifc0ayvxbzp9jv1b2w316gqanfl0fsinif3yy")))

(define-public crate-rasn-smi-0.9.4 (c (n "rasn-smi") (v "0.9.4") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.4") (d #t) (k 0)))) (h "1rlmm07i3irff2lbl1vjb5jv298817x9w9frk18jg8q39lk6n0im")))

(define-public crate-rasn-smi-0.9.5 (c (n "rasn-smi") (v "0.9.5") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.9.5") (d #t) (k 0)))) (h "07r2k201miglcjkn07iiqsbq65g80fbp6jix8gjw295masbakx9r")))

(define-public crate-rasn-smi-0.10.0 (c (n "rasn-smi") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.0") (d #t) (k 0)))) (h "1mi872kmj33k0ryc7x0bpbprw3b9slh9bkhd5m1cj488zzpya1a5")))

(define-public crate-rasn-smi-0.10.1 (c (n "rasn-smi") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.1") (d #t) (k 0)))) (h "0cgmmnjw3p6nn7v2xapv6jxyavkdl8m8dya3jdq486vjsi882ysy")))

(define-public crate-rasn-smi-0.10.2 (c (n "rasn-smi") (v "0.10.2") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.2") (d #t) (k 0)))) (h "1nsx72n820ngr1xsqdibm3j904m1hg857zj8prm0ijbryd2vpy0d")))

(define-public crate-rasn-smi-0.10.3 (c (n "rasn-smi") (v "0.10.3") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.3") (d #t) (k 0)))) (h "1yzb2p3fx3r8jqajp52g2pam5bbnwvhvgrpwjv2w8xn3m94v99zy")))

(define-public crate-rasn-smi-0.10.4 (c (n "rasn-smi") (v "0.10.4") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.4") (d #t) (k 0)))) (h "0gvaq63fbcdsva1c9c6yh1zkqdff3qln845dy7q7gsjck5hb4slk")))

(define-public crate-rasn-smi-0.10.5 (c (n "rasn-smi") (v "0.10.5") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.5") (d #t) (k 0)))) (h "01dlls3jn7c8gxhgv2wq38rbfkchdm3dway29g36x2c8c0qb9g74")))

(define-public crate-rasn-smi-0.10.6 (c (n "rasn-smi") (v "0.10.6") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.10.6") (d #t) (k 0)))) (h "0xjdb6cg2siswykmxrllgir5ibmm83mvwask50mfs91fpxdm6bpd")))

(define-public crate-rasn-smi-0.11.0 (c (n "rasn-smi") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.11.0") (d #t) (k 0)))) (h "1gnzzwwryaa3v3dsr3g329cg6a77qiv9y75fdrhx7agyaxvp1n91")))

(define-public crate-rasn-smi-0.11.1 (c (n "rasn-smi") (v "0.11.1") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.11.1") (d #t) (k 0)))) (h "15n111dzgl6jwgbwf2sqjlfyrswqpy5fgxi300ykhgansrikak9w")))

(define-public crate-rasn-smi-0.12.0 (c (n "rasn-smi") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.0") (d #t) (k 0)))) (h "13y1gzwibhdf3hljx0fn9pw64li92xvwiz81g121kmjchvss5pdp")))

(define-public crate-rasn-smi-0.12.1 (c (n "rasn-smi") (v "0.12.1") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.1") (d #t) (k 0)))) (h "048n20rf080ws34jmq87pc7h55838b30db26dmrs9v3h9lhhcmm2")))

(define-public crate-rasn-smi-0.12.2 (c (n "rasn-smi") (v "0.12.2") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.2") (d #t) (k 0)))) (h "1klh1yk7w4h4xj3sj1pi0fbrba0iwn5q7njjsay85g5h7c4jmlcv")))

(define-public crate-rasn-smi-0.12.3 (c (n "rasn-smi") (v "0.12.3") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.3") (d #t) (k 0)))) (h "1zsj0y205dj1v6b3m3wjcdhw7w4nhkrizbpbk9qhi4j3rnllwap7")))

(define-public crate-rasn-smi-0.12.4 (c (n "rasn-smi") (v "0.12.4") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.4") (d #t) (k 0)))) (h "1j0v2dzzvhk6c5cgia0jdjb5nzmj0hkag9jrybcqxl4l6ljjvc9h")))

(define-public crate-rasn-smi-0.12.5 (c (n "rasn-smi") (v "0.12.5") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.5") (d #t) (k 0)))) (h "0v81d104xv1w7k7gvmjm3a0z8r23sw5mms824d6f06w1ybbm4hy9")))

(define-public crate-rasn-smi-0.12.6 (c (n "rasn-smi") (v "0.12.6") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.12.6") (d #t) (k 0)))) (h "1004fjdvchnpxwaw216y7riahy91fscbcs0a559j01m8pmz27i26")))

(define-public crate-rasn-smi-0.13.1 (c (n "rasn-smi") (v "0.13.1") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.13") (d #t) (k 0)))) (h "1df45yxl2h9ggfn0s9vnq3sbb8a1rqqs028hm04p6wgyqkk1gvbc")))

(define-public crate-rasn-smi-0.14.0 (c (n "rasn-smi") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.14") (d #t) (k 0)))) (h "065n0bybqynyr1azawj56jdq9vixy93shchpajwv4bigps6642wn")))

(define-public crate-rasn-smi-0.15.0 (c (n "rasn-smi") (v "0.15.0") (d (list (d (n "chrono") (r "^0.4.27") (f (quote ("alloc"))) (k 0)) (d (n "rasn") (r "^0.15") (d #t) (k 0)))) (h "1gdamnvl2wkhmirhf6hjaqmq28x6fmnik3h7454qcabh5p0n3d2i")))

