(define-module (crates-io ra sn rasn-compiler-derive) #:use-module (crates-io))

(define-public crate-rasn-compiler-derive-0.1.0 (c (n "rasn-compiler-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0yac45flxvrbhnfpp358zilbwi0ww41kiij74x84pj58dyd70cdl")))

(define-public crate-rasn-compiler-derive-0.1.1 (c (n "rasn-compiler-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1qzs68i3v2zcjxqz37x92dhy94ss38j9isazfiiizy57iddv7rrg")))

(define-public crate-rasn-compiler-derive-0.1.2 (c (n "rasn-compiler-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1xs9j7z4yjfpl0dk0jw7cvp8m49a36q6lf3fvn71sdjgiza2mqp2")))

(define-public crate-rasn-compiler-derive-0.1.3 (c (n "rasn-compiler-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.1.3") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gvaq44xav7kmv80saavnmbmq78xw3r1zvp00c8l1vc3871xjcwy")))

(define-public crate-rasn-compiler-derive-0.1.4 (c (n "rasn-compiler-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.1.4") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k6zdsssybarhvccq6i42f7mkdcqadfmsgl9m9m35p2chxnws274")))

(define-public crate-rasn-compiler-derive-0.2.0 (c (n "rasn-compiler-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0g94svsbfh1l6028w0g9z6x8hgann8j5i9x1dbxmnxyhhz8ifvwq")))

(define-public crate-rasn-compiler-derive-0.3.0 (c (n "rasn-compiler-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "rasn-compiler") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0frz58m1nambqxdfpx2x17pa1gv6vibylrdrgbzm4qg0yy8pam7s")))

