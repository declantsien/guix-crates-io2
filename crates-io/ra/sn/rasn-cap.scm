(define-module (crates-io ra sn rasn-cap) #:use-module (crates-io))

(define-public crate-rasn-cap-0.8.0 (c (n "rasn-cap") (v "0.8.0") (d (list (d (n "rasn") (r "^0.8.1") (d #t) (k 0)))) (h "1brqcxh10szzbw4cq63mvxyw364hghy60p4fp0nnb3fqk48hy6yn")))

(define-public crate-rasn-cap-0.8.2 (c (n "rasn-cap") (v "0.8.2") (d (list (d (n "rasn") (r "^0.8.1") (d #t) (k 0)))) (h "0900v9drxmpd39vcwa6yadahzcirnsapz52pbssp0dpnwqlzwnqp")))

(define-public crate-rasn-cap-0.9.0 (c (n "rasn-cap") (v "0.9.0") (d (list (d (n "rasn") (r "^0.9.0") (d #t) (k 0)))) (h "08rhhgr714hvgvqgwkapy6pcqxcz0mlb16yq5fds7mmrijiff7wm")))

(define-public crate-rasn-cap-0.9.1 (c (n "rasn-cap") (v "0.9.1") (d (list (d (n "rasn") (r "^0.9.0") (d #t) (k 0)))) (h "051yf18bmswkdv7b185c582j0dvj10lcfi46d3i288kyk7lgvgyq")))

(define-public crate-rasn-cap-0.9.2 (c (n "rasn-cap") (v "0.9.2") (d (list (d (n "rasn") (r "^0.9.2") (d #t) (k 0)))) (h "15g4w9mpxpnsxabbwbk77yz1cx05mghjzmlyqvg86br6w74hw5sa")))

(define-public crate-rasn-cap-0.9.3 (c (n "rasn-cap") (v "0.9.3") (d (list (d (n "rasn") (r "^0.9.3") (d #t) (k 0)))) (h "1g90jpv2i3c7fz1lmx3ivfykpkf05wlppnp0m7cscqjfayl2skaz")))

(define-public crate-rasn-cap-0.9.4 (c (n "rasn-cap") (v "0.9.4") (d (list (d (n "rasn") (r "^0.9.4") (d #t) (k 0)))) (h "057q3383svhzqvhjhvg17cpvcbakz012hqhki17lmk7lmnr5hwdr")))

(define-public crate-rasn-cap-0.9.5 (c (n "rasn-cap") (v "0.9.5") (d (list (d (n "rasn") (r "^0.9.5") (d #t) (k 0)))) (h "1amywd1wk596dc39cfizmmr3dsmhxsbcsmd6rxgy36xrlif9zca5")))

(define-public crate-rasn-cap-0.10.0 (c (n "rasn-cap") (v "0.10.0") (d (list (d (n "rasn") (r "^0.10.0") (d #t) (k 0)))) (h "0lnc5sds8kndvgjlc7xw8j4j5pkl943q3z6hv03icb9j3n245q44")))

(define-public crate-rasn-cap-0.10.1 (c (n "rasn-cap") (v "0.10.1") (d (list (d (n "rasn") (r "^0.10.1") (d #t) (k 0)))) (h "1kwxjs8z25gvkrl0drynwpv9kp6cy8wwbnbxwlh7kai6yfjhnhya")))

(define-public crate-rasn-cap-0.10.2 (c (n "rasn-cap") (v "0.10.2") (d (list (d (n "rasn") (r "^0.10.2") (d #t) (k 0)))) (h "1fps87acz9mvf4mpq4izwv54br1lrxwm5g8fd6d49ssy0i3zm8y3")))

(define-public crate-rasn-cap-0.10.3 (c (n "rasn-cap") (v "0.10.3") (d (list (d (n "rasn") (r "^0.10.3") (d #t) (k 0)))) (h "0d6zg2hy9l4dd7r42j5p85gz1jkasy1qqc4rvfms3r5mva68jsn4")))

(define-public crate-rasn-cap-0.10.4 (c (n "rasn-cap") (v "0.10.4") (d (list (d (n "rasn") (r "^0.10.4") (d #t) (k 0)))) (h "08ggw0q7s319jlpqr4yjawrdlswx2iwzz4gwxdispda0l7aq5agm")))

(define-public crate-rasn-cap-0.10.5 (c (n "rasn-cap") (v "0.10.5") (d (list (d (n "rasn") (r "^0.10.5") (d #t) (k 0)))) (h "0ml3ys7l0j60srhr6prmc4mz0l7h6q4vfs4qf3asr0v5f17g2nzw")))

(define-public crate-rasn-cap-0.10.6 (c (n "rasn-cap") (v "0.10.6") (d (list (d (n "rasn") (r "^0.10.6") (d #t) (k 0)))) (h "0gy519rbfn4wg6bzxagb7lpfmpqbwwjhji8l1ysh7ahs57yp17gg")))

(define-public crate-rasn-cap-0.11.0 (c (n "rasn-cap") (v "0.11.0") (d (list (d (n "rasn") (r "^0.11.0") (d #t) (k 0)))) (h "06d7fpwbjzpn2mmly4nswf55c6wzz49mhqzll79blwjdsm9vjs3x")))

(define-public crate-rasn-cap-0.11.1 (c (n "rasn-cap") (v "0.11.1") (d (list (d (n "rasn") (r "^0.11.1") (d #t) (k 0)))) (h "1vaarmfhv8c4jp8pdmnpyrs38n2hw1ffg2ry65b0y29mwcmp0pwg")))

(define-public crate-rasn-cap-0.12.0 (c (n "rasn-cap") (v "0.12.0") (d (list (d (n "rasn") (r "^0.12.0") (d #t) (k 0)))) (h "12y37r91mmwmzih2gpbip6ay2dbdji5fkrkvijpb7xff800f6zq5")))

(define-public crate-rasn-cap-0.12.1 (c (n "rasn-cap") (v "0.12.1") (d (list (d (n "rasn") (r "^0.12.1") (d #t) (k 0)))) (h "1wjvky7fjnjsnj9bk50ph5hs5r82nqs221p44vxwfrmb3libyghi")))

(define-public crate-rasn-cap-0.12.2 (c (n "rasn-cap") (v "0.12.2") (d (list (d (n "rasn") (r "^0.12.2") (d #t) (k 0)))) (h "023z0yqrdb22izq8zsd0xbva08qqx8kcb9cxcny5ilk556sz69qp")))

(define-public crate-rasn-cap-0.12.3 (c (n "rasn-cap") (v "0.12.3") (d (list (d (n "rasn") (r "^0.12.3") (d #t) (k 0)))) (h "0zgbpkp88rhyvd14gzn8q5lsgparbrmbx9xnj8lmp3pyw8b6zvis")))

(define-public crate-rasn-cap-0.12.4 (c (n "rasn-cap") (v "0.12.4") (d (list (d (n "rasn") (r "^0.12.4") (d #t) (k 0)))) (h "0k1rshcvhw56fqly22w02pr7zh655hx60qdzfy4qwgsdr5z86fwv")))

(define-public crate-rasn-cap-0.12.5 (c (n "rasn-cap") (v "0.12.5") (d (list (d (n "rasn") (r "^0.12.5") (d #t) (k 0)))) (h "0psyxppmq65z63175hvmgf9778l9hbgmj3akxcx8v0f4pbizkjzx")))

(define-public crate-rasn-cap-0.12.6 (c (n "rasn-cap") (v "0.12.6") (d (list (d (n "rasn") (r "^0.12.6") (d #t) (k 0)))) (h "089dz4w6ll49cn367shis5kgg84ib9z9ck9h551k5sf7ahsw5dq8")))

(define-public crate-rasn-cap-0.13.1 (c (n "rasn-cap") (v "0.13.1") (d (list (d (n "rasn") (r "^0.13") (d #t) (k 0)))) (h "0qzw5d3xl4dyay64syzvsrpxydd2fyi3pn1zkma9a7y22v1xwdq5")))

(define-public crate-rasn-cap-0.14.0 (c (n "rasn-cap") (v "0.14.0") (d (list (d (n "rasn") (r "^0.14") (d #t) (k 0)))) (h "16f44di3b4vagy08bnsjd1dgq2l5c2l94whdcih2y90bdnnbq5g7")))

(define-public crate-rasn-cap-0.15.0 (c (n "rasn-cap") (v "0.15.0") (d (list (d (n "rasn") (r "^0.15") (d #t) (k 0)))) (h "17m800qjl0kj1ixhndh97lmljw4dlg9shcbd68s39szs9w80i4cl")))

