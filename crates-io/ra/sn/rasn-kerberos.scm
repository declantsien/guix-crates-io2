(define-module (crates-io ra sn rasn-kerberos) #:use-module (crates-io))

(define-public crate-rasn-kerberos-0.5.0 (c (n "rasn-kerberos") (v "0.5.0") (d (list (d (n "rasn") (r "^0.5.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "08a9pn31sjcmjzvwnk262ma2ylv608wcg5ljvnr8va0q5fzr73b2") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.5.1 (c (n "rasn-kerberos") (v "0.5.1") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "rasn") (r "^0.5.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1xxpkjnjx329zwn2ckbzlrp04cv6gm8xbhyrzc8y8dmagz7hg03y") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.5.2 (c (n "rasn-kerberos") (v "0.5.2") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "rasn") (r "^0.5.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1lww37jywc9hl7nlc3b02vhg0rdmyqd6pn7f25060ibca2gb2qmm") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.5.3 (c (n "rasn-kerberos") (v "0.5.3") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "rasn") (r "^0.5.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1zkhd6c5bvd8p8iw4rsysxyxv2wjdkwp7almi0347dcawg3yfmdr") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.6.0 (c (n "rasn-kerberos") (v "0.6.0") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "rasn") (r "^0.6.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "04n3nk08ayjl2254rmg4y24j0bq8n2a0wja1c7bc6d08rnfy77xj") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.7.0 (c (n "rasn-kerberos") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "rasn") (r "^0.7.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0hnjhyqygffqpj9n3n85w3izjghk02jh563kkq7wib9nwsf6drli") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.8.2 (c (n "rasn-kerberos") (v "0.8.2") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.8.1") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "1qhg30wc8ar27i8nb39q3ml8lh3zairabwryrldwmjb1hkc4zqxf") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.0 (c (n "rasn-kerberos") (v "0.9.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "068af0dvwspav8888zk6xv0xn3021hrs458yz7dn85hzrqjzsyw8") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.1 (c (n "rasn-kerberos") (v "0.9.1") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "13spkf5kl2jdh4mwwwygagvp19ciqryz2n2v3ifby1l28vnbcv09") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.2 (c (n "rasn-kerberos") (v "0.9.2") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.2") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "0bn92k8lk92dvqqm5hmpricgf5r5kmv4c7v08j0grf78zi17r364") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.3 (c (n "rasn-kerberos") (v "0.9.3") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.3") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "0gh3r3w85f8wlf0a2q5fwf0nb3nlpl3ypmhrkbz1rifxbg8qrd8j") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.4 (c (n "rasn-kerberos") (v "0.9.4") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.4") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.4") (o #t) (d #t) (k 0)))) (h "0q9n8vzlnjjjjfssksi6mn23izvy8ga89zqgypf51y0dd4pn2wzc") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.9.5 (c (n "rasn-kerberos") (v "0.9.5") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.9.5") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.9.5") (o #t) (d #t) (k 0)))) (h "16d16qig93jiw6d9900jgnrc2hxxd7r0wnw8wyfhhnsr3vmmvd57") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.0 (c (n "rasn-kerberos") (v "0.10.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "07p0l3pbwaxxa978jyi2b7g7iv6wvc1nx274d2z9ymjbwar6vb11") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.1 (c (n "rasn-kerberos") (v "0.10.1") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.1") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.1") (o #t) (d #t) (k 0)))) (h "126x5wrllxjw97rsx6bm1cykrvnbzgnrq7195p4ihmwq0bs0p33v") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.2 (c (n "rasn-kerberos") (v "0.10.2") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.2") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.2") (o #t) (d #t) (k 0)))) (h "1d1gbh0ik0bsvz6pmc711q1m3lmqrd824zbmbf6p3ppm4x8b9d6a") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.3 (c (n "rasn-kerberos") (v "0.10.3") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.3") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.3") (o #t) (d #t) (k 0)))) (h "0x69qw53qxd707kx2qrja883saqbdzsajkb229la98xhk0vv6rmk") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.4 (c (n "rasn-kerberos") (v "0.10.4") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.4") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.4") (o #t) (d #t) (k 0)))) (h "0g5h70n7hmc1kwsxwzr94xqimy18m53pcchpz084f8c1nd9a525m") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.5 (c (n "rasn-kerberos") (v "0.10.5") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.5") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.5") (o #t) (d #t) (k 0)))) (h "0w3s9bmrgkv3nwdyrc5snwa5i3wjsjnyh96lqykaglj81s1xz3qi") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.10.6 (c (n "rasn-kerberos") (v "0.10.6") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.10.6") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.10.6") (o #t) (d #t) (k 0)))) (h "0vghvry52mlcv4qyg18k9npx77cny57xzj1sjd1s9gr9l08y192v") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.11.0 (c (n "rasn-kerberos") (v "0.11.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.11.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "0j7iaxakfjm4n09dyzvs2cvd0y3yjxr59ppid65yjsmjxgl9pppn") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.11.1 (c (n "rasn-kerberos") (v "0.11.1") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.11.1") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.11.1") (o #t) (d #t) (k 0)))) (h "0s5zpmvmzikv5dg8hyg62xvx2wyl3vickhkydd6p9snqp7bpxyzm") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.0 (c (n "rasn-kerberos") (v "0.12.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.0") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.0") (o #t) (d #t) (k 0)))) (h "1bcv6f9m8vpv6cis0ssyb6m6f7ck5xbr12yhzrz1sh46mh18jahb") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.1 (c (n "rasn-kerberos") (v "0.12.1") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.1") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "19nd2jkdwsl9gqi1mj7zrcrkl45j8v30z78mm0n2i429sr40gk44") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.2 (c (n "rasn-kerberos") (v "0.12.2") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.2") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.2") (o #t) (d #t) (k 0)))) (h "1n336x65kdh672vxg1xfz4vhq8s733iplqbjffnqjzqgw5izvh5j") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.3 (c (n "rasn-kerberos") (v "0.12.3") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.3") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "170i2kgda3i4anjb2ragw6i0pm8xlmybpal93y62m648d8zn39kr") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.4 (c (n "rasn-kerberos") (v "0.12.4") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.4") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "1q76032lr7hdanpwvpmfkzxlzczy4dh8pnch3ag4dx13yr6vcjbx") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.5 (c (n "rasn-kerberos") (v "0.12.5") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.5") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "0v23dnw21a1l8ls0xzjmg2y66r2p75fwc5iagm4gkhmsjnxvrd8q") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.12.6 (c (n "rasn-kerberos") (v "0.12.6") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.12.6") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.12.6") (o #t) (d #t) (k 0)))) (h "10s9khxf6zynghjdnf8r78spwh1nmlmmczj0ar0fj2wrlfm9z7jv") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.13.1 (c (n "rasn-kerberos") (v "0.13.1") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.13") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.13") (o #t) (d #t) (k 0)))) (h "0sg5531nvwcb0is9s2szg6yk4d7cqm0bgcqsaifnahn6l7ivd9nr") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.14.0 (c (n "rasn-kerberos") (v "0.14.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.14") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.14") (o #t) (d #t) (k 0)))) (h "0afvdp8nz117la4q4bbmfd62h79a2fbzdz0d6q27s0vkqz94yvib") (f (quote (("otp" "rasn-pkix"))))))

(define-public crate-rasn-kerberos-0.15.0 (c (n "rasn-kerberos") (v "0.15.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "rasn") (r "^0.15") (d #t) (k 0)) (d (n "rasn-pkix") (r "^0.15") (o #t) (d #t) (k 0)))) (h "077zvwsh2p8nn40qqmkhpaqkn58v7skmi4cbh6gj6hq4bi2729za") (f (quote (("otp" "rasn-pkix"))))))

