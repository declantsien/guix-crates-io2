(define-module (crates-io ra y- ray-tracer) #:use-module (crates-io))

(define-public crate-ray-tracer-0.1.0 (c (n "ray-tracer") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "02yrjl47rcshpcyfybr455y3w9qw281yx8z7qwf97lanb2xz44vl")))

