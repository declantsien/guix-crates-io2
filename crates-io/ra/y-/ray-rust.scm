(define-module (crates-io ra y- ray-rust) #:use-module (crates-io))

(define-public crate-ray-rust-0.1.0 (c (n "ray-rust") (v "0.1.0") (h "02x1ifhh6fgpisdygfdgpx6r8cik5fp37c7h9pp7kpxrwancnw29") (y #t)))

(define-public crate-ray-rust-0.1.1 (c (n "ray-rust") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "19h6jriq219i48y9rvxlljzmyy98h4b51ky6v9ckrmi398brz719")))

(define-public crate-ray-rust-0.1.2 (c (n "ray-rust") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0f8dfjx3zzvrlfxc1pmwya6f96h4wshgzbwbhjbx6il3q8v8xw1h")))

(define-public crate-ray-rust-0.1.3 (c (n "ray-rust") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0nwlz8yc3ac1znw5i6cn3gw5w82k5638zfmkw35fs551n0l5zva1")))

(define-public crate-ray-rust-0.1.4 (c (n "ray-rust") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0qn26lbvhfl5x88082fcr7492f83vk0xw4zxpq4l3qn4fy6ixba1")))

(define-public crate-ray-rust-0.1.5 (c (n "ray-rust") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "12sf5pfr20brcq1rzffrh88b4gal4zhv02kmqh5bnfwgdrx6zqh5") (f (quote (("with_tokio" "tokio") ("default"))))))

