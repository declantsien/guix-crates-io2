(define-module (crates-io ra t- rat-scrolled) #:use-module (crates-io))

(define-public crate-rat-scrolled-0.10.0 (c (n "rat-scrolled") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rat-event") (r "^0.12") (d #t) (k 0)) (d (n "rat-input") (r "^0.13") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (f (quote ("unstable-widget-ref" "unstable-rendered-line-info"))) (d #t) (k 0)) (d (n "tui-tree-widget") (r "^0.19.0") (d #t) (k 0)))) (h "0v5532d2gnky78bzbrhrwx04g34bbdcr7zyz1cyjfpdg66ai4iz3")))

(define-public crate-rat-scrolled-0.10.1 (c (n "rat-scrolled") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rat-event") (r "^0.12") (d #t) (k 0)) (d (n "rat-input") (r "^0.15") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)))) (h "1zcyzcwfqv6qra1677hsdq3wflqhmbiz6svyvp2w71y3pigjm1vw")))

