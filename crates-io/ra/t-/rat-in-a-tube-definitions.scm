(define-module (crates-io ra t- rat-in-a-tube-definitions) #:use-module (crates-io))

(define-public crate-rat-in-a-tube-definitions-0.2.0 (c (n "rat-in-a-tube-definitions") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07azivfki1g24jsb5633nv8r743xrfcl4h0lfam15sx6105kqywa")))

(define-public crate-rat-in-a-tube-definitions-0.2.1 (c (n "rat-in-a-tube-definitions") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zpn1lk5lkr85rshalrq492wpl9dcn1xdayrgcsqw4mi3yrbqic3")))

(define-public crate-rat-in-a-tube-definitions-0.2.2 (c (n "rat-in-a-tube-definitions") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g1miizb3k2n7l8jfcg8qs521mlzjjsm0h8j1lncyp4n45wyvh3m")))

(define-public crate-rat-in-a-tube-definitions-0.2.3 (c (n "rat-in-a-tube-definitions") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12vpimbagchkicvm9q0xjqgkjlf7bkisfys8mm49dkw2y32i7rfn")))

