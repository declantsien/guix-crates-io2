(define-module (crates-io ra t- rat-focus) #:use-module (crates-io))

(define-public crate-rat-focus-0.9.0 (c (n "rat-focus") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rat-event") (r "^0.12") (d #t) (k 0)) (d (n "rat-input") (r "^0.13.1") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (f (quote ("unstable-widget-ref" "unstable-rendered-line-info"))) (d #t) (k 0)))) (h "0hqkjq5wk35x4dy4r72487fzl83mvnsi0cz1mbq57ch512jn1ll6")))

(define-public crate-rat-focus-0.9.1 (c (n "rat-focus") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 2)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rat-event") (r "^0.12") (d #t) (k 0)) (d (n "rat-input") (r "^0.15") (d #t) (k 2)) (d (n "ratatui") (r "^0.26") (f (quote ("unstable-rendered-line-info"))) (d #t) (k 0)))) (h "0icmq69mpkxkznljqznvsxz3cy3nsawkb1cwf1cb0zc3m8073sfr")))

