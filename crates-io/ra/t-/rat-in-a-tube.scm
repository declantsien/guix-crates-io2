(define-module (crates-io ra t- rat-in-a-tube) #:use-module (crates-io))

(define-public crate-rat-in-a-tube-0.2.0 (c (n "rat-in-a-tube") (v "0.2.0") (d (list (d (n "riat-definitions") (r "^0.2.0") (d #t) (k 0) (p "rat-in-a-tube-definitions")))) (h "10x6777cff8rncp7li2wsff5s8siwkh9a36qjxr59qi1l79rvkpa")))

(define-public crate-rat-in-a-tube-0.2.1 (c (n "rat-in-a-tube") (v "0.2.1") (d (list (d (n "riat-definitions") (r "^0.2.1") (d #t) (k 0) (p "rat-in-a-tube-definitions")))) (h "11lh1rnsnc8szj0inn2q8d48532jc61y67ci69a3d42m1lybgz0v")))

(define-public crate-rat-in-a-tube-0.2.2 (c (n "rat-in-a-tube") (v "0.2.2") (d (list (d (n "riat-definitions") (r "^0.2.2") (d #t) (k 0) (p "rat-in-a-tube-definitions")))) (h "1wwch91sxhg0838c3fmczgv49gxw4n000b17fiwhzzk144091aq8")))

(define-public crate-rat-in-a-tube-0.2.3 (c (n "rat-in-a-tube") (v "0.2.3") (d (list (d (n "riat-definitions") (r "^0.2.3") (d #t) (k 0) (p "rat-in-a-tube-definitions")))) (h "147zi61gb050xj2mmqcckd4vfmnpjlwrj9d09m62pacgi83cf99h")))

