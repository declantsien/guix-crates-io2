(define-module (crates-io ra ra rara) #:use-module (crates-io))

(define-public crate-rara-0.1.1 (c (n "rara") (v "0.1.1") (d (list (d (n "combu") (r "^1.1.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wmyk5fdwzx71lkl4r097xppczndf43yzbmiw2dszabx348r5lfx")))

(define-public crate-rara-0.1.2 (c (n "rara") (v "0.1.2") (d (list (d (n "combu") (r "^1.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "12fi5chn865bdqh9dnpp9pplvh9flxl6cajya4ds6fld1ljva8w5")))

(define-public crate-rara-1.0.0 (c (n "rara") (v "1.0.0") (d (list (d (n "combu") (r "^1.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02xj3y6mj775d9dvyqgfc72pcc0grw54bg5pwc15xily9ii7mxkh")))

(define-public crate-rara-1.0.1 (c (n "rara") (v "1.0.1") (d (list (d (n "combu") (r "^1.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02qgnswn63z2l2q0bj3h56s8q1knqcp0kswdab6mqpqvnsricgb6")))

(define-public crate-rara-1.0.2 (c (n "rara") (v "1.0.2") (d (list (d (n "combu") (r "^1.1.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ac8wi50dl8956j7n818qvcd7j4nyb1ikaky25340p7sb6r0rnn5")))

