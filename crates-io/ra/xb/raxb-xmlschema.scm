(define-module (crates-io ra xb raxb-xmlschema) #:use-module (crates-io))

(define-public crate-raxb-xmlschema-0.1.0 (c (n "raxb-xmlschema") (v "0.1.0") (h "0nqgxqscsmyss22h7hblnm5zgpmxx8f4pq2vxkcbviwa0h5mlgxl") (r "1.78.0")))

(define-public crate-raxb-xmlschema-0.2.0 (c (n "raxb-xmlschema") (v "0.2.0") (h "007wwfhns6whdn48p445pyl16jh0wp3ajf65ksk4rc71666zcsfc") (r "1.78.0")))

