(define-module (crates-io ra xb raxb) #:use-module (crates-io))

(define-public crate-raxb-0.2.0 (c (n "raxb") (v "0.2.0") (d (list (d (n "raxb-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0gx5qa3qzsjsc1xf9lj9xmx8pxhllj8svdicsdyyaik8v072mbcb") (r "1.78.0")))

