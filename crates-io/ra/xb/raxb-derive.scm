(define-module (crates-io ra xb raxb-derive) #:use-module (crates-io))

(define-public crate-raxb-derive-0.1.0 (c (n "raxb-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0nfh5k4viipx5x4lrdsqydzq1670wkmykpf3xwbakzkqw6vvsrjs") (r "1.78.0")))

(define-public crate-raxb-derive-0.2.0 (c (n "raxb-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1yl0nsfa1wrv3d495mbzvrl0g8v1xx0wi64ggl4422cz7rgvrm4x") (r "1.78.0")))

