(define-module (crates-io ra xb raxb-validate) #:use-module (crates-io))

(define-public crate-raxb-validate-0.1.0 (c (n "raxb-validate") (v "0.1.0") (h "1z8b83584jr9y9jvk8fnx8mhw5c8gjwdrl95dn51s3b90f5mw3yc") (r "1.78.0")))

(define-public crate-raxb-validate-0.2.0 (c (n "raxb-validate") (v "0.2.0") (h "1g16cs0ra2glfhp3gp1winyk6w5sjbbyxlgzs7jyc5hb67171kbr") (r "1.78.0")))

