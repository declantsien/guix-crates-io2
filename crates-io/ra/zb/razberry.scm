(define-module (crates-io ra zb razberry) #:use-module (crates-io))

(define-public crate-razberry-0.0.1 (c (n "razberry") (v "0.0.1") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "0iayasqmkkjwxfz0z3id0jk43qd6pfi47cnhs1qy0qkczvnz0psb")))

(define-public crate-razberry-0.0.2 (c (n "razberry") (v "0.0.2") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "1ksw3m3vh5scgc8wr526qr29q1ariyvm8199kvf8n06bkm0mv7wz")))

(define-public crate-razberry-0.1.0 (c (n "razberry") (v "0.1.0") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "0wm1dnkm1yq839dnkhpr66hqvc5z4hk2kmg23zczs484lhly1hv5")))

(define-public crate-razberry-0.1.1 (c (n "razberry") (v "0.1.1") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "0fvxkhq5qrk0w52jlb6fhvw2i4s04x1qng8p0zgdxvhs8q5vcr84")))

(define-public crate-razberry-0.2.0 (c (n "razberry") (v "0.2.0") (d (list (d (n "hyper") (r "0.7.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "0.5.*") (d #t) (k 0)))) (h "1m6sqazh3jaaxmaz2jnvs5p3ryfapfc73rg6fpxl6y20dmb5bk4d")))

(define-public crate-razberry-0.2.1 (c (n "razberry") (v "0.2.1") (d (list (d (n "hyper") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "1.4.*") (d #t) (k 0)))) (h "14416h1scnysnrdj6gr34sq5x5rldpppivipjbi8545wzkc8lb3i")))

(define-public crate-razberry-0.3.0 (c (n "razberry") (v "0.3.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "0.10.*") (d #t) (k 0)) (d (n "log") (r "0.3.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "1.4.*") (d #t) (k 0)))) (h "0188r15zpbcbhfd7rymsl8i41s3nsnnjk9lpif33q86qf5r79dbl")))

