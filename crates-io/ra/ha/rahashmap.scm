(define-module (crates-io ra ha rahashmap) #:use-module (crates-io))

(define-public crate-rahashmap-0.1.0 (c (n "rahashmap") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0kzpmfpvrxxjnpr9rf0jbgb4aj6d7w126607hghxssj7b2xpkbjr")))

(define-public crate-rahashmap-0.2.0 (c (n "rahashmap") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "19wdf1dnvv90zjrbcb80g4w0n9bwzkv6yx8i3k6p9sg7ds45bfr9")))

(define-public crate-rahashmap-0.2.1 (c (n "rahashmap") (v "0.2.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "07q8qk26v9m0r1dqxvqnn6qxpr5pgcfxlgj09wbyhijnx62xxh7d")))

(define-public crate-rahashmap-0.2.2 (c (n "rahashmap") (v "0.2.2") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0japypr31svbw7sx0vxqp7w9qpc85dhq2afv0r1rk0hxhkm0k3py")))

(define-public crate-rahashmap-0.2.3 (c (n "rahashmap") (v "0.2.3") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "161w6kmsn8y84hl4cj65j6419qqqm8irc4k25dmr1shfyllmzwhk")))

(define-public crate-rahashmap-0.2.4 (c (n "rahashmap") (v "0.2.4") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0nbm9jn3f1dlg65b7d2zzmvgzpyhqr3agk1bip756xcpgs1kihkn")))

(define-public crate-rahashmap-0.2.5 (c (n "rahashmap") (v "0.2.5") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "10ifvcbkrhyxvf9ykic6n3f60h7cqdflr0vk3ln1r5bqzi805gwx")))

(define-public crate-rahashmap-0.2.6 (c (n "rahashmap") (v "0.2.6") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1l88i2iciqz1jfgwwb0rjyx53m1xyk7ml3h23n53w7nbaw9z95k7")))

(define-public crate-rahashmap-0.2.7 (c (n "rahashmap") (v "0.2.7") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1wc3hh1wrlr0cqxcxw6cd1ybxn970gvsi8zfzm5lfci5615ly98w")))

(define-public crate-rahashmap-0.2.8 (c (n "rahashmap") (v "0.2.8") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "15r9s0cff9hxl4830vjypbl5k41v31is4g7b82b8ra51w8qi04n8")))

(define-public crate-rahashmap-0.2.9 (c (n "rahashmap") (v "0.2.9") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0wvqhgj5y1qzf00qi82nzzjnws5dra5vl4l1j162rf9wvxmydjjn")))

(define-public crate-rahashmap-0.2.10 (c (n "rahashmap") (v "0.2.10") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "1j39b10fsdq6sx32ia3iz9a6i4rdv8yp510caplvl2f6iqfjzcyd")))

(define-public crate-rahashmap-0.2.11 (c (n "rahashmap") (v "0.2.11") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "06l2ydm9mhw1pn8ma1kj3v7vzdvvrzzb7sa1a8ks9d8l51xbi6dc")))

(define-public crate-rahashmap-0.2.12 (c (n "rahashmap") (v "0.2.12") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "0aiwyi11zhvsibz933glzc8kx4mh5y9rc212x0yxr5iv8ragk2pl")))

(define-public crate-rahashmap-0.2.13 (c (n "rahashmap") (v "0.2.13") (d (list (d (n "rand") (r "^0.5.3") (d #t) (k 0)))) (h "1h5iw9ari5d3p7062cy4hgq6l20jn7bb3c923n2js1myf1k6i1h9")))

(define-public crate-rahashmap-0.2.14 (c (n "rahashmap") (v "0.2.14") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1d3b3kvlg9chxcc36j3kf634si0z3y70vr9l84sshn6i7jkbz33c")))

