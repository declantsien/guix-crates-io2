(define-module (crates-io ra ha rahat3062_minigrep) #:use-module (crates-io))

(define-public crate-rahat3062_minigrep-0.1.0 (c (n "rahat3062_minigrep") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0mi1zxngz5z63sh43b1dzvlfnpz08y462hypqz4ycdy23v80gmy3") (y #t)))

(define-public crate-rahat3062_minigrep-0.1.1 (c (n "rahat3062_minigrep") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1809551wzj3hqlrppy3p97qdz495yai1khpwvlpqj0gzz3w1flfg") (y #t)))

(define-public crate-rahat3062_minigrep-0.1.2 (c (n "rahat3062_minigrep") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "012h8p6jba0a4kypswlb2cc585az07v1n9n460ygplbfg3w1fm0i") (y #t)))

(define-public crate-rahat3062_minigrep-0.1.3 (c (n "rahat3062_minigrep") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1vh9dpxj3m6kc2wjpk5xjq1h3b2jz257rgyj8dj6w6g92zrr82y0")))

