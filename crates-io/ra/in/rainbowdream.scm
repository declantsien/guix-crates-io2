(define-module (crates-io ra in rainbowdream) #:use-module (crates-io))

(define-public crate-rainbowdream-1.0.0 (c (n "rainbowdream") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gingerlib") (r "^1.0.0") (d #t) (k 0)))) (h "1k84kra8d6bddykm50ndckcrkxcxxzhhx97a9138159awa3p4ym0")))

(define-public crate-rainbowdream-1.1.0 (c (n "rainbowdream") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gingerlib") (r "^1.0.0") (d #t) (k 0)) (d (n "steamlocate") (r "^2.0.0-beta.2") (d #t) (k 0)))) (h "016jikd8zl19z09gvjariv4yzgbddrg55gs1df4lr5xyi9h9mic6")))

(define-public crate-rainbowdream-1.2.0 (c (n "rainbowdream") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gingerlib") (r "^1.0.0") (d #t) (k 0)) (d (n "steamlocate") (r "^2.0.0-beta.2") (d #t) (k 0)))) (h "1zxc67vpwz2chwr3hcg2szj60mh5rn8pb73h7l9zmpyj6xs4q3sg")))

