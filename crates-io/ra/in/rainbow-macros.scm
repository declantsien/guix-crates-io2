(define-module (crates-io ra in rainbow-macros) #:use-module (crates-io))

(define-public crate-rainbow-macros-0.1.0 (c (n "rainbow-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "rainbow-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwnvia5l6pd24aqnq1wxkndm76skv0d39c27zwh1ii4bxkhm0p6")))

