(define-module (crates-io ra in rainbow) #:use-module (crates-io))

(define-public crate-rainbow-0.0.0 (c (n "rainbow") (v "0.0.0") (h "0n6vrgvha2d8jcmg1vrn648xz5670pbxqjzmnmmnjlyw54lb6r5k")))

(define-public crate-rainbow-0.1.0 (c (n "rainbow") (v "0.1.0") (d (list (d (n "rainbow-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "rainbow-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "uno") (r "^0.1.0") (d #t) (k 0)))) (h "13qmnh17d46y66a2sk97f2k18376gkrjjc4h7f3nvmpvqpxg2byy")))

