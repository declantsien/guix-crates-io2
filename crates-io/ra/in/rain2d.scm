(define-module (crates-io ra in rain2d) #:use-module (crates-io))

(define-public crate-rain2d-0.1.0 (c (n "rain2d") (v "0.1.0") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (d #t) (k 0)))) (h "014nm1k2kzz4dj6y9rsc7hi3haxqb971s8vrh7j997h51zimbdmg")))

(define-public crate-rain2d-0.1.1 (c (n "rain2d") (v "0.1.1") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (d #t) (k 0)))) (h "0il6wxpilsicljgbkrc2iiqpl0vvxpd3yh8iws4kyhfw4m8cwqmv")))

(define-public crate-rain2d-0.2.0 (c (n "rain2d") (v "0.2.0") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (d #t) (k 0)))) (h "0rycnqx0g5jl71czhk72p0rzdv0z4jwp8z4ynyz0cij4x7mkhb3k")))

(define-public crate-rain2d-0.2.1 (c (n "rain2d") (v "0.2.1") (d (list (d (n "minifb") (r "^0.15.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (d #t) (k 0)))) (h "1n64k023ylgvm832zh088rwlbqiicvr7n799nnfhm7dim9vsn0jk")))

(define-public crate-rain2d-0.2.2 (c (n "rain2d") (v "0.2.2") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "minifb") (r "^0.15.1") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.5.0") (d #t) (k 0)))) (h "1x1p3kv2l7ip3c9zjma8anq001zf53zybdyhfvqpsyykg82hlajh")))

(define-public crate-rain2d-1.0.0 (c (n "rain2d") (v "1.0.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "minifb") (r "^0.19") (d #t) (k 0)))) (h "0p10j62xq22mghilynx68q3fwawbkkj88mhpvjja94sj97jxkjad")))

