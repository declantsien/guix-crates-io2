(define-module (crates-io ra in rainbowid) #:use-module (crates-io))

(define-public crate-rainbowid-0.0.1-dev.0 (c (n "rainbowid") (v "0.0.1-dev.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08br29nlc79fpah5mdx7bpdj7xgar372ywin3d8ndm1kh4495qdg") (y #t)))

(define-public crate-rainbowid-0.0.1-dev.1 (c (n "rainbowid") (v "0.0.1-dev.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gqlcrid5l4s2nnq3s1h4zck9677azpd9w2f9il36ymp1zrsxa13") (y #t)))

