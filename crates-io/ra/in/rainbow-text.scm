(define-module (crates-io ra in rainbow-text) #:use-module (crates-io))

(define-public crate-rainbow-text-0.1.0 (c (n "rainbow-text") (v "0.1.0") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1qxa21dabjzbjnhzzj726nk8kgqdarybqh2x4ydbq61vvqyabxw0")))

(define-public crate-rainbow-text-0.1.1 (c (n "rainbow-text") (v "0.1.1") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0r0ksd3krsrwzqdk7glzqjds4jmz5pbvh9ch5py3jxljqmc7rvzs")))

(define-public crate-rainbow-text-0.1.2 (c (n "rainbow-text") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1crp33k22wff3jjsa0ql4kh2x9iswgnzm8na1gzd6njn9adk6c01")))

(define-public crate-rainbow-text-0.1.3 (c (n "rainbow-text") (v "0.1.3") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "05pcai651204h8y6f11raw1jn5d27s8ngidrcbam60cdww8lkdgv")))

(define-public crate-rainbow-text-0.1.4 (c (n "rainbow-text") (v "0.1.4") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "10sqs0lwkbswfbyp05zjmkrsf5cm1hcb1w27yqqqgq3b4vgjd08v")))

(define-public crate-rainbow-text-0.1.5 (c (n "rainbow-text") (v "0.1.5") (d (list (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1416g3w0zzxs1q43fbga3qbw5r2by4n69ali9w5879c42h47j656")))

