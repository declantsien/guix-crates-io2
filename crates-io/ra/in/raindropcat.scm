(define-module (crates-io ra in raindropcat) #:use-module (crates-io))

(define-public crate-raindropcat-0.1.0 (c (n "raindropcat") (v "0.1.0") (d (list (d (n "gio") (r "^0.14.0") (d #t) (k 0)) (d (n "glib") (r "^0.14.2") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17rvry5vfpd60b32p43yg1ba00nf3rk2hfpjwi22ik1ywh4lj4ls")))

(define-public crate-raindropcat-0.1.2 (c (n "raindropcat") (v "0.1.2") (d (list (d (n "gio") (r "^0.14.0") (d #t) (k 0)) (d (n "glib") (r "^0.14.2") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1adqjcimk6q1v2n7l1qs4flh5b9f1nfq77zdl06c359wdxfvw86n")))

(define-public crate-raindropcat-0.1.3 (c (n "raindropcat") (v "0.1.3") (d (list (d (n "gio") (r "^0.14.0") (d #t) (k 0)) (d (n "glib") (r "^0.14.2") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ihqph2wsz54ska35d5jq7n79s380v73xkl2465d3vfxy9kq4idx")))

(define-public crate-raindropcat-0.1.5 (c (n "raindropcat") (v "0.1.5") (d (list (d (n "gio") (r "^0.14.0") (d #t) (k 0)) (d (n "gio") (r "^0.14.0") (d #t) (k 1)) (d (n "glib") (r "^0.14.2") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wiqglm9yfjbj8f7wsqqi1sa5p7ws1hlsx4xg3kgi6lqf8p43kkm")))

