(define-module (crates-io ra in rainbow-pest) #:use-module (crates-io))

(define-public crate-rainbow-pest-0.1.0 (c (n "rainbow-pest") (v "0.1.0") (d (list (d (n "hex_color") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1yzycxfpa0pk3cikpslj1ab87c57mh9lp3nd2bxqy7r63slm6dk1")))

(define-public crate-rainbow-pest-0.1.1 (c (n "rainbow-pest") (v "0.1.1") (d (list (d (n "hex_color") (r "^1.0.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1.3") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 2)))) (h "0ri40g8hf6yv58yxzv4s8ss6lygrbmk9m3d4035cqi1nc59qxybi")))

