(define-module (crates-io ra in raingrep) #:use-module (crates-io))

(define-public crate-raingrep-0.1.0 (c (n "raingrep") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04i6277s60bf3q9xydlp1zfwh1k05b9bqv7ymhgd52h67yl558gk")))

