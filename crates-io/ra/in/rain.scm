(define-module (crates-io ra in rain) #:use-module (crates-io))

(define-public crate-rain-0.1.0 (c (n "rain") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "term_size") (r "^0") (d #t) (k 0)))) (h "0ynvqvk9ndihkbxvzwb3zl7pwrlqpcas7y7c0bbbddgpv0zbsvsr")))

(define-public crate-rain-0.2.0 (c (n "rain") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "18mz5x0v454cl8g3y5ya89ill96ip31ldpn1dk0w7anh30yb3kd9")))

(define-public crate-rain-0.2.1 (c (n "rain") (v "0.2.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0h69gd5w8vfms5q4879nv22100iy5jyhxwwqs5jkf36xy1r81s53")))

(define-public crate-rain-0.2.2 (c (n "rain") (v "0.2.2") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0rphnkb4nlbmrj781xq9hd6kmy05byrm7rb0xi3l8rxayg26zipn")))

(define-public crate-rain-0.3.0 (c (n "rain") (v "0.3.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "0bbk2pr9qwna4mfdjzm5n0dknrws6jrn9jf6il71bj60bdz861s5")))

(define-public crate-rain-0.3.1 (c (n "rain") (v "0.3.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "mowl") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "13qz4l5qpz9da7z8ybd2765wc3ai956qln92crfanbz8p5c0ihg9")))

(define-public crate-rain-1.0.0 (c (n "rain") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "mowl") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0zbp7z7hps3ik3gj6w7l9vz6db4vcn560wf264fb430cas93pzvx")))

(define-public crate-rain-1.0.1 (c (n "rain") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)) (d (n "mowl") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "01qmfj0f3139z9rah4bc9si78cbfxy7167bpjac8n7h8shpd9fj3")))

