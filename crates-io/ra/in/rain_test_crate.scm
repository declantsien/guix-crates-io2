(define-module (crates-io ra in rain_test_crate) #:use-module (crates-io))

(define-public crate-rain_test_crate-0.1.0 (c (n "rain_test_crate") (v "0.1.0") (h "1bc8v64q7njrjq60ln9q2h6sm187khlwx8vv3g927j2bxxgs8g5v")))

(define-public crate-rain_test_crate-0.1.1 (c (n "rain_test_crate") (v "0.1.1") (h "1076kw3bc40f5pagjz3vfz3m35npj8mh2zsbl9q9r5m4afi5zc6s")))

