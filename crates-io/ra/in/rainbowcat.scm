(define-module (crates-io ra in rainbowcat) #:use-module (crates-io))

(define-public crate-rainbowcat-0.1.0 (c (n "rainbowcat") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "13nx1dwl1w3zyj5ymf9n46w0240vrd4bwahmdkw16gwny4fxyg4q")))

