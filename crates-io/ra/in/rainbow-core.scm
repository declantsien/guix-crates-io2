(define-module (crates-io ra in rainbow-core) #:use-module (crates-io))

(define-public crate-rainbow-core-0.1.0 (c (n "rainbow-core") (v "0.1.0") (d (list (d (n "html_parser") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "rainbow-pest") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10vwshb4b1736br61x58amrybi3dilhpsrkmzbyhgarmfzs5lrp8") (f (quote (("html" "html_parser") ("default" "html"))))))

