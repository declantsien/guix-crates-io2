(define-module (crates-io ra in rainbows) #:use-module (crates-io))

(define-public crate-rainbows-1.0.0 (c (n "rainbows") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1fy8g7dxcz7i0wp5aqrqrnw2byhj6i41mnbhrj2y00siinn3s2ny")))

