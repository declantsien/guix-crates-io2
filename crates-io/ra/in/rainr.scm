(define-module (crates-io ra in rainr) #:use-module (crates-io))

(define-public crate-rainr-0.1.0 (c (n "rainr") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "ezemoji") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01cwnnrn0m6vpvw1xd3b5cgzhbd76p7gj7kfivjnsh609z45jvr9")))

