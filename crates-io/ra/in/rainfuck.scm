(define-module (crates-io ra in rainfuck) #:use-module (crates-io))

(define-public crate-rainfuck-1.0.0 (c (n "rainfuck") (v "1.0.0") (d (list (d (n "owo-colors") (r "^4.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rkinbm4wyswdkh19x77la39rgclwisrg488jr4gvm3ri6nfwbvj") (f (quote (("full" "debug-colors" "serde")))) (s 2) (e (quote (("debug-colors" "dep:owo-colors"))))))

