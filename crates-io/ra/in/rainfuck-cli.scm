(define-module (crates-io ra in rainfuck-cli) #:use-module (crates-io))

(define-public crate-rainfuck-cli-1.0.0 (c (n "rainfuck-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fspp") (r "^2.1.0") (f (quote ("filesystem"))) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "piglog") (r "^1.4.1") (d #t) (k 0)) (d (n "rainfuck") (r "^1.0.0") (f (quote ("debug-colors"))) (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "0pd75ig84vgw7k1rd503gcpqb3nzcji2pyjgsim39h2d3x1x1iwy")))

