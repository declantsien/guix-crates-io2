(define-module (crates-io ra in rainbow-hat-rs) #:use-module (crates-io))

(define-public crate-rainbow-hat-rs-0.1.0 (c (n "rainbow-hat-rs") (v "0.1.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1yid056d2r80l38b9vccw10bd6c301bviqkqc516lqw99q6w09hs")))

(define-public crate-rainbow-hat-rs-0.2.0 (c (n "rainbow-hat-rs") (v "0.2.0") (d (list (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0fshidk3lkx0rz84fiz3qlc41v171islpay631cvf4rnz6ljxyma")))

(define-public crate-rainbow-hat-rs-0.2.1 (c (n "rainbow-hat-rs") (v "0.2.1") (d (list (d (n "palette") (r "^0.6.0") (d #t) (k 2)) (d (n "rppal") (r "^0.12.0") (d #t) (k 0)))) (h "1sxyzrbyb9hvz3p633bdy68aglx86zkrj1pyxrzyk4p65mdbk9cy")))

