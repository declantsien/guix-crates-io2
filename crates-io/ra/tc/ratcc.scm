(define-module (crates-io ra tc ratcc) #:use-module (crates-io))

(define-public crate-ratcc-0.1.0 (c (n "ratcc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0k48vndq37b146ykm9li68ff61l5mbg6zfkm8flmwgvhmd0vsmdg")))

(define-public crate-ratcc-0.1.1 (c (n "ratcc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0dcn6s38favcbbglcxdr6hmwf12rskvaxjqimq34wlhk2qdl3f6z")))

(define-public crate-ratcc-0.1.2 (c (n "ratcc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "15vnpap01nl0yx3ycg29gzws3zlx4jj3qphwz28bbnnv50i8z8lv")))

(define-public crate-ratcc-0.1.3 (c (n "ratcc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ql9p42xy685zajsskcdhyxzh1xf0l3xz7ypglnx7gbpkppnrqss")))

