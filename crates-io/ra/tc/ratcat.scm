(define-module (crates-io ra tc ratcat) #:use-module (crates-io))

(define-public crate-ratcat-0.1.0 (c (n "ratcat") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1ij3w63aql6lhf1vgvrpi238vmw8nazz86gh71zam45hk61y0sbs")))

(define-public crate-ratcat-0.1.2 (c (n "ratcat") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.3") (d #t) (k 2)))) (h "1ij833wlff68581kkc7l91mn793k0kp03jhlzs6xaibfrvyjrx8z")))

