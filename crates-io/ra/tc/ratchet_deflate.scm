(define-module (crates-io ra tc ratchet_deflate) #:use-module (crates-io))

(define-public crate-ratchet_deflate-0.1.0 (c (n "ratchet_deflate") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07nigvapazik5nyv9m45md37xdqvcmfi11607lzmavl4yhzhzjqf")))

(define-public crate-ratchet_deflate-0.1.1 (c (n "ratchet_deflate") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pcmhim93a8a3ki9i0z8zgjfdfzr9b1ilr3rlxxh8ig681prm896")))

(define-public crate-ratchet_deflate-0.1.2 (c (n "ratchet_deflate") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "070dlas04k2rqf2gvbwi57f4q9fbqsm024rzaj46mdp83l0b9rx6")))

(define-public crate-ratchet_deflate-0.1.4 (c (n "ratchet_deflate") (v "0.1.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jvnsf3dhc477n3i4qldpv2lfjc98qmx9kxafbiyxqnsl2zvhcxb")))

(define-public crate-ratchet_deflate-0.2.0 (c (n "ratchet_deflate") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01mzp9s1s5rv9msf04wbj440xjdhifhxqk1nca4hbdai4vfb8n5q")))

(define-public crate-ratchet_deflate-0.3.0 (c (n "ratchet_deflate") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1p3s414hi0mkq8axqfs25dvxgprzjj3lnzvk4l5q2vd74g5l9cgh")))

(define-public crate-ratchet_deflate-0.4.0 (c (n "ratchet_deflate") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h2il7bww13ch95nnkb8w32xb1s2wm154972g35b3zgjamnb2mwi")))

(define-public crate-ratchet_deflate-0.4.1 (c (n "ratchet_deflate") (v "0.4.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04bgr1ifkwlfj644vfjn402j03ix5lyl275xw2149xjjvxi868vp")))

(define-public crate-ratchet_deflate-0.4.2 (c (n "ratchet_deflate") (v "0.4.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ratchet_ext") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ix8xdwiqqljz91kyanwvg94k42587k507g9hivb2r8vx7m5qqzh")))

