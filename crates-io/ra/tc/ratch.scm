(define-module (crates-io ra tc ratch) #:use-module (crates-io))

(define-public crate-ratch-0.1.0 (c (n "ratch") (v "0.1.0") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)))) (h "0nys0san7hk2fi17sf3y0pwlsg5fqprhp64vh6n35vw4qkmr8xcj")))

(define-public crate-ratch-0.2.0 (c (n "ratch") (v "0.2.0") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)))) (h "0ds4j9sihfwbxpd5pwmh32fizdsnvbd8cakq8ks7q5m9d66fwymn")))

(define-public crate-ratch-0.3.0 (c (n "ratch") (v "0.3.0") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)))) (h "1f4pykdg3dh9bga5yafd5p1cpmav74wvf68lcia7s054fw0ih5wm")))

(define-public crate-ratch-0.3.1 (c (n "ratch") (v "0.3.1") (d (list (d (n "clap") (r "^2.23.0") (d #t) (k 0)) (d (n "duct") (r "^0.11.1") (d #t) (k 0)) (d (n "os_pipe") (r "^0.8.0") (d #t) (k 0)) (d (n "pancurses") (r "^0.16.0") (d #t) (k 0)))) (h "0srzlw92idxlifgqa7pwcf03qnyd9aswyw8kqzjps5h2wk4s1ga4")))

