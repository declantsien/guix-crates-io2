(define-module (crates-io ra tc ratchet_ext) #:use-module (crates-io))

(define-public crate-ratchet_ext-0.1.0 (c (n "ratchet_ext") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "103r8a981bb4i7wwcb7dr83il2cyx2w7i24b0x1k2v2j1a52jrrj")))

(define-public crate-ratchet_ext-0.1.1 (c (n "ratchet_ext") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "1jlvra3k357bsr1k12fqk81lpcr7yjj34mrd8qv6g39s82mw1yxb")))

(define-public crate-ratchet_ext-0.1.2 (c (n "ratchet_ext") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "14yaqvs8s3ajv1389vba32pxidnmh821av3wv14f13w87wjifkf4")))

(define-public crate-ratchet_ext-0.1.3 (c (n "ratchet_ext") (v "0.1.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "02bjf2c5wxrv1dx8j0y2fqqg17br60psa7fv8ri14n9rlsanfi90")))

(define-public crate-ratchet_ext-0.1.4 (c (n "ratchet_ext") (v "0.1.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "0cxlrlv2i9nf9mdir9mcvbsv35z2k2h83xbv6yb4z7mraz0w7kyn")))

(define-public crate-ratchet_ext-0.2.0 (c (n "ratchet_ext") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "0682fzh9s2y7173wck5a3wh8yyw652bnclgnqdcnpn9v4isncxcg")))

(define-public crate-ratchet_ext-0.3.0 (c (n "ratchet_ext") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "1bg1p18cw9jrgpczksxpy3wy6grlz849y7ls64h5f6bdfyq7pyb7")))

(define-public crate-ratchet_ext-0.4.0 (c (n "ratchet_ext") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "1y8in2lzmw62g6ijfqscb67f4z059f3lli7v0853bb6g67hl6his")))

(define-public crate-ratchet_ext-0.4.1 (c (n "ratchet_ext") (v "0.4.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "0sx3gqidj3yljm7mrvl4g5i4by9sw2ayha82fdvlpa8ms0xvzx9m")))

(define-public crate-ratchet_ext-0.4.2 (c (n "ratchet_ext") (v "0.4.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.3") (d #t) (k 0)) (d (n "httparse") (r "^1.4.1") (d #t) (k 0)))) (h "1qdagivqqgav0np7q6zqiv3pkv5w0lcrdmq0glrl5aipxk1wiqkj")))

