(define-module (crates-io ra mm rammer) #:use-module (crates-io))

(define-public crate-rammer-0.1.0 (c (n "rammer") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1y8mc6w78k5lga913m3gp4mgxxvmlg9sa92icygbj3rwbih0iyzz")))

(define-public crate-rammer-0.1.1 (c (n "rammer") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "152wk06f6b3bb3xiq96nfhyz7pvylzd6nanrwgnwnv4mm7cl1bfp")))

(define-public crate-rammer-0.2.1 (c (n "rammer") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1kafpn5x6w5q0sd2wff5gg3f5gxjx8zrbx4cl54267paxinh49lz")))

(define-public crate-rammer-0.2.2 (c (n "rammer") (v "0.2.2") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "156kawing3fz17m2srajwyiyzy0p4jwnb4jivzyv0dn0z6qij1g3")))

(define-public crate-rammer-0.2.3 (c (n "rammer") (v "0.2.3") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "19agpz22vk42na6pw0qf24r1q920ffs49phbmsk2lqlbvbrsl6c0")))

(define-public crate-rammer-0.2.4 (c (n "rammer") (v "0.2.4") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1c94g8i8vflm2f2m10sbx8fdg4h5kilaxjw8rrqcxh0i3qanr44p")))

(define-public crate-rammer-0.3.0 (c (n "rammer") (v "0.3.0") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "1zjzic3hnmkxmn9jlqr060yahmmg2g2fbfryjnkgrhrr51qy4s11")))

(define-public crate-rammer-0.3.1 (c (n "rammer") (v "0.3.1") (d (list (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "053ppdj5744xd57yf402ic43sxaifx372a2n2z1awz41riw050yw")))

