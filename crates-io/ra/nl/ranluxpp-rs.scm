(define-module (crates-io ra nl ranluxpp-rs) #:use-module (crates-io))

(define-public crate-ranluxpp-rs-0.1.0 (c (n "ranluxpp-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "166kqalj7d58gp3rxrfy6qwds6v2acjs5prm48ykhq435kmn6j6s") (y #t)))

(define-public crate-ranluxpp-rs-0.2.0 (c (n "ranluxpp-rs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1sykwx4xqhdn6g8pnk23yhbf3pmqlwjhm90ixqiahzi56233rp1a") (y #t)))

(define-public crate-ranluxpp-rs-0.3.0 (c (n "ranluxpp-rs") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0c61plf8hk9mdpnaz6kzwaag2zx5gdz7f1wiw12im7fykihc2n0r")))

(define-public crate-ranluxpp-rs-0.3.1 (c (n "ranluxpp-rs") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "03ydffmgj9jh0jbidfrc1jj64bn475kc6rvlli21n16ss392zx87")))

