(define-module (crates-io ra sk rasks) #:use-module (crates-io))

(define-public crate-rasks-0.1.0 (c (n "rasks") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "02f3a455yw9z6qk301gb5bilx4y36ygrn5rbvz6q7m2zlcv7gzzh")))

(define-public crate-rasks-0.2.0 (c (n "rasks") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1511vhfv3ir9c6x7p862wk4jjvcjzm7xcg0cqg7xkrm1wdrk20wh")))

(define-public crate-rasks-0.3.0 (c (n "rasks") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d7wxrjgadk6001xq07gjfiz3z1ab6wzqwza5h1szm5k2slf2jlj")))

(define-public crate-rasks-0.3.1 (c (n "rasks") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08hj6q9w6gaxihk1ljbc8xbag6vd60brxxmp3vc2i6hwxfnn2qhz")))

(define-public crate-rasks-0.3.2 (c (n "rasks") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0v2vjridwgzkmarmsw9dd7mcpj3baf2vwjnhf61q06dpc5nn6bh1")))

(define-public crate-rasks-0.3.3 (c (n "rasks") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l5rs9n9y2r5bj6fzla36px23mirm08hd2mfw9ys6dnnffwm1r35")))

(define-public crate-rasks-0.3.4 (c (n "rasks") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c1r8yb8knpz1yl3j7ghaiddqy0s23d05iagdzlm4wv5givgdzla")))

