(define-module (crates-io ra sk rask-core) #:use-module (crates-io))

(define-public crate-rask-core-0.1.0 (c (n "rask-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "rask-liburing") (r "^2.4.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "slab") (r "^0.4.8") (d #t) (k 0)))) (h "0nzpgrfvvb26rnzninh14va15q6387x665lii46h4jby0sx45v8k")))

