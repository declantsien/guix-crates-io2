(define-module (crates-io ra sk rask) #:use-module (crates-io))

(define-public crate-rask-0.0.0 (c (n "rask") (v "0.0.0") (h "0gbgyhmkw5wkf9w3s197i1cniw0vjygamx4w3v1vx30ls8v5dxh4")))

(define-public crate-rask-0.1.2 (c (n "rask") (v "0.1.2") (d (list (d (n "chainmap") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "019z2ynw70w9lr9p8dv63051m32jhlkjmyrdjgnyghsn2skf6d2q")))

(define-public crate-rask-0.1.3 (c (n "rask") (v "0.1.3") (d (list (d (n "chainmap") (r "^0.1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0g32q3ld7769m0cic97vg9yrfwg06hhqh8mbnpfn4h2nx2b8yczg")))

(define-public crate-rask-0.1.4 (c (n "rask") (v "0.1.4") (d (list (d (n "chainmap") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0j1dbxnrdm49664rxfz9fzlh20aifx83pfql7g62j74gz83i5kbp")))

(define-public crate-rask-0.1.5 (c (n "rask") (v "0.1.5") (d (list (d (n "chainmap") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "102hkz9dbn8rdarwyrbjfwp36hssi11csywgb9v1v9drip161ryf")))

(define-public crate-rask-0.2.1 (c (n "rask") (v "0.2.1") (d (list (d (n "chainmap") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0zqrys2ahw0icqda7c51sghmx30451a40dpz3bdpfkp18g85d981")))

