(define-module (crates-io ra sk rask-liburing-sys) #:use-module (crates-io))

(define-public crate-rask-liburing-sys-2.4.0 (c (n "rask-liburing-sys") (v "2.4.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0mv10np91dyllch0k118n1lzczz4nv3iwmza6z5w2iwpb563bkip")))

