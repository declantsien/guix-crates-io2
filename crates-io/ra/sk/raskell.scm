(define-module (crates-io ra sk raskell) #:use-module (crates-io))

(define-public crate-raskell-0.0.1 (c (n "raskell") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1n5xfp6ngxl89421rn2dhqcfp7jzzh3xzs9hb1v0wsxwc171fpyn") (r "1.61.0")))

