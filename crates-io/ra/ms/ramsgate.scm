(define-module (crates-io ra ms ramsgate) #:use-module (crates-io))

(define-public crate-ramsgate-0.1.0 (c (n "ramsgate") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "0p6hn30c3ai8kz843qjrac1v24gfp8a9pr2k15is9wlz46hfkhi9")))

