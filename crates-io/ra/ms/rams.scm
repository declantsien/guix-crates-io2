(define-module (crates-io ra ms rams) #:use-module (crates-io))

(define-public crate-rams-0.1.0 (c (n "rams") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4.0") (d #t) (k 0)) (d (n "dioxus-web") (r "^0.4.0") (d #t) (k 0)) (d (n "rams-proc-macros-internal") (r "^0.1.0") (d #t) (k 0)))) (h "0zsripcak4jka61var01di8c2iq6g69jig9rdhv8l2pfh8cqqp3i") (y #t)))

