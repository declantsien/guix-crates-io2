(define-module (crates-io ra ms rams-proc-macros-internal) #:use-module (crates-io))

(define-public crate-rams-proc-macros-internal-0.1.0 (c (n "rams-proc-macros-internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "proc-macro" "parsing" "printing" "clone-impls"))) (k 0)))) (h "0fbmd4y4qqcc6sznkal11qcrhiick5s9m316yk5ingak8h4c22jx") (y #t)))

