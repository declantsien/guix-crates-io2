(define-module (crates-io ra tr ratrod-rs) #:use-module (crates-io))

(define-public crate-RatRod-rs-0.1.0 (c (n "RatRod-rs") (v "0.1.0") (d (list (d (n "dylib") (r "^0.0.3") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "serde_json_any_key") (r "^2.0.0") (d #t) (k 0)) (d (n "sparse_matrix") (r "^0.6.0") (d #t) (k 0)))) (h "1a7jgr91lwcvaahgb4xq11zxn0hdi94vhcvkxnajvw4zf40k070c")))

