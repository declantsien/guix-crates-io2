(define-module (crates-io ra to rato) #:use-module (crates-io))

(define-public crate-rato-0.1.0 (c (n "rato") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "19cz1mvxhyabqfjn8acxdclb4hcyzy7q4y24zqw6rqflhkk7wvm0")))

(define-public crate-rato-0.1.1 (c (n "rato") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1kp0j86xyvspxrhjz9y7xw94r1qlb7ilflh1nf61b94rk7gc53kb")))

(define-public crate-rato-0.1.2 (c (n "rato") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (f (quote ("max_level_trace" "release_max_level_info"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "14pppmk3bckmyjpsb365al2jr3jqk06k4vzb3yfyiv38iff6hqd4")))

