(define-module (crates-io ra ft raftmodel) #:use-module (crates-io))

(define-public crate-raftmodel-0.1.0 (c (n "raftmodel") (v "0.1.0") (h "1x6ypbmf0vfssmx8a2087phr9c6chzhg3ma9gb7j940haqrsqaz1")))

(define-public crate-raftmodel-0.1.1 (c (n "raftmodel") (v "0.1.1") (h "056k7ssz0ac0qfgn1vxkb7m7p3x9zncg8csrbx35qfbhl0njpilh")))

(define-public crate-raftmodel-0.1.2 (c (n "raftmodel") (v "0.1.2") (h "09aj2rgbd061b8wf5ydn94g8fwi7w9g0a8lii6c1k5jf82zf941s")))

(define-public crate-raftmodel-0.1.3 (c (n "raftmodel") (v "0.1.3") (h "0a91kdv0d8lqrbfihggwqd8hbmv3qy9iy5dzw5qi3gslvhqs8vd7")))

