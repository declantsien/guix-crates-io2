(define-module (crates-io ra ft raft-engine-ctl) #:use-module (crates-io))

(define-public crate-raft-engine-ctl-0.1.0 (c (n "raft-engine-ctl") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "raft-engine") (r "^0.1.0") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "1iq5snw2mxmw2ympb001inbxsh3f1ja2sr13144m0vgnxv4l8jyz") (r "1.56")))

(define-public crate-raft-engine-ctl-0.2.0 (c (n "raft-engine-ctl") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "raft-engine") (r "^0.2.0") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "1grlksnvn5n3kwdmm7jr7rq3pbzf7pj5jdccs7ksgwhdpbvfvrv0") (r "1.57")))

(define-public crate-raft-engine-ctl-0.2.1 (c (n "raft-engine-ctl") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "raft-engine") (r "^0.2.1") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "05rirsg40y871hd2vqk4mmqlmxkn6xii93lbrmqih3nk3zbl4qd2") (r "1.57")))

(define-public crate-raft-engine-ctl-0.2.2 (c (n "raft-engine-ctl") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-rc.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "raft-engine") (r "^0.2.2") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "0l454wxv3vimrpdij6fhhxlmqz62zfzl936ixwl34ky6lccgilha") (r "1.57")))

(define-public crate-raft-engine-ctl-0.3.0 (c (n "raft-engine-ctl") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "raft-engine") (r "^0.3.0") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "1f0x6q802k7sfcjd7hlrg9nw4z3dixc0b32lgrcddfa9cblbxpa3") (r "1.57")))

(define-public crate-raft-engine-ctl-0.4.0 (c (n "raft-engine-ctl") (v "0.4.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "raft-engine") (r "^0.4.0") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "0s5fpp78s8bxpp5i5x82qacrb80wqqrc0dpwl6p236i3gm5l7vhg") (r "1.61.0")))

(define-public crate-raft-engine-ctl-0.4.1 (c (n "raft-engine-ctl") (v "0.4.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "raft-engine") (r "^0.4.1") (f (quote ("scripting" "internals"))) (d #t) (k 0)))) (h "0vg626rj8pimlx9wjz789fh3bnh4nk9374q5cm7gh59s94msx4q5") (r "1.61.0")))

