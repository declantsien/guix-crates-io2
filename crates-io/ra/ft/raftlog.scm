(define-module (crates-io ra ft raftlog) #:use-module (crates-io))

(define-public crate-raftlog-0.4.0 (c (n "raftlog") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1xdvipq3sfijcph2s8lhac4gv2gsi3wf5s95slba99pcs2w0wkpx")))

(define-public crate-raftlog-0.4.1 (c (n "raftlog") (v "0.4.1") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0wm29lqn012hhby69lkzb5ky75hkwxg5dxmsm4sqhnxr9ikina9d")))

(define-public crate-raftlog-0.5.0 (c (n "raftlog") (v "0.5.0") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "07h3c803rp5abl7d26irvbdvlajy97isiv59qbsx81fphz8jcns6")))

(define-public crate-raftlog-0.6.0 (c (n "raftlog") (v "0.6.0") (d (list (d (n "fibers") (r "^0.1") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prometrics") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1yvpp4a7kphqjm7sfcn0pmx7g1cpsxnlppsdcdvis95z7c6rp5wl")))

