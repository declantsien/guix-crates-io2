(define-module (crates-io ra ft raftlog_protobuf) #:use-module (crates-io))

(define-public crate-raftlog_protobuf-0.1.0 (c (n "raftlog_protobuf") (v "0.1.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "raftlog") (r "^0.4") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "03dhamxxkdmrcjhgqddyv741b8a7g65wrj7c77dqa78vbbm2k063")))

(define-public crate-raftlog_protobuf-0.2.0 (c (n "raftlog_protobuf") (v "0.2.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "raftlog") (r "^0.5") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1d3ziw4sr0v17ql26qh4pr802qy64pmqxs5rw2af7miz8s1g8q5n")))

(define-public crate-raftlog_protobuf-0.3.0 (c (n "raftlog_protobuf") (v "0.3.0") (d (list (d (n "bytecodec") (r "^0.4") (d #t) (k 0)) (d (n "protobuf_codec") (r "^0.2") (d #t) (k 0)) (d (n "raftlog") (r "^0.6") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "19s0qm2mn1krlhxcysgn1kyr914vhh77g2q9v43ykrp9viv4w0xm")))

