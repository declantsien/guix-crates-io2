(define-module (crates-io ra wb rawbytes) #:use-module (crates-io))

(define-public crate-rawbytes-0.1.0 (c (n "rawbytes") (v "0.1.0") (h "1r1r2v9m1g0m2zfs22gylxgnsxw9hv4a2b0paas5jgvc1n071c88")))

(define-public crate-rawbytes-0.1.1 (c (n "rawbytes") (v "0.1.1") (h "1fyqfl205ckyzwis8857y7n0m0fii7dcnmmzcfni3z92qbs82vd2")))

(define-public crate-rawbytes-0.1.2 (c (n "rawbytes") (v "0.1.2") (h "0q1yf6aa4n5bw4kv4gcsr7n6iyip8vgdhfgjqw96vf5g2s7hil04")))

(define-public crate-rawbytes-1.0.0 (c (n "rawbytes") (v "1.0.0") (h "106sccmc4j6i61amf2df0648q9iid87c1rh33lvwlcc7v2xg2dz0")))

(define-public crate-rawbytes-1.0.1 (c (n "rawbytes") (v "1.0.1") (h "0lrjcrwvhvi8iy8970yavz7iiqiwa7bygwrabid5zybn3vhgkk0m")))

