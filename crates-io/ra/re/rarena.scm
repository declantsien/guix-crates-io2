(define-module (crates-io ra re rarena) #:use-module (crates-io))

(define-public crate-rarena-0.0.0 (c (n "rarena") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (k 0)) (d (n "fs4") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "loom") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0961d9sq8sg24vx83nnqkyhs6jwy6h7ybghk2hmqph2lp2aciszc") (f (quote (("std") ("memmap" "memmap2" "fs4" "std") ("default" "std") ("alloc")))) (r "1.56")))

