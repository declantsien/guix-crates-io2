(define-module (crates-io ra wu rawuefi) #:use-module (crates-io))

(define-public crate-rawuefi-0.1.0 (c (n "rawuefi") (v "0.1.0") (h "1hmnbzggpn3m61kgfh5ix36m1whqq6dgsj4pnygk199ipvhyv26k") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.1 (c (n "rawuefi") (v "0.1.1") (h "1fyqzc1g3pbiqdfpf30mma4bq6rvkac39zg229ms3rdrwljan1y9") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.2 (c (n "rawuefi") (v "0.1.2") (h "0hhqh3pz54dvny3fsy05v6lvd54hk2hry0pc3vg9bbzgffp8w3zc") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.3 (c (n "rawuefi") (v "0.1.3") (h "08k869nqkmr29fy3lpxy1viywqsklw2x54d1bkx44xwk5bz515bj") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.1 (c (n "rawuefi") (v "0.1.4-dev.1") (h "03gn1zc30r4n6299anvi85alr7i8afnys8k6f1lv1lyiym1zcl0r") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.2 (c (n "rawuefi") (v "0.1.4-dev.2") (h "19js1jc025hd9qi3nzmvspci4bb3by420n50yya4v4imsd8rnhz0") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.3 (c (n "rawuefi") (v "0.1.4-dev.3") (h "0p40r2i0z89rsh2fndprqva44q1xfm2pw8dyjac0nd86vcxcghz2") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.4 (c (n "rawuefi") (v "0.1.4-dev.4") (h "0r1mh73gcxw3affgr0nrws8ffnawdwig9f09lkxyhs8ks1wr4ff1") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.5 (c (n "rawuefi") (v "0.1.4-dev.5") (h "0ww5fljyr8wrp7bm9bvws1brk4rdibhj77vmz7zh76xv4yhmlvsr") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4-dev.6 (c (n "rawuefi") (v "0.1.4-dev.6") (h "08233s9552lrf6yxmi0kyvgnq4rf2z22rfss02ql8fk3i8lqybnn") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.4 (c (n "rawuefi") (v "0.1.4") (h "0l2malkgby1cc1cs4d53ygs6dnmxdzdq63g1a7nlby8mkpw045fm") (y #t) (r "1.69.0")))

(define-public crate-rawuefi-0.1.5-dev.1 (c (n "rawuefi") (v "0.1.5-dev.1") (h "1pz7pggbcxnrc52cchwsyc4r5dkkrcv5sn6hvk718848b7ry9s8b") (y #t) (r "1.70.0")))

(define-public crate-rawuefi-0.1.5-dev.2 (c (n "rawuefi") (v "0.1.5-dev.2") (h "0lzdrjw4z0a0hbvinn7ym0cbwz498s4rrr4sddzycir9kfjgfx51") (y #t) (r "1.70.0")))

(define-public crate-rawuefi-0.1.5-dev.3 (c (n "rawuefi") (v "0.1.5-dev.3") (h "10g5vg4i2l0zdjcyw3hy7k58xz3c7mqpnr6b7h7dzrd1gw5v2816") (y #t) (r "1.70.0")))

