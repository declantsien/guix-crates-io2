(define-module (crates-io ra ct ractor_cluster_derive) #:use-module (crates-io))

(define-public crate-ractor_cluster_derive-0.6.0 (c (n "ractor_cluster_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jlis20wj8wri3jp47kg9arbys203zqqckipsm582nw7dk00qb35")))

(define-public crate-ractor_cluster_derive-0.6.1 (c (n "ractor_cluster_derive") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hy0qpyb077hr68bc30f5fik7pabvic0w7cradx0i7jwshd5jygl")))

(define-public crate-ractor_cluster_derive-0.7.0 (c (n "ractor_cluster_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0v7ic2yy6ppwf6qwh3py0bqgqgiph80qbpabqij4d0r9g1y85snv")))

(define-public crate-ractor_cluster_derive-0.7.1 (c (n "ractor_cluster_derive") (v "0.7.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0p3szdypmc56in8m8k5lc9p5glvp6zy7wgqh8wwsyfmirb7yin8j")))

(define-public crate-ractor_cluster_derive-0.7.2 (c (n "ractor_cluster_derive") (v "0.7.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vzkhkwf6anc4zh26djhcw5zzailx7ahanm5zfc6792641pwf1n9")))

(define-public crate-ractor_cluster_derive-0.7.3 (c (n "ractor_cluster_derive") (v "0.7.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1p9mq0kfrc5x2jsi3nnsg345q5s2d6vhwlmb6r7hxjrjg4lqag4c")))

(define-public crate-ractor_cluster_derive-0.7.4 (c (n "ractor_cluster_derive") (v "0.7.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wyhx4qa72wz49rnpv8hhada37kc08iyv223qmb0vx19ks4xh6cf")))

(define-public crate-ractor_cluster_derive-0.7.5 (c (n "ractor_cluster_derive") (v "0.7.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rimjrf4fz1kv8aadg9s6y55kr0q5aynrs8ycawg1wi65nrgs1lg")))

(define-public crate-ractor_cluster_derive-0.7.6 (c (n "ractor_cluster_derive") (v "0.7.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xyia0y5zwx710s487zki0hxx2zbm489sj6a7wgn11zggs3d8y9r")))

(define-public crate-ractor_cluster_derive-0.8.0 (c (n "ractor_cluster_derive") (v "0.8.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bva0hzag47w8vc21k9674gxw8v1c8c1sj0fz5iqkjm9l7lch8kv")))

(define-public crate-ractor_cluster_derive-0.8.1 (c (n "ractor_cluster_derive") (v "0.8.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "17ags0cwsh2yz2sahf6310jngd16vkddxmg6qnqy7pvjwkcbhvr1")))

(define-public crate-ractor_cluster_derive-0.8.2 (c (n "ractor_cluster_derive") (v "0.8.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "06byjwhanqqnb4am76429a547v6js77i56gsl4qmvjapi1w98l6k")))

(define-public crate-ractor_cluster_derive-0.8.3 (c (n "ractor_cluster_derive") (v "0.8.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cq9qjdc3wbj8h624m4c2zkzmpw7rg7nmm98g43kj85vac86h6x3")))

(define-public crate-ractor_cluster_derive-0.8.4 (c (n "ractor_cluster_derive") (v "0.8.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01bncv19hyz2ngbqr6f8b1y35nczqcjz90sb8jj8pg6pv9rnzbwb")))

(define-public crate-ractor_cluster_derive-0.8.5 (c (n "ractor_cluster_derive") (v "0.8.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "03aklka59fkangixkdrpvz8rgddvpjpkhygik27bvqcqflsli1ks") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.0 (c (n "ractor_cluster_derive") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gvc4v6kip4g1cd96qwxddrs19x1cfzfxy6ipkq2wdxq3ykpvcxm") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.1 (c (n "ractor_cluster_derive") (v "0.9.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0q3nizv10hbhy5zxcwhd602fiivglq31wn7glbriml32vmrjxmq1") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.2 (c (n "ractor_cluster_derive") (v "0.9.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fk127jl1n7sdz0cdbyc0gni4mgasqyvn99bx8y7ss7m0fnbpyri") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.3 (c (n "ractor_cluster_derive") (v "0.9.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bzzd0rm6c6rnxhnfs5yfza04m3xj3s4c92z62k050z3apwqifa1") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.5 (c (n "ractor_cluster_derive") (v "0.9.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0w4g3zqxqd97d1wql5mmcalzy4dla8xysrd216lnaj3k6q86l004") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.6 (c (n "ractor_cluster_derive") (v "0.9.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ijaak8zmbkfk0wsjjzpwx7jwpyn9swv5g9xkn64pgly1b1vq8i3") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.7 (c (n "ractor_cluster_derive") (v "0.9.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wkrmhv0149kdamy3wd3ws27wy4hd2pxjsc4ms10kisg16hp96xm") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.9.8 (c (n "ractor_cluster_derive") (v "0.9.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0awr81w0vyq3n0gjmm23kgpwvhrllghzplhp8k2z15pzjk4knqbq") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.10.0 (c (n "ractor_cluster_derive") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qmihsmgbmh3lh512sx1nbpdg2psqgmvd2nppsyyn4p2c2ng2wjq") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.10.1 (c (n "ractor_cluster_derive") (v "0.10.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02n1dhgh5dxm9prfjk8gi5s9f4i90mfjdbw13mc19r4yd2xva1qw") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.10.2 (c (n "ractor_cluster_derive") (v "0.10.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0iv2xw6fn6979fzr64cj0zy03n36k9vyfsaqhw77cq48md4y2zd2") (r "1.64")))

(define-public crate-ractor_cluster_derive-0.10.3 (c (n "ractor_cluster_derive") (v "0.10.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "111d7wx5fw6p11qpbf2fiwyzwsyz85jfc6wcmmh3d9hjqdr97yc7") (r "1.64")))

