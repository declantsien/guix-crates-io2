(define-module (crates-io ra ui raui-binary-renderer) #:use-module (crates-io))

(define-public crate-raui-binary-renderer-0.5.0 (c (n "raui-binary-renderer") (v "0.5.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d7vjg75y78rwifnvhzzxi9jw77y24pj1vyy5hs9gaa3mscf8x0l")))

(define-public crate-raui-binary-renderer-0.6.0 (c (n "raui-binary-renderer") (v "0.6.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1krg071skph9p9hppslybccvif0pf90ji7a42s2dqbhwr0q8ngla")))

(define-public crate-raui-binary-renderer-0.7.0 (c (n "raui-binary-renderer") (v "0.7.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sr1vp7y9ap5pwqa5bplgsypaz9092dbb1mjz76395vdf9sn9rma")))

(define-public crate-raui-binary-renderer-0.8.0 (c (n "raui-binary-renderer") (v "0.8.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fgbp4213nbj871w16xhpr0xihlzpkc04dvxkxb5wf7j564in6la")))

(define-public crate-raui-binary-renderer-0.9.1 (c (n "raui-binary-renderer") (v "0.9.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bnhjkac0wvhrmj5qi845gqf3gd20mmhdv5kzrp9ql6lpm3z7q4c")))

(define-public crate-raui-binary-renderer-0.9.2 (c (n "raui-binary-renderer") (v "0.9.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a2aj8q1s1dgm20y8fdlx39xf93i122ddh89p3qk94ilk9k7lflk")))

(define-public crate-raui-binary-renderer-0.10.0 (c (n "raui-binary-renderer") (v "0.10.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f97vqxd3vygis3g1bldx64rb3188v9bh154dyi3vfkq3mir3sxi")))

(define-public crate-raui-binary-renderer-0.11.0 (c (n "raui-binary-renderer") (v "0.11.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09x7zjpr52h2nwzw822wlrk2rxc07s0swx1hvcpcfxrzk86ghvc9")))

(define-public crate-raui-binary-renderer-0.12.1 (c (n "raui-binary-renderer") (v "0.12.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0959ww3yjamaclmjin3x720v7dp5x1vfpid0izw74zm0dvx7aqfm")))

(define-public crate-raui-binary-renderer-0.13.0 (c (n "raui-binary-renderer") (v "0.13.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "186va6glm0f4hkf7vm24gf8mhps7r30pdq6nv7af5h2sp2rvzalg")))

(define-public crate-raui-binary-renderer-0.14.0 (c (n "raui-binary-renderer") (v "0.14.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mcx868gm0rv4jq9va5w17b4gbinpi9wd8r4g1bllzrq5zs5l4v5")))

(define-public crate-raui-binary-renderer-0.15.0 (c (n "raui-binary-renderer") (v "0.15.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "007wwlpssjiwb59rc5mhsn1mqvyivxpbf2pm5gmpry6ckxnv082f")))

(define-public crate-raui-binary-renderer-0.16.0 (c (n "raui-binary-renderer") (v "0.16.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09700k7k0xny893fv01a6063imdp6qcbpcdi6pn050s24w1c47r2")))

(define-public crate-raui-binary-renderer-0.17.0 (c (n "raui-binary-renderer") (v "0.17.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i0x5f89w2anra76wjf1di2abk0ydhiz8xc4lmbhvwgy1icbr9h2")))

(define-public crate-raui-binary-renderer-0.18.0 (c (n "raui-binary-renderer") (v "0.18.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q1j5nv7l1bnxzh7dabqzdpzylpw6vryc2r9jym420iw7z5w3929")))

(define-public crate-raui-binary-renderer-0.19.0 (c (n "raui-binary-renderer") (v "0.19.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rp5qi24yyf76a5h1xwvnm7mgs5scxi3l31nz9p9frr3hd6clj0n")))

(define-public crate-raui-binary-renderer-0.20.0 (c (n "raui-binary-renderer") (v "0.20.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i7wniwvzbl1n5bx3xzvlbiv0mlr0mrydrwziw55mx13prjx7zh6")))

(define-public crate-raui-binary-renderer-0.20.1 (c (n "raui-binary-renderer") (v "0.20.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12d43p9m9akw252apgayfd2dm30r57ykpskbkfnkkpm56pz40myj")))

(define-public crate-raui-binary-renderer-0.20.2 (c (n "raui-binary-renderer") (v "0.20.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0is27bpw38n0znsnm1w61kfs32qm8q7rwdg7b336l4lsyd410v68")))

(define-public crate-raui-binary-renderer-0.21.0 (c (n "raui-binary-renderer") (v "0.21.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cgha687aa55rbx6jdxyfwg6b9yyi7i382fpn4fif1zxvj8qgdfn")))

(define-public crate-raui-binary-renderer-0.21.1 (c (n "raui-binary-renderer") (v "0.21.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h5mx7bj9p8a0q15fmr4ibwf0qvkdgcwn1h4w1shprgscygkmy6")))

(define-public crate-raui-binary-renderer-0.21.2 (c (n "raui-binary-renderer") (v "0.21.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04h68475396a2xhirb9iw4hf71n01ncy7rl0p87l7f91gv6hx5bm")))

(define-public crate-raui-binary-renderer-0.21.3 (c (n "raui-binary-renderer") (v "0.21.3") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r7ksmw94kxa9fyfiz0f6pzzlfycv9sghn01v6mwxk1lxnf0f90l")))

(define-public crate-raui-binary-renderer-0.22.0 (c (n "raui-binary-renderer") (v "0.22.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bgnb7j7hydls0bcy52163484zahb3l2q5rk870am8iddk427x90")))

(define-public crate-raui-binary-renderer-0.23.0 (c (n "raui-binary-renderer") (v "0.23.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r2miicwmjcfpr4bd120rw87500mcvxz68xzh2xgvrm5ndhvq2qw")))

(define-public crate-raui-binary-renderer-0.24.0 (c (n "raui-binary-renderer") (v "0.24.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jvily0qvh7p31bff65wmp82bllh2sy25jbicr19in7qm6akp67c")))

(define-public crate-raui-binary-renderer-0.25.0 (c (n "raui-binary-renderer") (v "0.25.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rnmkc0f62qwh389zl5hac24rd9sv6vrf30c1p5rgwhj9x06lym1")))

(define-public crate-raui-binary-renderer-0.26.0 (c (n "raui-binary-renderer") (v "0.26.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10zw885bq7ywb7alsxhll22gr0qnk8pnkcmg78d2xrgqq935ycv0")))

(define-public crate-raui-binary-renderer-0.27.0 (c (n "raui-binary-renderer") (v "0.27.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hq4kj1yl4wy7yg13ka43k8xws4kpwdx30az4g98idycg3hzabfa")))

(define-public crate-raui-binary-renderer-0.27.1 (c (n "raui-binary-renderer") (v "0.27.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cvx6hdrd530i3a7jmcypamjb3kz0mj48mr5bm1c65xa254i7df5")))

(define-public crate-raui-binary-renderer-0.28.0 (c (n "raui-binary-renderer") (v "0.28.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z1b5633rlpd7sng5vhnsslh98v606sxvks7q1x6k7wpgz6xrpwh")))

(define-public crate-raui-binary-renderer-0.28.1 (c (n "raui-binary-renderer") (v "0.28.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fkly5ab7vjb1hg6q3di6kcinqhb4rqas9sp7776cyl6br1x66zr")))

(define-public crate-raui-binary-renderer-0.29.0 (c (n "raui-binary-renderer") (v "0.29.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bhk63wv55zxgxsiw0sz5dlnwd6dbvnv23qahkqldx9vaszb9y4n")))

(define-public crate-raui-binary-renderer-0.30.0 (c (n "raui-binary-renderer") (v "0.30.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vfn3s69kdrl7mi4xmq8a9v4ywibnlxn0bvz4b8k08i514d1yz62")))

(define-public crate-raui-binary-renderer-0.31.0 (c (n "raui-binary-renderer") (v "0.31.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ih0w2b1a7h2zvl1xzb4w6d0ibw3vx3vybbw0k6vixqfvyd0kzs3")))

(define-public crate-raui-binary-renderer-0.32.0 (c (n "raui-binary-renderer") (v "0.32.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hk134nyiacb10pbrjhc3v0m5yad4dsgbnyxf4zfj062j4jvqc8j")))

(define-public crate-raui-binary-renderer-0.33.0 (c (n "raui-binary-renderer") (v "0.33.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lqp86sx2dka60rl6bva76r0ka9dz81wy85bg2q6ds0ra9x29jd8")))

(define-public crate-raui-binary-renderer-0.34.0 (c (n "raui-binary-renderer") (v "0.34.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c7lzh5hvw4divvcvrcblswlszcyyhjg769nvd6xllgn7d9vj4sm")))

(define-public crate-raui-binary-renderer-0.35.0 (c (n "raui-binary-renderer") (v "0.35.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "012vw11gmmp1lqcz7jxq3rws0cbxkrmnprjgiif4shd80xawcgwp")))

(define-public crate-raui-binary-renderer-0.36.0 (c (n "raui-binary-renderer") (v "0.36.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s9vf10sik5acn79mr1a2f1zcb275z5b1y61hb5sm3d0brnql6qd")))

(define-public crate-raui-binary-renderer-0.37.0 (c (n "raui-binary-renderer") (v "0.37.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c62bwqvw93f7a3zms2nbyjqzmy6q3pzq7mzkfmb6c2p6527zayv")))

(define-public crate-raui-binary-renderer-0.37.1 (c (n "raui-binary-renderer") (v "0.37.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a2912anlg712yx34w8j27h1q9zvcnshikwpi5gpmsi9ld41xynw")))

(define-public crate-raui-binary-renderer-0.38.0 (c (n "raui-binary-renderer") (v "0.38.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pcdg832zgzn3ic8la5zff5j3s52w1zzsvy8z1va263jgxkd3fb7")))

(define-public crate-raui-binary-renderer-0.38.1 (c (n "raui-binary-renderer") (v "0.38.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02s0m16ind35x1qyqbs8y14zx7c8r5ipapkhayf6gll8903g54sm")))

(define-public crate-raui-binary-renderer-0.38.2 (c (n "raui-binary-renderer") (v "0.38.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yg28zcw0zah5vrg34137lnb19dzg6lgf2wvqqy745qramrf433r")))

(define-public crate-raui-binary-renderer-0.38.3 (c (n "raui-binary-renderer") (v "0.38.3") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02hmlfcanfdfqvpcx6dm4lyb4aq8rc5dcc3b56708yalxd1v862y")))

(define-public crate-raui-binary-renderer-0.38.4 (c (n "raui-binary-renderer") (v "0.38.4") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wsp7f44jn0ym32ldanh0fcskh0qbmzm90gscm16n9i1jlsz53mn")))

(define-public crate-raui-binary-renderer-0.40.0 (c (n "raui-binary-renderer") (v "0.40.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wmq147bkdykcj9ymqqby5ksnmli9d0wrcjq3jhczhhiplknz43j")))

(define-public crate-raui-binary-renderer-0.41.0 (c (n "raui-binary-renderer") (v "0.41.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qki0qxfp6hw5pnpay105k6mvg1i4z4krmvq5i0mqjkpxxb07jq8")))

(define-public crate-raui-binary-renderer-0.42.0 (c (n "raui-binary-renderer") (v "0.42.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12rnb04lckhwvagkgpwdjfvnh2niw0x2v291k9dbjna7g0n5zdr3")))

(define-public crate-raui-binary-renderer-0.43.0 (c (n "raui-binary-renderer") (v "0.43.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vysla48139w0r5q2vb1srjn4q7f37zklm8dz3szvppy6pi7x8k0")))

