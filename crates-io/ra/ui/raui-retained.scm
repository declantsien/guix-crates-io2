(define-module (crates-io ra ui raui-retained) #:use-module (crates-io))

(define-public crate-raui-retained-0.42.0 (c (n "raui-retained") (v "0.42.0") (d (list (d (n "raui-core") (r "^0.42") (d #t) (k 0)))) (h "1p6vam82xdlnpsj8hp9g9wkshcqhj1qzfzc9026palf178ar9f25")))

(define-public crate-raui-retained-0.43.0 (c (n "raui-retained") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)))) (h "1r3xd3n3rnhj92w2mn0n31gfg3nmdc160xhk9g969q6mqf8n91f5")))

(define-public crate-raui-retained-0.44.0 (c (n "raui-retained") (v "0.44.0") (d (list (d (n "raui-core") (r "^0.44") (d #t) (k 0)))) (h "1n4a2qgplhhjaiszf4jf8b27i968lnvwadp615hn4jx0wd6pyyrg")))

(define-public crate-raui-retained-0.45.0 (c (n "raui-retained") (v "0.45.0") (d (list (d (n "raui-core") (r "^0.45") (d #t) (k 0)))) (h "1jf18pv9ir58gkfwddyz29d9y2kl0b1ya4m5adv40z5hin5piqvx")))

(define-public crate-raui-retained-0.46.0 (c (n "raui-retained") (v "0.46.0") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)))) (h "0dbk4x3kgamzlwppbsv637aksn9lrv11iw6k17rjcfmamm9l5dxn")))

(define-public crate-raui-retained-0.46.1 (c (n "raui-retained") (v "0.46.1") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)))) (h "1p8bysq8rvihacirf0vkbdxh4njhk107qmrwn5zcq9zw05knsazz")))

(define-public crate-raui-retained-0.46.2 (c (n "raui-retained") (v "0.46.2") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)))) (h "0lzw5a8r3ff6z4wa65z0b2ypb78fml1wvdzvx2g6bjrhqi6jbag4")))

(define-public crate-raui-retained-0.46.3 (c (n "raui-retained") (v "0.46.3") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)))) (h "1ibmsl34ribgf2l3znjmaz12cgvxw105hyni9z0vjhm1jqxw91qz")))

(define-public crate-raui-retained-0.47.0 (c (n "raui-retained") (v "0.47.0") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)))) (h "0gg6lzyg5kgsjkkindw0hmbhbj6yvdrs17jpk3f322x4v777mqzs")))

(define-public crate-raui-retained-0.47.1 (c (n "raui-retained") (v "0.47.1") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)))) (h "161610plbqa1rpnr48bzw38fz72bsf44absqqqhldxia6blmg790")))

(define-public crate-raui-retained-0.47.2 (c (n "raui-retained") (v "0.47.2") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)))) (h "13rxpgfy5qnng0qwn6ajqx3k8ydyk34b7j053bv9vv17h4j7nhdz")))

(define-public crate-raui-retained-0.48.0 (c (n "raui-retained") (v "0.48.0") (d (list (d (n "raui-core") (r "^0.48") (d #t) (k 0)))) (h "11jjczgvw5rnwqmy5fjaxzii4k0r5ijicb9kmg1xah2128asaqz5")))

(define-public crate-raui-retained-0.49.0 (c (n "raui-retained") (v "0.49.0") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)))) (h "1rjim43pxy2ws8273yakxkkwkssnhk42pkv44594ir1sc4mh9hjf")))

(define-public crate-raui-retained-0.49.1 (c (n "raui-retained") (v "0.49.1") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)))) (h "1rqlscv8yk1fffqwh0swmmlkrbv3fray0ppfgl711285j0waifxn")))

(define-public crate-raui-retained-0.49.2 (c (n "raui-retained") (v "0.49.2") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)))) (h "1xkhfhrj9y2hrc0wapa6c4g722vqs8qylm4albig3lc32j0mqyq1")))

(define-public crate-raui-retained-0.50.0 (c (n "raui-retained") (v "0.50.0") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)))) (h "0x3x56idr83r6jd08a7m1lxxifsgx3haang5kvad3k5192xssxh9")))

(define-public crate-raui-retained-0.50.1 (c (n "raui-retained") (v "0.50.1") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)))) (h "0hbbqdy7d9fr8aq79kbzwhry1qppncl4gbkf48z4b0yhnxp5rxzz")))

(define-public crate-raui-retained-0.50.2 (c (n "raui-retained") (v "0.50.2") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)))) (h "1zz2dd4vlf64hpl9in0lcw3w8zj9sc29xhwfbv67dnvnzbhbm98w")))

(define-public crate-raui-retained-0.50.3 (c (n "raui-retained") (v "0.50.3") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)))) (h "1vh4vrzbd3sywc8d1h367j11d75x3r2cv9niyxfwipfvxf8iqv98")))

(define-public crate-raui-retained-0.51.0 (c (n "raui-retained") (v "0.51.0") (d (list (d (n "raui-core") (r "^0.51") (d #t) (k 0)))) (h "0mmihzhx0k8yifif4py8y7d5zsmj880clbnd48x555i80nbgq7fx")))

(define-public crate-raui-retained-0.52.0 (c (n "raui-retained") (v "0.52.0") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)))) (h "1haa0ap0vja55z3y0c1r00wjcqgm6ny4lymcsxr9kg7v8abrk0g1")))

(define-public crate-raui-retained-0.52.1 (c (n "raui-retained") (v "0.52.1") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)))) (h "1dvc25pnhff7ibv7ib6d13i1s9q6v5n136xa9p9p04n93gq4yrpf")))

(define-public crate-raui-retained-0.53.0 (c (n "raui-retained") (v "0.53.0") (d (list (d (n "raui-core") (r "^0.53") (d #t) (k 0)))) (h "0zv82bb0zvb79m0fwq4s34yfhbs20clxxiaa33va1zqqjd419wk2")))

(define-public crate-raui-retained-0.54.0 (c (n "raui-retained") (v "0.54.0") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)))) (h "0ylavyq11qdmn5h4p7mgyayxrqhgnfq1wafwhdjbbbpmsk5dz1ky")))

(define-public crate-raui-retained-0.54.1 (c (n "raui-retained") (v "0.54.1") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)))) (h "0ll24ksp5jf3gk6lwl260kfcir0k1gyd6syh9xj4dz33wfnhblg1")))

(define-public crate-raui-retained-0.55.0 (c (n "raui-retained") (v "0.55.0") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "17icn69balww9zcjr8bwgjjvk3s6w37grzv54iwsq950hnfh8inl")))

(define-public crate-raui-retained-0.55.1 (c (n "raui-retained") (v "0.55.1") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "0gyvrk9h738mg5xhx5nkw29j2gnwpmlcpa7jp69pysi6mg4z97xg")))

(define-public crate-raui-retained-0.55.2 (c (n "raui-retained") (v "0.55.2") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "11gkwmd418721bf02q4hbh4y724gqks525flkpb28i625a79iync")))

(define-public crate-raui-retained-0.55.3 (c (n "raui-retained") (v "0.55.3") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "0rcx2rkwfhf5br0frsh67prwbdddn2zhh5a5xpyhh0yvha8fx286")))

(define-public crate-raui-retained-0.55.4 (c (n "raui-retained") (v "0.55.4") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "0mvynhwizz40zrm2g5pfhbzqppgdv67n9025k7a46yl4g9kgsyl2")))

(define-public crate-raui-retained-0.55.5 (c (n "raui-retained") (v "0.55.5") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "1vlapcx7pvf45pb5kaxjqsmcccq0xrprhdshpazp1h0xw8nls2py")))

(define-public crate-raui-retained-0.55.6 (c (n "raui-retained") (v "0.55.6") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)))) (h "0cvhsfzhdxssvprhbv63zdd2jfvh6h19jbnsn4xlnywqfr8rw21g")))

(define-public crate-raui-retained-0.56.0 (c (n "raui-retained") (v "0.56.0") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)))) (h "0alj7z9p8j2ccr8q7gz2zrm81w6zdrsi9aaxcbv6ihx1l1wdzcap")))

(define-public crate-raui-retained-0.56.1 (c (n "raui-retained") (v "0.56.1") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)))) (h "19299jbihf7rrzknmwbyb58dbjqrnmpndb8m9vkz2mzdlqrsyj5d")))

(define-public crate-raui-retained-0.57.0 (c (n "raui-retained") (v "0.57.0") (d (list (d (n "raui-core") (r "^0.57") (d #t) (k 0)))) (h "1hb4rjg76sjxkp8hg26v79nrdzzpahfjp1a6gjpm1330b5v0a93n")))

(define-public crate-raui-retained-0.58.0 (c (n "raui-retained") (v "0.58.0") (d (list (d (n "raui-core") (r "^0.58") (d #t) (k 0)))) (h "1p6hzfq6vwn11y1sh5wdbqx0zbhyi2qwxm6x9qkl6ihlvq4z3jlq")))

(define-public crate-raui-retained-0.59.0 (c (n "raui-retained") (v "0.59.0") (d (list (d (n "raui-core") (r "^0.59") (d #t) (k 0)))) (h "1z5c6nh8a0h79bprwfyp9si89954mzaz0kvjhky9kdzp7msfksqh")))

(define-public crate-raui-retained-0.60.0 (c (n "raui-retained") (v "0.60.0") (d (list (d (n "raui-core") (r "^0.60") (d #t) (k 0)))) (h "0kd7xcspf904w23jnw9szv7ii7c61w0qm969pk2v6w2fdj9bhqnx")))

(define-public crate-raui-retained-0.61.0 (c (n "raui-retained") (v "0.61.0") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)))) (h "1r37f4qfn7r9anal054wqyxk5z8qlc3nnm1wpf338lk5bh7gxks9")))

(define-public crate-raui-retained-0.61.1 (c (n "raui-retained") (v "0.61.1") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)))) (h "0a0a51y5prnlyd709kpfnsl191fp59i6lbpcmlc0yfc8v2vf0433")))

(define-public crate-raui-retained-0.61.2 (c (n "raui-retained") (v "0.61.2") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)))) (h "0v5jb1f3wawmyf0ii3ryp0fpbgmi61x97grvzhqrhf71cji2sxh0")))

(define-public crate-raui-retained-0.62.0 (c (n "raui-retained") (v "0.62.0") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)))) (h "1l6zg2g7n676a5phfgvi2gpzya7n76lzfyahzc1qrx1hkyckm4nm")))

(define-public crate-raui-retained-0.62.1 (c (n "raui-retained") (v "0.62.1") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)))) (h "0l5rvjky4bld6vz7nnbxd7wak2615bvgkibxr5mnrsq3ff3c179z")))

(define-public crate-raui-retained-0.63.0 (c (n "raui-retained") (v "0.63.0") (d (list (d (n "raui-core") (r "^0.63") (d #t) (k 0)))) (h "0hyix7f6dykp8s7arzp24b9xv6rbjwhw8ripf3lassyaiz1k4rj0")))

