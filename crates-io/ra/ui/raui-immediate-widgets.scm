(define-module (crates-io ra ui raui-immediate-widgets) #:use-module (crates-io))

(define-public crate-raui-immediate-widgets-0.43.0 (c (n "raui-immediate-widgets") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.43") (d #t) (k 0)) (d (n "raui-material") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s1b2kgjlir52lkncg1sjcvp48x203ljx9dq9c8qv8m5ipingxbw")))

(define-public crate-raui-immediate-widgets-0.44.0 (c (n "raui-immediate-widgets") (v "0.44.0") (d (list (d (n "raui-core") (r "^0.44") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.44") (d #t) (k 0)) (d (n "raui-material") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gj7gn3kqffn3948ikxj5j5dq8z79kv5xivvbyvapqhbq9vl71jk")))

(define-public crate-raui-immediate-widgets-0.45.0 (c (n "raui-immediate-widgets") (v "0.45.0") (d (list (d (n "raui-core") (r "^0.45") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.45") (d #t) (k 0)) (d (n "raui-material") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jb59mrrpvfvrbzaxmfki29k5wza0mf8jcy9jhf883mrcq1c0hsz")))

(define-public crate-raui-immediate-widgets-0.46.0 (c (n "raui-immediate-widgets") (v "0.46.0") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.46") (d #t) (k 0)) (d (n "raui-material") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "025v5d2kzk7rinafc9mkp413gav3bm388lsffkjdrhsx4ipgmdg7")))

(define-public crate-raui-immediate-widgets-0.46.1 (c (n "raui-immediate-widgets") (v "0.46.1") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.46") (d #t) (k 0)) (d (n "raui-material") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s3q9cnvg0rwvk4ki2x3cyvpp2jw0v5mcgpky315jwfrrn8dcgdw")))

(define-public crate-raui-immediate-widgets-0.46.2 (c (n "raui-immediate-widgets") (v "0.46.2") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.46") (d #t) (k 0)) (d (n "raui-material") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n3zszjaz9ag67wqkgh5p3qc22vc7v6i300agvppd6l1hanf8792")))

(define-public crate-raui-immediate-widgets-0.46.3 (c (n "raui-immediate-widgets") (v "0.46.3") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.46") (d #t) (k 0)) (d (n "raui-material") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17r6240n25h1r36dgxphl3p531h7xxznzbqzj4j9l4acaf1cz7gd")))

(define-public crate-raui-immediate-widgets-0.47.0 (c (n "raui-immediate-widgets") (v "0.47.0") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.47") (d #t) (k 0)) (d (n "raui-material") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nxcgcpxgq2yxf1bsa585i2zc0pppksf98i4s8bsl5jq3rmkk3ww")))

(define-public crate-raui-immediate-widgets-0.47.1 (c (n "raui-immediate-widgets") (v "0.47.1") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.47") (d #t) (k 0)) (d (n "raui-material") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ixbg3kadd7zhm0q78xs62dn8r59r1g2w2c3ni2wfsih324w0yzl")))

(define-public crate-raui-immediate-widgets-0.47.2 (c (n "raui-immediate-widgets") (v "0.47.2") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.47") (d #t) (k 0)) (d (n "raui-material") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5396xl8q1akzcf6lyks4m5v2mpyncb6gpzfs1mzj40npak035x")))

(define-public crate-raui-immediate-widgets-0.48.0 (c (n "raui-immediate-widgets") (v "0.48.0") (d (list (d (n "raui-core") (r "^0.48") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.48") (d #t) (k 0)) (d (n "raui-material") (r "^0.48") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wfbvp452js7ysx2yaqr92kpwqch9fq3r36fyp2mcmp2598d80yc")))

(define-public crate-raui-immediate-widgets-0.49.0 (c (n "raui-immediate-widgets") (v "0.49.0") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.49") (d #t) (k 0)) (d (n "raui-material") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hss586p5h5acmdnzjpd80fhnavrmbm3a7cw7kvp42ickvi5f507")))

(define-public crate-raui-immediate-widgets-0.49.1 (c (n "raui-immediate-widgets") (v "0.49.1") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.49") (d #t) (k 0)) (d (n "raui-material") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11hz3pnnjq9dkpvmrgixk8zjcasxg62qj4kimrsq57ws83hmqmq9")))

(define-public crate-raui-immediate-widgets-0.49.2 (c (n "raui-immediate-widgets") (v "0.49.2") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.49") (d #t) (k 0)) (d (n "raui-material") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bkblfskwlk47ypzmxddg6pwplgzmwzpdhwx60lnb6723farmyc7")))

(define-public crate-raui-immediate-widgets-0.50.0 (c (n "raui-immediate-widgets") (v "0.50.0") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.50") (d #t) (k 0)) (d (n "raui-material") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cmr7w7pzhpyy88b6mkj3igdpxx05lnwiqf1gpwfzv103cwsym7s")))

(define-public crate-raui-immediate-widgets-0.50.1 (c (n "raui-immediate-widgets") (v "0.50.1") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.50") (d #t) (k 0)) (d (n "raui-material") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00ivy9gvvibk81lfsxa7sc2fagn533p8rz9c98g3ri9rb676yi8g")))

(define-public crate-raui-immediate-widgets-0.50.2 (c (n "raui-immediate-widgets") (v "0.50.2") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.50") (d #t) (k 0)) (d (n "raui-material") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cnq85aqjw9g6sf8zzbccxk72wgwsc6afq4q3q2n0fm6nza9lk19")))

(define-public crate-raui-immediate-widgets-0.50.3 (c (n "raui-immediate-widgets") (v "0.50.3") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.50") (d #t) (k 0)) (d (n "raui-material") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zilpkanfaz9gb72586r3hmaiq6x2xbn209wg7bj0kn55mfc5ski")))

(define-public crate-raui-immediate-widgets-0.51.0 (c (n "raui-immediate-widgets") (v "0.51.0") (d (list (d (n "raui-core") (r "^0.51") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.51") (d #t) (k 0)) (d (n "raui-material") (r "^0.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "177p4nv2qvlfmxq957wdk0174x50vh2bbmk5sv7iw5dix986aahc")))

(define-public crate-raui-immediate-widgets-0.52.0 (c (n "raui-immediate-widgets") (v "0.52.0") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.52") (d #t) (k 0)) (d (n "raui-material") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06n1ikji2cijyvjgqbwg7k7lwy5xfbl2jkyjl147iar5zh6qa3yn")))

(define-public crate-raui-immediate-widgets-0.52.1 (c (n "raui-immediate-widgets") (v "0.52.1") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.52") (d #t) (k 0)) (d (n "raui-material") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09zjsyhw4gjl7bvrj0crlim9pkknbjr3v1x5rrmwbld4ygzf4bdz")))

(define-public crate-raui-immediate-widgets-0.53.0 (c (n "raui-immediate-widgets") (v "0.53.0") (d (list (d (n "raui-core") (r "^0.53") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.53") (d #t) (k 0)) (d (n "raui-material") (r "^0.53") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qxk2vr3iyk2v8bc43859vd4l9i49cnnd2vr5ldq9zci4cmnrz73")))

(define-public crate-raui-immediate-widgets-0.54.0 (c (n "raui-immediate-widgets") (v "0.54.0") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.54") (d #t) (k 0)) (d (n "raui-material") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10y7dw6cjc0n5n3fladckl3v3vwarfvxajj2kd7fmbich6138h8y")))

(define-public crate-raui-immediate-widgets-0.54.1 (c (n "raui-immediate-widgets") (v "0.54.1") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.54") (d #t) (k 0)) (d (n "raui-material") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sfp23syzc7q619xz1w7rd0p2v3bsfa2i2k1xwy5hixdyx4j6hhx")))

(define-public crate-raui-immediate-widgets-0.55.0 (c (n "raui-immediate-widgets") (v "0.55.0") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qk6fzhlkjihjh0hx0048hjx4q2lfwci534mx49m8rrd3c9l1qjh")))

(define-public crate-raui-immediate-widgets-0.55.1 (c (n "raui-immediate-widgets") (v "0.55.1") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12hjbdd17v81ibnfvr165qhjv0nfzgqb7rrqzsgals9vrqg860y4")))

(define-public crate-raui-immediate-widgets-0.55.2 (c (n "raui-immediate-widgets") (v "0.55.2") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ip5lg1vxf6lq9lrhgbp8vgwdbh7116m4vmvsrhhqapj3f4d7a1l")))

(define-public crate-raui-immediate-widgets-0.55.3 (c (n "raui-immediate-widgets") (v "0.55.3") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gx2r7dpz6qxspmk5lrynpimrwxs79w5piasz6rz0nx38xp0h5kh")))

(define-public crate-raui-immediate-widgets-0.55.4 (c (n "raui-immediate-widgets") (v "0.55.4") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c9sd1xcrvlbvyrlq9cmbq1qvyhjkaykjjqxbg7pv5r6pni5qjvy")))

(define-public crate-raui-immediate-widgets-0.55.5 (c (n "raui-immediate-widgets") (v "0.55.5") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "117g3xc9np0ypnqyng4jlyvbhc6njr14ji8sbvi0l6m8lc470xfx")))

(define-public crate-raui-immediate-widgets-0.55.6 (c (n "raui-immediate-widgets") (v "0.55.6") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.55") (d #t) (k 0)) (d (n "raui-material") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x2dadarc3klb4hgsqsn0s07iv8br56s9877myslyf8r58m63nbv")))

(define-public crate-raui-immediate-widgets-0.56.0 (c (n "raui-immediate-widgets") (v "0.56.0") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.56") (d #t) (k 0)) (d (n "raui-material") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fhkayn4ql958izmkdh168xx2lpsiwh95s56rlr4sic8dik91203")))

(define-public crate-raui-immediate-widgets-0.56.1 (c (n "raui-immediate-widgets") (v "0.56.1") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.56") (d #t) (k 0)) (d (n "raui-material") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nhw69nr79xnbw9wc2fbkdnx740ps9rn7nq5mr2g4icxm0gqj6gd")))

(define-public crate-raui-immediate-widgets-0.57.0 (c (n "raui-immediate-widgets") (v "0.57.0") (d (list (d (n "raui-core") (r "^0.57") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.57") (d #t) (k 0)) (d (n "raui-material") (r "^0.57") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03891kqakgh575qxwscxq4mvy60g75nzy0aif5rvmal9kqp5vs1s")))

(define-public crate-raui-immediate-widgets-0.58.0 (c (n "raui-immediate-widgets") (v "0.58.0") (d (list (d (n "raui-core") (r "^0.58") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.58") (d #t) (k 0)) (d (n "raui-material") (r "^0.58") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19iaszi67zy5ra340palwwgi391l4vy3d2zh92103hgxvgb1hfs3")))

(define-public crate-raui-immediate-widgets-0.59.0 (c (n "raui-immediate-widgets") (v "0.59.0") (d (list (d (n "raui-core") (r "^0.59") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.59") (d #t) (k 0)) (d (n "raui-material") (r "^0.59") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0266l77wagb9af6ryc2va42jshlbafadwmjwxcpxhb12ks554b0f")))

(define-public crate-raui-immediate-widgets-0.60.0 (c (n "raui-immediate-widgets") (v "0.60.0") (d (list (d (n "raui-core") (r "^0.60") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.60") (d #t) (k 0)) (d (n "raui-material") (r "^0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "195wllrcpsa4gl7qqyj65b30i99vqmpn6j9xmnxdwaqf3i3616bv")))

(define-public crate-raui-immediate-widgets-0.61.0 (c (n "raui-immediate-widgets") (v "0.61.0") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.61") (d #t) (k 0)) (d (n "raui-material") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g31q07p0jfb8xk3d8ddvcqqm15jk4pcni6hdb7nmjx3sfyvwq1w")))

(define-public crate-raui-immediate-widgets-0.61.1 (c (n "raui-immediate-widgets") (v "0.61.1") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.61") (d #t) (k 0)) (d (n "raui-material") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17kp51iidxgwqjn0ipy8xhwjm06j3l8dywxgzvb9ywvlssl373r4")))

(define-public crate-raui-immediate-widgets-0.61.2 (c (n "raui-immediate-widgets") (v "0.61.2") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.61") (d #t) (k 0)) (d (n "raui-material") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v5gxll50z72ibq0dwr2pms9ajqg24lbfn8f4z81qjzfqzynb6kn")))

(define-public crate-raui-immediate-widgets-0.62.0 (c (n "raui-immediate-widgets") (v "0.62.0") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.62") (d #t) (k 0)) (d (n "raui-material") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mdwj2d01w835pf2hhjwxij8ml6708m2xgjkzw2dxrl9vpzzmw0a")))

(define-public crate-raui-immediate-widgets-0.62.1 (c (n "raui-immediate-widgets") (v "0.62.1") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.62") (d #t) (k 0)) (d (n "raui-material") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqnh3ky39icz73saadzr9i66qpj25k8sl2hcfhk23daj457zhlv")))

(define-public crate-raui-immediate-widgets-0.63.0 (c (n "raui-immediate-widgets") (v "0.63.0") (d (list (d (n "raui-core") (r "^0.63") (d #t) (k 0)) (d (n "raui-immediate") (r "^0.63") (d #t) (k 0)) (d (n "raui-material") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0piffhkqc4b640n85rg41v5sv1jz9l1q0qdns52yn0vw5039z3hd")))

