(define-module (crates-io ra ui raui-derive) #:use-module (crates-io))

(define-public crate-raui-derive-0.30.0 (c (n "raui-derive") (v "0.30.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0bhdp3k37i8gdw78gwjippcgqkpy9kb4acpfyd55r9lglq1dp72q")))

(define-public crate-raui-derive-0.31.0 (c (n "raui-derive") (v "0.31.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "07gdk6gkq9ldm749jk6my4k6k07mc5d94dg018p0a5hh47j6q6ax")))

(define-public crate-raui-derive-0.32.0 (c (n "raui-derive") (v "0.32.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1a2qvz49nmym1713xd0mhz0winaxg7z69l5j73gn1hhx8gim21fi")))

(define-public crate-raui-derive-0.33.0 (c (n "raui-derive") (v "0.33.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y5b375lwya1qlh2g1d80i361nf5wrzc0s97lamj0y63zhvc275b")))

(define-public crate-raui-derive-0.34.0 (c (n "raui-derive") (v "0.34.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1dra9riafvy7vk54h9bj928vbaqgxaikh4lpp110lqdyh9ysyvfc")))

(define-public crate-raui-derive-0.35.0 (c (n "raui-derive") (v "0.35.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0krf1r17lkvkc9z4wddhcq42s483zi6g5821821v2c1nrcwznyms")))

(define-public crate-raui-derive-0.36.0 (c (n "raui-derive") (v "0.36.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0z25saxbc7qk0hmhvy8hzz7j4w7a8wcqnf32qdxsb9vqkskkdmdi")))

(define-public crate-raui-derive-0.37.0 (c (n "raui-derive") (v "0.37.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0skfvwwmbddwx4nhfp5dd99qhl2iz379dhj6ymay6hw72b23qg8y")))

(define-public crate-raui-derive-0.37.1 (c (n "raui-derive") (v "0.37.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cr99r7d1ym4q482chzvs0mcixngblassj4d2zjzj5g2wms8srf6")))

(define-public crate-raui-derive-0.38.0 (c (n "raui-derive") (v "0.38.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1iwf80f1f9hdi5710q7iwqv8zvzk2as9r48rig4m0kyf41px24fp")))

(define-public crate-raui-derive-0.38.1 (c (n "raui-derive") (v "0.38.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1l222v19dpmkk0r7vysb8g276kzc69fi9a40jqgfcvbna07jl7fc")))

(define-public crate-raui-derive-0.38.2 (c (n "raui-derive") (v "0.38.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0rjlpyqn8r5c8dc4h4p4vqaaxnn9s67s3ng9jsd47sgkyjs60y9c")))

(define-public crate-raui-derive-0.38.3 (c (n "raui-derive") (v "0.38.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mf7sfhqcw281wlv7smgqdvb3gc2hjnjx8jjfx574a82f218hxp4")))

(define-public crate-raui-derive-0.38.4 (c (n "raui-derive") (v "0.38.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1pp6z37vn42by17x4fffirlcp1ngg9daa9dis9b22x4hd0kw3r98")))

(define-public crate-raui-derive-0.40.0 (c (n "raui-derive") (v "0.40.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1p6dzr8a9jfs254wwx8iw3vhhh972c550d3041h60yjri2qz3idm")))

(define-public crate-raui-derive-0.41.0 (c (n "raui-derive") (v "0.41.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1vlyb66lngmzghqh5cp5rcsg9xgm16dkw8h89dvr3hj21zc9sdg8")))

(define-public crate-raui-derive-0.42.0 (c (n "raui-derive") (v "0.42.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1932bv8flynywrqrxn0rl2v2jxqmyimbz8f1vkh02i7z7cccpbbk")))

(define-public crate-raui-derive-0.43.0 (c (n "raui-derive") (v "0.43.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1p55wpp83m6f600h789jf6kmadxhgc8r0hkl055yxpdlwyk0fzvf")))

(define-public crate-raui-derive-0.44.0 (c (n "raui-derive") (v "0.44.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "070sf5sa04axzavsz7b87pvv59rq6lmnl6bi8hw8i3qjhr58qy70")))

(define-public crate-raui-derive-0.45.0 (c (n "raui-derive") (v "0.45.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18927f6j8vb9qk4k2afhjrwv7xm4m09xf9fikfpl2sks3y1asxac")))

(define-public crate-raui-derive-0.46.0 (c (n "raui-derive") (v "0.46.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0w8c2w8527mnpk8d7r2kfpsvl033qm8aspcskbnz346khb30vrlc")))

(define-public crate-raui-derive-0.46.1 (c (n "raui-derive") (v "0.46.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ppfd4rsgdn0pr68b45d421xm52mk8k4qw9fn5a52r4v83ihd9hq")))

(define-public crate-raui-derive-0.46.2 (c (n "raui-derive") (v "0.46.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gmgb16ywmm3iz3ngkaqyg2diczwarqkskxbza0krsyhhqa0hgzx")))

(define-public crate-raui-derive-0.46.3 (c (n "raui-derive") (v "0.46.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n52k2mmcjm22dlbdyrii8jq65saj1xpjz5n2zwxm1wdjfgkc7qp")))

(define-public crate-raui-derive-0.47.0 (c (n "raui-derive") (v "0.47.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1w4ykwmcsljxxj0k8h60bxl4rap17085vwld5069hjdflwyfbdl5")))

(define-public crate-raui-derive-0.47.1 (c (n "raui-derive") (v "0.47.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0hs4xf1lhm8blxsrljxbbbgz5vwyy1w33815zfdqlfmn42i8ml9k")))

(define-public crate-raui-derive-0.47.2 (c (n "raui-derive") (v "0.47.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "012z960dw743kb496jmvf9j0s5p3v2lm65a8jgpi9xfpnr96r38k")))

(define-public crate-raui-derive-0.48.0 (c (n "raui-derive") (v "0.48.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1gv749si59d5a1iyrd5a02ppxbpgrd6pw3s01gkwsc28iabpidi2")))

(define-public crate-raui-derive-0.49.0 (c (n "raui-derive") (v "0.49.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0w4n8m2yaizyjyv564qymz0cjj40z6jk8qhgc7c4nqf3mf7j5whn")))

(define-public crate-raui-derive-0.49.1 (c (n "raui-derive") (v "0.49.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "060blkhh5b0m5klnsvyzgqq3z0yw6hd8w9xvbz7m1cw509nv6jv2")))

(define-public crate-raui-derive-0.49.2 (c (n "raui-derive") (v "0.49.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mr18fbh1wxhz69kg9il597gl957gqbb4cc3fzbh801v68arkd9q")))

(define-public crate-raui-derive-0.50.0 (c (n "raui-derive") (v "0.50.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06ajddb2a900awrkix3sj24fccxzj4lladnx4z175gwccfszlsm1")))

(define-public crate-raui-derive-0.50.1 (c (n "raui-derive") (v "0.50.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "18s4b91ydghnwif8xsppavivn2ynlz6ij9cqgrlgn1h2mfwwvsgv")))

(define-public crate-raui-derive-0.50.2 (c (n "raui-derive") (v "0.50.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1hklnqcs90dj6chrwjbyilwxqr9crq00624s9icfjq20n3ja3b0x")))

(define-public crate-raui-derive-0.50.3 (c (n "raui-derive") (v "0.50.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0m13fs38il29yr49alabggpv9nd3jkc9firrddr1mkc3wi2shfjx")))

(define-public crate-raui-derive-0.51.0 (c (n "raui-derive") (v "0.51.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1d3knkh4h0scwwm4cxwjs99zl3w6a2p13kdkssggxrc5pi13vzkp")))

(define-public crate-raui-derive-0.52.0 (c (n "raui-derive") (v "0.52.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "07kqg2d8pj219lzkz3pbgamkrnq8nzsk7znmgl8xhz6m7a7dmy82")))

(define-public crate-raui-derive-0.52.1 (c (n "raui-derive") (v "0.52.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yr04dnjf11l03w98a2qc2xf72542vyjydpb5z9jp4kfvv5qq3i8")))

(define-public crate-raui-derive-0.53.0 (c (n "raui-derive") (v "0.53.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0600v6bln7n0zbpy830nwqvwlqn2p53nhyqc3dvzak9bif9lii7p")))

(define-public crate-raui-derive-0.54.0 (c (n "raui-derive") (v "0.54.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09mirb0pvj4qry0119250yvz15d9yna9jc06xa6ka01d1f05n0ra")))

(define-public crate-raui-derive-0.54.1 (c (n "raui-derive") (v "0.54.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13g12rqfcmzcjxmcvzsmjxsbki2cqc9pahvfczk48y5j17jblpv0")))

(define-public crate-raui-derive-0.55.0 (c (n "raui-derive") (v "0.55.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mrqh40nhsazls05ckh9l2rhhjc9a48l0phjsd1ddbl3zpxlv842")))

(define-public crate-raui-derive-0.55.1 (c (n "raui-derive") (v "0.55.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1wg70hbb272ml5nmnxny732knsv0yi95y2y6mr5qbllkd75hb0vv")))

(define-public crate-raui-derive-0.55.2 (c (n "raui-derive") (v "0.55.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0d91alwlwjr3pg4hrx3g4vwvh2kvipqj9q8i62h4mp7v4bq9w1sk")))

(define-public crate-raui-derive-0.55.3 (c (n "raui-derive") (v "0.55.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1440066nnir7blryc5n6d2n4lpp4ngy4hsvdgwfq0rm0va7j4yiq")))

(define-public crate-raui-derive-0.55.4 (c (n "raui-derive") (v "0.55.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1y2ia08kxpnrhmidyym8sg759ma0k6n0lbsrdyql5kzk7gby95i3")))

(define-public crate-raui-derive-0.55.5 (c (n "raui-derive") (v "0.55.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0cddg5i8knc9i4vglqp1bdacgagzypkcbqyjzbqbnyvbcam33a4s")))

(define-public crate-raui-derive-0.55.6 (c (n "raui-derive") (v "0.55.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05ssd9n46pr9pz4h4pf5mk59379vazyycmdlxhqjq6srqhi7la2w")))

(define-public crate-raui-derive-0.56.0 (c (n "raui-derive") (v "0.56.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "06s8phh324qydv0k6x2psz8zfw9110h72jhzcbbb5am9i2kld142")))

(define-public crate-raui-derive-0.56.1 (c (n "raui-derive") (v "0.56.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "037y2llidg4xgxwiqnd2nnz0h8gksg0364ciybhz9pynbbi026yb")))

(define-public crate-raui-derive-0.57.0 (c (n "raui-derive") (v "0.57.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0zy6h3h3h0i7ijrkbxmxjb3nlc0qzs1rr80cv5pkfcnk72ci8wn2")))

(define-public crate-raui-derive-0.58.0 (c (n "raui-derive") (v "0.58.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0f2i8y3vnmdv5vc7v6ic5r5mjhx6mva3ii0c81xx7v1dmj1m4x2w")))

(define-public crate-raui-derive-0.59.0 (c (n "raui-derive") (v "0.59.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "058c72hzixrrrfd0z1pzp4iqvb9kv9rrrrpik169mwyd8vgggcf5")))

(define-public crate-raui-derive-0.60.0 (c (n "raui-derive") (v "0.60.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04hkwv4jrwpa58mh9ax196qql0mzp4f8fnvg3yb9ds76bm9mrxhg")))

(define-public crate-raui-derive-0.61.0 (c (n "raui-derive") (v "0.61.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1s1a38bkgg9wc1qkg5ddimyxdhxminz17qsnbcxds4p0gfkdwdia")))

(define-public crate-raui-derive-0.61.1 (c (n "raui-derive") (v "0.61.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "08pd74ak3jjgpm3zc5qih6m6iii7iy7dghisigml8zvc689dmwcc")))

(define-public crate-raui-derive-0.61.2 (c (n "raui-derive") (v "0.61.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0glcha4z62bgxcp1kwygc1waixv3m241h4jgxrjk940g92x4mv2b")))

(define-public crate-raui-derive-0.62.0 (c (n "raui-derive") (v "0.62.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1xa5whz9zpb40vrg35pi0x027c8b0vcazb2c5k2zg4q7yjh3kkfj")))

(define-public crate-raui-derive-0.62.1 (c (n "raui-derive") (v "0.62.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0qxhrv2p5gx41h2aqn8qigd8jw73n3j61vvyk3c6pw0k56rwk97b")))

(define-public crate-raui-derive-0.63.0 (c (n "raui-derive") (v "0.63.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0fydbdyr9dn8p2j38ls6mxrpl9rgxj7095q4yb8ipr39wfj3xw3g")))

