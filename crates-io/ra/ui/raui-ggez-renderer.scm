(define-module (crates-io ra ui raui-ggez-renderer) #:use-module (crates-io))

(define-public crate-raui-ggez-renderer-0.6.0 (c (n "raui-ggez-renderer") (v "0.6.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p5qnmyac4qifq07vh846vc5294fqc7fr92nd0v5s1jbwwlk8bbc")))

(define-public crate-raui-ggez-renderer-0.7.0 (c (n "raui-ggez-renderer") (v "0.7.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0djcdr1j47lcg26s7ikvh7fd6if0pdxdzq8mhmygjf18508ah5kl")))

(define-public crate-raui-ggez-renderer-0.8.0 (c (n "raui-ggez-renderer") (v "0.8.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ivhl0q4aqrklkpy1npvwy47n5c875lflp6d9n67288b3ckvd3az")))

(define-public crate-raui-ggez-renderer-0.9.2 (c (n "raui-ggez-renderer") (v "0.9.2") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b9n1wd5q7a71ak4xwg8k70gv9479lbg1llv76lrxw74cm6j6mjz")))

(define-public crate-raui-ggez-renderer-0.10.0 (c (n "raui-ggez-renderer") (v "0.10.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x6j593scr688iyw9xfpyx488kgfz7rx7mykd1qbjlr27prrz77c")))

(define-public crate-raui-ggez-renderer-0.11.0 (c (n "raui-ggez-renderer") (v "0.11.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j1rmwnhz4nzr7dr161wna5ykwi489fmks83fravabh6zy8mfi46")))

(define-public crate-raui-ggez-renderer-0.12.1 (c (n "raui-ggez-renderer") (v "0.12.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1va5kd2wmw7zs55qxdwznhp58zcgzvyfxn3gwf8dp5s8anz5clgm")))

(define-public crate-raui-ggez-renderer-0.13.0 (c (n "raui-ggez-renderer") (v "0.13.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sqx8p3297x85w33d586kggpklfx7zh4iiv8i194399rhdg17k3s")))

(define-public crate-raui-ggez-renderer-0.14.0 (c (n "raui-ggez-renderer") (v "0.14.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0713pnmx6fpanc7ad5h3q4qsjm6jnha5pdd9crmcnshcawdai12q")))

(define-public crate-raui-ggez-renderer-0.15.0 (c (n "raui-ggez-renderer") (v "0.15.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zpprna8dbslxyladc94585xklbpd8sm42f1bgcc2k03nc2hh5wv")))

(define-public crate-raui-ggez-renderer-0.16.0 (c (n "raui-ggez-renderer") (v "0.16.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "13r60108rxvchc1bmdf4imb402n09pdwscilidm7bm983lx2bhq8")))

(define-public crate-raui-ggez-renderer-0.17.0 (c (n "raui-ggez-renderer") (v "0.17.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iqzdwbir3lwx5481jsy3axbkcnbqxy263hjw7x4nr2bpl3nkk41")))

(define-public crate-raui-ggez-renderer-0.18.0 (c (n "raui-ggez-renderer") (v "0.18.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g057prj88fb6nwiqssfis5mli82db8pwf2kziwx1ganyc6qhdsg")))

(define-public crate-raui-ggez-renderer-0.19.0 (c (n "raui-ggez-renderer") (v "0.19.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vvc35a9zpmvxkcnhfdmsmlyq386kqr6hkijad7p7ry4a9xd1z3h")))

(define-public crate-raui-ggez-renderer-0.20.0 (c (n "raui-ggez-renderer") (v "0.20.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g3q929nhrv1m66j10p83rmyqyj1wwr9fxy97wcxmq85gd3xnwkn")))

(define-public crate-raui-ggez-renderer-0.20.1 (c (n "raui-ggez-renderer") (v "0.20.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ma7vzmxrw22agzn6szdqk81lv7kw2dhlhz1xwibjan3a51641n5")))

(define-public crate-raui-ggez-renderer-0.20.2 (c (n "raui-ggez-renderer") (v "0.20.2") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqdiwwsd9r6xq1afrqfq4k9j240y0sccdnnhh2a7gmn5h73rfpa")))

(define-public crate-raui-ggez-renderer-0.21.0 (c (n "raui-ggez-renderer") (v "0.21.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b8nl6z93dv2simxqrm4dg5djgid822265by39y09qw3vkh4rlbh")))

(define-public crate-raui-ggez-renderer-0.21.1 (c (n "raui-ggez-renderer") (v "0.21.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z1imyxdlafhzr8cmx6spy1g22zybfdwm8hgrbwf7lwsxa6bg73x")))

(define-public crate-raui-ggez-renderer-0.21.2 (c (n "raui-ggez-renderer") (v "0.21.2") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02b5naa9dapg6v2ps5ygysm1q2dhcp7sylfly8s01x1qickrhald")))

(define-public crate-raui-ggez-renderer-0.21.3 (c (n "raui-ggez-renderer") (v "0.21.3") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "133s97xis1aca0azx4k99jkb4lgcbf29hy7da97a3nafwbbcxinv")))

(define-public crate-raui-ggez-renderer-0.22.0 (c (n "raui-ggez-renderer") (v "0.22.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bkj67b55khsxn0f7ys557l7sr31idgif8k7xrsfnvwys25js1p9")))

(define-public crate-raui-ggez-renderer-0.23.0 (c (n "raui-ggez-renderer") (v "0.23.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hmipjmkrgqn7vhdpr9rym3cya6mg2rmpzy0b15bgd6ykry57kml")))

(define-public crate-raui-ggez-renderer-0.24.0 (c (n "raui-ggez-renderer") (v "0.24.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aslqrkxj5b9immfds4gdk1apxa1l0jm8hgiys27p6ggh2hnhb00")))

(define-public crate-raui-ggez-renderer-0.25.0 (c (n "raui-ggez-renderer") (v "0.25.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k8izzh7wq8ls06gs8pffa6hbnclp6g5b465qv2kmz61g9jgfrqz")))

(define-public crate-raui-ggez-renderer-0.26.0 (c (n "raui-ggez-renderer") (v "0.26.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aj67qmapjl9naxbv84xg6xsimk5m5pbl7s89yir3lxfq0q39gzr")))

(define-public crate-raui-ggez-renderer-0.27.0 (c (n "raui-ggez-renderer") (v "0.27.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zn3xbvj86d1nmbzshb8g6by3fxpdy8ixr6wkh3i21c8z5lwh2gw")))

(define-public crate-raui-ggez-renderer-0.27.1 (c (n "raui-ggez-renderer") (v "0.27.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kqj7sgngsnns882zdcb2ixz9007jlcf59mqz5hq0ri0y84dl3yi")))

(define-public crate-raui-ggez-renderer-0.28.0 (c (n "raui-ggez-renderer") (v "0.28.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dp1lrbhsxkcb8h1nvc9mz4mh2bkh2skkrzm72jgxr6y3skcbr79")))

(define-public crate-raui-ggez-renderer-0.28.1 (c (n "raui-ggez-renderer") (v "0.28.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jadiilxy1pbm98c9v169rkn7v9phr0phf9vkmlkjpd9an6dl7ph")))

(define-public crate-raui-ggez-renderer-0.29.0 (c (n "raui-ggez-renderer") (v "0.29.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fyywm84s4p9snxd7hj42djmqkfkf654sifpw1sa6jvn3ww5xci6")))

(define-public crate-raui-ggez-renderer-0.30.0 (c (n "raui-ggez-renderer") (v "0.30.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yhkdy0s1ajgx8qi36h6lcm7fs3pga9ywjigik5q891kzx3w22wm")))

(define-public crate-raui-ggez-renderer-0.31.0 (c (n "raui-ggez-renderer") (v "0.31.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07m7l3gg868cqb1gf9j7sb9k0icmx8xpiflbqv94s8jmxbfc6331")))

(define-public crate-raui-ggez-renderer-0.32.0 (c (n "raui-ggez-renderer") (v "0.32.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kla993pamnqyiyhc4abj2zwjjiw7h8ssjfmx5n69qjbdqg4l02g")))

(define-public crate-raui-ggez-renderer-0.33.0 (c (n "raui-ggez-renderer") (v "0.33.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p304ynhr4yqsk0d44djjjrdjkj0n495i7yyrnvv70n89nmbv00f")))

(define-public crate-raui-ggez-renderer-0.34.0 (c (n "raui-ggez-renderer") (v "0.34.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18d9aqm3ymcl5wh1arkbsqblidli6z583y3p9vj8nrsqc8xiab6y")))

(define-public crate-raui-ggez-renderer-0.35.0 (c (n "raui-ggez-renderer") (v "0.35.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lqrgpmknng3air2lnw043rfr2jwfgcpha9xk55rq891hrfi36yf")))

(define-public crate-raui-ggez-renderer-0.36.0 (c (n "raui-ggez-renderer") (v "0.36.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14c9ixvlsi33r991n4bgs5hdm3x8vvx83n97ac6pnkfhlzg6fx6s")))

(define-public crate-raui-ggez-renderer-0.37.0 (c (n "raui-ggez-renderer") (v "0.37.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rz0x01bz79hpzhg8rw9cy4zyda4i82nmxij4l46h6h75j0hnya3")))

(define-public crate-raui-ggez-renderer-0.37.1 (c (n "raui-ggez-renderer") (v "0.37.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a3xvgq11rjr7cpv0g09gwjgsxpkzzi3il2g7fpcx32v7y2dch9s")))

(define-public crate-raui-ggez-renderer-0.38.0 (c (n "raui-ggez-renderer") (v "0.38.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01m59gxcjs5ih2b681li15vsrggzfga63fh6l6syydfzkpa5711n")))

(define-public crate-raui-ggez-renderer-0.38.1 (c (n "raui-ggez-renderer") (v "0.38.1") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "160dgghzl2r3mbrcbpy48impinfr7cirnrpidnzc9gl8ab63qydg")))

(define-public crate-raui-ggez-renderer-0.38.2 (c (n "raui-ggez-renderer") (v "0.38.2") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zxxrj6aa98dbsvy24snyf9lyp933wb00pnkrl6h9dz9yfzzpzka")))

(define-public crate-raui-ggez-renderer-0.38.3 (c (n "raui-ggez-renderer") (v "0.38.3") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q6xh04mb0bzf9s9g1a576h0zllcq9mhcvhwhvx22r1rgng0jxw0")))

(define-public crate-raui-ggez-renderer-0.38.4 (c (n "raui-ggez-renderer") (v "0.38.4") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z53h5bk5ad3sp625xn8f5aw6kdbcbmfwyxflv7vz2zwsggfnhiw")))

(define-public crate-raui-ggez-renderer-0.40.0 (c (n "raui-ggez-renderer") (v "0.40.0") (d (list (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bx3ivnh2l4s95falg6wap2xns9h7q71y2biiyvkxn2fc1ff20g8")))

