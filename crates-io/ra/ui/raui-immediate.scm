(define-module (crates-io ra ui raui-immediate) #:use-module (crates-io))

(define-public crate-raui-immediate-0.42.0 (c (n "raui-immediate") (v "0.42.0") (d (list (d (n "raui-core") (r "^0.42") (d #t) (k 0)))) (h "1xfyc590wbd7gngi1jrshihaj5wf9kplir2sbnw9zc9agywb07vk")))

(define-public crate-raui-immediate-0.43.0 (c (n "raui-immediate") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0902j69g8w9b6wd5xga2zqcii3nm9v1dw53i3jx6vc8sab092wyd")))

(define-public crate-raui-immediate-0.44.0 (c (n "raui-immediate") (v "0.44.0") (d (list (d (n "raui-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hx7f80yl8vi95wga9bb6xnifr5assprbwrbc3gvv9mlxgasmvl8")))

(define-public crate-raui-immediate-0.45.0 (c (n "raui-immediate") (v "0.45.0") (d (list (d (n "raui-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05m4rivzlj7j8f2h1ni6k8cv9z1grn053z2g26x6n1y6cvs3wkmq")))

(define-public crate-raui-immediate-0.46.0 (c (n "raui-immediate") (v "0.46.0") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wy4bym4wkgznva0plq60ss35b12ldkwa6pkzldhl32sxcc1c47c")))

(define-public crate-raui-immediate-0.46.1 (c (n "raui-immediate") (v "0.46.1") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qwk3x42csj86w327zap29smx99gcrmyskakn251blf748h5fp3x")))

(define-public crate-raui-immediate-0.46.2 (c (n "raui-immediate") (v "0.46.2") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pacv5xz2impkniwy9105fsaqr3iilnffxzyfbvyyck1119kv382")))

(define-public crate-raui-immediate-0.46.3 (c (n "raui-immediate") (v "0.46.3") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11bb00xm0zm4wgrjjzm9vlac7vwc2lvvz5mqlz0s83srq3nkmz0b")))

(define-public crate-raui-immediate-0.47.0 (c (n "raui-immediate") (v "0.47.0") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jb1cxcpk34px2ccl5a89hv12j73d7zf8pj5ph4zgvldvzqxxjkc")))

(define-public crate-raui-immediate-0.47.1 (c (n "raui-immediate") (v "0.47.1") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01lb1mn4zivbnr9zs4pjjngbwm6zzqkn990zq0s16gndq2vmr40w")))

(define-public crate-raui-immediate-0.47.2 (c (n "raui-immediate") (v "0.47.2") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fs2hgand9gwy97657p0lr2f652dcrnzw51rc0x5z2m5szka2f0l")))

(define-public crate-raui-immediate-0.48.0 (c (n "raui-immediate") (v "0.48.0") (d (list (d (n "raui-core") (r "^0.48") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1glp8r8zl8a50fkw89vv8y2adywnr3wm58s5p1hr7c8jlj1fzcnr")))

(define-public crate-raui-immediate-0.49.0 (c (n "raui-immediate") (v "0.49.0") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05hrq8ls1hanrg9kqhgryjy2l1hv7m5681k32jhch9hfa7z578wm")))

(define-public crate-raui-immediate-0.49.1 (c (n "raui-immediate") (v "0.49.1") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f38b0qhhai1sqiqrnll6dpgv63ciz4dwbzffjs05g79scllyg68")))

(define-public crate-raui-immediate-0.49.2 (c (n "raui-immediate") (v "0.49.2") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00z4620qmffx8jprmzkag85nxsl4955gh5jsxrnrmijap5dwml5h")))

(define-public crate-raui-immediate-0.50.0 (c (n "raui-immediate") (v "0.50.0") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k7qf9aqmvac283vr646flqhpfcs0mjk1vk51qk9gwbq2yahkvng")))

(define-public crate-raui-immediate-0.50.1 (c (n "raui-immediate") (v "0.50.1") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a5dvzfyxja2gdmjz6psg22n29h09s4762h7j4pzjschhc8ifs4n")))

(define-public crate-raui-immediate-0.50.2 (c (n "raui-immediate") (v "0.50.2") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15w6w9vjqgyp6y61hsh8jsz3sss71sij91jvbiqs15q17cn9i1c6")))

(define-public crate-raui-immediate-0.50.3 (c (n "raui-immediate") (v "0.50.3") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c5r0qnj9gc4r3qj7r1a1cfilj2j2k721wjv9nvqwbh5c8hmxc39")))

(define-public crate-raui-immediate-0.51.0 (c (n "raui-immediate") (v "0.51.0") (d (list (d (n "raui-core") (r "^0.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jmjj4h63yk7zk458943avc1ql1cc3s9kjrhqj96j342nfwyzlw4")))

(define-public crate-raui-immediate-0.52.0 (c (n "raui-immediate") (v "0.52.0") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hb77r26ygy0ijvk71gswi1rcq9wbn8sx7gla3bpjdv3q3v1800h")))

(define-public crate-raui-immediate-0.52.1 (c (n "raui-immediate") (v "0.52.1") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06hx30jc12dh7bdsxfpwxp56xcrkqa6ldb3fmp6jwrg5ycfag3z0")))

(define-public crate-raui-immediate-0.53.0 (c (n "raui-immediate") (v "0.53.0") (d (list (d (n "raui-core") (r "^0.53") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1szimzkifl1n0iwd4p4ckadhz4x7f3za5a8nhgni3bdzhv0473ir")))

(define-public crate-raui-immediate-0.54.0 (c (n "raui-immediate") (v "0.54.0") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "054s32hzndbanx92bzc46a54xc9p7pnkdaqgf05p3644l50lrpal")))

(define-public crate-raui-immediate-0.54.1 (c (n "raui-immediate") (v "0.54.1") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09dcvffgyg6d9ika2hfvd1x0mvmdlr3iz1i3s6jv0xzbbmazck5s")))

(define-public crate-raui-immediate-0.55.0 (c (n "raui-immediate") (v "0.55.0") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rl9qm42q1mgzfiy2qsck37kk0kn74x3cprfqy8piwa5wx34kwzi")))

(define-public crate-raui-immediate-0.55.1 (c (n "raui-immediate") (v "0.55.1") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "024jhnp8qqgsgczlpnxndcylj8lmzzc7dmjs75n968a2mj4a92c2")))

(define-public crate-raui-immediate-0.55.2 (c (n "raui-immediate") (v "0.55.2") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01vnwn665giqx5lg65vf97asj2g6z17rf528vcr8lmlyfyi1hdwj")))

(define-public crate-raui-immediate-0.55.3 (c (n "raui-immediate") (v "0.55.3") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cpb316fjcqfj5627kqlrwp4zcbma8mj5zc616xdnlyj7yhv3px1")))

(define-public crate-raui-immediate-0.55.4 (c (n "raui-immediate") (v "0.55.4") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18rarpw1yvrnmszv0rp8wazqv704l6z64qrz35iv14g4cl2hpp4h")))

(define-public crate-raui-immediate-0.55.5 (c (n "raui-immediate") (v "0.55.5") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0npifzw4rcgdajnk8x31x2nk9r6x9k8dyd72vfxm11a0acszr236")))

(define-public crate-raui-immediate-0.55.6 (c (n "raui-immediate") (v "0.55.6") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "148vd7pm79j8272p2l676sr6wfqmw4hk6rs0fpdnwk2fr7zcr2ag")))

(define-public crate-raui-immediate-0.56.0 (c (n "raui-immediate") (v "0.56.0") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wc66x5g8wzmvlc1r34hh3ig8haig1b9hm4bls650aybfn6prqfj")))

(define-public crate-raui-immediate-0.56.1 (c (n "raui-immediate") (v "0.56.1") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nipaa96ycsg02zwwqwpd1mf0lqr4lx0f4q2sn2fqnvsqniq2i8f")))

(define-public crate-raui-immediate-0.57.0 (c (n "raui-immediate") (v "0.57.0") (d (list (d (n "raui-core") (r "^0.57") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h023iahvf492nang4ldnzi192l2hvww9zil8bms6s93jky30zr5")))

(define-public crate-raui-immediate-0.58.0 (c (n "raui-immediate") (v "0.58.0") (d (list (d (n "raui-core") (r "^0.58") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vx65j7bb8gav41rbl5z5k28zrc4bw9fmhzgz8yb3i7bjcymz4sc")))

(define-public crate-raui-immediate-0.59.0 (c (n "raui-immediate") (v "0.59.0") (d (list (d (n "raui-core") (r "^0.59") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cai5hs9b17nnm13asin8yv4mibnlx3x03rj9zqimgxkls1cnzvd")))

(define-public crate-raui-immediate-0.60.0 (c (n "raui-immediate") (v "0.60.0") (d (list (d (n "raui-core") (r "^0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fkdrr8ijhn7xy20xj6q6mgiq3rydnpbkac9fv2g41ydriyx33q1")))

(define-public crate-raui-immediate-0.61.0 (c (n "raui-immediate") (v "0.61.0") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b7rkd2skjq51dxp0ws9rhmz11fdnza1v839l8jzff1hz1d7sanw")))

(define-public crate-raui-immediate-0.61.1 (c (n "raui-immediate") (v "0.61.1") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jg9gs85knnmn50ag4hlg2l5526arz16vqixab2az16phjm2zj9z")))

(define-public crate-raui-immediate-0.61.2 (c (n "raui-immediate") (v "0.61.2") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ffkl67zmardk0g41q330q8adj55rdvn4jixmc985yqfbyl8ps66")))

(define-public crate-raui-immediate-0.62.0 (c (n "raui-immediate") (v "0.62.0") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h6w6lxfhvi9zz8j0jpq5gsdb5bh77yfb0yrhxv9q7hyr9qqvjwm")))

(define-public crate-raui-immediate-0.62.1 (c (n "raui-immediate") (v "0.62.1") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dmfjdgdl1j680hi9zmwyb3ykym32yvvqfq1wfhisaqwz3225hbd")))

(define-public crate-raui-immediate-0.63.0 (c (n "raui-immediate") (v "0.63.0") (d (list (d (n "raui-core") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p47k8k7gyik1l0lr7958adkgrafl3969vzi2l8fyw9cdys4xr0v")))

