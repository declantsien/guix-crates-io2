(define-module (crates-io ra ui raui-quick-start) #:use-module (crates-io))

(define-public crate-raui-quick-start-0.35.0 (c (n "raui-quick-start") (v "0.35.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.35") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.35") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "0ccik94szn0f4fvhzb5nf0bim8v40vyycavh04hrgszwhidbzzm6")))

(define-public crate-raui-quick-start-0.36.0 (c (n "raui-quick-start") (v "0.36.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.36") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.36") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "13yzw1icjmvwa0b8cf37gybj0v90hl0d1szprbihxs0l162sm977")))

(define-public crate-raui-quick-start-0.37.0 (c (n "raui-quick-start") (v "0.37.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.37") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.37") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "15bsbcnsmznb480gb9blc7248w45hrdzwni9yr53p0xq3271r275")))

(define-public crate-raui-quick-start-0.37.1 (c (n "raui-quick-start") (v "0.37.1") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.37") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.37") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "19dsb6axmc89s9mfnjhccnbh0andj8c19yavryli5fs3r3jf36db")))

(define-public crate-raui-quick-start-0.38.0 (c (n "raui-quick-start") (v "0.38.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.38") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.38") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "1zi3w58lj609da2w9mv799gnjd784lphisjcc3qqp5sxvblnnyqs")))

(define-public crate-raui-quick-start-0.38.1 (c (n "raui-quick-start") (v "0.38.1") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.38") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.38") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "13bzpnx3s7w8fwrxfrx6yvnn3mjrshm64ma3whydag2vcd6sh7wa")))

(define-public crate-raui-quick-start-0.38.2 (c (n "raui-quick-start") (v "0.38.2") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.38") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.38") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "07831713fj8bq8m60fizymy5f4qlq54z0l2iwy7r196g1r4289y7")))

(define-public crate-raui-quick-start-0.38.3 (c (n "raui-quick-start") (v "0.38.3") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.38") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.38") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "06vw8677c6pdk8x5ac0yi2bz0k644qf32fxhlv47w50vr5cma412")))

(define-public crate-raui-quick-start-0.38.4 (c (n "raui-quick-start") (v "0.38.4") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.38") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.38") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "1xwqsljr1y73ik9k7azzirxiq7lsr170insr3f27pkndy05f8rjk")))

(define-public crate-raui-quick-start-0.40.0 (c (n "raui-quick-start") (v "0.40.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.40") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.40") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "13hfdzn0g5xdykzcpj20xdakxpy7w2gciz2jmws2z78wkxwfia7y")))

(define-public crate-raui-quick-start-0.41.0 (c (n "raui-quick-start") (v "0.41.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.41") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.41") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "07439jcrxy9ibbrh4kllvfy72bl2dyb4r4h8w5x2d996j7wifbsw")))

(define-public crate-raui-quick-start-0.42.0 (c (n "raui-quick-start") (v "0.42.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.42") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.42") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "0ry6rn9kfs7xdyh77rk5kqkny56hh124i3i4xlkva91sh1mz648c")))

(define-public crate-raui-quick-start-0.43.0 (c (n "raui-quick-start") (v "0.43.0") (d (list (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "raui") (r "^0.43") (d #t) (k 0)) (d (n "raui-tetra-renderer") (r "^0.43") (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("sdl2_bundled"))) (d #t) (k 0)))) (h "1v74brqc8zmck5vz6z68lws6jqqfl2qc76jqhq736grg79y9gkzx")))

