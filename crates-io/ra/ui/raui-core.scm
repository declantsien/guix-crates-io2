(define-module (crates-io ra ui raui-core) #:use-module (crates-io))

(define-public crate-raui-core-0.5.0 (c (n "raui-core") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n9d1190k9sjx2p5g53daf2cfsavr34qrni48rky8ay6q40lnark")))

(define-public crate-raui-core-0.6.0 (c (n "raui-core") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k24bhpzmh247ip2mqmwdsj2wcj3v9rv36qq3y6d0a66b4m1igh0")))

(define-public crate-raui-core-0.7.0 (c (n "raui-core") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yafnxdr1rfiffcw62jji7lfb9bzvw7lr71vzn6zzv2rmxd7h8ii")))

(define-public crate-raui-core-0.8.0 (c (n "raui-core") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jnl1kw5cbfzgrmsxpra03k0n9v3kmq36ggibifnx9y116hdfcdv")))

(define-public crate-raui-core-0.9.0 (c (n "raui-core") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0l3hammxn0d64djnqg2fb4y9dk8665kp9xds57imp2cavsaj8w2m")))

(define-public crate-raui-core-0.9.1 (c (n "raui-core") (v "0.9.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "08al8qc1k6bpml0l2d3bx0fszakbf189biiy692r7x685bprs7s9")))

(define-public crate-raui-core-0.9.2 (c (n "raui-core") (v "0.9.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0h9519hsns3xaj9fyb2qdwbj13p71ln2gk0lh5h16azn1l6lq2lw")))

(define-public crate-raui-core-0.10.0 (c (n "raui-core") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "03k5anajxdqsjar3vinfg0n2n0dvi8fshydixxzzdvihzf9i5psc")))

(define-public crate-raui-core-0.11.0 (c (n "raui-core") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "18pqwsjb0n9k2x62xa44cn1jm4p1ish8xi8g80f7hxs7b920a0k5")))

(define-public crate-raui-core-0.12.0 (c (n "raui-core") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1d3abqdyhn9q2wgdig8lhpy17k5vqh59ah78rr3dzbqlb4kp58ci")))

(define-public crate-raui-core-0.12.1 (c (n "raui-core") (v "0.12.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "18mbn33a5ss784nshaprkdjn305zdbkaayik1xh2a1h2pl733zbq")))

(define-public crate-raui-core-0.13.0 (c (n "raui-core") (v "0.13.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "15j675ngdxrrqscrs0a303ph1jh91frj4iqshw0mzvsrdmhbm40a")))

(define-public crate-raui-core-0.14.0 (c (n "raui-core") (v "0.14.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "18f3kkn6rl1cmih4c21ny87x3xpdlqqnqz5w7jqsay6znii6nwml")))

(define-public crate-raui-core-0.15.0 (c (n "raui-core") (v "0.15.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0v2g4n7srzw540gj6qkl7h2c777mzmlnpb089lz1v24pz8bkv3c5") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.16.0 (c (n "raui-core") (v "0.16.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "06nblxp3yiypbg37nyg6kqf3z1qv6118cs31w2naq4yallw57k0f") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.17.0 (c (n "raui-core") (v "0.17.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "094fqp5x3mwyf7g77kn5q2czz288dnyz76bamnv0ds5jh7na6v68") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.18.0 (c (n "raui-core") (v "0.18.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1wmh157q3336a456wp6ffqg13wx09bl1iwkp759csaq8jah6s4d7") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.19.0 (c (n "raui-core") (v "0.19.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0cj7q3xc16nhy9wkzgsbapk2v50klhnbjzq5n0avwwcvg54mylh8") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.20.0 (c (n "raui-core") (v "0.20.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0mmia05vc4133m80pz4n0wiqiba5vl54hxcgiqxy4gjn2yy2d5m5") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.20.1 (c (n "raui-core") (v "0.20.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0jkwlcdw7jsjrfwjvx2gq71rn5aycy9dbwc6yxs1akk805z716fb") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.20.2 (c (n "raui-core") (v "0.20.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0ksv0ap0qj7azsdx3g72dp3zrpi2xl5301mmfx0v4pf4q569q2yy") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.21.0 (c (n "raui-core") (v "0.21.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1r7fszd5949krmc0932pipdbx3lxmbyc1vd1nf4ifslmii63w97d") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.21.1 (c (n "raui-core") (v "0.21.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0d66nf1y9sjbirf7j3zh50wrqxwkvg0lypylip7zsy28hlw18v7f") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.21.2 (c (n "raui-core") (v "0.21.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "192f6c960nb4q2mc05p7a6fjp08y53rsys3wnvv0gyy4bcqvdyv6") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.21.3 (c (n "raui-core") (v "0.21.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03nhqm1fz12xmzcvys3ylvmmcfa5gcwfj9slddv7mx37n4jwjjid") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.22.0 (c (n "raui-core") (v "0.22.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "04vd89n3bshadf6giidfy0i3zgz22q3nvba77pn64m1rsqax7zzr") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.23.0 (c (n "raui-core") (v "0.23.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1gsfyjbsdx13l078rs902h2i5g0qxy39z789mz21mn8ilhi4wgv3") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.24.0 (c (n "raui-core") (v "0.24.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "16vq4fkkxqbnb8zp2588q0i2012fdrk4ly02jl8v7g6m0r1pfpp9") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.25.0 (c (n "raui-core") (v "0.25.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "119iam6p7lmdr6xx1w331hnn5f5aq3wqg89q7va2v5nx93qwp73v") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.26.0 (c (n "raui-core") (v "0.26.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0sjq5f53k7c3nlhx3q1bg6xb70jms6d0xk05vcni24qyw9qrysl8") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.27.0 (c (n "raui-core") (v "0.27.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1qga2y7ywbxv282grh4hxgdd9928mhq0cpb3jq11b016fyc1c1n9") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.27.1 (c (n "raui-core") (v "0.27.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "01c5r8iqcmh39zyjwrh6ldb4w6f86wy9jz0r4x9jy83xx8pc12w8") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.28.0 (c (n "raui-core") (v "0.28.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1jrn54gx6idim568b1n6fms7cyfzjyplr71j1qck4c15jsrysknw") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.28.1 (c (n "raui-core") (v "0.28.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "01hnkv8z6n9qyya15i0ax30m3pqxhbzmdqa4i1bhczj654vgk4rn") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.29.0 (c (n "raui-core") (v "0.29.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1mrf8xf89awylcmk9mnq9ck2fc9k8qpx281wxviff3qa1hmm2li1") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.30.0 (c (n "raui-core") (v "0.30.0") (d (list (d (n "raui-derive") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "184ma19gcwmfzf82b775kdg4zszh3b2l0ndafajlbwnspqlrcsdx") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.31.0 (c (n "raui-core") (v "0.31.0") (d (list (d (n "raui-derive") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ji9x68a8q9m9j3a8i0klv8i6k06fhl51h08lwcmphy253s7ar2i") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.32.0 (c (n "raui-core") (v "0.32.0") (d (list (d (n "raui-derive") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "162sh3dzd0lzfzrmd5a6smqx0vj4bayswaskm47fj3yms96b4cvg") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.33.0 (c (n "raui-core") (v "0.33.0") (d (list (d (n "raui-derive") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1mhw0s7srcypicra48m8zkgq7zqzfhgda564mliyl6bhzq1lf1km") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.34.0 (c (n "raui-core") (v "0.34.0") (d (list (d (n "raui-derive") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1c4822b538ymdd7bqj6zxd49i695zw6cq7d2pf3ya4hq7023hbfv") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.35.0 (c (n "raui-core") (v "0.35.0") (d (list (d (n "raui-derive") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1midwb5cagwmvpxnrh5q111a3c1f6akcpbx1l8m45rvii1kz5k5a") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.36.0 (c (n "raui-core") (v "0.36.0") (d (list (d (n "raui-derive") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1s0irkigd34ph3pfhkww4gw0pk8lyhx973mcripsams697kg358x") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.37.0 (c (n "raui-core") (v "0.37.0") (d (list (d (n "raui-derive") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1848gyybwamsd1r5qp94xrzgqmaijzbxi712fpkvdf12877wvniw") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.37.1 (c (n "raui-core") (v "0.37.1") (d (list (d (n "raui-derive") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "15hiad0qi509dn2wmxrdpz89l4ynj0dy535f0nbv116j6dn8r1nw") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.38.0 (c (n "raui-core") (v "0.38.0") (d (list (d (n "raui-derive") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10005dv4y2a4xj8mg71q758mf38byywakcqpj5wi3d2sb2szypdz") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.38.1 (c (n "raui-core") (v "0.38.1") (d (list (d (n "raui-derive") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1qmn80h1b50nf69l7ym0fzzgzikm929h4qwa3nqvlb10xgm4ia27") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.38.2 (c (n "raui-core") (v "0.38.2") (d (list (d (n "raui-derive") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ij7yh3bz89cs58bknrhrbzypnjkb637vfxrxz5v6cflk8qx1xwj") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.38.3 (c (n "raui-core") (v "0.38.3") (d (list (d (n "raui-derive") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "01xnibngk35bc67pqw2652w3gpmpsw8d2am7j5g5h281gk5v2hdc") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.38.4 (c (n "raui-core") (v "0.38.4") (d (list (d (n "raui-derive") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0hkfzksqp4x8r99gzzqi1smyvk3vvwr4cgjwc5j6rxks0830s606") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.40.0 (c (n "raui-core") (v "0.40.0") (d (list (d (n "intuicio-data") (r "^0.21") (d #t) (k 0)) (d (n "raui-derive") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0vz55swv7if4af2qxsl4z3dkazfaizv1knn4csjrnc0s7yadzksn") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.41.0 (c (n "raui-core") (v "0.41.0") (d (list (d (n "intuicio-data") (r "^0.21") (d #t) (k 0)) (d (n "raui-derive") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05hmv9qj67wivggns50agngdsfwa068dpbsb8aasf4bkck09wq0z") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.42.0 (c (n "raui-core") (v "0.42.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qsav1dppwk2n1q33q0zpgfcs6sy2ygb3np1g2b56p5bchb19232") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.43.0 (c (n "raui-core") (v "0.43.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j7gs7s9j95q3qb77adxci99w96bg8ms7yrfi8zmynxlslgxnbp0") (f (quote (("scalar64") ("integer64"))))))

(define-public crate-raui-core-0.44.0 (c (n "raui-core") (v "0.44.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1bp59cjlihcd8vzch0z0llrh87wd7k6np0ar15j300gr3hw9wdi1")))

(define-public crate-raui-core-0.45.0 (c (n "raui-core") (v "0.45.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dyp1b2r1gmx4y987cf49fmz1wvdksldd4d8dai5a30xj5y8mzkf")))

(define-public crate-raui-core-0.46.0 (c (n "raui-core") (v "0.46.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18nrjg2c048sf25vp60jqwffnisarbkaqn6cx5vnf4fli6wps4j9")))

(define-public crate-raui-core-0.46.1 (c (n "raui-core") (v "0.46.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z0r0rk1cwk3xg9y8vfb9wswzar80fjb4c1rmgzzjxivcqzh4yrp")))

(define-public crate-raui-core-0.46.2 (c (n "raui-core") (v "0.46.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16lyskmxzpcq16g12cl3gj86fgkj740w9wvfckk8809gmr7j6l01")))

(define-public crate-raui-core-0.46.3 (c (n "raui-core") (v "0.46.3") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lpd13lc53hk81j70as7cbwqabfrrvba25qqpcbz5snyfk0f14d0")))

(define-public crate-raui-core-0.47.0 (c (n "raui-core") (v "0.47.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bm8lx1k9qbb5i30c5cf9bwmv6v7xyzxm870zb78f7xx1jjm54m1")))

(define-public crate-raui-core-0.47.1 (c (n "raui-core") (v "0.47.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i4ymx173riaxbc5k5l5x27dwlvjyx3b2phv2lplx0xrrm2j5pss")))

(define-public crate-raui-core-0.47.2 (c (n "raui-core") (v "0.47.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01np4law9m9qx07r5i5vn2l74sw8sc0309h3hb5j8n3s9m3h22na")))

(define-public crate-raui-core-0.48.0 (c (n "raui-core") (v "0.48.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.48") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aaqyiwn8chhk0inrvahk8w5mzh10iaclwn9h58k85p1ybznyyp1")))

(define-public crate-raui-core-0.49.0 (c (n "raui-core") (v "0.49.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01lv47fdjqf4cpl3bhc461yakdi03zkz2m9373dkzn3jxpjq1bkf")))

(define-public crate-raui-core-0.49.1 (c (n "raui-core") (v "0.49.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ghhc6pj3zy5nwcvaxyqg0f1a977r5yc0c0hl54pxcjl47nl7830")))

(define-public crate-raui-core-0.49.2 (c (n "raui-core") (v "0.49.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0scjxbsbqsglsanw4si055mp566zsq7sj7xmjhi03y3dk5ba25cj")))

(define-public crate-raui-core-0.50.0 (c (n "raui-core") (v "0.50.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03y5hj53d6hnp4hx501qi0m7pq4fidim4z8cwsya4ysvh63fvqj7")))

(define-public crate-raui-core-0.50.1 (c (n "raui-core") (v "0.50.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j1f7fhgscqlz7pklshhln1p5mj2nwg2zr2gqzyw7g6xzgnldmj3")))

(define-public crate-raui-core-0.50.2 (c (n "raui-core") (v "0.50.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18j1klzd1bpk885rh2dljq6nhqbqp2plrkw7z1gx11wgg82gylzy")))

(define-public crate-raui-core-0.50.3 (c (n "raui-core") (v "0.50.3") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11yniydc84pc316w5h9s8dyppxllz3y10s1iw0afxkccr2mggyra")))

(define-public crate-raui-core-0.51.0 (c (n "raui-core") (v "0.51.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19n76mi1f8n7ccqcyid46fhzkxwgg5nq85i9abm8x96j85sr09kw")))

(define-public crate-raui-core-0.52.0 (c (n "raui-core") (v "0.52.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0brg4b9kv1zahqvwsc1dja3yixn6y17qynpa6xbwvzabcqp12hi9")))

(define-public crate-raui-core-0.52.1 (c (n "raui-core") (v "0.52.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0frn9nn7wkn0sqsxvcy5lrjqwvmng4lhh3jy9c1zgsa4iiscq9pk")))

(define-public crate-raui-core-0.53.0 (c (n "raui-core") (v "0.53.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.53") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ibj3am56vvclhjbgfv0d20wcvim96dk5xffpdlh2ablmp42yb9k")))

(define-public crate-raui-core-0.54.0 (c (n "raui-core") (v "0.54.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09b7f1hnx39jcbmkm52xq673cjx7gg1zhdqx79wj3fk8j4r7p6hx")))

(define-public crate-raui-core-0.54.1 (c (n "raui-core") (v "0.54.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ibypl80ih8xrq31ag5ygh07y1pvjrdhzrasqr4bbqm5jzmb04d")))

(define-public crate-raui-core-0.55.0 (c (n "raui-core") (v "0.55.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05dhg2gazxgslnlbqgfm2if1p6v7xci3yhdinqk13wisnjm5v2w5")))

(define-public crate-raui-core-0.55.1 (c (n "raui-core") (v "0.55.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p1m6365z6spi9rvcf3ryrid2hnf9q4l3p0v87zny7k46fyv3xd9")))

(define-public crate-raui-core-0.55.2 (c (n "raui-core") (v "0.55.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nvcp7lfxfj8pb6cv2zmijqwpgsid3g31wfsqvyzqscpnbrnsdry")))

(define-public crate-raui-core-0.55.3 (c (n "raui-core") (v "0.55.3") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jl1f20k21zhb1r9i0jzlyf0vyvn5llyxczd5iqsbj0scs6p2rm2")))

(define-public crate-raui-core-0.55.4 (c (n "raui-core") (v "0.55.4") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ncz306xb91sj9ckzzfzbgrx0chgi7ldfi88h6z72qp1gaisjzgv")))

(define-public crate-raui-core-0.55.5 (c (n "raui-core") (v "0.55.5") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15dhw2rzkfn1vfd110axsnk1bz1kxjj9bmgxf0gf4lawra1hnbq6")))

(define-public crate-raui-core-0.55.6 (c (n "raui-core") (v "0.55.6") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvnahj64gydz0w3hnwxj9cfv28glcgdrvs6kq8pbir70z83ynrf")))

(define-public crate-raui-core-0.56.0 (c (n "raui-core") (v "0.56.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gqszs41akl452x12gf76gb3ngjaxg45paqzln2zpzh0npal1l5d")))

(define-public crate-raui-core-0.56.1 (c (n "raui-core") (v "0.56.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qqlkyf3sgdai347abmmhihxkgj4f533ps2aq2chv3qw79mbwf88")))

(define-public crate-raui-core-0.57.0 (c (n "raui-core") (v "0.57.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.57") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iqri3i3jwv8s44db0by19kw4ivjg19n8phzydx461zd5wb2b2m2")))

(define-public crate-raui-core-0.58.0 (c (n "raui-core") (v "0.58.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.58") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvla66izsr45xq7dc3hqnk8xmvgckp7c3zimbfrlcy6kdnqhx62")))

(define-public crate-raui-core-0.59.0 (c (n "raui-core") (v "0.59.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.59") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fi1gw00k4w6lyy3wmvv46ywggb0g17wbkdqg35j6pjg66z23ds3")))

(define-public crate-raui-core-0.60.0 (c (n "raui-core") (v "0.60.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19nipg19rhgjqxw02mvvprxcgz2axvi2dgk14719f0b1cc6609gs")))

(define-public crate-raui-core-0.61.0 (c (n "raui-core") (v "0.61.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m17qjcn7cdzm78b2pmgi647qjrjnbn0g7aqj8p47g5kjvjjv7sy")))

(define-public crate-raui-core-0.61.1 (c (n "raui-core") (v "0.61.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "014cn4lika3sprfjpwxjqrcrfzkd6f9lyrvyvrni85wclcb705g1")))

(define-public crate-raui-core-0.61.2 (c (n "raui-core") (v "0.61.2") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dkf832lald366c3rc87jzk0fzhmak792cgcwfgmkg29xyncsyva")))

(define-public crate-raui-core-0.62.0 (c (n "raui-core") (v "0.62.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0as2y6n1pc7rkvn00a06rwkqm17hdrczll5g3la1qag05kdv0cqd")))

(define-public crate-raui-core-0.62.1 (c (n "raui-core") (v "0.62.1") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "084abwn3pp2cfsvpxp54whzfn582a7bmb81i6x8kiv6wabzps0ph")))

(define-public crate-raui-core-0.63.0 (c (n "raui-core") (v "0.63.0") (d (list (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "raui-derive") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vz1czfifllmpa2pbmnglmsa7p3qsqv36rpgbbchibgvk8c88mf4")))

