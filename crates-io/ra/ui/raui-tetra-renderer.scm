(define-module (crates-io ra ui raui-tetra-renderer) #:use-module (crates-io))

(define-public crate-raui-tetra-renderer-0.28.0 (c (n "raui-tetra-renderer") (v "0.28.0") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.28") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "1cwpyxppbhrbilbyg9y6nhkmh0nbjnhv12l6wdnh588z19hf7jp7")))

(define-public crate-raui-tetra-renderer-0.28.1 (c (n "raui-tetra-renderer") (v "0.28.1") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.28") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "0kvz7kgfxdmv5ps9smjgy92p7qgmjj3qz0r263rwmn5iqap9v9wh")))

(define-public crate-raui-tetra-renderer-0.29.0 (c (n "raui-tetra-renderer") (v "0.29.0") (d (list (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.29") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "1c69gadjr3p5d4g3rd55if09f23351yj11a718z4ph44r7n4cfjw")))

(define-public crate-raui-tetra-renderer-0.30.0 (c (n "raui-tetra-renderer") (v "0.30.0") (d (list (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.30") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "1i6dl90nxxswnl7cnf1drh7bcj9yv0vmdqi3czli08p0qg9gpcim")))

(define-public crate-raui-tetra-renderer-0.31.0 (c (n "raui-tetra-renderer") (v "0.31.0") (d (list (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.31") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "1r3yl1nl8s8d02al848pw2c4rhpss61ng4i7mdhi6z4vlw0znpap")))

(define-public crate-raui-tetra-renderer-0.32.0 (c (n "raui-tetra-renderer") (v "0.32.0") (d (list (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.32") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "192vbd8qc9fhb5zcra0qrd4lbbasq187xa28z5adqrhk4r8ld2kc")))

(define-public crate-raui-tetra-renderer-0.33.0 (c (n "raui-tetra-renderer") (v "0.33.0") (d (list (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.33") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (d #t) (k 0)))) (h "129qhf056m3m6jx2id34rzyqc5jwqc03fc3vqhijhnx60r0nc3zr")))

(define-public crate-raui-tetra-renderer-0.34.0 (c (n "raui-tetra-renderer") (v "0.34.0") (d (list (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.34") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0y4yzsgaa14jgcd9vzcx9sd3gd36bibzviipyjmhsxs8nc01qzmh")))

(define-public crate-raui-tetra-renderer-0.35.0 (c (n "raui-tetra-renderer") (v "0.35.0") (d (list (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.35") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "08ahzddkrfwfljf4qpgfaql0w6iinklygh0bgnnjd9pa5l1vw1n4")))

(define-public crate-raui-tetra-renderer-0.36.0 (c (n "raui-tetra-renderer") (v "0.36.0") (d (list (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.36") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "07agkifid4wcz7d5jgfjkcbvd8vzdczb8l8ivqy7jjdcrapazzbm")))

(define-public crate-raui-tetra-renderer-0.37.0 (c (n "raui-tetra-renderer") (v "0.37.0") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.37") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "023frvflsi21j93bx992pyn1v9bcibfknxzf6gga1xs8hmnw2m6b")))

(define-public crate-raui-tetra-renderer-0.37.1 (c (n "raui-tetra-renderer") (v "0.37.1") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.37") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0l5yrsqdq6lg8dfy38fbj8672jg7a9ch8hmp0zpky5i55v2rbbq7")))

(define-public crate-raui-tetra-renderer-0.38.0 (c (n "raui-tetra-renderer") (v "0.38.0") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.38") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0z0rb0w2ajafi0xj31akqgwz9s3dwjhigqfcz7wj8h83gp1rrjsn")))

(define-public crate-raui-tetra-renderer-0.38.1 (c (n "raui-tetra-renderer") (v "0.38.1") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.38") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "1dskxn977lj31cb2yd9hx6ag85q7r7fj28n5xddfhdjnyggqj8p8")))

(define-public crate-raui-tetra-renderer-0.38.2 (c (n "raui-tetra-renderer") (v "0.38.2") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.38") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0biwhv8jgjxi26hrc0p1wrhf6d02nhsp632vvczgkk30298bimib")))

(define-public crate-raui-tetra-renderer-0.38.3 (c (n "raui-tetra-renderer") (v "0.38.3") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.38") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "1z37xpq5w30h03dzshbzysbpfanj343dzfmz1my1bqiyqgq93z4m")))

(define-public crate-raui-tetra-renderer-0.38.4 (c (n "raui-tetra-renderer") (v "0.38.4") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.38") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0jkl517zi75na47dslzik3kwba13xykvizickx1q00djkb675rd7")))

(define-public crate-raui-tetra-renderer-0.40.0 (c (n "raui-tetra-renderer") (v "0.40.0") (d (list (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.40") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "1qag1a5zn5rhhjhnjc51v8maq23ls0vkc8650x7jxhnzx63hc9d6")))

(define-public crate-raui-tetra-renderer-0.41.0 (c (n "raui-tetra-renderer") (v "0.41.0") (d (list (d (n "raui-core") (r "^0.41") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.41") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "1m10hcy8n1lsaivbs5ixch8lrfz6a8dfg07xaak0kyy0by4ajd77")))

(define-public crate-raui-tetra-renderer-0.42.0 (c (n "raui-tetra-renderer") (v "0.42.0") (d (list (d (n "raui-core") (r "^0.42") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.42") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0gnxm18zgc8z3sdy169j1rrj77ryhws6iphgwzkpbh7vba7p34mk")))

(define-public crate-raui-tetra-renderer-0.43.0 (c (n "raui-tetra-renderer") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "raui-tesselate-renderer") (r "^0.43") (f (quote ("index32"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tetra") (r "^0.6") (f (quote ("texture_png" "font_ttf"))) (k 0)))) (h "0pikp237q1z6xvs9q3bannh2g4rqxgc8wjyc0i5v5cr8k6iwm5rz")))

