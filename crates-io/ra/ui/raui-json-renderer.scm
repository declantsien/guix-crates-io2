(define-module (crates-io ra ui raui-json-renderer) #:use-module (crates-io))

(define-public crate-raui-json-renderer-0.5.0 (c (n "raui-json-renderer") (v "0.5.0") (d (list (d (n "raui-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1zxysfi71hvkag2nv5d7wqbb4aa4w0808hd4sm6936y8frlxf5q2")))

(define-public crate-raui-json-renderer-0.6.0 (c (n "raui-json-renderer") (v "0.6.0") (d (list (d (n "raui-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13kdy9sq5d8zcnmyjfj9kaqgmhvfgz1k7d8hd6ymfjdvzqi6ahi2")))

(define-public crate-raui-json-renderer-0.7.0 (c (n "raui-json-renderer") (v "0.7.0") (d (list (d (n "raui-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0c57pnvyc3zlhdka038z0qxnwzypbmkshs8rn3rc2ffys8niy2ds")))

(define-public crate-raui-json-renderer-0.8.0 (c (n "raui-json-renderer") (v "0.8.0") (d (list (d (n "raui-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hs31c96q8q54p372m08nyajk7y13vxcvdhcnka3yi02gp55y873")))

(define-public crate-raui-json-renderer-0.9.1 (c (n "raui-json-renderer") (v "0.9.1") (d (list (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0si74f3mbzwf2ckxk1ck0y0lfmbsb3prz64kmbs4sa292asd78ad")))

(define-public crate-raui-json-renderer-0.9.2 (c (n "raui-json-renderer") (v "0.9.2") (d (list (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nhmsr8z8kqmyqjc62zhsdrkirhnwcxgyjbn1pjx6c22fz0nnmpc")))

(define-public crate-raui-json-renderer-0.10.0 (c (n "raui-json-renderer") (v "0.10.0") (d (list (d (n "raui-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08gm76mqak0nwkgvig3vvm9g9f1y3i91g23wdgdvmxp52pd51j4s")))

(define-public crate-raui-json-renderer-0.11.0 (c (n "raui-json-renderer") (v "0.11.0") (d (list (d (n "raui-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0276pmvg27i1hxsmb4131cnkh6iym4hwybw0fphs330h0ll113dp")))

(define-public crate-raui-json-renderer-0.12.1 (c (n "raui-json-renderer") (v "0.12.1") (d (list (d (n "raui-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17smhy7dlxhzwzkhyicpcn0nxqd43gjsz8p71zwc1pmdpdqwbm48")))

(define-public crate-raui-json-renderer-0.13.0 (c (n "raui-json-renderer") (v "0.13.0") (d (list (d (n "raui-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04gkyp5d1yis3d28yc82790vnqq5fnn3dkndpsf99kcc5cpkzrdv")))

(define-public crate-raui-json-renderer-0.14.0 (c (n "raui-json-renderer") (v "0.14.0") (d (list (d (n "raui-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ndkv97yksbgvi4g65gd9xyd83g5k39jain0vp18hn5qaklj9vln")))

(define-public crate-raui-json-renderer-0.15.0 (c (n "raui-json-renderer") (v "0.15.0") (d (list (d (n "raui-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "036511g5bv7hn30zskq2bn9jay3xwca60xzkspd54wfzwccfb6ws")))

(define-public crate-raui-json-renderer-0.16.0 (c (n "raui-json-renderer") (v "0.16.0") (d (list (d (n "raui-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f34fda4k4lkn3qghfa2kgpnmj9bk779d3mnzald3dgigsy8mvkr")))

(define-public crate-raui-json-renderer-0.17.0 (c (n "raui-json-renderer") (v "0.17.0") (d (list (d (n "raui-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yw0hznr3nr46ijxcfndvcc1jc1mh52nvzgw289hkd00gh3cilj3")))

(define-public crate-raui-json-renderer-0.18.0 (c (n "raui-json-renderer") (v "0.18.0") (d (list (d (n "raui-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sqr275lpwfq0jagkdsn3f79fdvjk3alc7v1pwqbhxdvkxdazjl6")))

(define-public crate-raui-json-renderer-0.19.0 (c (n "raui-json-renderer") (v "0.19.0") (d (list (d (n "raui-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gc7rrsncrnxxd2m4p3yxx96phh36g38imaaz6s3bp30j1nqaf03")))

(define-public crate-raui-json-renderer-0.20.0 (c (n "raui-json-renderer") (v "0.20.0") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18646z23b3mrkpl1a73xlhqfd2vi8mbzlayqvh2zf6snk7fvi4vy")))

(define-public crate-raui-json-renderer-0.20.1 (c (n "raui-json-renderer") (v "0.20.1") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0f674m5kf1rk6v6jrajnk314jz0mcgvk083r0x8isbzj8hiwi1bj")))

(define-public crate-raui-json-renderer-0.20.2 (c (n "raui-json-renderer") (v "0.20.2") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1l2mf9sjky0bvw06dmrj1w1sv97lfd40lpzayjc7bmknrg1ghwpg")))

(define-public crate-raui-json-renderer-0.21.0 (c (n "raui-json-renderer") (v "0.21.0") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1z0nc8krw6ya5yhc52qpqyssrsb6jrmshly4f58lfz6jjg9jjv3a")))

(define-public crate-raui-json-renderer-0.21.1 (c (n "raui-json-renderer") (v "0.21.1") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13nfcmxbn7y0lsbajjisyn4hh5qzg2qpgimk8a2wid2sriwhcji8")))

(define-public crate-raui-json-renderer-0.21.2 (c (n "raui-json-renderer") (v "0.21.2") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wrm4rshrp856jwwcsmqmhgam8nn4bpw5kdgpcrncylma65xfz06")))

(define-public crate-raui-json-renderer-0.21.3 (c (n "raui-json-renderer") (v "0.21.3") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "079sy31hddkcdyzv9q7idy15k54bpp82715z1nd7bb7vx44gy3mn")))

(define-public crate-raui-json-renderer-0.22.0 (c (n "raui-json-renderer") (v "0.22.0") (d (list (d (n "raui-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "099mil2mm5i0yqyvgg7si7h2pqsyl2imngkxw6cmm104y8xic8bd")))

(define-public crate-raui-json-renderer-0.23.0 (c (n "raui-json-renderer") (v "0.23.0") (d (list (d (n "raui-core") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10hj2p90130b8mrzdnjlivikdjss693n309k5bpa6ii46pda3lig")))

(define-public crate-raui-json-renderer-0.24.0 (c (n "raui-json-renderer") (v "0.24.0") (d (list (d (n "raui-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wn7cvb2waid624b0qsp4fwqq7wyxjl4d1akgvcph2lg66s4jwwb")))

(define-public crate-raui-json-renderer-0.25.0 (c (n "raui-json-renderer") (v "0.25.0") (d (list (d (n "raui-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ax48j9jgwby1hqd20dqyzv4g4hsyllmryxq8bkv7zn7j7rcnqvy")))

(define-public crate-raui-json-renderer-0.26.0 (c (n "raui-json-renderer") (v "0.26.0") (d (list (d (n "raui-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cjadsxy2x21z7q5ywabcaym5m2mbh3a4qngmrn4k2mn1iclrnvg")))

(define-public crate-raui-json-renderer-0.27.0 (c (n "raui-json-renderer") (v "0.27.0") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1v76z6z4k8iax2k9snv149sl95gjsghgspg67rn5yqx1j68prhm2")))

(define-public crate-raui-json-renderer-0.27.1 (c (n "raui-json-renderer") (v "0.27.1") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nlahqs6kwvfzvhq2rcnyyk3hn2ilpj384m41q72r62ziz4jdsvg")))

(define-public crate-raui-json-renderer-0.28.0 (c (n "raui-json-renderer") (v "0.28.0") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1x0wdd4dj7rpmkxhwkphiikpz3bw87ykxvw2xyj0970xwz352dq2")))

(define-public crate-raui-json-renderer-0.28.1 (c (n "raui-json-renderer") (v "0.28.1") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0fwmfznjzxnb3250kk9jzljj88lp18xd9mx4amprbkw9yky1sms1")))

(define-public crate-raui-json-renderer-0.29.0 (c (n "raui-json-renderer") (v "0.29.0") (d (list (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "065myay1y0b7sxwyk3wccm22sgyrg58a131agmk3xqny7jzqfah4")))

(define-public crate-raui-json-renderer-0.30.0 (c (n "raui-json-renderer") (v "0.30.0") (d (list (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1scb4har4bxsg8s1g8cwglpzcii83ijz4l5bi9mad7kp2dah8dc7")))

(define-public crate-raui-json-renderer-0.31.0 (c (n "raui-json-renderer") (v "0.31.0") (d (list (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03609ir1wxm9n68zkvhhdsynb8s4kqq64an5vfkr540m1jdbz9mb")))

(define-public crate-raui-json-renderer-0.32.0 (c (n "raui-json-renderer") (v "0.32.0") (d (list (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p6a51ixh23pv5lqzz6wzyzp5dacvc3330pc49mkfpdslhv3jcgc")))

(define-public crate-raui-json-renderer-0.33.0 (c (n "raui-json-renderer") (v "0.33.0") (d (list (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "11xaz60khqj21njan9d4jpmpdavf6gbs9jw4pbg545mwyyqxh0w6")))

(define-public crate-raui-json-renderer-0.34.0 (c (n "raui-json-renderer") (v "0.34.0") (d (list (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xim326km05zjahab3dc9iidfxly5yxksbj6d9xbj7cl7mcj2lk9")))

(define-public crate-raui-json-renderer-0.35.0 (c (n "raui-json-renderer") (v "0.35.0") (d (list (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ksds6z25c989kqsdhl0lrpkx5l3ppwcniqhpfc3bpq5rjx7p1wy")))

(define-public crate-raui-json-renderer-0.36.0 (c (n "raui-json-renderer") (v "0.36.0") (d (list (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18i89wxw7gbx6ybax6i8r3m9a2j5xvgq2c4fiqywc758amnwxiay")))

(define-public crate-raui-json-renderer-0.37.0 (c (n "raui-json-renderer") (v "0.37.0") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12n8a6w8qjgq0lkpxfwzzl81cxzxxi8f593ndcj9kb0vpiikyvj0")))

(define-public crate-raui-json-renderer-0.37.1 (c (n "raui-json-renderer") (v "0.37.1") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "016bpw8lsbgfgi4jqi3rwylkx4b72sd4kc7r9k7jvmkbhlxvfwy0")))

(define-public crate-raui-json-renderer-0.38.0 (c (n "raui-json-renderer") (v "0.38.0") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wh6qk5hrylsbr7sby730wk4ripdv8igpwx4j28l5m1ajx4b81b9")))

(define-public crate-raui-json-renderer-0.38.1 (c (n "raui-json-renderer") (v "0.38.1") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0dqqxw18nizh4h3jgnfx6xcgsydwr9nplsxpa486k8kz15iw7ydk")))

(define-public crate-raui-json-renderer-0.38.2 (c (n "raui-json-renderer") (v "0.38.2") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0s77mdxd9ydj3diqkxprn1qqhw6zycmip0w8iamkq6chgrjjxzai")))

(define-public crate-raui-json-renderer-0.38.3 (c (n "raui-json-renderer") (v "0.38.3") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cw8dk85pbxjssykcw5sgj7q2im6cxklb9ba5wc27ppc393fwbij")))

(define-public crate-raui-json-renderer-0.38.4 (c (n "raui-json-renderer") (v "0.38.4") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xwdy25m9qps4k6yd93rb9y9allp6ybvldcxdsrx59mffki9j1wk")))

(define-public crate-raui-json-renderer-0.40.0 (c (n "raui-json-renderer") (v "0.40.0") (d (list (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yipx3afdg47v0nm5ms6jrwyszkfc0pd84p25fpdmxailrkdp80y")))

(define-public crate-raui-json-renderer-0.41.0 (c (n "raui-json-renderer") (v "0.41.0") (d (list (d (n "raui-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0xq63blcplj7r2abljj4f4nnaxijr271885ny5vx7pfckhsdqjd1")))

(define-public crate-raui-json-renderer-0.42.0 (c (n "raui-json-renderer") (v "0.42.0") (d (list (d (n "raui-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nnh5sh7dbjcxh0ljcs9dimspg8mxcpfdnl98brjpcci3grwsl0z")))

(define-public crate-raui-json-renderer-0.43.0 (c (n "raui-json-renderer") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ld98cx9pz3h66f2wcp6vh49ymd1xa298gqzaa56n4afv87vfy78")))

(define-public crate-raui-json-renderer-0.44.0 (c (n "raui-json-renderer") (v "0.44.0") (d (list (d (n "raui-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0sdkqcs6y32qi0fbbnvsj4x03zz40nxxdmrc1hrqxg9h56iczjnc")))

(define-public crate-raui-json-renderer-0.45.0 (c (n "raui-json-renderer") (v "0.45.0") (d (list (d (n "raui-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ldjgv0sppzll17jar46wjq4p1d1jm1b0hvw0id3gsd2ap76xpz9")))

(define-public crate-raui-json-renderer-0.46.0 (c (n "raui-json-renderer") (v "0.46.0") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "158wmawxzbl5yrd147fdwx5yp7hk3wj6dqf2qk34zp75zbqknrrq")))

(define-public crate-raui-json-renderer-0.46.1 (c (n "raui-json-renderer") (v "0.46.1") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0qvnmxnss4h8ppldjqigw7h880yffnb2vpb6iizaj74kzxaz3aif")))

(define-public crate-raui-json-renderer-0.46.2 (c (n "raui-json-renderer") (v "0.46.2") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0v9w4ihwh69w59xhvarllczzr17w3ggb58rlka25akmk5j47wc4i")))

(define-public crate-raui-json-renderer-0.46.3 (c (n "raui-json-renderer") (v "0.46.3") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cjbjcapq219h60d08fw9ckfviq80l69ww5124nl6393hlwqy83j")))

(define-public crate-raui-json-renderer-0.47.0 (c (n "raui-json-renderer") (v "0.47.0") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1l9xsjxra0a4ij5cbs0mcli75g5g085ry8xafa2fq07dly9c31k1")))

(define-public crate-raui-json-renderer-0.47.1 (c (n "raui-json-renderer") (v "0.47.1") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dxsdd7v3548nym8am9byshk62nbiksghq5qiligncajgsim7zmf")))

(define-public crate-raui-json-renderer-0.47.2 (c (n "raui-json-renderer") (v "0.47.2") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09qbl3x01fagh31ar3hmscgm8akr5bcm19zn5k359n1kx4d9wxzi")))

(define-public crate-raui-json-renderer-0.48.0 (c (n "raui-json-renderer") (v "0.48.0") (d (list (d (n "raui-core") (r "^0.48") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1j6j2xwf0dnxck53zikd1gyc7i3wnpfkbh0x0cgx0pn36hszcfln")))

(define-public crate-raui-json-renderer-0.49.0 (c (n "raui-json-renderer") (v "0.49.0") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "16v3zd67yfnq6ri8k81h1pv1m3343ca9af38mdwyin4qwmiw8k6s")))

(define-public crate-raui-json-renderer-0.49.1 (c (n "raui-json-renderer") (v "0.49.1") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01my5mx9yf32n02qq09dw2irjfh1ln166z55j1vsgfdhjwy7j1sw")))

(define-public crate-raui-json-renderer-0.49.2 (c (n "raui-json-renderer") (v "0.49.2") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ykwwq1z2ra56yjdizryh3gsxi3x6al5k2nczs4mzyg5zbgl4md6")))

(define-public crate-raui-json-renderer-0.50.0 (c (n "raui-json-renderer") (v "0.50.0") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1qfpd7m509b93nh1cgwqynfavaz8rm5k2y1xxsxdflcfcbplv155")))

(define-public crate-raui-json-renderer-0.50.1 (c (n "raui-json-renderer") (v "0.50.1") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1873bv7pp6y40b7syvfsfqacq7za6zvr0ia0p7426nd8mlaqmwvn")))

(define-public crate-raui-json-renderer-0.50.2 (c (n "raui-json-renderer") (v "0.50.2") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ls83zm5xyg5gjnna9rq5936lnsca1qymciq25s9gpdi4la4hjn9")))

(define-public crate-raui-json-renderer-0.50.3 (c (n "raui-json-renderer") (v "0.50.3") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yyi8sfdd13ispq9vmsd6ldd99i5im264hf0y1zks9n42ym3a22m")))

(define-public crate-raui-json-renderer-0.51.0 (c (n "raui-json-renderer") (v "0.51.0") (d (list (d (n "raui-core") (r "^0.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0alqdp0yiac7ggjmsijp32rajl368wvf8jl5sk4asgbbl73685kj")))

(define-public crate-raui-json-renderer-0.52.0 (c (n "raui-json-renderer") (v "0.52.0") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0bh73q1bgih7b5p7cx0adp98kw2arxz13lp2yawv9bb0ns408k66")))

(define-public crate-raui-json-renderer-0.52.1 (c (n "raui-json-renderer") (v "0.52.1") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kxrbixlgsd9qs5d7igbgnz50piljqjg3v82rglixngy7lgin79f")))

(define-public crate-raui-json-renderer-0.53.0 (c (n "raui-json-renderer") (v "0.53.0") (d (list (d (n "raui-core") (r "^0.53") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0m9ssrnacng90zmg4qcw9zj4bnf03c37dvagikkszbsgdwjy235m")))

(define-public crate-raui-json-renderer-0.54.0 (c (n "raui-json-renderer") (v "0.54.0") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1abhmlgzcdgmzadpch01p6n4ad31cii4r99z4aysqwy810znfg3v")))

(define-public crate-raui-json-renderer-0.54.1 (c (n "raui-json-renderer") (v "0.54.1") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1cjwyxh8l2rxaxwfpw885rhq3clyirg33cdzllqa7knnrkizci0i")))

(define-public crate-raui-json-renderer-0.55.0 (c (n "raui-json-renderer") (v "0.55.0") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1g66hv88lws97s7nks5gf5sgmdm6w8p6213c8hl6kmklhqgnwpy0")))

(define-public crate-raui-json-renderer-0.55.1 (c (n "raui-json-renderer") (v "0.55.1") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13jh1s339q70ys269vh16rsi10a2zzag524iv1kafnwxjn50sh4c")))

(define-public crate-raui-json-renderer-0.55.2 (c (n "raui-json-renderer") (v "0.55.2") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1sv4z3wricwrqh1hwfmki919q557mymwpa0qbn2llpilifhhv55d")))

(define-public crate-raui-json-renderer-0.55.3 (c (n "raui-json-renderer") (v "0.55.3") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "132qvlrfg4jbr3axqap0rvg8vjv7kivb5f39mykk6dcjj7hmc991")))

(define-public crate-raui-json-renderer-0.55.4 (c (n "raui-json-renderer") (v "0.55.4") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04h8s9rvw16cagikm3chb96k8g5sw26aakl2bl8351ybii51g7mh")))

(define-public crate-raui-json-renderer-0.55.5 (c (n "raui-json-renderer") (v "0.55.5") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p711clqp4kdgymnxrj2bw280v7a2fym0n9f7mzjkr7dvhpsr4xd")))

(define-public crate-raui-json-renderer-0.55.6 (c (n "raui-json-renderer") (v "0.55.6") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1f1fvx4nf698z71my3ghn49c4badsr2l5hgbzx21zqqifnklcb47")))

(define-public crate-raui-json-renderer-0.56.0 (c (n "raui-json-renderer") (v "0.56.0") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wyc6dzyxqz9jp406wcr0ga71x55rqzcwnskxnvchi0w2nfvpsk4")))

(define-public crate-raui-json-renderer-0.56.1 (c (n "raui-json-renderer") (v "0.56.1") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07wbw37f35ybhzf3sdk4a165542r72y212d71x4qh7hm6lismswv")))

(define-public crate-raui-json-renderer-0.57.0 (c (n "raui-json-renderer") (v "0.57.0") (d (list (d (n "raui-core") (r "^0.57") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0pc2wi2n5ig4wb8hcnj2z0gmdf7pjcmhm84bzbcckpivh34spiza")))

(define-public crate-raui-json-renderer-0.58.0 (c (n "raui-json-renderer") (v "0.58.0") (d (list (d (n "raui-core") (r "^0.58") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "17365208mc6r9vl52xk4758gnzf69gaag5c2fx34g1hia6xbkys3")))

(define-public crate-raui-json-renderer-0.59.0 (c (n "raui-json-renderer") (v "0.59.0") (d (list (d (n "raui-core") (r "^0.59") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yj7rjnkxszh4k306jhhdy2hwmdbijcnc424hycy4x7sffxc7jlj")))

(define-public crate-raui-json-renderer-0.60.0 (c (n "raui-json-renderer") (v "0.60.0") (d (list (d (n "raui-core") (r "^0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0mxipdypjzpy0bn2jvam2hgq5ahd1cf0fbmwsi4pj03ayr260zkl")))

(define-public crate-raui-json-renderer-0.61.0 (c (n "raui-json-renderer") (v "0.61.0") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hnrbiwnrbr21rkw9qri57f1rkny11zrc3pw79vdfgjvhff026yk")))

(define-public crate-raui-json-renderer-0.61.1 (c (n "raui-json-renderer") (v "0.61.1") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0i74r2846kw6k36bfjicwixakdg587ifqfqk1ic6zrxzqlal870n")))

(define-public crate-raui-json-renderer-0.61.2 (c (n "raui-json-renderer") (v "0.61.2") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1jcrx94iiybj9yvmvcmsmsapqdgsw4p5vvcj1pq2arqmiif07b7j")))

(define-public crate-raui-json-renderer-0.62.0 (c (n "raui-json-renderer") (v "0.62.0") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1wgdjx723rcpqknc5x3qbmvxb3p48dm1yg6hc63idfh9r1wralcr")))

(define-public crate-raui-json-renderer-0.62.1 (c (n "raui-json-renderer") (v "0.62.1") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0549f95239c7vc2lx3m3nf4dxyxrvdwiqdgbmqnjp80z0spr593k")))

(define-public crate-raui-json-renderer-0.63.0 (c (n "raui-json-renderer") (v "0.63.0") (d (list (d (n "raui-core") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08mja0q38sfayi84f5z9v6fwwwc7bwm2mgs6arxp44srxh5z21px")))

