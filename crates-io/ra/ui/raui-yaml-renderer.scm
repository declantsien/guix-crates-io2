(define-module (crates-io ra ui raui-yaml-renderer) #:use-module (crates-io))

(define-public crate-raui-yaml-renderer-0.5.0 (c (n "raui-yaml-renderer") (v "0.5.0") (d (list (d (n "raui-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0bkdcpc3mpv8hpg5rqs9zfa8jq7wc0602pm56qgfqnyx1z9as0w0")))

(define-public crate-raui-yaml-renderer-0.6.0 (c (n "raui-yaml-renderer") (v "0.6.0") (d (list (d (n "raui-core") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0qbfzrqmb80w6zkdph0yk9gg0gna8iv9yv0m0wndk0klndsbzkc9")))

(define-public crate-raui-yaml-renderer-0.7.0 (c (n "raui-yaml-renderer") (v "0.7.0") (d (list (d (n "raui-core") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "03sin316j4mh8a0s43hia6sihd8rhjqz2gvw26cw49zlpfwn6pp9")))

(define-public crate-raui-yaml-renderer-0.8.0 (c (n "raui-yaml-renderer") (v "0.8.0") (d (list (d (n "raui-core") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1a7c051z4pm9z07757dybhldqh9hd1rc1rb106wi9b57ygdarc33")))

(define-public crate-raui-yaml-renderer-0.9.1 (c (n "raui-yaml-renderer") (v "0.9.1") (d (list (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1za2jp6bzm9jwsv73v5p66hcs2r2a2vadhbp5gb9xfjvgj9mcncd")))

(define-public crate-raui-yaml-renderer-0.9.2 (c (n "raui-yaml-renderer") (v "0.9.2") (d (list (d (n "raui-core") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0j63qywqi73q8890gdmp9fcgbhd43z50w6xrpmj038addbysazq1")))

(define-public crate-raui-yaml-renderer-0.10.0 (c (n "raui-yaml-renderer") (v "0.10.0") (d (list (d (n "raui-core") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1gwvlv5rlhlyypv2008zq811wqsjqndqqfr3vs8rb3r8dvvl03bl")))

(define-public crate-raui-yaml-renderer-0.11.0 (c (n "raui-yaml-renderer") (v "0.11.0") (d (list (d (n "raui-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ra178j205nvyy08vfaal9062k2mqpsmy27pvk1ccz00xf4fv85x")))

(define-public crate-raui-yaml-renderer-0.12.1 (c (n "raui-yaml-renderer") (v "0.12.1") (d (list (d (n "raui-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0h717i6jkhmhkfwik0shv44hqk35c38niiw5j9sb2k4pxwkdqcjq")))

(define-public crate-raui-yaml-renderer-0.13.0 (c (n "raui-yaml-renderer") (v "0.13.0") (d (list (d (n "raui-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "05vcw4v5lk3d7n4glxd68ygzmrapxclkq8zp8h7xy9li7n0hlc6h")))

(define-public crate-raui-yaml-renderer-0.14.0 (c (n "raui-yaml-renderer") (v "0.14.0") (d (list (d (n "raui-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "16bbswwlv0xb8kcdn7ba965kkh449l5pi0fvw9zsrzagfcz16z16")))

(define-public crate-raui-yaml-renderer-0.15.0 (c (n "raui-yaml-renderer") (v "0.15.0") (d (list (d (n "raui-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ycdvxqyfvvvz5yh8xzhblq9gfn8kpygcxdvpgagfcdhxwfkrvwf")))

(define-public crate-raui-yaml-renderer-0.16.0 (c (n "raui-yaml-renderer") (v "0.16.0") (d (list (d (n "raui-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1frspp5px93cmsq37pwccd5rkmsp160892gw9vqfjhkcyjnhp3jr")))

(define-public crate-raui-yaml-renderer-0.17.0 (c (n "raui-yaml-renderer") (v "0.17.0") (d (list (d (n "raui-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "12q8fgpincbq8dszrlynny4i8nxbk8rxfzxl8r1chmjag99ac4z4")))

(define-public crate-raui-yaml-renderer-0.18.0 (c (n "raui-yaml-renderer") (v "0.18.0") (d (list (d (n "raui-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1vd8xxv42swkl5iiyzi1giav1wlg9d19gknm97j4iic18dla4qqa")))

(define-public crate-raui-yaml-renderer-0.19.0 (c (n "raui-yaml-renderer") (v "0.19.0") (d (list (d (n "raui-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "07318075j5s2xni2j20ikqdbwqh7sbbansn3izygdaqr1dgvxbsl")))

(define-public crate-raui-yaml-renderer-0.20.0 (c (n "raui-yaml-renderer") (v "0.20.0") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1pmmhjj5cpraw8i6m0acwwq50g3zwqgs53c4dxjifq7y1gha96p5")))

(define-public crate-raui-yaml-renderer-0.20.1 (c (n "raui-yaml-renderer") (v "0.20.1") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "00gkqzp6f7z5skyy8s88jxxkma7jgzy8yvdwxj3jin8xan53ybs6")))

(define-public crate-raui-yaml-renderer-0.20.2 (c (n "raui-yaml-renderer") (v "0.20.2") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ni76n8sd0a6vgawdgnxhgsap22kmnjyf56f6xzq26l0kmaxsb23")))

(define-public crate-raui-yaml-renderer-0.21.0 (c (n "raui-yaml-renderer") (v "0.21.0") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "163bzazabha74vkfls2k4p9y20qqn3kpymv1978g1bjgmjaslymz")))

(define-public crate-raui-yaml-renderer-0.21.1 (c (n "raui-yaml-renderer") (v "0.21.1") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "136xa5sls9d500xiv5aip727nbp7ai8c3hapy6kzz0i3ln80a2fg")))

(define-public crate-raui-yaml-renderer-0.21.2 (c (n "raui-yaml-renderer") (v "0.21.2") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0yafm4pq5n7nd25ddywhcj3nwidmzp2ylqhh2sv2i4zpv351wsbi")))

(define-public crate-raui-yaml-renderer-0.21.3 (c (n "raui-yaml-renderer") (v "0.21.3") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1sf2984bkdmacqkl3j2p5q69dxhp1980kimhrv8573x4fvivr8np")))

(define-public crate-raui-yaml-renderer-0.22.0 (c (n "raui-yaml-renderer") (v "0.22.0") (d (list (d (n "raui-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1lxswq1aqnqrwqp9l3rf9dyga36709qimf7jjg3nw4cfi3acp357")))

(define-public crate-raui-yaml-renderer-0.23.0 (c (n "raui-yaml-renderer") (v "0.23.0") (d (list (d (n "raui-core") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1bbxxn1l5w3xl99c38fl641bf2fccd82xvb8zgraw6hciw756bx6")))

(define-public crate-raui-yaml-renderer-0.24.0 (c (n "raui-yaml-renderer") (v "0.24.0") (d (list (d (n "raui-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "13nwp4yb4iylr8ii8bbq6nc3bhyyya0p92agq3p1y5ls66lwfl5s")))

(define-public crate-raui-yaml-renderer-0.25.0 (c (n "raui-yaml-renderer") (v "0.25.0") (d (list (d (n "raui-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0iqzihdc6dav54scwjmdqka5bmzhldvgw9gxr299m0dd8a5w8xfv")))

(define-public crate-raui-yaml-renderer-0.26.0 (c (n "raui-yaml-renderer") (v "0.26.0") (d (list (d (n "raui-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0j6mdhg4rrm46r65a1gjsv9dlz327l8qcnbvzl1znli6zkrr3y2x")))

(define-public crate-raui-yaml-renderer-0.27.0 (c (n "raui-yaml-renderer") (v "0.27.0") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1l157lg094jmpj99v3y3vvj34i8rq46wacsfx4vx74l899yva3am")))

(define-public crate-raui-yaml-renderer-0.27.1 (c (n "raui-yaml-renderer") (v "0.27.1") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "06ca36v57h7l7rxmp69hljwmjdf801ixzpackvbrlkld44przala")))

(define-public crate-raui-yaml-renderer-0.28.0 (c (n "raui-yaml-renderer") (v "0.28.0") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0kfc1ij26mciraqnazls4b1bdqaka6awa4sry6qpqnw49izjj391")))

(define-public crate-raui-yaml-renderer-0.28.1 (c (n "raui-yaml-renderer") (v "0.28.1") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "00if58zdhxpaivzcqcaqdl8x5x2pk177g61ipa6bsqlyk3z4g8d8")))

(define-public crate-raui-yaml-renderer-0.29.0 (c (n "raui-yaml-renderer") (v "0.29.0") (d (list (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1dqfiazzapp20zj4h03gmz612cnx93kinxl34vqw37qvwxl6ni5i")))

(define-public crate-raui-yaml-renderer-0.30.0 (c (n "raui-yaml-renderer") (v "0.30.0") (d (list (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1rhdlhsz4ybmgm93c8ywh4j566zsvknigrj1xcy0ggp311p3agnm")))

(define-public crate-raui-yaml-renderer-0.31.0 (c (n "raui-yaml-renderer") (v "0.31.0") (d (list (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1rbzccin4vbpdgiq6mqhbpcg8wmycpd52qizdjyc3lw1f1wa9zy1")))

(define-public crate-raui-yaml-renderer-0.32.0 (c (n "raui-yaml-renderer") (v "0.32.0") (d (list (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1ijab9gjfk9fal56g29gkph8ghzgfqw60g8kpagqpn5cngc9iqxs")))

(define-public crate-raui-yaml-renderer-0.33.0 (c (n "raui-yaml-renderer") (v "0.33.0") (d (list (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "16dxky4r1jcbmwzyy7g8r2cwlzfrpsqqhwm43qxh8xw841rz51av")))

(define-public crate-raui-yaml-renderer-0.34.0 (c (n "raui-yaml-renderer") (v "0.34.0") (d (list (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1h8fmgxd4vkp619b3vb2hrp6vs15jam95zljq3r29wg6s20gb2nf")))

(define-public crate-raui-yaml-renderer-0.35.0 (c (n "raui-yaml-renderer") (v "0.35.0") (d (list (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ygjddzvx7213hzv6ndgsvp4xfq7jpvnb916r26nf6fdpkmnx32n")))

(define-public crate-raui-yaml-renderer-0.36.0 (c (n "raui-yaml-renderer") (v "0.36.0") (d (list (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10wp0qqmhlzgsx1v5rp3ryl4p7pksapvihvn8pgk8asmd53rwhfg")))

(define-public crate-raui-yaml-renderer-0.37.0 (c (n "raui-yaml-renderer") (v "0.37.0") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "111ykdysc38rfcvammd6xjqyg5im6zbpkj3w5d83ly0fwp05wqxf")))

(define-public crate-raui-yaml-renderer-0.37.1 (c (n "raui-yaml-renderer") (v "0.37.1") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "10464wsq8q6zpsfqzmriwf6rhr5hhv24r3kd3pvxivkx8inkr391")))

(define-public crate-raui-yaml-renderer-0.38.0 (c (n "raui-yaml-renderer") (v "0.38.0") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1x2hmwq8b0pd2vymb6f5ix11dyv30qv14a4p09dr4gh2d97ag2jj")))

(define-public crate-raui-yaml-renderer-0.38.1 (c (n "raui-yaml-renderer") (v "0.38.1") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1pd9xq0zrhmmljr0wwr4gwfzpfmkbvy59ifpsrx8cpd7dpk83ilb")))

(define-public crate-raui-yaml-renderer-0.38.2 (c (n "raui-yaml-renderer") (v "0.38.2") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1nngns25h022ljkhkcavwvpywjrccpxrx7flg0lsaa33fdap0dyd")))

(define-public crate-raui-yaml-renderer-0.38.3 (c (n "raui-yaml-renderer") (v "0.38.3") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0ish1fqmi4an4nfy173k0sy1rcbkx0m8x1k3mbfhj54lychwblcy")))

(define-public crate-raui-yaml-renderer-0.38.4 (c (n "raui-yaml-renderer") (v "0.38.4") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1vqrz004zg772fkgg85cgk0cfqyh4pj3laqw1z2p37qav3anljy2")))

(define-public crate-raui-yaml-renderer-0.40.0 (c (n "raui-yaml-renderer") (v "0.40.0") (d (list (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "199yz1w80kxl77r82ci9bdqifa2vsfgvrgz9ld0k3xkfrf72ljkr")))

(define-public crate-raui-yaml-renderer-0.41.0 (c (n "raui-yaml-renderer") (v "0.41.0") (d (list (d (n "raui-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "011w7dmr2f6h8mqc28rn3x0nqi7rcs1vd5sgis1gbjp1lpv9zrja")))

