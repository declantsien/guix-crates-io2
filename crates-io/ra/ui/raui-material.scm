(define-module (crates-io ra ui raui-material) #:use-module (crates-io))

(define-public crate-raui-material-0.11.0 (c (n "raui-material") (v "0.11.0") (d (list (d (n "raui-core") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c3vslyx1f1zz3np2k873r68nx2nk1gs31bfzm7d6n1rib18ds2q")))

(define-public crate-raui-material-0.12.1 (c (n "raui-material") (v "0.12.1") (d (list (d (n "raui-core") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h7wqkbil9345z50nb834mjjhqlsh8056yhiijc4j6pa0k87py5h")))

(define-public crate-raui-material-0.13.0 (c (n "raui-material") (v "0.13.0") (d (list (d (n "raui-core") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1984pns0rzxvacjapwa0yw3zw90vmf517x4a4ap1j5ll91vhwc0n")))

(define-public crate-raui-material-0.14.0 (c (n "raui-material") (v "0.14.0") (d (list (d (n "raui-core") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18r15pla6v19w03l0shyr9l9f5z786yzl3jj06pgld1ccablnd5s")))

(define-public crate-raui-material-0.15.0 (c (n "raui-material") (v "0.15.0") (d (list (d (n "raui-core") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sp1cab0ywc0aw6mmv471zn9khri062d3cpba6vq49f7a71k6d8k")))

(define-public crate-raui-material-0.16.0 (c (n "raui-material") (v "0.16.0") (d (list (d (n "raui-core") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "173ghv9wpskk365w8iq19yfspza9yr84417ibf73ix0haclbm2zv")))

(define-public crate-raui-material-0.17.0 (c (n "raui-material") (v "0.17.0") (d (list (d (n "raui-core") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15b54s7s3bpb0w0z2g7vfcg3nqk4bgsh8gsdb8fhfdv8wpw50dp8")))

(define-public crate-raui-material-0.18.0 (c (n "raui-material") (v "0.18.0") (d (list (d (n "raui-core") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0fcv0b7f8hxxj6zprx0j7rimprl7gz6gyqka8yks38my712xg45s")))

(define-public crate-raui-material-0.19.0 (c (n "raui-material") (v "0.19.0") (d (list (d (n "raui-core") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "020kwxy00ydyb5fcvmw7s74bswpl8whnmygnrw8bxx5qlgyknsbn")))

(define-public crate-raui-material-0.20.0 (c (n "raui-material") (v "0.20.0") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0mnr8596i8iyz5pvi63kgggzhfqb6hsnach0pbi4y65pk50kz981")))

(define-public crate-raui-material-0.20.1 (c (n "raui-material") (v "0.20.1") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0dbqzkqqrsw30ax42gfq7v8cpvjsdpmsl2jpqhj6x8ac8971wg3l")))

(define-public crate-raui-material-0.20.2 (c (n "raui-material") (v "0.20.2") (d (list (d (n "raui-core") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "09b9yigcdycxmb90wrdmczhm0pjkdqy02cmazlvvh48m6cgka4sb")))

(define-public crate-raui-material-0.21.0 (c (n "raui-material") (v "0.21.0") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p53vawcl22k3anxg29rs9zgy8pjv0iwag7vga74pv9438y2kla4")))

(define-public crate-raui-material-0.21.1 (c (n "raui-material") (v "0.21.1") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fxfxf6fkyqdnwp06y9m9aff26rsfixf5r6l869zal5809wc2hgp")))

(define-public crate-raui-material-0.21.2 (c (n "raui-material") (v "0.21.2") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "039x14xq1844kaizbrs3pqkl4c9bapwnvgjrz49czavcj1wl3m7r")))

(define-public crate-raui-material-0.21.3 (c (n "raui-material") (v "0.21.3") (d (list (d (n "raui-core") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bw1gad0yl2rf8bjabpm6l7hgzi7blvlgdksmb9glgh22mj3m7ar")))

(define-public crate-raui-material-0.22.0 (c (n "raui-material") (v "0.22.0") (d (list (d (n "raui-core") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bsh0qgdlnkdr36v1q2fqks8a8wjhabl76ck81lm7c9bm9f02gw9")))

(define-public crate-raui-material-0.23.0 (c (n "raui-material") (v "0.23.0") (d (list (d (n "raui-core") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q0indsplxvjya5kvxfpl1ml0vxw7y7ja91pjkk54aiqkr605lz6")))

(define-public crate-raui-material-0.24.0 (c (n "raui-material") (v "0.24.0") (d (list (d (n "raui-core") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fk22943wa2yrcqh6cdv9sqj3g5ybpka9222y0gzr9r461jg72cr")))

(define-public crate-raui-material-0.25.0 (c (n "raui-material") (v "0.25.0") (d (list (d (n "raui-core") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ygz5gmnyszimx2h6jq6qpkr28dldcx42xjmmgfw4h74n7gvc3hh")))

(define-public crate-raui-material-0.26.0 (c (n "raui-material") (v "0.26.0") (d (list (d (n "raui-core") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "004kflkrkd721d94ascnrih3pngwbnh8han9f2037gmms858awgm")))

(define-public crate-raui-material-0.27.0 (c (n "raui-material") (v "0.27.0") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12vk8ln66hlvjbshy24sqs7s4drgjv0n4fjqz8wwha72vkjwhxmd")))

(define-public crate-raui-material-0.27.1 (c (n "raui-material") (v "0.27.1") (d (list (d (n "raui-core") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1akmf9pjpjlvgs4ag19ql7n6qwld76kbf5pdnih27rxhyf141j94")))

(define-public crate-raui-material-0.28.0 (c (n "raui-material") (v "0.28.0") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08pxna5b55bghalxw1mjjni3hvnxvlm0mh4dsqsrlj50vzf5pqmx") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.28.1 (c (n "raui-material") (v "0.28.1") (d (list (d (n "raui-core") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rn3nm8bk19wy5rknkg32am32mq7d59szz3bhslwzcfzcfswg859") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.29.0 (c (n "raui-material") (v "0.29.0") (d (list (d (n "raui-core") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a2pchrcqwhgvl6xj9kg73zz2cwnibm6i76db28jdyi8w8lrwyki") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.30.0 (c (n "raui-material") (v "0.30.0") (d (list (d (n "raui-core") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02qh3j3nyighw1aixqvfay78vfbnygzs61m8220w45xrjhw6mb5l") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.31.0 (c (n "raui-material") (v "0.31.0") (d (list (d (n "raui-core") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wrmc6iagvx3482hb4nqq79az8mj19gyy9mzsl78ps95bdydivi3") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.32.0 (c (n "raui-material") (v "0.32.0") (d (list (d (n "raui-core") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1shln2by275602h6s3cjnax3g0j68102bym9ymyhy5iaympjhb5f") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.33.0 (c (n "raui-material") (v "0.33.0") (d (list (d (n "raui-core") (r "^0.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b8xqg4ws0wqjyk3pp3vzasi9i07p4xpzi8mfwaw820ylj440dga") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.34.0 (c (n "raui-material") (v "0.34.0") (d (list (d (n "raui-core") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02yy87nf2wax4px4g1y49i9vf07y259xgr07x0k0xlbm6k4bjb94") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.35.0 (c (n "raui-material") (v "0.35.0") (d (list (d (n "raui-core") (r "^0.35") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g0xwayzg53ig0acgmfp15wcghmk9d9y1mn40j65i359ning1xih") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.36.0 (c (n "raui-material") (v "0.36.0") (d (list (d (n "raui-core") (r "^0.36") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mw4f8dy0v17s90iibbs8czn7v9dd37kazynb0bi4xk1kn4x099h") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.37.0 (c (n "raui-material") (v "0.37.0") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vjjmi03zlfycgxjcyz9kwqr7v31kphl04g4ivynzskxc5xbayys") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.37.1 (c (n "raui-material") (v "0.37.1") (d (list (d (n "raui-core") (r "^0.37") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zfz96yknx5zf8q5s6pf1qsmmff9dqy505fxzwvxzp2lcg9wa56d") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.38.0 (c (n "raui-material") (v "0.38.0") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c6kyr8gs8s5dsrcbr19l8058yvf71k5j9lnh8jynzkpns9hwy2s") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.38.1 (c (n "raui-material") (v "0.38.1") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p9d4ifspgqpidp610ws1fm6sc5rlic66jsifnppim9ma8h1f58r") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.38.2 (c (n "raui-material") (v "0.38.2") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f4l76m37zlf9nixyhfhx8hxqarwibvaj98iw93xqis1dj0m6dkq") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.38.3 (c (n "raui-material") (v "0.38.3") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11020gkb5bp2i0gjcq1p60kadagk4y43bh41mc21dwdribxyv0zw") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.38.4 (c (n "raui-material") (v "0.38.4") (d (list (d (n "raui-core") (r "^0.38") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c94sa2afyknap9dzl1c689kf9g744wxf9aqi8g68b6gpgm41rg1") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.40.0 (c (n "raui-material") (v "0.40.0") (d (list (d (n "raui-core") (r "^0.40") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mxsp3nzmcv0icwgqlscilgqzdyhxgnzgssjggvz523f2z26ys9h") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.41.0 (c (n "raui-material") (v "0.41.0") (d (list (d (n "raui-core") (r "^0.41") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s466ya136sxhp59smmz5524g8viy4914q9rzfi4fvnn4mrwpbzp") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.42.0 (c (n "raui-material") (v "0.42.0") (d (list (d (n "raui-core") (r "^0.42") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bafhy65fpk2vwxk94vp8a8zy20ichsgadvq8sixpl99ys5gvh6z") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.43.0 (c (n "raui-material") (v "0.43.0") (d (list (d (n "raui-core") (r "^0.43") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wz7n5h9gnbdxcxxjzx44psscc66f26xpk42xmz6gnpa6ns8r3ck") (f (quote (("scalar64" "raui-core/scalar64") ("integer64" "raui-core/integer64"))))))

(define-public crate-raui-material-0.44.0 (c (n "raui-material") (v "0.44.0") (d (list (d (n "raui-core") (r "^0.44") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nndnaai0cm4wq7n6dfk38zcz3nzzi8brklqf4jq3rarlx2ld073")))

(define-public crate-raui-material-0.45.0 (c (n "raui-material") (v "0.45.0") (d (list (d (n "raui-core") (r "^0.45") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n32532ag6scyqkgvr72myhr6pxl1dshyxmac0zcd697x9x4xbs8")))

(define-public crate-raui-material-0.46.0 (c (n "raui-material") (v "0.46.0") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09xnj9aw9009lx2nblr5bf83rlgp9kg21r76qh8zwlv7q5h72fsx")))

(define-public crate-raui-material-0.46.1 (c (n "raui-material") (v "0.46.1") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0an3py4xldglkih7fkqzxq27cqvk27bcbpjb8d9vq0myk42d4vvk")))

(define-public crate-raui-material-0.46.2 (c (n "raui-material") (v "0.46.2") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p25yjrh7zv6zi1bpfrjvs24hcchgnlyn73ycq7nw8p54j6l2ny6")))

(define-public crate-raui-material-0.46.3 (c (n "raui-material") (v "0.46.3") (d (list (d (n "raui-core") (r "^0.46") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0752wkf0dr1yzlklvzak9ifwgfva0qmdpf2h6xkr5mv4hj72i2l1")))

(define-public crate-raui-material-0.47.0 (c (n "raui-material") (v "0.47.0") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "059z6h8kfvbv7izkicif1v2xsm1l80ndlizqhckq4f6dspq9mw5f")))

(define-public crate-raui-material-0.47.1 (c (n "raui-material") (v "0.47.1") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wh93l2d65r8iil8m8ls3cs4r1pq97x3v5lsm9i1kb3m37wd0nxk")))

(define-public crate-raui-material-0.47.2 (c (n "raui-material") (v "0.47.2") (d (list (d (n "raui-core") (r "^0.47") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rddh1fjdnwv3v9gmlkc2jcars8kjg62fvh9pcfp7960k4wn5d7p")))

(define-public crate-raui-material-0.48.0 (c (n "raui-material") (v "0.48.0") (d (list (d (n "raui-core") (r "^0.48") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mk0i3qjlfpj49fqj4gywhdbr5jl22mifhmw1d37kbg1fvbznjfb")))

(define-public crate-raui-material-0.49.0 (c (n "raui-material") (v "0.49.0") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ll3i8g49sq5afrl165k87jqfk85xfkfjfkvwmimdg3kv60q399k")))

(define-public crate-raui-material-0.49.1 (c (n "raui-material") (v "0.49.1") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j99dr5an64gi3h05h3249c9l0x58c6wwn8sscsfyddic1rzw7ky")))

(define-public crate-raui-material-0.49.2 (c (n "raui-material") (v "0.49.2") (d (list (d (n "raui-core") (r "^0.49") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f8sz9f0g6hlihypm6w576kqil15sanq5d31hf32npcw4m69s4la")))

(define-public crate-raui-material-0.50.0 (c (n "raui-material") (v "0.50.0") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q5nyi8g2nppx3d9cgc082fkmdw95slns8x5baj5p4blkl7yxs0w")))

(define-public crate-raui-material-0.50.1 (c (n "raui-material") (v "0.50.1") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr7ghsyjfzqzqxfyz33vj9wchx5p56mlnlz3n25a9fmz9z2dayn")))

(define-public crate-raui-material-0.50.2 (c (n "raui-material") (v "0.50.2") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f2i6jiwqjlavn1binq1w9iwhnfapxknpn21x7rvchjbhzwfplbl")))

(define-public crate-raui-material-0.50.3 (c (n "raui-material") (v "0.50.3") (d (list (d (n "raui-core") (r "^0.50") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dhlcirvdw5j98wflykx5f6p3fbwlzalylmm4d7kdarf0jwc1d50")))

(define-public crate-raui-material-0.51.0 (c (n "raui-material") (v "0.51.0") (d (list (d (n "raui-core") (r "^0.51") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "149788p2dpmx0yb3r76bsjib1937l8kkjmjkf8j3ymnkcqp0x697")))

(define-public crate-raui-material-0.52.0 (c (n "raui-material") (v "0.52.0") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mrlq4w07x3cf2jhcl9767cd2sgi01mmzhw02xly0qqqv9952siz")))

(define-public crate-raui-material-0.52.1 (c (n "raui-material") (v "0.52.1") (d (list (d (n "raui-core") (r "^0.52") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03d7n72dsj01izl4ssvxa8da94siplb9iknrrsbh7cgfcw3gzdpc")))

(define-public crate-raui-material-0.53.0 (c (n "raui-material") (v "0.53.0") (d (list (d (n "raui-core") (r "^0.53") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0896asp6z2mh5gw4ilyjpmd55p356hn0syj8xhljq0djcsniv5af")))

(define-public crate-raui-material-0.54.0 (c (n "raui-material") (v "0.54.0") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04r68n0y85q20vr6kbmgi3lzlwh7c73ygdfax79iik4qrvbr86xd")))

(define-public crate-raui-material-0.54.1 (c (n "raui-material") (v "0.54.1") (d (list (d (n "raui-core") (r "^0.54") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12rfk3np332sl1yxcq0249psxpd8p8i6h4sr0fcwyk1yafp2bgwr")))

(define-public crate-raui-material-0.55.0 (c (n "raui-material") (v "0.55.0") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0njjh49j0mdmblp8dkj40yzalkrppyyc4l7fdqi9a53pxrylg6p5")))

(define-public crate-raui-material-0.55.1 (c (n "raui-material") (v "0.55.1") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ai6f7xpqhvdprhx9ndvra1ydxwx7v7gabgagd5zdcx1p2x5g4z")))

(define-public crate-raui-material-0.55.2 (c (n "raui-material") (v "0.55.2") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0drg59pnniz02gvkvz9835pxszkpqwpgzjdg3m8f0qv2i6sn9zha")))

(define-public crate-raui-material-0.55.3 (c (n "raui-material") (v "0.55.3") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s9d1zkgp4jl6rlsrzr0n35781q5z5lh3dyh6743ixas8i1aw398")))

(define-public crate-raui-material-0.55.4 (c (n "raui-material") (v "0.55.4") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05820m0ghvnhvi968wi4yj5z7vhy4wipvrh4yp33pygxrnqxzm9p")))

(define-public crate-raui-material-0.55.5 (c (n "raui-material") (v "0.55.5") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0276jclqhdy48hrspmlsmjihfy46v942ggwxgpfshmzivw2y6s4m")))

(define-public crate-raui-material-0.55.6 (c (n "raui-material") (v "0.55.6") (d (list (d (n "raui-core") (r "^0.55") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0py3n87qn6in0ay1n4qp2dxvcnp43188x55qrvzsxfrhclkgbbbl")))

(define-public crate-raui-material-0.56.0 (c (n "raui-material") (v "0.56.0") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aqzaz3gzv14akzbc835gfr8byz9cc364vwbh4ng6617mj77qb29")))

(define-public crate-raui-material-0.56.1 (c (n "raui-material") (v "0.56.1") (d (list (d (n "raui-core") (r "^0.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05yyidwyj6bcgbvi6b6iziq6h775bcwix7l1mzl8l1ygprzrislz")))

(define-public crate-raui-material-0.57.0 (c (n "raui-material") (v "0.57.0") (d (list (d (n "raui-core") (r "^0.57") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zlgggklgqnr35fzvqh2bg4i3agkyk362pms5wlics5jrkgz7i9f")))

(define-public crate-raui-material-0.58.0 (c (n "raui-material") (v "0.58.0") (d (list (d (n "raui-core") (r "^0.58") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c9wnwnczih09l2dqcbgjffxaxs7lig3qj8dddsvh6gkvs4d0vfj")))

(define-public crate-raui-material-0.59.0 (c (n "raui-material") (v "0.59.0") (d (list (d (n "raui-core") (r "^0.59") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n8djfh3c4wql9wnidqinxi3xz55ql8gw5a0vk1pxv9wn0qkjqpq")))

(define-public crate-raui-material-0.60.0 (c (n "raui-material") (v "0.60.0") (d (list (d (n "raui-core") (r "^0.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "09krsyg5xcn7kjv0wzidqvd6a085603vkdhlx36awbv9pkwxr1h5")))

(define-public crate-raui-material-0.61.0 (c (n "raui-material") (v "0.61.0") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mc0ia752427b4fw41llscwnajlh47ik8zkf1yl3zh0bvfcpwmqv")))

(define-public crate-raui-material-0.61.1 (c (n "raui-material") (v "0.61.1") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zyky3xkykh2vc8msjls26cfi8n1zbn07wpfv5wssg0lkmb2nblv")))

(define-public crate-raui-material-0.61.2 (c (n "raui-material") (v "0.61.2") (d (list (d (n "raui-core") (r "^0.61") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10hj18fcm3brpxvrgfxwmbp6c8kkk4s8krdihmps613wdy8wg4ln")))

(define-public crate-raui-material-0.62.0 (c (n "raui-material") (v "0.62.0") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zq4yyc7sc8i47yb7qwqm8vb438pc1qg2j68qjyaxi5vcr83734a")))

(define-public crate-raui-material-0.62.1 (c (n "raui-material") (v "0.62.1") (d (list (d (n "raui-core") (r "^0.62") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j4z3ai6b6d4l0kz3khkkjvkl3x9f5g8lnh8r23179f9f7h3jl3x")))

(define-public crate-raui-material-0.63.0 (c (n "raui-material") (v "0.63.0") (d (list (d (n "raui-core") (r "^0.63") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1li66yl5dna3iyiz0sbk9y9hq0qild6x1lz9qr2vf367arl1mc6b")))

