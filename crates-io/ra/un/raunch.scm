(define-module (crates-io ra un raunch) #:use-module (crates-io))

(define-public crate-raunch-1.0.0 (c (n "raunch") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0zrin15n9ljg8a54bivhwiz96fawh2mng9031l5js76xfbfxiiw6")))

(define-public crate-raunch-1.0.1 (c (n "raunch") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "1ia9mfxpsvpdr0dzcr1kb6q911nmyw26x0jwbz2ikm6ci5kj4rqs")))

