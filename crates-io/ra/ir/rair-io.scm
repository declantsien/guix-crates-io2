(define-module (crates-io ra ir rair-io) #:use-module (crates-io))

(define-public crate-rair-io-0.1.0 (c (n "rair-io") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "rtrees") (r "^0.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yqlchf68qh09l4y2nrvdfanl2a3qvc99vk3jh892q4kmh1xjr31")))

