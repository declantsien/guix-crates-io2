(define-module (crates-io ra ir rair) #:use-module (crates-io))

(define-public crate-rair-0.1.0 (c (n "rair") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rair-io") (r "^0.1") (d #t) (k 0)) (d (n "rcmd") (r "^0.1") (d #t) (k 0)) (d (n "rcore") (r "^0.1") (d #t) (k 0)) (d (n "rtrees") (r "^0.1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1b5hqy7wlrhxb5skd1mj6m63y9k4vq2j7d4rkvxqxz2jh5ryk6ib")))

