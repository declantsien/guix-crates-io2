(define-module (crates-io ra ir rair-gui) #:use-module (crates-io))

(define-public crate-rair-gui-0.1.0 (c (n "rair-gui") (v "0.1.0") (d (list (d (n "app_dirs") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "rair-io") (r "^0.1.0") (d #t) (k 0)) (d (n "rcore") (r "^0.1.0") (d #t) (k 0)) (d (n "rgui") (r "^0.1.0") (d #t) (k 0)) (d (n "rtrees") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1nzdgy1c11qyrnqg0y3ph2qy6avwqdqlyha4rgfbwhlmqin9hhxc")))

