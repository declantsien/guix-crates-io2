(define-module (crates-io ra d1 rad1o-l0dable) #:use-module (crates-io))

(define-public crate-rad1o-l0dable-0.0.0 (c (n "rad1o-l0dable") (v "0.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "lpc43xx") (r "^0.1.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)))) (h "0s5sa1q385blw4h9nf8x38vd03mb2630cj14qxddkzmxnfq1ndp4")))

