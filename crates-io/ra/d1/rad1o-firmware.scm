(define-module (crates-io ra d1 rad1o-firmware) #:use-module (crates-io))

(define-public crate-rad1o-firmware-0.0.0 (c (n "rad1o-firmware") (v "0.0.0") (d (list (d (n "core_io") (r "^0.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (f (quote ("device"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "fatfs") (r "^0.2.0") (k 0)) (d (n "lpc43xx") (r "^0.1.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "lpc43xx-hal") (r "^0.0.0") (d #t) (k 0)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "vga-framebuffer") (r "^0.2.1") (d #t) (k 0)))) (h "1sbi8qwiyxsfgz4xpgx029hi54d06r4nvdg8mxwvfl0syky8s1d4")))

