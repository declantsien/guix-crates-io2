(define-module (crates-io ra nt rantz_random) #:use-module (crates-io))

(define-public crate-rantz_random-0.3.6 (c (n "rantz_random") (v "0.3.6") (d (list (d (n "bevy") (r "^0.13.2") (o #t) (k 0)) (d (n "fastrand") (r "^2.1.0") (d #t) (k 0)) (d (n "rantz_spatial2d") (r "^0.3.6") (o #t) (d #t) (k 0)))) (h "0f5b1c11ry6y57g1sl0272agpnphnd78yij3qqhzh40y95l598cd") (f (quote (("spatial2d" "rantz_spatial2d") ("default" "spatial2d" "bevy") ("bevy" "bevy/bevy_render"))))))

