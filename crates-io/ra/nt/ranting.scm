(define-module (crates-io ra nt ranting) #:use-module (crates-io))

(define-public crate-ranting-0.1.0 (c (n "ranting") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "in_definite") (r "^0.2") (d #t) (k 0)) (d (n "ranting_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0scqqpdgyymfwnwx7h9p41mcc7lbm7ym3x0zsiyhv9kz5f5ddggv")))

(define-public crate-ranting-0.1.2 (c (n "ranting") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (o #t) (d #t) (k 0)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "in_definite") (r "^0.2") (d #t) (k 0)) (d (n "ranting_derive") (r "=0.1.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1ww6sj8hmgfwblhgfgjl53r3kd6l8hjqgwd4ldqwhs6vm60mzlwi") (f (quote (("inflector" "Inflector") ("default"))))))

(define-public crate-ranting-0.2.0 (c (n "ranting") (v "0.2.0") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "in_definite") (r "^0.2") (d #t) (k 0)) (d (n "ranting_derive") (r "=0.2.0") (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0yzzh914b611m6kxn1b2r34clkm2wagfs3bxrzy7jx96dcirsl1m") (f (quote (("default" "ranting_derive/default") ("debug" "ranting_derive/debug"))))))

(define-public crate-ranting-0.2.1 (c (n "ranting") (v "0.2.1") (d (list (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "english-numbers") (r "^0.3") (d #t) (k 0)) (d (n "in_definite") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ranting_derive") (r "=0.2.1") (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0ga8nsfdfqfmgw2qn5rh1kkym1j4p5wrabi0xk5frqgnrc5dpdvp") (f (quote (("default" "ranting_derive/default") ("debug" "ranting_derive/debug"))))))

