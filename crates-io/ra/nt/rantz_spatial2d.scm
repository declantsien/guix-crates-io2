(define-module (crates-io ra nt rantz_spatial2d) #:use-module (crates-io))

(define-public crate-rantz_spatial2d-0.2.3 (c (n "rantz_spatial2d") (v "0.2.3") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "13vqs46y9ybfc91hm0qy18mf9l94l5ikg1l14xxymhw4f6nwis4g") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.0 (c (n "rantz_spatial2d") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "1zpc2ls8bna20kwj7knxjin7zm6ljpf59kxf25z58ypi1dwwxij8") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.1 (c (n "rantz_spatial2d") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "05j9h5s9bi11bzdcamcymhaiia622xbxc79vkrxnazh4qfwv0704") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render")))) (y #t)))

(define-public crate-rantz_spatial2d-0.3.2 (c (n "rantz_spatial2d") (v "0.3.2") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "1k1dh2f5l6bl5cx9zzvr6503k4c1rn6chf71i9cpy65im6z9wdsa") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.3 (c (n "rantz_spatial2d") (v "0.3.3") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "0kwdg3lawxdy7wx9zgp95i3jqb58argx0fx586vqwgqbszpkcyl1") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.4 (c (n "rantz_spatial2d") (v "0.3.4") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "1fqmxl9chi9vhamwkv2wirac5qybgrmsi7hqnda493w7acshmx03") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.5 (c (n "rantz_spatial2d") (v "0.3.5") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "1yhycpy7x57659m72mpa4l0f2yrw67wr28rcc74qh1vs1darml58") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

(define-public crate-rantz_spatial2d-0.3.6 (c (n "rantz_spatial2d") (v "0.3.6") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)))) (h "03pm1ig6qwyycdwp4i6gqkcmww8sl5fb3m3wi23ja60mhfmy5nn7") (f (quote (("default" "bevy_render") ("bevy_render" "bevy/bevy_render"))))))

