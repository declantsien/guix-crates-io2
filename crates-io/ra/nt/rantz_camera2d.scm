(define-module (crates-io ra nt rantz_camera2d) #:use-module (crates-io))

(define-public crate-rantz_camera2d-0.3.3 (c (n "rantz_camera2d") (v "0.3.3") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "rantz_spatial2d") (r "^0.3.3") (d #t) (k 0)))) (h "1g4grnd8678nbzf4qbyx6qrl00im0ncrb5gipklqacach2y39n6f") (f (quote (("default" "bevy_render" "bevy_core_pipeline") ("bevy_render" "bevy/bevy_render") ("bevy_core_pipeline" "bevy/bevy_core_pipeline"))))))

(define-public crate-rantz_camera2d-0.3.4 (c (n "rantz_camera2d") (v "0.3.4") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "rantz_spatial2d") (r "^0.3.4") (d #t) (k 0)))) (h "0h43jri3d6y0d5xbry5rbicsxiba8zqnmhmv8gf09isynsqkb8as") (f (quote (("default" "bevy_render" "bevy_core_pipeline") ("bevy_render" "bevy/bevy_render") ("bevy_core_pipeline" "bevy/bevy_core_pipeline"))))))

(define-public crate-rantz_camera2d-0.3.5 (c (n "rantz_camera2d") (v "0.3.5") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "rantz_spatial2d") (r "^0.3.5") (d #t) (k 0)))) (h "1jblx0wdgpz71vsg52fvca53f56d0s7pk8qfdyl6wi9d4m191a15") (f (quote (("default" "bevy_render" "bevy_core_pipeline") ("bevy_render" "bevy/bevy_render") ("bevy_core_pipeline" "bevy/bevy_core_pipeline"))))))

(define-public crate-rantz_camera2d-0.3.6 (c (n "rantz_camera2d") (v "0.3.6") (d (list (d (n "bevy") (r "^0.13.2") (k 0)) (d (n "bevy") (r "^0.13.2") (d #t) (k 2)) (d (n "rantz_spatial2d") (r "^0.3.6") (d #t) (k 0)))) (h "1wwida7fk8r473lsxfsr9d9dd7kchyk40fvf4sq00ilxp63k3114") (f (quote (("default" "bevy_render" "bevy_core_pipeline") ("bevy_render" "bevy/bevy_render") ("bevy_core_pipeline" "bevy/bevy_core_pipeline"))))))

