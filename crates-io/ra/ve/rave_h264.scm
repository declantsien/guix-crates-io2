(define-module (crates-io ra ve rave_h264) #:use-module (crates-io))

(define-public crate-rave_h264-0.1.1 (c (n "rave_h264") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "openh264") (r "^0.4.1") (d #t) (k 0)) (d (n "rave_types") (r "^0.1.1") (d #t) (k 0)))) (h "0yw668ia5shax59sm6idnii4i1g2vgm2m4isgfhdcjcci37qrh0c")))

