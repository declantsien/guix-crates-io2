(define-module (crates-io ra ve raven) #:use-module (crates-io))

(define-public crate-raven-0.1.0 (c (n "raven") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "0giqrc6ica3ba3a2kx62r385rll2pirz2s6518zrnz4049j587wy")))

(define-public crate-raven-0.1.1 (c (n "raven") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "19nqpc5y2wshm5z42mwhhm7lvgrl8v5xrf8ll4a87agcgs40b37s")))

(define-public crate-raven-0.2.0 (c (n "raven") (v "0.2.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1dpmapr2hk0rp7dhbbhzip6x1nfks8b0kq905k70pdz40yxbz47q")))

(define-public crate-raven-0.2.1 (c (n "raven") (v "0.2.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "10q15alcad94dvgf6pzz6i67iv3b8pfi21smkq476kybnb66gir6")))

