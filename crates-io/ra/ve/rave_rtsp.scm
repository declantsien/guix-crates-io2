(define-module (crates-io ra ve rave_rtsp) #:use-module (crates-io))

(define-public crate-rave_rtsp-0.1.1 (c (n "rave_rtsp") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (k 0)))) (h "1qv8476cbdkkhjkhq2ni7hiwg34644g4dbkjmv5gcs54s8py3r3k")))

