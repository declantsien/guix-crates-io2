(define-module (crates-io ra ve raventhemer) #:use-module (crates-io))

(define-public crate-raventhemer-1.1.0 (c (n "raventhemer") (v "1.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1m2h5qx6r5wcq7zjibxmwqcsbksd70lajgd790g36309pw974jnl")))

(define-public crate-raventhemer-1.2.0 (c (n "raventhemer") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1ny8g3rjclgy74ciqf8xwkzpafvrj0w9ihzjl0n63vplrbdlmvq2")))

(define-public crate-raventhemer-1.3.0 (c (n "raventhemer") (v "1.3.0") (d (list (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1dvysxjha264sk6qk19v4lh6ixkss4fgjm2ikcp115971hqf5p25")))

(define-public crate-raventhemer-1.3.1 (c (n "raventhemer") (v "1.3.1") (d (list (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1jvl3ifgj8053m3zvh5akknj90xsnbkd943xp05bxn39zafd38xn")))

(define-public crate-raventhemer-1.4.0 (c (n "raventhemer") (v "1.4.0") (d (list (d (n "hyper") (r "^0.10.13") (d #t) (k 0)) (d (n "multipart") (r "^0.15.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1d93gh448d86ml2z98bmxkcd835fcfv0brcx998zknbq8akrqhv1")))

(define-public crate-raventhemer-1.4.1 (c (n "raventhemer") (v "1.4.1") (d (list (d (n "hyper") (r "^0.10.13") (d #t) (k 0)) (d (n "multipart") (r "^0.15.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b764wimpnnldqzii5fap1x4l8j9p1qdn24323knqpx0r136vjgr")))

(define-public crate-raventhemer-1.4.3 (c (n "raventhemer") (v "1.4.3") (d (list (d (n "multipart") (r "^0.15.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11clsrm4bsp247jljzla3i2hf6azy5zqbnyy121jay4an9l3c85c")))

(define-public crate-raventhemer-1.4.4 (c (n "raventhemer") (v "1.4.4") (d (list (d (n "multipart") (r "^0.15.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jvkb2sqilaph36l76di1mxyrab7xpwffpma4r458qrcmhrw3fzp")))

(define-public crate-raventhemer-1.5.0 (c (n "raventhemer") (v "1.5.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "multipart") (r "^0.15.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1irshai06q3ay9dqqw4v867fp7932rkszggr33bhwgrk2shqh1n4")))

(define-public crate-raventhemer-1.5.1 (c (n "raventhemer") (v "1.5.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "ravenlib") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0sbijgiaki93rpvk6q8gwz3lg801j4nv99c1wrrs98ig1aly8db7")))

(define-public crate-raventhemer-1.5.2 (c (n "raventhemer") (v "1.5.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "ravenlib") (r "^1.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1734ynih01rzw7999r0nilbi8qhgjr51wmm31wzy3dc60qzxly2i")))

(define-public crate-raventhemer-1.5.3 (c (n "raventhemer") (v "1.5.3") (d (list (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ravenlib") (r "^1.2.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "13f6vf4f7i3w4idqc8xhdnakflg60nc4339zvm9cmrwg5g3n1wsa")))

