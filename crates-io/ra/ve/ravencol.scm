(define-module (crates-io ra ve ravencol) #:use-module (crates-io))

(define-public crate-ravencol-0.1.0 (c (n "ravencol") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "1c5vzns653d5fq2slv8cq6lgpkf7p2c16hprvs1ddgmzs6jgd1xl")))

(define-public crate-ravencol-0.1.1 (c (n "ravencol") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "0dmnw4szb1bswv7r2k6l3w1lvx0gcm3k0mkznnc73l9c821ykshx")))

(define-public crate-ravencol-0.1.2 (c (n "ravencol") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "09gs67djq16h404q3j8lvkymcy8rc10bk4vxinmn4z1vb25aqd45")))

(define-public crate-ravencol-0.1.3 (c (n "ravencol") (v "0.1.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "0j0hc59s2irra0r9kwys1lhzkjf7ly535636rdig7szzrgrd12f2")))

(define-public crate-ravencol-0.1.4 (c (n "ravencol") (v "0.1.4") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 0)))) (h "14pzsqfxahxcjn4g7k3hfa0f4pmk285csixig8yys3kavhyrqllh")))

