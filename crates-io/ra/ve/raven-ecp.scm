(define-module (crates-io ra ve raven-ecp) #:use-module (crates-io))

(define-public crate-raven-ecp-0.1.0 (c (n "raven-ecp") (v "0.1.0") (d (list (d (n "fastly") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-human-readable"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ykhma375m0jbjgyiw3kqc05pah8asn5snfa0vf7fab0iczgh0ix")))

(define-public crate-raven-ecp-0.1.1 (c (n "raven-ecp") (v "0.1.1") (d (list (d (n "fastly") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("serde-human-readable"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1acjr9yg658yi1k8hvi8611di13bs02fin97m2wjsa2qa7acvxa4")))

