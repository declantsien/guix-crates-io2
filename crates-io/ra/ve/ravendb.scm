(define-module (crates-io ra ve ravendb) #:use-module (crates-io))

(define-public crate-ravendb-0.2.16 (c (n "ravendb") (v "0.2.16") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15szavak96v7szmhgs2jksk5p4zyr3gp1lqrgcipmc3pksl4ihj1")))

