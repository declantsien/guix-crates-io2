(define-module (crates-io ra ve rave_types) #:use-module (crates-io))

(define-public crate-rave_types-0.1.0 (c (n "rave_types") (v "0.1.0") (h "13r3n05d8rdwk1y2ixpv5kp3cihw8gqjqvfarrq1rsfdbqk61kwa")))

(define-public crate-rave_types-0.1.1 (c (n "rave_types") (v "0.1.1") (h "1480y6gg2s6g5dwq48v0mlvgnz8xbajjc2ybgkvs145rm7crijvy")))

