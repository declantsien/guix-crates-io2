(define-module (crates-io ra #{2a}# ra2a1) #:use-module (crates-io))

(define-public crate-ra2a1-0.2.0 (c (n "ra2a1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0r8g489b8f8kjrdvld1hj1m5nljnswxgan43326f87afn9ccv6sf") (f (quote (("rt" "cortex-m-rt/device"))))))

