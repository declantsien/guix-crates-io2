(define-module (crates-io ra lt ralte32) #:use-module (crates-io))

(define-public crate-ralte32-0.1.0 (c (n "ralte32") (v "0.1.0") (h "15a5ydpmqlnwkkga329yby95s0fmmq976l01vbjjsh3n8ch3hiqv")))

(define-public crate-ralte32-0.1.1 (c (n "ralte32") (v "0.1.1") (h "0d5avwwwx883f27bqagakwsbbrq9r1phshyrqghwdhrhb008bxq9")))

(define-public crate-ralte32-0.1.2 (c (n "ralte32") (v "0.1.2") (h "00vil0v8fs10lviy8cipg86967a9262spwkvc90labzi9jgr0r6a")))

(define-public crate-ralte32-0.1.3 (c (n "ralte32") (v "0.1.3") (h "1ri90bnrrkkszxj6wd3sqmzkfhqvl38wjdz1kh6ibz4qd3czhq5m")))

