(define-module (crates-io ra nn rann) #:use-module (crates-io))

(define-public crate-rann-0.1.0 (c (n "rann") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "12lxsh8vrq6j50f0jxhdq27r0p3s8a3gdn4hj7rgq3mh4djvx952")))

(define-public crate-rann-0.1.1 (c (n "rann") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1cl0pjgyyaljn9jkfki66d74900h032xdw2w50pqbldnqxa4xxni")))

(define-public crate-rann-0.1.2 (c (n "rann") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0hgrqk1qk0n7vgqwajkbl3qdhf7cigj1199pn8kqm8x5p4a1fpq5")))

