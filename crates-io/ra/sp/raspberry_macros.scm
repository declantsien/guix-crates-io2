(define-module (crates-io ra sp raspberry_macros) #:use-module (crates-io))

(define-public crate-raspberry_macros-0.1.0 (c (n "raspberry_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03i5x9qxsns9agg725j5gyyfg64hs6spw6hy9pw6qg0dik9yjcwf")))

