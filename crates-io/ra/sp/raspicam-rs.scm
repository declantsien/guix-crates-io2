(define-module (crates-io ra sp raspicam-rs) #:use-module (crates-io))

(define-public crate-raspicam-rs-0.1.0 (c (n "raspicam-rs") (v "0.1.0") (d (list (d (n "autocxx") (r "^0.22.4") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.22.4") (d #t) (k 1)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.76") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (o #t) (d #t) (k 0)))) (h "1ayp0fjp0sxnmz6k9z9df207aymimpzrxszq745vadrw4djqf8mh") (l "raspicam-rs") (s 2) (e (quote (("opencv" "dep:opencv"))))))

(define-public crate-raspicam-rs-0.1.1 (c (n "raspicam-rs") (v "0.1.1") (d (list (d (n "autocxx") (r "^0.22.4") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.22.4") (d #t) (k 1)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.76") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (o #t) (d #t) (k 0)))) (h "1zsqm5bgblwkq8s9836jgdbiv67clb5wlmkb6jmqhsy4yr89jvsz") (l "raspicam-rs") (s 2) (e (quote (("opencv" "dep:opencv"))))))

(define-public crate-raspicam-rs-0.1.2 (c (n "raspicam-rs") (v "0.1.2") (d (list (d (n "autocxx") (r "^0.22.4") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.22.4") (d #t) (k 1)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.76") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0qkj82cfwalmj4ai6f648ifjhbrknzg5xqz7qnkm24bsnzcwa3ha") (l "raspicam-rs") (s 2) (e (quote (("opencv" "dep:opencv"))))))

(define-public crate-raspicam-rs-0.1.3 (c (n "raspicam-rs") (v "0.1.3") (d (list (d (n "autocxx") (r "^0.22.4") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.22.4") (d #t) (k 1)) (d (n "cxx") (r "^1.0.78") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.76") (d #t) (k 1)) (d (n "opencv") (r "^0.70.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1q7qljqpq7wi7yxqkzlzff6wx5sw8i93iy8jml1rn99fl70yz6vf") (l "raspicam-rs") (s 2) (e (quote (("opencv" "dep:opencv"))))))

