(define-module (crates-io ra sp rasp_mgr) #:use-module (crates-io))

(define-public crate-rasp_mgr-1.0.0 (c (n "rasp_mgr") (v "1.0.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "libmedium") (r "^0.5.5") (d #t) (k 0)) (d (n "mnt") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sys-info") (r "^0.9.0") (d #t) (k 0)) (d (n "tide") (r "^0.16.0") (d #t) (k 0)))) (h "08jbrafnx11hqaay3plksqpgbs3h3bqqwpxjs68sajb32vvsgmk9")))

