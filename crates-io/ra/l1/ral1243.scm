(define-module (crates-io ra l1 ral1243) #:use-module (crates-io))

(define-public crate-ral1243-1.0.0 (c (n "ral1243") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "z80emu") (r ">=0.9") (k 0)))) (h "0jfr8fy4s9y1if1d6wl2f0zi9xf90qink8z3jrr8c1yqflzpggpk") (f (quote (("std" "z80emu/std") ("default"))))))

(define-public crate-ral1243-2.0.0 (c (n "ral1243") (v "2.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "z80emu") (r ">=0.11") (k 0)))) (h "08hqhm0nff7wj6nz89skp39c44z7g99hm25al44ki3isx4bw87fy") (f (quote (("std" "z80emu/std") ("default"))))))

(define-public crate-ral1243-2.0.1 (c (n "ral1243") (v "2.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "z80emu") (r ">=0.11") (k 0)))) (h "1vjh2mcjyvb1fc5j5l37qq97w3jzwsvpllk541lkysz15mka028d") (f (quote (("std" "z80emu/std") ("default"))))))

