(define-module (crates-io ra wa rawarray) #:use-module (crates-io))

(define-public crate-rawarray-0.1.0 (c (n "rawarray") (v "0.1.0") (d (list (d (n "half") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "microbench") (r "^0.5.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "0336s23hbmliq9rhx4nlq6z6x3p3wv70sc7790da837jrdnfri9m")))

(define-public crate-rawarray-0.1.1 (c (n "rawarray") (v "0.1.1") (d (list (d (n "half") (r "^1.4.0") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.3") (d #t) (k 0)) (d (n "microbench") (r "^0.5.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "14ycl2jlbgqcpqdqr4vlzfsf6hqpws0xyil2sgvwmfz1c9ffc1xq")))

