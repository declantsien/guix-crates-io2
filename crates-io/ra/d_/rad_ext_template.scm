(define-module (crates-io ra d_ rad_ext_template) #:use-module (crates-io))

(define-public crate-rad_ext_template-0.1.0 (c (n "rad_ext_template") (v "0.1.0") (h "0ck4rkwfmfq4j60kf0fj3arghwx2mxv7ikz3nzxc2yiy6sddqjw1")))

(define-public crate-rad_ext_template-0.2.0 (c (n "rad_ext_template") (v "0.2.0") (h "19sqd2564affpfasphnakm5bclvb6y8nd3vv89yn3phw61dsqav2") (f (quote (("binary"))))))

(define-public crate-rad_ext_template-0.3.0 (c (n "rad_ext_template") (v "0.3.0") (h "12qx4lm7r7gm5svd41whppsmii7i7ycc6hh8zsh4n2hi753fk5h8") (f (quote (("binary"))))))

