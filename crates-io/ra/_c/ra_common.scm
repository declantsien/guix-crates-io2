(define-module (crates-io ra _c ra_common) #:use-module (crates-io))

(define-public crate-ra_common-0.0.2 (c (n "ra_common") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0jvy9s180a0qygjhm6dlns55pmydx8f7w9lvirz9zlq4ga1bg5hv")))

(define-public crate-ra_common-0.0.5 (c (n "ra_common") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04pfd7kqx1vnry0smbjnc8j8vgzd55q9lil3y5g1al7x64rs3rg3")))

(define-public crate-ra_common-0.0.6 (c (n "ra_common") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19g1bggm6qx24qh81w0m30lx5swqzrggk9lh8bwcd1x9066hwdqb")))

(define-public crate-ra_common-0.0.7 (c (n "ra_common") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0zr4ln03wvbrd72x9vsa2m5wm39i7k4i41r6afghdqd4sqx43nqx")))

(define-public crate-ra_common-0.0.8 (c (n "ra_common") (v "0.0.8") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "04z26z387znmhswn40c3mv59gfl3vijp8rrrxzwfqzr41yskjqir")))

(define-public crate-ra_common-0.0.9 (c (n "ra_common") (v "0.0.9") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0664vyg6jw04fcvcqc3kfgpl67mq7dbg2ix09azhg0yfgganx447")))

(define-public crate-ra_common-0.0.11 (c (n "ra_common") (v "0.0.11") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1xl84qpyxyz3rhv6avr30l4xvpjp97327xvksz53abh4d29f7nz6")))

(define-public crate-ra_common-0.0.12 (c (n "ra_common") (v "0.0.12") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "02kvw7n2hvq699y8z7d3kkkhsqq6dpn1yx90lnl57a0my6kvn8f4")))

(define-public crate-ra_common-0.0.13 (c (n "ra_common") (v "0.0.13") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1nw7x0c4ilr2v7hnkjks1fszbcq18khzcla91hh1nvl0d8nsmk2h")))

(define-public crate-ra_common-0.0.14 (c (n "ra_common") (v "0.0.14") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0plxr9n4b5h25fibs0vj7ai6xxiw6hax61dj61351kqzvlng8ias")))

(define-public crate-ra_common-0.0.15 (c (n "ra_common") (v "0.0.15") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1spp0q08bx1pq9dc7y88f9d6w4cc16hmwms6jrl2nk8k2va94hf7")))

(define-public crate-ra_common-0.0.16 (c (n "ra_common") (v "0.0.16") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17fc563h79705sgkvar0iajfyyh3spx69prgayngdn542xyh9ikm")))

(define-public crate-ra_common-0.0.17 (c (n "ra_common") (v "0.0.17") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1i2ys8ylk8g0zbkhnlzwkh4yci2464mzjflkdb1ns84gxk6plv02")))

(define-public crate-ra_common-0.0.18 (c (n "ra_common") (v "0.0.18") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0gd54zg5bh4dgmdammk5z3zly7h10vqywaz67fb20sy8z0zqyk2q")))

(define-public crate-ra_common-0.0.19 (c (n "ra_common") (v "0.0.19") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0i0pjsknvahmlknjgppmw2xyr20gwd8sk23nn93ifpfynnizjb09")))

(define-public crate-ra_common-0.0.21 (c (n "ra_common") (v "0.0.21") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "13g9g0inj4kwcxksf7sc3czbjyjy1w292rhdrriyk5nqmq1x4v5n")))

(define-public crate-ra_common-0.0.22 (c (n "ra_common") (v "0.0.22") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1d1xk85b6rkm5pvf1xmf44gf8ykv9x6p9xrlppal0qxzlscddv62")))

(define-public crate-ra_common-0.0.23 (c (n "ra_common") (v "0.0.23") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1vj0fhrkkva4kh7wd9ymhxwfgryrh70g23vqgdbnykg9lwpj6i6f")))

(define-public crate-ra_common-0.0.24 (c (n "ra_common") (v "0.0.24") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0aci2cj9splavrqh65xdfhnd4jkzjv68sps95pddfcbhp1dv8kww")))

(define-public crate-ra_common-0.0.25 (c (n "ra_common") (v "0.0.25") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1dp79k37mkxyyfpi5iw4xr7igqzfln23hg3j4ppgknzwf2d0lmag")))

(define-public crate-ra_common-0.0.26 (c (n "ra_common") (v "0.0.26") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1x7jq01jfh5q5gf2v9ppa230n84b3j4y6i68x535xlj9jdgqxk57")))

(define-public crate-ra_common-0.0.27 (c (n "ra_common") (v "0.0.27") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "01ysmnzw94jxc3ci1zrwmc5lh2naal2yza23f2w8abjzxvy2m3da")))

(define-public crate-ra_common-0.0.28 (c (n "ra_common") (v "0.0.28") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0r5wshvps6wy4jnbpg0skwansqwz2zv2kz2wjhap6ij172i3vw1q")))

(define-public crate-ra_common-0.0.29 (c (n "ra_common") (v "0.0.29") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1s6ci2n8570nn1k2x0lw1mwc8j1rh2jf6xp0263wpwlv1ji58bcr")))

(define-public crate-ra_common-0.0.30 (c (n "ra_common") (v "0.0.30") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16py7k61f0lv4k420djxl23hqbnyykaqp5q1l9vmm3jmxa9dckwk")))

(define-public crate-ra_common-0.0.31 (c (n "ra_common") (v "0.0.31") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "008s904062gn5avpjd5y0dpcfs03dxpicqc3r30n9mfrjgqx2v49")))

(define-public crate-ra_common-0.0.32 (c (n "ra_common") (v "0.0.32") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0804494f4qryg2dlig4r0r84wh3fbcwi97h5h23syp63ski3yq8f")))

(define-public crate-ra_common-0.0.33 (c (n "ra_common") (v "0.0.33") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1x052gvfxcmca4hm0srijn0vwj3vll2whrywx0pa59z4cajw14vs")))

(define-public crate-ra_common-0.0.34 (c (n "ra_common") (v "0.0.34") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1y69bi82p1lkaf8z4pnrwia7bmay3mkd58xpjk5f8gb97w79h0hk")))

(define-public crate-ra_common-0.0.35 (c (n "ra_common") (v "0.0.35") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "10daxx131kshi0aazpphzqb4f2ds8ls1zyc9zymgknarmxkm9vdf")))

(define-public crate-ra_common-0.0.36 (c (n "ra_common") (v "0.0.36") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0na83abvlckrnjy9lish3md0lafp74p09n4m4cwk9cmcv6ypgh53")))

(define-public crate-ra_common-0.0.38 (c (n "ra_common") (v "0.0.38") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0n7c5w4xpf8gb65hpfghqna2qm5py69kgn8136rjirvvn604gyg3")))

(define-public crate-ra_common-0.0.39 (c (n "ra_common") (v "0.0.39") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0754bbwbfs6w4cj2kmjjwpdx9cfwyqb7ghldf40rb11lrzlri7zq")))

(define-public crate-ra_common-0.0.40 (c (n "ra_common") (v "0.0.40") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "10pa5g4lf74cnmiq3cbf2w9c4gv7s3c1rkzxsancl44zjkmjmk6k")))

(define-public crate-ra_common-0.0.41 (c (n "ra_common") (v "0.0.41") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0piprp27w5wl1lwxwdcv4zqk65bh7nn157b0d13l9l3389ajwx8w")))

(define-public crate-ra_common-0.0.43 (c (n "ra_common") (v "0.0.43") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1n22x4lxgks87xns8q4vhb0i4q594npjbnzj26fbkj8mx559nncg")))

(define-public crate-ra_common-0.0.44 (c (n "ra_common") (v "0.0.44") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "122hs0j5ky5mpr9y93gwa53v9h9njsd1h22ynbq9djfppw2h0wjs")))

(define-public crate-ra_common-0.1.0 (c (n "ra_common") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "16dljlxki36gpxlg3yiy8f5chh82dysczalpmsdgpw500kqx498p")))

(define-public crate-ra_common-0.1.1 (c (n "ra_common") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0knyf8fpyin3lb0hzlbl2k5nq1anf3zschvd0s0glpxvc0xz50al")))

(define-public crate-ra_common-0.1.2 (c (n "ra_common") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0ckqhkcr90l2d34yzk4nggc5n6nysjc956w0h9nw9rlgp1q43ria")))

(define-public crate-ra_common-0.1.3 (c (n "ra_common") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "12k9w65xq1823qizf034xs1sl5vib2dmiblna3q4gp9bk0pai7l5")))

(define-public crate-ra_common-0.1.4 (c (n "ra_common") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "140nws8x8akhgl4f493w1xf5r2x1dh48ws2kqamfx05yskjrvyk3")))

(define-public crate-ra_common-0.1.5 (c (n "ra_common") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "028cxnh5wgm1qqbyc428f8ddj3py8mmxbgvjwy6jc8mm2a59zsc7")))

