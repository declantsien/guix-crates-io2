(define-module (crates-io ra s_ ras_auth) #:use-module (crates-io))

(define-public crate-ras_auth-0.1.0 (c (n "ras_auth") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "ras_service") (r "^0.2.0") (f (quote ("Authentication"))) (d #t) (k 0)) (d (n "ring") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.5.0") (f (quote ("runtime-tokio-native-tls" "sqlite"))) (d #t) (k 0)))) (h "0jniwafjmq4jck8d417wwi6hb197xfbl8nbcy443lc0r0crm1zin")))

