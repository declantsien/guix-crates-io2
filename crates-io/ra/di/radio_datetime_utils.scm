(define-module (crates-io ra di radio_datetime_utils) #:use-module (crates-io))

(define-public crate-radio_datetime_utils-0.1.0 (c (n "radio_datetime_utils") (v "0.1.0") (d (list (d (n "heapless") (r "^0.7.13") (d #t) (k 0)))) (h "0p2f37d6xy5irrpq0fwl5gyr7khw0f5hg2i9dgmbkh05clip94wk")))

(define-public crate-radio_datetime_utils-0.2.0 (c (n "radio_datetime_utils") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.13") (d #t) (k 0)))) (h "010zxi48vawv7ln2a8a8bnab44lk2wpggnqb44avk631mrqj2lnf")))

(define-public crate-radio_datetime_utils-0.2.1 (c (n "radio_datetime_utils") (v "0.2.1") (d (list (d (n "heapless") (r "^0.7.14") (d #t) (k 0)))) (h "1mfdjza737vja00v129r0l9cr8b7fxwyyskfhzs8c7741cika8nv")))

(define-public crate-radio_datetime_utils-0.3.0 (c (n "radio_datetime_utils") (v "0.3.0") (d (list (d (n "heapless") (r "^0.7.14") (d #t) (k 0)))) (h "02jx63n1fs1ygc1fzyrq67hag8r09v1mfnr6ldgg5jm57a6c8y8r")))

(define-public crate-radio_datetime_utils-0.4.0 (c (n "radio_datetime_utils") (v "0.4.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 0)))) (h "0bjwva3ba8wz1hkg713g0fav5b4kpiy6k89w0qv53mw4cnz0cdgc")))

(define-public crate-radio_datetime_utils-0.4.1 (c (n "radio_datetime_utils") (v "0.4.1") (h "1s0gwjp55c6fapm69aryac5d72a1q7s1d9sg4rwsmklm2fa6492a")))

(define-public crate-radio_datetime_utils-0.4.2 (c (n "radio_datetime_utils") (v "0.4.2") (h "1mljqg5plvnqmdmkmwmsz5lf67fc2bcrg1w4l6x8phhv87av35vv")))

(define-public crate-radio_datetime_utils-0.5.0 (c (n "radio_datetime_utils") (v "0.5.0") (h "0vjxckmcw55a8cy9p7ypsx160gjpk25xa1wvjxr91n6w1swx7kcm")))

(define-public crate-radio_datetime_utils-1.0.0 (c (n "radio_datetime_utils") (v "1.0.0") (h "1zxqnb8q6dab3spcyr6w7m95aiwjsfzq8ljz0kzcdi2jljv5m86x")))

