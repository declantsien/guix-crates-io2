(define-module (crates-io ra di radio_datetime_analyzer) #:use-module (crates-io))

(define-public crate-radio_datetime_analyzer-0.1.0 (c (n "radio_datetime_analyzer") (v "0.1.0") (d (list (d (n "dcf77_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "npl_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "04az8dshj6f66q3j67lm6s2mar5qbfdabizpzp14g84n9q0z0jd4")))

(define-public crate-radio_datetime_analyzer-0.1.1 (c (n "radio_datetime_analyzer") (v "0.1.1") (d (list (d (n "dcf77_utils") (r "^0.4.0") (d #t) (k 0)) (d (n "npl_utils") (r "^0.2.1") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "0r1z7p9682cbcrxwh5xji0c9lvmk3vid8dwfv3pbarf6x4vr3a7w")))

(define-public crate-radio_datetime_analyzer-0.1.2 (c (n "radio_datetime_analyzer") (v "0.1.2") (d (list (d (n "dcf77_utils") (r "^0.5.0") (d #t) (k 0)) (d (n "npl_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "0vmf6bvclcfx82gj0dxc5645rf9ik41r2l4s0cldmsl2sh4y65f9")))

(define-public crate-radio_datetime_analyzer-0.1.3 (c (n "radio_datetime_analyzer") (v "0.1.3") (d (list (d (n "dcf77_utils") (r "^0.5.0") (d #t) (k 0)) (d (n "msf60_utils") (r "^0.3.2") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "10wgbwql505p953wilav70si1i26wpv5kc7bgajhrlz8xbyrmgrd")))

(define-public crate-radio_datetime_analyzer-0.1.4 (c (n "radio_datetime_analyzer") (v "0.1.4") (d (list (d (n "dcf77_utils") (r "^0.6") (d #t) (k 0)) (d (n "msf60_utils") (r "^0.3") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.5") (d #t) (k 0)))) (h "0siwz1qlbmsgik6c20fpibxs7rw70l819b6qn626x5kn1ziqmqql")))

(define-public crate-radio_datetime_analyzer-0.1.5 (c (n "radio_datetime_analyzer") (v "0.1.5") (d (list (d (n "dcf77_utils") (r "^0.6") (d #t) (k 0)) (d (n "msf60_utils") (r "^0.4") (d #t) (k 0)) (d (n "radio_datetime_utils") (r "^0.5") (d #t) (k 0)))) (h "04fnmq8z3pjq770gk4qz1nn4i2j7psw73kb5h8r9jk4sss78rm0a")))

