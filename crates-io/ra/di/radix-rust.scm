(define-module (crates-io ra di radix-rust) #:use-module (crates-io))

(define-public crate-radix-rust-1.2.0-dev (c (n "radix-rust") (v "1.2.0-dev") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (k 0)))) (h "17vmfqxxbs3p0a3sgq16nghvix6ib9d4r3gbp0k5zzzbyj2ss2yh") (f (quote (("std" "indexmap/std") ("serde" "serde/derive" "indexmap/serde") ("fuzzing" "indexmap/arbitrary") ("default" "std") ("alloc" "hashbrown"))))))

(define-public crate-radix-rust-1.2.0 (c (n "radix-rust") (v "1.2.0") (d (list (d (n "hashbrown") (r "^0.13.2") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.2.5") (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (k 0)))) (h "0r4w20k47k0b5as35ys76jpznhyb1m5ca9854wyklbbwpmzgs430") (f (quote (("std" "indexmap/std") ("serde" "serde/derive" "indexmap/serde") ("fuzzing" "indexmap/arbitrary") ("default" "std") ("alloc" "hashbrown"))))))

