(define-module (crates-io ra di radix-native-sdk) #:use-module (crates-io))

(define-public crate-radix-native-sdk-1.2.0-dev (c (n "radix-native-sdk") (v "1.2.0-dev") (d (list (d (n "radix-common") (r "^1.2.0-dev") (k 0)) (d (n "radix-engine-interface") (r "^1.2.0-dev") (k 0)) (d (n "radix-rust") (r "^1.2.0-dev") (k 0)) (d (n "sbor") (r "^1.2.0-dev") (k 0)))) (h "0v3q8fk5542pz06diy02mf22d78cfqwvlr0naw5qk7andw8mqpi6") (f (quote (("std" "sbor/std" "radix-engine-interface/std" "radix-rust/std" "radix-common/std") ("default" "std") ("alloc" "sbor/alloc" "radix-engine-interface/alloc" "radix-rust/alloc" "radix-common/alloc"))))))

(define-public crate-radix-native-sdk-1.2.0 (c (n "radix-native-sdk") (v "1.2.0") (d (list (d (n "radix-common") (r "^1.2.0") (k 0)) (d (n "radix-engine-interface") (r "^1.2.0") (k 0)) (d (n "radix-rust") (r "^1.2.0") (k 0)) (d (n "sbor") (r "^1.2.0") (k 0)))) (h "0j5c467diixi82cxjzdslj82fa1lxqzknvi325z7ag14p9b6yl7h") (f (quote (("std" "sbor/std" "radix-engine-interface/std" "radix-rust/std" "radix-common/std") ("default" "std") ("alloc" "sbor/alloc" "radix-engine-interface/alloc" "radix-rust/alloc" "radix-common/alloc"))))))

