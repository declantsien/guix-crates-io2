(define-module (crates-io ra di radiko-sdk) #:use-module (crates-io))

(define-public crate-radiko-sdk-0.1.0 (c (n "radiko-sdk") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "m3u8-rs") (r "^5.0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04znbf7yiqkyfdxqsnggxd7qv46b6p98p5wcrrw90l8achbb3nbp")))

