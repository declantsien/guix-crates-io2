(define-module (crates-io ra di radixmap) #:use-module (crates-io))

(define-public crate-radixmap-0.0.0 (c (n "radixmap") (v "0.0.0") (h "1l7rdyk5fkggadz7wdg9ghz5s3l30x8b8pfwrds0rd6gc2hhcqg9") (y #t)))

(define-public crate-radixmap-0.1.0 (c (n "radixmap") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "0syy3f2jwgk2dflicpis140b6p41plgdzbddf8yg4qkpi58kyccf")))

(define-public crate-radixmap-0.2.0 (c (n "radixmap") (v "0.2.0") (d (list (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1xvmvjvbb742f384ywvcnhvc78186ky3011gvbd3375942m5x0g6")))

(define-public crate-radixmap-0.2.1 (c (n "radixmap") (v "0.2.1") (d (list (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "004apfymf6ka3kyv8djfy79r3dsny27v24p64bh5zbaiqy5n4321")))

(define-public crate-radixmap-0.2.2 (c (n "radixmap") (v "0.2.2") (d (list (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "00q32l8qh9m0wq66b2kk0q3a9afizj9k7xp6mc9y47mjg0ny8hrp")))

(define-public crate-radixmap-0.2.3 (c (n "radixmap") (v "0.2.3") (d (list (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.7") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.8") (d #t) (k 0)))) (h "1ic4h65h2gcyjb7jh8pc8gb8fab1hmqf0ra8iavgdhjnjvszlnrk")))

