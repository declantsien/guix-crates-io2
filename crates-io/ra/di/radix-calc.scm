(define-module (crates-io ra di radix-calc) #:use-module (crates-io))

(define-public crate-radix-calc-1.0.0 (c (n "radix-calc") (v "1.0.0") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1zjwzdglq1x6qcdh0ldmygyz7bbpgyhlflhvlk8zzcs4zfaa3v4i")))

(define-public crate-radix-calc-1.0.1 (c (n "radix-calc") (v "1.0.1") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0g2wl2fdbkyfl59gaplnpbi4rxdg8ansvjlmh95j4k4q5jc82r95")))

(define-public crate-radix-calc-1.0.2 (c (n "radix-calc") (v "1.0.2") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0dmzfzf81l4k2ww0i8smcqq3ngpsrg8abwms4s74ms8i42mv9v8w")))

(define-public crate-radix-calc-1.0.3 (c (n "radix-calc") (v "1.0.3") (d (list (d (n "docopt") (r "^0.7") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0ir8bvz665l3w3q6zsl3qgk8fbl2hsf399y5i6fk984sj1k2qk1p")))

