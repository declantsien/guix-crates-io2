(define-module (crates-io ra di radient) #:use-module (crates-io))

(define-public crate-radient-0.1.0 (c (n "radient") (v "0.1.0") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "peroxide") (r "^0.34") (d #t) (k 0)))) (h "162dv9ssi6cm832wvw18adr2a4maz7krrbyxj1r9289i27sp8fa8")))

(define-public crate-radient-0.2.0 (c (n "radient") (v "0.2.0") (d (list (d (n "casey") (r "^0.4") (d #t) (k 0)) (d (n "peroxide") (r "^0.34") (d #t) (k 2)) (d (n "peroxide-num") (r "^0.1") (d #t) (k 0)))) (h "0ndjpk9hiwmsmwnn1g0kx069q4r8i7vv468nj6a3jf0a130zvhwq")))

