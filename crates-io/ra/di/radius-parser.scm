(define-module (crates-io ra di radius-parser) #:use-module (crates-io))

(define-public crate-radius-parser-0.1.0 (c (n "radius-parser") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0xc6jx5pbhi3whjymzd62lxfsy7g07fqff768hh54frh703921vl")))

(define-public crate-radius-parser-0.1.1 (c (n "radius-parser") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0wgvc6gpjsill6v73k3872wp74idc1mik8wjwbh1nf93hgldfjan")))

(define-public crate-radius-parser-0.2.0 (c (n "radius-parser") (v "0.2.0") (d (list (d (n "nom") (r "^4.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1j0ibq2narv6n13g3zgaxixkxjjys63ijvxpcaa8bv8ywjcf4vdj")))

(define-public crate-radius-parser-0.3.0 (c (n "radius-parser") (v "0.3.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "0vp7x0gjkh0rav0b61rml9ymna9cp25j7nhg9g03khbabrgp7y6x")))

(define-public crate-radius-parser-0.4.0 (c (n "radius-parser") (v "0.4.0") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "15q0n5fjhmpv57a68vlx82jca4clabl040w9xgr3szl5pr2maf1x")))

(define-public crate-radius-parser-0.4.1 (c (n "radius-parser") (v "0.4.1") (d (list (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "1j430ck83nqrf8aa20y56jxricbqbiap47l5l3fpyzrawmmlfb5s")))

(define-public crate-radius-parser-0.5.0 (c (n "radius-parser") (v "0.5.0") (d (list (d (n "nom") (r "^7.0") (d #t) (k 0)))) (h "086ld6ymaixw8pdh2qnsmhcd0m1cw1rrj9d603i1nzn6pdjwnlvj")))

