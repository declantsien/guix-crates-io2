(define-module (crates-io ra di radix50) #:use-module (crates-io))

(define-public crate-radix50-0.1.0 (c (n "radix50") (v "0.1.0") (d (list (d (n "const_for") (r "^0.1") (d #t) (k 0)))) (h "1xy34qvl06qzn9gyicyl2v5z28nn8nn1sxr9hlrldj9jy2gzrz5n")))

(define-public crate-radix50-0.1.1 (c (n "radix50") (v "0.1.1") (d (list (d (n "const_for") (r "^0.1") (d #t) (k 0)))) (h "0xmzpksspncfxhv7hdry1n8im970i5n95slq25c1fw521lqmg4bg")))

(define-public crate-radix50-0.2.0 (c (n "radix50") (v "0.2.0") (d (list (d (n "const_for") (r "^0.1") (d #t) (k 0)))) (h "1r2xrhpiz6wbb1f3jqwncz9knydpxzbjn28jphbby2w429sxw4by")))

(define-public crate-radix50-0.2.1 (c (n "radix50") (v "0.2.1") (d (list (d (n "const_for") (r "^0.1") (d #t) (k 0)))) (h "1jnfy5fpn3cvzk7mg44pqj8nf04xhys6izm7hgjgjxvf9m189p40")))

