(define-module (crates-io ra di radix_fmt) #:use-module (crates-io))

(define-public crate-radix_fmt-0.1.0 (c (n "radix_fmt") (v "0.1.0") (h "1z2m84d15k4hvai9ajbni42hhl2l0zvdqk3impsffhg96drmh2ii")))

(define-public crate-radix_fmt-0.1.1 (c (n "radix_fmt") (v "0.1.1") (h "01vlzhmjwnvxkvbx4vsb6vn1myj7ng0lf6vmpqj6s96qk751a2hg")))

(define-public crate-radix_fmt-1.0.0 (c (n "radix_fmt") (v "1.0.0") (d (list (d (n "fluid") (r "^0.4") (d #t) (k 2)))) (h "09jlq152iwn56215kghqby4pi8vamhg0nzcb9any5b5782cjl26f")))

