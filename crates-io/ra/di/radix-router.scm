(define-module (crates-io ra di radix-router) #:use-module (crates-io))

(define-public crate-radix-router-0.1.0 (c (n "radix-router") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1il74hw5514vr3v9i0vlfgairgivzp3fmhgf8dg81f29m6xrxqhr")))

(define-public crate-radix-router-0.1.1 (c (n "radix-router") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0bpc7in0cy7gm2va6v2mgg143l7bk3kiibxbnqk5b0i8sxvflv6d")))

(define-public crate-radix-router-0.1.2 (c (n "radix-router") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.2.4") (d #t) (k 2)) (d (n "tokio-fs") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1v4i79nxynvh69qvm2a301ndjwxasva15i9lwfxv63rrz1rygiyz")))

