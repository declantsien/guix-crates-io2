(define-module (crates-io ra di radiate_matrix_tree) #:use-module (crates-io))

(define-public crate-radiate_matrix_tree-1.0.0 (c (n "radiate_matrix_tree") (v "1.0.0") (d (list (d (n "radiate") (r "^1.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "simple-matrix") (r "^0.1.2") (d #t) (k 0)))) (h "0zmcgbc6bw6ndvgnigkhxksapgcj3pjsf52kfskzrgdbr8ryv2fp")))

(define-public crate-radiate_matrix_tree-1.0.1 (c (n "radiate_matrix_tree") (v "1.0.1") (d (list (d (n "radiate") (r "^1.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "simple-matrix") (r "^0.1.2") (d #t) (k 0)))) (h "1fxsmm77napgnvh4gxz4d7w007pkw1f79sgyl6ab191xk8m5009z")))

