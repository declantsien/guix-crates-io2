(define-module (crates-io ra di radiantkit-macros) #:use-module (crates-io))

(define-public crate-radiantkit-macros-0.0.1 (c (n "radiantkit-macros") (v "0.0.1") (d (list (d (n "macro_magic") (r "^0.5.0") (f (quote ("proc_support"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "proc-utils") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1s329ccd37lmmgxllayz18gp13bcqi6bk7xikd95a3g7nprgnfx6")))

