(define-module (crates-io ra di radiate_web) #:use-module (crates-io))

(define-public crate-radiate_web-0.1.0 (c (n "radiate_web") (v "0.1.0") (d (list (d (n "radiate") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "060swg6snzsk716fkkhr2mm6g1x2dszr2wk6hf58vszp3m24l0fs")))

(define-public crate-radiate_web-0.1.1 (c (n "radiate_web") (v "0.1.1") (d (list (d (n "radiate") (r "^1.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0d6d07pwg8gq39z2fdmc24dcpacj6gwfzy43xmr2gww1m7s7b2kz")))

(define-public crate-radiate_web-0.1.12 (c (n "radiate_web") (v "0.1.12") (d (list (d (n "radiate") (r "^1.1.41") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "0nr0zi187hdf5krz10i9zs4g2z3j91g34sf4mn98g5axhhl5875f")))

(define-public crate-radiate_web-0.1.13 (c (n "radiate_web") (v "0.1.13") (d (list (d (n "radiate") (r "^1.1.56") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1yxxf9364prq82mxfc7hc4r18bmraranxkaxpfini2zbaxqmw3rn")))

