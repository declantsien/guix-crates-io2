(define-module (crates-io ra di radius-debug) #:use-module (crates-io))

(define-public crate-radius-debug-0.1.0 (c (n "radius-debug") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1kw5xcsn8yc7r0g12sjvaq1nm0ja1ilsqry1mrwa3xh0qnnkyn46")))

(define-public crate-radius-debug-0.1.1 (c (n "radius-debug") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0vvss55p0s4pd1ijsdbc5cfw4xqfldf6lqq32i9dk8j55dk7g5c0")))

(define-public crate-radius-debug-0.1.2 (c (n "radius-debug") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0zqvc62v978zhakhc6szrs5n7r3xgymjyn07gphzdwrlngh5brsw")))

