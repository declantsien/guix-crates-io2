(define-module (crates-io ra di radiation-macros) #:use-module (crates-io))

(define-public crate-radiation-macros-0.1.0 (c (n "radiation-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "1phdh8i101bwmlrv5hbvph6ksj262i98vadx8hb80wgyqf8agv9p") (r "1.58.1")))

(define-public crate-radiation-macros-0.1.1 (c (n "radiation-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "1byz8zii5lm17gygxwprzqfwyxixam8fxawg2nkjwdmc39hxppv4") (r "1.58.1")))

(define-public crate-radiation-macros-0.1.2 (c (n "radiation-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "032qvisvpqq7cy7djkvmhz07pygyk969syaw9m0gv6dscbbiik1w") (r "1.58.1")))

(define-public crate-radiation-macros-0.2.0 (c (n "radiation-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "1bax5xz28kh2rpldc79dwd8a47xvf717m4vyriy0vz1zd9npg3vm") (r "1.58.1")))

(define-public crate-radiation-macros-0.2.1 (c (n "radiation-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "0kx89lw04n1yvxnnjl610gmlw5ixqhwx5nx5i23xbkhm3wqrfw4q") (r "1.58.1")))

(define-public crate-radiation-macros-0.2.2 (c (n "radiation-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "150nymchyq1kahvn12n6gz2iam8d9837ppdd71v2wqgijqvn1qck") (r "1.58.1")))

(define-public crate-radiation-macros-0.3.0 (c (n "radiation-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)))) (h "19zc0x3qzpfidc4gqikq5vlknml89xm77zyyxbv7vmy9add8w999") (r "1.58.1")))

(define-public crate-radiation-macros-0.3.1 (c (n "radiation-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.1") (d #t) (k 0)))) (h "1z1xmj7plsw4fxx0na2pf0acl1zdgw1hyn11khagyyv1l4lxx5h3") (r "1.58.1")))

