(define-module (crates-io ra di radiant-utils) #:use-module (crates-io))

(define-public crate-radiant-utils-0.1.0 (c (n "radiant-utils") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)))) (h "0gk388m6hmcmxij0pzr367r5ip45kcsx8qni62vhjxlgdngbls4r")))

(define-public crate-radiant-utils-0.1.1 (c (n "radiant-utils") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.11") (d #t) (k 0)))) (h "10a4k4ydd9ajv2f1fhivvcldkzwr2bjwccharp2y55jigwj7k94a")))

(define-public crate-radiant-utils-0.1.2 (c (n "radiant-utils") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.11") (d #t) (k 0)))) (h "1cf54jw4jscsvlarh5c4sng5pwnfh7v3bq1l8b1cdbd2dkbdzai8")))

(define-public crate-radiant-utils-0.1.4 (c (n "radiant-utils") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.12") (d #t) (k 0)))) (h "1d1mp0lgg7cig8h77gkimvkyncr5fkfxz6zip46k5ydip5sqd9ql")))

(define-public crate-radiant-utils-0.1.5 (c (n "radiant-utils") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.12") (d #t) (k 0)))) (h "1nsp4abc29cq23i39s7az4g2590bpq9rhgmil11d2rlvvv3cylfk")))

(define-public crate-radiant-utils-0.2.0 (c (n "radiant-utils") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.12") (d #t) (k 0)))) (h "0f9z8kjyi0dh9lncdqpq9hz21sldbr7lh45q0xvs9y5sh75ha2pw")))

(define-public crate-radiant-utils-0.2.1 (c (n "radiant-utils") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jcx30f343nhkgng6ircgyyrdd61kmn8vv7h4b5v1m9vw0w5sj0x") (f (quote (("serialize-serde" "serde" "serde_derive"))))))

(define-public crate-radiant-utils-0.2.2 (c (n "radiant-utils") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dw30gnlkzs3iwgm40f6pazrdn2i093vf5gdyldyynh2hqz7ybbg") (f (quote (("serialize-serde" "serde" "serde_derive"))))))

(define-public crate-radiant-utils-0.3.0 (c (n "radiant-utils") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wld2l843izjrvrd5vww6c5ickbckivmj6r7vpqf0wdis7nvbxyn") (f (quote (("serialize-serde" "serde" "serde_derive"))))))

(define-public crate-radiant-utils-0.4.0 (c (n "radiant-utils") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "radiant-rs") (r ">= 0.13, < 0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nq47qipdsmrnbwbnnav04mmm9cprgh1lpkb1w9vhmq7icncyx6f") (f (quote (("uniforms" "radiant-rs") ("serialize-serde" "serde" "serde_derive"))))))

