(define-module (crates-io ra di radix_trie) #:use-module (crates-io))

(define-public crate-radix_trie-0.0.1 (c (n "radix_trie") (v "0.0.1") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "00cyqd73834f41m513ilhfxvnqircvljim4sj4l6ah0rfs4a64da")))

(define-public crate-radix_trie-0.0.2 (c (n "radix_trie") (v "0.0.2") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "1nfi49yd7frr66pkc9znp2y7z0xa1j3gdy5i1a5lmw39ziyr6rfd")))

(define-public crate-radix_trie-0.0.3 (c (n "radix_trie") (v "0.0.3") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "0g96w3khr2drg9iw1lqwv6fmhbza4rz7b3s5ck7wvpppr9s8zhpf")))

(define-public crate-radix_trie-0.0.4 (c (n "radix_trie") (v "0.0.4") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "1938nvqyv543zclb7ki1baqlzg5k2qavhcb4iv2gvx5swk0b6q27")))

(define-public crate-radix_trie-0.0.5 (c (n "radix_trie") (v "0.0.5") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "0qmsv4ymn8bagd49c5y0qq4rgyk3hbsdiwsg27g97wl3q38c6sr3")))

(define-public crate-radix_trie-0.0.6 (c (n "radix_trie") (v "0.0.6") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "1jzjffdamsjrvhqc8xmmbzba9vcsps8wdq2zn8w66c4988y1vj1m")))

(define-public crate-radix_trie-0.0.7 (c (n "radix_trie") (v "0.0.7") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)))) (h "0pkbm77awqk2d86r1hhqzg4ldzjqfh1shpygmf48mn2chbikjl6q")))

(define-public crate-radix_trie-0.0.8 (c (n "radix_trie") (v "0.0.8") (d (list (d (n "nibble_vec") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1iigwzyshckq14n3lfr9vaii1jj40c5sr6jffdirnp9hjwcq9nls")))

(define-public crate-radix_trie-0.0.9 (c (n "radix_trie") (v "0.0.9") (d (list (d (n "nibble_vec") (r "^0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1zrxh3j8dmb0869k9cfra87ahcpn96d4bjbzhddj29c3na0gsawd")))

(define-public crate-radix_trie-0.1.0 (c (n "radix_trie") (v "0.1.0") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "^0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0znn00cccq8xay96d40aj0n5mhhikr99ya613219109c35iasnyi")))

(define-public crate-radix_trie-0.1.1 (c (n "radix_trie") (v "0.1.1") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "^0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "04fnv8y2md78lwgd4fbv2ma4c6qflh9rz6s5533wna5ikfn7a63a")))

(define-public crate-radix_trie-0.1.2 (c (n "radix_trie") (v "0.1.2") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "^0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1s3f889mbrx9ndabjbm1a7hm2fng885sd76xs47sqp4rm6v4j711")))

(define-public crate-radix_trie-0.1.3 (c (n "radix_trie") (v "0.1.3") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "~0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1abha1a6lpjlb3f27sbqgxfrckq1pw2d7lv1r8hgd2hy91qdgl03")))

(define-public crate-radix_trie-0.1.4 (c (n "radix_trie") (v "0.1.4") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "~0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1iwmgbpm1s7ndcfd90gx92v478inp2qd0k9xnsj1lz01czkp5kzb")))

(define-public crate-radix_trie-0.1.5 (c (n "radix_trie") (v "0.1.5") (d (list (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "~0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1314kwfi6bg5ipsc7wvij8paw3p1ifhklwljrrsqk9d0ppvvzbyy")))

(define-public crate-radix_trie-0.1.6 (c (n "radix_trie") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "~0.0.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "158fh8qj35bfgfyrfnxild7m196nh8gl9a8fasqcynnrijr82dix")))

(define-public crate-radix_trie-0.2.0 (c (n "radix_trie") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "18qndfc0xyk2d0whbgn6xcwd0fhisw56xzsflymdzih9j6hs5ssn")))

(define-public crate-radix_trie-0.2.1 (c (n "radix_trie") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "endian-type") (r "^0.1.2") (d #t) (k 0)) (d (n "nibble_vec") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1zaq3im5ss03w91ij11cj97vvzc5y1f3064d9pi2ysnwziww2sf0")))

