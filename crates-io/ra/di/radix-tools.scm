(define-module (crates-io ra di radix-tools) #:use-module (crates-io))

(define-public crate-radix-tools-0.1.0 (c (n "radix-tools") (v "0.1.0") (h "0ma0skd2rpwqqwjd3ghn28yq861waddcfrmnsvlqmb378gx5fqhw")))

(define-public crate-radix-tools-0.1.1 (c (n "radix-tools") (v "0.1.1") (h "17rnr7f7hxa4s017hchl45q1a150pq5rf9x5ia9sqgnlgby8h417")))

(define-public crate-radix-tools-0.1.2 (c (n "radix-tools") (v "0.1.2") (h "0fcg400a5rfc58wh16qiyvrgpp4hkihmp1sqq5a6525dvrqrc1pz")))

