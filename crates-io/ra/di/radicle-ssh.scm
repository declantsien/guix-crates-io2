(define-module (crates-io ra di radicle-ssh) #:use-module (crates-io))

(define-public crate-radicle-ssh-0.2.0 (c (n "radicle-ssh") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "1bxsrvw4kdv2xjnwxb0n85j4jw8vvj6009ngzczi8406bmf6qqy4")))

(define-public crate-radicle-ssh-0.9.0 (c (n "radicle-ssh") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 0)))) (h "1ik6fd64idgdyknl7lahcjqibg1wxgxr31diwhmlhr7v2207bvpv")))

