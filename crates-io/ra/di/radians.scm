(define-module (crates-io ra di radians) #:use-module (crates-io))

(define-public crate-radians-0.1.0 (c (n "radians") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12lc1y1hjkbjhv6n7aq974dwawnm5hxs75rjkfn4jig2alrdskqn")))

(define-public crate-radians-0.1.1 (c (n "radians") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1c4mgi4z3viq0xlib1c2fzl4kg1zdmf22nz1asdaiqvfdp76yl5l")))

(define-public crate-radians-0.2.0 (c (n "radians") (v "0.2.0") (h "05nn6gfm9sfj92rdschj96la6pylf0l4l2d1p8c9ay3ddcfp2i2l")))

(define-public crate-radians-0.2.1 (c (n "radians") (v "0.2.1") (h "1vxwsfmgiqr1yc5ybazwb3s10kqpzi6qqjlz7fwn0ajhdzf7xr5z")))

(define-public crate-radians-0.2.2 (c (n "radians") (v "0.2.2") (d (list (d (n "real_float") (r "^0.1.1") (d #t) (k 0)))) (h "089k7idh1f6h5z0ljvvx0ksxsqgaf3ymy6fygkpqssfr8q0ahl7q")))

(define-public crate-radians-0.2.3 (c (n "radians") (v "0.2.3") (d (list (d (n "real_float") (r "^0.2.0") (d #t) (k 0)))) (h "1zlfc27axyhrpv0fcqi9zhffigyp1hdxd0x05y7n4h2ws3qc24wz")))

(define-public crate-radians-0.3.0 (c (n "radians") (v "0.3.0") (d (list (d (n "real_float") (r "^0.3") (d #t) (k 0)))) (h "0l67hcc184sqpm077lz5sf25jcdvv6rk2lxyzhcvrrjwaq2709ip")))

(define-public crate-radians-0.3.1 (c (n "radians") (v "0.3.1") (d (list (d (n "real_float") (r "^0.3") (d #t) (k 0)))) (h "0s15n80cj7gnlr58v7r6fjxflaxb26h6y132x7wdil744kjw7gcw")))

