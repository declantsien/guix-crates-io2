(define-module (crates-io ra di radicle-dag) #:use-module (crates-io))

(define-public crate-radicle-dag-0.2.0 (c (n "radicle-dag") (v "0.2.0") (d (list (d (n "fastrand") (r "^1.9.0") (d #t) (k 0)))) (h "0n52i3c972z2xv41mfzvrlfhngcms478r9fm48890iqfyal9sbyg")))

(define-public crate-radicle-dag-0.9.0 (c (n "radicle-dag") (v "0.9.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 0)))) (h "0rrh6108ii7qbjvncd7n19m0d7szkbm557yx6imax24s0k1pi9n2")))

