(define-module (crates-io ra di radium) #:use-module (crates-io))

(define-public crate-radium-0.1.0 (c (n "radium") (v "0.1.0") (h "156fnnnb2lmm57fapggadpnfaz24dhs35y7k4j00j3cjm2iz76cw")))

(define-public crate-radium-0.1.1 (c (n "radium") (v "0.1.1") (h "0q7b7zwihf6awx5xyb4b480hwh3l6mcsdzf8dckqhyk52kadi1kz")))

(define-public crate-radium-0.2.0 (c (n "radium") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1i180sfb3slgk0515f5vl1wa3m5igmzq97qfv7v6g8c8fx0106l1")))

(define-public crate-radium-0.3.0 (c (n "radium") (v "0.3.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1b27ihsm5sg17nrkj475y1wymzdxzg7lxy770c98crb16230mxfy")))

(define-public crate-radium-0.4.0 (c (n "radium") (v "0.4.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0f0rv0rkhyp25syidqdmz7b3wxqf1q6h8mjxpmzj9vq3p4rfrjak")))

(define-public crate-radium-0.5.0 (c (n "radium") (v "0.5.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1nnjgmn4kp136zc2ycwmq3kp8hfkcbb4lbn5ifsgkdb1z84r7wvx")))

(define-public crate-radium-0.4.1 (c (n "radium") (v "0.4.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "0si856rqka9ljw01mqvgfq78gv3id2l737zwmvqk9q31ac69mpk4")))

(define-public crate-radium-0.5.1 (c (n "radium") (v "0.5.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "199qad1qfzx3lvz2l8anpfmijlyb3vmc4pg9ya4mmzyyd9gkncss")))

(define-public crate-radium-0.5.2 (c (n "radium") (v "0.5.2") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1k9imh4wmxfk1alsfzi8x2fq5y767imlf7akdixmq5q7vwdwhs07") (y #t)))

(define-public crate-radium-0.5.3 (c (n "radium") (v "0.5.3") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1f5vj5zy4kcsw8p87y976dm5pln6v6jfw5f0fkj7qbwfipbsj6wl")))

(define-public crate-radium-0.6.0 (c (n "radium") (v "0.6.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1dplr3gif05mb2z1w3fby45x3y83fz9l254nck05w8bkbisj35sm")))

(define-public crate-radium-0.6.1 (c (n "radium") (v "0.6.1") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1p4d7jdsl8ix03n0rmb7ycly9p8q9ksaf8085dki5p8z3s0hdq79")))

(define-public crate-radium-0.6.2 (c (n "radium") (v "0.6.2") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1ys4bpp2l701ghdniz90zhkvb5ykmfw2pj0m8pfcbi7bm10qygv4")))

(define-public crate-radium-0.7.0 (c (n "radium") (v "0.7.0") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "02cxfi3ky3c4yhyqx9axqwhyaca804ws46nn4gc1imbk94nzycyw")))

(define-public crate-radium-1.0.0-rc1 (c (n "radium") (v "1.0.0-rc1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1si5k5grigh413msxjyd7lca6wwpahcaaacshv6612rnd3mgfzg3")))

(define-public crate-radium-1.0.0-rc2 (c (n "radium") (v "1.0.0-rc2") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0as1x0crijkr9jvcqw2sbnafxiw24rl3xbv2ih1av78lgyxw9yx9")))

(define-public crate-radium-1.0.0-rc3 (c (n "radium") (v "1.0.0-rc3") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0rw0w7fxkglp4mkb6mwrhn02hawpxibbms6f6h9y40k77v7qa9y4")))

(define-public crate-radium-1.0.0 (c (n "radium") (v "1.0.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "13kiq812bjvghycmmc4bc9vlaxzax12klkpi4r7m104rhbms01ci") (r "1.60")))

(define-public crate-radium-1.1.0 (c (n "radium") (v "1.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "portable-atomic") (r "^1") (o #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0b07iabyksxfp8vq3fx46k69bn6jfdmh84jahr631nq2i4l7c2yv") (f (quote (("portable-atomic-fallback" "portable-atomic" "portable-atomic/fallback")))) (s 2) (e (quote (("portable-atomic" "dep:portable-atomic")))) (r "1.60")))

