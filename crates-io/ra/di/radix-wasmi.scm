(define-module (crates-io ra di radix-wasmi) #:use-module (crates-io))

(define-public crate-radix-wasmi-1.0.0 (c (n "radix-wasmi") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (k 2)) (d (n "radix-wasmi-arena") (r "^1.0.0") (k 0)) (d (n "spin") (r "^0.9") (f (quote ("mutex" "spin_mutex" "rwlock"))) (k 0)) (d (n "wasmi_core") (r "^0.8") (k 0)) (d (n "wasmparser") (r "^0.91") (k 0) (p "wasmparser-nostd")) (d (n "wast") (r "^44.0") (d #t) (k 2)) (d (n "wat") (r "^1") (d #t) (k 2)))) (h "0bblg830p5prnvs6k049nrpbphl9k073dy96wwi328pdk024mgsr") (f (quote (("std" "wasmi_core/std" "radix-wasmi-arena/std" "wasmparser/std" "spin/std") ("default" "std"))))))

