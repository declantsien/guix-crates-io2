(define-module (crates-io ra di radiant) #:use-module (crates-io))

(define-public crate-radiant-0.2.0 (c (n "radiant") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1w8sn1alw38sslymfx1h7xak0s9mmsqrd9b5crxkq9hi9492i9qw")))

(define-public crate-radiant-0.2.1 (c (n "radiant") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "cargo-husky") (r "^1.5") (f (quote ("run-cargo-fmt" "run-cargo-clippy" "user-hooks"))) (d #t) (k 2)) (d (n "minifb") (r "^0.19") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0rl982hijix5xvi1w5pr5v83c8vf3jd0jgvp3rypkxw9c82b2yrs")))

(define-public crate-radiant-0.3.0 (c (n "radiant") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bytemuck") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "cargo-husky") (r "^1.5") (f (quote ("run-cargo-fmt" "run-cargo-clippy" "user-hooks"))) (d #t) (k 2)) (d (n "minifb") (r "^0.19") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "1lyc0pkggwpbakh7777c4r29dr2c7yhd9kxdy3dnbfrk639miq4g") (f (quote (("impl-bytemuck" "bytemuck"))))))

