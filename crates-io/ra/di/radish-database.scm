(define-module (crates-io ra di radish-database) #:use-module (crates-io))

(define-public crate-radish-database-0.1.0 (c (n "radish-database") (v "0.1.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "num") (r "^0") (d #t) (k 0)) (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0g63hrqs4xvsq0x4nl0f24j82k9kza418861gij9fbffj71wpxv3")))

