(define-module (crates-io ra di radixtree) #:use-module (crates-io))

(define-public crate-radixtree-0.1.0 (c (n "radixtree") (v "0.1.0") (h "07nyrs5la8nc9zyh90v1jqa6xq6pdgy1qs1cd9jsm69yzp1cplzn")))

(define-public crate-radixtree-0.1.1 (c (n "radixtree") (v "0.1.1") (h "04j072k72805jmpv1kys7xxpb7xwdmqx1hpcpiwsh1f1md9z98qn")))

