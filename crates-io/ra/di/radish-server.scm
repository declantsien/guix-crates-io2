(define-module (crates-io ra di radish-server) #:use-module (crates-io))

(define-public crate-radish-server-0.1.0 (c (n "radish-server") (v "0.1.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "radish-database") (r "^0") (d #t) (k 0)) (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05jb0cn2fmyspi8ay34szrgzimqc95an2dfzahfh07hbc9b587hy")))

(define-public crate-radish-server-0.1.2 (c (n "radish-server") (v "0.1.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "radish-database") (r "^0") (d #t) (k 0)) (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qzqq44fl3d4g9q4qwb855kpgl6i1ziq7ddms7044h5f6zlyh3rw")))

