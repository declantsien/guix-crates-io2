(define-module (crates-io ra di radicle-remote-helper) #:use-module (crates-io))

(define-public crate-radicle-remote-helper-0.9.0 (c (n "radicle-remote-helper") (v "0.9.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "radicle") (r "^0") (d #t) (k 0)) (d (n "radicle-cli") (r "^0") (d #t) (k 0)) (d (n "radicle-crypto") (r "^0") (d #t) (k 0)) (d (n "radicle-git-ext") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z3gqp5413v5mijnacw9lwidwgz8r49dxvka95j6xnzf5hccgn5m")))

