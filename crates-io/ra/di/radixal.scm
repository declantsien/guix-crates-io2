(define-module (crates-io ra di radixal) #:use-module (crates-io))

(define-public crate-radixal-0.1.0 (c (n "radixal") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "14daxvii944m7bnp0y3sq6k7w8bya4dhzmicg6q7cdm6qcayqp36") (f (quote (("std") ("default" "std"))))))

(define-public crate-radixal-0.2.0 (c (n "radixal") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1chpssgh0lq6bq56imxrzi8512mswkm9cjx8cnzmf0zzdx7imwxz") (f (quote (("std") ("default" "std"))))))

(define-public crate-radixal-0.2.1 (c (n "radixal") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "05yqdyjysx61ph9xcnn9g5692nh7wsp331hplgv7djc9zjv742ga") (f (quote (("std") ("default" "std"))))))

(define-public crate-radixal-0.2.2 (c (n "radixal") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "18v9avv20lbjjx5kj8s73xypyja71aar7g6n9ikxkw6hfyp1payw") (f (quote (("std") ("default" "std"))))))

(define-public crate-radixal-0.3.0 (c (n "radixal") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0l3b9c3r7xxc1b7ggwvyg49qkq54ldb6wz5601zjrl4hvdjp3vvq") (f (quote (("std") ("default" "std"))))))

