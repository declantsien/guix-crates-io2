(define-module (crates-io ra di radix-engine-profiling-derive) #:use-module (crates-io))

(define-public crate-radix-engine-profiling-derive-1.2.0-dev (c (n "radix-engine-profiling-derive") (v "1.2.0-dev") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "radix-engine-profiling") (r "^1.2.0-dev") (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "1qhyx0gzdzm663gnd89ghk7hxwrynr41k92b7v8iqm19jsfl9rfc") (f (quote (("resource_tracker") ("default"))))))

(define-public crate-radix-engine-profiling-derive-1.2.0 (c (n "radix-engine-profiling-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "radix-engine-profiling") (r "^1.2.0") (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "0icpr3l5zgmn35qgxzzkc2h82wxlhai8p8dilcycb486b2phcvhj") (f (quote (("resource_tracker" "radix-engine-profiling/resource_tracker") ("default"))))))

