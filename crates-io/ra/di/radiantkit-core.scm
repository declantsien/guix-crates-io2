(define-module (crates-io ra di radiantkit-core) #:use-module (crates-io))

(define-public crate-radiantkit-core-0.0.1 (c (n "radiantkit-core") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "epaint") (r "^0.22.0") (f (quote ("bytemuck" "serde"))) (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "macro_magic") (r "^0.5.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.17") (f (quote ("webgl"))) (d #t) (k 0)))) (h "108hvs1gbaraljhhyc6agv4w0wj6ipvb2y3gaakz85yqrc6799b4")))

