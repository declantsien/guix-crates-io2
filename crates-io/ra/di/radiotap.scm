(define-module (crates-io ra di radiotap) #:use-module (crates-io))

(define-public crate-radiotap-1.0.0 (c (n "radiotap") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pcap") (r "^0.7") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "03xckkgsw75rvpnmp03jvfs4dc66zaa8693pz9r98dp2gjy9qmzd")))

(define-public crate-radiotap-1.1.0 (c (n "radiotap") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pcap") (r "^0.7") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0v2ym0cli3j29jy4zrcr8rb3sxbnl95qm08wqdywmwrgw4xd8s7z")))

(define-public crate-radiotap-1.2.0 (c (n "radiotap") (v "1.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pcap") (r "^0.7") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "088dn1nqmh7a98p50m0m4090lqsxl1zhi3whg094aqlsvxd4qb5c")))

(define-public crate-radiotap-1.3.0 (c (n "radiotap") (v "1.3.0") (d (list (d (n "bitops") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "pcap") (r "^0.7") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0jirr3s0sqzmnccg8sgymc5znqzaxy3rl5h5swga98lr4w1mf1z5")))

(define-public crate-radiotap-2.0.0-beta.1 (c (n "radiotap") (v "2.0.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "frombytes") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "pcap") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vahmnm8vz02cvci005gm2qhfmdanfjpkq5n97dd3shm78rnqv4a") (y #t)))

