(define-module (crates-io ra di radicle-ed25519-compact) #:use-module (crates-io))

(define-public crate-radicle-ed25519-compact-2.0.3 (c (n "radicle-ed25519-compact") (v "2.0.3") (d (list (d (n "ct-codecs") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "ed25519") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ct-codecs") (r "^1.1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (d #t) (k 2)))) (h "1h3kzfy89lw0a0iahzbvlnmzffif6qkcd4gcyyyzcx6zs7nxma0m") (f (quote (("x25519") ("traits" "ed25519") ("std") ("self-verify") ("random" "getrandom") ("pem" "ct-codecs") ("opt_size") ("disable-signatures") ("default" "random" "std" "x25519" "pem") ("blind-keys")))) (y #t)))

