(define-module (crates-io ra di radicle-cli-test) #:use-module (crates-io))

(define-public crate-radicle-cli-test-0.1.0 (c (n "radicle-cli-test") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k1qphj2zsbfz8w7mn372k0k8swdvgz3jqfa4hggqk7rps48gci1") (r "1.67")))

(define-public crate-radicle-cli-test-0.1.1 (c (n "radicle-cli-test") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09xwa94ypgys5hqrl8m5gcb1bn5qx35ilsxfpx4wkpz243hpw66z") (r "1.67")))

(define-public crate-radicle-cli-test-0.9.0 (c (n "radicle-cli-test") (v "0.9.0") (d (list (d (n "escargot") (r "^0.5.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "radicle") (r "^0.9.0") (f (quote ("logger" "test"))) (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "snapbox") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01vp1w6crsdh1gp6l86r3clbdlga1brixpry22b556y98sm3f5pd")))

