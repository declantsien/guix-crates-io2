(define-module (crates-io ra di radix-sbor-derive) #:use-module (crates-io))

(define-public crate-radix-sbor-derive-1.2.0-dev (c (n "radix-sbor-derive") (v "1.2.0-dev") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "sbor-derive-common") (r "^1.2.0-dev") (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ajh5fd1nqggfg4ik765qiqvqgbsbg82ivlw7np95zik1m50g6x7") (f (quote (("trace" "sbor-derive-common/trace") ("std") ("default" "std") ("alloc"))))))

(define-public crate-radix-sbor-derive-1.2.0 (c (n "radix-sbor-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "sbor-derive-common") (r "^1.2.0") (k 0)) (d (n "syn") (r "^1.0.93") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "088k0x7lkasn5nhwh8hvm0fqbahqh4pgx0lspqasyadic1bjpjbw") (f (quote (("trace" "sbor-derive-common/trace") ("std") ("default" "std") ("alloc"))))))

