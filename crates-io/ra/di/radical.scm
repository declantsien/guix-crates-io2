(define-module (crates-io ra di radical) #:use-module (crates-io))

(define-public crate-radical-0.1.0 (c (n "radical") (v "0.1.0") (d (list (d (n "dimensioned") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "10nc41ywhnd1j4b2vh1bqyf62ymrh8jpjc3s12c824bywydss6qa")))

(define-public crate-radical-0.1.1 (c (n "radical") (v "0.1.1") (d (list (d (n "dimensioned") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.4") (k 0)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "10x93fjfx64danglyvyvifrdf9r1aiv2sim8j8fcqxlmrfg986qm")))

(define-public crate-radical-0.0.0 (c (n "radical") (v "0.0.0") (h "1kdfrh1irk42zxx8h9mgx3bldpfylx4jphcgy4q81cx486p7aqwf")))

