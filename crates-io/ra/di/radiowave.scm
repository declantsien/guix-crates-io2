(define-module (crates-io ra di radiowave) #:use-module (crates-io))

(define-public crate-radiowave-0.1.0 (c (n "radiowave") (v "0.1.0") (h "106fql4y40y7cawq34n6brijc2ch39kwjvlvwv1325yvdqmblfn1")))

(define-public crate-radiowave-0.1.1 (c (n "radiowave") (v "0.1.1") (h "0hi1h4lhnryzd3vgw068iq8nrk34igw8ab6k62a895fx18qnbx01")))

(define-public crate-radiowave-0.1.2 (c (n "radiowave") (v "0.1.2") (h "0aybgfgj1y0inbm2mym61lx150pxny3jzs3p3sbhnj5mm98jf8vh")))

(define-public crate-radiowave-1.0.0 (c (n "radiowave") (v "1.0.0") (h "1s20ly5zc53xqxdpyxzyfffz2did4n1mdghr60sdbqp1fxgirf27")))

(define-public crate-radiowave-2.0.0 (c (n "radiowave") (v "2.0.0") (h "0cd7x3zpd440j18n03gmmpfj9fgnrgb38wq5444ybphchlcs9cz3")))

