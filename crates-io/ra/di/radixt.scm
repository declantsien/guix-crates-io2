(define-module (crates-io ra di radixt) #:use-module (crates-io))

(define-public crate-radixt-0.1.0 (c (n "radixt") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zkw0igckagsp4wxs4wcab0b49y1xmddyppvb4ihpazhq5p25zkc")))

