(define-module (crates-io ra di radish-cli) #:use-module (crates-io))

(define-public crate-radish-cli-0.1.0 (c (n "radish-cli") (v "0.1.0") (d (list (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0sgxnd9j69a2nw2q4ggihx2j9sj0kc78jadf9pzbggw76xhv4pby")))

(define-public crate-radish-cli-0.1.1 (c (n "radish-cli") (v "0.1.1") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cd71kjsrjivc6ln6b7civ9qrswn4lcvxv8ld2f2ls2vp8jh7yf8")))

(define-public crate-radish-cli-0.1.2 (c (n "radish-cli") (v "0.1.2") (d (list (d (n "env_logger") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "radish-types") (r "^0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "13m1xpvxhki5qpk319dcjf5m6dkz9h6lnksn7n9mncg53kw6c1p1")))

