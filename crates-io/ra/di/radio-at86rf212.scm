(define-module (crates-io ra di radio-at86rf212) #:use-module (crates-io))

(define-public crate-radio-at86rf212-0.1.0 (c (n "radio-at86rf212") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)))) (h "14786hjwpyhlzl0ln7dyqvagfpj8hd1niipiwkybwmgm5pn9s3y5")))

(define-public crate-radio-at86rf212-0.2.0 (c (n "radio-at86rf212") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.2") (d #t) (k 2)) (d (n "radio") (r "^0.1.0") (d #t) (k 0)))) (h "0v6h0ndqmnz6rbbxmnh5xl7r3yh6ghb8k3bc45fkgz52gvb57f9j")))

