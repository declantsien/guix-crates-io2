(define-module (crates-io ra sc rascal_parser) #:use-module (crates-io))

(define-public crate-rascal_parser-0.1.1 (c (n "rascal_parser") (v "0.1.1") (d (list (d (n "rascal_scanner") (r "^0.1.1") (d #t) (k 0)))) (h "1xmwiyrgyq96g6i6ma8gz8ikcmnd1ny929g8zcpc2ljkyr19wd4q")))

(define-public crate-rascal_parser-0.1.2 (c (n "rascal_parser") (v "0.1.2") (d (list (d (n "rascal_scanner") (r "^0.1.2") (d #t) (k 0)))) (h "0kl862ix8xgvzzz77xcfxir3h0qb1iz826mqa2h1gzbf15ghqdc7")))

