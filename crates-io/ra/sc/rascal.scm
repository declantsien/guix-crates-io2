(define-module (crates-io ra sc rascal) #:use-module (crates-io))

(define-public crate-rascal-0.1.0 (c (n "rascal") (v "0.1.0") (h "0ibp3nvh0gzczgcnnsr90j762v2qahyjvb48wp2pipbq4biq8df3")))

(define-public crate-rascal-0.1.1 (c (n "rascal") (v "0.1.1") (d (list (d (n "rascal_bytecode") (r "^0.1") (d #t) (k 0)) (d (n "rascal_compiler") (r "^0.1") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rascal_scanner") (r "^0.1.1") (d #t) (k 0)) (d (n "rascal_vm") (r "^0.1") (d #t) (k 0)))) (h "02vrw724mj1iqsvl8imh49lkz9imhdscg677dwjxm65fvfnwzl2k")))

(define-public crate-rascal-0.1.2 (c (n "rascal") (v "0.1.2") (d (list (d (n "rascal_bytecode") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_compiler") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_scanner") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_vm") (r "^0.1.2") (d #t) (k 0)))) (h "0g4r6dn4s8iv1qfc72sxk5m00mavpp2ndxb7ffpls4n10scc2nms")))

