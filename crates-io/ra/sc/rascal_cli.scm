(define-module (crates-io ra sc rascal_cli) #:use-module (crates-io))

(define-public crate-rascal_cli-0.1.2 (c (n "rascal_cli") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "rascal") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1clljnzdfvyjyzns7g837fs5k6nqkpkzjyq9mksagn44k5jz1w2y")))

