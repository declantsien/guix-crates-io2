(define-module (crates-io ra sc rascii) #:use-module (crates-io))

(define-public crate-rascii-0.1.0 (c (n "rascii") (v "0.1.0") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1mr67wmmq13c1qnial6fx22v131rm5b1ixys3cjml59gfgrmriih")))

(define-public crate-rascii-0.1.1 (c (n "rascii") (v "0.1.1") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1ybrw9np1lnh7i11186mmz67bp6jx3ylgypzi0db7d1ka4nskg9v")))

(define-public crate-rascii-0.1.2 (c (n "rascii") (v "0.1.2") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1ldxw8kpvggpmwg54bkil007sxxsx2b4y209pji65snrjysnixpf")))

