(define-module (crates-io ra sc rasch) #:use-module (crates-io))

(define-public crate-rasch-0.1.0 (c (n "rasch") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)))) (h "125rvdq1bzkwh4xwx4jakc947qdny67syfl7kdb0wnf08c46507i")))

(define-public crate-rasch-0.1.1 (c (n "rasch") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)))) (h "0rjzryxbzpm2b1rqqmc3pnhixpf3yrdqqpl1c3azxw9b6m1m8g1b")))

