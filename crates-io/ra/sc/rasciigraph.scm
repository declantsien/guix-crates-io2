(define-module (crates-io ra sc rasciigraph) #:use-module (crates-io))

(define-public crate-rasciigraph-0.1.0 (c (n "rasciigraph") (v "0.1.0") (h "18dp71md5wz5wbrsqsiwhrcij3fp3narpmi0za5jqh1nlazcqyv5")))

(define-public crate-rasciigraph-0.1.1 (c (n "rasciigraph") (v "0.1.1") (h "10mc6552k4ci5nz62lmf7yl12b8nqwz9jm2m8z0vfpckgq6nky8h")))

(define-public crate-rasciigraph-0.2.0 (c (n "rasciigraph") (v "0.2.0") (h "0x6bmmsq2blarlqdvin9yc8r45kdgl0gdlpaxh4vpkd0fqyjg8w9")))

