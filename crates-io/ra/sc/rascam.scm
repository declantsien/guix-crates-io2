(define-module (crates-io ra sc rascam) #:use-module (crates-io))

(define-public crate-rascam-0.0.1 (c (n "rascam") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmal-sys") (r "^0.1.0-1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "scopeguard") (r "^0.3.3") (d #t) (k 0)))) (h "0mkh4qfifv4k103wyxz91n4m7i5i6b2grfp3kmsdl2w5dg8l0z8l") (f (quote (("default") ("debug"))))))

(define-public crate-rascam-0.0.2 (c (n "rascam") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmal-sys") (r "^0.1.0-3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "scopeguard") (r "^1.0") (d #t) (k 0)))) (h "1rzvb247yf0pws6pbfj5b20ch2mjwg6sg7idjzkmb04l0l24lkr8") (f (quote (("default") ("debug"))))))

