(define-module (crates-io ra sc rascal_bytecode) #:use-module (crates-io))

(define-public crate-rascal_bytecode-0.1.0 (c (n "rascal_bytecode") (v "0.1.0") (h "1pfcjw6drv1jxrs67ac6i5ip0ldrfmcskkrxfgqymmhdml95ayxw")))

(define-public crate-rascal_bytecode-0.1.2 (c (n "rascal_bytecode") (v "0.1.2") (h "03y9775ln05zbzv3pv588gxj5rijqfndx9hw5kb4jjgrnp1sy3xi")))

