(define-module (crates-io ra sc rascal_vm) #:use-module (crates-io))

(define-public crate-rascal_vm-0.1.0 (c (n "rascal_vm") (v "0.1.0") (d (list (d (n "rascal_bytecode") (r "^0.1") (d #t) (k 0)) (d (n "rascal_compiler") (r "^0.1") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "rascal_scanner") (r "^0.1.1") (d #t) (k 0)))) (h "10z03zdzdk8mn14zjh1cqg8sb6wlffx1ar51xijqnidpfviybz8r")))

(define-public crate-rascal_vm-0.1.2 (c (n "rascal_vm") (v "0.1.2") (d (list (d (n "rascal_bytecode") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_compiler") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_scanner") (r "^0.1.2") (d #t) (k 0)))) (h "035q338hzrmij6mw4ndi2hbdz0albiqni7rkhmf7rkjbkpbqnm6f")))

