(define-module (crates-io ra sc rascii_art) #:use-module (crates-io))

(define-public crate-rascii_art-0.1.0 (c (n "rascii_art") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "0vzl5z6wfpgigbjxdlp2as2rgwkp4yxpz0nd7zdafnsq5wkwdsnx")))

(define-public crate-rascii_art-0.2.0 (c (n "rascii_art") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "0328mq52pzas3sld8a232ig92yi8lv1g04j5bkfn4kv7nkb2i7sl")))

(define-public crate-rascii_art-0.2.1 (c (n "rascii_art") (v "0.2.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "0ddak078994fvy9ibgqk32y67n90b7xxky80k7hhjwd5fmdd0kys")))

(define-public crate-rascii_art-0.2.2 (c (n "rascii_art") (v "0.2.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "11max5qx90ba82da6aa0nyjjwngs61paljb64gwrmmc94bvlvlv2")))

(define-public crate-rascii_art-0.2.3 (c (n "rascii_art") (v "0.2.3") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "0dskd77a9z8ynjdid3j8gwp3d92da375g7ixfmpjj5r9wbks1r4y")))

(define-public crate-rascii_art-0.2.4 (c (n "rascii_art") (v "0.2.4") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rgb2ansi256") (r "^0.1.1") (d #t) (k 0)))) (h "1x29ksjxqnsk9ii91fy2rmfdjl5s131lkmx21cyv8j11b3yylq0a")))

(define-public crate-rascii_art-0.1.2 (c (n "rascii_art") (v "0.1.2") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "18nz0z5s545q5lxigp3cxl38akv5wv2yxk4s2pbw7g87sdinz348")))

(define-public crate-rascii_art-0.2.5 (c (n "rascii_art") (v "0.2.5") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "13nq7vkd1n692b18m6rgmawdkm0k7nlm161lcj7h6lhv7z3j61mf")))

(define-public crate-rascii_art-0.2.6 (c (n "rascii_art") (v "0.2.6") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0m5k538nl85960wnswqgs8mnlvxzg7vy2dnbdmnj2hh9j8m48x0c")))

(define-public crate-rascii_art-0.2.61 (c (n "rascii_art") (v "0.2.61") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "0vr66zjmfq4by6zjvpgafi8974hy1kvg8pfwi9kajxpwcny7s0p1")))

(define-public crate-rascii_art-0.3.3 (c (n "rascii_art") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1754h1lc1qar14rykxqkc0gib3kk40d3vnmyrxhmr10asn2gf42d")))

(define-public crate-rascii_art-0.3.4 (c (n "rascii_art") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0w0519p0xxj61r77cfmdrcxnc3kqc9alabznnqxcm25mhzbs3442")))

(define-public crate-rascii_art-0.4.0 (c (n "rascii_art") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0kbmni5iah236w2z8vilmlkqvlgs041axn1kcvb8yfjcn8qa91an")))

(define-public crate-rascii_art-0.4.1 (c (n "rascii_art") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0fvnnnwf3l095l72gczcf66lhwly76mdwr7yqhvgf1bsqvcry6fb") (y #t)))

(define-public crate-rascii_art-0.4.2 (c (n "rascii_art") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "expanduser") (r "^1.2.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0mrlha1ghxz2bqs0vvs0lkd0zpbvkpxxc3jys2w3vav9p1w1ywn1")))

(define-public crate-rascii_art-0.4.3 (c (n "rascii_art") (v "0.4.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "expanduser") (r "^1.2.2") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1gfp924zfmw81laq8armnk2mb1yrhcc21l1fc0s672iw84ncqx41")))

(define-public crate-rascii_art-0.4.4 (c (n "rascii_art") (v "0.4.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "069ayzgvy5md4bv9ynqb78ri2r2qzw8nx1dlypn83cvhnf6qw953")))

(define-public crate-rascii_art-0.4.5 (c (n "rascii_art") (v "0.4.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0r8wddadw4fb2v8h3kpg5hyrlh65x128zbjavprd9d5a6qx5fzz2")))

