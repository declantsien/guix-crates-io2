(define-module (crates-io ra sc rascal_compiler) #:use-module (crates-io))

(define-public crate-rascal_compiler-0.1.0 (c (n "rascal_compiler") (v "0.1.0") (d (list (d (n "rascal_bytecode") (r "^0.1") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.1") (d #t) (k 0)))) (h "137ws1dsgpdqx2rhnnzvq7v7lvgj30di4qjqkifvrq6k1pxp1355")))

(define-public crate-rascal_compiler-0.1.2 (c (n "rascal_compiler") (v "0.1.2") (d (list (d (n "rascal_bytecode") (r "^0.1.2") (d #t) (k 0)) (d (n "rascal_parser") (r "^0.1.2") (d #t) (k 0)))) (h "0ki088n1kys4r1ci1wwkxkldcr26nihqn2zqj9sxz3sjs7ps7298")))

