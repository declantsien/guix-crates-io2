(define-module (crates-io ra sc rascal_scanner) #:use-module (crates-io))

(define-public crate-rascal_scanner-0.1.1 (c (n "rascal_scanner") (v "0.1.1") (h "192bslka9blv26q0hs99wh4fgf3vnw0a5g8dcrjk70hl78p2fg30")))

(define-public crate-rascal_scanner-0.1.2 (c (n "rascal_scanner") (v "0.1.2") (h "1l6h7x0jy8rvxv9mlmmkwhdp9sp3682yw6li2r29wk7qhw0xkmpc")))

