(define-module (crates-io ra wc rawcmd_style) #:use-module (crates-io))

(define-public crate-rawcmd_style-0.1.0 (c (n "rawcmd_style") (v "0.1.0") (d (list (d (n "rawcmd_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1xxq4mm72l9wzy65wb4fjbxj0q847klryb38qyh18lc8adxw0xs8")))

(define-public crate-rawcmd_style-0.1.1 (c (n "rawcmd_style") (v "0.1.1") (d (list (d (n "rawcmd_utils") (r "^0.1.1") (d #t) (k 0)))) (h "15xbx1bbl00bqc11rnk4llb90ngzsn06h772g7d7s0fml3hplm0b")))

