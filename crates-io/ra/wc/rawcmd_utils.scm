(define-module (crates-io ra wc rawcmd_utils) #:use-module (crates-io))

(define-public crate-rawcmd_utils-0.1.0 (c (n "rawcmd_utils") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0bs1w1vx8m9sqqyvzyg0hj1q9r09glvkwpdih7w796jiml55jn90")))

(define-public crate-rawcmd_utils-0.1.1 (c (n "rawcmd_utils") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "15wybhn3l453dkddl23shdm3j6ajqx28r4dj6hv66zh9xwdmcbh2")))

