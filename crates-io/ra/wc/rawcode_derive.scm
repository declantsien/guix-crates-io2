(define-module (crates-io ra wc rawcode_derive) #:use-module (crates-io))

(define-public crate-rawcode_derive-0.1.0 (c (n "rawcode_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v115wkimk0cv460rxiv3z6y40nixcxvfba317fjzzbz7yqya6h4") (f (quote (("default"))))))

(define-public crate-rawcode_derive-0.2.0 (c (n "rawcode_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1alw6k9lgvz3z88siij0amafhwnzvdw72wzbyz4p906ijc08r31x") (f (quote (("default"))))))

(define-public crate-rawcode_derive-0.2.1 (c (n "rawcode_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pzkj0z04a8wzpwrs6x6yyvr9z27ykjbzlw8dccsdb8l7rcw6r48") (f (quote (("default"))))))

(define-public crate-rawcode_derive-0.3.0 (c (n "rawcode_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1azh45kbk1qjjh01vm8v1iillcrxcbz1wzkxigg8kdzaishnzr2l") (f (quote (("default"))))))

