(define-module (crates-io ra wc rawcode) #:use-module (crates-io))

(define-public crate-rawcode-0.1.0 (c (n "rawcode") (v "0.1.0") (d (list (d (n "rawcode_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "071avr2lszn5r305gdhj9gcjd24r5lvm14vc4p483jjkf92i25d6") (f (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.2.0 (c (n "rawcode") (v "0.2.0") (d (list (d (n "rawcode_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1xh8hhs7xyfq9a4y3j8w0kijwwp4rdfbxfzpnl5m10rn8va4japl") (f (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.2.1 (c (n "rawcode") (v "0.2.1") (d (list (d (n "rawcode_derive") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0sc50wy1l3h9rjhwc8y69n5a251kgdqxk6fxpifcykidnizlvng6") (f (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.3.0 (c (n "rawcode") (v "0.3.0") (d (list (d (n "rawcode_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0253w32s7fmdf0faqzq3xwpmw0bz8f5k6ajqrx0z25fvc4dz4wq1") (f (quote (("std") ("derive" "rawcode_derive") ("default" "std"))))))

(define-public crate-rawcode-0.3.1 (c (n "rawcode") (v "0.3.1") (d (list (d (n "rawcode_derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1pg3r17039rv9dy2wnr1l61vpc80bnw0g79jvw4pwjas2dvsflj7") (f (quote (("std") ("derive" "rawcode_derive") ("default" "std"))))))

