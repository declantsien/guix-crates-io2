(define-module (crates-io ra wc rawcopy-rs-next) #:use-module (crates-io))

(define-public crate-rawcopy-rs-next-0.1.3 (c (n "rawcopy-rs-next") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "normpath") (r "^1.2.0") (d #t) (k 0)) (d (n "ntfs") (r "^0.4.0") (d #t) (k 0)))) (h "0q7cpn0cnjmc7wy4c37g37sxl5w304cxpvj6swfm4cv61nlidr8s")))

