(define-module (crates-io ra wc rawcmd) #:use-module (crates-io))

(define-public crate-rawcmd-0.0.0 (c (n "rawcmd") (v "0.0.0") (h "07f420m2ab69sr58c1lskcgbwx5r8v0j1gzachmyfm4dh9sh8nbv")))

(define-public crate-rawcmd-0.1.0 (c (n "rawcmd") (v "0.1.0") (h "0hlrmnp5z2jh4v01w2dj3gv5rp45ygw7jghfw90lrn01nfix4i9m")))

(define-public crate-rawcmd-0.2.0 (c (n "rawcmd") (v "0.2.0") (h "0zqpzvz52dy6h51mynchljzfbmb88m3c6vimjpfvy904x5qjvy7i")))

(define-public crate-rawcmd-0.2.1 (c (n "rawcmd") (v "0.2.1") (h "1bz7i3w5h6nlvz0pl626lcf81cxw8rfgv7kvbf9q9sp4gg61wsda")))

(define-public crate-rawcmd-0.2.2 (c (n "rawcmd") (v "0.2.2") (h "0jh0236339z0kqibqxfqn8labl8zgmvwh3nhhpw5vlm8xp7w03mg")))

(define-public crate-rawcmd-0.2.3 (c (n "rawcmd") (v "0.2.3") (h "1qisvjwv3xhn8pancn9ry0c4mq65miy4wvivf89j7xsxljm3xz63")))

(define-public crate-rawcmd-0.3.0 (c (n "rawcmd") (v "0.3.0") (h "1c3ahmk0mb3zwjk4vrn6gh22yjmcy7hgij49y667l1qqv40yhyqd")))

(define-public crate-rawcmd-0.3.1 (c (n "rawcmd") (v "0.3.1") (h "18b8cqgmpjmzmbldfhrd2x9kf0fp3lm30g2l6ajrqm2j4rwcy2jy")))

(define-public crate-rawcmd-0.3.2 (c (n "rawcmd") (v "0.3.2") (h "1a3lns1gcnrn2ng2x2fhss2xp853pzgaxclvyp1j2z8lkqgskwpv")))

(define-public crate-rawcmd-0.4.0 (c (n "rawcmd") (v "0.4.0") (h "1f0q9g58cba42pd2hfr8il38mfbm2kl5akzsj4r9lfkz8cx7danv")))

(define-public crate-rawcmd-0.4.1 (c (n "rawcmd") (v "0.4.1") (h "112k6n7csfhjdviy6scay633pfk355r7dkyrl4svqd06qnxbbfzb")))

(define-public crate-rawcmd-0.4.2 (c (n "rawcmd") (v "0.4.2") (h "11rr44pr24v3fmxlinwsjcad0r2r4h9bcncagwkhl1861mxw6jlv")))

(define-public crate-rawcmd-0.4.3 (c (n "rawcmd") (v "0.4.3") (h "0dkviz9058c6w9dlv8mx17dvbai4wdjw08ic9rmbfgl0y46m93yn")))

(define-public crate-rawcmd-0.4.4 (c (n "rawcmd") (v "0.4.4") (h "03j5dkbh90l1vvyvbil30506db3wj88pvz5sbx2ydmygdp90v3gf")))

(define-public crate-rawcmd-0.4.5 (c (n "rawcmd") (v "0.4.5") (h "09jd9kvm14vskjiw9fjx88xsfwibri1yd1005szxx4zf11fgmpma")))

(define-public crate-rawcmd-0.4.6 (c (n "rawcmd") (v "0.4.6") (h "1r2xg1izc6w1zjykq3syvwprx8lmm29gkpkwmdnwz59wmq31ffha")))

(define-public crate-rawcmd-0.5.0 (c (n "rawcmd") (v "0.5.0") (h "14fdl4wjzxk38rl262pkn7a8700aizc59g5p50ms627vj4myxld6")))

(define-public crate-rawcmd-0.6.0 (c (n "rawcmd") (v "0.6.0") (h "0n42208p2xiibq8r4bvxxa68v1n5ad5jc6wdjwpmq5jw38mkywkn")))

(define-public crate-rawcmd-0.6.1 (c (n "rawcmd") (v "0.6.1") (h "17vw4vl7pr2rp7q6kddfx9h7rsdz9dl7garkr59q5wr4jhfyycjg")))

(define-public crate-rawcmd-0.6.2 (c (n "rawcmd") (v "0.6.2") (h "1bw161rczw1i4h8jn0d1dl8fjk72553rc22ddmvvgjjk60ngp2qh")))

(define-public crate-rawcmd-0.6.3 (c (n "rawcmd") (v "0.6.3") (h "1d2q2j3dv4bff622czfbb0ii8d0w0k46if7im8dxmqg98r3zs1sh")))

