(define-module (crates-io ra wc rawcopy-rs) #:use-module (crates-io))

(define-public crate-rawcopy-rs-0.1.0 (c (n "rawcopy-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "normpath") (r "^1.2.0") (d #t) (k 0)) (d (n "ntfs") (r "^0.4.0") (d #t) (k 0)))) (h "1aaywqm5g5i7js058yfhb80h05wds4nchg7aykjzihnbgsiyhal5") (y #t)))

(define-public crate-rawcopy-rs-0.1.1 (c (n "rawcopy-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "normpath") (r "^1.2.0") (d #t) (k 0)) (d (n "ntfs") (r "^0.4.0") (d #t) (k 0)))) (h "046d306qjl4sfg7kgv1x4ck7f95hx1hgisqc550hx0pdp1mwgi4g") (y #t)))

(define-public crate-rawcopy-rs-0.1.2 (c (n "rawcopy-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "normpath") (r "^1.2.0") (d #t) (k 0)) (d (n "ntfs") (r "^0.4.0") (d #t) (k 0)))) (h "0g879nlcnp1pgnlj04rq3ksd90vd9s3syiig24lb3a6yzd3k158k")))

(define-public crate-rawcopy-rs-0.1.3 (c (n "rawcopy-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "normpath") (r "^1.2.0") (d #t) (k 0)) (d (n "ntfs") (r "^0.4.0") (d #t) (k 0)))) (h "1vpbwva2vfm2cs920z3lrhq3b6bh1rs9z25mbf80jzzr3bzygx0g")))

