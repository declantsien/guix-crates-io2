(define-module (crates-io ra ng ranged) #:use-module (crates-io))

(define-public crate-ranged-0.1.0 (c (n "ranged") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0nr8ly7a1nl1yn363jhl0gc7zkqhg4sqd29p6hcb1jyi4pzgl4yz")))

(define-public crate-ranged-0.1.1 (c (n "ranged") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1vr2yg9q9h6r0hs5i7hs361pk0q07r117ay641wr9dx9clmm7mzz")))

