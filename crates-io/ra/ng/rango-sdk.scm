(define-module (crates-io ra ng rango-sdk) #:use-module (crates-io))

(define-public crate-rango-sdk-0.0.1 (c (n "rango-sdk") (v "0.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "ureq") (r "^2.0") (f (quote ("json"))) (d #t) (k 0)))) (h "12rk1lf4kq4kgam9jbzq97gs0708bvmn5csjn6wrz82sjqwizwc1")))

