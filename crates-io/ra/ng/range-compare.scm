(define-module (crates-io ra ng range-compare) #:use-module (crates-io))

(define-public crate-range-compare-0.1.0 (c (n "range-compare") (v "0.1.0") (h "1vq6r8nbax6g6p6jkzbrp9ji71y9rm1frlmhlkfc5chf0pidwdj5")))

(define-public crate-range-compare-0.1.1 (c (n "range-compare") (v "0.1.1") (h "1jk15prgbmzv35m927jp6hzjkqrwfc9z23gjb6vpamn5xcrncvia")))

