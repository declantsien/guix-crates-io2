(define-module (crates-io ra ng range-action-map) #:use-module (crates-io))

(define-public crate-range-action-map-0.1.0 (c (n "range-action-map") (v "0.1.0") (h "090adpmcx17jhdd25hb1n8r3wh393az9zd6cddjyqs9lnkllmj68") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.1 (c (n "range-action-map") (v "0.1.1") (h "1zqjkvq1fj9sdzwm2nwb9c2jxn3sgnf4jmyianpy0zqqdf0ihlvc") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.2 (c (n "range-action-map") (v "0.1.2") (h "19a40rv7n62m9jm2ws3cclsa4612aq3ksjic7h8kfm0vfp7nyczd") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.3 (c (n "range-action-map") (v "0.1.3") (h "0f76i8lrnsgqrv3nlvm4mazqgss6aq9spz2sl5v7ih7ka879js82") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.4 (c (n "range-action-map") (v "0.1.4") (h "0j335avmsmyfpz1389bgh41xj4ishwzw9a2wfy6gzs6qgl61pdn5") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.5 (c (n "range-action-map") (v "0.1.5") (h "05r5yihfxhcr6ifj4bxsx1b200h8aay520lrbghnid1iilx5pn12") (f (quote (("std") ("default" "std"))))))

(define-public crate-range-action-map-0.1.6 (c (n "range-action-map") (v "0.1.6") (h "1l76kcmg8gyrjc1xvhmaa9vjz8clnrnamsiidhxlylnq7grw0il8") (f (quote (("std") ("default" "std"))))))

