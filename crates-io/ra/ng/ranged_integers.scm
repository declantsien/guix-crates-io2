(define-module (crates-io ra ng ranged_integers) #:use-module (crates-io))

(define-public crate-ranged_integers-0.3.0 (c (n "ranged_integers") (v "0.3.0") (h "16ghid9lka8gh9xzxqmyvdvdwky0h7d7w2yp7qj167wb5ia96f65")))

(define-public crate-ranged_integers-0.3.1 (c (n "ranged_integers") (v "0.3.1") (h "18zmia9hz0xi7rsfckhpa5l00vdgi5pswa0xicisl4rc5n075d1l")))

(define-public crate-ranged_integers-0.4.0 (c (n "ranged_integers") (v "0.4.0") (h "04nypz4qmslv9zvsy3n6vgdslmnn0m1amigk0zsii1yh3x1ivhzi")))

(define-public crate-ranged_integers-0.4.1 (c (n "ranged_integers") (v "0.4.1") (h "0m1i15nys5fc0cdzwmqfpagv6sv51v2sfrls5yvk9ixx2px308x1")))

(define-public crate-ranged_integers-0.4.2 (c (n "ranged_integers") (v "0.4.2") (h "1bzn32lnp44mk1h40ahm9jf5srsxv017ag7x95blq1096f0g1fl5")))

(define-public crate-ranged_integers-0.5.0 (c (n "ranged_integers") (v "0.5.0") (h "1yak8bpbyymxd3180bmyikyp9nci5pxyppm1c5ax04kadr3g7hps")))

(define-public crate-ranged_integers-0.5.1 (c (n "ranged_integers") (v "0.5.1") (h "0gxz2m8pszwiv9hjwj8kx18cj2sjhq81ianxc86088hq1vi7saam")))

(define-public crate-ranged_integers-0.6.0 (c (n "ranged_integers") (v "0.6.0") (h "1ri4x5x5x48vfdcnja17f6w02kskq2v1qzli8741fbm5ivnwr1cg")))

(define-public crate-ranged_integers-0.7.0 (c (n "ranged_integers") (v "0.7.0") (h "0hkfv7z2gxz3rsrlcd8j0iic1wr1m9dvrhykq89mb8kp55ib0p11")))

(define-public crate-ranged_integers-0.7.1 (c (n "ranged_integers") (v "0.7.1") (h "1ghwyslhrpb9rprb793svjir8pp4lv7zpjczky5f27c90rfq74l0")))

(define-public crate-ranged_integers-0.8.0 (c (n "ranged_integers") (v "0.8.0") (h "0c4gi10i1xbk5xa8xv02hrpww7f1fsld11qm92vy2c4a7xs2b2yx")))

