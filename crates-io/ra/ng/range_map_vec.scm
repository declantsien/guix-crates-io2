(define-module (crates-io ra ng range_map_vec) #:use-module (crates-io))

(define-public crate-range_map_vec-0.1.0 (c (n "range_map_vec") (v "0.1.0") (h "158pmz48l0v06mwsw3mzlcn383d6y0cs45pgqv26pkw3lpm8kp4f")))

(define-public crate-range_map_vec-0.2.0 (c (n "range_map_vec") (v "0.2.0") (h "0a0kwgpgkhxpw6b3xxqi25bdycqask7rrw2cvqz0x1gxq4g1khkw")))

