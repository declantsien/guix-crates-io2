(define-module (crates-io ra ng rangeinfo) #:use-module (crates-io))

(define-public crate-rangeinfo-0.1.0 (c (n "rangeinfo") (v "0.1.0") (h "0555iwa3jl082j8ijzyf3k1q51v6dj2g6naqdmxf0l28086g6bib")))

(define-public crate-rangeinfo-0.1.1 (c (n "rangeinfo") (v "0.1.1") (h "0kpg7w44yrrh74ds4k3ar26d8xbk6z2f0iijv5yx65qadm09v5g9")))

