(define-module (crates-io ra ng rangoon) #:use-module (crates-io))

(define-public crate-rangoon-0.0.0 (c (n "rangoon") (v "0.0.0") (d (list (d (n "hecs") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (k 0)) (d (n "web") (r "^0") (d #t) (k 0)))) (h "0fdi3hhi8c7jnriyz7mklg1vnrslq4qdhbddascn1cd5m7ajldg7")))

