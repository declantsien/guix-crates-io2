(define-module (crates-io ra ng range_check) #:use-module (crates-io))

(define-public crate-range_check-0.1.0 (c (n "range_check") (v "0.1.0") (h "1js8ffcpvws562n22c5bqzkx6sw57b6178kd6zw6wxab9mz9m5i1")))

(define-public crate-range_check-0.2.0 (c (n "range_check") (v "0.2.0") (h "10yqi9gjldl1p0afzf4rqhz0s85fpbca988d4bmqi3svfwz9v6yn")))

