(define-module (crates-io ra ng range-traits) #:use-module (crates-io))

(define-public crate-range-traits-0.1.0 (c (n "range-traits") (v "0.1.0") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "05ha3h02d2x63d1q83l0kbziyl3f5kr93hrrcpmmbzanwirx4ws4")))

(define-public crate-range-traits-0.2.0 (c (n "range-traits") (v "0.2.0") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "1c7mhisshmvjacfwzw73lmxpx1w9ri3cjjzps3sdzyyfniw7a140") (y #t)))

(define-public crate-range-traits-0.3.0 (c (n "range-traits") (v "0.3.0") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "0b59ipxkmpif3zg9l8qz51vhvx28ad8mb31vhwb40aq4vaa6d8ly")))

(define-public crate-range-traits-0.2.1 (c (n "range-traits") (v "0.2.1") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "0w62m4bijmavcmxv4hwf98484gyk20layhbf1wgr6vw30n5g15vy")))

(define-public crate-range-traits-0.3.1 (c (n "range-traits") (v "0.3.1") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "0nfw7agdmaz5hvhwphyi3wak579fbdv2xfqvsjdhabycg57n11bx")))

(define-public crate-range-traits-0.3.2 (c (n "range-traits") (v "0.3.2") (d (list (d (n "ordered-float") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "1ay8ghrp7phr8z1l2kg9fcszwjki5d0s5wfzqw9sjvyp5mrq21fj")))

