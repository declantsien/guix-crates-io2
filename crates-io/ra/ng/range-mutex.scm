(define-module (crates-io ra ng range-mutex) #:use-module (crates-io))

(define-public crate-range-mutex-0.1.0 (c (n "range-mutex") (v "0.1.0") (h "06rfn62p8hrh85m13j74m37yy35snbrq0m8labis4i454pmkyc03") (y #t)))

(define-public crate-range-mutex-0.1.1 (c (n "range-mutex") (v "0.1.1") (h "16ii71shfp8m2kwx3wldgin6i93ninff0bwinnb6x6j08hgrj9g6") (y #t)))

(define-public crate-range-mutex-0.1.2 (c (n "range-mutex") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "05wn1lzblhsrrg3q71470x6yhd7apz4q8gqbx7jgj4y5135l2phy") (y #t)))

(define-public crate-range-mutex-0.1.3 (c (n "range-mutex") (v "0.1.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "09hx262h3wdafgi89dm3g56dj801z08zn592zr84n7dgs4bx66m8") (f (quote (("default" "async") ("async")))) (y #t)))

(define-public crate-range-mutex-0.1.4 (c (n "range-mutex") (v "0.1.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "1555rfmxvlzbzazdrq1cwg47m7nsj558xgj6m54fxhs1km728c9m") (f (quote (("default" "async") ("async"))))))

(define-public crate-range-mutex-0.1.5 (c (n "range-mutex") (v "0.1.5") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "076qp136vh7sqmhvgng6wpppsiq1zw88jj834fg5bbcxm8k4a3qx") (f (quote (("default" "async") ("async"))))))

(define-public crate-range-mutex-0.1.6 (c (n "range-mutex") (v "0.1.6") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "time" "rt" "test-util"))) (d #t) (k 2)))) (h "1k8bdszgdk4ic5vazsv0iyqbnw7v6ydhl0ccf0k8nk6wshrdv9aj") (f (quote (("default" "async") ("async"))))))

