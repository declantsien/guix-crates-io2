(define-module (crates-io ra ng range-set) #:use-module (crates-io))

(define-public crate-range-set-0.0.3 (c (n "range-set") (v "0.0.3") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "0kdwzh27brhznfch7yfjkr6qg668dvs11llpcjgy9j315g0mwii5")))

(define-public crate-range-set-0.0.4 (c (n "range-set") (v "0.0.4") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "1qakpglksg55hfjav59bmpmfl746sb1mjf495539nj7vfr85m7jc")))

(define-public crate-range-set-0.0.5 (c (n "range-set") (v "0.0.5") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "04cdz5x2rbf1np81gjxvb5ls9475vb5ra3vkx292syp828s3c775")))

(define-public crate-range-set-0.0.6 (c (n "range-set") (v "0.0.6") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "0kzx3h7gjj5z2g3mgcq7yh1hws0m231pkx9crhnswbkh5qzjrc2y")))

(define-public crate-range-set-0.0.7 (c (n "range-set") (v "0.0.7") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "16is86j359pkdsypvcs0lc2qnixgdlmjpmilcjijnj0gdyrfszg6")))

(define-public crate-range-set-0.0.8 (c (n "range-set") (v "0.0.8") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "smallvec") (r "0.6.*") (d #t) (k 0)))) (h "198fsiaabbnrnnhpww4v7zkccv7clpyds814yp1savvzx4hvly12")))

(define-public crate-range-set-0.0.9 (c (n "range-set") (v "0.0.9") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde_crate") (r "1.*") (o #t) (d #t) (k 0) (p "serde")) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "1p7qhimcqahg9mhb03lh6z4pbkyb90vpr58f1kw81a7gzvzcqn35") (f (quote (("serde" "serde_crate" "smallvec/serde"))))))

(define-public crate-range-set-0.0.10 (c (n "range-set") (v "0.0.10") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "serde") (r "1.*") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "04l8fs1033gs12lilhny2a9yigqds5w8s0w19gbdrb13d4krjkpm") (f (quote (("derive_serdes" "serde" "smallvec/serde"))))))

(define-public crate-range-set-0.0.11 (c (n "range-set") (v "0.0.11") (d (list (d (n "num-traits") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.8.*") (d #t) (k 2)) (d (n "rand_xorshift") (r "0.3.*") (d #t) (k 2)) (d (n "serde") (r "1.*") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "1.*") (d #t) (k 0)))) (h "0l0hnbjxskik2vh2640rsskz60if057rnzhphampg7rrkj2c8jvi") (f (quote (("derive_serdes" "serde" "smallvec/serde"))))))

