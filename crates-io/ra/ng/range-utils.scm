(define-module (crates-io ra ng range-utils) #:use-module (crates-io))

(define-public crate-range-utils-0.1.0 (c (n "range-utils") (v "0.1.0") (h "12zg4qfmgl82nma24cjpw1d55w0jhk1hl460h5vp9l81imngk53m")))

(define-public crate-range-utils-0.1.1 (c (n "range-utils") (v "0.1.1") (h "11asi776v9r2ib94wp851csyahhg5h0jyrwvjqn26b4qsnvx6hzw") (y #t)))

(define-public crate-range-utils-0.1.2 (c (n "range-utils") (v "0.1.2") (h "0kbqyiagi4p0s183a85fgivamxwkvs1225bvc14n5vv7lkd5mbg5")))

(define-public crate-range-utils-0.1.3 (c (n "range-utils") (v "0.1.3") (h "0kh4xsmqlcchfc7q69vz7w1knqpwny8y6mdlqrvqfamx72jiq0xi")))

