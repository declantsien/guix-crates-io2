(define-module (crates-io ra ng range-alloc) #:use-module (crates-io))

(define-public crate-range-alloc-0.1.0 (c (n "range-alloc") (v "0.1.0") (h "1y8lgdbn151h0bvvj9fz9dy0hm84755pwzfk2nvyia93cy9jfnfx")))

(define-public crate-range-alloc-0.1.1 (c (n "range-alloc") (v "0.1.1") (h "0qqsy1zw5an759yf4hhnm11m3frq2a0kqd30zdrhqgrsbbjg2wd8")))

(define-public crate-range-alloc-0.1.2 (c (n "range-alloc") (v "0.1.2") (h "1diq0lpp4649qrzx88q26aal13x563bb5w6j037nvk09bv23bsb3")))

(define-public crate-range-alloc-0.1.3 (c (n "range-alloc") (v "0.1.3") (h "1azfwh89nd4idj0s272qgmw3x1cj6m7d3f44b2la02wzvkyrk2lw")))

