(define-module (crates-io ra ng rangetools) #:use-module (crates-io))

(define-public crate-rangetools-0.1.0 (c (n "rangetools") (v "0.1.0") (h "1j837h7v4ldgnbhdbxzi4k0zp7ygbq3yf9bxcs6m30shk3qfwm6h")))

(define-public crate-rangetools-0.1.1 (c (n "rangetools") (v "0.1.1") (h "0xf1gsdry3ddh64a79bll54w3xxg64sl4zfxb7xawqgdfxypvsgf")))

(define-public crate-rangetools-0.1.2 (c (n "rangetools") (v "0.1.2") (h "0jv26nq07v3yc3x5a14ixw0xzlvh6yjgc2f8jl9j2qplkim2dc82")))

(define-public crate-rangetools-0.1.3 (c (n "rangetools") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lblh2zczlaaigwrc6brmfgbg2ax0mj1jyzrx2kxqqgq5xqpazrn")))

(define-public crate-rangetools-0.1.4 (c (n "rangetools") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cwndba0b4zrgjmkq8m9y8s9zj1pxw69zq6g6dh1xn5bvfiprsg6")))

