(define-module (crates-io ra ng range-regex) #:use-module (crates-io))

(define-public crate-range-regex-0.1.0 (c (n "range-regex") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "02aga9n8kbs5wivj54nfd61q81hadv043k0ypvqa8y12ypkbah5n")))

