(define-module (crates-io ra ng ranged-num) #:use-module (crates-io))

(define-public crate-ranged-num-0.1.0 (c (n "ranged-num") (v "0.1.0") (d (list (d (n "typenum") (r ">=1.12.0, <2.0.0") (d #t) (k 0)))) (h "00cjfrr0gxz6jqb64zmj06ajl9bh11wpqvb457civrbycv9my6x8")))

(define-public crate-ranged-num-0.1.1 (c (n "ranged-num") (v "0.1.1") (d (list (d (n "typenum") (r ">=1.12.0, <2.0.0") (d #t) (k 0)))) (h "1z1vjqg3i2jcyvjziwgbxz2ki4bh51qd0678kf9aciyr2lnbzxyw")))

