(define-module (crates-io ra ng range-map) #:use-module (crates-io))

(define-public crate-range-map-0.1.1 (c (n "range-map") (v "0.1.1") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1mpgbrapvnywx3kwy8ksrrvdcyjrh8ya1w9h5dzmbpjavv50n78z")))

(define-public crate-range-map-0.1.2 (c (n "range-map") (v "0.1.2") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "01zsj11i3s4pjpksk0xzv5wd3wbwkniyd3l8njwqbc9w82ykll8b")))

(define-public crate-range-map-0.1.3 (c (n "range-map") (v "0.1.3") (d (list (d (n "num-iter") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0jr3nn3ywbk8dr3iw9qsygjfj565hb2nkk018dmp5phbdvr8dayk")))

(define-public crate-range-map-0.1.5 (c (n "range-map") (v "0.1.5") (d (list (d (n "num-iter") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1qa2k85ha40hiskybxznpg5a58wi04n5k774lsxjvqzkn3rqzp47")))

(define-public crate-range-map-0.2.0 (c (n "range-map") (v "0.2.0") (d (list (d (n "num-iter") (r "^0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)))) (h "0pz9mrj55s6g67as86pndn0ml8g1kcwa8whlcapmk403qzba598j")))

