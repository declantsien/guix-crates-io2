(define-module (crates-io ra ng range-ext) #:use-module (crates-io))

(define-public crate-range-ext-0.1.0 (c (n "range-ext") (v "0.1.0") (h "0xzb12l5jmd6im171r8hzd46q0n1df5sv1bia6xc2a0v4y0678cg") (y #t)))

(define-public crate-range-ext-0.1.1 (c (n "range-ext") (v "0.1.1") (h "19mznzpiw67lqnhbz2gp3xbba754i016zb83c470mns729jnllb7") (y #t)))

(define-public crate-range-ext-0.1.2 (c (n "range-ext") (v "0.1.2") (h "0h5i6iwxzjlp7yb5wi92jks7p3m7bf6i0yznpmds37vscgnklj5y") (y #t)))

(define-public crate-range-ext-0.1.3 (c (n "range-ext") (v "0.1.3") (h "0qr7048x6k1mpqcbfnk3dkyvbkfbaa1122jhd9ja0v30dpjid7lj")))

(define-public crate-range-ext-0.2.0 (c (n "range-ext") (v "0.2.0") (h "0zxsknplpqfdfdswr8mghsy6kp5w2llznqixcbg7p9fq7q22a5zz")))

(define-public crate-range-ext-0.3.0 (c (n "range-ext") (v "0.3.0") (h "0wh8dhnq0l3f873kzml9806h8w0bpakvnk7y6mgsr6b93kv2iycf")))

