(define-module (crates-io ra ng ranges) #:use-module (crates-io))

(define-public crate-ranges-0.1.0 (c (n "ranges") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "0rphdhqsqw1272rcgah8vpq8pa11fblgirswc3mbf7n7x99ayrr7") (f (quote (("inline")))) (y #t)))

(define-public crate-ranges-0.2.0 (c (n "ranges") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0z3hcb10jf49x85kghqlv5frgczkac4lcqwr56qsmss468b08885") (y #t)))

(define-public crate-ranges-0.2.1 (c (n "ranges") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "1j0x6rlacglpmvdna98njp4j1s17x0yvbgw6wn2vh9p4kj0vk7dq") (y #t)))

(define-public crate-ranges-0.3.0 (c (n "ranges") (v "0.3.0") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "180s7s91q2xlkh1r77hyvxpxraq7l3w1a1w0c5z8hrd69h34cy8q")))

(define-public crate-ranges-0.3.1 (c (n "ranges") (v "0.3.1") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "026c1b9sd11x3zm766pan2icnlgbiyxmpyx55v9fvwx5d591s7ls")))

(define-public crate-ranges-0.3.2 (c (n "ranges") (v "0.3.2") (d (list (d (n "arbitrary") (r "^0.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)))) (h "113m3655h88c506fxcvikhmalwx06p3l51qmfy8q4msx27v0b9vi")))

(define-public crate-ranges-0.3.3 (c (n "ranges") (v "0.3.3") (d (list (d (n "arbitrary") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "noisy_float") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0j1r1vaq6py6rxqgfvyz5szwj2kym0rnxllzw1vxyf28rn8x70gp")))

