(define-module (crates-io ra ng range_union_find) #:use-module (crates-io))

(define-public crate-range_union_find-0.1.0 (c (n "range_union_find") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0y44p4l5fzzv9p6ld1g0mzyy50w5j9dgbjpizqvlkskkznh34m33")))

(define-public crate-range_union_find-0.2.0 (c (n "range_union_find") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1mcfbi1lyadkr245z7412bkf4f0fx4wwv08h15rbfjsn2zibsbcf")))

(define-public crate-range_union_find-0.2.1 (c (n "range_union_find") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)))) (h "1ayrh1mdma47z6q5057n3yk62fx4wv33iyszlk319m6lg18rrgip")))

(define-public crate-range_union_find-0.2.2 (c (n "range_union_find") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)))) (h "16mbsr5n9vydfhkcm52n7c7wwsc7bk3gczh4h1ndi8ri11cgmbh8")))

(define-public crate-range_union_find-0.3.0 (c (n "range_union_find") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0rnycc2nkych6s0i40qm4jzpzjcb43v9nwbrdfnngh3g2hchnh5s")))

(define-public crate-range_union_find-0.4.0 (c (n "range_union_find") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1j7w031bxv7q77racg08ss274j16az2dp3s13na09f7i68w0lxdc") (f (quote (("std") ("include_serde" "serde" "sorted-vec/serde") ("default" "std")))) (y #t)))

(define-public crate-range_union_find-0.4.1 (c (n "range_union_find") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.5.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1w029miyl4jkpzzksdbawyhhcjp5a27anhpvawm0if24v2dhgzq7") (f (quote (("std") ("include_serde" "serde" "sorted-vec/serde") ("default" "std"))))))

(define-public crate-range_union_find-0.4.2 (c (n "range_union_find") (v "0.4.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r ">=0.9.3, <0.10.0") (f (quote ("html_root_url_updated"))) (k 2)))) (h "15s2z9cqhmhigjdcgsfylrpwwjqaskf91r5nz3i8q1abc5y21fpf") (f (quote (("std") ("include_serde" "serde" "sorted-vec/serde") ("default" "std"))))))

(define-public crate-range_union_find-0.4.3 (c (n "range_union_find") (v "0.4.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r ">=0.9.3, <0.10.0") (f (quote ("html_root_url_updated"))) (k 2)))) (h "0n3sgj28g2iw8x9m12m8hl10ify26l9mjdj15d1kjn1yq1akjpp9") (f (quote (("std") ("include_serde" "serde" "sorted-vec/serde") ("default" "std"))))))

(define-public crate-range_union_find-0.4.4 (c (n "range_union_find") (v "0.4.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r ">=0.9.3, <0.10.0") (f (quote ("html_root_url_updated"))) (k 2)))) (h "0fii9w0zpd1c0zwl6rji7ah9vy0qn0dnlc4kk7japs6sxbhgnfhm") (f (quote (("std") ("include_serde" "serde" "sorted-vec/serde") ("default" "std"))))))

(define-public crate-range_union_find-0.5.0 (c (n "range_union_find") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sorted-vec") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r ">=0.9.3, <0.10.0") (f (quote ("html_root_url_updated"))) (k 2)))) (h "0fy62jb2i4229ij7szc6jyyfi5llrpnj5rkbgcvagkj2frbs6335") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("include_serde" "serde" "sorted-vec/serde-nontransparent") ("default" "std"))))))

