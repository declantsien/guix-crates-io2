(define-module (crates-io ra ng range_rover) #:use-module (crates-io))

(define-public crate-range_rover-0.1.0 (c (n "range_rover") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0v14rpkx35gq0qarbq3g0pz3cjaqiadgvbi21dqcw8g2irp51z8p")))

(define-public crate-range_rover-0.1.1 (c (n "range_rover") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fcy7p7zjvysclnbz9lm551sd2x1p916h0l8wfxi41r5p0dprxc0")))

(define-public crate-range_rover-0.1.2 (c (n "range_rover") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15khx6cp61zmj5zrm8nd0l96pwz0srpzrrm6zj5ny6ygyslfr33j")))

