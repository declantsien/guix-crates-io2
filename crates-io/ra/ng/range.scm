(define-module (crates-io ra ng range) #:use-module (crates-io))

(define-public crate-range-0.0.0 (c (n "range") (v "0.0.0") (h "1jg4x5sqm1jk2wzq6z3j9d826b86lviprjvpfkx9mckz8i4s5kmw")))

(define-public crate-range-0.0.1 (c (n "range") (v "0.0.1") (h "0cihb5kfar4cbdpvgn4xyfacviwiksf2pn718vmnclq4wdp7p8lf")))

(define-public crate-range-0.0.5 (c (n "range") (v "0.0.5") (h "1wwma2j5lh6q8c0iga9s1h7a0qkaywwqvq9cid62mswm3pl2lwx9")))

(define-public crate-range-0.0.6 (c (n "range") (v "0.0.6") (h "07n1al81jx9wq7vv00jbbjqydvdq29417jz8rbjszscb0h1fw76j")))

(define-public crate-range-0.0.7 (c (n "range") (v "0.0.7") (h "1sxcfnhhbr7v6grllax35ngddkskq95rbnb14iqvsizjhxvrf2jx")))

(define-public crate-range-0.0.9 (c (n "range") (v "0.0.9") (h "1zlxgf9d8vqxprdpyyb9bvyxv5af39h1w4bx8av13v9qi3dyhpcf")))

(define-public crate-range-0.0.10 (c (n "range") (v "0.0.10") (h "0fx2i5fs9knx73vaygwk25ia586s1g7jar8k2r21rscwj9fm5r66")))

(define-public crate-range-0.1.0 (c (n "range") (v "0.1.0") (h "1dgsyyrva1d8h94jjlcfjm2dg5qsvmn4dkvkspglqv2s0chqga09")))

(define-public crate-range-0.1.1 (c (n "range") (v "0.1.1") (h "0fdz3ydjhyzda2b9q2j7mhg3qffm7sidh3nryvq1hqi2jzx4nrw7")))

(define-public crate-range-0.2.0 (c (n "range") (v "0.2.0") (h "1jzc4kqb1idrhzll3sf0wj6xyp28g2wsz80mqca3l5ddfpbhq9qq")))

(define-public crate-range-0.3.0 (c (n "range") (v "0.3.0") (h "0ywvjjd9wmyfi6s0i8qabhfw884af87p3wzw3cx0qq77l3vgx46k")))

(define-public crate-range-0.3.1 (c (n "range") (v "0.3.1") (h "12cslm7qkxs7qs9cydjcz8w2jh3q3lbsb7ijbcq7hbil8an04x2y")))

(define-public crate-range-1.0.0 (c (n "range") (v "1.0.0") (h "1wd6gd0dbxqzsdbf0mlnan0qrmaxykm2x8fy61fvrbdj4dr1v1av")))

