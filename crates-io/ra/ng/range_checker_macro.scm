(define-module (crates-io ra ng range_checker_macro) #:use-module (crates-io))

(define-public crate-range_checker_macro-0.1.0 (c (n "range_checker_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n9qxr2dnksni7p9gy036fx322bfgh7d6waxf57b6iv75dybhs58")))

(define-public crate-range_checker_macro-0.2.0 (c (n "range_checker_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yjswx0c1a2sgwi95ngcrbl10mx1wd5xhzk9i3s71mn4n935p59m")))

(define-public crate-range_checker_macro-0.2.1 (c (n "range_checker_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08jb92iqiqbim6vh78rlj53w7jvbc5xg4p4w1b45708mx6rd7pf5")))

(define-public crate-range_checker_macro-0.2.2 (c (n "range_checker_macro") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jzkz4lxvnvf8bc8d3wyy4hdapv6agh122nn18pr36di6vg39in1")))

