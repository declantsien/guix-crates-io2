(define-module (crates-io ra ng rangex) #:use-module (crates-io))

(define-public crate-rangex-0.1.0 (c (n "rangex") (v "0.1.0") (d (list (d (n "async-graphql") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlx") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "sqlx") (r "^0.5.5") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 2)))) (h "1scc9cvjz26vypljzxy2b61yava8312gdb7l1wf26hfq21a3azk2")))

(define-public crate-rangex-0.1.1 (c (n "rangex") (v "0.1.1") (d (list (d (n "async-graphql") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlx") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "sqlx") (r "^0.5.5") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 2)))) (h "1g8mymk3n3r9z99q20zgfi5qg3xjncvbhnk2dm55i1jnp51vjbsv")))

(define-public crate-rangex-0.2.0 (c (n "rangex") (v "0.2.0") (d (list (d (n "async-graphql") (r "^4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlx") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 2)))) (h "0762m3jydkmb01nznl47ymsrphl80ll11nkg6a8xdy4qbfx0m5wk")))

