(define-module (crates-io ra ng range_minimum_query) #:use-module (crates-io))

(define-public crate-range_minimum_query-0.1.0 (c (n "range_minimum_query") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fqyk8c50v96k022qmvrgm9a0gs8jmgqwdprz535liyws0qvi321") (s 2) (e (quote (("serde" "dep:serde" "bitvec/serde"))))))

