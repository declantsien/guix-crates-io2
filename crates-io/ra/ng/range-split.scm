(define-module (crates-io ra ng range-split) #:use-module (crates-io))

(define-public crate-range-split-0.1.0-beta.1 (c (n "range-split") (v "0.1.0-beta.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0dg87xm91dh5y8gbkc4c8w8da23xf1z1gsnd1f4ag73w0g1s84m8") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.1.0 (c (n "range-split") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1rjld594sgsb9gwhfnmcjddix0qabsw3ciifl3f7nj7cis679nhq") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.2.0-beta.1 (c (n "range-split") (v "0.2.0-beta.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "00g9cdi82c14bwmjcgqhx4qn2fx4645ml8dq2nzi8qj0vcq23gvc") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.2.0-beta.2 (c (n "range-split") (v "0.2.0-beta.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0aiwda8jv58165mp9hfqkzkl4rc44yw6qhns4pr0r2zf4rhq07xp") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.2.0 (c (n "range-split") (v "0.2.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0k8xfgc7bk0kgbkz4pv44vnsjb652kppdm66n7g7hji078sl8bfy") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.3.0 (c (n "range-split") (v "0.3.0") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0v8jr2fnnyl17zcka122jhxjyw7z5z5ims591gq1iypkhf26pvbx") (f (quote (("default" "bytes"))))))

(define-public crate-range-split-0.4.0 (c (n "range-split") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0c9dww3bfxl4hc79i7d5m6h2gdnxmibvhvzwllg959r55mjr1b10") (f (quote (("default" "bytes"))))))

