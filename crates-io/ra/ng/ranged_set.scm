(define-module (crates-io ra ng ranged_set) #:use-module (crates-io))

(define-public crate-ranged_set-0.2.0 (c (n "ranged_set") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)))) (h "0q7rznq3spxxp3q9kilg3n6z4ig29fr7bw6g1s5g1l27p3x6gprr")))

(define-public crate-ranged_set-0.3.0 (c (n "ranged_set") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "step") (r "^0.1.0") (d #t) (k 0)))) (h "11jwzfgnf288pfg0p2q9cfqz1gnvzqri6xiicqqn3sc15yzk3m82")))

(define-public crate-ranged_set-0.4.0 (c (n "ranged_set") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "step") (r "^0.1.0") (d #t) (k 0)))) (h "0yaighmfraa8ap5vwn5ldfl3827rmrnwnvj9695zhspfrsx2n4iw")))

(define-public crate-ranged_set-0.4.1 (c (n "ranged_set") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)) (d (n "step") (r "^0.1.0") (d #t) (k 0)))) (h "1di73q5sc9bdfg3d6mfcqi90cxrna7da6sr27ci6ajzk6kkbqypg")))

