(define-module (crates-io ra ng rangetree) #:use-module (crates-io))

(define-public crate-rangetree-0.1.0 (c (n "rangetree") (v "0.1.0") (h "0x3gbi7dba9szjfh8bw3dzxlwacy2q5722n5gj8p5cx7kndyh747")))

(define-public crate-rangetree-0.1.1 (c (n "rangetree") (v "0.1.1") (h "06y48nkhzmjwbasl1008s6wsrb49aq8dwrf3ng01r07fywv0hj4j")))

(define-public crate-rangetree-0.1.2 (c (n "rangetree") (v "0.1.2") (h "0v9idgxh4nbz2rwyh1s756gb5xa28z64sa3p0rimzqzfpd62cfg1")))

