(define-module (crates-io ra ng range-encoding) #:use-module (crates-io))

(define-public crate-range-encoding-0.1.0 (c (n "range-encoding") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "09na8f1q9d42gqgxkzg995j6lf5fj63fxxjydxvv62zm9syczy4n")))

(define-public crate-range-encoding-0.1.1 (c (n "range-encoding") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0pq225h68zsdhb746ig5cgrwbpsl9bkynh79q02n5f2w3zpcpj1v")))

(define-public crate-range-encoding-0.1.2 (c (n "range-encoding") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0gkl825lb0g1d329dmcalgk9qsvcf8dca7qddlj0h1rx24v5q67l")))

(define-public crate-range-encoding-0.1.3 (c (n "range-encoding") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1b7pwlj57yq5jrba1df4z2paymsfb4xsn8l73zjzy94ga2bmhsh7")))

(define-public crate-range-encoding-0.1.4 (c (n "range-encoding") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0wfygmy4mbvcclas7i1fibsgwsgijcgvyvl2nls7npzya9ydrbv3")))

(define-public crate-range-encoding-0.1.5 (c (n "range-encoding") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1wmmfqa2x36pzmr04z739f1jmb9sjrmyxgdjzp4my7qs2873s6q7")))

(define-public crate-range-encoding-0.1.6 (c (n "range-encoding") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "0yybb6flkyy8bl2zzjpl7d3zxim4psva91ffq108995hkpi7x3a5")))

(define-public crate-range-encoding-0.1.7 (c (n "range-encoding") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "09c7kfnp2c3j0vsfw14ji41qcfw5m531ydlsc44634xlbk3xz7qy")))

(define-public crate-range-encoding-0.1.8 (c (n "range-encoding") (v "0.1.8") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1b63c8bkx57ybqy83548cjc0phdphsjacm1qdp085li58pbdc04z")))

(define-public crate-range-encoding-0.1.9 (c (n "range-encoding") (v "0.1.9") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1mw646ych1l15a5iya5c2klcyfbyhr032pb6ya4mbgj09z1b138i")))

(define-public crate-range-encoding-0.2.0 (c (n "range-encoding") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "07z3wb55kazwhgv8vikmsw9jwaajzhcv2n4qvs4xympil15w55i9")))

(define-public crate-range-encoding-0.2.1 (c (n "range-encoding") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "10q4yl4h6kh9flbi4jca1zc93kdd3qdjzzlizw6gpyl4x0yxcjn7")))

