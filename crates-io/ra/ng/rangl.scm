(define-module (crates-io ra ng rangl) #:use-module (crates-io))

(define-public crate-rangl-0.0.1 (c (n "rangl") (v "0.0.1") (h "0rd3f1m5bp4jm725237mvrms6l1b2q3qmkmqckjyspxj9ll3rkgd") (y #t)))

(define-public crate-rangl-0.0.2 (c (n "rangl") (v "0.0.2") (h "19nf1gd8p0b7sni1wdx0mri0x0j452cs0v4qg3msshkpl8rl88vg") (y #t)))

(define-public crate-rangl-0.0.5 (c (n "rangl") (v "0.0.5") (h "1k9l1fgvsf47mysf4gx4wkwm5jnsq0xjj0ia8n7lnp84i9faw1va") (y #t)))

(define-public crate-rangl-0.0.7 (c (n "rangl") (v "0.0.7") (h "0gbr3jwk8srvkv1z6758m5i9wpbcglh1rsgfnpryfbxv9bwqzrcl") (y #t)))

(define-public crate-rangl-0.1.1 (c (n "rangl") (v "0.1.1") (h "0lqf57pcavjvmbrbfq6g2qy7i166sj30wrwjmgcdgwbqpyhgmysa") (y #t)))

(define-public crate-rangl-0.1.3 (c (n "rangl") (v "0.1.3") (h "1xm8ncvbv4sw86j3rzhvbvfzrfq55492w5k4jzrip7val3a9j58n") (y #t)))

(define-public crate-rangl-0.1.4 (c (n "rangl") (v "0.1.4") (h "168ja7wds98fxyppkka4cx5izw36d8gdw2c2p0wy1wjwdnkxxc8b") (y #t)))

(define-public crate-rangl-0.1.77 (c (n "rangl") (v "0.1.77") (h "12y20m4c6slk6nvxd3snax9pylf2i9i5rmy9w66jpzz7ibi3llz7") (y #t)))

(define-public crate-rangl-0.77.0 (c (n "rangl") (v "0.77.0") (h "1ny4cmk1x9a5pr7bkx0dwysqznqds1gvcnl6jllqw95xl51q17zs") (y #t)))

(define-public crate-rangl-7.7.18 (c (n "rangl") (v "7.7.18") (h "0lz15sbx9j3jkcz2kjp8564zwijdrvdjvvjs83sx2cldkj5r9v4p") (y #t)))

(define-public crate-rangl-2018.7.7 (c (n "rangl") (v "2018.7.7") (h "0896262j7clzdqz69irfbrxd4ir6054r40dbjbg6n1hrslk1grmi") (y #t)))

(define-public crate-rangl-2019.12.13 (c (n "rangl") (v "2019.12.13") (h "1vph1p6i72cfm07rlkav5fgd9cbl5c2m9dddnz7rdi5s76z4wf8c") (y #t)))

(define-public crate-rangl-9999.999.99 (c (n "rangl") (v "9999.999.99") (h "046syaa3hsnagqxc9a3wqra35yb7ymcn0yx7gbgb5x3vzaiphrjd") (y #t)))

(define-public crate-rangl-9.9.9 (c (n "rangl") (v "9.9.9") (h "005qv0rnrmiil3n5bgvl2h8cbsjrdchzkpgljr090kj8cblr2j60") (y #t)))

(define-public crate-rangl-99999.99999.99999 (c (n "rangl") (v "99999.99999.99999") (h "04qm0g3pfs2z8m24xjbz9wq0i0cjn1d4jbjjh7ncavr8qrlim1ag") (y #t)))

(define-public crate-rangl-9999999.9999999.9999999 (c (n "rangl") (v "9999999.9999999.9999999") (h "07h35vsh6kca07faiydrpwfxsn36pvpzcyss3hvf14zy63d5vanj") (y #t)))

(define-public crate-rangl-999999999.999999999.999999999 (c (n "rangl") (v "999999999.999999999.999999999") (h "0zagv7ijj2iv3asa38r7nii1l1ax990iajsab6v0s8qjwi5aj34k")))

