(define-module (crates-io ra ng rang) #:use-module (crates-io))

(define-public crate-rang-0.1.0 (c (n "rang") (v "0.1.0") (d (list (d (n "gio") (r "^0.9.1") (d #t) (k 0)) (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)))) (h "1m529gp3bzsznfy09jnyyv77lx9j98w86msxhw7gp1nfqy95qx2m") (y #t)))

(define-public crate-rang-0.1.1 (c (n "rang") (v "0.1.1") (d (list (d (n "gio") (r "^0.9.1") (d #t) (k 0)) (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)))) (h "05bh89x1c4rb8mnaqx9g4cd82hm41zgp7z4ng9inmyww3fqljhbq")))

