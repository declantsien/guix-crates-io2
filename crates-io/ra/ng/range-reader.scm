(define-module (crates-io ra ng range-reader) #:use-module (crates-io))

(define-public crate-range-reader-0.1.0 (c (n "range-reader") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parquet2") (r "^0.7") (d #t) (k 2)) (d (n "rust-s3") (r "^0.27") (f (quote ("blocking" "futures"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "fs"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("compat"))) (d #t) (k 2)))) (h "08mm3z0l69xjiz5mrwad35y5kjf3lggr6z1g7l3q8j2q878kx2r7") (f (quote (("sync") ("default" "sync" "async") ("async" "futures"))))))

(define-public crate-range-reader-0.2.0 (c (n "range-reader") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "parquet2") (r "^0.7") (d #t) (k 2)) (d (n "rust-s3") (r "^0.27") (f (quote ("blocking" "futures"))) (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "fs"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.6") (f (quote ("compat"))) (d #t) (k 2)))) (h "1fzy3j7lzk9r5gqibqby0raql4l5r9w53yda1n432glmq8kzjcgm") (f (quote (("sync") ("default" "sync" "async") ("async" "futures"))))))

