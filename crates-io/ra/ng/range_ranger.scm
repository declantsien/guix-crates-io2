(define-module (crates-io ra ng range_ranger) #:use-module (crates-io))

(define-public crate-range_ranger-0.1.0 (c (n "range_ranger") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "18bm6r7drsg8wgmpzilk3z9lxdyhdz8vsqcxld9795jz23figmcr") (r "1.38")))

(define-public crate-range_ranger-0.1.1 (c (n "range_ranger") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)))) (h "1q4v4lc0ss75wf7bb1hb6lr7963d5vjyl7asmmvffn63mczpdn20") (r "1.38")))

