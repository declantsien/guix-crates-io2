(define-module (crates-io ra ng rangetype) #:use-module (crates-io))

(define-public crate-rangetype-0.1.0 (c (n "rangetype") (v "0.1.0") (d (list (d (n "static_assertions") (r "^0.2.5") (d #t) (k 0)))) (h "0wswj53rpqrxp58s2vd0372jssngw9vr67lx5v5kjidpcb38nhnr")))

(define-public crate-rangetype-0.1.1 (c (n "rangetype") (v "0.1.1") (d (list (d (n "static_assertions") (r "^0.2.5") (d #t) (k 0)))) (h "024n8pp4qc7rxzc1bm59cvd4s87iin71z5104x22qc93safrrhxm")))

