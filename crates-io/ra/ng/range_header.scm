(define-module (crates-io ra ng range_header) #:use-module (crates-io))

(define-public crate-range_header-0.1.0 (c (n "range_header") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "0f5yk40x53dz58848vcmw7xb082cdk9irgamwcagnn2av3qwmywi")))

(define-public crate-range_header-0.2.0 (c (n "range_header") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0d29sia22jn11in8pr350399f07lpl5fmy9cyiffpn80pjxnc47r")))

