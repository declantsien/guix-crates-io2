(define-module (crates-io ra ng ranged_type) #:use-module (crates-io))

(define-public crate-ranged_type-0.1.0 (c (n "ranged_type") (v "0.1.0") (d (list (d (n "arith_traits") (r "^0.1") (d #t) (k 0)) (d (n "arith_wrappers") (r "^0.1") (d #t) (k 0)) (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pi5l1y3715dr8x4dz0hmms109vmjikdrawwrqnlpp31i8qjf2dw")))

