(define-module (crates-io ra ng rango) #:use-module (crates-io))

(define-public crate-rango-0.1.0 (c (n "rango") (v "0.1.0") (h "1a5g5zhrfbrqh5banqg1dlsim8d2z1f1716jd9sn057bjngas1da") (y #t)))

(define-public crate-rango-0.1.1-rc.1 (c (n "rango") (v "0.1.1-rc.1") (d (list (d (n "anyhow") (r "^1.0.60") (d #t) (k 0)) (d (n "ethers") (r "^0.17.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1g479fqkxsx70zm2skgbbmbs00ncyczihh1pvrzxvcq1h10y0wcx")))

