(define-module (crates-io ra il railgun) #:use-module (crates-io))

(define-public crate-railgun-0.1.0 (c (n "railgun") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "0w3f5yw2asw4fiabyl2y3b4g1kx2byvmca984sxp9n5zprvgbr02") (y #t)))

(define-public crate-railgun-0.1.1 (c (n "railgun") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "0gg2jprnl4n9i8f2vp5zprgbvh5271rqwyjw7r90a7ylhdgf9lix") (y #t)))

(define-public crate-railgun-0.1.2 (c (n "railgun") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0-alpha.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "0p6p2ssg3283ipppplji7caj921rykmgc4r2pykjmvxm6hlbbg41") (y #t)))

