(define-module (crates-io ra il railwind_cli) #:use-module (crates-io))

(define-public crate-railwind_cli-0.0.1 (c (n "railwind_cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.1") (d #t) (k 0)))) (h "19nsr8r68wxbg41b6lqj4hw187ga5zz74dmgmjg7pyc6xxdghpnn")))

(define-public crate-railwind_cli-0.0.2 (c (n "railwind_cli") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.2") (d #t) (k 0)))) (h "0p1jxpns3qshgwgxzgrw40yhni62pwc4zpz3j7g5338b5ac9zihg")))

(define-public crate-railwind_cli-0.0.3 (c (n "railwind_cli") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.4") (d #t) (k 0)))) (h "1iaxlwjnqkgyl3h6ch1c1qq23fzahk8ndf019crlyxdk91nvdz7a")))

(define-public crate-railwind_cli-0.0.4 (c (n "railwind_cli") (v "0.0.4") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.5") (d #t) (k 0)))) (h "00srm8wndi6f8qzxzgm683syn6g9fc5wbwjyl01ialxyzz3m8lg8")))

(define-public crate-railwind_cli-0.0.5 (c (n "railwind_cli") (v "0.0.5") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.6") (d #t) (k 0)))) (h "1vfqify3gjc3hdp73i62dsd7lrp31x1rdnr3zpbrlsb1hdgxpw74")))

(define-public crate-railwind_cli-0.0.7 (c (n "railwind_cli") (v "0.0.7") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.8") (d #t) (k 0)))) (h "0h37k4nx0hfxg9xkxhbplqh67jxcs921xi1zhlfhjnp6lxd01lcg")))

(define-public crate-railwind_cli-0.0.8 (c (n "railwind_cli") (v "0.0.8") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.11") (d #t) (k 0)))) (h "1x29067hvfdg9w8iyi8i9py3pihbzxysv63nmh4plg1abbw4abvh")))

(define-public crate-railwind_cli-0.0.10 (c (n "railwind_cli") (v "0.0.10") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "railwind") (r "^0.0.12") (d #t) (k 0)))) (h "009fa68yq43s4ypzp6q83ym7kcd7ba607dl7vl6h35zgjn3cql3c")))

(define-public crate-railwind_cli-0.1.0 (c (n "railwind_cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "railwind") (r "^0.1.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yf7zx5399sanzfpzw6rc1ishzxsqayvb1qwcbxd18dgxlj1wdan")))

(define-public crate-railwind_cli-0.1.1 (c (n "railwind_cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "railwind") (r "^0.1.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hw9xiniy5g2xwdhr8nxxsi7h6wxrk8nkmqiclr8n33gzyzjfm04")))

(define-public crate-railwind_cli-0.1.2 (c (n "railwind_cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "globwalk") (r "^0.8.1") (d #t) (k 0)) (d (n "notify") (r "^5.1.0") (d #t) (k 0)) (d (n "railwind") (r "^0.1.5") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rdnn0smg26n2qkvbych4gz1yaw33xq8w1ckv2k7fz1v50sdgx0d")))

