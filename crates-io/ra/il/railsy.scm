(define-module (crates-io ra il railsy) #:use-module (crates-io))

(define-public crate-railsy-0.1.0 (c (n "railsy") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("default" "blocking" "cookies" "json" "socks"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0rak8r6l3mmsij4rvsq8jgfyc62dc28xkrhc77csk08ihcl6kask")))

