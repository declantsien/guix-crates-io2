(define-module (crates-io ra il rail) #:use-module (crates-io))

(define-public crate-rail-0.3.0 (c (n "rail") (v "0.3.0") (d (list (d (n "arrayfire") (r "^3.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1mcazs50j36b38nsbklfs9fzlzm1r45ha6vrz1cvclk03dz0mndg")))

(define-public crate-rail-0.3.1 (c (n "rail") (v "0.3.1") (d (list (d (n "arrayfire") (r "^3.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "10i94v6sa42041518abkgqkbdm2a7d76d04nb8abwir2irpyk91l")))

