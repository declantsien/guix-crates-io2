(define-module (crates-io ra il railsgun) #:use-module (crates-io))

(define-public crate-railsgun-0.1.2 (c (n "railsgun") (v "0.1.2") (d (list (d (n "tokio") (r "^1.8.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1q3c682307hky2ycqq3wlr2907gj79lhqyzp6mnnm2w63bpnx1q2")))

(define-public crate-railsgun-0.1.3 (c (n "railsgun") (v "0.1.3") (d (list (d (n "tokio") (r "^1.8.1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "1sdy35vlgsiwjs37jp8icphrcciaaqc173vr9x2wbhpy24prva5s")))

(define-public crate-railsgun-0.1.4 (c (n "railsgun") (v "0.1.4") (d (list (d (n "tokio") (r "^1.10.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "1vcwablwf2rqirnjx5sdl8189i7hn63nyj64ls6yycriy3qj3ixc")))

(define-public crate-railsgun-1.0.1 (c (n "railsgun") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "156k05mfnaagvcjpf2xgia9jm7z7g2jp4azmng7j6pbjxyb641cy") (f (quote (("unstable_try") ("default" "unstable_try"))))))

(define-public crate-railsgun-1.0.2 (c (n "railsgun") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "1r1491wrjgyk1dlsj5nfvd6rp929a0z3ga2j7kcqrw77mkvd3p1h")))

(define-public crate-railsgun-1.0.3 (c (n "railsgun") (v "1.0.3") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "0qyhkimphikx6qyi69mxl4gxdiwcpqm7sd6di816fxpp9rzcfqyd")))

(define-public crate-railsgun-2.0.0 (c (n "railsgun") (v "2.0.0") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "1wqsg9g0fcjxw4j7jba4b7xw7fc1lig903qmiszv1s6ljypipdim") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

(define-public crate-railsgun-2.0.1 (c (n "railsgun") (v "2.0.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "05iliq644h4gjc08c5fr4akzw1w90la9l63623g8qml4r4n9d50y") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

(define-public crate-railsgun-2.0.2 (c (n "railsgun") (v "2.0.2") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "170g6vivqmlnw0xz1py0vij59s1mhfvdpyb41ikv8c5q69555wrd") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

(define-public crate-railsgun-2.0.3 (c (n "railsgun") (v "2.0.3") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "0rmmpgfgas5c5aivcaql6s5r0rjmd4vaz9sqv7p4d9kzr0l4cvx5") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

(define-public crate-railsgun-3.0.0 (c (n "railsgun") (v "3.0.0") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "1yjq098ap7r48zyzqd46cf9z10x4xqnmwfgrygrc41n8w0y7071y") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

(define-public crate-railsgun-3.0.1 (c (n "railsgun") (v "3.0.1") (d (list (d (n "tokio") (r "^1.12.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "00a3ij6wsn9n093bi42izc28ylaalk3swrr6hn43w467d0xm4pch") (f (quote (("railsgun-deprecated") ("rail-tap") ("rail-merge") ("option-extended") ("map-iter") ("future-result") ("default" "rail-merge" "rail-tap" "railsgun-deprecated" "future-result" "option-extended" "map-iter"))))))

