(define-module (crates-io ra il rail-lang) #:use-module (crates-io))

(define-public crate-rail-lang-0.2.0 (c (n "rail-lang") (v "0.2.0") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1x4398ckvmjx2k4hbdgd1hb24z20hnp75aap01vrfg87790qxffq")))

(define-public crate-rail-lang-0.3.0 (c (n "rail-lang") (v "0.3.0") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "081v065gshy9irn1cq6rk8xiyv62fgylwcw605qxw3h976azihqs")))

(define-public crate-rail-lang-0.3.1 (c (n "rail-lang") (v "0.3.1") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0g66fk30k7dzylqf942y62s5hi8dr12xgh5swp2bn7d0ny849zfp")))

(define-public crate-rail-lang-0.3.2 (c (n "rail-lang") (v "0.3.2") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0sbsidmv5vc8vrjn4q85lhv7123xiarg2fyq9rz9v9nl03123i9a")))

(define-public crate-rail-lang-0.4.0 (c (n "rail-lang") (v "0.4.0") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1dk0a470lp198ayvc6ax6bbbxfz1iisrj76c8lksdpkv08kyil8y")))

(define-public crate-rail-lang-0.4.3 (c (n "rail-lang") (v "0.4.3") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0w7gcp0r8mv4bf31lrrz9j56gk8h90p62d2wn2q9zsmlszxpbc7l")))

(define-public crate-rail-lang-0.4.4 (c (n "rail-lang") (v "0.4.4") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0b8k6pxcwgi84dbgb549r2l0pwmzgsl0k571ggwqx3h325vyksjd")))

(define-public crate-rail-lang-0.4.5 (c (n "rail-lang") (v "0.4.5") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0zir6f2nng7p2zb643jjnxmdfv3m81z2fb2c8hvxnr0jn842d6bg")))

(define-public crate-rail-lang-0.4.6 (c (n "rail-lang") (v "0.4.6") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0zyr9958xl1y1jh927s4laajqc2kbl0pcn29gvr10fdrmxxvqx3x")))

(define-public crate-rail-lang-0.4.7 (c (n "rail-lang") (v "0.4.7") (d (list (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "01iiz8ywcp5m9qlgw7k0hzrx7rx1zikhq88xjmc0hg5qrii41mdk")))

(define-public crate-rail-lang-0.5.0 (c (n "rail-lang") (v "0.5.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0wkxa0i1n4s23sbhh59nsvaz4asky1fxxbskv3q4848gw0j4519d")))

(define-public crate-rail-lang-0.5.1 (c (n "rail-lang") (v "0.5.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1p9hc0p8k55iawdkzqyq1j9qmkqscswdnl9w69n1aqq8njg9p6va")))

(define-public crate-rail-lang-0.6.0 (c (n "rail-lang") (v "0.6.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "06q98qlssw3k4dmmqy34ys4ijlc7sk24vizd6fghx1502qspkicy")))

(define-public crate-rail-lang-0.6.1 (c (n "rail-lang") (v "0.6.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "13c9gps2kvpbv1fypjmwwfy411piqgaw9kfbiarvwmjil4q6yc34")))

(define-public crate-rail-lang-0.7.0 (c (n "rail-lang") (v "0.7.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "192s1ifq2281jb0jxbz2danb2mrcvcsr1l8m86v0b7gzxdnaj5hx")))

(define-public crate-rail-lang-0.7.1 (c (n "rail-lang") (v "0.7.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "010zmdqyqdcqcc9i2s1jw7ilszl15i8r05pqmwahdj9nb6ar1328")))

(define-public crate-rail-lang-0.8.0 (c (n "rail-lang") (v "0.8.0") (d (list (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "06zxwpa8qbpg3lfd93hwlnnnbh8baz9i2dkky5rmj36j9vf2xwaf")))

(define-public crate-rail-lang-0.9.0 (c (n "rail-lang") (v "0.9.0") (d (list (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1j8v13caxfbvb980dg3cqxsp5wqdqiibp3qha1410yhiblfnb803")))

(define-public crate-rail-lang-0.10.1 (c (n "rail-lang") (v "0.10.1") (d (list (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "12mhpiy4dsbyk6k8arnp5b6b9k9wawa95la8zvkhgq3703vxli5i")))

(define-public crate-rail-lang-0.11.0 (c (n "rail-lang") (v "0.11.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1zjjd9d2lavsnq9sqd0dnb7w9cv8xlgkbpbxdvaaggl9sag4b503")))

(define-public crate-rail-lang-0.11.1 (c (n "rail-lang") (v "0.11.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1hx2p88fxiqw0dy98yfa20wig1xnf4ycc3k9ai1m76kf1yxkafh3")))

(define-public crate-rail-lang-0.11.2 (c (n "rail-lang") (v "0.11.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0mxacb4cilcxvzrw9b1ssnbkb3370wv17gdg6rp0wz4ic1ylqrlh")))

(define-public crate-rail-lang-0.12.0 (c (n "rail-lang") (v "0.12.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "07il4nm659sqbjzkrqf9l52yv44r8pnm2bgsprhwmwdahi70k815")))

(define-public crate-rail-lang-0.12.1 (c (n "rail-lang") (v "0.12.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1gjqv21crypbvsnw0jsmwakl8bspaw8z40p9mvh5frg9zhg1x57z")))

(define-public crate-rail-lang-0.12.2 (c (n "rail-lang") (v "0.12.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1c8bfdax47pmml9fxfikfp94r7cmj900lyz37i313fhahy3vgnsj")))

(define-public crate-rail-lang-0.13.0 (c (n "rail-lang") (v "0.13.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "16nk01xa345w377f714fha8y7dbykqz1dqks4450g4kggybaka8c")))

(define-public crate-rail-lang-0.13.1 (c (n "rail-lang") (v "0.13.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0ln47dwv2383xhmg0g6zcghlz9i66sa3k6m46g65p0vis8csh69a")))

(define-public crate-rail-lang-0.14.0 (c (n "rail-lang") (v "0.14.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1zi2kx684qd20n9k74aj74ck8qhc5vnx8y8pfrbmcs7rj08216gv")))

(define-public crate-rail-lang-0.15.0 (c (n "rail-lang") (v "0.15.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1jx4hmck1f01sdfx472mf0rq5w7qx3xx3pifmi1gnnrnw6iwn0r3")))

(define-public crate-rail-lang-0.16.0 (c (n "rail-lang") (v "0.16.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0xkcxznhg2c08kgi4sivf54xzk3fhzgllazpk768vh93ak9s63n3")))

(define-public crate-rail-lang-0.16.1 (c (n "rail-lang") (v "0.16.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "02bkflvsjggyfcx3x84bf1k0vhmk6chp4x0bbsdcmpw9aqx9ygpc")))

(define-public crate-rail-lang-0.17.0 (c (n "rail-lang") (v "0.17.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "0irjhgsbp5pgqh11bwb3zgjmaiwjl39xaw8i6rz7fcazqa0pyrwl")))

(define-public crate-rail-lang-0.17.1 (c (n "rail-lang") (v "0.17.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "1kpm1ybjwwnawsnax0skgzv1svdzypgjl88nk37hhkpchs4b79wz")))

(define-public crate-rail-lang-0.18.0 (c (n "rail-lang") (v "0.18.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "02c54jbz3ncmrd4xpsvvb6n22svfn2m1wqv4whny0kqknl0ryki6")))

(define-public crate-rail-lang-0.18.2 (c (n "rail-lang") (v "0.18.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0v71fw1cdxr02dbdbpvnm51yalmr791pamwl1qamr1i8759lrl5n")))

(define-public crate-rail-lang-0.19.0 (c (n "rail-lang") (v "0.19.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "026q0fpv6pcrvgy34jmgzhdr3rbnyrgrhczzk91lfg2ls7mwn6gf")))

(define-public crate-rail-lang-0.19.1 (c (n "rail-lang") (v "0.19.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1hq15lx9nar7g99d2c3spn30g4hmlgil0fn6drc82hm66ccqdsc0")))

(define-public crate-rail-lang-0.20.0 (c (n "rail-lang") (v "0.20.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0dw2srbb831m41rv566w8ph4j6gxazv9fvp32n88mjg0b2i241fl")))

(define-public crate-rail-lang-0.20.1 (c (n "rail-lang") (v "0.20.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1zb5llrv50rsyn6vw7lqwxn7haj75ms7i6xzqq4r61jkawipg9pz")))

(define-public crate-rail-lang-0.20.2 (c (n "rail-lang") (v "0.20.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1j2mphxkv28savss047k7sq0r0yy24j15qd5k82ys3rpyi4220q9")))

(define-public crate-rail-lang-0.20.3 (c (n "rail-lang") (v "0.20.3") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1y342xr7bqv3jkjf9alby8pbbix2qhgpdzqpnwdzqk8379c7a913")))

(define-public crate-rail-lang-0.21.0 (c (n "rail-lang") (v "0.21.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1x6wff6j1rlq6qa044wvvbvks4js8wsmsbz7q5hriy8985icpln7")))

(define-public crate-rail-lang-0.21.1 (c (n "rail-lang") (v "0.21.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1angk5lral2f5y59c4ci6rfj4g7s1s3msg3vfv2xyz06fylxzyw2")))

(define-public crate-rail-lang-0.22.0 (c (n "rail-lang") (v "0.22.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "09hl0hsdhbgrrs79phismdx3pcd9mkfn7xyhzriz4xbiaqksss1y")))

(define-public crate-rail-lang-0.22.1 (c (n "rail-lang") (v "0.22.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "199ipzdhqn7jhxl47h7jb3f6hc09yfdb9qvxlxxg9jjikgr4kv1k")))

(define-public crate-rail-lang-0.22.2 (c (n "rail-lang") (v "0.22.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0ky17msmk3h0pmxrl32xnnhlbyl8z708b500nj0kvy7gm4iq3g3y")))

(define-public crate-rail-lang-0.23.0 (c (n "rail-lang") (v "0.23.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "13if0bmf7p6q8n60mwx4x72ldidskkfzdd7f6nj0r1jixahdblhj")))

(define-public crate-rail-lang-0.24.0 (c (n "rail-lang") (v "0.24.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1xqpic8b440mp73r8q1cn0rrxdf2gzsqb9mh03ngc5w7v22czzak")))

(define-public crate-rail-lang-0.24.1 (c (n "rail-lang") (v "0.24.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "01bnhkzlkfwzqk0qfbxzhd7my2hvj1d3vj2g37slmayhd8pdvp3p")))

(define-public crate-rail-lang-0.24.2 (c (n "rail-lang") (v "0.24.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0x9sfiy34144rkh4frafzqhpmhf7cq8saja0q69z6dl7w3w1sgxy")))

(define-public crate-rail-lang-0.25.0 (c (n "rail-lang") (v "0.25.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1hp03d8f8p5mwcx08br9ghl9pmcw74pa4y1986nihf9c1jzp35c8")))

(define-public crate-rail-lang-0.25.1 (c (n "rail-lang") (v "0.25.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "1qny4z6vnavh5d2hxwix7f300g0y4flp2n9qnaj7x1v76n0i4wpl")))

(define-public crate-rail-lang-0.26.0 (c (n "rail-lang") (v "0.26.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0rrb4akz3v0p5vk7bcrbf584awbr5417g4sky60d149f3z0qv0xv")))

(define-public crate-rail-lang-0.27.0 (c (n "rail-lang") (v "0.27.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0byh2q5ybfijxb0715bzl15l7rc82bbzikl1750rpcq830jhc6wa")))

(define-public crate-rail-lang-0.27.1 (c (n "rail-lang") (v "0.27.1") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "03z4pcpvk8dd6vwrnx8yikkl67i7c7h9cbra2q63admxgpvz080d")))

(define-public crate-rail-lang-0.28.0 (c (n "rail-lang") (v "0.28.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^10.0") (d #t) (k 0)))) (h "0i188d7prlk0ry8n51hb0971lhmvldlm6hd55mbzshmx1sm8lkwc")))

(define-public crate-rail-lang-0.29.0 (c (n "rail-lang") (v "0.29.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)))) (h "1kiad4wn4vn47c9r3m8ksrzbqr4x39hbayakpgl45k1nyxyf4h6f")))

(define-public crate-rail-lang-0.30.0 (c (n "rail-lang") (v "0.30.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)))) (h "1i2iz7ampbpkslp5l1rfx7lqmlcd3cljj3kkg4n7rgvd5k4w0433")))

(define-public crate-rail-lang-0.30.1 (c (n "rail-lang") (v "0.30.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)))) (h "1pnn74fvpbl981xxylpgd2iiyw8wqvdi341jmhwvbrxmg1iqggk8")))

(define-public crate-rail-lang-0.30.2 (c (n "rail-lang") (v "0.30.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "directories") (r "^5.0") (d #t) (k 0)) (d (n "im") (r "^15.1") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)))) (h "1sd4lk391szgs1nljmsipaaawbg0z4g31r0d3hbysxscry0lsdnw")))

