(define-module (crates-io ra il railwind) #:use-module (crates-io))

(define-public crate-railwind-0.0.1 (c (n "railwind") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1kqrpq76qbp6hk6xcs35dp5l7v0h97yfh3d6p1mhb4lzmkzk80sz")))

(define-public crate-railwind-0.0.2 (c (n "railwind") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "19h7z6gb709g4zfflp5anh2f81p22qs00r8sk6y2b7xyndvb4scz")))

(define-public crate-railwind-0.0.3 (c (n "railwind") (v "0.0.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pkc1sgx3iqkyxvadldvs9zzp0v73ixz2gflkqm5kd5n2aqgfvq8")))

(define-public crate-railwind-0.0.4 (c (n "railwind") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0df5bxkmw4lzahdwakpy7r7b79kp06rwmng0y5bcjw0p2y2gh997")))

(define-public crate-railwind-0.0.5 (c (n "railwind") (v "0.0.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0yc237dyl2pbif1d16m2flspvmjvfyygmf6jd1xz1gpkn2dgfpnx")))

(define-public crate-railwind-0.0.6 (c (n "railwind") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0xhv5glyx6xpp5akx3qi5m36dzzr1g5m9llczyrgw2a593m11vnn")))

(define-public crate-railwind-0.0.7 (c (n "railwind") (v "0.0.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1xfhjhqq7jhyd8l6h8nzxl4ih5x5964624jf0p3va5gaxhn6ywvq")))

(define-public crate-railwind-0.0.8 (c (n "railwind") (v "0.0.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1ir0vn0zvj0glwfgk8nsgw8x2hz4ijdn0yw41wrr2dk9mqgx4ab5")))

(define-public crate-railwind-0.0.11 (c (n "railwind") (v "0.0.11") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "06gamhxdslhfiv2r9g7dcrz2g0c65ya9j14wnmdq35y0wvks1v7x")))

(define-public crate-railwind-0.0.12 (c (n "railwind") (v "0.0.12") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1maga6xc9g2i9pnfyhgi5wsars499jpsp78avah16ac8pryff5f0")))

(define-public crate-railwind-0.1.0 (c (n "railwind") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "01f0p6w79d5mm2wca3asrjycxgfplhagar7795hmfpkic5cd48iw")))

(define-public crate-railwind-0.1.1 (c (n "railwind") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "18s7bci05vmrzrmalyphv8k57k3zy3y7qdl07gcm3i5khnch83hz")))

(define-public crate-railwind-0.1.2 (c (n "railwind") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1190qr5x6bpph5n3hszivd7ns0n28d7dnkxn5chbcn25qy3gdpmg")))

(define-public crate-railwind-0.1.4 (c (n "railwind") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "1yc5hacv0gfwqsdjkvb2mr3cnfz30dnxzjxsblpx5bla28iral9n")))

(define-public crate-railwind-0.1.5 (c (n "railwind") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-col") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_regex") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "000qsiwj7alr5xx2n3xn5szvqhibw899wfcdds5cfqj1cg5bdhib")))

