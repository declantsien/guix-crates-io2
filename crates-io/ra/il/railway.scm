(define-module (crates-io ra il railway) #:use-module (crates-io))

(define-public crate-railway-0.1.0 (c (n "railway") (v "0.1.0") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0yh4mgiw7p1a4b6az5pbclw09kq88fh7lxmcq520yfbhgjsz3gj3")))

(define-public crate-railway-0.1.1 (c (n "railway") (v "0.1.1") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "084p7q1k0baxa5hi27zn3ss55j8wx44jnb6q7vi5152faa9qprib")))

(define-public crate-railway-0.1.2 (c (n "railway") (v "0.1.2") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0y16iswwgcb17ll3k2hw201ksq3zbhqa44alg9pf3z24mcsc6wrb")))

(define-public crate-railway-0.1.3 (c (n "railway") (v "0.1.3") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "106ifnl1f5arsdqpclqwc3mpywjwyx3f6f6qqvnp5cff1b01c9zs")))

(define-public crate-railway-0.1.4 (c (n "railway") (v "0.1.4") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1rnlpnwji40h52svjljycj2z6m6pag0pxy7k405ylw9r7fh4nrc6")))

(define-public crate-railway-0.1.5 (c (n "railway") (v "0.1.5") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0acklvx2ycwf1sqiivfa6h5pss92nsby5kidcqnizvcymgawv5cn")))

(define-public crate-railway-0.1.6 (c (n "railway") (v "0.1.6") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "17kzd0q3f2r82i41g7xq8j7xl3mc5hjdgn5m2cjrrjbx5rs7khhp")))

(define-public crate-railway-0.1.7 (c (n "railway") (v "0.1.7") (d (list (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "zeno") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0p5l63yw0kkdg8jkxxz8yrwqh1wf12sazqvrdcgnmgd1y2wb1nq0")))

(define-public crate-railway-0.2.0 (c (n "railway") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.0.0") (d #t) (k 0)))) (h "03rbrxbrsjyy6a24s93lr9c671a85931g9v1zpp9wd1dk1rd4635")))

(define-public crate-railway-0.2.1 (c (n "railway") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.1.1") (d #t) (k 0)))) (h "14mcd9iajv19dn05vx6l6p323jcryg379qv4ryqs9984bj2j083m")))

(define-public crate-railway-0.2.2 (c (n "railway") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.1.2") (d #t) (k 0)))) (h "0c2w8jxgrl8p6m49nzjhf0nsk575pk8kpr631m79nxgj23xrd9i7")))

(define-public crate-railway-0.2.3 (c (n "railway") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.1.3") (d #t) (k 0)))) (h "1ls2104b6xcfgghrhab37nzpicp63hhazxq93h03y4bgnfxh9ac5")))

(define-public crate-railway-0.3.1 (c (n "railway") (v "0.3.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.1.5") (d #t) (k 0)))) (h "0s6cavbcal7fswalv55bck6qww4kpb2xyyac0kkb6izd2sr4670k")))

(define-public crate-railway-0.3.2 (c (n "railway") (v "0.3.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.2.0") (d #t) (k 0)))) (h "057c5h5dgv7fs9mmik21pbvd3hrpiw50fji2zc998fzh1nrxzf0m") (f (quote (("simd" "wizdraw/simd") ("default"))))))

(define-public crate-railway-0.3.3 (c (n "railway") (v "0.3.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rgb") (r "^0.8.36") (d #t) (k 0)) (d (n "vek") (r "^0.15.8") (k 0)) (d (n "wizdraw") (r "^1.2.1") (d #t) (k 0)))) (h "1qn6glzpc84m9nk4i4riyj030ly1g8vfbhjsmnrcymzx5wklb27m") (f (quote (("simd" "wizdraw/simd") ("default"))))))

