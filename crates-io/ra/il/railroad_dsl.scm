(define-module (crates-io ra il railroad_dsl) #:use-module (crates-io))

(define-public crate-railroad_dsl-0.1.0 (c (n "railroad_dsl") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "railroad") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)))) (h "0lc38in39nblrk0y3psqr3l083rgl9i5qd4gshvcbzcj8slcghhm")))

(define-public crate-railroad_dsl-0.1.1 (c (n "railroad_dsl") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "railroad") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)))) (h "0mzb6mz7gz675vkgjzl4gz1ya549af97v4zs0xxc39pzmy3j3l72")))

(define-public crate-railroad_dsl-0.1.2 (c (n "railroad_dsl") (v "0.1.2") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "railroad") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.12") (d #t) (k 0)))) (h "0igyv0y4ssgn4qv5a255dfq2qhnvjcs83q8l8fjf0d3vn9wqlhj9")))

(define-public crate-railroad_dsl-0.1.3 (c (n "railroad_dsl") (v "0.1.3") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "railroad") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ly5s4qh5crzq168hwgrv20fhx0p09q7i4z46pnckf3w22iwh9k9")))

(define-public crate-railroad_dsl-0.2.0 (c (n "railroad_dsl") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "htmlescape") (r "^0.3") (d #t) (k 2)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)) (d (n "railroad") (r "^0.2") (d #t) (k 0)))) (h "02cxr5f8la22yv6a374dymacb85hj9hi63jkg7vb73gfcgs1rhs4")))

