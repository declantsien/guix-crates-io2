(define-module (crates-io ra il railyard) #:use-module (crates-io))

(define-public crate-railyard-0.0.1 (c (n "railyard") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "prost") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustls") (r "^0.21.7") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "048mqfn0w13h71m1zb4xdd54l5f7qxqkym8j1n7q46qgjnjxqs7h")))

