(define-module (crates-io ra il railroad) #:use-module (crates-io))

(define-public crate-railroad-0.1.0 (c (n "railroad") (v "0.1.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0wc7z2xn6pciwxbkwa5m6kv7g4b3frxgz3laqrm2pp3cr9a819pc") (f (quote (("visual-debug"))))))

(define-public crate-railroad-0.1.1 (c (n "railroad") (v "0.1.1") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "railroad_verification") (r "^0.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1aa763g039ig0mbh4y1nfxqnvsn1b0c7nzjgxs0vb0z37d6pri2r") (f (quote (("visual-debug"))))))

(define-public crate-railroad-0.2.0 (c (n "railroad") (v "0.2.0") (d (list (d (n "htmlescape") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 2)) (d (n "railroad_verification") (r "^0.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "08y86hb3vp0i9k9dkz3lbfhrd8x051f6pcg3p87z65ss3shdmzh1") (f (quote (("visual-debug"))))))

