(define-module (crates-io ra ze razer-naga-2014-key-remap) #:use-module (crates-io))

(define-public crate-razer-naga-2014-key-remap-0.1.1 (c (n "razer-naga-2014-key-remap") (v "0.1.1") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "1n1fwjc8wdrcx7198f8x6sfiwcgng2kv8bg8inqbaa2kc5wrs8f7")))

(define-public crate-razer-naga-2014-key-remap-0.1.3 (c (n "razer-naga-2014-key-remap") (v "0.1.3") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "1a2v2n63906pr9z7k3bq0a9042h6cpjmz6fq6sif4pmd3dxa31i4")))

(define-public crate-razer-naga-2014-key-remap-0.1.4 (c (n "razer-naga-2014-key-remap") (v "0.1.4") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "1spwi5pmhzb6m90pgfl7skw8q89l107z44wdaj1vbxghsqbr5kgw")))

(define-public crate-razer-naga-2014-key-remap-0.1.5 (c (n "razer-naga-2014-key-remap") (v "0.1.5") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "0zys3ndxvy0fqmq4cq0sr2ljwrvs4i01kz0lnl6s80gv2gygf3x6")))

(define-public crate-razer-naga-2014-key-remap-0.1.6 (c (n "razer-naga-2014-key-remap") (v "0.1.6") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "0da29slc5r7bld9v9b9ns177fxqznv3pg2bix16gpzzlhq4igmlc")))

(define-public crate-razer-naga-2014-key-remap-0.1.7 (c (n "razer-naga-2014-key-remap") (v "0.1.7") (d (list (d (n "evdev-rs") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "toml") (r "^0.4.8") (d #t) (k 0)) (d (n "uinput") (r "^0.1.3") (d #t) (k 0)))) (h "0i6z4y09yblp6014br14xhmj0nnjdf570v98m4l4ma8j6w2ah3rn")))

