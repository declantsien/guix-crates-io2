(define-module (crates-io ra ze razer_driver_rs) #:use-module (crates-io))

(define-public crate-razer_driver_rs-0.1.0 (c (n "razer_driver_rs") (v "0.1.0") (d (list (d (n "associated") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "hidapi") (r "^1.3.3") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14klgkys13jkm8qh3yb8lk0w13hzfgsp10r812ypqvxi7qfm6jyq")))

