(define-module (crates-io ra ze razer_chroma) #:use-module (crates-io))

(define-public crate-razer_chroma-0.1.0 (c (n "razer_chroma") (v "0.1.0") (d (list (d (n "arraymap") (r "^0.1.1") (d #t) (k 0)) (d (n "bindgen") (r "0.*") (d #t) (k 1)) (d (n "cc") (r "^1.0.36") (d #t) (k 1)))) (h "110wac8rkc8hqds8bbxjgdl4k0kng3mg96gzx7rmd48pz63alm5z")))

