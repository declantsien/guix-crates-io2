(define-module (crates-io ra lf ralf) #:use-module (crates-io))

(define-public crate-ralf-0.1.0 (c (n "ralf") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raft") (r "^0.1.0") (d #t) (k 0)))) (h "1abyw44068phdddbzxydq1y4nvwha47lzai3fpf3nv97wf8higrr")))

(define-public crate-ralf-0.1.1 (c (n "ralf") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "raft") (r "^0.1.0") (d #t) (k 0)))) (h "0vmkfvq4x2d8pairid20vjr6kg44kvw7v9hbqpggfvvi0vkp8qlj")))

