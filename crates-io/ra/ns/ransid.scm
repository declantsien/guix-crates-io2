(define-module (crates-io ra ns ransid) #:use-module (crates-io))

(define-public crate-ransid-0.1.1 (c (n "ransid") (v "0.1.1") (h "1x48bi5vzcvki9vvm59j5r2a9yafj239c7nkq3f0xqdfrz7yj35y")))

(define-public crate-ransid-0.1.2 (c (n "ransid") (v "0.1.2") (h "1s10l0gmws2da2vq6xvw17kf17zv58b6kdyd2yyjznmnyd34v6k5")))

(define-public crate-ransid-0.2.0 (c (n "ransid") (v "0.2.0") (h "185xl8ry6wxwq463ncci0qjylcz4pk8p78qzqyy18k621m61n36f")))

(define-public crate-ransid-0.2.1 (c (n "ransid") (v "0.2.1") (h "1n4dziv23zbxx4v0fb3big6ms7vpwyz5pcmcj0yc5g6vfa90vsza")))

(define-public crate-ransid-0.2.2 (c (n "ransid") (v "0.2.2") (h "0h80yny611s4v3iskxaq5a1pgbip8wmx82flii1byqqm0pibd556")))

(define-public crate-ransid-0.2.3 (c (n "ransid") (v "0.2.3") (h "0knxqwj58pa2386i5wgsv80rif2apa06dnydar375b8s0kkc7vq1")))

(define-public crate-ransid-0.2.4 (c (n "ransid") (v "0.2.4") (h "0p2z4rkvrardydi8ab2xc16sl00z716j5q2sh58pd8qk93kldja2")))

(define-public crate-ransid-0.2.5 (c (n "ransid") (v "0.2.5") (h "109dfj5xjr11dc44dbsbapig7vdy6bxpp9qv1cxhprpnbj416rvc")))

(define-public crate-ransid-0.2.6 (c (n "ransid") (v "0.2.6") (h "1w2f9bn41mw2k2wpkk5szv8c3khf6qyadklqbwgakagcxrqlc1z2")))

(define-public crate-ransid-0.2.7 (c (n "ransid") (v "0.2.7") (h "09rpsc90ryssa6kyhjasajm4jhnq71xg130ynyikb3bm0g7afw78")))

(define-public crate-ransid-0.2.8 (c (n "ransid") (v "0.2.8") (h "0wkf605jq6ak43xvsm0np87iapxay6kdsqs232x6d0vl2ycwh65h")))

(define-public crate-ransid-0.2.9 (c (n "ransid") (v "0.2.9") (h "0hacglx2f4awm8gp58j97dhd53q18c7s4v5f18sxh6aab8r768z2")))

(define-public crate-ransid-0.2.10 (c (n "ransid") (v "0.2.10") (h "197gqdj8fkbykz9m6kddm21mc1abznpj9azhac9zq235qlpxhkkm")))

(define-public crate-ransid-0.3.0 (c (n "ransid") (v "0.3.0") (h "1z5v0z8qryq8j3igdjkq7sqbni8xdwgn3a40kf9vb18iz28lhjq7")))

(define-public crate-ransid-0.3.1 (c (n "ransid") (v "0.3.1") (h "1k9f0lazkg4ifjziv3028z9n7vrgnw3560v89018wh1aybz54jax")))

(define-public crate-ransid-0.3.2 (c (n "ransid") (v "0.3.2") (h "02fkqdfamiqwvkk5p0wsfx6cippw9cdk53wq5s975ddbrlkcslzh")))

(define-public crate-ransid-0.3.3 (c (n "ransid") (v "0.3.3") (h "0m6891crv8xf8fjlpl3dvw3smyp03ybyhf2cr8idhn9wy63910w1")))

(define-public crate-ransid-0.3.4 (c (n "ransid") (v "0.3.4") (h "19lxk8nhqdg89x9facrw61rn5bj7dzhxzmybv56q4bv15sbnpism")))

(define-public crate-ransid-0.3.5 (c (n "ransid") (v "0.3.5") (h "0x0bfdm9kybwbp7dgr5hnm5lii1p5kd2rgsbh0nrvjvf821h8gmz")))

(define-public crate-ransid-0.3.6 (c (n "ransid") (v "0.3.6") (h "0m87hmp7y077pbmyjap78204hjlcrb7drgzyzrkd0yhhbxy5qqiw")))

(define-public crate-ransid-0.3.7 (c (n "ransid") (v "0.3.7") (h "1lia7cn537zh641sds5mxzy284cndw8zvih29blm57acx7h3kq07")))

(define-public crate-ransid-0.3.8 (c (n "ransid") (v "0.3.8") (h "1ajv5sk5xi4rfpw43ldhhw6qfi4ng2k1fvz9d6z4izvsbxa8z6v5")))

(define-public crate-ransid-0.4.0 (c (n "ransid") (v "0.4.0") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "18kz4g3d9iclh8h2j934mb55jvmn8zz18zm77zv4ms5706nafscd")))

(define-public crate-ransid-0.4.1 (c (n "ransid") (v "0.4.1") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0nwrivy4pymc579nk9j72m8f1g2wwkfqm2hid9r0kvxwk5w5gzja")))

(define-public crate-ransid-0.4.2 (c (n "ransid") (v "0.4.2") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "1g2bz1xlqaww5m0fdf0h4388isj36dva24rrp6cg3sbvnl40fv6j")))

(define-public crate-ransid-0.4.3 (c (n "ransid") (v "0.4.3") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "15xfzyv6p5vis6j6n9ldasfgvsfwj5659i2ag5zc7iijq1hq3zvc")))

(define-public crate-ransid-0.4.4 (c (n "ransid") (v "0.4.4") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0njb30ayvldaym92sjwynxpzsfih5bpk74bhyvcjfglk3f1f8b3f")))

(define-public crate-ransid-0.4.5 (c (n "ransid") (v "0.4.5") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0lxbc8g08r11v07hm3rkrqgzv08wnlk0992m0y41biz01v05pn20")))

(define-public crate-ransid-0.4.6 (c (n "ransid") (v "0.4.6") (d (list (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0w9g3bj3l3cm3jpj82abr0w5a11pq7h38d83pwjbp82cgb6zvicg")))

(define-public crate-ransid-0.4.7 (c (n "ransid") (v "0.4.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "008n7qmls0a0v7c87gn4y1h6iwh6amw6hwk3lykvd2kd7qk6waz0")))

(define-public crate-ransid-0.4.8 (c (n "ransid") (v "0.4.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0i9hph73j16742cc3x1djihpsdri10nckqb4z8yid4053caszf92")))

(define-public crate-ransid-0.4.9 (c (n "ransid") (v "0.4.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vte") (r "^0.3") (d #t) (k 0)))) (h "0ls597icw6i08zv501f6y511cb0vc6npzm8y4kx5xv73pfq0pr46")))

(define-public crate-ransid-0.5.0 (c (n "ransid") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vte") (r "^0.8") (d #t) (k 0)))) (h "1dgsvymwp0l501qk81xhxr3i28j99n64r36m3rgllz8jgqvbp621")))

