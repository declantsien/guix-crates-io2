(define-module (crates-io ra ns rans) #:use-module (crates-io))

(define-public crate-rans-0.1.0 (c (n "rans") (v "0.1.0") (h "073vrw5hlmqg49nyil8qdan567kq8h6lhwr9am4gqd8g0a1cwk04")))

(define-public crate-rans-0.1.1 (c (n "rans") (v "0.1.1") (h "0minjyjbjg06f58c4908d1j4cw4vljd83kkynwygy007hz8mzppi")))

(define-public crate-rans-0.1.2 (c (n "rans") (v "0.1.2") (h "0w4p3nzx2lbr9vwg35agzbdyk05502brwb6qrgim6amr64nqfwr0")))

(define-public crate-rans-0.2.0 (c (n "rans") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryg-rans-sys") (r "^1.0.7") (f (quote ("byte" "64"))) (d #t) (k 0)))) (h "1392jd674ffgpqij4ikpnry43w59mm3rp76di1zxj6c5jwc4lhqq")))

(define-public crate-rans-0.2.1 (c (n "rans") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryg-rans-sys") (r "^1.0.7") (f (quote ("byte" "64"))) (d #t) (k 0)))) (h "0w863c0ldd6anbh7mfssw5w1ply4wgmqh3xgznxgyhm70i760c9a") (r "1.57.0")))

(define-public crate-rans-0.3.0 (c (n "rans") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryg-rans-sys") (r "^1.1.0") (f (quote ("byte" "64"))) (d #t) (k 0)))) (h "0jv0b3v1lywz7cn0kj1yihcdwpv2c5p1ay4vvipdnwavspxz5iyi") (r "1.60.0")))

(define-public crate-rans-0.4.0 (c (n "rans") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 2)) (d (n "ryg-rans-sys") (r "^1.2.0") (f (quote ("byte" "64"))) (d #t) (k 0)))) (h "14zs5lmgqzb5y39dkb0iypsr4clq66wdjk6ws77mjjfvg0bw7gvi") (r "1.71.0")))

