(define-module (crates-io ra ns ransel) #:use-module (crates-io))

(define-public crate-ransel-0.1.0 (c (n "ransel") (v "0.1.0") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1y7xi2lm29zxqsixgw60b9sdx5wwy3clgm9m2pj1ic6iihxnaazv")))

(define-public crate-ransel-0.1.1 (c (n "ransel") (v "0.1.1") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1p8czcb6w0ln313y3fnw7s8ymnj0hl1ww0s3dhr73qrvgia3bwxw")))

(define-public crate-ransel-0.1.2 (c (n "ransel") (v "0.1.2") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "18hgn57jm1775vxdcrhf9d162cfhrq7m6npm382w6gd6xj12ysq9")))

(define-public crate-ransel-0.1.3 (c (n "ransel") (v "0.1.3") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "006rxm4cfbkqhj0wfm561g8z2cyaljahz3jp12q5bqsc8ylzgxsj")))

(define-public crate-ransel-0.1.4 (c (n "ransel") (v "0.1.4") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "129z0bk45admrr6cl3lxgij1h5in80dwll0vshnhpvz6f0hbm1hx")))

(define-public crate-ransel-0.1.5 (c (n "ransel") (v "0.1.5") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1v1yhgmw1v6vx237al7cknl6mbkdn4x35mqwskfdnpknmw9g70ml")))

(define-public crate-ransel-0.1.6 (c (n "ransel") (v "0.1.6") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0jz5hy66dvbfqfra6n461gnl08py3m5sbgi84q5dzxfsv1mci2hb")))

(define-public crate-ransel-0.1.7 (c (n "ransel") (v "0.1.7") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1pl2iivmg3kzsbd9q6plvmgw4wihf6yqngrdj8g2gb6v11zs1zv0")))

(define-public crate-ransel-0.1.8 (c (n "ransel") (v "0.1.8") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "11hmmsgzhrlv27wv62dmw0mp33km01kakc3if6r3xgyk0g45lz7y")))

(define-public crate-ransel-0.2.0 (c (n "ransel") (v "0.2.0") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1kj0si5k2hy19pdwngv81h1yq5zcb83qy2s9bqavbyb018h8x6lj")))

(define-public crate-ransel-0.2.1 (c (n "ransel") (v "0.2.1") (d (list (d (n "bitintr") (r "^0.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "random") (r "^0.14.0") (d #t) (k 2)))) (h "0x2m1gzw1jhc157j5q4wgbcsqz6lj0i81rkq6chm8fsk8nzr61a0")))

