(define-module (crates-io ra pt rapture) #:use-module (crates-io))

(define-public crate-rapture-0.1.2 (c (n "rapture") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "os_info") (r "^1.1.1") (d #t) (k 0)))) (h "0bcfj9pwpky5xvhpahk8vya8g0lz4r7x7xakgwngmd1z82lml957")))

(define-public crate-rapture-0.2.0 (c (n "rapture") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "hex_d_hex") (r "^1.0.1") (d #t) (k 0)) (d (n "os_info") (r "^1.1.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.8") (d #t) (k 0)))) (h "11xkwrb0mkb2yky8l8lqrghlr9r30p3ahg5rv7whwysf02rx75wx")))

