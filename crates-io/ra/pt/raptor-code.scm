(define-module (crates-io ra pt raptor-code) #:use-module (crates-io))

(define-public crate-raptor-code-1.0.3 (c (n "raptor-code") (v "1.0.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14dy3qvjm7ifwfvfn8idby0qvjv3a8j4bjjx15g26h43nfhr8x1p")))

(define-public crate-raptor-code-1.0.4 (c (n "raptor-code") (v "1.0.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0b2vibhx6c63a7bf7va2ybqyzs1q6f35a9krmlj04g0fsw1887g0")))

(define-public crate-raptor-code-1.0.5 (c (n "raptor-code") (v "1.0.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0767vzdm9rhapvjmzangih0qfach9zg3bghpj2b2jpirfzp41img")))

(define-public crate-raptor-code-1.0.6 (c (n "raptor-code") (v "1.0.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "primes") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1g8m98g711aax84mzgsx3gz10mi3gknbdqkljiip0mignsq8lbw4")))

