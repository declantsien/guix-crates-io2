(define-module (crates-io ra pt raptr) #:use-module (crates-io))

(define-public crate-raptr-0.1.0 (c (n "raptr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.9.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1685m0bxdy4amnnyv80wg5xai0nc8lxr5a7pzkix5dpzgfnzfd5m")))

(define-public crate-raptr-0.2.0 (c (n "raptr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "comrak") (r "^0.9.0") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "tera") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1nfngalibvysbzgnv6l6l6694wmfblfvkhs831cw5wnxbdn1lsg2")))

