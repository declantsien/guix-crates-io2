(define-module (crates-io ra id raiden-macros) #:use-module (crates-io))

(define-public crate-raiden-macros-0.1.0 (c (n "raiden-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cvk3fsj1bbzwvd0ral5dhg0hyr22a6jw73gljysizc03abrpypd") (r "1.59")))

