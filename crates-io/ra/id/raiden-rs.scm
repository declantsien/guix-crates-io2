(define-module (crates-io ra id raiden-rs) #:use-module (crates-io))

(define-public crate-raiden-rs-0.1.0 (c (n "raiden-rs") (v "0.1.0") (d (list (d (n "raiden-api") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-blockchain") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-network-messages") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-network-transport") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-pathfinding") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-state-machine") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-transition") (r "^0.1.0") (d #t) (k 0)))) (h "0qw85ynp0mbsj15r8cipas3hx5cirlrwjksdq428i60cw43n4x8q") (r "1.59")))

