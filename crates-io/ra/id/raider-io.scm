(define-module (crates-io ra id raider-io) #:use-module (crates-io))

(define-public crate-raider-io-0.1.0 (c (n "raider-io") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0psy3fdgnpchvp4g8farx7hdl7ldqsfxl10jamf0win3mk9vxzci")))

