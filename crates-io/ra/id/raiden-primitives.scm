(define-module (crates-io ra id raiden-primitives) #:use-module (crates-io))

(define-public crate-raiden-primitives-0.1.0 (c (n "raiden-primitives") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.11") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.21") (f (quote ("recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.59") (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("sync"))) (k 0)) (d (n "web3") (r "^0.18.0") (f (quote ("signing"))) (k 0)))) (h "0vyl77hd6hs1q38a15gas9m6zia3l6c1gxkpnkplj9d4amivsz1f") (r "1.59")))

