(define-module (crates-io ra id raiden-transition) #:use-module (crates-io))

(define-public crate-raiden-transition-0.1.0 (c (n "raiden-transition") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.21") (k 0)) (d (n "parking_lot") (r "^0.11.2") (k 0)) (d (n "raiden-blockchain") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-network-messages") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-pathfinding") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-state-machine") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "web3") (r "^0.18.0") (k 0)))) (h "0jdnbbs2ixisn24l05zhbvypsdf4jl442p3dbijfqwvzz2hd39bm") (r "1.59")))

