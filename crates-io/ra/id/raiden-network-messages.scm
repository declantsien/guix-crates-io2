(define-module (crates-io ra id raiden-network-messages) #:use-module (crates-io))

(define-public crate-raiden-network-messages-0.1.0 (c (n "raiden-network-messages") (v "0.1.0") (d (list (d (n "canonical_json") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (k 0)) (d (n "raiden-blockchain") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-primitives") (r "^0.1.0") (d #t) (k 0)) (d (n "raiden-state-machine") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "serde-rlp") (r "^0.1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (k 0)) (d (n "web3") (r "^0.18.0") (f (quote ("signing"))) (k 0)))) (h "094arq33ibaaym4yd5kca3rbwf3vwlb00jpr8jb22iqk4alsyyfk") (r "1.59")))

