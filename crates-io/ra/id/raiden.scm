(define-module (crates-io ra id raiden) #:use-module (crates-io))

(define-public crate-raiden-0.1.0 (c (n "raiden") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1rslvji3crhcyyldwn3614658b8a34n9lx23l2nw2pfmbkjz11fb")))

