(define-module (crates-io ra w- raw-syscall) #:use-module (crates-io))

(define-public crate-raw-syscall-0.1.0 (c (n "raw-syscall") (v "0.1.0") (h "0rihk15srqpgnica7q1amcv5y1585z93cfamc295kc8gfslir546")))

(define-public crate-raw-syscall-0.7.3 (c (n "raw-syscall") (v "0.7.3") (d (list (d (n "raw-syscall-base") (r "^0.7.3") (d #t) (k 0)))) (h "0p2m5ynn7c128awcq5wpr2xs7shld02g92fbarf68y65gacckdcj")))

(define-public crate-raw-syscall-0.7.4 (c (n "raw-syscall") (v "0.7.4") (d (list (d (n "raw-syscall-base") (r "^0.7.3") (d #t) (k 0)))) (h "10cgbdcryn5sf8zxhhbxy5xqki3lvbw6yprs6l4bdyf1dwq20nh9")))

