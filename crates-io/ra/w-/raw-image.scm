(define-module (crates-io ra w- raw-image) #:use-module (crates-io))

(define-public crate-raw-image-0.1.0 (c (n "raw-image") (v "0.1.0") (h "147567j4jph44h46v9v03793445636fwj9ibk2dic8gswd51nlrq") (y #t)))

(define-public crate-raw-image-0.1.1 (c (n "raw-image") (v "0.1.1") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0m8v1lj07nsx8nlk5l24rdb4ar1hpqv48hxqkmrnkjrfyslb3hpf") (y #t)))

(define-public crate-raw-image-0.1.2 (c (n "raw-image") (v "0.1.2") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1jaqmvlc7vvdh80z7lmd8w3qfashn1ppgms1q0fykgc7rrv2iryf") (y #t)))

