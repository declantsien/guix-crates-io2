(define-module (crates-io ra w- raw-cstr) #:use-module (crates-io))

(define-public crate-raw-cstr-0.1.1 (c (n "raw-cstr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0i383bzjchr1dygfnh1lirxvcljklrz2gsjgy335vl6wqfc1laqf")))

(define-public crate-raw-cstr-0.1.2 (c (n "raw-cstr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1rgcf575bmn38krhsa2zzjwqnqw5i5x6rxbp95lcp290shva1gib") (y #t)))

(define-public crate-raw-cstr-0.1.3 (c (n "raw-cstr") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0sdkbv32al473gqn04mzkw3ds3qbxqv8zybddp7prv4ggpqs3qpd")))

(define-public crate-raw-cstr-0.1.4 (c (n "raw-cstr") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1zrm038k99l4gblrql4j7dpy5bk2ys7mm54cvb2syk6cgmq63mzx")))

