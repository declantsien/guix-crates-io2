(define-module (crates-io ra w- raw-parts) #:use-module (crates-io))

(define-public crate-raw-parts-1.0.0 (c (n "raw-parts") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "14fzlsh5pnib3spdxf0b2f0z8wzssz0w2xv3kwlgx2k6jbwris7r")))

(define-public crate-raw-parts-1.0.1 (c (n "raw-parts") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1gj4fqa0vs45i7zrssddfirddgy3mzz7zs63d98j8p4nq1r288a0")))

(define-public crate-raw-parts-1.0.2 (c (n "raw-parts") (v "1.0.2") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "116wld9kiab149kmx49jpg7m0bzdasim6xmnpzyspqjymnc9avxq") (r "1.56.0")))

(define-public crate-raw-parts-1.1.0 (c (n "raw-parts") (v "1.1.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "05ljl1z2a0hi7ihmjxiydrkwbw2bivcdpqlhs05id7883r18yzga") (r "1.56.0")))

(define-public crate-raw-parts-1.1.1 (c (n "raw-parts") (v "1.1.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0kixbd4w9vvnyv9ffy9i0d8hsmczflbfa254a0bmaqfmjav4ws2d") (r "1.56.0")))

(define-public crate-raw-parts-1.1.2 (c (n "raw-parts") (v "1.1.2") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1mwmrhpwyngln1z1lgjfnz6zlxcc5hbl5kwjm63higi8kzj2rqmi") (r "1.56.0")))

(define-public crate-raw-parts-2.0.0 (c (n "raw-parts") (v "2.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1d6cp9jb5b3phqr9k5sashfxw67v3xnpv0jibppllcngnf56lcgb") (r "1.56.0")))

