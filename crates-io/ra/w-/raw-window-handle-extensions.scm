(define-module (crates-io ra w- raw-window-handle-extensions) #:use-module (crates-io))

(define-public crate-raw-window-handle-extensions-0.5.0 (c (n "raw-window-handle-extensions") (v "0.5.0") (d (list (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)))) (h "123qsvzsw6zjxhh3l7my2cf62i15sx2f68bl6dj2zb9dz321qlja")))

(define-public crate-raw-window-handle-extensions-0.5.1 (c (n "raw-window-handle-extensions") (v "0.5.1") (d (list (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)))) (h "0kddxhnsnxpypd6jri3lsw0akbh1ggs171h00dbdb68irxzx4ik9")))

(define-public crate-raw-window-handle-extensions-0.5.2 (c (n "raw-window-handle-extensions") (v "0.5.2") (d (list (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)))) (h "1wz85nrl5khv2x90vjk802xnm6awqy5h4yijvgmqd83mf4klznnp")))

(define-public crate-raw-window-handle-extensions-0.5.3 (c (n "raw-window-handle-extensions") (v "0.5.3") (d (list (d (n "raw-window-handle") (r "^0.5") (d #t) (k 0)))) (h "1fnir4llvmp4vfqls8zhf3m16ywsvqaf6i8j89vcvhjgd040ncs1")))

