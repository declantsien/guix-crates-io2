(define-module (crates-io ra w- raw-window-handle) #:use-module (crates-io))

(define-public crate-raw-window-handle-0.0.0 (c (n "raw-window-handle") (v "0.0.0") (h "1rcgwnkgnav52dds1028i9f64qv9f12d27qll4fcvjj35gj3l49j") (y #t)))

(define-public crate-raw-window-handle-0.1.0 (c (n "raw-window-handle") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1b6nlljk7aib49sxj8dpfbz7k4y8f3573k91g2gplq2zc7qcgrr0") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.1.1 (c (n "raw-window-handle") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0acyrrwd5k5vf1yijbap5bxzqm9iiiwwl2g256wr6srqd5jys50k") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.1.2 (c (n "raw-window-handle") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11wfzz3w2jqkjlk478765imwwf9lzy5rmhpgf4hzzcsk20p3ngdg") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.2.0 (c (n "raw-window-handle") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15m2hvcmjwkdl32w96zcrar1wywrq8cjb01glycdib46jp02la9s") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.3.0 (c (n "raw-window-handle") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11vg4fldr5f968iars3aky5lbjz9jbbb33nxm5y3jk8ynf2mp09f") (f (quote (("nightly-docs")))) (y #t)))

(define-public crate-raw-window-handle-0.3.1 (c (n "raw-window-handle") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wf184jwfnlbybmdd6yhbcavdgzl1bg4dvrxzgj7r17dsc40vf4x") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.3.2 (c (n "raw-window-handle") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zlhwmn7405z8wc4yjn0g3x4z5hli6zj7857an1zvjpkg8pafzha") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.3.3 (c (n "raw-window-handle") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04c2wir7qq3g2b143yav52a1g5ack8ffqx2bpmrn9bc0dix1li0a") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.4.0 (c (n "raw-window-handle") (v "0.4.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "11mfh48gj67mnqk053jq76pl953h3zsghzn1d4p1p3h6cdr9qaj2") (y #t)))

(define-public crate-raw-window-handle-0.4.1 (c (n "raw-window-handle") (v "0.4.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0b701nkig4w11z6z351mamglrlch6qp00igqjzf4y0h5jvxdnfsh") (f (quote (("alloc")))) (y #t)))

(define-public crate-raw-window-handle-0.4.2 (c (n "raw-window-handle") (v "0.4.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1dr24lzairc9hdw2a8sk03vixzngv42y3qf9d0x2gmd9jkp5x9zv") (f (quote (("alloc"))))))

(define-public crate-raw-window-handle-0.3.4 (c (n "raw-window-handle") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "new") (r "^0.4.1") (d #t) (k 0) (p "raw-window-handle")))) (h "0xisj116xpaz1i2hci9jqfnccyixba1xryxl1gbdlj057la5b3z2") (f (quote (("nightly-docs"))))))

(define-public crate-raw-window-handle-0.4.3 (c (n "raw-window-handle") (v "0.4.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0hgvrqbr2b62zhq4ryv08h92mwis9v8f7j9pwcgxzlp7nswvw05q") (f (quote (("alloc"))))))

(define-public crate-raw-window-handle-0.5.0 (c (n "raw-window-handle") (v "0.5.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "02j2jdf2l17n5864xxvinm31fnzqpkxs6grg6w69xqb61fakszpd") (f (quote (("alloc"))))))

(define-public crate-raw-window-handle-0.5.1 (c (n "raw-window-handle") (v "0.5.1") (h "0namvcx36iyqmvrp90v5dcfh3dvcz43pyi1f2cqgvvhwal1im1ag") (f (quote (("alloc")))) (r "1.64")))

(define-public crate-raw-window-handle-0.5.2 (c (n "raw-window-handle") (v "0.5.2") (h "1f9k10fgda464ia1b2hni8f0sa8i0bphdsbs3di032x80qgrmzzj") (f (quote (("std" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-raw-window-handle-0.6.0 (c (n "raw-window-handle") (v "0.6.0") (d (list (d (n "wasm-bindgen") (r "^0.2.87") (f (quote ("std"))) (o #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "0i5mxxfcgqmvmzg4f0lcz68g4xfd9jybhrdkxd2v37qv1q587aa2") (f (quote (("wasm-bindgen-0-2" "wasm-bindgen" "std") ("std" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-raw-window-handle-0.6.1 (c (n "raw-window-handle") (v "0.6.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (f (quote ("std"))) (o #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "12s1ck4v5ib1zclasr348sxpb76cnkk6hag603ki3z6xn6yvrhwc") (f (quote (("wasm-bindgen-0-2" "wasm-bindgen" "std") ("std" "alloc") ("alloc")))) (r "1.64")))

(define-public crate-raw-window-handle-0.6.2 (c (n "raw-window-handle") (v "0.6.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2.87") (f (quote ("std"))) (o #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "0ff5c648hncwx7hm2a8fqgqlbvbl4xawb6v3xxv9wkpjyrr5arr0") (f (quote (("wasm-bindgen-0-2" "wasm-bindgen" "std") ("std" "alloc") ("alloc")))) (r "1.64")))

