(define-module (crates-io ra w- raw-vec) #:use-module (crates-io))

(define-public crate-raw-vec-0.1.0 (c (n "raw-vec") (v "0.1.0") (h "0l92vql1mx1gdddqjswb3jyb3add1ds6v0xn37xf44ccpkzjf5bh")))

(define-public crate-raw-vec-0.2.0 (c (n "raw-vec") (v "0.2.0") (h "18zwd8zvavv4hsh0hf1sj01x8dkh03plf3lfqs4dkf61nz5phqg5")))

