(define-module (crates-io ra w- raw-printer) #:use-module (crates-io))

(define-public crate-raw-printer-0.1.0 (c (n "raw-printer") (v "0.1.0") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Graphics_Printing" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "1vd73q6ymazw6g4mwz4mh5mkazvwjvw9s6s90ix2nplzljrqisa1") (y #t)))

(define-public crate-raw-printer-0.1.1 (c (n "raw-printer") (v "0.1.1") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Graphics_Printing" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "08zcj2phm8s7lk33r0pwmzh4zgh94b3hf400xqbkxrd4a5rp5mgj")))

(define-public crate-raw-printer-0.1.2 (c (n "raw-printer") (v "0.1.2") (d (list (d (n "windows") (r "^0.51.1") (f (quote ("Win32_Foundation" "Win32_Graphics_Printing" "Win32_Graphics_Gdi"))) (d #t) (k 0)))) (h "0xmkd6nv1lgr32sy2hmv4i8pwr6373kp67w3g7030gbva0g89syb")))

