(define-module (crates-io ra w- raw-to-img) #:use-module (crates-io))

(define-public crate-raw-to-img-0.3.1 (c (n "raw-to-img") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5.0") (d #t) (k 0)) (d (n "rawloader") (r "^0.37.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0lxj1z7kskbz7k1w8v2d9v0jvrk53nwyv5apc7xva8mg86h3wpji")))

(define-public crate-raw-to-img-0.3.2 (c (n "raw-to-img") (v "0.3.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5.0") (d #t) (k 0)) (d (n "rawloader") (r "^0.37.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1br08p7bs6n7mq8qlqpaiz09lzy3pmdrymbwsyx8a8qvikqxp830")))

(define-public crate-raw-to-img-0.3.3 (c (n "raw-to-img") (v "0.3.3") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "imagepipe") (r "^0.5.0") (d #t) (k 0)) (d (n "rawloader") (r "^0.37.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0mrc3i5q5b2qg3pqdgqarvlpl6cqn2qx7klyvb96452vrr302brc")))

