(define-module (crates-io ra w- raw-btree) #:use-module (crates-io))

(define-public crate-raw-btree-0.1.0 (c (n "raw-btree") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0bmaknx4sg7p5phdip9832cq4myqsplmfy1avxnd1xjfl2sxmrhv") (f (quote (("dot") ("default")))) (y #t)))

(define-public crate-raw-btree-0.1.1 (c (n "raw-btree") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "10p364745mli1jnvw47z5gyi67r3jq8l0d963n3i37x9rdpvzzz9") (f (quote (("dot") ("default")))) (y #t)))

(define-public crate-raw-btree-0.1.2 (c (n "raw-btree") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1by74pqxms8vm4ilb5clv6dm9qsc0gf9xx737day7plz5wald7cf") (f (quote (("dot") ("default")))) (y #t)))

(define-public crate-raw-btree-0.1.3 (c (n "raw-btree") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "11yymxlx6fqisi9bivndk3wf3w8hmmi2yi0as6gmxnclp1kda206") (f (quote (("dot") ("default"))))))

(define-public crate-raw-btree-0.1.4 (c (n "raw-btree") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1w2vaasfnnaryjkrxfjn6v63yhng8jx9h1jcgc1wd7iazwi1knrb") (f (quote (("dot") ("default"))))))

(define-public crate-raw-btree-0.2.0 (c (n "raw-btree") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0kxpnjvfiy9hvr7aqj89kh93wml1403iqb4mizq7mwyr3kk7g6ls") (f (quote (("dot") ("default"))))))

(define-public crate-raw-btree-0.3.0 (c (n "raw-btree") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0p5z0dxzw55j4yfwngr7qizzg83lffq8djn5a85ljrx7k7bnwb7y") (f (quote (("dot") ("default"))))))

