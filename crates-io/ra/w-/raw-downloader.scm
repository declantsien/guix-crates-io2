(define-module (crates-io ra w- raw-downloader) #:use-module (crates-io))

(define-public crate-raw-downloader-0.1.0 (c (n "raw-downloader") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ka2xlvqdijzc7yv1zaq8zky3s0ry9igvm8xsgr95cjjjv60wp3a")))

