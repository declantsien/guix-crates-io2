(define-module (crates-io ra w- raw-string) #:use-module (crates-io))

(define-public crate-raw-string-0.1.0 (c (n "raw-string") (v "0.1.0") (h "0jzp2dkjq3dhrhw3mbrw1875km3rg4rdhslv0l7f5kx0r51x9gyi")))

(define-public crate-raw-string-0.1.1 (c (n "raw-string") (v "0.1.1") (h "1jcl8y0z9iig9dxc18hzfaylkww6098l0z302ahkmyidmm3m2g7g")))

(define-public crate-raw-string-0.2.0 (c (n "raw-string") (v "0.2.0") (h "1fk0j6wg2369pzm40ff0pz2c7y2hvwwimiq1wawbbixl42q2k5xd")))

(define-public crate-raw-string-0.2.1 (c (n "raw-string") (v "0.2.1") (h "01s8jiabnks7y5a56wpafqyz562rw24dhfpcnsvj2ch4a0qj0ndq")))

(define-public crate-raw-string-0.2.2 (c (n "raw-string") (v "0.2.2") (h "10w0wwv75p9pnm0gamd6w70sxs08b1zz03a4qmlygxpdzan2mf27")))

(define-public crate-raw-string-0.2.3 (c (n "raw-string") (v "0.2.3") (h "1bdna0pj1svqlla816pz7p19qzr34azhx0dprm0nf0anmh8rm52y")))

(define-public crate-raw-string-0.2.4 (c (n "raw-string") (v "0.2.4") (h "0nghb78jhhihiicwjfn12sq2srdisvfzzzcc98mzjmnm04a79yhq")))

(define-public crate-raw-string-0.3.0 (c (n "raw-string") (v "0.3.0") (h "0dg89jwvqxz8cjrc0zwhhncfrjm728lsdh8r06lcy4blrp0bw44b")))

(define-public crate-raw-string-0.3.1 (c (n "raw-string") (v "0.3.1") (h "1w6nar88j62lxx3lz7jj8ypj39xh068hj6iq6yx412p7ng56jx89")))

(define-public crate-raw-string-0.3.2 (c (n "raw-string") (v "0.3.2") (h "1r0xsk2cbcr4yi01ldsygx9d4hl0k5s9ynkp29g7sd6cqgmbabap") (f (quote (("old-nightly"))))))

(define-public crate-raw-string-0.3.3 (c (n "raw-string") (v "0.3.3") (h "17vnshkf6lbkn3ps2jjziw1v2gd678i64yirvkdq5rk039zrs9j5") (f (quote (("old-nightly"))))))

(define-public crate-raw-string-0.3.4 (c (n "raw-string") (v "0.3.4") (h "002xbcd0c8wali1l7jcyhbzdh4xw476l91yvk0m5bl7rzj59dfxr") (f (quote (("old-nightly"))))))

(define-public crate-raw-string-0.3.5 (c (n "raw-string") (v "0.3.5") (h "1whsq3g301vx79ackyngfvv1n9p1lyq2bv8gy7hzw1b99h9iwl70") (f (quote (("old-nightly"))))))

