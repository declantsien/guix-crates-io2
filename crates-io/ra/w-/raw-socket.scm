(define-module (crates-io ra w- raw-socket) #:use-module (crates-io))

(define-public crate-raw-socket-0.0.0 (c (n "raw-socket") (v "0.0.0") (h "1hryd227a8hw29ai1as24d92h8flgxwz686miwzpvj1cdnvp0grp")))

(define-public crate-raw-socket-0.0.1 (c (n "raw-socket") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "mio") (r "^0.6.22") (o #t) (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("io-driver"))) (o #t) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("macros"))) (d #t) (k 2)))) (h "04s538ls8aqs3rvl6z7b078zhw1k6qmvc91wcjy13bxb35bsv2al") (f (quote (("default" "async-tokio") ("async-tokio" "futures-core" "mio" "tokio"))))))

(define-public crate-raw-socket-0.0.2 (c (n "raw-socket") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.37") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "socket2") (r "^0.3.19") (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1ab6kb0ahfal65gwwl38sskzn805xpiy6xwin6h5jmhwbh8q8mf8") (f (quote (("default" "async-tokio") ("async-tokio" "tokio"))))))

