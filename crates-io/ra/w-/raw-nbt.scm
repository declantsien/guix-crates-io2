(define-module (crates-io ra w- raw-nbt) #:use-module (crates-io))

(define-public crate-raw-nbt-0.1.0 (c (n "raw-nbt") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.5") (f (quote ("rust_backend"))) (k 2)))) (h "0zi028v1ic9lyycw774r5llm32fz8h2wvqnzip7cwp4pz7kh2zw0")))

(define-public crate-raw-nbt-0.1.1 (c (n "raw-nbt") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.5") (f (quote ("rust_backend"))) (k 2)))) (h "0w9h311mlvg3h6fn7x09z11izc66c2g8wyfmaw4j1dkx5jwa09zk")))

