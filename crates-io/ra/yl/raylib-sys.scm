(define-module (crates-io ra yl raylib-sys) #:use-module (crates-io))

(define-public crate-raylib-sys-0.9.0 (c (n "raylib-sys") (v "0.9.0") (h "1y35r10hhjjk6564virzkrkhjigbrs5a56jv7rs7dcavg4s4grqz")))

(define-public crate-raylib-sys-0.9.1 (c (n "raylib-sys") (v "0.9.1") (h "09c8wiskv2n7q99py3ffph32b8rvlhwz3w5qzly71d2raba7qxv8")))

(define-public crate-raylib-sys-0.9.2 (c (n "raylib-sys") (v "0.9.2") (h "131gyv6njwqfwg6j47ygbvhr7akamrcyc88h6ddd1nxbc0qpxb6w")))

(define-public crate-raylib-sys-0.11.0 (c (n "raylib-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1dmd8xwbm4fvk0c4yy001bwk08w8i94gjsbr56p1v1l8h3lm8g1c")))

(define-public crate-raylib-sys-1.0.0 (c (n "raylib-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^0.11") (d #t) (k 1)))) (h "0s0r3b0w3fnfb1zzzip7hmnmi9xi5m9g5zaakdacbzl02rzsxjc4") (f (quote (("nobuild") ("default"))))))

(define-public crate-raylib-sys-3.0.0 (c (n "raylib-sys") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "1rrzj6mrn4xm8aip3l4b9wzl6kycgbf08fcbdwzplpvbabmhjsaw") (f (quote (("nobuild") ("default"))))))

(define-public crate-raylib-sys-3.5.0 (c (n "raylib-sys") (v "3.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "035kn7qrk3w9g0ri5vz6g0kihp2j4g16x06am7m2pyxx3isp2ahf") (f (quote (("nobuild") ("default"))))))

(define-public crate-raylib-sys-3.7.0 (c (n "raylib-sys") (v "3.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1") (d #t) (k 1)))) (h "0h38x2j0byl3psdc2i7ja0841asc21b4fkcihchwawqv4mg7pj90") (f (quote (("nobuild") ("default"))))))

(define-public crate-raylib-sys-5.0.0 (c (n "raylib-sys") (v "5.0.0") (d (list (d (n "bindgen") (r "^0.69.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)))) (h "0dzs4g3hwar49bw0qkjgwn5a2mlnb134hsy4iy2q70vcsqdyxj7c") (f (quote (("wayland") ("opengl_es_20") ("opengl_33") ("opengl_21") ("default") ("custom_frame_control"))))))

