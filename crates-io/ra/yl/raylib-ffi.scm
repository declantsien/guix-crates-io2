(define-module (crates-io ra yl raylib-ffi) #:use-module (crates-io))

(define-public crate-raylib-ffi-4.2.0 (c (n "raylib-ffi") (v "4.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1vhq4pldmxliw5wyrhnb7kx7ni433vhbgqan4hx8yirl4w2d85sx")))

(define-public crate-raylib-ffi-4.2.1 (c (n "raylib-ffi") (v "4.2.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "12z6igbqh085vvdwvr52xnbddlwjfkgh8gx76x08nqjjnh6d6c44")))

(define-public crate-raylib-ffi-4.5.0 (c (n "raylib-ffi") (v "4.5.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "02s3pksv3710gr5rhy6k5xwrwry23il9k4nq1brcwi12p1gh4b7y")))

(define-public crate-raylib-ffi-4.5.1 (c (n "raylib-ffi") (v "4.5.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1cflikclnpl7bdl6mgccdqgxv3mznlrz8qcw4mmq90d4x11pzm1h") (f (quote (("macros") ("enums") ("default" "enums" "macros" "colors") ("colors"))))))

(define-public crate-raylib-ffi-4.5.2 (c (n "raylib-ffi") (v "4.5.2") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1zasrv71vxfpvkxjbbjk4r72238j5sa1r6kh5rv7jixxzik9w4pd") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-raylib-ffi-4.5.3 (c (n "raylib-ffi") (v "4.5.3") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1ycf5ynnddln019p9h6252migw2jcqx5hwpbknjfdwgwxx99znfa") (f (quote (("macros") ("default" "macros"))))))

(define-public crate-raylib-ffi-4.5.4 (c (n "raylib-ffi") (v "4.5.4") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "08ksd118dg4b3w7l21ihmic2zdqnyf18ix13v1sxb802x5b5c1j4") (f (quote (("macros") ("dylib") ("default" "macros"))))))

(define-public crate-raylib-ffi-4.5.5 (c (n "raylib-ffi") (v "4.5.5") (d (list (d (n "bindgen") (r "^0.65.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1sg01qvh7xnz6wkz991k1b5yh2ljjl3xvl0hqlh3mx97lfg0gxq6") (f (quote (("macros") ("dylib") ("default" "macros"))))))

(define-public crate-raylib-ffi-5.0.0 (c (n "raylib-ffi") (v "5.0.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0cl5qnikncglxahkshfnxaxjn1w13d8f6f4idygxcfs69y8vhdfh") (f (quote (("macros") ("dylib") ("default" "macros"))))))

(define-public crate-raylib-ffi-5.0.1 (c (n "raylib-ffi") (v "5.0.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1fhk852g48qvsx2n5d7l7dkf3dsjpkxn0p1rjyl6lmznhblbzxnm") (f (quote (("macros") ("dylib") ("default" "macros"))))))

