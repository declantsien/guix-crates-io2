(define-module (crates-io ra yl raylib4-sys) #:use-module (crates-io))

(define-public crate-raylib4-sys-0.1.0 (c (n "raylib4-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0b9972hj3in68ir9c3phghrcl0wijjjy7fhw3gkr00yxc5zgjkjh")))

(define-public crate-raylib4-sys-0.1.1 (c (n "raylib4-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0skv8jgwrmxk0jw5qcz39kh6x7ycwsl0w1zaicm2hdwpmnsp4rq7")))

(define-public crate-raylib4-sys-0.1.2 (c (n "raylib4-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "02a972r6gbaghr2dsp7f0swp51bf129xssjln8nn85qx93k8a4d8")))

