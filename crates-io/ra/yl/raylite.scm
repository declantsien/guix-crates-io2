(define-module (crates-io ra yl raylite) #:use-module (crates-io))

(define-public crate-raylite-0.1.0 (c (n "raylite") (v "0.1.0") (h "1cfhpiqhidcd4d9yps92nacgwv1qfwk9yg0azgmzavf13xvcyr8x")))

(define-public crate-raylite-0.1.1 (c (n "raylite") (v "0.1.1") (h "0v81c6jlqy6ngmngi9bjw0hr9vqmpabbfm5h55mkipi11w42zm95")))

(define-public crate-raylite-0.1.5 (c (n "raylite") (v "0.1.5") (h "1xq3cpv63mqxdbs7vi9hbc58sqbvmfj2zs8s4gsxzc9ylkwm30z9")))

(define-public crate-raylite-0.1.6 (c (n "raylite") (v "0.1.6") (h "046zfd9fzfqvw7awsc9hxk2bqnrpwrl2vv78ilk1kgzci5igg24g")))

(define-public crate-raylite-0.1.7 (c (n "raylite") (v "0.1.7") (h "1y7ji3ld4xq44sb4xfhmrg7lp14v41zqj7y4d4bib805ly0rgzdc")))

