(define-module (crates-io ra yl raylib-project-generator) #:use-module (crates-io))

(define-public crate-raylib-project-generator-1.0.0 (c (n "raylib-project-generator") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02vx40bkdadws6lk64qvcblw0vw1vrc8dqwj3a7f2azvhx7y00fl")))

(define-public crate-raylib-project-generator-1.1.0 (c (n "raylib-project-generator") (v "1.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a7i446kj4hf3c5as82m03y1cpbi8s09cmr816g4ms4b02x3dxq3")))

