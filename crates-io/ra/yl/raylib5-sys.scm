(define-module (crates-io ra yl raylib5-sys) #:use-module (crates-io))

(define-public crate-raylib5-sys-0.1.0 (c (n "raylib5-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "0d03ybfyq3agwiq2kkcbx4xlgwm6xczqn58m1j1p5k5rxsyacxa4") (y #t)))

(define-public crate-raylib5-sys-1.0.0 (c (n "raylib5-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "1ipbckjaxh8kxy4gi8svqz1hd7akzghyimg938s35akj23nsza14")))

