(define-module (crates-io ra mp ramp-primes) #:use-module (crates-io))

(define-public crate-ramp-primes-0.1.0 (c (n "ramp-primes") (v "0.1.0") (d (list (d (n "ramp") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1x7jhxpginsxw1qsm7dh23b9sny1h810akms40nmfx4r7anjq0q1")))

(define-public crate-ramp-primes-0.2.0 (c (n "ramp-primes") (v "0.2.0") (d (list (d (n "ramp") (r "^0.5.5") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "0c0652qg2vqrvzbvrf5kg923c735nig5iaa4b1lb8shkqhzhqdaa")))

(define-public crate-ramp-primes-0.3.0 (c (n "ramp-primes") (v "0.3.0") (d (list (d (n "ramp") (r "^0.5.7") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "11pcbmjjaq7rz8ldhfyr736bq1dv1w765mkzfb6f4icc3c0rzac2")))

(define-public crate-ramp-primes-0.4.0 (c (n "ramp-primes") (v "0.4.0") (d (list (d (n "ramp") (r "^0.5.7") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "16hk8bxwhzga4dfv7hwc3rr2mn4xk0anj71cym977f1ja6vy79ds")))

(define-public crate-ramp-primes-0.4.1 (c (n "ramp-primes") (v "0.4.1") (d (list (d (n "ramp") (r "^0.5.7") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1z3swnzpidf1gkaayrn2fp55g16s96j0jdhii5zi86zvc2l2l60m")))

