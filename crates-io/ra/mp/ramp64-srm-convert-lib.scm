(define-module (crates-io ra mp ramp64-srm-convert-lib) #:use-module (crates-io))

(define-public crate-ramp64-srm-convert-lib-0.1.0 (c (n "ramp64-srm-convert-lib") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0w8v6ka3wq8n6yvbny4ryvwmxcgrhnc8bc35blpnv8cpwafrv7km") (y #t)))

(define-public crate-ramp64-srm-convert-lib-0.2.0 (c (n "ramp64-srm-convert-lib") (v "0.2.0") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b29zdyvyhx18shvxhsz6y07sjy52c14s7834ih9v3h56xfhmg6r") (y #t)))

(define-public crate-ramp64-srm-convert-lib-0.4.0 (c (n "ramp64-srm-convert-lib") (v "0.4.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "0xaxwzq15ws9zprrcy19y5148fmasb9fvxnlz1scyp43anb01l6s")))

(define-public crate-ramp64-srm-convert-lib-0.5.0 (c (n "ramp64-srm-convert-lib") (v "0.5.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "1sx34w6p2jhid8in88p88ncw8ixjdk3zdfll62a8wq6fdmib94df")))

