(define-module (crates-io ra mp ramp-maker) #:use-module (crates-io))

(define-public crate-ramp-maker-0.1.0 (c (n "ramp-maker") (v "0.1.0") (d (list (d (n "fixed") (r "^1.5.0") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "17lbv02vbvil9sq7iizrbhva7hkwfks4fyln1xy53bn9gvidb65c") (f (quote (("std"))))))

(define-public crate-ramp-maker-0.1.1 (c (n "ramp-maker") (v "0.1.1") (d (list (d (n "fixed") (r "^1.5.0") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1gfpvv0gmfdsf7mlvj2kz6f01d6nb73fp32d0jrks5a45rvhlvm9") (f (quote (("std"))))))

(define-public crate-ramp-maker-0.2.0 (c (n "ramp-maker") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "az") (r "^1.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.5.0") (f (quote ("az" "num-traits"))) (d #t) (k 0)) (d (n "fixed-sqrt") (r "^0.2.4") (d #t) (k 0)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "0b192mj1p3b4pj9nxz5gb7vd9p5n8iqns37p5d1xcndfhd53wjjr") (f (quote (("std"))))))

