(define-module (crates-io ra qm raqm-sys) #:use-module (crates-io))

(define-public crate-raqm-sys-0.1.0 (c (n "raqm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)))) (h "0ql5am0hn32xnb5qwch2cc2m3r2hsh7hln9wc8aj4h96sss517c3")))

(define-public crate-raqm-sys-0.2.0 (c (n "raqm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "freetype") (r "^0.4.0") (d #t) (k 0)))) (h "05c3pcvyah3dqqwzls0qb46vvwpay16ma22vdx87giiqg4i6h1jd")))

(define-public crate-raqm-sys-0.2.1 (c (n "raqm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.17") (d #t) (k 1)) (d (n "freetype") (r "^0.4.0") (d #t) (k 0)))) (h "03nsbhr6y6d4xxxfn0qqr02fxmh1qfdxvx56467wk8gfc7mximsc")))

