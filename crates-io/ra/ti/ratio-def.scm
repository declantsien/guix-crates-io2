(define-module (crates-io ra ti ratio-def) #:use-module (crates-io))

(define-public crate-ratio-def-0.1.0 (c (n "ratio-def") (v "0.1.0") (h "0sm1xnp63lcgiy3w64hz0i70am9gb4ijrcwjpybw8chzs2rqpnyp")))

(define-public crate-ratio-def-0.1.1 (c (n "ratio-def") (v "0.1.1") (h "1k56r4y9sgz8kj2ppxpf5iz94z7kbgz1138fvb3yzd6jxqn3p5df")))

(define-public crate-ratio-def-0.1.2 (c (n "ratio-def") (v "0.1.2") (h "1c8ss02gbpj6wa6zwkizs2icjkffyahmhcc2hhkncw3kfv7z9363")))

(define-public crate-ratio-def-0.2.0 (c (n "ratio-def") (v "0.2.0") (h "0vyy6gwr4pc079lrjxbn7gilp1894sl6xwh6f9cv4baq047i2jj1")))

(define-public crate-ratio-def-0.3.0 (c (n "ratio-def") (v "0.3.0") (h "0blky5b46cwy8z6afxsc162x01682cxl1k10cprxv1zw1n4h2qv4")))

(define-public crate-ratio-def-0.3.1 (c (n "ratio-def") (v "0.3.1") (h "1yw0myiin4qp9zhi6pia290q2whbfrp9x92gcr1rxrxnz7mlrxri")))

