(define-module (crates-io ra ti rating) #:use-module (crates-io))

(define-public crate-rating-0.0.1 (c (n "rating") (v "0.0.1") (d (list (d (n "rust_decimal") (r "^1.28") (f (quote ("maths"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.28") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0r6gjqj0896hpv5mjs5qj2dnfjmwf92c6jmy4qqqcbi7gaxnfzv3")))

(define-public crate-rating-0.1.0 (c (n "rating") (v "0.1.0") (d (list (d (n "rust_decimal") (r "^1.28") (f (quote ("maths"))) (d #t) (k 0)) (d (n "rust_decimal_macros") (r "^1.28") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0fhiraaikz3abxw8y5xnzpvz2vh2bbhahinv0i5b19hxbxqxplnz")))

