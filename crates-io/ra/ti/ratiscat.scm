(define-module (crates-io ra ti ratiscat) #:use-module (crates-io))

(define-public crate-ratiscat-0.1.0 (c (n "ratiscat") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1cn3waxsch5zws6pv61cpljcagp8g3a5fnf3wlmqq6ndplq97xb3") (r "1.71.0")))

(define-public crate-ratiscat-0.2.0 (c (n "ratiscat") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0g64g6mxsmlsqz3mhbpva5jq7zdi3l8sjlifrjz6hhbrv2604ikm") (r "1.71.0")))

(define-public crate-ratiscat-0.3.0 (c (n "ratiscat") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "0yac3cb36n1b51dg7wq5vqqw0di6g0qys073m923bqp0nng6v28w") (r "1.71.0")))

(define-public crate-ratiscat-0.3.1 (c (n "ratiscat") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1y72wgkp57n2yvlazms82w1r8xnh4zmaskwhxpcfl9g9fs20kn3q") (r "1.71.0")))

(define-public crate-ratiscat-0.4.0 (c (n "ratiscat") (v "0.4.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "01ndim8fyysjhxp52sk13s04cl8ffihh6l99wiyvk38bxpglrpvi") (r "1.71.0")))

(define-public crate-ratiscat-0.4.1 (c (n "ratiscat") (v "0.4.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.26") (d #t) (t "cfg(unix)") (k 0)))) (h "0w8pwdsby1nsy73dgjwljxnzqdgaldryx2na62pzd12ig5ih5zjc") (r "1.71.0")))

