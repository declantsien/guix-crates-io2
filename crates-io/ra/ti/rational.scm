(define-module (crates-io ra ti rational) #:use-module (crates-io))

(define-public crate-rational-0.1.0 (c (n "rational") (v "0.1.0") (h "0xps5j9f0vd1pp9fgbpmkyjgv43fqpz09c08xc953rwxwv1c6y5p") (y #t)))

(define-public crate-rational-0.1.1 (c (n "rational") (v "0.1.1") (h "0zhpab421n9qdjycblyy5h9p1pxi8l42b1d90xzkc4526rkdg90c")))

(define-public crate-rational-0.1.2 (c (n "rational") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0qcvswspaa0wal2dcm619i7wi17g3gymb4pbk89804idb0vy805x")))

(define-public crate-rational-0.2.2 (c (n "rational") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1js9wci6cd10fg1ckrchgx4jji5ggkjygw6g9iq6262w93425chh")))

(define-public crate-rational-0.2.3 (c (n "rational") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "091mlyc3jv6any6i60piwc2nsipgcnqmbgx03rkp64pm1l916f9a")))

(define-public crate-rational-0.3.0 (c (n "rational") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "08z20asbi05kq6w7al97ffz1lis9dibk99b5pd5w7zcgs24al47n")))

(define-public crate-rational-1.0.0 (c (n "rational") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "04l51j6za08l94hnmi5zk4x7y2iqmvzhw9wvl4b14iwhsrw1gm73")))

(define-public crate-rational-1.1.0 (c (n "rational") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0h4zbh2ypd183aicf9l9c5d0ql6zx1gir652jgnd3iljrnxq258z")))

(define-public crate-rational-1.2.0 (c (n "rational") (v "1.2.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "013xfbcvyyld8wlkkicpxz02cbhdsakwingp3814sd095hcsj7sl")))

(define-public crate-rational-1.2.1 (c (n "rational") (v "1.2.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ipkp8yafgcadscp0g7pqy0gbg37b8z097l81kk14avx8x5qmnlx")))

(define-public crate-rational-1.2.2 (c (n "rational") (v "1.2.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1kh146vl86gchlsqm8xqz37wnrp1d90dfgbhrjq5735805nc7v2k")))

(define-public crate-rational-1.3.0 (c (n "rational") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "103d0ijiwbmy7akvhl63li6wni8z60y78axxbfjaswkf4d69vz1i")))

(define-public crate-rational-1.4.0 (c (n "rational") (v "1.4.0") (d (list (d (n "num-traits") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1m31ax8ylsljksi1pz48kz5d703d7nh7ffzgmpf0ya0v9nm9yf1r")))

(define-public crate-rational-1.5.0 (c (n "rational") (v "1.5.0") (d (list (d (n "num-traits") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1b2grvvnp6gplb1ahr4s1ab7vs0vmvlc5y4d6vcqsfbsl205pjq4")))

