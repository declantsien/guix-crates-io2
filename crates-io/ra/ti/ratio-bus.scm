(define-module (crates-io ra ti ratio-bus) #:use-module (crates-io))

(define-public crate-ratio-bus-0.1.0 (c (n "ratio-bus") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "snafu") (r "^0.7.2") (d #t) (k 0)))) (h "0asg9aprml6dvg19bpkbajcy30qjb540bsljmws7pqaj5c9zhpvv")))

