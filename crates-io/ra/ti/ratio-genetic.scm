(define-module (crates-io ra ti ratio-genetic) #:use-module (crates-io))

(define-public crate-ratio-genetic-0.1.0 (c (n "ratio-genetic") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1i4pzgaawa8y47x5g7jllg33b58mp1cyl2is8spf1yisfcscmaa3")))

(define-public crate-ratio-genetic-0.1.1 (c (n "ratio-genetic") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "04p7ypfbzrrxc7m87r5a9bd16qkkfc2xc26w0c5x3aqixg5r9p92")))

(define-public crate-ratio-genetic-0.1.2 (c (n "ratio-genetic") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "106sxv7819w42hngg347yjphcxvdvrx0agxy66b87gc689vf3rq8")))

(define-public crate-ratio-genetic-0.2.0 (c (n "ratio-genetic") (v "0.2.0") (d (list (d (n "hdrhistogram") (r "^7.5.2") (f (quote ("sync"))) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "072nzhzq6fdgrf5nvpwpz4j027aidw4gki8g9k8mcajvrkclg67z")))

(define-public crate-ratio-genetic-0.3.0 (c (n "ratio-genetic") (v "0.3.0") (d (list (d (n "hdrhistogram") (r "^7.5.2") (f (quote ("sync"))) (k 0)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1kw8ypml9ylfgwpvpc40098zm5lv2z00rs975dcg7y3mircvpjzv")))

