(define-module (crates-io ra ti rational_deduction) #:use-module (crates-io))

(define-public crate-rational_deduction-0.0.0 (c (n "rational_deduction") (v "0.0.0") (h "0sms0kqv5zqmwfaflvxvxlq8id14x2r8vvjr3b080klxy10ijyiz")))

(define-public crate-rational_deduction-0.0.1 (c (n "rational_deduction") (v "0.0.1") (d (list (d (n "exprz-core") (r "^0.0.1") (d #t) (k 0)))) (h "08ynbqylqzvyj9jjr9avy7xjc0q79yc4akagkkzkwjb4vml9f3ax")))

(define-public crate-rational_deduction-0.0.2 (c (n "rational_deduction") (v "0.0.2") (d (list (d (n "exprz") (r ">=0.0.5, <0.0.6") (d #t) (k 0)))) (h "023832qxs3kz9n4al04n44b9j9shn4vxvr23hsagyl21sflv3dji")))

(define-public crate-rational_deduction-0.0.3 (c (n "rational_deduction") (v "0.0.3") (d (list (d (n "bitvec") (r "^0.21.1") (f (quote ("alloc"))) (k 0)) (d (n "exprz") (r "^0.0.8") (d #t) (k 0)))) (h "0h63njmi13ipwsifgbl4hc5v9c1349l1549mhjf9pyl2paqqgz8d")))

(define-public crate-rational_deduction-0.0.4 (c (n "rational_deduction") (v "0.0.4") (d (list (d (n "bitvec") (r "^0.21.1") (f (quote ("alloc"))) (k 0)) (d (n "exprz") (r "^0.0.9") (d #t) (k 0)))) (h "0l2pa2zjh6rraabnq9d3vxfpklbx8bnf9rq1awkbx50fb37ixsgl")))

(define-public crate-rational_deduction-0.0.5 (c (n "rational_deduction") (v "0.0.5") (d (list (d (n "bitvec") (r "^0.21.1") (f (quote ("alloc"))) (k 0)) (d (n "exprz") (r "^0.0.12") (f (quote ("shape"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)))) (h "0hmb6994jdmc47spfbnpimiv0c3xb6v98vksh3vdyi0aw48rim4l")))

