(define-module (crates-io ra do radoscript) #:use-module (crates-io))

(define-public crate-radoscript-0.1.0 (c (n "radoscript") (v "0.1.0") (h "1n6i39il9krvgmbnfdzqmjp0ig3xf7bn4nl0kjqn3qgg38igsk4i")))

(define-public crate-radoscript-0.1.1 (c (n "radoscript") (v "0.1.1") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "06jldhb66igsm16whwx465miqi27gggivsrp28qy37mvqilbb9fl")))

(define-public crate-radoscript-0.1.2 (c (n "radoscript") (v "0.1.2") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)))) (h "1isychbk0vqlbw5m1vq71bkfmry1iwk34x13zswavgr24vwvfj78")))

