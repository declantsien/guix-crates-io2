(define-module (crates-io ra do rados_hi) #:use-module (crates-io))

(define-public crate-rados_hi-0.1.0 (c (n "rados_hi") (v "0.1.0") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "1i57b2p8iwqc9y0zannfs89lxmcyjli8jcsmjqp4dra58zmhwmqh")))

(define-public crate-rados_hi-0.2.0 (c (n "rados_hi") (v "0.2.0") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "1kiiysxwwic8bjvisjmzjqbc1scn7h4yhjznc54znxbj3hi5pxpi")))

(define-public crate-rados_hi-0.2.1 (c (n "rados_hi") (v "0.2.1") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "0ib0x6wf7xj4s9qz5bnn0dz4s58rm927axaa6vjc44flc0qzginh")))

(define-public crate-rados_hi-0.2.2 (c (n "rados_hi") (v "0.2.2") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "1azkk78r0pwx2p26x0bcynhzvmn2xy8sxa2vbn1w8c2mn0qmss6y")))

(define-public crate-rados_hi-0.2.3 (c (n "rados_hi") (v "0.2.3") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "1fb6n8kay5dk6fvyl59q09bral4b89lr99vrapk3ibs25fmjnvpd")))

(define-public crate-rados_hi-0.2.4 (c (n "rados_hi") (v "0.2.4") (d (list (d (n "ceph-rust") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "gstuff") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "scoped-pool") (r "^1") (d #t) (k 2)) (d (n "timeit") (r "^0.1") (d #t) (k 2)))) (h "077yjnq932va7i61rcyy67511qh0v7z1p39g1zrf3fb20w8lfjnk")))

