(define-module (crates-io ra do radolan) #:use-module (crates-io))

(define-public crate-radolan-0.0.1 (c (n "radolan") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "time") (r "^0.3.36") (f (quote ("serde" "macros" "parsing" "formatting"))) (d #t) (k 0)))) (h "17ag0nkwsfx525n72pwd8w9i7r6dlxw5glcrqmzq2gancmncqmgd")))

