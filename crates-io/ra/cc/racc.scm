(define-module (crates-io ra cc racc) #:use-module (crates-io))

(define-public crate-racc-0.0.1 (c (n "racc") (v "0.0.1") (h "15d9b0ghmq12pflf388816l1vhgr1s0yi641z12mymnvw2wyd4p8")))

(define-public crate-racc-0.0.2 (c (n "racc") (v "0.0.2") (h "1lrd0asqkdf5nljfi7pggwhpwpp4isf4sfqmx6l3a4jwgsna5c4i")))

(define-public crate-racc-0.0.3 (c (n "racc") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "ramp_table") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1mlwly9ly7fxi5bkg4alhyq2idgl9g98746ncjzqiijn95nc3gqh") (y #t)))

(define-public crate-racc-0.0.4 (c (n "racc") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "0.4.*") (f (quote ("release_max_level_warn"))) (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "ramp_table") (r "^0.1.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "151mm5ympw59r74rj2lna1wxp9yfrcpn1w53812bfd6ns5psz4rs") (f (quote (("racc_log") ("default" "racc_log"))))))

