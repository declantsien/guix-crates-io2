(define-module (crates-io ra cc raccoin_ui) #:use-module (crates-io))

(define-public crate-raccoin_ui-0.1.0 (c (n "raccoin_ui") (v "0.1.0") (d (list (d (n "slint") (r "^1.2.2") (d #t) (k 0)) (d (n "slint-build") (r "^1.2.2") (d #t) (k 1)))) (h "1pxgxfj8fkmdvhspw2waa4gab38bjbxdx36dzyqm08lg2gry96gc")))

(define-public crate-raccoin_ui-0.2.0 (c (n "raccoin_ui") (v "0.2.0") (d (list (d (n "slint") (r "^1.3.2") (f (quote ("compat-1-2" "std"))) (k 0)) (d (n "slint-build") (r "^1.3.2") (d #t) (k 1)))) (h "0k6k128gzml224sr9f0asablqqfvslrzlkdcadwkibm7v8x2ymfw")))

