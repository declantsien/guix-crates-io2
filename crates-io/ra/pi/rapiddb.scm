(define-module (crates-io ra pi rapiddb) #:use-module (crates-io))

(define-public crate-rapiddb-0.1.0 (c (n "rapiddb") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "08cclg1z7bky4529xj3qnjq763qpdl5m6alx49qil1ixsx16av5r")))

(define-public crate-rapiddb-0.1.1 (c (n "rapiddb") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1djijk0q38gabyg1dccnhk7yl6v9phy6lfy29hhq61xrhqsvkf29")))

(define-public crate-rapiddb-0.1.2 (c (n "rapiddb") (v "0.1.2") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0ijg21blg3mlrn1kd2n6wz7i4fq4r4lh4dgbivdmgqvlv3qdsv9n")))

(define-public crate-rapiddb-0.1.3 (c (n "rapiddb") (v "0.1.3") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1yk5r1md0ac6y3ps8gfjhlwm8rvz1w8hhhd96n708zgy9vrhwhcn")))

(define-public crate-rapiddb-0.1.4 (c (n "rapiddb") (v "0.1.4") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0vjzsw8jvd05jfpbga5vkjhsj731gadpxc85n7hczi6jzb28p604")))

(define-public crate-rapiddb-0.1.5 (c (n "rapiddb") (v "0.1.5") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0hz8h7rnc3gahqxypj1355h4wikd0wr1461zam30zb114xl5f9fm")))

(define-public crate-rapiddb-0.1.6 (c (n "rapiddb") (v "0.1.6") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "mysql") (r "^22.1") (d #t) (k 0)) (d (n "redis") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0xrndb2d844fz8bm7a1zpdx936hnc4zmdlmkj33lvgj5vxjpcf7l")))

(define-public crate-rapiddb-0.1.7 (c (n "rapiddb") (v "0.1.7") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1vvllwrhcv0r43fqxiqkmgsdjji424qyvmp1fxb6sl3fa9jcz069")))

(define-public crate-rapiddb-0.1.8 (c (n "rapiddb") (v "0.1.8") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1nvkbcxzhnlszh37cvzw9qbzg5ni6wpi06w8rfd0iicdk7zlmlvw")))

(define-public crate-rapiddb-0.1.9 (c (n "rapiddb") (v "0.1.9") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0pfyb9pz80kv0h4rk0ddhabx8ddcfxdvg3n6czgmwbyc4ii32l6a")))

(define-public crate-rapiddb-0.1.10 (c (n "rapiddb") (v "0.1.10") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "079pznq33s76z6bx7m06d3287z93b1pqargp5hc2h47gn0mmn6zy")))

(define-public crate-rapiddb-0.1.11 (c (n "rapiddb") (v "0.1.11") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0pcsicd6wqw396mhkwgwxp9fz66ka7y5aajgw8wg6g1j8vnx4p80")))

(define-public crate-rapiddb-0.1.12 (c (n "rapiddb") (v "0.1.12") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "068lfnn0y43n09sg549jv1p56x0945cz7vmzqs52v2jdj64l3xls")))

(define-public crate-rapiddb-0.1.13 (c (n "rapiddb") (v "0.1.13") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0fx2hc9j146ksyxa76qaw95yvkmyf7ngvl02hj9naqsfyb92q7qx")))

(define-public crate-rapiddb-0.1.14 (c (n "rapiddb") (v "0.1.14") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1as85akk8a3j276xvif46sw08x2mr843p71jbc3mcf4qs3vjjba1")))

(define-public crate-rapiddb-0.1.15 (c (n "rapiddb") (v "0.1.15") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1bqh7yqnk44yf3zycirhkl23rxxncbjns6kawxlf4d76x2rqx4ld")))

(define-public crate-rapiddb-0.1.16 (c (n "rapiddb") (v "0.1.16") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0cfld0dry9csc01hhzqgh39kcjf16pvlf2h6wxi7kq2scma8praa")))

(define-public crate-rapiddb-0.1.17 (c (n "rapiddb") (v "0.1.17") (d (list (d (n "memmap2") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1z3n5ziclgdg60j1vrqinrz1bmm96mplgf4wvfpa8ji69vd1zb77")))

(define-public crate-rapiddb-0.1.18 (c (n "rapiddb") (v "0.1.18") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0jhnkxcajxrm3jv2vcybffacqc8qjfwa0llpcc5hp0i33hgrh41j")))

(define-public crate-rapiddb-0.1.19 (c (n "rapiddb") (v "0.1.19") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "08853m81rbswdjy6lc9147d33qmgg0hb5gyy5jpxy53n7kiwsh94")))

(define-public crate-rapiddb-0.1.20 (c (n "rapiddb") (v "0.1.20") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "03jjnfh0cfizjf7af229plap681nk8kd0nv70j6gxqxhs7lbzjbs")))

(define-public crate-rapiddb-0.1.21 (c (n "rapiddb") (v "0.1.21") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1w9abgniw6f9z13dz1ypvq2hh19kic96afcg1nszvdbk5xa92k2y")))

(define-public crate-rapiddb-0.1.22 (c (n "rapiddb") (v "0.1.22") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0d1ph05qgiqgzp8kk4ilnai6ad10pscnsd3admk9g4cpc0xxr91w")))

(define-public crate-rapiddb-0.1.23 (c (n "rapiddb") (v "0.1.23") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "03j283s50z8pz46j994z2pfqhp2ak90fq74sghkw7an2lwmdjysw")))

(define-public crate-rapiddb-0.1.24 (c (n "rapiddb") (v "0.1.24") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0pk41jh104msghpsj0cfd532ij6ac86ih06myniv1cj3lrh95qk5")))

(define-public crate-rapiddb-0.1.26 (c (n "rapiddb") (v "0.1.26") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1860cv568r9d8cpjhyd7hfwzcx2z6q2zlg4iaibsblrvlzmh1azr")))

(define-public crate-rapiddb-0.1.27 (c (n "rapiddb") (v "0.1.27") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "045bzas9sh5b5r6q4zc2g8wxlvrir31ab0g7iimj59az7cmzsmwy")))

(define-public crate-rapiddb-0.1.28 (c (n "rapiddb") (v "0.1.28") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1nz2jbasyxdan7xcpy09g91lj039knna66mnck1qg9c3wm6yrm19")))

(define-public crate-rapiddb-0.1.29 (c (n "rapiddb") (v "0.1.29") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0866k1w4m9j4mpajzjs586g91fizhic50y0866ksiqi65d079i96")))

(define-public crate-rapiddb-0.1.30 (c (n "rapiddb") (v "0.1.30") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0anbpyqzwi9hwh12yxarcidwzkn05fz38ib4ds756hl5m6mswqxg")))

(define-public crate-rapiddb-0.1.31 (c (n "rapiddb") (v "0.1.31") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "04v05d4w2ks5l594vgmlj31nhz360rs1qdn22n9zb4vyzscal370")))

