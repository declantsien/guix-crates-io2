(define-module (crates-io ra pi rapidfuzz) #:use-module (crates-io))

(define-public crate-rapidfuzz-0.1.0 (c (n "rapidfuzz") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0vd4jgsrx31wy6dnzbfcpay5mm61a6bc2fsdpz5zq9h5d03cf58i")))

(define-public crate-rapidfuzz-0.3.0 (c (n "rapidfuzz") (v "0.3.0") (h "1js00fpxl3zr1jp0lb5q1qjrc8a6nfil2rwc2jlbfzfgwnnfgkrc")))

(define-public crate-rapidfuzz-0.3.1 (c (n "rapidfuzz") (v "0.3.1") (h "0cwwkiw75n0m3114i4y1468kzb9fhy6zidcp2cgb1l7zbkm6yf6c")))

(define-public crate-rapidfuzz-0.3.2 (c (n "rapidfuzz") (v "0.3.2") (h "1d3s6fvgrvj2cmzj3hbjh5rgkwyamlzx2nkl9axxnnadcnxjljbr")))

(define-public crate-rapidfuzz-0.4.0 (c (n "rapidfuzz") (v "0.4.0") (h "0qdrz3gvf3dwxzbhy29p3177dv9vc7x8pmf4l7sxcsgggr91jrps")))

(define-public crate-rapidfuzz-0.5.0 (c (n "rapidfuzz") (v "0.5.0") (h "06v0n6lx8pfygvy9gkvvccc1dvi93i2mxc9bji0him31xbjh83i7")))

