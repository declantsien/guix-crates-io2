(define-module (crates-io ra pi rapid-fs) #:use-module (crates-io))

(define-public crate-rapid-fs-0.1.0 (c (n "rapid-fs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1x0n50p87bhyb556qac1mb1firrjg2p4sqv8jzr2n4hjrlfz5c37")))

(define-public crate-rapid-fs-0.1.1 (c (n "rapid-fs") (v "0.1.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0zj4gghf5g71hxrdzg5pyby2iqg4rkhjlrl4ygg2lxg3ka2sz97n")))

