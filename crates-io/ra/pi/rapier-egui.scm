(define-module (crates-io ra pi rapier-egui) #:use-module (crates-io))

(define-public crate-rapier-egui-0.1.0 (c (n "rapier-egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.23") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "rapier3d") (r "^0.17") (f (quote ("debug-render"))) (d #t) (k 0)))) (h "1pva944j7zx5d0k8c8ify9js0rcdky6yz4bvhirsl3sw27qr0jn0")))

