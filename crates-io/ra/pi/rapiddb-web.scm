(define-module (crates-io ra pi rapiddb-web) #:use-module (crates-io))

(define-public crate-rapiddb-web-0.1.22 (c (n "rapiddb-web") (v "0.1.22") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1wv1b1x7bv5b1syrikax8p1wb1d9anifrnkdx4bpfnh74kvp903k")))

(define-public crate-rapiddb-web-0.1.23 (c (n "rapiddb-web") (v "0.1.23") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "07w3xcaavi2r7165ynkass7c3wxhnydwpz3x094mv7z1qgigxkpz")))

(define-public crate-rapiddb-web-0.1.24 (c (n "rapiddb-web") (v "0.1.24") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "073lqf4l7iqx4r098lhcwdbn73j0cmd2cyb3wrh44rsc386y2ihm")))

(define-public crate-rapiddb-web-0.1.26 (c (n "rapiddb-web") (v "0.1.26") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1415slxqjw1m68cpyqds25j3mk7fm18ni4z6kgn7p1f2dh427cxa")))

(define-public crate-rapiddb-web-0.1.27 (c (n "rapiddb-web") (v "0.1.27") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "080r0i2izvvi1rdxfcnw5rc55jn7yhi55kb19al8zsv7pg1xdggy")))

(define-public crate-rapiddb-web-0.1.28 (c (n "rapiddb-web") (v "0.1.28") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1kri77irxri02aqqd70mhkn6j4a3bc5fdfx7qszl7fjaxafmah1i")))

(define-public crate-rapiddb-web-0.1.29 (c (n "rapiddb-web") (v "0.1.29") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1z7h6gk4s98zjz64h81x6lvqjkcdl17lsfgzlf5dnh6fc17jds58")))

(define-public crate-rapiddb-web-0.1.30 (c (n "rapiddb-web") (v "0.1.30") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0l13rsmd4v9gqrdxhviwlnazlfn3g7dgpwx3zkf4xj03l7ck1c73")))

(define-public crate-rapiddb-web-0.1.31 (c (n "rapiddb-web") (v "0.1.31") (d (list (d (n "rapiddb") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1n8l66zrd4vgaj9cr7h14pq2mhhfva0vi2nm2djz4ji7j39f8w17")))

