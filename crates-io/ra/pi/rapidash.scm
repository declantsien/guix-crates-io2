(define-module (crates-io ra pi rapidash) #:use-module (crates-io))

(define-public crate-rapidash-0.1.0 (c (n "rapidash") (v "0.1.0") (d (list (d (n "ballista") (r "^0.9.0") (d #t) (k 0)) (d (n "datafusion") (r "^13.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (d #t) (k 0)))) (h "17prddkid7g23h4kab59sfkhs7ml3kdhpjx71g1vb7681yg9m8dw")))

