(define-module (crates-io ra pi rapid-qoi) #:use-module (crates-io))

(define-public crate-rapid-qoi-0.0.0 (c (n "rapid-qoi") (v "0.0.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)))) (h "1qikz7395p7zw72j1m5aiyaf93a27iglq7j0jk7ymq3vqfxda7w5") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-rapid-qoi-0.1.0 (c (n "rapid-qoi") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)))) (h "0ivngzjbdk2svamy2n546mdqmjp7206q911salv7imvbjnwgxi6k") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-rapid-qoi-0.2.0 (c (n "rapid-qoi") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)))) (h "0d1pqg315f88y51zq9g1bwq16cgjbyj6y4q5hmrvr0aqfxsa636b") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.3.0 (c (n "rapid-qoi") (v "0.3.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "1afjv2jkpjjnvh91ibnxjfvf4lwlfn1k1hwwmcy3bfp8p9apyr3k") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.3.1 (c (n "rapid-qoi") (v "0.3.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "0kb868jqhj6d9crm202kzx0v9mx1v5k4wpsg5ka0kzqksdv83p3l") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.4.0 (c (n "rapid-qoi") (v "0.4.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "0qqxcyibv3n3b7i4mwjrg1d4qa1zwjjpnnrhjsq1h06zpg441rkl") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.4.1 (c (n "rapid-qoi") (v "0.4.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "0bj3q13phjxdi0dj6b9zk0c12l0bhc5lvvi4jbh8sxik8v1zv5w9") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.4.2 (c (n "rapid-qoi") (v "0.4.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "1knmzv0cl8c1j1cw39p766a6c8z55wvxg3m0ndhq2yd7gr3d7qil") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.4.3 (c (n "rapid-qoi") (v "0.4.3") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "qoi") (r "^0.3") (d #t) (k 2)) (d (n "qoi_rs") (r "^0.1") (d #t) (k 2)))) (h "00c7h08lfgaf01fa41g6ki4l46v910v5igwaih1224q183lr49rr") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.5.0 (c (n "rapid-qoi") (v "0.5.0") (h "0wy5dk6nyr7ynq56n2nimyi5wwbhmk7bnc4mws2wcir8zvysaspd") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-rapid-qoi-0.5.1 (c (n "rapid-qoi") (v "0.5.1") (h "0c5n9wbs8pvckrdidi069imi5kb8d9phy77nw3pgr2iaal2vbyng") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-rapid-qoi-0.5.2 (c (n "rapid-qoi") (v "0.5.2") (h "0awsy3avg03zp12z1cs37ylj6zj75ybrd94dba206iv500had7ab") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-rapid-qoi-0.5.3 (c (n "rapid-qoi") (v "0.5.3") (h "1h7qqcbv7ryyabwgyx2x0v203rz0qhjf6pdi85x3xxahmjcw8rfx") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-rapid-qoi-0.5.4 (c (n "rapid-qoi") (v "0.5.4") (h "0yl4v8zj1r5x51cg4l1blfh9f8np0jsrvfy5xl0xsdg7hzkk71hn") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-rapid-qoi-0.5.5 (c (n "rapid-qoi") (v "0.5.5") (h "0wxa3icgvspmp1v9kp6cf8bvzsy530gq0z1rj5p2ma2pv81vc2pm") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.6.0 (c (n "rapid-qoi") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1.0") (f (quote ("min_const_generics"))) (d #t) (k 0)))) (h "0kmax5nipwbiw7zy69kb51lvbdz0i9g2k96j7hr4w3h8whnlq2x9") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc"))))))

(define-public crate-rapid-qoi-0.6.1 (c (n "rapid-qoi") (v "0.6.1") (d (list (d (n "bytemuck") (r "^1.0") (f (quote ("min_const_generics"))) (d #t) (k 0)))) (h "1b2pl80x685lcfaj1ffjsdrgj50iikh01yi72nz4f5f7mfilypcn") (f (quote (("std" "alloc") ("io" "std") ("default" "std") ("alloc"))))))

