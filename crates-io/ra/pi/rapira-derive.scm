(define-module (crates-io ra pi rapira-derive) #:use-module (crates-io))

(define-public crate-rapira-derive-0.1.0 (c (n "rapira-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qpcvp8hhh852vvcv7mfdasfa8qww78dnarcpn8rrczwxsv9s6g1")))

(define-public crate-rapira-derive-0.2.0 (c (n "rapira-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0h1a05g7y5a72jj2kaaj5g7nkbnmp1shd3y28iy8kzyyfilgjdb2")))

(define-public crate-rapira-derive-0.2.1 (c (n "rapira-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "072xkjv0394d4jvdx88728248rsv8m1v63yhs7jb4v3iibmbw27c")))

(define-public crate-rapira-derive-0.2.2 (c (n "rapira-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lcbm5xll48saznh4p6z6czcqbjjzr24fxwfjwha8ld7hb4wxwx7")))

(define-public crate-rapira-derive-0.2.3 (c (n "rapira-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j95bl5n45z0cfgvffl4ws2n312d3n0djgzihasi7wlhi9fkw3ld")))

(define-public crate-rapira-derive-0.4.0 (c (n "rapira-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0spcgriiqjax3cwhkq59dj9fn64qvzs9dg741flvy87pwmpypp3b")))

(define-public crate-rapira-derive-0.5.0 (c (n "rapira-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1096n38nl30c2cpp9j57s57m44ll5hd38ly1qffz9gfy9fr1zbzb")))

(define-public crate-rapira-derive-0.6.0 (c (n "rapira-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0pmac40zzrmjyl6vg3vz0wdk88rmn3pb9gbgxcqaj6s571j29ir9")))

(define-public crate-rapira-derive-0.7.0 (c (n "rapira-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qmalak496qgfli9mxblli3vnql5sibifxgixkw1l03fxkc923gj")))

