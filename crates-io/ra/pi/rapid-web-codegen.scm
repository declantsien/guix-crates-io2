(define-module (crates-io ra pi rapid-web-codegen) #:use-module (crates-io))

(define-public crate-rapid-web-codegen-0.1.0 (c (n "rapid-web-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "0fygbskxkbwmq5zv1ggzvq87xmahz0mmsp6nlg674ssscgvqilgd")))

(define-public crate-rapid-web-codegen-0.0.1 (c (n "rapid-web-codegen") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1w9lzc759ms2mna774216mdznj1z0cvr9h9z68n8kka0k94w49q3")))

(define-public crate-rapid-web-codegen-0.0.2 (c (n "rapid-web-codegen") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1fa4cqrc6aij68rv0dk4jyv7j7zwdlk0n1lqininmmfdkalcz2wf")))

(define-public crate-rapid-web-codegen-0.0.3 (c (n "rapid-web-codegen") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1fvn3fi9qwldf50mcx4xdxzpwzyfami3ib7hnkq2zzn94glxz1rh")))

(define-public crate-rapid-web-codegen-0.0.4 (c (n "rapid-web-codegen") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "062731jgvj35ardvmvk7am8bfajg3gqwv0qf3x91lnxyha63gyvd")))

(define-public crate-rapid-web-codegen-0.0.5 (c (n "rapid-web-codegen") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1qihv7kngznwbmr5hxhiwbkcs5250qb7kkqwqs2mxy9r2jjgl9cc")))

(define-public crate-rapid-web-codegen-0.0.6 (c (n "rapid-web-codegen") (v "0.0.6") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "17xrgwx29cp2awcmvac9w7ibiwdzkbg3k9d3f18jsl5kridlns2z")))

(define-public crate-rapid-web-codegen-0.0.7 (c (n "rapid-web-codegen") (v "0.0.7") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "0c6n9awf5dp9793dv21zz005ls2hmchgcp0nf68y3kqxg93izh7x")))

(define-public crate-rapid-web-codegen-0.0.8 (c (n "rapid-web-codegen") (v "0.0.8") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1px1r1mlrzij7drbpgkkwqxrg0yg2rsj69i8nqbi5d3sryvj6zgc")))

(define-public crate-rapid-web-codegen-0.0.9 (c (n "rapid-web-codegen") (v "0.0.9") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.3") (d #t) (k 0)))) (h "12gbc80fh05gqshmpsll1jfm6qwnrvfpz4cncrk5l88ciw71x24c")))

(define-public crate-rapid-web-codegen-0.1.1 (c (n "rapid-web-codegen") (v "0.1.1") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1x9gljm7kpj3k6mqvxsmr573gssdz47j6lyhd7lfrpsi84sxm1mf")))

(define-public crate-rapid-web-codegen-0.1.2 (c (n "rapid-web-codegen") (v "0.1.2") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1b6iabpm1c71dj68wg7dgaa476apfvbbsvzsxs5y5x7ilfvgx8g2")))

(define-public crate-rapid-web-codegen-0.1.3 (c (n "rapid-web-codegen") (v "0.1.3") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "0njlsascgv117svdalr4slwkmgpq12in0r2nl216blnb4rrl114n")))

(define-public crate-rapid-web-codegen-0.1.4 (c (n "rapid-web-codegen") (v "0.1.4") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "07234jgdwi1gwgirw3dq2i2y8wzf7gcaxgr8qq7hij062nac9999")))

(define-public crate-rapid-web-codegen-0.1.5 (c (n "rapid-web-codegen") (v "0.1.5") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1fp5mk7n6sbbygjk0shfnbpcrdrj43y5l0my82a503ssw6dykp37")))

(define-public crate-rapid-web-codegen-0.1.6 (c (n "rapid-web-codegen") (v "0.1.6") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rapid-cli") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0ix5hky9lr873yi633zgxw1xsbwa57b5bkz3dgdxjxqg2140dimb")))

(define-public crate-rapid-web-codegen-0.1.7 (c (n "rapid-web-codegen") (v "0.1.7") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rapid-cli") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0h06fnkgx2a0j8svhvryx89sp5viy0y2arlh7amq65kgsw19g8rj")))

(define-public crate-rapid-web-codegen-0.1.8 (c (n "rapid-web-codegen") (v "0.1.8") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rapid-cli") (r "^0.2.7") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "0dmvvgv5sai43g84v9a9hmcfvrg6yygklshysbgrb8yq0d4689jf")))

(define-public crate-rapid-web-codegen-0.1.9 (c (n "rapid-web-codegen") (v "0.1.9") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rapid-cli") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "07ljv8n37yk5anvpcj97fjivvr2ik3hqf5j7y245jhdj03h53385")))

(define-public crate-rapid-web-codegen-0.2.0 (c (n "rapid-web-codegen") (v "0.2.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "rapid-cli") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "0wkamqwzcadn18s71sh8mryvclxgjr2lmh0pi1c8484k8gw3p8xx")))

(define-public crate-rapid-web-codegen-0.2.1 (c (n "rapid-web-codegen") (v "0.2.1") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pvm9nxxwfzk0r65crlwh868x1mvinc79h71zsk73f3b2cpw2bd0")))

(define-public crate-rapid-web-codegen-0.2.2 (c (n "rapid-web-codegen") (v "0.2.2") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "176yjabxa0pn34qxk8cxn4l2nd5jzps1pw68anmma3hp54vgq8dn")))

(define-public crate-rapid-web-codegen-0.2.3 (c (n "rapid-web-codegen") (v "0.2.3") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kljs4nkvz1353npg4ndyzsfzzkzxpdiqk42hx3lgfiw138cnl2y")))

(define-public crate-rapid-web-codegen-0.2.4 (c (n "rapid-web-codegen") (v "0.2.4") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wkp25h6cncas2wwwm3slzdahf8nccpf9ld4d3pvcc0wcg7m1whr")))

(define-public crate-rapid-web-codegen-0.2.5 (c (n "rapid-web-codegen") (v "0.2.5") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gbk3y0y0chwiqs9dad9h3v9pl6a6a1snwnckcd80jzyc8xa4ghy")))

(define-public crate-rapid-web-codegen-0.2.6 (c (n "rapid-web-codegen") (v "0.2.6") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dsn1day98h84al04ndnmc4v0rwl57as07rsg0fdrybd1n30rj33")))

(define-public crate-rapid-web-codegen-0.2.7 (c (n "rapid-web-codegen") (v "0.2.7") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "005g1vbfz9d3z4a7lr2yi5mc1fnimh3wnd6346282dldl8if9kby")))

(define-public crate-rapid-web-codegen-0.2.8 (c (n "rapid-web-codegen") (v "0.2.8") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04a4fcn8v9x0vfpnbfd8b4ka3y2kblcha12lzz0aw0fl1qmhp6z1")))

