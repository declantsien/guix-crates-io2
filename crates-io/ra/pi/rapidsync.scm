(define-module (crates-io ra pi rapidsync) #:use-module (crates-io))

(define-public crate-rapidsync-0.1.0-alpha1 (c (n "rapidsync") (v "0.1.0-alpha1") (h "0aazjx0vxqyll1q53xvkbi1x3jny0wqmfplw9qxh6sd5ds7pykb7") (y #t)))

(define-public crate-rapidsync-0.1.1-alpha1 (c (n "rapidsync") (v "0.1.1-alpha1") (h "00sgvlyfslx5jb97lnwjpilz3zg6yz2fnqha290mg4yp6gf3yp7b")))

(define-public crate-rapidsync-0.1.2-alpha1 (c (n "rapidsync") (v "0.1.2-alpha1") (h "07qr4cy599ympl1gq0gc4g6yvfm2b8hc8nwqsv24hjbms92h5yd6")))

(define-public crate-rapidsync-0.1.3-alpha1 (c (n "rapidsync") (v "0.1.3-alpha1") (h "1wb2grmjp0k3kh7pjchd86lgwvxx70vn0crfdg1xc1jbkzhr2h6m")))

(define-public crate-rapidsync-0.1.4-alpha1 (c (n "rapidsync") (v "0.1.4-alpha1") (h "16s0mxsv963arm26a6xvaj1vphrj1xd5167wazwn4jq84cyk5169")))

(define-public crate-rapidsync-0.1.5-alpha1 (c (n "rapidsync") (v "0.1.5-alpha1") (h "0wqp5jzwwk54wikn369nckg5c4psgkdl44icb0d6zqj9yvwvz2n9")))

(define-public crate-rapidsync-0.1.6-alpha1 (c (n "rapidsync") (v "0.1.6-alpha1") (h "0gd02ayzp6vlzplqqkdynp1kan737hnjb4j2rimlxq3a4mk1pgcv")))

(define-public crate-rapidsync-0.1.7-alpha1 (c (n "rapidsync") (v "0.1.7-alpha1") (h "1pdc7yv6kj180bq4zf5by763x96afn9fgvjz896x9yspk8ln6i67")))

(define-public crate-rapidsync-0.1.8-alpha1 (c (n "rapidsync") (v "0.1.8-alpha1") (h "0vrq4rv6gk7q8z77jc83v1ggd0cym8a6037qfi6gljysy1kwc0la")))

(define-public crate-rapidsync-0.1.9-alpha1 (c (n "rapidsync") (v "0.1.9-alpha1") (h "0a7v1nyhvxq7w21dvgljd8hl3v0c4g8bvzvay1klpmd6y8nqphyk")))

