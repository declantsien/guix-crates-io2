(define-module (crates-io ra pi rapiy-art) #:use-module (crates-io))

(define-public crate-rapiy-art-0.1.0 (c (n "rapiy-art") (v "0.1.0") (h "1x46956815sxp3rvmmb9ny7kbch4zfd68bvzwz8h4krijqd7r8bv")))

(define-public crate-rapiy-art-0.1.1 (c (n "rapiy-art") (v "0.1.1") (h "1dkmkyxidnsnyp77rn8czvyfkrpgrpwda5v4af5hml7km55qz725")))

