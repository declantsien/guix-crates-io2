(define-module (crates-io ra mu ramu_rs) #:use-module (crates-io))

(define-public crate-ramu_rs-0.1.0 (c (n "ramu_rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.74") (f (quote ("c++20"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0p42gsklqb74wvlm336p65smxs2z5m4qdv445rgh5n33i2pv2brq")))

(define-public crate-ramu_rs-0.1.1 (c (n "ramu_rs") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.74") (f (quote ("c++20"))) (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "00apwq2fvhp7ipzw4i0gk901c98z5k8smvbq9rdbx636iiil76qj")))

