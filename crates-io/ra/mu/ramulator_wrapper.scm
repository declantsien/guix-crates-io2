(define-module (crates-io ra mu ramulator_wrapper) #:use-module (crates-io))

(define-public crate-ramulator_wrapper-0.0.1 (c (n "ramulator_wrapper") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12nw12s2q2z9qhc824mk5s9fwmhkwl8pb6dp33376qyna9p33vs8")))

(define-public crate-ramulator_wrapper-0.0.2 (c (n "ramulator_wrapper") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12r2srdl9a4fpzvghr4ww2kwnsgq4l3f8vpfm2my384iw8051523")))

(define-public crate-ramulator_wrapper-0.0.3 (c (n "ramulator_wrapper") (v "0.0.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18c5a11gs4bl3j7kqx2ygny9sfl0sj1vycmz1pr0hx32a9k1vcps")))

(define-public crate-ramulator_wrapper-0.1.0 (c (n "ramulator_wrapper") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r46lrmv8p5rkz6j1gcyl3lkbkas0giv3ra4whn9prpj58r5cfv5")))

(define-public crate-ramulator_wrapper-0.1.1 (c (n "ramulator_wrapper") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00y9jgq75cqvzpr1fdymw8ra1j4sh5s9my49fy1db6w23mg24ljh")))

(define-public crate-ramulator_wrapper-0.1.2 (c (n "ramulator_wrapper") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04zgf2yswhk3cbblx9k6cgj8997ygir2vw56zmyl3aink1vn4k33")))

(define-public crate-ramulator_wrapper-0.2.0 (c (n "ramulator_wrapper") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ynxvwg8s4lbk7zg8vlcxivac2n575hghc81gwywjccwr5jh0sbf")))

(define-public crate-ramulator_wrapper-0.2.1 (c (n "ramulator_wrapper") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ss6kl57l9859sq13dbvqh7xay5d1lxqsm2j5iib0ylawdaz4b8q")))

(define-public crate-ramulator_wrapper-0.2.2 (c (n "ramulator_wrapper") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cpgaykc5iy76dw8qw4qpsyzg3gcry9pkzgkgi4s9vpch552n7fm")))

(define-public crate-ramulator_wrapper-0.2.3 (c (n "ramulator_wrapper") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mk8q1wwrn8ssc5fj2k68127nxzw50p6i8z1jrjlyj5knspi6dj7")))

(define-public crate-ramulator_wrapper-0.2.4 (c (n "ramulator_wrapper") (v "0.2.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "09rqvb1r7pmghjs1whxwf7jbm3pxjy5v60klylimlj5p9mgqf68g")))

(define-public crate-ramulator_wrapper-0.2.5 (c (n "ramulator_wrapper") (v "0.2.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b50pa30jz2202qbcbm2na2vcf0rvk8dpxrydsad13mir067wxdr")))

(define-public crate-ramulator_wrapper-0.2.6 (c (n "ramulator_wrapper") (v "0.2.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)))) (h "12gih84fwdxks0mjjrr2c22j7j4akix007vji4b6vys4ig84w64h")))

