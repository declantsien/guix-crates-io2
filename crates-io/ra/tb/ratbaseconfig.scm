(define-module (crates-io ra tb ratbaseconfig) #:use-module (crates-io))

(define-public crate-ratbaseconfig-0.1.0 (c (n "ratbaseconfig") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "03rgga4zwlqiiiyh4h7al9pjhkgn3577gidbz09ki7bw71sw8j54")))

(define-public crate-ratbaseconfig-0.1.1 (c (n "ratbaseconfig") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "0k6nih134xrw7jshmbpsb6fy5q0z5byxb59svi7gas2rrxqrn53j")))

