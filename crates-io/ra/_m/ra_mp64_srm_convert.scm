(define-module (crates-io ra _m ra_mp64_srm_convert) #:use-module (crates-io))

(define-public crate-ra_mp64_srm_convert-0.9.0 (c (n "ra_mp64_srm_convert") (v "0.9.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "01jby4fxksk4a79qg6x64l0d1if8l2m7vhysxq159ywm68gvxcfm") (y #t)))

(define-public crate-ra_mp64_srm_convert-0.9.1 (c (n "ra_mp64_srm_convert") (v "0.9.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1by7931n0bkzqvc3xjax9i8icr8j2dhgc94pmsgs2k65hn9lssax") (y #t)))

(define-public crate-ra_mp64_srm_convert-0.9.2 (c (n "ra_mp64_srm_convert") (v "0.9.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bn8l82ahjxadpc919v86r176qlgsfs75y22qabll803wvlhh7av") (y #t)))

(define-public crate-ra_mp64_srm_convert-0.10.0 (c (n "ra_mp64_srm_convert") (v "0.10.0") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 2)))) (h "1lnha4rmq8nj3i6q4qvynjjirqim58i07gabbcn3sf686h99vz3x") (y #t)))

(define-public crate-ra_mp64_srm_convert-0.11.0 (c (n "ra_mp64_srm_convert") (v "0.11.0") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "ramp64-srm-convert-lib") (r "^0.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 2)))) (h "0ag4i2r554s5zq316km7113anlpwn3qvs7jdxr3zgrxzrqs9gy1x") (y #t)))

(define-public crate-ra_mp64_srm_convert-0.12.0 (c (n "ra_mp64_srm_convert") (v "0.12.0") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.10") (d #t) (k 2)) (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "ramp64-srm-convert-lib") (r "^0.2") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (f (quote ("deflate"))) (k 2)))) (h "0kpz10vknv4qgs11fzkaw0v96c0ar341c6c37dkgin34dz60acy4") (y #t)))

(define-public crate-ra_mp64_srm_convert-1.0.0 (c (n "ra_mp64_srm_convert") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "ramp64-srm-convert-lib") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "1l8i2hicziphvxafkpgrnw0plbyj9k4l3kdpb7m5fckl13gpz5m8")))

(define-public crate-ra_mp64_srm_convert-1.1.0 (c (n "ra_mp64_srm_convert") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "ramp64-srm-convert-lib") (r "^0.5") (d #t) (k 0)) (d (n "simplelog") (r "^0.12") (d #t) (k 0)))) (h "0z1fp18ahh42b6286qni11444a6wpz6lps74gyvkdc6hhdhn568q")))

