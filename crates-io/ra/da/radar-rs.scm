(define-module (crates-io ra da radar-rs) #:use-module (crates-io))

(define-public crate-radar-rs-0.0.1 (c (n "radar-rs") (v "0.0.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1183qy9xyzazhc5dbr20hqgy5f6jm2v34ck89ii5x2jbyz9cnmz9")))

(define-public crate-radar-rs-0.0.2 (c (n "radar-rs") (v "0.0.2") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zamy035ksgj37zbgmy425jgkm90n43p2sy20lypbb2lid7ksnzn")))

(define-public crate-radar-rs-0.1.0 (c (n "radar-rs") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wwsk1llybi2w9icnl23brlijpl6xg2b2vbwl0wca5yxs2a121zm")))

