(define-module (crates-io ra da radamsa) #:use-module (crates-io))

(define-public crate-radamsa-0.1.0 (c (n "radamsa") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 1)))) (h "1l23mbcbw4j7fa0z72qkc6wwbfyqyrrgwkc7158ld5wcn702h47s")))

(define-public crate-radamsa-0.1.1 (c (n "radamsa") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 1)))) (h "1nvd7d834pfm413imp1xciycc76xbvvlqygr2ylmi9z4wf970hkd")))

