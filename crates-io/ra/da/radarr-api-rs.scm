(define-module (crates-io ra da radarr-api-rs) #:use-module (crates-io))

(define-public crate-radarr-api-rs-3.0.0 (c (n "radarr-api-rs") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "09bdrg8y3skcnc2r3x6n0274agy16p6pihcicqwyd9gyj69832wq") (y #t)))

(define-public crate-radarr-api-rs-3.0.1 (c (n "radarr-api-rs") (v "3.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0skfc20jl60wbn9i1g6bwbcrii4i81c32lx8mpm55gzqsy9h7isi")))

