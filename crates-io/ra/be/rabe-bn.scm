(define-module (crates-io ra be rabe-bn) #:use-module (crates-io))

(define-public crate-rabe-bn-0.4.6 (c (n "rabe-bn") (v "0.4.6") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0ifh3dacvsk9gh04ck7j4111jk2fjmbghkjnrh7ya639khmb78il")))

(define-public crate-rabe-bn-0.4.7 (c (n "rabe-bn") (v "0.4.7") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde-hex") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "13hsfdma249z2cfpfiy6aw4adfhmrg1dq8j7xvgkgrqi5qa7rrpg")))

(define-public crate-rabe-bn-0.4.8 (c (n "rabe-bn") (v "0.4.8") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1is315lj1c80v0y5z0dv4plqggv78d3mgx0shw7dh40n9kd9pfgy")))

(define-public crate-rabe-bn-0.4.9 (c (n "rabe-bn") (v "0.4.9") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)))) (h "0aiw25zgqa9px3y7fgg7vnflnlp1ajvwlz7bmqwl3gl1a6jpm6zb")))

(define-public crate-rabe-bn-0.4.10 (c (n "rabe-bn") (v "0.4.10") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)))) (h "0ix34dhbrmk0z2rx2s87cqywazlp44yh18sp4hf20vss27rg4msl")))

(define-public crate-rabe-bn-0.4.11 (c (n "rabe-bn") (v "0.4.11") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "04rwkvirkcj1z9aax60j64gjhpzvk2alxwasdhcv7v0v96cs4hhs")))

(define-public crate-rabe-bn-0.4.12 (c (n "rabe-bn") (v "0.4.12") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "183r5bmy98xwynpmcbvlf7l1vysb6iz04nn8pjp328qqaq0jbi9j")))

(define-public crate-rabe-bn-0.4.13 (c (n "rabe-bn") (v "0.4.13") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "0qbpkckc414qdlh7vnznj7bkg7m96q2kw5y56bvgq7xmc67xni4l")))

(define-public crate-rabe-bn-0.4.14 (c (n "rabe-bn") (v "0.4.14") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "boolvec") (r "^0.2.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "0rzzpajl3pskkxr44kb4z1as6d8wh9db43j42wvm65vswc1pnc1q")))

(define-public crate-rabe-bn-0.4.15 (c (n "rabe-bn") (v "0.4.15") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "0p3v0y7a58j934f8ix37yxc2bznf2fkvl2wibk0d3lsqr9d34bdr")))

(define-public crate-rabe-bn-0.4.16 (c (n "rabe-bn") (v "0.4.16") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "1a3yy4wpadl7a3wmvjsbrdasv64ya191i35g8l1d3f1ybsl20x6w")))

(define-public crate-rabe-bn-0.4.17 (c (n "rabe-bn") (v "0.4.17") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)))) (h "0sl926l4mygg85yvpzcdvyhcjc1k6mjdk2ssxxab7k2lnsq13c46")))

(define-public crate-rabe-bn-0.4.18 (c (n "rabe-bn") (v "0.4.18") (d (list (d (n "bincode") (r "^1.0.0") (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)))) (h "0yz3xr02574qpf07swa9vmvahdq8z08c1nvcry5dqxr9zw4m2il7")))

(define-public crate-rabe-bn-0.4.20 (c (n "rabe-bn") (v "0.4.20") (d (list (d (n "borsh") (r "^0.9.3") (o #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a1mgfgrxqvszba8qhibpy8zc16q3qxhimn6savygs0mdjmba7j6") (f (quote (("serde" "serde/std") ("default" "serde") ("borsh" "borsh/std"))))))

(define-public crate-rabe-bn-0.4.21 (c (n "rabe-bn") (v "0.4.21") (d (list (d (n "borsh") (r "^1.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "091a9r1rls6z5whhi0m8qppf6wcr312rqw3pkvyznmh78xw1phzx") (f (quote (("serde" "serde/std") ("default" "serde") ("borsh" "borsh/std"))))))

(define-public crate-rabe-bn-0.4.22 (c (n "rabe-bn") (v "0.4.22") (d (list (d (n "borsh") (r "^1.5.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0f84js34hcp3jdskkvz87148wp3jp1phgdb0ac8ilmrljg6na42j") (f (quote (("serde" "serde/std") ("default" "serde") ("borsh" "borsh/std"))))))

(define-public crate-rabe-bn-0.4.23 (c (n "rabe-bn") (v "0.4.23") (d (list (d (n "borsh") (r "^1.5.0") (o #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "02630n5x06mrpyxvap0zs2p5xkwyi6qycq1nlv7k54w5pdv3qn44") (f (quote (("serde" "serde/derive") ("default" "serde") ("borsh" "borsh/derive"))))))

