(define-module (crates-io ra sl raslib) #:use-module (crates-io))

(define-public crate-raslib-0.1.1 (c (n "raslib") (v "0.1.1") (h "1zmavd6al1lqab47ggdg6zclcn0qy3p99mkacaj5pn1qmif6gz32")))

(define-public crate-raslib-0.1.2 (c (n "raslib") (v "0.1.2") (h "1gnfmwg6mnx3iys72xrg2xbqw86r5g0kx5lyy34grs9x5iymcz8c")))

