(define-module (crates-io ra #{4w}# ra4w1) #:use-module (crates-io))

(define-public crate-ra4w1-0.2.0 (c (n "ra4w1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1p9n9rrz3mirfr5pjmjkqznhb3ac69fhif48icppi5j02wbbs1g4") (f (quote (("rt" "cortex-m-rt/device"))))))

