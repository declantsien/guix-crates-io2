(define-module (crates-io ra wp rawproxy) #:use-module (crates-io))

(define-public crate-rawproxy-0.1.0 (c (n "rawproxy") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-uninet") (r "^0.1.1") (d #t) (k 0)))) (h "0yj85qlyy0i78y049332h3634lyfnqvll6h428l2af0rj9mw935z")))

(define-public crate-rawproxy-0.2.0 (c (n "rawproxy") (v "0.2.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-uninet") (r "^0.1.1") (d #t) (k 0)))) (h "0zhp0zh0n4w4cl7glfq0ha9ccpw8pv6n07cdhpqnlidcq6m65zrc")))

(define-public crate-rawproxy-0.3.0 (c (n "rawproxy") (v "0.3.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-uninet") (r "^0.1.1") (d #t) (k 0)))) (h "1kccsbx2y37jv044q3zw4rrwp1l4ixhnl6vvkk5w0858g8aa03fc")))

