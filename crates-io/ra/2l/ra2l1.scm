(define-module (crates-io ra #{2l}# ra2l1) #:use-module (crates-io))

(define-public crate-ra2l1-0.2.0 (c (n "ra2l1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1h4jm1qw9mn0p9whlpha5zvy52vp2a2xhjnx06flkssy4w92azyq") (f (quote (("rt" "cortex-m-rt/device"))))))

