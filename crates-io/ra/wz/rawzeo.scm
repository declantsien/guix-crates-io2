(define-module (crates-io ra wz rawzeo) #:use-module (crates-io))

(define-public crate-rawzeo-0.0.1 (c (n "rawzeo") (v "0.0.1") (d (list (d (n "circular-buffer") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (o #t) (d #t) (k 0)))) (h "103i11vxjb81amwwag0jpd8nqmjhf1s2z1cppgmac83k2j9qy9jh") (f (quote (("default" "bin") ("bin" "circular-buffer" "serialport")))) (r "1.66.1")))

