(define-module (crates-io ra #{4m}# ra4m3) #:use-module (crates-io))

(define-public crate-ra4m3-0.2.0 (c (n "ra4m3") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0n8c2m15sidfh0gfhf2wwcmhypgaqmvgjvd0ad01kq8zqlr92acn") (f (quote (("rt" "cortex-m-rt/device"))))))

