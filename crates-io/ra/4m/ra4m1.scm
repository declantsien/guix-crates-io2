(define-module (crates-io ra #{4m}# ra4m1) #:use-module (crates-io))

(define-public crate-ra4m1-0.2.0 (c (n "ra4m1") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.13") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "^0.3") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0mdc4d0z0ywqmspxv0y5hz36llkk61kxnyp97pk0w0g2gq46pn4s") (f (quote (("rt" "cortex-m-rt/device"))))))

