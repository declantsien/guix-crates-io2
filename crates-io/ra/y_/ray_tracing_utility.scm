(define-module (crates-io ra y_ ray_tracing_utility) #:use-module (crates-io))

(define-public crate-ray_tracing_utility-0.1.0 (c (n "ray_tracing_utility") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ray_tracing_core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rm60p1sppvzr6qskgfrs534vzg6nvv2l297x8p49l5ndcxyfv64")))

(define-public crate-ray_tracing_utility-0.1.1 (c (n "ray_tracing_utility") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "ray_tracing_core") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xjnyp4mw6ss1iw8nab9mvp2d5wvy6a6lgr33j7liib70q217gzl")))

