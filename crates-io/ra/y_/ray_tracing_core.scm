(define-module (crates-io ra y_ ray_tracing_core) #:use-module (crates-io))

(define-public crate-ray_tracing_core-0.1.0 (c (n "ray_tracing_core") (v "0.1.0") (d (list (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "18xzb6gyn1xizz46lz6wngrkdm3dzbzw3dlycgfr00mgp6d7xbkj")))

(define-public crate-ray_tracing_core-0.1.1 (c (n "ray_tracing_core") (v "0.1.1") (d (list (d (n "glm") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1v4v58mqq82ncqp42i8v50apwv0f48akp72b927qhry0wb70smi8")))

