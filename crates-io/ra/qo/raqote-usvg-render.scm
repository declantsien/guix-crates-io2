(define-module (crates-io ra qo raqote-usvg-render) #:use-module (crates-io))

(define-public crate-raqote-usvg-render-0.11.0 (c (n "raqote-usvg-render") (v "0.11.0") (d (list (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.15") (k 0)) (d (n "raqote") (r "^0.8") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.11") (k 0)))) (h "0md4p7171gc0qf8gbcm61840irkqp2811h8a9a7jyygjl6hn3d3y") (f (quote (("text" "usvg/text") ("default" "text"))))))

(define-public crate-raqote-usvg-render-0.12.0 (c (n "raqote-usvg-render") (v "0.12.0") (d (list (d (n "jpeg-decoder") (r "^0.1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pico-args") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.15") (k 0)) (d (n "raqote") (r "^0.8") (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "svgfilters") (r "^0.1") (d #t) (k 0)) (d (n "usvg") (r "^0.11") (k 0)))) (h "1arv22gxalxb2s8bgyibgz1c4s7lyr4hyn2jgq3fy5rfh8whr6l4") (f (quote (("text" "usvg/text") ("default" "text"))))))

