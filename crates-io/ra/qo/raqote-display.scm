(define-module (crates-io ra qo raqote-display) #:use-module (crates-io))

(define-public crate-raqote-display-0.1.0 (c (n "raqote-display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "raqote") (r "^0.8") (d #t) (k 0)))) (h "1g3hmkvsznqmvy140yw3qcgwrr30zp1fqc8d91v4j7gd9kfsy5a5")))

(define-public crate-raqote-display-0.1.1 (c (n "raqote-display") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "raqote") (r "^0.8") (k 0)))) (h "1c1vfmf5p2q2a2vdqk37kh0wq321x29qp1s6k7bgwkywh96d233l")))

(define-public crate-raqote-display-0.1.2 (c (n "raqote-display") (v "0.1.2") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "raqote") (r "^0.8") (k 0)))) (h "0zml9zvgvysxkhfiwm0nl7jn5j3wa57arnhsr9qw7x62nshp83d9")))

