(define-module (crates-io ra ll ralloc) #:use-module (crates-io))

(define-public crate-ralloc-1.0.0 (c (n "ralloc") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.85") (o #t) (d #t) (k 0)) (d (n "ralloc_shim") (r "^0.1") (d #t) (k 0)) (d (n "unborrow") (r "^0.3.0") (d #t) (k 0)))) (h "1p9fbfrhxs5zdd2jkws0kx1w0z81lk1c02b9p1f397prb1x7zj0h") (f (quote (("write") ("unsafe_no_mutex_lock") ("tls") ("testing" "log" "debugger") ("security") ("no_log_lock" "log") ("log" "write" "alloc_id") ("default" "allocator" "clippy" "tls") ("debugger") ("allocator") ("alloc_id"))))))

