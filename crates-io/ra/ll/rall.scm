(define-module (crates-io ra ll rall) #:use-module (crates-io))

(define-public crate-rall-0.1.0 (c (n "rall") (v "0.1.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "07xb97nn6iz1l5591qy3y36pf3gmdcxgfisvypkmc8z91xk50ixl") (f (quote (("doc-images"))))))

(define-public crate-rall-0.2.0 (c (n "rall") (v "0.2.0") (d (list (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "0rhp6qw3kmljfbjkw557jhf3kvbkcpc1q3xgwjsb12vy0s7j9l04") (f (quote (("doc-images"))))))

(define-public crate-rall-0.3.0 (c (n "rall") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)))) (h "1flkx20f9dbwwavq4vz8n0hpb6z11570sy867cm330j7s2ymgc4k") (f (quote (("doc-images"))))))

