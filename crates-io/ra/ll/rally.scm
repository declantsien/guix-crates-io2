(define-module (crates-io ra ll rally) #:use-module (crates-io))

(define-public crate-rally-0.1.0 (c (n "rally") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1l8zalc421zfcrql4085i7j6g845pfycs18c7hbgfb7pwrbpian7")))

(define-public crate-rally-0.1.1 (c (n "rally") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)))) (h "0iv9xld62jdmmchbhqnsz87flqw9j0vx9l59d9hpfx53ap25gj90")))

