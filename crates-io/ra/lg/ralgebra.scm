(define-module (crates-io ra lg ralgebra) #:use-module (crates-io))

(define-public crate-ralgebra-0.1.0 (c (n "ralgebra") (v "0.1.0") (h "0faqk4a2l8v8m4h51r2ac3ar2yh43lr0k12mbylf4qv149bhg380")))

(define-public crate-ralgebra-0.1.1 (c (n "ralgebra") (v "0.1.1") (h "14ixr0l6raw86rbgp2scj9lxhrnk3wl1s91ysdqqzaimfnjawgp2")))

(define-public crate-ralgebra-0.1.2 (c (n "ralgebra") (v "0.1.2") (h "0lnl0lypnhl01hp0va6wbaqkiik48fp2rvwhjyaniknhs5c3lzr6")))

(define-public crate-ralgebra-0.1.3 (c (n "ralgebra") (v "0.1.3") (h "01xhvsfvfqbddrahipdgagqgbr9rfwv9s070jb9dmf68xqd51sm4") (y #t)))

(define-public crate-ralgebra-0.1.4 (c (n "ralgebra") (v "0.1.4") (h "1g5c2clr7m78iwnfv9flr046jqzs2a7rbx5ksx34n70ijj8hp9qh")))

(define-public crate-ralgebra-0.2.0 (c (n "ralgebra") (v "0.2.0") (h "04dh8fqc2zdmggiwwa1f4dngzxma9ymmkdb3vjgfivw7hmhfngv7")))

