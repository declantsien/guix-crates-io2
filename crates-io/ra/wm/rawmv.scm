(define-module (crates-io ra wm rawmv) #:use-module (crates-io))

(define-public crate-rawmv-0.1.0 (c (n "rawmv") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.106") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1vk39s5h7b4pbgm2g4sqv3v47szi3lybvp2n3d8sf3yfg488hrkv")))

(define-public crate-rawmv-0.2.0 (c (n "rawmv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "libc") (r "^0.2.106") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (f (quote ("combined-flags"))) (k 0)))) (h "1x6czh8348gydz2ap7n9srnkyvb083l879bzlvmsqdgp139c6zsc")))

(define-public crate-rawmv-1.0.0 (c (n "rawmv") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (f (quote ("combined-flags"))) (k 0)) (d (n "rustix") (r "^0.37.19") (f (quote ("fs" "std"))) (k 0)))) (h "1nr6v3r2jl6f2lbyvbc2dmmg0lkmzg4arrkrs04w8lvbq70s0nvm")))

(define-public crate-rawmv-1.0.1 (c (n "rawmv") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (f (quote ("combined-flags"))) (k 0)) (d (n "rustix") (r "^0.37.19") (f (quote ("fs" "std"))) (k 0)))) (h "1pa07npqx135vjrmj10bn3rc676dbhj1kq84fkylpbxv8g400hj8")))

(define-public crate-rawmv-1.0.2 (c (n "rawmv") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "pico-args") (r "^0.5") (f (quote ("combined-flags"))) (k 0)) (d (n "rustix") (r "^0.38") (f (quote ("fs" "std"))) (k 0)))) (h "1w0r4fnygdjayl1ss1vmyj6lq517b70kyjay00mv4hi09ajnd4fd")))

