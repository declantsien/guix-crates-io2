(define-module (crates-io ra ek raekna-parser) #:use-module (crates-io))

(define-public crate-raekna-parser-0.1.0 (c (n "raekna-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "raekna-common") (r "^0.1.0") (d #t) (k 0)))) (h "1kjdayn0lq49rnmw56lw0yl7nsymh66q2qgx3blwkagr8s1439yj")))

(define-public crate-raekna-parser-0.1.1 (c (n "raekna-parser") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "raekna-common") (r "^0.1") (d #t) (k 0)))) (h "1y4f0g4dxj0jrjy8572dxhgycsgsc00wajsi87nikv750bxrshpy")))

(define-public crate-raekna-parser-0.2.0 (c (n "raekna-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "099nw33v2hx7daaa82vyd8zbnr2v3g37kl9mdaixx8j9awhd4jl0")))

(define-public crate-raekna-parser-0.2.1 (c (n "raekna-parser") (v "0.2.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "0ywfa98650nq91hpf4lxd8pfa9br6fs5h3nc1lwyxgdycairlar6")))

