(define-module (crates-io ra ek raekna) #:use-module (crates-io))

(define-public crate-raekna-0.1.0 (c (n "raekna") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "raekna-common") (r "^0.1.0") (d #t) (k 0)) (d (n "raekna-compute") (r "^0.1.0") (d #t) (k 0)) (d (n "raekna-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "raekna-storage") (r "^0.1.0") (d #t) (k 0)) (d (n "raekna-ui") (r "^0.1.0") (d #t) (k 0)))) (h "0gf3fr4ywmmyd8cxbz0ny14alizn1fwdb75f1hchj2lpa6r0vwvw")))

(define-public crate-raekna-0.1.1 (c (n "raekna") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "raekna-common") (r "^0.1.1") (d #t) (k 0)) (d (n "raekna-compute") (r "^0.1.1") (d #t) (k 0)) (d (n "raekna-parser") (r "^0.1.1") (d #t) (k 0)) (d (n "raekna-storage") (r "^0.1.1") (d #t) (k 0)) (d (n "raekna-ui") (r "^0.1.1") (d #t) (k 0)))) (h "17h9db6n8v7079q2qxq536jyv0byqjwni3rmr299lfm2pxczkvnh")))

(define-public crate-raekna-0.2.0 (c (n "raekna") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)) (d (n "raekna-compute") (r "^0.2") (d #t) (k 0)) (d (n "raekna-parser") (r "^0.2") (d #t) (k 0)) (d (n "raekna-storage") (r "^0.2") (d #t) (k 0)) (d (n "raekna-ui") (r "^0.2") (d #t) (k 0)))) (h "00ijv4bkql4sigsb4b7794z1xsr9gggry1wk85954lsfw38kmapn")))

(define-public crate-raekna-0.2.1 (c (n "raekna") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)) (d (n "raekna-compute") (r "^0.2") (d #t) (k 0)) (d (n "raekna-parser") (r "^0.2") (d #t) (k 0)) (d (n "raekna-storage") (r "^0.2") (d #t) (k 0)) (d (n "raekna-ui") (r "^0.2") (d #t) (k 0)))) (h "0zpjnw0sndsc5kjsd87z114kdk8xaz89lh6n9mqq830dwb9afkf6")))

