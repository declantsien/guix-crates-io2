(define-module (crates-io ra ek raekna-storage) #:use-module (crates-io))

(define-public crate-raekna-storage-0.1.0 (c (n "raekna-storage") (v "0.1.0") (d (list (d (n "raekna-common") (r "^0.1.0") (d #t) (k 0)))) (h "1fih58zg891hq18xg3wkicmgvnvgi49c5fbarxk5k9yz027jqx0m")))

(define-public crate-raekna-storage-0.1.1 (c (n "raekna-storage") (v "0.1.1") (d (list (d (n "raekna-common") (r "^0.1") (d #t) (k 0)))) (h "0pj90akaqvjywnwzsbamj1jpcd3y7rwbbccrjz7c74fy2jvnnva6")))

(define-public crate-raekna-storage-0.2.0 (c (n "raekna-storage") (v "0.2.0") (d (list (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "0g40sgf4yik3a2lw71lv7wr8kd1iz2n7j5da71c7x9pwi27gph69")))

(define-public crate-raekna-storage-0.2.1 (c (n "raekna-storage") (v "0.2.1") (d (list (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "0wyppck32xv5y93v5ifd3hqvzr999iy3zbr3wp7ym9qy32s8549r")))

