(define-module (crates-io ra ek raekna-common) #:use-module (crates-io))

(define-public crate-raekna-common-0.1.0 (c (n "raekna-common") (v "0.1.0") (h "1yinxzcr29yk5g6xz6vxpnvgi8b6x6px5i0y01npwli49ydq6sh6")))

(define-public crate-raekna-common-0.1.1 (c (n "raekna-common") (v "0.1.1") (h "0xr2g7cb9scfq26qa0xk3achjv0fqjj8ljxwjc5ldxbilwgypkl9")))

(define-public crate-raekna-common-0.2.0 (c (n "raekna-common") (v "0.2.0") (h "1bap57kzmsn6qpwm45z9p8a9w1yxkzzlx6989lp5x6qagdjm2kj3")))

(define-public crate-raekna-common-0.2.1 (c (n "raekna-common") (v "0.2.1") (h "0n25id5qzmsivzy55wjfbvc2yw8xanl9l4sgr84kjg3v199g8w1v")))

