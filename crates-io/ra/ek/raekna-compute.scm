(define-module (crates-io ra ek raekna-compute) #:use-module (crates-io))

(define-public crate-raekna-compute-0.1.0 (c (n "raekna-compute") (v "0.1.0") (d (list (d (n "raekna-common") (r "^0.1.0") (d #t) (k 0)))) (h "0wm24bsss96a68yna3psygybygfsx9gx1gha2bjripl2pr2m94qb")))

(define-public crate-raekna-compute-0.1.1 (c (n "raekna-compute") (v "0.1.1") (d (list (d (n "raekna-common") (r "^0.1") (d #t) (k 0)))) (h "1sb65vfn9dlix6kxp70xy5pb2k5fvrnq8d3pry46ihnm0h8725ph")))

(define-public crate-raekna-compute-0.2.0 (c (n "raekna-compute") (v "0.2.0") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "04gz31mahk4bjc4v8c357ykz482jlkifj2krskn21gyr43n1l9x2")))

(define-public crate-raekna-compute-0.2.1 (c (n "raekna-compute") (v "0.2.1") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "raekna-common") (r "^0.2") (d #t) (k 0)))) (h "00a8nd9xwws5xkq134kc6wgmg121p5mkp3s8zplfr5nmnf6wnwgy")))

