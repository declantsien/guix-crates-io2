(define-module (crates-io ra i- rai-datasets) #:use-module (crates-io))

(define-public crate-rai-datasets-0.7.0 (c (n "rai-datasets") (v "0.7.0") (h "1987vqmpimv2cnrld243mija82y0ysvqi8n2bk1a79na9dnm8gg3")))

(define-public crate-rai-datasets-0.10.0 (c (n "rai-datasets") (v "0.10.0") (d (list (d (n "hf-hub") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg" "png"))) (k 0)) (d (n "parquet") (r "^50.0.0") (d #t) (k 0)) (d (n "rai") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "044krfwyxk3ghmrvm4l2wwv5lfr783ppxi82i1hm805y7bysyip7")))

(define-public crate-rai-datasets-0.11.0 (c (n "rai-datasets") (v "0.11.0") (d (list (d (n "hf-hub") (r "^0.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("jpeg" "png"))) (k 0)) (d (n "parquet") (r "^50.0.0") (d #t) (k 0)) (d (n "rai") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1hjdz1bccyz768p7vp69dlhciw1qy2fdazia5w7nqa5fkljn867l")))

