(define-module (crates-io ra i- rai-nn) #:use-module (crates-io))

(define-public crate-rai-nn-0.1.0 (c (n "rai-nn") (v "0.1.0") (h "1jzmvdz9i84bn6dfqb9dx8cvk18cq47cfa6ai1mzaqnaqd8b2gw5")))

(define-public crate-rai-nn-0.2.0 (c (n "rai-nn") (v "0.2.0") (h "13c1fnf6ybnazm14bwx9fz4284gy166s993as6kz1r9c6n4drxfb")))

(define-public crate-rai-nn-0.3.1 (c (n "rai-nn") (v "0.3.1") (d (list (d (n "rai-core") (r "^0.3.0") (d #t) (k 0)))) (h "02689858pjhxnbm2v4yzbwrrjnj7c6frk39acz69fhm1g66zr4ds")))

(define-public crate-rai-nn-0.4.0 (c (n "rai-nn") (v "0.4.0") (d (list (d (n "rai-core") (r "^0.4.0") (d #t) (k 0)))) (h "08aw2kilkhfjpijg6vji62kk89n15xjgm4bjgz5fbqigbwnvsiqc")))

(define-public crate-rai-nn-0.5.0 (c (n "rai-nn") (v "0.5.0") (d (list (d (n "rai-core") (r "^0.5.0") (d #t) (k 0)))) (h "0vdaia6fyx9ma5gjwd9s3nzbql4yxiy0p7kisbwg8w9n012a1xi9")))

(define-public crate-rai-nn-0.6.0 (c (n "rai-nn") (v "0.6.0") (d (list (d (n "rai-core") (r "^0.6.0") (d #t) (k 0)))) (h "113yysf1hk8zf0a3id99kf8ni6aqwckzwm79garw7mcajym10lws")))

(define-public crate-rai-nn-0.7.0 (c (n "rai-nn") (v "0.7.0") (d (list (d (n "rai-core") (r "^0.7.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.3.1") (d #t) (k 0)))) (h "089yddscr5rvzc2rvb9x8g66p6c5xvgimwzx2abdz0i60l9nvvw8")))

(define-public crate-rai-nn-0.8.0 (c (n "rai-nn") (v "0.8.0") (d (list (d (n "rai-core") (r "^0.8.0") (d #t) (k 0)) (d (n "rai-derive") (r "^0.8.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.4.1") (d #t) (k 0)))) (h "0xcz4lfzbnyfg5ya3b972g94wqfp3xdfb00mwsyqv05cd47kmzia") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-nn-0.9.0 (c (n "rai-nn") (v "0.9.0") (d (list (d (n "rai-core") (r "^0.9.0") (d #t) (k 0)) (d (n "rai-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0plikp8bjplzswb7v941cjs2bqm9rf1lqry6g45vl6vqz5sablmj") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-nn-0.10.0 (c (n "rai-nn") (v "0.10.0") (d (list (d (n "rai-core") (r "^0.10.0") (d #t) (k 0)) (d (n "rai-derive") (r "^0.10.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a5xnfnnawbmhgry9xdmslw8rbhll9zgfwkvbmnsfr91x1p7njvh") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-nn-0.11.0 (c (n "rai-nn") (v "0.11.0") (d (list (d (n "rai-core") (r "^0.11.0") (d #t) (k 0)) (d (n "rai-derive") (r "^0.11.0") (d #t) (k 0)) (d (n "safetensors") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xv66jqj3n3cbz5cmr97zmylbcb6g11nnmnalzy5ja16sjgsc4hl") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

