(define-module (crates-io ra i- rai-models) #:use-module (crates-io))

(define-public crate-rai-models-0.7.0 (c (n "rai-models") (v "0.7.0") (h "138igl0qifv3f1kzyjlkjbai1gjlwk0zgyi6b3rlbzm62c0lrg5p")))

(define-public crate-rai-models-0.8.0 (c (n "rai-models") (v "0.8.0") (d (list (d (n "rai") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "09k2sl4wc1zkwq8r2fkhgan523di73zy0hh7xf4p4nb9zvrnp3mv")))

(define-public crate-rai-models-0.9.0 (c (n "rai-models") (v "0.9.0") (d (list (d (n "rai") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "168jaapic4p1g5i426b05g7vx0f2i0wgvc6d7izi4ydrxj1n6ngd")))

(define-public crate-rai-models-0.10.0 (c (n "rai-models") (v "0.10.0") (d (list (d (n "rai") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "0843avs76mqvjk2hwg2n7qnpbl2icdmr2cgibkal3q694jmjmb2i")))

(define-public crate-rai-models-0.11.0 (c (n "rai-models") (v "0.11.0") (d (list (d (n "rai") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.0") (f (quote ("onig"))) (d #t) (k 0)))) (h "0bw1jhji09k76q8f9xwifpqvs761x4kpkp9wa2c5yzlkxhcydmbj")))

