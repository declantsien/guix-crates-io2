(define-module (crates-io ra i- rai-opt) #:use-module (crates-io))

(define-public crate-rai-opt-0.4.0 (c (n "rai-opt") (v "0.4.0") (d (list (d (n "rai-core") (r "^0.4.0") (d #t) (k 0)))) (h "0pggw9k82kaz5z7ip10an6fqi143yj8r9l3lw2g4g168xyjl7ir1")))

(define-public crate-rai-opt-0.5.0 (c (n "rai-opt") (v "0.5.0") (d (list (d (n "rai-core") (r "^0.5.0") (d #t) (k 0)))) (h "1dn8623qbrv1w792z4b0vik8n21hgx4mf8gm282fdafmvnb23s6z")))

(define-public crate-rai-opt-0.6.0 (c (n "rai-opt") (v "0.6.0") (d (list (d (n "rai-core") (r "^0.6.0") (d #t) (k 0)))) (h "1lai4vsgfllz4qagf18c8s6pr6mh3y7r4wqz6s471fqhcwz3wzv2")))

(define-public crate-rai-opt-0.7.0 (c (n "rai-opt") (v "0.7.0") (d (list (d (n "rai-core") (r "^0.7.0") (d #t) (k 0)))) (h "1nfnv54lwk9jxswl925cij9nkag048ck69pazc132zfw4k4r876k")))

(define-public crate-rai-opt-0.8.0 (c (n "rai-opt") (v "0.8.0") (d (list (d (n "rai-core") (r "^0.8.0") (d #t) (k 0)))) (h "1l48l44041rn34p3fkk82y579xqzxz1in4sc699m0llr9a5m5i3q") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-opt-0.9.0 (c (n "rai-opt") (v "0.9.0") (d (list (d (n "rai-core") (r "^0.9.0") (d #t) (k 0)))) (h "07rgacsc3yby8wll2za9xi0wmcylqi1illvymy9qi3p2pxhd0avz") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-opt-0.10.0 (c (n "rai-opt") (v "0.10.0") (d (list (d (n "rai-core") (r "^0.10.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-test") (r "^0.1") (d #t) (k 0)))) (h "12kxixqx7hnnkksvxfwc3fdrgr6rcya84vps51fi16yqjr7954ym") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

(define-public crate-rai-opt-0.11.0 (c (n "rai-opt") (v "0.11.0") (d (list (d (n "rai-core") (r "^0.11.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-test") (r "^0.1") (d #t) (k 0)))) (h "0p3hz592mzdk5yk450s9v0g8xhgw19r21jjn3rf00wgywh4i13r6") (f (quote (("mkl" "rai-core/mkl") ("metal" "rai-core/metal") ("default" "candle-backend") ("cudnn" "rai-core/cudnn") ("cuda" "rai-core/cuda") ("candle-backend" "rai-core/candle-backend") ("accelerate" "rai-core/accelerate"))))))

