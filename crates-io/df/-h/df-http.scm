(define-module (crates-io df -h df-http) #:use-module (crates-io))

(define-public crate-df-http-0.1.1 (c (n "df-http") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "df-file") (r "^0.1.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "0ybxbklz10fz316xaxriwmnab0s326jqlwc1l1b2vsdn24bpawp1")))

(define-public crate-df-http-0.1.2 (c (n "df-http") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "df-file") (r "^0.1.1") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "0yapzmz3ibh5dl7lyr95vx6ycpsqfkmdafs27zrcx5lwbmvn2i17")))

