(define-module (crates-io df mn dfmn) #:use-module (crates-io))

(define-public crate-dfmn-0.1.0 (c (n "dfmn") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "online") (r "^4.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "14g816pwyy04jmybbgv6h2vrz2ddy2mc4crh75lmd9r4k3wc0hjd")))

