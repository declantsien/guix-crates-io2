(define-module (crates-io df x- dfx-base) #:use-module (crates-io))

(define-public crate-dfx-base-1.0.0-alpha (c (n "dfx-base") (v "1.0.0-alpha") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "1wjx94yzl9qp0ac0i59b28fsjwsfn68sx3ziqv24vbbl9hvgf9a6")))

(define-public crate-dfx-base-1.0.0-beta (c (n "dfx-base") (v "1.0.0-beta") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pprof") (r "^0.12") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "1x15018qxyf1ngcqwyfmihlkc93kgd7m2hq4clfzz9178m0ndv4y")))

