(define-module (crates-io df kz dfkzr) #:use-module (crates-io))

(define-public crate-dfkzr-0.1.0 (c (n "dfkzr") (v "0.1.0") (d (list (d (n "proptest") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "proptest-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0rfhkx9s7290f87s8xcc21nsvywa4ps6ckqaz5v07lkl8xrmfryq") (f (quote (("default")))) (s 2) (e (quote (("proptest" "dep:proptest" "dep:proptest-derive"))))))

