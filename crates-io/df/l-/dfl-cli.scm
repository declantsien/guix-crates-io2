(define-module (crates-io df l- dfl-cli) #:use-module (crates-io))

(define-public crate-dfl-cli-1.2.3 (c (n "dfl-cli") (v "1.2.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "invidious") (r "^0.7.2") (f (quote ("reqwest_async"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.3") (d #t) (k 0)))) (h "1b7rn2kqlidl3d0pg5w4yfsxd70mz64yycsn4s599mpayindzg98")))

