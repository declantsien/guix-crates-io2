(define-module (crates-io df u- dfu-core) #:use-module (crates-io))

(define-public crate-dfu-core-0.0.0 (c (n "dfu-core") (v "0.0.0") (h "0y1xndrwcpwmri8pg746x126bd5pxwbmm97sz2rgb01z96fyr2ws") (y #t)))

(define-public crate-dfu-core-0.1.0 (c (n "dfu-core") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1vmnv13brdza4376f3zbzk9wsj4bk5cais13hkz8m4cvb9m0ibf2") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.2.0 (c (n "dfu-core") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1wsrkhcxy4bixchjwqspcd8fx8v8q339jcxw3lvzjjhfg70zz0iv") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.2.1 (c (n "dfu-core") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "03499lfgdzlkhy46wicv5y7iwa5iicrzz8ak93dh3j1148wbgyh0") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.3.0 (c (n "dfu-core") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1wxfsl8x36z31hyvpdn1q1p94fwqvn79gk40b852g0cyhj23si9b") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.4.0 (c (n "dfu-core") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "16imck6bapchg5pqf7i6g8j2js2j4gxrsw29s42s1g4s284k8wmy") (f (quote (("std" "thiserror")))) (y #t)))

(define-public crate-dfu-core-0.4.1 (c (n "dfu-core") (v "0.4.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1kr73vfcbnw05cibk5ghwfnd3hz7wrz58630danckkwss2qni2k7") (f (quote (("std" "thiserror")))) (y #t)))

(define-public crate-dfu-core-0.4.2 (c (n "dfu-core") (v "0.4.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1271pljh2sn4lfl8y466vazdpyln91vzdjfzfsyqpp5yji7b6gvc") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.5.0 (c (n "dfu-core") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "12sfyjj2vqy2sghnn3bfmxdp9qvfnqlh54g3nkmwrwi7cd3hk9jp") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.5.1 (c (n "dfu-core") (v "0.5.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "1yjrmx79ljxs8r32wy9c7788kvyhnfhhyhf4kq6lmz6x5r24jp41") (f (quote (("std" "thiserror"))))))

(define-public crate-dfu-core-0.6.0 (c (n "dfu-core") (v "0.6.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 2)) (d (n "pretty-hex") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0xh38v6szc227bhacf8pyyg72cnp1avcpli0v9kmqa098703c4rw") (f (quote (("std" "thiserror"))))))

