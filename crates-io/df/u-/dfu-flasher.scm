(define-module (crates-io df u- dfu-flasher) #:use-module (crates-io))

(define-public crate-dfu-flasher-0.2.0 (c (n "dfu-flasher") (v "0.2.0") (d (list (d (n "dfu") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.2") (d #t) (k 0)))) (h "1ig2fnk930v6kx12wc2fv9fb8va34spif3xfap46zyr6p1g85zfy")))

(define-public crate-dfu-flasher-0.2.2 (c (n "dfu-flasher") (v "0.2.2") (d (list (d (n "dfu") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.2") (d #t) (k 0)))) (h "0gzhkxncyg3gdv3cary0hxvh7mgkk8n6zyhnp4x0p9764qd33n4j")))

(define-public crate-dfu-flasher-0.3.0 (c (n "dfu-flasher") (v "0.3.0") (d (list (d (n "dfu") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.3") (d #t) (k 0)))) (h "169dnqapx10dccn3h4dxa3334x2528mqvqsr8brrmkyp0jnnk4cg")))

(define-public crate-dfu-flasher-0.3.1 (c (n "dfu-flasher") (v "0.3.1") (d (list (d (n "dfu") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "145zxw2qnjqfz10vix5f4nn8mlnzcrp0iywawsmf67zyg91i6y0w")))

(define-public crate-dfu-flasher-0.4.0 (c (n "dfu-flasher") (v "0.4.0") (d (list (d (n "dfu") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1jivh3h549zqyn7r128m121in8x6msjwb0hv3a9zzd9s7lv6gar6")))

(define-public crate-dfu-flasher-0.4.1 (c (n "dfu-flasher") (v "0.4.1") (d (list (d (n "dfu") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "1vmzm3022k7w3ag4y6vcazkrbpaks2dsgckwl6y9884pmv0wpdk4")))

(define-public crate-dfu-flasher-0.4.2 (c (n "dfu-flasher") (v "0.4.2") (d (list (d (n "dfu") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h28053acm5h1lw4kl77622gr4ign5hf202yjynv254fnnsa120v")))

