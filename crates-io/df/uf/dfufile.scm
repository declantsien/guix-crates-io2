(define-module (crates-io df uf dfufile) #:use-module (crates-io))

(define-public crate-dfufile-0.1.0 (c (n "dfufile") (v "0.1.0") (h "0abpkm94id6yx26764xa3bhac168pk4hi1w1q3nldfxr97kiy7jm")))

(define-public crate-dfufile-0.2.0 (c (n "dfufile") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "169hmy2z7kd5ns724zpn7pqpkx4k8jqq4zk4ff91s5xjb0amz72k") (r "1.58")))

