(define-module (crates-io df an dfang) #:use-module (crates-io))

(define-public crate-dfang-0.1.0 (c (n "dfang") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fhbmjnp0j5rpvgnv67fiacljwqhsb9aabhgs0752hajjjxnvkgs")))

(define-public crate-dfang-0.1.1 (c (n "dfang") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0rswxcgkry5zl9mc6blxk9r4pzy8kh74mpd27b7ldfp55cvkmdxp")))

(define-public crate-dfang-0.1.2 (c (n "dfang") (v "0.1.2") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hzznx8maasjyy59g741y7k4i4rvc5zlgrp6j3fdibq939hs6bw5")))

(define-public crate-dfang-0.1.3 (c (n "dfang") (v "0.1.3") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18kvi585ksvcjj98hah756sxwz6hy213x0pp6vrvjayp2bfmc3vz")))

(define-public crate-dfang-0.1.4 (c (n "dfang") (v "0.1.4") (d (list (d (n "atty") (r "0.2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "011rbq50maczsyzgv85j5cajw7116p8yp8v0cfy1nry4wq6graaq")))

(define-public crate-dfang-0.1.5 (c (n "dfang") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12andrkj3pczyjxvmr51xs518hi4akpf22gs8zxa3izb7y3qh2fa")))

