(define-module (crates-io df _s df_st_derive) #:use-module (crates-io))

(define-public crate-df_st_derive-0.3.0-development-1 (c (n "df_st_derive") (v "0.3.0-development-1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03g1bbdfr92fsc33pcv5rzzckrz7clcjc4sjz9744bscmhss6qkl")))

(define-public crate-df_st_derive-0.3.0-development-2 (c (n "df_st_derive") (v "0.3.0-development-2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zp4i4zvrd6wvly0p41fwassx1i86iivg0pqad4bxbllj9i9ppk2")))

