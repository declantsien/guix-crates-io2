(define-module (crates-io df _s df_st_image_maps) #:use-module (crates-io))

(define-public crate-df_st_image_maps-0.3.0-development-1 (c (n "df_st_image_maps") (v "0.3.0-development-1") (d (list (d (n "df_st_core") (r "^0.3.0-development-1") (d #t) (k 0)) (d (n "df_st_derive") (r "^0.3.0-development-1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xlixxg0kcwy3xxdjnzr1qmg913x6lnb5z7xdbjkd005a84a1k60")))

(define-public crate-df_st_image_maps-0.3.0-development-2 (c (n "df_st_image_maps") (v "0.3.0-development-2") (d (list (d (n "df_st_core") (r "^0.3.0-development-2") (d #t) (k 0)) (d (n "df_st_derive") (r "^0.3.0-development-2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fyl927ivjrr08nynlid9g0f9k2ii4jk4qr205g1c90s3bh2wvb4")))

