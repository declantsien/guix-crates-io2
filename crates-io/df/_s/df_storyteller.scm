(define-module (crates-io df _s df_storyteller) #:use-module (crates-io))

(define-public crate-df_storyteller-0.3.0-development-1 (c (n "df_storyteller") (v "0.3.0-development-1") (d (list (d (n "df_st_cli") (r "^0.3.0-development-1") (d #t) (k 0)))) (h "18jigad2ciip2ns81cp5yjbm76ksan714wcdr67qzppr2chhasf1") (f (quote (("sqlite" "df_st_cli/sqlite") ("postgres" "df_st_cli/postgres") ("default" "sqlite"))))))

(define-public crate-df_storyteller-0.3.0-development-2 (c (n "df_storyteller") (v "0.3.0-development-2") (d (list (d (n "df_st_cli") (r "^0.3.0-development-2") (k 0)))) (h "1gh6hpyw6nn5cyn4bdx2gvlxn2507w9jzps3by74djr8pw26vkbp") (f (quote (("sqlite" "df_st_cli/sqlite") ("postgres" "df_st_cli/postgres") ("default" "sqlite"))))))

