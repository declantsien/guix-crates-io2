(define-module (crates-io df _s df_st_world_history) #:use-module (crates-io))

(define-public crate-df_st_world_history-0.3.0-development-1 (c (n "df_st_world_history") (v "0.3.0-development-1") (h "0qq1l47312y44z2h1kyskrdb3jfpyijhrq926nvn74cxsc32z8fc")))

(define-public crate-df_st_world_history-0.3.0-development-2 (c (n "df_st_world_history") (v "0.3.0-development-2") (h "0b03rriziry0nf1x7xxw7clljl67yzzl3nr5swis66akn0wv24sm")))

