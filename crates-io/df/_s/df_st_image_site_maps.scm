(define-module (crates-io df _s df_st_image_site_maps) #:use-module (crates-io))

(define-public crate-df_st_image_site_maps-0.3.0-development-1 (c (n "df_st_image_site_maps") (v "0.3.0-development-1") (d (list (d (n "df_st_core") (r "^0.3.0-development-1") (d #t) (k 0)) (d (n "df_st_derive") (r "^0.3.0-development-1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11z18208a39xvqzgfqdaqs5ylb9iv8ckmwbkjvqpcjnxx8a3fs7z")))

(define-public crate-df_st_image_site_maps-0.3.0-development-2 (c (n "df_st_image_site_maps") (v "0.3.0-development-2") (d (list (d (n "df_st_core") (r "^0.3.0-development-2") (d #t) (k 0)) (d (n "df_st_derive") (r "^0.3.0-development-2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "084mzx902jqnk1wy1yg7prb0pvyi5j6k1zw4v61xbpd50q5wnxpi")))

