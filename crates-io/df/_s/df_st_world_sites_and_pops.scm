(define-module (crates-io df _s df_st_world_sites_and_pops) #:use-module (crates-io))

(define-public crate-df_st_world_sites_and_pops-0.3.0-development-1 (c (n "df_st_world_sites_and_pops") (v "0.3.0-development-1") (h "066qkcm6gnm1s0hb2jdm2g2lxhgr9p90n2fmwqqczb1jd3zwzdny")))

(define-public crate-df_st_world_sites_and_pops-0.3.0-development-2 (c (n "df_st_world_sites_and_pops") (v "0.3.0-development-2") (h "1mhhjwi0wp34jqi050l1m38fp4mgk9lpfb2a6m3j62xszhpnwhaq")))

