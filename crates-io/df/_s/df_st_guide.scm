(define-module (crates-io df _s df_st_guide) #:use-module (crates-io))

(define-public crate-df_st_guide-0.3.0-development-1 (c (n "df_st_guide") (v "0.3.0-development-1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (f (quote ("tls"))) (d #t) (k 0)) (d (n "rust-embed") (r "^5.5") (d #t) (k 0)))) (h "04b37yxncjz1gwzidq5wyqm0r9i73vx9wg898pglmjny7mh2s75g")))

(define-public crate-df_st_guide-0.3.0-development-2 (c (n "df_st_guide") (v "0.3.0-development-2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (f (quote ("tls"))) (d #t) (k 0)) (d (n "rust-embed") (r "^5.5") (d #t) (k 0)))) (h "0cbk7z9a7ddsg0l8qdn3nrmki7b957jdbrhx96z3q77y0i3r8nrc")))

