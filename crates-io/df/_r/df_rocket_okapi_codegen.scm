(define-module (crates-io df _r df_rocket_okapi_codegen) #:use-module (crates-io))

(define-public crate-df_rocket_okapi_codegen-0.4.1 (c (n "df_rocket_okapi_codegen") (v "0.4.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rocket_http") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "059ch4x04mghpza4pr213srl76d2m15fbbn0bc95fgcram7p70hc")))

