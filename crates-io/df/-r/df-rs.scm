(define-module (crates-io df -r df-rs) #:use-module (crates-io))

(define-public crate-df-rs-0.1.0 (c (n "df-rs") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)))) (h "1hwg1l30zfxlhr41nqrk6lw58bmcpz1zc4z67m6fqf1zxy03cjrn")))

(define-public crate-df-rs-0.2.0 (c (n "df-rs") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)))) (h "14wlsx7srcg7cyhisdj48rvd39g2hwhqjc6rlvf8rfawsp6rvhis")))

(define-public crate-df-rs-0.2.1 (c (n "df-rs") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)))) (h "1yzpdsy00dc232vxl19wpsyfylxcq7ahqiwc4ba30fdz2a8bjdww")))

(define-public crate-df-rs-0.2.2 (c (n "df-rs") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1bimg6ljwmx0bfp17gmr7bl4iw7wlp6wgzwz1rbk7d4ypcghyf0s")))

