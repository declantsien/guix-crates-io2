(define-module (crates-io df il dfiles) #:use-module (crates-io))

(define-public crate-dfiles-0.1.0 (c (n "dfiles") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "srcu") (r "^0.1.0") (d #t) (k 0)))) (h "0vf37ifmhmm76p1jjgln6diai8vwcsjdnshyibrb6chgwskhpkav") (r "1.73")))

(define-public crate-dfiles-0.1.1 (c (n "dfiles") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "srcu") (r "^0.1.0") (d #t) (k 0)))) (h "02f8gg8lvc669q17w90zlibj2dhk5ir75fhdniq9b5mmbq5vl582") (r "1.73")))

(define-public crate-dfiles-0.1.2 (c (n "dfiles") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.3.2") (d #t) (k 0)) (d (n "srcu") (r "^0.1") (d #t) (k 0)))) (h "0vy4gsa51fxa7rvvbk7kbh48dfrh96cfd2s82lh97ay1wdca07zp") (r "1.73")))

