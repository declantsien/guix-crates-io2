(define-module (crates-io df -e df-excel) #:use-module (crates-io))

(define-public crate-df-excel-0.1.0 (c (n "df-excel") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.25.0") (d #t) (k 0)))) (h "04c3k01z4nsww4lp3056kxrqbq5q9jwspbm33s5kapky28an7af2")))

(define-public crate-df-excel-0.1.1 (c (n "df-excel") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.25.0") (d #t) (k 0)))) (h "0grl09w26knzdjwvgc96z9pksd5lbwrrswhzq2015mwiaj9h7zgp")))

(define-public crate-df-excel-0.1.2 (c (n "df-excel") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.26.0") (d #t) (k 0)))) (h "033nnvsz9nhcb9n0y8wj19ivfd45c83vs3disynkf3l9nw8jjkkp")))

(define-public crate-df-excel-0.1.3 (c (n "df-excel") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "054aq88m5s86lhild0kyaakxwc6f8fxi1l78jfjqc94jy18yi5hn")))

(define-public crate-df-excel-0.1.4 (c (n "df-excel") (v "0.1.4") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "09zzk7mr3yfp5sxwr2y38is27b6kjgbrsnk080j504p5zflsd7gx")))

(define-public crate-df-excel-0.1.5 (c (n "df-excel") (v "0.1.5") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "1ii26sccj4mnxfp9g7pkbix8c2za0lkzmkmgsc111ipwm8lcym33")))

(define-public crate-df-excel-0.1.6 (c (n "df-excel") (v "0.1.6") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "04kwnm1g8ypir1x8773ml7p83n7yvpk1nk1zzdcvq52x0l8f030w")))

(define-public crate-df-excel-0.1.7 (c (n "df-excel") (v "0.1.7") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)))) (h "1703j1gl987k414ggzfcsy2rhr748miffr8gaa2j0hrm07akvwcl")))

(define-public crate-df-excel-0.1.8 (c (n "df-excel") (v "0.1.8") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "office") (r "^0.8.1") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.29.0") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)) (d (n "df-file") (r "^0.1.3") (d #t) (k 2)))) (h "185m58xaqlxki4zpj2mn0g3xfk8m4pzz09cr05gbfy5azlnbymjf")))

(define-public crate-df-excel-0.1.9 (c (n "df-excel") (v "0.1.9") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)) (d (n "df-file") (r "^0.1.3") (d #t) (k 2)))) (h "0lfviww3mwdm58i6m10af7vr955dnpljmcar7zp1mfx18z3gcamz")))

(define-public crate-df-excel-0.1.10 (c (n "df-excel") (v "0.1.10") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "umya-spreadsheet") (r "^0.9.1") (d #t) (k 0)) (d (n "df-file") (r "^0.1.3") (d #t) (k 2)))) (h "0n8agmrqcnlrylq03wrw90kvd4y75nappx1a1s95zw0xkk34l9g9")))

