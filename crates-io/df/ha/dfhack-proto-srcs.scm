(define-module (crates-io df ha dfhack-proto-srcs) #:use-module (crates-io))

(define-public crate-dfhack-proto-srcs-0.1.0 (c (n "dfhack-proto-srcs") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "1qzmv3z56p7wbgl2jrim8cj98ivgh7i6wb4msnr1zcc00dv0zalj") (y #t)))

(define-public crate-dfhack-proto-srcs-0.1.1 (c (n "dfhack-proto-srcs") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "13lg5iij7q4dd63xi26cmy4af5s8i5k81b56mp8s7a4sn1y7fv1p") (y #t)))

(define-public crate-dfhack-proto-srcs-0.2.0 (c (n "dfhack-proto-srcs") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "1l7xqvk4n2jqbz49scg1bh8d0wv920dhh97kard6jw139jg38n7w")))

(define-public crate-dfhack-proto-srcs-0.3.0 (c (n "dfhack-proto-srcs") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "01ld2mnc5xak1hgh3p2khqhk221gsdb2p42r64b6khzk6x9f62dc")))

(define-public crate-dfhack-proto-srcs-0.4.0 (c (n "dfhack-proto-srcs") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0pamppf901svjl895p94p9hibvns43y9wrw1aaf89xb0vglmh6hr")))

(define-public crate-dfhack-proto-srcs-0.4.1 (c (n "dfhack-proto-srcs") (v "0.4.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0g7qdwn1x3fhjx0ipb0dvsvzx7qqxx3whga8jrgz1iwd8wmh6fld")))

(define-public crate-dfhack-proto-srcs-0.4.2-dev (c (n "dfhack-proto-srcs") (v "0.4.2-dev") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "1vly7xabappq4rfl63dcgbwnvp7ggacfhcskszhmqfizwf3j3rrf")))

(define-public crate-dfhack-proto-srcs-0.4.3 (c (n "dfhack-proto-srcs") (v "0.4.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "150q78pf6p9flig5ibndfc9ika1v4gspr1zl1dhhibinma9g9m1w")))

(define-public crate-dfhack-proto-srcs-0.4.4 (c (n "dfhack-proto-srcs") (v "0.4.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "10abwck98ip3i7fcbq984w8hsa0njajv96vy1rkw7zyrsi9cwz82")))

(define-public crate-dfhack-proto-srcs-0.5.0 (c (n "dfhack-proto-srcs") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0z9rp7vbs7kc4s0303ia3qjy2rasyk7z3v1r43p6zn7w30xs9x34")))

(define-public crate-dfhack-proto-srcs-0.6.0 (c (n "dfhack-proto-srcs") (v "0.6.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0r8ifv51lxfqia0ndx3xvjbqnw0fcqv3gxz920y60r5jv8maf8wx")))

(define-public crate-dfhack-proto-srcs-0.6.1 (c (n "dfhack-proto-srcs") (v "0.6.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "1h85m43vzsg8nhznk23dd5afs09nn3xqz3a19951241zym79m7a7")))

(define-public crate-dfhack-proto-srcs-0.6.2 (c (n "dfhack-proto-srcs") (v "0.6.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0lcrrjx8m6vw8sd3fah3rg2dbf3j13446p62p38rjkl0ynf90g9x")))

(define-public crate-dfhack-proto-srcs-0.7.0 (c (n "dfhack-proto-srcs") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "0x7ng9b211d781fmfj1jjks7xa36zbjhi4cfj3mcd0w259n9mapn")))

(define-public crate-dfhack-proto-srcs-0.8.0 (c (n "dfhack-proto-srcs") (v "0.8.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^0.6.2") (d #t) (k 1)))) (h "16baxb97cs5xcffz323xf0cfl7sbvaqvaj8xf7cibqm3syzdhwrs")))

(define-public crate-dfhack-proto-srcs-0.9.0 (c (n "dfhack-proto-srcs") (v "0.9.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "zip") (r "^1.1.1") (d #t) (k 1)))) (h "1wjaa6mzm3xs44azmmmzpq3vzywb6sjxvlfhnpg0a1c6291hfqiv")))

