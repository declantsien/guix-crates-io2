(define-module (crates-io df -f df-fields) #:use-module (crates-io))

(define-public crate-df-fields-0.1.0 (c (n "df-fields") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0rlafs26sanchk4gf1chsjx6zqg24r5dwry4jfgh518p3vqpn2f7") (y #t)))

(define-public crate-df-fields-0.1.1 (c (n "df-fields") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1bjy9zv2zlzzchq5567d3ag7biwyy7vnh6q2i9vc908m53gv68pg") (y #t)))

(define-public crate-df-fields-0.1.2 (c (n "df-fields") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "06yzzdlhi9624gsh0m36jvc91283g4wkdanmd3bxzq5l8yiyj3pw") (y #t)))

(define-public crate-df-fields-0.1.3 (c (n "df-fields") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1jjmg0dmxmslh2xs80j07w6p12h8fbswgb9xlfp7ch1sihs7z3ci") (y #t)))

(define-public crate-df-fields-0.1.4 (c (n "df-fields") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0im0xz20qss5zcrwx5bbbpf6ljkwhp9wip3n4qcs3lc5azp9qkfh") (y #t)))

(define-public crate-df-fields-0.1.5 (c (n "df-fields") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "135n39fdjxihif01vrlmyqj3327yabipx6hv13fxyc78f6vf29q1") (y #t)))

(define-public crate-df-fields-0.1.6 (c (n "df-fields") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1yz9p8syqdrfr1xpiw6vqvpry8nvnd9z33aq8k84whp4s42c52br") (y #t)))

(define-public crate-df-fields-0.1.7 (c (n "df-fields") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1qj6aipa9yjbcv9mgy7a04kq9739rwvlaxpwgdraqs2a0gzz0bd6") (y #t)))

(define-public crate-df-fields-0.1.8 (c (n "df-fields") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16g5dd5a3qmdd8vx2mg1i0wgz1xg3nxhhd33iabp4lpnsa6dnx65") (y #t)))

(define-public crate-df-fields-0.1.9 (c (n "df-fields") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1lbvmimnv8yz032savy46wy4pcbyskw38qdqkq1q9vzra5vvf7v6") (y #t)))

(define-public crate-df-fields-0.1.10 (c (n "df-fields") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1kixsv6ikaffrjl8npmzy9krn08hzhcmdfxpa87yw34kphsda1sb") (y #t)))

(define-public crate-df-fields-0.1.11 (c (n "df-fields") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1ja6y48n929h099dy1z1vjangm8a31q7qgh2l9l1517aig2q6zzi") (y #t)))

(define-public crate-df-fields-0.1.12 (c (n "df-fields") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "13cv1psly7m3h3dprqjgjbidgcydf0ds04ik41fh3f8ncn5fa3kp") (y #t)))

(define-public crate-df-fields-0.1.13 (c (n "df-fields") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1vvbgw05s3109wg1ngpzlnkb8c6ykx36ipc9kw3341vvpfskf4v7") (y #t)))

(define-public crate-df-fields-0.1.14 (c (n "df-fields") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16vdkh08rpxpmwikjh93mjl6rric9d5xmsn2jlx9pqxns9r9jk0s") (y #t)))

(define-public crate-df-fields-0.1.15 (c (n "df-fields") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "172ya436q3n70an5m3wvdqrz0rllibck8avkpq8z8qdxcvqkh2dq") (y #t)))

(define-public crate-df-fields-0.1.16 (c (n "df-fields") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16axqf10lndwnrcjlczj8qnnvfmvb87f08xj83mb1v7nc70g8k5n")))

