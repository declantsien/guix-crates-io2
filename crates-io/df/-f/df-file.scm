(define-module (crates-io df -f df-file) #:use-module (crates-io))

(define-public crate-df-file-0.1.0 (c (n "df-file") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "01s13adwrx031k6vxn8jyshcjmmz49hz9zksy5kqb4svi35m6nrr") (y #t)))

(define-public crate-df-file-0.1.1 (c (n "df-file") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "11rh320x3x7xm0avc08fp1pm4wmwly3ghzlhygsinl78iw4fx5ga") (y #t)))

(define-public crate-df-file-0.1.2 (c (n "df-file") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "oxipng") (r "^8.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zc3gqx2n8p2y2f581hz5w0631hlm6rb1q94sfj06j1h6w7q5mjh") (y #t)))

(define-public crate-df-file-0.1.3 (c (n "df-file") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "oxipng") (r "^8.0.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1wpl29963y60b9yndwg4nqk5dnwsjvlnvyxgvchvy2xvjh58512v")))

