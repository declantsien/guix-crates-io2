(define-module (crates-io df s- dfs-rs) #:use-module (crates-io))

(define-public crate-dfs-rs-0.0.1 (c (n "dfs-rs") (v "0.0.1") (h "0chld4i22sas8pyb08wdgag18f8h46mdrmr4i0hvcbxdcl7kpd1f")))

(define-public crate-dfs-rs-0.0.2 (c (n "dfs-rs") (v "0.0.2") (h "14nrhi7pbvgcaw3pkxh7pawzvv9naq5qsdnn4cbs545lyfk7d9sq") (f (quote (("unsafe") ("default" "unsafe"))))))

