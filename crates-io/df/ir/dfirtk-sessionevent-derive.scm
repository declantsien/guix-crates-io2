(define-module (crates-io df ir dfirtk-sessionevent-derive) #:use-module (crates-io))

(define-public crate-dfirtk-sessionevent-derive-0.1.0 (c (n "dfirtk-sessionevent-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "dfirtk-eventdata") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08vhl8f5s5ybx3g3zgglxszp7dk956qgrpwiyv8vw772z8h4igip")))

