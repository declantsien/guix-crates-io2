(define-module (crates-io df ir dfirtk-eventdata) #:use-module (crates-io))

(define-public crate-dfirtk-eventdata-0.1.0 (c (n "dfirtk-eventdata") (v "0.1.0") (h "0zxyisr51cbmh3y2hyg79asj5kklw64cyg7dh4yvgi0d5pqdz995") (y #t)))

(define-public crate-dfirtk-eventdata-0.1.1 (c (n "dfirtk-eventdata") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "evtx") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08j94phh5rpp9p4mg7xdkwqpnh7j9p465dxbvmifyg63ycj4fzbq")))

(define-public crate-dfirtk-eventdata-0.1.2 (c (n "dfirtk-eventdata") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "evtx") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_info"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02dmm18y6d04q3pqrbf2i8ywc7gdx7nynvfs1bd62ihfaq8nbjgg")))

