(define-module (crates-io df #{23}# df2301q) #:use-module (crates-io))

(define-public crate-df2301q-0.1.0 (c (n "df2301q") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "0yaqkx971923pvws92ah9vnq947417cgs6dc8nclaw6ncxnjhwnm")))

