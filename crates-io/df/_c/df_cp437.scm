(define-module (crates-io df _c df_cp437) #:use-module (crates-io))

(define-public crate-df_cp437-1.0.0 (c (n "df_cp437") (v "1.0.0") (h "1zjibz4lpj2z768842lhi0kc6nbr8p89wvldzz5cssbhjdgc46hy")))

(define-public crate-df_cp437-1.0.1 (c (n "df_cp437") (v "1.0.1") (h "0pl6c856g1lx2j1z51jl3jjy7msiagpj8c7mrih1mi905dnniyyn")))

(define-public crate-df_cp437-1.1.0 (c (n "df_cp437") (v "1.1.0") (h "1wza9vx8rbm9amxmbyw25zaz0v8pjaf3bi797wcwsfif1lvp20v8")))

