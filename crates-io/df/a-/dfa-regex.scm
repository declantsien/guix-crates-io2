(define-module (crates-io df a- dfa-regex) #:use-module (crates-io))

(define-public crate-dfa-regex-0.0.1 (c (n "dfa-regex") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)))) (h "03izj7k45935x279jw0dnd9ifg0cvv52mb7xsww8zxfa174wqv3c")))

(define-public crate-dfa-regex-0.0.2 (c (n "dfa-regex") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "03bmczagyiya65i5a71j3ybdv9fv3wk83l9z61s2gixydnv5fkv7")))

(define-public crate-dfa-regex-0.0.3 (c (n "dfa-regex") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.83") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.65") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "00q2x6gihy6hx8cgampz236015scmgl0s3w1gy3mxbxj1x8kx8hj")))

