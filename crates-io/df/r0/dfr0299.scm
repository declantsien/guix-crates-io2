(define-module (crates-io df r0 dfr0299) #:use-module (crates-io))

(define-public crate-dfr0299-0.1.0 (c (n "dfr0299") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1r36w0783c5i7yniias3v4gmxyg4w9dvfilv5mlp56khlizgbcn7") (f (quote (("std" "num_enum/std")))) (s 2) (e (quote (("use_defmt" "dep:defmt"))))))

(define-public crate-dfr0299-0.1.1 (c (n "dfr0299") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1qk9pcphlvqmy9qaizsjnmx9hng7kqaszy1rq3xrbpr5fa4jy8pi") (f (quote (("std" "num_enum/std")))) (s 2) (e (quote (("use_defmt" "dep:defmt"))))))

