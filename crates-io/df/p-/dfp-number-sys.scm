(define-module (crates-io df p- dfp-number-sys) #:use-module (crates-io))

(define-public crate-dfp-number-sys-0.0.1 (c (n "dfp-number-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0rvmgkrnlan5syqc69x9c0fzf0nvlnlvq2zl2q7z6ikdimh3m1s2")))

(define-public crate-dfp-number-sys-0.0.2 (c (n "dfp-number-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1ja1lmqql9zqk1s78hid2f33nni1n0ib5cms6j6w4n0ing2dm7sh") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.3 (c (n "dfp-number-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1p970z454wzqzmpv1ljsxipng9x2y3km6zmlikhcxwi2c6qlij23") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.4 (c (n "dfp-number-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0gyk00452lq6prxmaa7xd27k9yypl24x3y7rnj4y872vh02sr088") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.5 (c (n "dfp-number-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0fv4kg336r666cb8k6yabsryqjz9hanj2dw6ag0ys59v0n70ql2h") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.6 (c (n "dfp-number-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0v7dgh90g690xs03i35r4drnvv6k9h1fbc0bzzr7r7r983wqj7jm") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.7 (c (n "dfp-number-sys") (v "0.0.7") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "16x01mm6gk8acsa03iz3838f20ldlps9alw0mn6zr6bkh06l8qxs") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.8 (c (n "dfp-number-sys") (v "0.0.8") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0xxdf6wnr8xbh4si12x56imwfw6miqhnbnl7dx6skw72sd5qlsqx") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.9 (c (n "dfp-number-sys") (v "0.0.9") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "1rb5s1i5cxr76w0b02np5p57wsk7cgk6kll289n3ijfy6hwnnbj4") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.10 (c (n "dfp-number-sys") (v "0.0.10") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "028v7i123y2bsv7id8ldzlmdz32b3lxsd5pgdsz6f58wy27spldp") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.11 (c (n "dfp-number-sys") (v "0.0.11") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "0mi5mfgm8s3c1kspv7mjzjvcqjws2mg62983iscpq4y2clcxavwa") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.12 (c (n "dfp-number-sys") (v "0.0.12") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)))) (h "15li48zf2bmk2lbd47s0gdmzn5b108n8ma9ajhjb3ypmjzq5fp49") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.13 (c (n "dfp-number-sys") (v "0.0.13") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)))) (h "0dyznrsq4mkkgxwp96hlqrz5x7l062i00bhwsf9zvxmd9dqkirja") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.14 (c (n "dfp-number-sys") (v "0.0.14") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "19akjk5bsnq4l31h2pmqr64bnkdil97lpv0pgh3qci7fgvjk3sdk") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.15 (c (n "dfp-number-sys") (v "0.0.15") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)))) (h "1vdvn4xnalaghv3p3adapzc4hfd7ayklxfs79z82vk59vpw4lz21") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.16 (c (n "dfp-number-sys") (v "0.0.16") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "0mv9wag4v2653xcf6y4a57qyvd5svhgxk2xaskjc5gmb6cnkh225") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.17 (c (n "dfp-number-sys") (v "0.0.17") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "1x3xzisn8rpkbfxz9yarjj2lpsa2ya5mjlr168wdphiz4pvi1878") (f (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.18 (c (n "dfp-number-sys") (v "0.0.18") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "0wl092b44nkwx8yikfhwci1rhpghjckpdpgfjgv1kw9nk12i5k1x")))

(define-public crate-dfp-number-sys-0.0.19 (c (n "dfp-number-sys") (v "0.0.19") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)))) (h "1axasfk4010zlq2b6bmcg27sgaxd0h3xy7ijp2yicrzbgiaws7z3")))

(define-public crate-dfp-number-sys-0.1.0 (c (n "dfp-number-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)))) (h "16h875r7iqpvrfjja64qlmv9sj5p69clbfq5xpjrrys0qnypb3yk")))

