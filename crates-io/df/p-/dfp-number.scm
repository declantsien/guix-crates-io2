(define-module (crates-io df p- dfp-number) #:use-module (crates-io))

(define-public crate-dfp-number-0.0.1 (c (n "dfp-number") (v "0.0.1") (d (list (d (n "dfp-number-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0hk8d3n9y5r8sgn307x2v8yshk7dn20mg0kf9ypic0aanpgzjqqz")))

(define-public crate-dfp-number-0.0.2 (c (n "dfp-number") (v "0.0.2") (d (list (d (n "dfp-number-sys") (r "^0.0.2") (d #t) (k 0)))) (h "107hmv05jvlbgawy5px22mazp9d1pc4v3wa4n1z0ql9877wap0i9")))

(define-public crate-dfp-number-0.0.3 (c (n "dfp-number") (v "0.0.3") (d (list (d (n "dfp-number-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1pv95fa2dfap9a2g66pcjmxllzanz1gckpspzgj22241big0imi5")))

(define-public crate-dfp-number-0.0.4 (c (n "dfp-number") (v "0.0.4") (d (list (d (n "dfp-number-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1affkz72jyhrl8jzj1s9sbavwkmca399ljagdn0nb3bzfgdpr173")))

(define-public crate-dfp-number-0.0.5 (c (n "dfp-number") (v "0.0.5") (d (list (d (n "dfp-number-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0cxjckz5b8dby6881ivn7f8bihp8z6db28m97m2rsq04qw28ycz6")))

(define-public crate-dfp-number-0.0.6 (c (n "dfp-number") (v "0.0.6") (d (list (d (n "dfp-number-sys") (r "^0.0.3") (d #t) (k 0)))) (h "12j1kbsxpz73aqx6h75zxxalprg8ykxz0my541b10vprc0yz4h3r")))

(define-public crate-dfp-number-0.0.7 (c (n "dfp-number") (v "0.0.7") (d (list (d (n "dfp-number-sys") (r "^0.0.10") (d #t) (k 0)))) (h "1l9p3b7vrsv3qi9mkymvk3lxh2bkljcpv9j76njkl3gzzmj9z3ms")))

