(define-module (crates-io df _l df_ls_diagnostics) #:use-module (crates-io))

(define-public crate-df_ls_diagnostics-0.3.0-rc.1 (c (n "df_ls_diagnostics") (v "0.3.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.2") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.8") (d #t) (k 0)))) (h "18fif37p3s2485nlgai12p34g9lp2s0pwrplfs86haarg0ibmniv")))

