(define-module (crates-io df _l df_ls_semantic_analysis) #:use-module (crates-io))

(define-public crate-df_ls_semantic_analysis-0.3.0-rc.1 (c (n "df_ls_semantic_analysis") (v "0.3.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "df_ls_core") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "df_ls_diagnostics") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "df_ls_structure") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "df_ls_syntax_analysis") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "071ygh0yi01sp5hl6f43ljg2ri6pf4fglb704i2zm8lc5jyd4y7p")))

