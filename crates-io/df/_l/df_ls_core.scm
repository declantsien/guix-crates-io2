(define-module (crates-io df _l df_ls_core) #:use-module (crates-io))

(define-public crate-df_ls_core-0.3.0-rc.1 (c (n "df_ls_core") (v "0.3.0-rc.1") (d (list (d (n "df_ls_derive") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1794hmvfg7h2h4s8ifh7vdrja617vbypi21aj899lda2adzr2218")))

