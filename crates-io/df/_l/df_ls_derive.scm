(define-module (crates-io df _l df_ls_derive) #:use-module (crates-io))

(define-public crate-df_ls_derive-0.3.0-rc.1 (c (n "df_ls_derive") (v "0.3.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0i29p7wih8qa3m6mn8wsfh021dl471h6868lbiwml5lkwiqvd5f8")))

