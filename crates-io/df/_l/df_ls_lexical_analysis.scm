(define-module (crates-io df _l df_ls_lexical_analysis) #:use-module (crates-io))

(define-public crate-df_ls_lexical_analysis-0.3.0-rc.1 (c (n "df_ls_lexical_analysis") (v "0.3.0-rc.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "df_cp437") (r "^1.1.0") (d #t) (k 2)) (d (n "df_ls_diagnostics") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1far3jiv0b89109cli0pbnl6x6w7zi9745bgxldm238l3lk3x5ld")))

