(define-module (crates-io df _l df_ls_debug_structure) #:use-module (crates-io))

(define-public crate-df_ls_debug_structure-0.3.0-rc.1 (c (n "df_ls_debug_structure") (v "0.3.0-rc.1") (d (list (d (n "df_ls_core") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "df_ls_diagnostics") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "df_ls_syntax_analysis") (r "^0.3.0-rc.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "172q2js3f1a0s34fyk27kxd8z794l6cn0fy3y87ymka5jlc3vw2d")))

