(define-module (crates-io df re dfree) #:use-module (crates-io))

(define-public crate-dfree-0.2.3 (c (n "dfree") (v "0.2.3") (d (list (d (n "sysinfo") (r "^0.16.4") (d #t) (k 0)))) (h "0z513a4z645848lgvczpyvp2ld77vr5sxrajs3bqfnm66v622f9j")))

(define-public crate-dfree-0.2.4 (c (n "dfree") (v "0.2.4") (d (list (d (n "sysinfo") (r "^0.17.4") (d #t) (k 0)))) (h "0jhrygnp1cll7mljqrz34vbbzh6pdq3i0hr7jkjlv9v8f0bpcvzd")))

(define-public crate-dfree-0.2.5 (c (n "dfree") (v "0.2.5") (d (list (d (n "sysinfo") (r "^0.17.4") (d #t) (k 0)))) (h "1gpbykjbdffb9bx580bap77phqchprpyhviv7a4bm6lxs2cabwdq")))

(define-public crate-dfree-0.3.0 (c (n "dfree") (v "0.3.0") (d (list (d (n "sysinfo") (r "^0.17.4") (d #t) (k 0)))) (h "1040y8s8b66m4gnqgr8yiwb54l5lipjq1ha9fw3l7s6lqk3rn0d5")))

