(define-module (crates-io df la dflake) #:use-module (crates-io))

(define-public crate-dflake-0.1.0 (c (n "dflake") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1531v88w8n8hsjjyv9wzc24qqcl0hsn7n370fwmry64lfg2m698d") (f (quote (("default") ("chrono_support" "chrono"))))))

