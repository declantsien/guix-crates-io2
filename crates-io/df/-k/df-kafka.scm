(define-module (crates-io df -k df-kafka) #:use-module (crates-io))

(define-public crate-df-kafka-0.1.0 (c (n "df-kafka") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "kafka") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1imz4jksy8sp8874ifgnji9lpq6wr4f2r4mp2jg95gvwfkf4piy3") (y #t)))

(define-public crate-df-kafka-0.1.1 (c (n "df-kafka") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "kafka") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "15g65hrh2z8x8wshjjn9a26sl0v9nyckncrh748bbn5hjqifmfrb") (y #t)))

(define-public crate-df-kafka-0.1.2 (c (n "df-kafka") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "kafka") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0l6in7g45z3cb4msh8v4hs4d967bx6gqbnbv9n80zsbhx5jbdzrf")))

(define-public crate-df-kafka-0.1.3 (c (n "df-kafka") (v "0.1.3") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "kafka") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0viidc1zmkww90dglylk6h4nha7fl41bcbanm8n3hzalb23gy6fl")))

