(define-module (crates-io df a_ dfa_learning_toolkit) #:use-module (crates-io))

(define-public crate-dfa_learning_toolkit-1.0.0 (c (n "dfa_learning_toolkit") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1di999qjr9hcm1p2dbhw8z3yfkib2llvk5kax67baazrp8p1baz0") (y #t)))

