(define-module (crates-io df -c df-code) #:use-module (crates-io))

(define-public crate-df-code-0.1.0 (c (n "df-code") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vbhlg60v80cz6r1ri54pl08ik7m0a25lrkw8x9w0gpk3ph4w9mw")))

(define-public crate-df-code-0.1.1 (c (n "df-code") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1x1kc5liw259axx9hxrfc9p9ici5i9j762kg2hwhsv7mq62g40hi")))

