(define-module (crates-io df di dfdi) #:use-module (crates-io))

(define-public crate-dfdi-0.1.0 (c (n "dfdi") (v "0.1.0") (d (list (d (n "dfdi-core") (r "^0.1.0") (d #t) (k 0)) (d (n "dfdi-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1xyfd3vyqzjflb96pvwcj37z9zd2jw1avcf5szvaaichgszvqyxg") (f (quote (("derive" "dfdi-macros") ("default" "derive")))) (r "1.65")))

(define-public crate-dfdi-0.2.0 (c (n "dfdi") (v "0.2.0") (d (list (d (n "dfdi-core") (r "^0.2.0") (d #t) (k 0)) (d (n "dfdi-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "0hvxzy3v7z3a13k2qrhijmz8rh8r0khq77laqr9byn399bncisky") (f (quote (("derive" "dfdi-macros") ("default" "derive")))) (r "1.65")))

