(define-module (crates-io df di dfdi-macros) #:use-module (crates-io))

(define-public crate-dfdi-macros-0.1.0 (c (n "dfdi-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1vlh7zi5kz6rgj78xc8i66il5h3l94f5qxlq1ann0qxc1bac0bv7") (r "1.65")))

(define-public crate-dfdi-macros-0.2.0 (c (n "dfdi-macros") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("visit-mut"))) (d #t) (k 0)))) (h "02dnbl50j3c6npy1ly8m89nk19ifj5d2x8ixjskqsj72nrm72apc") (r "1.65")))

