(define-module (crates-io df di dfdi-core) #:use-module (crates-io))

(define-public crate-dfdi-core-0.1.0 (c (n "dfdi-core") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "095h36qnbxq7dq50ymipxch1y0igzhjy5x28w3bxw9p5nc2ggmmb") (r "1.65")))

(define-public crate-dfdi-core-0.2.0 (c (n "dfdi-core") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 2)))) (h "1p7kdajwnrwjbgrhja5cpjra000fxzb5w3n03f2zqv055nydqisz") (r "1.65")))

