(define-module (crates-io df co dfconfig) #:use-module (crates-io))

(define-public crate-dfconfig-0.1.0 (c (n "dfconfig") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jjw3zm9klk7kcd6bnz88rr1g18j54dnyqlbhkv5pmxlbvnh5yln")))

(define-public crate-dfconfig-0.2.0 (c (n "dfconfig") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r03hbx06923122bgjalzznc66myx2qfy1qrbggapsg4rin0hzsm")))

