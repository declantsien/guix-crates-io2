(define-module (crates-io ui d_ uid_store) #:use-module (crates-io))

(define-public crate-uid_store-0.0.1 (c (n "uid_store") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0y6bgik132gyn887mdi95m2w7i6r1c7hrvdqp19y7rdkwv8xh4mc")))

(define-public crate-uid_store-0.0.2 (c (n "uid_store") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04mhlqx406kjli34k5261g3ayqx6ii2215qczfvnfdkps9dlmvvk")))

(define-public crate-uid_store-0.0.4 (c (n "uid_store") (v "0.0.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w979cdyijfah2ss1cfakdf94n4y532jn0fi2qwk0h0fxi4asni1")))

(define-public crate-uid_store-0.0.5 (c (n "uid_store") (v "0.0.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wp84nbiv7f34cqp3sfjgdwv1a2vgdz3f18v1s9aiy9w45jmdgn9")))

(define-public crate-uid_store-0.1.0 (c (n "uid_store") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mzp9lxx2f99ra3czpggkz0n9rfzhxp863kl71j6rcyn05wfbpiz")))

(define-public crate-uid_store-0.1.1 (c (n "uid_store") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00cz62kwjp809zbspmlm1gpsr5hll5s8pd570dwvdiqsmfd36xkq")))

(define-public crate-uid_store-0.1.2 (c (n "uid_store") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nvpa93bmx4gf3542w9sdrzgw9bfim80ykp8ihlswwil1h1fkqr4")))

(define-public crate-uid_store-0.1.3 (c (n "uid_store") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1r88m7cqpl31pcs542wh9lwl690bl2v4j4gk92kz939clx7i5zhd")))

(define-public crate-uid_store-0.1.4 (c (n "uid_store") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03pm4imwh2b82rz3i9wfdr63zwz6sc10y7p832y4i55f7s4blffh")))

(define-public crate-uid_store-0.1.5 (c (n "uid_store") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q52d3v9h2728il88hdw1w1wsv18g1vq1wb7k5cidzhmsh6fg84s")))

(define-public crate-uid_store-0.1.6 (c (n "uid_store") (v "0.1.6") (h "04l3lklwyxdlrvqc2jfwf7c6rssf4wii5x1vcfyq234rn3hx6gn7")))

