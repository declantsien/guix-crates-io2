(define-module (crates-io ui np uinput) #:use-module (crates-io))

(define-public crate-uinput-0.1.0 (c (n "uinput") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "1qh4wsz3aadmcy36iz9y29622m5w4h7yfiazzkbdc58s5l12jh1j") (f (quote (("udev" "libudev") ("default" "udev"))))))

(define-public crate-uinput-0.1.1 (c (n "uinput") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "0qm8d1i4qy8p8n54yk02ka5yscq94lhzw47h449bybkw2m8c0mbr") (f (quote (("udev" "libudev") ("default" "udev"))))))

(define-public crate-uinput-0.1.2 (c (n "uinput") (v "0.1.2") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "1yj65nlgks7s4iwh8f4cy0s8638ja9zbdcq18jrryrij0sclgyy6") (f (quote (("udev" "libudev") ("default" "udev"))))))

(define-public crate-uinput-0.1.3 (c (n "uinput") (v "0.1.3") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "enum_derive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "uinput-sys") (r "^0.1") (d #t) (k 0)))) (h "0nv84siqzw89k2lnj4r9wfld1vhc9ja3zzk3l24jmqqbr5als1wb") (f (quote (("udev" "libudev") ("default" "udev"))))))

