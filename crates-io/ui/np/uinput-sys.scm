(define-module (crates-io ui np uinput-sys) #:use-module (crates-io))

(define-public crate-uinput-sys-0.1.0 (c (n "uinput-sys") (v "0.1.0") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sv2ha2bzjgc18725396aipa34lq0frpwj2dgw1c8jfj3s5gx3pm")))

(define-public crate-uinput-sys-0.1.1 (c (n "uinput-sys") (v "0.1.1") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "087fz2c0mi77ainynrldbs4zjan1phzzgg38dvnfq6l7dvnmpqxy")))

(define-public crate-uinput-sys-0.1.2 (c (n "uinput-sys") (v "0.1.2") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hbbrpf8ahkfwj57i2yay9878x06m68gdnq3cz2sdlw7hjhvbpf6")))

(define-public crate-uinput-sys-0.1.3 (c (n "uinput-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)))) (h "0i0s83ny7y25cmdbzzvml9k7jw74ff1d79wah406sfy0mx13v66c")))

(define-public crate-uinput-sys-0.1.4 (c (n "uinput-sys") (v "0.1.4") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0aq893ilkbc68sz0cp906rdp2idds3bf2872rlkrw1jd6hqc5bcj")))

(define-public crate-uinput-sys-0.1.5 (c (n "uinput-sys") (v "0.1.5") (d (list (d (n "ioctl") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mvjnljmy29w5chjk28hdga6g8mm1qq4xr7jbrvqnngshwycyjnq")))

(define-public crate-uinput-sys-0.1.6 (c (n "uinput-sys") (v "0.1.6") (d (list (d (n "ioctl-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07ra8a11hq18ndc4nh7ghg9bs6qnc1fndg7a8ivg5s9ivssi50lz")))

(define-public crate-uinput-sys-0.1.7 (c (n "uinput-sys") (v "0.1.7") (d (list (d (n "ioctl-sys") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1iyanb28r2vhqp2sgxpwzasivjvnn9mk9azy19hdvjjc2zcdvaws")))

