(define-module (crates-io ui ev uievents-code) #:use-module (crates-io))

(define-public crate-uievents-code-0.1.0 (c (n "uievents-code") (v "0.1.0") (h "0sgr9c9n68r858vs77jyjx4lsq56sblbzmpqfn60mx1s7c3y5dnq") (f (quote (("non_standard_intl") ("legacy")))) (r "1.56.1")))

(define-public crate-uievents-code-0.1.1 (c (n "uievents-code") (v "0.1.1") (h "0arahqfaf4bzx3b2v0mhzw90qjbkqc82jsf2ibkwf3nmbz29cd2l") (f (quote (("non_standard_intl") ("legacy") ("enum")))) (r "1.56.1")))

(define-public crate-uievents-code-0.1.2 (c (n "uievents-code") (v "0.1.2") (d (list (d (n "strum") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (o #t) (d #t) (k 0)))) (h "1iwf9pd6lzp5c3d2x9z3n8fvnxarjdmrq8p86n4bbckhgnkp1rd8") (f (quote (("non_standard_intl") ("legacy") ("enum" "strum_macros" "strum")))) (r "1.56.1")))

