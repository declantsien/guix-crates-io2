(define-module (crates-io ui #{4-}# ui4-macros) #:use-module (crates-io))

(define-public crate-ui4-macros-0.1.0 (c (n "ui4-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15s78zjrcvl7swhvn6s0v7xzckls6djq9mk4qs2636xanv5w4kk9")))

