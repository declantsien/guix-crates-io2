(define-module (crates-io ui -s ui-sys) #:use-module (crates-io))

(define-public crate-ui-sys-0.1.0 (c (n "ui-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "095kmc2nybafkdzpqyqn21lj28dgh1pbm9ccdfx9gb898m1xij06")))

(define-public crate-ui-sys-0.1.1 (c (n "ui-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0p649yylkaxcpc9nik2hyi1kgylrll906b8csp23lzpn05rj7z5k")))

(define-public crate-ui-sys-0.1.3 (c (n "ui-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xf76ydsscbqidqclcnn87w9hdc98wcb4sfgi3wnz47f8qiz9wby") (f (quote (("fetch") ("default" "fetch" "build") ("build")))) (l "ui")))

