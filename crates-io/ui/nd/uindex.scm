(define-module (crates-io ui nd uindex) #:use-module (crates-io))

(define-public crate-uindex-0.1.0 (c (n "uindex") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "069mggwr5c919q9p6wzw9qgxyrcvqk5nkaiaalvslx4xzgyqsq12")))

(define-public crate-uindex-0.1.1 (c (n "uindex") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13hrn7kj3ydh7lj3hhqax4b3x1x8ixxxhlk8p1ywkk0sxbb2krh6")))

