(define-module (crates-io ui l_ uil_shared) #:use-module (crates-io))

(define-public crate-uil_shared-0.0.2 (c (n "uil_shared") (v "0.0.2") (h "0a8sj8dhsp0pqy0yr6khrzgg54zjr52jxlcz082j3sh1kw353zl5")))

(define-public crate-uil_shared-0.0.3 (c (n "uil_shared") (v "0.0.3") (h "12jhhljcmv4icnmwwp7im4j24ggkjidnv4ysmp216h2jhfvcqsq0")))

