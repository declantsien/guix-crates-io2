(define-module (crates-io ui l_ uil_parsers) #:use-module (crates-io))

(define-public crate-uil_parsers-0.0.2 (c (n "uil_parsers") (v "0.0.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "phf") (r "*") (d #t) (k 0)) (d (n "phf_macros") (r "*") (d #t) (k 0)) (d (n "uil_shared") (r "= 0.0.2") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "0jphvan8vrnvx6f07fkx9zszydyfc9nmm2fal8kvax5qvdh0zsb5")))

(define-public crate-uil_parsers-0.0.3 (c (n "uil_parsers") (v "0.0.3") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "phf") (r "*") (d #t) (k 0)) (d (n "phf_macros") (r "*") (d #t) (k 0)) (d (n "uil_shared") (r "= 0.0.3") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "0rcv8cqld6h6n7cpyyyhpy7i67wf6an9dwls0nddafmgswm9lkya")))

