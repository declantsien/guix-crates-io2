(define-module (crates-io ui au uiautomation_derive) #:use-module (crates-io))

(define-public crate-uiautomation_derive-0.0.1 (c (n "uiautomation_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "0kf1jya68awj29ykg0g802lm8ib6kwckxp5z1j471abmn2ai7r38")))

(define-public crate-uiautomation_derive-0.0.2 (c (n "uiautomation_derive") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1gwj4lgqlqfhgc20hkk5h3bzn6f5rhw0w7qk51mg0dxbj8hss75q")))

(define-public crate-uiautomation_derive-0.0.3 (c (n "uiautomation_derive") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (d #t) (k 0)))) (h "1agm06nr9fxfm796pxkik5p7srrl259cp8llnji8x1sy02q88v1y")))

(define-public crate-uiautomation_derive-0.0.4 (c (n "uiautomation_derive") (v "0.0.4") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1ng384z4cw942ymd42y5sadyvd30i82hl2dqx9hbljjp7z51095b")))

(define-public crate-uiautomation_derive-0.0.5 (c (n "uiautomation_derive") (v "0.0.5") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1lqlp3nv150d1gyrlj32rks01m2d2vl0h03s4kx3yf4yskdv94zp")))

(define-public crate-uiautomation_derive-0.0.6 (c (n "uiautomation_derive") (v "0.0.6") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0awdvzmqic2llcjy8qqxj1acs6x8fq8p4apfzxv7hcisp3hsjhy5")))

(define-public crate-uiautomation_derive-0.0.7 (c (n "uiautomation_derive") (v "0.0.7") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0xg9hb3vbjihq9kbi9szc5j7clp7ywl6ixfr1fxfrx5h6wmdm6ag")))

(define-public crate-uiautomation_derive-0.0.8 (c (n "uiautomation_derive") (v "0.0.8") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "000rq10j62qv5qn06n2ns50dwanqi2wa4c6af78xij5cq6dk7s07")))

(define-public crate-uiautomation_derive-0.0.9 (c (n "uiautomation_derive") (v "0.0.9") (d (list (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "03wsq4arj0gs6qz61hb69qaxalbpkwzw3iswb57flmf4bvmm3v0q")))

(define-public crate-uiautomation_derive-0.1.0 (c (n "uiautomation_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0l0ifsz55ygfplyzxk5pzm5a83bv8npbrvcw9rhz51xrdpffrxl5")))

(define-public crate-uiautomation_derive-0.1.1 (c (n "uiautomation_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1nb5myc44cancn4ird365id3yy85zsmy5764742xdnvw27gghnhg")))

(define-public crate-uiautomation_derive-0.1.2 (c (n "uiautomation_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "05n5byszc50s5938pwvgafz8ihq7l6r57isj2s5di81b4132zfmv")))

(define-public crate-uiautomation_derive-0.1.3 (c (n "uiautomation_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0qd2jj3549aak9ky4f677nf1w887jvhsj99brkadvfb5ybrja50l")))

(define-public crate-uiautomation_derive-0.1.4 (c (n "uiautomation_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1dc7cvw56j2z291bnf70qpg6sydixh8l7a7afl6xk23wlbpp3lmg")))

(define-public crate-uiautomation_derive-0.1.5 (c (n "uiautomation_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "03c9h7yxm1ckd5wgh4x9jqrps1fjw0055zpqj64d1rzc33nl0mwy")))

(define-public crate-uiautomation_derive-0.1.6 (c (n "uiautomation_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2wdll0h62mmhq3hjjm2sa6706ggb2v8ir7dvjja65yp5xbayrk")))

(define-public crate-uiautomation_derive-0.2.0 (c (n "uiautomation_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "01qb4wlfhxd0h9c9s6swfglqgaz1fhpl00zy7pj8yj4l14kvmrg3")))

(define-public crate-uiautomation_derive-0.2.1 (c (n "uiautomation_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)))) (h "19gxn7zrq8fmk7rrl2xxfm8457sm6a8qcnvc9j37xz0gsg82lrli")))

(define-public crate-uiautomation_derive-0.2.2 (c (n "uiautomation_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0rqljgz21kwmhjadcj3cn4jdh61fl1v5j9nm2nbsik9f1dbrxyxm")))

(define-public crate-uiautomation_derive-0.2.3 (c (n "uiautomation_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncdpcahi3c93vksah47ks6vgyjcnfd3j7amyb5h34b7zjmj7n85")))

(define-public crate-uiautomation_derive-0.2.4 (c (n "uiautomation_derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0ip0k77ai7w5xgplrkkp4qr45b4w22jpxdw5jfkkhg86mn6dd66w")))

(define-public crate-uiautomation_derive-0.2.5 (c (n "uiautomation_derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1qijbfwa4g64wfxwl3j891gzqvzcggnhkzrdmpk9xz2zdjalv3kz")))

(define-public crate-uiautomation_derive-0.2.6 (c (n "uiautomation_derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "18g0y88nx8ghz0khsc6s9l8qg737h6zfynpi4aj448xpmchv38fj")))

(define-public crate-uiautomation_derive-0.2.7 (c (n "uiautomation_derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.57") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0qnmnh75fpxvjw0p50hd4xjr20vq23bb1b67aplnmxrpgm9pdn37")))

(define-public crate-uiautomation_derive-0.2.8 (c (n "uiautomation_derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "19jqrlza09ws6z0v3p95dnzb7wih4zzl8ff3z9lgw3p6wrsfi6f8")))

(define-public crate-uiautomation_derive-0.2.9 (c (n "uiautomation_derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1fhf87dlqppgwcfh2d771cx13lhg2k78hx33714sw34z6hx7bmmq")))

(define-public crate-uiautomation_derive-0.2.10 (c (n "uiautomation_derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0qiv069bsl8xgql69bg64r0r9082h3198c8690j7h31smml14alg")))

(define-public crate-uiautomation_derive-0.2.11 (c (n "uiautomation_derive") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "02lx3p5rq9rjk1ry1vx3d3v8fikc93hpfqsfnhawnnvml1wldwls")))

(define-public crate-uiautomation_derive-0.2.12 (c (n "uiautomation_derive") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1nlmlcqn3z7irfjg1fcf3c4q5dkhgij5gbh223yf1ndwqwhg7lxi")))

(define-public crate-uiautomation_derive-0.2.13 (c (n "uiautomation_derive") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full"))) (d #t) (k 0)))) (h "0i1fjxm6b7gcwl28ppj8f3c6jdq0vkgbpflw3qmlb7mvsp7fbxy8")))

(define-public crate-uiautomation_derive-0.2.14 (c (n "uiautomation_derive") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0h62n589mfsv6nx4zkf3lqzy2prdlm88xhgx0pfapqsz62j6mizx")))

(define-public crate-uiautomation_derive-0.2.15 (c (n "uiautomation_derive") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5xrb1gr500k2592f6l0pmi9rh2ijzvvrdgp9jph2sfdznrkh6r")))

(define-public crate-uiautomation_derive-0.2.16 (c (n "uiautomation_derive") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0csgsrnvxlwpl39dm1rp8a3cc47h7ii08gyy6dqdqw2l44wr5wp4")))

(define-public crate-uiautomation_derive-0.2.17 (c (n "uiautomation_derive") (v "0.2.17") (d (list (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "0i8qmfxzdw0kh1f8cc82l898czrbp39sjz6bmh01mvm57qxvf619")))

(define-public crate-uiautomation_derive-0.2.18 (c (n "uiautomation_derive") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1k0mpnrb7b7ssjw1fmin9vfc2imm7ai8j14dsd42r5fv7njr464q")))

(define-public crate-uiautomation_derive-0.2.19 (c (n "uiautomation_derive") (v "0.2.19") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "0zyblnn7yk2bn0d6ysl1cb7qnrk62zglbli09jpbnj5cdfm5kpms")))

(define-public crate-uiautomation_derive-0.2.20 (c (n "uiautomation_derive") (v "0.2.20") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0paivf92zcx9r6knvrgk8mbllbnm8djkfdpfz73944nbdzs1kxli")))

(define-public crate-uiautomation_derive-0.2.21 (c (n "uiautomation_derive") (v "0.2.21") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0bhikj07qkx6lwawm0a7h1bhakrnc68y85b9ahv7mlna6c9jbzfa")))

(define-public crate-uiautomation_derive-0.2.22 (c (n "uiautomation_derive") (v "0.2.22") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "18snp9v7mvz29p4nrwn5gvqnmkfhnpmm8p0blzn00vvf684rndl1")))

(define-public crate-uiautomation_derive-0.2.23 (c (n "uiautomation_derive") (v "0.2.23") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1pnas1rhlfp41kvzjjnhvf6qznh12b694bkly88l7fjwzq41i0ri")))

(define-public crate-uiautomation_derive-0.2.24 (c (n "uiautomation_derive") (v "0.2.24") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1s3vb985a9g63yv6fq1xhl9dahksapqh50ckyq1hxam3i7bk26d8")))

(define-public crate-uiautomation_derive-0.3.0 (c (n "uiautomation_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0a7mz92xiylzwvggkqywah4csk9y46jbsqmcilhl8wcbnkjgrnqs")))

(define-public crate-uiautomation_derive-0.3.1 (c (n "uiautomation_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("full"))) (d #t) (k 0)))) (h "15zfhzgl7qjnv8ppww5a6rqmhm4vfiixkl3bf8p0kdbyvb3fzqyw")))

