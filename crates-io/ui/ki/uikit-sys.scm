(define-module (crates-io ui ki uikit-sys) #:use-module (crates-io))

(define-public crate-uikit-sys-0.0.1 (c (n "uikit-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54.1") (k 1)) (d (n "block") (r "^0.1.6") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "color-backtrace") (r "^0.2.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "objc") (r "^0.2.7") (d #t) (t "cfg(target_os = \"ios\")") (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 2)) (d (n "winit") (r "^0.21.0") (d #t) (k 2)))) (h "1j3wxsvan9vlgda686bpcyrvrn2wddngnyx1k41x80rqv84y26hz")))

