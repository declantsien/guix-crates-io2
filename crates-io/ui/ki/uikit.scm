(define-module (crates-io ui ki uikit) #:use-module (crates-io))

(define-public crate-uikit-0.0.1 (c (n "uikit") (v "0.0.1") (d (list (d (n "core-graphics") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.0") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)))) (h "01cb3qvq5nc2wf1qb6dy38m1iwb8shxssn1slx6rxvssh9bybrak")))

(define-public crate-uikit-0.0.2 (c (n "uikit") (v "0.0.2") (d (list (d (n "core-graphics") (r "^0.2") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc-foundation") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (k 0)))) (h "0qisp0rz9982wl8fr51pb8j55lkqi2dbvaa21h9a2qswgshxiznd")))

