(define-module (crates-io ui an uianimator) #:use-module (crates-io))

(define-public crate-uianimator-0.1.0 (c (n "uianimator") (v "0.1.0") (h "0qh00izhjqsyaknfgych12aaqj7p8f4k6i5f2ywmlffgb8gnfffy")))

(define-public crate-uianimator-0.1.1 (c (n "uianimator") (v "0.1.1") (h "0x6ygql57i99p80a08s6dx62m6v8ry2bnm2vjy5x3jy14g9y8gx2")))

