(define-module (crates-io ui r- uir-core) #:use-module (crates-io))

(define-public crate-uir-core-0.0.0 (c (n "uir-core") (v "0.0.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "intertrait") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0w3dy5rigqawpl8kllb5mlbw1i8dzpsnwjw3gg11gz7zrld74crs") (f (quote (("default"))))))

