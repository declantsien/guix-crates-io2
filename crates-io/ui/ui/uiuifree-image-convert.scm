(define-module (crates-io ui ui uiuifree-image-convert) #:use-module (crates-io))

(define-public crate-uiuifree-image-convert-0.1.1 (c (n "uiuifree-image-convert") (v "0.1.1") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)) (d (n "webp") (r "^0.2.2") (d #t) (k 0)))) (h "1hp934wyhyy4bwvwdsvin20k7cww105p3r1c03vqjln4bv77spmb")))

(define-public crate-uiuifree-image-convert-0.1.2 (c (n "uiuifree-image-convert") (v "0.1.2") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)) (d (n "webp") (r "^0.2.2") (d #t) (k 0)))) (h "160hyskggna7zyvmy5jw8x0hf122ikmrs5r48v7n6s14qhz68zjs")))

(define-public crate-uiuifree-image-convert-0.1.3 (c (n "uiuifree-image-convert") (v "0.1.3") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)) (d (n "webp") (r "^0.2.2") (d #t) (k 0)))) (h "00i4ljbsb9kndbray52wmiqmswi90v6v996c4cx3vp5lxwy9lykh")))

(define-public crate-uiuifree-image-convert-0.1.4 (c (n "uiuifree-image-convert") (v "0.1.4") (d (list (d (n "image") (r "^0.24.1") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)) (d (n "webp") (r "^0.2.2") (d #t) (k 0)))) (h "1nydi32hyqs7sds2w7111dkfbyvzb0q1y72d7f61v08c96aha942")))

