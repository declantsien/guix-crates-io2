(define-module (crates-io ui ui uiuifree-normalize) #:use-module (crates-io))

(define-public crate-uiuifree-normalize-0.1.0 (c (n "uiuifree-normalize") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)) (d (n "voca_rs") (r "^1.14") (d #t) (k 0)))) (h "10p8202v2y4ypqnckjbrsvb9dfp91f50y6myqa65r9v5zga2fynr")))

(define-public crate-uiuifree-normalize-0.1.1 (c (n "uiuifree-normalize") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)) (d (n "voca_rs") (r "^1.14") (d #t) (k 0)))) (h "06dhgfkwl21ppirkpnv48fbch0acmipvcym69k6c2xqlak8cvm7k")))

