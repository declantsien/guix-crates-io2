(define-module (crates-io ui ui uiuifree-dictionary) #:use-module (crates-io))

(define-public crate-uiuifree-dictionary-0.1.0 (c (n "uiuifree-dictionary") (v "0.1.0") (d (list (d (n "mecab") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)))) (h "1r3lan9v5j120hz464y0c6fka745m5d75c3r1a0099vnbw3nhys9")))

(define-public crate-uiuifree-dictionary-0.1.1 (c (n "uiuifree-dictionary") (v "0.1.1") (d (list (d (n "mecab") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)))) (h "0an3whwysd0bbnhxyrl34pw18v317dhjc0y6p82caq8mblazvqy1")))

(define-public crate-uiuifree-dictionary-0.1.2 (c (n "uiuifree-dictionary") (v "0.1.2") (d (list (d (n "mecab") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)))) (h "121jmji5kzbpvy42613f2791b8sc2j08qflfvr86jp1830bx5hrv")))

(define-public crate-uiuifree-dictionary-0.1.3 (c (n "uiuifree-dictionary") (v "0.1.3") (d (list (d (n "mecab") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4") (d #t) (k 0)))) (h "0i76pwwqc442i8gw4xj0f9axxgczs20hqv9x0isziwxv1whzg7dh") (f (quote (("dic-mecab" "mecab") ("default"))))))

