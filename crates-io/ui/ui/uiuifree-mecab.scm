(define-module (crates-io ui ui uiuifree-mecab) #:use-module (crates-io))

(define-public crate-uiuifree-mecab-0.1.0 (c (n "uiuifree-mecab") (v "0.1.0") (d (list (d (n "mecab") (r "^0.1") (d #t) (k 0)))) (h "0754xhrrjdy7zxp475w02iil3k8nyxh4ml2y1whsx1kr2j7yaba6")))

(define-public crate-uiuifree-mecab-0.1.1 (c (n "uiuifree-mecab") (v "0.1.1") (d (list (d (n "mecab") (r "^0.1") (d #t) (k 0)))) (h "0823i2w0953jii0lpi5k2kybgg2rwaibk6l31bfl9arlv9vzxf9k")))

