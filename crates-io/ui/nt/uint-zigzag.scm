(define-module (crates-io ui nt uint-zigzag) #:use-module (crates-io))

(define-public crate-uint-zigzag-0.1.0 (c (n "uint-zigzag") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1pbd6f89p46w5gkdik22dbw5hh32vlqqlqhq0nglraivj29jpsf3") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-uint-zigzag-0.2.0 (c (n "uint-zigzag") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "17yy65s9hkvvrrvwszp9n63mh0dc9hd0lfd5gnim2a3bq8ys7yk1") (f (quote (("std") ("default") ("alloc"))))))

(define-public crate-uint-zigzag-0.2.1 (c (n "uint-zigzag") (v "0.2.1") (d (list (d (n "core2") (r "^0.4") (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "05gc728wmzjhrkv2ps6wjfd3vgkrb0v8q4q7pal8bf2wssp7ggxb") (f (quote (("std" "core2/std") ("default") ("alloc" "core2/alloc"))))))

