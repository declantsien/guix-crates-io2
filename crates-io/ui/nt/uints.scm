(define-module (crates-io ui nt uints) #:use-module (crates-io))

(define-public crate-uints-0.1.0 (c (n "uints") (v "0.1.0") (h "1s5k27anlf9y50i7j73qjwpmcmq9fg4cx10dapdxb9mgspik2w9l") (y #t)))

(define-public crate-uints-0.1.1 (c (n "uints") (v "0.1.1") (h "1wl2kfd2q45n63g6mn0hjnnpfkwq4hlzjdnp3xss0r42sb7jigla")))

(define-public crate-uints-0.2.0 (c (n "uints") (v "0.2.0") (h "1mg4iaih54v1yhnashbn38n2a9bm307s8pg2m3ip3mvqv58kyj7i")))

(define-public crate-uints-0.3.0 (c (n "uints") (v "0.3.0") (h "0893cs0wcqg5zyrgnfidpdhl6njifpxffdr2gl7nwxzlyx5sxjgc")))

(define-public crate-uints-0.4.0 (c (n "uints") (v "0.4.0") (h "1lzdfpmxjxi8jai746pid7vglzvb7xdjrp35z3gi84zz4nnn1fzc")))

(define-public crate-uints-0.5.0 (c (n "uints") (v "0.5.0") (d (list (d (n "fixed-array") (r "^0.1.0") (d #t) (k 0)))) (h "0xlg23a6hdy1ja49776517rgcshiam8qny1c7x69f25kmca27pir")))

(define-public crate-uints-0.5.1 (c (n "uints") (v "0.5.1") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)))) (h "05ml8biy2990bwpmxksvw66pwbllbpfag7kvrqw18n5d0hpjxizw")))

(define-public crate-uints-0.6.0 (c (n "uints") (v "0.6.0") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)))) (h "1w63lxb8qfypyzg0rx7civky5wkkfr1bjiryncsn9jw2qg6cachz")))

(define-public crate-uints-0.6.1 (c (n "uints") (v "0.6.1") (d (list (d (n "fixed-array") (r "^0.1.1") (d #t) (k 0)))) (h "01gy9sa3yl4nz0ycajl5m3bw6ld5v3gvrbq85fx22l1kkqz2s21m")))

(define-public crate-uints-0.7.0 (c (n "uints") (v "0.7.0") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)))) (h "05as8wb2c4sb0lgfjnlkfiip01c3aq86hp23fv2l46mlsdl4jw20")))

(define-public crate-uints-0.8.0 (c (n "uints") (v "0.8.0") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)))) (h "18a6p2qzi04fq7zkddfydg5xcc8clbxabgsca38l351b40jzbzzi")))

(define-public crate-uints-0.8.1 (c (n "uints") (v "0.8.1") (d (list (d (n "fixed-array") (r "^0.2.0") (d #t) (k 0)))) (h "003fh3qj3p4xh1v6z3v39d8zk72ijk39wign71qpskrzy2drv8r2")))

(define-public crate-uints-0.9.0 (c (n "uints") (v "0.9.0") (d (list (d (n "fixed-array") (r "^0.3.0") (d #t) (k 0)))) (h "0cl2cp8zdqpr5mdhwa00k1k6d0i2x1gads1ym2mnkn6h67pm2gcs")))

(define-public crate-uints-0.10.0 (c (n "uints") (v "0.10.0") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)))) (h "14ng6wyqbn47k63in0gg0hva6xsbghg4bwdp2jidm5fj3cflqwrv")))

(define-public crate-uints-0.11.0 (c (n "uints") (v "0.11.0") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)))) (h "1rv1mv2wabv3lvg99jvrba8iirviiwsa5sq8fjhg2iyxvjm95xnp")))

(define-public crate-uints-0.11.1 (c (n "uints") (v "0.11.1") (d (list (d (n "fixed-array") (r "^0.4.0") (d #t) (k 0)))) (h "0gxiqa60n4k6k6a7iczwb219hyisr63jvpx4r43jf8cd60w8n46n")))

(define-public crate-uints-0.11.2 (c (n "uints") (v "0.11.2") (d (list (d (n "fixed-array") (r "^0.4.1") (d #t) (k 0)))) (h "13j1hmw87yvksqr63j6p6pnp1b1633srds08ffnxhn16flwm6gca")))

