(define-module (crates-io ok sp okspiel) #:use-module (crates-io))

(define-public crate-okspiel-0.4.0 (c (n "okspiel") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "copypasta") (r "^0.7") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "iced") (r "^0.3") (f (quote ("tokio" "qr_code" "svg"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.34") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gx21c0hg440sckcg86flpxkpw304da0ic7dj57hcbkc10xpdg7h")))

