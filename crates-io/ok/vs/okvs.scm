(define-module (crates-io ok vs okvs) #:use-module (crates-io))

(define-public crate-okvs-0.1.0 (c (n "okvs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (d #t) (k 0)))) (h "1qgh2n8gvnxla8fqbp8wr53y4xxychsw41japr0gg9lhfwa2nf0q")))

(define-public crate-okvs-0.1.1 (c (n "okvs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (d #t) (k 0)))) (h "1483x0ff2khjizlsz8gd31vw03pc98h3x427fjs2kfawgmw78z0z")))

(define-public crate-okvs-0.2.0 (c (n "okvs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "xxh3") (r "^0.1") (d #t) (k 0)))) (h "04khlv1qjjd9ry1sskydjb4m5pdljafhbjyam333sirql6vs9dlv")))

