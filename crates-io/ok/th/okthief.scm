(define-module (crates-io ok th okthief) #:use-module (crates-io))

(define-public crate-okthief-0.1.0 (c (n "okthief") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ok-picker") (r "^0.0.3") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "00f8j8sz9fclqidkygdl884fvcxsmazdqybfi4gqzhhdfkzi0a2i")))

