(define-module (crates-io ok iz okizeme_input) #:use-module (crates-io))

(define-public crate-okizeme_input-0.1.0 (c (n "okizeme_input") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "okizeme_types") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "1sxcqqyj4wixbfavk9lxinf92308sl7wcg2vy2rsdnijnmx97b4l")))

