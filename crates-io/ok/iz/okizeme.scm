(define-module (crates-io ok iz okizeme) #:use-module (crates-io))

(define-public crate-okizeme-0.1.0 (c (n "okizeme") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "okizeme_core") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m0gfmzkmsm6lf9xb0wzchgbp520sh9lh1wj2v9l58mbhkg3v7fa")))

