(define-module (crates-io ok iz okizeme_core) #:use-module (crates-io))

(define-public crate-okizeme_core-0.1.0 (c (n "okizeme_core") (v "0.1.0") (d (list (d (n "okizeme_animation") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_defense") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_input") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_offense") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_player") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_types") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1017pgl822np17zlrpycw1ja9wwbjayhcy5jfl17qzc03azk4j6v")))

