(define-module (crates-io ok iz okizeme_offense) #:use-module (crates-io))

(define-public crate-okizeme_offense-0.1.0 (c (n "okizeme_offense") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "okizeme_defense") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_types") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ysfcyswchp1bbpnx9hvj129gj23zs89f17jf32lwl7mn27ffv56")))

