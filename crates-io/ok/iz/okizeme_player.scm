(define-module (crates-io ok iz okizeme_player) #:use-module (crates-io))

(define-public crate-okizeme_player-0.1.0 (c (n "okizeme_player") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7") (d #t) (k 0)) (d (n "okizeme_input") (r "^0.1.0") (d #t) (k 0)) (d (n "okizeme_types") (r "^0.1.0") (d #t) (k 0)))) (h "0h9kcixrmcfq5y1hh7m5040ygxgz9rnk7ifsgkjq1h538zq93rnm")))

