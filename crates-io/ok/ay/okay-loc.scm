(define-module (crates-io ok ay okay-loc) #:use-module (crates-io))

(define-public crate-okay-loc-0.1.0 (c (n "okay-loc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "gitignore") (r "^1.0.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "00xc86nhd0bhdxrchzv7kwqn5i6fl4iknv27n0hwwjwwkw8cx2ia")))

