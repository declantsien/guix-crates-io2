(define-module (crates-io ok ay okaywal) #:use-module (crates-io))

(define-public crate-okaywal-0.0.0-reserve.0 (c (n "okaywal") (v "0.0.0-reserve.0") (h "1av6c6c1fi1760nxmq6yqy0y6pfj37gbgslgrn3cfz8dd8vwjy7i") (y #t)))

(define-public crate-okaywal-0.1.0 (c (n "okaywal") (v "0.1.0") (d (list (d (n "crc32c") (r "^0.6.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "10h0vjnsby3w1gqbknvm2yz6804gvkaszr6q5kanwd5k1ra53yf9") (y #t) (r "1.58")))

(define-public crate-okaywal-0.2.0 (c (n "okaywal") (v "0.2.0") (d (list (d (n "crc32c") (r "^0.6.3") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "125jl0r9wka6qycm58xlibb0irf4bjbjbjqq4i40zhcmxkz7ic56") (y #t) (r "1.58")))

(define-public crate-okaywal-0.3.0 (c (n "okaywal") (v "0.3.0") (d (list (d (n "crc32c") (r "^0.6.3") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "0gdd5w6jzccm3xc9af9z5mvz3xx5z8pcphykjpkjnapj4fh1v3d9") (y #t) (r "1.58")))

(define-public crate-okaywal-0.3.1 (c (n "okaywal") (v "0.3.1") (d (list (d (n "crc32c") (r "^0.6.3") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "079ks36i7n0g2wbvxaqpi6c2i7c2ybnp0d79ig51zdld1cg3my2d") (r "1.58")))

