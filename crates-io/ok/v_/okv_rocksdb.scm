(define-module (crates-io ok v_ okv_rocksdb) #:use-module (crates-io))

(define-public crate-okv_rocksdb-0.3.0 (c (n "okv_rocksdb") (v "0.3.0") (d (list (d (n "inherent") (r "^1.0") (d #t) (k 0)) (d (n "okv_core") (r "^0.3.0") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (f (quote ("multi-threaded-cf"))) (k 0)) (d (n "self_cell") (r "^1.0") (d #t) (k 0)))) (h "0hdqvnxm8qfzdxhqar4mhag0z89pi83d0y179fgvx6bx4yjm5p7p") (f (quote (("multi_threaded" "rocksdb/multi-threaded-cf"))))))

