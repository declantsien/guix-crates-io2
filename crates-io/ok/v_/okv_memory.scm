(define-module (crates-io ok v_ okv_memory) #:use-module (crates-io))

(define-public crate-okv_memory-0.3.0 (c (n "okv_memory") (v "0.3.0") (d (list (d (n "dashmap") (r "^5.5") (d #t) (k 0)) (d (n "okv_core") (r "^0.3.0") (d #t) (k 0)) (d (n "self_cell") (r "^1.0") (d #t) (k 0)))) (h "0s122mc3xd8f7l2figyzglb6h0a4fm5m605gqbm86a2z1z0his2a") (f (quote (("multi_threaded"))))))

