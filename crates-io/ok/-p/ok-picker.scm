(define-module (crates-io ok -p ok-picker) #:use-module (crates-io))

(define-public crate-ok-picker-0.0.1 (c (n "ok-picker") (v "0.0.1") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)))) (h "03x6fkpx72snfj8c98ii10b88kd5mx6x354jwaij3dms4gibkw8w")))

(define-public crate-ok-picker-0.0.2 (c (n "ok-picker") (v "0.0.2") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0b7gcky67psxgax8wgq6fqg8mz9d38g9dqcl6m151v1j1xldc11v")))

(define-public crate-ok-picker-0.0.3 (c (n "ok-picker") (v "0.0.3") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "0dqrirfw03dkqdc03snf5gy1r6gxx55mc4f5502aply1rc5ywqja")))

