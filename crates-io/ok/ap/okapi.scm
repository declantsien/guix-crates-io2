(define-module (crates-io ok ap okapi) #:use-module (crates-io))

(define-public crate-okapi-0.1.0 (c (n "okapi") (v "0.1.0") (d (list (d (n "schemars") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cyy7jg0vn1wcrzz1fnwlxa4da05n8ip1syvr40ckbav4yxy0zh7") (f (quote (("derive_json_schema"))))))

(define-public crate-okapi-0.1.2 (c (n "okapi") (v "0.1.2") (d (list (d (n "schemars") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kh19w9bl98jjfs6a7vrh9l361wcxi0y514ya5whkpwnqw8yna89") (f (quote (("derive_json_schema"))))))

(define-public crate-okapi-0.2.0 (c (n "okapi") (v "0.2.0") (d (list (d (n "schemars") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n1z30q6l4zc7qw7w64i48pxjgjy7gs7sy7zxk2cixlzhd7mp5y8") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi-0.3.0 (c (n "okapi") (v "0.3.0") (d (list (d (n "schemars") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v7r61pzbvha5xcs0jf6f7vwcvz3c6gajvz6f4fb5dvjb70fhqka") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi-0.4.0 (c (n "okapi") (v "0.4.0") (d (list (d (n "schemars") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "149qv4ny9y6wpzsazn2v8isdgkzl0gi6nx3fhdhqf5bcr19sqsg9") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi-0.5.0-alpha-1 (c (n "okapi") (v "0.5.0-alpha-1") (d (list (d (n "schemars") (r "^0.8.0-alpha-4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07hapsl5lbpl1yd185yzmx7j44rdb283mi9pccw1ayqhnzk1gixh") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi-0.6.0-alpha-1 (c (n "okapi") (v "0.6.0-alpha-1") (d (list (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x5wqwvdasnwigx435ldi4l3wl39irrv9z8gpyfmpmzqv805w25v") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi-0.7.0-rc.1 (c (n "okapi") (v "0.7.0-rc.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14r8dsgs1x5a1jkiryip7hd1l2v3vgyj709pbjiq1604dqvbcrnf") (f (quote (("preserve_order" "schemars/preserve_order") ("impl_json_schema" "schemars/impl_json_schema"))))))

(define-public crate-okapi-0.7.0 (c (n "okapi") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1if340zcn1g00i0f0hkcx00d45z8rq0pcvv9hx74frdhg8yqar4s") (f (quote (("preserve_order" "schemars/preserve_order") ("impl_json_schema" "schemars/impl_json_schema"))))))

