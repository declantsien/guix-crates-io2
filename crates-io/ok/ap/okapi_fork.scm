(define-module (crates-io ok ap okapi_fork) #:use-module (crates-io))

(define-public crate-okapi_fork-0.6.0-alpha-1 (c (n "okapi_fork") (v "0.6.0-alpha-1") (d (list (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ssi0q4xc3jajsil75anyl5plgzfnqwwfy4nhb47b9gdkg03mwr6") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

(define-public crate-okapi_fork-0.6.1 (c (n "okapi_fork") (v "0.6.1") (d (list (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cvagl4fvrjpbmv5h7ppqw89ivw2hax42x2w26qcrq5czl4ras7y") (f (quote (("derive_json_schema" "schemars/derive_json_schema"))))))

