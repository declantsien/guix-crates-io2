(define-module (crates-io ok of okofdb) #:use-module (crates-io))

(define-public crate-okofdb-0.1.0 (c (n "okofdb") (v "0.1.0") (d (list (d (n "snap") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)))) (h "023fbnbkdf3yjc8blgb6i69kg9qll7vfv9klj8hnxibmqsk9n0hg")))

(define-public crate-okofdb-0.1.1 (c (n "okofdb") (v "0.1.1") (d (list (d (n "snap") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)))) (h "0bj26sryqzznzmzcp7xh48b3b3xb021q2bq5ml55n0v77v55xw1y")))

(define-public crate-okofdb-0.1.2 (c (n "okofdb") (v "0.1.2") (d (list (d (n "snap") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)))) (h "1ixdsishpdy4whjrvkwb4j8zl1zp0qcl58fxicd7c684a5gc4j5d")))

(define-public crate-okofdb-0.1.3 (c (n "okofdb") (v "0.1.3") (d (list (d (n "snap") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)))) (h "1b8j8mlskai5s22bk20ncigcy11cs7p9v007a0xvn3d5wlvdlkwc")))

