(define-module (crates-io ok la oklab) #:use-module (crates-io))

(define-public crate-oklab-1.0.0 (c (n "oklab") (v "1.0.0") (d (list (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "04cz3dy3jh2c8zhixws1b2hnq1xmc9w71qqrj2qvl4qdlnnl0zj6")))

(define-public crate-oklab-1.0.1 (c (n "oklab") (v "1.0.1") (d (list (d (n "rgb") (r "^0.8.36") (d #t) (k 0)))) (h "0hpcha8knid3qqr08049y2r5m7f39f9fkk09lhq03w57akgdb1iy")))

