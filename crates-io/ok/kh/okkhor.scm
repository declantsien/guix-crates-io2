(define-module (crates-io ok kh okkhor) #:use-module (crates-io))

(define-public crate-okkhor-0.4.0 (c (n "okkhor") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rupantor") (r "^0.3") (d #t) (k 2)))) (h "1a537w0h670ydrhngkqa6d4jnci4kjf9cbb1jvkq7ypq8l59acl4") (f (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.4.1 (c (n "okkhor") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rupantor") (r "^0.3") (d #t) (k 2)))) (h "1bnh0gsgpj19ivq8hlxsx0k2bdclvc95h9cdnd2h3cisdlnm3vys") (f (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5.0 (c (n "okkhor") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rupantor") (r "^0.3") (d #t) (k 2)))) (h "196z9jv1hjwwin5508ci74chl3ym2wpcam8ic7d9aaq70h591ldc") (f (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5.1 (c (n "okkhor") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rupantor") (r "^0.3") (d #t) (k 2)))) (h "10rikb96vv0g58v26v2jsjgn6mkrdmmc4jbhmf5v4byx93aa0vds") (f (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5.2 (c (n "okkhor") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rupantor") (r "^0.3") (d #t) (k 2)))) (h "0bqhbjs7ydchxqakspkrq8df0wcjpy2299a2i2z39yy9g0h4bvz6") (f (quote (("regex") ("editor"))))))

