(define-module (crates-io ck #{2j}# ck2json) #:use-module (crates-io))

(define-public crate-ck2json-0.1.0 (c (n "ck2json") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.17") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1nm064fwlrzi8yg77b7p3b7kbdr1k74q04mgjic4y3g6x3i6r5bk")))

