(define-module (crates-io ck me ckmeans) #:use-module (crates-io))

(define-public crate-ckmeans-0.1.0 (c (n "ckmeans") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fr58iasgll5q6pm5hmdfkib7cda5nm2cdmb9ydlm5cfiw4am3x1") (r "1.70")))

(define-public crate-ckmeans-0.1.1 (c (n "ckmeans") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0y2fr3vi5yg42y1n348kfrinb01i1pvvg2nqhsjmgmc929jx1b78") (r "1.70")))

(define-public crate-ckmeans-0.1.2 (c (n "ckmeans") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15hfan1yig8kgns38b2fbz25917zw8lx4zxfgmdphslv7m2r5d62") (r "1.70")))

(define-public crate-ckmeans-0.1.3 (c (n "ckmeans") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "13yidy6wg87jamksqdjd9hj8a7bdbnqgd2qnv4hv96bkkbxjlccl") (r "1.70")))

(define-public crate-ckmeans-0.2.0 (c (n "ckmeans") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vpk9p0s6sdzjf40n1jchzllmjcwgriwxlvhzfrwx66phz86km8h") (r "1.70")))

(define-public crate-ckmeans-0.2.1 (c (n "ckmeans") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0af277mc6va657z5bd9f88a1z18dv65hpwpgn2yj81hxl24mzn03") (r "1.70")))

(define-public crate-ckmeans-0.2.2 (c (n "ckmeans") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1cfwvbfnczf84ii1b14kyz5w7b7g13bkc42fs440j5a786rbpn09") (r "1.70")))

(define-public crate-ckmeans-0.2.3 (c (n "ckmeans") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0m2h3n5iihxzk7697kvffmzvkcqz9mbjc579srdxrb5w6mvgzdq4") (r "1.70")))

(define-public crate-ckmeans-0.2.4 (c (n "ckmeans") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "008a7v2z0k3997q7kdvhp6dc2c65w8iwv10w7h3cvd09q3d912vf") (r "1.70")))

(define-public crate-ckmeans-0.3.0 (c (n "ckmeans") (v "0.3.0") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1kwd3x305lqrmx99vqp6higka7mmdq7jv613x8fgh52z31b40n7p") (f (quote (("headers" "cbindgen")))) (r "1.70")))

(define-public crate-ckmeans-0.3.1 (c (n "ckmeans") (v "0.3.1") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jwpal5bd5mxays4yqld7z12bsk50hdxb8kyryvzcaf2kp138qmf") (f (quote (("headers" "cbindgen")))) (r "1.70")))

(define-public crate-ckmeans-0.3.2 (c (n "ckmeans") (v "0.3.2") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "0fbajrl80ys8hm3kgihibf3bdjw137k8drys99l7shl98zgzba4f") (f (quote (("headers" "cbindgen")))) (r "1.70")))

(define-public crate-ckmeans-0.3.3 (c (n "ckmeans") (v "0.3.3") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0sc6zx9nf0a8c0gcaq82h9zy57ijmg08jmz48r1c5bdhndakdmkc") (f (quote (("headers" "cbindgen")))) (r "1.70")))

(define-public crate-ckmeans-1.0.0 (c (n "ckmeans") (v "1.0.0") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1nd8argfghp6hz7q21g2f1lagh39552y6vmszd89zz9qr8gy08sa") (f (quote (("headers" "cbindgen")))) (r "1.70")))

(define-public crate-ckmeans-1.0.6 (c (n "ckmeans") (v "1.0.6") (d (list (d (n "cbindgen") (r "^0.24.5") (o #t) (k 1)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0p6hh4kz819958sxwnzwdap7v1m0ycq1gvm1mvnqrilk6cnm9d6q") (f (quote (("headers" "cbindgen")))) (r "1.70")))

