(define-module (crates-io ck b_ ckb_schemars_derive) #:use-module (crates-io))

(define-public crate-ckb_schemars_derive-0.8.15 (c (n "ckb_schemars_derive") (v "0.8.15") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1pmvpgs75k20vaakgbcnfdjwlfwwam9axs8pais6r89y636n13zh") (r "1.60")))

(define-public crate-ckb_schemars_derive-0.8.16 (c (n "ckb_schemars_derive") (v "0.8.16") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1j9l8rks8c5x560cwwprvr678pvwb1f0zlln5p7h6y899bh06h3v") (r "1.60")))

(define-public crate-ckb_schemars_derive-0.8.19 (c (n "ckb_schemars_derive") (v "0.8.19") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.26.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wwfyzdq85pws58f3yyjr1axjdwmzbfs20ngn4rrzpfvzas17j20") (r "1.60")))

