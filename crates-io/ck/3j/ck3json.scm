(define-module (crates-io ck #{3j}# ck3json) #:use-module (crates-io))

(define-public crate-ck3json-0.2.0 (c (n "ck3json") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "13lsywgkhvsmlxhpsfjahgfd52iva0amxxv2qpxssa0hjkv05fsx")))

(define-public crate-ck3json-0.3.0 (c (n "ck3json") (v "0.3.0") (d (list (d (n "ck3save") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1.6") (d #t) (k 0)) (d (n "pest") (r "^2.1.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0yxpckfvw4jga2240hwkiy9xkasa9mwbbd9r680zi4rnpvqkryb3")))

(define-public crate-ck3json-0.3.1 (c (n "ck3json") (v "0.3.1") (d (list (d (n "ck3save") (r ">=0.1.4, <0.2.0") (d #t) (k 0)) (d (n "clap") (r ">=2.33.3, <3.0.0") (d #t) (k 0)) (d (n "encoding_rs_io") (r ">=0.1.7, <0.2.0") (d #t) (k 0)) (d (n "pest") (r ">=2.1.3, <3.0.0") (d #t) (k 0)) (d (n "pest_derive") (r ">=2.1.0, <3.0.0") (d #t) (k 0)))) (h "1miq6xbkiq0kwyk4qwpcpjgdkz19ymqyx5jbz3a7880xxb0bvhr2")))

