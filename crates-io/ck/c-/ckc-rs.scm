(define-module (crates-io ck c- ckc-rs) #:use-module (crates-io))

(define-public crate-ckc-rs-0.1.1 (c (n "ckc-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (k 0)))) (h "0kl7sm3f82bfmzgmqmm2ilz5z1wynnfwhh8af7gkmak523zf3bvd")))

(define-public crate-ckc-rs-0.1.2 (c (n "ckc-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (k 0)))) (h "10m3hxxmmjlrqb0417najrimgqch52dd087vikdzwp1k616vz050")))

(define-public crate-ckc-rs-0.1.3 (c (n "ckc-rs") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sza4f2mkzbwv2bl1b0c3jsgav7qd5q4hz14lwzy6sn08m2rmzcq")))

(define-public crate-ckc-rs-0.1.4 (c (n "ckc-rs") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hr493bbr10q2qv1zhaxa3x8smh1na6lf1hhblaml935iw0hxg9k")))

(define-public crate-ckc-rs-0.1.5 (c (n "ckc-rs") (v "0.1.5") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "13q7zvjh1n2l1v93jl9f101bihj4jp3s4cys0h1vy89h6p7mp1qy")))

(define-public crate-ckc-rs-0.1.6 (c (n "ckc-rs") (v "0.1.6") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i6aklwmzv2n9c71gjgyb9p57iga1sp37dg9j10af2imipph80cg")))

(define-public crate-ckc-rs-0.1.7 (c (n "ckc-rs") (v "0.1.7") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "00f28gs4ic8nl26xv847m0h4bqy9gbvd7j64lk2yq7fj53003ssa")))

(define-public crate-ckc-rs-0.1.8 (c (n "ckc-rs") (v "0.1.8") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a02nva318bxqxk8njhsdsnargcj27f0a4r27f8waw7p2ifhyfz5")))

(define-public crate-ckc-rs-0.1.9 (c (n "ckc-rs") (v "0.1.9") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "122s63dy6a7pq06szry4fdayxazs2qgzm762qfmnsrjxmid1krfv")))

(define-public crate-ckc-rs-0.1.10 (c (n "ckc-rs") (v "0.1.10") (d (list (d (n "bitvec") (r "^1.0.0") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "16cvxj245dbs2ivr6gh26isqy7bgb7r91drl0y4gc51alcfdnvf7")))

(define-public crate-ckc-rs-0.1.11 (c (n "ckc-rs") (v "0.1.11") (d (list (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i2lm7mdsy6yah7n7h91p2s1iiq6kdk98h2wjyjpb0pz2sngh7vm")))

(define-public crate-ckc-rs-0.1.12 (c (n "ckc-rs") (v "0.1.12") (d (list (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gjw9hqw5iqyfp4yi48pj0ifiqvb9hqb78z3599lh3fwyb7v5lvi")))

(define-public crate-ckc-rs-0.1.13 (c (n "ckc-rs") (v "0.1.13") (d (list (d (n "cardpack") (r "^0.4.13") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0914ayhyr6c8rmh06p83629hk2hzjpwx1wg2fkjkz5bx8hymrr5f")))

(define-public crate-ckc-rs-0.1.14 (c (n "ckc-rs") (v "0.1.14") (d (list (d (n "cardpack") (r "^0.4.17") (d #t) (k 2)) (d (n "log") (r "^0.4.16") (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "02wpmx9a8xm2pcfrccp5iswdgzp8gi290rm0dd2ks8ml9dr0309p") (r "1.63")))

(define-public crate-ckc-rs-0.1.15 (c (n "ckc-rs") (v "0.1.15") (d (list (d (n "cardpack") (r "^0.5.1") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a1kr9dflnwzmgrrnfrxw7k3q93w47p48rjsd2sx2v3dxza9fqr3") (r "1.70")))

