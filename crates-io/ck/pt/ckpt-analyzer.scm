(define-module (crates-io ck pt ckpt-analyzer) #:use-module (crates-io))

(define-public crate-ckpt-analyzer-1.0.0 (c (n "ckpt-analyzer") (v "1.0.0") (d (list (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "18253w1c4k25p1mjf50z2d5j6qw9jc3n4lcf13xvzmkiyl84l824")))

(define-public crate-ckpt-analyzer-1.0.1 (c (n "ckpt-analyzer") (v "1.0.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "sha256") (r "^1.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0bqwqx22d935rg32phhbdwnq5lwws2ah3f8bscis2lj0d7mlqbgv")))

