(define-module (crates-io ck an ckanapi) #:use-module (crates-io))

(define-public crate-ckanapi-0.1.0 (c (n "ckanapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.10") (f (quote ("multipart" "blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1zg01g3yxdvwqhw3y5fi00bg0k85m13h0awlfgiqy3dyag8gd8r7")))

(define-public crate-ckanapi-0.1.1 (c (n "ckanapi") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("multipart" "blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "02fkm7vsp07iwmw5nym48058b4di86yairljnv1b7clmb778kr1p")))

(define-public crate-ckanapi-0.1.2 (c (n "ckanapi") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("multipart" "blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1ck3bbq3b5njvdd1xvrig6pv8czx7bx0j8wsg6sr1h01s48jz14k")))

