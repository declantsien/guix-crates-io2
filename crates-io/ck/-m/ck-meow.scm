(define-module (crates-io ck -m ck-meow) #:use-module (crates-io))

(define-public crate-ck-meow-0.1.0 (c (n "ck-meow") (v "0.1.0") (d (list (d (n "keccak") (r "^0.1.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "08qkv1acv7s2x949r2sj9srw6arhy12fiz8g4schc4f4iwqild90")))

