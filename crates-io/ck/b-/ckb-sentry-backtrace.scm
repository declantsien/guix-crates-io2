(define-module (crates-io ck b- ckb-sentry-backtrace) #:use-module (crates-io))

(define-public crate-ckb-sentry-backtrace-0.21.0 (c (n "ckb-sentry-backtrace") (v "0.21.0") (d (list (d (n "backtrace") (r "^0.3.44") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "0aja76mfgljx5vh6pb6g7p8jjhh5kqhs62lh23260p37g5w9m7yp")))

