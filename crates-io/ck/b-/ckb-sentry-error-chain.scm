(define-module (crates-io ck b- ckb-sentry-error-chain) #:use-module (crates-io))

(define-public crate-ckb-sentry-error-chain-0.21.0 (c (n "ckb-sentry-error-chain") (v "0.21.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-backtrace")) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "1q0whqs9ryzbc5xrxbsrphrhqpp6hkjmzim7npxfidvbiabgf9zv")))

