(define-module (crates-io ck b- ckb-vm-pprof-protos) #:use-module (crates-io))

(define-public crate-ckb-vm-pprof-protos-0.112.1 (c (n "ckb-vm-pprof-protos") (v "0.112.1") (d (list (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "1j8p3ymn2kh9ywkh4xlxm1r0lwx70slsh8z682yqxfkd2vsbfr9s")))

(define-public crate-ckb-vm-pprof-protos-0.113.0 (c (n "ckb-vm-pprof-protos") (v "0.113.0") (d (list (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "096igpay2qn5lsph6pcrxzc0li47nxbwx1pydj23aafpj1f0fa7a")))

(define-public crate-ckb-vm-pprof-protos-0.114.0 (c (n "ckb-vm-pprof-protos") (v "0.114.0") (d (list (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "0bf9mym6d0ald1g37pryyvfncwbvnd4izhiyic2ky3y1c56y3wcv")))

(define-public crate-ckb-vm-pprof-protos-0.115.0-rc2 (c (n "ckb-vm-pprof-protos") (v "0.115.0-rc2") (d (list (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "1s4kdgy7hwj03mivp54mviv5hq5slj7whi7jpnj2s1191x5hsavr")))

(define-public crate-ckb-vm-pprof-protos-0.116.1 (c (n "ckb-vm-pprof-protos") (v "0.116.1") (d (list (d (n "protobuf") (r "^2.25.1") (f (quote ("with-bytes" "with-serde"))) (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)))) (h "1050nym1acrax18kwvgj5gjhs49fwyy1ygs1rhgmwjchngzxz213")))

