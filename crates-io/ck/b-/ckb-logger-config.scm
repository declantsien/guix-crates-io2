(define-module (crates-io ck b- ckb-logger-config) #:use-module (crates-io))

(define-public crate-ckb-logger-config-0.37.0-pre (c (n "ckb-logger-config") (v "0.37.0-pre") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pzwlf4vnanl5hg7783vlssf5iq6wpr07qj031fwai024hx30fj2")))

(define-public crate-ckb-logger-config-0.37.0 (c (n "ckb-logger-config") (v "0.37.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "152v23vbcfkal8sbvnwmr658zdb0qjp1zdi8wsb3dzm9n1gh536m")))

(define-public crate-ckb-logger-config-0.38.0 (c (n "ckb-logger-config") (v "0.38.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a2vvdah6ylkiqayp7rbsypy7x7vakx9cw289b9m286gkn12w0b1")))

(define-public crate-ckb-logger-config-0.39.0 (c (n "ckb-logger-config") (v "0.39.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gpv78pw4p8dczxrx0480a81dij47k1a3m8asblzqf4yn9pc9g8a")))

(define-public crate-ckb-logger-config-0.39.1 (c (n "ckb-logger-config") (v "0.39.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ish6jg07lihjg9mwprhs9mzzfipmwz6swpppy1pnm1ms9fc7jl3")))

(define-public crate-ckb-logger-config-0.40.0 (c (n "ckb-logger-config") (v "0.40.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m88qryz8lw6xfl78rminmlqm635lyg6qmb9q6lr0rlsswkm7l47")))

(define-public crate-ckb-logger-config-0.42.0 (c (n "ckb-logger-config") (v "0.42.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rzp3dksnqr6nzs6ga3r38jcv7jlg9l9vdam3lcgkk2kjga08clh")))

(define-public crate-ckb-logger-config-0.43.0 (c (n "ckb-logger-config") (v "0.43.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gh7x2s2llna7b6s63p6spjkg19ji9pd82c8agg6sbks6wi9b264")))

(define-public crate-ckb-logger-config-0.100.0-rc2 (c (n "ckb-logger-config") (v "0.100.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zmhg3gpgvizgqrlf6b4bzsadxs2ndd50m6f73qln93ibp53vp17")))

(define-public crate-ckb-logger-config-0.43.2 (c (n "ckb-logger-config") (v "0.43.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0igfn9jn0s6swkkhi6svvyy0al634wnivkwhkl0lflnalipr6q2c")))

(define-public crate-ckb-logger-config-0.100.0-rc3 (c (n "ckb-logger-config") (v "0.100.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gc9dqxhy8fvj8aj20yyby10m2i08cw0wav3xmz64nvfy4yxfaas") (y #t)))

(define-public crate-ckb-logger-config-0.100.0-rc4 (c (n "ckb-logger-config") (v "0.100.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sa569qf5qyi9mm0jbzg5qn2mi23pyg8f2msfjv7nw90bnjhx5mc")))

(define-public crate-ckb-logger-config-0.100.0-rc5 (c (n "ckb-logger-config") (v "0.100.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gm3aglz6q221lx32x0gy0px8dpsinsi5lzamfv51xwp2qvm9c6x")))

(define-public crate-ckb-logger-config-0.100.0 (c (n "ckb-logger-config") (v "0.100.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wjgbaw13c4qny8b8gnzqya3sfla7b69fbjs86j60cflkpqm8qhh")))

(define-public crate-ckb-logger-config-0.101.0 (c (n "ckb-logger-config") (v "0.101.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fwn0ic77drijc0c4dj3hxg6ff5pv4mfm9sssmd7s11lp21fs0yj")))

(define-public crate-ckb-logger-config-0.101.1 (c (n "ckb-logger-config") (v "0.101.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mgywpv9yvd1by8hsx3a31gd91ngrw3v7g7aigabnmbn13ax50jj")))

(define-public crate-ckb-logger-config-0.101.2-testnet1 (c (n "ckb-logger-config") (v "0.101.2-testnet1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wgx2aqs3nzifhva6v6fh9bmlyka85phvrvy99rib18zia25l8b1")))

(define-public crate-ckb-logger-config-0.101.2 (c (n "ckb-logger-config") (v "0.101.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n36abrh26hyhvah4lw3zyl8kkxs29f6s97bn25q1ahyf52833nr")))

(define-public crate-ckb-logger-config-0.101.3 (c (n "ckb-logger-config") (v "0.101.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v33jzv8bm940kmiqyrbb7sm0xsnji4gb2m2i2rx0wq2vhi2vgvl")))

(define-public crate-ckb-logger-config-0.101.4 (c (n "ckb-logger-config") (v "0.101.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ikj1dw7pz8s5ij4wx1jqgfk8fga7mg82jiswpsbi3pa1yz67fzm")))

(define-public crate-ckb-logger-config-0.101.5 (c (n "ckb-logger-config") (v "0.101.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cqykbmaibzf1q7cqc1d3z5lhgfrpn66grmbfxd8cp8gzd3rjf3g") (y #t)))

(define-public crate-ckb-logger-config-0.101.6 (c (n "ckb-logger-config") (v "0.101.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18bajma6gpwhdwxrd6d9ajrrcr7jpy82g0fp6hc6cnqvzcvix6r2")))

(define-public crate-ckb-logger-config-0.101.7 (c (n "ckb-logger-config") (v "0.101.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17kajf0b1xrjaajj49gklx0z8dhbynp24pklqbis4sqfd57h8mn6")))

(define-public crate-ckb-logger-config-0.101.8 (c (n "ckb-logger-config") (v "0.101.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k9jx3w604lkxgl393vgxc7yr56fvmshblklc9qq8yjzajly5b25")))

(define-public crate-ckb-logger-config-0.102.0 (c (n "ckb-logger-config") (v "0.102.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ha22bm8z3y9xswbhilrm8ldd6dj9fii6hcbj04cs8lfzl1zxwj7") (y #t)))

(define-public crate-ckb-logger-config-0.103.0 (c (n "ckb-logger-config") (v "0.103.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dj91vahmngdzxm18rn0vs8gz75ckwrj77cqj999fzh7mjdg3dpd")))

(define-public crate-ckb-logger-config-0.104.0 (c (n "ckb-logger-config") (v "0.104.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cxc06jn80wcg66xj3xnw1mh35vcf5lkc09vlnaiwgpkiwjlkz9h")))

(define-public crate-ckb-logger-config-0.104.1 (c (n "ckb-logger-config") (v "0.104.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ssvjdk06j3f66d6wgp1q030p86s8p4lfs8k450z71jjd8xcah7c")))

(define-public crate-ckb-logger-config-0.105.0 (c (n "ckb-logger-config") (v "0.105.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12n395kph6li2rn6dhn7zayqpa4ig3k7i7lfdnj3bxvaby0xr2hi") (y #t)))

(define-public crate-ckb-logger-config-0.105.1 (c (n "ckb-logger-config") (v "0.105.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ycjjw51ng85ajglk8kffhr0hrbc8c0b3s2px4isj7swsfvlzapf") (y #t)))

(define-public crate-ckb-logger-config-0.106.0 (c (n "ckb-logger-config") (v "0.106.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jc107s97jkma4112l7203wc8wdw9ds30jzic4h9lc12n1y3vcc5")))

(define-public crate-ckb-logger-config-0.107.0 (c (n "ckb-logger-config") (v "0.107.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1affibnqa71ixr3h3c97rwrcbgn5aqqcinxb6mj4w2fn0rq39ygr")))

(define-public crate-ckb-logger-config-0.108.0 (c (n "ckb-logger-config") (v "0.108.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1czq7h3kh4i88qi1lplw0krh2h4mha4fnqc5zr17dqwf97ibxwsf")))

(define-public crate-ckb-logger-config-0.108.1 (c (n "ckb-logger-config") (v "0.108.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11zd042ir9afrpvlbk5aqk8jaz0avviysjng99cxqbh4qj3kh28v")))

(define-public crate-ckb-logger-config-0.109.0 (c (n "ckb-logger-config") (v "0.109.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09k9ign13s3kh4kigg3r51k3y51bprwj5vfk0qnwaq0bf1jzk14f")))

(define-public crate-ckb-logger-config-0.110.0 (c (n "ckb-logger-config") (v "0.110.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mlvagify1yh8iz2lbwz57j5hc3mk9i97k73cyyklxfn8njhzrnl")))

(define-public crate-ckb-logger-config-0.110.0-rc1 (c (n "ckb-logger-config") (v "0.110.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08495lzb498jhf9i5l1dis4s0wyqhzcf0fkh7w99gmmwgfa7nz5p")))

(define-public crate-ckb-logger-config-0.111.0-rc2 (c (n "ckb-logger-config") (v "0.111.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z3631y4bkb0zliqh116gr01fqz9a7233ipcy8ix1n2nqhhym92d")))

(define-public crate-ckb-logger-config-0.111.0-rc3 (c (n "ckb-logger-config") (v "0.111.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c6kg8i0hcq9nwx9z0fc3d90jkbqckxc721bnhyvnqd0yazdwf37")))

(define-public crate-ckb-logger-config-0.111.0-rc4 (c (n "ckb-logger-config") (v "0.111.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pwfk8m2fjmfbg10k3x36rx3l09f62xf5zjiih38mlf3dw6b888h")))

(define-public crate-ckb-logger-config-0.111.0-rc5 (c (n "ckb-logger-config") (v "0.111.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fdc4ylxagsq686axiddn6qha785advk3gcmh93xnspyqrpa7mnc")))

(define-public crate-ckb-logger-config-0.111.0-rc6 (c (n "ckb-logger-config") (v "0.111.0-rc6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0850wq7f3nfw93h4x2c711n8fzc05dmlx1z8mqz8h56hmg887j5w")))

(define-public crate-ckb-logger-config-0.111.0-rc7 (c (n "ckb-logger-config") (v "0.111.0-rc7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17aw985l4parfgmw7i35dsihnphsy1m8xjhglqiiyzvl7zrn073j")))

(define-public crate-ckb-logger-config-0.111.0-rc8 (c (n "ckb-logger-config") (v "0.111.0-rc8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rs51ldiavyy7d41ciy4plwxjjzpr5743ay42h9qqjz06ryir9b1")))

(define-public crate-ckb-logger-config-0.110.1-rc1 (c (n "ckb-logger-config") (v "0.110.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08sk79ijw3fb2gpygi30m9nsf0yzlvxa8slybd2gf7l17g23aklg")))

(define-public crate-ckb-logger-config-0.110.1-rc2 (c (n "ckb-logger-config") (v "0.110.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0skbzjvnfi4h91w8n824pkfqh0ij6krnmmhycp0vjd7lpc16p2ci")))

(define-public crate-ckb-logger-config-0.111.0-rc9 (c (n "ckb-logger-config") (v "0.111.0-rc9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i65jl7b4fffyzm2v925znrj5yfr3ps4ixcch39dybl7lj9k9fxk")))

(define-public crate-ckb-logger-config-0.111.0-rc10 (c (n "ckb-logger-config") (v "0.111.0-rc10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l6v42qr78cymaa6c8j30qqkd0xg4dp0lngpa4x3iicipj7brann")))

(define-public crate-ckb-logger-config-0.110.1 (c (n "ckb-logger-config") (v "0.110.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16j7r17fvksv5dgfczklysa25kajyzir386hlpg24zw9y2md80xz")))

(define-public crate-ckb-logger-config-0.110.2-rc1 (c (n "ckb-logger-config") (v "0.110.2-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l8cd3j3aqym4sfz14rl49m29k6dl42a01z7a8snybanip05h131")))

(define-public crate-ckb-logger-config-0.111.0-rc11 (c (n "ckb-logger-config") (v "0.111.0-rc11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ymfyz62cvvc67xxjn0zn54n4n7g5c8nk3iyaks8vk329w6jz4rz")))

(define-public crate-ckb-logger-config-0.110.2-rc2 (c (n "ckb-logger-config") (v "0.110.2-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12fx582h4z3zr4hxzk41ydxfhzgbw7psrhl488fzs7xchfad43vx")))

(define-public crate-ckb-logger-config-0.111.0-rc12 (c (n "ckb-logger-config") (v "0.111.0-rc12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cxicgbgkl06ny1nflccxk8jnbcfjvsmj9s1rdrd9zq3g0ayfgm0")))

(define-public crate-ckb-logger-config-0.110.2 (c (n "ckb-logger-config") (v "0.110.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "071rid6p05vy2bb8ks0i4z20yqb2g70mynv2xxygsrp750qhidw8")))

(define-public crate-ckb-logger-config-0.111.0 (c (n "ckb-logger-config") (v "0.111.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0as2c0479lxw11nfs9wfpr1aqk1sw8dwq3s3ynx9xsnj19c6hb83")))

(define-public crate-ckb-logger-config-0.112.0-rc1 (c (n "ckb-logger-config") (v "0.112.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y2vzs5zi60pk8imdj46q5h3w3nn5nrmaihhw7vzq1iv7jcw17b0")))

(define-public crate-ckb-logger-config-0.112.0-rc2 (c (n "ckb-logger-config") (v "0.112.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qg9rqknayw070qn2arhw3xn2jkjqzjscvbv1qbvsbkn35j644i0")))

(define-public crate-ckb-logger-config-0.112.0 (c (n "ckb-logger-config") (v "0.112.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kd0r6yffflf7ynag0j5v09d4w9pplpvagi9p0i4d3g7kjnvgvsb")))

(define-public crate-ckb-logger-config-0.112.0-rc3 (c (n "ckb-logger-config") (v "0.112.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d9046la9ckjq6w1g87phryawv8glf670ps4lglc18fknmwxhfm1")))

(define-public crate-ckb-logger-config-0.112.1 (c (n "ckb-logger-config") (v "0.112.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ya3ryrrdispjnx1nxn1k4vfhxpgmpz3nc8aaqy670f03lqxnm1d")))

(define-public crate-ckb-logger-config-0.113.0-rc1 (c (n "ckb-logger-config") (v "0.113.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13gwyv15yn5mk7hpv9m93vz4d678k26gka4hqxv4qysml5xpbg9q")))

(define-public crate-ckb-logger-config-0.113.0-rc2 (c (n "ckb-logger-config") (v "0.113.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jyf8j3xi8c24rmlfcm0jzaw2ldma6nbsn33ssjd3ra7mwingr9p")))

(define-public crate-ckb-logger-config-0.113.0-rc3 (c (n "ckb-logger-config") (v "0.113.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09lmva0ahd91w8ddpr436n192ppas5m1k7r23gzzra7ng77knxnl")))

(define-public crate-ckb-logger-config-0.113.0 (c (n "ckb-logger-config") (v "0.113.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03afzk5a8mhfhw0km5yinjldrmg2gxqdc2q9d627hp7vsikkji1i")))

(define-public crate-ckb-logger-config-0.113.1-rc1 (c (n "ckb-logger-config") (v "0.113.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r96pvcg4k8hslakx8v1gkbkc36ni4szg2qg981gsrp8ww93jcxj")))

(define-public crate-ckb-logger-config-0.114.0-rc1 (c (n "ckb-logger-config") (v "0.114.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "097lr95pfac0znvxmdlwizkh6zp68vdmgciyf2f9fz537bvn5wxd")))

(define-public crate-ckb-logger-config-0.113.1-rc2 (c (n "ckb-logger-config") (v "0.113.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mlvyvayfl2dzk18ryp0kkvs7v7yypfbnnp10g4839laqqzp75qg")))

(define-public crate-ckb-logger-config-0.113.1 (c (n "ckb-logger-config") (v "0.113.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xfg5sia0qp5hlrzwgf1qplg7jish8ifrplazm0gsfg8vymf5nrv")))

(define-public crate-ckb-logger-config-0.114.0-rc2 (c (n "ckb-logger-config") (v "0.114.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r3wrp7vihwwxqyq37r12zxg07j20ixif310hs4lf4n027810zki")))

(define-public crate-ckb-logger-config-0.114.0-rc3 (c (n "ckb-logger-config") (v "0.114.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v9nij60fmr0cn95yx8nw8wkazk437k6pk4bb2z6awha2in4mafn")))

(define-public crate-ckb-logger-config-0.114.0 (c (n "ckb-logger-config") (v "0.114.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09i6qsax9g8m8kv7jg3qgpxnzz7b0r0v1z250v0yvacqk1iki7bx")))

(define-public crate-ckb-logger-config-0.115.0-pre (c (n "ckb-logger-config") (v "0.115.0-pre") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yar0n7cx931lkfbcd4d11abqbxj5am200hwhiv8m3xxq5vz1pz8")))

(define-public crate-ckb-logger-config-0.115.0-rc1 (c (n "ckb-logger-config") (v "0.115.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1czwyzz2dk36bbn7pyasc992xwwxdkawrxbd9l1jxzk42264qvzg")))

(define-public crate-ckb-logger-config-0.115.0-rc2 (c (n "ckb-logger-config") (v "0.115.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gqimjgpwh4s4wd5gnzgmrig8q9ll90kxw0qrvyz1vbh0xfh0gvp")))

(define-public crate-ckb-logger-config-0.115.0 (c (n "ckb-logger-config") (v "0.115.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bam9vcj2i9k2d9791aw86qvxxx1vfr1kpk4j76jih8dwyq7g2jx")))

(define-public crate-ckb-logger-config-0.116.0-rc1 (c (n "ckb-logger-config") (v "0.116.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19j6vp204arlnpbjqxfnc8qzmpbday4ks6jbkck61q57nqsfw9cf")))

(define-public crate-ckb-logger-config-0.116.0-rc2 (c (n "ckb-logger-config") (v "0.116.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ys8i2il2yj1v57rmy6wxng2nmml1x8q198wyx8vk1qw4f63l6bx")))

(define-public crate-ckb-logger-config-0.116.0 (c (n "ckb-logger-config") (v "0.116.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i6198zdcy2fjxzw361hz4vqc0jwlck4lp9sllqf0z5m4hl7gyg8") (y #t)))

(define-public crate-ckb-logger-config-0.116.1 (c (n "ckb-logger-config") (v "0.116.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cjhrd4ksf8b15k6knfy5f5q9f6yk8p0sz1bp6kd0wanv08g5b9y")))

