(define-module (crates-io ck b- ckb-fixed-hash-macros) #:use-module (crates-io))

(define-public crate-ckb-fixed-hash-macros-0.37.0-pre (c (n "ckb-fixed-hash-macros") (v "0.37.0-pre") (d (list (d (n "ckb-fixed-hash-core") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1m8yz1hmp46dj8pq2s4zxkfsicda8wkd8a5alw2wzixrymgl3xz9")))

(define-public crate-ckb-fixed-hash-macros-0.37.0 (c (n "ckb-fixed-hash-macros") (v "0.37.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.37.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zfbps1ys4d8jpz7bd4q03fmc1yviyynf0bkpfgvhggslin7425b")))

(define-public crate-ckb-fixed-hash-macros-0.38.0 (c (n "ckb-fixed-hash-macros") (v "0.38.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.38.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05l39x3dyq391h6n5rvq9x8fmpb4pr4rlv0p18cz9mpnmpv0scn3")))

(define-public crate-ckb-fixed-hash-macros-0.39.0 (c (n "ckb-fixed-hash-macros") (v "0.39.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.39.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j5w9bppk1g8w7l77v2sbvsf376kca4vnr2bjl02ddi58z9vbf0y")))

(define-public crate-ckb-fixed-hash-macros-0.39.1 (c (n "ckb-fixed-hash-macros") (v "0.39.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.39.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r83dnvfrpfak49zqwg1im8bp33pf6rl8c2pp3l6p7q8qn2k2smi")))

(define-public crate-ckb-fixed-hash-macros-0.40.0 (c (n "ckb-fixed-hash-macros") (v "0.40.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.40.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07r2m6ppnzf87niaknm8ibmvp0rkgln1xwr3k0rfbx85adqbyadw")))

(define-public crate-ckb-fixed-hash-macros-0.42.0 (c (n "ckb-fixed-hash-macros") (v "0.42.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.42.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cri8lrcxvgsvykq3s1pnz9s50d4vr7zq9k3701c5msvpzqjms2x")))

(define-public crate-ckb-fixed-hash-macros-0.43.0 (c (n "ckb-fixed-hash-macros") (v "0.43.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.43.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "190fm57rmzrrni5zxv8az5y3gwqmcjzlkx0xcmn5sss3yynnkrdj")))

(define-public crate-ckb-fixed-hash-macros-0.100.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.100.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rjydcfvikbk9zgn4x0y309z4ymgmmn4w29r683v37xf8pwg04zz")))

(define-public crate-ckb-fixed-hash-macros-0.43.2 (c (n "ckb-fixed-hash-macros") (v "0.43.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.43.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jlis48kyb1pkf1ijfnr4jzh5grdxywz0v7pxj67vax3piqjfd7a")))

(define-public crate-ckb-fixed-hash-macros-0.100.0-rc3 (c (n "ckb-fixed-hash-macros") (v "0.100.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18n9hy9z8078fkjs0hfdskyqq5danq4pfsr81wscganyyvczgra7") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.100.0-rc4 (c (n "ckb-fixed-hash-macros") (v "0.100.0-rc4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12q7ilvx19m2ic6wjliwr0ykg6dkjm8cv3h0r4kwhpcn4hmvzg34")))

(define-public crate-ckb-fixed-hash-macros-0.100.0-rc5 (c (n "ckb-fixed-hash-macros") (v "0.100.0-rc5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z27a6xhnz3p26akn806mrdrdmh5gg5zzymhhzf9mjnhzk4f5ibr")))

(define-public crate-ckb-fixed-hash-macros-0.100.0 (c (n "ckb-fixed-hash-macros") (v "0.100.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.100.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mzq3a4s5xv24gjw4sn2yr49z4061m6ld5klxzb13xjvai8sk350")))

(define-public crate-ckb-fixed-hash-macros-0.101.0 (c (n "ckb-fixed-hash-macros") (v "0.101.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "010vickgiivnqvqh8m3lvxn4ci759vfzsrxg46j1zknbbb1x8l6k")))

(define-public crate-ckb-fixed-hash-macros-0.101.1 (c (n "ckb-fixed-hash-macros") (v "0.101.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yb8a1zmhswz6almdycb0rqvqgqjh7yxkdqjlkr87ir0dbb4xjx0")))

(define-public crate-ckb-fixed-hash-macros-0.101.2-testnet1 (c (n "ckb-fixed-hash-macros") (v "0.101.2-testnet1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1a58f9q62ibf8cmpcprv3hfrnfbqpk39a7fb1rrj3101k69nijk1")))

(define-public crate-ckb-fixed-hash-macros-0.101.2 (c (n "ckb-fixed-hash-macros") (v "0.101.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y57gfilp3ldzwbzaa1m5n6ajjld4sh5km0jq15h9xgr290r68wb")))

(define-public crate-ckb-fixed-hash-macros-0.101.3 (c (n "ckb-fixed-hash-macros") (v "0.101.3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1966w7qk5qikswbvhnyywjjrbiybwz91ycf3xqzmf2ywrq3l8ygl")))

(define-public crate-ckb-fixed-hash-macros-0.101.4 (c (n "ckb-fixed-hash-macros") (v "0.101.4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0im7j9x0mc4adkcj495jr4h5ixrynl1581l5z04rwx4n477dp75b")))

(define-public crate-ckb-fixed-hash-macros-0.101.5 (c (n "ckb-fixed-hash-macros") (v "0.101.5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05gmv471vfw15afarhmkd89qd5wvf25v6nlg06bvsvd5skw15wvs") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.101.6 (c (n "ckb-fixed-hash-macros") (v "0.101.6") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19rrvqh4ml3s5wza8zm7a2l0aj0wgmllgzs5z8jpgmrz78sl94bq")))

(define-public crate-ckb-fixed-hash-macros-0.101.7 (c (n "ckb-fixed-hash-macros") (v "0.101.7") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h4qfa9mvzxwgx3j0w8xk2g0bn03wzfhw6vrsrk16p7s7h5q6drl")))

(define-public crate-ckb-fixed-hash-macros-0.101.8 (c (n "ckb-fixed-hash-macros") (v "0.101.8") (d (list (d (n "ckb-fixed-hash-core") (r "=0.101.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zmkm3kamhndwk64llzkjdc9n78dymy39m62b0f8b5x6njc54hs7")))

(define-public crate-ckb-fixed-hash-macros-0.102.0 (c (n "ckb-fixed-hash-macros") (v "0.102.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.102.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hlbqyyc6m5dlkg02wj40gxv3zd5q7c736kcskjvx7m2w3yinr3m") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.103.0 (c (n "ckb-fixed-hash-macros") (v "0.103.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.103.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x2ri79sziv2q4vjp4vyj9x3zjg0ksjabbi4h2gxl0bfssjgbj5w")))

(define-public crate-ckb-fixed-hash-macros-0.104.0 (c (n "ckb-fixed-hash-macros") (v "0.104.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.104.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19ph92rx219mrcayj8wqahw00mlx400ngf3kv35xi777fwb2hwqg")))

(define-public crate-ckb-fixed-hash-macros-0.104.1 (c (n "ckb-fixed-hash-macros") (v "0.104.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.104.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a6pz0hyf2nbnkz6ax7nmlgag8lx65vvwcqcqix31m9pm5a4bbx6")))

(define-public crate-ckb-fixed-hash-macros-0.105.0 (c (n "ckb-fixed-hash-macros") (v "0.105.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.105.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09wpwncls31xl6rz84k3dlnrc6hn55a33a0p8c29b2sx7hd50z9k") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.105.1 (c (n "ckb-fixed-hash-macros") (v "0.105.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.105.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ly9dvf0xdkpwg6sff3i6z2n1qya0wldw6avifn93n545nw44kpj") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.106.0 (c (n "ckb-fixed-hash-macros") (v "0.106.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.106.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r08f95r1s8zgd6r68pq9c56xiflainzjpiad875lngbyrc67s31")))

(define-public crate-ckb-fixed-hash-macros-0.107.0 (c (n "ckb-fixed-hash-macros") (v "0.107.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.107.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04d7sx1kdmxi5c2ggh0j61f0iv0yli0rds86hbj6bdrnwzrwi267")))

(define-public crate-ckb-fixed-hash-macros-0.108.0 (c (n "ckb-fixed-hash-macros") (v "0.108.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.108.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d542yyv64vg84a858vqbq7qjiwz4vgldsvja611ghl8xy0ckkz1")))

(define-public crate-ckb-fixed-hash-macros-0.108.1 (c (n "ckb-fixed-hash-macros") (v "0.108.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.108.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vkrprhb1lmw4ai04yb0lgjmp5ph2y966xmady82wipk0nf5sbv0")))

(define-public crate-ckb-fixed-hash-macros-0.109.0 (c (n "ckb-fixed-hash-macros") (v "0.109.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.109.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "139ikgzprwwsibmhk9fi8zk69nh8f8h18809c9n1r8w29bll1n9r")))

(define-public crate-ckb-fixed-hash-macros-0.110.0 (c (n "ckb-fixed-hash-macros") (v "0.110.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wwz4flgsbdxpzlxjkwwx9kx0li248xhp8wq1qrq1jjf2z0ghj97")))

(define-public crate-ckb-fixed-hash-macros-0.110.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.110.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z70zcmc96gkjzprb88cv9a0ck4yzgb6nqyyxwq974pl8j53ps15")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12ymapb94gv9yf5xkxfhyk9mhyqr3x3jkyrwgyj2jmg3ikharb0d")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc3 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "014spirrscr95viggyg49y1d2h78yz4agrmb6mwixddn71b9w27r")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc4 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc4") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ll5fbfyfnjkybym99dhi1gz9llb1lp36385qn32lbibkf1wz6dh")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc5 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc5") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12abxf6piwr222jk2v33jpn0g3lqfzyqssm42qk159r1zm6vdlvc")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc6 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc6") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "166jnn87vyjr61dp9y5f88rv3lgp40rcain6jnbfdlw843ksrvfg")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc7 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc7") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12a1qjy9dxdcr8rr9pyxygmmqjcs1rbjj0n42s0r5589vgylrvhh")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc8 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc8") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y7mf31gwl786w18v5xnxj4rmwk5ffqksimvgi53344i4ddz5wnz")))

(define-public crate-ckb-fixed-hash-macros-0.110.1-rc1 (c (n "ckb-fixed-hash-macros") (v "0.110.1-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08w56ahsim6rqh25n08nqyyznllq6gwrlfrh32gyzz9hylary2kg")))

(define-public crate-ckb-fixed-hash-macros-0.110.1-rc2 (c (n "ckb-fixed-hash-macros") (v "0.110.1-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17hgadavwq2psavrbcahi9cgnj0vg0v98s6lfjgpp5l7pyrmbm2l")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc9 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc9") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cg8zvby6nf2ynv2h84d1ls5s5l6sc2zi9f670zn8v46qmnknkmf")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc10 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc10") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14rgnr6csqsz9gdl1cipzrp9dp488x3q9d3llrwxcixvsiz53yw7")))

(define-public crate-ckb-fixed-hash-macros-0.110.1 (c (n "ckb-fixed-hash-macros") (v "0.110.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cdis8sknsmlwcrf3dxlk671cvykw8h67cw3xh2lmxima6rzllh1")))

(define-public crate-ckb-fixed-hash-macros-0.110.2-rc1 (c (n "ckb-fixed-hash-macros") (v "0.110.2-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08djw0nd9jsdmkav1aznzah5a46i759j00fpr6r7h58cd9ivp33z")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc11 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc11") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kz4hi7d0747lbcw6zdvv2ld5mss3469j4m7hmrnd4ryiqj11q5g")))

(define-public crate-ckb-fixed-hash-macros-0.110.2-rc2 (c (n "ckb-fixed-hash-macros") (v "0.110.2-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03431xrgmd7qk4vcc4pm4vibk4d4hvn8rnbi22q4mc2xpy49gjh0")))

(define-public crate-ckb-fixed-hash-macros-0.111.0-rc12 (c (n "ckb-fixed-hash-macros") (v "0.111.0-rc12") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02zng6bd1fgz9mqjinvf3ny4ya040rwhy3dzzr9zxcyzhyrbhbxl")))

(define-public crate-ckb-fixed-hash-macros-0.110.2 (c (n "ckb-fixed-hash-macros") (v "0.110.2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.110.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18jpyf9gplv5n53xpphsa17r934fbm5dmcl115dqlr4rys9wp8qh")))

(define-public crate-ckb-fixed-hash-macros-0.111.0 (c (n "ckb-fixed-hash-macros") (v "0.111.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.111.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yg4hcag04y6a5g6ql23mzblc0rvvnhy7r6dbxqkap2q696a6pbv")))

(define-public crate-ckb-fixed-hash-macros-0.112.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.112.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ylyjbsrx4h1gq9hhd6av8kyp8xhc5qsxb3z76js8x9k752ghypf")))

(define-public crate-ckb-fixed-hash-macros-0.112.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.112.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01yqkymb0f3azscpyaqnqcz2n3k304i13d008q8zavzzpsmjvlzx")))

(define-public crate-ckb-fixed-hash-macros-0.112.0 (c (n "ckb-fixed-hash-macros") (v "0.112.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d5r73rpz8xlmq66q2awf1bhlzs5kniz0dpqhzcl3v8v4mk12q76")))

(define-public crate-ckb-fixed-hash-macros-0.112.0-rc3 (c (n "ckb-fixed-hash-macros") (v "0.112.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d8csvnjk512ayn3zwii5dfcbq052yds0kpvpf58qcq6rlmq4jvz")))

(define-public crate-ckb-fixed-hash-macros-0.112.1 (c (n "ckb-fixed-hash-macros") (v "0.112.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.112.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ag4klqr4xm7ax2g8v98drlragll51p50rjylsisbjhqgnda39ds")))

(define-public crate-ckb-fixed-hash-macros-0.113.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.113.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lq8gg89ysa9cxr34csd73hgx12g4ii67afggkkqdrvn0pyzj57f")))

(define-public crate-ckb-fixed-hash-macros-0.113.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.113.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jldala91hijbgjnghv97m5mdv2b7vfyvc7mgqw52rdx0k5ql08b")))

(define-public crate-ckb-fixed-hash-macros-0.113.0-rc3 (c (n "ckb-fixed-hash-macros") (v "0.113.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18p9p8xphiznnw5m8jyyhlvw3gxkcwym7nrd17hzljqmi55akwh5")))

(define-public crate-ckb-fixed-hash-macros-0.113.0 (c (n "ckb-fixed-hash-macros") (v "0.113.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1331b428jhkrsgvw4ypnkl06msh67x02z44xhcv2xg8cyy243biv")))

(define-public crate-ckb-fixed-hash-macros-0.113.1-rc1 (c (n "ckb-fixed-hash-macros") (v "0.113.1-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmj22anp0yyldhyngl6a6sqivz9ap2jmp0zaa9qd7q2m5qjw3ic")))

(define-public crate-ckb-fixed-hash-macros-0.114.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.114.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kxvp4ysa0appcb1m5ckpvxsmkyjxj6h75z27xhxbjcjlcxh3b14")))

(define-public crate-ckb-fixed-hash-macros-0.113.1-rc2 (c (n "ckb-fixed-hash-macros") (v "0.113.1-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04vk4f6jg4dwjzjk3cnbwzv67w2flhixsgxl0q2h2xsvcvhh7wn2")))

(define-public crate-ckb-fixed-hash-macros-0.113.1 (c (n "ckb-fixed-hash-macros") (v "0.113.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.113.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yyxjbchmi0w450zi69yjpdd85pa0i0xknxqimz3s25kja7cmg40")))

(define-public crate-ckb-fixed-hash-macros-0.114.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.114.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rf5zpz29dfaafw05982i39yic34kn8d3y38hnviilm35gh1mjhq")))

(define-public crate-ckb-fixed-hash-macros-0.114.0-rc3 (c (n "ckb-fixed-hash-macros") (v "0.114.0-rc3") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fk4bh3gfw7z97r5y33ni3ydxm8illrmr2n0gclxmr76qybbiph7")))

(define-public crate-ckb-fixed-hash-macros-0.114.0 (c (n "ckb-fixed-hash-macros") (v "0.114.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.114.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1afb9vf9hn7swvihkdcyz81814yl8g9i0fg2qzjls36jwchp53q0")))

(define-public crate-ckb-fixed-hash-macros-0.115.0-pre (c (n "ckb-fixed-hash-macros") (v "0.115.0-pre") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ld9gr5w18m29nx6cbifqjjpxw9i1syn702xs7mf1as43kgnkpwq")))

(define-public crate-ckb-fixed-hash-macros-0.115.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.115.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fz2z83nqfhjbp5gvb2gplv9blpn2g3awn2ihmj9rkvj0sglydjy")))

(define-public crate-ckb-fixed-hash-macros-0.115.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.115.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y36irqayvz9q71jscy5qanwk5xpjxphy1806ziq8yz5bjlyc2g1")))

(define-public crate-ckb-fixed-hash-macros-0.115.0 (c (n "ckb-fixed-hash-macros") (v "0.115.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.115.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08qdk8gjcqwabdsis6ar10sw77zr3lfrv2f4qycwzg311qqsh3wn")))

(define-public crate-ckb-fixed-hash-macros-0.116.0-rc1 (c (n "ckb-fixed-hash-macros") (v "0.116.0-rc1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b0w2npwz99p79p49r55yyc110l8lkxssy4msclv7l5mm6qsmp2x")))

(define-public crate-ckb-fixed-hash-macros-0.116.0-rc2 (c (n "ckb-fixed-hash-macros") (v "0.116.0-rc2") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jnwqaiwd6lp2wivbfwgpgwv5dqcnyzf7qq8dsvp5537q9gi5ysb")))

(define-public crate-ckb-fixed-hash-macros-0.116.0 (c (n "ckb-fixed-hash-macros") (v "0.116.0") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rdhhdpl7kz0gaw5imy9fpf9zy7i8d5dy44dkia8gw260bvgsqk6") (y #t)))

(define-public crate-ckb-fixed-hash-macros-0.116.1 (c (n "ckb-fixed-hash-macros") (v "0.116.1") (d (list (d (n "ckb-fixed-hash-core") (r "=0.116.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cg9pq77j1pmpaabwdckhh612kq8bxps8ll7g586yx6sllrrbn7x")))

