(define-module (crates-io ck b- ckb-async-runtime) #:use-module (crates-io))

(define-public crate-ckb-async-runtime-0.37.0-pre (c (n "ckb-async-runtime") (v "0.37.0-pre") (d (list (d (n "tokio") (r "^0.2") (f (quote ("sync" "blocking" "rt-threaded"))) (d #t) (k 0)))) (h "0kd6s0fbaldkv63xwazqazfw0a4a81cbx48h71x2xd7l75mvn6gz")))

(define-public crate-ckb-async-runtime-0.37.0 (c (n "ckb-async-runtime") (v "0.37.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("sync" "blocking" "rt-threaded"))) (d #t) (k 0)))) (h "0kd6dszw3m7pprjv54dag7b1kp1p7532rp3ds1yr72rqg1srzi3b")))

(define-public crate-ckb-async-runtime-0.38.0 (c (n "ckb-async-runtime") (v "0.38.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("sync" "blocking" "rt-threaded"))) (d #t) (k 0)))) (h "00i5jmxvircxrrfbzgks2dw7xdha8yw2sa701x20pzcyhm5chmlq")))

(define-public crate-ckb-async-runtime-0.39.0 (c (n "ckb-async-runtime") (v "0.39.0") (d (list (d (n "ckb-logger") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.39.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1px4yv4dbjmhzz53hxfh9xrjhfqw599v4wn76155v5xmx2xb8jyc")))

(define-public crate-ckb-async-runtime-0.39.1 (c (n "ckb-async-runtime") (v "0.39.1") (d (list (d (n "ckb-logger") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.39.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "00dw9djp6kzxwlm0c0qddajw2c6hy434mbl6widcdwj9907rfyk5")))

(define-public crate-ckb-async-runtime-0.40.0 (c (n "ckb-async-runtime") (v "0.40.0") (d (list (d (n "ckb-logger") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.40.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.40.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j9xx8w6ygxwy645fmd5pbc6c3zyicq3zg1ql406ng0nyb4cnpvq")))

(define-public crate-ckb-async-runtime-0.42.0 (c (n "ckb-async-runtime") (v "0.42.0") (d (list (d (n "ckb-logger") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.42.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.42.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2vd4i8avfs8d4k2ppsq03ffpr4c57icrlgn7gbirwj8dd8i8j7")))

(define-public crate-ckb-async-runtime-0.43.0 (c (n "ckb-async-runtime") (v "0.43.0") (d (list (d (n "ckb-logger") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.43.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.43.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bxp1486i2c8l355kwggdg5xa1cyzxv1a9f9yaibwpdiyb3gb7i4")))

(define-public crate-ckb-async-runtime-0.100.0-rc2 (c (n "ckb-async-runtime") (v "0.100.0-rc2") (d (list (d (n "ckb-logger") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g45rp8alfj1sxa9si3bivcakqnqps9r9ilgc3czqkfflxpk50qd")))

(define-public crate-ckb-async-runtime-0.43.2 (c (n "ckb-async-runtime") (v "0.43.2") (d (list (d (n "ckb-logger") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.43.2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.43.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jf5aza3r1yfnqaj687ya0fq2kmfw8p5xv3b926shid3cfx39rqd")))

(define-public crate-ckb-async-runtime-0.100.0-rc3 (c (n "ckb-async-runtime") (v "0.100.0-rc3") (d (list (d (n "ckb-logger") (r "=0.100.0-rc3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.100.0-rc3") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.100.0-rc3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jiyrsznj1rj6y1r2vlh4kglvlynz0j2igqxa120jw3958zi5ym5") (y #t)))

(define-public crate-ckb-async-runtime-0.100.0-rc4 (c (n "ckb-async-runtime") (v "0.100.0-rc4") (d (list (d (n "ckb-logger") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y7p6dgv74rrls4gxhfpycs8h852hhqmd83hcgp2zl7wd5jzklaj")))

(define-public crate-ckb-async-runtime-0.100.0-rc5 (c (n "ckb-async-runtime") (v "0.100.0-rc5") (d (list (d (n "ckb-logger") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1lpcdmirzn7azb3i95gmgyz837hnjl99wc34z86v7457lpp3lpwm")))

(define-public crate-ckb-async-runtime-0.100.0 (c (n "ckb-async-runtime") (v "0.100.0") (d (list (d (n "ckb-logger") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.100.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.100.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jqgpvnzickbqdhdik2aqbgy0y7bqjp7rg1a375bgri8gxjaj9qm")))

(define-public crate-ckb-async-runtime-0.101.0 (c (n "ckb-async-runtime") (v "0.101.0") (d (list (d (n "ckb-logger") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07ny7gda34qq0b21r8xrhqi5dqwygl34kxpa171rhghn9nhngkk5")))

(define-public crate-ckb-async-runtime-0.101.1 (c (n "ckb-async-runtime") (v "0.101.1") (d (list (d (n "ckb-logger") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "134142igvlkvkixwp2rqsd1z4lrhvzjhyybapi6cjhjgsf7i19ng")))

(define-public crate-ckb-async-runtime-0.101.2-testnet1 (c (n "ckb-async-runtime") (v "0.101.2-testnet1") (d (list (d (n "ckb-logger") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ay46yykpbw78rzpm3xb0f7acfwkzjkk7nxxs54nzq1z96gb5xlp")))

(define-public crate-ckb-async-runtime-0.101.2 (c (n "ckb-async-runtime") (v "0.101.2") (d (list (d (n "ckb-logger") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1idg04w063c220m8fdgfj9vib0n9ilbv8i6ln9gc03vxnmxx7qy4")))

(define-public crate-ckb-async-runtime-0.101.3 (c (n "ckb-async-runtime") (v "0.101.3") (d (list (d (n "ckb-logger") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.3") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bdlp6ghzy1pby6a5l42ksd5ly2agh4ck3wb4k635m5n4jl8hz4m")))

(define-public crate-ckb-async-runtime-0.101.4 (c (n "ckb-async-runtime") (v "0.101.4") (d (list (d (n "ckb-logger") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.4") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s82izq8yqniq4f3lngkd4g7p3rc6zn7rp7nr60fzrixdyh6pd4l")))

(define-public crate-ckb-async-runtime-0.101.5 (c (n "ckb-async-runtime") (v "0.101.5") (d (list (d (n "ckb-logger") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.5") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03f6mdm1m2c67gwyk7cicsyb29lsdgxk29gqk9nha8dp8f0qpip0") (y #t)))

(define-public crate-ckb-async-runtime-0.101.6 (c (n "ckb-async-runtime") (v "0.101.6") (d (list (d (n "ckb-logger") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.6") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0irk7karcpay7nzqs1iwyqll7m86dvmk7jml4xakdh2f4rgzznhj")))

(define-public crate-ckb-async-runtime-0.101.7 (c (n "ckb-async-runtime") (v "0.101.7") (d (list (d (n "ckb-logger") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.7") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qmsfzcc79hvxzwzk6hyipzs5pgjfjq4wbf1i6rzwhv99zjv4bbv")))

(define-public crate-ckb-async-runtime-0.101.8 (c (n "ckb-async-runtime") (v "0.101.8") (d (list (d (n "ckb-logger") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.101.8") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.101.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7dyfym4232j6ambs02v10dzjn9vga47yahhscbpsipanxbvyj1")))

(define-public crate-ckb-async-runtime-0.102.0 (c (n "ckb-async-runtime") (v "0.102.0") (d (list (d (n "ckb-logger") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.102.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.102.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "044586h52sl466lr99vhz8wk323n1nvsph0dgb6p9bwhjwprmzv1") (y #t)))

(define-public crate-ckb-async-runtime-0.103.0 (c (n "ckb-async-runtime") (v "0.103.0") (d (list (d (n "ckb-logger") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.103.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.103.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wgp0vp9760xg9fd8310lxgxn4f6akcz42xf6kmr84vn0iak86r5")))

(define-public crate-ckb-async-runtime-0.104.0 (c (n "ckb-async-runtime") (v "0.104.0") (d (list (d (n "ckb-logger") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.104.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.104.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n6gamr0i9sm3na07rn39r1ndcg2kymaxifgjydmamx023swvcfg")))

(define-public crate-ckb-async-runtime-0.104.1 (c (n "ckb-async-runtime") (v "0.104.1") (d (list (d (n "ckb-logger") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.104.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.104.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kp301cvg4nhacdkl2c3nk7zsrhzcn67jwwqja65f2036n0dhyr0")))

(define-public crate-ckb-async-runtime-0.105.0 (c (n "ckb-async-runtime") (v "0.105.0") (d (list (d (n "ckb-logger") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.105.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.105.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17h10qafd5vd1517c15y3vika8w8rg0xw6466425zihnzra10vx4") (y #t)))

(define-public crate-ckb-async-runtime-0.105.1 (c (n "ckb-async-runtime") (v "0.105.1") (d (list (d (n "ckb-logger") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.105.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.105.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bzfy6w9gqs81zjccwa7kx43qpn7d08yxid3rpp4hay23nv6zzk3") (y #t)))

(define-public crate-ckb-async-runtime-0.106.0 (c (n "ckb-async-runtime") (v "0.106.0") (d (list (d (n "ckb-logger") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.106.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.106.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pkkq5kfl5bbghvrk3wssh1g89xdxsr28mdw5rbrr5h1lhyy7v4d")))

(define-public crate-ckb-async-runtime-0.107.0 (c (n "ckb-async-runtime") (v "0.107.0") (d (list (d (n "ckb-logger") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.107.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.107.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17mv5bxrxqrfxl03a5bfp26803nia7lmqk0ssmny8nmiwq7k7hqc")))

(define-public crate-ckb-async-runtime-0.108.0 (c (n "ckb-async-runtime") (v "0.108.0") (d (list (d (n "ckb-logger") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.108.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.108.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nknzvqscv6mp7pq15ki8annnqvjdjc59yn4nqds619raisbd4a9")))

(define-public crate-ckb-async-runtime-0.108.1 (c (n "ckb-async-runtime") (v "0.108.1") (d (list (d (n "ckb-logger") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.108.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.108.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywf28585kjqdyn3jia9mi6b0vx4i03mm0d4vprwxqpiinsavsdz")))

(define-public crate-ckb-async-runtime-0.109.0 (c (n "ckb-async-runtime") (v "0.109.0") (d (list (d (n "ckb-logger") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.109.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.109.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sdf56jxg1khd4511cb1lhxph6qfm24wpdsiip7m7a1cma44x2ac")))

(define-public crate-ckb-async-runtime-0.110.0 (c (n "ckb-async-runtime") (v "0.110.0") (d (list (d (n "ckb-logger") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.0") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zpcp89j94z90nj3x2safp92w0fbhb4f3z3rv2c1fpc4lk9f247x")))

(define-public crate-ckb-async-runtime-0.110.0-rc1 (c (n "ckb-async-runtime") (v "0.110.0-rc1") (d (list (d (n "ckb-logger") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09xzm2nq0v1xzvbdnxqdmfm8kkbpl3n362qj30m1m69igx3wv1pb")))

(define-public crate-ckb-async-runtime-0.111.0-rc2 (c (n "ckb-async-runtime") (v "0.111.0-rc2") (d (list (d (n "ckb-logger") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zr2k7hvbr2k23zj4vf9s2pi9xwl3r1ariafcwsim3d5sg1cdm3n")))

(define-public crate-ckb-async-runtime-0.111.0-rc3 (c (n "ckb-async-runtime") (v "0.111.0-rc3") (d (list (d (n "ckb-logger") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03wfx1zacrv49wh3xwd9r9y251bjn3vv6n8wcxv74w7ny0fm88gj")))

(define-public crate-ckb-async-runtime-0.111.0-rc4 (c (n "ckb-async-runtime") (v "0.111.0-rc4") (d (list (d (n "ckb-logger") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qhcqbwm9icyp10zdjvgv2qw7zf3w4bvanbh2w7wzfc9w9xybm7g")))

(define-public crate-ckb-async-runtime-0.111.0-rc5 (c (n "ckb-async-runtime") (v "0.111.0-rc5") (d (list (d (n "ckb-logger") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8swb44mbiihlh9hbc80xcvd5z1m9ad4llqj789cqi63ijvsscm")))

(define-public crate-ckb-async-runtime-0.111.0-rc6 (c (n "ckb-async-runtime") (v "0.111.0-rc6") (d (list (d (n "ckb-logger") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sdwlzz6hjblnvgz0b42p1hn2jhvdl4pvnvcd7ydy71l08lkfd7h")))

(define-public crate-ckb-async-runtime-0.111.0-rc7 (c (n "ckb-async-runtime") (v "0.111.0-rc7") (d (list (d (n "ckb-logger") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17r7rbabc1r1krhwlzm0inqi9fw32cfvpivngsbc7k5r89l90rsl")))

(define-public crate-ckb-async-runtime-0.111.0-rc8 (c (n "ckb-async-runtime") (v "0.111.0-rc8") (d (list (d (n "ckb-logger") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "11ix7kg7zyvns4dcqvbp5dlkh0camyjckflhp0m1hgcrlmsl21l6")))

(define-public crate-ckb-async-runtime-0.110.1-rc1 (c (n "ckb-async-runtime") (v "0.110.1-rc1") (d (list (d (n "ckb-logger") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0al5nyb7vbxisys8j6l42n43d56zws13iws4wy6cwrgni3mkjb1i")))

(define-public crate-ckb-async-runtime-0.110.1-rc2 (c (n "ckb-async-runtime") (v "0.110.1-rc2") (d (list (d (n "ckb-logger") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v387z7r94k98yr8l3w60ciflc4gxsfh4didshblr0m2df8199c2")))

(define-public crate-ckb-async-runtime-0.111.0-rc9 (c (n "ckb-async-runtime") (v "0.111.0-rc9") (d (list (d (n "ckb-logger") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19rcg3k1xq96g2s1zs8h8hp4b1difdix7s21n555qhgghv0hq4ya")))

(define-public crate-ckb-async-runtime-0.111.0-rc10 (c (n "ckb-async-runtime") (v "0.111.0-rc10") (d (list (d (n "ckb-logger") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0j0rqgcyc9jv4c02hi5ggd30bnhc0j9sx8y7m9gx02l05712y965")))

(define-public crate-ckb-async-runtime-0.110.1 (c (n "ckb-async-runtime") (v "0.110.1") (d (list (d (n "ckb-logger") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1flm9yf6945dj6hbf7dprgnhzz4xxwl2x34wpd52445kl1n0c3dz")))

(define-public crate-ckb-async-runtime-0.110.2-rc1 (c (n "ckb-async-runtime") (v "0.110.2-rc1") (d (list (d (n "ckb-logger") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxm9kr8gcgw99l8wi5ayb4cpy4klry9lzqs2kv1zz4bjvq7x1bf")))

(define-public crate-ckb-async-runtime-0.111.0-rc11 (c (n "ckb-async-runtime") (v "0.111.0-rc11") (d (list (d (n "ckb-logger") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19b53vjzpqizhi3r2dwnpnkr8yl7k9d2zazbyfs59g613ak42li7")))

(define-public crate-ckb-async-runtime-0.110.2-rc2 (c (n "ckb-async-runtime") (v "0.110.2-rc2") (d (list (d (n "ckb-logger") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06p8i5z2l1hc8vk45svbn13bfj7m2b39ymsl9a9ad1pjsm5n1c0p")))

(define-public crate-ckb-async-runtime-0.111.0-rc12 (c (n "ckb-async-runtime") (v "0.111.0-rc12") (d (list (d (n "ckb-logger") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0prbj1sq0q0hxwbk0l5g011zvbmpl40xnfdy974gd4xn0m7wnnh3")))

(define-public crate-ckb-async-runtime-0.110.2 (c (n "ckb-async-runtime") (v "0.110.2") (d (list (d (n "ckb-logger") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.110.2") (d #t) (k 0)) (d (n "ckb-stop-handler") (r "=0.110.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07r0640k1wpbqmf9xrwd2amh00l4rab662sqg03v48hsps947vxi")))

(define-public crate-ckb-async-runtime-0.111.0 (c (n "ckb-async-runtime") (v "0.111.0") (d (list (d (n "ckb-logger") (r "=0.111.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.111.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lk8481hmdxy8gx115j9vwvzxdmi6wdqj1dydc0h8brc4knz5wb6")))

(define-public crate-ckb-async-runtime-0.112.0-rc1 (c (n "ckb-async-runtime") (v "0.112.0-rc1") (d (list (d (n "ckb-logger") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "010q5nz2z4aqfk01zwrarpybilyld2aassgxm9py6q176fv7dny9")))

(define-public crate-ckb-async-runtime-0.112.0-rc2 (c (n "ckb-async-runtime") (v "0.112.0-rc2") (d (list (d (n "ckb-logger") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17fcnm4x6p6iq98j9xclfbgjrnwhw4xzmzwidhd5zp604knx9fyz")))

(define-public crate-ckb-async-runtime-0.112.0 (c (n "ckb-async-runtime") (v "0.112.0") (d (list (d (n "ckb-logger") (r "=0.112.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.112.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12ax26k000rf8pqs4fh40cz990yzwfif2w5mfyhc37009vdainsk")))

(define-public crate-ckb-async-runtime-0.112.0-rc3 (c (n "ckb-async-runtime") (v "0.112.0-rc3") (d (list (d (n "ckb-logger") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0shdvrk7q5iqqgrk7say5zj06mw6kbah9abc4vz8zx2wikz98047")))

(define-public crate-ckb-async-runtime-0.112.1 (c (n "ckb-async-runtime") (v "0.112.1") (d (list (d (n "ckb-logger") (r "=0.112.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.112.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15r4i56j9dykhv9ma0lgrqwdiq4p9prcjcdb3kyxpkv36i7zifh4")))

(define-public crate-ckb-async-runtime-0.113.0-rc1 (c (n "ckb-async-runtime") (v "0.113.0-rc1") (d (list (d (n "ckb-logger") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qrcya6ag4schjkb8x4ipbs9x6l050v6j9znlrjhkygi6d792bwj")))

(define-public crate-ckb-async-runtime-0.113.0-rc2 (c (n "ckb-async-runtime") (v "0.113.0-rc2") (d (list (d (n "ckb-logger") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vcqnz6khai4lbvx2p9586200lmyfvqj0xqw31y9wffxy8xdbzvm")))

(define-public crate-ckb-async-runtime-0.113.0-rc3 (c (n "ckb-async-runtime") (v "0.113.0-rc3") (d (list (d (n "ckb-logger") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1csk5axnyzh1znbas4rj956c5m0balafvp6gbhiv9vdsdjx1k8")))

(define-public crate-ckb-async-runtime-0.113.0 (c (n "ckb-async-runtime") (v "0.113.0") (d (list (d (n "ckb-logger") (r "=0.113.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1p0k26hamybj0zw8nvs9l0rvwf27ihp5m5cyl5bay8mvxl0wwhnm")))

(define-public crate-ckb-async-runtime-0.113.1-rc1 (c (n "ckb-async-runtime") (v "0.113.1-rc1") (d (list (d (n "ckb-logger") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jd78ihxma1r0n3y1ppcaah80ild49xy9ijvyai01mysfpqfhlcw")))

(define-public crate-ckb-async-runtime-0.114.0-rc1 (c (n "ckb-async-runtime") (v "0.114.0-rc1") (d (list (d (n "ckb-logger") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hhmg7xx2zq4wa8plbkq0c3nk72ia36zxqw746ldass76na9hssr")))

(define-public crate-ckb-async-runtime-0.113.1-rc2 (c (n "ckb-async-runtime") (v "0.113.1-rc2") (d (list (d (n "ckb-logger") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n507fzl8r3c0pj0ksjhbgb24sbrscrym9iq20lmahxihrxlbcv6")))

(define-public crate-ckb-async-runtime-0.113.1 (c (n "ckb-async-runtime") (v "0.113.1") (d (list (d (n "ckb-logger") (r "=0.113.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.113.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0m5250i0yc68skmkb7w07j9qg52mc2hm4q4ijkypdsgxswd2lxwj")))

(define-public crate-ckb-async-runtime-0.114.0-rc2 (c (n "ckb-async-runtime") (v "0.114.0-rc2") (d (list (d (n "ckb-logger") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0fddqpzij0wn140lj7dmr208h17iqin7c0g0gwcf4121cpg09vj7")))

(define-public crate-ckb-async-runtime-0.114.0-rc3 (c (n "ckb-async-runtime") (v "0.114.0-rc3") (d (list (d (n "ckb-logger") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yb8zr9qdsc89dbb8kxhiam5avxz20bkb7y8fvmyz9hdcs9i5di3")))

(define-public crate-ckb-async-runtime-0.114.0 (c (n "ckb-async-runtime") (v "0.114.0") (d (list (d (n "ckb-logger") (r "=0.114.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.114.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y66dd76cbq8v4rd1m2vj5kpv66iq56hkjh7ss14a2z9q6fry8jf")))

(define-public crate-ckb-async-runtime-0.115.0-pre (c (n "ckb-async-runtime") (v "0.115.0-pre") (d (list (d (n "ckb-logger") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9v0jpx2dzbcxc4l63fqsvys4kg085bizbg2a8x6p5kcaq4jazm")))

(define-public crate-ckb-async-runtime-0.115.0-rc1 (c (n "ckb-async-runtime") (v "0.115.0-rc1") (d (list (d (n "ckb-logger") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1h86llbqmvc1pdwhhv4bk20dl7rpy917s905sj6fcb24pyj1x0h1")))

(define-public crate-ckb-async-runtime-0.115.0-rc2 (c (n "ckb-async-runtime") (v "0.115.0-rc2") (d (list (d (n "ckb-logger") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1clpvspb1dqw0asgsmcgz4rr7kfphv7j4bgld9l0wnb63illc7w2")))

(define-public crate-ckb-async-runtime-0.115.0 (c (n "ckb-async-runtime") (v "0.115.0") (d (list (d (n "ckb-logger") (r "=0.115.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.115.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19siv1s1zkwlifz414wxz9kpmxa6s95zqzcskfv91l736mg3wl9a")))

(define-public crate-ckb-async-runtime-0.116.0-rc1 (c (n "ckb-async-runtime") (v "0.116.0-rc1") (d (list (d (n "ckb-logger") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1npy4sy54dp5lhjgbrr58zzc3qp04n1lsyk9rhajmj66wfkmxxca")))

(define-public crate-ckb-async-runtime-0.116.0-rc2 (c (n "ckb-async-runtime") (v "0.116.0-rc2") (d (list (d (n "ckb-logger") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1iyvs8nqm87a93q7z2hw88b97lf5y7ykf9agz7k0pbb6x2w79c3h")))

(define-public crate-ckb-async-runtime-0.116.0 (c (n "ckb-async-runtime") (v "0.116.0") (d (list (d (n "ckb-logger") (r "=0.116.0") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.116.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c5liwzfq4aavyldl91m6q8p4r7pbd5a7x8jwk86c4cyla20z14y") (y #t)))

(define-public crate-ckb-async-runtime-0.116.1 (c (n "ckb-async-runtime") (v "0.116.1") (d (list (d (n "ckb-logger") (r "=0.116.1") (d #t) (k 0)) (d (n "ckb-spawn") (r "=0.116.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sximxg1rr0fsymb3bx6p0br2p3aw8n3x8hq3wqf5fd09g21h86w")))

