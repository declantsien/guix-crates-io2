(define-module (crates-io ck b- ckb-fixed-hash-core) #:use-module (crates-io))

(define-public crate-ckb-fixed-hash-core-0.37.0-pre (c (n "ckb-fixed-hash-core") (v "0.37.0-pre") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "039viivwsi3bvlfmd4rrxpya09kq5xh9bdj77dzyiip4k6l59dgb")))

(define-public crate-ckb-fixed-hash-core-0.37.0 (c (n "ckb-fixed-hash-core") (v "0.37.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v0rjca4zlx4r49pfqj2m6vnmnilg6jyh2xdfmwgzpgd3bj675p2")))

(define-public crate-ckb-fixed-hash-core-0.38.0 (c (n "ckb-fixed-hash-core") (v "0.38.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lxyzc4n52xkpl9my572drjli0y44y0mnsyxjiaiyqlx7gcz8w3d")))

(define-public crate-ckb-fixed-hash-core-0.39.0 (c (n "ckb-fixed-hash-core") (v "0.39.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1q270ppabq5clbfwgf1zsf2l07z1c21xc2vgjnv6qzy0d8n1pj3x")))

(define-public crate-ckb-fixed-hash-core-0.39.1 (c (n "ckb-fixed-hash-core") (v "0.39.1") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0d91s2isqbqq2xp796jbs4lh0jrzf62987wv7gfdr7h3n1jqj5q5")))

(define-public crate-ckb-fixed-hash-core-0.40.0 (c (n "ckb-fixed-hash-core") (v "0.40.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1sj7kkhr2jkmi4yizk5wnl8i50nq0hyyzm8yiyfxxzcwjz0mf8az")))

(define-public crate-ckb-fixed-hash-core-0.42.0 (c (n "ckb-fixed-hash-core") (v "0.42.0") (d (list (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "00dxfyzjl0kigh3xsq0qpf0ypjnylqmgvskhfk30jymw7wsri0qb")))

(define-public crate-ckb-fixed-hash-core-0.43.0 (c (n "ckb-fixed-hash-core") (v "0.43.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0x0n81pv5wmbzzpfhg0rsdqikmwmlkj2w625qvi0q43wkvpajprc")))

(define-public crate-ckb-fixed-hash-core-0.100.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.100.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0blfl8qg1si4jf87j3vz2iq17ns39asnkny8af2gcm8vxqgxfidq")))

(define-public crate-ckb-fixed-hash-core-0.43.2 (c (n "ckb-fixed-hash-core") (v "0.43.2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ifnhdh5k07hwrxfi1zhh301ks7ywb9sf43chqc15y5960jw50y4")))

(define-public crate-ckb-fixed-hash-core-0.100.0-rc3 (c (n "ckb-fixed-hash-core") (v "0.100.0-rc3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1ww69rasdn2zhi5jsch3w693y9fj8y9vwzfmhjyscva4p47nda15") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.100.0-rc4 (c (n "ckb-fixed-hash-core") (v "0.100.0-rc4") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "01pmfv9i83lxj9xqh8kj12b6kws139vavsbcwi689gisqzrjympq")))

(define-public crate-ckb-fixed-hash-core-0.100.0-rc5 (c (n "ckb-fixed-hash-core") (v "0.100.0-rc5") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ld6nb23d5m5w7f98z415b30n2jy0xk2gy5sza1hbfiy49kx0r47")))

(define-public crate-ckb-fixed-hash-core-0.100.0 (c (n "ckb-fixed-hash-core") (v "0.100.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "08gpy3k8na7nwznk9jfhpqd2yp5167y7cmbq9pjhwlkldpcvnwg3")))

(define-public crate-ckb-fixed-hash-core-0.101.0 (c (n "ckb-fixed-hash-core") (v "0.101.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0njjbr4d2ylb7lfpxaj0w5bn5a05wakqpn4042g7yzpmvi8c8cr6")))

(define-public crate-ckb-fixed-hash-core-0.101.1 (c (n "ckb-fixed-hash-core") (v "0.101.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1pfcjvn6g5b7l3wbcn8casswszgs4c0qrm4640nkdlrzawa51qf6")))

(define-public crate-ckb-fixed-hash-core-0.101.2-testnet1 (c (n "ckb-fixed-hash-core") (v "0.101.2-testnet1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1p1xqvq90imq5lhd67mjsmi7p6rfkig8r8vhqpw7plc42d3pmxam")))

(define-public crate-ckb-fixed-hash-core-0.101.2 (c (n "ckb-fixed-hash-core") (v "0.101.2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "03xdx8arxf51wa4ybsxfmzkkvd9ilh8w9kzavp4lqlxrjrsvim39")))

(define-public crate-ckb-fixed-hash-core-0.101.3 (c (n "ckb-fixed-hash-core") (v "0.101.3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1v2pzb5l70szbfsg9nps8ai0zf6050gkqpcpfzrkpk476l8d27g0")))

(define-public crate-ckb-fixed-hash-core-0.101.4 (c (n "ckb-fixed-hash-core") (v "0.101.4") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "162ahh3d02nk25spkjhv0fl66pdj5bm669vwx3jgpxim0p6n2mfn")))

(define-public crate-ckb-fixed-hash-core-0.101.5 (c (n "ckb-fixed-hash-core") (v "0.101.5") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0g186p72bdj15avdjc26y31bsy8bz7z30zcqaff3qqf4hlbc54c3") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.101.6 (c (n "ckb-fixed-hash-core") (v "0.101.6") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0crknc11n1a4japg8pfcka899dc094bm1ll2fc3wmxlzjn5lmj2j")))

(define-public crate-ckb-fixed-hash-core-0.101.7 (c (n "ckb-fixed-hash-core") (v "0.101.7") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "02a8gx7fdgcwc0n85c3202zbvw3pk94yns6a2za7c7aaa2jimsvl")))

(define-public crate-ckb-fixed-hash-core-0.101.8 (c (n "ckb-fixed-hash-core") (v "0.101.8") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cm0bw1w9klk38g1674pr742p7c9s34r9s0z4imwgdgrvyv9jvb1")))

(define-public crate-ckb-fixed-hash-core-0.102.0 (c (n "ckb-fixed-hash-core") (v "0.102.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1j002qh57n1hydwwr28ghyz8y6h6rnpr6l111lsm7afnya9wsg6y") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.103.0 (c (n "ckb-fixed-hash-core") (v "0.103.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1k2xv7xprvg3fygqz6yg0ankqhk93jdkkrc8cz2ggl0fiflwviql")))

(define-public crate-ckb-fixed-hash-core-0.104.0 (c (n "ckb-fixed-hash-core") (v "0.104.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cj7mbpns39fashgzvbfmb1azkmjmwmahib0kfgwwx3ikzvais81")))

(define-public crate-ckb-fixed-hash-core-0.104.1 (c (n "ckb-fixed-hash-core") (v "0.104.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1dbbhnrnw2pndzkvdcilymxkja5flv1rc0x46s70f8ad4zjlab4m")))

(define-public crate-ckb-fixed-hash-core-0.105.0 (c (n "ckb-fixed-hash-core") (v "0.105.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0n9yg1in1gy9if5sxabhnjv0k6yil260dij7gshllj775j5xd0ig") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.105.1 (c (n "ckb-fixed-hash-core") (v "0.105.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0pmiz4qcvbwlh060w4isbl5swlx5208as5zf02li3zdvj7bbk4nx") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.106.0 (c (n "ckb-fixed-hash-core") (v "0.106.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0wrcfgnhsx3l9vdm4ivxn81j2r0yw2ri2q04cvmxxn1p9m35pca4")))

(define-public crate-ckb-fixed-hash-core-0.107.0 (c (n "ckb-fixed-hash-core") (v "0.107.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0falf7cv43yzgvyqcjllgiy95aw6jip7ss6grya2ic0bg60glnfn")))

(define-public crate-ckb-fixed-hash-core-0.108.0 (c (n "ckb-fixed-hash-core") (v "0.108.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0lgpadjhwbxvrpcpgbzyg470hhr0q3vcs12spss2arh29r568kng")))

(define-public crate-ckb-fixed-hash-core-0.108.1 (c (n "ckb-fixed-hash-core") (v "0.108.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0bsbwv331zdk8jcxk1i4kyvxjc5q6lbc904mjr1badrwwyalz32l")))

(define-public crate-ckb-fixed-hash-core-0.109.0 (c (n "ckb-fixed-hash-core") (v "0.109.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0ghmkvhgdaihxhrfx3kqrxqrqnxlwb0b8b7z7xspjkxq057w6r3w")))

(define-public crate-ckb-fixed-hash-core-0.110.0 (c (n "ckb-fixed-hash-core") (v "0.110.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1g3pw2r36qr8w5rc9asbv0xryfp1jigcnayvjashwfvydwg3rkx0")))

(define-public crate-ckb-fixed-hash-core-0.110.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.110.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1splskz92ydwjplnqh0y47lyzk2pphwfyc1527lw7mnkpxv38scz")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1cfmiib2mjjh80rs50qribwcdb5s00df3mw8k4r24mpcm89xxqsb")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc3 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0i952ncifdxnhyi7yh5wxpybw9mfwvnssmxxm0gmdlra92h5m9vj")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc4 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc4") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1bhlr5nbpkd2j42880rc8ll4id9dgg3ipcc14phczlyajbh9gz05")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc5 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc5") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1r697j092wdammyy3pxndrg401h4q1y2ivbr65sx5f70kfsjdg1r")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc6 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc6") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0d8jizh72pq1gdvybkmq1c47k79wxdxvg81x6rqhsbslq84x7k01")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc7 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc7") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0i5x33rivzfc6x9xlykqfkl3pg1mwl9ham39sgl2wvmi50xd0xy9")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc8 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc8") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1df0mrzfl3c72qlx7va15yijpl9khw2dhklqf0ggnvwp6dsvydah")))

(define-public crate-ckb-fixed-hash-core-0.110.1-rc1 (c (n "ckb-fixed-hash-core") (v "0.110.1-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1wm8vj3g73in9ajr8s4vgdng3xiv4pb95skbcmajjibadbc5rp7x")))

(define-public crate-ckb-fixed-hash-core-0.110.1-rc2 (c (n "ckb-fixed-hash-core") (v "0.110.1-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1snlgyp704plal1nb0smpfb9f11jbr7kjdrqqbfc4x6mixdkdvii")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc9 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc9") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1a8xr0q64128cmcjswdzcr6wydvalfynb6hhaj6y774y04ijl3gd")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc10 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc10") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1prnisfg89z3ippjzhhz76l4lmhxb322c4w4zni72sbv9lbml69n")))

(define-public crate-ckb-fixed-hash-core-0.110.1 (c (n "ckb-fixed-hash-core") (v "0.110.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0dqs5qcjjd0xfywl0q8526vxfgvwxjl93l0lsjlwych1q0brlfxa")))

(define-public crate-ckb-fixed-hash-core-0.110.2-rc1 (c (n "ckb-fixed-hash-core") (v "0.110.2-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1rh6ni0m9bcgbmm83kp5sb0w0h5jixhwzz8bsf0d65mdac10ls7v")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc11 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc11") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "086d4gyvsslhldcq4lbww6pjzbvjqfc5flj6s2rjnidrj08f538n")))

(define-public crate-ckb-fixed-hash-core-0.110.2-rc2 (c (n "ckb-fixed-hash-core") (v "0.110.2-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0nhj2bzghwg620va8zxzi4ii2i60ql7ihrp3rwqnbp816p8jfpg8")))

(define-public crate-ckb-fixed-hash-core-0.111.0-rc12 (c (n "ckb-fixed-hash-core") (v "0.111.0-rc12") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1386zjrkgckrp0fkps65jgm8r245zvw6l5sd5f0pwsgr87m5d9h1")))

(define-public crate-ckb-fixed-hash-core-0.110.2 (c (n "ckb-fixed-hash-core") (v "0.110.2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0f9h0bjkc0jhb9xspsdxcy5nsn7mbrgavhp1ybh0agq1gwdn7r8x")))

(define-public crate-ckb-fixed-hash-core-0.111.0 (c (n "ckb-fixed-hash-core") (v "0.111.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "105hihh0bgr5ky7l76kfz8wyk4py0zvixjq4nv50nkfddrx75ldx")))

(define-public crate-ckb-fixed-hash-core-0.112.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.112.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0rfd8kndmi09d5hr7j0aj7w6r36cz9izv2spml5s08d8js1ahyhc")))

(define-public crate-ckb-fixed-hash-core-0.112.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.112.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "08l07n4p7ykivlwz15slffniwz3g0m2cn3y19l5md7776wmkx6ia")))

(define-public crate-ckb-fixed-hash-core-0.112.0 (c (n "ckb-fixed-hash-core") (v "0.112.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1z7p3mx0xq0i3k7277c0db9jr2jl72bvgsb8kdrp9lx2kbcrra2z")))

(define-public crate-ckb-fixed-hash-core-0.112.0-rc3 (c (n "ckb-fixed-hash-core") (v "0.112.0-rc3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1jwld5yidlb7w98q4gx6i8jw7inzlfrlz323a39a5hlqzia8k3ir")))

(define-public crate-ckb-fixed-hash-core-0.112.1 (c (n "ckb-fixed-hash-core") (v "0.112.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0kzbghddzkx2b7yy27kx8h5jjyvlvqh5060kb7346mlbmi82iz2z")))

(define-public crate-ckb-fixed-hash-core-0.113.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.113.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1dpwlki3l1ahwbwnbnpasgrx13rcpk028lrkxbl6jzkmbihlz44y")))

(define-public crate-ckb-fixed-hash-core-0.113.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.113.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1cr20fg8rz6xjqz86r5wjb9w845ry1rbbg2k5nnzvqd9w70v8bxp")))

(define-public crate-ckb-fixed-hash-core-0.113.0-rc3 (c (n "ckb-fixed-hash-core") (v "0.113.0-rc3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "09q037whb9a3ab4l4lsf95lh8akq2qvj7wvyxc8i008qzx6q4mfl")))

(define-public crate-ckb-fixed-hash-core-0.113.0 (c (n "ckb-fixed-hash-core") (v "0.113.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0a544jn9cf4hxi3mvladqvz19wy3dnf3jzssk716bkln5vaxg0n5")))

(define-public crate-ckb-fixed-hash-core-0.113.1-rc1 (c (n "ckb-fixed-hash-core") (v "0.113.1-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1llkb7nrwyv4v7bs2dg3zklibgcxs5gwj1n2jjyxp7vrz9qwzib9")))

(define-public crate-ckb-fixed-hash-core-0.114.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.114.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "05v3q7hibwp0x3vv7blyqmbvrp0n3lfpgjhqmxgz28jdvp4pab3i")))

(define-public crate-ckb-fixed-hash-core-0.113.1-rc2 (c (n "ckb-fixed-hash-core") (v "0.113.1-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0i27jyv2yldw9z89ng9188f27329caa7z0l25242pyfzkf5d943k")))

(define-public crate-ckb-fixed-hash-core-0.113.1 (c (n "ckb-fixed-hash-core") (v "0.113.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "008c95i7cfdjyhbcmnywjkaas8fxllsdng5llrz9c1785slh7h5m")))

(define-public crate-ckb-fixed-hash-core-0.114.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.114.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0jwyi44ak8b9f3padjrfn4w7vwlzdg5bihm2wmvdwf12p9v2pqx6")))

(define-public crate-ckb-fixed-hash-core-0.114.0-rc3 (c (n "ckb-fixed-hash-core") (v "0.114.0-rc3") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "10kqngym4wkyg1r43mnafh30bg4hvg09241y3c0dllndqrb9vyl3")))

(define-public crate-ckb-fixed-hash-core-0.114.0 (c (n "ckb-fixed-hash-core") (v "0.114.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "08m84yhs8vszlp8gb2x4lhjv4iky88gqdbf9rmwfbijsqldkvdk4")))

(define-public crate-ckb-fixed-hash-core-0.115.0-pre (c (n "ckb-fixed-hash-core") (v "0.115.0-pre") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0w4m64wyra469d3ap8fymdvvh0gzd8smcpy8rjcp13l8vnavbls3")))

(define-public crate-ckb-fixed-hash-core-0.115.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.115.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1d87czc0khkxyh4h0c05qg8lgnqdrb1xk5j06jc97pxdnzlalmal")))

(define-public crate-ckb-fixed-hash-core-0.115.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.115.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0csrhr99k073yfxfdsy6lbw73zfywzkl1vzy97za1ybr6k3qrr0d")))

(define-public crate-ckb-fixed-hash-core-0.115.0 (c (n "ckb-fixed-hash-core") (v "0.115.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "147yg08ralyv6hjcqncg0g095afj714p509m7i7xckrzqs6yicb5")))

(define-public crate-ckb-fixed-hash-core-0.116.0-rc1 (c (n "ckb-fixed-hash-core") (v "0.116.0-rc1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0iwfxbzr50r4in0j82xcy5i1q3668x579ksbnfps5cfg3k4pa58v")))

(define-public crate-ckb-fixed-hash-core-0.116.0-rc2 (c (n "ckb-fixed-hash-core") (v "0.116.0-rc2") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0qhnj9g77s4pwz1kmxazmn5b5y4knkm7ph15shhf8rh6yn399hzc")))

(define-public crate-ckb-fixed-hash-core-0.116.0 (c (n "ckb-fixed-hash-core") (v "0.116.0") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1fixi9byck9ycnnxp2p8h7h4909gijdnvda29gxdnzaxw80ds034") (y #t)))

(define-public crate-ckb-fixed-hash-core-0.116.1 (c (n "ckb-fixed-hash-core") (v "0.116.1") (d (list (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1y0cqx2gr2nha525svalv8lv1dmcqlypvd0dzlnf9dmrxlzzc2cm")))

