(define-module (crates-io ck b- ckb-spawn) #:use-module (crates-io))

(define-public crate-ckb-spawn-0.39.0 (c (n "ckb-spawn") (v "0.39.0") (h "11ayqrbnxrd9qmq43yyv8l1w34w5bxymin8h3nmmird4yys6afnn")))

(define-public crate-ckb-spawn-0.39.1 (c (n "ckb-spawn") (v "0.39.1") (h "1im4biy8rvxs4qxdsq50i51pv09vcf3jvc8skvva94i1acszanv4")))

(define-public crate-ckb-spawn-0.40.0 (c (n "ckb-spawn") (v "0.40.0") (h "1rjzsr7ldfkva64f4mhcxm57bknilwavfw5shmzx1vh3fi3ljbm4")))

(define-public crate-ckb-spawn-0.42.0 (c (n "ckb-spawn") (v "0.42.0") (h "0haa9a4wbq8826fln1lxn793jvw7x36a4lld7kyvm380h8f70ilr")))

(define-public crate-ckb-spawn-0.43.0 (c (n "ckb-spawn") (v "0.43.0") (h "049sa5ffhbmdpqvgqc7lp6zrqmn0f0m8xbs7jafzmggb1hcqxibp")))

(define-public crate-ckb-spawn-0.100.0-rc2 (c (n "ckb-spawn") (v "0.100.0-rc2") (h "1hc58plz9hga7mfwgfr7cbvj4i96pc8wcdix1mfn24mwkw2gwiha")))

(define-public crate-ckb-spawn-0.43.2 (c (n "ckb-spawn") (v "0.43.2") (h "068kxhygvhpxwchi8lg2gk3c0d041mi4m3nbzm0c3xhjnk8065x8")))

(define-public crate-ckb-spawn-0.100.0-rc3 (c (n "ckb-spawn") (v "0.100.0-rc3") (h "04pmivbzilag5ni5wmkm9zfw06h289bbv80q76jz3y4cxmasbrfc") (y #t)))

(define-public crate-ckb-spawn-0.100.0-rc4 (c (n "ckb-spawn") (v "0.100.0-rc4") (h "020kkfqf3mnc0wa02m0zq788yhzm0sg8dfdq7azq15ca14l6b28r")))

(define-public crate-ckb-spawn-0.100.0-rc5 (c (n "ckb-spawn") (v "0.100.0-rc5") (h "0j9rkm84dv3sj93jb6pc8hd9kfl4ldxd3gvjcyzwwjaw6pbcirgx")))

(define-public crate-ckb-spawn-0.100.0 (c (n "ckb-spawn") (v "0.100.0") (h "0kk16nm5adsiwx8v2b2m78ka5azb9yjk5v6bg1z2nrmzrj9iwsy0")))

(define-public crate-ckb-spawn-0.101.0 (c (n "ckb-spawn") (v "0.101.0") (h "1wb3r77hnhmdrzm6v5gdmcdmplw98paw15mqdxis10zgxk061fd7")))

(define-public crate-ckb-spawn-0.101.1 (c (n "ckb-spawn") (v "0.101.1") (h "0sxc8bvzfxfkzw1v1lp4vg2w9nzp921kznd86di2cq6p8b9g58s3")))

(define-public crate-ckb-spawn-0.101.2-testnet1 (c (n "ckb-spawn") (v "0.101.2-testnet1") (h "18slv4hhaxc0qrrprb3dik88j29l6hqdn2ygjn0iv5by2v0v6lbw")))

(define-public crate-ckb-spawn-0.101.2 (c (n "ckb-spawn") (v "0.101.2") (h "0fqalkdwxr0azlnrhck35by5bh5768bbnbl3kxsrymyxpjw5l96h")))

(define-public crate-ckb-spawn-0.101.3 (c (n "ckb-spawn") (v "0.101.3") (h "0pcl2zh6cfymgkfgnvc32mjkvjb2kav6mpqb2s1b3zqjhf60aq91")))

(define-public crate-ckb-spawn-0.101.4 (c (n "ckb-spawn") (v "0.101.4") (h "15pv1wsz9rdl1g92xwyqpy5rd1r3m50gd6vblnhl3sd9rb4jf2ja")))

(define-public crate-ckb-spawn-0.101.5 (c (n "ckb-spawn") (v "0.101.5") (h "0wdy581ws8sk1byw3wy1jn6p9dxn7wpzqq6n05fdxbqp1732s8a7") (y #t)))

(define-public crate-ckb-spawn-0.101.6 (c (n "ckb-spawn") (v "0.101.6") (h "0sd0i5y40qz0kx1yzpdxakbar9v8pp0gcnljlf97jli0gn27n6na")))

(define-public crate-ckb-spawn-0.101.7 (c (n "ckb-spawn") (v "0.101.7") (h "0ixmm6d8gd5rlq08pqkx3w2fk6k18a2chxb0k08003wpil80jfnv")))

(define-public crate-ckb-spawn-0.101.8 (c (n "ckb-spawn") (v "0.101.8") (h "0ggfkd4zsjjvygx4rskv4kw6l2q8f2xnf1d7cjb3g2agzrshf056")))

(define-public crate-ckb-spawn-0.102.0 (c (n "ckb-spawn") (v "0.102.0") (h "1m0677d8wmk6pl73i5n9bzfl1mqwg4wzjm3bv7k20xryvkk71zfk") (y #t)))

(define-public crate-ckb-spawn-0.103.0 (c (n "ckb-spawn") (v "0.103.0") (h "1q716zwr5h2rvxni6mqw99a1wz7wag8qagss6ap0ki272vh666q8")))

(define-public crate-ckb-spawn-0.104.0 (c (n "ckb-spawn") (v "0.104.0") (h "06j8dl8dgf9d62hmgl0wq71kxkm2arba3307nxywxkkc28ky1pnk")))

(define-public crate-ckb-spawn-0.104.1 (c (n "ckb-spawn") (v "0.104.1") (h "0yxg9k8yfshv7rqx6aanh4bkrnmafypd0vvav4lj180rfq3shkck")))

(define-public crate-ckb-spawn-0.105.0 (c (n "ckb-spawn") (v "0.105.0") (h "1znjkqa0rwlq4b5gihmwab80jla2q6cfg782zq97xxwiqfbh6fy5") (y #t)))

(define-public crate-ckb-spawn-0.105.1 (c (n "ckb-spawn") (v "0.105.1") (h "1ihqbh5g9m86rw99kzm1hya6im3ckvy9anxpcy6z0pjcf6fprjfd") (y #t)))

(define-public crate-ckb-spawn-0.106.0 (c (n "ckb-spawn") (v "0.106.0") (h "171643jv5nbsc4qgl42wifp7fiqibn14si2i5qcb9kikrz1azh1d")))

(define-public crate-ckb-spawn-0.107.0 (c (n "ckb-spawn") (v "0.107.0") (h "15xjkq6xsay06lyayibqzs1sxx4wsrgfk85lnwnpjkhmk8czsyi8")))

(define-public crate-ckb-spawn-0.108.0 (c (n "ckb-spawn") (v "0.108.0") (h "12nds51xp01mijcmcfhr4ws4l476lwswj1xl8d63g7rba9xlz83r")))

(define-public crate-ckb-spawn-0.108.1 (c (n "ckb-spawn") (v "0.108.1") (h "1zv1sd8n7v4fwbps0lzing4d805ykx9mmr6hwd6fycayc1qxpi16")))

(define-public crate-ckb-spawn-0.109.0 (c (n "ckb-spawn") (v "0.109.0") (h "1amsf2fmvbrp54wldvvszg281vypmmm2yl9ml87xxzrdqwdggihv")))

(define-public crate-ckb-spawn-0.110.0 (c (n "ckb-spawn") (v "0.110.0") (h "0sxnq466q0jjlgd83nxf3fn04d6nimymdfg29jyj2kdqsbnfs23v")))

(define-public crate-ckb-spawn-0.110.0-rc1 (c (n "ckb-spawn") (v "0.110.0-rc1") (h "15vyf84hv3sjbklrnx4iazjw0dya40sjjyc38gf2vys0qc5q464j")))

(define-public crate-ckb-spawn-0.111.0-rc2 (c (n "ckb-spawn") (v "0.111.0-rc2") (h "0pcgs7sfsdpja40ia9zkv2s3zbp5412kr478zipkf2898wcxdbah")))

(define-public crate-ckb-spawn-0.111.0-rc3 (c (n "ckb-spawn") (v "0.111.0-rc3") (h "1r5pd2w404plmmv3xx2i4idjzxn1qh6rc22d11371q34yqy6i246")))

(define-public crate-ckb-spawn-0.111.0-rc4 (c (n "ckb-spawn") (v "0.111.0-rc4") (h "1z4zzlnnc7s731i7zz727y0xdw7yp8qifaxryv13b97qcir76k3q")))

(define-public crate-ckb-spawn-0.111.0-rc5 (c (n "ckb-spawn") (v "0.111.0-rc5") (h "1kyssjxvlda439iy3qdbmd9b6dkdgpxxhchz2sslhp51kqjnvnwm")))

(define-public crate-ckb-spawn-0.111.0-rc6 (c (n "ckb-spawn") (v "0.111.0-rc6") (h "13aqzzky3jryvn2hs124d1slid8s0yy2v6zhwa5653cjlkb63xzd")))

(define-public crate-ckb-spawn-0.111.0-rc7 (c (n "ckb-spawn") (v "0.111.0-rc7") (h "0d58pj8g9zjmqzvzzax0d7xpjglq9rjr9chdcyhk3gray9bv8wp1")))

(define-public crate-ckb-spawn-0.111.0-rc8 (c (n "ckb-spawn") (v "0.111.0-rc8") (h "054ig11jjjq42psjqyhk8y28y1r75zjp2vcbjc0lp2fki3lwyqsw")))

(define-public crate-ckb-spawn-0.110.1-rc1 (c (n "ckb-spawn") (v "0.110.1-rc1") (h "17s64crgv85jc9h76hphbklbgzll7m1vnpqcpcgb0jv0mrjvdmc9")))

(define-public crate-ckb-spawn-0.110.1-rc2 (c (n "ckb-spawn") (v "0.110.1-rc2") (h "1qqhd3wl4icnam02r00ihljgq8dqp8rpkz5gymy02840dd4ajh7w")))

(define-public crate-ckb-spawn-0.111.0-rc9 (c (n "ckb-spawn") (v "0.111.0-rc9") (h "0r4xrvqw642cpynyjvwbpnl501nv72mc8c8v1yf96sgk5xhp7khi")))

(define-public crate-ckb-spawn-0.111.0-rc10 (c (n "ckb-spawn") (v "0.111.0-rc10") (h "0kx2qkhwmsggymqivi6mn8q893274rzm4vnafrbj0vrz93kbaaar")))

(define-public crate-ckb-spawn-0.110.1 (c (n "ckb-spawn") (v "0.110.1") (h "0v55k01x19wv10wn6p2cn2llb5b5mcy64i6xab2mzc2w96jbi0f0")))

(define-public crate-ckb-spawn-0.110.2-rc1 (c (n "ckb-spawn") (v "0.110.2-rc1") (h "1rfszlj7r56v58v1id58mby38v6zdh3g66ib4gqqm3isq86jx04a")))

(define-public crate-ckb-spawn-0.111.0-rc11 (c (n "ckb-spawn") (v "0.111.0-rc11") (h "0g6mngdqji0lw0c5p1mqg6s2q3kxlbwdgwm3hjhndnlqzsxbllvk")))

(define-public crate-ckb-spawn-0.110.2-rc2 (c (n "ckb-spawn") (v "0.110.2-rc2") (h "12gs9zivsfg7fwjkgdy3plxb4fv13z2l00msi0x29zq66cm2brdg")))

(define-public crate-ckb-spawn-0.111.0-rc12 (c (n "ckb-spawn") (v "0.111.0-rc12") (h "186war4im42viqwkd47mc3py6iycv17yr3mdgwfrlr39cqd48wpz")))

(define-public crate-ckb-spawn-0.110.2 (c (n "ckb-spawn") (v "0.110.2") (h "0lbhkncvy4kxqrxp1chmnn1jh3gqmnqc1l4g50p52a2c332f8cnx")))

(define-public crate-ckb-spawn-0.111.0 (c (n "ckb-spawn") (v "0.111.0") (h "1if7j4frb72r0qfz8kya3i6jzzabjb5bsby6axx1x90hn9xlzllk")))

(define-public crate-ckb-spawn-0.112.0-rc1 (c (n "ckb-spawn") (v "0.112.0-rc1") (h "02rfp9j9xy3v9pzpyhv2bjghwslnp46iff7qphjxml9qf47nzx0c")))

(define-public crate-ckb-spawn-0.112.0-rc2 (c (n "ckb-spawn") (v "0.112.0-rc2") (h "07xc83nkw82jpsp6c1w78p2nf2zms6bxi2firk7bqw8rvk489dhd")))

(define-public crate-ckb-spawn-0.112.0 (c (n "ckb-spawn") (v "0.112.0") (h "0xipz1qkl2534930rjcxc4lwqi7c4zx0cpvccj5d73kpzs6wbch0")))

(define-public crate-ckb-spawn-0.112.0-rc3 (c (n "ckb-spawn") (v "0.112.0-rc3") (h "0f63cmqbqq7dcvbb1xqrpnmr1kmv1j95l5w6b6hhay9i9jwnb85r")))

(define-public crate-ckb-spawn-0.112.1 (c (n "ckb-spawn") (v "0.112.1") (h "158ly1yarpn5xqlk08dqryqymqq8kmjf8pnvwfba648k83h4ia4c")))

(define-public crate-ckb-spawn-0.113.0-rc1 (c (n "ckb-spawn") (v "0.113.0-rc1") (h "0cbmc0lbh2v0gpgcgjq0p3z2nmbp70323brb9qcmlsw0abnn3z3w")))

(define-public crate-ckb-spawn-0.113.0-rc2 (c (n "ckb-spawn") (v "0.113.0-rc2") (h "0k5xfvwxv2ykq9a7shj59hw89avy5m4r50xczp78ld1gdidwx55g")))

(define-public crate-ckb-spawn-0.113.0-rc3 (c (n "ckb-spawn") (v "0.113.0-rc3") (h "0nz2i2nf1j01z1m24mj3rr5ac8qc3msp6sa3pjw5p6amq592q8gd")))

(define-public crate-ckb-spawn-0.113.0 (c (n "ckb-spawn") (v "0.113.0") (h "0njqb2hkkn98d56njvyficd6yd0v9a807lf2zmj4k91nbdkjgxa8")))

(define-public crate-ckb-spawn-0.113.1-rc1 (c (n "ckb-spawn") (v "0.113.1-rc1") (h "0g7hmcpr4nv7f0b8iwphcqsb4kpzrix2khh1139c4w948szq3h99")))

(define-public crate-ckb-spawn-0.114.0-rc1 (c (n "ckb-spawn") (v "0.114.0-rc1") (h "11xj7z3hyl4vmnc81q88i5ahwnxd3xr0a9942n9i3gxjyi7lg5px")))

(define-public crate-ckb-spawn-0.113.1-rc2 (c (n "ckb-spawn") (v "0.113.1-rc2") (h "0r45qaqwmfh7pr8yhbzxc11cf1kl83pi8vz6gz9nxwr38rg7n3z3")))

(define-public crate-ckb-spawn-0.113.1 (c (n "ckb-spawn") (v "0.113.1") (h "08rhzymvgcaiwgb1ph0ya50n7f8marc88r0mcff7qv9jc6za4fmj")))

(define-public crate-ckb-spawn-0.114.0-rc2 (c (n "ckb-spawn") (v "0.114.0-rc2") (h "0w33m035j9wmpc621g6c78djdgslg0244bmmg7kh9943mrlh45q2")))

(define-public crate-ckb-spawn-0.114.0-rc3 (c (n "ckb-spawn") (v "0.114.0-rc3") (h "100rzblyz32mr31g40n4vbgk65piz779jw9wbsp73da2ryx6qsbv")))

(define-public crate-ckb-spawn-0.114.0 (c (n "ckb-spawn") (v "0.114.0") (h "1mkbszkrhk2if3z9c8p1kpsvlc3z29dn3w664zh7ys7l69mppnfj")))

(define-public crate-ckb-spawn-0.115.0-pre (c (n "ckb-spawn") (v "0.115.0-pre") (h "0l39fdsk9mkrxppz3qjgkhcwpw1p65f6i3prbfww7lrwz1v0294y")))

(define-public crate-ckb-spawn-0.115.0-rc1 (c (n "ckb-spawn") (v "0.115.0-rc1") (h "1zsv1l6n6nkgdnj1gkkn21jibk4siwlr1hglphvwwmx01w6pihpd")))

(define-public crate-ckb-spawn-0.115.0-rc2 (c (n "ckb-spawn") (v "0.115.0-rc2") (h "09vri63jw6h6hjr0yv5xlsamygj1bm55gw17ngnbb90yp767v57v")))

(define-public crate-ckb-spawn-0.115.0 (c (n "ckb-spawn") (v "0.115.0") (h "0bcxhm981hsnzv2g0g1xy5h35477prs1hlw61sb41aj726ik5q38")))

(define-public crate-ckb-spawn-0.116.0-rc1 (c (n "ckb-spawn") (v "0.116.0-rc1") (h "11cx76cnan0s4xs2ghfn7cdk17zp1gzzc9h412rwq7q6cd37xxyq")))

(define-public crate-ckb-spawn-0.116.0-rc2 (c (n "ckb-spawn") (v "0.116.0-rc2") (h "03jn2pdfhji68z40zz3m3z1jslv4d4d313dn4dm904manv2y86dc")))

(define-public crate-ckb-spawn-0.116.0 (c (n "ckb-spawn") (v "0.116.0") (h "0kpp2b9psb6gvz5wwydijljy4qrb6qj1myf33pj6x0z7qy65s9wp") (y #t)))

(define-public crate-ckb-spawn-0.116.1 (c (n "ckb-spawn") (v "0.116.1") (h "0xhasn3hg3d97cz40yrlzc0m9fzxkx3di8w9hc5gz2lbphg4szb3")))

