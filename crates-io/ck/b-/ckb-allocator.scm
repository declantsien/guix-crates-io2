(define-module (crates-io ck b- ckb-allocator) #:use-module (crates-io))

(define-public crate-ckb-allocator-0.1.0 (c (n "ckb-allocator") (v "0.1.0") (d (list (d (n "buddy-alloc") (r "^0.2.0") (d #t) (k 0)))) (h "0ia8akk3ycv347xwrwh52afdalx2dhzhhx8y6qpdcs6xidx27fys")))

(define-public crate-ckb-allocator-0.1.1 (c (n "ckb-allocator") (v "0.1.1") (d (list (d (n "buddy-alloc") (r "^0.3") (d #t) (k 0)))) (h "1k72r1qd1bw1gj3vkyc415g4wx2b1k3n5lzn8zahbqckbm0f0b2p")))

