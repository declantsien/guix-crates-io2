(define-module (crates-io ck b- ckb-logger) #:use-module (crates-io))

(define-public crate-ckb-logger-0.37.0-pre (c (n "ckb-logger") (v "0.37.0-pre") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "184nn1l2sla14q31d9zk6vmjz6l9f5mgbhi4rsdqxnagwzg48zbb")))

(define-public crate-ckb-logger-0.37.0 (c (n "ckb-logger") (v "0.37.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rg39wd5i7g53kzhx2kgg5a6c2bd6cxs3y6y8prx5fsdqwb324g7")))

(define-public crate-ckb-logger-0.38.0 (c (n "ckb-logger") (v "0.38.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l96nmlsd5zz08lhwqdyxp45gv1q8x583nsqgr0jh2g2v4g65yk8")))

(define-public crate-ckb-logger-0.39.0 (c (n "ckb-logger") (v "0.39.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01k94ykfz55jdra27ba3v04qpfbla9qyfyvqmp6clfvpss5m16ph")))

(define-public crate-ckb-logger-0.39.1 (c (n "ckb-logger") (v "0.39.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1a8c37pas5ahaj6pzxi0j3m4akr80zzlvxqqijga97ybsqm1xarg")))

(define-public crate-ckb-logger-0.40.0 (c (n "ckb-logger") (v "0.40.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09s6qmd2i8x70lc44j31wkj8ylpldiqif4nkm3c10xnzyzvcw1m4")))

(define-public crate-ckb-logger-0.42.0 (c (n "ckb-logger") (v "0.42.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00rg474jcqnjw9cygqyqmd8bj6903gmyxxivrnvnh7xns3wy2akk")))

(define-public crate-ckb-logger-0.43.0 (c (n "ckb-logger") (v "0.43.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19ricfxy61ymm9hl3ikpxz0pvp4mmr4dmyc5fhrnq67aj4m4yki8")))

(define-public crate-ckb-logger-0.100.0-rc2 (c (n "ckb-logger") (v "0.100.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kwjviwg748cixxlisvcq3rvbrpmz84hd34bmpgfbl0gp74gzzhl")))

(define-public crate-ckb-logger-0.43.2 (c (n "ckb-logger") (v "0.43.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y1dsrw0z49xa4w24kzk1nryn19cy9g08gxg0071dq9l1nf7wss9")))

(define-public crate-ckb-logger-0.100.0-rc3 (c (n "ckb-logger") (v "0.100.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0576gzkj1crnrmjhyl5v5nink80zqzg90qviw635s8gzpa84ifpw") (y #t)))

(define-public crate-ckb-logger-0.100.0-rc4 (c (n "ckb-logger") (v "0.100.0-rc4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0zhlqj8v0vvw4w24ii7sn39yappn4xz27vii9dgldgfiy6bcx72y")))

(define-public crate-ckb-logger-0.100.0-rc5 (c (n "ckb-logger") (v "0.100.0-rc5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vhq3dfmggri0hhynkbcpp55fkmlqhb3a9km1nlddj145cfs4bbm")))

(define-public crate-ckb-logger-0.100.0 (c (n "ckb-logger") (v "0.100.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1sd1lasvzn83b251xkcmzj0fl3hi4w1cilpmil0v26ixpr2mz8j8")))

(define-public crate-ckb-logger-0.101.0 (c (n "ckb-logger") (v "0.101.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qgkxkfha3zxx51ar4wkhapcv44ia72hfd3dslwfy9s1akv4m0cv")))

(define-public crate-ckb-logger-0.101.1 (c (n "ckb-logger") (v "0.101.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y40h14psicj7cs8g5p0slzr6kg9kn26dd49zp284q1lp3c2w7ry")))

(define-public crate-ckb-logger-0.101.2-testnet1 (c (n "ckb-logger") (v "0.101.2-testnet1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "189zc11skx17xmpqd7891gykwzs1ill3v0zwsx8v30bdx6y4y32h")))

(define-public crate-ckb-logger-0.101.2 (c (n "ckb-logger") (v "0.101.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12z901zdf0anb6lk3ikk230xnz8b4rps0qm22fj3vny6dwj2jrkp")))

(define-public crate-ckb-logger-0.101.3 (c (n "ckb-logger") (v "0.101.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dg1y46ai0y6kqgxmdxm0yx8jcrd52s23l3ap3w2fgjp12q00047")))

(define-public crate-ckb-logger-0.101.4 (c (n "ckb-logger") (v "0.101.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0n4xw5waw4qibn5y4pmahd725v14vbdbxp5cbzrkc2hij600andn")))

(define-public crate-ckb-logger-0.101.5 (c (n "ckb-logger") (v "0.101.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h9alyj4jffvy01gixz52bmafhzc9mql6vcd9457spa9fqfjcp0d") (y #t)))

(define-public crate-ckb-logger-0.101.6 (c (n "ckb-logger") (v "0.101.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09zk7i9a0x57g1fjncg89nz4advmwacx1viwsm9f2n43xgvr6n7h")))

(define-public crate-ckb-logger-0.101.7 (c (n "ckb-logger") (v "0.101.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08agv3rpz62bisym7zlrpkpcb0rqv7mjrq7pspjbzpcrfbfwipfr")))

(define-public crate-ckb-logger-0.101.8 (c (n "ckb-logger") (v "0.101.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y53xl2a21wwv076am16ffycqpgmw7shz77qkkpxaib8xpb8scsc")))

(define-public crate-ckb-logger-0.102.0 (c (n "ckb-logger") (v "0.102.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iy19j78lvs4irqs5q14mpxv9my04sdqxcqa6x8k81950a1sffcw") (y #t)))

(define-public crate-ckb-logger-0.103.0 (c (n "ckb-logger") (v "0.103.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p1djml3d45zyjcnd3lvlx21bjbh90xla62ljiqy2klw3fhvagd7")))

(define-public crate-ckb-logger-0.104.0 (c (n "ckb-logger") (v "0.104.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rla4h3vym2q6xgdsx26smjmkim0an6x2wfbsgrr5yl2ncm9x0yn")))

(define-public crate-ckb-logger-0.104.1 (c (n "ckb-logger") (v "0.104.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z5dsb43yl77yrb1nhsmns5a3v4c204mgl87lsa2mrxnm4h0xclc")))

(define-public crate-ckb-logger-0.105.0 (c (n "ckb-logger") (v "0.105.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hpr4i1c9772psi6855dvigyw857xrs4pndlwm9wi2jn0xs2glcl") (y #t)))

(define-public crate-ckb-logger-0.105.1 (c (n "ckb-logger") (v "0.105.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1smhzx1zs7d2fqn215m8jx9biqy9sg24ln7qkk4mv9bvvy024cca") (y #t)))

(define-public crate-ckb-logger-0.106.0 (c (n "ckb-logger") (v "0.106.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kqhhnsa9bh4ikypnrrabvcdghs29mx6kbqp9nv6n23zsrnl07gy")))

(define-public crate-ckb-logger-0.107.0 (c (n "ckb-logger") (v "0.107.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "014xc721b0kv98rn4d059hmqgs42m5pl4j6pkn66fvzy4ypc2lss")))

(define-public crate-ckb-logger-0.108.0 (c (n "ckb-logger") (v "0.108.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fr5n4q216pw3hxwjs96iksk3qmvja83ajsiizd7hbzqvnalc74i")))

(define-public crate-ckb-logger-0.108.1 (c (n "ckb-logger") (v "0.108.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00qqqbssd43amlqfz69f0l13pfphrl5vpbp2167qivc1y9x5bc63")))

(define-public crate-ckb-logger-0.109.0 (c (n "ckb-logger") (v "0.109.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06qs9clscsi5m23mmwy9r23snq7klgcyn8z3f27jlsj1yqb9a6bd")))

(define-public crate-ckb-logger-0.110.0 (c (n "ckb-logger") (v "0.110.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0f178qs444r8cqbbfsfg918cpwr2zna0nd8m6fjfdvmkkahjwxzj")))

(define-public crate-ckb-logger-0.110.0-rc1 (c (n "ckb-logger") (v "0.110.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h6w30x7gk2bfnkwmf6rn8iqj1q18zb9jj4zisxifmm4dkdhsvpg")))

(define-public crate-ckb-logger-0.111.0-rc2 (c (n "ckb-logger") (v "0.111.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "133535r1gs0y2zi8ipwqkiiqza1iwdffnd3802p0y3cl7n2dh6ig")))

(define-public crate-ckb-logger-0.111.0-rc3 (c (n "ckb-logger") (v "0.111.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07hkxc2ay5dkl2y45m2r6znf8m5zgvv6b7zgsvx0fphglvkyyl7j")))

(define-public crate-ckb-logger-0.111.0-rc4 (c (n "ckb-logger") (v "0.111.0-rc4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1d1ss498m2mnxvc2djj4lwzk33fmcmva4nk70h13f14i3ybdx9ff")))

(define-public crate-ckb-logger-0.111.0-rc5 (c (n "ckb-logger") (v "0.111.0-rc5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vx17k547jzsjmqsf0vnyyigvz314nvb2qgy2b1m98cv5rqhaxsh")))

(define-public crate-ckb-logger-0.111.0-rc6 (c (n "ckb-logger") (v "0.111.0-rc6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "01nwh53fm3n9k0d9zcfv9abzix0ydva9dnl69cgkvpv4k7pw8mfb")))

(define-public crate-ckb-logger-0.111.0-rc7 (c (n "ckb-logger") (v "0.111.0-rc7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07ci1cwasxkal6d69vry28l50x5rg50gfr12d2f1kr17lwf807hk")))

(define-public crate-ckb-logger-0.111.0-rc8 (c (n "ckb-logger") (v "0.111.0-rc8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "116ryi19sbm882cqzdjd5v25wdhm9bqhl9lkrdp667wdp46qv87v")))

(define-public crate-ckb-logger-0.110.1-rc1 (c (n "ckb-logger") (v "0.110.1-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cbhw4rykirz9a5k2psqmiqa45lham4yal8zd3ilrnc8ifjcy020")))

(define-public crate-ckb-logger-0.110.1-rc2 (c (n "ckb-logger") (v "0.110.1-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mr7v4bji6xjw5h8hdyxb5x048kgjbajrxbw0l5l34923kbb2lb0")))

(define-public crate-ckb-logger-0.111.0-rc9 (c (n "ckb-logger") (v "0.111.0-rc9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0kdrahm3z53kgiq2s2qn6d9hng4rby5n0vdzjzg90kdc2ab74rin")))

(define-public crate-ckb-logger-0.111.0-rc10 (c (n "ckb-logger") (v "0.111.0-rc10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kl5mlhm9wc7mzc6cymhs117yykf8lkwrrymqg9hbgfby7c12qq3")))

(define-public crate-ckb-logger-0.110.1 (c (n "ckb-logger") (v "0.110.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mn8n0yhc70761806h4f5mdaf9mxaz5ckagjpbdnd8rch81dn9c6")))

(define-public crate-ckb-logger-0.110.2-rc1 (c (n "ckb-logger") (v "0.110.2-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05201vyh7wi4ds991blg5l68i9b6r30bgmb50kcm1n4n5szx1fph")))

(define-public crate-ckb-logger-0.111.0-rc11 (c (n "ckb-logger") (v "0.111.0-rc11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vsrxljsgiliix53rghjgv8v9r413v7cqdzc43icclxywcb720mp")))

(define-public crate-ckb-logger-0.110.2-rc2 (c (n "ckb-logger") (v "0.110.2-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cx6w4vq4i2cm3f0sbq4jcpvj3kmppmf796zpc364ny7391z0g1r")))

(define-public crate-ckb-logger-0.111.0-rc12 (c (n "ckb-logger") (v "0.111.0-rc12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "12q5f43w7kq6h5nd8z393pindrbrpysbyzl4wjwqjxxi20py7cwz")))

(define-public crate-ckb-logger-0.110.2 (c (n "ckb-logger") (v "0.110.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rk6w26dadb261g2valmjymp18ddpc4wgpkyfgpb47qc36pixryz")))

(define-public crate-ckb-logger-0.111.0 (c (n "ckb-logger") (v "0.111.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rw07s6x6ajxnp2zni366h8pxz0ny132amajsk9lhhjklffa17wk")))

(define-public crate-ckb-logger-0.112.0-rc1 (c (n "ckb-logger") (v "0.112.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00diafqcgzpd2lsrfzqm2mdnmgcdzdjq2plcjxby910ypk76gcfz")))

(define-public crate-ckb-logger-0.112.0-rc2 (c (n "ckb-logger") (v "0.112.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16hkl0xbqsinp1fzjmn40qndxds6n9q7pl6n08nhvxkc2v2n783w")))

(define-public crate-ckb-logger-0.112.0 (c (n "ckb-logger") (v "0.112.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ghnz8vijj03f1rglcjdndf59rh114rfdmplhxpfpxvf2a1zsaki")))

(define-public crate-ckb-logger-0.112.0-rc3 (c (n "ckb-logger") (v "0.112.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x29s2p72kpijx63kd6jsw5x49sx7b4k4qr529z62mwws3f16kxh")))

(define-public crate-ckb-logger-0.112.1 (c (n "ckb-logger") (v "0.112.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zl2hy70rsypppf69400ma4hydvzs5aqlcz6m6dwbnyi33k8v3ac")))

(define-public crate-ckb-logger-0.113.0-rc1 (c (n "ckb-logger") (v "0.113.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fb84sy4bfca976paaw22jrv34ma32qwmzvydm1j7zrflv9nnblv")))

(define-public crate-ckb-logger-0.113.0-rc2 (c (n "ckb-logger") (v "0.113.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1az3id22403hnkqh4c1b2f9pmzq0z3xs2b63q3dspvijl4mbqw22")))

(define-public crate-ckb-logger-0.113.0-rc3 (c (n "ckb-logger") (v "0.113.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jyj51jdd913f0bmn34mv7phjravh57nhmd9r0j3kix003555a2b")))

(define-public crate-ckb-logger-0.113.0 (c (n "ckb-logger") (v "0.113.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1p1abmm6s371kp100m58mzkv2w57ih65iz6aazpimmyh8ybgixg3")))

(define-public crate-ckb-logger-0.113.1-rc1 (c (n "ckb-logger") (v "0.113.1-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11v50lwlfkqsm9pg46d2hjplidssy8b100ign67k9m95j6hd0307")))

(define-public crate-ckb-logger-0.114.0-rc1 (c (n "ckb-logger") (v "0.114.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hhcx45illkx1arywvnq9n8ifm2h3f9xypagzsapvra0hwmihljj")))

(define-public crate-ckb-logger-0.113.1-rc2 (c (n "ckb-logger") (v "0.113.1-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11rlyrg370pqadmhfnsz4x0nagvp3fyw40gc3qq783kdw5glbhci")))

(define-public crate-ckb-logger-0.113.1 (c (n "ckb-logger") (v "0.113.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vqrid9s25qfw5rnhqpjjdlq3cv9dgjldp475x8x407bvm5zli28")))

(define-public crate-ckb-logger-0.114.0-rc2 (c (n "ckb-logger") (v "0.114.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "011kf2ddy9xbsainx2bi10b3l02q8y6nwy8wa17ygpyf3vmgpb46")))

(define-public crate-ckb-logger-0.114.0-rc3 (c (n "ckb-logger") (v "0.114.0-rc3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0cw5bb3nd7y9bqfcvn6yzihpimgsns3ykq9rm9f5nf805wc96256")))

(define-public crate-ckb-logger-0.114.0 (c (n "ckb-logger") (v "0.114.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wjd7nl5wz5b183iy71biwi4hsp13bsm7hr7kcdpppcj7q5r30vv")))

(define-public crate-ckb-logger-0.115.0-pre (c (n "ckb-logger") (v "0.115.0-pre") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h6qdm1vvbjv7zw503ggyssj9gfrzpbyr4zhwp174yayihsqy3hp")))

(define-public crate-ckb-logger-0.115.0-rc1 (c (n "ckb-logger") (v "0.115.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c9i9v61shhnf784nxbrxbvlc2ijiqap16xsw6prdpsrbv070w6j")))

(define-public crate-ckb-logger-0.115.0-rc2 (c (n "ckb-logger") (v "0.115.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00ymk88kry31ndj4xxnrzzavb4b0wfiwk2zrf35ldfh9akmxz8sh")))

(define-public crate-ckb-logger-0.115.0 (c (n "ckb-logger") (v "0.115.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bkvqvgg2wpv5dc1rqprdhqkv331zxpshzbi7wcl6zv5izr5zixa")))

(define-public crate-ckb-logger-0.116.0-rc1 (c (n "ckb-logger") (v "0.116.0-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1wl9954nl7nicwfdz9034hqc7b580xjkgyqdzdrkbii2c6bi976r")))

(define-public crate-ckb-logger-0.116.0-rc2 (c (n "ckb-logger") (v "0.116.0-rc2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0axnrbh82wbyyppf1cx3ilcx0224p5918glfdr9gwkn570p2pfvh")))

(define-public crate-ckb-logger-0.116.0 (c (n "ckb-logger") (v "0.116.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "091dczx11z1q4nbqfhb8i023aclw9wbj8lkv4pnqvx4w0i4wjk5q") (y #t)))

(define-public crate-ckb-logger-0.116.1 (c (n "ckb-logger") (v "0.116.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z986nk5yadwa1bm2d9byyyq1ydpnrk9xhxmvhxlbjwsdkayrssr")))

