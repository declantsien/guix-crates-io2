(define-module (crates-io ck b- ckb-lib-secp256k1) #:use-module (crates-io))

(define-public crate-ckb-lib-secp256k1-0.1.0 (c (n "ckb-lib-secp256k1") (v "0.1.0") (d (list (d (n "blake2b-rs") (r ">=0.1.5, <0.2.0") (d #t) (k 1)) (d (n "ckb-std") (r ">=0.7.2, <0.8.0") (d #t) (k 0)))) (h "08mh0bq2b1sbbk4bmyajv7gmpp26sir9knzy5jvmp194628fr6s6") (y #t)))

