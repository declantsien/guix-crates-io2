(define-module (crates-io ck b- ckb-systemtime) #:use-module (crates-io))

(define-public crate-ckb-systemtime-0.108.0 (c (n "ckb-systemtime") (v "0.108.0") (h "17mymbcg0ykjiypxamy7b2i650aarscn3wfan5nbpmk91xl9fc94") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.108.1 (c (n "ckb-systemtime") (v "0.108.1") (h "08a00mvlph0amj2nkhm5a509lm2pw0k923db0kk2rdj2fbw4q41l") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.109.0 (c (n "ckb-systemtime") (v "0.109.0") (h "1mzh1q51d8sx8sg9y6nrk784zv8zj6l2ikvpw2bnczm8clww0jzn") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.0 (c (n "ckb-systemtime") (v "0.110.0") (h "0g845vf8xan18c8scdn06scky53zipamgm9qskhvnaz8ld42qmmr") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.0-rc1 (c (n "ckb-systemtime") (v "0.110.0-rc1") (h "180zwcqbl5lxv0x284xrsaq4xjxzz070i0wccd2g0s3ayng1sm6a") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc2 (c (n "ckb-systemtime") (v "0.111.0-rc2") (h "0dpl8wk58wxgcx4nrbwwjjpl315jrvxkbv908y0qcl6izqpyagqh") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc3 (c (n "ckb-systemtime") (v "0.111.0-rc3") (h "0yyy1b5syyi4ix6lpi0a8fsiacb8z9gm5lbx6n8byk7zi8jiw6gm") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc4 (c (n "ckb-systemtime") (v "0.111.0-rc4") (h "0rd4s8hy7g47zp65y6dxxnf3yjys08bw71nq8j4pychagfdqlq8s") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc5 (c (n "ckb-systemtime") (v "0.111.0-rc5") (h "1y3251w6ivg7pmzh6vkm1bfvmcyc9bv1gzv448y00d4qq25l6qsz") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc6 (c (n "ckb-systemtime") (v "0.111.0-rc6") (h "0dbgrv5m7c280035idgyvy778w5yin77sbjbyfkzhwhalqvkfdzd") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc7 (c (n "ckb-systemtime") (v "0.111.0-rc7") (h "1a48qh4i0ahln11hx4my1rbn3389x9wa6wbwidd1x5hfxnkbhzdm") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc8 (c (n "ckb-systemtime") (v "0.111.0-rc8") (h "1kvfw2mvxc477wgqx672sn42b78ndxryymwnmg11bsqmhp9mpv93") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.1-rc1 (c (n "ckb-systemtime") (v "0.110.1-rc1") (h "134bfnx4d7kmhzvc72dxm2p2a39kg3n12vbpgdipssp3hzngvl8l") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.1-rc2 (c (n "ckb-systemtime") (v "0.110.1-rc2") (h "1fll3w0vsi75kq152gxhhx940adn7ibv6pyx95v1ggmn096mbdyd") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc9 (c (n "ckb-systemtime") (v "0.111.0-rc9") (h "01hcqjjxnh0gbhw16wk561nrc4w5ckh1wbd8jnaslg4y2kik0c1b") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc10 (c (n "ckb-systemtime") (v "0.111.0-rc10") (h "1266pxq51bg4wsq0vka8k4shi3sbsn89lrja0a951q16jwrscky4") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.1 (c (n "ckb-systemtime") (v "0.110.1") (h "0s6mq316lq5lbpxa2wxcy8c36vd6l5yxsz218k43ck4n9m4pfgl3") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.2-rc1 (c (n "ckb-systemtime") (v "0.110.2-rc1") (h "1vdqjywhnghdajv58m82b1j3s4sza3p0v5xsh9n5sffmdgxy6la6") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc11 (c (n "ckb-systemtime") (v "0.111.0-rc11") (h "1yn8plq1cbsx1i17pgjkzlrg946rb4dymrilfbbi4l416br3hviy") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.2-rc2 (c (n "ckb-systemtime") (v "0.110.2-rc2") (h "1xs2f05259h7m6im98pswkn9vvvwjda85zyrm363k7kzp14y46z7") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0-rc12 (c (n "ckb-systemtime") (v "0.111.0-rc12") (h "1klbz09iql58n85z3dq6gsb2gr725mjl7j4wbaajgk240jr9q0hi") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.110.2 (c (n "ckb-systemtime") (v "0.110.2") (h "0cllp133mz68h3hqhr216l055kkmc66r6p29ysb06qwjb01mg1lx") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.111.0 (c (n "ckb-systemtime") (v "0.111.0") (h "1nhc63imlg65l9asl6dy9ass101ds6nvpc8z2ypnx9k32b83vh6m") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.112.0-rc1 (c (n "ckb-systemtime") (v "0.112.0-rc1") (h "0kk5w4dfmzmxp8746a5dnpb1asy55j5brhsnb7xxdpq0vj0df7z6") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.112.0-rc2 (c (n "ckb-systemtime") (v "0.112.0-rc2") (h "0xl9cp3znh1wdxm2rhd080rq93sbf3a0frfhngb21v0klcd4sy4x") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.112.0 (c (n "ckb-systemtime") (v "0.112.0") (h "17hgpikfvy4kfkcdjv4xdn6dfa10n7xsrkmksxq8krx59c0962yd") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.112.0-rc3 (c (n "ckb-systemtime") (v "0.112.0-rc3") (h "0z7f9kkvqrykckzfc3zsax9k3hpn78pgg8jnf4jai9cvpjrxpsi8") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.112.1 (c (n "ckb-systemtime") (v "0.112.1") (h "0m6dh0ga46mbpgl8bxki06lsb0ij9vrrqdcp1c75sk9akz86kqxc") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.0-rc1 (c (n "ckb-systemtime") (v "0.113.0-rc1") (h "072kmxj3qbg3r3cwh7hpnfxj5f929raq2bfn3cypd2gpa6z3b05m") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.0-rc2 (c (n "ckb-systemtime") (v "0.113.0-rc2") (h "0ins28bhmc9gga55k3ac1qsywjakcy8a98dy69yrlyhkfvs2qwr2") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.0-rc3 (c (n "ckb-systemtime") (v "0.113.0-rc3") (h "0bfiz8l4h6vld6myhvncchkf1jrr6qghbnmnikhjnkwfn66b03kn") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.0 (c (n "ckb-systemtime") (v "0.113.0") (h "1m683cjczgabxn4waa9nlql4p6vcdlpxy648q21hfcazz9zmrldl") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.1-rc1 (c (n "ckb-systemtime") (v "0.113.1-rc1") (h "0vi0mz03kd5nf6bzfa6br04b6kqdv39qk1vmwn5pyj8l8x4jhiy4") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.114.0-rc1 (c (n "ckb-systemtime") (v "0.114.0-rc1") (h "0ixdv7s19cbx892c3pqvlhb4sa4vv87dnb8w23lildr5ivli0xxs") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.1-rc2 (c (n "ckb-systemtime") (v "0.113.1-rc2") (h "1bgzkh5jgf8pzk3i9rili0v803fqswbcqln7hzlcgrzgmyrln5j0") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.113.1 (c (n "ckb-systemtime") (v "0.113.1") (h "0m3yrajlbzm1d7j6zscp8szcp0qinb46s7za4mzr117r1hwk8n54") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.114.0-rc2 (c (n "ckb-systemtime") (v "0.114.0-rc2") (h "0f2jki2i8dd1ly8ynlh1s0m8c6kbyf86ghkqjbxcj95jm6as5cm6") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.114.0-rc3 (c (n "ckb-systemtime") (v "0.114.0-rc3") (h "15lw265g98m6589qh1gpcwh0dcyw6q413b81gdfp49g4aibjycq3") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.114.0 (c (n "ckb-systemtime") (v "0.114.0") (h "0sia247xk44z6xj6gk6clkjqgmhp80sh7xy7brk5n08p0mi3fndf") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.115.0-pre (c (n "ckb-systemtime") (v "0.115.0-pre") (h "1mnr3gjij438m6vpffl7vmbi305njnm30a2j18n0c19pcqki0wrg") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.115.0-rc1 (c (n "ckb-systemtime") (v "0.115.0-rc1") (h "05yxf7m2xbl575nhcnzfk1yfzixcca3krbzykj9p219cy78zr3kp") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.115.0-rc2 (c (n "ckb-systemtime") (v "0.115.0-rc2") (h "1s2bmzd1vmgzya7ci8qrxvzdx1sskl5yi61bvcvylpmsglsdpas7") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.115.0 (c (n "ckb-systemtime") (v "0.115.0") (h "07ayn9f25n8ss4dkn934ah39ccwanxhvvy1bvzizd44pkad27i6d") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.116.0-rc1 (c (n "ckb-systemtime") (v "0.116.0-rc1") (h "0qdxbhw8k2z1g8q3xcg8aq31g9x2m6vrp78pdn6dpk8dbnhnhrfh") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.116.0-rc2 (c (n "ckb-systemtime") (v "0.116.0-rc2") (h "0lfkvmz73p4fmqr26i7cyj49w8vs2m7343j2a2szwh764zak0fpm") (f (quote (("enable_faketime") ("default"))))))

(define-public crate-ckb-systemtime-0.116.0 (c (n "ckb-systemtime") (v "0.116.0") (h "0bdips2vvi8r4bpnc4s7zacj2s4hv5n82c6p38x0mmqaap3dxlda") (f (quote (("enable_faketime") ("default")))) (y #t)))

(define-public crate-ckb-systemtime-0.116.1 (c (n "ckb-systemtime") (v "0.116.1") (h "1vwna8znnpdi37h992z0zvlqpi9r46rxmjl5y86cp7ksz9q9pp6l") (f (quote (("enable_faketime") ("default"))))))

