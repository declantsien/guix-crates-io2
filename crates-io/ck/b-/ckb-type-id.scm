(define-module (crates-io ck b- ckb-type-id) #:use-module (crates-io))

(define-public crate-ckb-type-id-0.1.0-alpha.1 (c (n "ckb-type-id") (v "0.1.0-alpha.1") (d (list (d (n "blake2b-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "ckb-std") (r "^0.10") (d #t) (k 0)) (d (n "molecule") (r "^0.7.3") (k 0)))) (h "1jg5rfbqj2bd3wbfndz5g0f4mrwy408ygv5jg7l7zkygbjxgf5qg")))

(define-public crate-ckb-type-id-0.1.0-alpha.2 (c (n "ckb-type-id") (v "0.1.0-alpha.2") (d (list (d (n "blake2b-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "ckb-std") (r "^0.10") (d #t) (k 0)) (d (n "molecule") (r "^0.7.3") (k 0)))) (h "1r0afgvbjzifj1wvbhigymnm1mqygp5nj5zvdy38k5pjx24afwll")))

(define-public crate-ckb-type-id-0.1.0-alpha.3 (c (n "ckb-type-id") (v "0.1.0-alpha.3") (d (list (d (n "blake2b-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "ckb-std") (r "^0.13") (d #t) (k 0)) (d (n "molecule") (r "^0.7.3") (k 0)))) (h "1fak5vv501x3lv8rv9zkdpd1ryf5spza5kigk54dm92pwrg77pfc")))

