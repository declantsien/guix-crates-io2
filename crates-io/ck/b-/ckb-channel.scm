(define-module (crates-io ck b- ckb-channel) #:use-module (crates-io))

(define-public crate-ckb-channel-0.37.0-pre (c (n "ckb-channel") (v "0.37.0-pre") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "1nvk4mzv3hxfax48riq8krjwzspwvsxqra0galb7qy9dr6ik2bix")))

(define-public crate-ckb-channel-0.37.0 (c (n "ckb-channel") (v "0.37.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "0ywarfnxf7ahw67dl7q7n83apk2djnm00axsn73fd30x8kbyyfna")))

(define-public crate-ckb-channel-0.38.0 (c (n "ckb-channel") (v "0.38.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "1mjsisw6fjb4c4a93czkjvf61xilh1g2wgyiajyjbkw48854mdmh")))

(define-public crate-ckb-channel-0.39.0 (c (n "ckb-channel") (v "0.39.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "1iawq7606kxfpdadd26ch9p5lml7qz6n4pkss67zrs21c6w52djw")))

(define-public crate-ckb-channel-0.39.1 (c (n "ckb-channel") (v "0.39.1") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "0441l8v6imi3wav912lj3sz6pm85hfggydw6b74733a7l7hkda95")))

(define-public crate-ckb-channel-0.40.0 (c (n "ckb-channel") (v "0.40.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "1kwvfwi7klrp1qhdmgglw97hjf9a9p9w8141xmscc0b9s9n276w7")))

(define-public crate-ckb-channel-0.42.0 (c (n "ckb-channel") (v "0.42.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "0hzhwfl3rblrhib37r2c4ssnc3yyd3hsphxxjawkk26wndx21cw6")))

(define-public crate-ckb-channel-0.43.0 (c (n "ckb-channel") (v "0.43.0") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "0ckssdd7vq3wz3ic32ia1a4sxmrv67qm7l9aglxyn3g4hh6qm6vd")))

(define-public crate-ckb-channel-0.100.0-rc2 (c (n "ckb-channel") (v "0.100.0-rc2") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "02p5ys2sblxcdllcpjyblcdlxy2217ms881a6ddm4sf75mry2j00")))

(define-public crate-ckb-channel-0.43.2 (c (n "ckb-channel") (v "0.43.2") (d (list (d (n "crossbeam-channel") (r "~0.3") (d #t) (k 0)))) (h "0c1nx59yxva29d34dqwc69sfg0wrbdrfyylf6kmnsb2ad9lwsgw5")))

(define-public crate-ckb-channel-0.100.0-rc3 (c (n "ckb-channel") (v "0.100.0-rc3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "01bpscp81a6nr7b47rccdn1yv8myv259ivn35zg92993v3ksj6sp") (y #t)))

(define-public crate-ckb-channel-0.100.0-rc4 (c (n "ckb-channel") (v "0.100.0-rc4") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0ngcz8rk6ffhq47c9vdrpc01nkv6q87ms184w4ybfinbb809lrr0")))

(define-public crate-ckb-channel-0.100.0-rc5 (c (n "ckb-channel") (v "0.100.0-rc5") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "04zc9wvkkgry62iqdfzydj5fp0pmhf8ikcfxkixhix01y1c0ib5p")))

(define-public crate-ckb-channel-0.100.0 (c (n "ckb-channel") (v "0.100.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0yikz5n22zw7fw99qgs2sdxyl7ixxnpzr520dyvmkjrdhilnklkg")))

(define-public crate-ckb-channel-0.101.0 (c (n "ckb-channel") (v "0.101.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "152ncb60nf4skgk8p9gmzxzam7rfgmss9hl7bxq2b54fczrpa8x1")))

(define-public crate-ckb-channel-0.101.1 (c (n "ckb-channel") (v "0.101.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0wnyw08kk11ar45sxh6adwkb8a84kpcp8jhvmqwi87g3f41569g8")))

(define-public crate-ckb-channel-0.101.2-testnet1 (c (n "ckb-channel") (v "0.101.2-testnet1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "077268wx516fsj3yv07pn4591mhbb3hi8wi2pwhsa2mwjjvw5m2i")))

(define-public crate-ckb-channel-0.101.2 (c (n "ckb-channel") (v "0.101.2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1l38bn4vh50vzrnzdaw794q04mysr3fa6dm3s7sxkv804hiyk5ri")))

(define-public crate-ckb-channel-0.101.3 (c (n "ckb-channel") (v "0.101.3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0abbwyn18d51q02bq3yis6kprjccd77kaklg4x2qf12l3d2iykq8")))

(define-public crate-ckb-channel-0.101.4 (c (n "ckb-channel") (v "0.101.4") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "14qcdypbdgg33q2qn0m038zdhfzf9n1lbl4jzc9393q8yj7jsndy")))

(define-public crate-ckb-channel-0.101.5 (c (n "ckb-channel") (v "0.101.5") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0fs8l79z8m180i6xzqvwv4g3ygq2x8i1lwblljfg2239qfjjix42") (y #t)))

(define-public crate-ckb-channel-0.101.6 (c (n "ckb-channel") (v "0.101.6") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "145c66qdbiddllvh7zb6dpb532j2022h0iw375jbawrljpdwgpkj")))

(define-public crate-ckb-channel-0.101.7 (c (n "ckb-channel") (v "0.101.7") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0hf71q4kzilyj7pl423nz42hxwjn9501x4aj2xzlrnmdv0fx9z88")))

(define-public crate-ckb-channel-0.101.8 (c (n "ckb-channel") (v "0.101.8") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0dpmfz6q72s2rngblgz8prd3r1n78pf4m346wzb2rvwpf3ds5qa4")))

(define-public crate-ckb-channel-0.102.0 (c (n "ckb-channel") (v "0.102.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0wh35d5fxhqx4j6s83mvxwr4jlgzlrx3n3qhpx9bz50jgyl36hw3") (y #t)))

(define-public crate-ckb-channel-0.103.0 (c (n "ckb-channel") (v "0.103.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0zi4ag2dh8anfx7wx14i3xr5prnn45i9i43l0r2525c8xv65hzy3")))

(define-public crate-ckb-channel-0.104.0 (c (n "ckb-channel") (v "0.104.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1rpm1vdzx5zb6xp2v651b3cxl6g4rnfnvvlwmh4r2ncbhlr7iqbg")))

(define-public crate-ckb-channel-0.104.1 (c (n "ckb-channel") (v "0.104.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0y9hrv0d0mfkv2bcjxdcz45m2k0fxxnf6nirwb0r56dfgrj6c993")))

(define-public crate-ckb-channel-0.105.0 (c (n "ckb-channel") (v "0.105.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "06ibcllk1xn0xsqvi1x2ifvpj76g0f1zr0pcg8bglaa8906w9s1g") (y #t)))

(define-public crate-ckb-channel-0.105.1 (c (n "ckb-channel") (v "0.105.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0vmvq03f8i6l6p7wf6dn8h1l2fyslhyn1b9hbsq4ipklwksa23az") (y #t)))

(define-public crate-ckb-channel-0.106.0 (c (n "ckb-channel") (v "0.106.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0mj1i7jcah1z0lsnysw8mk1z9lmajqymgyn7jp7brc4qf3x8sway")))

(define-public crate-ckb-channel-0.107.0 (c (n "ckb-channel") (v "0.107.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "07h8g59qw2cm5vfr0h22aqb4zjvbrm1xvdggsszhx24qrjaqka28")))

(define-public crate-ckb-channel-0.108.0 (c (n "ckb-channel") (v "0.108.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0n48zh01q7hyyf7dlqb37dimbwzrx7yn0ffcgdpszp6a9362c3wj")))

(define-public crate-ckb-channel-0.108.1 (c (n "ckb-channel") (v "0.108.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1ynfwyxpc5xs1qmln23ryx2lpzyqzw3130qw3xg66hlmw014wrjj")))

(define-public crate-ckb-channel-0.109.0 (c (n "ckb-channel") (v "0.109.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "056bl16lbmfvcx6nqci9py45l3zjh2s9j4acbc18dqg6ks576qfi")))

(define-public crate-ckb-channel-0.110.0 (c (n "ckb-channel") (v "0.110.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "06l3cjcxq5rhik8ziaj9233nnx75f5g67801mfh3rywlsaw2xrzl")))

(define-public crate-ckb-channel-0.110.0-rc1 (c (n "ckb-channel") (v "0.110.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1nkibgidz5wfijc61sf90k4q7xgp5q650qb2dcvs43z514843x76")))

(define-public crate-ckb-channel-0.111.0-rc2 (c (n "ckb-channel") (v "0.111.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "11ic9nx2mribilv39cnzzkr3ahzf5g8ihgxdhj7h86mh5gmnw4yk")))

(define-public crate-ckb-channel-0.111.0-rc3 (c (n "ckb-channel") (v "0.111.0-rc3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0mxash6h08av7pk970w40l6jpwdkah0iibmzcgiih40wgchl5i1b")))

(define-public crate-ckb-channel-0.111.0-rc4 (c (n "ckb-channel") (v "0.111.0-rc4") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1abx9zy7gpm1j7mhf97j0dx166gf6fvx2k7paa65855p49dmr07k")))

(define-public crate-ckb-channel-0.111.0-rc5 (c (n "ckb-channel") (v "0.111.0-rc5") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "14ljrrxf5c2lw9673ajg373a6dqjyarvifzcmhvwmdy9iavld7k5")))

(define-public crate-ckb-channel-0.111.0-rc6 (c (n "ckb-channel") (v "0.111.0-rc6") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1a7bb0p5cxmrxag5vb1zhrp5q4xrbfhjpisdfnanc7n3g1ph7i8h")))

(define-public crate-ckb-channel-0.111.0-rc7 (c (n "ckb-channel") (v "0.111.0-rc7") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0cs034izyilg6rn41a1g1yjp2mdxpypdzk8nwabkqk7g1xljfd7q")))

(define-public crate-ckb-channel-0.111.0-rc8 (c (n "ckb-channel") (v "0.111.0-rc8") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0pxs8cq96v394zw5m996j8676q7ki2m4s0nk76s0maywv35iqkbn")))

(define-public crate-ckb-channel-0.110.1-rc1 (c (n "ckb-channel") (v "0.110.1-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0x604wysc3rhiz9ikb481dwydybgdpkgc3dflg28a8wjfd31cjzk")))

(define-public crate-ckb-channel-0.110.1-rc2 (c (n "ckb-channel") (v "0.110.1-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0d6k4pikmrf75kicrjp63w7cg6xn57iay64vywrz456x264g5lh7")))

(define-public crate-ckb-channel-0.111.0-rc9 (c (n "ckb-channel") (v "0.111.0-rc9") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1pmh2xvq3hd03wb259cny8vzbyl4vm0fk9b04478mcr96pqq90wn")))

(define-public crate-ckb-channel-0.111.0-rc10 (c (n "ckb-channel") (v "0.111.0-rc10") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "113mw0x6bnwbciz5c3m1px5yk8xw7sg3b2slan4n246a0s3yx6al")))

(define-public crate-ckb-channel-0.110.1 (c (n "ckb-channel") (v "0.110.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0xfpk4fgn5d1ml9sss35w58ch4rk3cjm0japmwcld9v70al4j29x")))

(define-public crate-ckb-channel-0.110.2-rc1 (c (n "ckb-channel") (v "0.110.2-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0is886xh9pbbhch78rrng17arx9xmf9lfsm804jf3p8cni1a2mja")))

(define-public crate-ckb-channel-0.111.0-rc11 (c (n "ckb-channel") (v "0.111.0-rc11") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0k4cq71jlnhw5cif737sa36ydxvx7vjbz2dkf3fv7wnlyp98vc3l")))

(define-public crate-ckb-channel-0.110.2-rc2 (c (n "ckb-channel") (v "0.110.2-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1qkpmwksvq0hxnwh9ckknvpmch1lnqhm77k79zfx951nqhh8crvn")))

(define-public crate-ckb-channel-0.111.0-rc12 (c (n "ckb-channel") (v "0.111.0-rc12") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1858fhqvqpl31bjznlz4md2s3j2ahp6cmhxg8fszpcs2i434r5b0")))

(define-public crate-ckb-channel-0.110.2 (c (n "ckb-channel") (v "0.110.2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1mw7rkxnzqmxa19ghhm80xxix1lvbvf02sp3rqylw2zllz7wqi14")))

(define-public crate-ckb-channel-0.111.0 (c (n "ckb-channel") (v "0.111.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "00ll8n9d16j1pwfrbd1bdh54hih010yycba4s9ny9fnwqclnh7kh")))

(define-public crate-ckb-channel-0.112.0-rc1 (c (n "ckb-channel") (v "0.112.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1yj7bl1jaml7r197229gx8r8l2apncccy0rhznphrzbwzmrs09d6")))

(define-public crate-ckb-channel-0.112.0-rc2 (c (n "ckb-channel") (v "0.112.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0kchg1m1mrw2v0if51fnj1bx3nxbwfg1nqwg4d2dsqas6sfnx645")))

(define-public crate-ckb-channel-0.112.0 (c (n "ckb-channel") (v "0.112.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1n1z2ikq7rrzg4f4v8wbbar78qndlahl3m0abs8nwy8ndgkk1rpc")))

(define-public crate-ckb-channel-0.112.0-rc3 (c (n "ckb-channel") (v "0.112.0-rc3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0hipdg4ld7v6zlr5xr2qa4nw7sk8y02a7mmx54p815wn6hwwxh6q")))

(define-public crate-ckb-channel-0.112.1 (c (n "ckb-channel") (v "0.112.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0f5a1mqh45f7p3pfs65giwpj8mi48xd7gqp8156lwir97vz09jc2")))

(define-public crate-ckb-channel-0.113.0-rc1 (c (n "ckb-channel") (v "0.113.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1kg66dya1pwnpw98q1fr8jq7wnbpfmkhyznz2clj68scfdd4syl7")))

(define-public crate-ckb-channel-0.113.0-rc2 (c (n "ckb-channel") (v "0.113.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0q6b31b92282fla8z5ra1jmkp81lr310m13kvg67rm0z3j02j306")))

(define-public crate-ckb-channel-0.113.0-rc3 (c (n "ckb-channel") (v "0.113.0-rc3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "00c7fzhp6mr2qj1bnaabn123iw8h7k3k9glrqa6drn61vclh7cbl")))

(define-public crate-ckb-channel-0.113.0 (c (n "ckb-channel") (v "0.113.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "09091lx22mldivjpp3pjf1kvl8g8cdz8i7gdkih5hy6l99nixljh")))

(define-public crate-ckb-channel-0.113.1-rc1 (c (n "ckb-channel") (v "0.113.1-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0gd23cn24jx0rsgrafi55dh5vvh2zl70ngwnywfzi6f8rr5k24vs")))

(define-public crate-ckb-channel-0.114.0-rc1 (c (n "ckb-channel") (v "0.114.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0lfyg37vddkkf932a322299r2shs99h5p09dxly0agqlnsfadpxy")))

(define-public crate-ckb-channel-0.113.1-rc2 (c (n "ckb-channel") (v "0.113.1-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "05v9qb4jdqblnml0kaik4c5dw3s84ncv1p44mcd4sjp1n6mfrh4a")))

(define-public crate-ckb-channel-0.113.1 (c (n "ckb-channel") (v "0.113.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0vg5n2d26vc4s1h9wjs7d0fjn8hcr0b9j7aw9cw0jwl4rlp44f7r")))

(define-public crate-ckb-channel-0.114.0-rc2 (c (n "ckb-channel") (v "0.114.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "08fv1l3p46wfij7c6y26ccpqs4rvgz21c36wrivv3dx0zfasi3gr")))

(define-public crate-ckb-channel-0.114.0-rc3 (c (n "ckb-channel") (v "0.114.0-rc3") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "165cjcnis18lc1ic898wr31n0gxrjxvlhfgv2k3yqq36vbxrvksi")))

(define-public crate-ckb-channel-0.114.0 (c (n "ckb-channel") (v "0.114.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1a4y2brr0d78qg3js1mc1pqzcaxmksczl6w58v1ix6icaiq2xch3")))

(define-public crate-ckb-channel-0.115.0-pre (c (n "ckb-channel") (v "0.115.0-pre") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0lgs41pwnahqk7sl58awns6zzrz1ij5qsg54r7l1kfjma6mp2l3c")))

(define-public crate-ckb-channel-0.115.0-rc1 (c (n "ckb-channel") (v "0.115.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1whxka46ibw184x4zxjds0hakg6ckbx6y59znbjigl5sz1alqaw5")))

(define-public crate-ckb-channel-0.115.0-rc2 (c (n "ckb-channel") (v "0.115.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1hshp4p34805sbxld54r16kcp56h3xiwml7hgfz6cmvby1xllv70")))

(define-public crate-ckb-channel-0.115.0 (c (n "ckb-channel") (v "0.115.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0nnbdcajrigd55m8v2qqyvlrndfyb285jm3nizwm1ckxcpn8ax1h")))

(define-public crate-ckb-channel-0.116.0-rc1 (c (n "ckb-channel") (v "0.116.0-rc1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0s811lnnxycgcnijqr76rpcy1wfhwmh4m9br8hfz513jwxw6yx5c")))

(define-public crate-ckb-channel-0.116.0-rc2 (c (n "ckb-channel") (v "0.116.0-rc2") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "1rgba5hpqxxyzxqjplpcvbgrmsglwlmjcvcm6vbgb5l68pxjcmna")))

(define-public crate-ckb-channel-0.116.0 (c (n "ckb-channel") (v "0.116.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "0xdham6rdspk2zy8yp1isfklg5mwwnlxf9rlxzfi43fs5ksirr0l") (y #t)))

(define-public crate-ckb-channel-0.116.1 (c (n "ckb-channel") (v "0.116.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)))) (h "17c86nrvl6xxsgc4z1wwhn28k55fbwwarhc7vicdhngzj0g5cgw5")))

