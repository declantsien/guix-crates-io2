(define-module (crates-io ck b- ckb-build-info) #:use-module (crates-io))

(define-public crate-ckb-build-info-0.37.0-pre (c (n "ckb-build-info") (v "0.37.0-pre") (h "018qfm8pj3ahhlhmqqkg6223c2czsq4ah9l3mqhs7rrngjl3650f")))

(define-public crate-ckb-build-info-0.37.0 (c (n "ckb-build-info") (v "0.37.0") (h "1q4hfnddsx0cd78agwwjsnwix0gvrya9z6qwadf8k44wldm6cqpf")))

(define-public crate-ckb-build-info-0.38.0 (c (n "ckb-build-info") (v "0.38.0") (h "1dx1w7mz7ii8bz4v7v4rjn9kfq7adf69jmr7s5zs4mbiyk9m5jmp")))

(define-public crate-ckb-build-info-0.39.0 (c (n "ckb-build-info") (v "0.39.0") (h "0l3h6v3i5aimindpn1l5xjnq1wdh5bxb00zvw95mv499q144dglx")))

(define-public crate-ckb-build-info-0.39.1 (c (n "ckb-build-info") (v "0.39.1") (h "1vja2w5c8i7hz49ivl7i50l9bipic8byaqybk5z6l3bp1iywnz82")))

(define-public crate-ckb-build-info-0.40.0 (c (n "ckb-build-info") (v "0.40.0") (h "0xnc065kqamjj2fzsv8n2w8cq7zbq9639illp5mx2g1ikxyifm7v")))

(define-public crate-ckb-build-info-0.42.0 (c (n "ckb-build-info") (v "0.42.0") (h "0knkqdc9i6gjpyl0pza46ygg6v3kwifzqv6y22w7xyii177arama")))

(define-public crate-ckb-build-info-0.43.0 (c (n "ckb-build-info") (v "0.43.0") (h "061w00gbcdg9gi6p0vai7fh0jy5szapyqgs1a45iz6pggqhcdz0l")))

(define-public crate-ckb-build-info-0.100.0-rc2 (c (n "ckb-build-info") (v "0.100.0-rc2") (h "0jc7iwkmx6z8mds2fd5r3nxjm7ph68s7vy4qjvxqv00nv2ydx4l3")))

(define-public crate-ckb-build-info-0.43.2 (c (n "ckb-build-info") (v "0.43.2") (h "089pywa0afhj336fyw75pcxj8f5hmv8da32fyqfv7qr8pigh7mv1")))

(define-public crate-ckb-build-info-0.100.0-rc3 (c (n "ckb-build-info") (v "0.100.0-rc3") (h "1nmdizwymv7glb30d4jvblfs5bvadnh7q669vpwqj8gfrb3wsfc6") (y #t)))

(define-public crate-ckb-build-info-0.100.0-rc4 (c (n "ckb-build-info") (v "0.100.0-rc4") (h "1wg3jkh532q2bmx8d65sh2107awali1wbdkmhbdxr7c41jg9zghm")))

(define-public crate-ckb-build-info-0.100.0-rc5 (c (n "ckb-build-info") (v "0.100.0-rc5") (h "1f4hc5bk679xhybq6k2x8whkbyd54an7fnprj092iiphd8vm97in")))

(define-public crate-ckb-build-info-0.100.0 (c (n "ckb-build-info") (v "0.100.0") (h "1n7wf6wz027k9cam7p4wpmmpdz0fk0s1x4zbhw2zykj83sbsn79y")))

(define-public crate-ckb-build-info-0.101.0 (c (n "ckb-build-info") (v "0.101.0") (h "0539vxymri3zs99nq6lhm5ar98i8r1wk37jd74b3nizishdf4qsv")))

(define-public crate-ckb-build-info-0.101.1 (c (n "ckb-build-info") (v "0.101.1") (h "1f5s0fd9kd5yiz7h2m9wkh1zyzcfbdq45a7mn6hsa9mfkibac0ff")))

(define-public crate-ckb-build-info-0.101.2-testnet1 (c (n "ckb-build-info") (v "0.101.2-testnet1") (h "174rz1d60qi6d68s75krgjhw9sjys8zxc768xn4l6f27bap44dd0")))

(define-public crate-ckb-build-info-0.101.2 (c (n "ckb-build-info") (v "0.101.2") (h "07vn4lmq2lmf894rdlrbbi7gqbd3a2s7nv9g4j617873zmkm8l7r")))

(define-public crate-ckb-build-info-0.101.3 (c (n "ckb-build-info") (v "0.101.3") (h "0d9lrn7wvf5dmbk2s0ajinfp0dfhkb24lbc4cx1cv0by8mid9ian")))

(define-public crate-ckb-build-info-0.101.4 (c (n "ckb-build-info") (v "0.101.4") (h "14c4ni7vm2470aw5mh2x3xq6jgl3zharaszxq1xsg21p28m3wvm9")))

(define-public crate-ckb-build-info-0.101.5 (c (n "ckb-build-info") (v "0.101.5") (h "13f2865pjdmwx9hhdqngxqwf91m351jmffwl06jqyw3xgfhrys6k") (y #t)))

(define-public crate-ckb-build-info-0.101.6 (c (n "ckb-build-info") (v "0.101.6") (h "08lv7cyjw9kiq749i7c14vk4zlpv79wcma6gpv9anxr07v2x33s3")))

(define-public crate-ckb-build-info-0.101.7 (c (n "ckb-build-info") (v "0.101.7") (h "0cqq0jb17md2kzym978zv833b8cc8v87msy3gz5hj8kbpykl1jsx")))

(define-public crate-ckb-build-info-0.101.8 (c (n "ckb-build-info") (v "0.101.8") (h "1312rf2c4b0sk72wvmkmr39avpng325vfydvzjv6682596775s61")))

(define-public crate-ckb-build-info-0.102.0 (c (n "ckb-build-info") (v "0.102.0") (h "1ldmjz77pwdm0f7j1gnyry3zp7d2gffrhsd38dhhg2m6jllkp3ym") (y #t)))

(define-public crate-ckb-build-info-0.103.0 (c (n "ckb-build-info") (v "0.103.0") (h "0d9r4wpw4csr7719isdnhqndr9961zhwqhb6mc0q9plp4i5yh47g")))

(define-public crate-ckb-build-info-0.104.0 (c (n "ckb-build-info") (v "0.104.0") (h "185hdmygb5778s1fd86qc6wgx1a3sjpjqxkxyfx318ind8vqb2xi")))

(define-public crate-ckb-build-info-0.104.1 (c (n "ckb-build-info") (v "0.104.1") (h "0sjd4ii2lni3gim6zsichssld5w1j5jkng398hx039cf13xyy9w0")))

(define-public crate-ckb-build-info-0.105.0 (c (n "ckb-build-info") (v "0.105.0") (h "10fj7pmrzhd12g8rdghlcv603mx4rja852vqxj1iw92nkar2j01i") (y #t)))

(define-public crate-ckb-build-info-0.105.1 (c (n "ckb-build-info") (v "0.105.1") (h "1z0jfhs6sc46bqmyz0rjr9jhlanx6524x9731aqdxmf17nk1jwdj") (y #t)))

(define-public crate-ckb-build-info-0.106.0 (c (n "ckb-build-info") (v "0.106.0") (h "0jv4817jybc27kwk18xq9cgd77pfy687aw3zk8463rvpw2m88m47")))

(define-public crate-ckb-build-info-0.107.0 (c (n "ckb-build-info") (v "0.107.0") (h "1mbja561r51k05f84gsx33d34ps2w9hl3csgjx8h8iip3a3kfgjn")))

(define-public crate-ckb-build-info-0.108.0 (c (n "ckb-build-info") (v "0.108.0") (h "17jgnmadbyxc5xfranw7j8hkxprz9kmmdvmr4qjjmpi5kmgilbsb")))

(define-public crate-ckb-build-info-0.108.1 (c (n "ckb-build-info") (v "0.108.1") (h "0axbhgy8g4lcrvyv8ilwillc0j5z1pfcw56fhzpn5a7i47hmpwkd")))

(define-public crate-ckb-build-info-0.109.0 (c (n "ckb-build-info") (v "0.109.0") (h "0y4v7slc732afh5qj1r7fv0qg32ykid7b6alqvng4lx9337jmydw")))

(define-public crate-ckb-build-info-0.110.0 (c (n "ckb-build-info") (v "0.110.0") (h "16yiqn3y8vxqk0x7c0nshxwlxmpdfw10g8vlhxz1zzffcn1ja8xy")))

(define-public crate-ckb-build-info-0.110.0-rc1 (c (n "ckb-build-info") (v "0.110.0-rc1") (h "052djzs8ff9yyvvxs8l24vsrc3gg0w7blbk02q01mlwif9kck6sr")))

(define-public crate-ckb-build-info-0.111.0-rc2 (c (n "ckb-build-info") (v "0.111.0-rc2") (h "0pn0nd07h1kvl7mjxgxgvn8dzndv73n7kp0mjscnq7xzrq7q08cs")))

(define-public crate-ckb-build-info-0.111.0-rc3 (c (n "ckb-build-info") (v "0.111.0-rc3") (h "0hd2qqfz1g1mcj9l5yzybf8s8glg1zk7xjpl4cydzvhwb4qh7f7h")))

(define-public crate-ckb-build-info-0.111.0-rc4 (c (n "ckb-build-info") (v "0.111.0-rc4") (h "070bbss8a4iyw78z01fz4r59wypfaakfjjx9dhrv2714sisil4ch")))

(define-public crate-ckb-build-info-0.111.0-rc5 (c (n "ckb-build-info") (v "0.111.0-rc5") (h "0s2c0pr2qi5mk2agdgp7kq6f4pjb8ms5xrlh665ls0ypqr6cys72")))

(define-public crate-ckb-build-info-0.111.0-rc6 (c (n "ckb-build-info") (v "0.111.0-rc6") (h "0z920wl2n7c3kdga9zqpjbzag5k1zi1436pn9vrsn608xzj2ww7i")))

(define-public crate-ckb-build-info-0.111.0-rc7 (c (n "ckb-build-info") (v "0.111.0-rc7") (h "04d4n2fmxy6f54dgdr51f7iqip8grnj4swj50hz8djgrqzx17hwi")))

(define-public crate-ckb-build-info-0.111.0-rc8 (c (n "ckb-build-info") (v "0.111.0-rc8") (h "1hb4yzj88r00vbqfsym1z7jmc1xz39h81y38hvfi20313qwrwvl1")))

(define-public crate-ckb-build-info-0.110.1-rc1 (c (n "ckb-build-info") (v "0.110.1-rc1") (h "1wawbyxm39whgxzxmcygdyipv0jgcnn838gy30342ci4j8kvvql6")))

(define-public crate-ckb-build-info-0.110.1-rc2 (c (n "ckb-build-info") (v "0.110.1-rc2") (h "17aghy7jrzag05qp6pz2yrk129bzlgd2xv82cra399xya1a95ybr")))

(define-public crate-ckb-build-info-0.111.0-rc9 (c (n "ckb-build-info") (v "0.111.0-rc9") (h "0md63hrf900zymki2phif0949hlijp5c678qh5h28zgfvhgskq2g")))

(define-public crate-ckb-build-info-0.111.0-rc10 (c (n "ckb-build-info") (v "0.111.0-rc10") (h "1xm5337g5c30mwqv7z24r0v7qvy81wandf1xmzg29snhi3vmgnnn")))

(define-public crate-ckb-build-info-0.110.1 (c (n "ckb-build-info") (v "0.110.1") (h "08jwl5fra0dqhyd28q11ncfj6pc9qzkq2djh62wjvaw2z2nzmlk9")))

(define-public crate-ckb-build-info-0.110.2-rc1 (c (n "ckb-build-info") (v "0.110.2-rc1") (h "1vicf43jb2ldgfvrl8a77x9wj2qg5bj9ixpqf6qpri5gq22f185n")))

(define-public crate-ckb-build-info-0.111.0-rc11 (c (n "ckb-build-info") (v "0.111.0-rc11") (h "1rwrhskw567ddyyj3l9ac9573mwldgb37cf7n8b6z57zhpb81b4s")))

(define-public crate-ckb-build-info-0.110.2-rc2 (c (n "ckb-build-info") (v "0.110.2-rc2") (h "0qlpx6pdyyvh0hdgy7c3py4hd4zl87j1n4al3pnk2xjvj2ss4ykh")))

(define-public crate-ckb-build-info-0.111.0-rc12 (c (n "ckb-build-info") (v "0.111.0-rc12") (h "1p7y5vpk4my1hjfz0zf5fxx5rz8zb7cwg76n0lg167bhg6zgpay2")))

(define-public crate-ckb-build-info-0.110.2 (c (n "ckb-build-info") (v "0.110.2") (h "1ffz6c5q6brcz9m720902qzd7h7vswxky0dph4gcjp80w7schkz2")))

(define-public crate-ckb-build-info-0.111.0 (c (n "ckb-build-info") (v "0.111.0") (h "168jz2nk6pn5k6vg93w72ggaqggf6ainr973r3xy850lcgifrvhd")))

(define-public crate-ckb-build-info-0.112.0-rc1 (c (n "ckb-build-info") (v "0.112.0-rc1") (h "06813mvisq3dx8dw8kblxna3i3ys0z1ww4kd69lnv05mqdnj7gbh")))

(define-public crate-ckb-build-info-0.112.0-rc2 (c (n "ckb-build-info") (v "0.112.0-rc2") (h "0yabjzqa5zkc9v3sclwfybmss98y4w06q19c8f48hc6nkx8q4wfm")))

(define-public crate-ckb-build-info-0.112.0 (c (n "ckb-build-info") (v "0.112.0") (h "1m9amv1ci42wnngnq6mk4b1s6lfm35117fh4wwx3rzhm6vb3nkkn")))

(define-public crate-ckb-build-info-0.112.0-rc3 (c (n "ckb-build-info") (v "0.112.0-rc3") (h "1wqqc84kjkayfg8lyrhgkljswpr8k3wl14g51k818m08r0i2d4yy")))

(define-public crate-ckb-build-info-0.112.1 (c (n "ckb-build-info") (v "0.112.1") (h "0kmrg7nggg8jy6xrz6cswqcda3srnp5v18dwqh2vrdv1xxqc4a39")))

(define-public crate-ckb-build-info-0.113.0-rc1 (c (n "ckb-build-info") (v "0.113.0-rc1") (h "1w06vqa919l782klvk12hw871qqhnlxkr542dbs579zxn07qlvkw")))

(define-public crate-ckb-build-info-0.113.0-rc2 (c (n "ckb-build-info") (v "0.113.0-rc2") (h "0f9mgkk4hffzi9n7fqm478hlcgk0xincb758bb7w4ablnpcxg3zg")))

(define-public crate-ckb-build-info-0.113.0-rc3 (c (n "ckb-build-info") (v "0.113.0-rc3") (h "04c8hw9dqdckx8zk9pg8409fs8k67q18f8ifs0dng56sf87s2kd8")))

(define-public crate-ckb-build-info-0.113.0 (c (n "ckb-build-info") (v "0.113.0") (h "06dfz6vsr4hakhfd9l4isknnljqyf4vz6kzm95cs0craab5wmf0a")))

(define-public crate-ckb-build-info-0.113.1-rc1 (c (n "ckb-build-info") (v "0.113.1-rc1") (h "11c2b1x1h13kn6hqv15mmicdybyb7l2h2cv47pjqjakhhbljy8zy")))

(define-public crate-ckb-build-info-0.114.0-rc1 (c (n "ckb-build-info") (v "0.114.0-rc1") (h "0ggxs873n8s482mcn8v072k4nscr6zqj6p6ix3bjalivyc2kr5gb")))

(define-public crate-ckb-build-info-0.113.1-rc2 (c (n "ckb-build-info") (v "0.113.1-rc2") (h "07w0wyzch731ikibvh06lskhldcxfam9gwbcf6a4l7hhbjll2ckq")))

(define-public crate-ckb-build-info-0.113.1 (c (n "ckb-build-info") (v "0.113.1") (h "1n3b4md8f8zcmwxvprfwhyr7i3qjw5xc76ls4n1ypl7k9g49hrp5")))

(define-public crate-ckb-build-info-0.114.0-rc2 (c (n "ckb-build-info") (v "0.114.0-rc2") (h "1fi1mkjyzswha5941csf8f0g9m6fgy7pa0wzv10maqd2ifaprczj")))

(define-public crate-ckb-build-info-0.114.0-rc3 (c (n "ckb-build-info") (v "0.114.0-rc3") (h "160gigvki38arzj5cjsf2p02haid135yzxki5bhs5r2slkyjrb8q")))

(define-public crate-ckb-build-info-0.114.0 (c (n "ckb-build-info") (v "0.114.0") (h "1rjyl0bs2jm778pxmnn3hk47rrw8pwbp4ki4dykq7747fn3f1nn6")))

(define-public crate-ckb-build-info-0.115.0-pre (c (n "ckb-build-info") (v "0.115.0-pre") (h "1xfny24qnjw95g468zpi3fyv2pqx3i8jqmipbg7jimvmvvg9si3l")))

(define-public crate-ckb-build-info-0.115.0-rc1 (c (n "ckb-build-info") (v "0.115.0-rc1") (h "1csj9vghnbbn4cky3bgy39g9rqmix8m54npxkpbw7pvfgx4ccxcd")))

(define-public crate-ckb-build-info-0.115.0-rc2 (c (n "ckb-build-info") (v "0.115.0-rc2") (h "1aqimsq2n8sb8kkh9x07y4z5ah4zznw47jphkpva7kkiadd5v6yq")))

(define-public crate-ckb-build-info-0.115.0 (c (n "ckb-build-info") (v "0.115.0") (h "06f7ffnkj3pn96p2byzmfnlml51305lk50i19ircc7sp54066sj1")))

(define-public crate-ckb-build-info-0.116.0-rc1 (c (n "ckb-build-info") (v "0.116.0-rc1") (h "1a9z3zwaswv08yxzgl3v78s0brqiywvnbg9vc38hwj4wcfb08db3")))

(define-public crate-ckb-build-info-0.116.0-rc2 (c (n "ckb-build-info") (v "0.116.0-rc2") (h "0nb8fx21gqvgz6v0k4f38w2xl06h0h7azcnarkmc9f3j2hvyklva")))

(define-public crate-ckb-build-info-0.116.0 (c (n "ckb-build-info") (v "0.116.0") (h "1q2871ahldi92bvb559sgxjra950xlp6f7af9yh83dzpz5mrv56c") (y #t)))

(define-public crate-ckb-build-info-0.116.1 (c (n "ckb-build-info") (v "0.116.1") (h "0d49h45p01km5w24vy34q6mg31amb799v0nh4i2jqf5d2b5f72c5")))

