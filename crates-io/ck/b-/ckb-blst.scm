(define-module (crates-io ck b- ckb-blst) #:use-module (crates-io))

(define-public crate-ckb-blst-0.3.4 (c (n "ckb-blst") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "zeroize") (r "^1.1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "0rbljwxgdrhh050i4nd45xalff3g5p6wk248xgx4nn4cx6ymm3mh") (f (quote (("std") ("portable") ("force-adx") ("default" "ckb-vm") ("ckb-vm"))))))

(define-public crate-ckb-blst-0.100.0 (c (n "ckb-blst") (v "0.100.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "zeroize") (r "^1.1") (f (quote ("zeroize_derive"))) (d #t) (k 0)))) (h "18xf69cn0r10hhn77asdaiqizcqszg72yqr0m8kspavgjn7nr593") (f (quote (("std") ("portable") ("force-adx") ("default" "ckb-vm") ("ckb-vm"))))))

