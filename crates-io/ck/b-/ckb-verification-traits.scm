(define-module (crates-io ck b- ckb-verification-traits) #:use-module (crates-io))

(define-public crate-ckb-verification-traits-0.42.0 (c (n "ckb-verification-traits") (v "0.42.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.42.0") (d #t) (k 0)))) (h "04gazq9hapfbixvv9pcmfxv6r7k0098x98m0a6dd7m7qjf2qap49")))

(define-public crate-ckb-verification-traits-0.43.0 (c (n "ckb-verification-traits") (v "0.43.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.0") (d #t) (k 0)))) (h "11n3jlvikfpbk2fapc4b0ybl4i50pi9spzv6ryh85vs5jra4apj2")))

(define-public crate-ckb-verification-traits-0.100.0-rc2 (c (n "ckb-verification-traits") (v "0.100.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc2") (d #t) (k 0)))) (h "0rgx4s9136savv0n8myv6x8cn4ps0brhn957wwnf043sjqd104m5")))

(define-public crate-ckb-verification-traits-0.43.2 (c (n "ckb-verification-traits") (v "0.43.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.43.2") (d #t) (k 0)))) (h "0s8n27g8i2hbd8fvs8srczvswr92ghsqag4mhh3w8xipsqg6908b")))

(define-public crate-ckb-verification-traits-0.100.0-rc4 (c (n "ckb-verification-traits") (v "0.100.0-rc4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc4") (d #t) (k 0)))) (h "13m042jf40clbbnrzh3w7k17kri4rny86lk9yqh3yaqzh1jg2w08")))

(define-public crate-ckb-verification-traits-0.100.0-rc5 (c (n "ckb-verification-traits") (v "0.100.0-rc5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0-rc5") (d #t) (k 0)))) (h "1nbqzpq23npj29y8vpws9hkflfyr6f4zbvlq2anslpc0wc5zf067")))

(define-public crate-ckb-verification-traits-0.100.0 (c (n "ckb-verification-traits") (v "0.100.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.100.0") (d #t) (k 0)))) (h "0n379b4kzljf2rl19hrcdy563wwxblpii5rz7pc31xgdgn4iixg2")))

(define-public crate-ckb-verification-traits-0.101.0 (c (n "ckb-verification-traits") (v "0.101.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.0") (d #t) (k 0)))) (h "1al1h7gn30zhqfc2axpplq0wb0gscm6j3zh33las4fd4kdxikj4r")))

(define-public crate-ckb-verification-traits-0.101.1 (c (n "ckb-verification-traits") (v "0.101.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.1") (d #t) (k 0)))) (h "0l72hbn6xdykc7rdw34wgilwix1fmf45jbysk0czhgskkavcv6v3")))

(define-public crate-ckb-verification-traits-0.101.2-testnet1 (c (n "ckb-verification-traits") (v "0.101.2-testnet1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2-testnet1") (d #t) (k 0)))) (h "1ammv8cpcfjwxn3666qv0zn22115fpv8qv3zv9cq8j30f7wgbzsr")))

(define-public crate-ckb-verification-traits-0.101.2 (c (n "ckb-verification-traits") (v "0.101.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.2") (d #t) (k 0)))) (h "1ymbh4s0a1y0qclzsg1jmcia6yddl5ii67ldm0f4zjkafsr7klyy")))

(define-public crate-ckb-verification-traits-0.101.3 (c (n "ckb-verification-traits") (v "0.101.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.3") (d #t) (k 0)))) (h "1lpdxa26m6h5nyh1bk2n6vifl7dpyw2qdd0b7yc7amniihc3g7yg")))

(define-public crate-ckb-verification-traits-0.101.4 (c (n "ckb-verification-traits") (v "0.101.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.4") (d #t) (k 0)))) (h "0i56vmlqkdll41vgiypicwawpgm7m2cd0vxh9m8w3yrizvzl7jz6")))

(define-public crate-ckb-verification-traits-0.101.5 (c (n "ckb-verification-traits") (v "0.101.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.5") (d #t) (k 0)))) (h "0yg1ixbjlx0qsh3qbsklg5j68ddd0fkkz0s54zh8pmfz4wjr3yss") (y #t)))

(define-public crate-ckb-verification-traits-0.101.6 (c (n "ckb-verification-traits") (v "0.101.6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.6") (d #t) (k 0)))) (h "098zdb5vp4qw64wwiqdwb60z7h5fxyymrq74knmff25n3k8k3l87")))

(define-public crate-ckb-verification-traits-0.101.7 (c (n "ckb-verification-traits") (v "0.101.7") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.7") (d #t) (k 0)))) (h "136h6sdamyxydlmqdczvgdw7kmv5gcpj9vpvgyvy9iwwr7d7gr5l")))

(define-public crate-ckb-verification-traits-0.101.8 (c (n "ckb-verification-traits") (v "0.101.8") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.101.8") (d #t) (k 0)))) (h "00wnb19zi9n28r4fxpfm65wb5bfah4jwnn6bw9z9l5cwvb23kijd")))

(define-public crate-ckb-verification-traits-0.102.0 (c (n "ckb-verification-traits") (v "0.102.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.102.0") (d #t) (k 0)))) (h "0nyi978vbyikaw00xalj09xdik3p4hxcrvc6y4gq62yapwb6gkb3") (y #t)))

(define-public crate-ckb-verification-traits-0.103.0 (c (n "ckb-verification-traits") (v "0.103.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.103.0") (d #t) (k 0)))) (h "0fsv5n42smmlb5n19piq7p9spx6yj32y3z59ks254jb4nyi15n7r")))

(define-public crate-ckb-verification-traits-0.104.0 (c (n "ckb-verification-traits") (v "0.104.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.0") (d #t) (k 0)))) (h "1i2lixhi7agcv8zvs7iav0rxqrfpb718jcn48g3jhrkqhpwa7pa2")))

(define-public crate-ckb-verification-traits-0.104.1 (c (n "ckb-verification-traits") (v "0.104.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.104.1") (d #t) (k 0)))) (h "0fzcq85k4ac3bghz9vk980dwsjwy1m6zkayvgdq1h9hlya9sawj8")))

(define-public crate-ckb-verification-traits-0.105.0 (c (n "ckb-verification-traits") (v "0.105.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.0") (d #t) (k 0)))) (h "1kdczia4hlmwlks15flkmf3mx2ryf631agszv6090cqbf84qv26g") (y #t)))

(define-public crate-ckb-verification-traits-0.105.1 (c (n "ckb-verification-traits") (v "0.105.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.105.1") (d #t) (k 0)))) (h "1xzh0r3z0ilz9rzhsb51m9j0giyrikcmy1cwq5y61pi983a0h4mj") (y #t)))

(define-public crate-ckb-verification-traits-0.106.0 (c (n "ckb-verification-traits") (v "0.106.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.106.0") (d #t) (k 0)))) (h "0p8hmwfx7p1a10wxdydw7rv076vphxjd2z3wk1iachk3s578dzgj")))

(define-public crate-ckb-verification-traits-0.107.0 (c (n "ckb-verification-traits") (v "0.107.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.107.0") (d #t) (k 0)))) (h "15nh1dxcd4x8697pnhhi833fzm5737rzd5pp3903q4mp3v80ddqp")))

(define-public crate-ckb-verification-traits-0.108.0 (c (n "ckb-verification-traits") (v "0.108.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.0") (d #t) (k 0)))) (h "0c174w16lmm18rcqjjk0a2z42380q0zvc370336jrry221s5gpl8")))

(define-public crate-ckb-verification-traits-0.108.1 (c (n "ckb-verification-traits") (v "0.108.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.108.1") (d #t) (k 0)))) (h "1j2cm50bmz4nzdxpsi756vfswkb87kvagfby3fj44dwm2851gdxb")))

(define-public crate-ckb-verification-traits-0.109.0 (c (n "ckb-verification-traits") (v "0.109.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.109.0") (d #t) (k 0)))) (h "0mgpjgynand7pdk07z5ynfqaghgakzd4qfv54z8kvij7gbmgz84l")))

(define-public crate-ckb-verification-traits-0.110.0 (c (n "ckb-verification-traits") (v "0.110.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0") (d #t) (k 0)))) (h "146lb2am1cy8nnszkw4bnzphgz5lhdgv2rljk6hp71wasc21y2yh")))

(define-public crate-ckb-verification-traits-0.110.0-rc1 (c (n "ckb-verification-traits") (v "0.110.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.0-rc1") (d #t) (k 0)))) (h "0h8wzgwl7b1yizd4d1kcbaypi25n1irjf3jrsajgz1gfs3fa06l3")))

(define-public crate-ckb-verification-traits-0.111.0-rc2 (c (n "ckb-verification-traits") (v "0.111.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc2") (d #t) (k 0)))) (h "0xz676gc63xxw9pc3dp8i21z0jgl35vlm4lm355ann32ybfkpifw")))

(define-public crate-ckb-verification-traits-0.111.0-rc3 (c (n "ckb-verification-traits") (v "0.111.0-rc3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc3") (d #t) (k 0)))) (h "0lgcxw4lnl1vmdb63z8sgalqs2irblmy2nd03qgd8hf57dzvcdxh")))

(define-public crate-ckb-verification-traits-0.111.0-rc4 (c (n "ckb-verification-traits") (v "0.111.0-rc4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc4") (d #t) (k 0)))) (h "0spsrnnynxd7hf6p2ykd2lyniwj2bvp9dl2vaggqs2mjbi9dd3av")))

(define-public crate-ckb-verification-traits-0.111.0-rc5 (c (n "ckb-verification-traits") (v "0.111.0-rc5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc5") (d #t) (k 0)))) (h "084g41ni23z0jk2gs3b0babfl6xs06dmlwbpb55lij4sllw8a6g3")))

(define-public crate-ckb-verification-traits-0.111.0-rc6 (c (n "ckb-verification-traits") (v "0.111.0-rc6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc6") (d #t) (k 0)))) (h "1fd26sfm35lgf01a1c0hhp4wybxmg32bbq86y28h3jiz483cgzxr")))

(define-public crate-ckb-verification-traits-0.111.0-rc7 (c (n "ckb-verification-traits") (v "0.111.0-rc7") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc7") (d #t) (k 0)))) (h "0ysm8dj1kmig5fs71fh5622m93y9cchcjj2qrc1g8vzxyvbniw2r")))

(define-public crate-ckb-verification-traits-0.111.0-rc8 (c (n "ckb-verification-traits") (v "0.111.0-rc8") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc8") (d #t) (k 0)))) (h "0nza0ahvvs2ikmrbx319n1nqx299dpm3z7sqx32gwvnq8rvz8mbh")))

(define-public crate-ckb-verification-traits-0.110.1-rc1 (c (n "ckb-verification-traits") (v "0.110.1-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc1") (d #t) (k 0)))) (h "0q1d2p9f1pxm46gh57s7s44xxxnfs57yd09p1bbdx93waa43h2j4")))

(define-public crate-ckb-verification-traits-0.110.1-rc2 (c (n "ckb-verification-traits") (v "0.110.1-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1-rc2") (d #t) (k 0)))) (h "003ybsjyl0nhqc7cfd2qfps3x9s0hzqfzj799rp79606qrpawchw")))

(define-public crate-ckb-verification-traits-0.111.0-rc9 (c (n "ckb-verification-traits") (v "0.111.0-rc9") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc9") (d #t) (k 0)))) (h "0vbc6dnangzkq88q88m0h8jsxnrcvxdqdc2b3zah7x43h1lgd9nj")))

(define-public crate-ckb-verification-traits-0.111.0-rc10 (c (n "ckb-verification-traits") (v "0.111.0-rc10") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc10") (d #t) (k 0)))) (h "06jllq64ggmx0bcd44pwbkw4mnmjaqpybix6iazgws9pv9pf2ysr")))

(define-public crate-ckb-verification-traits-0.110.1 (c (n "ckb-verification-traits") (v "0.110.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.1") (d #t) (k 0)))) (h "0nsflfw38a6wfivxppd2jswvblfks32nbddpz7crgj27csfnyqsm")))

(define-public crate-ckb-verification-traits-0.110.2-rc1 (c (n "ckb-verification-traits") (v "0.110.2-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc1") (d #t) (k 0)))) (h "1qsvws38ar801vfs38knxf8ri4xkqi1c147zzyzfv6z89r1rdq45")))

(define-public crate-ckb-verification-traits-0.111.0-rc11 (c (n "ckb-verification-traits") (v "0.111.0-rc11") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc11") (d #t) (k 0)))) (h "1ibcz84qz25525i60qngy0bqiwgzdfl320bxmd8djmrpvqk2k1h7")))

(define-public crate-ckb-verification-traits-0.110.2-rc2 (c (n "ckb-verification-traits") (v "0.110.2-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2-rc2") (d #t) (k 0)))) (h "0hx3y7glrgvmyb7q5pcj7winpkr2l4l2x0sdw4zgm2gfcqrrk2f6")))

(define-public crate-ckb-verification-traits-0.111.0-rc12 (c (n "ckb-verification-traits") (v "0.111.0-rc12") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0-rc12") (d #t) (k 0)))) (h "1v3nbmbad5nnz8i3in0w7b7pv8j1y694grh6yj0kkfgjhiw9xcrg")))

(define-public crate-ckb-verification-traits-0.110.2 (c (n "ckb-verification-traits") (v "0.110.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.110.2") (d #t) (k 0)))) (h "1l5zzjx5847mp5xhq7d9yxkqgwh9bfilqhkcy8nhrc7chjwnf7d6")))

(define-public crate-ckb-verification-traits-0.111.0 (c (n "ckb-verification-traits") (v "0.111.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.111.0") (d #t) (k 0)))) (h "1zi75yz1i8qvbwjvnm1j961vlka98aaba0q1qqk0z4qzzzgby6xs")))

(define-public crate-ckb-verification-traits-0.112.0-rc1 (c (n "ckb-verification-traits") (v "0.112.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc1") (d #t) (k 0)))) (h "1kwlrpscikq2nc8wxnsyx035d78lw7cza3kk0x3pfmb4621pzn6l")))

(define-public crate-ckb-verification-traits-0.112.0-rc2 (c (n "ckb-verification-traits") (v "0.112.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc2") (d #t) (k 0)))) (h "16hv4071mrbravnqai5hdd3amf4xm11q0i4h17fb8xj6v6h2dfm0")))

(define-public crate-ckb-verification-traits-0.112.0 (c (n "ckb-verification-traits") (v "0.112.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0") (d #t) (k 0)))) (h "0i1w70p7qbg2708lwfab2c3fykq5xa7gxxqds6g782m4p8cb888s")))

(define-public crate-ckb-verification-traits-0.112.0-rc3 (c (n "ckb-verification-traits") (v "0.112.0-rc3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.0-rc3") (d #t) (k 0)))) (h "0kgl0xhbm07vbb69mrwr7ymcwk15vblgxzllwixnzxkichylvm2x")))

(define-public crate-ckb-verification-traits-0.112.1 (c (n "ckb-verification-traits") (v "0.112.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.112.1") (d #t) (k 0)))) (h "1zb69ql00zr6ksh50ndj7axzxb8qix0rb2iq32dchqmkfk15cd6l")))

(define-public crate-ckb-verification-traits-0.113.0-rc1 (c (n "ckb-verification-traits") (v "0.113.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc1") (d #t) (k 0)))) (h "1pm9l6fd88sl2bml9hkfipgbyyf9j5bgd0l472ws97mkgni43qya")))

(define-public crate-ckb-verification-traits-0.113.0-rc2 (c (n "ckb-verification-traits") (v "0.113.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc2") (d #t) (k 0)))) (h "17hm52bkv8s9af14kzpmlsbb34xclsz3qk9am2hsks39gs23pwsa")))

(define-public crate-ckb-verification-traits-0.113.0-rc3 (c (n "ckb-verification-traits") (v "0.113.0-rc3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0-rc3") (d #t) (k 0)))) (h "0llzj4lsb9nyk5drcjh2qh7allr1iw9lbf31g0xfh1n2wp9mxgmk")))

(define-public crate-ckb-verification-traits-0.113.0 (c (n "ckb-verification-traits") (v "0.113.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.0") (d #t) (k 0)))) (h "0drbjcnv58nv887q9x8hlvx1rc1wnnmkzrl5lhf3xcdz3sl53w58")))

(define-public crate-ckb-verification-traits-0.113.1-rc1 (c (n "ckb-verification-traits") (v "0.113.1-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc1") (d #t) (k 0)))) (h "1q8fgg8gihwn1v2v87yyh5k8dwhvr1ipi4xkkp1ylsipwys14lpp")))

(define-public crate-ckb-verification-traits-0.114.0-rc1 (c (n "ckb-verification-traits") (v "0.114.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc1") (d #t) (k 0)))) (h "0yk2qcj52xmsi6c22rfmm477n9pj5xrg95ma4sc419nks61r8bfn")))

(define-public crate-ckb-verification-traits-0.113.1-rc2 (c (n "ckb-verification-traits") (v "0.113.1-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1-rc2") (d #t) (k 0)))) (h "1hlksqqqri7rnwvn5qxkss02y18vdksiy9736kwflg9lm24411pj")))

(define-public crate-ckb-verification-traits-0.113.1 (c (n "ckb-verification-traits") (v "0.113.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.113.1") (d #t) (k 0)))) (h "1f5v5p071xa71ffvrn5wfnqazx0gn5dsb9p1x6s1ar5xyirmk7bw")))

(define-public crate-ckb-verification-traits-0.114.0-rc2 (c (n "ckb-verification-traits") (v "0.114.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc2") (d #t) (k 0)))) (h "195rjb62g2djq7gg351jwmllbrh4jdhqrw800zz7kxasixp9n5i7")))

(define-public crate-ckb-verification-traits-0.114.0-rc3 (c (n "ckb-verification-traits") (v "0.114.0-rc3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0-rc3") (d #t) (k 0)))) (h "07mk4xh8vlab04dqqy2yi69cf7jl31y4l441wys06gp576qjmx2b")))

(define-public crate-ckb-verification-traits-0.114.0 (c (n "ckb-verification-traits") (v "0.114.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.114.0") (d #t) (k 0)))) (h "0fjppswmgyf6n5ybvia4i4icajqp1j6iyygraa6mx8wy0z4lnbxr")))

(define-public crate-ckb-verification-traits-0.115.0-pre (c (n "ckb-verification-traits") (v "0.115.0-pre") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-pre") (d #t) (k 0)))) (h "0whc84hz5njjjxc4a5jz4yi9q2327z5jwpwjixav8m4y79gb0mk5")))

(define-public crate-ckb-verification-traits-0.115.0-rc1 (c (n "ckb-verification-traits") (v "0.115.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc1") (d #t) (k 0)))) (h "0q43jw4xy6zch6xl5xf9b5phl86p1aqxrbhnwi1w0z05x7i7xa0k")))

(define-public crate-ckb-verification-traits-0.115.0-rc2 (c (n "ckb-verification-traits") (v "0.115.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0-rc2") (d #t) (k 0)))) (h "1ppphlrjrn8vrpd59kg8h11y6rsxkjxfps6dki7qcs9mxl1gmbs8")))

(define-public crate-ckb-verification-traits-0.115.0 (c (n "ckb-verification-traits") (v "0.115.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.115.0") (d #t) (k 0)))) (h "1izqk8nhvl0x71bkjfiwwlps2vzig3f0syjxgqwbhxzlsndar39x")))

(define-public crate-ckb-verification-traits-0.116.0-rc1 (c (n "ckb-verification-traits") (v "0.116.0-rc1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc1") (d #t) (k 0)))) (h "0y21c9vk477jci39v7wzv1a55i1anpj9zzrvsx6xsg0biw2sk54p")))

(define-public crate-ckb-verification-traits-0.116.0-rc2 (c (n "ckb-verification-traits") (v "0.116.0-rc2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0-rc2") (d #t) (k 0)))) (h "1q7559bq0djk97r175is531xdnn0gsgszfj3jw901vp6ivlcq2jp")))

(define-public crate-ckb-verification-traits-0.116.0 (c (n "ckb-verification-traits") (v "0.116.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.0") (d #t) (k 0)))) (h "1zs5vn75iyy0n5rv8vqci8cmf4x6h9k7fkqlvvzwg71qfnwb7mm7") (y #t)))

(define-public crate-ckb-verification-traits-0.116.1 (c (n "ckb-verification-traits") (v "0.116.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ckb-error") (r "=0.116.1") (d #t) (k 0)))) (h "1rz88nbcrm654hw71algf7njspavc7z9gb3q5xfqsks5q88ca8ad")))

