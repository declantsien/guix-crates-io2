(define-module (crates-io ck b- ckb-occupied-capacity-core) #:use-module (crates-io))

(define-public crate-ckb-occupied-capacity-core-0.37.0-pre (c (n "ckb-occupied-capacity-core") (v "0.37.0-pre") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0chf66d9hzikc5n9njkgx5mnkscjkphxm5l23a7ns6nxs6dm4b8k")))

(define-public crate-ckb-occupied-capacity-core-0.37.0 (c (n "ckb-occupied-capacity-core") (v "0.37.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xbzmdkpshiknm2gmvg28l1lssj6gx33r8dwxw4f0fvix9pgvajg")))

(define-public crate-ckb-occupied-capacity-core-0.38.0 (c (n "ckb-occupied-capacity-core") (v "0.38.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4y7rbjrsvcbz2vzbnfnfxr6l3gpkf9k9m0g12jgff052sz3x8i")))

(define-public crate-ckb-occupied-capacity-core-0.39.0 (c (n "ckb-occupied-capacity-core") (v "0.39.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ij1z0bgbkzvw5435kpvx3m88zc7p9cw1cbihz2zw5hwdqxsjcl2")))

(define-public crate-ckb-occupied-capacity-core-0.39.1 (c (n "ckb-occupied-capacity-core") (v "0.39.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vlj490ajj912y2nlw1s4asi3mlavb780s0spx8r951wf1yk2qgg")))

(define-public crate-ckb-occupied-capacity-core-0.40.0 (c (n "ckb-occupied-capacity-core") (v "0.40.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "185vn9aihzj5bg2chs3j9p87x8sfc0vqz15hvczw9wsv9l7jicxw")))

(define-public crate-ckb-occupied-capacity-core-0.42.0 (c (n "ckb-occupied-capacity-core") (v "0.42.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l8gzxqzmfgnjvbgkqiyqs76hgw2qi6nmfn3kbpvzmaj0fbf4jjx")))

(define-public crate-ckb-occupied-capacity-core-0.43.0 (c (n "ckb-occupied-capacity-core") (v "0.43.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z3v572zjfjym4jqmfy4yax2ajpg3g0zzlkyp9lxbg70cq2i9qpf")))

(define-public crate-ckb-occupied-capacity-core-0.100.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.100.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vaxmawa64l5r14yzmcbwalbbfhcj133shakzyfjy0pgxs97li40")))

(define-public crate-ckb-occupied-capacity-core-0.43.2 (c (n "ckb-occupied-capacity-core") (v "0.43.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q62gzshs0q8vj7y6a36r7fgr4k1fhj841fdjl15d6wibwnfxg2a")))

(define-public crate-ckb-occupied-capacity-core-0.100.0-rc3 (c (n "ckb-occupied-capacity-core") (v "0.100.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h0s6icabr8w9wczb26m9w8gkx6n040v6l1h4fxkxx55c7vnj63b") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.100.0-rc4 (c (n "ckb-occupied-capacity-core") (v "0.100.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sp3a18vfvl87ixs96hhfsv5wk62ngjvk47g02pcx9z211a0k2s8")))

(define-public crate-ckb-occupied-capacity-core-0.100.0-rc5 (c (n "ckb-occupied-capacity-core") (v "0.100.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f96h4qpd8bq4kl5dsvg29jac0ijzn6zmmq5r8gkq1c9ihrgg31g")))

(define-public crate-ckb-occupied-capacity-core-0.100.0 (c (n "ckb-occupied-capacity-core") (v "0.100.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17clvajxy8rvj9qip1zcahr5q6ln3xjmd7vcf8shv9a817a3d8kl")))

(define-public crate-ckb-occupied-capacity-core-0.101.0 (c (n "ckb-occupied-capacity-core") (v "0.101.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15jx7ycvawfgy9nfb5l6p7s4q7y3j4d0f87lif31pd6nw7q4s9m4")))

(define-public crate-ckb-occupied-capacity-core-0.101.1 (c (n "ckb-occupied-capacity-core") (v "0.101.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qbh59za8fk9gk3xcfz267jr4yb8rqjkhs8shw03pnn4nx9ipphf")))

(define-public crate-ckb-occupied-capacity-core-0.101.2-testnet1 (c (n "ckb-occupied-capacity-core") (v "0.101.2-testnet1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v3mdp3l7a16lyi5lwxanf8gwgfrbiigsxkn76sbcxsnxzgdisvp")))

(define-public crate-ckb-occupied-capacity-core-0.101.2 (c (n "ckb-occupied-capacity-core") (v "0.101.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gfbyr5gdlic48asangdbq30048fdc4axmbxlgzmysmlx1g63fp0")))

(define-public crate-ckb-occupied-capacity-core-0.101.3 (c (n "ckb-occupied-capacity-core") (v "0.101.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jzvbv90jfqjkzsny7c71l5q3r1skcscgvrkjgkfy3cr8dcxkcml")))

(define-public crate-ckb-occupied-capacity-core-0.101.4 (c (n "ckb-occupied-capacity-core") (v "0.101.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q1248jkb8vfj5f1hk6jy4qa3idiwcy4bl5kz4mzrh0374q72r0d")))

(define-public crate-ckb-occupied-capacity-core-0.101.5 (c (n "ckb-occupied-capacity-core") (v "0.101.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j84hixypnwcafdkz7mcbr1lfgc57wviqw8axwzigmcgg3rgsccm") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.101.6 (c (n "ckb-occupied-capacity-core") (v "0.101.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pja7152f85jr60x09gm7js14vjm4dxa5ij6m3s9msq82rriziaj")))

(define-public crate-ckb-occupied-capacity-core-0.101.7 (c (n "ckb-occupied-capacity-core") (v "0.101.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04s1mwvgdzjyyfray184bslq9aapqlzmk9w15shh981k85n18qma")))

(define-public crate-ckb-occupied-capacity-core-0.101.8 (c (n "ckb-occupied-capacity-core") (v "0.101.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "059r101qvz0j4gzy9qvzzxq329mz0ajgjk58p1wdqdn44qp2jzbk")))

(define-public crate-ckb-occupied-capacity-core-0.102.0 (c (n "ckb-occupied-capacity-core") (v "0.102.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lwzb4g9kmd64zy0d0a4d7qsl2l122dk7kz5gks63k5m61iafhvb") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.103.0 (c (n "ckb-occupied-capacity-core") (v "0.103.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0whisr6wm955s9ca1bphgh7v8ahlqbf0ldnzqrha3m7l7gqqb00i")))

(define-public crate-ckb-occupied-capacity-core-0.104.0 (c (n "ckb-occupied-capacity-core") (v "0.104.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "188galvl12ralf92f0il6hgkwanqzn9v2876n3wxawfgqgah6fkp")))

(define-public crate-ckb-occupied-capacity-core-0.104.1 (c (n "ckb-occupied-capacity-core") (v "0.104.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qc1v7drvicc4n18w1mjgs3k6shqij7r892k0fix95bad8kv0976")))

(define-public crate-ckb-occupied-capacity-core-0.105.0 (c (n "ckb-occupied-capacity-core") (v "0.105.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pr574x2kmnwnl5sz643bsnirawf3azjaaizilrrkqsxm885fkg6") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.105.1 (c (n "ckb-occupied-capacity-core") (v "0.105.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q701ivl6gp17b59gf4219nsr62q6dqjnd7024khgzm30q1qzr03") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.106.0 (c (n "ckb-occupied-capacity-core") (v "0.106.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0azfrp87wixw040zyw6ia5c0nmvq8nrj2if636slbj0q8j18fwah")))

(define-public crate-ckb-occupied-capacity-core-0.107.0 (c (n "ckb-occupied-capacity-core") (v "0.107.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j6lvszw0dmwwx6l8kwscwfh1pl89spjx956cpckkc77yki5dqjp")))

(define-public crate-ckb-occupied-capacity-core-0.108.0 (c (n "ckb-occupied-capacity-core") (v "0.108.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kfis0g0rbr7izb9kdyb6kjsplawkff2h277z4892h9ckas7fqf")))

(define-public crate-ckb-occupied-capacity-core-0.108.1 (c (n "ckb-occupied-capacity-core") (v "0.108.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ksf5lwsxamg6yzpbyv5hn5799dajxbm752wvq0sayci0zfkkslx")))

(define-public crate-ckb-occupied-capacity-core-0.109.0 (c (n "ckb-occupied-capacity-core") (v "0.109.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w379aw421cf89jsmdyi8i822b0dfanqh7yb0ihirfa8n7wr055n")))

(define-public crate-ckb-occupied-capacity-core-0.110.0 (c (n "ckb-occupied-capacity-core") (v "0.110.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dddx9m72358rc1vn6zjyh0pc9zlz5avayngk592lxjw22a36piv")))

(define-public crate-ckb-occupied-capacity-core-0.110.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.110.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l33gd6fqlaalbnjv9xkpi7995ynki448j5b2apggpvhinpmwgfi")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08mci0wj8fr8sfkk3i1h87dsd1k53jjx61fwcmnw9x4k675wcaf9")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc3 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0svyfi90gpjxisi7ry79shnv1s8sd7d4dj3a960b40qdcidbmcrq")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc4 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vxvfhpwyk63k3p6v210s94svv662ri9i9g49cl2mnm0qnf2n9li")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc5 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04iwv6aaaw819bin2vs7bf12s4ipnd846z1pxj6kxrf8hvjq8ga0")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc6 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hshp3kbf11wk11gnb1maxxkylpflyqbsxcwx3ibkbxjzkazmyj7")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc7 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "107vr9prjvirvps6ri1sj0vbm1j2angj2l0zhp0fgf6b6xa88a92")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc8 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bpd40micp2i2l7yxzzpkdwlmmf8fgfnbxs2hgig0wpv5d4ycz8d")))

(define-public crate-ckb-occupied-capacity-core-0.110.1-rc1 (c (n "ckb-occupied-capacity-core") (v "0.110.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hpx4y2bgicm5x1p6v1n0jky75nz9yzcsfih03pxsc3and45yv2g")))

(define-public crate-ckb-occupied-capacity-core-0.110.1-rc2 (c (n "ckb-occupied-capacity-core") (v "0.110.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rr5l1dxfbdjp3dljpvjl1g18g0172nhf0gvpixfniz2jlpp7hbc")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc9 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13iqqrdm0a7ww9g9qf7v0vq4cx4bp4310jpha2brn62liblgr529")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc10 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l3m9h1lj20s83rgyw1v73n4fkdjg48lhd2wfcf2mr34laldr82n")))

(define-public crate-ckb-occupied-capacity-core-0.110.1 (c (n "ckb-occupied-capacity-core") (v "0.110.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l3lq2qis29yizsgnlq358ymaklljfqcmji8lb39r8303c3fas8x")))

(define-public crate-ckb-occupied-capacity-core-0.110.2-rc1 (c (n "ckb-occupied-capacity-core") (v "0.110.2-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "126b6jqg9r8zbrw0v5718wmrm6yng3zyr9gbrins7r6idxfn9fm7")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc11 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dapjzn07hdz1mmq3czcd918hcwqvdl79k82bdpdkkr5lgrcf4b2")))

(define-public crate-ckb-occupied-capacity-core-0.110.2-rc2 (c (n "ckb-occupied-capacity-core") (v "0.110.2-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08x8hbcr7fyx5g23gr2xa53kcrwdwsflh5pamyq4lbd4x9ilmq5n")))

(define-public crate-ckb-occupied-capacity-core-0.111.0-rc12 (c (n "ckb-occupied-capacity-core") (v "0.111.0-rc12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "062lnxfw39gsar8x9w47n8ymmgzqglfd4lixgqd6k6qp4if04x46")))

(define-public crate-ckb-occupied-capacity-core-0.110.2 (c (n "ckb-occupied-capacity-core") (v "0.110.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fvj3yya09fb3wfdh59ys934af7y34y87zr14p2la50d116yvpql")))

(define-public crate-ckb-occupied-capacity-core-0.111.0 (c (n "ckb-occupied-capacity-core") (v "0.111.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pai4in9ab0gljm0g6p5na50rfqppmnwanqlnxf9k2lgp5nw0bfy")))

(define-public crate-ckb-occupied-capacity-core-0.112.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.112.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18rjjm5nbipszgs0pdbajj3gkrmk7lnnshq9mx16961vxaz9hznq")))

(define-public crate-ckb-occupied-capacity-core-0.112.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.112.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "197263vdnm9vyg9arwb7h0q81qmzs7qzcidvzvbw1w6ryp8lksds")))

(define-public crate-ckb-occupied-capacity-core-0.112.0 (c (n "ckb-occupied-capacity-core") (v "0.112.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sbrj7bg1x19dhywpy759ipkwjksp86z8pya3g6a0iyzr6cfm05n")))

(define-public crate-ckb-occupied-capacity-core-0.112.0-rc3 (c (n "ckb-occupied-capacity-core") (v "0.112.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dqdc0sqwd1hfl46il65h0ljzkz6ffgx1ai7yqn0rksjr3n2v5lb")))

(define-public crate-ckb-occupied-capacity-core-0.112.1 (c (n "ckb-occupied-capacity-core") (v "0.112.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dbnj6mp3s3xlk4m99ssvvdw3367n80adsbnsfwzxr3fbmrfisx4")))

(define-public crate-ckb-occupied-capacity-core-0.113.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.113.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1aihf314yky12s8c65zhxx4n05466lckld4ik6cis26cll4inza9")))

(define-public crate-ckb-occupied-capacity-core-0.113.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.113.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mb0pvm0rxn50sm09zw344lqx0lfixvwd0a69qx87ag31hl1jqkl")))

(define-public crate-ckb-occupied-capacity-core-0.113.0-rc3 (c (n "ckb-occupied-capacity-core") (v "0.113.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jny0766pwb3lx1q4bszcikg053lb2d012dhrc3394jcf1xlwxrs")))

(define-public crate-ckb-occupied-capacity-core-0.113.0 (c (n "ckb-occupied-capacity-core") (v "0.113.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yzjfw7r6rcgirjp52d556j10jfn2jdwkd3md28a3vf9vl7sgnrm")))

(define-public crate-ckb-occupied-capacity-core-0.113.1-rc1 (c (n "ckb-occupied-capacity-core") (v "0.113.1-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xg43dsc4pdaqhzbjwz5k0lh0bsj77jy7dr9si6jbj65hc3pjc52")))

(define-public crate-ckb-occupied-capacity-core-0.114.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.114.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15d3h7wm37j7vf89zd5lwqmwnaz511wwjn38d4pn8acn7x0qb37n")))

(define-public crate-ckb-occupied-capacity-core-0.113.1-rc2 (c (n "ckb-occupied-capacity-core") (v "0.113.1-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "112ia9z5fimmkjg8kdsxri7vhp8fnpgc6ahzivsmb278d74874pc")))

(define-public crate-ckb-occupied-capacity-core-0.113.1 (c (n "ckb-occupied-capacity-core") (v "0.113.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1phm5bppkp13d47y6dzbxhb88f84xgr1byvzba6vxa33h1wrl68l")))

(define-public crate-ckb-occupied-capacity-core-0.114.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.114.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zhnsdcba96ahsziimsbrvrym2fr6li478q7psdf7pas1dcn19i6")))

(define-public crate-ckb-occupied-capacity-core-0.114.0-rc3 (c (n "ckb-occupied-capacity-core") (v "0.114.0-rc3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0682gbjy0xzvf3ssiz3c3nlwfw43gbhb24wkfr2562vqqc26ix6x")))

(define-public crate-ckb-occupied-capacity-core-0.114.0 (c (n "ckb-occupied-capacity-core") (v "0.114.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v3baajshgpbav1ndpm540adx45r4b8d139gs1q5pa2nspifp28m")))

(define-public crate-ckb-occupied-capacity-core-0.115.0-pre (c (n "ckb-occupied-capacity-core") (v "0.115.0-pre") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pp3iman79jjlfi5kbvd0j6p4vfi68jw5v9f5ww2ipa3jbz1yyvj")))

(define-public crate-ckb-occupied-capacity-core-0.115.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.115.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r191ln2p79yikys8nagd2ph7yqn7w3y80hnnngsq36jf0qyrp1w")))

(define-public crate-ckb-occupied-capacity-core-0.115.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.115.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05gywspaiq1405dnyf17z7b2ris7a7f40ch5pwzndywx7c9k97kh")))

(define-public crate-ckb-occupied-capacity-core-0.115.0 (c (n "ckb-occupied-capacity-core") (v "0.115.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dqv0yhcv84ngymm2av95sxgpz4qyzmxgs1inv0fbms2v56pfgcm")))

(define-public crate-ckb-occupied-capacity-core-0.116.0-rc1 (c (n "ckb-occupied-capacity-core") (v "0.116.0-rc1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0s0hipyrqphq3jkfvcc2ss6mv6c2cw31gyd63ccj6vxqainpyhqk")))

(define-public crate-ckb-occupied-capacity-core-0.116.0-rc2 (c (n "ckb-occupied-capacity-core") (v "0.116.0-rc2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p9cfbaqssqbc9w22pbdnnmzl3zrqfmklg9dllm26vjr100qw6j6")))

(define-public crate-ckb-occupied-capacity-core-0.116.0 (c (n "ckb-occupied-capacity-core") (v "0.116.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12iaqk2c6d8ac53bv6klirxsjh3qqb7y2w9vqgbjr01djiq4q47y") (y #t)))

(define-public crate-ckb-occupied-capacity-core-0.116.1 (c (n "ckb-occupied-capacity-core") (v "0.116.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xp1i4c4py5dw5160lp4lks978h2s0py52mcslkan95sjq4xjgm6")))

