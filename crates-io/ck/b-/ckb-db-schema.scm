(define-module (crates-io ck b- ckb-db-schema) #:use-module (crates-io))

(define-public crate-ckb-db-schema-0.39.0 (c (n "ckb-db-schema") (v "0.39.0") (h "0agmz8lpwalcz3ywb53q14ax4ryblmpq5ssllyg8kjlqi5isvzr9")))

(define-public crate-ckb-db-schema-0.39.1 (c (n "ckb-db-schema") (v "0.39.1") (h "1gv6wrq3y98hdjmcfmpnbzlb8w1hrlghdfiq4zngb5lfivhnipx9")))

(define-public crate-ckb-db-schema-0.40.0 (c (n "ckb-db-schema") (v "0.40.0") (h "0mnk7748b3iksn4n8vqy4csdd7fqr8q2i8byz3mvb0b250abpw1v")))

(define-public crate-ckb-db-schema-0.42.0 (c (n "ckb-db-schema") (v "0.42.0") (h "054w0a7c1qvv511i76in66791zrfw59ykhb0yazp0sjsvwif9fy1")))

(define-public crate-ckb-db-schema-0.43.0 (c (n "ckb-db-schema") (v "0.43.0") (h "1g8nn7yr3ixmfnp3w57kf6b7qwa8ddhvwbn0l5gywqjxfpin3hp6")))

(define-public crate-ckb-db-schema-0.100.0-rc2 (c (n "ckb-db-schema") (v "0.100.0-rc2") (h "0810z4vsgmqqrfxihg93szazzgcr10pps2rihymh310zzwg9n1vd")))

(define-public crate-ckb-db-schema-0.43.2 (c (n "ckb-db-schema") (v "0.43.2") (h "14nsgl59p3r8sbmhf4dxidnjs8jf7rva3xhb9zx9hjhixv1zz9p2")))

(define-public crate-ckb-db-schema-0.100.0-rc3 (c (n "ckb-db-schema") (v "0.100.0-rc3") (h "1hw9si5dydzlq3hh6ycp9k9yq6srkqydjmcy4r40cxcrwf4rbswz") (y #t)))

(define-public crate-ckb-db-schema-0.100.0-rc4 (c (n "ckb-db-schema") (v "0.100.0-rc4") (h "13qlhb2cbnkz2zxd36lmdad5si59zgv89yw86snzwcymy4a3ymf3")))

(define-public crate-ckb-db-schema-0.100.0-rc5 (c (n "ckb-db-schema") (v "0.100.0-rc5") (h "1c5hr5xcgz9122xmhkc7qx9cq8gm8q3as6mgri8py0ly0l25gpcj")))

(define-public crate-ckb-db-schema-0.100.0 (c (n "ckb-db-schema") (v "0.100.0") (h "0jgnzhjikfnkx9wc1mpv2a2kwbvw5nzv13856i17bv3amkf7fxif")))

(define-public crate-ckb-db-schema-0.101.0 (c (n "ckb-db-schema") (v "0.101.0") (h "1rir36hgcnz6nil94jqjdxxpg022syyfdhj0adss7ms8scnldrw6")))

(define-public crate-ckb-db-schema-0.101.1 (c (n "ckb-db-schema") (v "0.101.1") (h "11fv3d60bchzbcs4bz4xgwajz8b0q3r34g15680iprs3i0rx2vrg")))

(define-public crate-ckb-db-schema-0.101.2-testnet1 (c (n "ckb-db-schema") (v "0.101.2-testnet1") (h "1v1zx6fx2xvqrjk50wsjlkydirw66d71iy6jwxvip9a9f1iyq1xd")))

(define-public crate-ckb-db-schema-0.101.2 (c (n "ckb-db-schema") (v "0.101.2") (h "19jh8bvsxhzmqbyplqmgn4wy9s11cq2cmzbmg1w0pmr6gqj56akw")))

(define-public crate-ckb-db-schema-0.101.3 (c (n "ckb-db-schema") (v "0.101.3") (h "0amwrdbj38c44a0y38gv1mqsrwhryb6alhxyla1sg0l98wi4bfdg")))

(define-public crate-ckb-db-schema-0.101.4 (c (n "ckb-db-schema") (v "0.101.4") (h "1n5h14w8w19slv745dwd0wcjil1fi0p7vfybh4rxsak3q7x0i0pk")))

(define-public crate-ckb-db-schema-0.101.5 (c (n "ckb-db-schema") (v "0.101.5") (h "1r4cri7wssxljzfyz7p35fxwcrrggrsbfzmbfhkpmcrzbkqw5xgb") (y #t)))

(define-public crate-ckb-db-schema-0.101.6 (c (n "ckb-db-schema") (v "0.101.6") (h "1dhcw3gdd0ii20qq0waha1h6za5jl3wibas08vl5ry78v1mavjqd")))

(define-public crate-ckb-db-schema-0.101.7 (c (n "ckb-db-schema") (v "0.101.7") (h "1cillq1y0qj58frribc5hvyypw8j01b7y31zpg77ygnlgwazis3p")))

(define-public crate-ckb-db-schema-0.101.8 (c (n "ckb-db-schema") (v "0.101.8") (h "14apw1svpb76wy0wfjkdcqmi8hm00hijp2l06yniwc2gjh0bwcy7")))

(define-public crate-ckb-db-schema-0.102.0 (c (n "ckb-db-schema") (v "0.102.0") (h "1xrblxzn2a9af3w9hayf0hd45lxq5mzfkf3x2iayj1rwh52rpq4k") (y #t)))

(define-public crate-ckb-db-schema-0.103.0 (c (n "ckb-db-schema") (v "0.103.0") (h "097idfk8sbhm5wgxanjx0f766y9bln8vvk3s3s6zrw81mxv02rqz")))

(define-public crate-ckb-db-schema-0.104.0 (c (n "ckb-db-schema") (v "0.104.0") (h "1dypv6189hbb303m8624qsa5imd05g0ws62zqmm5x0xa8cb475ld")))

(define-public crate-ckb-db-schema-0.104.1 (c (n "ckb-db-schema") (v "0.104.1") (h "1imk7s4bjd6dbn8c5z2xcsgjmahacxgzz2sk618p8bh9k9sh7iyq")))

(define-public crate-ckb-db-schema-0.105.0 (c (n "ckb-db-schema") (v "0.105.0") (h "0nrw4b0f2b931c5v6v3y0hn2dmdr9ln3gzn5bb9ak857p382cg5z") (y #t)))

(define-public crate-ckb-db-schema-0.105.1 (c (n "ckb-db-schema") (v "0.105.1") (h "1g6nlgbb3in9003v5xa2szinph084jkiy6vlf0vr8vzx7qhslrih") (y #t)))

(define-public crate-ckb-db-schema-0.106.0 (c (n "ckb-db-schema") (v "0.106.0") (h "0dx99kdg9dadakmx6pc0bh6ajp367yx0i3msm1qh3xvai4z9mp9q")))

(define-public crate-ckb-db-schema-0.107.0 (c (n "ckb-db-schema") (v "0.107.0") (h "1hfv8akbwlr493a5bn39y49s03phjnzjpg4r6fkzpz727l3rdgr8")))

(define-public crate-ckb-db-schema-0.108.0 (c (n "ckb-db-schema") (v "0.108.0") (h "03jbnasnhd7i8ff71la5mcqqasihggcvdcmdb86w8qvhhx9273in")))

(define-public crate-ckb-db-schema-0.108.1 (c (n "ckb-db-schema") (v "0.108.1") (h "1hv14wyp79nikgjz5m3757gv6qc5gvj475zz0k867wnzay0fxsf2")))

(define-public crate-ckb-db-schema-0.109.0 (c (n "ckb-db-schema") (v "0.109.0") (h "01cav4mrs4zj1al2iqsgj8vpfiabgwk5q9yq0wl487ppnz05nr8k")))

(define-public crate-ckb-db-schema-0.110.0 (c (n "ckb-db-schema") (v "0.110.0") (h "0lxjsmnaq48slxlvxsy2a4a2x1p6w4n484xv51ykhgvf18sgs1c3")))

(define-public crate-ckb-db-schema-0.110.0-rc1 (c (n "ckb-db-schema") (v "0.110.0-rc1") (h "1wdwfrvhj3lgwynf952s3qvp7s501rwhm12mw4yrj78m0vhbhm72")))

(define-public crate-ckb-db-schema-0.111.0-rc2 (c (n "ckb-db-schema") (v "0.111.0-rc2") (h "1072bx4pmpjhasz34gi97xmp3fmrp79s82idncm6g149wjsnm23c")))

(define-public crate-ckb-db-schema-0.111.0-rc3 (c (n "ckb-db-schema") (v "0.111.0-rc3") (h "19hby8r5x1dkl5rwn1whss4mi8ld2rwsg9l3vx8gxd88pbc5zrhz")))

(define-public crate-ckb-db-schema-0.111.0-rc4 (c (n "ckb-db-schema") (v "0.111.0-rc4") (h "1g96phph60653s3p1314sjcm91p4njdvi54gfq8znvhjj9hxw0pv")))

(define-public crate-ckb-db-schema-0.111.0-rc5 (c (n "ckb-db-schema") (v "0.111.0-rc5") (h "0z9za5hlcga5xy6h6h4jkbnx1d3faxhvpgwv7bh3sc05zvhpddjf")))

(define-public crate-ckb-db-schema-0.111.0-rc6 (c (n "ckb-db-schema") (v "0.111.0-rc6") (h "0mh5rm4y5z693cngx30a3bm9lrgdfww2ix0zyacr6j126kjd4c2y")))

(define-public crate-ckb-db-schema-0.111.0-rc7 (c (n "ckb-db-schema") (v "0.111.0-rc7") (h "115zyck56scnzz82qklnr6ym06wgykyg85jdybh1rs19gkylja66")))

(define-public crate-ckb-db-schema-0.111.0-rc8 (c (n "ckb-db-schema") (v "0.111.0-rc8") (h "1cdyfan30rkj5qqrscnnbh93g8janvq7m6nxwjp7s0w35fq747p7")))

(define-public crate-ckb-db-schema-0.110.1-rc1 (c (n "ckb-db-schema") (v "0.110.1-rc1") (h "19ssh7189ghkpmzshf0sn3z54nn123bq2hjcx4qjq70a4xx425fd")))

(define-public crate-ckb-db-schema-0.110.1-rc2 (c (n "ckb-db-schema") (v "0.110.1-rc2") (h "0ak2gnx8wvw8r58w61ir36fvv65vwif965abywl8ygfglzlvqrfc")))

(define-public crate-ckb-db-schema-0.111.0-rc9 (c (n "ckb-db-schema") (v "0.111.0-rc9") (h "1xlry997kfjxj1q98fwk6qqpiwibrryvz530m9q3qn4kpkgnapqb")))

(define-public crate-ckb-db-schema-0.111.0-rc10 (c (n "ckb-db-schema") (v "0.111.0-rc10") (h "0wqd8h69hp8dhwyh0p5m3zi2qsnsdl2fhgmvlpgrv6v3i9l4qd5b")))

(define-public crate-ckb-db-schema-0.110.1 (c (n "ckb-db-schema") (v "0.110.1") (h "0ibff2kmaxh4wgbjahfjfxj7vz5svpqwjc5aiddvaki3bjaz1l3s")))

(define-public crate-ckb-db-schema-0.110.2-rc1 (c (n "ckb-db-schema") (v "0.110.2-rc1") (h "1xxahrkxppydzsz84y4nc5py0f4frxzjsf7j9z2i23vi64rynl0b")))

(define-public crate-ckb-db-schema-0.111.0-rc11 (c (n "ckb-db-schema") (v "0.111.0-rc11") (h "1v6daxbl36qjcijip9mh4ws6bj3b787ccds0z1lr3amdq1fvii7b")))

(define-public crate-ckb-db-schema-0.110.2-rc2 (c (n "ckb-db-schema") (v "0.110.2-rc2") (h "1yg4x671c3sqfhzny511zily1nn4mb8b39rfbzl9kvmy1vfp1iq9")))

(define-public crate-ckb-db-schema-0.111.0-rc12 (c (n "ckb-db-schema") (v "0.111.0-rc12") (h "1kpdx688qshln7fvy14sf18yskv6xiqglwplwcyd7gzhqzq2rzp7")))

(define-public crate-ckb-db-schema-0.110.2 (c (n "ckb-db-schema") (v "0.110.2") (h "1shl3spr5c9fk3mi5w1y5nkzb9dswrgn34rkrhggkdrf779jql4c")))

(define-public crate-ckb-db-schema-0.111.0 (c (n "ckb-db-schema") (v "0.111.0") (h "1v8zln4rbx5vw4mibchjwmf6f6iqps9yr4v2b8bcxbb4v7mxwbpd")))

(define-public crate-ckb-db-schema-0.112.0-rc1 (c (n "ckb-db-schema") (v "0.112.0-rc1") (h "0s7s74jkp5a0xwvfv42rzzarf3y56pmnkvyhafyxs068x0vjabrw")))

(define-public crate-ckb-db-schema-0.112.0-rc2 (c (n "ckb-db-schema") (v "0.112.0-rc2") (h "0brjhhyi5ixvwj3vgz1csvf6nq63vva99zaj24wqby2v9csshim4")))

(define-public crate-ckb-db-schema-0.112.0 (c (n "ckb-db-schema") (v "0.112.0") (h "1qry8mjwcrkj1vfm87qm28ck2x33x9wwsxm6r1mjx6xipr0qb2av")))

(define-public crate-ckb-db-schema-0.112.0-rc3 (c (n "ckb-db-schema") (v "0.112.0-rc3") (h "1a6lq8jip8rbvpmakyi40rvh6k9azg03p5k33qqkqazx7py10aj8")))

(define-public crate-ckb-db-schema-0.112.1 (c (n "ckb-db-schema") (v "0.112.1") (h "1y65c4j4d5cxzamkbck1d3m8y3ashrc96q705v11qrnw65xgr881")))

(define-public crate-ckb-db-schema-0.113.0-rc1 (c (n "ckb-db-schema") (v "0.113.0-rc1") (h "0q9r04hsnjg5bbd24k5v6x9kckr6zqisjsdprqzhh4d1hai6p542")))

(define-public crate-ckb-db-schema-0.113.0-rc2 (c (n "ckb-db-schema") (v "0.113.0-rc2") (h "1nkd4wv52hyyxlwhiqc1rlplq123rrq599g680zyf5ddd1grjblj")))

(define-public crate-ckb-db-schema-0.113.0-rc3 (c (n "ckb-db-schema") (v "0.113.0-rc3") (h "0n5ckkz3h7l4ynmwn30jn4bfs6da3dg6va90avpl0x8l66zc3rfc")))

(define-public crate-ckb-db-schema-0.113.0 (c (n "ckb-db-schema") (v "0.113.0") (h "09spa2bdm9mzw28z56vb7yszin9riv269clmwmb7lvd6x8n2961h")))

(define-public crate-ckb-db-schema-0.113.1-rc1 (c (n "ckb-db-schema") (v "0.113.1-rc1") (h "1zw1hd6z6yrsq9l2ybdd3r15kh5hm3zbvvcg9mjjf7hxvnvirqcf")))

(define-public crate-ckb-db-schema-0.114.0-rc1 (c (n "ckb-db-schema") (v "0.114.0-rc1") (h "0kx3szjb8lqr4bpnf2b11jh5ji0cfv57f4f4jpi05dhjlk6ckxxs")))

(define-public crate-ckb-db-schema-0.113.1-rc2 (c (n "ckb-db-schema") (v "0.113.1-rc2") (h "1c3lw3cd2c03yv7srlcsw4vwfz094snglfxkfrvlbl94kxpyysla")))

(define-public crate-ckb-db-schema-0.113.1 (c (n "ckb-db-schema") (v "0.113.1") (h "1b9xhfbicjln71zy42d3zgshkmgr8x7ybj2ihx9l5vjp9di5z07i")))

(define-public crate-ckb-db-schema-0.114.0-rc2 (c (n "ckb-db-schema") (v "0.114.0-rc2") (h "1pvk2ywmcfpfpajdbxqcpqcdqncwcm4z5xm1msjzwdd5a56gv1r8")))

(define-public crate-ckb-db-schema-0.114.0-rc3 (c (n "ckb-db-schema") (v "0.114.0-rc3") (h "05dlnr93qifk8zr50n16xnk83f0j9680slyglx0mvxm2894x2ych")))

(define-public crate-ckb-db-schema-0.114.0 (c (n "ckb-db-schema") (v "0.114.0") (h "1xqla49193z9cnni5gvd3arbjxgyyy2rgwjx5n3ma2w4s3fylb5m")))

(define-public crate-ckb-db-schema-0.115.0-pre (c (n "ckb-db-schema") (v "0.115.0-pre") (h "1wlryamxjd0lqq7ys2xcn1mv9jrmj98nlxm7zdd7qjc3vlxpsknm")))

(define-public crate-ckb-db-schema-0.115.0-rc1 (c (n "ckb-db-schema") (v "0.115.0-rc1") (h "05di3f3h45pvrz48n8a7xxkzz2dnf5iw9x7zvcirmmxn8hvlw31h")))

(define-public crate-ckb-db-schema-0.115.0-rc2 (c (n "ckb-db-schema") (v "0.115.0-rc2") (h "0hzmmlmw1pcrfa915r6xbjbhlr3c7mmd6q0ba8xsb9v59iyzrs50")))

(define-public crate-ckb-db-schema-0.115.0 (c (n "ckb-db-schema") (v "0.115.0") (h "1ks12lr88kaf5kyqg1c6svpwsjpxfc1172kbj8dxvrj1a9icqfxg")))

(define-public crate-ckb-db-schema-0.116.0-rc1 (c (n "ckb-db-schema") (v "0.116.0-rc1") (h "0yyk9rpl6i38i0hxg1k6jg0hnmj933qr4al7vcr2dn7q4cxyi2pb")))

(define-public crate-ckb-db-schema-0.116.0-rc2 (c (n "ckb-db-schema") (v "0.116.0-rc2") (h "1gmcx91w1sfnr63s4mpr116bdsky5r0czw9rjw1hq8g363yic2lv")))

(define-public crate-ckb-db-schema-0.116.0 (c (n "ckb-db-schema") (v "0.116.0") (h "10bp5bccvsgc5a7r4932g6w7jfhik49ic00m8vlc85mdxsc5yw8f") (y #t)))

(define-public crate-ckb-db-schema-0.116.1 (c (n "ckb-db-schema") (v "0.116.1") (h "0sal5f612ngmvxji6zmgczrsqifnx5vd0mjhqppvnv2msy6q9war")))

