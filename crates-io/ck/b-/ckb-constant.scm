(define-module (crates-io ck b- ckb-constant) #:use-module (crates-io))

(define-public crate-ckb-constant-0.42.0 (c (n "ckb-constant") (v "0.42.0") (h "1djqp8pll54g409pcb0hscx7fgc2zwq5w0vyp859x8qlmkbgyzya")))

(define-public crate-ckb-constant-0.43.0 (c (n "ckb-constant") (v "0.43.0") (h "06k3d6mblnzgkqfsydwh5iki8czabs682sk4j2kb40bvyda2dxq5")))

(define-public crate-ckb-constant-0.100.0-rc2 (c (n "ckb-constant") (v "0.100.0-rc2") (h "0g2yr36y9gvhpab668mjkcicbx3x1as27gxv8qd77lnfwylajzi3")))

(define-public crate-ckb-constant-0.43.2 (c (n "ckb-constant") (v "0.43.2") (h "11l9dvamx3ah2qqvascc41k0fm1zlg7h9n855xwy9apccbqmi793")))

(define-public crate-ckb-constant-0.100.0-rc4 (c (n "ckb-constant") (v "0.100.0-rc4") (h "0xb6z9lz4pf26g6y3x1p144d6q90an1djrl9qfak4j0qp29n16cp")))

(define-public crate-ckb-constant-0.100.0-rc5 (c (n "ckb-constant") (v "0.100.0-rc5") (h "11prfrccdndbxgyqifz00izwg3csh3l5ynpc6h29v762jfagbjn3")))

(define-public crate-ckb-constant-0.100.0 (c (n "ckb-constant") (v "0.100.0") (h "0jxrl8dazrxh2kkk7x68i4jfjj3qwg23mqc6ywqhx17p9adnxxw3")))

(define-public crate-ckb-constant-0.101.0 (c (n "ckb-constant") (v "0.101.0") (h "0ql51fnrqhkbfp0lwdgrgwcs532lmkw5swjrsxa071xchikq38l0")))

(define-public crate-ckb-constant-0.101.1 (c (n "ckb-constant") (v "0.101.1") (h "1day1r9fdjr21lmjmwkcyjmd569f44chzvp4brf72jb5h7c7scnn")))

(define-public crate-ckb-constant-0.101.2-testnet1 (c (n "ckb-constant") (v "0.101.2-testnet1") (h "0p4rzz08j4hgzjgg1i0b6wmp0h2gm23scndzn4kii3agxcvcp3xd")))

(define-public crate-ckb-constant-0.101.2 (c (n "ckb-constant") (v "0.101.2") (h "136ikj64zcd2614skzla0h56n1sjxf9v25yy480j41jbyhm45fxs")))

(define-public crate-ckb-constant-0.101.3 (c (n "ckb-constant") (v "0.101.3") (h "16ffcy5kcca96n876ra76hy5jcs76x1m11r9nis9agg30n3qwfbi")))

(define-public crate-ckb-constant-0.101.4 (c (n "ckb-constant") (v "0.101.4") (h "0n9gh9vdp3lr2rxr1p8clx3xkcbmbsd25hy0czyc07h8qmcqapps")))

(define-public crate-ckb-constant-0.101.5 (c (n "ckb-constant") (v "0.101.5") (h "0ybjc32vqzzm35kf4hqg7yi0qgw1rk83yn7q0cq4cpp4423qs9ql") (y #t)))

(define-public crate-ckb-constant-0.101.6 (c (n "ckb-constant") (v "0.101.6") (h "1yci5rhyb9gxxrw08lnbawqw42c0xrbz0n2pxv2z15y60wj7rsxf")))

(define-public crate-ckb-constant-0.101.7 (c (n "ckb-constant") (v "0.101.7") (h "021fma31fgkifbm7fgcgp3c0dhbws3d9qdx5cm19n3zgqmq41dj8")))

(define-public crate-ckb-constant-0.101.8 (c (n "ckb-constant") (v "0.101.8") (h "1yy0kp57n2pllmhzdk70xhwwncis15j64vw90pfl8vsp296s42pr")))

(define-public crate-ckb-constant-0.102.0 (c (n "ckb-constant") (v "0.102.0") (h "0z6sh76cdrah8pi7l83visb97mfvah2k73yvcb095hbfp54dhxw9") (y #t)))

(define-public crate-ckb-constant-0.103.0 (c (n "ckb-constant") (v "0.103.0") (h "12fvq7bybp2qlrikb947c6w15a5wx0bva3v5b5smi9v08mm3pa55")))

(define-public crate-ckb-constant-0.104.0 (c (n "ckb-constant") (v "0.104.0") (h "185gm7wgs98raa5zqkf2686330xifqi2a24y2ykm17js755bm94j")))

(define-public crate-ckb-constant-0.104.1 (c (n "ckb-constant") (v "0.104.1") (h "15679waslmsc34r0r736j6y9558vvsjnssars89mxxz055f19lah")))

(define-public crate-ckb-constant-0.105.0 (c (n "ckb-constant") (v "0.105.0") (h "1z8kk4vqic3cc3vls63g8whd5cbbf5v2wlr3dls1yxbb5d1dyd96") (y #t)))

(define-public crate-ckb-constant-0.105.1 (c (n "ckb-constant") (v "0.105.1") (h "14y435zn147k3zwfc8m40wnahn41v403ls1ay7jz2n3xk7yzjvca") (y #t)))

(define-public crate-ckb-constant-0.106.0 (c (n "ckb-constant") (v "0.106.0") (h "1x1iiyfc9bib777m0ha53ixpappf48cvv5vidwbjl6sjw1z33p2x")))

(define-public crate-ckb-constant-0.107.0 (c (n "ckb-constant") (v "0.107.0") (h "18zd59c3kpz1mffjyx3wbjp2w0mgzy6ybnxr19fydizbblx3pggl")))

(define-public crate-ckb-constant-0.108.0 (c (n "ckb-constant") (v "0.108.0") (h "1s97cpb9v4qmrizsa6ggqb3s4k42f6zlb0jymiincaavir06c99h")))

(define-public crate-ckb-constant-0.108.1 (c (n "ckb-constant") (v "0.108.1") (h "1mz7lga1mylaaif552dz94cac2lp2g47yly08lwnj3djg0c5xgi0")))

(define-public crate-ckb-constant-0.109.0 (c (n "ckb-constant") (v "0.109.0") (h "07f1y6n4j8c26q6dx1zai8m1cmdmbxxl77q7jvm3079vf92mvkzb")))

(define-public crate-ckb-constant-0.110.0 (c (n "ckb-constant") (v "0.110.0") (h "011qrl93dh2byzs386j0icg5ysw0hl4040kcp576kh22a4d1lcqv")))

(define-public crate-ckb-constant-0.110.0-rc1 (c (n "ckb-constant") (v "0.110.0-rc1") (h "04lcaa2qrwysk8zv6qibj8nbj66r7zxybnf2aqc75a521f96rflx")))

(define-public crate-ckb-constant-0.111.0-rc2 (c (n "ckb-constant") (v "0.111.0-rc2") (h "1rc2sipkkrwlxibqm5lpcbx0k1bi5jvvi6djq4bbv583r457bsrr")))

(define-public crate-ckb-constant-0.111.0-rc3 (c (n "ckb-constant") (v "0.111.0-rc3") (h "139jvzxyfhinpy8kb6flaa2ln97hrm00nhjdj5js4sqbq7pj2w3c")))

(define-public crate-ckb-constant-0.111.0-rc4 (c (n "ckb-constant") (v "0.111.0-rc4") (h "0z1ghfxm3m5kvnrldc9skdp15gjbn7mmx0pcdcjwvih861fbx4ki")))

(define-public crate-ckb-constant-0.111.0-rc5 (c (n "ckb-constant") (v "0.111.0-rc5") (h "096jr31rskf9d7nm31akvy4mbxydncs25brya60lldvv26yrxba5")))

(define-public crate-ckb-constant-0.111.0-rc6 (c (n "ckb-constant") (v "0.111.0-rc6") (h "0i7n17zap7kcjsaq1rk7qg72sy415gnhmqrbzcz7rcl4vd7sxyn0")))

(define-public crate-ckb-constant-0.111.0-rc7 (c (n "ckb-constant") (v "0.111.0-rc7") (h "0ig6zjpz1cij4xfy5pq1kqkwkahydwhqr4qi9mr20y7sacq7kcch")))

(define-public crate-ckb-constant-0.111.0-rc8 (c (n "ckb-constant") (v "0.111.0-rc8") (h "0z0qqxk0zsp1ripc19c16lpsny0shfrawpbbdhkppgxgdfm4lrw6")))

(define-public crate-ckb-constant-0.110.1-rc1 (c (n "ckb-constant") (v "0.110.1-rc1") (h "1w3xgg6jrivghnv443q0vvcaviac1r5r7a2gkxygizsr7mc6gxgr")))

(define-public crate-ckb-constant-0.110.1-rc2 (c (n "ckb-constant") (v "0.110.1-rc2") (h "0knk6ic4db73agsb3iqlqn6p6b6kbjldm1a5kbgrg99ks56l16b3")))

(define-public crate-ckb-constant-0.111.0-rc9 (c (n "ckb-constant") (v "0.111.0-rc9") (h "1gizn5q1lkb4pz0mpzys6vsps6aazwy7hgygssmb3b29lds33szy")))

(define-public crate-ckb-constant-0.111.0-rc10 (c (n "ckb-constant") (v "0.111.0-rc10") (h "1mh62nkrz81ap5g7mxs4s5apqg796lhrz92k7kdbdalqylk5gd1d")))

(define-public crate-ckb-constant-0.110.1 (c (n "ckb-constant") (v "0.110.1") (h "0j7x2j0qr1gy43j5mxnqh69nlmhcq51lp7hqfs46n7j2bw12c1xc")))

(define-public crate-ckb-constant-0.110.2-rc1 (c (n "ckb-constant") (v "0.110.2-rc1") (h "0vxclv5hc9r50lmg8i3pqv44lq96jvl21s2by8m0bc3m49zrvmjb")))

(define-public crate-ckb-constant-0.111.0-rc11 (c (n "ckb-constant") (v "0.111.0-rc11") (h "04cmslmn0r7qkaf7xqmb1g7pcq0h3zc0nmbwddymxgwlgg4im7jv")))

(define-public crate-ckb-constant-0.110.2-rc2 (c (n "ckb-constant") (v "0.110.2-rc2") (h "1vxgxhw7glgz0c5c0ixl5ccqpa8qdjmvkbmyxz55aqd60kmmb3nj")))

(define-public crate-ckb-constant-0.111.0-rc12 (c (n "ckb-constant") (v "0.111.0-rc12") (h "0pnzy51fm0x5v7ih5r26zcf1m4pdgx9n66m4i48hfxxiw2hqvn2y")))

(define-public crate-ckb-constant-0.110.2 (c (n "ckb-constant") (v "0.110.2") (h "0hyirmbwxjkx9jz3fv52mksy1krll25xw9chczg85gv0q77qslw4")))

(define-public crate-ckb-constant-0.111.0 (c (n "ckb-constant") (v "0.111.0") (h "062s9bdjyyd3w9bfhi8yi4adjkd6dsigzkkvldr0yxr48w6rhp4x")))

(define-public crate-ckb-constant-0.112.0-rc1 (c (n "ckb-constant") (v "0.112.0-rc1") (h "1a97jd708pjrnjxjw3l2nnfy73wsh1vh70fn3n7m4999w8ldpiyx")))

(define-public crate-ckb-constant-0.112.0-rc2 (c (n "ckb-constant") (v "0.112.0-rc2") (h "0bal45bsw45wxacz8ksq5fazhhp69hblwbfpa29jsx9qps0pq3fs")))

(define-public crate-ckb-constant-0.112.0 (c (n "ckb-constant") (v "0.112.0") (h "1071lkqcrnsd3vhxx4c2vlcfkadfvffn1wc1vl3z074spbhsjm1h")))

(define-public crate-ckb-constant-0.112.0-rc3 (c (n "ckb-constant") (v "0.112.0-rc3") (h "036p4kvrpajdjwff4yb9fjak1rk8gv3h33ivppqxx4f3zfbajcp1")))

(define-public crate-ckb-constant-0.112.1 (c (n "ckb-constant") (v "0.112.1") (h "06h35934frxnz5nzmz030vngj9dwhr68yp7mbc6g05k22pkn5mf7")))

(define-public crate-ckb-constant-0.113.0-rc1 (c (n "ckb-constant") (v "0.113.0-rc1") (h "1li77cnb3crc5mp79j2hmjxgw6h7mx201h8q2vxgpzl4shphckv7")))

(define-public crate-ckb-constant-0.113.0-rc2 (c (n "ckb-constant") (v "0.113.0-rc2") (h "1jgphnki7kjw00wziw2x2ydjsda8m6dic0kd2hhjbxlzx3w22yn4")))

(define-public crate-ckb-constant-0.113.0-rc3 (c (n "ckb-constant") (v "0.113.0-rc3") (h "06pay68iy1dn34nb7yjgyp409nwijf291aplxaskj58fxxg0lr68")))

(define-public crate-ckb-constant-0.113.0 (c (n "ckb-constant") (v "0.113.0") (h "19yv1xcipa3pmssf9i3qwr712y0hmslhmz4jj9b72sw7f3ir685l")))

(define-public crate-ckb-constant-0.113.1-rc1 (c (n "ckb-constant") (v "0.113.1-rc1") (h "0w7mxjscg0njwah6mnfhbjx5qf67flmlspg8l22d7d6hw9famjia")))

(define-public crate-ckb-constant-0.114.0-rc1 (c (n "ckb-constant") (v "0.114.0-rc1") (h "1d2q4jwr2h02qwr120h6j325ysz0vrpdip0wgcpaznfmb2bj0ndj")))

(define-public crate-ckb-constant-0.113.1-rc2 (c (n "ckb-constant") (v "0.113.1-rc2") (h "16zki62pq3lx99a752jnxdj6wz3c8k11k8s9i1vjc2c4f7kggha5")))

(define-public crate-ckb-constant-0.113.1 (c (n "ckb-constant") (v "0.113.1") (h "05f3dj29705aa6g86ignaqz33jbh0galzmh78x99x75iyycib957")))

(define-public crate-ckb-constant-0.114.0-rc2 (c (n "ckb-constant") (v "0.114.0-rc2") (h "0d2pdsjkm3yyyzm5l83k3bdndvrgriqav8ks06nlp3ad2siglwna")))

(define-public crate-ckb-constant-0.114.0-rc3 (c (n "ckb-constant") (v "0.114.0-rc3") (h "07gscbljami7y5g8ilzip8n6v2lm0qc28r8k26n5ww9ipgqlqb3q")))

(define-public crate-ckb-constant-0.114.0 (c (n "ckb-constant") (v "0.114.0") (h "0pn29jcq7wcxnbcsyymak9pq8swz0hbrjhxc5p6rd8cis2gabcyv")))

(define-public crate-ckb-constant-0.115.0-pre (c (n "ckb-constant") (v "0.115.0-pre") (h "1gszlg0l21xxgl2wj2p4r5b4liaicrm6vr39979iy8lzsxyfvzdf")))

(define-public crate-ckb-constant-0.115.0-rc1 (c (n "ckb-constant") (v "0.115.0-rc1") (h "09xpnb8p4d5s0c9bm1rc8x185h9qfi7xs7yzndqgspgqmxp8cxng")))

(define-public crate-ckb-constant-0.115.0-rc2 (c (n "ckb-constant") (v "0.115.0-rc2") (h "0lrsgx79fdhlm5ilqlg4418rid0zqj8vrj6dadx8bbna0f8v56mc")))

(define-public crate-ckb-constant-0.115.0 (c (n "ckb-constant") (v "0.115.0") (h "1ggiizsh0jj6l92krqd5hi18l3fixghp15pqnbsgyxrj007v728l")))

(define-public crate-ckb-constant-0.116.0-rc1 (c (n "ckb-constant") (v "0.116.0-rc1") (h "1vqd6m956f390rqv2966ndw1jaiyx9q4fki7hnfzwg5drv3216hx")))

(define-public crate-ckb-constant-0.116.0-rc2 (c (n "ckb-constant") (v "0.116.0-rc2") (h "0hpyzpc3jn8mld63jpjqbgcrndmr31w6kd4ns5nkmm25cn7m295m")))

(define-public crate-ckb-constant-0.116.0 (c (n "ckb-constant") (v "0.116.0") (h "039igxqgsd9zsn4miflr88br9k1z04bhh5zvcrn3yk2hl1ywjm8a") (y #t)))

(define-public crate-ckb-constant-0.116.1 (c (n "ckb-constant") (v "0.116.1") (h "04p34yrdcmbxcfqsc7jpmvrs3h5dsbrxzpqiab4610rvdaqr3bsv")))

