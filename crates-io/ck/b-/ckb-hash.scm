(define-module (crates-io ck b- ckb-hash) #:use-module (crates-io))

(define-public crate-ckb-hash-0.37.0-pre (c (n "ckb-hash") (v "0.37.0-pre") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 0)))) (h "1dy95jaxnlrm44yfp03r51sqhd8gv0w53ghhiybksv6cd8vrimln")))

(define-public crate-ckb-hash-0.37.0 (c (n "ckb-hash") (v "0.37.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 0)))) (h "1idswci9pb08vnhbr895qki1lzyplprgvmp1hfvl93lr71yj223m")))

(define-public crate-ckb-hash-0.38.0 (c (n "ckb-hash") (v "0.38.0") (d (list (d (n "blake2b-rs") (r "^0.1.5") (d #t) (k 0)))) (h "0ysia0ldri6qzdix8izcp9a2pzgy8mcw2sl1c8kq438sfm8l90qx")))

(define-public crate-ckb-hash-0.39.0 (c (n "ckb-hash") (v "0.39.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16sjiwyf50bfihcp767iskm54z9g9716as5qk9931sk27dnlzgmi")))

(define-public crate-ckb-hash-0.39.1 (c (n "ckb-hash") (v "0.39.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0p2mssn860sy20p8d8hmz5i1qpzvm0p1dxnrl6n0hgifxi6rfpda")))

(define-public crate-ckb-hash-0.40.0 (c (n "ckb-hash") (v "0.40.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "17hg4isbsig142df8vkqqrzbz5q5var1kk6ab6j2v5i4ni8zgm0i")))

(define-public crate-ckb-hash-0.42.0 (c (n "ckb-hash") (v "0.42.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02fn6kyfqg27v9d3nv72whddny8vsvq459zkhjlifx27lwxw71s0")))

(define-public crate-ckb-hash-0.43.0 (c (n "ckb-hash") (v "0.43.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "150kgv9mbzmcbj3l03rhglhxba0af55zgy2nj730zx859s8x3v1d")))

(define-public crate-ckb-hash-0.100.0-rc2 (c (n "ckb-hash") (v "0.100.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "04vrgnjr6n8kjfagl47ph9vd85pkyd5r4jgabpqynp41kipj62gl")))

(define-public crate-ckb-hash-0.43.2 (c (n "ckb-hash") (v "0.43.2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "05anf6hm9ai30prb3s2340plmkl3g79mbq079540xvl5p196dlpb")))

(define-public crate-ckb-hash-0.100.0-rc3 (c (n "ckb-hash") (v "0.100.0-rc3") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1vax1dkqjavkzzr5sy3i2f39h71cwjv0if1lvyhd1rj8wv8iacqg") (y #t)))

(define-public crate-ckb-hash-0.100.0-rc4 (c (n "ckb-hash") (v "0.100.0-rc4") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0y1vzkqzg2x0wfnsnrjk2qcdz3pi0v8hz8pvrf9bp1q2vqdyms75")))

(define-public crate-ckb-hash-0.100.0-rc5 (c (n "ckb-hash") (v "0.100.0-rc5") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "198xvbna3j5h1qdybqbjdkmf9yhsx6ikyhcswaf91hz86yirikm5")))

(define-public crate-ckb-hash-0.100.0 (c (n "ckb-hash") (v "0.100.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.1.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0r43x6h3xx92fj54nghzxc72g6z5j08b9ckwhna2sx7f4n6qjdyz")))

(define-public crate-ckb-hash-0.101.0 (c (n "ckb-hash") (v "0.101.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "155z57cgk0gfpjl0fwrqlj4h4d4kazxdqvchw3ajfcqlljq1fydd")))

(define-public crate-ckb-hash-0.101.1 (c (n "ckb-hash") (v "0.101.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0gpv0n685byxia52yj3xqmy5vf8cp80g9kslax40wdl8693p82qj")))

(define-public crate-ckb-hash-0.101.2-testnet1 (c (n "ckb-hash") (v "0.101.2-testnet1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0f760y5q1xpf26v0nymc7djxhrnb1139x0zc8h5kb30xjx176c70")))

(define-public crate-ckb-hash-0.101.2 (c (n "ckb-hash") (v "0.101.2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0xgrmf2wbb9nrzkpdq5p0dkkvrzcsj6j4shq29m8xqywhyb42mzp")))

(define-public crate-ckb-hash-0.101.3 (c (n "ckb-hash") (v "0.101.3") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "13m6kj5zchzy42xqbw6m9jnd5h3w4af91a32jn2z1gk7ifmj00nj")))

(define-public crate-ckb-hash-0.101.4 (c (n "ckb-hash") (v "0.101.4") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ghayf3rxsqgjnlhx9p8z5520zx3icnyzszsk2hy0cfpywmigdf2")))

(define-public crate-ckb-hash-0.101.5 (c (n "ckb-hash") (v "0.101.5") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1dxkrjgrnh7ygbrd9i3qs7w31wcz02m4bmzgf455yqrfxalypldm") (y #t)))

(define-public crate-ckb-hash-0.101.6 (c (n "ckb-hash") (v "0.101.6") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1g8vv4ip0hkk820zghm8srj0z4l39hz4apmnhr3byxblmvz058kq")))

(define-public crate-ckb-hash-0.101.7 (c (n "ckb-hash") (v "0.101.7") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ll8944yhc4rq304cfif1cdxvpkr3bmczkjyd3gqa1cjy9rv2y2a")))

(define-public crate-ckb-hash-0.101.8 (c (n "ckb-hash") (v "0.101.8") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0n7grshn5z2agq0mxmq5aw7g3jdy2x52vk6jk12fh6w4pqsc8rsx")))

(define-public crate-ckb-hash-0.102.0 (c (n "ckb-hash") (v "0.102.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "190mfrhvvzk5az0nqr38m0ais1jwiak8z84fqzr2phb9fm8gp84g") (y #t)))

(define-public crate-ckb-hash-0.103.0 (c (n "ckb-hash") (v "0.103.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "11g2l47f2q4lpz0zrp47i1xv3pka6cmxfxgh123kgb07jb0041rq")))

(define-public crate-ckb-hash-0.104.0 (c (n "ckb-hash") (v "0.104.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1gp6hq3zbqbabs45307m6i920hj75q50rmzadgq0di6sp4hfyw5c")))

(define-public crate-ckb-hash-0.104.1 (c (n "ckb-hash") (v "0.104.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0dzzwhxz2l9k2m0d7cn5q33nihllg0xfpxciggdh6azy3xn4zfm1")))

(define-public crate-ckb-hash-0.105.0 (c (n "ckb-hash") (v "0.105.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1fk46haw6prljhi154w0h81j0ak1ph742yf1bll5ns9ncmzm7xv6") (y #t)))

(define-public crate-ckb-hash-0.105.1 (c (n "ckb-hash") (v "0.105.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "15dck1f0m7aykd1jl9krdn94f10sx105ala1azsayfcqhvzlxdn6") (y #t)))

(define-public crate-ckb-hash-0.106.0 (c (n "ckb-hash") (v "0.106.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1n52shc6409521xp1flzs3qaab6qwv2221mmfv6z92aa1j2dd2h3")))

(define-public crate-ckb-hash-0.107.0 (c (n "ckb-hash") (v "0.107.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1a66my4ssx3zmx8ra6mw6sy628pl4szdmd19037kg7l8r0x7n9rl")))

(define-public crate-ckb-hash-0.108.0 (c (n "ckb-hash") (v "0.108.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "10w9dsihp1gbsdvvyajfl9zbwsqbn3m74hdgbbfzzr4sx21vdnak")))

(define-public crate-ckb-hash-0.108.1 (c (n "ckb-hash") (v "0.108.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1274hmakzq9r94c3yqm66dmprjlc31j8ji5b93gpiri08mqkymr8")))

(define-public crate-ckb-hash-0.109.0 (c (n "ckb-hash") (v "0.109.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "15hj46mbznkkcqnsh3k1qhdxw77qcqcc2hxj51kzxq8djwsz30kf")))

(define-public crate-ckb-hash-0.110.0 (c (n "ckb-hash") (v "0.110.0") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1aik8skgy61lng2hvlxz4055llhp96afmplybimmy0988hrs9w19")))

(define-public crate-ckb-hash-0.110.0-rc1 (c (n "ckb-hash") (v "0.110.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0af52xjnsy6v0miciqhq2bpgipq8q3ccxp7q78nm1ddfm7amrmbc")))

(define-public crate-ckb-hash-0.111.0-rc2 (c (n "ckb-hash") (v "0.111.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1x3jn3ci57h1w652ya6d2ds6is11l1v5wxrp0z7981zygfzd3rcl")))

(define-public crate-ckb-hash-0.111.0-rc3 (c (n "ckb-hash") (v "0.111.0-rc3") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0rqrprkg1n68wqc6phgnhp0a1m704slv354kdr9h1l6rmwzc1knv")))

(define-public crate-ckb-hash-0.111.0-rc4 (c (n "ckb-hash") (v "0.111.0-rc4") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1mmqqn699hkzkcgcixaxdy95ivnrymmj3h03yfb4qfczlgpny7w7")))

(define-public crate-ckb-hash-0.111.0-rc5 (c (n "ckb-hash") (v "0.111.0-rc5") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0x0yi0n850n1s22fs6ygs4rxjw1djx8ks3y9876nvq8lpsidgyiq")))

(define-public crate-ckb-hash-0.111.0-rc6 (c (n "ckb-hash") (v "0.111.0-rc6") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0711xmk74fzbybj91isfpba7inmjd1gpm0hd3alxn6narlk2bg8l")))

(define-public crate-ckb-hash-0.111.0-rc7 (c (n "ckb-hash") (v "0.111.0-rc7") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1c5iklvzyfsfwb7dfxjfbk7i4h2kkzkj7fm5vaflbpr9grlms6lw")))

(define-public crate-ckb-hash-0.111.0-rc8 (c (n "ckb-hash") (v "0.111.0-rc8") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02qairqyyiy6jk1hn7h8nczxn7rj40y0kvn3hwzs9qn8j1l5dibd")))

(define-public crate-ckb-hash-0.110.1-rc1 (c (n "ckb-hash") (v "0.110.1-rc1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0m1i03xnh3q516rq49vivdjmsjnng57cdngiiymgg7g2hpi6f4bq")))

(define-public crate-ckb-hash-0.110.1-rc2 (c (n "ckb-hash") (v "0.110.1-rc2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1br76iij9d5zfc5d5akyqbcd34zp9bsrn7irfrv4i4ijf3h65nsv")))

(define-public crate-ckb-hash-0.111.0-rc9 (c (n "ckb-hash") (v "0.111.0-rc9") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ka458c6kk004w299d40k8q098gsa7pnx3pyrzdnpb24kx5dy676")))

(define-public crate-ckb-hash-0.111.0-rc10 (c (n "ckb-hash") (v "0.111.0-rc10") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "02ck4spb3y9v98zfrs53075awnnwlg90xa69ji4g1yzmsxihvvcr")))

(define-public crate-ckb-hash-0.110.1 (c (n "ckb-hash") (v "0.110.1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0chpa9ys2aqllz5nv31i6lcrsw4i4vllac649w9s912scschrg1r")))

(define-public crate-ckb-hash-0.110.2-rc1 (c (n "ckb-hash") (v "0.110.2-rc1") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0w4m38zj5wawr0b8f7jsv5bz5z941zy43gxw1ln2cq2q7czm3kb8")))

(define-public crate-ckb-hash-0.111.0-rc11 (c (n "ckb-hash") (v "0.111.0-rc11") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0g3267wbb91s4b1gcsdg7rvz1fc0j48gz9ryf6337cvdrapy3mcv")))

(define-public crate-ckb-hash-0.110.2-rc2 (c (n "ckb-hash") (v "0.110.2-rc2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0y367v176gbx3jsw7qcca4psz1i1j462v92r44gmg4rz4iahga0s")))

(define-public crate-ckb-hash-0.111.0-rc12 (c (n "ckb-hash") (v "0.111.0-rc12") (d (list (d (n "blake2b") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0) (p "blake2b-rs")) (d (n "blake2b") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0) (p "blake2b-ref")) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)))) (h "05kpynq724rr0vj8wfsifww0hc14vfmjsbiabq3ly0943d0yml16") (f (quote (("default" "blake2b") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.110.2 (c (n "ckb-hash") (v "0.110.2") (d (list (d (n "blake2b-ref") (r "^0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0nph0y4892apzf3qnnz37hdr6h6lnq19cwgsh574kagkwjzc8cxd")))

(define-public crate-ckb-hash-0.111.0 (c (n "ckb-hash") (v "0.111.0") (d (list (d (n "blake2b") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0) (p "blake2b-rs")) (d (n "blake2b") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0) (p "blake2b-ref")) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)))) (h "081inlaz65k4vl8g57wnmz3574w7fn8v9y2wza4f8js5svifb24c") (f (quote (("default" "blake2b") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.112.0-rc1 (c (n "ckb-hash") (v "0.112.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16a5wkv6svic03hmzpja5yqv6c24zls5ng4kyibs3fljkxgciyps") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.112.0-rc2 (c (n "ckb-hash") (v "0.112.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1zd7njzcz3y8c476xwzkq0as8m4ashqjzjv7ccdna6ax7c2bl7nk") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.112.0 (c (n "ckb-hash") (v "0.112.0") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0f77plwgvzvh1xhxxmaz1bx8gjlz7wrl9kwlmdhiv8d3yck334b8") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.112.0-rc3 (c (n "ckb-hash") (v "0.112.0-rc3") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1d4xbzkclwirccs50wkap48fhbay7igdr429g7y5kd8kab3x7ag7") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.112.1 (c (n "ckb-hash") (v "0.112.1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "14lka635n2i33g7a3dk3clh79a79xm3smrj4cispqvvlr07ndbr5") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.0-rc1 (c (n "ckb-hash") (v "0.113.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0mxydzn230vm3c0aa06mrdcdjv6ym406zl0cg1zl72lfj2j46fil") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.0-rc2 (c (n "ckb-hash") (v "0.113.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16if6yld154fl29qicqhcz616k7cwnv4d9m686fdsanjnsnnd4sm") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.0-rc3 (c (n "ckb-hash") (v "0.113.0-rc3") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1c56g7f2fj0r3j80j5hqi5cdm81c48rsxl0bnkyh22vjg1za6hj3") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.0 (c (n "ckb-hash") (v "0.113.0") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "13zli0lam0s1zw43cg346zpifacaycy5l7ac780l7w7dkqih91dp") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.1-rc1 (c (n "ckb-hash") (v "0.113.1-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0ms6g4iahkikmpffc2ii1kcjgyag3vwjp6adhma3q2ij8h28wv72") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.114.0-rc1 (c (n "ckb-hash") (v "0.114.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "159sdaqn2yk8d5sj7yacb6g9jp1jcdbyxign4mvn4mpvl20skrjs") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.1-rc2 (c (n "ckb-hash") (v "0.113.1-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0imjn4cpc2z68vcziafa5mp6ygay2aly3wrfy6wcchmdbcfnk9yp") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.113.1 (c (n "ckb-hash") (v "0.113.1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ih9mnwq1f8jd8dkfjn58rg03ylsvm651b8704983lcap4iv8d73") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.114.0-rc2 (c (n "ckb-hash") (v "0.114.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0d2gpxy4nxj7n64b5zkjg3vg3q39d0jyknwrw88r9wjn5dqrmc4f") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.114.0-rc3 (c (n "ckb-hash") (v "0.114.0-rc3") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0074wkfr9q7xm54ayv6khm9mbfwbxiary16qjzwi1mzvnhkp38l5") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.114.0 (c (n "ckb-hash") (v "0.114.0") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1n13lvcg0di0nh4467vpan17mfw8wdblvk750cm4frh5hmw095wr") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.115.0-pre (c (n "ckb-hash") (v "0.115.0-pre") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0ja3967lamlkap4njaxld0qy5r5g3yl4mirijngmninq1caws54y") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.115.0-rc1 (c (n "ckb-hash") (v "0.115.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ib2xr5d5fqsjf9zmlmrgqhs025rckh09arwnyqqkzbymlf6dv7s") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.115.0-rc2 (c (n "ckb-hash") (v "0.115.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0fp7fsrprqqlx7sznp27b3xcbs17b6hbwp7ygg9dg5s1s8c1vsc6") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.115.0 (c (n "ckb-hash") (v "0.115.0") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ix7n1g4n0lg4x48abbzvr33lqimx8zfg7vs7sb5aqi398khpa1p") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.116.0-rc1 (c (n "ckb-hash") (v "0.116.0-rc1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "10l2lpf8sc8r06w1g0m04lzwb8js75yv4pgpfdfkv72l29vap5pa") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.116.0-rc2 (c (n "ckb-hash") (v "0.116.0-rc2") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "17s2kn2d3w4skp26glb6gf0r3gcjc33y4h6rzmqmw5f0b76sxv3n") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

(define-public crate-ckb-hash-0.116.0 (c (n "ckb-hash") (v "0.116.0") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "16hj53nm38cvqh2svbrir6hg6avwic8sp2gn093vpsyfx077s09a") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref")))) (y #t)))

(define-public crate-ckb-hash-0.116.1 (c (n "ckb-hash") (v "0.116.1") (d (list (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "blake2b-ref") (r "^0.3") (o #t) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "blake2b-rs") (r "^0.2") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "00yk52pdcxic0wgm4yvpm6y4qa51xvqpqspy5647wsppkk24nxfm") (f (quote (("default" "blake2b-ref" "blake2b-rs") ("ckb-contract" "blake2b-ref"))))))

