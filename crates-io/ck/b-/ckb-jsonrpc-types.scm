(define-module (crates-io ck b- ckb-jsonrpc-types) #:use-module (crates-io))

(define-public crate-ckb-jsonrpc-types-0.37.0-pre (c (n "ckb-jsonrpc-types") (v "0.37.0-pre") (d (list (d (n "ckb-types") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "regex") (r "^1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "086blm1gmfvxsznfsap6m3x3cr24pbjglhg89ldywv5s1qbmy55j")))

(define-public crate-ckb-jsonrpc-types-0.37.0 (c (n "ckb-jsonrpc-types") (v "0.37.0") (d (list (d (n "ckb-types") (r "=0.37.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xfd5y9bw0bnwg6zjzah0i3a63y812dasq1wg3gi4j6q7ddjvliv")))

(define-public crate-ckb-jsonrpc-types-0.38.0 (c (n "ckb-jsonrpc-types") (v "0.38.0") (d (list (d (n "ckb-types") (r "=0.38.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10w0jwz0y7xpb07nlv959hrz1gflb9699a7da5bwivfy6w8fmrym")))

(define-public crate-ckb-jsonrpc-types-0.39.0 (c (n "ckb-jsonrpc-types") (v "0.39.0") (d (list (d (n "ckb-types") (r "=0.39.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1frrqgk85qvy39fxxcbj69l3c6vwnpzych1sisgp77b937c9y1xi")))

(define-public crate-ckb-jsonrpc-types-0.39.1 (c (n "ckb-jsonrpc-types") (v "0.39.1") (d (list (d (n "ckb-types") (r "=0.39.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ifm5fz3g2gba83chgvgjnfhxwi8b1jj9shcfy6szwlf1ix7z1xl")))

(define-public crate-ckb-jsonrpc-types-0.40.0 (c (n "ckb-jsonrpc-types") (v "0.40.0") (d (list (d (n "ckb-types") (r "=0.40.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vyg0dsgnxhksj46i07a4sjg4ifn50510y0nk16h7yvbydq48f7a")))

(define-public crate-ckb-jsonrpc-types-0.42.0 (c (n "ckb-jsonrpc-types") (v "0.42.0") (d (list (d (n "ckb-types") (r "=0.42.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15l9jlir8aljh9yr29wzvbmr44aisy5fb6fhxlwp2swrrdga3rff")))

(define-public crate-ckb-jsonrpc-types-0.43.0 (c (n "ckb-jsonrpc-types") (v "0.43.0") (d (list (d (n "ckb-types") (r "=0.43.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qv7p4c3i94kbqvwx7n4vyla4pgxqhqf82h6my18f1amm58xfkhh")))

(define-public crate-ckb-jsonrpc-types-0.100.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.100.0-rc2") (d (list (d (n "ckb-types") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00z0vhq44n2x4hchgqm5cxx8n7d4diq1dczpracz5f4a51w1zcdr")))

(define-public crate-ckb-jsonrpc-types-0.43.2 (c (n "ckb-jsonrpc-types") (v "0.43.2") (d (list (d (n "ckb-types") (r "=0.43.2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rm3bzs4v7c2mv0xsvg7j53s5azs14fkcjp06vcbm6ykr8bvxcmz")))

(define-public crate-ckb-jsonrpc-types-0.100.0-rc4 (c (n "ckb-jsonrpc-types") (v "0.100.0-rc4") (d (list (d (n "ckb-types") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06lw3fpm3az4vn7sai89520fmv8ayzplcr2q0ygvyf550skijn43")))

(define-public crate-ckb-jsonrpc-types-0.100.0-rc5 (c (n "ckb-jsonrpc-types") (v "0.100.0-rc5") (d (list (d (n "ckb-types") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0llky1lfz27q868sfdhlciviz1i3ynh9w83aax87d3lvg8kjczy8")))

(define-public crate-ckb-jsonrpc-types-0.100.0 (c (n "ckb-jsonrpc-types") (v "0.100.0") (d (list (d (n "ckb-types") (r "=0.100.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07w74xl6jhfichqcbr53l74sa1x7mm0rpqmw9p600hwq701hy6dm")))

(define-public crate-ckb-jsonrpc-types-0.101.0 (c (n "ckb-jsonrpc-types") (v "0.101.0") (d (list (d (n "ckb-types") (r "=0.101.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dwy251fvvz8cjalbqga0ilyb6fmbn0qmvwyly9d04kx66df9nyd")))

(define-public crate-ckb-jsonrpc-types-0.101.1 (c (n "ckb-jsonrpc-types") (v "0.101.1") (d (list (d (n "ckb-types") (r "=0.101.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pn4lf4xc3xgvyqx6rkkdjkbym9sngzqf9rpsbygr86ib1aa5zbq")))

(define-public crate-ckb-jsonrpc-types-0.101.2-testnet1 (c (n "ckb-jsonrpc-types") (v "0.101.2-testnet1") (d (list (d (n "ckb-types") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzq46j7l5ivy071bf84nvgkpzn77k03q5yccy8ylys73hasdswy")))

(define-public crate-ckb-jsonrpc-types-0.101.2 (c (n "ckb-jsonrpc-types") (v "0.101.2") (d (list (d (n "ckb-types") (r "=0.101.2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n97d8bgkaacxrf064lj2xqsvy0kifapcig5bjsznvpyw9yq7z15")))

(define-public crate-ckb-jsonrpc-types-0.101.3 (c (n "ckb-jsonrpc-types") (v "0.101.3") (d (list (d (n "ckb-types") (r "=0.101.3") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ya4zflk1nmk5qr8zvs9y2zrsffgrmx302qknc27fchaqbd7hvpi")))

(define-public crate-ckb-jsonrpc-types-0.101.4 (c (n "ckb-jsonrpc-types") (v "0.101.4") (d (list (d (n "ckb-types") (r "=0.101.4") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08jhxj86bzlfy9ddjz8qj7cw49gynfhl5hvrrid2fhggz6c8r2r9")))

(define-public crate-ckb-jsonrpc-types-0.101.5 (c (n "ckb-jsonrpc-types") (v "0.101.5") (d (list (d (n "ckb-types") (r "=0.101.5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13z99n2cqk6yiqjv4jl584icb01bha0mck346xvmc9hzcfrcl9xh") (y #t)))

(define-public crate-ckb-jsonrpc-types-0.101.6 (c (n "ckb-jsonrpc-types") (v "0.101.6") (d (list (d (n "ckb-types") (r "=0.101.6") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mdg55vvvs5pmmlyb2749kkxhwsh8i37dybqk559mfkvnkijilgi")))

(define-public crate-ckb-jsonrpc-types-0.101.7 (c (n "ckb-jsonrpc-types") (v "0.101.7") (d (list (d (n "ckb-types") (r "=0.101.7") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08kr13k1qxcbqy08314mx9w9x5i7sz8a0nq0jxvk7c3rds928dgp")))

(define-public crate-ckb-jsonrpc-types-0.101.8 (c (n "ckb-jsonrpc-types") (v "0.101.8") (d (list (d (n "ckb-types") (r "=0.101.8") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dyxz2ks36rhwnl1xrqzqxp4lafmz0gsr84ppf1ycb3sli0ghbpq")))

(define-public crate-ckb-jsonrpc-types-0.102.0 (c (n "ckb-jsonrpc-types") (v "0.102.0") (d (list (d (n "ckb-types") (r "=0.102.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1y7s4zvsi3vnbk6q51sf2si18xbigwg2xx2fxig6j5nq6v98nwy8") (y #t)))

(define-public crate-ckb-jsonrpc-types-0.103.0 (c (n "ckb-jsonrpc-types") (v "0.103.0") (d (list (d (n "ckb-types") (r "=0.103.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qcyf7zgwxqmn0jjfhrr7sv1rs5gww7fivxi25dwwjsbbv160mrh")))

(define-public crate-ckb-jsonrpc-types-0.104.0 (c (n "ckb-jsonrpc-types") (v "0.104.0") (d (list (d (n "ckb-types") (r "=0.104.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ikfxz2y0iq5k8lb27hlx0hnc3j1aljkca4s5cxajpkx7g0iq3aj")))

(define-public crate-ckb-jsonrpc-types-0.104.1 (c (n "ckb-jsonrpc-types") (v "0.104.1") (d (list (d (n "ckb-types") (r "=0.104.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gq5bkqlgcw59n8z7fnq8rvd0ya0w8b87rphq3gkd8rs0ah80q91")))

(define-public crate-ckb-jsonrpc-types-0.105.0 (c (n "ckb-jsonrpc-types") (v "0.105.0") (d (list (d (n "ckb-types") (r "=0.105.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02v4rn8nzalin846idhf8xwzsdam524j131hrk5zaixlsrpzpj2r") (y #t)))

(define-public crate-ckb-jsonrpc-types-0.105.1 (c (n "ckb-jsonrpc-types") (v "0.105.1") (d (list (d (n "ckb-types") (r "=0.105.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zv8a5jbp8p7i19iik7y8543mp3bgnrz3pnnjr2355czl3g8s9l2") (y #t)))

(define-public crate-ckb-jsonrpc-types-0.106.0 (c (n "ckb-jsonrpc-types") (v "0.106.0") (d (list (d (n "ckb-types") (r "=0.106.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aagx1cg14n496axi00wgr6jlg6771y0j0pj976qgr15nzh278gc")))

(define-public crate-ckb-jsonrpc-types-0.107.0 (c (n "ckb-jsonrpc-types") (v "0.107.0") (d (list (d (n "ckb-types") (r "=0.107.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x4wz2bp538yk9whb61lykzap5yccf6pvzvj48lqy1pdfd6326f6")))

(define-public crate-ckb-jsonrpc-types-0.108.0 (c (n "ckb-jsonrpc-types") (v "0.108.0") (d (list (d (n "ckb-types") (r "=0.108.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "047q25blkqjnhxc5x3dkig6s4x5c7nfr4g0byhlyfr7rx9bpc25c")))

(define-public crate-ckb-jsonrpc-types-0.108.1 (c (n "ckb-jsonrpc-types") (v "0.108.1") (d (list (d (n "ckb-types") (r "=0.108.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l23ar2l2skllxqmcgzanb045sdmvas2livvsy1ps7qbnpjkd6h6")))

(define-public crate-ckb-jsonrpc-types-0.109.0 (c (n "ckb-jsonrpc-types") (v "0.109.0") (d (list (d (n "ckb-types") (r "=0.109.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0178ka0y6c42h827cgigb7ahvmjljd9wzcjhq33r2mski23aph2m")))

(define-public crate-ckb-jsonrpc-types-0.110.0 (c (n "ckb-jsonrpc-types") (v "0.110.0") (d (list (d (n "ckb-types") (r "=0.110.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h18d48gz7ag05l25lkcknh65as2vw96ssi0gv8j545hfrgpb2gz")))

(define-public crate-ckb-jsonrpc-types-0.110.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.110.0-rc1") (d (list (d (n "ckb-types") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ib2g2r4n4bpq51lvpc3v5dz0486cvhldv3n2fm8lga1xkzz17yr")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc2") (d (list (d (n "ckb-types") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ca8dxa6zfhxqm1ghs7i4h8w6c0zid0wjq1v8cbdz98ilk0ibya")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc3 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc3") (d (list (d (n "ckb-types") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15rflrvyld4yz43wndadvw18fa0kb9hcrasmr1ifdxy6rdr26mfc")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc4 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc4") (d (list (d (n "ckb-types") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01s3v3ayxcjik8fq9vz1vx1s1aw3s5h0aqrnp1lci0hbz98dxz9x")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc5 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc5") (d (list (d (n "ckb-types") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "144x1pbqclza6ipn0dwq6vkgjgz8qifgnyyz64vga7xb1rrsjvfx")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc6 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc6") (d (list (d (n "ckb-types") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "168af50nyddnkcf6jia39fdy1hzpkgv17ikrax4sd8dc0gkni2zb")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc7 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc7") (d (list (d (n "ckb-types") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pk5wwjwvb1p3p9k3n8l5jhd4q6x95vn3xdr8c8hp8vqm6p4xgpg")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc8 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc8") (d (list (d (n "ckb-types") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1izjyh44gsf8yc7l9n1yaqjaw6wb38ywh9m9lb2hiwf8dqrhb6vy")))

(define-public crate-ckb-jsonrpc-types-0.110.1-rc1 (c (n "ckb-jsonrpc-types") (v "0.110.1-rc1") (d (list (d (n "ckb-types") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a7i2sc5qaxcy4j1lrw9529df296b2b0rkwpk04xg2jnd8kpmdz2")))

(define-public crate-ckb-jsonrpc-types-0.110.1-rc2 (c (n "ckb-jsonrpc-types") (v "0.110.1-rc2") (d (list (d (n "ckb-types") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c1xv347yilgjy2aw1nb9c9bsf1ym2zfxgf6mirj34ch1kz5p7q6")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc9 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc9") (d (list (d (n "ckb-types") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bppfsbhp8hls5jcixxahbkkxsrc5p8hn0bmdk5c2lnlhddaiyzw")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc10 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc10") (d (list (d (n "ckb-types") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s6y2xibvf9ahkv3yx9v5i33fvbz2mmky0nqd01zy5pcf9g1y2nk")))

(define-public crate-ckb-jsonrpc-types-0.110.1 (c (n "ckb-jsonrpc-types") (v "0.110.1") (d (list (d (n "ckb-types") (r "=0.110.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hpvbv70k61k5xqnvidfrsafm0c4xscp373asyj3x9zl88g8ah5i")))

(define-public crate-ckb-jsonrpc-types-0.110.2-rc1 (c (n "ckb-jsonrpc-types") (v "0.110.2-rc1") (d (list (d (n "ckb-types") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r8sw1pwl2p66kvj2j7hbcycvshk0lpvcvv279dgglpl0s6qrcf0")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc11 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc11") (d (list (d (n "ckb-types") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q0qy79shawi3ygrqzrnpz6hc363yfy94j4w6xlhj8wks86v3bg3")))

(define-public crate-ckb-jsonrpc-types-0.110.2-rc2 (c (n "ckb-jsonrpc-types") (v "0.110.2-rc2") (d (list (d (n "ckb-types") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "118y15majw75ksp1qd34lms7bmcs378g44mggcgyhz0dgw1qjqmr")))

(define-public crate-ckb-jsonrpc-types-0.111.0-rc12 (c (n "ckb-jsonrpc-types") (v "0.111.0-rc12") (d (list (d (n "ckb-types") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h4yfqjx90868rkd0yf1xj4laz0xa0pw5bdkc3cyglhznzmnaxbk")))

(define-public crate-ckb-jsonrpc-types-0.110.2 (c (n "ckb-jsonrpc-types") (v "0.110.2") (d (list (d (n "ckb-types") (r "=0.110.2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "111wsf178zwhn1jvgjrl633vwl502r12zn6bfkfsxq6krvz9w9lc")))

(define-public crate-ckb-jsonrpc-types-0.111.0 (c (n "ckb-jsonrpc-types") (v "0.111.0") (d (list (d (n "ckb-types") (r "=0.111.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s142lqmwi8vzb15zvgyplkb8av3dj6x5jxf24f8f1ys70asg2fp")))

(define-public crate-ckb-jsonrpc-types-0.112.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.112.0-rc1") (d (list (d (n "ckb-types") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gfs3x1rvq1662fa2mc61x44d64p0cdp3k6pv0y1xizr66apkg52")))

(define-public crate-ckb-jsonrpc-types-0.112.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.112.0-rc2") (d (list (d (n "ckb-types") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06x7pd0dhq301kfhl6vmhc8yiqqaj03aay33wwjhaykd5ix7afj7")))

(define-public crate-ckb-jsonrpc-types-0.112.0 (c (n "ckb-jsonrpc-types") (v "0.112.0") (d (list (d (n "ckb-types") (r "=0.112.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k01ra17jg4vxnga2lhiah12p6fxsha7iwpanh4w364blnq69imc")))

(define-public crate-ckb-jsonrpc-types-0.112.0-rc3 (c (n "ckb-jsonrpc-types") (v "0.112.0-rc3") (d (list (d (n "ckb-types") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fm17j8x6rlw2yzyy7w77i8la8pn00yqirwzbga9467z3r2dnsgp")))

(define-public crate-ckb-jsonrpc-types-0.112.1 (c (n "ckb-jsonrpc-types") (v "0.112.1") (d (list (d (n "ckb-types") (r "=0.112.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvw8i567imip0xh1nhazw5dandy8nzrw72glcjl3zfpmidqny86")))

(define-public crate-ckb-jsonrpc-types-0.113.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.113.0-rc1") (d (list (d (n "ckb-types") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pq8ib7299wpy811lai6qd7nwlmvd3mrl7mqzahc2c4c8n9x351i")))

(define-public crate-ckb-jsonrpc-types-0.113.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.113.0-rc2") (d (list (d (n "ckb-types") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07i9v6vxyzhafrp2gsw267c6m6g3d38hwn88l3sbgfbvb26v0lia")))

(define-public crate-ckb-jsonrpc-types-0.113.0-rc3 (c (n "ckb-jsonrpc-types") (v "0.113.0-rc3") (d (list (d (n "ckb-types") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ddv8kbbpj5gjiskmak61gh2vfacp564v713sb3l4akhdbf6df1n")))

(define-public crate-ckb-jsonrpc-types-0.113.0 (c (n "ckb-jsonrpc-types") (v "0.113.0") (d (list (d (n "ckb-types") (r "=0.113.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "179x1fmlp0j2xnb822faxdqrndwrd7qgvylkv7rq1fcgl40pxkwp")))

(define-public crate-ckb-jsonrpc-types-0.113.1-rc1 (c (n "ckb-jsonrpc-types") (v "0.113.1-rc1") (d (list (d (n "ckb-types") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b4i82hj3ga6gg3702yz9hq4dljvwfmrs5x17zjl8n3qf3qf1qj1")))

(define-public crate-ckb-jsonrpc-types-0.114.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.114.0-rc1") (d (list (d (n "ckb-types") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1873wvxqzclpapanryi1xilw1jprd67pa3idfj729cc1ilcgzlc7")))

(define-public crate-ckb-jsonrpc-types-0.113.1-rc2 (c (n "ckb-jsonrpc-types") (v "0.113.1-rc2") (d (list (d (n "ckb-types") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04cnng6m12v4byj0s6rr590ihm49i3z3klq50phrn16a1i5z1f4n")))

(define-public crate-ckb-jsonrpc-types-0.113.1 (c (n "ckb-jsonrpc-types") (v "0.113.1") (d (list (d (n "ckb-types") (r "=0.113.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fhihm5zqaiwws61ir67paf43y6r9ya4rpg8h191j7kybn15m1gf")))

(define-public crate-ckb-jsonrpc-types-0.114.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.114.0-rc2") (d (list (d (n "ckb-types") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19g4dn6wrdazaqsz9xnxb75h5fhix0z7rgwyzrw2iphg0dc0dqfb")))

(define-public crate-ckb-jsonrpc-types-0.114.0-rc3 (c (n "ckb-jsonrpc-types") (v "0.114.0-rc3") (d (list (d (n "ckb-types") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xlbr79aa3sb65spzn3dnndwia01f3qdb8xwf5jpwxqrw8bwq0h2")))

(define-public crate-ckb-jsonrpc-types-0.114.0 (c (n "ckb-jsonrpc-types") (v "0.114.0") (d (list (d (n "ckb-types") (r "=0.114.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yzrda2wzci25q60c4zgcjgylsz65xi53a3c87sw9igbjh1xdjfn")))

(define-public crate-ckb-jsonrpc-types-0.115.0-pre (c (n "ckb-jsonrpc-types") (v "0.115.0-pre") (d (list (d (n "ckb-types") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fjvwhkkdxnsg1pp9dbhf6l6ckqb8m5r1gbkqijpb8anribr95sp")))

(define-public crate-ckb-jsonrpc-types-0.115.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.115.0-rc1") (d (list (d (n "ckb-types") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fa5hxf3f1yfhmmifzcrh4kwbvyhczd7ci3np1p5qyh5aa437xya")))

(define-public crate-ckb-jsonrpc-types-0.115.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.115.0-rc2") (d (list (d (n "ckb-types") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16c287v87n2pwam1rfmnj4vdhb4cjf7ymj0pbj04cy9zrgy2wj9p")))

(define-public crate-ckb-jsonrpc-types-0.115.0 (c (n "ckb-jsonrpc-types") (v "0.115.0") (d (list (d (n "ckb-types") (r "=0.115.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19qaxch96r34az0pkk8skrjma5wsml11x51k8gswn68ll1gj7vyd")))

(define-public crate-ckb-jsonrpc-types-0.116.0-rc1 (c (n "ckb-jsonrpc-types") (v "0.116.0-rc1") (d (list (d (n "ckb-types") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r1gdw0cjps3gbaz9qd5f9xif97ipxy11qpjlzcg02khp47rn3m1")))

(define-public crate-ckb-jsonrpc-types-0.116.0-rc2 (c (n "ckb-jsonrpc-types") (v "0.116.0-rc2") (d (list (d (n "ckb-types") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04n27xlamg46axqbji46wv7d9rlq7lwczmklp9vinrcy6bygg749")))

(define-public crate-ckb-jsonrpc-types-0.116.0 (c (n "ckb-jsonrpc-types") (v "0.116.0") (d (list (d (n "ckb-types") (r "=0.116.0") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14hj3x83rhcdr8wikxh0rig22jpanhp0c0dfgdppbw27l9b2g3bv") (y #t)))

(define-public crate-ckb-jsonrpc-types-0.116.1 (c (n "ckb-jsonrpc-types") (v "0.116.1") (d (list (d (n "ckb-types") (r "=0.116.1") (d #t) (k 0)) (d (n "faster-hex") (r "^0.6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.19") (d #t) (k 0) (p "ckb_schemars")) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rrbf8pr4s7s7a64r6hh16b1ffsgnfckck5s0p7h2dya8cq14zpg")))

