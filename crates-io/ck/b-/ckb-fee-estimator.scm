(define-module (crates-io ck b- ckb-fee-estimator) #:use-module (crates-io))

(define-public crate-ckb-fee-estimator-0.37.0-pre (c (n "ckb-fee-estimator") (v "0.37.0-pre") (d (list (d (n "ckb-logger") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v7v1km3120z1zdx0y5jgmqgkfbvrfij35d33a34ai8blndl66xn")))

(define-public crate-ckb-fee-estimator-0.37.0 (c (n "ckb-fee-estimator") (v "0.37.0") (d (list (d (n "ckb-logger") (r "=0.37.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.37.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zqc3qk8fy9r04ram6shsgvggbxg25rn5j50ljwkxai7wchm5b0d")))

(define-public crate-ckb-fee-estimator-0.38.0 (c (n "ckb-fee-estimator") (v "0.38.0") (d (list (d (n "ckb-logger") (r "=0.38.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.38.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fx51gzhnjn78p2pn4lwn6j73s6vwa870sym4i94p5px6jb0fm93")))

(define-public crate-ckb-fee-estimator-0.39.0 (c (n "ckb-fee-estimator") (v "0.39.0") (d (list (d (n "ckb-logger") (r "=0.39.0") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02ddjrzgsi2m7l1xjzxi7xd85c02s2ll5k6071vyjh1vpc0hd85p")))

(define-public crate-ckb-fee-estimator-0.39.1 (c (n "ckb-fee-estimator") (v "0.39.1") (d (list (d (n "ckb-logger") (r "=0.39.1") (d #t) (k 0)) (d (n "ckb-types") (r "=0.39.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hcxr35r1d9541a6mzsfqmfyksg8z2fhkpb05valr98lp3n25vmr")))

