(define-module (crates-io ck b- ckb-vm-definitions) #:use-module (crates-io))

(define-public crate-ckb-vm-definitions-0.13.0 (c (n "ckb-vm-definitions") (v "0.13.0") (h "1skdl56p4ddklmawy1kd0w9k9jznkbghah9nhvdvch38qmw2ardv")))

(define-public crate-ckb-vm-definitions-0.15.0 (c (n "ckb-vm-definitions") (v "0.15.0") (h "1250vfw54a12vpai9a6ccz5isbbv279ff07sfajdqgbqwilpcagx")))

(define-public crate-ckb-vm-definitions-0.16.0 (c (n "ckb-vm-definitions") (v "0.16.0") (h "1av0x9ca87nbpi3vyaf2y1ids2gqrd11w2yrjp1wrdxbs6q15vyd")))

(define-public crate-ckb-vm-definitions-0.17.0 (c (n "ckb-vm-definitions") (v "0.17.0") (h "06vnb9dl7aq5iywbdbnzjliwvi9li36m78vkbr6yax213qhw5h2m")))

(define-public crate-ckb-vm-definitions-0.18.0 (c (n "ckb-vm-definitions") (v "0.18.0") (h "19z655n9lga131cv6ap6slrc2rhz05rdgjdc1hzlnr7m9k7b06i0")))

(define-public crate-ckb-vm-definitions-0.18.1 (c (n "ckb-vm-definitions") (v "0.18.1") (h "0v6y88dhdjqzacdvq7mq0d2y1brmq4z4mgm3fgyia4zz7i4vy0dx")))

(define-public crate-ckb-vm-definitions-0.18.2 (c (n "ckb-vm-definitions") (v "0.18.2") (h "0ksak99hybd71b1q9hca677z4h2czhmryqc5qyip92gxdrlnajjv")))

(define-public crate-ckb-vm-definitions-0.19.0 (c (n "ckb-vm-definitions") (v "0.19.0") (h "1qchnb8crcv8ppbvcrr20wyq4q701xp9b87x6sr1ac4alpaixz0c")))

(define-public crate-ckb-vm-definitions-0.19.1-alpha.0+perf-test (c (n "ckb-vm-definitions") (v "0.19.1-alpha.0+perf-test") (h "0y0nb0vs5kl109kb7bhdh5p0fi1b57jgvfpwb6khxf29bnm2nr20")))

(define-public crate-ckb-vm-definitions-0.19.1 (c (n "ckb-vm-definitions") (v "0.19.1") (h "1lhadh99ji1varsqvbpv0dibr0mf67sds0hyi1c29v8rg7wp9rrw")))

(define-public crate-ckb-vm-definitions-0.19.2 (c (n "ckb-vm-definitions") (v "0.19.2") (h "1a9zn59nak255m5s1mm42fijxf0fzwdlfy5sv6dagf1fmsj92bh6")))

(define-public crate-ckb-vm-definitions-0.19.3 (c (n "ckb-vm-definitions") (v "0.19.3") (h "01fvbzw6mlq0y6xhxach36wr2dp79cqcvc3ahxj6qwq0gn3d6xpa")))

(define-public crate-ckb-vm-definitions-0.19.4 (c (n "ckb-vm-definitions") (v "0.19.4") (h "0glv7zsbilcrivwsfsjzqbm5q6adb7wpg94kss1q5zcg4il4ppgm")))

(define-public crate-ckb-vm-definitions-0.20.0-alpha (c (n "ckb-vm-definitions") (v "0.20.0-alpha") (h "1yrydvya58i49mi21fxxmnd7pbscnm5z40p60y4hvh08ix0br0qr") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-alpha2 (c (n "ckb-vm-definitions") (v "0.20.0-alpha2") (h "0z9yn4600j28gi3ppnh1i8xyb02xqxgbrfy8sj317mqpx2pl2kfz") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc1 (c (n "ckb-vm-definitions") (v "0.20.0-rc1") (h "0gv37rhj8pwvwvyfdi391ds79gx8j5dzmh9zp3p1667pnsv8rs7v") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc2 (c (n "ckb-vm-definitions") (v "0.20.0-rc2") (h "1hh3dy81klfncrsick0ya7rxv6hbj6qrppsc7rvgvlz8p7nx7mw5") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc3 (c (n "ckb-vm-definitions") (v "0.20.0-rc3") (h "1g40mcf562s9fhc4zb12jgfkkc9mhadhmrb7ski3yhyyl5p38q5v") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc4 (c (n "ckb-vm-definitions") (v "0.20.0-rc4") (h "1jf91fri7k6d74rjmz2r8wk71afwh2532winiwllxb3bpfr4apxn") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc5 (c (n "ckb-vm-definitions") (v "0.20.0-rc5") (h "0kx3rp56bhqg1ijikn1jhapvlg7wvn9v5xrxibfb5bisfp94rd6y") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0-rc6 (c (n "ckb-vm-definitions") (v "0.20.0-rc6") (h "03lh1309ln9ajxk359qlvlj9cd7z3vbkzypm0fcgq1pb8080v2hm") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.0 (c (n "ckb-vm-definitions") (v "0.20.0") (h "0krfxr96cpb2mbpjivm3aj6d0j5yw6h5ldxg9f020jmvflvj75hi") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.20.1 (c (n "ckb-vm-definitions") (v "0.20.1") (h "1p8x2x518c79ih94ag452j6yp0mz1qnq0fbpa7hr2vsjhkjrpbs5") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.21.0 (c (n "ckb-vm-definitions") (v "0.21.0") (h "10h517cyn23ffqih6n8dl0nn7lfgzghrzfjlifxmiq8q4v90p5cy") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.21.1 (c (n "ckb-vm-definitions") (v "0.21.1") (h "1qklzz7in80z2mapgmamahrx6jdzlk28wlsbqvaabdayydb5pf2r") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.21.2 (c (n "ckb-vm-definitions") (v "0.21.2") (h "045xzk1x7xypranrwwyam6h6rzapr2jniq7j3n7gxr308z1ba52b") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.21.3 (c (n "ckb-vm-definitions") (v "0.21.3") (h "076xib05z90s9g1id2iyh2x3l6nx5wc8fkinj4vq6s4b95cwas3r") (f (quote (("enable-chaos-mode-by-default"))))))

(define-public crate-ckb-vm-definitions-0.21.4 (c (n "ckb-vm-definitions") (v "0.21.4") (h "15k2ab32xsw5b15mrm8lqgdaid1kckidxym4lhm6raj7qcf9qp8n") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.21.5 (c (n "ckb-vm-definitions") (v "0.21.5") (h "1l26c2sg0gn607a9dpxz5vx0i8mhm6kypgklhkkv72s87vjq09a2") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.21.6 (c (n "ckb-vm-definitions") (v "0.21.6") (h "0gck8bicynkgxgdkq4ylf5mbwdn2znzbkllpamykmhskyhwdg7ll") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.21.7 (c (n "ckb-vm-definitions") (v "0.21.7") (h "0ndv8yy8nlyjfipp99chcwzg2yps37vi3198vnd9krawmf586ykg") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.22.0 (c (n "ckb-vm-definitions") (v "0.22.0") (h "1k4gylyqb48wbw03dmi52yjbdbm1mgb7vv2pjbg6kha8jvsrnwql") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.22.1 (c (n "ckb-vm-definitions") (v "0.22.1") (h "0n2dhqj0z1b1xdyb5pbglzjb49mclz6gcyxkmgsjpaz70hkbcial") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.23.0 (c (n "ckb-vm-definitions") (v "0.23.0") (h "0vjy5l1dbvmxswp8vi9qi3q7kbj38bjlyc699787k5nazpzcn8fr") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.0-alpha (c (n "ckb-vm-definitions") (v "0.24.0-alpha") (h "14xxb7avbnd0gc05dswdqr8fmrywzm0lqic0knp0qz7lb5nqiwxv") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.22.2 (c (n "ckb-vm-definitions") (v "0.22.2") (h "09a2jwjf5fy96anw66nxqbm2x989b80sp39rz87bfm3c5fp01y2a") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.23.1 (c (n "ckb-vm-definitions") (v "0.23.1") (h "0zysd03dh9dnqym6xwv618znkqg0sy07vp80dlfl49l8yj4zzif7") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.23.2 (c (n "ckb-vm-definitions") (v "0.23.2") (h "1gxwzjyvyf25r09yzp7gna4d2f00gx9rcpphmhzj1a40f7d3qh59") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.0-beta (c (n "ckb-vm-definitions") (v "0.24.0-beta") (h "1b4d1fpvmwx2sx2347xs43jg188jcp4mpnzc239xkzswx9bxpxqr") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.0 (c (n "ckb-vm-definitions") (v "0.24.0") (h "0axz9mxd1xg6br8g3ck4f076c1y4l2nv1rzs09f3sj34armpm31j") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.1 (c (n "ckb-vm-definitions") (v "0.24.1") (h "0z1y2ldwh7s7krk0ch51nmbw908q0zbnhcz66cz1b5ijd09l7v6i") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.2 (c (n "ckb-vm-definitions") (v "0.24.2") (h "15p3sxam9cyh9mk1g5gk25xja07w78kjf4cjlqvjmghfansz6lcx") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.3 (c (n "ckb-vm-definitions") (v "0.24.3") (h "0cpn3hdbv8g8sp6j23n5fcic66lsdwvvfm2vvszxvq9d6afrr1l3") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.4 (c (n "ckb-vm-definitions") (v "0.24.4") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1nsiclaqyn4fgygf0hkxavh8j6gpj89y0m7yb05vj3f2ipmbslq2") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.5 (c (n "ckb-vm-definitions") (v "0.24.5") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "15gqd01n30n6k1hm1x1z2symlsxcbz2zdw8cs4n7hvkhhshnd4gm") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.6 (c (n "ckb-vm-definitions") (v "0.24.6") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0gz8xq09b1aww181hn1k1qlr8ml53xn0ywh6q69kvdbrkpzx7kn4") (f (quote (("enable-chaos-mode-by-default")))) (r "1.61.0")))

(define-public crate-ckb-vm-definitions-0.24.7 (c (n "ckb-vm-definitions") (v "0.24.7") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "07sivh44z55zz4662hsw5ixqbp5grr2zpffjn4abrlshna2hi55x") (f (quote (("enable-chaos-mode-by-default")))) (r "1.71.1")))

(define-public crate-ckb-vm-definitions-0.24.8 (c (n "ckb-vm-definitions") (v "0.24.8") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0bvwj2yjv84nx827524bas21kcmrzfs12nyfgj03i687zmagmxi7") (f (quote (("enable-chaos-mode-by-default")))) (r "1.71.1")))

(define-public crate-ckb-vm-definitions-0.24.9 (c (n "ckb-vm-definitions") (v "0.24.9") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0bx533hd4ribhw2dqg8mh5w2s918i3c9lfyj10i9nh0lxv4gkzd2") (f (quote (("enable-chaos-mode-by-default")))) (r "1.71.1")))

(define-public crate-ckb-vm-definitions-0.24.10 (c (n "ckb-vm-definitions") (v "0.24.10") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0k5m24ghn3rjy3d84blqg16mqpypmkxk0wyqc9k02pr03ikfpfn0") (f (quote (("enable-chaos-mode-by-default")))) (r "1.75.0")))

(define-public crate-ckb-vm-definitions-0.24.11 (c (n "ckb-vm-definitions") (v "0.24.11") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "03fczgzffbmbis2bj8h18fiwnj06pipfqkb6i6dp6f11myd047pg") (f (quote (("enable-chaos-mode-by-default")))) (r "1.75.0")))

