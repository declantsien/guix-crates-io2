(define-module (crates-io ck b- ckb-suite-rpc) #:use-module (crates-io))

(define-public crate-ckb-suite-rpc-0.37.0 (c (n "ckb-suite-rpc") (v "0.37.0") (d (list (d (n "ckb-jsonrpc-types") (r "^0.37.0") (d #t) (k 0)) (d (n "ckb-types") (r "^0.37.0") (d #t) (k 0)) (d (n "ckb-util") (r "^0.37.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.27") (d #t) (k 0)) (d (n "jsonrpc-client-core") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-client-http") (r "^0.5.0") (d #t) (k 0)))) (h "11p9ij15n375gmfm9yzf4qg228is9nnr6l6xsiidrfs9r6yvv3wc")))

(define-public crate-ckb-suite-rpc-0.38.0 (c (n "ckb-suite-rpc") (v "0.38.0") (d (list (d (n "ckb-jsonrpc-types") (r ">=0.38.0, <0.39.0") (d #t) (k 0)) (d (n "ckb-types") (r ">=0.38.0, <0.39.0") (d #t) (k 0)) (d (n "ckb-util") (r ">=0.38.0, <0.39.0") (d #t) (k 0)) (d (n "hyper") (r ">=0.11.27, <0.12.0") (d #t) (k 0)) (d (n "jsonrpc-client-core") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "jsonrpc-client-http") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1pd3g05d0rjgry85zhrsv8b6yyr069fifkknvxsgl28qwla0zp07")))

(define-public crate-ckb-suite-rpc-0.39.2 (c (n "ckb-suite-rpc") (v "0.39.2") (d (list (d (n "ckb-jsonrpc-types") (r "^0.39.1") (d #t) (k 0)) (d (n "ckb-types") (r "^0.39.1") (d #t) (k 0)) (d (n "ckb-util") (r "^0.39.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.27") (d #t) (k 0)) (d (n "jsonrpc-client-core") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-client-http") (r "^0.5.0") (d #t) (k 0)) (d (n "lru") (r "^0.6.1") (d #t) (k 0)))) (h "14488jm9wzlh0l5mh23zhfghk7lzdwqk54sjv3q8bkksw2j63jl3")))

