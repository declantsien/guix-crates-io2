(define-module (crates-io ck b- ckb-sentry-core) #:use-module (crates-io))

(define-public crate-ckb-sentry-core-0.21.0 (c (n "ckb-sentry-core") (v "0.21.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log_") (r "^0.4.8") (f (quote ("std"))) (o #t) (d #t) (k 0) (p "log")) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "sentry-types") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-types")) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)))) (h "0p3lj5ibabc4zfg5n12f6gsfbw4xy0c8j77h8yqhm6jsw35chiwz") (f (quote (("test" "client") ("default") ("debug-logs" "log_") ("client" "rand"))))))

