(define-module (crates-io ck b- ckb-vm-pprof) #:use-module (crates-io))

(define-public crate-ckb-vm-pprof-0.111.0 (c (n "ckb-vm-pprof") (v "0.111.0") (d (list (d (n "addr2line") (r "^0.13.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.6") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "object") (r "^0.20") (d #t) (k 0)))) (h "0giay45wcdihd6yq9cwl1n0gxk6gycb18nbxiah14g8x3mwzgbhs")))

(define-public crate-ckb-vm-pprof-0.112.1 (c (n "ckb-vm-pprof") (v "0.112.1") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.6") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)))) (h "07kvng6lv5hpl0fh03mqvrkfj3li3qb14pdr3cn96zrz5aqd8bqw")))

(define-public crate-ckb-vm-pprof-0.113.0 (c (n "ckb-vm-pprof") (v "0.113.0") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.6") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)))) (h "18lsvpw8vg6prcdnpyxi0pmkfwjvydjirkm0f2rjwvkhryahrx21")))

(define-public crate-ckb-vm-pprof-0.114.0 (c (n "ckb-vm-pprof") (v "0.114.0") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.8") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)))) (h "1nf3rs3kca2695835i1a53i57m49hhqxysb0jmks2cbrc3wpvhlc")))

(define-public crate-ckb-vm-pprof-0.115.0-rc2 (c (n "ckb-vm-pprof") (v "0.115.0-rc2") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.9") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)))) (h "17nf1bgjj8xpb04xc7r47riydy1cq7x3x773i5i7m8ds43y7b8db")))

(define-public crate-ckb-vm-pprof-0.116.1 (c (n "ckb-vm-pprof") (v "0.116.1") (d (list (d (n "addr2line") (r "^0.17.0") (d #t) (k 0)) (d (n "ckb-vm") (r "=0.24.9") (f (quote ("pprof"))) (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "goblin") (r "^0.4") (d #t) (k 0)))) (h "0c3qxv1nbffc9cwxxpsdd4wy297y06f3hka0jjpz8a4aal47xrdl")))

