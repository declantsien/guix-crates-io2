(define-module (crates-io ck b- ckb-occupied-capacity-macros) #:use-module (crates-io))

(define-public crate-ckb-occupied-capacity-macros-0.37.0-pre (c (n "ckb-occupied-capacity-macros") (v "0.37.0-pre") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.37.0-pre") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qkh24wqi00l2sa2fb4knl2r0f3d56dblz5ddckxl2iq37zwz9n0")))

(define-public crate-ckb-occupied-capacity-macros-0.37.0 (c (n "ckb-occupied-capacity-macros") (v "0.37.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.37.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1kam0pxfiajxpcxzs6q9ynf28ybwh768q1w26alyiyis5n40d0cz")))

(define-public crate-ckb-occupied-capacity-macros-0.38.0 (c (n "ckb-occupied-capacity-macros") (v "0.38.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.38.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y3s1wbiy774813vnw0a6l77b4ckfdhk0327qbgds4rmpx3x694x")))

(define-public crate-ckb-occupied-capacity-macros-0.39.0 (c (n "ckb-occupied-capacity-macros") (v "0.39.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.39.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fkhpzi5vwg4mrwdaxhzrsp5fmk1b52315w884wk92wm2j9aai7k")))

(define-public crate-ckb-occupied-capacity-macros-0.39.1 (c (n "ckb-occupied-capacity-macros") (v "0.39.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.39.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l564v6kx495wlwm8kl22pr3wjrpaf86hq65f086p05y7x5sdm4m")))

(define-public crate-ckb-occupied-capacity-macros-0.40.0 (c (n "ckb-occupied-capacity-macros") (v "0.40.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.40.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18cw89rqmiy80xby3isc8059iszq5q14ih9x8p1277nghgjg84yn")))

(define-public crate-ckb-occupied-capacity-macros-0.42.0 (c (n "ckb-occupied-capacity-macros") (v "0.42.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.42.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1841nv7vlxhlqq818lar361lihx8n9g79dwjzsjlch2wh08db8nx")))

(define-public crate-ckb-occupied-capacity-macros-0.43.0 (c (n "ckb-occupied-capacity-macros") (v "0.43.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.43.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16dky7cpfyv8jybv9xf8l1v0y0i3mzw1zmzyi5aprdvzlyd1012l")))

(define-public crate-ckb-occupied-capacity-macros-0.100.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.100.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d8w02idw87k5rdz3l345pghm0v1fgibyx6j1z79ghi1kz1lddms")))

(define-public crate-ckb-occupied-capacity-macros-0.43.2 (c (n "ckb-occupied-capacity-macros") (v "0.43.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.43.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f127r7b6by178r8vw7fzn8dw37yb6hrx9kyb4jkwklsrnkfsl8k")))

(define-public crate-ckb-occupied-capacity-macros-0.100.0-rc3 (c (n "ckb-occupied-capacity-macros") (v "0.100.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pm45f97g1hli02hzf8qvwwcdjpvm5frq48lxap1ly4zhpls4ykd") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.100.0-rc4 (c (n "ckb-occupied-capacity-macros") (v "0.100.0-rc4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v54sc4jzwrx5k17rlz3fms98wq58b1sddfhyd6xa0r2v1jm8vmh")))

(define-public crate-ckb-occupied-capacity-macros-0.100.0-rc5 (c (n "ckb-occupied-capacity-macros") (v "0.100.0-rc5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0-rc5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gv7rjcaaz83dczn1h486wzrcxanqwy2h2zgszv0n3r1wwfblgqr")))

(define-public crate-ckb-occupied-capacity-macros-0.100.0 (c (n "ckb-occupied-capacity-macros") (v "0.100.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.100.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nx0mgs7k27jhhx33d3gm9kpihdhnb6j6y2azwlmd2vj9m96cc5d")))

(define-public crate-ckb-occupied-capacity-macros-0.101.0 (c (n "ckb-occupied-capacity-macros") (v "0.101.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kq1mvqh1ihibq6r5r6dbvp4rj10riyiwxfnvw21wncp6qxafrna")))

(define-public crate-ckb-occupied-capacity-macros-0.101.1 (c (n "ckb-occupied-capacity-macros") (v "0.101.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0a1m4zs0fvlr0ykk7q8lsx8ccnms3lbhqlj8fii69h9kixz4cm04")))

(define-public crate-ckb-occupied-capacity-macros-0.101.2-testnet1 (c (n "ckb-occupied-capacity-macros") (v "0.101.2-testnet1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.2-testnet1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dc84ny7npkhlfg85yybqckmm61fj0skmfdk19dldgn41vqmlwvc")))

(define-public crate-ckb-occupied-capacity-macros-0.101.2 (c (n "ckb-occupied-capacity-macros") (v "0.101.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g4p6n6j6lgci9z9pn69npiqwf92hymdkapmn47i425q65yhkf8h")))

(define-public crate-ckb-occupied-capacity-macros-0.101.3 (c (n "ckb-occupied-capacity-macros") (v "0.101.3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05i1h7gx5zfcaafp83w4lckss6x6ck2bmf6n2ap6llshhgg7rr02")))

(define-public crate-ckb-occupied-capacity-macros-0.101.4 (c (n "ckb-occupied-capacity-macros") (v "0.101.4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sgkng7wsm86gld9j30mjnnym8kij9r2vz8yimjq06izdjjhr0hn")))

(define-public crate-ckb-occupied-capacity-macros-0.101.5 (c (n "ckb-occupied-capacity-macros") (v "0.101.5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nby17z65rmis2whl7hb1i9214l2x5xdfwvs9s2qsvspy388a9l2") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.101.6 (c (n "ckb-occupied-capacity-macros") (v "0.101.6") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "192vvpm7jd3hjc2gpvclvabf2g612cw104plrbagx2jnsqlb72n2")))

(define-public crate-ckb-occupied-capacity-macros-0.101.7 (c (n "ckb-occupied-capacity-macros") (v "0.101.7") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xafq51ll3fa13qdgn27nnih8jmlvb62pn3r24ch0x64r86ha754")))

(define-public crate-ckb-occupied-capacity-macros-0.101.8 (c (n "ckb-occupied-capacity-macros") (v "0.101.8") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.101.8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07v7z9gyjlgxf021wil3dgg0dqb08bv3ydv0rw9lh5b7yxaxq53n")))

(define-public crate-ckb-occupied-capacity-macros-0.102.0 (c (n "ckb-occupied-capacity-macros") (v "0.102.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.102.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m4r7qy575lbaha5fvwl8l5325cpbd5skr2mwfk6xdwig0qbdzj8") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.103.0 (c (n "ckb-occupied-capacity-macros") (v "0.103.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.103.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0nbv7ai62413ka35wlxihk1yrbc7j3ywd3457x7ji0f3n3app91c")))

(define-public crate-ckb-occupied-capacity-macros-0.104.0 (c (n "ckb-occupied-capacity-macros") (v "0.104.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.104.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dan89aq6nw3iqlc6yas1wsyiyfdhcv0m3yq0vixabw1ygwv3kyz")))

(define-public crate-ckb-occupied-capacity-macros-0.104.1 (c (n "ckb-occupied-capacity-macros") (v "0.104.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.104.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lrkbsip7wblaxiic4nqnand716q08pm5gidsbhy95zrsjgyab8j")))

(define-public crate-ckb-occupied-capacity-macros-0.105.0 (c (n "ckb-occupied-capacity-macros") (v "0.105.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.105.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "177hd34wwd3rwd37sjdywfchy5b2nhl0hfl150gmwlfb483gdb39") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.105.1 (c (n "ckb-occupied-capacity-macros") (v "0.105.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.105.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v8g3nflvss8pqw0n4nbwbjpnjgvk17h53m365xqs6vrbihvavb5") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.106.0 (c (n "ckb-occupied-capacity-macros") (v "0.106.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.106.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ak2wzwy84x03xmscqr4y0m6v6pbpx6am4365hpsvi9pxjqmr0hp")))

(define-public crate-ckb-occupied-capacity-macros-0.107.0 (c (n "ckb-occupied-capacity-macros") (v "0.107.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.107.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pvpgbi1lblyaz2li7lwh01n55kapq6agmncjm81fvsbb643c9ii")))

(define-public crate-ckb-occupied-capacity-macros-0.108.0 (c (n "ckb-occupied-capacity-macros") (v "0.108.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.108.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k0x4shlarmi028jzd39d2890jnxa5l9134x08j9gpswm2xj2qyf")))

(define-public crate-ckb-occupied-capacity-macros-0.108.1 (c (n "ckb-occupied-capacity-macros") (v "0.108.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.108.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12q3v9kkzbz8nzk9zj2qz21s10l2ry10dir7s5b3qns52cn0cmmy")))

(define-public crate-ckb-occupied-capacity-macros-0.109.0 (c (n "ckb-occupied-capacity-macros") (v "0.109.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.109.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qqqg4nv6iks2bd30q0718drpy6lrzgc1vzw0ma0pn0z7w488aah")))

(define-public crate-ckb-occupied-capacity-macros-0.110.0 (c (n "ckb-occupied-capacity-macros") (v "0.110.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1q5brly291n4305wvkf2s88xg2y2m9b15vrr2mm93xz215yckpfk")))

(define-public crate-ckb-occupied-capacity-macros-0.110.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.110.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h3mbnh7ng4paprykfpbcn8ph6caknrx6ds7bc7lvps917r1pn97")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wkqc8j2id4k6bsxhdx431h52bp642b1x92c73f3jdpkjxni412v")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc3 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03myvnqykmhrx9lj8r27svskwpi06jxlm593yk15rwd5w6n7f0pc")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc4 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc4") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11pga1n2bl0blc91k22zvq3gcf786xmbvaigwk6vqajvyj5fhi6w")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc5 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc5") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rlnqzfjnyrr7xp0rn9gnapl644b0p6pznsijvmqdxfbib374dvb")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc6 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc6") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11jj3jjp2yqq5llnhbz3nr3j7v46i2rs9x7lk6z1rjaks1qwss96")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc7 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc7") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04afc89wczgwdj19m7mdyk5af90zirhq9m1nj5zxbk24km6cksni")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc8 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc8") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc8") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yjqlny6bs4qcw944syfglp5f1xigrbi6b8bnzlqminx5vrlpj1p")))

(define-public crate-ckb-occupied-capacity-macros-0.110.1-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.110.1-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1llf489nxgqz6qcpr460y6awvcjsh6rd9abmad6ikxhb4yv2b7bd")))

(define-public crate-ckb-occupied-capacity-macros-0.110.1-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.110.1-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xnd9xqvgmycgbddwan05vix866rcd4dqxxi1sgnjkqxji3lhw25")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc9 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc9") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cjpi0xyxmvmlxxvqmxfllk5wqn0srk8kiml49hvqr6d073pjqpb")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc10 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc10") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gi10p9izwf4a2nfbndgqxyamc3picwgd8627j2ybrl1bbn2xa44")))

(define-public crate-ckb-occupied-capacity-macros-0.110.1 (c (n "ckb-occupied-capacity-macros") (v "0.110.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02mqps6vhyslkxgxm7scqxrm9azn31fl0jsa5d3ksv79v7v7ivr6")))

(define-public crate-ckb-occupied-capacity-macros-0.110.2-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.110.2-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bwrv59hhhb0nqfhvk3xr1ja0h0czl80b1v0363984acx4dzjr2f")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc11 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc11") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l66ryi0fh5kwwilby55cs0h3a33wgi7gygyg72jxxm8443lmv55")))

(define-public crate-ckb-occupied-capacity-macros-0.110.2-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.110.2-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zs0sg3pnwgrrljbq6gi8dr63pfiw7k4g3784pqqncj9vx0xkqrs")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0-rc12 (c (n "ckb-occupied-capacity-macros") (v "0.111.0-rc12") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0-rc12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "115xmxnfhwg7y3qvcp0zhzxx58mvy6la76fxmynhzjbv0csc6c2n")))

(define-public crate-ckb-occupied-capacity-macros-0.110.2 (c (n "ckb-occupied-capacity-macros") (v "0.110.2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.110.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "011p89k0b0bh35411f0if6z8jkvbk00ydm89cr8d6ggi22pmfaki")))

(define-public crate-ckb-occupied-capacity-macros-0.111.0 (c (n "ckb-occupied-capacity-macros") (v "0.111.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.111.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d8dsma6ak2993iy3b0d9vdkrc40wj93cndi57pw89hh207rww5i")))

(define-public crate-ckb-occupied-capacity-macros-0.112.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.112.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pi3xd42jgnpal7gqyxmzgff915bc0s470qjsadvv9xdzx0sxrvf")))

(define-public crate-ckb-occupied-capacity-macros-0.112.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.112.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13smpssqrw0qjcv0009xviz08cb7li1p6ldsl1j3y0vmxhc2714k")))

(define-public crate-ckb-occupied-capacity-macros-0.112.0 (c (n "ckb-occupied-capacity-macros") (v "0.112.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z1jsr21v0ks4d9vsgb11hf7cjyl61dq92fkbj7s1p857xrz8d67")))

(define-public crate-ckb-occupied-capacity-macros-0.112.0-rc3 (c (n "ckb-occupied-capacity-macros") (v "0.112.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.0-rc3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wmzdg0kd9chrbgy3anag0imr10gq31lmp4160f7jk0wyhcwwy5a")))

(define-public crate-ckb-occupied-capacity-macros-0.112.1 (c (n "ckb-occupied-capacity-macros") (v "0.112.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.112.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cvcjzz763xc835vs3rklqcx0j7574mscb711i27cyrpbb9j26i0")))

(define-public crate-ckb-occupied-capacity-macros-0.113.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.113.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dm8rs0jxhjkggbbj7p577mr3scwb5flf6srk53hlaydpbf59yr4")))

(define-public crate-ckb-occupied-capacity-macros-0.113.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.113.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c7m8xdxlq7z5y6xjm35d7j53qbzijcd8vns03zh197zqa8lgr4f")))

(define-public crate-ckb-occupied-capacity-macros-0.113.0-rc3 (c (n "ckb-occupied-capacity-macros") (v "0.113.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0-rc3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y025qf6p2izcdzsd3fixhry40q7j7avqy010wi6n6rzf81j7jzr")))

(define-public crate-ckb-occupied-capacity-macros-0.113.0 (c (n "ckb-occupied-capacity-macros") (v "0.113.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d0ddm2s6wg5y1wnaz4s8h2644hjf0z78qfqxavd3j2g66iyydqs")))

(define-public crate-ckb-occupied-capacity-macros-0.113.1-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.113.1-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rxf3zmhpkxg0x6ilc4l7h2pq3nbg1f55hij5hj08wd9ykhfmqnj")))

(define-public crate-ckb-occupied-capacity-macros-0.114.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.114.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02qlr13rg3c3zd80qd5avcfx812s6ks1s4ycaq1ykx59jr2l2app")))

(define-public crate-ckb-occupied-capacity-macros-0.113.1-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.113.1-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wvaaba68wv55dw2graqq502pw12b2ks884p5wfzq9b78anlp50y")))

(define-public crate-ckb-occupied-capacity-macros-0.113.1 (c (n "ckb-occupied-capacity-macros") (v "0.113.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.113.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b97v5vx7sis35vs18049fzqshfvglpbzg5mzbxbh2syyzp15rwp")))

(define-public crate-ckb-occupied-capacity-macros-0.114.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.114.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17xq7mnc2zwyyxgghx46gvxizqk9dw0iay4yp8pqgcx0r436xid8")))

(define-public crate-ckb-occupied-capacity-macros-0.114.0-rc3 (c (n "ckb-occupied-capacity-macros") (v "0.114.0-rc3") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0-rc3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w56s341091323gha017az0bf4igihxc176flr1ynaw2swwr5sya")))

(define-public crate-ckb-occupied-capacity-macros-0.114.0 (c (n "ckb-occupied-capacity-macros") (v "0.114.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.114.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "104cp8m6s4c8lsiih897rpfzqcpdavlwqvhpaxp5apznqknl565v")))

(define-public crate-ckb-occupied-capacity-macros-0.115.0-pre (c (n "ckb-occupied-capacity-macros") (v "0.115.0-pre") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-pre") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bg50zh5hy5lvr4j2g0yywy2a2y9g3kpf8y4n66hrxgl7yfbs9gs")))

(define-public crate-ckb-occupied-capacity-macros-0.115.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.115.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pygc7qah18m9n65ph6b0wvbsg8h19yyh8i2q437dzvcb8a84c91")))

(define-public crate-ckb-occupied-capacity-macros-0.115.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.115.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qfvjwprlgs6d7n591p2msdypx2w1fkw1qkyzprpzz7a1prbkjd4")))

(define-public crate-ckb-occupied-capacity-macros-0.115.0 (c (n "ckb-occupied-capacity-macros") (v "0.115.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.115.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fydnilsip31ap2lq22139s0qk798j99c6q40z38yhwrbsq4gmxw")))

(define-public crate-ckb-occupied-capacity-macros-0.116.0-rc1 (c (n "ckb-occupied-capacity-macros") (v "0.116.0-rc1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0-rc1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02mmnv9dd7wjippn9szd30g8f2w6a55msmglh3403z31nz7lv88v")))

(define-public crate-ckb-occupied-capacity-macros-0.116.0-rc2 (c (n "ckb-occupied-capacity-macros") (v "0.116.0-rc2") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0-rc2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06pjgpnfw7mlw10yg1ffr2a4w3g11mazp3wvpb84y6g1arq6z5wr")))

(define-public crate-ckb-occupied-capacity-macros-0.116.0 (c (n "ckb-occupied-capacity-macros") (v "0.116.0") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xjclg4j6vq19k98ikvkhd23fz882j1m04893k55gp83rzxycnip") (y #t)))

(define-public crate-ckb-occupied-capacity-macros-0.116.1 (c (n "ckb-occupied-capacity-macros") (v "0.116.1") (d (list (d (n "ckb-occupied-capacity-core") (r "=0.116.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13v0xnlhdp3caxqjfswpfg67l38z533wja23ppalq6cd3wzswsls")))

