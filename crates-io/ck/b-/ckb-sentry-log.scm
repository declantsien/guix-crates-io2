(define-module (crates-io ck b- ckb-sentry-log) #:use-module (crates-io))

(define-public crate-ckb-sentry-log-0.21.0 (c (n "ckb-sentry-log") (v "0.21.0") (d (list (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "0yil9001x86p5858vvkmyipp9f8k6wnasvv31qr9zmki672an3ck")))

