(define-module (crates-io ck b- ckb-sentry-panic) #:use-module (crates-io))

(define-public crate-ckb-sentry-panic-0.21.0 (c (n "ckb-sentry-panic") (v "0.21.0") (d (list (d (n "sentry-backtrace") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-backtrace")) (d (n "sentry-core") (r "^0.21.0") (d #t) (k 0) (p "ckb-sentry-core")))) (h "1wmw2cpad7yib5i10s2ac8l9irc261v4wysxvpdqs6r0qc4sq9pp")))

