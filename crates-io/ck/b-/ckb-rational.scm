(define-module (crates-io ck b- ckb-rational) #:use-module (crates-io))

(define-public crate-ckb-rational-0.37.0-pre (c (n "ckb-rational") (v "0.37.0-pre") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0jkpw2inkpf836k0df0hw46pnsr29qyj426n62prslii2rgqiabr")))

(define-public crate-ckb-rational-0.37.0 (c (n "ckb-rational") (v "0.37.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)))) (h "1vxaz3446ykjk2avhnd7g9s2fh7k8bad1pixbnjnhpnwcby6r11z")))

(define-public crate-ckb-rational-0.38.0 (c (n "ckb-rational") (v "0.38.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)))) (h "1mhkw8igdhqy7yddkwdardwmy9384j3yr16x1anrjlms5nkxnah3")))

(define-public crate-ckb-rational-0.39.0 (c (n "ckb-rational") (v "0.39.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v7ickxffi2p214k3qn3bpxcwqmbmxwrafn70wgrf2w2kafcvx9a")))

(define-public crate-ckb-rational-0.39.1 (c (n "ckb-rational") (v "0.39.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0px042mwlaxnd27i74fjlf4xf6v6grzw10ki0ky5m05wi3vsa8wr")))

(define-public crate-ckb-rational-0.40.0 (c (n "ckb-rational") (v "0.40.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c251mf231nsdy3k69blgxma7clzcksf8bab6cbb0vki2wq4arsq")))

(define-public crate-ckb-rational-0.42.0 (c (n "ckb-rational") (v "0.42.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vh589ff7ch18kc5lxz3isvzm8h8vxrf3sqzqcv601yx5x8a6hlm")))

(define-public crate-ckb-rational-0.43.0 (c (n "ckb-rational") (v "0.43.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cam238129gpjs2xjafxlfb1j6jsyqhs8zkafqb7mnbkgpf9zv04")))

(define-public crate-ckb-rational-0.100.0-rc2 (c (n "ckb-rational") (v "0.100.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "174rqghnyk5qdgwnwmizbpjcvaxsvdp7izbx96b9qhmj0hpqwr40")))

(define-public crate-ckb-rational-0.43.2 (c (n "ckb-rational") (v "0.43.2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1brpp0pkx4wxmvay0wz1hzm7lp8zw3x7frz0irlar7mcpbm91i77")))

(define-public crate-ckb-rational-0.100.0-rc3 (c (n "ckb-rational") (v "0.100.0-rc3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k582dkicj3b8h0znh0bq3c83yrrrc910ryx7cqi3gd7i183bhmc") (y #t)))

(define-public crate-ckb-rational-0.100.0-rc4 (c (n "ckb-rational") (v "0.100.0-rc4") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pll0m1qnix67arbayidy5jwnvddd9vkbjss0ih7k9rsa3xj10ij")))

(define-public crate-ckb-rational-0.100.0-rc5 (c (n "ckb-rational") (v "0.100.0-rc5") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l7n1zfcn1525nichqwapawld2zdm9cgc2xqrscvivvpwnlcx48r")))

(define-public crate-ckb-rational-0.100.0 (c (n "ckb-rational") (v "0.100.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dn6d6ksfrv50lj2kjl92lzn754f5mnzn213rkbh57qqyjy6ca3q")))

(define-public crate-ckb-rational-0.101.0 (c (n "ckb-rational") (v "0.101.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09q4r32hdka979s7fqvbc341rnbzsyqq6n712x83pspzwg3csqcb")))

(define-public crate-ckb-rational-0.101.1 (c (n "ckb-rational") (v "0.101.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xwpii79m6l104xp6xbq9p9b64s7rvnfg9h77gaz1szccpncgxxq")))

(define-public crate-ckb-rational-0.101.2-testnet1 (c (n "ckb-rational") (v "0.101.2-testnet1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s9bh3n864ws1dglwrbk51gxhb23iga0hq5rlvnaf0v6wqhxizis")))

(define-public crate-ckb-rational-0.101.2 (c (n "ckb-rational") (v "0.101.2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0089yjinif9lbn6b98k3x0v8ygbavlbxiknywfxmrmnji3gc45vv")))

(define-public crate-ckb-rational-0.101.3 (c (n "ckb-rational") (v "0.101.3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00gz2rs0a43ify1zry9zi3bg6xzc4azy0fyfwgi2hi2nf87n6py7")))

(define-public crate-ckb-rational-0.101.4 (c (n "ckb-rational") (v "0.101.4") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16wcsry6wrym1500nn3bhkhn887b37n88894hqib8jwcc8gc40bq")))

(define-public crate-ckb-rational-0.101.5 (c (n "ckb-rational") (v "0.101.5") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04726al18jb6myny8gm7pvrp7sdf6c6v7i4whncd7c98ra42854i") (y #t)))

(define-public crate-ckb-rational-0.101.6 (c (n "ckb-rational") (v "0.101.6") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v9v9n8822g18xncdfq84khz1r00kkiqsxc37756r1y0mvjjqp6d")))

(define-public crate-ckb-rational-0.101.7 (c (n "ckb-rational") (v "0.101.7") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "104p3s67lja73cl4sy0v6kn7q0iyqc87h1flynh4818k98xrp00n")))

(define-public crate-ckb-rational-0.101.8 (c (n "ckb-rational") (v "0.101.8") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kky4dl4rsz3ny4bq28z64w515pbwkh0xb94k0pg0bzvqd9xy66m")))

(define-public crate-ckb-rational-0.102.0 (c (n "ckb-rational") (v "0.102.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18rm82j6i53i1rg4g9vfh49kw1zjn1v7555x8sxcn98x8y04z7h8") (y #t)))

(define-public crate-ckb-rational-0.103.0 (c (n "ckb-rational") (v "0.103.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i0b61hd60ni4ilxfdkr6h1kv7xsb1nqywqy6aq99nmr8pmclhk9")))

(define-public crate-ckb-rational-0.104.0 (c (n "ckb-rational") (v "0.104.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0anikvf699wrqb2syjav2jcbjywkd0ic3wrzk70l2wy8dlll2057")))

(define-public crate-ckb-rational-0.104.1 (c (n "ckb-rational") (v "0.104.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mxsjqp6ply5jaf6j2pq377lm7ldm63j2fhn0akhmy132gzigshb")))

(define-public crate-ckb-rational-0.105.0 (c (n "ckb-rational") (v "0.105.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09awnb5xaww7bb7yknn836j1p7r3zrzh8b3brx3yrj9p7nb5fara") (y #t)))

(define-public crate-ckb-rational-0.105.1 (c (n "ckb-rational") (v "0.105.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n7g6pz4ji08ixfs8z2imcdp9zmw601zjxcfp6r8kmwsbay2cja4") (y #t)))

(define-public crate-ckb-rational-0.106.0 (c (n "ckb-rational") (v "0.106.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cwmbcd0whbz2mk1bqmnll0d35ir4qd0nias05br930kfx9xypps")))

(define-public crate-ckb-rational-0.107.0 (c (n "ckb-rational") (v "0.107.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xxrkg63mnmrm1c3023hz2d1hf5yh61wp62r7jrrrdcgck5lgk7m")))

(define-public crate-ckb-rational-0.108.0 (c (n "ckb-rational") (v "0.108.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g9x3mkci0kl89hrvmxcybh9rii7jc2g6g7vsdcafzylz14r4ld2")))

(define-public crate-ckb-rational-0.108.1 (c (n "ckb-rational") (v "0.108.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jxybry7pbb567mzs9yazkj2n03n2a87nbnf3fjcq5fvmpjs9h4c")))

(define-public crate-ckb-rational-0.109.0 (c (n "ckb-rational") (v "0.109.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m1q9wzsv3q9s2535nnf5spr5c95gfpjl7jdalrmdnfymibd5yaj")))

(define-public crate-ckb-rational-0.110.0 (c (n "ckb-rational") (v "0.110.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06nmfqac4n8r0f9jfjrcyqilrksn97lrf7n4js9wcar34p0sh4hi")))

(define-public crate-ckb-rational-0.110.0-rc1 (c (n "ckb-rational") (v "0.110.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1601z48yamjkv0l4fqfnvaynjc152yrwx50v5hbsqxpdcm26cixd")))

(define-public crate-ckb-rational-0.111.0-rc2 (c (n "ckb-rational") (v "0.111.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "126wk0p16zwjlrm4xxacr50c4wdwgmplwvzwkk0dd9hr2f8m4aix")))

(define-public crate-ckb-rational-0.111.0-rc3 (c (n "ckb-rational") (v "0.111.0-rc3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qasdkb4290kydl1p5kzs1pybvn21fln2g5pwi5dlb216r6rsczl")))

(define-public crate-ckb-rational-0.111.0-rc4 (c (n "ckb-rational") (v "0.111.0-rc4") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0njkkp54m64rllqx2rz46q1f0awcfdj9m2yqg515j6bwkfs0clkh")))

(define-public crate-ckb-rational-0.111.0-rc5 (c (n "ckb-rational") (v "0.111.0-rc5") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10d9yihfjy6zzd4s0zxrnnfhjijzpfncs9a9jbg7c80m12acbpig")))

(define-public crate-ckb-rational-0.111.0-rc6 (c (n "ckb-rational") (v "0.111.0-rc6") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d46kaj8r24w7vjjicqmkwyknq1mdz178m3b4d05ldpdyh2n2zkf")))

(define-public crate-ckb-rational-0.111.0-rc7 (c (n "ckb-rational") (v "0.111.0-rc7") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02dsgy7hbhdzl5zc889npzl1papiyihbyx5nn6iq7q4794x15vrd")))

(define-public crate-ckb-rational-0.111.0-rc8 (c (n "ckb-rational") (v "0.111.0-rc8") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ldrkyhg7d2cl9pycyk4bj688rqc5n075mkwzrgw96vk4msq42hr")))

(define-public crate-ckb-rational-0.110.1-rc1 (c (n "ckb-rational") (v "0.110.1-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qzra5yflrz4ky58kymvjjqqnwmgcb8v0cyff80g4nds5fi06yiq")))

(define-public crate-ckb-rational-0.110.1-rc2 (c (n "ckb-rational") (v "0.110.1-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dm692i2xmjffxrks9509mib5ps5b9hrlphyfisg7c3lpk3kk6j9")))

(define-public crate-ckb-rational-0.111.0-rc9 (c (n "ckb-rational") (v "0.111.0-rc9") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xxkzcaa1ma3i6cbf7p19mxvapba2ngjj9676plr9jl18mnyi13l")))

(define-public crate-ckb-rational-0.111.0-rc10 (c (n "ckb-rational") (v "0.111.0-rc10") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yhx5fksbizfi6jmnakzhz32rzdpm7k8f3mv3zcv3i21klsjxqvi")))

(define-public crate-ckb-rational-0.110.1 (c (n "ckb-rational") (v "0.110.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x0i3f84a2l3wzz4az3ddfdvskhnp9dlffjjy5hph2hi1791sgl8")))

(define-public crate-ckb-rational-0.110.2-rc1 (c (n "ckb-rational") (v "0.110.2-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19dw6nqymdkxfxr52nbkk08035qiw6cx4nmb4bic9sivvhiv8gmw")))

(define-public crate-ckb-rational-0.111.0-rc11 (c (n "ckb-rational") (v "0.111.0-rc11") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02kfyr7kjbcgviz3w73xsxi10vpjihj0v3prsnx9lps556n0d9x6")))

(define-public crate-ckb-rational-0.110.2-rc2 (c (n "ckb-rational") (v "0.110.2-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15aavjilfz7fyp4b9ci18x5vszq5b9h1f5bgnymapsfk7p41izpi")))

(define-public crate-ckb-rational-0.111.0-rc12 (c (n "ckb-rational") (v "0.111.0-rc12") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vbc6b9nvmqil8cxp9vd3yhk0rv90y5afi8vyf1fijdqvlrx9w4f")))

(define-public crate-ckb-rational-0.110.2 (c (n "ckb-rational") (v "0.110.2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kjzij2znrj0v190c0hgfjsrl009gbmgji8id28b0n55c6ccdpz")))

(define-public crate-ckb-rational-0.111.0 (c (n "ckb-rational") (v "0.111.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g7609bkk7ghx9p4vcqz737a3b9jayvqvakq4mm5vc2g38wmjfdx")))

(define-public crate-ckb-rational-0.112.0-rc1 (c (n "ckb-rational") (v "0.112.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00dpq54blnk56qarp3hdf62zf8hdk6vl5ngy5kfpp9z4xrfh6291")))

(define-public crate-ckb-rational-0.112.0-rc2 (c (n "ckb-rational") (v "0.112.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07756p582gm5lpdysdavidkqgl7vnsmdvl5sxgbdd5mh1yfzkdd8")))

(define-public crate-ckb-rational-0.112.0 (c (n "ckb-rational") (v "0.112.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06j55858iin91vcjsalcmc6xd5ss5jw514ba2ckp29n7mn4bs30x")))

(define-public crate-ckb-rational-0.112.0-rc3 (c (n "ckb-rational") (v "0.112.0-rc3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14jq56bd5g1jhml0r9bj201k2gb4r7g35amnhnq19ncak7g6b06p")))

(define-public crate-ckb-rational-0.112.1 (c (n "ckb-rational") (v "0.112.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ldnjaycdmpmmb3x8bfldmspwh35304fyp9agm2i074vypkdld2i")))

(define-public crate-ckb-rational-0.113.0-rc1 (c (n "ckb-rational") (v "0.113.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0090bsrwjyfdsijyhc81zgs3p6jax1h69rcl3p96qdsr5qh9i187")))

(define-public crate-ckb-rational-0.113.0-rc2 (c (n "ckb-rational") (v "0.113.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0haf2sgcrzv9v4iklnygk1n03xmj6d15k28lq3ypxcgbq3iqysak")))

(define-public crate-ckb-rational-0.113.0-rc3 (c (n "ckb-rational") (v "0.113.0-rc3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14z06fca1rqbhl4pd2zdfia8sls1bppgwcns0s5qp2r10z2kfs2g")))

(define-public crate-ckb-rational-0.113.0 (c (n "ckb-rational") (v "0.113.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gcvsdffdnfrj7g3djibqxmgmpmvf9hakhfd58dmwy5hx7vlxmm0")))

(define-public crate-ckb-rational-0.113.1-rc1 (c (n "ckb-rational") (v "0.113.1-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fah0fh8rs1nas86l54anaa2wwascyiyw5m872bsjh6rfq0hamj8")))

(define-public crate-ckb-rational-0.114.0-rc1 (c (n "ckb-rational") (v "0.114.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yfpwys7060bv00bvknvliaga4ri7mfmziqmyyznmc6yf8v8rp54")))

(define-public crate-ckb-rational-0.113.1-rc2 (c (n "ckb-rational") (v "0.113.1-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z1pwdps0pdy43abk96rihqx1avv7w6sc6xjq5gha06hf7r0c4m7")))

(define-public crate-ckb-rational-0.113.1 (c (n "ckb-rational") (v "0.113.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09m0dwzf1jk68rbddw1fxs7w77qdshcs59ixjya55s4sahzapl6h")))

(define-public crate-ckb-rational-0.114.0-rc2 (c (n "ckb-rational") (v "0.114.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mw0gk7xcq3c0hz35fhyfvdfaajzwbk9cls0chc3jfk8a5sipq8i")))

(define-public crate-ckb-rational-0.114.0-rc3 (c (n "ckb-rational") (v "0.114.0-rc3") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vjhkifsbmzcvk9gx2fps56sx74hsb11j09pk7502lvl027c1wrs")))

(define-public crate-ckb-rational-0.114.0 (c (n "ckb-rational") (v "0.114.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15c21alq9qfgn94qq4mrq328fj9jfl3z8473b8d0yzrgx24vx78i")))

(define-public crate-ckb-rational-0.115.0-pre (c (n "ckb-rational") (v "0.115.0-pre") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11lgxbawx0msgp1kmc3rq6qbgjidvn3r320dwdmmlhsa5ww6ajqq")))

(define-public crate-ckb-rational-0.115.0-rc1 (c (n "ckb-rational") (v "0.115.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "001jacq62ladvkkg4ihlkaqc36p2sidsimgcn0bm3nr04rgn75d1")))

(define-public crate-ckb-rational-0.115.0-rc2 (c (n "ckb-rational") (v "0.115.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nw1lny66m4wk53r4qv1rs8yvi3jv0l7lnrcnh90v86iznrcwddz")))

(define-public crate-ckb-rational-0.115.0 (c (n "ckb-rational") (v "0.115.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hn7c0zs6fizxn1fhizzs51im583jg0mvashlvmhi76m806as1z5")))

(define-public crate-ckb-rational-0.116.0-rc1 (c (n "ckb-rational") (v "0.116.0-rc1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14gyz170dbgy609hn4ys8yhavz2klzs76wpgd0kxhn0nshxyhsxp")))

(define-public crate-ckb-rational-0.116.0-rc2 (c (n "ckb-rational") (v "0.116.0-rc2") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jgpnpr67m105vwyx9kz9av42iif2r5lbn0vaskl2ky1jjxlxdkm")))

(define-public crate-ckb-rational-0.116.0 (c (n "ckb-rational") (v "0.116.0") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qf910g85rna1g672dm5zq216379dnvdwyl4mv260188ilysqmn8") (y #t)))

(define-public crate-ckb-rational-0.116.1 (c (n "ckb-rational") (v "0.116.1") (d (list (d (n "numext-fixed-uint") (r "^0.1") (f (quote ("support_rand" "support_heapsize" "support_serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10bk3bfpdyaz9s19vrvclm5wc24cvyawiisqzgxgvzw0dj0hwmzd")))

