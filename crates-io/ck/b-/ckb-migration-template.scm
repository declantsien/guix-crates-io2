(define-module (crates-io ck b- ckb-migration-template) #:use-module (crates-io))

(define-public crate-ckb-migration-template-0.40.0 (c (n "ckb-migration-template") (v "0.40.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "01f523qai33jr315cv23s9xhx0lpksy553kgvkcwbp0c6i5xyk5f")))

(define-public crate-ckb-migration-template-0.42.0 (c (n "ckb-migration-template") (v "0.42.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0c2gd92wfl2r9rggvqw0rpz97iprrpc6zbi08nx9f8kivkwjllhr")))

(define-public crate-ckb-migration-template-0.43.0 (c (n "ckb-migration-template") (v "0.43.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0231vx144ddwf6lmiz3hlmjzv3ahdq1mxns4ngs7wdxg1r7qq41y")))

(define-public crate-ckb-migration-template-0.100.0-rc2 (c (n "ckb-migration-template") (v "0.100.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0azsq0ankgyz2hzcalikbnrcfj6ah5syvdglqmqxhlgv3n8xcvl6")))

(define-public crate-ckb-migration-template-0.43.2 (c (n "ckb-migration-template") (v "0.43.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1lrbssf8ijp0v6342mhn0l374f5j9mx94cv29936w53742gwrq8v")))

(define-public crate-ckb-migration-template-0.100.0-rc4 (c (n "ckb-migration-template") (v "0.100.0-rc4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "18xnsivfi0yj932gss4ab3p93sp62zf4643smjb2p0x19vs10n7n")))

(define-public crate-ckb-migration-template-0.100.0-rc5 (c (n "ckb-migration-template") (v "0.100.0-rc5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "041rkzrvzxp5wvwhmv7n5i21a7w0gjqsli7mjbj9davc7savc9kd")))

(define-public crate-ckb-migration-template-0.100.0 (c (n "ckb-migration-template") (v "0.100.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "09lvbscgz0ihgx7awzc0ilyxsnwimdjzwrx4rx7lfsjx23f18lj4")))

(define-public crate-ckb-migration-template-0.101.0 (c (n "ckb-migration-template") (v "0.101.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1abw4h7jraf2sm35ggls5zf546pr9ix96g2677il67g8d936lvsj")))

(define-public crate-ckb-migration-template-0.101.1 (c (n "ckb-migration-template") (v "0.101.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1mhxfngxqs0dy9b415ris1xns900x96a58k38pc2sdxsj3al4wpy")))

(define-public crate-ckb-migration-template-0.101.2-testnet1 (c (n "ckb-migration-template") (v "0.101.2-testnet1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "05kxdh27pcrp91bzmdq76v3qqxsrcvjidygx3bw08dfzj3k73w7a")))

(define-public crate-ckb-migration-template-0.101.2 (c (n "ckb-migration-template") (v "0.101.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1x117yvsjp648d8hn5d1rlagp12jdx8n3qyvv9s3vwafdm4zgrxq")))

(define-public crate-ckb-migration-template-0.101.3 (c (n "ckb-migration-template") (v "0.101.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0z010rz9lfaa165zxycsqbq11br1590z1nmh1jyqpn978p7as7ic")))

(define-public crate-ckb-migration-template-0.101.4 (c (n "ckb-migration-template") (v "0.101.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0jhcmnqap6rx5fdq9jv7k6f6n8mbik58fqxr1p24fwqy1fqdkzq1")))

(define-public crate-ckb-migration-template-0.101.5 (c (n "ckb-migration-template") (v "0.101.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1cxp2gqhv4vb6fygchafq9sd2ihldn0yd5sb1s8pnwafq25arj2x") (y #t)))

(define-public crate-ckb-migration-template-0.101.6 (c (n "ckb-migration-template") (v "0.101.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1c8cffrpjz69azk5kdqc1bfy8rfkimb0v5d081y1gj7fp4nsv90d")))

(define-public crate-ckb-migration-template-0.101.7 (c (n "ckb-migration-template") (v "0.101.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1i4pqmjx8cma1llqc0y5cz4xnv090j3yb70rkxd1w07srp7j98rb")))

(define-public crate-ckb-migration-template-0.101.8 (c (n "ckb-migration-template") (v "0.101.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "09x74vc6rf4903kvz5xzj9mmhlw2wdpiw5938fi137phgszpfanj")))

(define-public crate-ckb-migration-template-0.102.0 (c (n "ckb-migration-template") (v "0.102.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "026db93mpanlc5fbr04jrzzcqm2023j6vi5mxd7lgay8wwnhd534") (y #t)))

(define-public crate-ckb-migration-template-0.103.0 (c (n "ckb-migration-template") (v "0.103.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1x1kcs48cl7ldgijmksgdivsf7671w79wnzi1kb288y399qzpg97")))

(define-public crate-ckb-migration-template-0.104.0 (c (n "ckb-migration-template") (v "0.104.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0p3azj67vy6yq4384y1kw39pjpxp1rf7l3y31jmh3zig67grs389")))

(define-public crate-ckb-migration-template-0.104.1 (c (n "ckb-migration-template") (v "0.104.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1y8f713s4i7mh8brxayv8gyb78b22m3s87qvhm6w4ich0psmj58a")))

(define-public crate-ckb-migration-template-0.105.0 (c (n "ckb-migration-template") (v "0.105.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "02xzvacz7xbyz6wpcdmd6zqrvmi6sj3d6wjlmkracd4fvpsarzgq") (y #t)))

(define-public crate-ckb-migration-template-0.105.1 (c (n "ckb-migration-template") (v "0.105.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1216fryglkdwwshzh9zlrj5i4hs0sn66zp3i4bpb6vk9jv6bxxir") (y #t)))

(define-public crate-ckb-migration-template-0.106.0 (c (n "ckb-migration-template") (v "0.106.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0vshawfrvxm7bj5091i7w1pqpn4sh5wxj6idcx1qr33q5v36pw1w")))

(define-public crate-ckb-migration-template-0.107.0 (c (n "ckb-migration-template") (v "0.107.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "00sdg9ixxkdiw8ij60dvxpsvymsqjqg0qfq7b78y3qfi8bsv0axm")))

(define-public crate-ckb-migration-template-0.108.0 (c (n "ckb-migration-template") (v "0.108.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0pf5kkdxmb377fsqlb7054wynr4ic0qcd504k4l7y1n2y8388ncb")))

(define-public crate-ckb-migration-template-0.108.1 (c (n "ckb-migration-template") (v "0.108.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1r5ax1n3bmjjkp3ai7cwx6qp7spfdgc8wf0mqk01a99nj4nb7ak0")))

(define-public crate-ckb-migration-template-0.109.0 (c (n "ckb-migration-template") (v "0.109.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "11jpqfgg9jfb53c97c806ws7c3q3qspqb6hjx9fskyc5qizddibb")))

(define-public crate-ckb-migration-template-0.110.0 (c (n "ckb-migration-template") (v "0.110.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1cs373cw26ccppk9pikk73pqcn2f1sidi9zn97n9305035jg6srr")))

(define-public crate-ckb-migration-template-0.110.0-rc1 (c (n "ckb-migration-template") (v "0.110.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0qv9kfrd50ya75aw29jhm1hkbln6gxid20qi1yk07cwq235bd158")))

(define-public crate-ckb-migration-template-0.111.0-rc2 (c (n "ckb-migration-template") (v "0.111.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "056qnrshlxjq8j8dz345jl27c5lpyza19caj241sm9a17xrb6iqc")))

(define-public crate-ckb-migration-template-0.111.0-rc3 (c (n "ckb-migration-template") (v "0.111.0-rc3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1p0bxb0lybai8xrdg074afr7bjdkvb1f800fllpkn4li9cpqq853")))

(define-public crate-ckb-migration-template-0.111.0-rc4 (c (n "ckb-migration-template") (v "0.111.0-rc4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1m705wr7b1nbsdpknfiv9q8f4704ag316vcqrwzrxv7s1fiz4bvg")))

(define-public crate-ckb-migration-template-0.111.0-rc5 (c (n "ckb-migration-template") (v "0.111.0-rc5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "04gip5n3yfpv4gsimzckjqrgw8qc01wg89nblm853qpll9h2a83j")))

(define-public crate-ckb-migration-template-0.111.0-rc6 (c (n "ckb-migration-template") (v "0.111.0-rc6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0ibk409j6bpiij35l1p7ng2i2fpmjx7xm9x2yjsqw88lf9pcxz7a")))

(define-public crate-ckb-migration-template-0.111.0-rc7 (c (n "ckb-migration-template") (v "0.111.0-rc7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0p3ns05fins20wnfg5xy23l59xy0yszlyha1qcyx4mrg624cm9mi")))

(define-public crate-ckb-migration-template-0.111.0-rc8 (c (n "ckb-migration-template") (v "0.111.0-rc8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0dapx923qdmb28k4i9mj2hz2yxcd86i7w5wrv4zs0nlrzlvk344h")))

(define-public crate-ckb-migration-template-0.110.1-rc1 (c (n "ckb-migration-template") (v "0.110.1-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "175p5dx38l8bm4j3hgyq1mfbh3yxb4ni7kad66nbwivv5l6k2bcv")))

(define-public crate-ckb-migration-template-0.110.1-rc2 (c (n "ckb-migration-template") (v "0.110.1-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "195lvwbz84493idyiqyf8n62za3c2fg1digcj3f927q3dsps6mgl")))

(define-public crate-ckb-migration-template-0.111.0-rc9 (c (n "ckb-migration-template") (v "0.111.0-rc9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0l9ykc7w259zp5rag1s8q3sa69jp1254hrvk30w3qp3nhm7fvyps")))

(define-public crate-ckb-migration-template-0.111.0-rc10 (c (n "ckb-migration-template") (v "0.111.0-rc10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1m1qvwn0s6i0bv4gbzypz5gnnzhf268acqwdka7lh048vda24aq8")))

(define-public crate-ckb-migration-template-0.110.1 (c (n "ckb-migration-template") (v "0.110.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "074la9r3as0w2p5vzfswprp3xdyifpqcvfjpw8hsngc87yyyxwp8")))

(define-public crate-ckb-migration-template-0.110.2-rc1 (c (n "ckb-migration-template") (v "0.110.2-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "06mpir3a8bk1y63q15mdvya533bbbqkl4m8mx1d1xf0s3aa840dy")))

(define-public crate-ckb-migration-template-0.111.0-rc11 (c (n "ckb-migration-template") (v "0.111.0-rc11") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0m0030fmir2vpabvxrq46a1xi2b7ljrcla99qavaa9h0x2ya0wvl")))

(define-public crate-ckb-migration-template-0.110.2-rc2 (c (n "ckb-migration-template") (v "0.110.2-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1nmlj9f9g4nvx28lblmlgx9dbrszww466xgl4h5sdqds9l9dfvh2")))

(define-public crate-ckb-migration-template-0.111.0-rc12 (c (n "ckb-migration-template") (v "0.111.0-rc12") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0xv1fr9flaxrw2gz5dw6iz1s5czqip1p84bb0w995fzz1br87all")))

(define-public crate-ckb-migration-template-0.110.2 (c (n "ckb-migration-template") (v "0.110.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "102k5kxdv0qswzv72pz13rbxjrw81422x8wzvwi3fi5b8fdj7qw7")))

(define-public crate-ckb-migration-template-0.111.0 (c (n "ckb-migration-template") (v "0.111.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1zd73ng0il0nkn1rmdfrnks6glsgyns6hq0mp8h3hwnqas28czw9")))

(define-public crate-ckb-migration-template-0.112.0-rc1 (c (n "ckb-migration-template") (v "0.112.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1wlx4dry20fb3878i7rrqsxjkcgk1lkh3pah0z8jjdak0wfn9a4l")))

(define-public crate-ckb-migration-template-0.112.0-rc2 (c (n "ckb-migration-template") (v "0.112.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0wf2gx33h3xqz1z6w1szmjdxb3zvg78rkxrg070spaga130sa98f")))

(define-public crate-ckb-migration-template-0.112.0 (c (n "ckb-migration-template") (v "0.112.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "13yr98hhsb08xngwcn8qzk30pq9pvyb3kx6m81gbwrnpdyz28x09")))

(define-public crate-ckb-migration-template-0.112.0-rc3 (c (n "ckb-migration-template") (v "0.112.0-rc3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0zjl28bx2dlasgvixg96yzlnsk8njwaxqy4q6yzdx88cfchd5vrh")))

(define-public crate-ckb-migration-template-0.112.1 (c (n "ckb-migration-template") (v "0.112.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1ic16ry9m7ci7810gm0vdfs4qhdw0n2q3lzn038m42i9dpnnpds3")))

(define-public crate-ckb-migration-template-0.113.0-rc1 (c (n "ckb-migration-template") (v "0.113.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "14q1cbxs4mk5269c00cwq6gf8h4mz06zdfr175700clml23mghgz")))

(define-public crate-ckb-migration-template-0.113.0-rc2 (c (n "ckb-migration-template") (v "0.113.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "173q3964ddcl89paqam5b2yjpgims3f19ys579nn2fgnkj7y03yk")))

(define-public crate-ckb-migration-template-0.113.0-rc3 (c (n "ckb-migration-template") (v "0.113.0-rc3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0hs8dphwlzwx3vyv5q1n393k23qhi1zwbgy59gccmhk1glmwq0qr")))

(define-public crate-ckb-migration-template-0.113.0 (c (n "ckb-migration-template") (v "0.113.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "01059bzp88r3137qmy0rn0x4q5chrcy3d9wxxvc48nlh8wy7fjcl")))

(define-public crate-ckb-migration-template-0.113.1-rc1 (c (n "ckb-migration-template") (v "0.113.1-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1da55x09baqlzx0xhsg26gwn6pyrypafvcm7q966wzyhqfr7lvf0")))

(define-public crate-ckb-migration-template-0.114.0-rc1 (c (n "ckb-migration-template") (v "0.114.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1jad6k2k4h47mkw4d4wf4i8ld30cmg1z00bdrvkw29cs30qbslhl")))

(define-public crate-ckb-migration-template-0.113.1-rc2 (c (n "ckb-migration-template") (v "0.113.1-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0lwi5x52di2sx5fqa88mm4l2wxck4raw24jn23rj78brbxql00i4")))

(define-public crate-ckb-migration-template-0.113.1 (c (n "ckb-migration-template") (v "0.113.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0fy04bccl9ijv2j0czx3kd9wbfdh03dsm4qwl53v3fxxr6bjsa3m")))

(define-public crate-ckb-migration-template-0.114.0-rc2 (c (n "ckb-migration-template") (v "0.114.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0qfwklvr6zpapsd3myah9rgniscxcdxjxisa2h0jkfp3xdcd00jq")))

(define-public crate-ckb-migration-template-0.114.0-rc3 (c (n "ckb-migration-template") (v "0.114.0-rc3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1bbfzlbgz3vjcwcld47l5wqqr61jrsgvwrs53phz86fmp81x4lgp")))

(define-public crate-ckb-migration-template-0.114.0 (c (n "ckb-migration-template") (v "0.114.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1yfi1bzbfpz7ai77vbkm4x9an7dprvinhmrxvf4hm7n1yp70h146")))

(define-public crate-ckb-migration-template-0.115.0-pre (c (n "ckb-migration-template") (v "0.115.0-pre") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0dxj57azk2s2rjnraniv4rybppjgf107dpy8k53s5wjxswibxk2c")))

(define-public crate-ckb-migration-template-0.115.0-rc1 (c (n "ckb-migration-template") (v "0.115.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "119xrakgjywk96cs3aadx021bjbklhyap2yd3vqk9941l3qfnk8p")))

(define-public crate-ckb-migration-template-0.115.0-rc2 (c (n "ckb-migration-template") (v "0.115.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "03dzldfdmrrx4q453mcfj0sxsmf0p98kjivavf3gaargh9bkcsav")))

(define-public crate-ckb-migration-template-0.115.0 (c (n "ckb-migration-template") (v "0.115.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1ld81mscxdqv4ak8849mj4ya74l2fczpz26fwrnidqx3cpm4sc87")))

(define-public crate-ckb-migration-template-0.116.0-rc1 (c (n "ckb-migration-template") (v "0.116.0-rc1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1d6vl9gp9zlj4cwcmx59fpdlil62b1y7vz0x6rh1q20zibx9zpb2")))

(define-public crate-ckb-migration-template-0.116.0-rc2 (c (n "ckb-migration-template") (v "0.116.0-rc2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "11vglcvr8dbq3bz0wl21h19kma9rajla3ng7d4036iayig8nj31g")))

(define-public crate-ckb-migration-template-0.116.0 (c (n "ckb-migration-template") (v "0.116.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0nq0zh6qljhpsmjz0q9sj8dfsn460cjyr06prgldqy9iwrmj8rlg") (y #t)))

(define-public crate-ckb-migration-template-0.116.1 (c (n "ckb-migration-template") (v "0.116.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1fpkxqqh958074dh7rdrhza0x0hb40m1nmz11q9n81vlwbgcwl3p")))

