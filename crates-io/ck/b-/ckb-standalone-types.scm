(define-module (crates-io ck b- ckb-standalone-types) #:use-module (crates-io))

(define-public crate-ckb-standalone-types-0.0.1-pre.1 (c (n "ckb-standalone-types") (v "0.0.1-pre.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "molecule") (r "^0.6") (k 0)))) (h "027g1qxlmfplcgzxlijb1dyygg44d4ys1x842nkisnzfcpgxnb5g") (f (quote (("std" "molecule/std") ("default" "std"))))))

(define-public crate-ckb-standalone-types-0.1.1 (c (n "ckb-standalone-types") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "molecule") (r "^0.7") (k 0)))) (h "15qj3f32ll63sxskij1h0gzqzlkkifxrjcaa1j4lqmswywzza1y1") (f (quote (("std" "molecule/std") ("default" "std"))))))

(define-public crate-ckb-standalone-types-0.1.2 (c (n "ckb-standalone-types") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "molecule") (r "^0.7") (k 0)))) (h "1k81g1vqkpp4mfjdxgq67h55ly98xjzqikq2l44virlnmfywpmr2") (f (quote (("std" "molecule/std") ("default" "std"))))))

(define-public crate-ckb-standalone-types-0.1.3 (c (n "ckb-standalone-types") (v "0.1.3") (d (list (d (n "blake2b-ref") (r "^0.3.1") (o #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "molecule") (r "^0.7") (k 0)))) (h "1f33h491xnd0wlhxyi07zf8zjk5qvmgw1k9p7i571s86lyhykbxc") (f (quote (("std" "molecule/std") ("default" "std") ("calc-hash" "blake2b-ref"))))))

(define-public crate-ckb-standalone-types-0.1.4 (c (n "ckb-standalone-types") (v "0.1.4") (d (list (d (n "blake2b-ref") (r "^0.3.1") (o #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "molecule") (r "^0.7") (k 0)))) (h "0iq4fpx0njg9h2dnzsih96gp68zkmgn7yk4cyc9zww5jv5q6886x") (f (quote (("std" "molecule/std") ("default" "std") ("calc-hash" "blake2b-ref"))))))

(define-public crate-ckb-standalone-types-0.1.5 (c (n "ckb-standalone-types") (v "0.1.5") (d (list (d (n "blake2b-ref") (r "^0.3.1") (o #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "molecule") (r "^0.7") (k 0)))) (h "17v0jky10rlzmmis1i606f6xmckpdmj5f20qlci0mxml1vbpdixm") (f (quote (("std" "molecule/std") ("default" "std") ("calc-hash" "blake2b-ref"))))))

